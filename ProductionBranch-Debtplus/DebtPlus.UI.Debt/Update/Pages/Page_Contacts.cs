#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Windows.Forms;
using DebtPlus.LINQ;
using System.Linq;

namespace DebtPlus.UI.Debt.Update.pages
{
    internal partial class Page_Contacts : DebtPlus.UI.Debt.Update.controls.ControlBaseDebt
    {
        public Page_Contacts()
            : base()
        {
            InitializeComponent();
        }

        #region " Windows Form Designer generated code "

        internal DevExpress.XtraEditors.LabelControl balance_verify_by;
        internal DevExpress.XtraEditors.LabelControl balance_verify_date;
        internal DevExpress.XtraEditors.ComboBoxEdit client_name;
        internal DevExpress.XtraEditors.TextEdit contact_name;
        internal DevExpress.XtraEditors.LabelControl LabelControl1;
        internal DevExpress.XtraEditors.LabelControl LabelControl2;
        internal DevExpress.XtraEditors.LabelControl LabelControl3;
        internal DevExpress.XtraEditors.LabelControl LabelControl4;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        private System.ComponentModel.IContainer components = null;

        //UserControl overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.contact_name = new DevExpress.XtraEditors.TextEdit();
            this.client_name = new DevExpress.XtraEditors.ComboBoxEdit();
            this.balance_verify_date = new DevExpress.XtraEditors.LabelControl();
            this.balance_verify_by = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)this.contact_name.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.client_name.Properties).BeginInit();
            this.SuspendLayout();
            //
            //LabelControl1
            //
            this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl1.Location = new System.Drawing.Point(8, 20);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(146, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Primary &Account Holder Name:";
            //
            //LabelControl2
            //
            this.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl2.Location = new System.Drawing.Point(69, 44);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(85, 13);
            this.LabelControl2.TabIndex = 2;
            this.LabelControl2.Text = "Name of &Contact:";
            //
            //LabelControl3
            //
            this.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl3.Location = new System.Drawing.Point(74, 73);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(80, 13);
            this.LabelControl3.TabIndex = 4;
            this.LabelControl3.Text = "Balance Verified:";
            //
            //LabelControl4
            //
            this.LabelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl4.Location = new System.Drawing.Point(256, 73);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(12, 13);
            this.LabelControl4.TabIndex = 6;
            this.LabelControl4.Text = "by";
            //
            //contact_name
            //
            this.contact_name.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.contact_name.Location = new System.Drawing.Point(168, 40);
            this.contact_name.Name = "contact_name";
            this.contact_name.Size = new System.Drawing.Size(272, 20);
            this.contact_name.TabIndex = 3;
            //
            //client_name
            //
            this.client_name.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.client_name.Location = new System.Drawing.Point(168, 16);
            this.client_name.Name = "client_name";
            this.client_name.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.client_name.Size = new System.Drawing.Size(272, 20);
            this.client_name.TabIndex = 1;
            //
            //balance_verify_date
            //
            this.balance_verify_date.Location = new System.Drawing.Point(168, 72);
            this.balance_verify_date.Name = "balance_verify_date";
            this.balance_verify_date.Size = new System.Drawing.Size(0, 13);
            this.balance_verify_date.TabIndex = 5;
            this.balance_verify_date.UseMnemonic = false;
            //
            //balance_verify_by
            //
            this.balance_verify_by.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.balance_verify_by.Location = new System.Drawing.Point(280, 72);
            this.balance_verify_by.Name = "balance_verify_by";
            this.balance_verify_by.Size = new System.Drawing.Size(0, 13);
            this.balance_verify_by.TabIndex = 7;
            this.balance_verify_by.UseMnemonic = false;
            //
            //Page_Contacts
            //
            this.Controls.Add(this.balance_verify_by);
            this.Controls.Add(this.balance_verify_date);
            this.Controls.Add(this.client_name);
            this.Controls.Add(this.contact_name);
            this.Controls.Add(this.LabelControl4);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.LabelControl1);
            this.Name = "Page_Contacts";
            this.Size = new System.Drawing.Size(456, 96);
            ((System.ComponentModel.ISupportInitialize)this.contact_name.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.client_name.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion " Windows Form Designer generated code "

        private DebtPlus.Interfaces.Debt.IDebtRecord currentDebtRecord;

        /// <summary>
        /// Read and display the current values when we switch to our tab
        /// </summary>
        public override void ReadForm(DebtPlus.Interfaces.Debt.IDebtRecord CurrentDebtRecord)
        {
            // Save the pointer to the debt record for the save operation
            this.currentDebtRecord = CurrentDebtRecord;

            // Load the list of people into the client_name table
            client_name.Properties.Items.Clear();
            using (var bc = new DebtPlus.LINQ.BusinessContext())
            {
                foreach (var q in bc.peoples.Join(bc.Names, p => p.NameID, n => n.Id, (p,n) => new { Client = p.Client, id = p.Id, n = n }).Where(s => s.Client == currentDebtRecord.ClientId))
                {
                    client_name.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(q.n.ToString(), q.id, true));
                }
            }

            // Process the information if we have pointers to the data
            contact_name.DataBindings.Clear();
            contact_name.DataBindings.Add("EditValue", currentDebtRecord, contact_name.Name, false, DataSourceUpdateMode.OnPropertyChanged);

            System.DateTime bal_verify_date = default(System.DateTime);

            if (currentDebtRecord.balance_verify_date != null && !object.ReferenceEquals(currentDebtRecord.balance_verify_date, DBNull.Value))
            {
                bal_verify_date = Convert.ToDateTime(currentDebtRecord.balance_verify_date);
                balance_verify_date.Text = bal_verify_date.ToShortDateString();
            }
            else
            {
                bal_verify_date = System.DateTime.MaxValue;
                balance_verify_date.Text = string.Empty;
            }

            string bal_verify_by = null;
            if (currentDebtRecord.balance_verify_by != null && !object.ReferenceEquals(currentDebtRecord.balance_verify_by, DBNull.Value))
            {
                bal_verify_by = Convert.ToString(currentDebtRecord.balance_verify_by).Trim();
            }
            else
            {
                bal_verify_by = string.Empty;
            }
            balance_verify_by.Text = bal_verify_by;

            // Select that person in the list
            client_name.SelectedIndex = -1;
            if (currentDebtRecord.client_name != null && !object.ReferenceEquals(currentDebtRecord.client_name, DBNull.Value))
            {
                client_name.Text = Convert.ToString(currentDebtRecord.client_name).Trim();
            }

            // Choose the corresponding item from the list according to the person field.
            if (currentDebtRecord.person > 0)
            {
                Int32 NewPerson = currentDebtRecord.person;
                foreach (DebtPlus.Data.Controls.ComboboxItem row in client_name.Properties.Items)
                {
                    if (Convert.ToInt32(row.value) == NewPerson)
                    {
                        client_name.SelectedItem = row;
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Save any changes to the forms when we switch away from our tab
        /// </summary>
        public override void SaveForm()
        {
            // Update the person field from the form.
            if (client_name.SelectedIndex < 0)
            {
                currentDebtRecord.person = 0;
                currentDebtRecord.client_name = client_name.Text.Trim();
            }
            else
            {
                currentDebtRecord.person = (Int32)((DebtPlus.Data.Controls.ComboboxItem)client_name.SelectedItem).value;
                currentDebtRecord.client_name = string.Empty;
            }
        }
    }
}
