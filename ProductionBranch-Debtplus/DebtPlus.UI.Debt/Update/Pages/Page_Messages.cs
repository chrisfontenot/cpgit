#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;
using DebtPlus.UI.Debt.Update.forms;

namespace DebtPlus.UI.Debt.Update.pages
{
    internal partial class Page_Messages : DebtPlus.UI.Debt.Update.controls.MyGridControlDebt
    {
        private System.Collections.Generic.List<debt_note> col;

        public Page_Messages()
            : base()
        {
            InitializeComponent();
            EnableMenus = true;
        }

        #region " Windows Form Designer generated code "

        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_bank;

        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_client;

        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_client_creditor;

        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Created_By;

        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_creditor;

        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_creditor_method;

        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_date_created;

        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_note_amount;

        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_note_date;

        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_note_text;

        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_output_batch;

        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_type;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraGrid.StyleFormatCondition StyleFormatCondition1;

        private System.ComponentModel.IContainer components = null;

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.StyleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.GridColumn_output_batch = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_output_batch.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_note_date = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_note_date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_note_amount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_note_amount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_note_text = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_note_text.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_Created_By = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Created_By.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_date_created = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_type = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_client = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_client.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_creditor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_creditor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_client_creditor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_client_creditor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_creditor_method = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_creditor_method.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_bank = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_bank.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            ((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
            this.SuspendLayout();
            //
            //GridView1
            //
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(59), Convert.ToByte(133), Convert.ToByte(195));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(38), Convert.ToByte(109), Convert.ToByte(189));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(59), Convert.ToByte(139), Convert.ToByte(206));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(139), Convert.ToByte(201), Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(139), Convert.ToByte(201), Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(105), Convert.ToByte(170), Convert.ToByte(225));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5f);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
				this.GridColumn_note_date,
				this.GridColumn_note_amount,
				this.GridColumn_note_text,
				this.GridColumn_Created_By,
				this.GridColumn_output_batch,
				this.GridColumn_date_created,
				this.GridColumn_type,
				this.GridColumn_client,
				this.GridColumn_creditor,
				this.GridColumn_client_creditor,
				this.GridColumn_creditor_method,
				this.GridColumn_bank
			});
            StyleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(255), Convert.ToByte(128), Convert.ToByte(128));
            StyleFormatCondition1.Appearance.BackColor2 = System.Drawing.Color.FromArgb(Convert.ToByte(255), Convert.ToByte(192), Convert.ToByte(192));
            StyleFormatCondition1.Appearance.BorderColor = System.Drawing.Color.Red;
            StyleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Black;
            StyleFormatCondition1.Appearance.Options.UseBackColor = true;
            StyleFormatCondition1.Appearance.Options.UseBorderColor = true;
            StyleFormatCondition1.Appearance.Options.UseForeColor = true;
            StyleFormatCondition1.ApplyToRow = true;
            StyleFormatCondition1.Column = this.GridColumn_output_batch;
            StyleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.NotEqual;
            StyleFormatCondition1.Value1 = null;
            this.GridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] { StyleFormatCondition1 });
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsBehavior.AutoPopulateColumns = false;
            this.GridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.GridView1.OptionsView.EnableAppearanceOddRow = true;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            //
            //GridControl1.EmbeddedNavigator
            //
            this.GridControl1.EmbeddedNavigator.Name = "";
            this.GridControl1.Name = "GridControl1";
            //
            //GridColumn_output_batch
            //
            this.GridColumn_output_batch.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_output_batch.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn_output_batch.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_output_batch.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn_output_batch.Caption = "Batch ID";
            this.GridColumn_output_batch.CustomizationCaption = "Batch ID";
            this.GridColumn_output_batch.FieldName = "output_batch";
            this.GridColumn_output_batch.Name = "GridColumn_output_batch";
            //
            //GridColumn_note_date
            //
            this.GridColumn_note_date.Caption = "Date";
            this.GridColumn_note_date.CustomizationCaption = "Note Date";
            this.GridColumn_note_date.FieldName = "note_date";
            this.GridColumn_note_date.Name = "GridColumn_note_date";
            this.GridColumn_note_date.Visible = true;
            this.GridColumn_note_date.VisibleIndex = 0;
            this.GridColumn_note_date.Width = 77;
            this.GridColumn_note_date.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_note_date.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn_note_date.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_note_date.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn_note_date.DisplayFormat.FormatString = "{0:d}";
            this.GridColumn_note_date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_note_date.GroupFormat.FormatString = "{0:d}";
            this.GridColumn_note_date.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_note_date.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Date;
            //
            //GridColumn_note_amount
            //
            this.GridColumn_note_amount.Caption = "Amount";
            this.GridColumn_note_amount.CustomizationCaption = "Amount";
            this.GridColumn_note_amount.FieldName = "note_amount";
            this.GridColumn_note_amount.Name = "GridColumn_note_amount";
            this.GridColumn_note_amount.Visible = true;
            this.GridColumn_note_amount.VisibleIndex = 1;
            this.GridColumn_note_amount.Width = 82;
            this.GridColumn_note_amount.DisplayFormat.FormatString = "{0:c}";
            this.GridColumn_note_amount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_note_amount.GroupFormat.FormatString = "{0:c}";
            this.GridColumn_note_amount.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_note_amount.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Value;
            //
            //GridColumn_note_text
            //
            this.GridColumn_note_text.Caption = "Note";
            this.GridColumn_note_text.CustomizationCaption = "Note";
            this.GridColumn_note_text.FieldName = "note_text";
            this.GridColumn_note_text.Name = "GridColumn_note_text";
            this.GridColumn_note_text.Visible = true;
            this.GridColumn_note_text.VisibleIndex = 2;
            this.GridColumn_note_text.Width = 354;
            //
            //GridColumn_Created_By
            //
            this.GridColumn_Created_By.Caption = "Creator";
            this.GridColumn_Created_By.CustomizationCaption = "Person who created the note";
            this.GridColumn_Created_By.FieldName = "created_by";
            this.GridColumn_Created_By.Name = "GridColumn_Created_By";
            this.GridColumn_Created_By.Visible = true;
            this.GridColumn_Created_By.VisibleIndex = 3;
            this.GridColumn_Created_By.Width = 84;
            //
            //GridColumn_date_created
            //
            this.GridColumn_date_created.Caption = "Created";
            this.GridColumn_date_created.CustomizationCaption = "Date Created";
            this.GridColumn_date_created.FieldName = "date_created";
            this.GridColumn_date_created.Name = "GridColumn_date_created";
            this.GridColumn_date_created.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_date_created.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn_date_created.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_date_created.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn_date_created.DisplayFormat.FormatString = "{0:d}";
            this.GridColumn_date_created.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_date_created.GroupFormat.FormatString = "{0:d}";
            this.GridColumn_date_created.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_date_created.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Date;
            //
            //GridColumn_type
            //
            this.GridColumn_type.Caption = "Type";
            this.GridColumn_type.CustomizationCaption = "Type";
            this.GridColumn_type.FieldName = "type";
            this.GridColumn_type.Name = "GridColumn_type";
            //
            //GridColumn_client
            //
            this.GridColumn_client.Caption = "Client";
            this.GridColumn_client.CustomizationCaption = "Client";
            this.GridColumn_client.FieldName = "client";
            this.GridColumn_client.Name = "GridColumn_client";
            //
            //GridColumn_creditor
            //
            this.GridColumn_creditor.Caption = "Creditor";
            this.GridColumn_creditor.CustomizationCaption = "Creditor";
            this.GridColumn_creditor.FieldName = "creditor";
            this.GridColumn_creditor.Name = "GridColumn_creditor";
            //
            //GridColumn_client_creditor
            //
            this.GridColumn_client_creditor.Caption = "Debt ID";
            this.GridColumn_client_creditor.CustomizationCaption = "Debt ID";
            this.GridColumn_client_creditor.FieldName = "client_creditor";
            this.GridColumn_client_creditor.Name = "GridColumn_client_creditor";
            //
            //GridColumn_creditor_method
            //
            this.GridColumn_creditor_method.Caption = "Creditor Method";
            this.GridColumn_creditor_method.CustomizationCaption = "Creditor Method ID";
            this.GridColumn_creditor_method.FieldName = "creditor_method";
            this.GridColumn_creditor_method.Name = "GridColumn_creditor_method";
            //
            //GridColumn_bank
            //
            this.GridColumn_bank.Caption = "Bank ID";
            this.GridColumn_bank.CustomizationCaption = "Bank ID";
            this.GridColumn_bank.FieldName = "bank";
            this.GridColumn_bank.Name = "GridColumn_bank";
            //
            //Page_Messages
            //
            this.Name = "Page_Messages";
            ((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "
        private DebtPlus.Interfaces.Debt.IDebtRecord currentDebtRecord;

        /// <summary>
        /// Read the data from the database and link it to the form
        /// </summary>
        public override void ReadForm(DebtPlus.Interfaces.Debt.IDebtRecord CurrentDebtRecord)
        {
            this.currentDebtRecord = CurrentDebtRecord;
            try
            {
                using (BusinessContext dc = new BusinessContext())
                {
                    col = dc.debt_notes.Where(n => n.client_creditor != null && n.client_creditor.Value == currentDebtRecord.client_creditor && n.type == DebtPlus.LINQ.debt_note.Type_AccountNote).ToList();
                    GridControl1.DataSource = col;
                    GridControl1.RefreshDataSource();
                    EnableDisableFocusRow();
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading debt_notes");
            }
        }

        protected override void OnCreate()
        {
            var note = DebtPlus.LINQ.Factory.Manufacture_debt_note(currentDebtRecord.ClientId, currentDebtRecord.client_creditor);
            note.type = DebtPlus.LINQ.debt_note.Type_AccountNote;

            // Edit the blank note
            using (var frm = new Form_Message(col, note))
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
            }

            using (var dc = new BusinessContext())
            {
                try
                {
                    dc.debt_notes.InsertOnSubmit(note);
                    dc.SubmitChanges();
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error saving debt_notes");
                }
                col.Add(note);
                GridControl1.RefreshDataSource();
                GridView1.RefreshData();
            }
        }

        /// <summary>
        /// Process the create operation
        /// </summary>
        /// <summary>
        /// Process the delete operation
        /// </summary>
        protected override void OnDelete(object obj)
        {
            debt_note note = obj as debt_note;
            if (note == null)
            {
                return;
            }

            if (!string.IsNullOrEmpty(note.output_batch))
            {
                DebtPlus.Data.Forms.MessageBox.Show("The note has been sent to the creditor and may not be deleted at this time.", "Sorry, but this is not possible", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() != DialogResult.Yes)
            {
                return;
            }

            using (var dc = new BusinessContext())
            {
                var q = dc.debt_notes.Where(n => n.Id == note.Id).FirstOrDefault();
                if (q != null)
                {
                    try
                    {
                        dc.debt_notes.DeleteOnSubmit(q);
                        dc.SubmitChanges();

                        col.Remove(note);
                    }
                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error saving debt_notes");
                    }

                    GridControl1.RefreshDataSource();
                    GridView1.RefreshData();
                }
            }
        }

        /// <summary>
        /// Process the edit operation
        /// </summary>
        protected override void OnEdit(object obj)
        {
            debt_note note = obj as debt_note;
            if (note == null)
            {
                return;
            }

            // Edit the note
            using (var frm = new Form_Message(col, note))
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
            }

            // Find the note in the tables. If the entry is found then update the table with the new note value
            // and write it to the database
            using (BusinessContext dc = new BusinessContext())
            {
                try
                {
                    var q = dc.debt_notes.Where(n => n.Id == note.Id).SingleOrDefault();
                    if (q != null)
                    {
                        q.bank                     = note.bank;
                        q.client                   = note.client;
                        q.client_creditor          = note.client_creditor;
                        q.client_creditor_proposal = note.client_creditor_proposal;
                        q.creditor_method          = note.creditor_method;
                        q.drop_reason              = note.drop_reason;
                        q.note_amount              = note.note_amount;
                        q.note_date                = note.note_date;
                        q.note_text                = note.note_text;
                        q.output_batch             = note.output_batch;
                        q.rpps_biller_id           = note.rpps_biller_id;
                        q.rpps_transaction         = note.rpps_transaction;
                        q.type                     = note.type;
                    }
                    else
                    {
                        // Not found. Do a simple insert at this point to re-add it
                        dc.debt_notes.InsertOnSubmit(note);
                    }

                    dc.SubmitChanges();
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error saving debt_notes");
                }
            }
        }

        /// <summary>
        /// Enable the focus row if there are two or more rows in the table
        /// </summary>
        /// <remarks></remarks>
        private void EnableDisableFocusRow()
        {
            if (col.Count < 2)
            {
                GridView1.OptionsSelection.EnableAppearanceFocusedCell   = false;
                GridView1.OptionsSelection.EnableAppearanceFocusedRow    = false;
                GridView1.OptionsSelection.EnableAppearanceHideSelection = false;
            }
            else
            {
                GridView1.OptionsSelection.EnableAppearanceFocusedCell   = true;
                GridView1.OptionsSelection.EnableAppearanceFocusedRow    = true;
                GridView1.OptionsSelection.EnableAppearanceHideSelection = true;
            }
        }

        /// <summary>
        /// Process changes to the messages table. We need to update the focus row
        /// to control the enable/disable as rows are added/deleted from the table.
        /// </summary>
        /// <param name="Sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void UpdateTable_ControlFocus(object Sender, EventArgs e)
        {
            EnableDisableFocusRow();
        }
    }
}
