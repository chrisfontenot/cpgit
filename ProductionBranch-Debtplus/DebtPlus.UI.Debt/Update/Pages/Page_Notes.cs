#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Windows.Forms;
using DebtPlus.UI.Debt.Update.controls;

namespace DebtPlus.UI.Debt.Update.pages
{
    internal partial class Page_Notes : ControlBaseDebt
    {
        public Page_Notes() : base()
        {
            InitializeComponent();
        }

        #region " Windows Form Designer generated code "

        internal NotesGrid NotesGrid1;

        private System.ComponentModel.IContainer components = null;

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.NotesGrid1 = new DebtPlus.UI.Debt.Update.controls.NotesGrid();
            this.SuspendLayout();
            //
            //NotesGrid1
            //
            this.NotesGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NotesGrid1.Location = new System.Drawing.Point(0, 0);
            this.NotesGrid1.Margin = new System.Windows.Forms.Padding(0);
            this.NotesGrid1.Name = "NotesGrid1";
            this.NotesGrid1.Size = new System.Drawing.Size(576, 296);
            this.NotesGrid1.TabIndex = 2;
            //
            //Page_Notes
            //
            this.Controls.Add(this.NotesGrid1);
            this.Name = "Page_Notes";
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        private DebtPlus.Interfaces.Debt.IDebtRecord currentDebtRecord;
        public override void ReadForm(DebtPlus.Interfaces.Debt.IDebtRecord CurrentDebtRecord)
        {
            this.currentDebtRecord = CurrentDebtRecord;
            NotesGrid1.ReadForm(currentDebtRecord);
        }

        private void CheckEdit_Alerts_CheckStateChanged(object sender, EventArgs e)
        {
            RefreshGrid();
        }

        private void RefreshGrid()
        {
            if (InvokeRequired)
            {
                IAsyncResult ia = BeginInvoke(new MethodInvoker(RefreshGrid));
                object Result = EndInvoke(ia);
            }
            else
            {
                NotesGrid1.RefreshList();
            }
        }
    }
}
