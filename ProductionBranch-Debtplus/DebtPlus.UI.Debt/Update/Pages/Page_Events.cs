#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;
using DebtPlus.UI.Debt.Update.forms;

namespace DebtPlus.UI.Debt.Update.pages
{
    internal partial class Page_Events : DebtPlus.UI.Debt.Update.controls.MyGridControlDebt
    {
        private System.Collections.Generic.List<DebtPlus.LINQ.client_creditor_event> colClientCreditorEvents;
        private System.Collections.Generic.List<DebtPlus.LINQ.config_fee> colConfigFee;

        public Page_Events()
            : base()
        {
            InitializeComponent();
            EnableMenus = true;
        }

        private DebtPlus.Interfaces.Debt.IDebtRecord currentDebtRecord;
        public override void ReadForm(DebtPlus.Interfaces.Debt.IDebtRecord CurrentDebtRecord)
        {
            // Save the pointer to the debt record for the save operation
            this.currentDebtRecord = CurrentDebtRecord;

            if (colClientCreditorEvents == null)
            {
                using (DebtPlus.LINQ.BusinessContext dc = new DebtPlus.LINQ.BusinessContext())
                {
                    colClientCreditorEvents = dc.client_creditor_events.Where(c => c.client_creditor == currentDebtRecord.client_creditor).ToList();
                    colConfigFee = DebtPlus.LINQ.Cache.config_fee.getList();
                }
            }

            GridControl1.DataSource = colClientCreditorEvents;
            GridControl1.RefreshDataSource();

            GridColumn_Event.DisplayFormat.Format = new config_Fee_formatter();
            GridColumn_Event.GroupFormat.Format = new config_Fee_formatter();
        }

        protected override void OnCreate()
        {
            Int32? client_config_fee = null;
            using (var bc = new BusinessContext())
            {
                var q = bc.clients.Where(s => s.Id == currentDebtRecord.ClientId).Select(s => new { config_fee = s.config_fee }).FirstOrDefault();
                if (q != null)
                {
                    client_config_fee = q.config_fee;
                }

                if (client_config_fee == null)
                {
                    client_config_fee = DebtPlus.LINQ.Cache.config_fee.getDefault();
                    if (client_config_fee == null)
                    {
                        client_config_fee = 1;
                    }
                }
            }

            // Allocate a new item to be edited
            DebtPlus.LINQ.client_creditor_event evt = new DebtPlus.LINQ.client_creditor_event()
            {
                client_creditor = currentDebtRecord.client_creditor,
                update_item = 0,
                date_updated = null,
                effective_date = DateTime.Now.AddDays(1).Date,
                config_fee = client_config_fee,
                value = 0m
            };

            // If the OK button was pressed then insert the new row
            using (Form_Event frm = new Form_Event(colClientCreditorEvents, colConfigFee, evt))
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
            }

            using (DebtPlus.LINQ.BusinessContext dc = new DebtPlus.LINQ.BusinessContext())
            {
                dc.client_creditor_events.InsertOnSubmit(evt);
                dc.SubmitChanges();
                colClientCreditorEvents.Add(evt);

                GridControl1.DataSource = colClientCreditorEvents;
                GridControl1.RefreshDataSource();
            }
        }

        protected override void OnDelete(object obj)
        {
            bool answer = false;
            DebtPlus.LINQ.client_creditor_event evt = obj as DebtPlus.LINQ.client_creditor_event;

            if (evt != null && !DesignMode)
            {
                answer = (DebtPlus.Data.Prompts.RequestConfirmation_Delete() == DialogResult.Yes);
                if (answer)
                {
                    using (DebtPlus.LINQ.BusinessContext dr = new DebtPlus.LINQ.BusinessContext())
                    {
                        // Find the item in the table
                        DebtPlus.LINQ.client_creditor_event q = dr.client_creditor_events.Where(f => f.Id == evt.Id).FirstOrDefault();
                        if (q != null)
                        {
                            dr.client_creditor_events.DeleteOnSubmit(q);
                            dr.SubmitChanges();

                            // Remove the item from the display list
                            colClientCreditorEvents.Remove(evt);
                            GridControl1.DataSource = colClientCreditorEvents;
                            GridControl1.RefreshDataSource();
                        }
                    }
                }
            }
        }

        protected override void OnEdit(object obj)
        {
            DebtPlus.LINQ.client_creditor_event evt = obj as DebtPlus.LINQ.client_creditor_event;
            if (evt == null)
            {
                return;
            }

            // Edit the record.
            using (Form_Event frm = new Form_Event(colClientCreditorEvents, colConfigFee, evt))
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
            }

            // Find the item in the collection as an update item
            using (DebtPlus.LINQ.BusinessContext dc = new DebtPlus.LINQ.BusinessContext())
            {
                DebtPlus.LINQ.client_creditor_event q = dc.client_creditor_events.Where(e => e.Id == evt.Id).FirstOrDefault();
                if (q != null)
                {
                    q.config_fee = evt.config_fee;
                    q.effective_date = evt.effective_date;
                    q.update_item = evt.update_item;
                    q.value = evt.value;
                    dc.SubmitChanges();

                    GridControl1.DataSource = colClientCreditorEvents;
                    GridControl1.RefreshDataSource();
                }
            }
        }

        private class config_Fee_formatter : System.ICustomFormatter, System.IFormatProvider
        {
            public config_Fee_formatter()
            {
            }

            public string Format(string format1, object arg, System.IFormatProvider formatProvider)
            {
                if (arg is Int32)
                {
                    DebtPlus.LINQ.config_fee fee = DebtPlus.LINQ.Cache.config_fee.getList().Where(c => c.Id == (Int32)arg).FirstOrDefault();
                    if (fee != null)
                    {
                        return fee.description;
                    }
                }

                return string.Empty;
            }

            public object GetFormat(System.Type formatType)
            {
                return this;
            }
        }

        #region " Windows Form Designer generated code "

        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Created_By;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Date;

        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Date_Created;

        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Event;

        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Value;

        private System.ComponentModel.IContainer components = null;

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.GridColumn_Date = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Event = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Value = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Created_By = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Date_Created = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)this.barManager1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.PopupMenu_Items).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
            this.SuspendLayout();
            //
            //barDockControlTop
            //
            this.barDockControlTop.Size = new System.Drawing.Size(576, 0);
            //
            //barDockControlBottom
            //
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 296);
            this.barDockControlBottom.Size = new System.Drawing.Size(576, 0);
            //
            //barDockControlLeft
            //
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 296);
            //
            //barDockControlRight
            //
            this.barDockControlRight.Location = new System.Drawing.Point(576, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 296);
            //
            //GridView1
            //
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(104)), Convert.ToInt32(Convert.ToByte(184)), Convert.ToInt32(Convert.ToByte(251)));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(104)), Convert.ToInt32(Convert.ToByte(184)), Convert.ToInt32(Convert.ToByte(251)));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(170)), Convert.ToInt32(Convert.ToByte(216)), Convert.ToInt32(Convert.ToByte(254)));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(170)), Convert.ToInt32(Convert.ToByte(216)), Convert.ToInt32(Convert.ToByte(254)));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(236)), Convert.ToInt32(Convert.ToByte(246)), Convert.ToInt32(Convert.ToByte(255)));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(247)), Convert.ToInt32(Convert.ToByte(251)), Convert.ToInt32(Convert.ToByte(255)));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(247)), Convert.ToInt32(Convert.ToByte(251)), Convert.ToInt32(Convert.ToByte(255)));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(104)), Convert.ToInt32(Convert.ToByte(184)), Convert.ToInt32(Convert.ToByte(251)));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(104)), Convert.ToInt32(Convert.ToByte(184)), Convert.ToInt32(Convert.ToByte(251)));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(236)), Convert.ToInt32(Convert.ToByte(246)), Convert.ToInt32(Convert.ToByte(255)));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(59)), Convert.ToInt32(Convert.ToByte(133)), Convert.ToInt32(Convert.ToByte(195)));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(38)), Convert.ToInt32(Convert.ToByte(109)), Convert.ToInt32(Convert.ToByte(189)));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(59)), Convert.ToInt32(Convert.ToByte(139)), Convert.ToInt32(Convert.ToByte(206)));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(104)), Convert.ToInt32(Convert.ToByte(184)), Convert.ToInt32(Convert.ToByte(251)));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(104)), Convert.ToInt32(Convert.ToByte(184)), Convert.ToInt32(Convert.ToByte(251)));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(104)), Convert.ToInt32(Convert.ToByte(184)), Convert.ToInt32(Convert.ToByte(251)));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(104)), Convert.ToInt32(Convert.ToByte(184)), Convert.ToInt32(Convert.ToByte(251)));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(170)), Convert.ToInt32(Convert.ToByte(216)), Convert.ToInt32(Convert.ToByte(254)));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(170)), Convert.ToInt32(Convert.ToByte(216)), Convert.ToInt32(Convert.ToByte(254)));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(236)), Convert.ToInt32(Convert.ToByte(246)), Convert.ToInt32(Convert.ToByte(255)));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(170)), Convert.ToInt32(Convert.ToByte(216)), Convert.ToInt32(Convert.ToByte(254)));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(170)), Convert.ToInt32(Convert.ToByte(216)), Convert.ToInt32(Convert.ToByte(254)));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(139)), Convert.ToInt32(Convert.ToByte(201)), Convert.ToInt32(Convert.ToByte(254)));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(139)), Convert.ToInt32(Convert.ToByte(201)), Convert.ToInt32(Convert.ToByte(254)));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(105)), Convert.ToInt32(Convert.ToByte(170)), Convert.ToInt32(Convert.ToByte(225)));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(83)), Convert.ToInt32(Convert.ToByte(155)), Convert.ToInt32(Convert.ToByte(215)));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(236)), Convert.ToInt32(Convert.ToByte(246)), Convert.ToInt32(Convert.ToByte(255)));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(104)), Convert.ToInt32(Convert.ToByte(184)), Convert.ToInt32(Convert.ToByte(251)));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(236)), Convert.ToInt32(Convert.ToByte(246)), Convert.ToInt32(Convert.ToByte(255)));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(236)), Convert.ToInt32(Convert.ToByte(246)), Convert.ToInt32(Convert.ToByte(255)));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5f);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(83)), Convert.ToInt32(Convert.ToByte(155)), Convert.ToInt32(Convert.ToByte(215)));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(247)), Convert.ToInt32(Convert.ToByte(251)), Convert.ToInt32(Convert.ToByte(255)));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(236)), Convert.ToInt32(Convert.ToByte(246)), Convert.ToInt32(Convert.ToByte(255)));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(83)), Convert.ToInt32(Convert.ToByte(155)), Convert.ToInt32(Convert.ToByte(215)));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToInt32(Convert.ToByte(104)), Convert.ToInt32(Convert.ToByte(184)), Convert.ToInt32(Convert.ToByte(251)));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
				this.GridColumn_Date,
				this.GridColumn_Event,
				this.GridColumn_Value,
				this.GridColumn_Created_By,
				this.GridColumn_Date_Created
			});
            this.GridView1.OptionsBehavior.AutoPopulateColumns = false;
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.GridView1.OptionsView.EnableAppearanceOddRow = true;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            //
            //GridColumn_Date
            //
            this.GridColumn_Date.Caption = "Date";
            this.GridColumn_Date.CustomizationCaption = "Date";
            this.GridColumn_Date.DisplayFormat.FormatString = "d";
            this.GridColumn_Date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_Date.FieldName = "effective_date";
            this.GridColumn_Date.GroupFormat.FormatString = "d";
            this.GridColumn_Date.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_Date.Name = "GridColumn_Date";
            this.GridColumn_Date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_Date.Visible = true;
            this.GridColumn_Date.VisibleIndex = 0;
            this.GridColumn_Date.Width = 99;
            //
            //GridColumn_Event
            //
            this.GridColumn_Event.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_Event.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GridColumn_Event.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_Event.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GridColumn_Event.Caption = "Event";
            this.GridColumn_Event.CustomizationCaption = "Event";
            this.GridColumn_Event.DisplayFormat.FormatString = "f0";
            this.GridColumn_Event.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GridColumn_Event.FieldName = "config_fee";
            this.GridColumn_Event.GroupFormat.FormatString = "f0";
            this.GridColumn_Event.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GridColumn_Event.Name = "GridColumn_Event";
            this.GridColumn_Event.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_Event.Visible = true;
            this.GridColumn_Event.VisibleIndex = 1;
            this.GridColumn_Event.Width = 389;
            //
            //GridColumn_Value
            //
            this.GridColumn_Value.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_Value.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_Value.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_Value.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_Value.Caption = "Value";
            this.GridColumn_Value.CustomizationCaption = "Value";
            this.GridColumn_Value.DisplayFormat.FormatString = "c2";
            this.GridColumn_Value.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_Value.FieldName = "value";
            this.GridColumn_Value.Name = "GridColumn_Value";
            this.GridColumn_Value.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_Value.Visible = true;
            this.GridColumn_Value.VisibleIndex = 2;
            this.GridColumn_Value.Width = 109;
            //
            //GridColumn_Created_By
            //
            this.GridColumn_Created_By.Caption = "Creator";
            this.GridColumn_Created_By.CustomizationCaption = "Person who created the event";
            this.GridColumn_Created_By.FieldName = "created_by";
            this.GridColumn_Created_By.Name = "GridColumn_Created_By";
            this.GridColumn_Created_By.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            //
            //GridColumn_Date_Created
            //
            this.GridColumn_Date_Created.Caption = "Date Created";
            this.GridColumn_Date_Created.CustomizationCaption = "Date/Time when created";
            this.GridColumn_Date_Created.DisplayFormat.FormatString = "d";
            this.GridColumn_Date_Created.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_Date_Created.FieldName = "date_created";
            this.GridColumn_Date_Created.GroupFormat.FormatString = "d";
            this.GridColumn_Date_Created.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_Date_Created.Name = "GridColumn_Date_Created";
            this.GridColumn_Date_Created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            //
            //Page_Events
            //
            this.Name = "Page_Events";
            ((System.ComponentModel.ISupportInitialize)this.barManager1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.PopupMenu_Items).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "
    }
}


