#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Windows.Forms;
using DevExpress.XtraBars;

namespace DebtPlus.UI.Debt.Update
{
    internal partial class ReportsMenuItem : BarButtonItem
    {
        private DebtPlus.Interfaces.Debt.IDebtRecord currentDebtRecord;
        private DebtPlus.LINQ.report rpt;

        public ReportsMenuItem()
            : base()
        {
        }

        public ReportsMenuItem(DebtPlus.Interfaces.Debt.IDebtRecord CurrentDebtRecord, string name, DebtPlus.LINQ.report rpt)
            : this(name)
        {
            this.rpt = rpt;
            this.currentDebtRecord = CurrentDebtRecord;
        }

        public ReportsMenuItem(string text) : this()
        {
            Caption = text;
        }

        protected override void OnClick(BarItemLink link)
        {
            base.OnClick(link);

            if (rpt != null)
            {
                if (string.IsNullOrEmpty(rpt.ClassName))
                {
                    DebtPlus.Data.Forms.MessageBox.Show("The report does not have a .NET equivalent. You must use the older 'Crystal Reports' interface to run this report.", "Configuration Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
                else
                {
                    // Attempt to load the report class into memory and run it. Pass along the creditor as a parameter.
                    DebtPlus.Interfaces.Reports.IReports irpt = DebtPlus.Reports.ReportLoader.LoadReport(rpt.ClassName);

                    irpt.SetReportParameter("ClientCreditor", currentDebtRecord.client_creditor);
                    if (irpt.RequestReportParameters() == DialogResult.OK)
                    {
                        irpt.RunReportInSeparateThread();
                    }
                }
            }
        }
    }
}
