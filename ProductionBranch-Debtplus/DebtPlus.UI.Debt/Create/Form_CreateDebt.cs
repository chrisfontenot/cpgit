#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion

using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.Interfaces.Debt;
using DebtPlus.UI.Creditor.Widgets.Controls;

namespace DebtPlus.UI.Debt.Create
{
	public partial class Form_CreateDebt : IDebtCreate
	{
        protected Int32 ClientId = Int32.MinValue;
        protected Int32 PrivateDebtId = Int32.MinValue;
        internal CreditorID CreditorID;

        public Form_CreateDebt() : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        public Form_CreateDebt(Int32 ClientId) : this()
        {
            this.ClientId = ClientId;
        }

		public Int32 DebtId
        {
			get
            {
                return PrivateDebtId;
            }
		}

        private void RegisterHandlers()
        {
            // Put in the "Handles" clauses here
            AccountNumber.EditValueChanged += FormChanged;
            CreditorName.EditValueChanged  += FormChanged;
            CreditorID.EditValueChanged    += FormChanged;
            UncodedName.TextChanged        += FormChanged;
            Button_OK.Click                += Button_OK_Click;
        }

        private void UnRegisterHandlers()
        {
            AccountNumber.EditValueChanged -= FormChanged;
            CreditorName.EditValueChanged  -= FormChanged;
            CreditorID.EditValueChanged    -= FormChanged;
            UncodedName.TextChanged        -= FormChanged;
            Button_OK.Click                -= Button_OK_Click;
        }

		public DialogResult ShowDialog(Int32 ClientId)
		{
			this.ClientId = ClientId;
			return ShowDialog();
		}

		public DialogResult ShowDialog(Int32 ClientId, IWin32Window owner)
		{
			this.ClientId = ClientId;
			return ShowDialog(owner);
		}

		/// <summary>
		/// Process the OK button CLICK event to create the creditor
		/// </summary>
		private void Button_OK_Click(object sender, EventArgs e)
		{
			// Remove the extra characters and reformat the account number. The search is very specific and does not like extra characters.
			string myAccountNumber = AccountNumber.Text.Replace("-", "").Replace(" ","");
            if (myAccountNumber.Length > 22)
            {
                myAccountNumber = myAccountNumber.Substring(0, 22);
            }

			// Create the debg when the creditor ID is given
            string myCreditorId = CreditorID.EditValue;
			if (! string.IsNullOrEmpty(myCreditorId))
            {
				ProcessCreditorSearchByID(myCreditorId, myAccountNumber);
                return;
            }

			// Create the debt when the creditor name is given
			string myCreditorName = CreditorName.Text.Trim();
            if (!string.IsNullOrWhiteSpace(myCreditorName))
            {
                ProcessCreditorSearchByName(myCreditorName, myAccountNumber);
                return;
            }

            // Create the debt when the creditor name is not known
            string myUncodedName = UncodedName.Text.Trim();
            if (! string.IsNullOrEmpty(myUncodedName))
            {
				ProcessCreditorSearchByUncodedName(myUncodedName, myAccountNumber);
                return;
            }
		}

		/// <summary>
		/// Called when the input form values are changed
		/// </summary>
		private void FormChanged(object sender, EventArgs e)
		{
			// Look at the input fields to determine if enough is entered to do a search for the ID.
            if (! string.IsNullOrWhiteSpace(UncodedName.Text))
            {
                Button_OK.Enabled = true;
                return;
            }

			if (! string.IsNullOrWhiteSpace(CreditorID.EditValue))
            {
                Button_OK.Enabled = true;
                return;
            }

            if (! string.IsNullOrWhiteSpace(CreditorName.Text))
            {
                Button_OK.Enabled = true;
                return;
            }

            if (! string.IsNullOrWhiteSpace(AccountNumber.Text))
            {
                Button_OK.Enabled = true;
                return;
            }

			// All fields are blank. ignore the OK button
			Button_OK.Enabled = false;
			return;
		}

        private void ProcessCreditorSearchByID(string myCreditorID, string myAccountNumber)
        {
            try
            {
                using (var bc = new DebtPlus.LINQ.BusinessContext())
                {
                    var q = bc.creditors.Where(s => s.Id == myCreditorID).Select(s => new { s.Id, s.prohibit_use }).FirstOrDefault();
                    if (q == null)
                    {
                        DebtPlus.Data.Forms.MessageBox.Show("The creditor does not exist. Please correct the creditor ID and try again.", "Data Entry Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        return;
                    }

                    if (q.prohibit_use)
                    {
                        DebtPlus.Data.Forms.MessageBox.Show("The creditor is marked PROHIBIT USE. You are not allowed to assign any new debts to this creditor. Please enter a valid creditor ID.", "Dataentry Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }

                    // Create the debt record.
                    string errorMessage;
                    var cdc = new global::DebtPlus.Svc.Debt.CreateDebtClass(ClientId);
                    PrivateDebtId = cdc.CreateDebt(myCreditorID, myAccountNumber, null, out errorMessage);
                    if (PrivateDebtId != 0)
                    {
                        DialogResult = DialogResult.OK;
                    }
                    else
                    {
                        DebtPlus.Data.Forms.MessageBox.Show("Unable to create the debt for unknown reasons.", "Data entry Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "error creating new debt");
                return;
            }
        }

		private void ProcessCreditorSearchByUncodedName(string myUncodedName, string myAccountNumber)
		{
            try
            {
                string errorMessage;
                var cdc = new global::DebtPlus.Svc.Debt.CreateDebtClass(ClientId);
                PrivateDebtId = cdc.CreateDebt(null, myAccountNumber, myUncodedName, out errorMessage);
                if (!string.IsNullOrEmpty(errorMessage))
                {
                    DebtPlus.Data.Forms.MessageBox.Show(errorMessage, "Error creating debt", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    DialogResult = System.Windows.Forms.DialogResult.OK;
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "error creating new debt");
                return;
            }
		}

		private void ProcessCreditorSearchByName(string myCreditorName, string myAccountNumber)
		{
            var cdc = new global::DebtPlus.Svc.Debt.CreateDebtClass(ClientId);
            SelectCreditorAndCreateDebt(myAccountNumber, cdc.GetCreditorsByName(myCreditorName));
		}

		private void ProcessCreditorSearchByAccountNumber(string myAccountNumber)
		{
			var cdc = new global::DebtPlus.Svc.Debt.CreateDebtClass(ClientId);
			SelectCreditorAndCreateDebt(myAccountNumber, cdc.GetCreditorsByAccountNumber(myAccountNumber));
		}

		private void SelectCreditorAndCreateDebt(string myAccountNumber, DataTable creditorsTable)
		{
            if (creditorsTable == null)
            {
                return;
            }

			string myCreditor = SelectCreditor(creditorsTable);
            if (string.IsNullOrWhiteSpace(myCreditor))
            {
                return;
            }

            var cdc = new global::DebtPlus.Svc.Debt.CreateDebtClass(ClientId);
            string emsg = string.Empty;
			cdc.CreateDebt(myCreditor, myAccountNumber, null, out emsg);
		}

		private string SelectCreditor(DataTable tbl)
		{
			DataView vue = tbl.DefaultView;

			if (vue.Count == 0)
            {
                DebtPlus.Data.Forms.MessageBox.Show("The creditor could not be determined from the information that you supplied. Please try a different method to create the debt.", "Creditor not found", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
				return string.Empty;
			}

			if (vue.Count == 1)
            {
				return Convert.ToString(vue[0]["creditor"]);
			}

            using (var frm = new Form_CreditorSelect(vue))
            {
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    return frm.Parameter_creditor;
                }
            }

			return string.Empty;
		}
	}
}
