
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.Creditor.Widgets.Controls;
using DebtPlus.UI.Creditor.Widgets;

namespace DebtPlus.UI.Debt.Create
{
	partial class Form_CreateDebt : DebtPlus.Data.Forms.DebtPlusForm
	{

		//Form overrides dispose to clean up the component list.
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		internal DevExpress.XtraEditors.SimpleButton Button_OK;
		internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
		internal DevExpress.XtraEditors.GroupControl GroupControl1;
		internal DevExpress.XtraEditors.LabelControl LabelControl1;
		internal DevExpress.XtraEditors.LabelControl LabelControl2;
		internal DevExpress.XtraEditors.LabelControl LabelControl3;
		internal DevExpress.XtraEditors.LabelControl LabelControl4;
		internal DevExpress.XtraEditors.TextEdit CreditorName;
		internal DevExpress.XtraEditors.TextEdit AccountNumber;
		internal DevExpress.XtraEditors.GroupControl GroupControl2;
		internal DevExpress.XtraEditors.TextEdit UncodedName;
		internal DevExpress.XtraEditors.LabelControl LabelControl5;
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_CreateDebt));
			DevExpress.Utils.SerializableAppearanceObject SerializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
			this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
			this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
			this.GroupControl1 = new DevExpress.XtraEditors.GroupControl();
			this.AccountNumber = new DevExpress.XtraEditors.TextEdit();
			this.CreditorName = new DevExpress.XtraEditors.TextEdit();
			this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.GroupControl2 = new DevExpress.XtraEditors.GroupControl();
			this.UncodedName = new DevExpress.XtraEditors.TextEdit();
			this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
			this.CreditorID = new CreditorID();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.GroupControl1).BeginInit();
			this.GroupControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.AccountNumber.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CreditorName.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.GroupControl2).BeginInit();
			this.GroupControl2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.UncodedName.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CreditorID.Properties).BeginInit();
			this.SuspendLayout();
			//
			//Button_OK
			//
			this.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.Button_OK.Enabled = false;
			this.Button_OK.Location = new System.Drawing.Point(111, 240);
			this.Button_OK.Name = "Button_OK";
			this.Button_OK.Size = new System.Drawing.Size(75, 23);
			this.Button_OK.TabIndex = 2;
			this.Button_OK.Text = "&OK";
			this.Button_OK.ToolTip = "Click here to create the new creditor";
			this.Button_OK.ToolTipController = this.ToolTipController1;
			//
			//Button_Cancel
			//
			this.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.Button_Cancel.Location = new System.Drawing.Point(207, 240);
			this.Button_Cancel.Name = "Button_Cancel";
			this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
			this.Button_Cancel.TabIndex = 3;
			this.Button_Cancel.Text = "&Cancel";
			this.Button_Cancel.ToolTip = "Click here to cancel the creation and return to the previous screen";
			this.Button_Cancel.ToolTipController = this.ToolTipController1;
			//
			//GroupControl1
			//
			this.GroupControl1.Appearance.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(192), (Int32)Convert.ToByte(255), (Int32)Convert.ToByte(192));
			this.GroupControl1.Appearance.Options.UseBackColor = true;
			this.GroupControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
			this.GroupControl1.Controls.Add(this.CreditorID);
			this.GroupControl1.Controls.Add(this.AccountNumber);
			this.GroupControl1.Controls.Add(this.CreditorName);
			this.GroupControl1.Controls.Add(this.LabelControl4);
			this.GroupControl1.Controls.Add(this.LabelControl3);
			this.GroupControl1.Controls.Add(this.LabelControl2);
			this.GroupControl1.Controls.Add(this.LabelControl1);
			this.GroupControl1.Dock = System.Windows.Forms.DockStyle.Top;
			this.GroupControl1.Location = new System.Drawing.Point(0, 0);
			this.GroupControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
			this.GroupControl1.LookAndFeel.UseDefaultLookAndFeel = false;
			this.GroupControl1.LookAndFeel.UseWindowsXPTheme = true;
			this.GroupControl1.Name = "GroupControl1";
			this.GroupControl1.ShowCaption = false;
			this.GroupControl1.Size = new System.Drawing.Size(392, 120);
			this.GroupControl1.TabIndex = 0;
			this.GroupControl1.Text = "GroupControl1";
			//
			//AccountNumber
			//
			this.AccountNumber.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.AccountNumber.Location = new System.Drawing.Point(103, 94);
			this.AccountNumber.Name = "AccountNumber";
			this.AccountNumber.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.AccountNumber.Size = new System.Drawing.Size(282, 20);
			this.AccountNumber.TabIndex = 6;
			this.AccountNumber.ToolTipController = this.ToolTipController1;
			//
			//CreditorName
			//
			this.CreditorName.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.CreditorName.Location = new System.Drawing.Point(103, 69);
			this.CreditorName.Name = "CreditorName";
			this.CreditorName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.CreditorName.Size = new System.Drawing.Size(282, 20);
			this.CreditorName.TabIndex = 4;
			this.CreditorName.ToolTipController = this.ToolTipController1;
			//
			//LabelControl4
			//
			this.LabelControl4.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.LabelControl4.Location = new System.Drawing.Point(7, 7);
			this.LabelControl4.Name = "LabelControl4";
			this.LabelControl4.Size = new System.Drawing.Size(359, 13);
			this.LabelControl4.TabIndex = 0;
			this.LabelControl4.Text = "Choose the creditor by entering the ID or the name or the account number";
			this.LabelControl4.ToolTipController = this.ToolTipController1;
			this.LabelControl4.UseMnemonic = false;
			//
			//LabelControl3
			//
			this.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
			this.LabelControl3.Location = new System.Drawing.Point(8, 96);
			this.LabelControl3.Name = "LabelControl3";
			this.LabelControl3.Size = new System.Drawing.Size(79, 13);
			this.LabelControl3.TabIndex = 5;
			this.LabelControl3.Text = "Account Number";
			this.LabelControl3.ToolTipController = this.ToolTipController1;
			//
			//LabelControl2
			//
			this.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
			this.LabelControl2.Location = new System.Drawing.Point(8, 72);
			this.LabelControl2.Name = "LabelControl2";
			this.LabelControl2.Size = new System.Drawing.Size(69, 13);
			this.LabelControl2.TabIndex = 3;
			this.LabelControl2.Text = "Creditor Name";
			this.LabelControl2.ToolTipController = this.ToolTipController1;
			//
			//LabelControl1
			//
			this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
			this.LabelControl1.Location = new System.Drawing.Point(8, 48);
			this.LabelControl1.Name = "LabelControl1";
			this.LabelControl1.Size = new System.Drawing.Size(53, 13);
			this.LabelControl1.TabIndex = 1;
			this.LabelControl1.Text = "Creditor ID";
			this.LabelControl1.ToolTipController = this.ToolTipController1;
			//
			//GroupControl2
			//
			this.GroupControl2.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.GroupControl2.Appearance.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(255), (Int32)Convert.ToByte(255), (Int32)Convert.ToByte(192));
			this.GroupControl2.Appearance.Options.UseBackColor = true;
			this.GroupControl2.Controls.Add(this.UncodedName);
			this.GroupControl2.Controls.Add(this.LabelControl5);
			this.GroupControl2.Location = new System.Drawing.Point(0, 120);
			this.GroupControl2.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
			this.GroupControl2.LookAndFeel.UseDefaultLookAndFeel = false;
			this.GroupControl2.Name = "GroupControl2";
			this.GroupControl2.ShowCaption = false;
			this.GroupControl2.Size = new System.Drawing.Size(392, 104);
			this.GroupControl2.TabIndex = 1;
			this.GroupControl2.Text = "GroupControl2";
			//
			//UncodedName
			//
			this.UncodedName.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.UncodedName.Location = new System.Drawing.Point(8, 72);
			this.UncodedName.Name = "UncodedName";
			this.UncodedName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.UncodedName.Size = new System.Drawing.Size(376, 20);
			this.UncodedName.TabIndex = 1;
			this.UncodedName.ToolTipController = this.ToolTipController1;
			//
			//LabelControl5
			//
			this.LabelControl5.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.LabelControl5.Appearance.Options.UseTextOptions = true;
			this.LabelControl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.LabelControl5.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.LabelControl5.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
			this.LabelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.LabelControl5.Location = new System.Drawing.Point(8, 8);
			this.LabelControl5.Name = "LabelControl5";
			this.LabelControl5.Size = new System.Drawing.Size(378, 58);
			this.LabelControl5.TabIndex = 0;
			this.LabelControl5.Text = resources.GetString("LabelControl5.Text");
			this.LabelControl5.ToolTipController = this.ToolTipController1;
			this.LabelControl5.UseMnemonic = false;
			//
			//CreditorID
			//
			this.CreditorID.EditValue = null;
			this.CreditorID.Location = new System.Drawing.Point(103, 44);
			this.CreditorID.Name = "CreditorID";
			this.CreditorID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, (System.Drawing.Image)resources.GetObject("CreditorID1.Properties.Buttons"), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1,
			"Click here to search for a creditor ID", "search", null, true) });
			this.CreditorID.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.CreditorID.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
			this.CreditorID.Properties.Mask.BeepOnError = true;
			this.CreditorID.Properties.Mask.EditMask = "[A-Z]{1,2}[0-9]{4,}";
			this.CreditorID.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.CreditorID.Properties.Mask.UseMaskAsDisplayFormat = true;
			this.CreditorID.Properties.MaxLength = 10;
			this.CreditorID.Size = new System.Drawing.Size(100, 20);
			this.CreditorID.TabIndex = 2;
			//
			//CreateDebtClass
			//
			this.AcceptButton = this.Button_OK;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.CancelButton = this.Button_Cancel;
			this.ClientSize = new System.Drawing.Size(392, 278);
			this.Controls.Add(this.GroupControl2);
			this.Controls.Add(this.GroupControl1);
			this.Controls.Add(this.Button_Cancel);
			this.Controls.Add(this.Button_OK);
			this.MaximizeBox = false;
			this.Name = "Form_CreateDebt";
			this.Text = "Create New Debt";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.GroupControl1).EndInit();
			this.GroupControl1.ResumeLayout(false);
			this.GroupControl1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)this.AccountNumber.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CreditorName.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.GroupControl2).EndInit();
			this.GroupControl2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.UncodedName.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CreditorID.Properties).EndInit();
			this.ResumeLayout(false);

		}
	}
}

