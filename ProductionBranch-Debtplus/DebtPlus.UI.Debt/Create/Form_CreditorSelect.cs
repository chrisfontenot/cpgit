#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.IO;
using System.Reflection;
using DebtPlus.Data.Forms;
using DevExpress.Utils;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

namespace DebtPlus.UI.Debt.Create
{
    internal partial class Form_CreditorSelect : DebtPlusForm
    {
        public string Parameter_creditor = string.Empty;

        private DataView vue = null;

        public Form_CreditorSelect() : base()
        {
            InitializeComponent();
        }

        public Form_CreditorSelect(DataView dv) : this()
        {
            vue = dv;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            GridView1.DoubleClick               += GridView1_DoubleClick;
            GridView1.FocusedRowChanged         += GridView1_FocusedRowChanged;
            GridControl1.Click                  += GridControl1_Click;
            Load                                += CreditorListForm_Load;
            GridView1.Layout                    += LayoutChanged;
            CheckEdit_Preview.CheckStateChanged += CheckEdit_Preview_CheckStateChanged;
        }

        private void UnRegisterHandlers()
        {
            GridView1.DoubleClick               -= GridView1_DoubleClick;
            GridView1.FocusedRowChanged         -= GridView1_FocusedRowChanged;
            GridControl1.Click                  -= GridControl1_Click;
            Load                                -= CreditorListForm_Load;
            GridView1.Layout                    -= LayoutChanged;
            CheckEdit_Preview.CheckStateChanged -= CheckEdit_Preview_CheckStateChanged;
        }

        /// <summary>
        /// Remember the form location and size for next time
        /// </summary>
        protected string ApplicationName()
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            return asm.GetName().FullName + "." + Name;
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraGrid.GridControl GridControl1;

        internal DevExpress.XtraGrid.Views.Grid.GridView GridView1;
        internal DevExpress.XtraEditors.SimpleButton Button_OK;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
        internal DevExpress.XtraGrid.Columns.GridColumn col_name;
        internal DevExpress.XtraGrid.Columns.GridColumn col_comment;
        internal DevExpress.XtraGrid.Columns.GridColumn col_fairshare;
        internal DevExpress.XtraGrid.Columns.GridColumn col_address;
        internal DevExpress.XtraGrid.Columns.GridColumn col_sic;
        internal DevExpress.XtraGrid.Columns.GridColumn col_fairshare_eft;
        internal DevExpress.XtraGrid.Columns.GridColumn col_fairshare_check;
        internal DevExpress.XtraGrid.Columns.GridColumn col_creditor;
        internal DevExpress.XtraGrid.Columns.GridColumn col_creditor_id;

        internal DevExpress.XtraEditors.CheckEdit CheckEdit_Preview;

        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col_creditor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_creditor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.col_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.col_comment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_comment.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.col_fairshare = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_fairshare.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.col_address = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_address.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.col_creditor_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_creditor_id.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.col_sic = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_sic.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.col_fairshare_eft = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_fairshare_eft.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.col_fairshare_check = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_fairshare_check.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.CheckEdit_Preview = new DevExpress.XtraEditors.CheckEdit();

            ((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.CheckEdit_Preview.Properties).BeginInit();
            this.SuspendLayout();

            //
            //GridControl1
            //
            this.GridControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            //
            //GridControl1.EmbeddedNavigator
            //
            this.GridControl1.EmbeddedNavigator.Name = "";
            this.GridControl1.Location = new System.Drawing.Point(8, 9);
            this.GridControl1.MainView = this.GridView1;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.Size = new System.Drawing.Size(352, 231);
            this.GridControl1.TabIndex = 0;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { this.GridView1 });
            //
            //GridView1
            //
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(59), Convert.ToByte(133), Convert.ToByte(195));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(38), Convert.ToByte(109), Convert.ToByte(189));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(59), Convert.ToByte(139), Convert.ToByte(206));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(139), Convert.ToByte(201), Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(139), Convert.ToByte(201), Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(105), Convert.ToByte(170), Convert.ToByte(225));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5f);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
                this.col_creditor,
                this.col_name,
                this.col_comment,
                this.col_fairshare,
                this.col_address,
                this.col_creditor_id,
                this.col_sic,
                this.col_fairshare_eft,
                this.col_fairshare_check
            });
            this.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.GridView1.GridControl = this.GridControl1;
            this.GridView1.Name = "GridView1";
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.GridView1.OptionsPrint.ExpandAllGroups = false;
            this.GridView1.OptionsPrint.PrintFooter = false;
            this.GridView1.OptionsPrint.PrintGroupFooter = false;
            this.GridView1.OptionsPrint.PrintPreview = true;
            this.GridView1.OptionsPrint.UsePrintStyles = true;
            this.GridView1.OptionsSelection.UseIndicatorForSelection = false;
            this.GridView1.OptionsView.AutoCalcPreviewLineCount = true;
            this.GridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            this.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedRow;
            this.GridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] { new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.col_creditor, DevExpress.Data.ColumnSortOrder.Ascending) });
            //
            //col_creditor
            //
            this.col_creditor.Caption = "Creditor";
            this.col_creditor.FieldName = "creditor";
            this.col_creditor.Name = "col_creditor";
            this.col_creditor.Visible = true;
            this.col_creditor.VisibleIndex = 0;
            this.col_creditor.Width = 68;
            //
            //col_name
            //
            this.col_name.Caption = "Name";
            this.col_name.FieldName = "name";
            this.col_name.Name = "col_name";
            this.col_name.Visible = true;
            this.col_name.VisibleIndex = 1;
            this.col_name.Width = 190;
            //
            //col_comment
            //
            this.col_comment.Caption = "Comment";
            this.col_comment.FieldName = "comment";
            this.col_comment.Name = "col_comment";
            this.col_comment.Visible = true;
            this.col_comment.VisibleIndex = 2;
            this.col_comment.Width = 176;
            //
            //col_fairshare
            //
            this.col_fairshare.Caption = "Fairshare";
            this.col_fairshare.FieldName = "fairshare";
            this.col_fairshare.Name = "col_fairshare";
            this.col_fairshare.Visible = true;
            this.col_fairshare.VisibleIndex = 3;
            this.col_fairshare.Width = 81;
            //
            //col_address
            //
            this.col_address.Caption = "Address";
            this.col_address.FieldName = "address";
            this.col_address.Name = "col_address";
            //
            //col_creditor_id
            //
            this.col_creditor_id.Caption = "CreditorID";
            this.col_creditor_id.DisplayFormat.FormatString = "f0";
            this.col_creditor_id.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.col_creditor_id.FieldName = "creditor_id";
            this.col_creditor_id.Name = "col_creditor_id";
            //
            //col_sic
            //
            this.col_sic.Caption = "S.I.C.";
            this.col_sic.FieldName = "sic";
            this.col_sic.Name = "col_sic";
            //
            //col_fairshare_eft
            //
            this.col_fairshare_eft.AppearanceCell.Options.UseTextOptions = true;
            this.col_fairshare_eft.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.col_fairshare_eft.AppearanceHeader.Options.UseTextOptions = true;
            this.col_fairshare_eft.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.col_fairshare_eft.Caption = "EFT Fairshare %";
            this.col_fairshare_eft.DisplayFormat.FormatString = "p2";
            this.col_fairshare_eft.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.col_fairshare_eft.FieldName = "fairshare_pct_eft";
            this.col_fairshare_eft.GroupFormat.FormatString = "p2";
            this.col_fairshare_eft.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.col_fairshare_eft.Name = "col_fairshare_eft";
            //
            //col_fairshare_check
            //
            this.col_fairshare_check.AppearanceCell.Options.UseTextOptions = true;
            this.col_fairshare_check.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.col_fairshare_check.AppearanceHeader.Options.UseTextOptions = true;
            this.col_fairshare_check.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.col_fairshare_check.Caption = "Chk Fairshare %";
            this.col_fairshare_check.DisplayFormat.FormatString = "p2";
            this.col_fairshare_check.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.col_fairshare_check.FieldName = "fairshare_pct_check";
            this.col_fairshare_check.GroupFormat.FormatString = "p2";
            this.col_fairshare_check.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.col_fairshare_check.Name = "col_fairshare_check";
            //
            //Button_OK
            //
            this.Button_OK.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Button_OK.Location = new System.Drawing.Point(376, 17);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 25);
            this.Button_OK.TabIndex = 1;
            this.Button_OK.Text = "&OK";
            this.Button_OK.ToolTip = "Click here to accept the creditor.";
            this.Button_OK.ToolTipController = this.ToolTipController1;
            //
            //Button_Cancel
            //
            this.Button_Cancel.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(376, 60);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 25);
            this.Button_Cancel.TabIndex = 2;
            this.Button_Cancel.Text = "&Cancel";
            this.Button_Cancel.ToolTip = "Click here to cancel this form and return to the previous imput form.";
            this.Button_Cancel.ToolTipController = this.ToolTipController1;
            //
            //CheckEdit_Preview
            //
            this.CheckEdit_Preview.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
            this.CheckEdit_Preview.Location = new System.Drawing.Point(8, 248);
            this.CheckEdit_Preview.Name = "CheckEdit_Preview";
            //
            //CheckEdit_Preview.Properties
            //
            this.CheckEdit_Preview.Properties.Caption = "Include creditor addresses";
            this.CheckEdit_Preview.Size = new System.Drawing.Size(352, 19);

            this.CheckEdit_Preview.TabIndex = 5;
            this.CheckEdit_Preview.ToolTip = "Do you wish to include the creditor addresses in the list? If so, check this box." + "";
            this.CheckEdit_Preview.ToolTipController = this.ToolTipController1;
            //
            //CreditorListForm
            //
            this.AcceptButton = this.Button_OK;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(464, 273);
            this.Controls.Add(this.CheckEdit_Preview);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.GridControl1);
            this.Name = "Form_CeditorSelect";
            this.ToolTipController1.SetSuperTip(this, null);
            this.Text = "Creditors Found";

            ((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.CheckEdit_Preview.Properties).EndInit();
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        /// <summary>
        /// Handle the double-click event on the grid view
        /// </summary>
        private void GridView1_DoubleClick(object sender, EventArgs e)
        {
            if (Button_OK.Enabled)
            {
                Button_OK.PerformClick();
            }
        }

        /// <summary>
        /// Process a change in the focused row for the grid
        /// </summary>
        private void GridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            try
            {
                // Find the row targeted as the double-click item
                GridView gv     = (GridView)sender;
                GridControl ctl = gv.GridControl;

                // If there is a row then process the double-click event. Ignore it if there is no row.
                DataRow row     = null;
                Int32 rowHandle = e.FocusedRowHandle;
                if (rowHandle  >= 0)
                {
                    row = gv.GetDataRow(rowHandle);
                }

                // From the data source, we can find the client in the table.
                if (row != null)
                {
                    Parameter_creditor = Convert.ToString(row["creditor"]);
                }
            }
            catch
            {
                Parameter_creditor = string.Empty;
            }

            // Enable the OK button if the creditor is specified
            Button_OK.Enabled = Parameter_creditor != string.Empty;
        }

        /// <summary>
        /// Process a click event on the grid
        /// </summary>
        private void GridControl1_Click(object sender, EventArgs e)
        {
            GridControl ctl = (GridControl)sender;
            GridView gv     = (GridView)ctl.DefaultView;
            GridHitInfo hi  = gv.CalcHitInfo((ctl.PointToClient(MousePosition)));

            if (hi.IsValid && hi.InRow)
            {
                Int32 rowHandle = hi.RowHandle;
                GridView1.FocusedRowHandle = rowHandle;
            }
        }

        /// <summary>
        /// Enable or disable the display the address field
        /// </summary>
        private void Enable_Disable_Addresses(bool Value)
        {
            if (Value)
            {
                // Display the preview lines
                GridView1.OptionsView.RowAutoHeight            = true;
                GridView1.OptionsView.ShowPreviewRowLines      = DefaultBoolean.True;
                GridView1.OptionsView.RowAutoHeight            = true;
                GridView1.OptionsView.AutoCalcPreviewLineCount = true;
                GridView1.OptionsView.ShowPreview              = true;
                GridView1.PreviewFieldName                     = "address";
            }
            else
            {
                // Hide them
                GridView1.OptionsView.ShowPreviewRowLines      = DefaultBoolean.False;
                GridView1.OptionsView.RowAutoHeight            = false;
                GridView1.OptionsView.AutoCalcPreviewLineCount = false;
                GridView1.OptionsView.ShowPreview              = false;
            }
        }

        /// <summary>
        /// Load and initialize the form
        /// </summary>
        private void CreditorListForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Bind the dataset to the grid control and attempt to restore the formatting information
                GridControl1.DataSource = vue;
                GridControl1.RefreshDataSource();

                // Find the base path to the saved file location
                string pathName = XMLBasePath();
                if (!string.IsNullOrEmpty(pathName))
                {
                    try
                    {
                        string fileName = Path.Combine(pathName, string.Format("{0}.Grid.xml", Name));
                        if (File.Exists(fileName))
                        {
                            GridView1.RestoreLayoutFromXml(fileName);
                        }
                    }
#pragma warning disable 168
                    catch (DirectoryNotFoundException ex) { }
                    catch (FileNotFoundException ex) { }
#pragma warning restore 168
                }

                // Enable or disable the addresses in the list
                bool Enabled = DebtPlus.Configuration.Config.ShowCreditorAddresses;
                CheckEdit_Preview.Checked = Enabled;
                Enable_Disable_Addresses(Enabled);
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Return the directory to the saved layout information
        /// </summary>
        protected virtual string XMLBasePath()
        {
            if (DesignMode)
            {
                return string.Empty;
            }
            string basePath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + Path.DirectorySeparatorChar + "DebtPlus";
            return Path.Combine(basePath, "Debt.Create");
        }

        /// <summary>
        /// If the layout is changed then update the file
        /// </summary>
        private void LayoutChanged(object Sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                string pathName = XMLBasePath();
                if (!string.IsNullOrEmpty(pathName))
                {
                    if (!Directory.Exists(pathName))
                    {
                        Directory.CreateDirectory(pathName);
                    }
                    string fileName = Path.Combine(pathName, string.Format("{0}.Grid.xml", Name));
                    GridView1.SaveLayoutToXml(fileName);
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process a change in the creditor_address checkbox
        /// </summary>
        private void CheckEdit_Preview_CheckStateChanged(object sender, EventArgs e)
        {
            DebtPlus.Configuration.DebtPlusRegistry.SetValue("\\options", "show creditor addresses", CheckEdit_Preview.Checked.ToString());
            Enable_Disable_Addresses(CheckEdit_Preview.Checked);
        }
    }
}