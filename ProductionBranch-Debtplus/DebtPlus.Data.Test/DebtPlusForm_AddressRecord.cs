#region Copyright 2000-2012 DebtPlus, L.L.C.;
// {*******************************************************************}
// {                                                                   }
// {       DebtPlus Debt Management System                             }
// {                                                                   }
// {       Copyright 2000-2012 DebtPlus, L.L.C.                        }
// {       ALL RIGHTS RESERVED                                         }
// {                                                                   }
// {   The entire contents of this file is protected by U.S. and       }
// {   International Copyright Laws. Unauthorized reproduction,        }
// {   reverse-engineering, and distribution of all or any portion of  }
// {   the code contained in this file is strictly prohibited and may  }
// {   result in severe civil and criminal penalties and will be       }
// {   prosecuted to the maximum extent possible under the law.        }
// {                                                                   }
// {   RESTRICTIONS                                                    }
// {                                                                   }
// {   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
// {   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
// {   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
// {   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
// {   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
// {                                                                   }
// {   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
// {   ADDITIONAL RESTRICTIONS.                                        }
// {                                                                   }
// {*******************************************************************}
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace Testing
{
    internal partial class DebtPlusForm_AddressRecord : global::DebtPlus.Data.Forms.DebtPlusForm
    {
        public DebtPlusForm_AddressRecord() : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        void RegisterHandlers()
        {
            Load += Form_Transfer_OperOper_Load;
            Button_Cancel.Click += Button_Cancel_Click;
            addressRecordControl1.AddressChanged += new global::DebtPlus.Data.Controls.AddressRecordControl.AddressChangedEventHandler(addressRecordControl1_AddressChanged);
            addressRecordControl1.StateChanged += new EventHandler(addressRecordControl1_StateChanged);
        }

        void UnRegisterHandlers()
        {
            Load -= Form_Transfer_OperOper_Load;
            Button_Cancel.Click -= Button_Cancel_Click;
            addressRecordControl1.AddressChanged -= new global::DebtPlus.Data.Controls.AddressRecordControl.AddressChangedEventHandler(addressRecordControl1_AddressChanged);
            addressRecordControl1.StateChanged -= new EventHandler(addressRecordControl1_StateChanged);
        }

        void addressRecordControl1_AddressChanged(object sender, global::DebtPlus.Data.Events.AddressChangedEventArgs e)
        {
            var sbChange = new System.Text.StringBuilder();
            if (!e.OldAddress.IsNew())
            {
                sbChange.Append("Address changed from:");
                sbChange.Append(Environment.NewLine);
                sbChange.Append(e.OldAddress.ToString());

                sbChange.Append(Environment.NewLine);
                sbChange.Append(Environment.NewLine);
            }

            sbChange.Append("Address changed to:");
            sbChange.Append(Environment.NewLine);
            sbChange.Append(e.NewAddress.ToString());

            global::DebtPlus.Data.Forms.MessageBox.Show(sbChange.ToString(), "Change Information");
        }

        private void addressRecordControl1_StateChanged(object sender, EventArgs e)
        {
            string msg = string.Format("state changed to '{0}'", addressRecordControl1.States.Text);
            System.Windows.Forms.MessageBox.Show(msg, "StateChanged Event");
        }

        private void Form_Transfer_OperOper_Load(object sender, EventArgs e) // Handles MyBase.Load
        {
            UnRegisterHandlers();
            try
            {
                addressRecordControl1.EditValue = System.DBNull.Value;
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            object iD = addressRecordControl1.EditValue;
            global::DebtPlus.Data.Forms.MessageBox.Show(string.Format("Resulting Value = {0}", iD == System.DBNull.Value ? "System.DBNull.Value" : iD.ToString() ), "Result", MessageBoxButtons.OK);
            Close();
        }

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            addressRecordControl1.ShowAttn = checkEdit1.Checked;
        }

        private void checkEdit2_CheckedChanged(object sender, EventArgs e)
        {
            addressRecordControl1.ShowCreditor1 = checkEdit2.Checked;
        }

        private void checkEdit3_CheckedChanged(object sender, EventArgs e)
        {
            addressRecordControl1.ShowCreditor2 = checkEdit3.Checked;
        }

        private void checkEdit4_CheckedChanged(object sender, EventArgs e)
        {
            addressRecordControl1.ShowLine3 = checkEdit4.Checked;
        }

        private void textEdit1_EditValueChanged(object sender, EventArgs e)
        {
            Int32 Value;
            if (Int32.TryParse(textEdit1.Text, out Value) && Value > 0)
            {
                textEdit1.ErrorText = string.Empty;
                addressRecordControl1.EditValue = Value;
            }
            else
            {
                textEdit1.ErrorText = "Invalid Value";
            }
        }
    }
}