﻿namespace Testing
{
    partial class DebtPlusForm_CreditorID
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            DebtPlusForm_CreditorID_Dispose(disposing);
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.CreditorID_creditor = new global::DebtPlus.UI.Creditor.Widgets.Controls.CreditorID();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreditorID_creditor.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.simpleButton1.Location = new System.Drawing.Point(112, 72);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 4;
            this.simpleButton1.Text = "&OK";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // CreditorID_creditor
            // 
            this.CreditorID_creditor.AllowDrop = true;
            this.CreditorID_creditor.EditValue = null;
            this.CreditorID_creditor.Location = new System.Drawing.Point(112, 12);
            this.CreditorID_creditor.Name = "CreditorID_creditor";
            this.CreditorID_creditor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new global::DebtPlus.UI.Common.Controls.SearchButton()});
            this.CreditorID_creditor.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.CreditorID_creditor.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.CreditorID_creditor.Properties.Mask.BeepOnError = true;
            this.CreditorID_creditor.Properties.Mask.EditMask = "[A-Z]{1,2}[0-9]{4,}";
            this.CreditorID_creditor.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.CreditorID_creditor.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CreditorID_creditor.Properties.MaxLength = 10;
            this.CreditorID_creditor.Size = new System.Drawing.Size(116, 20);
            toolTipTitleItem1.Text = "Creditor ID";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Enter the ID of the creditor into this field.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.CreditorID_creditor.SuperTip = superToolTip1;
            this.CreditorID_creditor.TabIndex = 3;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(53, 15);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(53, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Creditor ID";
            // 
            // DebtPlusForm_CreditorID
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 107);
            this.Controls.Add(this.CreditorID_creditor);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.simpleButton1);
            this.Name = "DebtPlusForm_CreditorID";
            this.Text = "DebtPlusForm_ClientID";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreditorID_creditor.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        protected internal global::DebtPlus.UI.Creditor.Widgets.Controls.CreditorID CreditorID_creditor;
        private DevExpress.XtraEditors.LabelControl labelControl2;
    }
}