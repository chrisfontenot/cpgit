﻿namespace Testing
{
    partial class DebtPlusForm_ClientID
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            DebtPlusForm_ClientID_Dispose(disposing);
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.ClientID_client = new global::DebtPlus.UI.Client.Widgets.Controls.ClientID();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientID_client.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(53, 13);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(41, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Client ID";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.simpleButton1.Location = new System.Drawing.Point(112, 72);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 4;
            this.simpleButton1.Text = "&OK";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // ClientID_client
            // 
            // this.ClientID_client.AllowDrop = true;
            this.ClientID_client.EnterMoveNextControl = true;
            this.ClientID_client.Location = new System.Drawing.Point(112, 10);
            this.ClientID_client.Name = "ClientID_client";
            this.ClientID_client.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.ClientID_client.Properties.Appearance.Options.UseTextOptions = true;
            this.ClientID_client.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ClientID_client.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new global::DebtPlus.UI.Common.Controls.SearchButton()});
            this.ClientID_client.Properties.DisplayFormat.FormatString = "0000000";
            this.ClientID_client.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.ClientID_client.Properties.EditFormat.FormatString = "f0";
            this.ClientID_client.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.ClientID_client.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.None;
            this.ClientID_client.Properties.Mask.BeepOnError = true;
            this.ClientID_client.Properties.Mask.EditMask = "\\d*";
            this.ClientID_client.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.ClientID_client.Properties.ValidateOnEnterKey = true;
            this.ClientID_client.Size = new System.Drawing.Size(116, 20);
            toolTipTitleItem1.Text = "Client ID Number";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Enter the ID number of the client into this field. The client must be acceptable " +
    "for deposits based upon their active state.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.ClientID_client.SuperTip = superToolTip1;
            this.ClientID_client.TabIndex = 1;
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(112, 37);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(116, 20);
            this.textEdit1.TabIndex = 3;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(53, 40);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(43, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "2nd Field";
            // 
            // DebtPlusForm_ClientID
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 107);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.textEdit1);
            this.Controls.Add(this.ClientID_client);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.labelControl1);
            this.Name = "DebtPlusForm_ClientID";
            this.Text = "DebtPlusForm_ClientID";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientID_client.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        protected internal global::DebtPlus.UI.Client.Widgets.Controls.ClientID ClientID_client;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
    }
}