﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Testing
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            DebtPlus.Repository.Setup.ConnectionString = (new DebtPlus.Utils.SQLInfoClass()).ConnectionString;
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new DebtPlusForm_AddressRecord());
        }
    }
}
