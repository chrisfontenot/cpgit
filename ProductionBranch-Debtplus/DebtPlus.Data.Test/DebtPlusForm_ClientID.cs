﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Testing
{
    public partial class DebtPlusForm_ClientID : global::DebtPlus.Data.Forms.DebtPlusForm
    {
        /// <summary>
        /// Process the disposal for the form. Remove any storage allocaetd in the form.
        /// </summary>
        /// <param name="disposing">TRUE when Dispose() is called. FALSE for Finalize.</param>
        private void DebtPlusForm_ClientID_Dispose(bool disposing)
        {
            // Remove the handlers so that the form may be properly destroyed
            if (!DesignMode)
            {
                UnRegisterHandlers();
            }
        }

        /// <summary>
        /// Create a new instance of our class
        /// </summary>
        /// <param name="DatabaseInfo">Pointer to the database connection object</param>
        public DebtPlusForm_ClientID()
            : base()
        {
            InitializeComponent();

            // If this is not the design mode, i.e. we are really running, then register the handlers.
            // If this is design mode then don't as they can cause problems with database accesses, etc.
            if (!DesignMode)
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            this.Load += new EventHandler(DebtPlusForm_ClientID_Load);
        }

        /// <summary>
        /// Remove the event handler registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            this.Load -= new EventHandler(DebtPlusForm_ClientID_Load);
        }

        /// <summary>
        /// Process the LOAD event on the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DebtPlusForm_ClientID_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // TODO -- Fill in the logic when the form is initially loaded.
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}