#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using DebtPlus;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using DebtPlus.UI.Housing.ARM.ARM;
using DebtPlus.UI.Housing.ARM.HUD;

namespace DebtPlus.UI.Housing.ARM
{
    public partial class ParametersForm : DebtPlus.Reports.Template.Forms.DateReportParametersForm
    {
        public ParametersForm() : base()
        {
            this.configInfo = new Configuration();
            InitializeComponent();
        }

        public ParametersForm(Configuration configInfo) : base(DebtPlus.Utils.DateRange.Last_Quarter)
        {
            this.configInfo = configInfo;
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += Form_Load;
            Resize += ParametersForm_Resize;
            checkedListBoxControl1.Resize += checkedListBoxControl1_Resize;
            GroupControl2.Resize += GroupControl2_Resize;
            checkedListBoxControl1.ItemCheck += formChanged;
            CheckEdit_send_all.CheckedChanged += formChanged;
            CheckEdit_send_specific.CheckedChanged += formChanged;
        }

        private void UnRegisterHandlers()
        {
            Load -= Form_Load;
            Resize -= ParametersForm_Resize;
            checkedListBoxControl1.Resize -= checkedListBoxControl1_Resize;
            GroupControl2.Resize -= GroupControl2_Resize;
            checkedListBoxControl1.ItemCheck -= formChanged;
            CheckEdit_send_all.CheckedChanged -= formChanged;
            CheckEdit_send_specific.CheckedChanged -= formChanged;
        }

        private void ParametersForm_Resize(object sender, EventArgs e)
        {
            var clnt = this.ClientSize;
        }

        private void checkedListBoxControl1_Resize(object sender, EventArgs e)
        {
            var clnt = this.ClientSize;
            var groupSize = GroupControl2.Size;
            var boxSize = checkedListBoxControl1.Size;
        }

        private void GroupControl2_Resize(object sender, EventArgs e)
        {
            var clnt = this.ClientSize;
            var groupSize = GroupControl2.Size;
        }

        protected Configuration configInfo;
        protected DebtPlus.UI.Housing.ARM.ARM.AgencyList profiles = new DebtPlus.UI.Housing.ARM.ARM.AgencyList();

        /// <summary>
        /// return the ID for the extract information
        /// </summary>
        public System.Collections.ArrayList Parameter_HCS_ID
        {
            get
            {
                var result = new System.Collections.ArrayList();
                foreach (DevExpress.XtraEditors.Controls.CheckedListBoxItem qDevExpress in checkedListBoxControl1.Items)
                {
                    DebtPlus.Data.Controls.SortedCheckedListboxControlItem q = (DebtPlus.Data.Controls.SortedCheckedListboxControlItem) qDevExpress;
                    if (CheckEdit_send_all.Checked || q.CheckState == System.Windows.Forms.CheckState.Checked)
                    {
                        result.Add(q.Value);
                    }
                }
                return result;
            }
        }

        /// <summary>
        /// Handle the FORM LOAD event
        /// </summary>
        private void Form_Load(object sender, EventArgs e)
        {
            if (!InDesignMode)
            {
                UnRegisterHandlers();
                try
                {
                    // Load the standard list of profiles
                    profiles.GetAgencyProfiles(configInfo);
                    checkedListBoxControl1.Items.Clear();
                    checkedListBoxControl1.SortOrder = System.Windows.Forms.SortOrder.Ascending;

                    foreach (var rec in profiles.AgencyListItems)
                    {
                        if (rec.ActiveFlag)
                        {
                            string description = string.Format("{1} ({0:f0})", rec.agc_hcs_id, rec.Description);
                            DebtPlus.Data.Controls.SortedCheckedListboxControlItem q = new DebtPlus.Data.Controls.SortedCheckedListboxControlItem(rec.agc_hcs_id, description);
                            checkedListBoxControl1.Items.Add(q);
                        }
                    }

                    // Enable or disable the OK button
                    this.ButtonOK.Enabled = !HasErrors();
                }
                finally
                {
                    RegisterHandlers();
                }
            }
        }

        private void formChanged(object sender, EventArgs e)
        {
            EnableList();
            ButtonOK.Enabled = !HasErrors();
        }

        private void EnableList()
        {
            checkedListBoxControl1.Enabled = CheckEdit_send_specific.CheckState == CheckState.Checked;
        }

        /// <summary>
        /// Determine if there are errors on the form
        /// </summary>
        protected override bool HasErrors()
        {
            // Look for any error condition. Disable the OK button if the form is not complete.
            if (base.HasErrors())
            {
                return true;
            }

            if (Parameter_HCS_ID.Count == 0)
            {
                return true;
            }

            return false;
        }
    }
}
