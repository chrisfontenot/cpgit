﻿#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using DebtPlus;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using DebtPlus.UI.Housing.ARM.ARM;
using DebtPlus.UI.Housing.ARM.HUD;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Housing.ARM
{
    partial class MainForm
    {
        // Data from the system for all possible ID tables. It is faster to retrieve all of the data than to
        // have to recalculate it for each ID since there are so many IDs.
        private System.Collections.Generic.List<DebtPlus.LINQ.xpr_housing_arm_v4_client> colClients = null;
        private System.Collections.Generic.List<DebtPlus.LINQ.xpr_housing_arm_v4_group_sessionsResult> colGroupSessions = null;
        private System.Collections.Generic.List<DebtPlus.LINQ.xpr_housing_arm_v4_counselor_languagesResult> colLanguages = null;
        private System.Collections.Generic.List<DebtPlus.LINQ.xpr_housing_arm_v4_counselorsResult> colCounselors = null;
        private System.Collections.Generic.List<DebtPlus.LINQ.xpr_housing_arm_v4_summaryResult> colSummary = null;

        /// <summary>
        /// Issue the command to build the client list. This is the first item.
        /// </summary>
        private void xpr_housing_arm_client_select(DateTime PeriodStart, DateTime PeriodEnd)
        {
            using (var cn = new System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.BusinessContext.ConnectionString()))
            {
                cn.Open();

                using (var bc = new DebtPlus.LINQ.BusinessContext(cn))
                {
                    bc.CommandTimeout = 600;        // increase the command timeout since this may take a while.
                    bc.xpr_housing_arm_v4_client_select(PeriodStart, PeriodEnd);

                    colSummary = bc.xpr_housing_arm_v4_summary();
                    colClients = bc.xpr_housing_arm_v4_client_fetch();
                    colGroupSessions = bc.xpr_housing_arm_v4_group_sessions();
                    colCounselors = bc.xpr_housing_arm_v4_counselors();
                    colLanguages = bc.xpr_housing_arm_v4_counselor_languages();

                    bc.xpr_housing_arm_v4_cleanup();
                }
            }
        }

        /// <summary>
        /// Read the contact information for the agency
        /// </summary>
        /// <param name="CurrentAgency"></param>
        /// <returns></returns>
        private List<HUD.agency_contact> ReadAgencyContacts(ref AgencyProfileData CurrentAgency)
        {
            List<HUD.agency_contact> Answer = new List<HUD.agency_contact>();

            // Find the location of the XML file that describes the contacts
            string Fname = DebtPlus.Configuration.Config.GetConfigValueForKey(DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.HousingContacts), "contactsFile");

            // If the file exists, then read the contact data
            if (System.IO.File.Exists(Fname))
            {
                XmlDocument doc = new XmlDocument();
                doc.PreserveWhitespace = false;
                doc.Load(Fname);

                foreach (XmlNode node in doc)
                {
                    if (node.NodeType == XmlNodeType.Element && node.Name == "contacts")
                    {
                        foreach (XmlNode SubNode in node)
                        {
                            if (SubNode.NodeType == XmlNodeType.Element && SubNode.Name == "contact")
                            {
                                HUD.agency_contact Contact = ReadAgencyContact(SubNode);
                                Answer.Add(Contact);
                            }
                        }
                    }
                }
            }

            return Answer;
        }

        /// <summary>
        /// Read a single agency contact
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public HUD.agency_contact ReadAgencyContact(XmlNode node)
        {
            HUD.agency_contact Contact = new HUD.agency_contact();

            foreach (XmlNode subnode in node)
            {
                if (subnode.NodeType == XmlNodeType.Element)
                {
                    string FieldName = subnode.Name;
                    System.Reflection.PropertyInfo pi = typeof(HUD.agency_contact).GetProperty(FieldName);
                    if (pi == null)
                    {
                        System.Diagnostics.Debug.WriteLine(node.Name);
                    }
                    else
                    {
                        pi.SetValue(Contact, subnode.InnerText, null);
                    }
                }
            }

            return Contact;
        }

        /// <summary>
        /// Build the whole 9902 report information
        /// </summary>
        public bool ReadReportInformation(ref AgencyProfileData CurrentAgency, DateTime PeriodStart, DateTime PeriodEnd)
        {
            bool Answer = false;

            try
            {
                using (var cm = new DebtPlus.UI.Common.CursorManager())
                {
                    hud_9902_clients(CurrentAgency);
                    hud_9902_workshops(ref CurrentAgency);
                    xpr_housing_arm_counselors(CurrentAgency);
                    // ReadCounselorServiceTypes(CurrentAgency); TO BE SUPPLIED LATER
                    xpr_housing_arm_summary(CurrentAgency);
                }

                Answer = true;
            }

            catch (SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error Reading Housing Information");
            }

            return Answer;
        }

        /// <summary>
        /// Extract the workshop attendees from the tables build by the client selection above
        /// </summary>
        private void hud_9902_workshops(ref AgencyProfileData CurrentAgency)
        {
            // Process the list of group sessions for this agency
            var agencyID = CurrentAgency.agc_hcs_id;
            foreach(var item in colGroupSessions.Where(s => s.hcs_id == agencyID))
            {
                // Add the workshop to the list of group sessions
                var workshop = new HUD.group_session()
                {
                    activity_type = 4,      // This is hard-coded for now. It should come from the grant and be passed by xpr_housing_arm_v4_group_sessions.
                    group_session_attribute_hud_grant = item.group_session_attribute_hud_grant.GetValueOrDefault(),
                    group_session_counselor_id = item.group_session_counselor_id,
                    group_session_date = item.group_session_date,
                    group_session_duration = item.group_session_duration,
                    group_session_id = item.group_session_id,
                    group_session_title = item.group_session_title,
                    group_session_type = item.group_session_type.GetValueOrDefault()
                };

                // From the workshop id, find the attendee list
                foreach(var atnde in (from s in colClients where s.is_hud_client > 0 && s.is_workshop > 0 && s.group_session_id == item.group_session_id select s).ToList())
                {
                    var q = new HUD.group_session_attendee()
                    {
                        Attendee_Fee_Amount = atnde.Client_Counseling_Fee.GetValueOrDefault(0M),
                        Attendee_FirstTime_Home_Buyer = atnde.Client_FirstTime_Home_Buyer.GetValueOrDefault(false),
                        attendee_id = atnde.Client_ID_Num.GetValueOrDefault(0),
                        Attendee_referred_by = atnde.Client_Referred_By.GetValueOrDefault(0),

                        group_session_id = item.group_session_id
                    };
                    workshop.group_session_attendees.Add(q);

                    // Determine if the attendee exists in the agency definition. If it does not then add it.
                    var qAgency = (from agncy in CurrentAgency.Attendees where agncy.attendee_id == atnde.Client_ID_Num select agncy).FirstOrDefault();
                    if (qAgency == null)
                    {
                        qAgency = new HUD.attendee()
                        {

                            Household_Lives_In_Rural_Area = atnde.Household_Lives_In_Rural_Area,
                            Household_Does_Not_Live_In_Rural_Area = atnde.Household_Does_Not_Live_In_Rural_Area,
                            Rural_Area_No_Response = atnde.Rural_Area_No_Response,
                            Household_Is_Limited_English_Proficient = atnde.Household_Is_Limited_English_Proficient,
                            Household_Is_Not_Limited_English_Proficient = atnde.Household_Is_Not_Limited_English_Proficient,
                            Limited_English_Proficient_No_Response = atnde.Limited_English_Proficient_No_Response,

                            Attendee_Address_1 = atnde.Client_Street_Address_1,
                            Attendee_Address_2 = atnde.Client_Street_Address_2,
                            Attendee_City = atnde.Client_City,
                            Attendee_Ethnicity_ID = atnde.Client_Ethnicity_ID,
                            Attendee_fname = atnde.Client_First_Name,
                            attendee_id = atnde.Client_ID_Num.Value,
                            Attendee_Income_Level = atnde.Client_Income_Level,
                            Attendee_lname = atnde.Client_Last_Name,
                            Attendee_mname = atnde.Client_Middle_Name,
                            Attendee_Race_ID = atnde.Client_Race_ID,
                            Attendee_State = atnde.Client_State,
                            Attendee_Zip_Code = atnde.Client_ZipCode
                        };

                        // Add the attendee to the list for the agency
                        CurrentAgency.Attendees.Add(qAgency);
                    }
                }

                // Add the group session to the list for the agency
                CurrentAgency.GroupSessions.Add(workshop);
            }
        }

        /// <summary>
        /// Construct the 9902 summary data from the client selection data
        /// </summary>
        private void xpr_housing_arm_summary(AgencyProfileData CurrentAgency)
        {
            foreach(var item in colSummary.Where(s => s.hcs_id == CurrentAgency.agc_hcs_id))
            {
                string FieldName = item.tag;
                System.Reflection.PropertyInfo pi = typeof(HUD.form_9902).GetProperty(FieldName);
                if (pi == null)
                {
                    System.Diagnostics.Debug.WriteLine(FieldName);
                }
                else
                {
                    var v = new HUD.Field_9902();
                    v.HUD_Housing = item.hud_count;
                    v.All_Housing = item.all_count;

                    pi.SetValue(CurrentAgency.Form9902, v, null);
                }
            }
        }

        /// <summary>
        /// Retrieve the counselors for the clients. This is a separate submission.
        /// </summary>
        private void xpr_housing_arm_counselors(AgencyProfileData CurrentAgency)
        {
            // Add distinct counselors to the agency data from the client list
            foreach (var cl in CurrentAgency.ClientList)
            {
                addCounselor(CurrentAgency, cl.Client_Counselor_ID);
            }

            // Add distinct counselors to the agency data from the workshop list
            foreach (var grp in CurrentAgency.GroupSessions)
            {
                addCounselor(CurrentAgency, grp.group_session_counselor_id);
            }
        }

        /// <summary>
        /// Local routine to add new counselors to the list for the agency
        /// </summary>
        private void addCounselor(AgencyProfileData CurrentAgency, Int32? CounselorID)
        {
            if (CounselorID.HasValue)
            {
                var q = (from a in CurrentAgency.CounselorProfiles where a.cms_counselor_id == CounselorID.Value select a).FirstOrDefault();
                if (q == null)
                {
                    var co = (from cnslr in DebtPlus.LINQ.Cache.counselor.getList() where cnslr.Id == CounselorID.Value select cnslr).FirstOrDefault();
                    if (co != null)
                    {
                        q = new counselor_profile()
                        {
                            cms_counselor_id = co.Id,
                            cnslor_billing_method = co.billing_mode,
                            cnslor_emp_end_date = co.emp_end_date,
                            cnslor_emp_start_date = co.emp_start_date,
                            cnslor_HUD_id = co.HUD_id.GetValueOrDefault(0),
                            cnslor_rate = co.rate.GetValueOrDefault(0),
                            cnslor_SSN = co.SSN,
                            cnslor_fname = co.Name == null ? string.Empty : co.Name.First,
                            cnslor_lname = co.Name == null ? string.Empty : co.Name.Last,
                            cnslor_mname = co.Name == null ? string.Empty : co.Name.Middle
                        };

                        // Find the distinct language from the maps. Insert each language only once per counselor.
                        Int32[] aryLanguage = (from l in colLanguages where l.cms_counselor_id == CounselorID.Value select l.Language).Distinct().ToArray();
                        q.languages = (from l in aryLanguage select new HUD.counselor_language() { language = l }).ToList();

                        // Add the counselor to the list
                        CurrentAgency.CounselorProfiles.Add(q);
                    }
                }
            }
        }

        /// <summary>
        /// Construct the list of clients for the submission. This is a separate submission.
        /// </summary>
        private void hud_9902_clients(AgencyProfileData CurrentAgency)
        {
            foreach(var cr in colClients.Where(s => s.is_client > 0 && s.hcs_id == CurrentAgency.agc_hcs_id))
            {
                var ClientInfo = new HUD.client_profile(cr);
                CurrentAgency.ClientList.Add(ClientInfo);
            }
        }

        /// <summary>
        /// Get the list of service types that are performed for each counselor.
        /// </summary>
        /// <param name="CurrentAgency"></param>
        /// <param name="cn"></param>
        /// <param name="txn"></param>
        private void ReadCounselorServiceTypes(AgencyProfileData CurrentAgency, SqlConnection cn, SqlTransaction txn)
        {
            SqlDataReader rd = null;

            try
            {
                using (var cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.Transaction = txn;
                    cmd.CommandText = "SELECT DISTINCT cms_counselor_id, dbo.map_hud_9902_counselor_service_type ( t.HUDServiceType ) as 'service_type' FROM ##hud_9902_counselors x INNER JOIN counselor_attributes a ON x.cms_counselor_id = a.Counselor INNER JOIN AttributeTypes t ON a.[Attribute] = t.[oID] AND t.[Grouping]='HOUSING' ORDER BY 1, 2";
                    cmd.CommandType = CommandType.Text;
                    rd = cmd.ExecuteReader(CommandBehavior.SingleResult);
                }

                bool MoreItems = false;
                if (rd != null)
                {
                    MoreItems = rd.Read();
                    while (MoreItems)
                    {
                        Int32 Counselor = -1;
                        object obj = rd.GetValue(rd.GetOrdinal("cms_counselor_id"));
                        if (obj != null && obj != DBNull.Value)
                        {
                            Counselor = Convert.ToInt32(obj);
                        }

                        // Find the counselor in the list of counselors
                        HUD.counselor_profile CounselorPtr = null;
                        foreach (HUD.counselor_profile item in CurrentAgency.CounselorProfiles)
                        {
                            if (item.cms_counselor_id == Counselor)
                            {
                                CounselorPtr = item;
                                break;
                            }
                        }

                        if (CounselorPtr == null)
                        {
                            MoreItems = rd.Read();
                        }
                        else
                        {
                            MoreItems = ReadCounselorServiceTypeItems(CounselorPtr, ref rd);
                        }
                    }
                }
            }
            finally
            {
                if (rd != null)
                {
                    rd.Dispose();
                }
            }
        }

        /// <summary>
        /// Read the service type information about each counselor's service type
        /// </summary>
        /// <param name="Counselor"></param>
        /// <param name="rd"></param>
        /// <returns></returns>
        private bool ReadCounselorServiceTypeItems(HUD.counselor_profile Counselor, ref SqlDataReader rd)
        {
            bool Answer = true;

            Int32 CurrentCounselor = Counselor.cms_counselor_id;
            while (Answer)
            {
                object obj = rd.GetValue(0);
                if (obj != null && obj != DBNull.Value)
                {
                    Int32 NewCounselor = Convert.ToInt32(obj);
                    if (NewCounselor != CurrentCounselor)
                    {
                        break;
                    }

                    obj = rd.GetValue(1);
                    if (obj != null && obj != DBNull.Value)
                    {
                        HUD.counselor_service_type ServiceType = new HUD.counselor_service_type(Convert.ToInt32(obj));
                        Counselor.service_types.Add(ServiceType);
                    }
                }
                Answer = rd.Read();
            }

            return Answer;
        }
    }
}
