﻿namespace DebtPlus.UI.Housing.ARM
{
    partial class ParametersForm
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode]
        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
{
            DebtPlus.Data.Controls.ComboboxItem comboboxItem2 = new DebtPlus.Data.Controls.ComboboxItem();
            this.GroupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.checkedListBoxControl1 = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.CheckEdit_send_specific = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEdit_send_all = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.XrGroup_param_08_1)).BeginInit();
            this.XrGroup_param_08_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.XrCombo_param_08_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XrDate_param_08_2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XrDate_param_08_2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XrDate_param_08_1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XrDate_param_08_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl2)).BeginInit();
            this.GroupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkedListBoxControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_send_specific.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_send_all.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // XrCombo_param_08_1
            // 
            comboboxItem2.tag = null;
            comboboxItem2.value = DebtPlus.Utils.DateRange.Today;
            this.XrCombo_param_08_1.EditValue = comboboxItem2;
            // 
            // XrDate_param_08_2
            // 
            this.XrDate_param_08_2.EditValue = new System.DateTime(2011, 1, 3, 0, 0, 0, 0);
            this.XrDate_param_08_2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(),
            new DevExpress.XtraEditors.Controls.EditorButton(),
            new DevExpress.XtraEditors.Controls.EditorButton(),
            new DevExpress.XtraEditors.Controls.EditorButton(),
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // XrDate_param_08_1
            // 
            this.XrDate_param_08_1.EditValue = new System.DateTime(2011, 1, 3, 0, 0, 0, 0);
            this.XrDate_param_08_1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(),
            new DevExpress.XtraEditors.Controls.EditorButton(),
            new DevExpress.XtraEditors.Controls.EditorButton(),
            new DevExpress.XtraEditors.Controls.EditorButton(),
            new DevExpress.XtraEditors.Controls.EditorButton()});
            // 
            // ButtonOK
            // 
            this.ButtonOK.TabIndex = 2;
            // 
            // ButtonCancel
            // 
            this.ButtonCancel.TabIndex = 3;
            // 
            // GroupControl2
            // 
            this.GroupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupControl2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.GroupControl2.Controls.Add(this.checkedListBoxControl1);
            this.GroupControl2.Controls.Add(this.CheckEdit_send_specific);
            this.GroupControl2.Controls.Add(this.CheckEdit_send_all);
            this.GroupControl2.Location = new System.Drawing.Point(8, 128);
            this.GroupControl2.Name = "GroupControl2";
            this.GroupControl2.Size = new System.Drawing.Size(316, 151);
            this.GroupControl2.TabIndex = 4;
            this.GroupControl2.Text = "IDs to be sent";
            // 
            // checkedListBoxControl1
            // 
            this.checkedListBoxControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkedListBoxControl1.CheckOnClick = true;
            this.checkedListBoxControl1.Enabled = false;
            this.checkedListBoxControl1.IncrementalSearch = true;
            this.checkedListBoxControl1.Location = new System.Drawing.Point(42, 51);
            this.checkedListBoxControl1.Name = "checkedListBoxControl1";
            this.checkedListBoxControl1.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.checkedListBoxControl1.Size = new System.Drawing.Size(269, 94);
            this.checkedListBoxControl1.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.checkedListBoxControl1.TabIndex = 3;
            // 
            // CheckEdit_send_specific
            // 
            this.CheckEdit_send_specific.Location = new System.Drawing.Point(16, 51);
            this.CheckEdit_send_specific.Name = "CheckEdit_send_specific";
            this.CheckEdit_send_specific.Properties.Caption = "";
            this.CheckEdit_send_specific.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.CheckEdit_send_specific.Properties.RadioGroupIndex = 0;
            this.CheckEdit_send_specific.Size = new System.Drawing.Size(20, 19);
            this.CheckEdit_send_specific.TabIndex = 1;
            this.CheckEdit_send_specific.TabStop = false;
            // 
            // CheckEdit_send_all
            // 
            this.CheckEdit_send_all.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CheckEdit_send_all.EditValue = true;
            this.CheckEdit_send_all.Location = new System.Drawing.Point(16, 26);
            this.CheckEdit_send_all.Name = "CheckEdit_send_all";
            this.CheckEdit_send_all.Properties.Caption = "    Send All";
            this.CheckEdit_send_all.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.CheckEdit_send_all.Properties.RadioGroupIndex = 0;
            this.CheckEdit_send_all.Size = new System.Drawing.Size(295, 19);
            this.CheckEdit_send_all.TabIndex = 0;
            // 
            // ParametersForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(336, 291);
            this.Controls.Add(this.GroupControl2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.Name = "ParametersForm";
            this.Text = "Extract for HUD\'s ARM Project";
            this.Controls.SetChildIndex(this.ButtonOK, 0);
            this.Controls.SetChildIndex(this.ButtonCancel, 0);
            this.Controls.SetChildIndex(this.XrGroup_param_08_1, 0);
            this.Controls.SetChildIndex(this.GroupControl2, 0);
            ((System.ComponentModel.ISupportInitialize)(this.XrGroup_param_08_1)).EndInit();
            this.XrGroup_param_08_1.ResumeLayout(false);
            this.XrGroup_param_08_1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.XrCombo_param_08_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XrDate_param_08_2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XrDate_param_08_2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XrDate_param_08_1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XrDate_param_08_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl2)).EndInit();
            this.GroupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkedListBoxControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_send_specific.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_send_all.Properties)).EndInit();
            this.ResumeLayout(false);

        }
        protected internal DevExpress.XtraEditors.GroupControl GroupControl2;
        protected internal DevExpress.XtraEditors.CheckedListBoxControl checkedListBoxControl1;
        protected internal DevExpress.XtraEditors.CheckEdit CheckEdit_send_specific;
        protected internal DevExpress.XtraEditors.CheckEdit CheckEdit_send_all;
    }
}
