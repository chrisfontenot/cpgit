#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using DebtPlus;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using DebtPlus.UI.Housing.ARM.ARM;
using DebtPlus.UI.Housing.ARM.HUD;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Housing.ARM
{
    public partial class MainForm : ParametersForm
    {
        // Current pointer to the status form
        StatusForm StatusFormInstance;

        // Name of the application when saving the form location/size
        private const string APPLICATION_NAME = "Extract.HUD.MainForm";

        // List of the known agencies
        private System.ComponentModel.BackgroundWorker bt = new System.ComponentModel.BackgroundWorker();

        /// <summary>
        /// Create a new instance of our form
        /// </summary>
        public MainForm(Configuration configInfo)
            : base(configInfo)
        {
            InitializeComponent();
            RegisterHandlers();

            // Hook into the certificate validation logic
            System.Net.ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AllowInvalidCertificates);
        }

        public MainForm()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();

            // Hook into the certificate validation logic
            System.Net.ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AllowInvalidCertificates);
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load += Form_Load;
            ButtonCancel.Click += ButtonCancel_Click;
            bt.DoWork += bt_DoWork;
            bt.RunWorkerCompleted += bt_RunWorkerCompleted;
        }

        /// <summary>
        /// Remove the event handler registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= Form_Load;
            ButtonCancel.Click -= ButtonCancel_Click;
            bt.DoWork -= bt_DoWork;
            bt.RunWorkerCompleted -= bt_RunWorkerCompleted;
        }

        /// <summary>
        /// Called when we need to handle invalid SSL certificates
        /// </summary>
        private static bool AllowInvalidCertificates(object Sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors policyErrors)
        {
            return true;  // accept all certificates at this point.
        }

        /// <summary>
        /// Handle the form LOAD event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                LoadPlacement(APPLICATION_NAME);
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Handle the CANCEL button CLICK event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void ButtonCancel_Click(object sender, EventArgs e)
        {
            // Close the non-modal status dialog.
            if (StatusFormInstance != null)
            {
                StatusFormInstance.Close();
                StatusFormInstance = null;
            }

            // Close this form out.
            Close();
        }

        /// <summary>
        /// Process the OK button CLICK event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void ButtonOK_Click(object sender, EventArgs e)
        {
            if (ButtonOK.Enabled)
            {
                // From the date range, obtain the current values
                configInfo.PeriodStart = Parameter_FromDate;
                configInfo.PeriodEnd = Parameter_ToDate;
                configInfo.itemsToSend = Parameter_HCS_ID;

                // Hide the OK button
                ButtonOK.Enabled = false;

                // Display the status form as a non-modal form.
                // We can do this since we don't have any other modal forms shown.
                StatusFormInstance = new StatusForm(configInfo);
                StatusFormInstance.GridControl1.DataSource = profiles.AgencyListItems;
                StatusFormInstance.GridControl1.RefreshDataSource();
                StatusFormInstance.Show();
                Application.DoEvents();

                bt.RunWorkerAsync();

                // Do the standard logic at this point.
                base.ButtonOK_Click(sender, e);
            }
        }

        /// <summary>
        /// Do the work for the program in the background
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bt_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            // Set the failure status
            e.Result = false;

            Int32 FiscalYear = -1;
            Int32 FiscalQuarter = -1;
            Int32 ReportingMonth = -1;

            try
            {
                using (var bc = new DebtPlus.LINQ.BusinessContext())
                {
                    var q = (from cal in bc.Calendars where cal.Id == configInfo.PeriodEnd select cal).FirstOrDefault();
                    if (q != null)
                    {
                        FiscalYear = q.FY;
                        ReportingMonth = q.FM;
                        FiscalQuarter = q.Q;
                    }
                }
            }

            catch (SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, MainForm_Strings.ErrorReadingFiscalYearMonthFromCalendar);
                e.Result = false;
                return;
            }

            // If there is no mapped value then complain
            if (FiscalYear < 0)
            {
                DebtPlus.Data.Forms.MessageBox.Show(string.Format(MainForm_Strings.UnableToDetermineFiscalYearFromCalendarForDate, XrDate_param_08_1.EditValue), MainForm_Strings.ErrorTitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Result = false;
                return;
            }

            if (FiscalQuarter < 0)
            {
                DebtPlus.Data.Forms.MessageBox.Show(string.Format(MainForm_Strings.UnableToDetermineFiscalQuarterFromCalendar, XrDate_param_08_1.EditValue), MainForm_Strings.ErrorTitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Result = false;
                return;
            }

            // At this point, assume success unless proven otherwise.
            e.Result = true;

            try
            {
                // Extract the data from the database for all IDs.
                xpr_housing_arm_client_select(configInfo.PeriodStart, configInfo.PeriodEnd);
            }

            catch (Exception ex)
            {
                WriteToErrorLog(configInfo, ex.Message);
                e.Result = false;
                return;
            }

            // Process all possible agencies in the list.
            for (Int32 Ordinal = 0; Ordinal < profiles.AgencyListItems.Count; Ordinal += 1)
            {
                // Look for agencies that we wish to send.
                configInfo.UpdateAgency = ((AgencyProfileData)profiles.AgencyListItems[Ordinal]);
                if (!configInfo.itemsToSend.Contains(configInfo.UpdateAgency.agc_hcs_id))
                {
                    continue;
                }

                try
                {
                    Application.DoEvents();
                    if (!SendHCS_ID(ref configInfo.UpdateAgency, FiscalYear, FiscalQuarter, ReportingMonth, configInfo.PeriodStart, configInfo.PeriodEnd))
                    {
                        e.Result = false;
                    }
                    Application.DoEvents();
                }

                catch (Exception ex)
                {
                    WriteToErrorLog(configInfo, ex.Message);
                    cls_StatusMsgUpdate(configInfo.UpdateAgency, new DebtPlus.UI.Housing.ARM.HUD.EventMessageArgs(ex.Message));
                    Application.DoEvents();
                    e.Result = false;
                }

                finally
                {
                    configInfo.UpdateAgency = null;
                }
            }
        }

        /// <summary>
        /// When the work is complete, notify the user
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bt_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            ButtonOK.Enabled = true;
            if (Convert.ToBoolean(e.Result))
            {
                DebtPlus.Data.Forms.MessageBox.Show("The Operation Has Completed", "Operation Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                DebtPlus.Data.Forms.MessageBox.Show("The Operation Has Completed", "Operation Completed", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Update the submission date line
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cls_StatusDateUpdate(object sender, DebtPlus.UI.Housing.ARM.HUD.EventMessageArgs e)
        {
            if (StatusFormInstance.GridControl1.InvokeRequired)
            {
                IAsyncResult ia = StatusFormInstance.GridControl1.BeginInvoke(new DebtPlus.UI.Housing.ARM.HUD.MessageUpdate(cls_StatusDateUpdate), new object[] { sender, e });
                StatusFormInstance.GridControl1.EndInvoke(ia);
            }
            else
            {
                AgencyProfileData agency = (AgencyProfileData)sender;
                agency.SubmissionDate = e.ItemDate.ToString();
                Int32 RowHandle = StatusFormInstance.GridView1.GetRowHandle(profiles.AgencyListItems.IndexOf(agency));
                if (RowHandle >= 0)
                {
                    StatusFormInstance.GridView1.RefreshRow(RowHandle);
                }
            }
        }

        /// <summary>
        /// Update the submission status message line
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cls_StatusMsgUpdate(object sender, DebtPlus.UI.Housing.ARM.HUD.EventMessageArgs e)
        {
            if (StatusFormInstance.GridControl1.InvokeRequired)
            {
                IAsyncResult ia = StatusFormInstance.GridControl1.BeginInvoke(new DebtPlus.UI.Housing.ARM.HUD.MessageUpdate(cls_StatusMsgUpdate), new object[] { sender, e });
                StatusFormInstance.GridControl1.EndInvoke(ia);
            }
            else
            {
                AgencyProfileData agency = (AgencyProfileData)sender;
                agency.SubmissionStatus = e.Msg;
                Int32 RowHandle = StatusFormInstance.GridView1.GetRowHandle(profiles.AgencyListItems.IndexOf(agency));
                if (RowHandle >= 0)
                {
                    StatusFormInstance.GridView1.RefreshRow(RowHandle);
                }
            }
        }

        /// <summary>
        /// Update the submission status ID line
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cls_StatusIDUpdate(object sender, DebtPlus.UI.Housing.ARM.HUD.EventMessageArgs e)
        {
            if (StatusFormInstance.GridControl1.InvokeRequired)
            {
                IAsyncResult ia = StatusFormInstance.GridControl1.BeginInvoke(new DebtPlus.UI.Housing.ARM.HUD.MessageUpdate(cls_StatusIDUpdate), new object[] { sender, e });
                StatusFormInstance.GridControl1.EndInvoke(ia);
            }
            else
            {
                AgencyProfileData agency = (AgencyProfileData)sender;
                agency.SubmissionID = e.Msg;
                Int32 RowHandle = StatusFormInstance.GridView1.GetRowHandle(profiles.AgencyListItems.IndexOf(agency));
                if (RowHandle >= 0)
                {
                    StatusFormInstance.GridView1.RefreshRow(RowHandle);
                }
            }
        }
    }
}
