#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using DebtPlus;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using DebtPlus.UI.Housing.ARM.ARM;
using DebtPlus.UI.Housing.ARM.HUD;
using System.Collections.Specialized;

namespace DebtPlus.UI.Housing.ARM
{
    public class Configuration : IDisposable
    {
        /// <summary>
        /// Pointer to the arguments given to the program
        /// </summary>
        public ArgParser ap;

        // Values from the parameters dialog
        public DateTime PeriodStart;
        public DateTime PeriodEnd;
        public System.Collections.ArrayList itemsToSend;
        public AgencyProfileData UpdateAgency;

        /// <summary>
        /// Initialize a new class instance
        /// </summary>
        public Configuration()
        {
        }

        private NameValueCollection privateConfigSettings = null;
        /// <summary>
        /// Configuration information
        /// </summary>
        public NameValueCollection ConfigSettings
        {
            get
            {
                if (privateConfigSettings == null)
                {
                    privateConfigSettings = DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.ExtractArmV4);
                }
                return privateConfigSettings;
            }
        }

        /// <summary>
        /// Filename for error logging
        /// </summary>
        public string ErrorLog
        {
            get
            {
                string Answer = DebtPlus.Configuration.Config.GetConfigValueForKey(ConfigSettings, "ERRORLOG");
                if (string.IsNullOrEmpty(Answer))
                {
                    Answer = string.Empty;
                }
                Answer = Answer.Trim();
                
                // If the file name starts with [Documents]/ then use the documents folder
                if (System.Text.RegularExpressions.Regex.IsMatch(Answer, @"^\[Documents\][/\\]", System.Text.RegularExpressions.RegexOptions.IgnoreCase))
                {
                    Answer = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), Answer.Substring(12));
                }
                return Answer;
            }
        }

        /// <summary>
        /// Filename for tracing information
        /// </summary>
        public string TraceFile
        {
            get
            {
                string Answer = DebtPlus.Configuration.Config.GetConfigValueForKey(ConfigSettings, "TraceFile");
                if (string.IsNullOrEmpty(Answer))
                {
                    Answer = string.Empty;
                }
                Answer = Answer.Trim();

                // If the file name starts with [Documents]/ then use the documents folder
                if (System.Text.RegularExpressions.Regex.IsMatch(Answer, @"^\[Documents\][/\\]", System.Text.RegularExpressions.RegexOptions.IgnoreCase))
                {
                    Answer = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), Answer.Substring(12));
                }
                return Answer;
            }
        }

        /// <summary>
        /// URL to the HUD server
        /// </summary>
        public string URL
        {
            get
            {
                string Value = DebtPlus.Configuration.Config.GetConfigValueForKey(ConfigSettings, "URL");
                if (! string.IsNullOrWhiteSpace(Value))
                {
                    return Value;
                }
                return string.Empty;
            }
        }

        /// <summary>
        /// Agency CMS type used by HUD
        /// </summary>
        public Int32 agc_cms_type
        {
            get
            {
                string strValue = DebtPlus.Configuration.Config.GetConfigValueForKey(ConfigSettings, "CMSID");
                if (!string.IsNullOrWhiteSpace(strValue))
                {
                    Int32 value;
                    if (Int32.TryParse(strValue, System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.InvariantCulture, out value) && value > 0)
                    {
                        return value;
                    }
                }
                return 29;
            }
        }

        /// <summary>
        /// Agency CMS type used by HUD
        /// </summary>
        public string agc_cms_name
        {
            get
            {
                string strValue = DebtPlus.Configuration.Config.GetConfigValueForKey(ConfigSettings, "CMSNAME");
                if (!string.IsNullOrWhiteSpace(strValue))
                {
                    return strValue;
                }
                return "DebtPlus";
            }
        }

        /// <summary>
        /// Agency CMS type used by HUD
        /// </summary>
        public string agc_cms_Password
        {
            get
            {
                string strValue = DebtPlus.Configuration.Config.GetConfigValueForKey(ConfigSettings, "CMSPWD");
                if (! string.IsNullOrWhiteSpace(strValue))
                {
                    return strValue;
                }
                return "xSw6V2j7"; // old password = "4VyP4Uva";
            }
        }

        /// <summary>
        /// Interval for updating the status information
        /// </summary>
        public Int32 StatusUpdateInterval
        {
            get
            {
                string value = DebtPlus.Configuration.Config.GetConfigValueForKey(ConfigSettings, "STATUSUPDATEINTERVAL");
                if (value != null)
                {
                    Int32 answer;
                    if (Int32.TryParse(value, System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.InvariantCulture, out answer) && answer > 0)
                    {
                        return answer * 1000;
                    }
                }

                return 60000;
            }
        }

        /// <summary>
        /// Type of compression to be used
        /// </summary>
        public string COMPRESSION
        {
            get
            {
                string Value = DebtPlus.Configuration.Config.GetConfigValueForKey(ConfigSettings, "COMPRESSION");
                if (!string.IsNullOrWhiteSpace(Value))
                {
                    switch (Value.ToLower())
                    {
                        case "gzip":
                            return "GZIP";

                        case "zip":
                            return "ZIP";

                        default:
                            break;
                    }
                }

                return "TEXT/XML";
            }
        }

        public System.Collections.Generic.List<getReferenceResponseReferenceItem> FiscalYears = new System.Collections.Generic.List<getReferenceResponseReferenceItem>();
        public System.Collections.Generic.List<getReferenceResponseReferenceItem> FiscalQuarters = new System.Collections.Generic.List<getReferenceResponseReferenceItem>();

        /// <summary>
        /// Map the current fiscal year into the value needed by HUD
        /// </summary>
        /// <param name="CurrentAgency"></param>
        /// <param name="FiscalYear"></param>
        /// <returns></returns>
        public Int32 map_fiscal_year(ref ArmServiceService svc, AgencyProfileData CurrentAgency, Int32 FiscalYear)
        {
            string TargetString = string.Format("{0:f0}", FiscalYear);

            // If there is no list then obtain the current reference items
            if (FiscalYears.Count <= 0)
            {
                getReference req = new getReference();
                req.agcHcsId = CurrentAgency.agc_hcs_id;
                req.referenceId = 28;

                getReferenceResponseReferenceItem[] Result = svc.getReference(req);
                FiscalYears.AddRange(Result);
            }

            // Locate the reporting fiscal year
            Int32 MappedFiscalYear = -1;
            foreach (var yr in FiscalYears)
            {
                if (yr.name.Trim() == TargetString)
                {
                    MappedFiscalYear = yr.id;
                    break;
                }
            }

            return MappedFiscalYear;
        }

        /// <summary>
        /// Map the current fiscal quarter into the value needed by HUD
        /// </summary>
        public Int32 map_fiscal_quarter(ref ArmServiceService svc, AgencyProfileData CurrentAgency, Int32 FiscalQuarter)
        {
            string TargetString = string.Format("Quarter {0:f0}", FiscalQuarter);
            //cls_StatusUpdate(string.Format(MainForm_Strings.AttemptingToMapToAValue, TargetString))

            // If there is no list then obtain the current reference items
            if (FiscalQuarters.Count <= 0)
            {
                getReference req = new getReference();
                req.agcHcsId = CurrentAgency.agc_hcs_id;
                req.referenceId = 29;

                getReferenceResponseReferenceItem[] Result = svc.getReference(req);
                FiscalQuarters.AddRange(Result);
            }

            // Locate the reporting fiscal year
            Int32 MappedFiscalQuarter = -1;
            foreach (var q in FiscalQuarters)
            {
                if (q.name.Trim() == TargetString)
                {
                    MappedFiscalQuarter = q.id;
                    break;
                }
            }

            return MappedFiscalQuarter;
        }

        /// <summary>
        /// Handle the disposal of the objects
        /// </summary>
        /// <param name="disposing">TRUE if disposing, FALSE if finalizing</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                UpdateAgency.Dispose();
            }
            UpdateAgency = null;
            privateConfigSettings = null;
        }

        bool IsDisposed = false;
        /// <summary>
        /// Dispose of local storage
        /// </summary>
        public void Dispose()
        {
            try
            {
                if (!IsDisposed)
                {
                    IsDisposed = true;
                    Dispose(true);
                }
            }
            finally
            {
                GC.SuppressFinalize(this);
            }
        }

        /// <summary>
        /// Called when dispose is not called and the information is simply to be tossed
        /// </summary>
        ~Configuration()
        {
            Dispose(false);
        }
    }
}
