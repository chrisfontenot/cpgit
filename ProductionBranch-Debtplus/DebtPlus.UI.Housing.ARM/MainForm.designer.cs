﻿namespace DebtPlus.UI.Housing.ARM
{
    partial class MainForm
    {

        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode]
        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            DebtPlus.Data.Controls.ComboboxItem comboboxItem1 = new DebtPlus.Data.Controls.ComboboxItem();
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl2)).BeginInit();
            this.GroupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkedListBoxControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_send_specific.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_send_all.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XrGroup_param_08_1)).BeginInit();
            this.XrGroup_param_08_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.XrCombo_param_08_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XrDate_param_08_2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XrDate_param_08_2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XrDate_param_08_1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.XrDate_param_08_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // GroupControl2
            // 
            this.GroupControl2.Size = new System.Drawing.Size(316, 173);
            // 
            // checkedListBoxControl1
            // 
            this.checkedListBoxControl1.Size = new System.Drawing.Size(269, 116);
            // 
            // CheckEdit_send_specific
            // 
            // 
            // CheckEdit_send_all
            // 
            // 
            // XrCombo_param_08_1
            // 
            comboboxItem1.tag = null;
            comboboxItem1.value = DebtPlus.Utils.DateRange.Today;
            this.XrCombo_param_08_1.EditValue = comboboxItem1;
            // 
            // XrDate_param_08_2
            // 
            this.XrDate_param_08_2.EditValue = new System.DateTime(2015, 3, 12, 0, 0, 0, 0);
            // 
            // XrDate_param_08_1
            // 
            this.XrDate_param_08_1.EditValue = new System.DateTime(2015, 3, 12, 0, 0, 0, 0);
            // 
            // ButtonOK
            // 
            this.ButtonOK.Location = new System.Drawing.Point(266, 17);
            this.ButtonOK.Text = "&Send Extract";
            // 
            // ButtonCancel
            // 
            this.ButtonCancel.Location = new System.Drawing.Point(266, 52);
            this.ButtonCancel.Text = "&Quit";
            // 
            // MainForm
            // 
            this.ClientSize = new System.Drawing.Size(354, 313);
            this.Name = "MainForm";
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl2)).EndInit();
            this.GroupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkedListBoxControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_send_specific.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_send_all.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XrGroup_param_08_1)).EndInit();
            this.XrGroup_param_08_1.ResumeLayout(false);
            this.XrGroup_param_08_1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.XrCombo_param_08_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XrDate_param_08_2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XrDate_param_08_2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XrDate_param_08_1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.XrDate_param_08_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            this.ResumeLayout(false);

        }
    }
}
