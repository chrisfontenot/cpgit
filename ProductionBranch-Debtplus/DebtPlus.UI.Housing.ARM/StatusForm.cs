#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using DebtPlus;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using DebtPlus.UI.Housing.ARM.ARM;
using DebtPlus.UI.Housing.ARM.HUD;

namespace DebtPlus.UI.Housing.ARM
{
    public partial class StatusForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        public StatusForm()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private Configuration configInfo;
        public StatusForm(Configuration configInfo)
            : this()
        {
            this.configInfo = configInfo;
        }

        private void RegisterHandlers()
        {
            BarButtonItem1.ItemClick += BarButtonItem1_ItemClick;
            BarButtonItem2.ItemClick += BarButtonItem2_ItemClick;
        }

        private void UnRegisterHandlers()
        {
            BarButtonItem1.ItemClick -= BarButtonItem1_ItemClick;
            BarButtonItem2.ItemClick -= BarButtonItem2_ItemClick;
        }

        /// <summary>
        /// Print the client list
        /// </summary>
        private void BarButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) // Handles BarButtonItem1.ItemClick
        {

            // Force the wait cursor
            Cursor currentCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;

            // If there is printing then show the printing function
            if (DevExpress.XtraPrinting.ComponentPrinter.IsPrintingAvailable(true))
            {
                DevExpress.XtraPrinting.ComponentPrinter printer = new DevExpress.XtraPrinting.ComponentPrinter(GridControl1);
                printer.ShowPreview(this, (new DevExpress.LookAndFeel.DefaultLookAndFeel()).LookAndFeel);
            }
            else
            {
                DebtPlus.Data.Forms.MessageBox.Show(this, "XtraPrinting Library is not found...", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            // Restore the cursor
            Cursor.Current = currentCursor;
        }

        private void BarButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e) // Handles BarButtonItem2.ItemClick
        {
            Close();
        }
    }
}
