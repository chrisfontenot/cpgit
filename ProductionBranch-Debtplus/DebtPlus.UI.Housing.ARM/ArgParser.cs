using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.UI.Housing.ARM.ARM;
using DebtPlus.UI.Housing.ARM.HUD;

namespace DebtPlus.UI.Housing.ARM
{
    /// <summary>
    /// Argument Parsing Class
    /// </summary>
    public class ArgParser : DebtPlus.Utils.ArgParserBase
    {
        /// <summary>
        /// Create an instance of our class
        /// </summary>
        public ArgParser()
            : base(new string[] { })
        {
        }

        /// <summary>
        /// Generate the command usage information
        /// </summary>
        protected override void OnUsage(string errorInfo)
        {
            if (errorInfo != null)
            {
                AppendErrorLine(string.Format("Invalid parameter: {0}" + Environment.NewLine, errorInfo));
            }

            System.Reflection.Assembly _asm = System.Reflection.Assembly.GetExecutingAssembly();
            string fname = System.IO.Path.GetFileName(_asm.CodeBase);
            AppendErrorLine("Usage: " + fname);
        }
    }
}
