using System;

namespace DebtPlus.UI.Housing.ARM.Report
{
    partial class HousingReport
	{
        //Component overrides dispose to clean up the component list.
        protected override void Dispose(Boolean disposing)
		{
            try
			{
                if (disposing)
				{
                    if (components != null)
					{
                        components.Dispose();
					}
                    cnf.Dispose();
                }
            }
			finally
			{
                base.Dispose(disposing);
            }
        }

        //Required by the Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Designer
        //It can be modified using the Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
            this.ParameterHCS_ID = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.summaryReport1 = new DebtPlus.UI.Housing.Report.SummaryReport();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.summaryReport1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // PageHeader
            // 
            this.PageHeader.HeightF = 106.3334F;
            // 
            // XrLabel_Title
            // 
            this.XrLabel_Title.StylePriority.UseFont = false;
            // 
            // XrPageInfo_PageNumber
            // 
            this.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = false;
            // 
            // XRLabel_Agency_Name
            // 
            this.XRLabel_Agency_Name.StylePriority.UseTextAlignment = false;
            // 
            // XrLabel_Agency_Address3
            // 
            this.XrLabel_Agency_Address3.StylePriority.UseFont = false;
            this.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = false;
            // 
            // XrLabel_Agency_Address1
            // 
            this.XrLabel_Agency_Address1.StylePriority.UseFont = false;
            this.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = false;
            // 
            // XrLabel_Agency_Phone
            // 
            this.XrLabel_Agency_Phone.StylePriority.UseFont = false;
            this.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = false;
            // 
            // XrLabel_Agency_Address2
            // 
            this.XrLabel_Agency_Address2.StylePriority.UseFont = false;
            this.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = false;
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport1});
            this.Detail.HeightF = 23F;
            // 
            // ParameterHCS_ID
            // 
            this.ParameterHCS_ID.Description = "HCS ID";
            this.ParameterHCS_ID.Name = "ParameterHCS_ID";
            this.ParameterHCS_ID.Type = typeof(int);
            this.ParameterHCS_ID.Value = -1;
            this.ParameterHCS_ID.Visible = false;
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ReportSource = this.summaryReport1;
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(800F, 23F);
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.GroupHeader1.HeightF = 48.95833F;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.RepeatEveryPage = true;
            // 
            // xrTable1
            // 
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 10.00001F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(742F, 25F);
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell3,
            this.xrTableCell4});
            this.xrTableRow1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableRow1.ForeColor = System.Drawing.Color.Maroon;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.StylePriority.UseFont = false;
            this.xrTableRow1.StylePriority.UseForeColor = false;
            this.xrTableRow1.StylePriority.UseTextAlignment = false;
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "HCS ID";
            this.xrTableCell1.Weight = 0.43939392089843754D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "agc_hcs_id", "{0:00000000}")});
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Weight = 0.93939392089843754D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Text = "NAME";
            this.xrTableCell3.Weight = 0.39393954190340918D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Description")});
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Weight = 3.6236362526633523D;
            // 
            // HousingReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.TopMarginBand1,
            this.Detail,
            this.BottomMarginBand1,
            this.PageHeader,
            this.PageFooter,
            this.GroupHeader1});
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.ParameterHCS_ID});
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.XrControlStyle_Header,
            this.XrControlStyle_Totals,
            this.XrControlStyle_HeaderPannel,
            this.XrControlStyle_GroupHeader});
            this.Version = "11.2";
            this.Controls.SetChildIndex(this.GroupHeader1, 0);
            this.Controls.SetChildIndex(this.PageFooter, 0);
            this.Controls.SetChildIndex(this.PageHeader, 0);
            this.Controls.SetChildIndex(this.BottomMarginBand1, 0);
            this.Controls.SetChildIndex(this.Detail, 0);
            this.Controls.SetChildIndex(this.TopMarginBand1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.summaryReport1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        internal DevExpress.XtraReports.Parameters.Parameter ParameterHCS_ID;
        private DevExpress.XtraReports.UI.XRSubreport xrSubreport1;
        private Housing.Report.SummaryReport summaryReport1;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
    }
}