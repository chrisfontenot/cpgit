﻿namespace DebtPlus.UI.Housing.Report
{
    partial class SummaryReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.detailBand1 = new DevExpress.XtraReports.UI.DetailBand();
            this.XrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow001 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_001_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow002 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_002_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_002_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_002_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_002_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow003 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_003_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_003_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_003_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_003_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow004 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_004_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_004_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_004_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_004_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow005 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_005_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_005_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_005_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow006 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_006_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow007 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_007_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow008 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_008_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow009 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_009_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_009_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_009_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_009_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow010 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_010_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_010_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_010_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_010_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow011 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_011_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_011_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_011_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_011_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow012 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_012_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_012_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_012_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_012_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow013 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_013_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_013_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_013_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_013_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow014 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_014_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow015 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_015_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_015_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_015_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_015_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow016 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_016_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_016_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_016_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_016_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow017 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_017_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_017_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_017_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_017_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow018 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_018_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_018_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_018_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_018_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow019 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_019_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_019_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_019_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_019_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow020 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_020_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_020_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_020_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_020_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow021 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_021_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_021_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_021_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow022 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_022_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow023 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_023_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow024 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_024_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_024_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_024_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_024_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow024B = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_024B_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_024B_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_024B_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_024B_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow025 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_025_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_025_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_025_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_025_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow026 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_026_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_026_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_026_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_026_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow027 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_027_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_027_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_027_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_027_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow028 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_028_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_028_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_028_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_028_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow029 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_029_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_029_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_029_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow030 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_030_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow031 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_031_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow032 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_032_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_032_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_032_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_032_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow033 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_033_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_033_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_033_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_033_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow034 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_034_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_034_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_034_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_034_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow035 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_035_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_035_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_035_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow036 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_036_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow037 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_037_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow038 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_038_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_038_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_038_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_038_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow039 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_039_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_039_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_039_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_039_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_039B_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_039B_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_039B_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_039B_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow040 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_040_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_040_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_040_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow041 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_041_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow043 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_043_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow044 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_044_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_044_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_044_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_044_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow045 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow046 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell383 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow047 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell384 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow048 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell385 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow049 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell386 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow050 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell387 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow051 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell388 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow055 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell392 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow052 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell389 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow053 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow054 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow056 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell168 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell393 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow057 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell151 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell152 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell394 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow058 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell180 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell181 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell182 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell395 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow059 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell186 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell187 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell188 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell396 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow060 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell183 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell184 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell185 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell397 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow061 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell165 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell166 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell167 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell398 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow062 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell178 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell179 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell399 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow064 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell171 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow065 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow063 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell175 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell400 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow066 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell159 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell160 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell161 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell403 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow067 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell156 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell157 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell158 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell404 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow068 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell139 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell149 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell405 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow069 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell217 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell218 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell219 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell406 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow070 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell214 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell215 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell216 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell407 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow071 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell211 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell212 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell213 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell408 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow075 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell199 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell200 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell201 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell412 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow076 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell196 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell197 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell198 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell413 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow077 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell227 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell228 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell229 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell414 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow078 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell233 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell234 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell235 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell415 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow079 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell230 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell231 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell232 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell416 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow080 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell190 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell191 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell192 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell417 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow081 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell240 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell241 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell418 = new DevExpress.XtraReports.UI.XRTableCell();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.XrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            ((System.ComponentModel.ISupportInitialize)(this.XrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.HeightF = 0F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // detailBand1
            // 
            this.detailBand1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.XrTable1});
            this.detailBand1.HeightF = 1209.607F;
            this.detailBand1.Name = "detailBand1";
            // 
            // XrTable1
            // 
            this.XrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.XrTable1.Name = "XrTable1";
            this.XrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 0, 0, 96F);
            this.XrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow001,
            this.xrTableRow002,
            this.xrTableRow003,
            this.xrTableRow004,
            this.xrTableRow005,
            this.xrTableRow006,
            this.xrTableRow007,
            this.xrTableRow008,
            this.xrTableRow009,
            this.xrTableRow010,
            this.xrTableRow011,
            this.xrTableRow012,
            this.xrTableRow013,
            this.xrTableRow014,
            this.xrTableRow015,
            this.xrTableRow016,
            this.xrTableRow017,
            this.xrTableRow018,
            this.xrTableRow019,
            this.xrTableRow020,
            this.xrTableRow021,
            this.xrTableRow022,
            this.xrTableRow023,
            this.xrTableRow024,
            this.xrTableRow024B,
            this.xrTableRow025,
            this.xrTableRow026,
            this.xrTableRow027,
            this.xrTableRow028,
            this.xrTableRow029,
            this.xrTableRow030,
            this.xrTableRow031,
            this.xrTableRow032,
            this.xrTableRow033,
            this.xrTableRow034,
            this.xrTableRow035,
            this.xrTableRow036,
            this.xrTableRow037,
            this.xrTableRow038,
            this.xrTableRow039,
            this.xrTableRow1,
            this.xrTableRow040,
            this.xrTableRow041,
            this.xrTableRow043,
            this.xrTableRow044,
            this.xrTableRow045,
            this.xrTableRow046,
            this.xrTableRow047,
            this.xrTableRow048,
            this.xrTableRow049,
            this.xrTableRow050,
            this.xrTableRow051,
            this.xrTableRow055,
            this.xrTableRow052,
            this.xrTableRow053,
            this.xrTableRow054,
            this.xrTableRow056,
            this.xrTableRow057,
            this.xrTableRow058,
            this.xrTableRow059,
            this.xrTableRow060,
            this.xrTableRow061,
            this.xrTableRow062,
            this.xrTableRow2,
            this.xrTableRow064,
            this.xrTableRow065,
            this.xrTableRow063,
            this.xrTableRow066,
            this.xrTableRow067,
            this.xrTableRow068,
            this.xrTableRow069,
            this.xrTableRow070,
            this.xrTableRow071,
            this.xrTableRow075,
            this.xrTableRow076,
            this.xrTableRow077,
            this.xrTableRow078,
            this.xrTableRow079,
            this.xrTableRow080,
            this.xrTableRow081});
            this.XrTable1.SizeF = new System.Drawing.SizeF(651.0417F, 1209.607F);
            this.XrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow001
            // 
            this.xrTableRow001.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_001_1});
            this.xrTableRow001.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableRow001.Name = "xrTableRow001";
            this.xrTableRow001.StylePriority.UseFont = false;
            this.xrTableRow001.Weight = 0.43934559037132359D;
            // 
            // xrTableCell_001_1
            // 
            this.xrTableCell_001_1.BackColor = System.Drawing.Color.LightGray;
            this.xrTableCell_001_1.Name = "xrTableCell_001_1";
            this.xrTableCell_001_1.StylePriority.UseBackColor = false;
            this.xrTableCell_001_1.Text = "3. Ethnicity of Clients";
            this.xrTableCell_001_1.Weight = 3D;
            // 
            // xrTableRow002
            // 
            this.xrTableRow002.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_002_1,
            this.xrTableCell_002_2,
            this.xrTableCell_002_3,
            this.xrTableCell_002_4});
            this.xrTableRow002.Name = "xrTableRow002";
            this.xrTableRow002.Weight = 0.43934562533570271D;
            // 
            // xrTableCell_002_1
            // 
            this.xrTableCell_002_1.Name = "xrTableCell_002_1";
            this.xrTableCell_002_1.Text = "a.";
            this.xrTableCell_002_1.Weight = 0.33185839653015137D;
            // 
            // xrTableCell_002_2
            // 
            this.xrTableCell_002_2.Name = "xrTableCell_002_2";
            this.xrTableCell_002_2.Text = "Hispanic";
            this.xrTableCell_002_2.Weight = 1.7323892354666142D;
            // 
            // xrTableCell_002_3
            // 
            this.xrTableCell_002_3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Ethnicity_Clients_Counseling_Hispanic.All_Housing", "{0:n0}")});
            this.xrTableCell_002_3.Name = "xrTableCell_002_3";
            this.xrTableCell_002_3.StylePriority.UseTextAlignment = false;
            this.xrTableCell_002_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_002_3.Weight = 0.43392007250425846D;
            // 
            // xrTableCell_002_4
            // 
            this.xrTableCell_002_4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Ethnicity_Clients_Counseling_Hispanic.HUD_Housing", "{0:n0}")});
            this.xrTableCell_002_4.Name = "xrTableCell_002_4";
            this.xrTableCell_002_4.StylePriority.UseTextAlignment = false;
            this.xrTableCell_002_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_002_4.Weight = 0.50183238490594329D;
            // 
            // xrTableRow003
            // 
            this.xrTableRow003.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_003_1,
            this.xrTableCell_003_2,
            this.xrTableCell_003_3,
            this.xrTableCell_003_4});
            this.xrTableRow003.Name = "xrTableRow003";
            this.xrTableRow003.Weight = 0.43934562533570265D;
            // 
            // xrTableCell_003_1
            // 
            this.xrTableCell_003_1.Name = "xrTableCell_003_1";
            this.xrTableCell_003_1.Text = "b.";
            this.xrTableCell_003_1.Weight = 0.33185839653015137D;
            // 
            // xrTableCell_003_2
            // 
            this.xrTableCell_003_2.Name = "xrTableCell_003_2";
            this.xrTableCell_003_2.Text = "Not Hispanic";
            this.xrTableCell_003_2.Weight = 1.7323889542166144D;
            // 
            // xrTableCell_003_3
            // 
            this.xrTableCell_003_3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Ethnicity_Clients_Counseling_Non_Hispanic.All_Housing", "{0:n0}")});
            this.xrTableCell_003_3.Name = "xrTableCell_003_3";
            this.xrTableCell_003_3.StylePriority.UseTextAlignment = false;
            this.xrTableCell_003_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_003_3.Weight = 0.43392007250425846D;
            // 
            // xrTableCell_003_4
            // 
            this.xrTableCell_003_4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Ethnicity_Clients_Counseling_Non_Hispanic.HUD_Housing", "{0:n0}")});
            this.xrTableCell_003_4.Name = "xrTableCell_003_4";
            this.xrTableCell_003_4.StylePriority.UseTextAlignment = false;
            this.xrTableCell_003_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_003_4.Weight = 0.50183266615594291D;
            // 
            // xrTableRow004
            // 
            this.xrTableRow004.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_004_1,
            this.xrTableCell_004_2,
            this.xrTableCell_004_3,
            this.xrTableCell_004_4});
            this.xrTableRow004.Name = "xrTableRow004";
            this.xrTableRow004.Weight = 0.43934562533570265D;
            // 
            // xrTableCell_004_1
            // 
            this.xrTableCell_004_1.Name = "xrTableCell_004_1";
            this.xrTableCell_004_1.Text = "c.";
            this.xrTableCell_004_1.Weight = 0.33185839653015137D;
            // 
            // xrTableCell_004_2
            // 
            this.xrTableCell_004_2.Name = "xrTableCell_004_2";
            this.xrTableCell_004_2.Text = "Chose not to respond";
            this.xrTableCell_004_2.Weight = 1.7323890245291143D;
            // 
            // xrTableCell_004_3
            // 
            this.xrTableCell_004_3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Ethnicity_Clients_Counseling_No_Response.All_Housing", "{0:n0}")});
            this.xrTableCell_004_3.Name = "xrTableCell_004_3";
            this.xrTableCell_004_3.StylePriority.UseTextAlignment = false;
            this.xrTableCell_004_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_004_3.Weight = 0.43391993187925865D;
            // 
            // xrTableCell_004_4
            // 
            this.xrTableCell_004_4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Ethnicity_Clients_Counseling_No_Response.HUD_Housing", "{0:n0}")});
            this.xrTableCell_004_4.Name = "xrTableCell_004_4";
            this.xrTableCell_004_4.StylePriority.UseTextAlignment = false;
            this.xrTableCell_004_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_004_4.Weight = 0.50183273646844273D;
            // 
            // xrTableRow005
            // 
            this.xrTableRow005.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_005_2,
            this.xrTableCell_005_3,
            this.xrTableCell_005_4});
            this.xrTableRow005.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableRow005.Name = "xrTableRow005";
            this.xrTableRow005.StylePriority.UseFont = false;
            this.xrTableRow005.Weight = 0.43934562533570276D;
            // 
            // xrTableCell_005_2
            // 
            this.xrTableCell_005_2.Name = "xrTableCell_005_2";
            this.xrTableCell_005_2.StylePriority.UseTextAlignment = false;
            this.xrTableCell_005_2.Text = "Section 3 Total:";
            this.xrTableCell_005_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_005_2.Weight = 2.0642473507467658D;
            // 
            // xrTableCell_005_3
            // 
            this.xrTableCell_005_3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Section_3_Total.All_Housing", "{0:n0}")});
            this.xrTableCell_005_3.Name = "xrTableCell_005_3";
            this.xrTableCell_005_3.StylePriority.UseTextAlignment = false;
            this.xrTableCell_005_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_005_3.Weight = 0.43392007250425846D;
            // 
            // xrTableCell_005_4
            // 
            this.xrTableCell_005_4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Section_3_Total.HUD_Housing", "{0:n0}")});
            this.xrTableCell_005_4.Name = "xrTableCell_005_4";
            this.xrTableCell_005_4.StylePriority.UseTextAlignment = false;
            this.xrTableCell_005_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_005_4.Weight = 0.50183266615594291D;
            // 
            // xrTableRow006
            // 
            this.xrTableRow006.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_006_2});
            this.xrTableRow006.Name = "xrTableRow006";
            this.xrTableRow006.Weight = 0.59927167368198375D;
            // 
            // xrTableCell_006_2
            // 
            this.xrTableCell_006_2.Name = "xrTableCell_006_2";
            this.xrTableCell_006_2.Weight = 3D;
            // 
            // xrTableRow007
            // 
            this.xrTableRow007.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_007_1});
            this.xrTableRow007.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableRow007.Name = "xrTableRow007";
            this.xrTableRow007.StylePriority.UseFont = false;
            this.xrTableRow007.Weight = 0.43919998407363892D;
            // 
            // xrTableCell_007_1
            // 
            this.xrTableCell_007_1.BackColor = System.Drawing.Color.LightGray;
            this.xrTableCell_007_1.Name = "xrTableCell_007_1";
            this.xrTableCell_007_1.StylePriority.UseBackColor = false;
            this.xrTableCell_007_1.Text = "4. Race of Clients";
            this.xrTableCell_007_1.Weight = 3D;
            // 
            // xrTableRow008
            // 
            this.xrTableRow008.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_008_1});
            this.xrTableRow008.Name = "xrTableRow008";
            this.xrTableRow008.Weight = 0.43919998407363892D;
            // 
            // xrTableCell_008_1
            // 
            this.xrTableCell_008_1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell_008_1.Name = "xrTableCell_008_1";
            this.xrTableCell_008_1.Text = "Single Race";
            this.xrTableCell_008_1.Weight = 3D;
            // 
            // xrTableRow009
            // 
            this.xrTableRow009.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_009_1,
            this.xrTableCell_009_2,
            this.xrTableCell_009_3,
            this.xrTableCell_009_4});
            this.xrTableRow009.Name = "xrTableRow009";
            this.xrTableRow009.Weight = 0.43919998407363892D;
            // 
            // xrTableCell_009_1
            // 
            this.xrTableCell_009_1.Name = "xrTableCell_009_1";
            this.xrTableCell_009_1.Text = "a.";
            this.xrTableCell_009_1.Weight = 0.33185839653015137D;
            // 
            // xrTableCell_009_2
            // 
            this.xrTableCell_009_2.Name = "xrTableCell_009_2";
            this.xrTableCell_009_2.Text = "American Indian/Alaskan Native";
            this.xrTableCell_009_2.Weight = 1.7323890948416143D;
            // 
            // xrTableCell_009_3
            // 
            this.xrTableCell_009_3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Race_Clients_Counseling_American_Indian_Alaskan_Native.All_Housing", "{0:n0}")});
            this.xrTableCell_009_3.Name = "xrTableCell_009_3";
            this.xrTableCell_009_3.StylePriority.UseTextAlignment = false;
            this.xrTableCell_009_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_009_3.Weight = 0.43391993187925859D;
            // 
            // xrTableCell_009_4
            // 
            this.xrTableCell_009_4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Race_Clients_Counseling_American_Indian_Alaskan_Native.HUD_Housing", "{0:n0}")});
            this.xrTableCell_009_4.Name = "xrTableCell_009_4";
            this.xrTableCell_009_4.StylePriority.UseTextAlignment = false;
            this.xrTableCell_009_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_009_4.Weight = 0.50183266615594291D;
            // 
            // xrTableRow010
            // 
            this.xrTableRow010.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_010_1,
            this.xrTableCell_010_2,
            this.xrTableCell_010_3,
            this.xrTableCell_010_4});
            this.xrTableRow010.Name = "xrTableRow010";
            this.xrTableRow010.Weight = 0.43919989466667175D;
            // 
            // xrTableCell_010_1
            // 
            this.xrTableCell_010_1.Name = "xrTableCell_010_1";
            this.xrTableCell_010_1.Text = "b.";
            this.xrTableCell_010_1.Weight = 0.33185839653015137D;
            // 
            // xrTableCell_010_2
            // 
            this.xrTableCell_010_2.Name = "xrTableCell_010_2";
            this.xrTableCell_010_2.Text = "Asian";
            this.xrTableCell_010_2.Weight = 1.7323890948416143D;
            // 
            // xrTableCell_010_3
            // 
            this.xrTableCell_010_3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Race_Clients_Counseling_Asian.All_Housing", "{0:n0}")});
            this.xrTableCell_010_3.Name = "xrTableCell_010_3";
            this.xrTableCell_010_3.StylePriority.UseTextAlignment = false;
            this.xrTableCell_010_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_010_3.Weight = 0.43391993187925859D;
            // 
            // xrTableCell_010_4
            // 
            this.xrTableCell_010_4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Race_Clients_Counseling_Asian.HUD_Housing", "{0:n0}")});
            this.xrTableCell_010_4.Name = "xrTableCell_010_4";
            this.xrTableCell_010_4.StylePriority.UseTextAlignment = false;
            this.xrTableCell_010_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_010_4.Weight = 0.50183266615594291D;
            // 
            // xrTableRow011
            // 
            this.xrTableRow011.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_011_1,
            this.xrTableCell_011_2,
            this.xrTableCell_011_3,
            this.xrTableCell_011_4});
            this.xrTableRow011.Name = "xrTableRow011";
            this.xrTableRow011.Weight = 0.43919989466667175D;
            // 
            // xrTableCell_011_1
            // 
            this.xrTableCell_011_1.Name = "xrTableCell_011_1";
            this.xrTableCell_011_1.Text = "c.";
            this.xrTableCell_011_1.Weight = 0.33185839653015137D;
            // 
            // xrTableCell_011_2
            // 
            this.xrTableCell_011_2.Name = "xrTableCell_011_2";
            this.xrTableCell_011_2.Text = "Black or African-American";
            this.xrTableCell_011_2.Weight = 1.7323890948416143D;
            // 
            // xrTableCell_011_3
            // 
            this.xrTableCell_011_3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Race_Clients_Counseling_Black_AfricanAmerican.All_Housing", "{0:n0}")});
            this.xrTableCell_011_3.Name = "xrTableCell_011_3";
            this.xrTableCell_011_3.StylePriority.UseTextAlignment = false;
            this.xrTableCell_011_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_011_3.Weight = 0.43391993187925859D;
            // 
            // xrTableCell_011_4
            // 
            this.xrTableCell_011_4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Race_Clients_Counseling_Black_AfricanAmerican.HUD_Housing", "{0:n0}")});
            this.xrTableCell_011_4.Name = "xrTableCell_011_4";
            this.xrTableCell_011_4.StylePriority.UseTextAlignment = false;
            this.xrTableCell_011_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_011_4.Weight = 0.50183266615594291D;
            // 
            // xrTableRow012
            // 
            this.xrTableRow012.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_012_1,
            this.xrTableCell_012_2,
            this.xrTableCell_012_3,
            this.xrTableCell_012_4});
            this.xrTableRow012.Name = "xrTableRow012";
            this.xrTableRow012.Weight = 0.43919989466667175D;
            // 
            // xrTableCell_012_1
            // 
            this.xrTableCell_012_1.Name = "xrTableCell_012_1";
            this.xrTableCell_012_1.Text = "d.";
            this.xrTableCell_012_1.Weight = 0.33185839653015137D;
            // 
            // xrTableCell_012_2
            // 
            this.xrTableCell_012_2.Name = "xrTableCell_012_2";
            this.xrTableCell_012_2.Text = "Native Hawaiian or Other Pacific Islander";
            this.xrTableCell_012_2.Weight = 1.7323890948416143D;
            // 
            // xrTableCell_012_3
            // 
            this.xrTableCell_012_3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Race_Clients_Counseling_Pacific_Islanders.All_Housing", "{0:n0}")});
            this.xrTableCell_012_3.Name = "xrTableCell_012_3";
            this.xrTableCell_012_3.StylePriority.UseTextAlignment = false;
            this.xrTableCell_012_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_012_3.Weight = 0.43391993187925859D;
            // 
            // xrTableCell_012_4
            // 
            this.xrTableCell_012_4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Race_Clients_Counseling_Pacific_Islanders.HUD_Housing", "{0:n0}")});
            this.xrTableCell_012_4.Name = "xrTableCell_012_4";
            this.xrTableCell_012_4.StylePriority.UseTextAlignment = false;
            this.xrTableCell_012_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_012_4.Weight = 0.50183266615594291D;
            // 
            // xrTableRow013
            // 
            this.xrTableRow013.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_013_1,
            this.xrTableCell_013_2,
            this.xrTableCell_013_3,
            this.xrTableCell_013_4});
            this.xrTableRow013.Name = "xrTableRow013";
            this.xrTableRow013.Weight = 0.43919989466667175D;
            // 
            // xrTableCell_013_1
            // 
            this.xrTableCell_013_1.Name = "xrTableCell_013_1";
            this.xrTableCell_013_1.Text = "e.";
            this.xrTableCell_013_1.Weight = 0.33185839653015137D;
            // 
            // xrTableCell_013_2
            // 
            this.xrTableCell_013_2.Name = "xrTableCell_013_2";
            this.xrTableCell_013_2.Text = "White";
            this.xrTableCell_013_2.Weight = 1.7323890948416143D;
            // 
            // xrTableCell_013_3
            // 
            this.xrTableCell_013_3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Race_Clients_Counseling_White.All_Housing", "{0:n0}")});
            this.xrTableCell_013_3.Name = "xrTableCell_013_3";
            this.xrTableCell_013_3.StylePriority.UseTextAlignment = false;
            this.xrTableCell_013_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_013_3.Weight = 0.43392021312925821D;
            // 
            // xrTableCell_013_4
            // 
            this.xrTableCell_013_4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Race_Clients_Counseling_White.HUD_Housing", "{0:n0}")});
            this.xrTableCell_013_4.Name = "xrTableCell_013_4";
            this.xrTableCell_013_4.StylePriority.UseTextAlignment = false;
            this.xrTableCell_013_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_013_4.Weight = 0.50183238490594329D;
            // 
            // xrTableRow014
            // 
            this.xrTableRow014.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_014_1});
            this.xrTableRow014.Name = "xrTableRow014";
            this.xrTableRow014.Weight = 0.43919989466667175D;
            // 
            // xrTableCell_014_1
            // 
            this.xrTableCell_014_1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell_014_1.Name = "xrTableCell_014_1";
            this.xrTableCell_014_1.Text = "Multi-Race";
            this.xrTableCell_014_1.Weight = 3D;
            // 
            // xrTableRow015
            // 
            this.xrTableRow015.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_015_1,
            this.xrTableCell_015_2,
            this.xrTableCell_015_3,
            this.xrTableCell_015_4});
            this.xrTableRow015.Name = "xrTableRow015";
            this.xrTableRow015.Weight = 0.43919989466667175D;
            // 
            // xrTableCell_015_1
            // 
            this.xrTableCell_015_1.Name = "xrTableCell_015_1";
            this.xrTableCell_015_1.Text = "f.";
            this.xrTableCell_015_1.Weight = 0.33185839653015137D;
            // 
            // xrTableCell_015_2
            // 
            this.xrTableCell_015_2.Name = "xrTableCell_015_2";
            this.xrTableCell_015_2.Text = "American Indian or Alaska Native and White";
            this.xrTableCell_015_2.Weight = 1.7323890948416143D;
            // 
            // xrTableCell_015_3
            // 
            this.xrTableCell_015_3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "MultiRace_Clients_Counseling_AMINDWHT.All_Housing", "{0:n0}")});
            this.xrTableCell_015_3.Name = "xrTableCell_015_3";
            this.xrTableCell_015_3.StylePriority.UseTextAlignment = false;
            this.xrTableCell_015_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_015_3.Weight = 0.43391993187925859D;
            // 
            // xrTableCell_015_4
            // 
            this.xrTableCell_015_4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "MultiRace_Clients_Counseling_AMINDWHT.HUD_Housing", "{0:n0}")});
            this.xrTableCell_015_4.Name = "xrTableCell_015_4";
            this.xrTableCell_015_4.StylePriority.UseTextAlignment = false;
            this.xrTableCell_015_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_015_4.Weight = 0.50183266615594291D;
            // 
            // xrTableRow016
            // 
            this.xrTableRow016.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_016_1,
            this.xrTableCell_016_2,
            this.xrTableCell_016_3,
            this.xrTableCell_016_4});
            this.xrTableRow016.Name = "xrTableRow016";
            this.xrTableRow016.Weight = 0.43919989466667175D;
            // 
            // xrTableCell_016_1
            // 
            this.xrTableCell_016_1.Name = "xrTableCell_016_1";
            this.xrTableCell_016_1.Text = "g.";
            this.xrTableCell_016_1.Weight = 0.33185839653015137D;
            // 
            // xrTableCell_016_2
            // 
            this.xrTableCell_016_2.Name = "xrTableCell_016_2";
            this.xrTableCell_016_2.Text = "Asian and White";
            this.xrTableCell_016_2.Weight = 1.7323890948416143D;
            // 
            // xrTableCell_016_3
            // 
            this.xrTableCell_016_3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "MultiRace_Clients_Counseling_ASIANWHT.All_Housing", "{0:n0}")});
            this.xrTableCell_016_3.Name = "xrTableCell_016_3";
            this.xrTableCell_016_3.StylePriority.UseTextAlignment = false;
            this.xrTableCell_016_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_016_3.Weight = 0.43391993187925859D;
            // 
            // xrTableCell_016_4
            // 
            this.xrTableCell_016_4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "MultiRace_Clients_Counseling_ASIANWHT.HUD_Housing", "{0:n0}")});
            this.xrTableCell_016_4.Name = "xrTableCell_016_4";
            this.xrTableCell_016_4.StylePriority.UseTextAlignment = false;
            this.xrTableCell_016_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_016_4.Weight = 0.50183266615594291D;
            // 
            // xrTableRow017
            // 
            this.xrTableRow017.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_017_1,
            this.xrTableCell_017_2,
            this.xrTableCell_017_3,
            this.xrTableCell_017_4});
            this.xrTableRow017.Name = "xrTableRow017";
            this.xrTableRow017.Weight = 0.43919989466667175D;
            // 
            // xrTableCell_017_1
            // 
            this.xrTableCell_017_1.Name = "xrTableCell_017_1";
            this.xrTableCell_017_1.Text = "h.";
            this.xrTableCell_017_1.Weight = 0.33185839653015137D;
            // 
            // xrTableCell_017_2
            // 
            this.xrTableCell_017_2.Name = "xrTableCell_017_2";
            this.xrTableCell_017_2.Text = "Black or African American and White";
            this.xrTableCell_017_2.Weight = 1.7323890948416143D;
            // 
            // xrTableCell_017_3
            // 
            this.xrTableCell_017_3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "MultiRace_Clients_Counseling_BLKWHT.All_Housing", "{0:n0}")});
            this.xrTableCell_017_3.Name = "xrTableCell_017_3";
            this.xrTableCell_017_3.StylePriority.UseTextAlignment = false;
            this.xrTableCell_017_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_017_3.Weight = 0.43391993187925859D;
            // 
            // xrTableCell_017_4
            // 
            this.xrTableCell_017_4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "MultiRace_Clients_Counseling_BLKWHT.HUD_Housing", "{0:n0}")});
            this.xrTableCell_017_4.Name = "xrTableCell_017_4";
            this.xrTableCell_017_4.StylePriority.UseTextAlignment = false;
            this.xrTableCell_017_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_017_4.Weight = 0.50183266615594291D;
            // 
            // xrTableRow018
            // 
            this.xrTableRow018.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_018_1,
            this.xrTableCell_018_2,
            this.xrTableCell_018_3,
            this.xrTableCell_018_4});
            this.xrTableRow018.Name = "xrTableRow018";
            this.xrTableRow018.Weight = 0.43919989466667175D;
            // 
            // xrTableCell_018_1
            // 
            this.xrTableCell_018_1.Name = "xrTableCell_018_1";
            this.xrTableCell_018_1.Text = "i.";
            this.xrTableCell_018_1.Weight = 0.33185839653015137D;
            // 
            // xrTableCell_018_2
            // 
            this.xrTableCell_018_2.Name = "xrTableCell_018_2";
            this.xrTableCell_018_2.Text = "American Indian or Alaska Native and Black or African American";
            this.xrTableCell_018_2.Weight = 1.7323890948416143D;
            // 
            // xrTableCell_018_3
            // 
            this.xrTableCell_018_3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "MultiRace_Clients_Counseling_AMRCINDBLK.All_Housing", "{0:n0}")});
            this.xrTableCell_018_3.Name = "xrTableCell_018_3";
            this.xrTableCell_018_3.StylePriority.UseTextAlignment = false;
            this.xrTableCell_018_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_018_3.Weight = 0.43391993187925859D;
            // 
            // xrTableCell_018_4
            // 
            this.xrTableCell_018_4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "MultiRace_Clients_Counseling_AMRCINDBLK.HUD_Housing", "{0:n0}")});
            this.xrTableCell_018_4.Name = "xrTableCell_018_4";
            this.xrTableCell_018_4.StylePriority.UseTextAlignment = false;
            this.xrTableCell_018_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_018_4.Weight = 0.50183266615594291D;
            // 
            // xrTableRow019
            // 
            this.xrTableRow019.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_019_1,
            this.xrTableCell_019_2,
            this.xrTableCell_019_3,
            this.xrTableCell_019_4});
            this.xrTableRow019.Name = "xrTableRow019";
            this.xrTableRow019.Weight = 0.43919989466667175D;
            // 
            // xrTableCell_019_1
            // 
            this.xrTableCell_019_1.Name = "xrTableCell_019_1";
            this.xrTableCell_019_1.Text = "j.";
            this.xrTableCell_019_1.Weight = 0.33185839653015137D;
            // 
            // xrTableCell_019_2
            // 
            this.xrTableCell_019_2.Name = "xrTableCell_019_2";
            this.xrTableCell_019_2.Text = "Other multiple race";
            this.xrTableCell_019_2.Weight = 1.7323890948416143D;
            // 
            // xrTableCell_019_3
            // 
            this.xrTableCell_019_3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "MultiRace_Clients_Counseling_OtherMLTRC.All_Housing", "{0:n0}")});
            this.xrTableCell_019_3.Name = "xrTableCell_019_3";
            this.xrTableCell_019_3.StylePriority.UseTextAlignment = false;
            this.xrTableCell_019_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_019_3.Weight = 0.43391993187925859D;
            // 
            // xrTableCell_019_4
            // 
            this.xrTableCell_019_4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "MultiRace_Clients_Counseling_OtherMLTRC.HUD_Housing", "{0:n0}")});
            this.xrTableCell_019_4.Name = "xrTableCell_019_4";
            this.xrTableCell_019_4.StylePriority.UseTextAlignment = false;
            this.xrTableCell_019_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_019_4.Weight = 0.50183266615594291D;
            // 
            // xrTableRow020
            // 
            this.xrTableRow020.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_020_1,
            this.xrTableCell_020_2,
            this.xrTableCell_020_3,
            this.xrTableCell_020_4});
            this.xrTableRow020.Name = "xrTableRow020";
            this.xrTableRow020.Weight = 0.43919989466667175D;
            // 
            // xrTableCell_020_1
            // 
            this.xrTableCell_020_1.Name = "xrTableCell_020_1";
            this.xrTableCell_020_1.Text = "k.";
            this.xrTableCell_020_1.Weight = 0.33185839653015137D;
            // 
            // xrTableCell_020_2
            // 
            this.xrTableCell_020_2.Name = "xrTableCell_020_2";
            this.xrTableCell_020_2.Text = "Chose not to respond";
            this.xrTableCell_020_2.Weight = 1.7323890948416143D;
            // 
            // xrTableCell_020_3
            // 
            this.xrTableCell_020_3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "MultiRace_Clients_Counseling_NoResponse.All_Housing", "{0:n0}")});
            this.xrTableCell_020_3.Name = "xrTableCell_020_3";
            this.xrTableCell_020_3.StylePriority.UseTextAlignment = false;
            this.xrTableCell_020_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_020_3.Weight = 0.43391993187925859D;
            // 
            // xrTableCell_020_4
            // 
            this.xrTableCell_020_4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "MultiRace_Clients_Counseling_NoResponse.HUD_Housing", "{0:n0}")});
            this.xrTableCell_020_4.Name = "xrTableCell_020_4";
            this.xrTableCell_020_4.StylePriority.UseTextAlignment = false;
            this.xrTableCell_020_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_020_4.Weight = 0.50183266615594291D;
            // 
            // xrTableRow021
            // 
            this.xrTableRow021.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_021_2,
            this.xrTableCell_021_3,
            this.xrTableCell_021_4});
            this.xrTableRow021.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableRow021.Name = "xrTableRow021";
            this.xrTableRow021.StylePriority.UseFont = false;
            this.xrTableRow021.Weight = 0.43919989466667175D;
            // 
            // xrTableCell_021_2
            // 
            this.xrTableCell_021_2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell_021_2.Name = "xrTableCell_021_2";
            this.xrTableCell_021_2.StylePriority.UseFont = false;
            this.xrTableCell_021_2.StylePriority.UseTextAlignment = false;
            this.xrTableCell_021_2.Text = "Section 4 Total:";
            this.xrTableCell_021_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_021_2.Weight = 2.0642474913717654D;
            // 
            // xrTableCell_021_3
            // 
            this.xrTableCell_021_3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Section_4_Total.All_Housing", "{0:n0}")});
            this.xrTableCell_021_3.Name = "xrTableCell_021_3";
            this.xrTableCell_021_3.StylePriority.UseTextAlignment = false;
            this.xrTableCell_021_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_021_3.Weight = 0.43391993187925859D;
            // 
            // xrTableCell_021_4
            // 
            this.xrTableCell_021_4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Section_4_Total.HUD_Housing", "{0:n0}")});
            this.xrTableCell_021_4.Name = "xrTableCell_021_4";
            this.xrTableCell_021_4.StylePriority.UseTextAlignment = false;
            this.xrTableCell_021_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_021_4.Weight = 0.50183266615594291D;
            // 
            // xrTableRow022
            // 
            this.xrTableRow022.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_022_2});
            this.xrTableRow022.Name = "xrTableRow022";
            this.xrTableRow022.Weight = 0.43920010328292847D;
            // 
            // xrTableCell_022_2
            // 
            this.xrTableCell_022_2.Name = "xrTableCell_022_2";
            this.xrTableCell_022_2.Weight = 3D;
            // 
            // xrTableRow023
            // 
            this.xrTableRow023.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_023_1});
            this.xrTableRow023.Name = "xrTableRow023";
            this.xrTableRow023.Weight = 0.43919989466667175D;
            // 
            // xrTableCell_023_1
            // 
            this.xrTableCell_023_1.BackColor = System.Drawing.Color.LightGray;
            this.xrTableCell_023_1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell_023_1.Name = "xrTableCell_023_1";
            this.xrTableCell_023_1.StylePriority.UseBackColor = false;
            this.xrTableCell_023_1.StylePriority.UseFont = false;
            this.xrTableCell_023_1.Text = "5. Income Levels";
            this.xrTableCell_023_1.Weight = 3D;
            // 
            // xrTableRow024
            // 
            this.xrTableRow024.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_024_1,
            this.xrTableCell_024_2,
            this.xrTableCell_024_3,
            this.xrTableCell_024_4});
            this.xrTableRow024.Name = "xrTableRow024";
            this.xrTableRow024.Weight = 0.43919989466667175D;
            // 
            // xrTableCell_024_1
            // 
            this.xrTableCell_024_1.Name = "xrTableCell_024_1";
            this.xrTableCell_024_1.Text = "a.";
            this.xrTableCell_024_1.Weight = 0.33185839653015137D;
            // 
            // xrTableCell_024_2
            // 
            this.xrTableCell_024_2.Name = "xrTableCell_024_2";
            this.xrTableCell_024_2.Text = "< 30% of Area Median Income (AMI)";
            this.xrTableCell_024_2.Weight = 1.7323888135916148D;
            // 
            // xrTableCell_024_3
            // 
            this.xrTableCell_024_3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Less30_AMI_Level.All_Housing", "{0:n0}")});
            this.xrTableCell_024_3.Name = "xrTableCell_024_3";
            this.xrTableCell_024_3.StylePriority.UseTextAlignment = false;
            this.xrTableCell_024_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_024_3.Weight = 0.43391996703550861D;
            // 
            // xrTableCell_024_4
            // 
            this.xrTableCell_024_4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Less30_AMI_Level.HUD_Housing", "{0:n0}")});
            this.xrTableCell_024_4.Name = "xrTableCell_024_4";
            this.xrTableCell_024_4.StylePriority.UseTextAlignment = false;
            this.xrTableCell_024_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_024_4.Weight = 0.50183291224969251D;
            // 
            // xrTableRow024B
            // 
            this.xrTableRow024B.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_024B_1,
            this.xrTableCell_024B_2,
            this.xrTableCell_024B_3,
            this.xrTableCell_024B_4});
            this.xrTableRow024B.Name = "xrTableRow024B";
            this.xrTableRow024B.Weight = 0.43919989466667175D;
            // 
            // xrTableCell_024B_1
            // 
            this.xrTableCell_024B_1.Name = "xrTableCell_024B_1";
            this.xrTableCell_024B_1.Text = "b.";
            this.xrTableCell_024B_1.Weight = 0.33185839653015137D;
            // 
            // xrTableCell_024B_2
            // 
            this.xrTableCell_024B_2.Name = "xrTableCell_024B_2";
            this.xrTableCell_024B_2.Text = "30 - 49% of AMI";
            this.xrTableCell_024B_2.Weight = 1.7323888135916148D;
            // 
            // xrTableCell_024B_3
            // 
            this.xrTableCell_024B_3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "a30_49_AMI_Level.All_Housing", "{0:n0}")});
            this.xrTableCell_024B_3.Name = "xrTableCell_024B_3";
            this.xrTableCell_024B_3.StylePriority.UseTextAlignment = false;
            this.xrTableCell_024B_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_024B_3.Weight = 0.43391996703550861D;
            // 
            // xrTableCell_024B_4
            // 
            this.xrTableCell_024B_4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "a30_49_AMI_Level.HUD_Housing", "{0:n0}")});
            this.xrTableCell_024B_4.Name = "xrTableCell_024B_4";
            this.xrTableCell_024B_4.StylePriority.UseTextAlignment = false;
            this.xrTableCell_024B_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_024B_4.Weight = 0.50183291224969251D;
            // 
            // xrTableRow025
            // 
            this.xrTableRow025.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_025_1,
            this.xrTableCell_025_2,
            this.xrTableCell_025_3,
            this.xrTableCell_025_4});
            this.xrTableRow025.Name = "xrTableRow025";
            this.xrTableRow025.Weight = 0.43919989466667175D;
            // 
            // xrTableCell_025_1
            // 
            this.xrTableCell_025_1.Name = "xrTableCell_025_1";
            this.xrTableCell_025_1.Text = "c.";
            this.xrTableCell_025_1.Weight = 0.33185839653015137D;
            // 
            // xrTableCell_025_2
            // 
            this.xrTableCell_025_2.Name = "xrTableCell_025_2";
            this.xrTableCell_025_2.Text = "50 - 79% of AMI";
            this.xrTableCell_025_2.Weight = 1.7323888135916148D;
            // 
            // xrTableCell_025_3
            // 
            this.xrTableCell_025_3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "a50_79_AMI_Level.All_Housing", "{0:n0}")});
            this.xrTableCell_025_3.Name = "xrTableCell_025_3";
            this.xrTableCell_025_3.StylePriority.UseTextAlignment = false;
            this.xrTableCell_025_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_025_3.Weight = 0.43391996703550861D;
            // 
            // xrTableCell_025_4
            // 
            this.xrTableCell_025_4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "a50_79_AMI_Level.HUD_Housing", "{0:n0}")});
            this.xrTableCell_025_4.Name = "xrTableCell_025_4";
            this.xrTableCell_025_4.StylePriority.UseTextAlignment = false;
            this.xrTableCell_025_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_025_4.Weight = 0.50183291224969251D;
            // 
            // xrTableRow026
            // 
            this.xrTableRow026.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_026_1,
            this.xrTableCell_026_2,
            this.xrTableCell_026_3,
            this.xrTableCell_026_4});
            this.xrTableRow026.Name = "xrTableRow026";
            this.xrTableRow026.Weight = 0.43919989466667175D;
            // 
            // xrTableCell_026_1
            // 
            this.xrTableCell_026_1.Name = "xrTableCell_026_1";
            this.xrTableCell_026_1.Text = "d.";
            this.xrTableCell_026_1.Weight = 0.33185839653015137D;
            // 
            // xrTableCell_026_2
            // 
            this.xrTableCell_026_2.Multiline = true;
            this.xrTableCell_026_2.Name = "xrTableCell_026_2";
            this.xrTableCell_026_2.Text = "80 - 100% of AMI";
            this.xrTableCell_026_2.Weight = 1.7323888135916148D;
            // 
            // xrTableCell_026_3
            // 
            this.xrTableCell_026_3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "a80_100_AMI_Level.All_Housing", "{0:n0}")});
            this.xrTableCell_026_3.Name = "xrTableCell_026_3";
            this.xrTableCell_026_3.StylePriority.UseTextAlignment = false;
            this.xrTableCell_026_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_026_3.Weight = 0.43391996703550861D;
            // 
            // xrTableCell_026_4
            // 
            this.xrTableCell_026_4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "a80_100_AMI_Level.HUD_Housing", "{0:n0}")});
            this.xrTableCell_026_4.Name = "xrTableCell_026_4";
            this.xrTableCell_026_4.StylePriority.UseTextAlignment = false;
            this.xrTableCell_026_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_026_4.Weight = 0.50183291224969251D;
            // 
            // xrTableRow027
            // 
            this.xrTableRow027.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_027_1,
            this.xrTableCell_027_2,
            this.xrTableCell_027_3,
            this.xrTableCell_027_4});
            this.xrTableRow027.Name = "xrTableRow027";
            this.xrTableRow027.Weight = 0.43919989466667175D;
            // 
            // xrTableCell_027_1
            // 
            this.xrTableCell_027_1.Name = "xrTableCell_027_1";
            this.xrTableCell_027_1.Text = "e.";
            this.xrTableCell_027_1.Weight = 0.33185839653015137D;
            // 
            // xrTableCell_027_2
            // 
            this.xrTableCell_027_2.Name = "xrTableCell_027_2";
            this.xrTableCell_027_2.Text = "> 100% AMI";
            this.xrTableCell_027_2.Weight = 1.7323888135916148D;
            // 
            // xrTableCell_027_3
            // 
            this.xrTableCell_027_3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Greater100_AMI_Level.All_Housing", "{0:n0}")});
            this.xrTableCell_027_3.Name = "xrTableCell_027_3";
            this.xrTableCell_027_3.StylePriority.UseTextAlignment = false;
            this.xrTableCell_027_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_027_3.Weight = 0.43391996703550861D;
            // 
            // xrTableCell_027_4
            // 
            this.xrTableCell_027_4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Greater100_AMI_Level.HUD_Housing", "{0:n0}")});
            this.xrTableCell_027_4.Name = "xrTableCell_027_4";
            this.xrTableCell_027_4.StylePriority.UseTextAlignment = false;
            this.xrTableCell_027_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_027_4.Weight = 0.50183291224969251D;
            // 
            // xrTableRow028
            // 
            this.xrTableRow028.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_028_1,
            this.xrTableCell_028_2,
            this.xrTableCell_028_3,
            this.xrTableCell_028_4});
            this.xrTableRow028.Name = "xrTableRow028";
            this.xrTableRow028.Weight = 0.43919989466667175D;
            // 
            // xrTableCell_028_1
            // 
            this.xrTableCell_028_1.Name = "xrTableCell_028_1";
            this.xrTableCell_028_1.Text = "f.";
            this.xrTableCell_028_1.Weight = 0.33185839653015137D;
            // 
            // xrTableCell_028_2
            // 
            this.xrTableCell_028_2.Name = "xrTableCell_028_2";
            this.xrTableCell_028_2.Text = "Chose not to respond";
            this.xrTableCell_028_2.Weight = 1.7323888135916148D;
            // 
            // xrTableCell_028_3
            // 
            this.xrTableCell_028_3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "AMI_No_Response.All_Housing", "{0:n0}")});
            this.xrTableCell_028_3.Name = "xrTableCell_028_3";
            this.xrTableCell_028_3.StylePriority.UseTextAlignment = false;
            this.xrTableCell_028_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_028_3.Weight = 0.43391996703550861D;
            // 
            // xrTableCell_028_4
            // 
            this.xrTableCell_028_4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "AMI_No_Response.HUD_Housing", "{0:n0}")});
            this.xrTableCell_028_4.Name = "xrTableCell_028_4";
            this.xrTableCell_028_4.StylePriority.UseTextAlignment = false;
            this.xrTableCell_028_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_028_4.Weight = 0.50183291224969251D;
            // 
            // xrTableRow029
            // 
            this.xrTableRow029.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_029_2,
            this.xrTableCell_029_3,
            this.xrTableCell_029_4});
            this.xrTableRow029.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableRow029.Name = "xrTableRow029";
            this.xrTableRow029.StylePriority.UseFont = false;
            this.xrTableRow029.Weight = 0.43919989466667175D;
            // 
            // xrTableCell_029_2
            // 
            this.xrTableCell_029_2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell_029_2.Name = "xrTableCell_029_2";
            this.xrTableCell_029_2.StylePriority.UseFont = false;
            this.xrTableCell_029_2.StylePriority.UseTextAlignment = false;
            this.xrTableCell_029_2.Text = "Section 5 Total:";
            this.xrTableCell_029_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_029_2.Weight = 2.0642474913717654D;
            // 
            // xrTableCell_029_3
            // 
            this.xrTableCell_029_3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Section_5_Total.All_Housing", "{0:n0}")});
            this.xrTableCell_029_3.Name = "xrTableCell_029_3";
            this.xrTableCell_029_3.StylePriority.UseTextAlignment = false;
            this.xrTableCell_029_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_029_3.Weight = 0.433919650629259D;
            // 
            // xrTableCell_029_4
            // 
            this.xrTableCell_029_4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Section_5_Total.HUD_Housing", "{0:n0}")});
            this.xrTableCell_029_4.Name = "xrTableCell_029_4";
            this.xrTableCell_029_4.StylePriority.UseTextAlignment = false;
            this.xrTableCell_029_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_029_4.Weight = 0.50183294740594242D;
            // 
            // xrTableRow030
            // 
            this.xrTableRow030.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_030_1});
            this.xrTableRow030.Name = "xrTableRow030";
            this.xrTableRow030.Weight = 0.43919989466667175D;
            // 
            // xrTableCell_030_1
            // 
            this.xrTableCell_030_1.Name = "xrTableCell_030_1";
            this.xrTableCell_030_1.Weight = 3D;
            // 
            // xrTableRow031
            // 
            this.xrTableRow031.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_031_1});
            this.xrTableRow031.Name = "xrTableRow031";
            this.xrTableRow031.Weight = 0.43919989466667175D;
            // 
            // xrTableCell_031_1
            // 
            this.xrTableCell_031_1.BackColor = System.Drawing.Color.LightGray;
            this.xrTableCell_031_1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell_031_1.Name = "xrTableCell_031_1";
            this.xrTableCell_031_1.StylePriority.UseBackColor = false;
            this.xrTableCell_031_1.StylePriority.UseFont = false;
            this.xrTableCell_031_1.Text = "6. Rural Area";
            this.xrTableCell_031_1.Weight = 3D;
            // 
            // xrTableRow032
            // 
            this.xrTableRow032.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_032_1,
            this.xrTableCell_032_2,
            this.xrTableCell_032_3,
            this.xrTableCell_032_4});
            this.xrTableRow032.Name = "xrTableRow032";
            this.xrTableRow032.Weight = 0.43919998407363892D;
            // 
            // xrTableCell_032_1
            // 
            this.xrTableCell_032_1.Name = "xrTableCell_032_1";
            this.xrTableCell_032_1.Text = "a.";
            this.xrTableCell_032_1.Weight = 0.33185839653015137D;
            // 
            // xrTableCell_032_2
            // 
            this.xrTableCell_032_2.Name = "xrTableCell_032_2";
            this.xrTableCell_032_2.Text = "Household Lives In Rural Area";
            this.xrTableCell_032_2.Weight = 1.7323888135916148D;
            // 
            // xrTableCell_032_3
            // 
            this.xrTableCell_032_3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Household_Lives_In_Rural_Area.All_Housing", "{0:n0}")});
            this.xrTableCell_032_3.Name = "xrTableCell_032_3";
            this.xrTableCell_032_3.StylePriority.UseTextAlignment = false;
            this.xrTableCell_032_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_032_3.Weight = 0.43392049437925784D;
            // 
            // xrTableCell_032_4
            // 
            this.xrTableCell_032_4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Household_Lives_In_Rural_Area.HUD_Housing", "{0:n0}")});
            this.xrTableCell_032_4.Name = "xrTableCell_032_4";
            this.xrTableCell_032_4.StylePriority.UseTextAlignment = false;
            this.xrTableCell_032_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_032_4.Weight = 0.50183238490594329D;
            // 
            // xrTableRow033
            // 
            this.xrTableRow033.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_033_1,
            this.xrTableCell_033_2,
            this.xrTableCell_033_3,
            this.xrTableCell_033_4});
            this.xrTableRow033.Name = "xrTableRow033";
            this.xrTableRow033.Weight = 0.43934609866559204D;
            // 
            // xrTableCell_033_1
            // 
            this.xrTableCell_033_1.Name = "xrTableCell_033_1";
            this.xrTableCell_033_1.Text = "b.";
            this.xrTableCell_033_1.Weight = 0.33185839653015137D;
            // 
            // xrTableCell_033_2
            // 
            this.xrTableCell_033_2.Multiline = true;
            this.xrTableCell_033_2.Name = "xrTableCell_033_2";
            this.xrTableCell_033_2.Text = "Household Does Not Live In Rural Area";
            this.xrTableCell_033_2.Weight = 1.7323890149585814D;
            // 
            // xrTableCell_033_3
            // 
            this.xrTableCell_033_3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Household_Does_Not_Live_In_Rural_Area.All_Housing", "{0:n0}")});
            this.xrTableCell_033_3.Name = "xrTableCell_033_3";
            this.xrTableCell_033_3.StylePriority.UseTextAlignment = false;
            this.xrTableCell_033_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_033_3.Weight = 0.4339202385069369D;
            // 
            // xrTableCell_033_4
            // 
            this.xrTableCell_033_4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Household_Does_Not_Live_In_Rural_Area.HUD_Housing", "{0:n0}")});
            this.xrTableCell_033_4.Name = "xrTableCell_033_4";
            this.xrTableCell_033_4.StylePriority.UseTextAlignment = false;
            this.xrTableCell_033_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_033_4.Weight = 0.501832409608975D;
            // 
            // xrTableRow034
            // 
            this.xrTableRow034.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_034_1,
            this.xrTableCell_034_2,
            this.xrTableCell_034_3,
            this.xrTableCell_034_4});
            this.xrTableRow034.Name = "xrTableRow034";
            this.xrTableRow034.Weight = 0.43934604668753963D;
            // 
            // xrTableCell_034_1
            // 
            this.xrTableCell_034_1.Name = "xrTableCell_034_1";
            this.xrTableCell_034_1.Text = "c.";
            this.xrTableCell_034_1.Weight = 0.33185839653015137D;
            // 
            // xrTableCell_034_2
            // 
            this.xrTableCell_034_2.Multiline = true;
            this.xrTableCell_034_2.Name = "xrTableCell_034_2";
            this.xrTableCell_034_2.Text = "No Response";
            this.xrTableCell_034_2.Weight = 1.7323889542166144D;
            // 
            // xrTableCell_034_3
            // 
            this.xrTableCell_034_3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Rural_Area_No_Response.All_Housing", "{0:n0}")});
            this.xrTableCell_034_3.Name = "xrTableCell_034_3";
            this.xrTableCell_034_3.StylePriority.UseTextAlignment = false;
            this.xrTableCell_034_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_034_3.Weight = 0.43391979125425889D;
            // 
            // xrTableCell_034_4
            // 
            this.xrTableCell_034_4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Rural_Area_No_Response.HUD_Housing", "{0:n0}")});
            this.xrTableCell_034_4.Name = "xrTableCell_034_4";
            this.xrTableCell_034_4.StylePriority.UseTextAlignment = false;
            this.xrTableCell_034_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_034_4.Weight = 0.50183294740594242D;
            // 
            // xrTableRow035
            // 
            this.xrTableRow035.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_035_2,
            this.xrTableCell_035_3,
            this.xrTableCell_035_4});
            this.xrTableRow035.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableRow035.Name = "xrTableRow035";
            this.xrTableRow035.StylePriority.UseFont = false;
            this.xrTableRow035.Weight = 0.43934611384112188D;
            // 
            // xrTableCell_035_2
            // 
            this.xrTableCell_035_2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell_035_2.Multiline = true;
            this.xrTableCell_035_2.Name = "xrTableCell_035_2";
            this.xrTableCell_035_2.StylePriority.UseFont = false;
            this.xrTableCell_035_2.StylePriority.UseTextAlignment = false;
            this.xrTableCell_035_2.Text = "Section 6 Total:";
            this.xrTableCell_035_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_035_2.Weight = 2.0642473507467658D;
            // 
            // xrTableCell_035_3
            // 
            this.xrTableCell_035_3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Section_6_Total.All_Housing", "{0:n0}")});
            this.xrTableCell_035_3.Name = "xrTableCell_035_3";
            this.xrTableCell_035_3.StylePriority.UseTextAlignment = false;
            this.xrTableCell_035_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_035_3.Weight = 0.43391979125425889D;
            // 
            // xrTableCell_035_4
            // 
            this.xrTableCell_035_4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Section_6_Total.HUD_Housing", "{0:n0}")});
            this.xrTableCell_035_4.Name = "xrTableCell_035_4";
            this.xrTableCell_035_4.StylePriority.UseTextAlignment = false;
            this.xrTableCell_035_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_035_4.Weight = 0.50183294740594242D;
            // 
            // xrTableRow036
            // 
            this.xrTableRow036.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_036_1});
            this.xrTableRow036.Name = "xrTableRow036";
            this.xrTableRow036.Weight = 0.43934596404379045D;
            // 
            // xrTableCell_036_1
            // 
            this.xrTableCell_036_1.Name = "xrTableCell_036_1";
            this.xrTableCell_036_1.Weight = 3.0000000894069672D;
            // 
            // xrTableRow037
            // 
            this.xrTableRow037.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_037_1});
            this.xrTableRow037.Name = "xrTableRow037";
            this.xrTableRow037.Weight = 0.43934590443914523D;
            // 
            // xrTableCell_037_1
            // 
            this.xrTableCell_037_1.BackColor = System.Drawing.Color.LightGray;
            this.xrTableCell_037_1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell_037_1.Name = "xrTableCell_037_1";
            this.xrTableCell_037_1.StylePriority.UseBackColor = false;
            this.xrTableCell_037_1.StylePriority.UseFont = false;
            this.xrTableCell_037_1.Text = "7. English Proficiency";
            this.xrTableCell_037_1.Weight = 3.0000000894069672D;
            // 
            // xrTableRow038
            // 
            this.xrTableRow038.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_038_1,
            this.xrTableCell_038_2,
            this.xrTableCell_038_3,
            this.xrTableCell_038_4});
            this.xrTableRow038.Name = "xrTableRow038";
            this.xrTableRow038.Weight = 0.43934609732657082D;
            // 
            // xrTableCell_038_1
            // 
            this.xrTableCell_038_1.Name = "xrTableCell_038_1";
            this.xrTableCell_038_1.Text = "a.";
            this.xrTableCell_038_1.Weight = 0.33185839653015137D;
            // 
            // xrTableCell_038_2
            // 
            this.xrTableCell_038_2.Multiline = true;
            this.xrTableCell_038_2.Name = "xrTableCell_038_2";
            this.xrTableCell_038_2.Text = "Limited English Proficient";
            this.xrTableCell_038_2.Weight = 1.7323889542166144D;
            // 
            // xrTableCell_038_3
            // 
            this.xrTableCell_038_3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Household_Is_Limited_English_Proficient.All_Housing", "{0:n0}")});
            this.xrTableCell_038_3.Name = "xrTableCell_038_3";
            this.xrTableCell_038_3.StylePriority.UseTextAlignment = false;
            this.xrTableCell_038_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_038_3.Weight = 0.43391979125425889D;
            // 
            // xrTableCell_038_4
            // 
            this.xrTableCell_038_4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Household_Is_Limited_English_Proficient.HUD_Housing", "{0:n0}")});
            this.xrTableCell_038_4.Name = "xrTableCell_038_4";
            this.xrTableCell_038_4.StylePriority.UseTextAlignment = false;
            this.xrTableCell_038_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_038_4.Weight = 0.50183294740594242D;
            // 
            // xrTableRow039
            // 
            this.xrTableRow039.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_039_1,
            this.xrTableCell_039_2,
            this.xrTableCell_039_3,
            this.xrTableCell_039_4});
            this.xrTableRow039.Name = "xrTableRow039";
            this.xrTableRow039.Weight = 0.43934614216103451D;
            // 
            // xrTableCell_039_1
            // 
            this.xrTableCell_039_1.Name = "xrTableCell_039_1";
            this.xrTableCell_039_1.Text = "b.";
            this.xrTableCell_039_1.Weight = 0.33185839653015137D;
            // 
            // xrTableCell_039_2
            // 
            this.xrTableCell_039_2.Multiline = true;
            this.xrTableCell_039_2.Name = "xrTableCell_039_2";
            this.xrTableCell_039_2.Text = "Not Limited English Proficient";
            this.xrTableCell_039_2.Weight = 1.7323889542166144D;
            // 
            // xrTableCell_039_3
            // 
            this.xrTableCell_039_3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Household_Is_Not_Limited_English_Proficient.All_Housing", "{0:n0}")});
            this.xrTableCell_039_3.Name = "xrTableCell_039_3";
            this.xrTableCell_039_3.StylePriority.UseTextAlignment = false;
            this.xrTableCell_039_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_039_3.Weight = 0.43391979125425889D;
            // 
            // xrTableCell_039_4
            // 
            this.xrTableCell_039_4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Household_Is_Not_Limited_English_Proficient.HUD_Housing", "{0:n0}")});
            this.xrTableCell_039_4.Name = "xrTableCell_039_4";
            this.xrTableCell_039_4.StylePriority.UseTextAlignment = false;
            this.xrTableCell_039_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_039_4.Weight = 0.50183294740594242D;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_039B_1,
            this.xrTableCell_039B_2,
            this.xrTableCell_039B_3,
            this.xrTableCell_039B_4});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 0.43934614216103407D;
            // 
            // xrTableCell_039B_1
            // 
            this.xrTableCell_039B_1.Name = "xrTableCell_039B_1";
            this.xrTableCell_039B_1.Text = "c.";
            this.xrTableCell_039B_1.Weight = 0.33185839653015137D;
            // 
            // xrTableCell_039B_2
            // 
            this.xrTableCell_039B_2.Name = "xrTableCell_039B_2";
            this.xrTableCell_039B_2.Text = "No Response";
            this.xrTableCell_039B_2.Weight = 1.7323889542166144D;
            // 
            // xrTableCell_039B_3
            // 
            this.xrTableCell_039B_3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Limited_English_Proficient_No_Response.All_Housing", "{0:n0}")});
            this.xrTableCell_039B_3.Name = "xrTableCell_039B_3";
            this.xrTableCell_039B_3.StylePriority.UseTextAlignment = false;
            this.xrTableCell_039B_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_039B_3.Weight = 0.43391979125425889D;
            // 
            // xrTableCell_039B_4
            // 
            this.xrTableCell_039B_4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Limited_English_Proficient_No_Response.HUD_Housing", "{0:n0}")});
            this.xrTableCell_039B_4.Name = "xrTableCell_039B_4";
            this.xrTableCell_039B_4.StylePriority.UseTextAlignment = false;
            this.xrTableCell_039B_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_039B_4.Weight = 0.50183294740594242D;
            // 
            // xrTableRow040
            // 
            this.xrTableRow040.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_040_2,
            this.xrTableCell_040_3,
            this.xrTableCell_040_4});
            this.xrTableRow040.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableRow040.Name = "xrTableRow040";
            this.xrTableRow040.StylePriority.UseFont = false;
            this.xrTableRow040.Weight = 0.4393461719633569D;
            // 
            // xrTableCell_040_2
            // 
            this.xrTableCell_040_2.Name = "xrTableCell_040_2";
            this.xrTableCell_040_2.StylePriority.UseTextAlignment = false;
            this.xrTableCell_040_2.Text = "Section 7 Total:";
            this.xrTableCell_040_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_040_2.Weight = 2.0642473507467658D;
            // 
            // xrTableCell_040_3
            // 
            this.xrTableCell_040_3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Section_7_Total.All_Housing", "{0:n0}")});
            this.xrTableCell_040_3.Name = "xrTableCell_040_3";
            this.xrTableCell_040_3.StylePriority.UseTextAlignment = false;
            this.xrTableCell_040_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_040_3.Weight = 0.43391979125425889D;
            // 
            // xrTableCell_040_4
            // 
            this.xrTableCell_040_4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Section_7_Total.HUD_Housing", "{0:n0}")});
            this.xrTableCell_040_4.Name = "xrTableCell_040_4";
            this.xrTableCell_040_4.StylePriority.UseTextAlignment = false;
            this.xrTableCell_040_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_040_4.Weight = 0.50183294740594242D;
            // 
            // xrTableRow041
            // 
            this.xrTableRow041.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_041_1});
            this.xrTableRow041.Name = "xrTableRow041";
            this.xrTableRow041.Weight = 0.43934590444647981D;
            // 
            // xrTableCell_041_1
            // 
            this.xrTableCell_041_1.Name = "xrTableCell_041_1";
            this.xrTableCell_041_1.Weight = 3D;
            // 
            // xrTableRow043
            // 
            this.xrTableRow043.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_043_1});
            this.xrTableRow043.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableRow043.Name = "xrTableRow043";
            this.xrTableRow043.StylePriority.UseFont = false;
            this.xrTableRow043.Weight = 0.43919989466667175D;
            // 
            // xrTableCell_043_1
            // 
            this.xrTableCell_043_1.BackColor = System.Drawing.Color.LightGray;
            this.xrTableCell_043_1.Name = "xrTableCell_043_1";
            this.xrTableCell_043_1.StylePriority.UseBackColor = false;
            this.xrTableCell_043_1.Text = "8. Group Sessions";
            this.xrTableCell_043_1.Weight = 3D;
            // 
            // xrTableRow044
            // 
            this.xrTableRow044.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_044_1,
            this.xrTableCell_044_2,
            this.xrTableCell_044_3,
            this.xrTableCell_044_4});
            this.xrTableRow044.Name = "xrTableRow044";
            this.xrTableRow044.Weight = 0.43919995427131653D;
            // 
            // xrTableCell_044_1
            // 
            this.xrTableCell_044_1.Name = "xrTableCell_044_1";
            this.xrTableCell_044_1.Text = "a.";
            this.xrTableCell_044_1.Weight = 0.33185839653015137D;
            // 
            // xrTableCell_044_2
            // 
            this.xrTableCell_044_2.Name = "xrTableCell_044_2";
            this.xrTableCell_044_2.Text = "Home Maintenance";
            this.xrTableCell_044_2.Weight = 1.7323890948416143D;
            // 
            // xrTableCell_044_3
            // 
            this.xrTableCell_044_3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Compl_HomeMaint_FinMngt.All_Housing", "{0:n0}")});
            this.xrTableCell_044_3.Name = "xrTableCell_044_3";
            this.xrTableCell_044_3.StylePriority.UseTextAlignment = false;
            this.xrTableCell_044_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_044_3.Weight = 0.43391993187925859D;
            // 
            // xrTableCell_044_4
            // 
            this.xrTableCell_044_4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Compl_HomeMaint_FinMngt.HUD_Housing", "{0:n0}")});
            this.xrTableCell_044_4.Name = "xrTableCell_044_4";
            this.xrTableCell_044_4.StylePriority.UseTextAlignment = false;
            this.xrTableCell_044_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell_044_4.Weight = 0.50183266615594291D;
            // 
            // xrTableRow045
            // 
            this.xrTableRow045.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell125,
            this.xrTableCell126,
            this.xrTableCell1,
            this.xrTableCell127});
            this.xrTableRow045.Name = "xrTableRow045";
            this.xrTableRow045.Weight = 0.43920034170150757D;
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.Text = "b.";
            this.xrTableCell125.Weight = 0.33185839653015137D;
            // 
            // xrTableCell126
            // 
            this.xrTableCell126.Name = "xrTableCell126";
            this.xrTableCell126.Text = "Predatory Lending";
            this.xrTableCell126.Weight = 1.7323890948416143D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Compl_Workshop_Predatory_Lend.All_Housing", "{0:n0}")});
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell1.Weight = 0.43392052953550775D;
            // 
            // xrTableCell127
            // 
            this.xrTableCell127.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Compl_Workshop_Predatory_Lend.HUD_Housing", "{0:n0}")});
            this.xrTableCell127.Name = "xrTableCell127";
            this.xrTableCell127.StylePriority.UseTextAlignment = false;
            this.xrTableCell127.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell127.Weight = 0.50183206849969375D;
            // 
            // xrTableRow046
            // 
            this.xrTableRow046.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell122,
            this.xrTableCell123,
            this.xrTableCell124,
            this.xrTableCell383});
            this.xrTableRow046.Name = "xrTableRow046";
            this.xrTableRow046.Weight = 0.43934611026736259D;
            // 
            // xrTableCell122
            // 
            this.xrTableCell122.Name = "xrTableCell122";
            this.xrTableCell122.Text = "c.";
            this.xrTableCell122.Weight = 0.33185839653015137D;
            // 
            // xrTableCell123
            // 
            this.xrTableCell123.Multiline = true;
            this.xrTableCell123.Name = "xrTableCell123";
            this.xrTableCell123.Text = "Fair Housing";
            this.xrTableCell123.Weight = 1.7323890948416143D;
            // 
            // xrTableCell124
            // 
            this.xrTableCell124.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Compl_Help_FairHousing_Workshop.All_Housing", "{0:n0}")});
            this.xrTableCell124.Name = "xrTableCell124";
            this.xrTableCell124.StylePriority.UseTextAlignment = false;
            this.xrTableCell124.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell124.Weight = 0.43391993187925859D;
            // 
            // xrTableCell383
            // 
            this.xrTableCell383.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Compl_Help_FairHousing_Workshop.HUD_Housing", "{0:n0}")});
            this.xrTableCell383.Name = "xrTableCell383";
            this.xrTableCell383.StylePriority.UseTextAlignment = false;
            this.xrTableCell383.Tag = "Compl_Help_FairHousing_Workshop.HUD";
            this.xrTableCell383.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell383.Weight = 0.50183266615594291D;
            // 
            // xrTableRow047
            // 
            this.xrTableRow047.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell119,
            this.xrTableCell120,
            this.xrTableCell121,
            this.xrTableCell384});
            this.xrTableRow047.Name = "xrTableRow047";
            this.xrTableRow047.Weight = 0.43934599635982097D;
            // 
            // xrTableCell119
            // 
            this.xrTableCell119.Name = "xrTableCell119";
            this.xrTableCell119.Text = "d.";
            this.xrTableCell119.Weight = 0.33185839653015137D;
            // 
            // xrTableCell120
            // 
            this.xrTableCell120.Multiline = true;
            this.xrTableCell120.Name = "xrTableCell120";
            this.xrTableCell120.Text = "Preventing Mortgage Delinquency";
            this.xrTableCell120.Weight = 1.7323890948416143D;
            // 
            // xrTableCell121
            // 
            this.xrTableCell121.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Compl_Resolv_Prevent_Mortg_Deliq.All_Housing", "{0:n0}")});
            this.xrTableCell121.Name = "xrTableCell121";
            this.xrTableCell121.StylePriority.UseTextAlignment = false;
            this.xrTableCell121.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell121.Weight = 0.43391993187925859D;
            // 
            // xrTableCell384
            // 
            this.xrTableCell384.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Compl_Resolv_Prevent_Mortg_Deliq.HUD_Housing", "{0:n0}")});
            this.xrTableCell384.Name = "xrTableCell384";
            this.xrTableCell384.StylePriority.UseTextAlignment = false;
            this.xrTableCell384.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell384.Weight = 0.50183266615594291D;
            // 
            // xrTableRow048
            // 
            this.xrTableRow048.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell76,
            this.xrTableCell77,
            this.xrTableCell118,
            this.xrTableCell385});
            this.xrTableRow048.Name = "xrTableRow048";
            this.xrTableRow048.Weight = 0.43934606870770421D;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.Text = "e.";
            this.xrTableCell76.Weight = 0.33185839653015137D;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.Text = "Renting";
            this.xrTableCell77.Weight = 1.7323890948416143D;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Counseling_Rental_Workshop.All_Housing", "{0:n0}")});
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.StylePriority.UseTextAlignment = false;
            this.xrTableCell118.Tag = "Counseling_Rental_Workshop";
            this.xrTableCell118.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell118.Weight = 0.43391993187925859D;
            // 
            // xrTableCell385
            // 
            this.xrTableCell385.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Counseling_Rental_Workshop.HUD_Housing", "{0:n0}")});
            this.xrTableCell385.Name = "xrTableCell385";
            this.xrTableCell385.StylePriority.UseTextAlignment = false;
            this.xrTableCell385.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell385.Weight = 0.50183266615594291D;
            // 
            // xrTableRow049
            // 
            this.xrTableRow049.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell112,
            this.xrTableCell113,
            this.xrTableCell114,
            this.xrTableCell386});
            this.xrTableRow049.Name = "xrTableRow049";
            this.xrTableRow049.Weight = 0.43934599635982119D;
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.Text = "f.";
            this.xrTableCell112.Weight = 0.33185839653015137D;
            // 
            // xrTableCell113
            // 
            this.xrTableCell113.Multiline = true;
            this.xrTableCell113.Name = "xrTableCell113";
            this.xrTableCell113.Text = "HomeBuyer Education";
            this.xrTableCell113.Weight = 1.7323890948416143D;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Compl_HomeBuyer_Educ_Workshop.All_Housing", "{0:n0}")});
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.StylePriority.UseTextAlignment = false;
            this.xrTableCell114.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell114.Weight = 0.43391993187925859D;
            // 
            // xrTableCell386
            // 
            this.xrTableCell386.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Compl_HomeBuyer_Educ_Workshop.HUD_Housing", "{0:n0}")});
            this.xrTableCell386.Name = "xrTableCell386";
            this.xrTableCell386.StylePriority.UseTextAlignment = false;
            this.xrTableCell386.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell386.Weight = 0.50183266615594291D;
            // 
            // xrTableRow050
            // 
            this.xrTableRow050.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell109,
            this.xrTableCell110,
            this.xrTableCell111,
            this.xrTableCell387});
            this.xrTableRow050.Name = "xrTableRow050";
            this.xrTableRow050.Weight = 0.43934584554230449D;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.Text = "g.";
            this.xrTableCell109.Weight = 0.33185839653015137D;
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.Text = "Non-Delinquency Post-Purchase";
            this.xrTableCell110.Weight = 1.7323890948416143D;
            // 
            // xrTableCell111
            // 
            this.xrTableCell111.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Compl_NonDelinqency_PostPurchase_Workshop.All_Housing", "{0:n0}")});
            this.xrTableCell111.Name = "xrTableCell111";
            this.xrTableCell111.StylePriority.UseTextAlignment = false;
            this.xrTableCell111.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell111.Weight = 0.43391993187925859D;
            // 
            // xrTableCell387
            // 
            this.xrTableCell387.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Compl_NonDelinqency_PostPurchase_Workshop.HUD_Housing", "{0:n0}")});
            this.xrTableCell387.Name = "xrTableCell387";
            this.xrTableCell387.StylePriority.UseTextAlignment = false;
            this.xrTableCell387.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell387.Weight = 0.50183266615594291D;
            // 
            // xrTableRow051
            // 
            this.xrTableRow051.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell103,
            this.xrTableCell104,
            this.xrTableCell105,
            this.xrTableCell388});
            this.xrTableRow051.Name = "xrTableRow051";
            this.xrTableRow051.Weight = 0.43934584554230405D;
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.Text = "h.";
            this.xrTableCell103.Weight = 0.33185839653015137D;
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.Text = "Resolving Mortgage Delinquency";
            this.xrTableCell104.Weight = 1.7323890948416143D;
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Compl_Resolv_Prevent_Mortg_Deliq.All_Housing", "{0:n0}")});
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.StylePriority.UseTextAlignment = false;
            this.xrTableCell105.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell105.Weight = 0.43391993187925859D;
            // 
            // xrTableCell388
            // 
            this.xrTableCell388.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Compl_Resolv_Prevent_Mortg_Deliq.HUD_Housing", "{0:n0}")});
            this.xrTableCell388.Name = "xrTableCell388";
            this.xrTableCell388.StylePriority.UseTextAlignment = false;
            this.xrTableCell388.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell388.Weight = 0.50183266615594291D;
            // 
            // xrTableRow055
            // 
            this.xrTableRow055.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell153,
            this.xrTableCell154,
            this.xrTableCell155,
            this.xrTableCell392});
            this.xrTableRow055.Name = "xrTableRow055";
            this.xrTableRow055.Weight = 0.43934584554230449D;
            // 
            // xrTableCell153
            // 
            this.xrTableCell153.Name = "xrTableCell153";
            this.xrTableCell153.Text = "i.";
            this.xrTableCell153.Weight = 0.33185839653015137D;
            // 
            // xrTableCell154
            // 
            this.xrTableCell154.Name = "xrTableCell154";
            this.xrTableCell154.Text = "Others";
            this.xrTableCell154.Weight = 1.7323890948416143D;
            // 
            // xrTableCell155
            // 
            this.xrTableCell155.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Compl_Other_Workshop.All_Housing", "{0:n0}")});
            this.xrTableCell155.Name = "xrTableCell155";
            this.xrTableCell155.StylePriority.UseTextAlignment = false;
            this.xrTableCell155.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell155.Weight = 0.43391993187925859D;
            // 
            // xrTableCell392
            // 
            this.xrTableCell392.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Compl_Other_Workshop.HUD_Housing", "{0:n0}")});
            this.xrTableCell392.Name = "xrTableCell392";
            this.xrTableCell392.StylePriority.UseTextAlignment = false;
            this.xrTableCell392.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell392.Weight = 0.50183266615594291D;
            // 
            // xrTableRow052
            // 
            this.xrTableRow052.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell107,
            this.xrTableCell108,
            this.xrTableCell389});
            this.xrTableRow052.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableRow052.Name = "xrTableRow052";
            this.xrTableRow052.StylePriority.UseFont = false;
            this.xrTableRow052.Weight = 0.43934584554230405D;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.StylePriority.UseTextAlignment = false;
            this.xrTableCell107.Text = "Section 8 Total:";
            this.xrTableCell107.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell107.Weight = 2.0642474913717654D;
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Section_8_Total.All_Housing", "{0:n0}")});
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.StylePriority.UseTextAlignment = false;
            this.xrTableCell108.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell108.Weight = 0.43391993187925859D;
            // 
            // xrTableCell389
            // 
            this.xrTableCell389.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Section_8_Total.HUD_Housing", "{0:n0}")});
            this.xrTableCell389.Name = "xrTableCell389";
            this.xrTableCell389.StylePriority.UseTextAlignment = false;
            this.xrTableCell389.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell389.Weight = 0.50183266615594291D;
            // 
            // xrTableRow053
            // 
            this.xrTableRow053.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell100});
            this.xrTableRow053.Name = "xrTableRow053";
            this.xrTableRow053.Weight = 0.43934584554230449D;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.Weight = 3D;
            // 
            // xrTableRow054
            // 
            this.xrTableRow054.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell45});
            this.xrTableRow054.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableRow054.Name = "xrTableRow054";
            this.xrTableRow054.StylePriority.UseFont = false;
            this.xrTableRow054.Weight = 0.43934584554230405D;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.BackColor = System.Drawing.Color.LightGray;
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.StylePriority.UseBackColor = false;
            this.xrTableCell45.Text = "9. One-On-One Counseling";
            this.xrTableCell45.Weight = 3D;
            // 
            // xrTableRow056
            // 
            this.xrTableRow056.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell168,
            this.xrTableCell169,
            this.xrTableCell170,
            this.xrTableCell393});
            this.xrTableRow056.Name = "xrTableRow056";
            this.xrTableRow056.Weight = 0.43934584554230449D;
            // 
            // xrTableCell168
            // 
            this.xrTableCell168.Name = "xrTableCell168";
            this.xrTableCell168.Text = "a.";
            this.xrTableCell168.Weight = 0.33185839653015137D;
            // 
            // xrTableCell169
            // 
            this.xrTableCell169.Name = "xrTableCell169";
            this.xrTableCell169.Tag = "One_Homeless_Assistance_Counseling";
            this.xrTableCell169.Text = "Homeless Assistance";
            this.xrTableCell169.Weight = 1.7323886729666149D;
            // 
            // xrTableCell170
            // 
            this.xrTableCell170.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "One_Homeless_Assistance_Counseling.All_Housing", "{0:n0}")});
            this.xrTableCell170.Name = "xrTableCell170";
            this.xrTableCell170.StylePriority.UseTextAlignment = false;
            this.xrTableCell170.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell170.Weight = 0.4339200725042584D;
            // 
            // xrTableCell393
            // 
            this.xrTableCell393.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "One_Homeless_Assistance_Counseling.HUD_Housing", "{0:n0}")});
            this.xrTableCell393.Name = "xrTableCell393";
            this.xrTableCell393.StylePriority.UseTextAlignment = false;
            this.xrTableCell393.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell393.Weight = 0.50183294740594242D;
            // 
            // xrTableRow057
            // 
            this.xrTableRow057.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell150,
            this.xrTableCell151,
            this.xrTableCell152,
            this.xrTableCell394});
            this.xrTableRow057.Name = "xrTableRow057";
            this.xrTableRow057.Weight = 0.43934584554230449D;
            // 
            // xrTableCell150
            // 
            this.xrTableCell150.Name = "xrTableCell150";
            this.xrTableCell150.Text = "b.";
            this.xrTableCell150.Weight = 0.33185839653015137D;
            // 
            // xrTableCell151
            // 
            this.xrTableCell151.Name = "xrTableCell151";
            this.xrTableCell151.Text = "Rental Topics";
            this.xrTableCell151.Weight = 1.7323886729666149D;
            // 
            // xrTableCell152
            // 
            this.xrTableCell152.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "One_Rental_Topics_Counseling.All_Housing", "{0:n0}")});
            this.xrTableCell152.Name = "xrTableCell152";
            this.xrTableCell152.StylePriority.UseTextAlignment = false;
            this.xrTableCell152.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell152.Weight = 0.4339200725042584D;
            // 
            // xrTableCell394
            // 
            this.xrTableCell394.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "One_Rental_Topics_Counseling.HUD_Housing", "{0:n0}")});
            this.xrTableCell394.Name = "xrTableCell394";
            this.xrTableCell394.StylePriority.UseTextAlignment = false;
            this.xrTableCell394.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell394.Weight = 0.50183294740594242D;
            // 
            // xrTableRow058
            // 
            this.xrTableRow058.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell180,
            this.xrTableCell181,
            this.xrTableCell182,
            this.xrTableCell395});
            this.xrTableRow058.Name = "xrTableRow058";
            this.xrTableRow058.Weight = 0.43934584554230405D;
            // 
            // xrTableCell180
            // 
            this.xrTableCell180.Name = "xrTableCell180";
            this.xrTableCell180.Text = "c.";
            this.xrTableCell180.Weight = 0.33185839653015137D;
            // 
            // xrTableCell181
            // 
            this.xrTableCell181.Name = "xrTableCell181";
            this.xrTableCell181.Text = "Pre-Purchase/HomeBuying";
            this.xrTableCell181.Weight = 1.7323886729666149D;
            // 
            // xrTableCell182
            // 
            this.xrTableCell182.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "One_PrePurchase_HomeBuying_Counseling.All_Housing", "{0:n0}")});
            this.xrTableCell182.Name = "xrTableCell182";
            this.xrTableCell182.StylePriority.UseTextAlignment = false;
            this.xrTableCell182.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell182.Weight = 0.4339200725042584D;
            // 
            // xrTableCell395
            // 
            this.xrTableCell395.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "One_PrePurchase_HomeBuying_Counseling.HUD_Housing", "{0:n0}")});
            this.xrTableCell395.Name = "xrTableCell395";
            this.xrTableCell395.StylePriority.UseTextAlignment = false;
            this.xrTableCell395.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell395.Weight = 0.50183294740594242D;
            // 
            // xrTableRow059
            // 
            this.xrTableRow059.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell186,
            this.xrTableCell187,
            this.xrTableCell188,
            this.xrTableCell396});
            this.xrTableRow059.Name = "xrTableRow059";
            this.xrTableRow059.Weight = 0.43934584554230449D;
            // 
            // xrTableCell186
            // 
            this.xrTableCell186.Name = "xrTableCell186";
            this.xrTableCell186.Text = "d.";
            this.xrTableCell186.Weight = 0.33185839653015137D;
            // 
            // xrTableCell187
            // 
            this.xrTableCell187.Name = "xrTableCell187";
            this.xrTableCell187.Text = "Home Maintenance and Financial Management";
            this.xrTableCell187.Weight = 1.7323886729666149D;
            // 
            // xrTableCell188
            // 
            this.xrTableCell188.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "One_Home_Maintenance_Fin_Management_Counseling.All_Housing", "{0:n0}")});
            this.xrTableCell188.Name = "xrTableCell188";
            this.xrTableCell188.StylePriority.UseTextAlignment = false;
            this.xrTableCell188.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell188.Weight = 0.4339200725042584D;
            // 
            // xrTableCell396
            // 
            this.xrTableCell396.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "One_Home_Maintenance_Fin_Management_Counseling.HUD_Housing", "{0:n0}")});
            this.xrTableCell396.Name = "xrTableCell396";
            this.xrTableCell396.StylePriority.UseTextAlignment = false;
            this.xrTableCell396.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell396.Weight = 0.50183294740594242D;
            // 
            // xrTableRow060
            // 
            this.xrTableRow060.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell183,
            this.xrTableCell184,
            this.xrTableCell185,
            this.xrTableCell397});
            this.xrTableRow060.Name = "xrTableRow060";
            this.xrTableRow060.Weight = 0.43934584554230449D;
            // 
            // xrTableCell183
            // 
            this.xrTableCell183.Name = "xrTableCell183";
            this.xrTableCell183.Text = "e.";
            this.xrTableCell183.Weight = 0.33185839653015137D;
            // 
            // xrTableCell184
            // 
            this.xrTableCell184.Name = "xrTableCell184";
            this.xrTableCell184.Text = "Reverse Mortgage";
            this.xrTableCell184.Weight = 1.7323886729666149D;
            // 
            // xrTableCell185
            // 
            this.xrTableCell185.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "One_Reverse_Mortgage_Counseling.All_Housing", "{0:n0}")});
            this.xrTableCell185.Name = "xrTableCell185";
            this.xrTableCell185.StylePriority.UseTextAlignment = false;
            this.xrTableCell185.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell185.Weight = 0.4339200725042584D;
            // 
            // xrTableCell397
            // 
            this.xrTableCell397.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "One_Reverse_Mortgage_Counseling.HUD_Housing", "{0:n0}")});
            this.xrTableCell397.Name = "xrTableCell397";
            this.xrTableCell397.StylePriority.UseTextAlignment = false;
            this.xrTableCell397.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell397.Weight = 0.50183294740594242D;
            // 
            // xrTableRow061
            // 
            this.xrTableRow061.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell165,
            this.xrTableCell166,
            this.xrTableCell167,
            this.xrTableCell398});
            this.xrTableRow061.Name = "xrTableRow061";
            this.xrTableRow061.Weight = 0.43934584554230405D;
            // 
            // xrTableCell165
            // 
            this.xrTableCell165.Name = "xrTableCell165";
            this.xrTableCell165.Text = "f.";
            this.xrTableCell165.Weight = 0.33185839653015137D;
            // 
            // xrTableCell166
            // 
            this.xrTableCell166.Name = "xrTableCell166";
            this.xrTableCell166.Text = "Resolving or Preventing Mortgage Delinquency";
            this.xrTableCell166.Weight = 1.7323886729666149D;
            // 
            // xrTableCell167
            // 
            this.xrTableCell167.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "One_Resolv_Prevent_Mortg_Delinq_Counseling.All_Housing", "{0:n0}")});
            this.xrTableCell167.Name = "xrTableCell167";
            this.xrTableCell167.StylePriority.UseTextAlignment = false;
            this.xrTableCell167.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell167.Weight = 0.4339200725042584D;
            // 
            // xrTableCell398
            // 
            this.xrTableCell398.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "One_Resolv_Prevent_Mortg_Delinq_Counseling.HUD_Housing", "{0:n0}")});
            this.xrTableCell398.Name = "xrTableCell398";
            this.xrTableCell398.StylePriority.UseTextAlignment = false;
            this.xrTableCell398.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell398.Weight = 0.50183294740594242D;
            // 
            // xrTableRow062
            // 
            this.xrTableRow062.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell178,
            this.xrTableCell179,
            this.xrTableCell399});
            this.xrTableRow062.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableRow062.Name = "xrTableRow062";
            this.xrTableRow062.StylePriority.UseFont = false;
            this.xrTableRow062.Weight = 0.43934584554230405D;
            // 
            // xrTableCell178
            // 
            this.xrTableCell178.Name = "xrTableCell178";
            this.xrTableCell178.StylePriority.UseTextAlignment = false;
            this.xrTableCell178.Text = "Section 9 Total:";
            this.xrTableCell178.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell178.Weight = 2.0642470694967665D;
            // 
            // xrTableCell179
            // 
            this.xrTableCell179.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Section_9_Total.All_Housing", "{0:n0}")});
            this.xrTableCell179.Name = "xrTableCell179";
            this.xrTableCell179.StylePriority.UseTextAlignment = false;
            this.xrTableCell179.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell179.Weight = 0.4339200725042584D;
            // 
            // xrTableCell399
            // 
            this.xrTableCell399.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Section_9_Total.HUD_Housing", "{0:n0}")});
            this.xrTableCell399.Name = "xrTableCell399";
            this.xrTableCell399.StylePriority.UseTextAlignment = false;
            this.xrTableCell399.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell399.Weight = 0.50183294740594242D;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5,
            this.xrTableCell4,
            this.xrTableCell3});
            this.xrTableRow2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.StylePriority.UseFont = false;
            this.xrTableRow2.StylePriority.UseTextAlignment = false;
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableRow2.Weight = 0.43934584554230449D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "Households Served Sections 8 and 9 Total:";
            this.xrTableCell5.Weight = 2.0642475995547627D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Section_8_and_9_Total.All_Housing", "{0:n0}")});
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell4.Weight = 0.43392029112749086D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Section_8_and_9_Total.HUD_Housing", "{0:n0}")});
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell3.Weight = 0.50183210931774647D;
            // 
            // xrTableRow064
            // 
            this.xrTableRow064.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell171});
            this.xrTableRow064.Name = "xrTableRow064";
            this.xrTableRow064.Weight = 0.43934588477712078D;
            // 
            // xrTableCell171
            // 
            this.xrTableCell171.Name = "xrTableCell171";
            this.xrTableCell171.Weight = 3.0000000894069672D;
            // 
            // xrTableRow065
            // 
            this.xrTableRow065.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell162});
            this.xrTableRow065.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableRow065.Name = "xrTableRow065";
            this.xrTableRow065.StylePriority.UseFont = false;
            this.xrTableRow065.Weight = 0.43934584554230405D;
            // 
            // xrTableCell162
            // 
            this.xrTableCell162.BackColor = System.Drawing.Color.LightGray;
            this.xrTableCell162.Name = "xrTableCell162";
            this.xrTableCell162.StylePriority.UseBackColor = false;
            this.xrTableCell162.Text = "10. Impacts";
            this.xrTableCell162.Weight = 3.0000000894069672D;
            // 
            // xrTableRow063
            // 
            this.xrTableRow063.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2,
            this.xrTableCell175,
            this.xrTableCell176,
            this.xrTableCell400});
            this.xrTableRow063.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTableRow063.Name = "xrTableRow063";
            this.xrTableRow063.StylePriority.UseFont = false;
            this.xrTableRow063.Weight = 0.43934584554230449D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Text = "a.";
            this.xrTableCell2.Weight = 0.33185842638714125D;
            // 
            // xrTableCell175
            // 
            this.xrTableCell175.Name = "xrTableCell175";
            this.xrTableCell175.StylePriority.UseTextAlignment = false;
            this.xrTableCell175.Text = "One-On-One and Group Counseling";
            this.xrTableCell175.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell175.Weight = 1.7323886431096254D;
            // 
            // xrTableCell176
            // 
            this.xrTableCell176.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Impact_One_On_One_And_Group.All_Housing", "{0:n0}")});
            this.xrTableCell176.Name = "xrTableCell176";
            this.xrTableCell176.StylePriority.UseTextAlignment = false;
            this.xrTableCell176.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell176.Weight = 0.4339200725042584D;
            // 
            // xrTableCell400
            // 
            this.xrTableCell400.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Impact_One_On_One_And_Group.HUD_Housing", "{0:n0}")});
            this.xrTableCell400.Name = "xrTableCell400";
            this.xrTableCell400.StylePriority.UseTextAlignment = false;
            this.xrTableCell400.Tag = "Impact_One_On_One_And_Group";
            this.xrTableCell400.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell400.Weight = 0.50183294740594242D;
            // 
            // xrTableRow066
            // 
            this.xrTableRow066.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell159,
            this.xrTableCell160,
            this.xrTableCell161,
            this.xrTableCell403});
            this.xrTableRow066.Name = "xrTableRow066";
            this.xrTableRow066.Weight = 0.43934590444291333D;
            // 
            // xrTableCell159
            // 
            this.xrTableCell159.Name = "xrTableCell159";
            this.xrTableCell159.Text = "b.";
            this.xrTableCell159.Weight = 0.33185839653015137D;
            // 
            // xrTableCell160
            // 
            this.xrTableCell160.Name = "xrTableCell160";
            this.xrTableCell160.Text = "Received Information on Fair Housing";
            this.xrTableCell160.Weight = 1.7323886729666149D;
            // 
            // xrTableCell161
            // 
            this.xrTableCell161.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Impact_Received_Info_Fair_Housing.All_Housing", "{0:n0}")});
            this.xrTableCell161.Name = "xrTableCell161";
            this.xrTableCell161.StylePriority.UseTextAlignment = false;
            this.xrTableCell161.Tag = "Impact_Received_Info_Fair_Housing";
            this.xrTableCell161.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell161.Weight = 0.4339200725042584D;
            // 
            // xrTableCell403
            // 
            this.xrTableCell403.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Impact_Received_Info_Fair_Housing.HUD_Housing", "{0:n0}")});
            this.xrTableCell403.Name = "xrTableCell403";
            this.xrTableCell403.StylePriority.UseTextAlignment = false;
            this.xrTableCell403.Tag = "Impact_Received_Info_Fair_Housing";
            this.xrTableCell403.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell403.Weight = 0.50183294740594242D;
            // 
            // xrTableRow067
            // 
            this.xrTableRow067.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell156,
            this.xrTableCell157,
            this.xrTableCell158,
            this.xrTableCell404});
            this.xrTableRow067.Name = "xrTableRow067";
            this.xrTableRow067.Weight = 0.43934562522883924D;
            // 
            // xrTableCell156
            // 
            this.xrTableCell156.Name = "xrTableCell156";
            this.xrTableCell156.Text = "c.";
            this.xrTableCell156.Weight = 0.33185839653015137D;
            // 
            // xrTableCell157
            // 
            this.xrTableCell157.Name = "xrTableCell157";
            this.xrTableCell157.Text = "Developed Sustainable Budget";
            this.xrTableCell157.Weight = 1.7323886729666149D;
            // 
            // xrTableCell158
            // 
            this.xrTableCell158.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Impact_Developed_Sustainable_Budget.All_Housing", "{0:n0}")});
            this.xrTableCell158.Name = "xrTableCell158";
            this.xrTableCell158.StylePriority.UseTextAlignment = false;
            this.xrTableCell158.Tag = "Impact_Developed_Sustainable_Budget";
            this.xrTableCell158.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell158.Weight = 0.4339200725042584D;
            // 
            // xrTableCell404
            // 
            this.xrTableCell404.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Impact_Developed_Sustainable_Budget.HUD_Housing", "{0:n0}")});
            this.xrTableCell404.Name = "xrTableCell404";
            this.xrTableCell404.StylePriority.UseTextAlignment = false;
            this.xrTableCell404.Tag = "Impact_Developed_Sustainable_Budget";
            this.xrTableCell404.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell404.Weight = 0.50183294740594242D;
            // 
            // xrTableRow068
            // 
            this.xrTableRow068.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell138,
            this.xrTableCell139,
            this.xrTableCell149,
            this.xrTableCell405});
            this.xrTableRow068.Name = "xrTableRow068";
            this.xrTableRow068.Weight = 0.43934572619891243D;
            // 
            // xrTableCell138
            // 
            this.xrTableCell138.Name = "xrTableCell138";
            this.xrTableCell138.Text = "d.";
            this.xrTableCell138.Weight = 0.33185839653015137D;
            // 
            // xrTableCell139
            // 
            this.xrTableCell139.Name = "xrTableCell139";
            this.xrTableCell139.Text = "Improved Financial Capacity";
            this.xrTableCell139.Weight = 1.7323890948416143D;
            // 
            // xrTableCell149
            // 
            this.xrTableCell149.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Impact_Improved_Financial_Capacity.All_Housing", "{0:n0}")});
            this.xrTableCell149.Name = "xrTableCell149";
            this.xrTableCell149.StylePriority.UseTextAlignment = false;
            this.xrTableCell149.Tag = "Impact_Improved_Financial_Capacity";
            this.xrTableCell149.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell149.Weight = 0.43392049437925778D;
            // 
            // xrTableCell405
            // 
            this.xrTableCell405.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Impact_Improved_Financial_Capacity.HUD_Housing", "{0:n0}")});
            this.xrTableCell405.Name = "xrTableCell405";
            this.xrTableCell405.StylePriority.UseTextAlignment = false;
            this.xrTableCell405.Tag = "Impact_Improved_Financial_Capacity";
            this.xrTableCell405.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell405.Weight = 0.50183210365594366D;
            // 
            // xrTableRow069
            // 
            this.xrTableRow069.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell217,
            this.xrTableCell218,
            this.xrTableCell219,
            this.xrTableCell406});
            this.xrTableRow069.Name = "xrTableRow069";
            this.xrTableRow069.Weight = 0.43934584540820243D;
            // 
            // xrTableCell217
            // 
            this.xrTableCell217.Name = "xrTableCell217";
            this.xrTableCell217.Text = "e.";
            this.xrTableCell217.Weight = 0.33185839653015137D;
            // 
            // xrTableCell218
            // 
            this.xrTableCell218.Name = "xrTableCell218";
            this.xrTableCell218.Text = "Gained Access Resources Improve Housing";
            this.xrTableCell218.Weight = 1.7323890948416143D;
            // 
            // xrTableCell219
            // 
            this.xrTableCell219.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Impact_Gained_Access_Resources_Improve_Housing.All_Housing", "{0:n0}")});
            this.xrTableCell219.Name = "xrTableCell219";
            this.xrTableCell219.StylePriority.UseTextAlignment = false;
            this.xrTableCell219.Tag = "Impact_Gained_Access_Resources_Improve_Housing";
            this.xrTableCell219.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell219.Weight = 0.43392049437925778D;
            // 
            // xrTableCell406
            // 
            this.xrTableCell406.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Impact_Gained_Access_Resources_Improve_Housing.HUD_Housing", "{0:n0}")});
            this.xrTableCell406.Name = "xrTableCell406";
            this.xrTableCell406.StylePriority.UseTextAlignment = false;
            this.xrTableCell406.Tag = "Impact_Gained_Access_Resources_Improve_Housing";
            this.xrTableCell406.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell406.Weight = 0.50183210365594366D;
            // 
            // xrTableRow070
            // 
            this.xrTableRow070.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell214,
            this.xrTableCell215,
            this.xrTableCell216,
            this.xrTableCell407});
            this.xrTableRow070.Name = "xrTableRow070";
            this.xrTableRow070.Weight = 0.439345845408202D;
            // 
            // xrTableCell214
            // 
            this.xrTableCell214.Name = "xrTableCell214";
            this.xrTableCell214.Text = "f.";
            this.xrTableCell214.Weight = 0.33185839653015137D;
            // 
            // xrTableCell215
            // 
            this.xrTableCell215.Name = "xrTableCell215";
            this.xrTableCell215.Text = "Gained Access Non-Housing Resources";
            this.xrTableCell215.Weight = 1.7323890948416143D;
            // 
            // xrTableCell216
            // 
            this.xrTableCell216.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Impact_Gained_Access_NonHousing_Resources.All_Housing", "{0:n0}")});
            this.xrTableCell216.Name = "xrTableCell216";
            this.xrTableCell216.StylePriority.UseTextAlignment = false;
            this.xrTableCell216.Tag = "Impact_Gained_Access_NonHousing_Resources";
            this.xrTableCell216.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell216.Weight = 0.43392049437925778D;
            // 
            // xrTableCell407
            // 
            this.xrTableCell407.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Impact_Gained_Access_NonHousing_Resources.HUD_Housing", "{0:n0}")});
            this.xrTableCell407.Name = "xrTableCell407";
            this.xrTableCell407.StylePriority.UseTextAlignment = false;
            this.xrTableCell407.Tag = "Impact_Gained_Access_NonHousing_Resources";
            this.xrTableCell407.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell407.Weight = 0.50183210365594366D;
            // 
            // xrTableRow071
            // 
            this.xrTableRow071.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell211,
            this.xrTableCell212,
            this.xrTableCell213,
            this.xrTableCell408});
            this.xrTableRow071.Name = "xrTableRow071";
            this.xrTableRow071.Weight = 0.43934584540820243D;
            // 
            // xrTableCell211
            // 
            this.xrTableCell211.Name = "xrTableCell211";
            this.xrTableCell211.Text = "g.";
            this.xrTableCell211.Weight = 0.33185839653015137D;
            // 
            // xrTableCell212
            // 
            this.xrTableCell212.Name = "xrTableCell212";
            this.xrTableCell212.Text = "Homeless Obtained Housing";
            this.xrTableCell212.Weight = 1.7323890948416143D;
            // 
            // xrTableCell213
            // 
            this.xrTableCell213.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Impact_Homeless_Obtained_Housing.All_Housing", "{0:n0}")});
            this.xrTableCell213.Name = "xrTableCell213";
            this.xrTableCell213.StylePriority.UseTextAlignment = false;
            this.xrTableCell213.Tag = "Impact_Homeless_Obtained_Housing";
            this.xrTableCell213.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell213.Weight = 0.43392049437925778D;
            // 
            // xrTableCell408
            // 
            this.xrTableCell408.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Impact_Homeless_Obtained_Housing.HUD_Housing", "{0:n0}")});
            this.xrTableCell408.Name = "xrTableCell408";
            this.xrTableCell408.StylePriority.UseTextAlignment = false;
            this.xrTableCell408.Tag = "Impact_Homeless_Obtained_Housing";
            this.xrTableCell408.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell408.Weight = 0.50183210365594366D;
            // 
            // xrTableRow075
            // 
            this.xrTableRow075.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell199,
            this.xrTableCell200,
            this.xrTableCell201,
            this.xrTableCell412});
            this.xrTableRow075.Name = "xrTableRow075";
            this.xrTableRow075.Weight = 0.439345845408202D;
            // 
            // xrTableCell199
            // 
            this.xrTableCell199.Name = "xrTableCell199";
            this.xrTableCell199.Text = "h.";
            this.xrTableCell199.Weight = 0.33185839653015137D;
            // 
            // xrTableCell200
            // 
            this.xrTableCell200.Name = "xrTableCell200";
            this.xrTableCell200.Text = "Received Rental Counseling and Avoided Eviction";
            this.xrTableCell200.Weight = 1.7323890948416143D;
            // 
            // xrTableCell201
            // 
            this.xrTableCell201.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Impact_Received_Rental_Counseling_Avoided_Eviction.All_Housing", "{0:n0}")});
            this.xrTableCell201.Name = "xrTableCell201";
            this.xrTableCell201.StylePriority.UseTextAlignment = false;
            this.xrTableCell201.Tag = "Impact_Received_Rental_Counseling_Avoided_Eviction";
            this.xrTableCell201.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell201.Weight = 0.43392049437925778D;
            // 
            // xrTableCell412
            // 
            this.xrTableCell412.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Impact_Received_Rental_Counseling_Avoided_Eviction.HUD_Housing", "{0:n0}")});
            this.xrTableCell412.Name = "xrTableCell412";
            this.xrTableCell412.StylePriority.UseTextAlignment = false;
            this.xrTableCell412.Tag = "Impact_Received_Rental_Counseling_Avoided_Eviction";
            this.xrTableCell412.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell412.Weight = 0.50183210365594366D;
            // 
            // xrTableRow076
            // 
            this.xrTableRow076.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell196,
            this.xrTableCell197,
            this.xrTableCell198,
            this.xrTableCell413});
            this.xrTableRow076.Name = "xrTableRow076";
            this.xrTableRow076.Weight = 0.43934584540820243D;
            // 
            // xrTableCell196
            // 
            this.xrTableCell196.Name = "xrTableCell196";
            this.xrTableCell196.Text = "i.";
            this.xrTableCell196.Weight = 0.33185839653015137D;
            // 
            // xrTableCell197
            // 
            this.xrTableCell197.Multiline = true;
            this.xrTableCell197.Name = "xrTableCell197";
            this.xrTableCell197.Text = "Received Rental Counseling and Improved Living Conditions";
            this.xrTableCell197.Weight = 1.7323890948416143D;
            // 
            // xrTableCell198
            // 
            this.xrTableCell198.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Impact_Received_Rental_Counseling_Improved_Living_Conditions.All_Housing", "{0:n0}")});
            this.xrTableCell198.Name = "xrTableCell198";
            this.xrTableCell198.StylePriority.UseTextAlignment = false;
            this.xrTableCell198.Tag = "Impact_Received_Rental_Counseling_Improved_Living_Conditions";
            this.xrTableCell198.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell198.Weight = 0.43392049437925778D;
            // 
            // xrTableCell413
            // 
            this.xrTableCell413.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Impact_Received_Rental_Counseling_Improved_Living_Conditions.HUD_Housing", "{0:n0}")});
            this.xrTableCell413.Name = "xrTableCell413";
            this.xrTableCell413.StylePriority.UseTextAlignment = false;
            this.xrTableCell413.Tag = "Impact_Received_Rental_Counseling_Improved_Living_Conditions";
            this.xrTableCell413.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell413.Weight = 0.50183210365594366D;
            // 
            // xrTableRow077
            // 
            this.xrTableRow077.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell227,
            this.xrTableCell228,
            this.xrTableCell229,
            this.xrTableCell414});
            this.xrTableRow077.Name = "xrTableRow077";
            this.xrTableRow077.Weight = 0.439345845408202D;
            // 
            // xrTableCell227
            // 
            this.xrTableCell227.Name = "xrTableCell227";
            this.xrTableCell227.Text = "j.";
            this.xrTableCell227.Weight = 0.33185839653015137D;
            // 
            // xrTableCell228
            // 
            this.xrTableCell228.Name = "xrTableCell228";
            this.xrTableCell228.Text = "Received PrePurchase Counseling Purchased Housing";
            this.xrTableCell228.Weight = 1.7323890948416143D;
            // 
            // xrTableCell229
            // 
            this.xrTableCell229.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Impact_Received_PrePurchase_Counseling_Purchased_Housing.All_Housing", "{0:n0}")});
            this.xrTableCell229.Name = "xrTableCell229";
            this.xrTableCell229.StylePriority.UseTextAlignment = false;
            this.xrTableCell229.Tag = "Impact_Received_PrePurchase_Counseling_Purchased_Housing";
            this.xrTableCell229.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell229.Weight = 0.43392049437925778D;
            // 
            // xrTableCell414
            // 
            this.xrTableCell414.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Impact_Received_PrePurchase_Counseling_Purchased_Housing.HUD_Housing", "{0:n0}")});
            this.xrTableCell414.Name = "xrTableCell414";
            this.xrTableCell414.StylePriority.UseTextAlignment = false;
            this.xrTableCell414.Tag = "Impact_Received_PrePurchase_Counseling_Purchased_Housing";
            this.xrTableCell414.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell414.Weight = 0.50183210365594366D;
            // 
            // xrTableRow078
            // 
            this.xrTableRow078.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell233,
            this.xrTableCell234,
            this.xrTableCell235,
            this.xrTableCell415});
            this.xrTableRow078.Name = "xrTableRow078";
            this.xrTableRow078.Weight = 0.4393454777233492D;
            // 
            // xrTableCell233
            // 
            this.xrTableCell233.Name = "xrTableCell233";
            this.xrTableCell233.Text = "k.";
            this.xrTableCell233.Weight = 0.33185839653015137D;
            // 
            // xrTableCell234
            // 
            this.xrTableCell234.Multiline = true;
            this.xrTableCell234.Name = "xrTableCell234";
            this.xrTableCell234.Text = "Obtained HECM";
            this.xrTableCell234.Weight = 1.7323890948416143D;
            // 
            // xrTableCell235
            // 
            this.xrTableCell235.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Impact_Received_Reverse_Mortgage_Counseling_Obtained_HECM.All_Housing", "{0:n0}")});
            this.xrTableCell235.Name = "xrTableCell235";
            this.xrTableCell235.StylePriority.UseTextAlignment = false;
            this.xrTableCell235.Tag = "Impact_Received_Reverse_Mortgage_Counseling_Obtained_HECM";
            this.xrTableCell235.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell235.Weight = 0.43392049437925778D;
            // 
            // xrTableCell415
            // 
            this.xrTableCell415.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Impact_Received_Reverse_Mortgage_Counseling_Obtained_HECM.HUD_Housing", "{0:n0}")});
            this.xrTableCell415.Name = "xrTableCell415";
            this.xrTableCell415.StylePriority.UseTextAlignment = false;
            this.xrTableCell415.Tag = "Impact_Received_Reverse_Mortgage_Counseling_Obtained_HECM";
            this.xrTableCell415.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell415.Weight = 0.50183210365594366D;
            // 
            // xrTableRow079
            // 
            this.xrTableRow079.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell230,
            this.xrTableCell231,
            this.xrTableCell232,
            this.xrTableCell416});
            this.xrTableRow079.Name = "xrTableRow079";
            this.xrTableRow079.Weight = 0.4393456780343058D;
            // 
            // xrTableCell230
            // 
            this.xrTableCell230.Name = "xrTableCell230";
            this.xrTableCell230.Text = "l.";
            this.xrTableCell230.Weight = 0.33185839653015137D;
            // 
            // xrTableCell231
            // 
            this.xrTableCell231.Name = "xrTableCell231";
            this.xrTableCell231.Text = "NonDelinquency PostPurchase Counseling Improve Affordability";
            this.xrTableCell231.Weight = 1.7323890948416143D;
            // 
            // xrTableCell232
            // 
            this.xrTableCell232.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Impact_Received_NonDelinquency_PostPurchase_Counseling_Improve_Conditions_Afforda" +
                    "bility.All_Housing", "{0:n0}")});
            this.xrTableCell232.Name = "xrTableCell232";
            this.xrTableCell232.StylePriority.UseTextAlignment = false;
            this.xrTableCell232.Tag = "Impact_Received_NonDelinquency_PostPurchase_Counseling_Improve_Conditions_Afforda" +
    "bility";
            this.xrTableCell232.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell232.Weight = 0.43392049437925778D;
            // 
            // xrTableCell416
            // 
            this.xrTableCell416.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Impact_Received_NonDelinquency_PostPurchase_Counseling_Improve_Conditions_Afforda" +
                    "bility.HUD_Housing", "{0:n0}")});
            this.xrTableCell416.Name = "xrTableCell416";
            this.xrTableCell416.StylePriority.UseTextAlignment = false;
            this.xrTableCell416.Tag = "Impact_Received_NonDelinquency_PostPurchase_Counseling_Improve_Conditions_Afforda" +
    "bility";
            this.xrTableCell416.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell416.Weight = 0.50183210365594366D;
            // 
            // xrTableRow080
            // 
            this.xrTableRow080.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell190,
            this.xrTableCell191,
            this.xrTableCell192,
            this.xrTableCell417});
            this.xrTableRow080.Name = "xrTableRow080";
            this.xrTableRow080.Weight = 0.43934601278209773D;
            // 
            // xrTableCell190
            // 
            this.xrTableCell190.Name = "xrTableCell190";
            this.xrTableCell190.Text = "m.";
            this.xrTableCell190.Weight = 0.33185839653015137D;
            // 
            // xrTableCell191
            // 
            this.xrTableCell191.Name = "xrTableCell191";
            this.xrTableCell191.Text = "Prevented Resolved Mortgage Default";
            this.xrTableCell191.Weight = 1.7323889542166144D;
            // 
            // xrTableCell192
            // 
            this.xrTableCell192.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Impact_Prevented_Resolved_Mortgage_Default.All_Housing", "{0:n0}")});
            this.xrTableCell192.Name = "xrTableCell192";
            this.xrTableCell192.StylePriority.UseTextAlignment = false;
            this.xrTableCell192.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell192.Weight = 0.43392063500425765D;
            // 
            // xrTableCell417
            // 
            this.xrTableCell417.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Impact_Prevented_Resolved_Mortgage_Default.HUD_Housing", "{0:n0}")});
            this.xrTableCell417.Name = "xrTableCell417";
            this.xrTableCell417.StylePriority.UseTextAlignment = false;
            this.xrTableCell417.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell417.Weight = 0.50183210365594366D;
            // 
            // xrTableRow081
            // 
            this.xrTableRow081.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell240,
            this.xrTableCell241,
            this.xrTableCell418});
            this.xrTableRow081.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableRow081.Name = "xrTableRow081";
            this.xrTableRow081.StylePriority.UseFont = false;
            this.xrTableRow081.StylePriority.UseTextAlignment = false;
            this.xrTableRow081.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableRow081.Weight = 0.51798792089431045D;
            // 
            // xrTableCell240
            // 
            this.xrTableCell240.Name = "xrTableCell240";
            this.xrTableCell240.Text = "Section 10 Total:";
            this.xrTableCell240.Weight = 2.0642476319967655D;
            // 
            // xrTableCell241
            // 
            this.xrTableCell241.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Section_10_Total.All_Housing", "{0:n0}")});
            this.xrTableCell241.Name = "xrTableCell241";
            this.xrTableCell241.StylePriority.UseTextAlignment = false;
            this.xrTableCell241.Tag = "Section_10_Total";
            this.xrTableCell241.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell241.Weight = 0.433920353754258D;
            // 
            // xrTableCell418
            // 
            this.xrTableCell418.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Section_10_Total.HUD_Housing", "{0:n0}")});
            this.xrTableCell418.Name = "xrTableCell418";
            this.xrTableCell418.StylePriority.UseTextAlignment = false;
            this.xrTableCell418.Tag = "Section_10_Total";
            this.xrTableCell418.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.xrTableCell418.Weight = 0.50183210365594366D;
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.HeightF = 4.166667F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.XrPanel1});
            this.GroupHeader1.HeightF = 42F;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.RepeatEveryPage = true;
            // 
            // XrPanel1
            // 
            this.XrPanel1.BackColor = System.Drawing.Color.Teal;
            this.XrPanel1.BorderColor = System.Drawing.Color.Teal;
            this.XrPanel1.CanGrow = false;
            this.XrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.XrLabel5,
            this.XrLabel3});
            this.XrPanel1.KeepTogether = false;
            this.XrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.XrPanel1.Name = "XrPanel1";
            this.XrPanel1.SizeF = new System.Drawing.SizeF(800F, 42F);
            this.XrPanel1.StylePriority.UseBackColor = false;
            this.XrPanel1.StylePriority.UseBorderColor = false;
            // 
            // xrLabel1
            // 
            this.xrLabel1.BackColor = System.Drawing.Color.Transparent;
            this.xrLabel1.BorderColor = System.Drawing.Color.Transparent;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.ForeColor = System.Drawing.Color.White;
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(551.3473F, 7.999992F);
            this.xrLabel1.Multiline = true;
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(99.6944F, 31F);
            this.xrLabel1.StylePriority.UseBackColor = false;
            this.xrLabel1.StylePriority.UseBorderColor = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseForeColor = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "HUD HOUSING";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // XrLabel5
            // 
            this.XrLabel5.BackColor = System.Drawing.Color.Transparent;
            this.XrLabel5.BorderColor = System.Drawing.Color.Transparent;
            this.XrLabel5.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.XrLabel5.ForeColor = System.Drawing.Color.White;
            this.XrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(447.9704F, 7.999992F);
            this.XrLabel5.Multiline = true;
            this.XrLabel5.Name = "XrLabel5";
            this.XrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel5.SizeF = new System.Drawing.SizeF(94.16666F, 31F);
            this.XrLabel5.StylePriority.UseBackColor = false;
            this.XrLabel5.StylePriority.UseBorderColor = false;
            this.XrLabel5.StylePriority.UseFont = false;
            this.XrLabel5.StylePriority.UseForeColor = false;
            this.XrLabel5.StylePriority.UseTextAlignment = false;
            this.XrLabel5.Text = "ALL HOUSING";
            this.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // XrLabel3
            // 
            this.XrLabel3.BackColor = System.Drawing.Color.Transparent;
            this.XrLabel3.BorderColor = System.Drawing.Color.Transparent;
            this.XrLabel3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.XrLabel3.ForeColor = System.Drawing.Color.White;
            this.XrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(72.01788F, 7.999992F);
            this.XrLabel3.Name = "XrLabel3";
            this.XrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel3.SizeF = new System.Drawing.SizeF(175F, 31F);
            this.XrLabel3.StylePriority.UseBackColor = false;
            this.XrLabel3.StylePriority.UseBorderColor = false;
            this.XrLabel3.StylePriority.UseFont = false;
            this.XrLabel3.StylePriority.UseForeColor = false;
            this.XrLabel3.StylePriority.UseTextAlignment = false;
            this.XrLabel3.Text = "DESCRIPTION";
            this.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.HeightF = 0F;
            this.GroupFooter1.Name = "GroupFooter1";
            this.GroupFooter1.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand;
            this.GroupFooter1.PrintAtBottom = true;
            // 
            // SummaryReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.topMarginBand1,
            this.detailBand1,
            this.bottomMarginBand1,
            this.GroupHeader1,
            this.GroupFooter1});
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 0, 4);
            this.Version = "11.2";
            ((System.ComponentModel.ISupportInitialize)(this.XrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.TopMarginBand topMarginBand1;
        private DevExpress.XtraReports.UI.DetailBand detailBand1;
        private DevExpress.XtraReports.UI.BottomMarginBand bottomMarginBand1;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        internal DevExpress.XtraReports.UI.XRPanel XrPanel1;
        internal DevExpress.XtraReports.UI.XRLabel XrLabel5;
        internal DevExpress.XtraReports.UI.XRLabel XrLabel3;
        internal DevExpress.XtraReports.UI.XRTable XrTable1;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow001;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_001_1;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow002;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_002_1;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_002_2;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_002_3;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow003;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_003_1;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_003_2;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_003_3;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow004;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_004_1;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_004_2;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_004_3;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow005;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_005_2;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_005_3;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow006;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_006_2;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow007;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_007_1;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow008;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_008_1;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow009;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_009_1;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_009_2;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_009_3;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow010;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_010_1;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_010_2;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_010_3;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow011;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_011_1;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_011_2;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_011_3;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow012;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_012_1;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_012_2;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_012_3;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow013;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_013_1;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_013_2;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_013_3;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow014;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_014_1;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow015;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_015_1;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_015_2;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_015_3;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow016;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_016_1;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_016_2;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_016_3;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow017;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_017_1;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_017_2;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_017_3;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow018;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_018_1;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_018_2;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_018_3;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow019;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_019_1;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_019_2;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_019_3;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow020;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_020_1;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_020_2;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_020_3;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow021;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_021_2;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_021_3;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow022;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_022_2;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow023;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_023_1;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow024;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_024_1;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_024_2;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_024_3;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow025;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_025_1;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_025_2;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_025_3;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow026;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_026_1;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_026_2;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_026_3;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow027;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_027_1;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_027_2;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_027_3;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow028;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_028_1;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_028_2;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_028_3;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow029;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_029_2;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_029_3;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow030;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_030_1;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow031;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_031_1;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow032;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_032_1;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_032_2;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_032_3;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow033;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_033_1;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_033_2;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_033_3;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow034;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_034_1;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_034_2;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_034_3;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow035;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_035_2;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_035_3;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow036;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_036_1;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow037;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_037_1;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow038;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_038_1;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_038_2;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_038_3;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow039;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_039_1;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_039_2;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_039_3;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow040;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_040_2;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_040_3;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow041;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_041_1;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow043;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_043_1;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow044;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_044_1;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_044_2;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell_044_3;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow045;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell125;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell126;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell127;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow046;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell122;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell123;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell124;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow047;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell119;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell120;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell121;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow048;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow049;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell113;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow050;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell111;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow051;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell103;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell104;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell105;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow052;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow053;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow054;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow055;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell153;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell154;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell155;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow056;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell168;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell169;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell170;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow057;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell150;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell151;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell152;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow058;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell180;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell181;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell182;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow059;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell186;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell187;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell188;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow060;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell183;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell184;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell185;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow061;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell165;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell166;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell167;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow062;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell178;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell179;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow063;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell175;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell176;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow064;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell171;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow065;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell162;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow066;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell159;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell160;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell161;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow067;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell156;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell157;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell158;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow068;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell138;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell139;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell149;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow069;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell217;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell218;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell219;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow070;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell214;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell215;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell216;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow071;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell211;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell212;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell213;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow075;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell199;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell200;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell201;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow076;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell196;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell197;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell198;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow077;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell227;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell228;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell229;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow078;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell233;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell234;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell235;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow079;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell230;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell231;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell232;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow080;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell190;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell191;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell192;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow081;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell240;
        internal DevExpress.XtraReports.UI.XRTableCell xrTableCell241;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_002_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_003_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_004_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_005_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_009_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_010_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_011_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_012_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_013_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_015_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_016_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_017_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_018_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_019_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_020_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_021_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_024_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_025_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_026_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_027_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_028_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_029_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_032_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_033_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_034_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_035_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_038_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_039_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_040_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_044_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell383;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell384;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell385;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell386;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell387;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell388;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell389;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell392;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell393;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell394;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell395;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell396;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell397;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell398;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell399;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell400;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell403;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell404;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell405;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell406;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell407;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell408;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell412;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell413;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell414;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell415;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell416;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell417;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell418;
        internal DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow024B;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_024B_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_024B_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_024B_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_024B_4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_039B_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_039B_2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_039B_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_039B_4;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;

    }
}
