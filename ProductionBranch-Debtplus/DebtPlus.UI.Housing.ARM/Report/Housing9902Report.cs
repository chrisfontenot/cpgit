using System;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Reflection;
using DebtPlus.Reports;
using DevExpress.XtraReports.UI;
using DebtPlus.LINQ;
using DebtPlus.UI.Housing.ARM.ARM;
using DebtPlus.UI.Housing.ARM.HUD;

namespace DebtPlus.UI.Housing.ARM.Report
{
    public partial class HousingReport : DebtPlus.Reports.Template.DatedTemplateXtraReportClass
    {
        // Agency configuration information
        private Configuration cnf = null;
        private System.Collections.Generic.List<DebtPlus.LINQ.xpr_housing_arm_v4_summaryResult> colSummary = null;
        private bool sendAll = false;

        public HousingReport()
            : base()
        {
            InitializeComponent();

            // Correct the list of items since they seem to be duplicated a great deal of the time.
            StyleSheet.Clear();
            StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] { XrControlStyle_Header, XrControlStyle_Totals, XrControlStyle_HeaderPannel, XrControlStyle_GroupHeader });

            this.BeforePrint += HousingReport_BeforePrint;
            this.xrSubreport1.BeforePrint += new PrintEventHandler(xrSubreport1_BeforePrint);
        }

        private void xrSubreport1_BeforePrint(object sender, PrintEventArgs e)
        {
            // Obtain the current agency from the report
            var CurrentAgency = GetCurrentRow() as HUD.AgencyProfileData;

            // Read the 9902 summary data from the agency
            xpr_housing_arm_summary(CurrentAgency);

            // Map the 9902 form to a list so that it may be processed by the sub-report
            var rptLst = new System.Collections.Generic.List<HUD.form_9902>();
            rptLst.Add(CurrentAgency.Form9902);

            var rpt = xrSubreport1.ReportSource as DevExpress.XtraReports.UI.XtraReportBase;
            rpt.DataSource = rptLst;
        }

        public override String ReportTitle
        {
            get
            {
                return "Housing 9902 Information";
            }
        }

        public override System.Windows.Forms.DialogResult RequestReportParameters()
        {
            System.Windows.Forms.DialogResult answer;

            if (cnf == null)
            {
                cnf = new DebtPlus.UI.Housing.ARM.Configuration();
            }

            using (var frm = new DebtPlus.UI.Housing.ARM.ParametersForm(cnf))
            {
                answer = frm.ShowDialog();

                sendAll = frm.CheckEdit_send_all.Checked;
                cnf.PeriodStart = frm.Parameter_FromDate;
                cnf.PeriodEnd = frm.Parameter_ToDate;
                cnf.itemsToSend = frm.Parameter_HCS_ID;

                Parameter_FromDate = cnf.PeriodStart;
                Parameter_ToDate = cnf.PeriodEnd;
            }

            return answer;
        }

        /// <summary>
        /// Construct the 9902 summary data from the client selection data
        /// </summary>
        private void xpr_housing_arm_summary(HUD.AgencyProfileData CurrentAgency)
        {
            foreach (var item in colSummary.Where(s => s.hcs_id == CurrentAgency.agc_hcs_id))
            {
                string FieldName = item.tag;
                System.Reflection.PropertyInfo pi = typeof(HUD.form_9902).GetProperty(FieldName);
                if (pi == null)
                {
                    System.Diagnostics.Debug.WriteLine(FieldName);
                }
                else
                {
                    var v = new HUD.Field_9902(item.all_count, item.hud_count);
                    pi.SetValue(CurrentAgency.Form9902, v, null);
                }
            }
        }

        private void HousingReport_BeforePrint(object sender, PrintEventArgs e)
        {
            if (cnf == null)
            {
                cnf = new DebtPlus.UI.Housing.ARM.Configuration();
            }

            try
            {
                using (var cn = new System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.BusinessContext.ConnectionString()))
                {
                    cn.Open();
                    using (var bc = new DebtPlus.LINQ.BusinessContext(cn))
                    {
                        bc.CommandTimeout = 600;        // increase the command timeout since this may take a while.
                        bc.xpr_housing_arm_v4_client_select(cnf.PeriodStart, cnf.PeriodEnd);
                        colSummary = bc.xpr_housing_arm_v4_summary();
                        bc.xpr_housing_arm_v4_cleanup();
                    }
                }

                AgencyProfileData cls = new AgencyProfileData(cnf);
                var lst = new AgencyList();
                lst.GetAgencyProfiles(cnf);

                // If we are sending all items then add the grand total to the list
                if (sendAll)
                {
                    // Add the "fake" item to generate the total information
                    var agency = new HUD.AgencyProfileData()
                    {
                        agc_hcs_id = 0,  // "magic" value for the total information.
                        Description = "Grand Totals"
                    };

                    lst.AgencyListItems.Add(agency);
                    DataSource = lst.AgencyListItems;
                }
                else
                {
                    // Otherwise, filter the list by the checked items
                    DataSource = (from l in lst.AgencyListItems where cnf.itemsToSend.Contains(l.agc_hcs_id) select l).ToList();
                }
            }

            catch (Exception ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading transactions");
            }
        }
    }
}