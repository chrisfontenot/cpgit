﻿namespace DebtPlus.UI.Housing.ARM
{
    partial class StatusForm
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components != null)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.GridColumn_hcs_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_hcs_id.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn3.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.Bar2 = new DevExpress.XtraBars.Bar();
            this.BarSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.BarButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.barManager1).BeginInit();
            this.SuspendLayout();
            //
            //GridControl1
            //
            this.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridControl1.Location = new System.Drawing.Point(0, 24);
            this.GridControl1.MainView = this.GridView1;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.Size = new System.Drawing.Size(502, 242);
            this.GridControl1.TabIndex = 0;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { this.GridView1 });
            //
            //GridView1
            //
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] { this.GridColumn_hcs_id, this.GridColumn1, this.GridColumn2, this.GridColumn3 });
            this.GridView1.GridControl = this.GridControl1;
            this.GridView1.Name = "GridView1";
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsBehavior.ReadOnly = true;
            this.GridView1.OptionsView.ShowDetailButtons = false;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            this.GridView1.OptionsView.ShowPreviewRowLines = DevExpress.Utils.DefaultBoolean.True;
            //
            //GridColumn_hcs_id
            //
            this.GridColumn_hcs_id.Caption = "ID";
            this.GridColumn_hcs_id.CustomizationCaption = "ID Number";
            this.GridColumn_hcs_id.DisplayFormat.FormatString = "f0";
            this.GridColumn_hcs_id.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_hcs_id.FieldName = "agc_hcs_id";
            this.GridColumn_hcs_id.GroupFormat.FormatString = "f0";
            this.GridColumn_hcs_id.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_hcs_id.Name = "GridColumn_hcs_id";
            this.GridColumn_hcs_id.Visible = true;
            this.GridColumn_hcs_id.VisibleIndex = 0;
            this.GridColumn_hcs_id.Width = 54;
            //
            //GridColumn1
            //
            this.GridColumn1.Caption = "Date";
            this.GridColumn1.CustomizationCaption = "Submission Date";
            this.GridColumn1.FieldName = "SubmissionDate";
            this.GridColumn1.Name = "GridColumn1";
            this.GridColumn1.Visible = true;
            this.GridColumn1.VisibleIndex = 1;
            //
            //GridColumn2
            //
            this.GridColumn2.Caption = "Submission";
            this.GridColumn2.CustomizationCaption = "Resulting submission ID";
            this.GridColumn2.FieldName = "SubmissionID";
            this.GridColumn2.Name = "GridColumn2";
            this.GridColumn2.Visible = true;
            this.GridColumn2.VisibleIndex = 2;
            this.GridColumn2.Width = 86;
            //
            //GridColumn3
            //
            this.GridColumn3.Caption = "Status";
            this.GridColumn3.CustomizationCaption = "Submission Status";
            this.GridColumn3.FieldName = "SubmissionStatus";
            this.GridColumn3.Name = "GridColumn3";
            this.GridColumn3.Visible = true;
            this.GridColumn3.VisibleIndex = 3;
            this.GridColumn3.Width = 283;
            //
            //barManager1
            //
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] { this.Bar2 });
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] { this.BarSubItem1, this.BarButtonItem1, this.BarButtonItem2 });
            this.barManager1.MainMenu = this.Bar2;
            this.barManager1.MaxItemId = 3;
            //
            //barDockControlTop
            //
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(502, 24);
            //
            //barDockControlBottom
            //
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 266);
            this.barDockControlBottom.Size = new System.Drawing.Size(502, 0);
            //
            //barDockControlLeft
            //
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 24);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 242);
            //
            //barDockControlRight
            //
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(502, 24);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 242);
            //
            //Bar2
            //
            this.Bar2.BarName = "Main menu";
            this.Bar2.DockCol = 0;
            this.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.Bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] { new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem1) });
            this.Bar2.OptionsBar.MultiLine = true;
            this.Bar2.OptionsBar.UseWholeRow = true;
            this.Bar2.Text = "Main menu";
            //
            //BarSubItem1
            //
            this.BarSubItem1.Caption = "&File";
            this.BarSubItem1.Id = 0;
            this.BarSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] { new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem1, true), new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem2, true) });
            this.BarSubItem1.Name = "BarSubItem1";
            //
            //BarButtonItem1
            //
            this.BarButtonItem1.Caption = "&Print Preview...";
            this.BarButtonItem1.Id = 1;
            this.BarButtonItem1.Name = "BarButtonItem1";
            //
            //BarButtonItem2
            //
            this.BarButtonItem2.Caption = "&Exit";
            this.BarButtonItem2.Id = 2;
            this.BarButtonItem2.Name = "BarButtonItem2";
            //
            //StatusForm
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6.0F, 13.0F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(502, 266);
            this.Controls.Add(this.GridControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "StatusForm";
            this.Text = "Submission Status";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.barManager1).EndInit();
            this.ResumeLayout(false);
        }

        internal DevExpress.XtraGrid.GridControl GridControl1;
        internal DevExpress.XtraGrid.Views.Grid.GridView GridView1;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_hcs_id;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn1;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn2;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn3;
        internal DevExpress.XtraBars.BarManager barManager1;
        internal DevExpress.XtraBars.Bar Bar2;
        internal DevExpress.XtraBars.BarSubItem BarSubItem1;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem1;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem2;
        internal DevExpress.XtraBars.BarDockControl barDockControlTop;
        internal DevExpress.XtraBars.BarDockControl barDockControlBottom;
        internal DevExpress.XtraBars.BarDockControl barDockControlLeft;
        internal DevExpress.XtraBars.BarDockControl barDockControlRight;
    }
}
