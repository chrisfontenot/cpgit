#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;

namespace DebtPlus.UI.Housing.ARM.HUD
{
    partial class AgencyProfileData : ExtractBase
    {
        // These fields are serialized in their own submission. They are agency specific locations.
        public form_9902 Form9902 = new form_9902();

        public System.Collections.Generic.List<counselor_profile> CounselorProfiles = new System.Collections.Generic.List<counselor_profile>();
        public System.Collections.Generic.List<client_profile> ClientList = new System.Collections.Generic.List<client_profile>();
        public System.Collections.Generic.List<CounselorTraining> CounselorTrainings = new System.Collections.Generic.List<CounselorTraining>();
        public System.Collections.Generic.List<CounselorTrainingCourse> CounselorTrainingCourses = new System.Collections.Generic.List<CounselorTrainingCourse>();
        public System.Collections.Generic.List<group_session> GroupSessions = new System.Collections.Generic.List<group_session>();
        public System.Collections.Generic.List<attendee> Attendees = new System.Collections.Generic.List<attendee>();

        public event MessageUpdate UpdateSubmittedDate;
        protected void Raise_UpdateSubmittedDate(EventMessageArgs e)
        {
            var evt = UpdateSubmittedDate;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        public void SubmissionDateChanged(DateTime ItemDate)
        {
            Raise_UpdateSubmittedDate(new EventMessageArgs(ItemDate));
        }

        public event MessageUpdate UpdateSubmissionID;
        protected void Raise_UpdateSubmissionID(EventMessageArgs e)
        {
            var evt = UpdateSubmissionID;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        public void SubmissionIDChanged(Int64 ID)
        {
            Raise_UpdateSubmissionID(new EventMessageArgs(ID.ToString()));
        }

        public event MessageUpdate UpdateSubmissionStatus;
        protected void Raise_UpdateSubmissionStatus(EventMessageArgs e)
        {
            var evt = UpdateSubmissionStatus;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        public void SubmissionStatusChanged(string Status)
        {
            Raise_UpdateSubmissionStatus(new EventMessageArgs(Status));
        }

        private readonly System.Collections.Generic.List<Language> privateagency_languages = new System.Collections.Generic.List<Language>();
        public System.Collections.Generic.List<Language> Agency_Languages
        {
            get
            {
                return privateagency_languages;
            }
        }

        private readonly System.Collections.Generic.List<Counseling_Method> privateagency_counseling_methods = new System.Collections.Generic.List<Counseling_Method>();
        public System.Collections.Generic.List<Counseling_Method> Agency_Counseling_Methods
        {
            get
            {
                return privateagency_counseling_methods;
            }
        }

        public override void SerializeData(AgencyProfileData CurrentAgency, System.Xml.XmlWriter xml)
        {
            xml.WriteStartElement("tns:AgencyProfileData");
            xml.WriteElementString("tns:Agency_EIN", FmtEIN(agc_ein));
            xml.WriteElementString("tns:Reported_Month", FmtInt32(reported_month));
            xml.WriteElementString("tns:Agency_DUN_Nbr", FmtDBN(agc_dun_nbr));
            xml.WriteElementString("tns:Agency_Physical_Address1", FmtString(agc_physical_address1));
            xml.WriteElementString("tns:Agency_Physical_Address2", FmtString(agc_physical_address2));
            xml.WriteElementString("tns:Agency_Physical_Address3", FmtString(agc_physical_address3));
            xml.WriteElementString("tns:Agency_Physical_Address4", FmtString(agc_physical_address4));
            xml.WriteElementString("tns:Agency_Physical_City", FmtString(agc_physical_city));
            xml.WriteElementString("tns:Agency_Physical_State", FmtInt32(agc_physical_state));
            xml.WriteElementString("tns:Agency_Physical_Zip", FmtZip(agc_physical_zip));
            xml.WriteElementString("tns:Agency_Mailing_Address1", FmtString(agc_mailing_address1));
            xml.WriteElementString("tns:Agency_Mailing_Address2", FmtString(agc_mailing_address2));
            xml.WriteElementString("tns:Agency_Mailing_Address3", FmtString(agc_mailing_address3));
            xml.WriteElementString("tns:Agency_Mailing_Address4", FmtString(agc_mailing_address4));
            xml.WriteElementString("tns:Agency_Mailing_City", FmtString(agc_mailing_city));
            xml.WriteElementString("tns:Agency_Mailing_State", FmtInt32(agc_mailing_state));
            xml.WriteElementString("tns:Agency_Mailing_Zip", FmtZip(agc_mailing_zip));
            xml.WriteElementString("tns:Agency_Website", FmtString(agc_web_site));
            xml.WriteElementString("tns:Agency_Phone_Nbr", FmtTelephoneNumber(agc_phone_nbr));
            xml.WriteElementString("tns:Agency_Tollfree_Phone_Nbr", FmtTelephoneNumber(agc_tollfree_phone_nbr));
            xml.WriteElementString("tns:Agency_Fax_Nbr", FmtTelephoneNumber(agc_fax_nbr));
            xml.WriteElementString("tns:Agency_Email", FmtString(agc_email));
            xml.WriteElementString("tns:Agency_Faith_Based_Ind", FmtYN(agc_faith_based_ind));
            xml.WriteElementString("tns:Agency_Colonias_Ind", FmtYN(agc_colonias_ind));
            xml.WriteElementString("tns:Agency_Migrfarm_Worker_Ind", FmtYN(agc_migrfarm_worker_ind));
            xml.WriteElementString("tns:Agency_Counseling_Budget_Amount", FmtDecimal(agc_counseling_budget_amount));

            if (Agency_Languages.Count > 0)
            {
                xml.WriteStartElement("tns:Agency_Languages");
                foreach (Language item in Agency_Languages)
                {
                    item.SerializeData(CurrentAgency, xml);
                }
                xml.WriteEndElement();
            }

            // Agency counseling methods
            if (Agency_Counseling_Methods.Count > 0)
            {
                xml.WriteStartElement("tns:Agency_Counseling_Methods");
                foreach (Counseling_Method item in Agency_Counseling_Methods)
                {
                    item.SerializeData(CurrentAgency, xml);
                }
                xml.WriteEndElement();
            }

            xml.WriteEndElement();
        }
    }
}
