#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;

namespace DebtPlus.UI.Housing.ARM.HUD
{
    public class form_9902 : ExtractBase
    {
        #region storage
        private Int32 private_report_period_id = 0;
        public Int32 report_period_id
        {
            get
            {
                return private_report_period_id;
            }
            set
            {
                DoSetProperty("report_period_id", ref private_report_period_id, value);
            }
        }

        private Field_9902 private_Ethnicity_Clients_Counseling_Hispanic = new Field_9902();
        public Field_9902 Ethnicity_Clients_Counseling_Hispanic
        {
            get
            {
                return private_Ethnicity_Clients_Counseling_Hispanic;
            }
            set
            {
                DoSetProperty("Ethnicity_Clients_Counseling_Hispanic", ref private_Ethnicity_Clients_Counseling_Hispanic, value);
            }
        }

        private Field_9902 private_Ethnicity_Clients_Counseling_Non_Hispanic = new Field_9902();
        public Field_9902 Ethnicity_Clients_Counseling_Non_Hispanic
        {
            get
            {
                return private_Ethnicity_Clients_Counseling_Non_Hispanic;
            }
            set
            {
                DoSetProperty("Ethnicity_Clients_Counseling_Non_Hispanic", ref private_Ethnicity_Clients_Counseling_Non_Hispanic, value);
            }
        }

        private Field_9902 private_Ethnicity_Clients_Counseling_No_Response = new Field_9902();
        public Field_9902 Ethnicity_Clients_Counseling_No_Response
        {
            get
            {
                return private_Ethnicity_Clients_Counseling_No_Response;
            }
            set
            {
                DoSetProperty("Ethnicity_Clients_Counseling_No_Response", ref private_Ethnicity_Clients_Counseling_No_Response, value);
            }
        }

        private Field_9902 private_Race_Clients_Counseling_American_Indian_Alaskan_Native = new Field_9902();
        public Field_9902 Race_Clients_Counseling_American_Indian_Alaskan_Native
        {
            get
            {
                return private_Race_Clients_Counseling_American_Indian_Alaskan_Native;
            }
            set
            {
                DoSetProperty("Race_Clients_Counseling_American_Indian_Alaskan_Native", ref private_Race_Clients_Counseling_American_Indian_Alaskan_Native, value);
            }
        }

        private Field_9902 private_Race_Clients_Counseling_Asian = new Field_9902();
        public Field_9902 Race_Clients_Counseling_Asian
        {
            get
            {
                return private_Race_Clients_Counseling_Asian;
            }
            set
            {
                DoSetProperty("Race_Clients_Counseling_Asian", ref private_Race_Clients_Counseling_Asian, value);
            }
        }

        private Field_9902 private_Race_Clients_Counseling_Black_AfricanAmerican = new Field_9902();
        public Field_9902 Race_Clients_Counseling_Black_AfricanAmerican
        {
            get
            {
                return private_Race_Clients_Counseling_Black_AfricanAmerican;
            }
            set
            {
                DoSetProperty("Race_Clients_Counseling_Black_AfricanAmerican", ref private_Race_Clients_Counseling_Black_AfricanAmerican, value);
            }
        }

        private Field_9902 private_Race_Clients_Counseling_Pacific_Islanders = new Field_9902();
        public Field_9902 Race_Clients_Counseling_Pacific_Islanders
        {
            get
            {
                return private_Race_Clients_Counseling_Pacific_Islanders;
            }
            set
            {
                DoSetProperty("Race_Clients_Counseling_Pacific_Islanders", ref private_Race_Clients_Counseling_Pacific_Islanders, value);
            }
        }

        private Field_9902 private_Race_Clients_Counseling_White = new Field_9902();
        public Field_9902 Race_Clients_Counseling_White
        {
            get
            {
                return private_Race_Clients_Counseling_White;
            }
            set
            {
                DoSetProperty("Race_Clients_Counseling_White", ref private_Race_Clients_Counseling_White, value);
            }
        }

        private Field_9902 private_MultiRace_Clients_Counseling_AMINDWHT = new Field_9902();
        public Field_9902 MultiRace_Clients_Counseling_AMINDWHT
        {
            get
            {
                return private_MultiRace_Clients_Counseling_AMINDWHT;
            }
            set
            {
                DoSetProperty("MultiRace_Clients_Counseling_AMINDWHT", ref private_MultiRace_Clients_Counseling_AMINDWHT, value);
            }
        }

        private Field_9902 private_MultiRace_Clients_Counseling_ASIANWHT = new Field_9902();
        public Field_9902 MultiRace_Clients_Counseling_ASIANWHT
        {
            get
            {
                return private_MultiRace_Clients_Counseling_ASIANWHT;
            }
            set
            {
                DoSetProperty("MultiRace_Clients_Counseling_ASIANWHT", ref private_MultiRace_Clients_Counseling_ASIANWHT, value);
            }
        }

        private Field_9902 private_MultiRace_Clients_Counseling_BLKWHT = new Field_9902();
        public Field_9902 MultiRace_Clients_Counseling_BLKWHT
        {
            get
            {
                return private_MultiRace_Clients_Counseling_BLKWHT;
            }
            set
            {
                DoSetProperty("MultiRace_Clients_Counseling_BLKWHT", ref private_MultiRace_Clients_Counseling_BLKWHT, value);
            }
        }

        private Field_9902 private_MultiRace_Clients_Counseling_AMRCINDBLK = new Field_9902();
        public Field_9902 MultiRace_Clients_Counseling_AMRCINDBLK
        {
            get
            {
                return private_MultiRace_Clients_Counseling_AMRCINDBLK;
            }
            set
            {
                DoSetProperty("MultiRace_Clients_Counseling_AMRCINDBLK", ref private_MultiRace_Clients_Counseling_AMRCINDBLK, value);
            }
        }

        private Field_9902 private_MultiRace_Clients_Counseling_OtherMLTRC = new Field_9902();
        public Field_9902 MultiRace_Clients_Counseling_OtherMLTRC
        {
            get
            {
                return private_MultiRace_Clients_Counseling_OtherMLTRC;
            }
            set
            {
                DoSetProperty("MultiRace_Clients_Counseling_OtherMLTRC", ref private_MultiRace_Clients_Counseling_OtherMLTRC, value);
            }
        }

        private Field_9902 private_MultiRace_Clients_Counseling_NoResponse = new Field_9902();
        public Field_9902 MultiRace_Clients_Counseling_NoResponse
        {
            get
            {
                return private_MultiRace_Clients_Counseling_NoResponse;
            }
            set
            {
                DoSetProperty("MultiRace_Clients_Counseling_NoResponse", ref private_MultiRace_Clients_Counseling_NoResponse, value);
            }
        }

        private Field_9902 private_Less30_AMI_Level = new Field_9902();
        public Field_9902 Less30_AMI_Level
        {
            get
            {
                return private_Less30_AMI_Level;
            }
            set
            {
                DoSetProperty("Less30_AMI_Level", ref private_Less30_AMI_Level, value);
            }
        }

        private Field_9902 private_a30_49_AMI_Level = new Field_9902();
        public Field_9902 a30_49_AMI_Level
        {
            get
            {
                return private_a30_49_AMI_Level;
            }
            set
            {
                DoSetProperty("a30_49_AMI_Level", ref private_a30_49_AMI_Level, value);
            }
        }

        private Field_9902 private_a50_79_AMI_Level = new Field_9902();
        public Field_9902 a50_79_AMI_Level
        {
            get
            {
                return private_a50_79_AMI_Level;
            }
            set
            {
                DoSetProperty("a50_79_AMI_Level", ref private_a50_79_AMI_Level, value);
            }
        }

        private Field_9902 private_a80_100_AMI_Level = new Field_9902();
        public Field_9902 a80_100_AMI_Level
        {
            get
            {
                return private_a80_100_AMI_Level;
            }
            set
            {
                DoSetProperty("a80_100_AMI_Level", ref private_a80_100_AMI_Level, value);
            }
        }

        private Field_9902 private_Greater100_AMI_Level = new Field_9902();
        public Field_9902 Greater100_AMI_Level
        {
            get
            {
                return private_Greater100_AMI_Level;
            }
            set
            {
                DoSetProperty("Greater100_AMI_Level", ref private_Greater100_AMI_Level, value);
            }
        }

        private Field_9902 private_AMI_No_Response = new Field_9902();
        public Field_9902 AMI_No_Response
        {
            get
            {
                return private_AMI_No_Response;
            }
            set
            {
                DoSetProperty("AMI_No_Response", ref private_AMI_No_Response, value);
            }
        }

        private Field_9902 private_Household_Lives_In_Rural_Area = new Field_9902();
        public Field_9902 Household_Lives_In_Rural_Area
        {
            get
            {
                return private_Household_Lives_In_Rural_Area;
            }
            set
            {
                DoSetProperty("Household_Lives_In_Rural_Area", ref private_Household_Lives_In_Rural_Area, value);
            }
        }

        private Field_9902 private_Household_Does_Not_Live_In_Rural_Area = new Field_9902();
        public Field_9902 Household_Does_Not_Live_In_Rural_Area
        {
            get
            {
                return private_Household_Does_Not_Live_In_Rural_Area;
            }
            set
            {
                DoSetProperty("Household_Does_Not_Live_In_Rural_Area", ref private_Household_Does_Not_Live_In_Rural_Area, value);
            }
        }

        private Field_9902 private_Rural_Area_No_Response = new Field_9902();
        public Field_9902 Rural_Area_No_Response
        {
            get
            {
                return private_Rural_Area_No_Response;
            }
            set
            {
                DoSetProperty("Rural_Area_No_Response", ref private_Rural_Area_No_Response, value);
            }
        }

        private Field_9902 private_Household_Is_Limited_English_Proficient = new Field_9902();
        public Field_9902 Household_Is_Limited_English_Proficient
        {
            get
            {
                return private_Household_Is_Limited_English_Proficient;
            }
            set
            {
                DoSetProperty("Household_Is_Limited_English_Proficient", ref private_Household_Is_Limited_English_Proficient, value);
            }
        }

        private Field_9902 private_Household_Is_Not_Limited_English_Proficient = new Field_9902();
        public Field_9902 Household_Is_Not_Limited_English_Proficient
        {
            get
            {
                return private_Household_Is_Not_Limited_English_Proficient;
            }
            set
            {
                DoSetProperty("Household_Is_Not_Limited_English_Proficient", ref private_Household_Is_Not_Limited_English_Proficient, value);
            }
        }

        private Field_9902 private_Limited_English_Proficient_No_Response = new Field_9902();
        public Field_9902 Limited_English_Proficient_No_Response
        {
            get
            {
                return private_Limited_English_Proficient_No_Response;
            }
            set
            {
                DoSetProperty("Limited_English_Proficient_No_Response", ref private_Limited_English_Proficient_No_Response, value);
            }
        }

        private Field_9902 private_Compl_HomeBuyer_Educ_Workshop = new Field_9902();
        public Field_9902 Compl_HomeBuyer_Educ_Workshop
        {
            get
            {
                return private_Compl_HomeBuyer_Educ_Workshop;
            }
            set
            {
                DoSetProperty("Compl_HomeBuyer_Educ_Workshop", ref private_Compl_HomeBuyer_Educ_Workshop, value);
            }
        }

        private Field_9902 private_Compl_Workshop_HomeFin_Credit_Repair = new Field_9902();
        public Field_9902 Compl_Workshop_HomeFin_Credit_Repair
        {
            get
            {
                return private_Compl_Workshop_HomeFin_Credit_Repair;
            }
            set
            {
                DoSetProperty("Compl_Workshop_HomeFin_Credit_Repair", ref private_Compl_Workshop_HomeFin_Credit_Repair, value);
            }
        }

        private Field_9902 private_Compl_Resolv_Prevent_Mortg_Deliq = new Field_9902();
        public Field_9902 Compl_Resolv_Prevent_Mortg_Deliq
        {
            get
            {
                return private_Compl_Resolv_Prevent_Mortg_Deliq;
            }
            set
            {
                DoSetProperty("Compl_Resolv_Prevent_Mortg_Deliq", ref private_Compl_Resolv_Prevent_Mortg_Deliq, value);
            }
        }

        private Field_9902 private_Compl_HomeMaint_FinMngt = new Field_9902();
        public Field_9902 Compl_HomeMaint_FinMngt
        {
            get
            {
                return private_Compl_HomeMaint_FinMngt;
            }
            set
            {
                DoSetProperty("Compl_HomeMaint_FinMngt", ref private_Compl_HomeMaint_FinMngt, value);
            }
        }

        private Field_9902 private_Compl_Help_FairHousing_Workshop = new Field_9902();
        public Field_9902 Compl_Help_FairHousing_Workshop
        {
            get
            {
                return private_Compl_Help_FairHousing_Workshop;
            }
            set
            {
                DoSetProperty("Compl_Help_FairHousing_Workshop", ref private_Compl_Help_FairHousing_Workshop, value);
            }
        }

        private Field_9902 private_Compl_Workshop_Predatory_Lend = new Field_9902();
        public Field_9902 Compl_Workshop_Predatory_Lend
        {
            get
            {
                return private_Compl_Workshop_Predatory_Lend;
            }
            set
            {
                DoSetProperty("Compl_Workshop_Predatory_Lend", ref private_Compl_Workshop_Predatory_Lend, value);
            }
        }

        private Field_9902 private_Compl_NonDelinqency_PostPurchase_Workshop = new Field_9902();
        public Field_9902 Compl_NonDelinqency_PostPurchase_Workshop
        {
            get
            {
                return private_Compl_NonDelinqency_PostPurchase_Workshop;
            }
            set
            {
                DoSetProperty("Compl_NonDelinqency_PostPurchase_Workshop", ref private_Compl_NonDelinqency_PostPurchase_Workshop, value);
            }
        }

        private Field_9902 private_Counseling_Rental_Workshop = new Field_9902();
        public Field_9902 Counseling_Rental_Workshop
        {
            get
            {
                return private_Counseling_Rental_Workshop;
            }
            set
            {
                DoSetProperty("Counseling_Rental_Workshop", ref private_Counseling_Rental_Workshop, value);
            }
        }

        private Field_9902 private_Compl_Other_Workshop = new Field_9902();
        public Field_9902 Compl_Other_Workshop
        {
            get
            {
                return private_Compl_Other_Workshop;
            }
            set
            {
                DoSetProperty("Compl_Other_Workshop", ref private_Compl_Other_Workshop, value);
            }
        }

        private Field_9902 private_One_Homeless_Assistance_Counseling = new Field_9902();
        public Field_9902 One_Homeless_Assistance_Counseling
        {
            get
            {
                return private_One_Homeless_Assistance_Counseling;
            }
            set
            {
                DoSetProperty("One_Homeless_Assistance_Counseling", ref private_One_Homeless_Assistance_Counseling, value);
            }
        }

        private Field_9902 private_One_Rental_Topics_Counseling = new Field_9902();
        public Field_9902 One_Rental_Topics_Counseling
        {
            get
            {
                return private_One_Rental_Topics_Counseling;
            }
            set
            {
                DoSetProperty("One_Rental_Topics_Counseling", ref private_One_Rental_Topics_Counseling, value);
            }
        }

        private Field_9902 private_One_PrePurchase_HomeBuying_Counseling = new Field_9902();
        public Field_9902 One_PrePurchase_HomeBuying_Counseling
        {
            get
            {
                return private_One_PrePurchase_HomeBuying_Counseling;
            }
            set
            {
                DoSetProperty("One_PrePurchase_HomeBuying_Counseling", ref private_One_PrePurchase_HomeBuying_Counseling, value);
            }
        }

        private Field_9902 private_One_Home_Maintenance_Fin_Management_Counseling = new Field_9902();
        public Field_9902 One_Home_Maintenance_Fin_Management_Counseling
        {
            get
            {
                return private_One_Home_Maintenance_Fin_Management_Counseling;
            }
            set
            {
                DoSetProperty("One_Home_Maintenance_Fin_Management_Counseling", ref private_One_Home_Maintenance_Fin_Management_Counseling, value);
            }
        }

        private Field_9902 private_One_Reverse_Mortgage_Counseling = new Field_9902();
        public Field_9902 One_Reverse_Mortgage_Counseling
        {
            get
            {
                return private_One_Reverse_Mortgage_Counseling;
            }
            set
            {
                DoSetProperty("One_Reverse_Mortgage_Counseling", ref private_One_Reverse_Mortgage_Counseling, value);
            }
        }

        private Field_9902 private_One_Resolv_Prevent_Mortg_Delinq_Counseling = new Field_9902();
        public Field_9902 One_Resolv_Prevent_Mortg_Delinq_Counseling
        {
            get
            {
                return private_One_Resolv_Prevent_Mortg_Delinq_Counseling;
            }
            set
            {
                DoSetProperty("One_Resolv_Prevent_Mortg_Delinq_Counseling", ref private_One_Resolv_Prevent_Mortg_Delinq_Counseling, value);
            }
        }

        private Field_9902 private_Impact_One_On_One_And_Group = new Field_9902();
        public Field_9902 Impact_One_On_One_And_Group
        {
            get
            {
                return private_Impact_One_On_One_And_Group;
            }
            set
            {
                DoSetProperty("Impact_One_On_One_And_Group", ref private_Impact_One_On_One_And_Group, value);
            }
        }

        private Field_9902 private_Impact_Received_Info_Fair_Housing = new Field_9902();
        public Field_9902 Impact_Received_Info_Fair_Housing
        {
            get
            {
                return private_Impact_Received_Info_Fair_Housing;
            }
            set
            {
                DoSetProperty("Impact_Received_Info_Fair_Housing", ref private_Impact_Received_Info_Fair_Housing, value);
            }
        }

        private Field_9902 private_Impact_Developed_Sustainable_Budget = new Field_9902();
        public Field_9902 Impact_Developed_Sustainable_Budget
        {
            get
            {
                return private_Impact_Developed_Sustainable_Budget;
            }
            set
            {
                DoSetProperty("Impact_Developed_Sustainable_Budget", ref private_Impact_Developed_Sustainable_Budget, value);
            }
        }

        private Field_9902 private_Impact_Improved_Financial_Capacity = new Field_9902();
        public Field_9902 Impact_Improved_Financial_Capacity
        {
            get
            {
                return private_Impact_Improved_Financial_Capacity;
            }
            set
            {
                DoSetProperty("Impact_Improved_Financial_Capacity", ref private_Impact_Improved_Financial_Capacity, value);
            }
        }

        private Field_9902 private_Impact_Gained_Access_Resources_Improve_Housing = new Field_9902();
        public Field_9902 Impact_Gained_Access_Resources_Improve_Housing
        {
            get
            {
                return private_Impact_Gained_Access_Resources_Improve_Housing;
            }
            set
            {
                DoSetProperty("Impact_Gained_Access_Resources_Improve_Housing", ref private_Impact_Gained_Access_Resources_Improve_Housing, value);
            }
        }

        private Field_9902 private_Impact_Gained_Access_NonHousing_Resources = new Field_9902();
        public Field_9902 Impact_Gained_Access_NonHousing_Resources
        {
            get
            {
                return private_Impact_Gained_Access_NonHousing_Resources;
            }
            set
            {
                DoSetProperty("Impact_Gained_Access_NonHousing_Resources", ref private_Impact_Gained_Access_NonHousing_Resources, value);
            }
        }

        private Field_9902 private_Impact_Homeless_Obtained_Housing = new Field_9902();
        public Field_9902 Impact_Homeless_Obtained_Housing
        {
            get
            {
                return private_Impact_Homeless_Obtained_Housing;
            }
            set
            {
                DoSetProperty("Impact_Homeless_Obtained_Housing", ref private_Impact_Homeless_Obtained_Housing, value);
            }
        }

        private Field_9902 private_Impact_Received_Rental_Counseling_Avoided_Eviction = new Field_9902();
        public Field_9902 Impact_Received_Rental_Counseling_Avoided_Eviction
        {
            get
            {
                return private_Impact_Received_Rental_Counseling_Avoided_Eviction;
            }
            set
            {
                DoSetProperty("Impact_Received_Rental_Counseling_Avoided_Eviction", ref private_Impact_Received_Rental_Counseling_Avoided_Eviction, value);
            }
        }

        private Field_9902 private_Impact_Received_Rental_Counseling_Improved_Living_Conditions = new Field_9902();
        public Field_9902 Impact_Received_Rental_Counseling_Improved_Living_Conditions
        {
            get
            {
                return private_Impact_Received_Rental_Counseling_Improved_Living_Conditions;
            }
            set
            {
                DoSetProperty("Impact_Received_Rental_Counseling_Improved_Living_Conditions", ref private_Impact_Received_Rental_Counseling_Improved_Living_Conditions, value);
            }
        }
            
        private Field_9902 private_Impact_Received_PrePurchase_Counseling_Purchased_Housing = new Field_9902();
        public Field_9902 Impact_Received_PrePurchase_Counseling_Purchased_Housing
        {
            get
            {
                return private_Impact_Received_PrePurchase_Counseling_Purchased_Housing;
            }
            set
            {
                DoSetProperty("Impact_Received_PrePurchase_Counseling_Purchased_Housing", ref private_Impact_Received_PrePurchase_Counseling_Purchased_Housing, value);
            }
        }

        private Field_9902 private_Impact_Received_Reverse_Mortgage_Counseling_Obtained_HECM = new Field_9902();
        public Field_9902 Impact_Received_Reverse_Mortgage_Counseling_Obtained_HECM
        {
            get
            {
                return private_Impact_Received_Reverse_Mortgage_Counseling_Obtained_HECM;
            }
            set
            {
                DoSetProperty("Impact_Received_Reverse_Mortgage_Counseling_Obtained_HECM", ref private_Impact_Received_Reverse_Mortgage_Counseling_Obtained_HECM, value);
            }
        }

        private Field_9902 private_Impact_Received_NonDelinquency_PostPurchase_Counseling_Improve_Conditions_Affordability = new Field_9902();
        public Field_9902 Impact_Received_NonDelinquency_PostPurchase_Counseling_Improve_Conditions_Affordability
        {
            get
            {
                return private_Impact_Received_NonDelinquency_PostPurchase_Counseling_Improve_Conditions_Affordability;
            }
            set
            {
                DoSetProperty("Impact_Received_NonDelinquency_PostPurchase_Counseling_Improve_Conditions_Affordability", ref private_Impact_Received_NonDelinquency_PostPurchase_Counseling_Improve_Conditions_Affordability, value);
            }
        }

        private Field_9902 private_Impact_Prevented_Resolved_Mortgage_Default = new Field_9902();
        public Field_9902 Impact_Prevented_Resolved_Mortgage_Default
        {
            get
            {
                return private_Impact_Prevented_Resolved_Mortgage_Default;
            }
            set
            {
                DoSetProperty("Impact_Prevented_Resolved_Mortgage_Default", ref private_Impact_Prevented_Resolved_Mortgage_Default, value);
            }
        }

        #endregion

        public Field_9902 Section_3_Total
        {
            get
            {
                return 0
                        + Ethnicity_Clients_Counseling_Hispanic
                        + Ethnicity_Clients_Counseling_Non_Hispanic
                        + Ethnicity_Clients_Counseling_No_Response;
            }
            set
            {
            }
        }

        public Field_9902 Section_4_Total
        {
            get
            {
                return 0
                        + Race_Clients_Counseling_American_Indian_Alaskan_Native
                        + Race_Clients_Counseling_Asian
                        + Race_Clients_Counseling_Black_AfricanAmerican
                        + Race_Clients_Counseling_Pacific_Islanders
                        + Race_Clients_Counseling_White
                        + MultiRace_Clients_Counseling_AMINDWHT
                        + MultiRace_Clients_Counseling_ASIANWHT
                        + MultiRace_Clients_Counseling_BLKWHT
                        + MultiRace_Clients_Counseling_AMRCINDBLK
                        + MultiRace_Clients_Counseling_OtherMLTRC
                        + MultiRace_Clients_Counseling_NoResponse;
            }
            set
            {
            }
        }

        public Field_9902 Section_5_Total
        {
            get
            {
                return 0
                + Less30_AMI_Level
                + a30_49_AMI_Level
                + a50_79_AMI_Level
                + a80_100_AMI_Level
                + Greater100_AMI_Level
                + AMI_No_Response;
            }
            set
            {
            }
        }

        public Field_9902 Section_6_Total
        {
            get
            {
                return 0
                + Household_Lives_In_Rural_Area
                + Household_Does_Not_Live_In_Rural_Area
                + Rural_Area_No_Response;
            }
            set
            {
            }
        }

        public Field_9902 Section_7_Total
        {
            get
            {
                return 0
                + Household_Is_Limited_English_Proficient
                + Household_Is_Not_Limited_English_Proficient
                + Limited_English_Proficient_No_Response;
            }
            set
            {
            }
        }

        public Field_9902 Section_8_Total
        {
            get
            {
                return 0
                + Compl_HomeMaint_FinMngt
                + Compl_Workshop_Predatory_Lend
                + Compl_Help_FairHousing_Workshop
                + Compl_Resolv_Prevent_Mortg_Deliq
                + Counseling_Rental_Workshop
                + Compl_HomeBuyer_Educ_Workshop
                + Compl_NonDelinqency_PostPurchase_Workshop
                + Compl_Resolv_Prevent_Mortg_Deliq
                + Compl_Other_Workshop;
            }
            set
            {
            }
        }

        public Field_9902 Section_9_Total
        {
            get
            {
                return 0
                + One_Homeless_Assistance_Counseling
                + One_Rental_Topics_Counseling
                + One_PrePurchase_HomeBuying_Counseling
                + One_Home_Maintenance_Fin_Management_Counseling
                + One_Reverse_Mortgage_Counseling
                + One_Resolv_Prevent_Mortg_Delinq_Counseling;
            }
            set
            {
            }
        }

        public Field_9902 Section_8_and_9_Total
        {
            get
            {
                return 0
                + Section_8_Total
                + Section_9_Total;
            }
            set
            {
            }
        }

        public Field_9902 Section_10_Total
        {
            get
            {
                return 0
                + Impact_One_On_One_And_Group
                + Impact_Received_Info_Fair_Housing
                + Impact_Developed_Sustainable_Budget
                + Impact_Improved_Financial_Capacity
                + Impact_Gained_Access_Resources_Improve_Housing
                + Impact_Gained_Access_NonHousing_Resources
                + Impact_Homeless_Obtained_Housing
                + Impact_Received_Rental_Counseling_Avoided_Eviction
                + Impact_Received_Rental_Counseling_Improved_Living_Conditions
                + Impact_Received_PrePurchase_Counseling_Purchased_Housing
                + Impact_Received_Reverse_Mortgage_Counseling_Obtained_HECM
                + Impact_Received_NonDelinquency_PostPurchase_Counseling_Improve_Conditions_Affordability
                + Impact_Prevented_Resolved_Mortgage_Default;
            }
            set
            {
            }
        }

        public override void SerializeData(AgencyProfileData CurrentAgency, System.Xml.XmlWriter xml)
        {
            xml.WriteStartElement("tns:Form_9902");

            xml.WriteStartElement("tns:Report_Period_Id");
            xml.WriteValue(FmtInt32(report_period_id));
            xml.WriteEndElement();

            Ethnicity_Clients_Counseling_Hispanic.SerializeData("Ethnicity_Households_Counseling_Hispanic", xml);
            Ethnicity_Clients_Counseling_Non_Hispanic.SerializeData("Ethnicity_Households_Counseling_Non_Hispanic", xml);
            Ethnicity_Clients_Counseling_No_Response.SerializeData("Ethnicity_Households_Counseling_No_Response", xml);
            Section_3_Total.SerializeData("Section_3_Total", xml);

            Race_Clients_Counseling_American_Indian_Alaskan_Native.SerializeData("Race_Households_Counseling_American_Indian", xml);
            Race_Clients_Counseling_Asian.SerializeData("Race_Households_Counseling_Asian", xml);
            Race_Clients_Counseling_Black_AfricanAmerican.SerializeData("Race_Households_Counseling_Black_African_American", xml);
            Race_Clients_Counseling_Pacific_Islanders.SerializeData("Race_Households_Counseling_Pacific_Islanders", xml);
            Race_Clients_Counseling_White.SerializeData("Race_Households_Counseling_White", xml);
            MultiRace_Clients_Counseling_AMINDWHT.SerializeData("MultiRace_Households_Counseling_AMINDWHT", xml);
            MultiRace_Clients_Counseling_ASIANWHT.SerializeData("MultiRace_Households_Counseling_ASIANWHT", xml);
            MultiRace_Clients_Counseling_BLKWHT.SerializeData("MultiRace_Households_Counseling_BLKWHT", xml);
            MultiRace_Clients_Counseling_AMRCINDBLK.SerializeData("MultiRace_Households_Counseling_AMRCINDBLK", xml);
            MultiRace_Clients_Counseling_OtherMLTRC.SerializeData("MultiRace_Households_Counseling_OtherMLTRC", xml);
            MultiRace_Clients_Counseling_NoResponse.SerializeData("MultiRace_Households_Counseling_No_Response", xml);
            Section_4_Total.SerializeData("Section_4_Total", xml);

            Less30_AMI_Level.SerializeData("Less30_AMI_Level", xml);
            a30_49_AMI_Level.SerializeData("a30_49_AMI_Level", xml);
            a50_79_AMI_Level.SerializeData("a50_79_AMI_Level", xml);
            a80_100_AMI_Level.SerializeData("a80_100_AMI_Level", xml);
            Greater100_AMI_Level.SerializeData("Greater100_AMI_Level", xml);
            AMI_No_Response.SerializeData("AMI_No_Response", xml);
            Section_5_Total.SerializeData("Section_5_Total", xml);

            Household_Lives_In_Rural_Area.SerializeData("Household_Lives_In_Rural_Area", xml);
            Household_Does_Not_Live_In_Rural_Area.SerializeData("Household_Does_Not_Live_In_Rural_Area", xml);
            Rural_Area_No_Response.SerializeData("Rural_Area_No_Response", xml);
            Section_6_Total.SerializeData("Section_6_Total", xml);

            Household_Is_Limited_English_Proficient.SerializeData("Household_Is_Limited_English_Proficient", xml);
            Household_Is_Not_Limited_English_Proficient.SerializeData("Household_Is_Not_Limited_English_Proficient", xml);
            Limited_English_Proficient_No_Response.SerializeData("Limited_English_Proficient_No_Response", xml);
            Section_7_Total.SerializeData("Section_7_Total", xml);

            Compl_HomeMaint_FinMngt.SerializeData("Group_Compl_Fin_Lit_Workshop", xml);
            Compl_Workshop_Predatory_Lend.SerializeData("Group_Compl_Pred_Lend_Workshop", xml);
            Compl_Help_FairHousing_Workshop.SerializeData("Group_Compl_Fair_Housing_Workshop", xml);
            Compl_Resolv_Prevent_Mortg_Deliq.SerializeData("Group_Compl_Homeless_Prev_Workshop", xml);
            Counseling_Rental_Workshop.SerializeData("Group_Compl_Rental_Workshop", xml);
            Compl_HomeBuyer_Educ_Workshop.SerializeData("Group_Compl_PrePurchase_HomeBuyer_Workshop", xml);
            Compl_NonDelinqency_PostPurchase_Workshop.SerializeData("Group_Compl_NonDelinqency_PostPurchase_Workshop", xml);
            Compl_Resolv_Prevent_Mortg_Deliq.SerializeData("Group_Compl_Resolv_Prevent_Mortg_Delinq_Workshop", xml);
            Compl_Other_Workshop.SerializeData("Group_Compl_Other_Workshop", xml);
            Section_8_Total.SerializeData("Section_8_Total", xml);

            One_Homeless_Assistance_Counseling.SerializeData("One_Homeless_Assistance_Counseling", xml);
            One_Rental_Topics_Counseling.SerializeData("One_Rental_Topics_Counseling", xml);
            One_PrePurchase_HomeBuying_Counseling.SerializeData("One_PrePurchase_HomeBuying_Counseling", xml);
            One_Home_Maintenance_Fin_Management_Counseling.SerializeData("One_Home_Maintenance_Fin_Management_Counseling", xml);
            One_Reverse_Mortgage_Counseling.SerializeData("One_Reverse_Mortgage_Counseling", xml);
            One_Resolv_Prevent_Mortg_Delinq_Counseling.SerializeData("One_Resolv_Prevent_Mortg_Delinq_Counseling", xml);
            Section_9_Total.SerializeData("Section_9_Total", xml);
            Impact_One_On_One_And_Group.SerializeData("Impact_One_On_One_And_Group", xml);

            Impact_Received_Info_Fair_Housing.SerializeData("Impact_Received_Info_Fair_Housing", xml);
            Impact_Developed_Sustainable_Budget.SerializeData("Impact_Developed_Sustainable_Budget", xml);
            Impact_Improved_Financial_Capacity.SerializeData("Impact_Improved_Financial_Capacity", xml);
            Impact_Gained_Access_Resources_Improve_Housing.SerializeData("Impact_Gained_Access_Resources_Improve_Housing", xml);
            Impact_Gained_Access_NonHousing_Resources.SerializeData("Impact_Gained_Access_NonHousing_Resources", xml);
            Impact_Homeless_Obtained_Housing.SerializeData("Impact_Homeless_Obtained_Housing", xml);
            Impact_Received_Rental_Counseling_Avoided_Eviction.SerializeData("Impact_Received_Rental_Counseling_Avoided_Eviction", xml);
            Impact_Received_Rental_Counseling_Improved_Living_Conditions.SerializeData("Impact_Received_Rental_Counseling_Improved_Living_Conditions", xml);
            Impact_Received_PrePurchase_Counseling_Purchased_Housing.SerializeData("Impact_Received_PrePurchase_Counseling_Purchased_Housing", xml);
            Impact_Received_Reverse_Mortgage_Counseling_Obtained_HECM.SerializeData("Impact_Received_Reverse_Mortgage_Counseling_Obtained_HECM", xml);
            Impact_Received_NonDelinquency_PostPurchase_Counseling_Improve_Conditions_Affordability.SerializeData("Impact_Received_NonDelinquency_PostPurchase_Counseling_Improve_Conditions_Affordability", xml);
            Impact_Prevented_Resolved_Mortgage_Default.SerializeData("Impact_Prevented_Resolved_Mortgage_Default", xml);
            Section_10_Total.SerializeData("Section_10_Total", xml);

            xml.WriteEndElement();
        }
    }
}
