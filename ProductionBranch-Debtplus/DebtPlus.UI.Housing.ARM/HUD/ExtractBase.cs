#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Text;

namespace DebtPlus.UI.Housing.ARM.HUD
{
    public class StringWriterWithEncoding : System.IO.StringWriter
    {
        private Encoding m_encoding;

        public StringWriterWithEncoding()
        {
        }

        public StringWriterWithEncoding(StringBuilder sb, Encoding encoding)
            : base(sb)
        {
            m_encoding = encoding;
        }

        public override Encoding Encoding
        {
            get
            {
                return m_encoding;
            }
        }
    }

    public abstract class ExtractBase : System.ComponentModel.INotifyPropertyChanged, System.ComponentModel.INotifyPropertyChanging
    {
        protected static Int32 DEFAULT_STATE = 61;

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;

        protected void RaisePropertyChanging(string PropertyName)
        {
            var evt = PropertyChanging;
            if( evt != null )
            {
                evt(this, new System.ComponentModel.PropertyChangingEventArgs(PropertyName));
            }
        }

        protected void RaisePropertyChanged(string PropertyName)
        {
            var evt = PropertyChanged;
            if( evt != null )
            {
                evt(this, new System.ComponentModel.PropertyChangedEventArgs(PropertyName));
            }
        }

        protected void DoSetProperty(string PropertyName, ref Field_9902 OldValue, Field_9902 NewValue)
        {
            // Update the setting appropriately when the value is changed
            if (OldValue != NewValue)
            {
                RaisePropertyChanging(PropertyName);
                OldValue = NewValue;
                RaisePropertyChanged(PropertyName);
            }
        }

        protected void DoSetProperty(string PropertyName, ref Int32 OldValue, Int32 NewValue)
        {
            // Update the setting appropriately when the value is changed
            if (OldValue.CompareTo(NewValue) != 0)
            {
                RaisePropertyChanging(PropertyName);
                OldValue = NewValue;
                RaisePropertyChanged(PropertyName);
            }
        }

        protected void DoSetProperty(string PropertyName, ref Int32? OldValue, Int32? NewValue)
        {
            // Update the setting appropriately when the value is changed
            if ((OldValue) != (NewValue))
            {
                RaisePropertyChanging(PropertyName);
                OldValue = NewValue;
                RaisePropertyChanged(PropertyName);
            }
        }

        protected void DoSetProperty(string PropertyName, ref string OldValue, string NewValue)
        {
            // Determine if the value is changed from the previous setting
            bool Different = (string.IsNullOrEmpty(OldValue) != string.IsNullOrEmpty(NewValue));
            if (!string.IsNullOrEmpty(OldValue))
            {
                if (!Different)
                {
                    Different = ((string)OldValue).CompareTo(NewValue) != 0;
                }
            }

            // Update the setting appropriately when the value is changed
            if (Different)
            {
                RaisePropertyChanging(PropertyName);
                OldValue = NewValue;
                RaisePropertyChanged(PropertyName);
            }
        }

        protected void DoSetProperty(string PropertyName, ref bool OldValue, bool NewValue)
        {
            // Determine if the value is changed from the previous setting
            if (OldValue != NewValue)
            {
                RaisePropertyChanging(PropertyName);
                OldValue = NewValue;
                RaisePropertyChanged(PropertyName);
            }

        }

        protected void DoSetProperty(string PropertyName, ref decimal OldValue, decimal NewValue)
        {
            // Update the setting appropriately when the value is changed
            if (OldValue.CompareTo(NewValue) != 0)
            {
                RaisePropertyChanging(PropertyName);
                OldValue = NewValue;
                RaisePropertyChanged(PropertyName);
            }
        }

        protected void DoSetProperty(string PropertyName, ref double OldValue, double NewValue)
        {
            // Update the setting appropriately when the value is changed
            if (OldValue.CompareTo(NewValue) != 0)
            {
                RaisePropertyChanging(PropertyName);
                OldValue = NewValue;
                RaisePropertyChanged(PropertyName);
            }
        }

        protected void DoSetProperty(string PropertyName, ref DateTime OldValue, DateTime NewValue)
        {
            // Update the setting appropriately when the value is changed
            if (OldValue.Date.CompareTo(NewValue.Date) != 0)
            {
                RaisePropertyChanging(PropertyName);
                OldValue = NewValue;
                RaisePropertyChanged(PropertyName);
            }
        }

        protected void DoSetProperty(string PropertyName, ref Nullable<DateTime> OldValue, Nullable<DateTime> NewValue)
        {
            // Determine if the value is changed from the previous setting
            bool Different = (OldValue.HasValue != NewValue.HasValue);
            if (OldValue.HasValue)
            {
                if (!Different)
                {
                    Different = ((DateTime)OldValue.Value).CompareTo(NewValue.Value) != 0;
                }
            }

            // Update the setting appropriately when the value is changed
            if (Different)
            {
                RaisePropertyChanging(PropertyName);
                OldValue = NewValue;
                RaisePropertyChanged(PropertyName);
            }
        }

        protected void DoSetProperty(string PropertyName, ref Nullable<DateTime> OldValue, DateTime NewValue)
        {
            // Determine if the value is changed from the previous setting
            if (OldValue.HasValue)
            {
                if (OldValue.Value == NewValue)
                {
                    return;
                }
            }

            RaisePropertyChanging(PropertyName);
            OldValue = NewValue;
            RaisePropertyChanged(PropertyName);
        }

        protected internal static string FmtSSN(string Input)
        {
            if (string.IsNullOrEmpty(Input))
            {
                Input = "0";
            }
            string NewNumber = System.Text.RegularExpressions.Regex.Replace(Input, "[^0-9]", string.Empty).PadLeft(9, '0');
            return string.Format("{0}-{1}-{2}", NewNumber.Substring(0, 3), NewNumber.Substring(3, 2), NewNumber.Substring(5, 4));
        }

        protected internal static string FmtSSNLast(string Input)
        {
            if (string.IsNullOrEmpty(Input))
            {
                Input = "0";
            }
            string NewNumber = System.Text.RegularExpressions.Regex.Replace(Input, "[^0-9]", string.Empty).PadLeft(9, '0');
            return NewNumber.Substring(5, 4);
        }

        protected internal static string FmtYN(System.Nullable<Int32> value)
        {
            return FmtYN(value.GetValueOrDefault(0));
        }

        protected internal static string FmtYN(Int32 value)
        {
            return FmtYN(value > 0);
        }

        protected internal static string FmtYN(System.Nullable<bool> value)
        {
            return FmtYN(value.GetValueOrDefault(false));
        }

        protected internal static string FmtYN(bool value)
        {
            return value ? "Y" : "N";
        }

        protected internal static string FmtDecimal(decimal Input)
        {
            return string.Format("{0:f0}", Input);
        }

        protected internal static string FmtInt32(System.Nullable<Int32> Input)
        {
            return FmtInt32(Input.GetValueOrDefault(0));
        }

        protected internal static string FmtInt32(Int32 Input)
        {
            return Input.ToString();
        }

        protected internal static string FmtInt32(string Input)
        {
            return FmtString(Input);
        }

        protected internal static string FmtString(string Input)
        {
            return FmtString(Input, string.Empty);
        }

        protected internal static string FmtString(string Input, string defaultItem)
        {
            if (string.IsNullOrEmpty(Input))
            {
                return defaultItem.Trim();
            }
            return Input.Trim();
        }

        protected internal static string FmtTelephoneNumber(string PhoneNumber)
        {
            return FmtTelephoneNumber(PhoneNumber, false);
        }

        protected internal static string FmtTelephoneNumber(string PhoneNumber, bool IncludeExtension)
        {
            if (string.IsNullOrEmpty(PhoneNumber))
            {
                return string.Empty;
            }

            // Split the extension from the telephone number if one is given.
            string Extension = string.Empty;
            Int32 IPos = PhoneNumber.IndexOf("x");
            if (IPos > 0)
            {
                Extension = PhoneNumber.Substring(IPos + 1).Trim();
                PhoneNumber = PhoneNumber.Substring(0, IPos - 1);
            }

            string NewNumber = System.Text.RegularExpressions.Regex.Replace(PhoneNumber, "[^0-9]", string.Empty).PadLeft(10, '0');
            string Answer = string.Format("{0}-{1}-{2}", NewNumber.Substring(0, 3), NewNumber.Substring(3, 3), NewNumber.Substring(6, 4));

            // If the extension is desired then ensure that it is properly formatted too
            if (IncludeExtension && Extension != string.Empty)
            {
                Extension = Extension.PadLeft(5, '0').Substring(0, 5);

                // Add the extension to the formatted number
                Answer = string.Format("{0}x{1}", Answer, Extension);
            }

            return Answer;
        }

        protected internal static string FmtZip(string Zipcode)
        {
            if (string.IsNullOrEmpty(Zipcode))
            {
                return "00000";
            }
            string NewNumber = System.Text.RegularExpressions.Regex.Replace(Zipcode, "[^0-9]", string.Empty).PadLeft(5, '0');
            return NewNumber.Substring(0, 5);
        }

        protected internal static string FmtDate(Nullable<DateTime> Input)
        {
            return FmtDate(Input, false);
        }

        protected internal static string FmtDate(Nullable<DateTime> Input, bool IncludeTime)
        {
            DateTime InputDate;
            if (Input.HasValue)
            {
                InputDate = Input.Value;
            }
            else
            {
                InputDate = new DateTime(1900, 1, 1);
            }

            return FmtDate(InputDate, IncludeTime);
        }

        protected internal static string FmtDate(DateTime InputDate)
        {
            return FmtDate(InputDate, false);
        }

        protected internal static string FmtDate(DateTime InputDate, bool IncludeTime)
        {
            string Answer;

            if (IncludeTime)
            {
                Answer = string.Format("{0:MM-dd-yyyy HH:mm}", InputDate);
            }
            else
            {
                Answer = string.Format("{0:MM-dd-yyyy}", InputDate);
            }
            return Answer;
        }

        protected internal static string FmtPercent(double Input)
        {
            if (Input < 0.0D)
            {
                Input = 0.0D;
            }
            while (Input >= 1.0D)
            {
                Input /= 100.0D;
            }

            return string.Format("{0:f3}", Input);
        }

        protected internal static string FmtEmployeeID(Int32 input)
        {
            return FmtInt32(input);
        }

        protected internal static string FmtEIN(string Input)
        {
            if (string.IsNullOrEmpty(Input))
            {
                Input = "0";
            }
            Input = System.Text.RegularExpressions.Regex.Replace(Input, "[^0-9]", string.Empty).PadLeft(9, '0');
            string Answer = string.Format("{0}-{1}", Input.Substring(0, 2), Input.Substring(2, 7));
            return Answer;
        }

        protected internal static string FmtDBN(string Input)
        {
            if (string.IsNullOrEmpty(Input))
            {
                Input = "0";
            }
            string NewNumber = System.Text.RegularExpressions.Regex.Replace(Input, "[^0-9]", string.Empty).Trim().PadLeft(9, '0');
            return NewNumber.Substring(0, 9);
        }

        public abstract void SerializeData(AgencyProfileData CurrentAgency, System.Xml.XmlWriter xml);
    }
}
