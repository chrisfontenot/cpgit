#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;

namespace DebtPlus.UI.Housing.ARM.HUD
{
    public class counselor_profile : ExtractBase
    {
        public counselor_profile(Int32 CounselorID) : this()
        {
            cms_counselor_id = CounselorID;
        }

        public counselor_profile()
        {
        }

        public System.Collections.Generic.List<counselor_service_type> service_types = new System.Collections.Generic.List<counselor_service_type>();
        public System.Collections.Generic.List<counselor_language> languages = new System.Collections.Generic.List<counselor_language>();

        private Int32 privatecms_counselor_id = 0;
        public Int32 cms_counselor_id
        {
            get
            {
                return privatecms_counselor_id;
            }
            set
            {
                if (value < 0)
                {
                    value = 0;
                }
                DoSetProperty("cms_counselor_id", ref privatecms_counselor_id, value);
            }
        }

        private string privatecnslor_fname = "-";
        public string cnslor_fname
        {
            get
            {
                return privatecnslor_fname;
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    value = "-";
                }
                if (value.Length > 50)
                {
                    value = value.Substring(0, 50);
                }
                DoSetProperty("cnslor_fname", ref privatecnslor_fname, value);
            }
        }

        private string privatecnslor_lname = "-";
        public string cnslor_lname
        {
            get
            {
                return privatecnslor_lname;
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    value = "-";
                }
                if (value.Length > 50)
                {
                    value = value.Substring(0, 50);
                }
                DoSetProperty("cnslor_lname", ref privatecnslor_lname, value);
            }
        }

        private string privatecnslor_mname = string.Empty;
        public string cnslor_mname
        {
            get
            {
                return privatecnslor_mname;
            }
            set
            {
                if (value != null)
                {
                    if (value.Length > 10)
                    {
                        value = value.Substring(0, 10);
                    }
                }
                DoSetProperty("cnslor_mname", ref privatecnslor_mname, value);
            }
        }

        private readonly DateTime private_cnslor_emp_start_date = DateTime.Now.Date;
        public DateTime cnslor_emp_start_date
        {
            get
            {
                return private_cnslor_emp_start_date;
            }
            set
            {
                DoSetProperty("cnslor_emp_start_date", ref privateemp_start_date, value);
            }
        }

        private DateTime privateemp_start_date;
        public DateTime emp_start_date
        {
            get
            {
                return privateemp_start_date;
            }
            set
            {
                DoSetProperty("emp_start_date", ref privateemp_start_date, value);
            }
        }

        private DateTime? private_cnslor_emp_end_date;
        public DateTime? cnslor_emp_end_date
        {
            get
            {
                return private_cnslor_emp_end_date;
            }
            set
            {
                DoSetProperty("cnslor_emp_end_date", ref private_cnslor_emp_end_date, value);
            }
        }

        private DateTime? privateemp_end_date;
        public DateTime? emp_end_date
        {
            get
            {
                return privateemp_end_date;
            }
            set
            {
                // If both do not have a value then they are both equal.
                if (!value.HasValue && !privateemp_end_date.HasValue)
                {
                    return;
                }

                // If one has a value and the other doesn't then they are different.
                if (!value.HasValue || !privateemp_end_date.HasValue)
                {
                    DoSetProperty("emp_start_date", ref privateemp_end_date, value.Value);
                    return;
                }

                // Otherwise, compare the two items since they both have a value.
                if (value.Value.Date != privateemp_end_date.Value.Date)
                {
                    DoSetProperty("emp_start_date",ref privateemp_end_date, value.Value);
                }
            }
        }

        private decimal private_cnslor_rate = 0M;
        public decimal cnslor_rate
        {
            get
            {
                return private_cnslor_rate;
            }
            set
            {
                DoSetProperty("cnslor_rate", ref private_cnslor_rate, value);
            }
        }

        private decimal private_rate;
        public decimal rate
        {
            get
            {
                return private_rate;
            }
            set
            {
                DoSetProperty("rate", ref private_rate, value);
            }
        }

        private string private_cnslor_billing_method = "Fixed";
        public string cnslor_billing_method
        {
            get
            {
                return private_cnslor_billing_method;
            }
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    if (string.Compare(value, "Fixed", true) == 0)
                    {
                        DoSetProperty("cnslor_billing_method", ref private_cnslor_billing_method, "Fixed");
                        return;
                    }
                }
                DoSetProperty("cnslor_billing_method", ref private_cnslor_billing_method, "Hourly");
            }
        }

        private Int32 private_cnslor_hud_id = -1;
        public Int32 cnslor_HUD_id
        {
            get
            {
                return private_cnslor_hud_id;
            }
            set
            {
                DoSetProperty("cnslor_HUD_id", ref private_cnslor_hud_id, value);
            }
        }

        private Int32 private_hud_id;
        public Int32 hud_id
        {
            get
            {
                return private_hud_id;
            }
            set
            {
                DoSetProperty("hud_id", ref private_hud_id, value);
            }
        }

        private string private_cnslor_SSN = "000-00-0000";
        public string cnslor_SSN
        {
            get
            {
                return private_cnslor_SSN;
            }
            set
            {
                DoSetProperty("cnslor_SSN", ref private_cnslor_SSN, value);
            }
        }

        private string private_ssn = string.Empty;
        public string ssn
        {
            get
            {
                return private_ssn;
            }
            set
            {
                DoSetProperty("ssn", ref private_ssn, value);
            }
        }

        private string private_hud_phone;
        public string hud_phone
        {
            get
            {
                return private_hud_phone;
            }
            set
            {
                DoSetProperty("cnslor_phone", ref private_hud_phone, value);
            }
        }

        private string private_email;
        public string hud_email
        {
            get
            {
                return private_email;
            }
            set
            {
                DoSetProperty("cnslor_email", ref private_email, value);
            }
        }

        private string private_cnslor_phone = string.Empty;
        public string cnslor_phone
        {
            get
            {
                return private_cnslor_phone;
            }
            set
            {
                DoSetProperty("cnslor_phone", ref private_cnslor_phone, value);
            }
        }

        private string private_cnslor_email = string.Empty;
        public string cnslor_email
        {
            get
            {
                return private_cnslor_email;
            }
            set
            {
                DoSetProperty("cnslor_email", ref private_cnslor_email, value);
            }
        }

        public override void SerializeData(AgencyProfileData CurrentAgency, System.Xml.XmlWriter xml)
        {
            xml.WriteStartElement("tns:Counselor_Profile");

            xml.WriteElementString("tns:CMS_Counselor_Id", FmtEmployeeID(cms_counselor_id));
            if (cnslor_fname != string.Empty)
            {
                xml.WriteElementString("tns:Counselor_Fname", FmtString(cnslor_fname));
            }
            if (cnslor_lname != string.Empty)
            {
                xml.WriteElementString("tns:Counselor_Lname", FmtString(cnslor_lname));
            }
            if (cnslor_mname != string.Empty)
            {
                xml.WriteElementString("tns:Counselor_Mname", FmtString(cnslor_mname));
            }

            xml.WriteElementString("tns:Counselor_Emp_Start_Date", FmtDate(cnslor_emp_start_date));
            if (cnslor_emp_end_date.HasValue)
            {
                xml.WriteElementString("tns:Counselor_Emp_End_Date", FmtDate(cnslor_emp_end_date));
            }
            xml.WriteElementString("tns:Counselor_Rate", FmtDecimal(cnslor_rate));
            if (cnslor_billing_method != string.Empty)
            {
                xml.WriteElementString("tns:Counselor_Billing_Method", FmtString(cnslor_billing_method));
            }

            if (cnslor_HUD_id > 0)
            {
                xml.WriteElementString("tns:Counselor_HUD_Id", cnslor_HUD_id.ToString());
            }

            if (cnslor_SSN != string.Empty)
            {
                xml.WriteElementString("tns:Counselor_SSN", FmtSSN(cnslor_SSN));
            }
            if (cnslor_phone != string.Empty)
            {
                xml.WriteElementString("tns:Counselor_Phone", FmtTelephoneNumber(cnslor_phone));
            }
            if (cnslor_email != string.Empty)
            {
                xml.WriteElementString("tns:Counselor_Email", FmtString(cnslor_email));
            }

            // Languages. ENGLISH is assumed if none are defined.
            if (languages.Count <= 0)
            {
                languages.Add(new counselor_language(19));
            }

            xml.WriteStartElement("tns:Counselor_Languages");
            foreach (counselor_language cnslr_language in languages)
            {
                cnslr_language.SerializeData(CurrentAgency, xml);
            }
            xml.WriteEndElement();

            // service_types
            if (service_types.Count > 0)
            {
                xml.WriteStartElement("tns:Counselor_Service_Types");
                foreach (counselor_service_type cnslr_service in service_types)
                {
                    cnslr_service.SerializeData(CurrentAgency, xml);
                }
                xml.WriteEndElement();
            }

            xml.WriteEndElement();
        }
    }
}
