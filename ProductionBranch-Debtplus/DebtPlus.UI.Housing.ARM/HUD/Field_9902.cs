﻿// -----------------------------------------------------------------------
// <copyright file="Field_9902.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace DebtPlus.UI.Housing.ARM.HUD
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml;

    /// <summary>
    /// Fields obtained from the database for the HUD 9902 information
    /// </summary>
    public class Field_9902 : IEquatable<Field_9902>
    {
        public Int32 All_Housing { get; set; }
        public Int32 HUD_Housing { get; set; }

        public override bool Equals(Object obj)
        {
            return Equals(obj as Field_9902);
        }

        public bool Equals(Field_9902 obj)
        {
            if (obj != null)
            {
                return (obj == this);
            }
            return false;
        }

        public override int GetHashCode()
        {
            return All_Housing.GetHashCode();
        }

        public Field_9902()
        {
            All_Housing = default(Int32);
            HUD_Housing = default(Int32);
        }

        public Field_9902(Int32 all, Int32 hud)
        {
            All_Housing = all;
            HUD_Housing = hud;
        }

        public Field_9902(System.Nullable<Int32> all, System.Nullable<Int32> hud)
        {
            All_Housing = all.GetValueOrDefault();
            HUD_Housing = hud.GetValueOrDefault();
        }

        public virtual void SerializeData(string keyName, System.Xml.XmlWriter xml)
        {
            xml.WriteStartElement(string.Format("tns:{0}", keyName));
            xml.WriteAttributeString("activity_type_id", "4");
            xml.WriteValue(ExtractBase.FmtInt32(All_Housing));
            xml.WriteEndElement();

            xml.WriteStartElement(string.Format("tns:{0}", keyName));
            xml.WriteAttributeString("activity_type_id", "9");
            xml.WriteValue(ExtractBase.FmtInt32(HUD_Housing));
            xml.WriteEndElement();
        }

        public static Boolean operator == (Field_9902 a, Field_9902 b)
        {
            return (a.All_Housing == b.All_Housing) && (a.HUD_Housing == b.HUD_Housing);
        }

        public static Boolean operator != (Field_9902 a, Field_9902 b)
        {
            return ! (a == b);
        }

        public static Field_9902 operator +(Field_9902 a, Int32 b)
        {
            return new Field_9902(a.All_Housing + b, a.HUD_Housing + b);
        }

        public static Field_9902 operator +(Int32 b, Field_9902 a)
        {
            return new Field_9902(a.All_Housing + b, a.HUD_Housing + b);
        }

        public static Field_9902 operator +(Field_9902 b, Field_9902 a)
        {
            return new Field_9902(a.All_Housing + b.All_Housing, a.HUD_Housing + b.HUD_Housing);
        }

        public new string ToString()
        {
            return string.Format(@"\{All={0}, HUD={1}\}", All_Housing.ToString(), HUD_Housing.ToString());
        }
    }
}
