#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion
using System.Linq;
using System;

namespace DebtPlus.UI.Housing.ARM.HUD
{
    public class group_session : ExtractBase
    {

        public group_session()
            : base()
        {
        }

        #region storage
        private Int32 private_group_session_id;
        public Int32 group_session_id
        {
            get
            {
                return private_group_session_id;
            }
            set
            {
                if (value < 0)
                {
                    value = 0;
                }
                DoSetProperty("group_session_id", ref private_group_session_id, value);
            }
        }

        private Int32 private_group_session_counselor_id;
        public Int32 group_session_counselor_id
        {
            get
            {
                return private_group_session_counselor_id;
            }
            set
            {
                if (value < 0)
                {
                    value = 0;
                }
                DoSetProperty("group_session_counselor_id", ref private_group_session_counselor_id, value);
            }
        }

        private Int32 private_activity_type = 4;
        public Int32 activity_type
        {
            get
            {
                return private_activity_type;
            }
            set
            {
                DoSetProperty("activity_type", ref private_activity_type, value);
            }
        }

        private string private_group_session_title = string.Empty;
        public string group_session_title
        {
            get
            {
                return private_group_session_title;
            }
            set
            {
                DoSetProperty("group_session_title", ref private_group_session_title, value);
            }
        }

        private Nullable<DateTime> private_group_session_date;
        public Nullable<DateTime> group_session_date
        {
            get
            {
                return private_group_session_date;
            }
            set
            {
                DoSetProperty("group_session_date", ref private_group_session_date, value);
            }
        }

        private Int32 private_group_session_duration;
        public Int32 group_session_duration
        {
            get
            {
                if (private_group_session_duration <= 0)
                {
                    return 1;
                }
                return private_group_session_duration;
            }
            set
            {
                if (value < 0)
                {
                    value = 0;
                }
                DoSetProperty("group_session_duration", ref private_group_session_duration, value);
            }
        }

        private Int32 private_group_session_type;
        public Int32 group_session_type
        {
            get
            {
                if (private_group_session_type < 2)
                {
                    return 9;
                }
                return private_group_session_type;
            }
            set
            {
                if (value < 0)
                {
                    value = 0;
                }
                DoSetProperty("group_session_type", ref private_group_session_type, value);
            }
        }

        private Int32 private_group_session_attribute_hud_grant;
        public Int32 group_session_attribute_hud_grant
        {
            get
            {
                if (private_group_session_attribute_hud_grant < 2)
                {
                    return 7; //-- NOTA (NOT APPLICABLE)
                }
                return private_group_session_attribute_hud_grant;
            }
            set
            {
                if (value < 0)
                {
                    value = 0;
                }
                DoSetProperty("group_session_attribute_hud_grant", ref private_group_session_attribute_hud_grant, value);
            }
        }
        #endregion

        public System.Collections.Generic.List<group_session_attendee> group_session_attendees = new System.Collections.Generic.List<group_session_attendee>();

        public override void SerializeData(AgencyProfileData CurrentAgency, System.Xml.XmlWriter xml)
        {
            xml.WriteStartElement("tns:Group_Session");

            xml.WriteElementString("tns:Group_Session_Id", FmtInt32(group_session_id));
            xml.WriteElementString("tns:Group_Session_Counselor_Id", FmtEmployeeID(group_session_counselor_id));
            xml.WriteElementString("tns:Group_Session_Title", FmtString(group_session_title));
            xml.WriteElementString("tns:Group_Session_Date", FmtDate(group_session_date));
            xml.WriteElementString("tns:Group_Session_Duration", FmtInt32(group_session_duration));
            xml.WriteElementString("tns:Group_Session_Type", FmtInt32(group_session_type));
            xml.WriteElementString("tns:Group_Session_Attribute_HUD_Grant", FmtInt32(group_session_attribute_hud_grant));
            xml.WriteElementString("tns:Group_Session_Activity_Type", FmtInt32(activity_type));

            // Now, include the attendee information
            if (group_session_attendees.Count > 0)
            {
                xml.WriteStartElement("tns:Group_Session_Attendees");
                foreach (group_session_attendee item in group_session_attendees)
                {
                    item.SerializeData(CurrentAgency, xml);
                }
                xml.WriteEndElement();
            }

            xml.WriteEndElement();
        }
    }
}
