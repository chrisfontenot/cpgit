#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;

namespace DebtPlus.UI.Housing.ARM.HUD
{
    public class group_session_attendee : ExtractBase
    {

        #region storage
        private Int32 private_attendee_id;
        public Int32 attendee_id
        {
            get
            {
                return private_attendee_id;
            }
            set
            {
                if (value < 0)
                {
                    value = 0;
                }
                DoSetProperty("attendee_id", ref private_attendee_id, value);
            }
        }

        private decimal private_Attendee_Fee_Amount;
        public decimal Attendee_Fee_Amount
        {
            get
            {
                return private_Attendee_Fee_Amount;
            }
            set
            {
                DoSetProperty("Attendee_Fee_Amount", ref private_Attendee_Fee_Amount, value);
            }
        }

        private Int32 private_Attendee_referred_by;
        public Int32 Attendee_referred_by
        {
            get
            {
                return private_Attendee_referred_by;
            }
            set
            {
                DoSetProperty("Attendee_referred_by", ref private_Attendee_referred_by, value);
            }
        }

        private Int32? private_Attendee_Income_Level;
        public Int32? Attendee_Income_Level
        {
            get
            {
                return private_Attendee_Income_Level;
            }
            set
            {
                DoSetProperty("Attendee_Income_Level", ref private_Attendee_Income_Level, value);
            }
        }

        private string private_Attendee_Address_1;
        public string Attendee_Address_1
        {
            get
            {
                return private_Attendee_Address_1;
            }
            set
            {
                DoSetProperty("Attendee_Address_1", ref private_Attendee_Address_1, value);
            }
        }

        private string private_Attendee_Address_2;
        public string Attendee_Address_2
        {
            get
            {
                return private_Attendee_Address_2;
            }
            set
            {
                DoSetProperty("Attendee_Address_2", ref private_Attendee_Address_2, value);
            }
        }

        private string private_Attendee_City;
        public string Attendee_City
        {
            get
            {
                return private_Attendee_City;
            }
            set
            {
                DoSetProperty("Attendee_City", ref private_Attendee_City, value);
            }
        }

        private Int32? private_Attendee_State;
        public Int32? Attendee_State
        {
            get
            {
                return private_Attendee_State;
            }
            set
            {
                DoSetProperty("Attendee_State", ref private_Attendee_State, value);
            }
        }

        private string private_Attendee_Zip_Code;
        public string Attendee_Zip_Code
        {
            get
            {
                return private_Attendee_Zip_Code;
            }
            set
            {
                DoSetProperty("Attendee_Zip_Code", ref private_Attendee_Zip_Code, value);
            }
        }

        private System.Nullable<int> _Household_Lives_In_Rural_Area;
        public System.Nullable<int> Household_Lives_In_Rural_Area
        {
            get
            {
                return this._Household_Lives_In_Rural_Area;
            }
            set
            {
                if ((this._Household_Lives_In_Rural_Area != value))
                {
                    this._Household_Lives_In_Rural_Area = value;
                }
            }
        }

        private System.Nullable<int> _Household_Does_Not_Live_In_Rural_Area;
        public System.Nullable<int> Household_Does_Not_Live_In_Rural_Area
        {
            get
            {
                return this._Household_Does_Not_Live_In_Rural_Area;
            }
            set
            {
                if ((this._Household_Does_Not_Live_In_Rural_Area != value))
                {
                    this._Household_Does_Not_Live_In_Rural_Area = value;
                }
            }
        }

        private System.Nullable<int> _Rural_Area_No_Response;
        public System.Nullable<int> Rural_Area_No_Response
        {
            get
            {
                return this._Rural_Area_No_Response;
            }
            set
            {
                if ((this._Rural_Area_No_Response != value))
                {
                    this._Rural_Area_No_Response = value;
                }
            }
        }

        public int Attendee_Rural_Area
        {
            get
            {
                if (Rural_Area_No_Response > 0)
                {
                    return 3;
                }
                else
                {
                    if (Household_Lives_In_Rural_Area > 0)
                    {
                        return 1;
                    }
                }

                return 2;
            }
        }

        private System.Nullable<int> _Household_Is_Limited_English_Proficient;
        public System.Nullable<int> Household_Is_Limited_English_Proficient
        {
            get
            {
                return this._Household_Is_Limited_English_Proficient;
            }
            set
            {
                if ((this._Household_Is_Limited_English_Proficient != value))
                {
                    this._Household_Is_Limited_English_Proficient = value;
                }
            }
        }

        private System.Nullable<int> _Limited_English_Proficient_No_Response;
        public System.Nullable<int> Limited_English_Proficient_No_Response
        {
            get
            {
                return this._Limited_English_Proficient_No_Response;
            }
            set
            {
                if ((this._Limited_English_Proficient_No_Response != value))
                {
                    this._Limited_English_Proficient_No_Response = value;
                }
            }
        }

        private System.Nullable<int> _Household_Is_Not_Limited_English_Proficient;
        public System.Nullable<int> Household_Is_Not_Limited_English_Proficient
        {
            get
            {
                return this._Household_Is_Not_Limited_English_Proficient;
            }
            set
            {
                if ((this._Household_Is_Not_Limited_English_Proficient != value))
                {
                    this._Household_Is_Not_Limited_English_Proficient = value;
                }
            }
        }

        public Int32 Attendee_Limited_English_Proficiency
        {
            get
            {
                if (Limited_English_Proficient_No_Response > 0)
                {
                    return 3;
                }

                if (Household_Is_Limited_English_Proficient > 0)
                {
                    return 1;
                }
                return 2;
            }
        }

        private bool private_Attendee_FirstTime_Home_Buyer;
        public bool Attendee_FirstTime_Home_Buyer
        {
            get
            {
                return private_Attendee_FirstTime_Home_Buyer;
            }
            set
            {
                DoSetProperty("Attendee_FirstTime_Home_Buyer", ref private_Attendee_FirstTime_Home_Buyer, value);
            }
        }
        #endregion

        private Int32 private_group_session_id = 0;
        public Int32 group_session_id
        {
            get
            {
                return private_group_session_id;
            }
            set
            {
                DoSetProperty("group_session_id", ref private_group_session_id, value);
            }
        }

        public override void SerializeData(AgencyProfileData CurrentAgency, System.Xml.XmlWriter xml)
        {
            xml.WriteStartElement("tns:Group_Session_Attendee");

            // These fields are fixed and required
            xml.WriteElementString("tns:Attendee_Id", FmtInt32(attendee_id));
            xml.WriteElementString("tns:Attendee_Fee_Amount", FmtInt32(Convert.ToInt32(Attendee_Fee_Amount)));
            xml.WriteElementString("tns:Attendee_Referred_By", FmtInt32(Attendee_referred_by));
            xml.WriteElementString("tns:Attendee_FirstTime_Home_Buyer", FmtYN(Attendee_FirstTime_Home_Buyer));

            // These are optional
            if (Attendee_Income_Level.HasValue)
            {
                xml.WriteElementString("tns:Attendee_Income_Level", FmtInt32(Convert.ToInt32(Attendee_Income_Level.Value)));
            }

            if (Attendee_Address_1 != null)
            {
                xml.WriteElementString("tns:Attendee_Address_1", Attendee_Address_1);
            }

            if (Attendee_Address_2 != null)
            {
                xml.WriteElementString("tns:Attendee_Address_2", Attendee_Address_2);
            }

            if (Attendee_City != null)
            {
                xml.WriteElementString("tns:Attendee_City", Attendee_City);
            }

            if (Attendee_State.HasValue)
            {
                xml.WriteElementString("tns:Attendee_State", FmtInt32(Attendee_State.Value));
            }

            if (Attendee_Zip_Code != null)
            {
                xml.WriteElementString("tns:Attendee_Zip_Code", Attendee_Zip_Code);
            }

            // xml.WriteElementString("tns:Attendee_Rural_Area", FmtInt32(Attendee_Rural_Area));
            // xml.WriteElementString("tns:Attendee_Limited_English_Proficiency", FmtInt32(Attendee_Limited_English_Proficiency));

            xml.WriteEndElement();
        }
    }
}
