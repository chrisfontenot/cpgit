#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;

namespace DebtPlus.UI.Housing.ARM.HUD
{
    public class attendee : ExtractBase
    {
        public attendee()
            : base()
        {
        }

        #region storage
        private Int32 private_attendee_id;
        public Int32 attendee_id
        {
            get
            {
                return private_attendee_id;
            }
            set
            {
                if (value < 0)
                {
                    value = 0;
                }
                DoSetProperty("attendee_id", ref private_attendee_id, value);
            }
        }

        private string private_Attendee_fname;
        public string Attendee_fname
        {
            get
            {
                if (private_Attendee_fname == string.Empty)
                {
                    return "-";
                }
                return private_Attendee_fname;
            }
            set
            {
                DoSetProperty("Attendee_fname", ref private_Attendee_fname, value);
            }
        }

        private string private_Attendee_lname;
        public string Attendee_lname
        {
            get
            {
                if (private_Attendee_lname == string.Empty)
                {
                    return "-";
                }
                return private_Attendee_lname;
            }
            set
            {
                DoSetProperty("Attendee_lname", ref private_Attendee_lname, value);
            }
        }

        private string private_Attendee_mname;
        public string Attendee_mname
        {
            get
            {
                return private_Attendee_mname;
            }
            set
            {
                DoSetProperty("Attendee_mname", ref private_Attendee_mname, value);
            }
        }

        private string private_Attendee_Address_1;
        public string Attendee_Address_1
        {
            get
            {
                return string.IsNullOrWhiteSpace(private_Attendee_Address_1) ? "-" : private_Attendee_Address_1;
            }
            set
            {
                DoSetProperty("Attendee_Address_1", ref private_Attendee_Address_1, value);
            }
        }

        private string private_Attendee_Address_2;
        public string Attendee_Address_2
        {
            get
            {
                return string.IsNullOrWhiteSpace(private_Attendee_Address_2) ? "-" : private_Attendee_Address_2;
            }
            set
            {
                DoSetProperty("Attendee_Address_2", ref private_Attendee_Address_2, value);
            }
        }

        private string private_Attendee_City;
        public string Attendee_City
        {
            get
            {
                return string.IsNullOrWhiteSpace(private_Attendee_City) ? "-" : private_Attendee_City;
            }
            set
            {
                DoSetProperty("Attendee_City", ref private_Attendee_City, value);
            }
        }

        private Int32? private_Attendee_State;
        public Int32? Attendee_State
        {
            get
            {
                return private_Attendee_State.GetValueOrDefault(61);
            }
            set
            {
                DoSetProperty("Attendee_State", ref private_Attendee_State, value);
            }
        }

        private string private_Attendee_Zip_Code;
        public string Attendee_Zip_Code
        {
            get
            {
                return (private_Attendee_Zip_Code ?? "").PadLeft(5, '0').Substring(0, 5);
            }
            set
            {
                DoSetProperty("Attendee_Zip_Code", ref private_Attendee_Zip_Code, value);
            }
        }

        private Int32? private_Attendee_Race_ID;
        public Int32? Attendee_Race_ID
        {
            get
            {
                return private_Attendee_Race_ID;
            }
            set
            {
                DoSetProperty("Attendee_Race_ID", ref private_Attendee_Race_ID, value);
            }
        }

        private Int32? private_Attendee_Ethnicity_ID;
        public Int32? Attendee_Ethnicity_ID
        {
            get
            {
                return private_Attendee_Ethnicity_ID;
            }
            set
            {
                DoSetProperty("Attendee_Ethnicity_ID", ref private_Attendee_Ethnicity_ID, value);
            }
        }

        private Int32? private_Attendee_Income_Level;
        public Int32? Attendee_Income_Level
        {
            get
            {
                return private_Attendee_Income_Level;
            }
            set
            {
                DoSetProperty("Attendee_Income_Level", ref private_Attendee_Income_Level, value);
            }
        }

        private System.Nullable<int> _Household_Lives_In_Rural_Area;
        public System.Nullable<int> Household_Lives_In_Rural_Area
        {
            get
            {
                return this._Household_Lives_In_Rural_Area;
            }
            set
            {
                if ((this._Household_Lives_In_Rural_Area != value))
                {
                    this._Household_Lives_In_Rural_Area = value;
                }
            }
        }

        private System.Nullable<int> _Household_Does_Not_Live_In_Rural_Area;
        public System.Nullable<int> Household_Does_Not_Live_In_Rural_Area
        {
            get
            {
                return this._Household_Does_Not_Live_In_Rural_Area;
            }
            set
            {
                if ((this._Household_Does_Not_Live_In_Rural_Area != value))
                {
                    this._Household_Does_Not_Live_In_Rural_Area = value;
                }
            }
        }

        private System.Nullable<int> _Rural_Area_No_Response;
        public System.Nullable<int> Rural_Area_No_Response
        {
            get
            {
                return this._Rural_Area_No_Response;
            }
            set
            {
                if ((this._Rural_Area_No_Response != value))
                {
                    this._Rural_Area_No_Response = value;
                }
            }
        }

        public int Attendee_Rural_Area
        {
            get
            {
                if (Rural_Area_No_Response > 0)
                {
                    return 3;
                }
                else
                {
                    if (Household_Lives_In_Rural_Area > 0)
                    {
                        return 1;
                    }
                }

                return 2;
            }
        }

        private System.Nullable<int> _Household_Is_Limited_English_Proficient;
        public System.Nullable<int> Household_Is_Limited_English_Proficient
        {
            get
            {
                return this._Household_Is_Limited_English_Proficient;
            }
            set
            {
                if ((this._Household_Is_Limited_English_Proficient != value))
                {
                    this._Household_Is_Limited_English_Proficient = value;
                }
            }
        }

        private System.Nullable<int> _Limited_English_Proficient_No_Response;
        public System.Nullable<int> Limited_English_Proficient_No_Response
        {
            get
            {
                return this._Limited_English_Proficient_No_Response;
            }
            set
            {
                if ((this._Limited_English_Proficient_No_Response != value))
                {
                    this._Limited_English_Proficient_No_Response = value;
                }
            }
        }

        private System.Nullable<int> _Household_Is_Not_Limited_English_Proficient;
        public System.Nullable<int> Household_Is_Not_Limited_English_Proficient
        {
            get
            {
                return this._Household_Is_Not_Limited_English_Proficient;
            }
            set
            {
                if ((this._Household_Is_Not_Limited_English_Proficient != value))
                {
                    this._Household_Is_Not_Limited_English_Proficient = value;
                }
            }
        }

        public Int32 Attendee_Limited_English_Proficiency
        {
            get
            {
                if (Limited_English_Proficient_No_Response > 0)
                {
                    return 3;
                }

                if (Household_Is_Limited_English_Proficient > 0)
                {
                    return 1;
                }
                return 2;
            }
        }
        #endregion

        public override void SerializeData(AgencyProfileData CurrentAgency, System.Xml.XmlWriter xml)
        {
            xml.WriteStartElement("tns:Attendee");

            xml.WriteElementString("tns:Attendee_Id", FmtInt32(attendee_id));
            
            if (!string.IsNullOrEmpty(Attendee_fname))
            {
                xml.WriteElementString("tns:Attendee_Fname", FmtString(Attendee_fname));
            }
            
            if (!string.IsNullOrEmpty(Attendee_lname))
            {
                xml.WriteElementString("tns:Attendee_Lname", FmtString(Attendee_lname));
            }
            
            if (!string.IsNullOrEmpty(Attendee_mname))
            {
                xml.WriteElementString("tns:Attendee_Mname", FmtString(Attendee_mname));
            }
            
            xml.WriteElementString("tns:Attendee_Address_1", FmtString(Attendee_Address_1));
            xml.WriteElementString("tns:Attendee_Address_2", FmtString(Attendee_Address_2));
            xml.WriteElementString("tns:Attendee_City", FmtString(Attendee_City));
            xml.WriteElementString("tns:Attendee_State", FmtInt32(Attendee_State.Value));
            xml.WriteElementString("tns:Attendee_Zip_Code", FmtString(Attendee_Zip_Code));

            if (Attendee_Race_ID.HasValue)
            {
                xml.WriteElementString("tns:Attendee_Race_ID", FmtInt32(Attendee_Race_ID.Value));
            }

            if (Attendee_Ethnicity_ID.HasValue)
            {
                xml.WriteElementString("tns:Attendee_Ethnicity_ID", FmtInt32(Attendee_Ethnicity_ID.Value));
            }

            if (Attendee_Income_Level.HasValue)
            {
                xml.WriteElementString("tns:Attendee_Income_Level", FmtInt32(Attendee_Income_Level.Value));
            }

            xml.WriteElementString("tns:Attendee_Rural_Area", FmtInt32(Attendee_Rural_Area));
            xml.WriteElementString("tns:Attendee_Limited_English_Proficiency", FmtInt32(Attendee_Limited_English_Proficiency));

            xml.WriteEndElement();
        }
    }
}
