#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using DebtPlus;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using DebtPlus.UI.Housing.ARM.ARM;
using DebtPlus.UI.Housing.ARM.HUD;

namespace DebtPlus.UI.Housing.ARM.ARM
{
    public class AgencyList : DebtPlus.UI.Housing.ARM.HUD.BaseAgencyList
    {
        public void GetAgencyProfiles(Configuration configInfo)
        {
            if (AgencyListItems.Count <= 0)
            {
                const string TableName = "xpr_housing_arm_v4_agencies";
                using (var ds = new DataSet("ds"))
                {
                    using (var cmd = new SqlCommand())
                    {
                        cmd.Connection = new SqlConnection(DebtPlus.LINQ.BusinessContext.ConnectionString());
                        cmd.CommandText = "xpr_housing_arm_v4_agencies";
                        cmd.CommandTimeout = 600;
                        cmd.CommandType = CommandType.StoredProcedure;

                        using (var da = new SqlDataAdapter(cmd))
                        {
                            da.Fill(ds, TableName);
                        }

                        DataTable tbl = ds.Tables[TableName];
                        if (tbl != null && tbl.Rows.Count > 0)
                        {
                            foreach (DataRow row in tbl.Rows)
                            {
                                AgencyProfileData ProfileData = new AgencyProfileData(configInfo);
                                foreach (DataColumn col in tbl.Columns)
                                {
                                    if (row[col] != System.DBNull.Value)
                                    {
                                        string FieldName = col.ColumnName;
                                        System.Reflection.PropertyInfo pi = typeof(AgencyProfileData).GetProperty(FieldName);
                                        if (pi == null)
                                        {
                                            System.Diagnostics.Debug.WriteLine(string.Format("Missing AgencyProfileData property '{0}'", FieldName));
                                        }
                                        else
                                        {
                                            pi.SetValue(ProfileData, row[col], null);
                                        }
                                    }
                                }
                                AgencyListItems.Add(ProfileData);
                            }
                        }
                    }
                }
            }
        }
    }
}
