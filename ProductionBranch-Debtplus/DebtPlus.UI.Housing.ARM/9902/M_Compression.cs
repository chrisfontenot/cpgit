#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using DebtPlus;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace DebtPlus.UI.Housing.ARM
{
    public static class Compression
    {
        public static byte[] CompressedData(string CompressionType, string SourceData)
        {
            byte[] Answer;
            Int32 StringCharacterCount = SourceData.Length;
            Int32 CompressedByteCount;
            string TempFname;

            // Compress the data as required
            switch (CompressionType)
            {
                case "GZIP":
                    TempFname = System.IO.Path.GetTempFileName();

                    // Use GZIP to compress the data
                    try
                    {
                        // Open the file for write access
                        using (var inp = new System.IO.FileStream(TempFname, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None))
                        {
                            using (var cmp = new ICSharpCode.SharpZipLib.GZip.GZipOutputStream(inp))
                            {
                                cmp.SetLevel(9);
                                byte[] BufferArea = new byte[StringCharacterCount];
                                BufferArea = (new System.Text.ASCIIEncoding()).GetBytes(SourceData);
                                cmp.Write(BufferArea, 0, StringCharacterCount);
                            }
                        }

                        using (var inp = new System.IO.FileStream(TempFname, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None))
                        {
                            inp.Seek(0, System.IO.SeekOrigin.Begin);
                            CompressedByteCount = Convert.ToInt32(inp.Length);
                            byte[] CompressedBuffer = new byte[CompressedByteCount];
                            inp.Read(CompressedBuffer, 0, CompressedByteCount);
                            Answer = CompressedBuffer;
                        }
                    }

                    finally
                    {
                        System.IO.File.Delete(TempFname);
                    }
                    break;

                case "ZIP":
                    TempFname = System.IO.Path.GetTempFileName();

                    // Use ZIP to compress the data
                    try
                    {
                        // Open the file for write access
                        using (var cmp = new ICSharpCode.SharpZipLib.Zip.ZipOutputStream(new System.IO.FileStream(TempFname, System.IO.FileMode.Open, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None)))
                        {
                            cmp.SetLevel(9);

                            // Zip files have a name. Add a "canned" value.
                            ICSharpCode.SharpZipLib.Zip.ZipEntry Entry = new ICSharpCode.SharpZipLib.Zip.ZipEntry("submission.xml");
                            Entry.Comment = "Agency Submission";
                            Entry.Size = StringCharacterCount;
                            Entry.CompressionMethod = ICSharpCode.SharpZipLib.Zip.CompressionMethod.Deflated;
                            Entry.DateTime = DateTime.Now;
                            cmp.PutNextEntry(Entry);

                            byte[] BufferArea = new byte[Convert.ToInt32(StringCharacterCount) - 1];
                            BufferArea = (new System.Text.ASCIIEncoding()).GetBytes(SourceData);
                            cmp.Write(BufferArea, 0, Convert.ToInt32(StringCharacterCount));
                            cmp.CloseEntry();
                        }

                        // Open the file for read access
                        using (var inp = new System.IO.FileStream(TempFname, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.None))
                        {
                            CompressedByteCount = Convert.ToInt32(inp.Length);
                            byte[] CompressedBuffer = new byte[CompressedByteCount];
                            inp.Read(CompressedBuffer, 0, CompressedByteCount);
                            Answer = CompressedBuffer;
                        }
                    }

                    finally
                    {
                        System.IO.File.Delete(TempFname);
                    }
                    break;

                default: // "TEXT/XML"
                    Answer = (new System.Text.ASCIIEncoding()).GetBytes(SourceData);
                    break;
            }

            return Answer;
        }
    }
}