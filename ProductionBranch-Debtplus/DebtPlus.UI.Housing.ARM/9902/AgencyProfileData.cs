#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using DebtPlus;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using DebtPlus.UI.Housing.ARM.ARM;

namespace DebtPlus.UI.Housing.ARM
{
    public class AgencyProfileData : DebtPlus.UI.Housing.ARM.HUD.AgencyProfileData, IDisposable
    {
        private Configuration configInfo;
        public AgencyProfileData(Configuration ConfigInfo) : base()
        {
            this.configInfo = ConfigInfo;
        }

        public void LoadAgencyLanguages()
        {
            Agency_Languages.Clear();

            SqlConnection cn = new SqlConnection(DebtPlus.LINQ.BusinessContext.ConnectionString());
            SqlDataReader rd = null;
            try
            {
                cn.Open();
                using (var cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.CommandText = "SELECT DISTINCT dbo.map_hud_9902_language([attribute]) as 'language' FROM housing_arm_hcs_id_languages WHERE [hcs_id] = @hcs_id ORDER BY 1";
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.Add("@hcs_id", SqlDbType.Int).Value = agc_hcs_id;
                    rd = cmd.ExecuteReader(CommandBehavior.CloseConnection | CommandBehavior.SingleResult);
                }

                if (rd != null)
                {
                    while (rd.Read())
                    {
                        Agency_Languages.Add(new DebtPlus.UI.Housing.ARM.HUD.Language(Convert.ToInt32(rd.GetValue(0))));
                    }
                }

            }
            finally
            {
                if (rd != null)
                {
                    rd.Dispose();
                }
                if (cn != null)
                {
                    cn.Dispose();
                }
            }

            // There must be a language. If none defined, use ENGLISH.
            if (Agency_Languages.Count == 0)
            {
                Agency_Languages.Add(new DebtPlus.UI.Housing.ARM.HUD.Language(19));  // ENGLISH
            }
        }

        public void LoadAgencyCounselingMethods()
        {
            Agency_Counseling_Methods.Clear();
            SqlConnection cn = new SqlConnection(DebtPlus.LINQ.BusinessContext.ConnectionString());
            SqlDataReader rd = null;
            try
            {
                cn.Open();
                using (var cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.CommandText = "SELECT DISTINCT dbo.map_hud_9902_contact_method([AppointmentType]) AS 'AppointmentType' FROM housing_arm_hcs_id_AppointmentTypes WHERE [hcs_id] = @hcs_id ORDER BY 1";
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.Add("@hcs_id", SqlDbType.Int).Value = agc_hcs_id;
                    rd = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                }

                while (rd.Read())
                {
                    Agency_Counseling_Methods.Add(new DebtPlus.UI.Housing.ARM.HUD.Counseling_Method(Convert.ToInt32(rd.GetValue(0))));
                }

            }
            finally
            {
                if (rd != null)
                {
                    rd.Dispose();
                }
                if (cn != null)
                {
                    cn.Dispose();
                }
            }

            // There must be a method. If none defined, use FACE-TO-FACE.
            if (Agency_Counseling_Methods.Count == 0)
            {
                Agency_Counseling_Methods.Add(new DebtPlus.UI.Housing.ARM.HUD.Counseling_Method(1));  // FACE-TO-FACE
            }
        }

        public void LoadCounselorTrainingCourses()
        {
            CounselorTrainingCourses.Clear();
            CounselorTrainings.Clear();
            using (var bc = new DebtPlus.LINQ.BusinessContext())
            {
                System.Int32[] aryCounselors = (from cnslr in CounselorProfiles select cnslr.cms_counselor_id).Distinct().ToArray();
                if (aryCounselors.GetUpperBound(0) > 0)
                {
                    var qList = (from t in bc.view_housing_counselor_trainings where aryCounselors.Contains(t.Counselor) select t).ToList();
                    foreach (var q in qList)
                    {
                        // Define the course for the counselor
                        var course = new DebtPlus.UI.Housing.ARM.HUD.CounselorTrainingCourse();
                        course.TrainingCourse = q.TrainingCourse;
                        course.cnslor_training_date = q.cnslor_training_date.GetValueOrDefault();
                        course.cnslor_training_org = q.cnslor_training_org.GetValueOrDefault();
                        course.cnslor_training_sponsor = q.cnslor_training_sponsor.GetValueOrDefault();

                        course.cnslor_training_duration = q.cnslor_training_duration;
                        course.cnslor_training_org_other = q.cnslor_training_org_other;
                        course.cnslor_training_sponsor_other = q.cnslor_training_sponsor_other;
                        course.cnslor_training_title = q.cnslor_training_title;

                        CounselorTrainingCourses.Add(course);

                        // Define the member of the courses
                        var student = new DebtPlus.UI.Housing.ARM.HUD.CounselorTraining();
                        student.counselor = q.Counselor;
                        student.TrainingCertificate = q.TrainingCertificate;
                        student.TrainingCourse = q.TrainingCourse;

                        CounselorTrainings.Add(student);
                    }
                }
            }
        }

        public submissionHeader50 GetHeader50()
        {
            submissionHeader50 hdr = new submissionHeader50();
            hdr.agcHcsId = agc_hcs_id;
            hdr.agcName = configInfo.agc_cms_name;
            hdr.cmsVendorId = configInfo.agc_cms_type;
            hdr.cmsPassword = configInfo.agc_cms_Password;
            return hdr;
        }

        protected virtual void Dispose(bool disposing)
        {
            configInfo = null;
        }

        bool IsDisposed = false;
        public void Dispose()
        {
            try
            {
                if (!IsDisposed)
                {
                    IsDisposed = true;
                    Dispose(true);
                }
            }
            finally
            {
                GC.SuppressFinalize(this);
            }
        }

        ~AgencyProfileData()
        {
            Dispose(false);
        }
    }
}
