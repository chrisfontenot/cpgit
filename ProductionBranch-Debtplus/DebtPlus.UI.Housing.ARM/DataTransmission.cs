﻿#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using DebtPlus;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using DebtPlus.UI.Housing.ARM.ARM;
using DebtPlus.UI.Housing.ARM.HUD;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Housing.ARM
{
    partial class MainForm
    {
        /// <summary>
        /// Send a PING request to the remote service to see if the connection is possible.
        /// </summary>
        private void doPing(AgencyProfileData CurrentAgency)
        {
            var req = new ARM.ping();
            req.agcHcsId = CurrentAgency.agc_hcs_id;
            
            // Construct the output request to send the agency data
            ArmServiceService svc = new ArmServiceService();
            svc.Url = configInfo.URL;

            System.Net.NetworkCredential credentials = new System.Net.NetworkCredential();
            credentials.UserName = CurrentAgency.UserName;
            credentials.Password = CurrentAgency.Password;
            svc.Credentials = credentials;
            svc.PreAuthenticate = true;
            svc.Timeout = Int32.MaxValue;

            try
            {
                // Send the request to the remote system
                var resp = svc.ping(req);
                DebtPlus.Data.Forms.MessageBox.Show(string.Format("Response from Remote: {0}", resp), "Suggessful PING response");
            }
            catch (Exception ex)
            {
                DebtPlus.Data.Forms.MessageBox.Show(ex.Message, "Error PING response");
            }

            finally
            {
                svc.Dispose();
            }

            Application.DoEvents();
        }

        /// <summary>
        /// Wait for the submission response data
        /// </summary>
        /// <param name="svc"></param>
        /// <param name="CurrentAgency"></param>
        /// <param name="SubmissionID"></param>
        /// <returns></returns>
        private Int32 WaitForDONE(ref ArmServiceService svc, DebtPlus.UI.Housing.ARM.HUD.AgencyProfileData CurrentAgency, Int64 SubmissionID)
        {
            Int32 Answer = 2;
            CurrentAgency.SubmissionStatusChanged("Requesting Submission Information");

            getSubmissionInfo req = new getSubmissionInfo();
            req.agcHcsId = CurrentAgency.agc_hcs_id;
            req.submissionId = SubmissionID;

            getSubmissionInfoResponse resp = svc.getSubmissionInfo(req);
            CurrentAgency.SubmissionDateChanged(resp.statusDate);
            CurrentAgency.SubmissionStatusChanged(resp.statusMessage);

            if (resp.statusMessage.Equals("DONE"))
            {
                Answer = 1;
            }
            else if (resp.statusMessage.Contains("ERROR"))
            {
                Answer = 0;
                WriteToErrorLog(configInfo, resp.statusMessage);
            }
            else
            {
                Answer = 2;
            }

            return Answer;
        }

        public void WriteToErrorLog(Configuration configInfo, string ErrorMessage)
        {
            // If there is an error then append it to the error file.
            string FileName = configInfo.ErrorLog;
            if (FileName != string.Empty)
            {
                // Create the empty file
                if (!System.IO.File.Exists(FileName))
                {
                    using (System.IO.StreamWriter fs = System.IO.File.CreateText(FileName))
                    {
                        fs.WriteLine(ErrorMessage);
                    }
                }
                else
                {
                    using (System.IO.StreamWriter fs = System.IO.File.AppendText(FileName))
                    {
                        fs.WriteLine(ErrorMessage);
                    }
                }
            }
        }

        /// <summary>
        /// Send the submission information to HUD
        /// </summary>
        public bool SendHCS_ID(ref AgencyProfileData CurrentAgency, Int32 FiscalYear, Int32 FiscalQuarter, Int32 ReportingMonth, DateTime PeriodStart, DateTime PeriodEnd)
        {
            bool Answer = false;

            // Register the handlers so that they can update the status messages
            CurrentAgency.UpdateSubmissionID += cls_StatusIDUpdate;
            CurrentAgency.UpdateSubmissionStatus += cls_StatusMsgUpdate;
            CurrentAgency.UpdateSubmittedDate += cls_StatusDateUpdate;

            try
            {
                // Update the fields for others to use
                CurrentAgency.fiscal_year = FiscalYear;
                CurrentAgency.fiscal_quarter = FiscalQuarter;
                CurrentAgency.reported_month = ReportingMonth;

                for (; ; )
                {
                    // Collect the additional information as needed
                    ReadReportInformation(ref CurrentAgency, PeriodStart, PeriodEnd);

                    // Always send the agency information
                    if (!SendHCS_ID_Agency(CurrentAgency))
                    {
                        break;
                    }

                    // Follow it up with optional information
                    if (CurrentAgency.Send9902)
                    {
                        if (!SendHCS_ID_Counselors(CurrentAgency))
                        {
                            break;
                        }

                        if (!SendHCS_ID_9902(CurrentAgency))
                        {
                            break;
                        }
                    }

                    // Send the client information
                    if (CurrentAgency.SendClients)
                    {
                        if (!SendHCS_ID_Clients(CurrentAgency))
                        {
                            break;
                        }
                    }

                    Answer = true;
                    break;
                }
            }

            finally
            {
                // Remove the registration
                CurrentAgency.UpdateSubmissionID -= cls_StatusIDUpdate;
                CurrentAgency.UpdateSubmissionStatus -= cls_StatusMsgUpdate;
                CurrentAgency.UpdateSubmittedDate -= cls_StatusDateUpdate;
            }

            return Answer;
        }

        /// <summary>
        /// Send the agency information
        /// </summary>
        /// <param name="CurrentAgency"></param>
        /// <returns></returns>
        public bool SendHCS_ID_Agency(AgencyProfileData CurrentAgency)
        {
            // Load the language and counseling methods for this agency
            CurrentAgency.LoadAgencyCounselingMethods();
            CurrentAgency.LoadAgencyLanguages();

            // Build a list of the contacts for this agency
            List<DebtPlus.UI.Housing.ARM.HUD.agency_contact> ContactList = ReadAgencyContacts(ref CurrentAgency);

            // Now, construct the text block to be sent as the data
            string AgencySubmissionDataText;

            // Extract the information from the structures into the XML document
            StringBuilder sb = new StringBuilder();

            using (var txt = new StringWriterWithEncoding(sb, Encoding.UTF8))
            {
                System.Xml.XmlTextWriter XmlWriter = new System.Xml.XmlTextWriter(txt);
                try
                {
                    XmlWriter.Formatting = Formatting.Indented;
                    XmlWriter.Indentation = 4;
                    XmlWriter.IndentChar = ' ';
                    XmlWriter.Namespaces = true;
                    XmlWriter.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
                    XmlWriter.WriteRaw(Environment.NewLine);

                    // Include the submission data information
                    XmlWriter.WriteRaw("<tns:SubmissionData xsi:schemaLocation='http://gov.hud.arm/agency_profile_databag_5_0 agency_profile_databag_5_0.xsd' xmlns:tns='http://gov.hud.arm/agency_profile_databag_5_0' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>");

                    // Include the agency profile data
                    CurrentAgency.SerializeData(CurrentAgency, XmlWriter);

                    // List the contact information
                    if (ContactList.Count > 0)
                    {
                        XmlWriter.WriteStartElement("tns:Agency_Contacts");
                        foreach (DebtPlus.UI.Housing.ARM.HUD.agency_contact item in ContactList)
                        {
                            item.SerializeData(CurrentAgency, XmlWriter);
                        }
                        XmlWriter.WriteEndElement();
                    }

                    // End the submission data block
                    XmlWriter.WriteRaw("</tns:SubmissionData>");
                }
                finally
                {
                    XmlWriter.Close();
                    txt.Close();
                }
            }

            // Translate the file memory copy to a string.
            AgencySubmissionDataText = sb.ToString();

            // Save the information into the trace file if one is provided
            string FileName = configInfo.TraceFile;
            if (FileName != string.Empty)
            {
                // Include the information for the agency data
                if (FileName.Contains("{"))
                {
                    FileName = string.Format(FileName, CurrentAgency.agc_hcs_id, "AgencyData");
                }

                if (System.IO.File.Exists(FileName))
                {
                    string BackupFile = System.IO.Path.ChangeExtension(FileName, "BAK");
                    if (System.IO.File.Exists(BackupFile))
                    {
                        System.IO.File.Delete(BackupFile);
                    }
                    System.IO.File.Move(FileName, BackupFile);
                }

                using (var OutTrace = new System.IO.FileStream(FileName, System.IO.FileMode.Create))
                {
                    using (var txtFile = new System.IO.StreamWriter(OutTrace))
                    {
                        txtFile.Write(AgencySubmissionDataText);
                        txtFile.Flush();
                    }
                }
            }

            // Construct the output request to send the agency data
            ArmServiceService svc = new ArmServiceService();
            svc.Url = configInfo.URL;
            Int32 Answer = 2;
            try
            {
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential();
                credentials.UserName = CurrentAgency.UserName;
                credentials.Password = CurrentAgency.Password;
                svc.Credentials = credentials;
                svc.PreAuthenticate = true;
                svc.Timeout = Int32.MaxValue;

                postAgencyData req = new postAgencyData();
                req.submissionHeader50 = CurrentAgency.GetHeader50();
                req.submissionHeader50.fiscalYearId = configInfo.map_fiscal_year(ref svc, CurrentAgency, CurrentAgency.fiscal_year);
                req.submissionDataEncoding = configInfo.COMPRESSION;
                req.submissionData = Compression.CompressedData(configInfo.COMPRESSION, AgencySubmissionDataText);

                // Send the request to the remote system
                postSubmissionResponse resp = svc.postAgencyData(req);
                Int64 submissionID = resp.submissionId;

                // Generate the submission trace information
                CurrentAgency.SubmissionIDChanged(submissionID);

                // Now, poll for the response information
                while ((Answer = WaitForDONE(ref svc, CurrentAgency, submissionID)) == 2)
                {
                    System.Threading.Thread.Sleep(configInfo.StatusUpdateInterval);
                }
            }

            finally
            {
                svc.Dispose();
            }

            Application.DoEvents();
            return Answer == 1;
        }

        /// <summary>
        /// Send the 9902 information
        /// </summary>
        /// <param name="CurrentAgency"></param>
        /// <returns></returns>
        public bool SendHCS_ID_9902(AgencyProfileData CurrentAgency)
        {
            Int32 Answer = 2;

            // Construct the output request to send the agency data
            ArmServiceService svc = new ArmServiceService();
            try
            {
                svc.Url = configInfo.URL;

                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential();
                credentials.UserName = CurrentAgency.UserName;
                credentials.Password = CurrentAgency.Password;

                svc.Credentials = credentials;
                svc.PreAuthenticate = true;
                svc.Timeout = Int32.MaxValue;

                // Ensure that the reporting period is properly defined
                postForm9902Data req = new postForm9902Data();
                req.submissionHeader50 = CurrentAgency.GetHeader50();
                req.submissionHeader50.fiscalYearId = configInfo.map_fiscal_year(ref svc, CurrentAgency, CurrentAgency.fiscal_year);
                CurrentAgency.Form9902.report_period_id = configInfo.map_fiscal_quarter(ref svc, CurrentAgency, CurrentAgency.fiscal_quarter);
                req.submissionDataEncoding = configInfo.COMPRESSION;

                // Extract the information from the structures into the XML document
                StringBuilder sb = new StringBuilder();
                using (var txt = new StringWriterWithEncoding(sb, Encoding.UTF8))
                {
                    System.Xml.XmlTextWriter XmlWriter = new System.Xml.XmlTextWriter(txt);
                    try
                    {
                        XmlWriter.Formatting = Formatting.Indented;
                        XmlWriter.Indentation = 4;
                        XmlWriter.IndentChar = ' ';
                        XmlWriter.Namespaces = true;
                        XmlWriter.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
                        XmlWriter.WriteRaw(Environment.NewLine);

                        // Include the submission data information
                        XmlWriter.WriteRaw("<tns:SubmissionData xsi:schemaLocation='http://gov.hud.arm/form_9902_databag_5_0 form_9902_databag_5_0.xsd' xmlns:tns='http://gov.hud.arm/form_9902_databag_5_0' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>");

                        // Write the 9902 form information
                        CurrentAgency.Form9902.SerializeData(CurrentAgency, XmlWriter);

                        // Add the group sessions if there are any
                        if (CurrentAgency.GroupSessions.Count > 0)
                        {
                            XmlWriter.WriteStartElement("tns:Group_Sessions");
                            foreach (DebtPlus.UI.Housing.ARM.HUD.group_session session in CurrentAgency.GroupSessions)
                            {
                                session.SerializeData(CurrentAgency, XmlWriter);
                            }
                            XmlWriter.WriteEndElement();

                            // Write the attendee information if there is any. There can't be if we don't have group sessions.
                            if (CurrentAgency.Attendees.Count > 0)
                            {
                                XmlWriter.WriteStartElement("tns:Attendees");
                                foreach (DebtPlus.UI.Housing.ARM.HUD.attendee person in CurrentAgency.Attendees)
                                {
                                    person.SerializeData(CurrentAgency, XmlWriter);
                                }
                                XmlWriter.WriteEndElement();
                            }
                        }

                        // End the submission data block
                        XmlWriter.WriteRaw("</tns:SubmissionData>");

                    }
                    finally
                    {
                        XmlWriter.Close();
                        txt.Close();
                    }
                }

                // Translate the file memory copy to a string.
                string Form9902SubmissionDataText = sb.ToString();

                // Update the submission data at this point.
                req.submissionData = Compression.CompressedData(configInfo.COMPRESSION, Form9902SubmissionDataText);

                // Save the information into the trace file if one is provided
                string FileName = configInfo.TraceFile;
                if (FileName != string.Empty)
                {

                    // Include the information for the agency data
                    if (FileName.Contains("{"))
                    {
                        FileName = string.Format(FileName, CurrentAgency.agc_hcs_id, "9902");
                    }

                    if (System.IO.File.Exists(FileName))
                    {
                        string BackupFile = System.IO.Path.ChangeExtension(FileName, "BAK");
                        if (System.IO.File.Exists(BackupFile))
                        {
                            System.IO.File.Delete(BackupFile);
                        }
                        System.IO.File.Move(FileName, BackupFile);
                    }

                    using (var OutTrace = new System.IO.FileStream(FileName, System.IO.FileMode.Create))
                    {
                        using (var txtFile = new System.IO.StreamWriter(OutTrace))
                        {
                            txtFile.Write(Form9902SubmissionDataText);
                            txtFile.Flush();
                        }
                    }
                }

                // Send the request to the remote system
                postSubmissionResponse resp = svc.postForm9902Data(req);
                Int64 submissionID = resp.submissionId;

                // Generate the submission trace information
                CurrentAgency.SubmissionIDChanged(submissionID);

                // Now, poll for the response information
                while ((Answer = WaitForDONE(ref svc, CurrentAgency, submissionID)) == 2)
                {
                    System.Threading.Thread.Sleep(configInfo.StatusUpdateInterval);
                }
            }
            finally
            {
                svc.Dispose();
            }

            Application.DoEvents();
            return Answer == 1;
        }

        /// <summary>
        /// Send the counselor information
        /// </summary>
        /// <param name="CurrentAgency"></param>
        /// <returns></returns>
        public bool SendHCS_ID_Counselors(AgencyProfileData CurrentAgency)
        {
            // Load the trainings and training courses
            CurrentAgency.LoadAgencyCounselingMethods();
            CurrentAgency.LoadCounselorTrainingCourses();

            // Now, construct the text block to be sent as the data
            string CounselorSubmissionDataText;

            // Extract the information from the structures into the XML document
            StringBuilder sb = new StringBuilder();

            using (var txt = new StringWriterWithEncoding(sb, Encoding.UTF8))
            {
                System.Xml.XmlTextWriter XmlWriter = new System.Xml.XmlTextWriter(txt);
                try
                {
                    XmlWriter.Formatting = Formatting.Indented;
                    XmlWriter.Indentation = 4;
                    XmlWriter.IndentChar = ' ';
                    XmlWriter.Namespaces = true;
                    XmlWriter.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
                    XmlWriter.WriteRaw(Environment.NewLine);

                    // Include the submission data information
                    XmlWriter.WriteRaw("<tns:SubmissionData xsi:schemaLocation='http://gov.hud.arm/counselor_profile_databag_5_0 counselor_profile_databag_5_0.xsd' xmlns:tns='http://gov.hud.arm/counselor_profile_databag_5_0' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>");

                    // Write the counselor profiles
                    if (CurrentAgency.CounselorProfiles.Count > 0)
                    {
                        XmlWriter.WriteStartElement("tns:Counselor_Profiles");
                        foreach (var counselor in CurrentAgency.CounselorProfiles)
                        {
                            counselor.SerializeData(CurrentAgency, XmlWriter);
                        }
                        XmlWriter.WriteEndElement();
                    }

                    // Write the training courses
                    if (CurrentAgency.CounselorTrainingCourses.Count > 0)
                    {
                        XmlWriter.WriteStartElement("tns:Counselor_Training_Courses");
                        foreach (DebtPlus.UI.Housing.ARM.HUD.CounselorTrainingCourse training_course in CurrentAgency.CounselorTrainingCourses)
                        {
                            training_course.SerializeData(CurrentAgency, XmlWriter);
                        }
                        XmlWriter.WriteEndElement();
                    }

                    // Write the counselor training information
                    if (CurrentAgency.CounselorTrainings.Count > 0)
                    {
                        XmlWriter.WriteStartElement("tns:Counselor_Trainings");
                        foreach (DebtPlus.UI.Housing.ARM.HUD.CounselorTraining training in CurrentAgency.CounselorTrainings)
                        {
                            training.SerializeData(CurrentAgency, XmlWriter);
                        }
                        XmlWriter.WriteEndElement();
                    }

                    // End the submission data block
                    XmlWriter.WriteRaw("</tns:SubmissionData>");
                }

                finally
                {
                    XmlWriter.Close();
                    txt.Close();
                }
            }

            // Translate the file memory copy to a string.
            CounselorSubmissionDataText = sb.ToString();

            // Save the information into the trace file if one is provided
            string FileName = configInfo.TraceFile;
            if (FileName != string.Empty)
            {

                // Include the information for the agency data
                if (FileName.Contains("{"))
                {
                    FileName = string.Format(FileName, CurrentAgency.agc_hcs_id, "Counselors");
                }

                if (System.IO.File.Exists(FileName))
                {
                    string BackupFile = System.IO.Path.ChangeExtension(FileName, "BAK");
                    if (System.IO.File.Exists(BackupFile))
                    {
                        System.IO.File.Delete(BackupFile);
                    }
                    System.IO.File.Move(FileName, BackupFile);
                }

                using (var OutTrace = new System.IO.FileStream(FileName, System.IO.FileMode.Create))
                {
                    using (var txtFile = new System.IO.StreamWriter(OutTrace))
                    {
                        txtFile.Write(CounselorSubmissionDataText);
                        txtFile.Flush();
                    }
                }
            }

            // Construct the output request to send the agency data
            ArmServiceService svc = new ArmServiceService();
            svc.Url = configInfo.URL;
            Int32 Answer = 2;
            try
            {
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential();
                credentials.UserName = CurrentAgency.UserName;
                credentials.Password = CurrentAgency.Password;

                svc.Credentials = credentials;
                svc.PreAuthenticate = true;
                svc.Timeout = Int32.MaxValue;

                postCounselorData req = new postCounselorData();
                req.submissionHeader50 = CurrentAgency.GetHeader50();
                req.submissionHeader50.fiscalYearId = configInfo.map_fiscal_year(ref svc, CurrentAgency, CurrentAgency.fiscal_year);
                req.submissionDataEncoding = configInfo.COMPRESSION;
                req.submissionData = Compression.CompressedData(configInfo.COMPRESSION, CounselorSubmissionDataText);

                // Send the request to the remote system
                postSubmissionResponse resp = svc.postCounselorData(req);
                Int64 submissionID = resp.submissionId;

                // Generate the submission trace information
                CurrentAgency.SubmissionIDChanged(submissionID);

                // Now, poll for the response information
                while ((Answer = WaitForDONE(ref svc, CurrentAgency, submissionID)) == 2)
                {
                    System.Threading.Thread.Sleep(configInfo.StatusUpdateInterval);
                }
            }
            finally
            {
                svc.Dispose();
            }

            Application.DoEvents();
            return Answer == 1;
        }

        /// <summary>
        /// Send the client information
        /// </summary>
        /// <param name="CurrentAgency"></param>
        /// <returns></returns>
        public bool SendHCS_ID_Clients(AgencyProfileData CurrentAgency)
        {
            // Now, construct the text block to be sent as the data
            string ClientSubmissionDataText;

            // Extract the information from the structures into the XML document
            StringBuilder sb = new StringBuilder();

            using (var txt = new StringWriterWithEncoding(sb, Encoding.UTF8))
            {
                System.Xml.XmlTextWriter XmlWriter = new System.Xml.XmlTextWriter(txt);
                try
                {
                    XmlWriter.Formatting = Formatting.Indented;
                    XmlWriter.Indentation = 4;
                    XmlWriter.IndentChar = ' ';
                    XmlWriter.Namespaces = true;
                    XmlWriter.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
                    XmlWriter.WriteRaw(Environment.NewLine);

                    // Include the submission data information
                    XmlWriter.WriteRaw("<tns:SubmissionData xsi:schemaLocation='http://gov.hud.arm/client_profile_databag_5_0 client_profile_databag_5_0.xsd' xmlns:tns='http://gov.hud.arm/client_profile_databag_5_0' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>");

                    // Write the counselor profiles
                    if (CurrentAgency.ClientList.Count > 0)
                    {
                        XmlWriter.WriteStartElement("tns:Client_Profiles");
                        foreach (DebtPlus.UI.Housing.ARM.HUD.client_profile client in CurrentAgency.ClientList)
                        {
                            client.SerializeData(CurrentAgency, XmlWriter);
                        }
                        XmlWriter.WriteEndElement();
                    }

                    // End the submission data block
                    XmlWriter.WriteRaw("</tns:SubmissionData>");
                }

                finally
                {
                    XmlWriter.Close();
                    txt.Close();
                }
            }

            // Translate the file memory copy to a string.
            ClientSubmissionDataText = sb.ToString();

            // Save the information into the trace file if one is provided
            string FileName = configInfo.TraceFile;
            if (FileName != string.Empty)
            {

                // Include the information for the agency data
                if (FileName.Contains("{"))
                {
                    FileName = string.Format(FileName, CurrentAgency.agc_hcs_id, "Clients");
                }

                if (System.IO.File.Exists(FileName))
                {
                    string BackupFile = System.IO.Path.ChangeExtension(FileName, "BAK");
                    if (System.IO.File.Exists(BackupFile))
                    {
                        System.IO.File.Delete(BackupFile);
                    }
                    System.IO.File.Move(FileName, BackupFile);
                }

                using (var OutTrace = new System.IO.FileStream(FileName, System.IO.FileMode.Create))
                {
                    using (var txtFile = new System.IO.StreamWriter(OutTrace))
                    {
                        txtFile.Write(ClientSubmissionDataText);
                        txtFile.Flush();
                    }
                }
            }

            // Construct the output request to send the agency data
            ArmServiceService svc = new ArmServiceService();
            svc.Url = configInfo.URL;
            Int32 Answer = 2;
            try
            {
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential();
                credentials.UserName = CurrentAgency.UserName;
                credentials.Password = CurrentAgency.Password;
                svc.Credentials = credentials;
                svc.PreAuthenticate = true;
                svc.Timeout = Int32.MaxValue;

                postClientData req = new postClientData();
                req.submissionHeader50 = CurrentAgency.GetHeader50();
                req.submissionHeader50.fiscalYearId = configInfo.map_fiscal_year(ref svc, CurrentAgency, CurrentAgency.fiscal_year);
                req.submissionDataEncoding = configInfo.COMPRESSION;
                req.submissionData = Compression.CompressedData(configInfo.COMPRESSION, ClientSubmissionDataText);

                // Send the request to the remote system
                postSubmissionResponse resp = svc.postClientData(req);
                Int64 submissionID = resp.submissionId;

                // Generate the submission trace information
                CurrentAgency.SubmissionIDChanged(submissionID);

                // Now, poll for the response information
                while ((Answer = WaitForDONE(ref svc, CurrentAgency, submissionID)) == 2)
                {
                    System.Threading.Thread.Sleep(configInfo.StatusUpdateInterval);
                }
            }
            finally
            {
                svc.Dispose();
            }

            Application.DoEvents();
            return Answer == 1;
        }
    }
}
