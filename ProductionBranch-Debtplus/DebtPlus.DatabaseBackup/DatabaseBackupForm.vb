#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports System.Data
Imports DebtPlus.Data.Forms
Imports DebtPlus.Utils
Imports System.Data.SqlClient

Friend Class DatabaseBackupForm
    Inherits DebtPlusForm

    Private WithEvents bt As New System.ComponentModel.BackgroundWorker

    Public Sub New()
        MyBase.New()
        InitializeComponent()
        AddHandler Button_Start.Click, AddressOf Button_Start_Click
        AddHandler bt.DoWork, AddressOf bt_DoWork
        AddHandler bt.RunWorkerCompleted, AddressOf bt_RunWorkerCompleted
    End Sub

#Region " Windows Form Designer generated code "

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If components IsNot Nothing Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents Button_Start As DevExpress.XtraEditors.SimpleButton
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DatabaseBackupForm))
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit
        Me.Button_Start = New DevExpress.XtraEditors.SimpleButton

        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()

        '
        'MemoEdit1
        '
        Me.MemoEdit1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MemoEdit1.EditValue = String.Format("{0}Click on the ""Start"" button to initiate the database backup:{0}", Environment.NewLine)
        Me.MemoEdit1.Location = New System.Drawing.Point(8, 9)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.ShowToolTips = False
        Me.MemoEdit1.Size = New System.Drawing.Size(356, 220)
        Me.MemoEdit1.TabIndex = 0
        '
        'Button_Start
        '
        Me.Button_Start.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Button_Start.Location = New System.Drawing.Point(154, 237)
        Me.Button_Start.Name = "Button_Start"
        Me.Button_Start.Size = New System.Drawing.Size(75, 25)
        Me.Button_Start.TabIndex = 1
        Me.Button_Start.Text = "&Start"
        '
        'DatabaseBackupForm
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(376, 270)
        Me.Controls.Add(Me.Button_Start)
        Me.Controls.Add(Me.MemoEdit1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "DatabaseBackupForm"
        Me.ToolTipController1.SetSuperTip(Me, Nothing)
        Me.Text = "Database Backup"

        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Button_Start_Click(ByVal sender As Object, ByVal e As EventArgs)

        If Button_Start.Text = "&Start" Then

            ' Clear the display to remove the "please click here" message.
            With MemoEdit1
                .Text = String.Empty
            End With

            ' Start the thread running to do the backup operation
            bt.WorkerSupportsCancellation = True
            bt.RunWorkerAsync()

            ' Start the thread to perform the backup operation
            Button_Start.Text = "&Abort"

            ' Try to abort the thread.
        ElseIf Button_Start.Text = "&Abort" Then
            BackupAborted = True
            bt.CancelAsync()
            WriteMessage("The backup operation is aborting...")

            ' Finally, look for the close operation
        ElseIf Button_Start.Text = "&Close" Then
            Close()
        End If

    End Sub

    Private Sub bt_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs)

        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
        Dim cmd As SqlCommand = New SqlCommand()
        Try
            WriteMessage("Backup operation started on " + Now.ToShortTimeString)

            cn.Open()

            AddHandler cn.StateChange, AddressOf cn_StateChange
            AddHandler cn.InfoMessage, AddressOf cn_InfoMessage

            Dim strVersion As String

            cmd.Connection = cn
            cmd.CommandText = "SELECT @@version"
            cmd.CommandType = CommandType.Text
            Dim objAnswer As Object = cmd.ExecuteScalar
            strVersion = DebtPlus.Utils.Nulls.DStr(objAnswer)

            WriteMessage(strVersion.Replace(ControlChars.Lf, Environment.NewLine))

            BackupSQL2008(cn)

            WriteMessage(String.Format("{0}Backup operation completed on {1}", Environment.NewLine, Now.ToShortTimeString))

        Catch ex As Exception
            MessageBox.Show(ex.ToString(), "Error backing up the database", MessageBoxButtons.OK)
            WriteMessage(ex.ToString())
            WriteMessage("The backup operation terminated with an error")

        Finally
            RemoveHandler cn.StateChange, AddressOf cn_StateChange
            RemoveHandler cn.InfoMessage, AddressOf cn_InfoMessage

            If cn IsNot Nothing Then cn.Dispose()
            If cmd IsNot Nothing Then cmd.Dispose()
        End Try
    End Sub

    Dim BackupCompleted As Boolean
    Dim BackupAborted As Boolean

    Private Function BackupLocation(ByVal cn As SqlConnection) As String
        Dim BackupDirectory As String = "C:\Backups"

        Dim cmd As SqlCommand = New SqlCommand()

        ' Ask the database for the backup directory. Do not complain about an error here. Just take the default.
        Try
            cmd.Connection = cn
            cmd.CommandText = "SELECT dbo.BackupDirectory()"
            cmd.CommandType = CommandType.Text
            Dim obj As Object = cmd.ExecuteScalar()
            If obj IsNot Nothing AndAlso obj IsNot DBNull.Value Then
                BackupDirectory = Convert.ToString(obj)
            End If

        Catch ex As SqlException

        Finally
            If cmd IsNot Nothing Then cmd.Dispose()
        End Try

        Return System.IO.Path.Combine(BackupDirectory, "b4post.BAK")
    End Function

    Private Sub BackupSQL2008(ByVal cn As SqlConnection)

        ' We have not completed the async backup
        BackupCompleted = False

        ' Form a logon block using the same information as the normal connection block
        Dim svr As New Microsoft.SqlServer.Management.Smo.Server
        svr.ConnectionContext.ConnectTimeout = 4
        svr.ConnectionContext.StatementTimeout = 0
        svr.ConnectionContext.ConnectionString = DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString

        ' Form the backup file
        Dim SaveFile As String = BackupLocation(cn)
        WriteMessage(String.Format("Saving backup to {0}", SaveFile))
        Dim dev As New Microsoft.SqlServer.Management.Smo.BackupDeviceItem(SaveFile, Microsoft.SqlServer.Management.Smo.DeviceType.File)

        ' Add the device to the backup
        Dim bkp As New Microsoft.SqlServer.Management.Smo.Backup
        Try
            With bkp
                AddHandler .PercentComplete, AddressOf ProgressEventHandler
                AddHandler .Complete, AddressOf AsyncBackupCompleted

                WriteMessage("Attempting to add the backup device")
                .Devices.Add(dev)
                WriteMessage("Backup device added")

                .Database = DebtPlus.LINQ.SQLInfoClass.getDefault().Database
                .Action = Microsoft.SqlServer.Management.Smo.BackupActionType.Database
                .Initialize = True
                .PercentCompleteNotification = 10
                .BackupSetDescription = "Before Posting Backup"

                WriteMessage("Sending backup request to the server")
                .SqlBackupAsync(svr)

                WriteMessage("Waiting for the backup to complete...")
                Do While Not BackupCompleted
                    If BackupAborted Then
                        .Abort()
                        Exit Do
                    End If

                    ' Put some delay here to allow others to run
                    Threading.Thread.Sleep(5000)
                Loop
            End With

        Finally
            WriteMessage("Attempting to remove the backup device")
            bkp.Devices.Remove(dev)
            WriteMessage("Backup device removed")
        End Try
    End Sub

    Private Sub ProgressEventHandler(ByVal sender As Object, ByVal e As Microsoft.SqlServer.Management.Smo.PercentCompleteEventArgs)
        WriteMessage(String.Format("Percent complete = {0:f0}%", e.Percent))
    End Sub

    Private Sub AsyncBackupCompleted(ByVal sender As Object, ByVal e As EventArgs)
        BackupCompleted = True
        WriteMessage("The backup operation completed without error")
    End Sub

    Private Sub cn_InfoMessage(ByVal sender As Object, ByVal e As SqlInfoMessageEventArgs)
        Dim sub_strings() As String = e.Message.Split("]"c)

        ' If the string is a message from Microsoft's SQL server then remove the leading source flags.
        If sub_strings.GetUpperBound(0) > 0 Then
            WriteMessage(sub_strings(sub_strings.GetUpperBound(0)))
        Else
            ' Otherwise, just write the message.
            WriteMessage(e.Message)
        End If
    End Sub

    Private Sub cn_StateChange(ByVal sender As Object, ByVal e As StateChangeEventArgs)
        Select Case e.CurrentState
            Case ConnectionState.Broken : WriteMessage("Connection state changed to BROKEN")
            Case ConnectionState.Closed : WriteMessage("Connection state changed to CLOSED")
            Case ConnectionState.Connecting : WriteMessage("Connection state changed to CONNECTING")
            Case ConnectionState.Executing : WriteMessage("Connection state changed to EXECUTING")
            Case ConnectionState.Fetching : WriteMessage("Connection state changed to FETCHING")
            Case ConnectionState.Open : WriteMessage("Connection state changed to OPEN")
        End Select
    End Sub

    Private Delegate Sub WriteMessageDelegate(ByVal MessageText As String)
    Private Sub WriteMessage(ByVal MessageText As String)
        If MemoEdit1.InvokeRequired Then
            Dim ia As IAsyncResult = MemoEdit1.BeginInvoke(New WriteMessageDelegate(AddressOf WriteMessage), New Object() {MessageText})
            Call MemoEdit1.EndInvoke(ia)
        Else
            With MemoEdit1
                .Text += MessageText + Environment.NewLine
            End With
        End If
    End Sub

    Private Sub bt_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs)
        Button_Start.Text = "&Close"
    End Sub

End Class
