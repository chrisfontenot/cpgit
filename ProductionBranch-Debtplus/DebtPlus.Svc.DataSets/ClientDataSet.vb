#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Text
Imports System.Reflection
Imports DebtPlus.Interfaces.Client
Imports DebtPlus.Utils.Format
Imports System.Linq
Imports DebtPlus.LINQ

Public Class ClientDataSet
    Inherits Object
    Implements IClient
    Implements IDisposable

    ' Local storage for the class objects
    Private _privateClientId As Int32
    Private _bc As DebtPlus.LINQ.BusinessContext = Nothing
    Private _clientDataSet As New DataSet("ClientDs")

    ''' <summary>
    ''' Context used to read records from the database into the collections. Since
    ''' records are tagged by the data context, we need one and only one data context
    ''' for these records. They must all be read with the same context to be updated properly.
    ''' </summary>
    ''' <returns>The BusinessContext object for the database entries</returns>
    Public Property bc As DebtPlus.LINQ.BusinessContext
        Get
            Return _bc
        End Get

        Set(value As DebtPlus.LINQ.BusinessContext)
            _bc = value
        End Set
    End Property

    ''' <summary>
    ''' Client deposit records
    ''' </summary>
    ''' <remarks></remarks>
    Public colClientDeposits As System.Collections.Generic.List(Of DebtPlus.LINQ.client_deposit) = Nothing

    ''' <summary>
    ''' List of non-managed debts. These contribute to the expenses but we don't disburse on them.
    ''' </summary>
    ''' <remarks></remarks>
    Public colOtherDebts As System.Collections.Generic.List(Of DebtPlus.LINQ.client_other_debt) = Nothing

    ''' <summary>
    ''' List of client assets
    ''' </summary>
    ''' <remarks></remarks>
    Public colAssets As System.Collections.Generic.List(Of DebtPlus.LINQ.asset) = Nothing

    ''' <summary>
    ''' Collection of people information
    ''' </summary>
    ''' <remarks></remarks>
    Public colPeople As System.Collections.Generic.List(Of DebtPlus.LINQ.people) = Nothing
    Public applicantRecord As DebtPlus.LINQ.people = Nothing

    '' <summary>
    '' Just a generic list of names from the names table to avoid having to read them again.
    '' We search this cache list first and if not found, read it and add it to the list.
    '' </summary>
    '' <remarks></remarks>
    Private colNames As New System.Collections.Generic.List(Of DebtPlus.Interfaces.Names.IName)()
    Private colNamesLockingObject As New Object()

    ''' <summary>
    ''' Retrieve the name from the cached list or the database if needed.
    ''' </summary>
    ''' <param name="key">Key field to the client name</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetNameByID(key As System.Nullable(Of System.Int32)) As DebtPlus.Interfaces.Names.IName

        ' Find the entry in the cache or read it from the database
        Return GetNameByID(bc, key)

    End Function

    ''' <summary>
    ''' Retrieve the name from the cached list or the database if needed.
    ''' </summary>
    ''' <param name="bc">Business context to be used</param>
    ''' <param name="key">Key field to the client name</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetNameByID(bc As DebtPlus.LINQ.BusinessContext, key As System.Nullable(Of System.Int32)) As DebtPlus.Interfaces.Names.IName

        ' If there is no key then there is no match
        If key Is Nothing Then
            Return Nothing
        End If

        ' Determine if we can find a match in the cache list
        SyncLock colNamesLockingObject
            Dim q As DebtPlus.LINQ.Name = colNames.Find(Function(s) s.Id = key.Value)
            If q Is Nothing Then

                ' Attempt to read the name from the database
                q = bc.Names.Where(Function(s) s.Id = key.Value).FirstOrDefault()
                If q IsNot Nothing Then
                    colNames.Add(q)
                End If
            End If

            Return q
        End SyncLock
    End Function

    ''' <summary>
    ''' Current client record
    ''' </summary>
    ''' <remarks></remarks>
    Public clientRecord As DebtPlus.LINQ.client = Nothing

    ''' <summary>
    ''' ACH Information for the client if needed
    ''' </summary>
    ''' <remarks></remarks>
    Public clientACHRecord As DebtPlus.LINQ.client_ach = Nothing

    ''' <summary>
    ''' Client address information
    ''' </summary>
    ''' <remarks></remarks>
    Public clientAddress As DebtPlus.Interfaces.Addresses.IAddress = Nothing

    ''' <summary>
    ''' Initialize the new class object
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()
    End Sub

    ''' <summary>
    ''' Current Client ID number.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ClientId() As Int32 Implements IClient.ClientId
        Get
            Return _privateClientId
        End Get
        Set(ByVal value As Int32)
            _privateClientId = value
        End Set
    End Property

    ''' <summary>
    ''' Pointer to the dataset used by the tables stored here
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ClientDs() As DataSet
        Get
            Return _clientDataSet
        End Get
    End Property

    ''' <summary>
    ''' Table names in the dataset
    ''' </summary>
    Public Class Names
        Public Const action_plans As String = "action_plans"
        Public Const budgets As String = "budgets"
        Public Const client_retention_events As String = "client_retention_events"
        Public Const client_retention_actions As String = "client_retention_actions"
        Public Const sales_files As String = "sales_files"
    End Class

#Region "action plans"
    ''' <summary>
    ''' Action Plans
    ''' </summary>
    <Obsolete("Use LINQ Tables")> _
    Public Function ActionPlansTable() As DataTable
        Dim tbl As DataTable = ClientDs.Tables(Names.action_plans)

        ' If the table is not found then read the information from the database
        If tbl Is Nothing Then
            ReloadActionPlansTable()
            tbl = ClientDs.Tables(Names.action_plans)
        End If

        Return tbl
    End Function

    <Obsolete("Use LINQ Tables")> _
    Public Sub ReloadActionPlansTable()

        Dim gdr As Repository.GetDataResult = Repository.ActionPlans.GetByClientId(ClientDs, Names.action_plans, ClientId)
        If Not gdr.Success Then
            DebtPlus.Svc.Common.ErrorHandling.HandleErrors(gdr)
            Return
        End If

        Dim tbl As DataTable = ClientDs.Tables(Names.action_plans)
        With tbl
            If .PrimaryKey.GetUpperBound(0) < 0 Then .PrimaryKey = New DataColumn() {.Columns("action_plan")}
            With .Columns("action_plan")
                .AutoIncrement = True
                .AutoIncrementSeed = -1
                .AutoIncrementStep = -1
            End With
        End With

    End Sub

    <Obsolete("Use LINQ Tables")> _
    Public Function UpdateActionPlan() As Int32
        If ClientDs.Tables(Names.action_plans) Is Nothing Then Return 0
        Dim ccr As Repository.CommitChangesResult = Repository.ActionPlans.CommitChanges(ClientDs.Tables(Names.action_plans), ClientId)
        Return ccr.RowsAffected
    End Function
#End Region

#Region "budget"
    ''' <summary>
    ''' Return the ID of the current budget for the client
    ''' </summary>
    Public Function CurrentBudget() As Object
        Dim answer As Object = Nothing
        Dim tbl As DataTable = BudgetsTable()
        If tbl IsNot Nothing Then
            Using vue As New DataView(tbl, String.Empty, "date_created desc", DataViewRowState.CurrentRows)
                If vue.Count > 0 Then answer = vue(0)("budget")
            End Using
        End If
        Return answer
    End Function

    ''' <summary>
    ''' Discard the budget table if there is one
    ''' </summary>
    Public Sub RemoveBudgetsTable()
        SyncLock ClientDs
            ClientDs.Tables.Remove(Names.budgets)
        End SyncLock
    End Sub

    ''' <summary>
    ''' Budget information table
    ''' </summary>
    Public Function BudgetsTable() As DataTable

        If ClientDs.Tables(Names.budgets) IsNot Nothing Then Return ClientDs.Tables(Names.budgets)

        Dim gdr As Repository.GetDataResult = Repository.Budgets.GetByClientId(ClientDs, Names.budgets, ClientId)
        If Not gdr.Success Then
            DebtPlus.Svc.Common.ErrorHandling.HandleErrors(gdr)
            Return Nothing
        End If

        With ClientDs.Tables(Names.budgets)
            RemoveHandler .ColumnChanged, AddressOf DataColumnChanged
            AddHandler .ColumnChanged, AddressOf DataColumnChanged
        End With

        Return ClientDs.Tables(Names.budgets)
    End Function
#End Region

#Region "client retention action"
    ''' <summary>
    ''' Client Retention Actions
    ''' </summary>
    Public Function client_retention_actions_table() As DataTable

        If ClientDs.Tables(Names.client_retention_actions) IsNot Nothing Then Return ClientDs.Tables(Names.client_retention_actions)

        Dim gdr As Repository.GetDataResult = Repository.ClientRetentionActions.GetByClientId(ClientDs, Names.client_retention_actions, ClientId)
        If Not gdr.Success Then
            DebtPlus.Svc.Common.ErrorHandling.HandleErrors(gdr)
            Return Nothing
        End If

        With ClientDs.Tables(Names.client_retention_actions)
            .PrimaryKey = New DataColumn() {.Columns("client_retention_action")}
            With .Columns("client_retention_action")
                .AutoIncrement = True
                .AutoIncrementStep = -1
                .AutoIncrementSeed = -1
            End With
            .Columns("message").DefaultValue = String.Empty
        End With
        AddHandler ClientDs.Tables(Names.client_retention_actions).ColumnChanged, AddressOf DataColumnChanged

        Return ClientDs.Tables(Names.client_retention_actions)
    End Function

    Public Function UpdateClientRetentionActions() As Int32
        If ClientDs.Tables(Names.client_retention_actions) Is Nothing Then Return 0
        Dim ccr As Repository.CommitChangesResult = Repository.ClientRetentionActions.CommitChanges(ClientDs.Tables(Names.client_retention_actions))
        Return ccr.RowsAffected
    End Function
#End Region

#Region "client retention events"
    ''' <summary>
    ''' Client Retention Events
    ''' </summary>
    Public Function client_retention_events_table() As DataTable

        If ClientDs.Tables(Names.client_retention_events) IsNot Nothing Then Return ClientDs.Tables(Names.client_retention_events)

        Dim gdr As Repository.GetDataResult = Repository.ClientRetentionEvents.GetByClientId(ClientDs, Names.client_retention_events, ClientId)
        If Not gdr.Success Then
            DebtPlus.Svc.Common.ErrorHandling.HandleErrors(gdr)
            Return Nothing
        End If

        With ClientDs.Tables(Names.client_retention_events)
            .PrimaryKey = New DataColumn() {.Columns("client_retention_event")}
            With .Columns("client_retention_event")
                .AutoIncrement = True
                .AutoIncrementStep = -1
                .AutoIncrementSeed = -1
            End With
            .Columns("amount").DefaultValue = 0D
            .Columns("message").DefaultValue = String.Empty
            .Columns("priority").DefaultValue = 9
            .Columns("expire_date").DefaultValue = Now.AddMonths(3).Date
            .Columns("expire_type").DefaultValue = 1
        End With
        AddHandler ClientDs.Tables(Names.client_retention_events).ColumnChanged, AddressOf DataColumnChanged

        Return ClientDs.Tables(Names.client_retention_events)
    End Function

    Public Function UpdateClientRetentionEvents() As Int32
        If ClientDs.Tables(Names.client_retention_events) Is Nothing Then Return 0
        Dim ccr As Repository.CommitChangesResult = Repository.ClientRetentionEvents.CommitChanges(ClientDs.Tables(Names.client_retention_events), ClientId)
        Return ccr.RowsAffected
    End Function
#End Region

#Region "sales files"
    ''' <summary>
    ''' Sales File information table
    ''' </summary>
    Public Function SalesFileTable() As DataTable

        Dim tbl As DataTable = ClientDs.Tables(Names.sales_files)
        If tbl IsNot Nothing Then Return tbl

        Dim gdr As Repository.GetDataResult = Repository.SalesFiles.GetAllByClientId(ClientDs, Names.sales_files, ClientId)
        If Not gdr.Success Then
            DebtPlus.Svc.Common.ErrorHandling.HandleErrors(gdr)
            Return Nothing
        End If

        tbl = ClientDs.Tables(Names.sales_files)
        AddHandler tbl.ColumnChanged, AddressOf DataColumnChanged

        Return tbl
    End Function

    Public Function UpdateSalesFiles() As Int32

        Dim tbl As DataTable = ClientDs.Tables(Names.sales_files)
        If tbl Is Nothing Then Return 0

        Dim ccr As Repository.CommitChangesResult = Repository.SalesFiles.CommitChanges(tbl)
        Return ccr.RowsAffected
    End Function
#End Region

    Private Function stringify(ByVal input As Object) As String

        ' NULL is special. It is simply NULL and not 'NULL'
        If input Is Nothing Or input Is System.DBNull.Value Then
            Return "NULL"
        End If

        ' Dates are special. They can not be just converted to a string like anything else.
        ' We need to use MM/DD/YYYY for a date.
        If TypeOf input Is System.DateTime Then
            Return String.Format("'{0}'", Convert.ToDateTime(input).ToShortDateString())
        End If

        Return "'" + input.ToString().Replace("'", "''") + "'"
    End Function

#Region " IDisposable Support "

    Private _disposedValue As Boolean
    ' To detect redundant calls

    ''' <summary>
    ''' Dispose of the information in this class
    ''' </summary>
    Public Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not _disposedValue Then
            _disposedValue = True
            If disposing Then
                If _clientDataSet IsNot Nothing Then _clientDataSet.Dispose()
            End If
        End If
        _clientDataSet = Nothing
        bc = Nothing
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

#End Region

#Region "Data Change Event Handling"
    Private ReadOnly HandlerList As New System.Collections.Generic.List(Of DebtPlus.Events.FieldChangedHandler)

    ''' <summary>
    ''' Register a handler to receive notices of data changes
    ''' </summary>
    Public Sub RegisterHandler(ByRef HandlerFunction As DebtPlus.Events.FieldChangedHandler)

        ' Determine if the new item is currently in the list. We don't add duplicates.
        For Each item As DebtPlus.Events.FieldChangedHandler In HandlerList
            If item Is HandlerFunction Then Return
        Next item

        ' Add the new item to the list with a key of a simple counter
        HandlerList.Add(HandlerFunction)
    End Sub

    ''' <summary>
    ''' Remove a previous registration for a datachange event
    ''' </summary>
    Public Sub DeRegisterHandler(ByRef HandlerFunction As DebtPlus.Events.FieldChangedHandler)
        Try
            HandlerList.Remove(HandlerFunction)
        Catch ex As Exception
        End Try
    End Sub

    ''' <summary>
    ''' Tell all of the handlers that we changed a field
    ''' </summary>
    Public Sub EnvokeHandlers(ByVal Sender As Object, ByVal e As DebtPlus.Events.FieldChangedEventArgs)
        For Each item As DebtPlus.Events.FieldChangedHandler In HandlerList
            item.Invoke(Sender, e)
        Next item
    End Sub

    ''' <summary>
    ''' Routine called by the table update procedures
    ''' </summary>
    Public Sub DataColumnChanged(ByVal sender As Object, ByVal e As DataColumnChangeEventArgs)
        With CType(sender, DataTable)
            EnvokeHandlers(sender, New DebtPlus.Events.FieldChangedEventArgs(.TableName + ":" + e.Column.ColumnName, e.ProposedValue))
        End With
    End Sub
#End Region
End Class
