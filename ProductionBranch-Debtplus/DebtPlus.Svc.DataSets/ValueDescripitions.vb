﻿#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Text
Imports System.Reflection
Imports DebtPlus.Interfaces.Client
Imports DebtPlus.Utils.Format
Imports System.Linq
Imports DebtPlus.LINQ

Public Class ValueDescripitions
    Implements IDisposable

    ''' <summary>
    ''' Determine the type of the indicated property for the record object
    ''' </summary>
    ''' <param name="record">Pointer to the record to be used</param>
    ''' <param name="FieldName">Field name in the record</param>
    ''' <returns>The system type for the record's field name</returns>
    ''' <remarks></remarks>
    Public Function GetFieldType(ByVal record As Object, ByVal FieldName As String) As System.Type

        Dim pi As System.Reflection.PropertyInfo = record.GetType.GetProperty(FieldName, BindingFlags.GetField Or BindingFlags.IgnoreCase Or BindingFlags.Instance Or BindingFlags.NonPublic Or BindingFlags.Public Or BindingFlags.Static)
        If pi IsNot Nothing Then
            Return pi.PropertyType
        End If
        Return GetType(System.Object)

    End Function

    ''' <summary>
    ''' Convert the column information to a suitable string
    ''' </summary>
    Public Function ClientStringValue(ByVal ADOcolumn As System.Data.DataColumn, ByVal value As Object) As String
        Return ClientStringValue(ADOcolumn.ColumnName, ADOcolumn.DataType, value)
    End Function

    ''' <summary>
    ''' Convert the column information to a suitable string
    ''' </summary>
    Public Function ClientStringValue(ByVal fieldName As String, ByVal fieldType As System.Type, ByVal fieldValue As Object) As String
        Dim answer As String = String.Empty
        If fieldValue IsNot Nothing AndAlso fieldValue IsNot DBNull.Value Then

            ' Try to convert the value to a string with a conversion function
            Try
                Dim MethodName As String = String.Format("Get{0}Description", fieldName)
                Dim mi As MethodInfo = Me.GetType.GetMethod(MethodName, BindingFlags.Instance Or BindingFlags.Static Or BindingFlags.Public Or BindingFlags.IgnoreCase, Nothing, New Type() {GetType(Object)}, Nothing)
                If mi IsNot Nothing Then
                    answer = Convert.ToString(mi.Invoke(Me, New Object() {fieldValue}))
                End If

            Catch ex As Exception
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
            End Try

            ' Do the conversion using the best method possible.
            If answer = String.Empty Then
                If fieldType Is GetType(String) Then
                    answer = Convert.ToString(fieldValue)
                ElseIf fieldType Is GetType(DateTime) OrElse fieldType Is GetType(Date) Then
                    answer = Convert.ToDateTime(fieldValue).ToShortDateString()
                ElseIf fieldType Is GetType(Boolean) Then
                    answer = Convert.ToBoolean(fieldValue).ToString()
                ElseIf fieldType Is GetType(Decimal) Then
                    answer = String.Format("{0:c}", Convert.ToDecimal(fieldValue))
                ElseIf fieldType Is GetType(Double) Then
                    answer = Convert.ToDouble(fieldValue).ToString()
                Else
                    answer = Convert.ToInt64(fieldValue).ToString()
                End If
            End If
        End If

        Return answer
    End Function

    Public Function GetdisabledDescription(ByVal key As Object) As String
        If key IsNot Nothing AndAlso key IsNot System.DBNull.Value Then
            If Convert.ToInt32(key) = 0 Then
                Return System.Boolean.FalseString
            End If
            Return System.Boolean.TrueString
        End If
        Return String.Empty
    End Function

    Public Function GetcounselorDescription(ByVal key As Object) As String

        If key IsNot Nothing AndAlso key IsNot DBNull.Value Then
            Dim q As DebtPlus.LINQ.counselor = DebtPlus.LINQ.Cache.counselor.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.Name.ToString()
            End If
        End If

        Return String.Empty
    End Function

    Public Function GetcountyDescription(ByVal key As Object) As String

        If key IsNot Nothing AndAlso key IsNot System.DBNull.Value Then
            Dim intKey As Int32 = Convert.ToInt32(key)
            If intKey > 0 Then
                Using dc As New BusinessContext()
                    Dim q = (From c In dc.counties Where c.Id = intKey Select c).FirstOrDefault()
                    If q IsNot Nothing Then
                        Return q.name
                    End If
                End Using
            End If
        End If

        Return String.Empty
    End Function

    Public Function GetcreditagencyDescription(ByVal key As Object) As String

        If key IsNot Nothing AndAlso key IsNot DBNull.Value Then
            Dim strKey As String = Convert.ToString(key).Trim()
            Dim q As DebtPlus.LINQ.InMemory.CreditAgencyType = DebtPlus.LINQ.InMemory.CreditAgencyTypes.getList().Find(Function(s) String.Compare(strKey, s.Id, True) = 0)
            If q IsNot Nothing Then
                Return q.description
            End If
        End If

        Return String.Empty
    End Function

    Public Function GetloandelinquencymonthsDescription(ByVal key As Object) As String

        If key IsNot Nothing AndAlso key IsNot System.DBNull.Value Then
            Dim intKey As Int32 = Convert.ToInt32(key)
            If intKey = 0 Then
                Return "Current"
            End If

            If intKey = 1 Then
                Return "Struggling"
            End If

            If intKey > 0 Then
                Return String.Format("{0:n0}", intKey)
            End If
        End If

        Return String.Empty
    End Function

    Public Function GethousingfinancingDescription(ByVal key As Object) As String

        If key IsNot Nothing AndAlso key IsNot System.DBNull.Value Then
            Dim intKey As Int32 = Convert.ToInt32(key)
            If intKey > 0 Then
                Dim q As DebtPlus.LINQ.Housing_FinancingType = DebtPlus.LINQ.Cache.Housing_FinancingType.getList().Find(Function(s) s.Id = intKey)
                If q IsNot Nothing Then
                    Return q.description
                End If
            End If
        End If

        Return String.Empty
    End Function

    Public Function GethousingloanpositionDescription(ByVal key As Object) As String

        If key IsNot Nothing AndAlso key IsNot System.DBNull.Value Then
            Dim intKey As Int32 = Convert.ToInt32(key)
            If intKey > 0 Then
                Dim q As DebtPlus.LINQ.Housing_LoanPositionType = DebtPlus.LINQ.Cache.Housing_LoanPositionType.getList().Find(Function(s) s.Id = intKey)
                If q IsNot Nothing Then
                    Return q.description
                End If
            End If
        End If

        Return String.Empty
    End Function

    Public Function GethousingresidencyDescription(ByVal key As Object) As String

        If key IsNot Nothing AndAlso key IsNot System.DBNull.Value Then
            Dim intKey As Int32 = Convert.ToInt32(key)
            If intKey > 0 Then
                Dim q As DebtPlus.LINQ.Housing_ResidencyType = DebtPlus.LINQ.Cache.Housing_ResidencyType.getList().Find(Function(s) s.Id = intKey)
                If q IsNot Nothing Then
                    Return q.description
                End If
            End If
        End If

        Return String.Empty
    End Function

    Public Function GetStateMessageTypeDescription(ByVal key As Object) As String
        If key IsNot Nothing AndAlso key IsNot System.DBNull.Value Then
            Dim intKey As Int32 = Convert.ToInt32(key)
            If intKey > 0 Then
                Using dc As New BusinessContext()
                    Dim q As DebtPlus.LINQ.StateMessageType = (From c In dc.StateMessageTypes Where c.Id = intKey Select c).FirstOrDefault()
                    If q IsNot Nothing Then
                        Return q.Description
                    End If
                End Using
            End If
        End If

        Return String.Empty
    End Function

    Public Function GetofficeDescription(ByVal key As Object) As String

        If key IsNot Nothing AndAlso key IsNot DBNull.Value Then
            Dim q As DebtPlus.LINQ.office = DebtPlus.LINQ.Cache.office.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.name
            End If
        End If

        Return String.Empty
    End Function

    Public Function Getdrop_reasonDescription(ByVal key As Object) As String
        If key IsNot Nothing AndAlso key IsNot DBNull.Value Then
            Dim q As DebtPlus.LINQ.drop_reason = DebtPlus.LINQ.Cache.drop_reason.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GeteducationDescription(ByVal key As Object) As String
        If key IsNot Nothing AndAlso key IsNot DBNull.Value Then
            Dim q As DebtPlus.LINQ.EducationType = DebtPlus.LINQ.Cache.EducationType.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GetemployerDescription(ByVal key As Object) As String

        If key IsNot Nothing AndAlso Not System.Object.Equals(System.DBNull.Value, key) Then
            Dim intKey As Int32 = Convert.ToInt32(key)
            If intKey > 0 Then
                Using bc As New BusinessContext()
                    Dim q As DebtPlus.LINQ.employer = (From emp In bc.employers Where emp.Id = intKey Select emp).FirstOrDefault()
                    If q IsNot Nothing Then
                        Return q.name
                    End If
                End Using
            End If
        End If

        Return String.Empty
    End Function

    Public Function GetethnicityDescription(ByVal key As Object) As String
        If key IsNot Nothing AndAlso key IsNot DBNull.Value Then
            Dim q As DebtPlus.LINQ.EthnicityType = DebtPlus.LINQ.Cache.EthnicityType.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GetgenderDescription(ByVal key As Object) As String
        If key IsNot Nothing AndAlso key IsNot DBNull.Value Then
            Dim q As DebtPlus.LINQ.GenderType = DebtPlus.LINQ.Cache.GenderType.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GetfinancialproblemDescription(ByVal key As Object) As String
        If key IsNot Nothing AndAlso key IsNot DBNull.Value Then
            Dim q As DebtPlus.LINQ.financial_problem = DebtPlus.LINQ.Cache.financial_problemType.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GethouseholdDescription(ByVal key As Object) As String
        If key IsNot Nothing AndAlso key IsNot DBNull.Value Then
            Dim q As DebtPlus.LINQ.HouseholdHeadType = DebtPlus.LINQ.Cache.HouseholdHeadType.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GethousinggrantDescription(ByVal key As Object) As String
        If key IsNot Nothing AndAlso key IsNot DBNull.Value Then
            Dim q As DebtPlus.LINQ.Housing_GrantType = DebtPlus.LINQ.Cache.Housing_GrantType.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GetHUDassistance_Description(ByVal key As Object) As String
        If key IsNot Nothing AndAlso key IsNot DBNull.Value Then
            Dim q As DebtPlus.LINQ.Housing_HUDAssistanceType = DebtPlus.LINQ.Cache.Housing_HUDAssistanceType.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function Getno_fico_score_reasonDescription(ByVal key As Object) As String
        If key IsNot Nothing AndAlso key IsNot DBNull.Value Then
            Dim q As DebtPlus.LINQ.Housing_FICONotIncludedReason = DebtPlus.LINQ.Cache.Housing_FICONotIncludedReason.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function Gethousingloandescription(ByVal key As Object) As String
        If key IsNot Nothing AndAlso key IsNot DBNull.Value Then
            Dim q As DebtPlus.LINQ.Housing_LoanType = DebtPlus.LINQ.Cache.Housing_LoanType.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GethousingmortgageDescription(ByVal key As Object) As String
        If key IsNot Nothing AndAlso key IsNot DBNull.Value Then
            Dim q As DebtPlus.LINQ.Housing_MortgageType = DebtPlus.LINQ.Cache.Housing_MortgageType.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GethousingtypeDescription(ByVal key As Object) As String
        If key IsNot Nothing AndAlso key IsNot DBNull.Value Then
            Dim q As DebtPlus.LINQ.HousingType = DebtPlus.LINQ.Cache.HousingType.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GetjobDescription(ByVal key As Object) As String
        If key IsNot Nothing AndAlso key IsNot DBNull.Value Then
            Dim q As DebtPlus.LINQ.job_description = DebtPlus.LINQ.Cache.job_description.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GetlanguageDescription(ByVal key As Object) As String
        If key IsNot Nothing AndAlso key IsNot DBNull.Value Then
            Dim q As DebtPlus.LINQ.AttributeType = DebtPlus.LINQ.Cache.AttributeType.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function Getmarital_statusDescription(ByVal key As Object) As String
        If key IsNot Nothing AndAlso key IsNot DBNull.Value Then
            Dim q As DebtPlus.LINQ.MaritalType = DebtPlus.LINQ.Cache.MaritalType.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GetfrequencyDescription(ByVal key As Object) As String
        If key IsNot Nothing AndAlso key IsNot DBNull.Value Then
            Dim q As DebtPlus.LINQ.PayFrequencyType = DebtPlus.LINQ.Cache.PayFrequencyType.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GetraceDescription(ByVal key As Object) As String
        If key IsNot Nothing AndAlso key IsNot DBNull.Value Then
            Dim q As DebtPlus.LINQ.RaceType = DebtPlus.LINQ.Cache.RaceType.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function Getreferred_byDescription(ByVal key As Object) As String
        If key IsNot Nothing AndAlso key IsNot DBNull.Value Then
            Dim q As DebtPlus.LINQ.referred_by = DebtPlus.LINQ.Cache.referred_by.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GetRegionDescription(ByVal key As Object) As String
        If key IsNot Nothing AndAlso key IsNot DBNull.Value Then
            Dim q As DebtPlus.LINQ.region = DebtPlus.LINQ.Cache.region.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GetRelationDescription(ByVal key As Object) As String
        If key IsNot Nothing AndAlso key IsNot DBNull.Value Then
            Dim q As DebtPlus.LINQ.RelationType = DebtPlus.LINQ.Cache.RelationType.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GetTriStateDescription(ByVal key As Object) As String
        If key IsNot Nothing AndAlso key IsNot DBNull.Value Then
            Dim q As DebtPlus.LINQ.InMemory.TristateType = DebtPlus.LINQ.InMemory.TristateTypes.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Shared Function GetSSNDescription(ByVal key As Object) As String
        Return DebtPlus.Utils.Format.SSN.FormatSSN(key)
    End Function

    Public Function Getcause_fin_problem1Description(ByVal key As Object) As String
        Return GetfinancialproblemDescription(key)
    End Function

    Public Function Getcause_fin_problem2Description(ByVal key As Object) As String
        Return GetfinancialproblemDescription(key)
    End Function

    Public Function Getcause_fin_problem3Description(ByVal key As Object) As String
        Return GetfinancialproblemDescription(key)
    End Function

    Public Function Getcause_fin_problem4Description(ByVal key As Object) As String
        Return GetfinancialproblemDescription(key)
    End Function

    Public Function GetcsrDescription(ByVal key As Object) As String
        Return GetcounselorDescription(key)
    End Function

    Public Function GetElectronicStatementsDescription(ByVal key As Object) As String
        Return GetTriStateDescription(key)
    End Function

    Public Function GetElectronicCorrespondenceDescription(ByVal key As Object) As String
        Return GetTriStateDescription(key)
    End Function

    Public Function GetMilitaryStatusIDDescription(ByVal key As Object) As String
        If key IsNot Nothing AndAlso key IsNot System.DBNull.Value Then
            Dim id As Int32 = Convert.ToInt32(key)
            Dim q As DebtPlus.LINQ.militaryStatusType = DebtPlus.LINQ.Cache.MilitaryStatusType.getList().Find(Function(s) s.Id = id)
            If q IsNot Nothing Then
                Return q.description
            End If
        End If

        Return String.Empty
    End Function

    Public Function GetMilitaryServiceIDDescription(ByVal key As Object) As String

        If key IsNot Nothing AndAlso key IsNot System.DBNull.Value Then
            Dim id As Int32 = Convert.ToInt32(key)
            Dim q As DebtPlus.LINQ.militaryServiceType = DebtPlus.LINQ.Cache.MilitaryServiceType.getList().Find(Function(s) s.Id = id)
            If q IsNot Nothing Then
                Return q.description
            End If
        End If

        Return String.Empty
    End Function

    Public Function GetMilitaryDependentIDDescription(ByVal key As Object) As String

        If key IsNot Nothing AndAlso key IsNot System.DBNull.Value Then
            Dim id As Int32 = Convert.ToInt32(key)
            Dim q As DebtPlus.LINQ.militaryDependentType = DebtPlus.LINQ.Cache.MilitaryDependentType.getList().Find(Function(s) s.Id = id)
            If q IsNot Nothing Then
                Return q.description
            End If
        End If

        Return String.Empty
    End Function

    Public Function GetMilitaryGradeIDDescription(ByVal key As Object) As String

        If key IsNot Nothing AndAlso key IsNot System.DBNull.Value Then
            Dim id As Int32 = Convert.ToInt32(key)
            Dim q As DebtPlus.LINQ.militaryGradeType = DebtPlus.LINQ.Cache.MilitaryGradeType.getList().Find(Function(s) s.Id = id)
            If q IsNot Nothing Then
                Return q.description
            End If
        End If

        Return String.Empty
    End Function

    Public Function Getsatisfaction_scoreDescription(ByVal key As Object) As String

        Dim intKey As Int32 = DebtPlus.Utils.Nulls.v_Int32(key).GetValueOrDefault(0)
        Dim q As DebtPlus.LINQ.InMemory.SatisfactionScoreType = DebtPlus.LINQ.InMemory.SatisfactionScoreTypes.getList().Find(Function(s) s.Id = intKey)
        If q IsNot Nothing Then
            Return q.description.Trim() ' We purposely put leading blanks in the description for sorting reasons.
        End If

        Return key.ToString()
    End Function

#Region "IDisposable Support"
    Private disposedValue As Boolean

    ''' <summary>
    ''' Dispose of the allocated storage if needed
    ''' </summary>
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
            End If
        End If
        Me.disposedValue = True
    End Sub

    ''' <summary>
    ''' Handle the case where the Dispose function is not called
    ''' </summary>
    Protected Overrides Sub Finalize()
        Dispose(False)
        MyBase.Finalize()
    End Sub

    ''' <summary>
    ''' Dispose of locally allocated storage
    ''' </summary>
    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region
End Class
