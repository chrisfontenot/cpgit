Imports System.Text
Imports DebtPlus.Utils.Format

Public Class TicklerDataSet
    Implements IDisposable

    Public ds As New DataSet("ds")

    ' Current date to be displayed
    Public date_effective As Date = Now.Date

    Public Event CounselorChanged As EventHandler

    Protected Overridable Sub OnCounselorChanged(ByVal e As EventArgs)
        RaiseEvent CounselorChanged(Me, e)
    End Sub

    Private _counselorId As Int32 = -1
    Public Property CounselorId() As Int32
        Get
            Return _counselorId
        End Get
        Set(ByVal Value As Int32)
            If _counselorId <> Value Then
                _counselorId = Value
                OnCounselorChanged(New EventArgs)
            End If
        End Set
    End Property

    Private _clientId As Int32 = -1
    Public Property ClientId() As Int32
        Get
            Return _clientId
        End Get
        Set(ByVal Value As Int32)
            If Value <> _clientId Then
                _clientId = Value
                OnClientChanged(New EventArgs)
            End If
        End Set
    End Property

    <Obsolete("Use LINQ tables")> _
    Public Function SetCounselor(ByVal Value As Int32) As Boolean
        CounselorId = Value
        Return True
    End Function

    <Obsolete("Use LINQ tables")> _
    Public Function SetCounselor(ByVal Value As String) As Boolean
        Dim q = DebtPlus.LINQ.Cache.counselor.getList().Find(Function(s) String.Compare(s.Person, Value, True) = 0)
        If q IsNot Nothing Then
            Return SetCounselor(q.Id)
        End If

        Return False
    End Function

    <Obsolete("Use LINQ tables")> _
    Public Function SetCounselor() As Boolean
        Return SetCounselor(DebtPlus.LINQ.BusinessContext.suser_sname())
    End Function

    Private privateCurrentSignon As String = String.Empty

    <Obsolete("Use LINQ tables")> _
    Public Function CurrentSignon() As String
        If privateCurrentSignon = String.Empty Then
            privateCurrentSignon = ReadCurrentSignon()
        End If
        Return privateCurrentSignon
    End Function

    <Obsolete("Use LINQ tables")> _
    Public Function ReadCurrentSignon() As String
        Dim gdr As Repository.GetScalarResult = Repository.SqlServerFunctions.SuserSname()
        If gdr.Success Then
            If gdr.Result IsNot Nothing Then
                Return Convert.ToString(gdr.Result)
            End If
        Else
            DebtPlus.Svc.Common.ErrorHandling.HandleErrors(gdr)
        End If
        Return String.Empty
    End Function

    Public Event ClientChanged As EventHandler

    Protected Overridable Sub OnClientChanged(ByVal e As EventArgs)
        RaiseEvent ClientChanged(Me, e)
    End Sub

    Public Class CounselorListClass
        Implements IComparable
        Public Property NameID() As Int32
        Public Property Office() As Int32
        Public Property TelephoneID() As Int32
        Public Property EmailID() As Int32
        Public Property SignonID() As String
        Public Property Note() As Object
        Public Property Counselor() As Int32
        Public Property Name() As String
        Public Property [Default]() As Boolean
        Public Property ActiveFlag() As Boolean

        Public Shadows Function toString() As String
            Return Name
        End Function

        Public Function CompareTo(ByVal obj As Object) As Int32 Implements IComparable.CompareTo
            Return Name.CompareTo(CType(obj, CounselorListClass).Name)
        End Function
    End Class

    Private ReadCounselorNames_FristTimeOnly As Boolean = True

    <Obsolete("Use LINQ tables")> _
    Public Function CounselorsList() As ArrayList
        Return CounselorsList(DBNull.Value)
    End Function

    <Obsolete("Use LINQ tables")> _
    Public Function CounselorsList(ByVal CurrentCounselor As Object) As ArrayList
        If ReadCounselorNames_FristTimeOnly Then
            ReadCounselorNames_FristTimeOnly = False
            ReadCounselorNames()
        End If

        Return BuildCounselorList("COUNSELOR", CurrentCounselor)
    End Function

    <Obsolete("Use LINQ tables")> _
    Public Function CSRList() As ArrayList
        Return CSRList(DBNull.Value)
    End Function

    <Obsolete("Use LINQ tables")> _
    Public Function CSRList(ByVal CurrentCSR As Object) As ArrayList
        If ReadCounselorNames_FristTimeOnly Then
            ReadCounselorNames_FristTimeOnly = False
            ReadCounselorNames()
        End If

        Return BuildCounselorList("CSR", CurrentCSR)
    End Function

    <Obsolete("Use LINQ tables")> _
    Public Function TotalList() As ArrayList
        Return TotalList(DBNull.Value)
    End Function

    <Obsolete("Use LINQ tables")> _
    Public Function TotalList(ByVal CurrentItem As Object) As ArrayList
        If ReadCounselorNames_FristTimeOnly Then
            ReadCounselorNames_FristTimeOnly = False
            ReadCounselorNames()
        End If

        Return BuildCounselorList(String.Empty, CurrentItem)
    End Function

    <Obsolete("Use LINQ tables")> _
    Private Function BuildCounselorList(ByVal RoleName As String, ByVal CurrentItem As Object) As ArrayList
        Dim lst As New ArrayList

        ' Find the COUNSELOR ROLE in the list of attributes for the system.
        Dim roleTable As DataTable = AttributeTypesTable()
        Dim roleType As Int32 = -1
        If RoleName <> String.Empty Then
            Dim rows() As DataRow = roleTable.Select(String.Format("[Grouping]='ROLE' AND [Attribute]='{0}'", RoleName))
            If rows.GetUpperBound(0) >= 0 Then
                roleType = DebtPlus.Utils.Nulls.DInt(rows(0)("oID"))
            End If
        End If

        ' Read the list of counselor_attributes
        Dim attributesTable As DataTable = CounselorAttributesTable()

        If (attributesTable IsNot Nothing) Then
            ' Buld the list of counselors
            For Each drv As DataRowView In CounselorsTable.DefaultView

                ' Retrieve the counselor ID. Ensure that this is a valid counselor
                Dim Counselor As Int32 = DebtPlus.Utils.Nulls.DInt(drv("counselor"))
                Dim validCounselor As Boolean = (Counselor > 0)
                If validCounselor AndAlso roleType > 0 Then

                    ' If the counselor row is not a member of the desired list, reject it.
                    If roleType > 0 Then
                        Dim rows() As DataRow = attributesTable.Select(String.Format("[counselor]={0:f0} AND [Attribute]={1:f0}", Counselor, roleType))
                        If rows.GetUpperBound(0) < 0 Then
                            validCounselor = False
                        End If
                    End If
                End If

                If validCounselor Then
                    Dim item As New CounselorListClass
                    With item
                        .Counselor = Counselor
                        .EmailID = DebtPlus.Utils.Nulls.DInt(drv("EmailID"))
                        .NameID = DebtPlus.Utils.Nulls.DInt(drv("NameID"))
                        .Office = DebtPlus.Utils.Nulls.DInt(drv("Office"))
                        .TelephoneID = DebtPlus.Utils.Nulls.DInt(drv("TelephoneID"))
                        .Default = DebtPlus.Utils.Nulls.DBool(drv("Default"))
                        .ActiveFlag = DebtPlus.Utils.Nulls.DBool(drv("Activeflag"))
                        .SignonID = DebtPlus.Utils.Nulls.DStr(drv("Person"))
                        .Note = drv("Note")

                        ' Read the name from the system
                        Dim nameRow As DataRow = GetNameRow(.NameID)
                        If (nameRow IsNot Nothing) Then
                            .Name = DebtPlus.LINQ.Name.FormatNormalName(Nothing, nameRow("first"), Nothing, nameRow("last"), Nothing)
                        End If

                        ' Do not include items that are inactive unless they are a match to the current value.
                        validCounselor = item.ActiveFlag
                        If Not validCounselor Then
                            If CurrentItem IsNot Nothing AndAlso CurrentItem IsNot DBNull.Value Then
                                If Convert.ToInt32(CurrentItem) = Counselor Then validCounselor = True
                            End If
                        End If

                        ' Add the item to the list if it is valid to do so
                        If validCounselor Then
                            lst.Add(item)
                        End If
                    End With
                End If
            Next

            ' Put the items in order by name
            lst.Sort()
        End If

        Return lst
    End Function

    <Obsolete("Use LINQ tables")> _
    Public Sub ReadCounselorNames()
        Dim tbl As DataTable = CounselorsTable()
        Dim sb As New StringBuilder
        For Each row As DataRow In tbl.Rows
            Dim nameID As Int32 = DebtPlus.Utils.Nulls.DInt(row("NameID"))
            If nameID > 0 Then
                sb.AppendFormat(",{0:f0}", nameID)
            End If
        Next

        If sb.Length > 0 Then sb.Remove(0, 1)
        If sb.Length > 0 Then
            Const tableName As String = "Names"

            Dim gdr As Repository.GetDataResult = Repository.Names.GetManyNamesByNameIDs(ds, tableName, sb.ToString())
            If Not gdr.Success Then
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(gdr)
                Return
            End If

            tbl = ds.Tables(tableName)
            If tbl.PrimaryKey.GetUpperBound(0) < 0 Then
                tbl.PrimaryKey = New DataColumn() {tbl.Columns("Name")}
                With tbl.Columns("Name")
                    .AutoIncrement = True
                    .AutoIncrementSeed = -1
                    .AutoIncrementStep = -1
                End With
            End If
        End If
    End Sub

    <Obsolete("Use LINQ tables")> _
    Public Function CounselorsTable() As DataTable
        Const tableName As String = "Counselors"

        Dim tbl As DataTable = ds.Tables(tableName)
        If tbl IsNot Nothing Then Return tbl

        Dim gdr As Repository.GetDataResult = Repository.LookupTables.Counselors.GetAll(ds, tableName)
        If Not gdr.Success Then
            DebtPlus.Svc.Common.ErrorHandling.HandleErrors(gdr)
            Return Nothing
        End If

        tbl = ds.Tables(tableName)
        With tbl
            If .PrimaryKey.GetUpperBound(0) < 0 Then .PrimaryKey = New DataColumn() {.Columns("Counselor")}
            With .Columns("Counselor")
                .AutoIncrement = True
                .AutoIncrementSeed = -1
                .AutoIncrementStep = -1
            End With
        End With

        Return tbl
    End Function

    <Obsolete("Use LINQ tables")> _
    Public Function CounselorAttributesTable() As DataTable
        Const tableName As String = "CounselorAttributes"

        Dim tbl As DataTable = ds.Tables(tableName)
        If tbl IsNot Nothing Then Return tbl

        Dim gdr As Repository.GetDataResult = Repository.LookupTables.CounselorAttributes.GetAll(ds, tableName)
        If Not gdr.Success Then
            DebtPlus.Svc.Common.ErrorHandling.HandleErrors(gdr)
            Return Nothing
        End If

        tbl = ds.Tables(tableName)
        If tbl.PrimaryKey.GetUpperBound(0) < 0 Then
            tbl.PrimaryKey = New DataColumn() {tbl.Columns("oID")}
        End If

        Return tbl
    End Function

    <Obsolete("Use LINQ tables")> _
    Public Function AttributeTypesTable() As DataTable
        Const tableName As String = "AttributeTypes"

        Dim tbl As DataTable = ds.Tables(tableName)
        If tbl IsNot Nothing Then Return tbl

        Dim gdr As Repository.GetDataResult = Repository.LookupTables.AttributeTypes.GetAll(ds, tableName)
        If Not gdr.Success Then
            DebtPlus.Svc.Common.ErrorHandling.HandleErrors(gdr)
            Return Nothing
        End If

        tbl = ds.Tables(tableName)
        If tbl.PrimaryKey.GetUpperBound(0) < 0 Then
            tbl.PrimaryKey = New DataColumn() {tbl.Columns("oID")}
        End If

        Return tbl
    End Function

    ''' <summary>
    ''' Find the specific row for the NAMEID
    ''' </summary>
    <Obsolete("Use LINQ tables")> _
    Public Function GetNameRow(ByVal NameID As Int32) As DataRow
        Dim answer As DataRow
        Dim tbl As DataTable = NamesTable(NameID)
        If tbl IsNot Nothing Then
            answer = tbl.Rows.Find(NameID)
        Else
            answer = Nothing
        End If
        Return answer
    End Function

    ' this is substantially the same as Client.BLL.Update.DataClass.NamesTable

    ''' <summary>
    ''' Name table information
    ''' </summary>
    <Obsolete("Use LINQ tables")> _
    Public Function NamesTable(ByVal nameId As Int32) As DataTable
        Const tableName As String = "Names"

        ' If the name row is already loaded then don't read it again.
        Dim tbl As DataTable = ds.Tables(tableName)
        If tbl IsNot Nothing Then
            If tbl.Rows.Find(nameId) IsNot Nothing Then
                Return tbl
            End If
        End If

        Dim gdr As Repository.GetDataResult = Repository.Names.GetNameById(ds, tableName, nameId)
        If Not gdr.Success Then
            DebtPlus.Svc.Common.ErrorHandling.HandleErrors(gdr)
            Return Nothing
        End If

        tbl = ds.Tables(tableName)
        If tbl.PrimaryKey.GetUpperBound(0) < 0 Then
            tbl.PrimaryKey = New DataColumn() {tbl.Columns("Name")}
            With tbl.Columns("Name")
                .AutoIncrement = True
                .AutoIncrementSeed = -1
                .AutoIncrementStep = -1
            End With
        End If
        Return ds.Tables(tableName)
    End Function

    <Obsolete("Use LINQ tables")> _
    Public Function NamesSelectSchema() As DataTable
        Const tableName As String = "Names"

        Dim tbl As DataTable = ds.Tables(tableName)
        If tbl IsNot Nothing Then Return tbl

        Dim gdr As Repository.GetDataResult = Repository.Names.GetSchema(ds, tableName)
        If Not gdr.Success Then
            DebtPlus.Svc.Common.ErrorHandling.HandleErrors(gdr)
            Return Nothing
        End If

        tbl = ds.Tables(tableName)
        If tbl.PrimaryKey.GetUpperBound(0) < 0 Then
            tbl.PrimaryKey = New DataColumn() {tbl.Columns("Name")}
            With tbl.Columns("Name")
                .AutoIncrement = True
                .AutoIncrementSeed = -1
                .AutoIncrementStep = -1
            End With
        End If

        Return tbl
    End Function

    <Obsolete("Use LINQ tables")> _
    Public Sub UpdateNamesTable()
        Repository.Names.CommitChanges(ds.Tables("Names"))
    End Sub

    <Obsolete("Use LINQ tables")> _
    Public Function TickerTypesTable() As DataTable

        Const tableName As String = "TicklerTypes"
        If ds.Tables(tableName) IsNot Nothing Then Return ds.Tables(tableName)

        Dim gdr As Repository.GetDataResult = Repository.LookupTables.Messages.GetAllTicklerMessages(ds, tableName)
        If Not gdr.Success Then
            DebtPlus.Svc.Common.ErrorHandling.HandleErrors(gdr)
            Return Nothing
        End If

        With ds.Tables(tableName)
            If .PrimaryKey.GetUpperBound(0) < 0 Then .PrimaryKey = New DataColumn() {.Columns("oID")}
        End With

        Return ds.Tables(tableName)
    End Function

    <Obsolete("Use LINQ tables")> _
    Public Sub LoadTicklersTableWithClientTicklers()
        Const tableName As String = "ticklers"
        Dim tbl As DataTable = ds.Tables(tableName)
        If tbl IsNot Nothing Then tbl.Clear()

        Dim gdr As Repository.GetDataResult = Repository.Ticklers.GetByClientId(ds, tableName, ClientId)
        If Not gdr.Success Then
            DebtPlus.Svc.Common.ErrorHandling.HandleErrors(gdr)
            Return
        End If
        SetTicklerTableMetaData()
    End Sub

    <Obsolete("Use LINQ tables")> _
    Public Sub LoadTicklersTableWithCounselorTicklers()
        Const tableName As String = "ticklers"

        Dim tbl As DataTable = ds.Tables(tableName)
        If tbl IsNot Nothing Then
            tbl.Clear()
        End If

        Dim gdr As Repository.GetDataResult = Repository.Ticklers.GetByCounselorId(ds, tableName, CounselorId)
        If gdr.Success Then
            SetTicklerTableMetaData()
        Else
            DebtPlus.Svc.Common.ErrorHandling.HandleErrors(gdr)
        End If
    End Sub

    <Obsolete("Use LINQ tables")> _
    Private Sub SetTicklerTableMetaData()
        Const tableName As String = "ticklers"
        Dim tbl As DataTable = ds.Tables(tableName)
        If tbl IsNot Nothing Then
            tbl = ds.Tables(tableName)

            ' Retrieve the tables from the database
            Dim descriptionTable As DataTable = TickerTypesTable()

            ' Add the relation linking the two tables together if needed
            Dim rel As DataRelation = ds.Relations("tickler_tickler_description")
            If rel Is Nothing Then
                rel = New DataRelation("tickler_tickler_description", descriptionTable.Columns("tickler_type"), tbl.Columns("tickler_type"))
                ds.Relations.Add(rel)
            End If

            ' Add the columns to retrieve the formatted information
            With tbl
                If Not .Columns.Contains("tickler_description") Then
                    .Columns.Add("tickler_description", GetType(String), "Parent(tickler_tickler_description).description")
                End If
            End With
        End If
    End Sub

    ''' <summary>
    ''' Return the pointer to the ticklers table
    ''' </summary>
    <Obsolete("Use LINQ tables")> _
    Public Function TicklersTable() As DataTable
        Return ds.Tables("ticklers")
    End Function

    ''' <summary>
    ''' Update the tickler list when appropriate
    ''' </summary>
    <Obsolete("Use LINQ tables")> _
    Public Sub SaveChanges()
        Repository.Ticklers.CommitChanges(ds.Tables("ticklers"))
    End Sub

    ''' <summary>
    ''' Find the default counselor
    ''' </summary>
    <Obsolete("Use LINQ tables")> _
    Public Function DefaultCounselor() As Int32
        Dim answer As Int32
        Dim tbl As DataTable = CounselorsTable()
        Dim rows As DataRow() = tbl.Select("[default]<>0", "counselor")
        If rows.GetUpperBound(0) >= 0 Then
            answer = Convert.ToInt32(rows(0)("counselor"))
        Else
            answer = -1
        End If
        Return answer
    End Function

    Public Class CounselorFormatter
        Implements ICustomFormatter
        Implements IFormatProvider

        Public Function Format(ByVal format1 As String, ByVal arg As Object, ByVal formatProvider As IFormatProvider) As String Implements ICustomFormatter.Format

            If arg IsNot Nothing AndAlso arg IsNot DBNull.Value Then
                Dim intArg As Int32 = Convert.ToInt32(arg)
                If intArg > 0 Then
                    Dim q As DebtPlus.LINQ.counselor = DebtPlus.LINQ.Cache.counselor.getList().Find(Function(s) s.Id = intArg)
                    If q IsNot Nothing AndAlso q.Name IsNot Nothing Then
                        Return q.Name.ToString()
                    End If
                End If
            End If

            Return String.Empty
        End Function

        Public Function GetFormat(formatType As System.Type) As Object Implements System.IFormatProvider.GetFormat
            Return Me
        End Function
    End Class

    <Obsolete("Use LINQ tables")> _
    Public Function GetCouselorFormatter() As IFormatProvider
        Return New CounselorFormatter()
    End Function

#Region " IDisposable Support "

    Private disposedValue As Boolean

    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not disposedValue Then
            If disposing Then
                ds.Dispose()
            End If
        End If
        disposedValue = True
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

#End Region
End Class
