﻿using System;

namespace DebtPlus.Events
{
    /// <summary>
    /// event arguments for the data changing event when it comes from the list. All records in the list
    /// are tied to this event so that you don't need to handle the individual items.
    /// </summary>
    /// <remarks></remarks>
    public class DataChangingEventArgs : System.ComponentModel.CancelEventArgs
    {
        private DataChangingEventArgs()
            : base()
        {
            oldValue = null;
            newValue = null;
            propertyName = string.Empty;
        }

        public DataChangingEventArgs(string propertyName, object oldValue, object newValue)
            : base()
        {
            this.propertyName = propertyName;
            this.oldValue = oldValue;
            this.newValue = newValue;
        }

        /// <summary>
        /// Property name being changed
        /// </summary>
        public string propertyName { get; private set; }

        /// <summary>
        /// Previous value for the property
        /// </summary>
        public object oldValue { get; private set; }

        /// <summary>
        /// Proposed new value for the property
        /// </summary>
        public object newValue { get; private set; }
    }

    /// <summary>
    /// Handler events for the DataChanging from the list of records
    /// </summary>
    /// <param name="sender">Pointer to the list that was changing.</param>
    /// <param name="e">Parameters for the item indicating the change</param>
    /// <remarks></remarks>
    public delegate void DataChangingEventHandler(object sender, DataChangingEventArgs e);
}
