﻿using System;

namespace DebtPlus.Events
{
    /// <summary>
    /// event arguments for the data changed event when it comes from the list. All records in the list
    /// are tied to this event so that you don't need to handle the individual items.
    /// </summary>
    /// <remarks></remarks>
    public class DataChangedEventArgs : System.ComponentModel.PropertyChangedEventArgs
    {
        static DataChangedEventArgs emptyClass = null;
        private DataChangedEventArgs()
            : base(null)
        {
            Handled = false;
        }

        public DataChangedEventArgs(object record, string propertyName)
            : base(propertyName)
        {
            this.Record = record;
            Handled = false;
        }

        public DataChangedEventArgs(string propertyName)
            : base(propertyName)
        {
            Handled = false;
        }

        /// <summary>
        /// Passed along as the "sender" of the DataChanged item to the record update event.
        /// </summary>
        /// <value>The debt record which is indicating the change in the data</value>
        /// <returns></returns>
        /// <remarks></remarks>
        public object Record { get; set; }

        /// <summary>
        /// return an empty object
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public static new DataChangedEventArgs Empty
        {
            get
            {
                if (emptyClass == null)
                {
                    emptyClass = new DataChangedEventArgs();
                }
                return emptyClass;
            }
        }

        /// <summary>
        /// set to TRUE if the event was handled and to not raise the PropertyChanged event.
        /// </summary>
        public bool Handled { get; set; }
    }

    /// <summary>
    /// Handler events for the DataChanged from the list of records
    /// </summary>
    /// <param name="sender">Pointer to the list that was changed.</param>
    /// <param name="e">Parameters for the item indicating the change</param>
    /// <remarks></remarks>
    public delegate void DataChangedEventHandler(object sender, DataChangedEventArgs e);
}
