using System;
using System.Collections.Generic;
using System.Linq;

namespace DebtPlus.Events
{
    [System.Xml.Serialization.XmlType(Namespace = "urn:EmailRecordControl")]
    [System.Xml.Serialization.XmlRoot(Namespace = "urn:EmailRecordControl")]
    public sealed class EmailChangedEventArgs : EventArgs
    {
        [System.Xml.Serialization.XmlIgnore()]
        public Interfaces.Email.IEmailAddress NewEmail { get; private set; }

        [System.Xml.Serialization.XmlIgnore()]
        public Interfaces.Email.IEmailAddress OldEmail { get; private set; }

        private EmailChangedEventArgs()
        {
        }

        public EmailChangedEventArgs(Interfaces.Email.IEmailAddress OldEmail, Interfaces.Email.IEmailAddress NewEmail)
        {
            this.OldEmail = OldEmail;
            this.NewEmail = NewEmail;
        }
    }

    /// <summary>
    /// Delegate to identify the change in the Email settings
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void EmailChangedEventHandler(object sender, EmailChangedEventArgs e);
}
