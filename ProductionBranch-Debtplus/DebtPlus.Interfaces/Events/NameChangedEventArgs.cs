using System;
using System.Collections.Generic;
using System.Linq;

namespace DebtPlus.Events
{
    [System.Xml.Serialization.XmlType(Namespace = "urn:NameRecordControl")]
    [System.Xml.Serialization.XmlRoot(Namespace = "urn:NameRecordControl")]
    public sealed class NameChangeEventArgs : EventArgs
    {
        /// <summary>
        /// The new or changed name
        /// </summary>
        [System.Xml.Serialization.XmlIgnore()]
        public Interfaces.Names.IName NewName { get; private set; }

        /// <summary>
        /// The previous or old name
        /// </summary>
        [System.Xml.Serialization.XmlIgnore()]
        public Interfaces.Names.IName OldName { get; private set; }

        /// <summary>
        /// Create a new instance of the class
        /// </summary>
        private NameChangeEventArgs()
        {
        }

        /// <summary>
        /// Create a new instance of the class
        /// </summary>
        /// <param name="OldName"></param>
        /// <param name="NewName"></param>
        public NameChangeEventArgs(Interfaces.Names.IName OldName, Interfaces.Names.IName NewName)
        {
            this.OldName = OldName;
            this.NewName = NewName;
        }
    }

    /// <summary>
    /// Delegate to handle the name changed event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void NameChangedEventHandler(object sender, NameChangeEventArgs e);
}
