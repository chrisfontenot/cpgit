using System;
using System.Collections.Generic;
using System.Linq;

namespace DebtPlus.Events
{
    /// <summary>
    /// Storage for the control.
    /// </summary>
    /// <remarks></remarks>
    [System.Xml.Serialization.XmlType(Namespace = "urn:TelephoneNumberRecordControl")]
    [System.Xml.Serialization.XmlRoot(Namespace = "urn:TelephoneNumberRecordControl")]
    public sealed class TelephoneNumberChangedEventArgs : EventArgs
    {
        [System.Xml.Serialization.XmlIgnore()]
        public Interfaces.TelephoneNumbers.ITelephoneNumber NewTelephoneNumber { get; private set; }

        [System.Xml.Serialization.XmlIgnore()]
        public Interfaces.TelephoneNumbers.ITelephoneNumber OldTelephoneNumber { get; private set; }

        private TelephoneNumberChangedEventArgs()
        {
        }

        public TelephoneNumberChangedEventArgs(Interfaces.TelephoneNumbers.ITelephoneNumber OldTelephoneNumber, Interfaces.TelephoneNumbers.ITelephoneNumber NewTelephoneNumber)
            : this()
        {
            this.NewTelephoneNumber = NewTelephoneNumber;
            this.OldTelephoneNumber = OldTelephoneNumber;
        }
    }

    /// <summary>
    /// Delegate to identify the change in the Email settings
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void TelephoneNumberChangedEventHandler(object sender, TelephoneNumberChangedEventArgs e);
}
