#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

namespace DebtPlus.Events
{
    /// <summary>
    /// Sent as an event when the letter composer wants a parameter value
    /// </summary>
    /// <remarks></remarks>
    public class ParameterValueEventArgs : System.EventArgs
    {
        // These are well known constants that are passed as parameters
        public const string name_client = "Client";
        public const string name_creditor = "Creditor";
        public const string name_debt = "Debt";
        public const string name_appointment = "Appointment";

        // Parameters passed to "GetValue" for items that the caller should supply as needed
        public const string name_DollarsDisbursed = "dollars_disbursed";
        public const string name_DisbursedCreditors = "disbursed_creditors";
        public const string name_LimitMonthlyMax = "limit_monthly_max";
        public const string name_LimitDisbMax = "limit_disb_max";
        public const string name_LimitDebtBalance = "limit_debt_balance";
        public const string name_Balance = "balance";
        public const string name_SchedPayment = "sched_payment";
        public const string name_DisbursementFactor = "disbursement_factor";
        public const string name_ThisMonthDisbursement = "payments_month_0";
        public const string name_AvailableFunds = "available_funds";
        public const string name_ConfigFee = "config_fee";
        public const string name_InProrate = "in_prorate";
        public const string name_paf_creditor = "paf_creditor";
        public const string name_deduct_creditor = "deduct_creditor";
        public const string name_setup_creditor = "setup_creditor";
        public const string name_total_fees = "total_fees";
        public const string name_debit_amt = "debit_amt";

        // Empty argument structure if needed
        private static ParameterValueEventArgs Result = null;

        /// <summary>
        /// return an empty copy of our structure. A class instance is not needed
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public static new ParameterValueEventArgs Empty
        {
            get
            {
                if (Result == null)
                {
                    Result = new ParameterValueEventArgs(string.Empty);
                }
                return Result;
            }
        }

        /// <summary>
        /// Was a value specified for the parameter?
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Xml.Serialization.XmlIgnore]
        public bool HasValue { get; private set; }

        /// <summary>
        /// This is the ID for the value requested
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Xml.Serialization.XmlIgnore]
        public string Name { get; private set; }

        private object privateValue;
        /// <summary>
        /// This is the value requested
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public object Value
        {
            get
            {
                return privateValue;
            }
            set
            {
                privateValue = value;
                HasValue = true;
            }
        }

        /// <summary>
        /// Colllection of arguments for the expression handler should it be needed
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public System.Collections.ArrayList Arguments { get; private set; }

        public ParameterValueEventArgs()
        {
            Name = string.Empty;
            Value = null;
            HasValue = false;
        }

        public ParameterValueEventArgs(string Name)
            : this()
        {
            this.Name = Name;
            HasValue = false;
        }

        public ParameterValueEventArgs(string Name, object Value)
            : this()
        {
            this.Name = Name;
            this.Value = Value;
            HasValue = false;
        }

        public ParameterValueEventArgs(string Name, System.Collections.ArrayList Arguments)
            : this()
        {
            this.Name = Name;
            this.Arguments = Arguments;
            HasValue = false;
        }

        /// <summary>
        /// A nice definition for debugging
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public override string ToString()
        {
            return string.Format("DebtPlus.Events.ParameterValueEventArgs('{0}'}", Name);
        }
    }

    /// <summary>
    /// Event handler for the event
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="e"></param>
    public delegate void ParameterValueEventHandler(object Sender, ParameterValueEventArgs e);
}