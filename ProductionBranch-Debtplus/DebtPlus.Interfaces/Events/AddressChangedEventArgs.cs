using System;
using System.Collections.Generic;
using System.Linq;

namespace DebtPlus.Events
{
    /// <summary>
    /// Storage for the control.
    /// </summary>
    /// <remarks></remarks>
    [System.Xml.Serialization.XmlType(Namespace = "urn:AddressRecordControl")]
    [System.Xml.Serialization.XmlRoot(Namespace = "urn:AddressRecordControl")]
    public sealed class AddressChangedEventArgs : EventArgs
    {
        [System.Xml.Serialization.XmlIgnore()]
        public Interfaces.Addresses.IAddress NewAddress { get; private set; }

        [System.Xml.Serialization.XmlIgnore()]
        public Interfaces.Addresses.IAddress OldAddress { get; private set; }

        private AddressChangedEventArgs()
        {
        }

        public AddressChangedEventArgs(Interfaces.Addresses.IAddress NewAddress, Interfaces.Addresses.IAddress OldAddress)
            : this()
        {
            this.NewAddress = NewAddress;
            this.OldAddress = OldAddress;
        }
    }

    /// <summary>
    /// Delegate to identify the change in the Email settings
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void AddressChangedEventHandler(object sender, AddressChangedEventArgs e);
}
