#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

namespace DebtPlus.Events
{
    public class FieldChangedEventArgs : System.EventArgs
    {
        /// <summary>
        /// Split the combined table and field into a table name
        /// </summary>
        public static string Table(FieldChangedEventArgs e)
        {
            string[] names = e.FieldName.Split(':');
            if (names.GetUpperBound(0) >= 0)
            {
                return names[0];
            }
            return e.FieldName;
        }

        /// <summary>
        /// Split the combined table and field into a field name
        /// </summary>
        public static string Field(FieldChangedEventArgs e)
        {
            string[] names = e.FieldName.Split(':');
            if (names.GetUpperBound(0) >= 1)
            {
                return names[1];
            }
            return string.Empty;
        }

        /// <summary>
        /// Combine the table and field names into the single change event argument.
        /// </summary>
        public static string Join(string Table, string Field)
        {
            if (string.IsNullOrEmpty(Table) || string.IsNullOrEmpty(Field))
            {
                return string.Empty;
            }
            return string.Join(":", new string[] { Table, Field });
        }

        static FieldChangedEventArgs emptyStructure = null;

        /// <summary>
        /// Return an empty structure if it is needed.
        /// </summary>
        public new static FieldChangedEventArgs Empty
        {
            get
            {
                if (emptyStructure == null)
                {
                    emptyStructure = new FieldChangedEventArgs();
                }
                return emptyStructure;
            }
        }

        /// <summary>
        /// Name of the field being changed
        /// </summary>
        public string FieldName { get; private set; }

        /// <summary>
        /// New value for the item
        /// </summary>
        public object NewValue { get; private set; }

        /// <summary>
        /// Create an instance of the class
        /// </summary>
        public FieldChangedEventArgs()
        {
            FieldName = string.Empty;
            NewValue = null;
        }

        /// <summary>
        /// Create an instance of the class
        /// </summary>
        /// <param name="FieldName"></param>
        public FieldChangedEventArgs(string FieldName)
            : this(FieldName, null)
        {
        }

        /// <summary>
        /// Create an instance of the class
        /// </summary>
        /// <param name="FieldName"></param>
        /// <param name="NewValue"></param>
        public FieldChangedEventArgs(string FieldName, object NewValue)
            : this()
        {
            this.FieldName = FieldName;
            this.NewValue = NewValue;
        }

        /// <summary>
        /// Convert the item to a displayable string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("DebtPlus.Events.FieldChangedEventArgs('{0}')", FieldName);
        }
    }

    /// <summary>
    /// Delegate to receive the events of our class
    /// </summary>
    /// <param name="Sender"></param>
    /// <param name="e"></param>
    public delegate void FieldChangedHandler(object Sender, FieldChangedEventArgs e);
}