﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Interfaces.Names
{
    public interface IName
    {
        int Id { get; set; }
        string Prefix { get; set; }
        string First { get; set; }
        string Middle { get; set; }
        string Last { get; set; }
        string Suffix { get; set; }
    }
}
