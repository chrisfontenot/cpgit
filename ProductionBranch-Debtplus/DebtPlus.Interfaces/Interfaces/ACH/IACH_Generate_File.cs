﻿namespace DebtPlus.Interfaces
{
    public interface IACH_Generate_File
    {
        string transaction_code { get; set; }
        string routing_number { get; set; }
        string account_number { get; set; }
        decimal amount { get; set; }
        int client { get; set; }
        string client_name { get; set; }
    }
}
