﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Interfaces.Email
{
    public interface IEmailAddress
    {
        int Id { get; set; }
        string Address { get; set; }
        int Validation { get; set; }
    }
}
