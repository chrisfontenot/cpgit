namespace DebtPlus.Interfaces
{
    /// <summary>
    /// Interface to indicate that we support this event
    /// </summary>
    public interface ISupportQueryValue
    {
        event DebtPlus.Events.ParameterValueEventHandler QueryValue;
    }
}
