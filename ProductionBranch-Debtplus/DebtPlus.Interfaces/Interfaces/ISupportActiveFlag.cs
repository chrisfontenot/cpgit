namespace DebtPlus.Interfaces
{
    public interface ISupportActiveFlag
    {
        bool ActiveFlag { get; set; }
    }
}
