﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Interfaces.Debt
{
    public interface IDebtInfoSourceCollection
    {
        string account_number { get; }
        System.Nullable<int> balance_client_creditor { get; }

        bool balance_verification_release { get; }

        string balance_verify_by { get; }
        System.Nullable<System.DateTime> balance_verify_date { get; }
        System.Nullable<bool> ccl_agency_account { get; }

        System.Nullable<bool> ccl_always_disburse { get; }

        System.Nullable<bool> ccl_prorate { get; }

        System.Nullable<bool> ccl_zero_balance { get; }

        int check_payments { get; }
        int client { get; }
        System.Nullable<int> client_creditor_balance { get; }

        System.Nullable<int> client_creditor_proposal { get; }

        string client_name { get; }

        string contact_name { get; }

        string cr_creditor_name { get; }

        string cr_division_name { get; }

        System.Nullable<decimal> cr_min_accept_amt { get; }

        System.Nullable<double> cr_min_accept_pct { get; }

        System.Nullable<char> cr_payment_balance { get; }

        string created_by { get; }

        string creditor { get; }

        System.Nullable<double> creditor_interest { get; }

        string creditor_name { get; }

        System.Nullable<decimal> current_sched_payment { get; }

        System.DateTime date_created { get; }

        System.Nullable<System.DateTime> date_disp_changed { get; }

        decimal disbursement_factor { get; }

        System.Nullable<double> dmp_interest { get; }

        System.Nullable<double> dmp_payout_interest { get; }

        System.Nullable<System.DateTime> drop_date { get; }

        System.Nullable<int> drop_reason { get; }

        System.Nullable<System.DateTime> drop_reason_sent { get; }

        System.Nullable<System.DateTime> expected_payout_date { get; }

        System.Nullable<double> fairshare_pct_check { get; }

        System.Nullable<double> fairshare_pct_eft { get; }

        System.Nullable<int> first_payment { get; }

        System.Nullable<decimal> first_payment_amt { get; }

        System.Nullable<System.DateTime> first_payment_date { get; }

        string first_payment_type { get; }

        bool hold_disbursements { get; }

        int Id { get; }
        decimal interest_this_creditor { get; }
        bool irs_form_on_file { get; }
        int IsActive { get; }

        string last_communication { get; }
        System.Nullable<int> last_payment { get; }

        System.Nullable<decimal> last_payment_amt { get; }

        System.Nullable<System.DateTime> last_payment_date { get; }

        System.Nullable<System.DateTime> last_payment_date_b4_dmp { get; }
        string last_payment_type { get; }

        System.Nullable<System.DateTime> last_stmt_date { get; }
        int line_number { get; }
        string message { get; }
        int months_delinquent { get; }
        double non_dmp_interest { get; }
        decimal non_dmp_payment { get; }
        System.Nullable<decimal> orig_balance { get; }

        System.Nullable<decimal> orig_balance_adjustment { get; }

        System.Nullable<decimal> orig_dmp_payment { get; }
        System.Nullable<decimal> payments_month_0 { get; }

        System.Nullable<decimal> payments_month_1 { get; }

        decimal payments_this_creditor { get; }
        System.Nullable<double> percent_balance { get; }
        System.Nullable<int> person { get; }
        System.Nullable<System.DateTime> prenote_date { get; }
        int priority { get; }
        System.Nullable<decimal> proposal_balance { get; }

        System.Nullable<int> proposal_status { get; }

        decimal returns_this_creditor { get; }
        string rpps_client_type_indicator { get; }

        decimal sched_payment { get; }
        int send_bal_verify { get; }
        bool send_drop_notice { get; }
        System.Nullable<System.DateTime> start_date { get; }
        bool student_loan_release { get; }
        System.Nullable<int> terms { get; }
        System.Nullable<decimal> total_interest { get; }

        System.Nullable<decimal> total_payments { get; }

        System.Nullable<decimal> total_sched_payment { get; }

        System.Nullable<System.DateTime> verify_request_date { get; }

        decimal current_balance { get; }
        string creditor_type { get; }
        decimal debit_amt { get; }
        Int32? oID { get; }
    }
}
