using System;

namespace DebtPlus.Interfaces.Debt
{
    public enum DebtTypeEnum
    {
        Normal          = 0,    // Normal, disbursable creditor (i.e. "Macy's")
        AgencyFee       = 1,    // An agency account, i.e. a book, one-time setup fee, etc. PRORATEABLE!
        FixedAgencyFee  = 2,    // An agency account, i.e. a book, one-time setup fee, etc. NON-PRORATEABLE
        MonthlyFee      = 3,    // The monthly re-occurring fee for managing the account. There can only be one per client as it is addressed by the client record itself.
        SetupFee        = 4,    // Account setup fee
        Secured         = 5,    // A secured, non-disbursable debt that we don't manage
        NonManaged      = 6,    // Some other non-managed, non-disbursable debt
        Fairshare       = 7,    // Special agency account to hold the creditor deduct fair-share amounts. Only on client 0, creditor Z9999.
        PaymentCushion  = 8     // Not a real debt but a holder for a cushion on the payments
    }

    public interface IProratable : Disbursement.IDisburseable
    {
        DebtTypeEnum DebtType               { get; set; }
        decimal sched_payment               { get; set; }
        decimal payments_month_0            { get; set; }
        decimal total_interest              { get; set; }
        Int32 priority                      { get; set; }
        bool IsActive                       { get; set; }
        decimal disbursement_factor         { get; set; }
        bool IsPayoff                       { get; set; }
        decimal adjusted_original_balance   { get; }
        decimal current_balance             { get; }
        bool IsProratable                   { get; }
        bool IsFee                          { get; }
    }
}
