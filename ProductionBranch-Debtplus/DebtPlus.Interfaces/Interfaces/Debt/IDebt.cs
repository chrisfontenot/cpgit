using System;

namespace DebtPlus.Interfaces.Debt
{
    public interface IDebt
    {
        Int32 DebtId { get; set; }
    }
}
