using System;
using System.Windows.Forms;

namespace DebtPlus.Interfaces.Debt
{
    public interface IDebtCreate : IDisposable
    {
        Int32 DebtId { get; }
        DialogResult ShowDialog(Int32 ClientId);
        DialogResult ShowDialog(Int32 ClientId, IWin32Window owner);
    }
}
