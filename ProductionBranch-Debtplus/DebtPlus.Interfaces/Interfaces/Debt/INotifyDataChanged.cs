using System;
using System.ComponentModel;

namespace DebtPlus.Interfaces.Debt
{
    public interface INotifyDataChanged : INotifyPropertyChanged
    {
        void RaisePropertyChanged(string PropertyName);
        event Events.DataChangedEventHandler DataChanged;
    }
}
