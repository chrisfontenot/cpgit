using System;

namespace DebtPlus.Interfaces.Debt
{
    public interface IDebtRecordList
    {
        double Threshold { get; set; }
        decimal MinimumPayment { get; set; }
        decimal MaximumPayment { get; set; }
    }
}
