using System;

namespace DebtPlus.Interfaces.Debt
{
    public struct SpecialCreditors
    {
        public string MonthlyFeeCreditor;
        public string DeductCreditor;
        public string SetupCreditor;
    }
}
