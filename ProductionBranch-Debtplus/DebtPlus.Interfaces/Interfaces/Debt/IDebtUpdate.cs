using System;
using System.Windows.Forms;

namespace DebtPlus.Interfaces.Debt
{
    public interface IDebtUpdate
    {
        DialogResult ShowEditDialog(Int32 Debt);
        DialogResult ShowEditDialog(IDebtRecord DebtRecord);
    }
}
