using System;

namespace DebtPlus.Interfaces.Debt
{
    public interface IFeeable
    {
        object QueryFeeValue(string item);
    }
}
