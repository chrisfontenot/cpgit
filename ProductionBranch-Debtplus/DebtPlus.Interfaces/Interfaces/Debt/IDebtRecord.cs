using System;

namespace DebtPlus.Interfaces.Debt
{
    public interface IDebtRecord : IDisposable, Client.IClient, Creditor.ICreditor, IDebt
    {
        DebtTypeEnum DebtType { get; set; }
        void SetDebtType(SpecialCreditors SpecialCreditorList);
        IDebtRecordList Parent { get; set; }

        decimal MinimumPayment();
        decimal MinimumPayment(decimal CurrentBalance);
        decimal MaximumPayment();
        decimal MaximumPayment(decimal CurrentBalance);

        Int32 balance_client_creditor { get; set; }
        Int32 check_payments { get; set; }
        Int32 client_creditor { get; set; }
        Int32 client_creditor_balance { get; set; }
        Int32 client_creditor_proposal { get; set; }
        Int32 first_payment { get; set; }
        Int32 last_payment { get; set; }
        Int32 line_number { get; set; }
        Int32 months_delinquent { get; set; }
        Int32 person { get; set; }
        Int32 priority { get; set; }
        Int32 proposal_status { get; set; }
        Int32 send_bal_verify { get; set; }
        Int32 terms { get; set; }

        decimal display_disbursement_factor { get; set; }

        decimal cr_min_accept_amt { get; set; }
        decimal current_sched_payment { get; set; }
        decimal disbursement_factor { get; set; }
        decimal first_payment_amt { get; set; }
        decimal interest_this_creditor { get; set; }
        decimal last_payment_amt { get; set; }
        decimal last_stmt_balance { get; set; }
        decimal non_dmp_payment { get; set; }
        decimal orig_balance { get; set; }
        decimal orig_balance_adjustment { get; set; }
        decimal orig_dmp_payment { get; set; }
        decimal payments_month_0 { get; set; }
        decimal payments_month_1 { get; set; }
        decimal payments_this_creditor { get; set; }
        decimal proposal_balance { get; set; }
        decimal returns_this_creditor { get; set; }
        decimal sched_payment { get; set; }
        decimal total_interest { get; set; }
        decimal total_payments { get; set; }
        decimal total_sched_payment { get; set; }

        double cr_min_accept_pct { get; set; }
        double non_dmp_interest { get; set; }
        double percent_balance { get; set; }

        string account_number { get; set; }
        string balance_verify_by { get; set; }
        string client_name { get; set; }
        string contact_name { get; set; }
        string cr_creditor_name { get; set; }
        string cr_division_name { get; set; }
        string cr_payment_balance { get; set; }
        string created_by { get; set; }
        string creditor_name { get; set; }
        string first_payment_type { get; set; }
        string last_communication { get; set; }
        string last_payment_type { get; set; }

        bool ccl_agency_account { get; set; }
        bool ccl_always_disburse { get; set; }
        bool ccl_prorate { get; set; }
        bool ccl_zero_balance { get; set; }
        bool hold_disbursements { get; set; }
        bool irs_form_on_file { get; set; }
        bool IsActive { get; set; }
        bool send_drop_notice { get; set; }
        bool student_loan_release { get; set; }

        DateTime date_created { get; set; }

        object balance_verification_release { get; set; }
        object balance_verify_date { get; set; }
        object creditor_interest { get; set; }
        object date_disp_changed { get; set; }
        object date_proposal_prenoted { get; set; }
        object dmp_interest { get; set; }
        object dmp_payout_interest { get; set; }
        object drop_date { get; set; }
        object drop_reason { get; set; }
        object drop_reason_date { get; set; }
        object drop_reason_sent { get; set; }
        object expected_payout_date { get; set; }
        object first_payment_date { get; set; }
        object last_payment_date { get; set; }
        object last_payment_date_b4_dmp { get; set; }
        object last_stmt_date { get; set; }
        object Message { get; set; }
        object payment_rpps_mask { get; set; }
        object prenote_date { get; set; }
        object rpps_client_type_indicator { get; set; }
        object rpps_mask { get; set; }
        object start_date { get; set; }
        object verify_request_date { get; set; }
        object fairshare_pct_check { get; set; }
        object fairshare_pct_eft { get; set; }
    }
}
