﻿using System;

namespace DebtPlus.Interfaces.Debt
{
    public interface IDebtRecordLoad
    {
        void LoadRecord(System.Type RecordType, ref System.Data.DataRow row);
        void LoadRecord(System.Type RecordType, ref System.Data.IDataReader rdr);
    }
}
