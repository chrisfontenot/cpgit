using System;

namespace DebtPlus.Interfaces.Debt
{
    public interface IProratableList : IFeeable
    {
        IProratable MonthlyFeeDebt      { get; }
        IProratable PaymentCushionDebt  { get; }
        decimal TotalPayments();
        decimal TotalFees();
    }
}
