﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Interfaces.TelephoneNumbers
{
    public interface ITelephoneNumber
    {
        int Id { get; set; }
        System.Nullable<int> Country { get; set; }
        string Acode { get; set; }
        string Number { get; set; }
        string Ext { get; set; }
    }
}
