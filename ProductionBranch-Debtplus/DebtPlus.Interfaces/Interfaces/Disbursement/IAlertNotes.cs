using System;

namespace DebtPlus.Interfaces.Disbursement
{
    public interface IAlertNotes : IDisposable
    {
        Int32 ShowAlerts(Int32 ClientID);
        Int32 ShowAlerts(Int32 ClientID, Int32 DisbursementRegister);
        Int32 ShowAlerts(Int32 ClientID, bool SHowOnlyNewNotes);
        Int32 ShowAlerts(Int32 ClientID, Int32 DisbursementRegister, bool SHowOnlyNewNotes);
    }
}
