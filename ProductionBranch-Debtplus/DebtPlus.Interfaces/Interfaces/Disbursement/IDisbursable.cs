using System;

namespace DebtPlus.Interfaces.Disbursement
{
    public interface IDisburseable
    {
        bool IsHeld { get; set; }
        decimal debit_amt { get; set; }
    }
}
