﻿namespace DebtPlus.Interfaces.Addresses
{
    public interface IAddress
    {
        int Id { get; set; }
        string creditor_prefix_1 { get; set; }
        string creditor_prefix_2 { get; set; }
        string house { get; set; }
        string direction { get; set; }
        string street { get; set; }
        string suffix { get; set; }
        string modifier { get; set; }
        string modifier_value { get; set; }
        string address_line_2 { get; set; }
        string address_line_3 { get; set; }
        string city { get; set; }
        int state { get; set; }
        string PostalCode { get; set; }
    }
}
