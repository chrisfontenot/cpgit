using System;

namespace DebtPlus.Interfaces.Creditor
{
    public interface ICreditorControls
    {
        void ReadForm();
        void SaveForm();
        void HandleMenuClick(string ItemName);
    }
}
