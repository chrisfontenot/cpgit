using System;

namespace DebtPlus.Interfaces.Creditor
{
    public interface ICreditor
    {
        string Creditor { get; set; }
    }
}
