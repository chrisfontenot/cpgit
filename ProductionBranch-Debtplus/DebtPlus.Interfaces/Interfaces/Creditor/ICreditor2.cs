using System;

namespace DebtPlus.Interfaces.Creditor
{
    // Newer interface that does not confuse the namespace "creditor" with the property "creditor".
    public interface ICreditor2
    {
        string creditorKey { get; set; }
    }
}
