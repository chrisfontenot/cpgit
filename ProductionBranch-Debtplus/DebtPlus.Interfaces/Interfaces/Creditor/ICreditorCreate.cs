using System;
using System.Windows.Forms;

namespace DebtPlus.Interfaces.Creditor 
{
    public interface ICreditorCreate : ICreditor, System.IDisposable
    {
        DialogResult ShowDialog();
        DialogResult ShowDialog(IWin32Window Owner);
    }
}
