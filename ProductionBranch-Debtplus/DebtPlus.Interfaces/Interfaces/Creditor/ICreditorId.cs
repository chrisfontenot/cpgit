using System;
using System.Windows.Forms;

namespace DebtPlus.Interfaces.Creditor 
{
    public interface ICreditorId
    {
        Int32 CreditorId { get; set; }
    }
}
