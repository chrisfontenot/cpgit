using System;
using System.Windows.Forms;

namespace DebtPlus.Interfaces.Creditor 
{
    public interface ICreditorSearch
    {
        string Creditor { get; }
        DialogResult ShowDialog();
        DialogResult ShowDialog(IWin32Window Owner);
        string FindCreditorByID(Int32 CreditorLabel);
    }
}
