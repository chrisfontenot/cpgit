using System;

namespace DebtPlus.Interfaces
{
    public interface IAccountNumberValidate
    {
        bool IsValidAccount(string Creditor, string AccountNumber);
        bool IsValidAccount(Int32 Creditor, string AccountNumber);
        string LastError { get; }
        string ValidatedAccountNumber { get; }
    }
}
