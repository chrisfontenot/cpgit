using System;

namespace DebtPlus.Interfaces.Client
{
    public interface IAlertNotes : IDisposable
    {
        Int32 ShowAlerts(Int32 ClientID);
        Int32 ShowAlerts(Int32 ClientID, object ClientCreditor);
    }
}
