﻿// -----------------------------------------------------------------------
// <copyright file="IPeople.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace DebtPlus.Interfaces.Client
{
    public interface IPeople
    {
        System.Nullable<int>             NameID { get; set; }
        System.Nullable<int>             EmailID { get; set; }
        string                           Former { get; set; }
        string                           SSN { get; set; }
        int                              Gender { get; set; }
        int                              Race { get; set; }
        int                              Ethnicity { get; set; }
        bool                             Disabled { get; set; }
        int                              MilitaryServiceID { get; set; }
        int                              MilitaryDependentID { get; set; }
        int                              MilitaryGradeID { get; set; }
        int                              MilitaryStatusID { get; set; }
        int                              Education { get; set; }
        System.Nullable<System.DateTime> Birthdate { get; set; }
        System.Nullable<int>             FICO_Score { get; set; }
        string                           CreditAgency { get; set; }
        System.Nullable<System.DateTime> emp_start_date { get; set; }
        System.Nullable<System.DateTime> emp_end_date { get; set; }
        System.Nullable<int>             job { get; set; }
        string                           other_job { get; set; }
        System.Nullable<System.DateTime> bkfileddate { get; set; }
        System.Nullable<System.DateTime> bkdischarge { get; set; }
        System.Nullable<int>             bkPaymentCurrent { get; set; }
        System.Nullable<System.DateTime> bkAuthLetterDate { get; set; }
        System.Nullable<int>             bkchapter { get; set; }
    }
}
