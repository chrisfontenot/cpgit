using System;

namespace DebtPlus.Interfaces.Client
{
    public interface IClient
    {
        Int32 ClientId { get; set; }
    }
}
