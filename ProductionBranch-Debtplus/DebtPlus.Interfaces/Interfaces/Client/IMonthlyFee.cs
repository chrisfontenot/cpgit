using System;
using System.Windows.Forms;

namespace DebtPlus.Interfaces.Client
{
    public interface IMonthlyFee : IDisposable, ISupportQueryValue
    {
        decimal FeeAmount();
    }
}
