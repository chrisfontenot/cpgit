using System;
using System.Windows.Forms;

namespace DebtPlus.Interfaces.Client
{
    public interface IClientCreate
    {
        Int32 ClientId { get; }
        DialogResult ShowDialog(IWin32Window owner);
        DialogResult ShowDialog();
    }
}
