using System;

namespace DebtPlus.Interfaces.Client
{
    public interface IClientControls
    {
        /// <summary>
        /// Read the form information when the tab page is opening. Called every time tab page is switched.
        /// </summary>
        void ReadForm();

        /// <summary>
        /// Save the form information when the tab page is closing. Called as needed.
        /// </summary>
        void SaveForm();

        /// <summary>
        /// Do one-time initialization on the page when it is created. Called only once.
        /// </summary>
        void LoadForm();
    }
}
