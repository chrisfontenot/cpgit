using System;

namespace DebtPlus.Interfaces.Client
{
    public interface IClientUpdate
    {
        /// <summary>
        /// Display the client update form.
        /// </summary>
        /// <param name="ClientId">Key to the clients table. The client id.</param>
        /// <param name="isCreate">TRUE if the form is for a new client, FALSE for an existing client.</param>
        System.Windows.Forms.DialogResult ShowEditDialog(Int32 ClientId, bool isCreated);

        /// <summary>
        /// Display the client update form.
        /// </summary>
        /// <param name="ClientId">Key to the clients table. The client id.</param>
        System.Windows.Forms.DialogResult ShowEditDialog(Int32 ClientId);
    }
}
