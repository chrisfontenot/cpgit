using System;
using System.Windows.Forms;

namespace DebtPlus.Interfaces.Client
{
    public interface IClientSearch : IClient, IDisposable
    {
        event System.ComponentModel.CancelEventHandler Validating;
        DialogResult ShowDialog();
        DialogResult ShowDialog(IWin32Window owner);
    }
}
