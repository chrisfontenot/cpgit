namespace DebtPlus.Interfaces.Reports
{
    public interface ICreateReport
    {
        IReports CreateReport(string reportDefinition);
    }
}
