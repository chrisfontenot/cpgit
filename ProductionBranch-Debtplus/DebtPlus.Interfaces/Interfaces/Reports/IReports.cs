﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace DebtPlus.Interfaces.Reports
{
    public interface IReports : IDisposable
    {
        // Report title and subtitle information displayed on the report.
        String ReportTitle { get; }
        String ReportSubTitle { get; }

        // Allow the parameters to be changed by the user?
        bool AllowParameterChangesByUser { get; set; }

        // Set a report parameter by name.
        void SetReportParameter(string parameterName, object value);

        // Request the report parameter dialog
        DialogResult RequestReportParameters();

        // Convert the parameters to a dictionary.
        Dictionary<string, string> ParametersDictionary();

        // Linkage to request parameters and run the report
        void RunReport();
        void RunReportInSeparateThread();

        // Display the setup page for the report
        DialogResult DisplayPageSetupDialog();

        // Display the preview for the report
        void DisplayPreviewDialog();

        // Bypass preview and go directly to the printer
        void PrintDocument();
        void PrintDocument(String printerName);

        // Some general creation routines to export the document to various formats
        void CreateRtfDocument(String path);
        void CreateRtfDocument(Stream stream);

        void CreatePdfDocument(String path);
        void CreatePdfDocument(Stream stream);
    }
}
