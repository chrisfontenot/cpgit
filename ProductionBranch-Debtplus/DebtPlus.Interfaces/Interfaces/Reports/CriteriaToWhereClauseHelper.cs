using System;
using System.Diagnostics;
using DevExpress.Data.Filtering;

namespace DebtPlus.Interfaces.Reports
{
    public class CriteriaToWhereClauseHelper
    {
        /// <summary>
        /// How should the wild card escape sequences be appended to the propertytype string?
        /// </summary>
        /// <remarks></remarks>
        private enum EscapeCodeEnum
        {
            None = 0,
            Left = 1,
            Right = 2,
            Both = 3
        }

        /// <summary>
        /// Decompile the current criteria operator
        /// </summary>
        /// <param name="Criteria">Criteria to be decompiled</param>
        /// <returns>The resulting string</returns>
        /// <remarks></remarks>
        public static string GetDataSetWhere(CriteriaOperator Criteria)
        {
            return GetDataSetWhere(Criteria, EscapeCodeEnum.None);
        }

        /// <summary>
        /// Decompile the current criteria operator
        /// </summary>
        /// <param name="Criteria">Criteria to be decompiled</param>
        /// <param name="EscapeCode">Value for the method of escaping a string value</param>
        /// <returns></returns>
        /// <remarks></remarks>
        private static string GetDataSetWhere(CriteriaOperator Criteria, EscapeCodeEnum EscapeCode)
        {
            // Accept the case where there is no criteria
            if (DevExpress.Data.Filtering.CriteriaOperator.ReferenceEquals(Criteria, null))
            {
                return string.Empty;
            }

            // if this is an operand value then we need to look at the "LIKE" field to find a wildcard sequence
            if (Criteria is OperandValue)
            {
                return DecompileOperandValue(((OperandValue)Criteria), EscapeCode);
            }

            // Look at the type. Parse the items based upon the type of the critera
            if (Criteria is FunctionOperator)
            {
                return DecompileFunctionOperator(((FunctionOperator)Criteria));
            }

            if (Criteria is OperandProperty)
            {
                return DecompileOperandProperty(((OperandProperty)Criteria));
            }

            if (Criteria is GroupOperator)
            {
                return DecompileGroupOperator(((GroupOperator)Criteria));
            }

            if (Criteria is BinaryOperator)
            {
                return DecompileBinaryOperator(((BinaryOperator)Criteria));
            }

            if (Criteria is BetweenOperator)
            {
                return DecompileBetweenOperator(((BetweenOperator)Criteria));
            }

            if (Criteria is UnaryOperator)
            {
                return DecompileUniaryOperator(((UnaryOperator)Criteria));
            }

            // For some, the standard logic works just fine....
            if (Criteria is InOperator)
            {
                return Criteria.ToString();
            }

            Debug.Assert(false);
            return string.Empty;
        }

        /// <summary>
        /// Decompile a function operator
        /// </summary>
        /// <param name="Criteria">Criteria to be decompiled</param>
        /// <returns></returns>
        /// <remarks></remarks>
        private static string DecompileFunctionOperator(FunctionOperator Criteria)
        {
            string Left = GetDataSetWhere(Criteria.Operands[0]);
            string Right;

            switch (Criteria.OperatorType)
            {
                case FunctionOperatorType.IsNull:
                    return string.Format("{0} IS NULL", Left);

                case FunctionOperatorType.IsNullOrEmpty:
                    return string.Format("({0} IS NULL) OR ({0} = '')", Left);

                case FunctionOperatorType.StartsWith:
                    Right = GetDataSetWhere(Criteria.Operands[1], EscapeCodeEnum.Right);
                    if (Right != string.Empty)
                    {
                        return string.Format("{0} LIKE {1}", Left, Right);
                    }
                    break;

                case FunctionOperatorType.EndsWith:
                    Right = GetDataSetWhere(Criteria.Operands[1], EscapeCodeEnum.Right);
                    if (Right != string.Empty)
                    {
                        return string.Format("{0} LIKE {1}", Left, Right);
                    }
                    break;

                case FunctionOperatorType.Contains:
                    Right = GetDataSetWhere(Criteria.Operands[1], EscapeCodeEnum.Both);
                    if (Right != string.Empty)
                    {
                        return string.Format("{0} LIKE {1}", Left, Right);
                    }
                    break;

                default:
                    Debug.Assert(false);
                    break;
            }
            return string.Empty;
        }

        /// <summary>
        /// Decompile a uniary operator
        /// </summary>
        /// <param name="Criteria">Criteria to be decompiled</param>
        /// <returns></returns>
        /// <remarks></remarks>
        private static string DecompileUniaryOperator(UnaryOperator Criteria)
        {
            string Right = GetDataSetWhere(Criteria.Operand);

            if (Right == string.Empty)
            {
                return string.Empty;
            }

            switch (Criteria.OperatorType)
            {
                case UnaryOperatorType.IsNull:
                    return string.Format("{0} IS NULL", Right);
                case UnaryOperatorType.Minus:
                    return string.Format("0 - ({0})", Right);
                case UnaryOperatorType.Not:
                    return string.Format("NOT ({0})", Right);
                case UnaryOperatorType.Plus:
                    return Right;
                default:
                    break;
            }

            Debug.Assert(false);
            return string.Empty;
        }

        /// <summary>
        /// Decompile a between operator
        /// </summary>
        /// <param name="Criteria">Criteria to be decompiled</param>
        /// <returns></returns>
        /// <remarks></remarks>
        private static string DecompileBetweenOperator(BetweenOperator Criteria)
        {
            string LeftOperand = GetDataSetWhere(Criteria.TestExpression);
            string LowerLimit = GetDataSetWhere(Criteria.BeginExpression);
            string UpperLimit = GetDataSetWhere(Criteria.EndExpression);

            // Expand the case as needed
            return string.Format("({0}>={1}) AND ({0}<={2})", LeftOperand, LowerLimit, UpperLimit);
        }

        /// <summary>
        /// Decompile a binary operator
        /// </summary>
        /// <param name="Criteria">Criteria to be decompiled</param>
        /// <returns></returns>
        /// <remarks></remarks>
        private static string DecompileBinaryOperator(BinaryOperator Criteria)
        {
            string Left = GetDataSetWhere(Criteria.LeftOperand);
            string Right = GetDataSetWhere(Criteria.RightOperand);
            if (Right == string.Empty)
            {
                return string.Empty;
            }

            switch (Criteria.OperatorType)
            {
                case BinaryOperatorType.Equal:
                    return string.Format("{0}={1}", Left, Right);

                case BinaryOperatorType.GreaterOrEqual:
                    return string.Format("{0}>={1}", Left, Right);

                case BinaryOperatorType.Less:
                    return string.Format("{0}<{1}", Left, Right);

                case BinaryOperatorType.LessOrEqual:
                    return string.Format("{0}<={1}", Left, Right);

                case BinaryOperatorType.NotEqual:
                    return string.Format("{0}<>{1}", Left, Right);

                case BinaryOperatorType.Plus:
                    return string.Format("{0}+{1}", Left, Right);

                case BinaryOperatorType.Minus:
                    return string.Format("{0}-{1}", Left, Right);

                case BinaryOperatorType.Divide:
                    return string.Format("{0}/{1}", Left, Right);

                case BinaryOperatorType.Multiply:
                    return string.Format("{0}*{1}", Left, Right);

                case BinaryOperatorType.Modulo:
                    return string.Format("{0}%{1}", Left, Right);

                case BinaryOperatorType.Like:
                    return string.Format("{0} LIKE {1}", Left, Right);

                default:
                    break;
            }

            Debug.Assert(false);
            return string.Empty;
        }

        /// <summary>
        /// Decompile a group operator
        /// </summary>
        /// <param name="Criteria">Criteria to be decompiled</param>
        /// <returns></returns>
        /// <remarks></remarks>
        private static string DecompileGroupOperator(GroupOperator Criteria)
        {
            string Left = GetDataSetWhere(Criteria.Operands[0]);
            string Right = GetDataSetWhere(Criteria.Operands[1]);

            switch (Criteria.OperatorType)
            {
                case GroupOperatorType.And:
                    if (Left != string.Empty && Right != string.Empty)
                    {
                        return string.Format("({0}) AND ({1})", Left, Right);
                    }
                    break;

                case GroupOperatorType.Or:
                    if (Left != string.Empty && Right != string.Empty)
                    {
                        return string.Format("({0}) OR ({1})", Left, Right);
                    }
                    break;
            }

            return Left + Right;
        }

        /// <summary>
        /// Decompile an operand value operator
        /// </summary>
        /// <param name="Criteria">Criteria to be decompiled</param>
        /// <param name="EscapeCode"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private static string DecompileOperandValue(OperandValue Criteria, EscapeCodeEnum EscapeCode)
        {

            // Allow for the NOTHING value to be used. Just return an empty string for later testing.
            if (Criteria.Value == null)
            {
                return string.Empty;
            }

            string StringValue;

            // Handle the date constants if needed
            if (Criteria.Value is DateTime)
            {
                StringValue = string.Format("#{0:d}#", Criteria.Value);

                // Look for a string. Quote the string.
            }
            else if (Criteria.Value is string)
            {
                System.Text.StringBuilder ValueString = new System.Text.StringBuilder(((string)Criteria.Value));

                if (EscapeCode != EscapeCodeEnum.None)
                {

                    // First, replace the brackets with a bracket
                    ValueString.Replace("[", "[[");
                    ValueString.Replace("]", "[]]");
                    ValueString.Replace("[[", "[[]");

                    // Then replace the wildcard characters with a bracket sequence
                    ValueString.Replace("*", "[*]");
                    ValueString.Replace("%", "[%]");
                }

                // Add the leading and trailing wildcard characters if needed
                if (EscapeCode == EscapeCodeEnum.Both || EscapeCode == EscapeCodeEnum.Left)
                {
                    ValueString.Insert(0, "%");
                }

                if (EscapeCode == EscapeCodeEnum.Both || EscapeCode == EscapeCodeEnum.Right)
                {
                    ValueString.Append("%");
                }

                // Re-quote all quote characters
                ValueString.Replace("'", "''");

                // Then insert the leading and trailing quotes
                ValueString.Insert(0, '\'');
                ValueString.Append('\'');

                // The result is now proper.
                StringValue = ValueString.ToString();
            }
            else
            {
                // Other values, such as numbers are not quoted.
                StringValue = Criteria.Value.ToString();
            }
            return StringValue;
        }

        /// <summary>
        /// Decompile an operand property operator
        /// </summary>
        /// <param name="Criteria">Criteria to be decompiled</param>
        /// <returns></returns>
        /// <remarks></remarks>
        private static string DecompileOperandProperty(OperandProperty Criteria)
        {
            return string.Format("[{0}]", Criteria.PropertyName);
        }
    }
}
