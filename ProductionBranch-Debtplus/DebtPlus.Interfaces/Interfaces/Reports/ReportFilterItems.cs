﻿using System;
using DevExpress.Data.Filtering;

namespace DebtPlus.Interfaces.Reports
{
    public class ReportFilterItems : IDisposable
    {
        public bool IsEnabled { get; set; }

        private CriteriaOperator _privateCriteriaFilter;
        public CriteriaOperator CriteriaFilter
        {
            get { return _privateCriteriaFilter; }
            set { _privateCriteriaFilter = value; }
        }

        public String ViewFilter
        {
            get { return CriteriaToWhereClauseHelper.GetDataSetWhere(CriteriaFilter);  }
        }

#region IDisposable
        private bool _disposedValue;
        protected void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    _privateCriteriaFilter = null;
                    IsEnabled = false;
                }
            }
            _disposedValue = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
#endregion

    }
}

