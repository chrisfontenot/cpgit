﻿namespace DebtPlus.Interfaces.Reports
{
    public interface IReportFilter
    {
        ReportFilterItems ReportFilter { get; set; }
    }
}
