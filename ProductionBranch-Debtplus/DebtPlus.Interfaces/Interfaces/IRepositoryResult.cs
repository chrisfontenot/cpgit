﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Interfaces
{
    public interface IRepositoryResult : IDisposable
    {
        string DataServiceMethod { get; }
        Exception ex { get; set; }
        bool Success { get; }
        Int32 RowsAffected { get; set; }
        IRepositoryResult HandleException(Exception ex);
    }
}
