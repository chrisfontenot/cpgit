﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Interfaces
{
    public interface IArgParser
    {
        bool Parse(string[] args);
    }
}
