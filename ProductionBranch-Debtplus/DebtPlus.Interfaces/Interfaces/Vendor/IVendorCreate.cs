using System;
using System.Windows.Forms;

namespace DebtPlus.Interfaces.Vendor 
{
    public interface IVendorCreate : IVendor, System.IDisposable
    {
        DialogResult ShowDialog();
        DialogResult ShowDialog(IWin32Window Owner);
    }
}
