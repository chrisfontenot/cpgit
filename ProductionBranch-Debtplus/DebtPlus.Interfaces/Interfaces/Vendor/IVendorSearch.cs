using System;
using System.Windows.Forms;

namespace DebtPlus.Interfaces.Vendor 
{
    public interface IVendorSearch
    {
        System.Nullable<System.Int32> Vendor { get; }
        DialogResult ShowDialog();
        DialogResult ShowDialog(IWin32Window Owner);
    }
}
