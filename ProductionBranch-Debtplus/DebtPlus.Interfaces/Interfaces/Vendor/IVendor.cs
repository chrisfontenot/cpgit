using System;

namespace DebtPlus.Interfaces.Vendor
{
    public interface IVendor
    {
        System.Nullable<System.Int32> Vendor { get; set; }
    }
}
