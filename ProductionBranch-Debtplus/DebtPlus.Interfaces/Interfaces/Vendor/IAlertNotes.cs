using System;
using System.Windows.Forms;

namespace DebtPlus.Interfaces.Vendor
{
    public interface IAlertNotes : IDisposable
    {
        Int32 ShowAlerts(Int32 CreditorID);
        Int32 ShowAlerts(string CreditorLabel);
    }
}
