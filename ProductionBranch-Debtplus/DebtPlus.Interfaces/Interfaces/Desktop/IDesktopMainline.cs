﻿using System;

namespace DebtPlus.Interfaces.Desktop
{
    public interface IDesktopMainline
    {
        void OldAppMain(string[] args);
    }
}
