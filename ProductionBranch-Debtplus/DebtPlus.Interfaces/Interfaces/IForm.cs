using DevExpress.Utils;
using DevExpress.XtraEditors.DXErrorProvider;
using System.Windows.Forms;

namespace DebtPlus.Interfaces
{
    public interface IForm
    {
        ToolTipController GetToolTipController { get; }
        DXErrorProvider GetErrorProvider { get; }
    }
}
