namespace DebtPlus.UI.FormLib.Check
{
    partial class ReconcileBatchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.Button_New = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Select = new DevExpress.XtraEditors.SimpleButton();
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.GridColumn_Batch = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Note = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_DateCreated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_CreatedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem_Grid = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_Button_Select = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_Button_New = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_Button_Cancel = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_Button_Select)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_Button_New)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_Button_Cancel)).BeginInit();
            this.SuspendLayout();
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(409, 70);
            this.Button_Cancel.MaximumSize = new System.Drawing.Size(75, 25);
            this.Button_Cancel.MinimumSize = new System.Drawing.Size(75, 25);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 25);
            this.Button_Cancel.StyleController = this.layoutControl1;
            this.Button_Cancel.TabIndex = 7;
            this.Button_Cancel.Text = "&Cancel";
            // 
            // Button_New
            // 
            this.Button_New.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_New.Location = new System.Drawing.Point(409, 41);
            this.Button_New.MaximumSize = new System.Drawing.Size(75, 25);
            this.Button_New.MinimumSize = new System.Drawing.Size(75, 25);
            this.Button_New.Name = "Button_New";
            this.Button_New.Size = new System.Drawing.Size(75, 25);
            this.Button_New.StyleController = this.layoutControl1;
            this.Button_New.TabIndex = 6;
            this.Button_New.Text = "&New ...";
            // 
            // Button_Select
            // 
            this.Button_Select.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Select.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Button_Select.Location = new System.Drawing.Point(409, 12);
            this.Button_Select.MaximumSize = new System.Drawing.Size(75, 25);
            this.Button_Select.MinimumSize = new System.Drawing.Size(75, 25);
            this.Button_Select.Name = "Button_Select";
            this.Button_Select.Size = new System.Drawing.Size(75, 25);
            this.Button_Select.StyleController = this.layoutControl1;
            this.Button_Select.TabIndex = 5;
            this.Button_Select.Text = "&Select";
            // 
            // GridControl1
            // 
            this.GridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GridControl1.Location = new System.Drawing.Point(12, 12);
            this.GridControl1.MainView = this.GridView1;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.Size = new System.Drawing.Size(393, 206);
            this.GridControl1.TabIndex = 4;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GridView1});
            // 
            // GridView1
            // 
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(133)))), ((int)(((byte)(195)))));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(109)))), ((int)(((byte)(189)))));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(139)))), ((int)(((byte)(206)))));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.GridColumn_Batch,
            this.GridColumn_Note,
            this.GridColumn_DateCreated,
            this.GridColumn_CreatedBy});
            this.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.GridView1.GridControl = this.GridControl1;
            this.GridView1.Name = "GridView1";
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedRow;
            // 
            // GridColumn_Batch
            // 
            this.GridColumn_Batch.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_Batch.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_Batch.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_Batch.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_Batch.Caption = "Batch";
            this.GridColumn_Batch.DisplayFormat.FormatString = "f0";
            this.GridColumn_Batch.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_Batch.FieldName = "Id";
            this.GridColumn_Batch.GroupFormat.FormatString = "f0";
            this.GridColumn_Batch.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_Batch.Name = "GridColumn_Batch";
            this.GridColumn_Batch.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_Batch.Visible = true;
            this.GridColumn_Batch.VisibleIndex = 0;
            this.GridColumn_Batch.Width = 86;
            // 
            // GridColumn_Note
            // 
            this.GridColumn_Note.Caption = "Description";
            this.GridColumn_Note.FieldName = "note";
            this.GridColumn_Note.Name = "GridColumn_Note";
            this.GridColumn_Note.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_Note.Visible = true;
            this.GridColumn_Note.VisibleIndex = 1;
            this.GridColumn_Note.Width = 352;
            // 
            // GridColumn_DateCreated
            // 
            this.GridColumn_DateCreated.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_DateCreated.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_DateCreated.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_DateCreated.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_DateCreated.Caption = "Date";
            this.GridColumn_DateCreated.DisplayFormat.FormatString = "d";
            this.GridColumn_DateCreated.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_DateCreated.FieldName = "date_created";
            this.GridColumn_DateCreated.Name = "GridColumn_DateCreated";
            this.GridColumn_DateCreated.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_DateCreated.Visible = true;
            this.GridColumn_DateCreated.VisibleIndex = 2;
            this.GridColumn_DateCreated.Width = 114;
            // 
            // GridColumn_CreatedBy
            // 
            this.GridColumn_CreatedBy.Caption = "Creator";
            this.GridColumn_CreatedBy.FieldName = "created_by";
            this.GridColumn_CreatedBy.Name = "GridColumn_CreatedBy";
            this.GridColumn_CreatedBy.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_CreatedBy.Visible = true;
            this.GridColumn_CreatedBy.VisibleIndex = 3;
            this.GridColumn_CreatedBy.Width = 226;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.GridControl1);
            this.layoutControl1.Controls.Add(this.Button_Cancel);
            this.layoutControl1.Controls.Add(this.Button_Select);
            this.layoutControl1.Controls.Add(this.Button_New);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(496, 230);
            this.layoutControl1.TabIndex = 8;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "New Button";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem_Grid,
            this.layoutControlItem_Button_Select,
            this.layoutControlItem_Button_New,
            this.layoutControlItem_Button_Cancel});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(496, 230);
            this.layoutControlGroup1.Text = "New Button";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem_Grid
            // 
            this.layoutControlItem_Grid.Control = this.GridControl1;
            this.layoutControlItem_Grid.CustomizationFormText = "Grid";
            this.layoutControlItem_Grid.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem_Grid.Name = "layoutControlItem_Grid";
            this.layoutControlItem_Grid.Size = new System.Drawing.Size(397, 210);
            this.layoutControlItem_Grid.Text = "Grid";
            this.layoutControlItem_Grid.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem_Grid.TextToControlDistance = 0;
            this.layoutControlItem_Grid.TextVisible = false;
            // 
            // layoutControlItem_Button_Select
            // 
            this.layoutControlItem_Button_Select.Control = this.Button_Select;
            this.layoutControlItem_Button_Select.CustomizationFormText = "Select Button";
            this.layoutControlItem_Button_Select.Location = new System.Drawing.Point(397, 0);
            this.layoutControlItem_Button_Select.Name = "layoutControlItem_Button_Select";
            this.layoutControlItem_Button_Select.Size = new System.Drawing.Size(79, 29);
            this.layoutControlItem_Button_Select.Text = "Select Button";
            this.layoutControlItem_Button_Select.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem_Button_Select.TextToControlDistance = 0;
            this.layoutControlItem_Button_Select.TextVisible = false;
            // 
            // layoutControlItem_Button_New
            // 
            this.layoutControlItem_Button_New.Control = this.Button_New;
            this.layoutControlItem_Button_New.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem_Button_New.Location = new System.Drawing.Point(397, 29);
            this.layoutControlItem_Button_New.Name = "layoutControlItem_Button_New";
            this.layoutControlItem_Button_New.Size = new System.Drawing.Size(79, 29);
            this.layoutControlItem_Button_New.Text = "layoutControlItem3";
            this.layoutControlItem_Button_New.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem_Button_New.TextToControlDistance = 0;
            this.layoutControlItem_Button_New.TextVisible = false;
            // 
            // layoutControlItem_Button_Cancel
            // 
            this.layoutControlItem_Button_Cancel.Control = this.Button_Cancel;
            this.layoutControlItem_Button_Cancel.CustomizationFormText = "Cancel Button";
            this.layoutControlItem_Button_Cancel.Location = new System.Drawing.Point(397, 58);
            this.layoutControlItem_Button_Cancel.Name = "layoutControlItem_Button_Cancel";
            this.layoutControlItem_Button_Cancel.Size = new System.Drawing.Size(79, 152);
            this.layoutControlItem_Button_Cancel.Text = "Cancel Button";
            this.layoutControlItem_Button_Cancel.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem_Button_Cancel.TextToControlDistance = 0;
            this.layoutControlItem_Button_Cancel.TextVisible = false;
            // 
            // ReconcileBatchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(496, 230);
            this.Controls.Add(this.layoutControl1);
            this.Name = "ReconcileBatchForm";
            this.Text = "Reconciliation Batch Selection";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_Button_Select)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_Button_New)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_Button_Cancel)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected DevExpress.XtraGrid.Views.Grid.GridView GridView1;
        protected DevExpress.XtraGrid.Columns.GridColumn GridColumn_Batch;
        protected DevExpress.XtraGrid.Columns.GridColumn GridColumn_Note;
        protected DevExpress.XtraGrid.Columns.GridColumn GridColumn_DateCreated;
        protected DevExpress.XtraGrid.Columns.GridColumn GridColumn_CreatedBy;
        protected DevExpress.XtraEditors.SimpleButton Button_Cancel;
        protected DevExpress.XtraEditors.SimpleButton Button_New;
        protected DevExpress.XtraEditors.SimpleButton Button_Select;
        protected DevExpress.XtraGrid.GridControl GridControl1;
        protected DevExpress.XtraLayout.LayoutControl layoutControl1;
        protected DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        protected DevExpress.XtraLayout.LayoutControlItem layoutControlItem_Grid;
        protected DevExpress.XtraLayout.LayoutControlItem layoutControlItem_Button_Select;
        protected DevExpress.XtraLayout.LayoutControlItem layoutControlItem_Button_New;
        protected DevExpress.XtraLayout.LayoutControlItem layoutControlItem_Button_Cancel;
    }
}