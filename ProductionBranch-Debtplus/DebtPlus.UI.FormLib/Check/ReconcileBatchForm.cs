using System;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.Data.Forms;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraLayout.Utils;

namespace DebtPlus.UI.FormLib.Check
{
    public partial class ReconcileBatchForm : DebtPlusForm
    {
        public ReconcileBatchForm()
        {
            InitializeComponent();
            RegisterHandlers();
            BatchID = -1;
        }

        /// <summary>
        ///     Should the NEW button be shown?
        /// </summary>
        public bool ShowNewButton
        {
            get { return layoutControlItem_Button_New.Visibility != LayoutVisibility.Never; }
            set { layoutControlItem_Button_New.Visibility = value ? LayoutVisibility.Always : LayoutVisibility.OnlyInCustomization; }
        }

        /// <summary>
        ///     Batch ID selected
        /// </summary>
        public Int32 BatchID { get; protected set; }

        private void RegisterHandlers()
        {
            Load                        += ReconcileBatchForm_Load;
            Button_New.Click            += Button_New_Click;
            GridView1.FocusedRowChanged += GridView1_FocusedRowChanged;
            GridView1.DoubleClick       += GridView1_DoubleClick;
            GridView1.Click             += GridView1_Click;
        }

        private void UnRegisterHandlers()
        {
            Load                        -= ReconcileBatchForm_Load;
            Button_New.Click            -= Button_New_Click;
            GridView1.FocusedRowChanged -= GridView1_FocusedRowChanged;
            GridView1.DoubleClick       -= GridView1_DoubleClick;
            GridView1.Click             -= GridView1_Click;
        }

        /// <summary>
        ///     Process the NEW button CLICK event
        /// </summary>
        private void Button_New_Click(object sender, EventArgs e)
        {
            var note = string.Empty;
            if (InputBox.Show("Note associated with the batch", ref note, "Reconciliation Batch Label", "", 50) != DialogResult.OK) return;
            if (CreateBatch(note))
            {
                DialogResult = DialogResult.OK;
            }
        }

        private bool CreateBatch(string note)
        {
            try
            {
                using (var bc = new DebtPlus.LINQ.BusinessContext())
                {
                    var record = DebtPlus.LINQ.Factory.Manufacture_recon_batch();
                    record.note = note;

                    bc.recon_batches.InsertOnSubmit(record);
                    bc.SubmitChanges();

                    BatchID = record.Id;
                    return true;
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error creating batch ID");
                return false;
            }
        }

        private void ReconcileBatchForm_Load(Object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                Load_Reconcile_Batches();
                ChangeRowHandle(GridView1.FocusedRowHandle);
            }

            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        ///     Load the list of batches in the grid control
        /// </summary>
        protected void Load_Reconcile_Batches()
        {
            try
            {
                using (var bc = new DebtPlus.LINQ.BusinessContext())
                {
                    GridControl1.DataSource = bc.recon_batches.Where(s => s.date_posted == null).OrderByDescending(s => s.date_created).Take(50).ToList();
                    GridControl1.RefreshDataSource();
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading reconciliation batch list");
            }
        }

        /// <summary>
        ///     Process a change in the grid FOCUS row
        /// </summary>
        private void GridView1_FocusedRowChanged(Object sender, FocusedRowChangedEventArgs e)
        {
            ChangeRowHandle(e.FocusedRowHandle);
        }

        /// <summary>
        ///     Process the change in the row handle to select the batch
        /// </summary>
        private void ChangeRowHandle(Int32 RowHandle)
        {
            if (RowHandle >= 0)
            {
                var row = (DebtPlus.LINQ.recon_batch)GridView1.GetRow(RowHandle);
                BatchID = row.Id;
            }
            else
            {
                BatchID = 0;
            }

            // Enable the OK button if the creditor is specified
            Button_Select.Enabled = (BatchID > 0);
        }

        /// <summary>
        ///     Process a DOUBLE-CLICK event on the grid
        /// </summary>
        private void GridView1_DoubleClick(object sender, EventArgs e)
        {
            // Find the row targeted as the double-click item
            var gv = (GridView) sender;
            GridHitInfo hi = gv.CalcHitInfo((gv.GridControl.PointToClient(MousePosition)));

            try
            {
                Int32 RowHandle = hi.RowHandle;
                GridView1.FocusedRowHandle = RowHandle;
                if (RowHandle >= 0)
                {
                    ChangeRowHandle(RowHandle);
                }
            }
            catch { }

            if (Button_Select.Enabled)
            {
                Button_Select.PerformClick();
            }
        }

        /// <summary>
        ///     Process a CLICK event on the grid
        /// </summary>
        private void GridView1_Click(object sender, EventArgs e)
        {
            // Find the row targeted as the double-click item
            var gv = (GridView) sender;
            GridHitInfo hi = gv.CalcHitInfo((gv.GridControl.PointToClient(MousePosition)));
            GridView1.FocusedRowHandle = hi.RowHandle;
        }
    }
}
