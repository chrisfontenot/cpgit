using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using DebtPlus.UI.Common;
using DebtPlus.Utils;
using DevExpress.Data;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

namespace DebtPlus.UI.FormLib.Proposals
{
    public partial class OpenControl : XtraUserControl
    {

        /// <summary>
        ///     Dataset with the list of items
        /// </summary>
        protected DataSet ds = new DataSet("ds");

        /// <summary>
        ///     Create a new instance of our class
        /// </summary>
        public OpenControl()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        ///     Do we allow multiple rows to be selected?
        /// </summary>
        public bool MultipleSelectionAllowed { get; set; }

        /// <summary>
        ///     Return the list of selected batches from the grid control
        /// </summary>
        public List<int> SelectedBatches
        {
            get
            {
                // Build the list of selected batch IDs
                var rowHandles = GridView1.GetSelectedRows();
                return rowHandles.Select(handle =>
                    {
                        var dataRowView = GridView1.GetRow(handle) as DataRowView;
                        return dataRowView != null ? Convert.ToInt32(dataRowView["item_key"]) : 0;
                    }).ToList();
            }
        }

        /// <summary>
        ///     Proposal batch information
        /// </summary>
        public Int32 Batch_ID
        {
            get
            {
                if (MultipleSelectionAllowed)
                {
                    throw new NotImplementedException("Batch_ID is not allowed for multiple selections");
                }
                return SelectedBatches.FirstOrDefault();
            }
        }

        #region Selected Event

        /// <summary>
        ///     Event when the OK button is pressed
        /// </summary>
        public event EventHandler Selected;

        /// <summary>
        ///     Raise the SELECTED event
        /// </summary>
        protected void RaiseSelected(EventArgs e)
        {
            var evt = Selected;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        /// <summary>
        ///     Handle the SELECTED event
        /// </summary>
        protected virtual void OnSelected(EventArgs e)
        {
            RaiseSelected(e);
        }

        #endregion

        #region Cancelled Event

        /// <summary>
        ///     Event when the CANCEL button is pressed
        /// </summary>
        public event EventHandler Cancelled;

        /// <summary>
        ///     Raise the CANCEL event
        /// </summary>
        protected void RaiseCancelled(EventArgs e)
        {
            EventHandler evt = Cancelled;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        /// <summary>
        ///     Handle the CANCEL event
        /// </summary>
        protected virtual void OnCancelled(EventArgs e)
        {
            RaiseCancelled(e);
        }

        #endregion

        /// <summary>
        ///     Dispose of any allocated resources
        /// </summary>
        /// <param name="disposing">TRUE if disposing, FALSE if finalizing</param>
        private void OpenControl_Dispose(bool disposing)
        {
            if (disposing)
            {
                ds.Dispose();
            }

            if (!DesignMode)
            {
                UnRegisterHandlers();
            }
        }

        /// <summary>
        ///     Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load += OpenControl_Load;
            Button_Cancel.Click += Button_Cancel_Click;
            Button_Select.Click += Button_Select_Click;
            GridView1.DoubleClick += GridView1_DoubleClick;
            GridView1.FocusedRowChanged += GridView1_FocusedRowChanged;
            GridView1.SelectionChanged += GridView1_SelectionChanged;
        }

        /// <summary>
        ///     Remove the event handler registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= OpenControl_Load;
            Button_Cancel.Click -= Button_Cancel_Click;
            Button_Select.Click -= Button_Select_Click;
            GridView1.DoubleClick -= GridView1_DoubleClick;
            GridView1.FocusedRowChanged -= GridView1_FocusedRowChanged;
            GridView1.SelectionChanged -= GridView1_SelectionChanged;
        }

        /// <summary>
        ///     Handle the CLICK event for the CANCEL button
        /// </summary>
        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            OnCancelled(e);
        }

        /// <summary>
        ///     Handle the CLICK event for the SELECT button
        /// </summary>
        private void Button_Select_Click(object sender, EventArgs e)
        {
            OnSelected(e);
        }

        /// <summary>
        ///     Handle the LOAD event of our control
        /// </summary>
        private void OpenControl_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Ignore the load event if using Visual Studio
                if (!DesignMode)
                {
                    RefreshList();
                }
            }

            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        ///     Load the list of batches into the grid control
        /// </summary>
        public void RefreshList()
        {
            using (var cm = new CursorManager())
            {
                // Find the list of proposals
                var tbl = ProposalTable();

                // Create a relationship between the proposal entry and its type
                const string relationName = "proposals_open_type";
                if (!ds.Relations.Contains(relationName))
                {
                    var rel1 = new DataRelation(relationName, ProposalTypes().Columns["proposal_type"], tbl.Columns["proposal_type"], false);
                    ds.Relations.Add(rel1);
                }

                // Add the column for the mapping from one table to the other
                if (!tbl.Columns.Contains("type_description"))
                {
                    tbl.Columns.Add("type_description", typeof (string), "Parent(proposals_open_type).description");
                }

                // Bind the data table to the grid
                GridControl1.DataSource = tbl.DefaultView;
                GridControl1.RefreshDataSource();
            }
        }

        /// <summary>
        ///     Erase the proposals table from the dataset
        /// </summary>
        private void ClearProposals()
        {
            const string tableName = "lst_proposals_open";
            var tbl = ds.Tables[tableName];
            if (tbl != null)
            {
                tbl.Clear();
            }
        }

        /// <summary>
        ///     Return the table of open proposals
        /// </summary>
        protected DataTable ProposalTable()
        {
            const string tableName = "lst_proposals_open";
            DataTable tbl = ds.Tables[tableName];
            if (tbl != null)
            {
                tbl.Clear();
            }

            using (var gdr = Repository.Proposals.GetAllOpenProposals(ds, tableName))
            {
                if (!gdr.Success)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, String.Format("Error retrieving {0} table.", tableName));
                    tbl = null;
                }
                else
                {
                    tbl = ds.Tables[tableName];
                }
            }

            return tbl;
        }

        /// <summary>
        ///     Return the table of the proposal type descriptions
        /// </summary>
        protected DataTable ProposalTypes()
        {
            const string tableName = "ProposalTypes";
            var tbl = ds.Tables[tableName];

            if (tbl == null)
            {
                tbl = new DataTable(tableName);
                tbl.Columns.Add("proposal_type", ProposalTable().Columns["proposal_type"].DataType);
                tbl.Columns.Add("description", typeof (String));
                tbl.PrimaryKey = new[] {tbl.Columns[0]};
                tbl.Rows.Add(new object[] {1, "Standard"});
                tbl.Rows.Add(new object[] {2, "Full Disclosure"});
                tbl.Rows.Add(new object[] {3, "EFT"});
                tbl.Rows.Add(new object[] {4, "EFT"});
                ds.Tables.Add(tbl);
            }

            return tbl;
        }

        /// <summary>
        ///     Return the table of the bank information
        /// </summary>
        protected DataTable Banks()
        {
            const string tableName = "banks";

            var gdr = Repository.LookupTables.Banks.GetAll(ds, tableName);
            if (!gdr.Success)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, String.Format("Error retrieving {0} table.", tableName));
            }

            return ds.Tables[tableName];
        }

        /// <summary>
        ///     Process the DOUBLE click event on the grid control.
        /// </summary>
        private void GridView1_DoubleClick(object sender, EventArgs e)
        {
            GridHitInfo hi = GridView1.CalcHitInfo((GridView1.GridControl.PointToClient(MousePosition)));
            if (hi.IsValid && hi.InRow)
            {
                GridView1.ClearSelection();
                GridView1.SelectRow(hi.RowHandle);
                Button_Select.PerformClick();
            }
        }

        /// <summary>
        ///     When the focused row changes, update the selection list if we select only one row.
        /// </summary>
        private void GridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            if (!MultipleSelectionAllowed)
            {
                GridView1.ClearSelection();
                GridView1.SelectRow(GridView1.FocusedRowHandle);
            }
        }

        /// <summary>
        ///     Handle the condition where the selection changes on the grid control. Enable or disable the OK button accordingly.
        /// </summary>
        private void GridView1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Button_Select.Enabled = (GridView1.SelectedRowsCount >= 1);
        }
    }
}
