namespace DebtPlus.UI.FormLib.Proposals
{
    partial class OpenForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            OpenForm_Dispose(disposing);
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openControl1 = new OpenControl();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // openControl1
            // 
            this.openControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.openControl1.Location = new System.Drawing.Point(0, 0);
            this.openControl1.Name = "openControl1";
            this.openControl1.Size = new System.Drawing.Size(664, 238);
            this.openControl1.TabIndex = 0;
            // 
            // OpenForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(664, 238);
            this.Controls.Add(this.openControl1);
            this.Name = "OpenForm";
            this.Text = "Select the proposal batch";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private OpenControl openControl1;
    }
}