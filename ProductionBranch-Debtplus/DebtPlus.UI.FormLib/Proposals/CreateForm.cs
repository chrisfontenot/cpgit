using System;
using System.Windows.Forms;
using DebtPlus.Data.Forms;

namespace DebtPlus.UI.FormLib.Proposals
{
    public partial class CreateForm : DebtPlusForm
    {
        public CreateForm()
        {
            InitializeComponent();

            // If this is not the design mode, i.e. we are really running, then register the handlers.
            // If this is design mode then don't as they can cause problems with database accesses, etc.
            if (!DesignMode)
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        ///     Return the selected batch ID from the input form
        /// </summary>
        public Int32 Batch_ID
        {
            get { return createControl1.Batch_ID; }
        }

        /// <summary>
        ///     Process the disposal for the form. Remove any storage allocaetd in the form.
        /// </summary>
        /// <param name="disposing">TRUE when Dispose() is called. FALSE for Finalize.</param>
        private void CreateForm_Dispose(bool disposing)
        {
            // Remove the handlers so that the form may be properly destroyed
            if (!DesignMode)
            {
                UnRegisterHandlers();
            }
        }

        /// <summary>
        ///     Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load += CreateForm_Load;
            createControl1.Selected += createControl1_Selected;
            createControl1.Cancelled += createControl1_Cancelled;
        }

        /// <summary>
        ///     Remove the event handler registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= CreateForm_Load;
            createControl1.Selected -= createControl1_Selected;
            createControl1.Cancelled -= createControl1_Cancelled;
        }

        /// <summary>
        ///     Process the LOAD event on the form
        /// </summary>
        private void CreateForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // TODO -- Fill in the logic when the form is initially loaded.
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        ///     Handle the OK button for the control
        /// </summary>
        private void createControl1_Selected(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        /// <summary>
        ///     Handle the CANCEL button for the control
        /// </summary>
        private void createControl1_Cancelled(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}