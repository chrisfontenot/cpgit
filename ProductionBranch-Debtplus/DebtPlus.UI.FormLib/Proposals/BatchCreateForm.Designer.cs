namespace DebtPlus.UI.FormLib.Proposals
{
    partial class BatchCreateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null) components.Dispose();
                }
                components = null;
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.TextEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.DateEdit2 = new DevExpress.XtraEditors.DateEdit();
            this.DateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.chkType2 = new DevExpress.XtraEditors.CheckEdit();
            this.chkType1 = new DevExpress.XtraEditors.CheckEdit();
            this.chkType0 = new DevExpress.XtraEditors.CheckEdit();
            this.Label4 = new DevExpress.XtraEditors.LabelControl();
            this.Label3 = new DevExpress.XtraEditors.LabelControl();
            this.Label2 = new DevExpress.XtraEditors.LabelControl();
            this.Label1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkType2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkType1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkType0.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(446, 91);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 32;
            this.Button_Cancel.Text = "&Cancel";
            this.Button_Cancel.ToolTipController = this.ToolTipController1;
            // 
            // Button_OK
            // 
            this.Button_OK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_OK.Location = new System.Drawing.Point(446, 59);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.TabIndex = 31;
            this.Button_OK.Text = "&OK";
            this.Button_OK.ToolTipController = this.ToolTipController1;
            // 
            // TextEdit1
            // 
            this.TextEdit1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextEdit1.EditValue = "";
            this.TextEdit1.Location = new System.Drawing.Point(102, 81);
            this.TextEdit1.Name = "TextEdit1";
            this.TextEdit1.Properties.MaxLength = 50;
            this.TextEdit1.Size = new System.Drawing.Size(328, 20);
            this.TextEdit1.TabIndex = 30;
            this.TextEdit1.ToolTipController = this.ToolTipController1;
            // 
            // DateEdit2
            // 
            this.DateEdit2.EditValue = new System.DateTime(2005, 10, 14, 0, 0, 0, 0);
            this.DateEdit2.Location = new System.Drawing.Point(262, 53);
            this.DateEdit2.Name = "DateEdit2";
            this.DateEdit2.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.DateEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEdit2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DateEdit2.Size = new System.Drawing.Size(88, 20);
            this.DateEdit2.TabIndex = 29;
            this.DateEdit2.ToolTipController = this.ToolTipController1;
            // 
            // DateEdit1
            // 
            this.DateEdit1.EditValue = new System.DateTime(2012, 1, 1, 0, 0, 0, 0);
            this.DateEdit1.Location = new System.Drawing.Point(102, 53);
            this.DateEdit1.Name = "DateEdit1";
            this.DateEdit1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.DateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEdit1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DateEdit1.Size = new System.Drawing.Size(88, 20);
            this.DateEdit1.TabIndex = 28;
            this.DateEdit1.ToolTipController = this.ToolTipController1;
            // 
            // chkType2
            // 
            this.chkType2.Cursor = System.Windows.Forms.Cursors.Default;
            this.chkType2.EditValue = true;
            this.chkType2.Location = new System.Drawing.Point(14, 147);
            this.chkType2.Name = "chkType2";
            this.chkType2.Properties.Appearance.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkType2.Properties.Appearance.Options.UseFont = true;
            this.chkType2.Properties.Caption = "Create a batch for &RPPS EDI Proposals";
            this.chkType2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkType2.Size = new System.Drawing.Size(224, 20);
            this.chkType2.TabIndex = 24;
            this.chkType2.ToolTipController = this.ToolTipController1;
            // 
            // chkType1
            // 
            this.chkType1.Cursor = System.Windows.Forms.Cursors.Default;
            this.chkType1.EditValue = true;
            this.chkType1.Location = new System.Drawing.Point(14, 131);
            this.chkType1.Name = "chkType1";
            this.chkType1.Properties.Appearance.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkType1.Properties.Appearance.Options.UseFont = true;
            this.chkType1.Properties.Caption = "Create a batch for Printed &Full Disclosure Proposals";
            this.chkType1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkType1.Size = new System.Drawing.Size(280, 20);
            this.chkType1.TabIndex = 23;
            this.chkType1.ToolTipController = this.ToolTipController1;
            // 
            // chkType0
            // 
            this.chkType0.Cursor = System.Windows.Forms.Cursors.Default;
            this.chkType0.EditValue = true;
            this.chkType0.Location = new System.Drawing.Point(14, 115);
            this.chkType0.Name = "chkType0";
            this.chkType0.Properties.Appearance.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkType0.Properties.Appearance.Options.UseFont = true;
            this.chkType0.Properties.Caption = "Create a batch for Printed &Standard Proposals";
            this.chkType0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chkType0.Size = new System.Drawing.Size(256, 20);
            this.chkType0.TabIndex = 22;
            this.chkType0.ToolTipController = this.ToolTipController1;
            // 
            // Label4
            // 
            this.Label4.Appearance.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label4.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.Label4.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label4.Location = new System.Drawing.Point(6, 55);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(78, 14);
            this.Label4.TabIndex = 27;
            this.Label4.Text = "Proposals dated";
            this.Label4.ToolTipController = this.ToolTipController1;
            // 
            // Label3
            // 
            this.Label3.Appearance.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.Label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label3.Location = new System.Drawing.Point(206, 55);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(37, 14);
            this.Label3.TabIndex = 26;
            this.Label3.Text = "through";
            this.Label3.ToolTipController = this.ToolTipController1;
            // 
            // Label2
            // 
            this.Label2.Appearance.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.Label2.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label2.Location = new System.Drawing.Point(14, 83);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(60, 14);
            this.Label2.TabIndex = 21;
            this.Label2.Text = "Batch &Label:";
            this.Label2.ToolTipController = this.ToolTipController1;
            // 
            // Label1
            // 
            this.Label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Label1.Appearance.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Label1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.Label1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Label1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(6, 7);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(515, 40);
            this.Label1.TabIndex = 20;
            this.Label1.Text = "Enter the information to create a new proposal batch. Each batch has a label so t" +
    "hat you may identifiy it later. You may determine which proposals are valid for " +
    "a new batch by selecting the type.";
            this.Label1.ToolTipController = this.ToolTipController1;
            // 
            // BatchCreateForm
            // 
            this.AcceptButton = this.Button_OK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(527, 190);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.TextEdit1);
            this.Controls.Add(this.DateEdit2);
            this.Controls.Add(this.DateEdit1);
            this.Controls.Add(this.chkType2);
            this.Controls.Add(this.chkType1);
            this.Controls.Add(this.chkType0);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label1);
            this.Name = "BatchCreateForm";
            this.Text = "Create a new proposal batch(es)";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkType2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkType1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkType0.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
        internal DevExpress.XtraEditors.SimpleButton Button_OK;
        internal DevExpress.XtraEditors.TextEdit TextEdit1;
        internal DevExpress.XtraEditors.DateEdit DateEdit2;
        internal DevExpress.XtraEditors.DateEdit DateEdit1;
        public DevExpress.XtraEditors.CheckEdit chkType2;
        public DevExpress.XtraEditors.CheckEdit chkType1;
        public DevExpress.XtraEditors.CheckEdit chkType0;
        public DevExpress.XtraEditors.LabelControl Label4;
        public DevExpress.XtraEditors.LabelControl Label3;
        public DevExpress.XtraEditors.LabelControl Label2;
        public DevExpress.XtraEditors.LabelControl Label1;
    }
}