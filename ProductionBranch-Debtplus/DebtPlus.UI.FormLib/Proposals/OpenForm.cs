using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DebtPlus.Data.Forms;

namespace DebtPlus.UI.FormLib.Proposals
{
    public partial class OpenForm : DebtPlusForm
    {
        public OpenForm()
        {
            InitializeComponent();

            // If this is not the design mode, i.e. we are really running, then register the handlers.
            // If this is design mode then don't as they can cause problems with database accesses, etc.
            if (!DesignMode)
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        ///     Are multiple selections allowed?
        /// </summary>
        public bool MultipleSelectionAllowed
        {
            get { return openControl1.MultipleSelectionAllowed; }
            set { openControl1.MultipleSelectionAllowed = value; }
        }

        /// <summary>
        ///     The batch values selected by the control.
        /// </summary>
        public List<int> SelectedBatches
        {
            get { return openControl1.SelectedBatches; }
        }

        /// <summary>
        ///     Return the single batch selected by the control. Invalid if multiple batches are possible.
        /// </summary>
        public Int32 Batch_ID
        {
            get { return openControl1.Batch_ID; }
        }

        /// <summary>
        ///     Process the disposal for the form. Remove any storage allocaetd in the form.
        /// </summary>
        /// <param name="disposing">TRUE when Dispose() is called. FALSE for Finalize.</param>
        private void OpenForm_Dispose(bool disposing)
        {
            // Remove the handlers so that the form may be properly destroyed
            if (!DesignMode)
            {
                UnRegisterHandlers();
            }
        }

        /// <summary>
        ///     Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load += OpenForm_Load;
            openControl1.Cancelled += openControl1_Cancelled;
            openControl1.Selected += openControl1_Selected;
        }

        /// <summary>
        ///     Remove the event handler registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= OpenForm_Load;
            openControl1.Cancelled -= openControl1_Cancelled;
            openControl1.Selected -= openControl1_Selected;
        }

        /// <summary>
        ///     Process the LOAD event on the form
        /// </summary>
        private void OpenForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // TODO -- Fill in the logic when the form is initially loaded.
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        ///     Handle the OK button event for the control
        /// </summary>
        private void openControl1_Selected(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        /// <summary>
        ///     Handle the CANCEL button event for the control.
        /// </summary>
        private void openControl1_Cancelled(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}