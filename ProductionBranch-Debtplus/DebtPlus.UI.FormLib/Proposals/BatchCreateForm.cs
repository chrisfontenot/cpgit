using System;
using System.Diagnostics;
using System.Windows.Forms;
using DebtPlus.Data.Forms;
using DebtPlus.UI.Common;
using DebtPlus.Utils;
using DevExpress.XtraEditors;

namespace DebtPlus.UI.FormLib.Proposals
{
    public partial class BatchCreateForm : DebtPlusForm
    {
        /// <summary>
        ///     Proposal types
        /// </summary>
        public enum ProposalTypes
        {
            ProposalTypes_Standard = 0,
            ProposalTypes_Full = 1,
            ProposalTypes_RPPS = 2
        }


        /// <summary>
        ///     Initialize the class
        /// </summary>
        public BatchCreateForm()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        /// <summary>
        ///     Return the Comment note for the proposals
        /// </summary>
        protected string NoteText
        {
            get { return TextEdit1.Text.Trim(); }
        }

        /// <summary>
        ///     Return the Starting Date for the proposals
        /// </summary>
        protected DateTime StartingDate
        {
            get { return DateEdit1.DateTime.Date; }
        }

        /// <summary>
        ///     Return the Ending Date for the proposals
        /// </summary>
        protected DateTime EndingDate
        {
            get { return DateEdit2.DateTime.Date; }
        }

        /// <summary>
        ///     Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load                       += BatchCreateForm_Load;
            TextEdit1.KeyPress         += TextEdit1_KeyPress;
            TextEdit1.Enter            += TextEdit1_Enter;
            TextEdit1.TextChanged      += FormChanged;
            DateEdit1.EditValueChanged += FormChanged;
            DateEdit2.EditValueChanged += FormChanged;
            chkType0.CheckedChanged    += FormChanged;
            chkType1.CheckedChanged    += FormChanged;
            chkType2.CheckedChanged    += FormChanged;
            Button_OK.Click            += Button_OK_Click;
        }

        /// <summary>
        ///     Remove the event handler registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load                       -= BatchCreateForm_Load;
            TextEdit1.KeyPress         -= TextEdit1_KeyPress;
            TextEdit1.Enter            -= TextEdit1_Enter;
            TextEdit1.TextChanged      -= FormChanged;
            DateEdit1.EditValueChanged -= FormChanged;
            DateEdit2.EditValueChanged -= FormChanged;
            chkType0.CheckedChanged    -= FormChanged;
            chkType1.CheckedChanged    -= FormChanged;
            chkType2.CheckedChanged    -= FormChanged;
            Button_OK.Click            -= Button_OK_Click;
        }

        /// <summary>
        ///     Translate the proposal type to a boolean value
        /// </summary>
        protected internal Int32 ProposalType(ProposalTypes DesiredType)
        {
            switch (DesiredType)
            {
                case ProposalTypes.ProposalTypes_Standard:
                    return checked_value(chkType0);
                case ProposalTypes.ProposalTypes_Full:
                    return checked_value(chkType1);
                case ProposalTypes.ProposalTypes_RPPS:
                    return checked_value(chkType2);
                default:
                    Debug.Assert(false);
                    return 0;
            }
        }

        /// <summary>
        ///     Determine if the checkbox is checked
        /// </summary>
        private Int32 checked_value(CheckEdit ctl)
        {
            return (ctl.CheckState == CheckState.Checked) ? 1 : 0;
        }

        /// <summary>
        ///     Process a change in the form controls
        /// </summary>
        private void FormChanged(object sneder, EventArgs e)
        {
            Button_OK.Enabled = ! HasErrors();
        }

        /// <summary>
        ///     Process the text input ENTR input field
        /// </summary>
        private void TextEdit1_Enter(object sender, EventArgs e)
        {
            TextEdit1.SelectAll();
        }

        /// <summary>
        ///     Handle a KEYPRESS event for the text buffer
        /// </summary>
        private void TextEdit1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (NoteText == string.Empty && Char.IsWhiteSpace(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        /// <summary>
        ///     Does the form have any errors?
        /// </summary>
        private bool HasErrors()
        {
            // A label is required
            if (NoteText == string.Empty)
            {
                return true;
            }

            // There needs to be a date
            if (DateEdit1.EditValue == null || DateEdit1.EditValue == DBNull.Value)
            {
                return true;
            }
            if (DateEdit2.EditValue == null || DateEdit2.EditValue == DBNull.Value)
            {
                return true;
            }

            // The dates must be in the proper order
            if (StartingDate > EndingDate)
            {
                return true;
            }

            // There must be a checked item to construct the batch
            return ProposalType(ProposalTypes.ProposalTypes_Standard) == 0 &&
                   ProposalType(ProposalTypes.ProposalTypes_Full) == 0 &&
                   ProposalType(ProposalTypes.ProposalTypes_RPPS) == 0;
        }

        /// <summary>
        ///     Handle the OK button to create the batch(es)
        /// </summary>
        private void Button_OK_Click(object sender, EventArgs e)
        {
            using (var cm = new CursorManager())
            {
                var rslt = Repository.ProposalBatch.Insert(StartingDate, EndingDate, NoteText, ProposalType(ProposalTypes.ProposalTypes_Standard), ProposalType(ProposalTypes.ProposalTypes_Full), ProposalType(ProposalTypes.ProposalTypes_RPPS));
                if (rslt.Success)
                {
                    DialogResult = DialogResult.OK;
                    return;
                }
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(rslt, "Error creating proposal batch");
            }
        }

        /// <summary>
        ///     Process the LOAD event on the form
        /// </summary>
        private void BatchCreateForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                DateEdit1.EditValue = new DateTime(1900, 1, 1);
                DateEdit2.EditValue = DateTime.Now.Date;
            }
            finally
            {
                RegisterHandlers();
            }
            Button_OK.Enabled = ! HasErrors();
        }
    }
}
