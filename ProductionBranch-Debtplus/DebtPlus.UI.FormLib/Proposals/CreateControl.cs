using System;
using System.Windows.Forms;

namespace DebtPlus.UI.FormLib.Proposals
{
    public partial class CreateControl : OpenControl
    {
        #region PerformCreate Event

        /// <summary>
        ///     Event when the NEW button is pressed
        /// </summary>
        public event EventHandler PerformCreate;

        /// <summary>
        ///     Raise the PERFORMCREATE event
        /// </summary>
        /// <param name="e"></param>
        protected void RaisePerformCreate(EventArgs e)
        {
            EventHandler evt = PerformCreate;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        /// <summary>
        ///     Handle the PERFORMCREATE event
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnPerformCreate(EventArgs e)
        {
            RaisePerformCreate(e);
        }

        #endregion

        public CreateControl()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        ///     Dispose of any allocated resources
        /// </summary>
        /// <param name="disposing">TRUE if disposing, FALSE if finalizing</param>
        private void CreateControl_Dispose(bool disposing)
        {
            if (!DesignMode)
            {
                UnRegisterHandlers();
            }
        }

        /// <summary>
        ///     Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Button_Create.Click += Button_Create_Click;
        }

        /// <summary>
        ///     Remove the event handler registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            Button_Create.Click -= Button_Create_Click;
        }

        /// <summary>
        ///     Handle the CLICK event for the NEW button
        /// </summary>
        private void Button_Create_Click(object sender, EventArgs e)
        {
            using (var frm = new BatchCreateForm())
            {
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    RefreshList();
                }
            }
        }
    }
}