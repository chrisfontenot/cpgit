namespace DebtPlus.UI.FormLib.Proposals
{
    partial class OpenControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                OpenControl_Dispose(disposing);
                if (disposing)
                {
                    if (components != null)
                    {
                        components.Dispose();
                    }
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Select = new DevExpress.XtraEditors.SimpleButton();
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.GridColumn_BatchID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_DateCreated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_CreatedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_TypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_BankDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Note = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Type = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Bank = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(591, 40);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 2;
            this.Button_Cancel.Text = "&Cancel";
            // 
            // Button_Select
            // 
            this.Button_Select.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Select.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Button_Select.Enabled = false;
            this.Button_Select.Location = new System.Drawing.Point(591, 11);
            this.Button_Select.Name = "Button_Select";
            this.Button_Select.Size = new System.Drawing.Size(75, 23);
            this.Button_Select.TabIndex = 1;
            this.Button_Select.Text = "&Select";
            // 
            // GridControl1
            // 
            this.GridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GridControl1.Location = new System.Drawing.Point(3, 3);
            this.GridControl1.MainView = this.GridView1;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.Size = new System.Drawing.Size(582, 225);
            this.GridControl1.TabIndex = 0;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GridView1});
            // 
            // GridView1
            // 
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.Silver;
            this.GridView1.Appearance.ColumnFilterButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(212)))), ((int)(((byte)(212)))));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.Silver;
            this.GridView1.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("Arial", 8F);
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Gray;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.LightGray;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor2 = System.Drawing.Color.WhiteSmoke;
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.LightGray;
            this.GridView1.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("Arial", 8F);
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.GridView1.Appearance.DetailTip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.GridView1.Appearance.DetailTip.Font = new System.Drawing.Font("Arial", 8F);
            this.GridView1.Appearance.DetailTip.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.DetailTip.Options.UseBackColor = true;
            this.GridView1.Appearance.DetailTip.Options.UseFont = true;
            this.GridView1.Appearance.DetailTip.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.GridView1.Appearance.Empty.Font = new System.Drawing.Font("Arial", 8F);
            this.GridView1.Appearance.Empty.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.Empty.Options.UseFont = true;
            this.GridView1.Appearance.Empty.Options.UseForeColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.GridView1.Appearance.EvenRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.GridView1.Appearance.EvenRow.Font = new System.Drawing.Font("Arial", 8F);
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseFont = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(208)))), ((int)(((byte)(200)))));
            this.GridView1.Appearance.FilterCloseButton.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(118)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(208)))), ((int)(((byte)(200)))));
            this.GridView1.Appearance.FilterCloseButton.Font = new System.Drawing.Font("Arial", 8F);
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterCloseButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseFont = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(80)))), ((int)(((byte)(135)))));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(208)))), ((int)(((byte)(200)))));
            this.GridView1.Appearance.FilterPanel.Font = new System.Drawing.Font("Arial", 8F);
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseFont = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(58)))), ((int)(((byte)(58)))));
            this.GridView1.Appearance.FixedLine.Font = new System.Drawing.Font("Arial", 8F);
            this.GridView1.Appearance.FixedLine.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FixedLine.Options.UseFont = true;
            this.GridView1.Appearance.FixedLine.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.GridView1.Appearance.FocusedCell.Font = new System.Drawing.Font("Arial", 8F);
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseFont = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.Navy;
            this.GridView1.Appearance.FocusedRow.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(178)))));
            this.GridView1.Appearance.FocusedRow.Font = new System.Drawing.Font("Arial", 8F);
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseFont = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.Silver;
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.Silver;
            this.GridView1.Appearance.FooterPanel.Font = new System.Drawing.Font("Arial", 8F);
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver;
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver;
            this.GridView1.Appearance.GroupButton.Font = new System.Drawing.Font("Arial", 8F);
            this.GridView1.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseFont = true;
            this.GridView1.Appearance.GroupButton.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(202)))), ((int)(((byte)(202)))));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(202)))), ((int)(((byte)(202)))));
            this.GridView1.Appearance.GroupFooter.Font = new System.Drawing.Font("Arial", 8F);
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseFont = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(110)))), ((int)(((byte)(165)))));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseFont = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.Gray;
            this.GridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Arial", 8F);
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Silver;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseFont = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Silver;
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.Silver;
            this.GridView1.Appearance.HeaderPanel.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Gray;
            this.GridView1.Appearance.HideSelectionRow.Font = new System.Drawing.Font("Arial", 8F);
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(208)))), ((int)(((byte)(200)))));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseFont = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.Silver;
            this.GridView1.Appearance.HorzLine.Font = new System.Drawing.Font("Arial", 8F);
            this.GridView1.Appearance.HorzLine.ForeColor = System.Drawing.Color.Gray;
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.HorzLine.Options.UseFont = true;
            this.GridView1.Appearance.HorzLine.Options.UseForeColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.OddRow.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.OddRow.Font = new System.Drawing.Font("Arial", 8F);
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseFont = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.Preview.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Arial", 8F);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.Navy;
            this.GridView1.Appearance.Preview.Options.UseBackColor = true;
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.Row.Font = new System.Drawing.Font("Arial", 8F);
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseFont = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.GridView1.Appearance.RowSeparator.Font = new System.Drawing.Font("Arial", 8F);
            this.GridView1.Appearance.RowSeparator.ForeColor = System.Drawing.Color.Gray;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.RowSeparator.Options.UseFont = true;
            this.GridView1.Appearance.RowSeparator.Options.UseForeColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(138)))));
            this.GridView1.Appearance.SelectedRow.Font = new System.Drawing.Font("Arial", 8F);
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseFont = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.Silver;
            this.GridView1.Appearance.VertLine.Font = new System.Drawing.Font("Arial", 8F);
            this.GridView1.Appearance.VertLine.ForeColor = System.Drawing.Color.Gray;
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.Options.UseFont = true;
            this.GridView1.Appearance.VertLine.Options.UseForeColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.GridColumn_BatchID,
            this.GridColumn_DateCreated,
            this.GridColumn_CreatedBy,
            this.GridColumn_TypeDescription,
            this.GridColumn_BankDescription,
            this.GridColumn_Note,
            this.GridColumn_Type,
            this.GridColumn_Bank});
            this.GridView1.CustomizationFormBounds = new System.Drawing.Rectangle(566, 230, 216, 185);
            this.GridView1.GridControl = this.GridControl1;
            this.GridView1.Name = "GridView1";
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsSelection.MultiSelect = true;
            this.GridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            // 
            // GridColumn_BatchID
            // 
            this.GridColumn_BatchID.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_BatchID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_BatchID.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_BatchID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_BatchID.Caption = "Batch";
            this.GridColumn_BatchID.DisplayFormat.FormatString = "{0:f0}";
            this.GridColumn_BatchID.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_BatchID.FieldName = "item_key";
            this.GridColumn_BatchID.Name = "GridColumn_BatchID";
            this.GridColumn_BatchID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_BatchID.Visible = true;
            this.GridColumn_BatchID.VisibleIndex = 0;
            this.GridColumn_BatchID.Width = 63;
            // 
            // GridColumn_DateCreated
            // 
            this.GridColumn_DateCreated.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_DateCreated.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_DateCreated.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_DateCreated.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_DateCreated.Caption = "Date Created";
            this.GridColumn_DateCreated.DisplayFormat.FormatString = "{0:d}";
            this.GridColumn_DateCreated.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_DateCreated.FieldName = "date_created";
            this.GridColumn_DateCreated.Name = "GridColumn_DateCreated";
            this.GridColumn_DateCreated.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_DateCreated.Visible = true;
            this.GridColumn_DateCreated.VisibleIndex = 2;
            this.GridColumn_DateCreated.Width = 99;
            // 
            // GridColumn_CreatedBy
            // 
            this.GridColumn_CreatedBy.Caption = "Creator";
            this.GridColumn_CreatedBy.FieldName = "created_by";
            this.GridColumn_CreatedBy.Name = "GridColumn_CreatedBy";
            this.GridColumn_CreatedBy.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_CreatedBy.Width = 81;
            // 
            // GridColumn_TypeDescription
            // 
            this.GridColumn_TypeDescription.Caption = "Type";
            this.GridColumn_TypeDescription.FieldName = "type_description";
            this.GridColumn_TypeDescription.Name = "GridColumn_TypeDescription";
            this.GridColumn_TypeDescription.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_TypeDescription.Visible = true;
            this.GridColumn_TypeDescription.VisibleIndex = 4;
            this.GridColumn_TypeDescription.Width = 94;
            // 
            // GridColumn_BankDescription
            // 
            this.GridColumn_BankDescription.Caption = "Bank";
            this.GridColumn_BankDescription.FieldName = "bank";
            this.GridColumn_BankDescription.Name = "GridColumn_BankDescription";
            this.GridColumn_BankDescription.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_BankDescription.Visible = true;
            this.GridColumn_BankDescription.VisibleIndex = 3;
            this.GridColumn_BankDescription.Width = 86;
            // 
            // GridColumn_Note
            // 
            this.GridColumn_Note.Caption = "Note";
            this.GridColumn_Note.FieldName = "note";
            this.GridColumn_Note.Name = "GridColumn_Note";
            this.GridColumn_Note.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_Note.Visible = true;
            this.GridColumn_Note.VisibleIndex = 1;
            this.GridColumn_Note.Width = 275;
            // 
            // GridColumn_Type
            // 
            this.GridColumn_Type.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_Type.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_Type.Caption = "Type";
            this.GridColumn_Type.DisplayFormat.FormatString = "{0:f0}";
            this.GridColumn_Type.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_Type.FieldName = "proposal_type";
            this.GridColumn_Type.Name = "GridColumn_Type";
            this.GridColumn_Type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // GridColumn_Bank
            // 
            this.GridColumn_Bank.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_Bank.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_Bank.Caption = "Bank";
            this.GridColumn_Bank.FieldName = "bank";
            this.GridColumn_Bank.Name = "GridColumn_Bank";
            this.GridColumn_Bank.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // OpenControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_Select);
            this.Controls.Add(this.GridControl1);
            this.Name = "OpenControl";
            this.Size = new System.Drawing.Size(680, 231);
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected DevExpress.XtraEditors.SimpleButton Button_Cancel;
        protected DevExpress.XtraEditors.SimpleButton Button_Select;
        protected DevExpress.XtraGrid.GridControl GridControl1;
        protected DevExpress.XtraGrid.Views.Grid.GridView GridView1;
        protected DevExpress.XtraGrid.Columns.GridColumn GridColumn_BatchID;
        protected DevExpress.XtraGrid.Columns.GridColumn GridColumn_DateCreated;
        protected DevExpress.XtraGrid.Columns.GridColumn GridColumn_CreatedBy;
        protected DevExpress.XtraGrid.Columns.GridColumn GridColumn_TypeDescription;
        protected DevExpress.XtraGrid.Columns.GridColumn GridColumn_BankDescription;
        protected DevExpress.XtraGrid.Columns.GridColumn GridColumn_Note;
        protected DevExpress.XtraGrid.Columns.GridColumn GridColumn_Type;
        protected DevExpress.XtraGrid.Columns.GridColumn GridColumn_Bank;
    }
}
