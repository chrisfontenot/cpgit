namespace DebtPlus.UI.FormLib.Proposals
{
    partial class CreateControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            CreateControl_Dispose(disposing);
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Button_Create = new DevExpress.XtraEditors.SimpleButton();
            this.SuspendLayout();
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.Location = new System.Drawing.Point(591, 70);
            // 
            // Button_Select
            // 
            this.Button_Select.TabIndex = 0;
            // 
            // Button_Create
            // 
            this.Button_Create.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Create.Location = new System.Drawing.Point(591, 41);
            this.Button_Create.Name = "Button_Create";
            this.Button_Create.Size = new System.Drawing.Size(75, 23);
            this.Button_Create.TabIndex = 1;
            this.Button_Create.Text = "&New...";
            // 
            // CreateControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Button_Create);
            this.Name = "CreateControl";
            this.Controls.SetChildIndex(this.Button_Create, 0);
            this.Controls.SetChildIndex(this.Button_Cancel, 0);
            this.Controls.SetChildIndex(this.Button_Select, 0);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton Button_Create;
    }
}
