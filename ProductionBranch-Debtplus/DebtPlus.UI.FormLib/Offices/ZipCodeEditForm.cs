using System;
using System.Data;
using System.Windows.Forms;

namespace DebtPlus.UI.FormLib.Offices
{
    public partial class ZipCodeEditForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        private DataRowView drv;

        /// <summary>
        /// Process the disposal for the form. Remove any storage allocaetd in the form.
        /// </summary>
        /// <param name="disposing">TRUE when Dispose() is called. FALSE for Finalize.</param>
        private void ZipCodeEditForm_Dispose(bool disposing)
        {
            // Remove the handlers so that the form may be properly destroyed
            UnRegisterHandlers();
        }

        public ZipCodeEditForm() : this((DataRowView) null)
        {
        }

        public ZipCodeEditForm(DataRowView drv)
        {
            this.drv = drv;
            InitializeComponent();
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            this.Load += ZipCodeEditForm_Load;
            TextEdit1.EditValueChanged += this.TextEdit1_EditValueChanged;
        }

        /// <summary>
        /// Remove the event handler registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            this.Load -= ZipCodeEditForm_Load;
            TextEdit1.EditValueChanged -= this.TextEdit1_EditValueChanged;
        }

        /// <summary>
        /// Process the LOAD event on the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ZipCodeEditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                TextEdit1.DataBindings.Clear();
                TextEdit1.DataBindings.Add("EditValue", drv, "PostalCode", false, DataSourceUpdateMode.OnPropertyChanged);

                string Value = DebtPlus.Utils.Nulls.DStr(drv["PostalCode"], string.Empty).Trim();
                SimpleButton1.Enabled = Value.Length == 5;
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void TextEdit1_EditValueChanged(object sender, EventArgs e)
        {
            string Value = TextEdit1.Text.Trim('_');
            SimpleButton1.Enabled = Value.Length == 5;
        }
    }
}