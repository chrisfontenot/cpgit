using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DebtPlus.Utils;

namespace DebtPlus.UI.FormLib.Offices
{
    internal static class Tables
    {
        // Allocate a static copy of the database linkage class
        internal static DataSet ds = new DataSet("ds");

        /// <summary>
        /// Return a list of the offices table
        /// </summary>
        /// <returns></returns>
        internal static DataTable OfficesTable()
        {
            return GetTypesTable("offices", "office", "SELECT [office],[name],[type],[AddressID],[EmailID],[TelephoneID],[AltTelephoneID],[FAXID],[Default] AS 'default',[ActiveFlag],[hud_hcs_id],[default_till_missed],[counselor],[region],[district],[directions],[date_created],[created_by] FROM offices WITH (NOLOCK)");
        }

        /// <summary>
        /// Retrieve the list of housing ID codes
        /// </summary>
        /// <returns>A pointer to the DataTable in the current DataSet</returns>
        internal static DataTable hcs_ids_table()
        {
            return GetTypesTable("housing_arm_hcs_ids", "hcs_id", "SELECT [hcs_id],[Description],[username],[password],[counseling_amount],[faith_based],[colonias],[migrant_farm_worker],[validation_date],[default],[activeflag] FROM housing_arm_hcs_ids WITH (NOLOCK)");
        }

        /// <summary>
        /// Return the list of office types
        /// </summary>
        /// <returns>A pointer to the DataTable in the current DataSet</returns>
        internal static DataTable OfficeTypesTable()
        {
            return GetTypesTable("OfficeTypes", "oID", "SELECT [oID], [description], [ActiveFlag], [Default] AS 'default' FROM [OfficeTypes] WITH (NOLOCK)");
        }

        /// <summary>
        /// Return the default office type from the system tables
        /// </summary>
        /// <returns>The value that corresponds to the default setting for the office types field.</returns>
        internal static object DefaultOfficeType
        {
            get
            {
                return getDefault(OfficeTypesTable());
            }
        }

        /// <summary>
        /// Return the default value for the indicated table. It must have a Default column.
        /// </summary>
        /// <param name="tbl">Pointer to the table for the search</param>
        /// <returns></returns>
        internal static object getDefault(DataTable tbl)
        {
            // Ensure that there is a Default column.
            System.Diagnostics.Debug.Assert(tbl != null && tbl.Columns.Contains("default"));

            object answer = DBNull.Value;
            if (tbl != null)
            {
                DataRow[] rows = tbl.Select("[default]<>False", tbl.PrimaryKey[0].ColumnName);
                if (rows.GetUpperBound(0) >= 0)
                {
                    answer = rows[0][tbl.PrimaryKey[0]];
                }
            }
            return answer;
        }

        /// <summary>
        /// Return a list of the counselor names
        /// </summary>
        /// <returns>A pointer to the DataTable in the current DataSet</returns>
        internal static DataTable CounselorsTable()
        {
            return GetTypesTable("Counselors", "counselor", "SELECT co.counselor, co.[default], co.[ActiveFlag], dbo.format_normal_name(default,cox.first,default,cox.last,default) AS name FROM counselors co WITH (NOLOCK) LEFT OUTER JOIN names cox on co.NameID=cox.Name");
        }

        /// <summary>
        /// Return a list of the state names
        /// </summary>
        /// <returns>A pointer to the DataTable in the current DataSet</returns>
        internal static DataTable StatesTable()
        {
            return GetTypesTable("states", "state", "SELECT s.State, s.MailingCode, s.Name, c.description as Country, s.[default], s.[ActiveFlag] FROM states s WITH (NOLOCK) LEFT OUTER JOIN countries c ON s.country = c.country ORDER BY s.country, s.MailingCode, s.name");
        }

        /// <summary>
        /// Return the default state record
        /// </summary>
        /// <returns></returns>
        internal static Object DefaultState()
        {
            return getDefault(StatesTable());
        }

        /// <summary>
        /// Return a list of the region names
        /// </summary>
        /// <returns>A pointer to the DataTable in the current DataSet</returns>
        internal static DataTable RegionsTable()
        {
            return GetTypesTable("regions", "region", "SELECT region, description, [default], [ActiveFlag] FROM regions WITH (NOLOCK)");
        }

        /// <summary>
        /// Return a list of the district names
        /// </summary>
        /// <returns>A pointer to the DataTable in the current DataSet</returns>
        internal static DataTable DistrictsTable()
        {
            return GetTypesTable("districts", "district", "SELECT district, description, [default], [ActiveFlag] FROM districts WITH (NOLOCK)");
        }

        /// <summary>
        /// Return a list of the county names
        /// </summary>
        /// <returns>A pointer to the DataTable in the current DataSet</returns>
        internal static DataTable CountiesTable()
        {
            return GetTypesTable("counties", "county", "SELECT county, description, [default], [ActiveFlag] FROM counties WITH (NOLOCK)");
        }

        /// <summary>
        /// Return a list of the country names
        /// </summary>
        /// <returns>A pointer to the DataTable in the current DataSet</returns>
        internal static DataTable CountriesTable()
        {
            return GetTypesTable("countries", "country", "SELECT [country], [description], [default], [ActiveFlag] FROM countries WITH (NOLOCK)");
        }

        /// <summary>
        /// Return a pointer to a types table.
        /// </summary>
        /// <param name="TableName">Name of the table</param>
        /// <param name="KeyName">Key field in the table.</param>
        /// <param name="SelectStatement">Statement used to select the data from the database</param>
        /// <returns>A pointer to the DataTable in the current DataSet</returns>
        private static DataTable GetTypesTable(string TableName, string KeyName, string SelectStatement)
        {
            DataTable tbl = ds.Tables[TableName];
            if (tbl == null)
            {
                SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                Cursor current_cursor = Cursor.Current;
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    cn.Open();

                    // Read the entire table into the dataset. This only works for the simple 'types' tables.
                    using (var cmd = new SqlCommand())
                    {
                        cmd.Connection = cn;
                        cmd.CommandText = SelectStatement;
                        cmd.CommandType = CommandType.Text;

                        using (var da = new SqlDataAdapter(cmd))
                        {
                            da.Fill(ds, TableName);
                            tbl = ds.Tables[TableName];

                            tbl.PrimaryKey = new DataColumn[] { tbl.Columns[KeyName] };
                            tbl.Columns[KeyName].AutoIncrement = true;
                            tbl.Columns[KeyName].AutoIncrementSeed = -1;
                            tbl.Columns[KeyName].AutoIncrementStep = -1;
                        }
                    }
                }

                catch (SqlException ex)
                {
                    // Log any error that is found. There should not be an error.
                    using (var gdr = new DebtPlus.Repository.GetDataResult())
                    {
                        gdr.HandleException(ex);
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, string.Format("Error reading {0} table", TableName));
                    }
                }

                finally
                {
                    // Ensure that the connection is closed down
                    if (cn != null)
                    {
                        if (cn.State != ConnectionState.Closed)
                        {
                            cn.Close();
                        }
                        cn.Dispose();
                    }
                    Cursor.Current = current_cursor;
                }
            }

            // Return the pointer to the table
            return tbl;
        }
    }
}
