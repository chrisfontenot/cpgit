using System;
using System.Data;
using System.Windows.Forms;

namespace DebtPlus.UI.FormLib.Offices
{
    public partial class OfficeEditForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        DataRowView drv;

        /// <summary>
        /// Process the disposal for the form. Remove any storage allocated in the form.
        /// </summary>
        /// <param name="disposing">TRUE when Dispose() is called. FALSE for Finalize.</param>
        private void OfficeEditForm_Dispose(bool disposing)
        {
            if (disposing)
            {
                // Remove the handlers so that the form may be properly destroyed
                UnRegisterHandlers();
            }

            drv = null;
        }

        public OfficeEditForm() : this(null)
        {
        }

        public OfficeEditForm(DataRowView drv) 
        {
            this.drv = drv;
            InitializeComponent();

            // Hook into the events
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            this.Load += OfficeEditForm_Load;

            lookupedit_hud_hcs_id.EditValueChanged += LookupEdit_EditValueChanged;
            office_type.EditValueChanged += LookupEdit_EditValueChanged;
            office_region.EditValueChanged += LookupEdit_EditValueChanged;
            office_district.EditValueChanged += LookupEdit_EditValueChanged;
            office_counselor.EditValueChanged += LookupEdit_EditValueChanged;
            office_name.EditValueChanged += office_name_EditValueChanged;

            lookupedit_hud_hcs_id.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            office_type.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            office_region.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            office_district.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            office_counselor.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;

            SimpleButton_OK.Click += SimpleButton_OK_Click;
        }

        /// <summary>
        /// Remove the event handler registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            this.Load -= OfficeEditForm_Load;

            lookupedit_hud_hcs_id.EditValueChanged -= LookupEdit_EditValueChanged;
            office_type.EditValueChanged -= LookupEdit_EditValueChanged;
            office_region.EditValueChanged -= LookupEdit_EditValueChanged;
            office_district.EditValueChanged -= LookupEdit_EditValueChanged;
            office_counselor.EditValueChanged -= LookupEdit_EditValueChanged;
            office_name.EditValueChanged -= office_name_EditValueChanged;

            lookupedit_hud_hcs_id.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            office_type.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            office_region.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            office_district.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            office_counselor.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;

            SimpleButton_OK.Click -= SimpleButton_OK_Click;
        }

        /// <summary>
        /// Process the LOAD event on the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OfficeEditForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();

            try
            {
                using (var cm = new DebtPlus.UI.Common.CursorManager())
                {
                    office_type.Properties.DataSource = Tables.OfficeTypesTable().DefaultView;
                    office_region.Properties.DataSource = Tables.RegionsTable().DefaultView;
                    office_district.Properties.DataSource = Tables.DistrictsTable().DefaultView;
                    office_counselor.Properties.DataSource = Tables.CounselorsTable().DefaultView;
                    lookupedit_hud_hcs_id.Properties.DataSource = Tables.hcs_ids_table().DefaultView;

                    // Bind the fields to the data form
                    office_ActiveFlag.DataBindings.Add("Checked", drv, "activeflag");
                    office_counselor.DataBindings.Add("EditValue", drv, "counselor");
                    office_default.DataBindings.Add("Checked", drv, "default");
                    office_default_till_missed.DataBindings.Add("EditValue", drv, "default_till_missed");
                    office_directions.DataBindings.Add("EditValue", drv, "directions");
                    office_district.DataBindings.Add("EditValue", drv, "district");
                    office_name.DataBindings.Add("EditValue", drv, "name");
                    office_region.DataBindings.Add("EditValue", drv, "region");
                    office_type.DataBindings.Add("EditValue", drv, "type");
                    TextEdit_NFCC.DataBindings.Add("EditValue", drv, "NFCC");
                    lookupedit_hud_hcs_id.DataBindings.Add("EditValue", drv, "HUD_hcs_id");

                    TelephoneNumberRecordControl_AltTelephoneID.EditValue = DebtPlus.Utils.Nulls.v_Int32(drv["AltTelephoneID"]);
                    TelephoneNumberRecordControl_FAXID.EditValue = DebtPlus.Utils.Nulls.v_Int32(drv["FAXID"]);
                    TelephoneNumberRecordControl_TelephoneID.EditValue = DebtPlus.Utils.Nulls.v_Int32(drv["TelephoneID"]);
                    AddressRecordControl1.EditValue = DebtPlus.Utils.Nulls.v_Int32(drv["AddressID"]);

                    // Load the list of ZIP-Codes
                    zipCodeListControl1.ReadForm(drv);
                }
            }
            finally
            {
                RegisterHandlers();
            }

            // Disable the OK button since there are no changes
            SimpleButton_OK.Enabled = ! HasErrors();
        }

        /// <summary>
        /// Public interface to save the ZIP-Code list for the updated office data.
        /// </summary>
        /// <param name="office"></param>
        public void SaveZipcodes(Int32 office)
        {
            zipCodeListControl1.SaveForm(office);
        }

        /// <summary>
        /// Determine if an error condition is being displayed
        /// </summary>
        /// <returns></returns>
        private bool HasErrors()
        {
            bool answer = true;
            for(;;)
            {
                if( office_ActiveFlag.ErrorText != string.Empty )
                {
                    break;
                }
            
                if( office_counselor.ErrorText != string.Empty )
                {
                    break;
                }
            
                if( office_default.ErrorText != string.Empty )
                {
                    break;
                }
            
                if( office_default_till_missed.ErrorText != string.Empty )
                {
                    break;
                }
            
                if( office_directions.ErrorText != string.Empty )
                {
                    break;
                }
            
                if( office_district.ErrorText != string.Empty )
                {
                    break;
                }
            
                if( office_name.ErrorText != string.Empty )
                {
                    break;
                }
            
                if( office_region.ErrorText != string.Empty )
                {
                    break;
                }

                if( office_type.ErrorText != string.Empty )
                {
                    break;
                }

                // There are no errors
                answer = false;
                break;
            }

            return answer;
        }

        /// <summary>
        /// Process a change in the lookup field values
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LookupEdit_EditValueChanged(object sender, EventArgs e) // Handles office_type.EditValueChanged, office_region.EditValueChanged, office_district.EditValueChanged, office_counselor.EditValueChanged
        {
            string ErrorText = string.Empty;
            if( ((DevExpress.XtraEditors.LookUpEdit) sender).EditValue == null )
            {
                ErrorText = "A value is required";
            }
            DxErrorProvider1.SetError((Control) sender, ErrorText);

            // Set or clear the error condition
            SimpleButton_OK.Enabled = ! HasErrors();
        }

        /// <summary>
        /// A change in the office name must have an office name
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void office_name_EditValueChanged(object sender, EventArgs e) // Handles office_name.EditValueChanged
        {
            string ErrorText = string.Empty;
            if( DebtPlus.Utils.Nulls.DStr( ((DevExpress.XtraEditors.TextEdit) sender).EditValue, string.Empty ).Trim() == string.Empty )
            {
                ErrorText = "A value is required";
            }

            DxErrorProvider1.SetError((Control) sender, ErrorText);

            // Set or clear the error condition
            SimpleButton_OK.Enabled = ! HasErrors();
        }

        /// <summary>
        /// Do updates to the non-data items in the row when we accept changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SimpleButton_OK_Click(object sender, EventArgs e) // Handles SimpleButton_OK.Click
        {
            drv["TelephoneID"] = DebtPlus.Utils.Nulls.ToDbNull(TelephoneNumberRecordControl_TelephoneID.EditValue);
            drv["AltTelephoneID"] = DebtPlus.Utils.Nulls.ToDbNull(TelephoneNumberRecordControl_AltTelephoneID.EditValue);
            drv["FAXID"] = DebtPlus.Utils.Nulls.ToDbNull(TelephoneNumberRecordControl_FAXID.EditValue);
            drv["AddressID"] = DebtPlus.Utils.Nulls.ToDbNull(AddressRecordControl1.EditValue);
        }
    }
}