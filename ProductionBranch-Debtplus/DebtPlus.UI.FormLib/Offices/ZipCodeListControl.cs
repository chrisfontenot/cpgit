using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DebtPlus.Utils;

namespace DebtPlus.UI.FormLib.Offices
{
    public partial class ZipCodeListControl : DevExpress.XtraEditors.XtraUserControl
    {


        const string TableName = "zipcodes";
        protected DataSet ds = new DataSet("ds");
        protected Int32 office { get; private set; }

        /// <summary>
        /// Handle the dispose event for the control
        /// </summary>
        /// <param name="disposing"></param>
        private void ZipCodeListControl_Dispose(bool disposing)
        {
            if( disposing )
            {
                ds.Dispose();
                UnRegisterHandlers();
            }
        }

        /// <summary>
        /// Create a new instance of our class
        /// </summary>
        public ZipCodeListControl()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        protected void RegisterHandlers()
        {
            ContextMenu1.Popup += ContextMenu1_Popup;
            ContextMenu1_Create.Click += ContextMenu1_Create_Click;
            ContextMenu1_Delete.Click += ContextMenu1_Delete_Click;
            ContextMenu1_Edit.Click += ContextMenu1_Edit_Click;
            GridView1.DoubleClick += GridView1_DoubleClick;
        }

        /// <summary>
        /// Remove the event registration from our form
        /// </summary>
        protected void UnRegisterHandlers()
        {
            ContextMenu1.Popup -= ContextMenu1_Popup;
            ContextMenu1_Create.Click -= ContextMenu1_Create_Click;
            ContextMenu1_Delete.Click -= ContextMenu1_Delete_Click;
            ContextMenu1_Edit.Click -= ContextMenu1_Edit_Click;
            GridView1.DoubleClick -= GridView1_DoubleClick;
        }

        /// <summary>
        /// Return the pointer to the Zipcodes table entry in the dataset
        /// </summary>
        /// <returns></returns>
        protected DataTable ZipCodeTable()
        {
            DataTable tbl = ds.Tables[TableName];
            if( tbl == null )
            {
                SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                try
                {
                    cn.Open();
                    using( SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = cn;
                        cmd.CommandText = "SELECT zipcode, zip_lower, zip_upper, left(zip_lower,5) as PostalCode FROM zipcodes WHERE [office]=@office";
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.Add("@office", SqlDbType.Int).Value = office;

                        using( SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            da.Fill(ds, TableName);
                            tbl = ds.Tables[TableName];
                        }
                    }

                    // We need a primary key to this table
                    tbl.PrimaryKey = new DataColumn[] {tbl.Columns["zipcode"]};

                    // Do not allow invalid items to appear in the table
                    if( office <= 0 )
                    {
                        tbl.Clear();
                    }

                    // Ensure that the primary key is auto-increment
                    if( ! tbl.Columns["zipcode"].AutoIncrement )
                    {
                        tbl.Columns["zipcode"].AutoIncrement = true;
                        tbl.Columns["zipcode"].AutoIncrementSeed = -1;
                        tbl.Columns["zipcode"].AutoIncrementStep = -1;
                    }
                }

                catch( SqlException ex )
                {
                    using (var gdr = new Repository.GetDataResult())
                    {
                        gdr.HandleException(ex);
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading zipcodes table");
                    }
                }

                finally
                {
                    if( cn != null )
                    {
                        if( cn.State != ConnectionState.Closed )
                        {
                            cn.Close();
                        }
                        cn.Dispose();
                    }
                }
            }

            return tbl;
        }

        /// <summary>
        /// Read the list of zipodes for the current office pointer
        /// </summary>
        /// <param name="drv">Pointer to the office record</param>
        public void ReadForm(DataRowView drv)
        {
            // Find the office number from the record
            office = drv.IsNew ? -1 : DebtPlus.Utils.Nulls.DInt(drv["office"], -1);

            // Obtain the list of zipcodes for this office and bind the list control with the view
            ds.Clear();
            GridControl1.DataSource = ZipCodeTable().DefaultView;
        }

        /// <summary>
        /// Save the list of zipcodes to the database
        /// </summary>
        /// <param name="office">The current office number if the records are to be inserted.</param>
        public void SaveForm(Int32 office)
        {
            DataTable tbl = ZipCodeTable();
            if( tbl != null )
            {
                Int32 RowCount = 0;
                SqlCommand InsertCmd = new SqlCommand();
                SqlCommand UpdateCmd = new SqlCommand();
                SqlCommand DeleteCmd = new SqlCommand();
                SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                Cursor current_cursor = Cursor.Current;

                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    cn.Open();
                    using ( var da = new SqlDataAdapter() )
                    {
                        // Insert command
                        InsertCmd.CommandText = "xpr_insert_zipcodes";
                        InsertCmd.CommandType = CommandType.StoredProcedure;
                        InsertCmd.Connection = cn;
                        InsertCmd.Parameters.Add("@RETURN", SqlDbType.Int, 0, "zipcode").Direction = ParameterDirection.ReturnValue;
                        InsertCmd.Parameters.Add("@office", SqlDbType.Int).Value = office;
                        InsertCmd.Parameters.Add("@Zipcode", SqlDbType.VarChar, 0, "PostalCode");
                        da.InsertCommand = InsertCmd;

                        // Update command
                        UpdateCmd.CommandText = "UPDATE zipcodes SET zip_lower=@PostalCode+'0000', zip_upper=@PostalCode+'9999',office=@office WHERE zipcode=@zipcode";
                        UpdateCmd.CommandType = CommandType.Text;
                        UpdateCmd.Connection = cn;
                        UpdateCmd.Parameters.Add("@zipcode", SqlDbType.Int, 0, "zipcode");
                        UpdateCmd.Parameters.Add("@office", SqlDbType.Int).Value = office;
                        UpdateCmd.Parameters.Add("@PostalCode", SqlDbType.VarChar, 0, "PostalCode");
                        da.UpdateCommand = UpdateCmd;

                        // DeleteCmd command
                        DeleteCmd.CommandText = "DELETE FROM zipcodes WHERE zipcode=@zipcode";
                        DeleteCmd.CommandType = CommandType.Text;
                        DeleteCmd.Connection = cn;
                        DeleteCmd.Parameters.Add("@zipcode", SqlDbType.Int, 0, "PostalCode");
                        da.DeleteCommand = DeleteCmd;

                        RowCount = da.Update(tbl);
                    }
                }

                catch( SqlException ex )
                {
                    // Generate an error on the request if needed
                    using (var gdr = new Repository.GetDataResult())
                    {
                        gdr.HandleException(ex);
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error updating zipcodes table");
                    }
                }

                finally
                {
                    // Ensure that the connection is disposed
                    if( cn != null )
                    {
                        if( cn.State != ConnectionState.Closed )
                        {
                            cn.Close();
                        }
                        cn.Dispose();
                    }

                    // Dispose of the command blocks
                    UpdateCmd.Dispose();
                    InsertCmd.Dispose();
                    DeleteCmd.Dispose();

                    // Restore the cursor for the form
                    Cursor.Current = current_cursor;
                }
            }
        }

        /// <summary>
        /// A DOUBLE-CLICK on a row is always an EDIT operation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridView1_DoubleClick(object sender, EventArgs e) // Handles GridView1.DoubleClick
        {
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = GridView1.CalcHitInfo((GridControl1.PointToClient(Control.MousePosition)));
            Int32 RowHandle = hi.IsValid && hi.InRow ? hi.RowHandle : -1;

            // Find the row view in the dataview object for this row.
            if( RowHandle >= 0 )
            {
                GridView1.FocusedRowHandle = RowHandle;
                DataRowView drv = (DataRowView) GridView1.GetRow(RowHandle);

                // If the row is defined then edit the row.
                if( drv != null )
                {
                    EditRow(drv);
                }
            }
        }

        /// <summary>
        /// Process the POPUP event on the context menu. Enable / Disable menu items accordingly.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ContextMenu1_Popup(object sender, EventArgs e) // Handles ContextMenu1.Popup
        {
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = GridView1.CalcHitInfo((GridControl1.PointToClient(Control.MousePosition)));
            Int32 RowHandle = hi.IsValid && hi.InRow ? hi.RowHandle : -1;

            if( RowHandle >= 0 )
            {
                DataRowView drv = (DataRowView) GridView1.GetRow(RowHandle);
                if( drv != null )
                {
                    ContextMenu1_Edit.Enabled = true;
                    ContextMenu1_Delete.Enabled = true;
                    return;
                }
            }

            ContextMenu1_Edit.Enabled = false;
            ContextMenu1_Delete.Enabled = false;
        }

        /// <summary>
        /// Handle the CLICK event on the CREATE popup menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ContextMenu1_Create_Click(object sender, EventArgs e) // Handles ContextMenu1_Create.Click
        {
            DataView vue = GridControl1.DataSource as DataView;
            if( vue != null )
            {
                EditRow( vue.AddNew() );
            }
        }

        /// <summary>
        /// Handle the CLICK event on the DELETE popup menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ContextMenu1_Delete_Click(object sender, EventArgs e) // Handles ContextMenu1_Delete.Click
        {
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = GridView1.CalcHitInfo((GridControl1.PointToClient(Control.MousePosition)));
            Int32 RowHandle = hi.IsValid && hi.InRow ? hi.RowHandle : -1;

            if( RowHandle >= 0 )
            {
                DataRowView drv = (DataRowView) GridView1.GetRow(RowHandle);
                if( drv != null )
                {
                    drv.Delete();
                }
            }
        }

        /// <summary>
        /// Handle the CLICK event on the EDIT popup menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ContextMenu1_Edit_Click(object sender, EventArgs e) // Handles ContextMenu1_Edit.Click
        {
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = GridView1.CalcHitInfo((GridControl1.PointToClient(Control.MousePosition)));
            Int32 RowHandle = hi.IsValid && hi.InRow ? hi.RowHandle : -1;

            // If there is a row then edit it.
            if( RowHandle >= 0 )
            {
                DataRowView drv = (DataRowView) GridView1.GetRow(RowHandle);
                if( drv != null )
                {
                    EditRow( drv );
                }
            }
        }

        /// <summary>
        /// Edit the indicated row in the table
        /// </summary>
        /// <param name="drv">Pointer to the row to be edited</param>
        private void EditRow(DataRowView drv)
        {
            // Edit the record with the appropriate edit form
            using( ZipCodeEditForm frm = new ZipCodeEditForm( drv ))
            {
                drv.BeginEdit();

                // Either cancel or accept the edit based upon the form completion.
                if ( frm.ShowDialog() == DialogResult.OK )
                {
                    drv.EndEdit();
                }
                else
                {
                    drv.CancelEdit();
                }
            }
        }
    }
}
