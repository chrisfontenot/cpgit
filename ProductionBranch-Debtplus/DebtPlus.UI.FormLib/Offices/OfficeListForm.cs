using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DebtPlus.Utils;

namespace DebtPlus.UI.FormLib.Offices
{
    public partial class OfficeListForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        /// <summary>
        /// Process the disposal for the form. Remove any storage allocated in the form.
        /// </summary>
        /// <param name="disposing">TRUE when Dispose() is called. FALSE for Finalize.</param>
        private void OfficeEditForm_Dispose(bool disposing)
        {
            if (disposing)
            {
                // Remove the handlers so that the form may be properly destroyed
                UnRegisterHandlers();
            }
        }

        public OfficeListForm()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            this.Load += MainTemplateForm_Load;
            barButtonItem_Create.ItemClick += ContextMenu1_Create_Click;
            barButtonItem_Delete.ItemClick += ContextMenu1_Delete_Click;
            barButtonItem_Edit.ItemClick += ContextMenu1_Edit_Click;
            GridView1.DoubleClick += GridView1_DoubleClick;
            GridView1.MouseDown += GridView1_MouseDown;
            SimpleButton_New.Click += this.SimpleButton_New_Click;
            SimpleButton_Edit.Click += this.SimpleButton_Edit_Click;
            SimpleButton_OK.Click += this.SimpleButton_OK_Click;
        }

        /// <summary>
        /// Remove the event handler registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            this.Load -= MainTemplateForm_Load;
            barButtonItem_Create.ItemClick -= ContextMenu1_Create_Click;
            barButtonItem_Delete.ItemClick -= ContextMenu1_Delete_Click;
            barButtonItem_Edit.ItemClick -= ContextMenu1_Edit_Click;
            GridView1.DoubleClick -= GridView1_DoubleClick;
            SimpleButton_New.Click -= SimpleButton_New_Click;
            SimpleButton_Edit.Click -= SimpleButton_Edit_Click;
            SimpleButton_OK.Click -= SimpleButton_OK_Click;
        }

        /// <summary>
        /// Process the LOAD event on the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainTemplateForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                DataTable tbl = Tables.OfficesTable();
                if( tbl != null )
                {
                    tbl.Columns["name"].DefaultValue = string.Empty;
                    tbl.Columns["ActiveFlag"].DefaultValue = true;
                    tbl.Columns["default"].DefaultValue = false;
                    tbl.Columns["type"].DefaultValue = Tables.DefaultOfficeType;
                    tbl.Columns["counselor"].DefaultValue = 1;
                    tbl.Columns["district"].DefaultValue = 1;
                    tbl.Columns["region"].DefaultValue = 1;
                    
                    GridControl1.DataSource = tbl.DefaultView;
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Commit the changes to the database
        /// </summary>
        public void CommitChanges()
        {
            DataTable tbl = Tables.OfficesTable();
            if( tbl != null )
            {
                SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                SqlCommand InsertCmd = new SqlCommand();
                SqlCommand UpdateCmd = new SqlCommand();
                SqlCommand DeleteCmd = new SqlCommand();

                Cursor current_cursor = Cursor.Current;
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    cn.Open();
                    using (var da = new SqlDataAdapter())
                    {
                        // Insert command
                        InsertCmd.Connection = cn;
                        InsertCmd.CommandText = "xpr_insert_office";
                        InsertCmd.CommandType = CommandType.StoredProcedure;

                        InsertCmd.Parameters.Add("@office", SqlDbType.Int, 0, "office").Direction = ParameterDirection.ReturnValue;
                        InsertCmd.Parameters.Add("@default_till_missed", SqlDbType.Int, 0, "default_till_missed");
                        InsertCmd.Parameters.Add("@Name", SqlDbType.VarChar, 80, "Name");
                        InsertCmd.Parameters.Add("@Type", SqlDbType.Int, 0, "Type");
                        InsertCmd.Parameters.Add("@AddressID", SqlDbType.Int, 0, "AddressID");
                        InsertCmd.Parameters.Add("@Counselor", SqlDbType.Int, 0, "Counselor");
                        InsertCmd.Parameters.Add("@TelephoneID", SqlDbType.Int, 0, "TelephoneID");
                        InsertCmd.Parameters.Add("@AltTelephoneID", SqlDbType.Int, 0, "AltTelephoneID");
                        InsertCmd.Parameters.Add("@FAXID", SqlDbType.Int, 0, "FAXID");
                        InsertCmd.Parameters.Add("@EmailID", SqlDbType.Int, 0, "EmailID");
                        InsertCmd.Parameters.Add("@Default", SqlDbType.Bit, 0, "Default");
                        InsertCmd.Parameters.Add("@ActiveFlag", SqlDbType.Bit, 0, "ActiveFlag");
                        InsertCmd.Parameters.Add("@Directions", SqlDbType.Text, Int32.MaxValue, "Directions");
                        InsertCmd.Parameters.Add("@Region", SqlDbType.Int, 0, "Region");
                        InsertCmd.Parameters.Add("@District", SqlDbType.Int, 0, "District");
                        InsertCmd.Parameters.Add("@NFCC", SqlDbType.VarChar, 80, "NFCC");
                        InsertCmd.Parameters.Add("@HUD_hcs_id", SqlDbType.VarChar, 80, "HUD_hcs_id");

                        da.InsertCommand = InsertCmd;

                        // Update command
                        UpdateCmd.CommandText = "UPDATE offices SET [default_till_missed]=@default_till_missed,[Name]=@Name,[Counselor]=@Counselor,[TelephoneID]=@TelephoneID,[AltTelephoneID]=@AltTelephoneID,[FAXID]=@FAXID,[EmailID]=@EmailID,[Default]=@Default,[ActiveFlag]=@ActiveFlag,[Directions]=@Directions,[Region]=@Region,[District]=@District,[AddressID]=@AddressID,[Type]=@Type,[HUD_hcs_id]=@HUD_hcs_id WHERE [office]=@office";
                        UpdateCmd.CommandType = CommandType.Text;
                        UpdateCmd.Connection = cn;

                        UpdateCmd.Parameters.Add("@default_till_missed", SqlDbType.Int, 0, "default_till_missed");
                        UpdateCmd.Parameters.Add("@Name", SqlDbType.VarChar, 80, "Name");
                        UpdateCmd.Parameters.Add("@Type", SqlDbType.Int, 0, "Type");
                        UpdateCmd.Parameters.Add("@AddressID", SqlDbType.Int, 0, "AddressID");
                        UpdateCmd.Parameters.Add("@Counselor", SqlDbType.Int, 0, "Counselor");
                        UpdateCmd.Parameters.Add("@TelephoneID", SqlDbType.Int, 0, "TelephoneID");
                        UpdateCmd.Parameters.Add("@AltTelephoneID", SqlDbType.Int, 0, "AltTelephoneID");
                        UpdateCmd.Parameters.Add("@FAXID", SqlDbType.Int, 0, "FAXID");
                        UpdateCmd.Parameters.Add("@EmailID", SqlDbType.Int, 0, "EmailID");
                        UpdateCmd.Parameters.Add("@Default", SqlDbType.Bit, 0, "Default");
                        UpdateCmd.Parameters.Add("@ActiveFlag", SqlDbType.Bit, 0, "ActiveFlag");
                        UpdateCmd.Parameters.Add("@Directions", SqlDbType.Text, Int32.MaxValue, "Directions");
                        UpdateCmd.Parameters.Add("@Region", SqlDbType.Int, 0, "Region");
                        UpdateCmd.Parameters.Add("@District", SqlDbType.Int, 0, "District");
                        UpdateCmd.Parameters.Add("@HUD_hcs_id", SqlDbType.VarChar, 80, "HUD_hcs_id");
                        UpdateCmd.Parameters.Add("@office", SqlDbType.Int, 0, "office");

                        da.UpdateCommand = UpdateCmd;

                        // Delete command
                        DeleteCmd.CommandText = "DELETE FROM offices WHERE [office]=@office";
                        DeleteCmd.CommandType = CommandType.Text;
                        DeleteCmd.Connection = cn;
                        DeleteCmd.Parameters.Add("@office", SqlDbType.Int, 0, "office");

                        da.DeleteCommand = DeleteCmd;

                        // Do the table update now.
                        da.Update(tbl);
                    }
                }

                catch (SqlException ex)
                {
                    using (var gdr = new Repository.GetDataResult())
                    {
                        gdr.HandleException(ex);
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error updating the offices table");
                    }
                }

                finally
                {
                    if( cn != null )
                    {
                        if( cn.State != ConnectionState.Closed )
                        {
                            cn.Close();
                        }
                        cn.Dispose();
                    }
                    Cursor.Current = current_cursor;
                }
            }
        }

        /// <summary>
        /// Perform the edit operation on the current office record
        /// </summary>
        /// <param name="drv"></param>
        public void EditRow(DataRowView drv)
        {
            drv.BeginEdit();

            // Edit the office. If accepted then update the database immdiately.
            using( Offices.OfficeEditForm frm = new Offices.OfficeEditForm(drv))
            {
                if( frm.ShowDialog() == DialogResult.OK )
                {
                    drv.EndEdit();
                    CommitChanges();

                    // Record the zipcode list once we have updated the office and there is a value for it.
                    frm.SaveZipcodes(Convert.ToInt32(drv["office"]));
                }
                else
                {
                    drv.CancelEdit();
                }
            }
        }

        /// <summary>
        /// Create a new office row
        /// </summary>
        public void CreateRow()
        {
            DataTable tbl = Tables.OfficesTable();
            if (tbl != null)
            {
                DataView vue = tbl.DefaultView;
                EditRow(vue.AddNew());
            }
        }

        /// <summary>
        /// Delete the indicated row
        /// </summary>
        /// <param name="drv"></param>
        public void DeleteRow(DataRowView drv)
        {
            if (DebtPlus.Data.Forms.MessageBox.Show("Warning: Deleting offices once they have been used can have serious consequences to your system.\r\nIt is generally better to simply make the office INACTIVE rather than delete it.\r\n\r\nAre you really sure that you want to delete this item?", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
            {
                drv.Delete();
                CommitChanges();
            }
        }

        private Int32 RowHandle;
        private void GridView1_MouseDown(object sender, MouseEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = GridView1.CalcHitInfo((GridControl1.PointToClient(Control.MousePosition)));
            RowHandle = hi.IsValid && hi.InRow ? hi.RowHandle : -1;

            // Find the row view in the dataview object for this row.
            if (RowHandle >= 0)
            {
                GridView1.FocusedRowHandle = RowHandle;
                DataRowView drv = (DataRowView)GridView1.GetRow(RowHandle);
                if (drv != null)
                {
                    barButtonItem_Delete.Enabled = true;
                    barButtonItem_Edit.Enabled = true;
                }
                else
                {
                    barButtonItem_Delete.Enabled = false;
                    barButtonItem_Edit.Enabled = false;
                }
                barButtonItem_Create.Enabled = true;
            }

            popupMenu1.ShowPopup(GridControl1.PointToScreen(e.Location));
        }

        /// <summary>
        /// A DOUBLE-CLICK on a row is always an EDIT operation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridView1_DoubleClick(object sender, EventArgs e) // Handles GridView1.DoubleClick
        {
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = GridView1.CalcHitInfo((GridControl1.PointToClient(Control.MousePosition)));
            RowHandle = hi.IsValid && hi.InRow ? hi.RowHandle : -1;

            // Find the row view in the dataview object for this row.
            if (RowHandle >= 0)
            {
                GridView1.FocusedRowHandle = RowHandle;
                DataRowView drv = (DataRowView)GridView1.GetRow(RowHandle);

                // If the row is defined then edit the row.
                if (drv != null)
                {
                    EditRow(drv);
                }
            }
        }

        /// <summary>
        /// Handle the CLICK event on the CREATE popup menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ContextMenu1_Create_Click(object sender, EventArgs e) // Handles ContextMenu1_Create.Click
        {
            CreateRow();
        }

        /// <summary>
        /// Handle the CLICK event on the DELETE popup menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ContextMenu1_Delete_Click(object sender, EventArgs e) // Handles ContextMenu1_Delete.Click
        {
            if (RowHandle >= 0)
            {
                DataRowView drv = (DataRowView)GridView1.GetRow(RowHandle);
                if (drv != null)
                {
                    DeleteRow(drv);
                }
            }
        }

        /// <summary>
        /// Handle the CLICK event on the EDIT popup menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ContextMenu1_Edit_Click(object sender, EventArgs e) // Handles ContextMenu1_Edit.Click
        {
            // If there is a row then edit it.
            if (RowHandle >= 0)
            {
                DataRowView drv = (DataRowView)GridView1.GetRow(RowHandle);
                if (drv != null)
                {
                    EditRow(drv);
                }
            }
        }

        /// <summary>
        /// Process the CLICK event on the NEW button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SimpleButton_New_Click(object sender, EventArgs e)
        {
            CreateRow();
        }

        /// <summary>
        /// Process the CLICK event on the EDIT button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SimpleButton_Edit_Click(object sender, EventArgs e)
        {
            RowHandle = GridView1.FocusedRowHandle;

            // If there is a row then edit it.
            if (RowHandle >= 0)
            {
                DataRowView drv = (DataRowView)GridView1.GetRow(RowHandle);
                if (drv != null)
                {
                    EditRow(drv);
                }
            }
        }

        /// <summary>
        /// Process the CLICK event on the OK button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SimpleButton_OK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            if (!this.Modal)
            {
                Close();
            }
        }
    }
}
