namespace DebtPlus.UI.FormLib.Disbursement
{
    partial class NewDisbursementRegisterControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Button_New = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.ds)).BeginInit();
            this.SuspendLayout();
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.Location = new System.Drawing.Point(424, 72);
            // 
            // Button_Select
            // 
            this.Button_Select.Location = new System.Drawing.Point(424, 9);
            // 
            // Button_New
            // 
            this.Button_New.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_New.Location = new System.Drawing.Point(424, 40);
            this.Button_New.Name = "Button_New";
            this.Button_New.Size = new System.Drawing.Size(75, 25);
            this.Button_New.TabIndex = 8;
            this.Button_New.Text = "&New";
            // 
            // NewDisbursementRegisterControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Button_New);
            this.Name = "NewDisbursementRegisterControl";
            this.Controls.SetChildIndex(this.Button_Select, 0);
            this.Controls.SetChildIndex(this.Button_Cancel, 0);
            this.Controls.SetChildIndex(this.Button_New, 0);
            ((System.ComponentModel.ISupportInitialize)(this.ds)).EndInit();
            this.ResumeLayout(false);
        }
        #endregion

        protected DevExpress.XtraEditors.SimpleButton Button_New;
    }
}
