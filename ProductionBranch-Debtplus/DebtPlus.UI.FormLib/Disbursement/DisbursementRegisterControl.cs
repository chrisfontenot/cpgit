using System;
using System.Data;
using System.Data.SqlClient;
using DebtPlus.Utils;

namespace DebtPlus.UI.FormLib.Disbursement
{
    public partial class DisbursementRegisterControl : DevExpress.XtraEditors.XtraUserControl
    {

        /// <summary>
        /// Dataset to hold the working tables
        /// </summary>
        protected readonly DataSet ds = new DataSet("ds");

        /// <summary>
        /// Event when the CANCEL button is pressed
        /// </summary>
        public event EventHandler Cancelled;

        /// <summrary>
        /// Routine to be called when the cancel status is raised
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnCancelled(EventArgs e)
        {
            if (Cancelled != null)
            {
                Cancelled(this, e);
            }
        }

        /// <summary>
        /// Raise the cancel event
        /// </summary>
        protected void RaiseCancelled()
        {
            OnCancelled(new EventArgs());
        }

        /// <summary>
        /// Event when the batch was selected
        /// </summary>
        public event EventHandler Selected;

        /// <summary>
        /// Routine called when the SELECTED event is to be raised
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnSelected(EventArgs e)
        {
            if (Selected != null)
            {
                Selected(this, e);
            }
        }

        /// <summary>
        /// Raise the SELECTED condtion
        /// </summary>
        protected void RaiseSelected()
        {
            OnSelected(new EventArgs());
        }

        /// <summary>
        /// Create a new instance of our class
        /// </summary>
        public DisbursementRegisterControl() : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        /// <summary>
        /// Assign the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Button_Select.Click += Button_Select_Click;
            Button_Cancel.Click += Button_Cancel_Click;
            GridView1.DoubleClick += GridView1_DoubleClick;
            GridView1.FocusedRowChanged += GridView1_FocusedRowChanged;
            GridView1.Click += GridView1_Click;
        }

        /// <summary>
        /// Remove the previous event handlers
        /// </summary>
        private void UnRegisterHandlers()
        {
            Button_Select.Click -= Button_Select_Click;
            Button_Cancel.Click -= Button_Cancel_Click;
            GridView1.DoubleClick -= GridView1_DoubleClick;
            GridView1.FocusedRowChanged -= GridView1_FocusedRowChanged;
            GridView1.Click -= GridView1_Click;
        }

        /// <summary>
        /// The current batch that was selected or created
        /// </summary>
        public Int32 BatchID { get; protected set; }

        /// <summary>
        /// Process the CLICK event on the SELECT button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Select_Click(object sender, EventArgs e) // Handles Button_Select.Click
        {
            if (BatchID > 0)
            {
                RaiseSelected();
            }
        }

        /// <summary>
        /// Process the CLICK event on the CANCEL button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Cancel_Click(object sender, EventArgs e) // Handles Button_Cancel.Click
        {
            RaiseCancelled();
        }

        /// <summary>
        /// Read the form information
        /// </summary>
        public void ReadForm()
        {
            UnRegisterHandlers();
            try
            {
                Load_Disbursement_Batches();
                ChangeRowHandle(GridView1.FocusedRowHandle);
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Return the command to retrieve the data from the database. This is overridable.
        /// </summary>
        /// <returns></returns>
        protected virtual SqlCommand SelectCommand()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            cmd.CommandText = "SELECT disbursement_register as 'BatchID', note as 'Note', created_by, date_created FROM registers_disbursement WITH (NOLOCK) WHERE date_posted IS NULL AND tran_type='AD' ORDER BY date_created DESC";
            cmd.CommandType = CommandType.Text;

            return cmd;
        }

        /// <summary>
        /// Load the list of disbursement batches
        /// </summary>
        private void Load_Disbursement_Batches()
        {
            const string TableName = "registers_disbursement";

            // Esure that there are no pending items in the table. We want only one copy of each row.
            DataTable tbl = ds.Tables[TableName];
            if (tbl != null)
            {
                tbl.Clear();
            }

            using (SqlCommand cmd = SelectCommand())
            {
                using (var da = new SqlDataAdapter(cmd))
                {
                    da.Fill(ds, TableName);
                    tbl = ds.Tables[TableName];

                    // Set the primary key to the table
                    tbl.PrimaryKey = new DataColumn[] { tbl.Columns["disbursement_register"] };
                }
            }

            GridControl1.DataSource = tbl.DefaultView;
            GridControl1.RefreshDataSource();
        }

        /// <summary>
        /// Handle the change in the focus row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            ChangeRowHandle(e.FocusedRowHandle);
        }

        /// <summary>
        /// Set the batch ID from the change in the focused row
        /// </summary>
        /// <param name="RowHandle"></param>
        private void ChangeRowHandle(Int32 RowHandle)
        {
            try
            {
                DataRow row = GridView1.GetDataRow(RowHandle);
                if (row != null)
                {
                    BatchID = Convert.ToInt32(row["BatchID"]);
                }
            }
#pragma warning disable 168
            catch (Exception ex) { }
#pragma warning restore 168

            // Enable the OK button if the creditor is specified
            Button_Select.Enabled = BatchID > 0;
        }

        /// <summary>
        /// Process a DOUBLE-CLICK event on the grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridView1_DoubleClick(object sender, EventArgs e)
        {
            // Find the row targetted as the double-click item
            DevExpress.XtraGrid.Views.Grid.GridView gv = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gv.CalcHitInfo((gv.GridControl.PointToClient(System.Windows.Forms.Control.MousePosition)));

            GridView1.FocusedRowHandle = hi.RowHandle;
            try
            {
                DataRow row = GridView1.GetDataRow(hi.RowHandle);
                BatchID = Convert.ToInt32(row["BatchID"]);
            }
#pragma warning disable 168
            catch (Exception ex) { }
#pragma warning restore 168

            Button_Select.Enabled = (BatchID > 0);
            if (BatchID > 0)
            {
                RaiseSelected();
            }
        }

        /// <summary>
        /// Process a single CLICK event on the GRID
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridView1_Click(object sender, EventArgs e)
        {
            // Find the row targetted as the double-click item
            DevExpress.XtraGrid.Views.Grid.GridView gv = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gv.CalcHitInfo((gv.GridControl.PointToClient(System.Windows.Forms.Control.MousePosition)));
            GridView1.FocusedRowHandle = hi.RowHandle;
        }
    }
}
