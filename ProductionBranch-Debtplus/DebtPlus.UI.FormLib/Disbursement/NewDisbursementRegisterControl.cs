using System;
using System.Windows.Forms;

namespace DebtPlus.UI.FormLib.Disbursement
{
    public partial class NewDisbursementRegisterControl : DisbursementRegisterControl
    {
        /// <summary>
        /// Create an instance of our class
        /// </summary>
        public NewDisbursementRegisterControl() : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Button_New.Click += Button_New_Click;
        }

        /// <summary>
        /// Remove the event handlers
        /// </summary>
        private void UnRegisterHandlers()
        {
            Button_New.Click -= Button_New_Click;
        }

        /// <summary>
        /// Handle the CLICK event on the NEW button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_New_Click(object sender, EventArgs e) // Handles Button_New.Click
        {
            using (var frm = new BatchCreateForm())
            {
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    BatchID = frm.BatchID;
                    RaiseSelected();
                }
            }
        }
    }
}
