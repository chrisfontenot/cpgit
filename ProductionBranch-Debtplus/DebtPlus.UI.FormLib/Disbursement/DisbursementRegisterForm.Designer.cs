namespace DebtPlus.UI.FormLib.Disbursement
{
    partial class DisbursementRegisterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.DisbursementRegisterControl1 = new DisbursementRegisterControl();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // LabelControl1
            // 
            this.LabelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelControl1.Location = new System.Drawing.Point(8, 8);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(325, 13);
            this.LabelControl1.TabIndex = 2;
            this.LabelControl1.Text = "Please select the batch that you wish to post from the following list.";
            // 
            // DisbursementRegisterControl1
            // 
            this.DisbursementRegisterControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DisbursementRegisterControl1.Location = new System.Drawing.Point(8, 32);
            this.DisbursementRegisterControl1.Name = "DisbursementRegisterControl1";
            this.DisbursementRegisterControl1.Size = new System.Drawing.Size(488, 216);
            this.DisbursementRegisterControl1.TabIndex = 3;
            // 
            // DisbursementRegisterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(504, 254);
            this.Controls.Add(this.DisbursementRegisterControl1);
            this.Controls.Add(this.LabelControl1);
            this.Name = "DisbursementRegisterForm";
            this.Text = "Disbursement Batch Selection";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal DevExpress.XtraEditors.LabelControl LabelControl1;
        private DisbursementRegisterControl DisbursementRegisterControl1;
    }
}