using System;
using System.Windows.Forms;
using DebtPlus.Data.Forms;

namespace DebtPlus.UI.FormLib.Disbursement
{
    public partial class DisbursementRegisterForm : DebtPlusForm
    {
        public DisbursementRegisterForm()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        /// <summary>
        ///     Resulting Batch from the selection criteria
        /// </summary>
        public Int32 BatchID { get; protected set; }

        private void RegisterHandlers()
        {
            Load += DisbursementRegisterForm_Load;
            DisbursementRegisterControl1.Cancelled += disbursementRegisterControl1_Cancelled;
            DisbursementRegisterControl1.Selected += disbursementRegisterControl1_Selected;
        }

        private void UnRegisterHandlers()
        {
            Load -= DisbursementRegisterForm_Load;
            DisbursementRegisterControl1.Cancelled -= disbursementRegisterControl1_Cancelled;
            DisbursementRegisterControl1.Selected -= disbursementRegisterControl1_Selected;
        }

        protected virtual void disbursementRegisterControl1_Cancelled(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        protected virtual void disbursementRegisterControl1_Selected(object sender, EventArgs e)
        {
            BatchID = DisbursementRegisterControl1.BatchID;
            DialogResult = DialogResult.OK;
        }

        protected virtual void DisbursementRegisterForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                DisbursementRegisterControl1.ReadForm();
            }
            finally
            {
                RegisterHandlers();
            }
        }
    }
}