namespace DebtPlus.UI.FormLib.Disbursement
{
    partial class DisbursementRegisterControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
                ds.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Select = new DevExpress.XtraEditors.SimpleButton();
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.GridColumn_Batch = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Note = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_DateCreated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_CreatedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Cancel.Location = new System.Drawing.Point(427, 41);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 25);
            this.Button_Cancel.TabIndex = 10;
            this.Button_Cancel.Text = "&Cancel";
            // 
            // Button_Select
            // 
            this.Button_Select.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Select.Location = new System.Drawing.Point(427, 9);
            this.Button_Select.Name = "Button_Select";
            this.Button_Select.Size = new System.Drawing.Size(75, 25);
            this.Button_Select.TabIndex = 9;
            this.Button_Select.Text = "&Select";
            // 
            // GridControl1
            // 
            this.GridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GridControl1.Location = new System.Drawing.Point(3, 1);
            this.GridControl1.MainView = this.GridView1;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.Size = new System.Drawing.Size(416, 215);
            this.GridControl1.TabIndex = 8;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GridView1});
            // 
            // GridView1
            // 
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.GridColumn_Batch,
            this.GridColumn_Note,
            this.GridColumn_DateCreated,
            this.GridColumn_CreatedBy});
            this.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.GridView1.GridControl = this.GridControl1;
            this.GridView1.Name = "GridView1";
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedRow;
            // 
            // GridColumn_Batch
            // 
            this.GridColumn_Batch.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_Batch.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_Batch.Caption = "Batch";
            this.GridColumn_Batch.DisplayFormat.FormatString = "f0";
            this.GridColumn_Batch.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_Batch.FieldName = "BatchID";
            this.GridColumn_Batch.Name = "GridColumn_Batch";
            this.GridColumn_Batch.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_Batch.Visible = true;
            this.GridColumn_Batch.VisibleIndex = 0;
            this.GridColumn_Batch.Width = 86;
            // 
            // GridColumn_Note
            // 
            this.GridColumn_Note.Caption = "Description";
            this.GridColumn_Note.FieldName = "Note";
            this.GridColumn_Note.Name = "GridColumn_Note";
            this.GridColumn_Note.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_Note.Visible = true;
            this.GridColumn_Note.VisibleIndex = 1;
            this.GridColumn_Note.Width = 352;
            // 
            // GridColumn_DateCreated
            // 
            this.GridColumn_DateCreated.Caption = "Date";
            this.GridColumn_DateCreated.DisplayFormat.FormatString = "d";
            this.GridColumn_DateCreated.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_DateCreated.FieldName = "date_created";
            this.GridColumn_DateCreated.Name = "GridColumn_DateCreated";
            this.GridColumn_DateCreated.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_DateCreated.Visible = true;
            this.GridColumn_DateCreated.VisibleIndex = 2;
            this.GridColumn_DateCreated.Width = 114;
            // 
            // GridColumn_CreatedBy
            // 
            this.GridColumn_CreatedBy.Caption = "Creator";
            this.GridColumn_CreatedBy.FieldName = "created_by";
            this.GridColumn_CreatedBy.Name = "GridColumn_CreatedBy";
            this.GridColumn_CreatedBy.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_CreatedBy.Visible = true;
            this.GridColumn_CreatedBy.VisibleIndex = 3;
            this.GridColumn_CreatedBy.Width = 226;
            // 
            // DisbursementRegisterControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_Select);
            this.Controls.Add(this.GridControl1);
            this.Name = "DisbursementRegisterControl";
            this.Size = new System.Drawing.Size(504, 216);
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).EndInit();
            this.ResumeLayout(false);
        }
        #endregion

        public DevExpress.XtraEditors.SimpleButton Button_Cancel;
        public DevExpress.XtraEditors.SimpleButton Button_Select;
        public DevExpress.XtraGrid.GridControl GridControl1;
        public DevExpress.XtraGrid.Views.Grid.GridView GridView1;
        public DevExpress.XtraGrid.Columns.GridColumn GridColumn_Batch;
        public DevExpress.XtraGrid.Columns.GridColumn GridColumn_Note;
        public DevExpress.XtraGrid.Columns.GridColumn GridColumn_DateCreated;
        public DevExpress.XtraGrid.Columns.GridColumn GridColumn_CreatedBy;
    }
}
