using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DebtPlus.Utils;

namespace DebtPlus.UI.FormLib.Disbursement
{
    public partial class BatchCreateForm : DebtPlus.Data.Forms.DebtPlusForm
    {


        public BatchCreateForm()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += BatchCreateForm_Load;
            Button_Create.Click += Button_Create_Click;
            Button_cycle.Click += Button_cycle_Click;
            Button_Region.Click += Button_cycle_Click;
            Text_Label.KeyPress += Text_Label_KeyPress;
            Text_Label.Enter += Text_Label_Enter;
        }

        private void UnRegisterHandlers()
        {
            Load -= BatchCreateForm_Load;
            Button_Create.Click -= Button_Create_Click;
            Button_cycle.Click -= Button_cycle_Click;
            Button_Region.Click -= Button_cycle_Click;
            Text_Label.KeyPress -= Text_Label_KeyPress;
            Text_Label.Enter -= Text_Label_Enter;
        }

        /// <summary>
        /// The new batch ID
        /// </summary>
        public Int32 BatchID { get; protected set; }

        /// <summary>
        /// Return the string for the disbursement cycles.
        /// </summary>
        /// <returns>The string or an empty string to mean "all cycles"</returns>
        private string CycleString()
        {
            var sb = new System.Text.StringBuilder();
            var selectedAll = true;

            // Find the list of checked cycle dates. If all are selected then treat it as "none"
            // and this will process all of the dates
            for( var index = 0; index < CheckedListBox_Cycle.ItemCount; ++index )
            {
                if ( CheckedListBox_Cycle.Items[index].CheckState == CheckState.Checked )
                {
                    sb.AppendFormat(",{0}", CheckedListBox_Cycle.Items[index].Value);
                }
                else
                {
                    selectedAll = false;  // Since we found one that was not selected then not all are selected
                }
            }

            // Return the empty string if all are selected.
            if (selectedAll) return string.Empty;

            if (sb.Length > 0) sb.Remove(0, 1);

            return sb.ToString();
        }

        /// <summary>
        /// Return a string of the regions selected
        /// </summary>
        /// <returns>A string or an empty string meaning "all regions"</returns>
        private string RegionString()
        {
            var sb = new System.Text.StringBuilder();
            var selectedAll = true;

            // Find the list of checked cycle dates. If all are selected then treat it as "none"
            // and this will process all of the dates
            for( var index = 0; index < CheckedListBox_Region.ItemCount; ++index )
            {
                if ( CheckedListBox_Region.Items[index].CheckState == CheckState.Checked )
                {
                    sb.AppendFormat(",{0}", CheckedListBox_Region.Items[index].Value);
                }
                else
                {
                    selectedAll = false;  // Since we found one that was not selected then not all are selected
                }
            }

            // Return the empty string if all are selected.
            if (selectedAll) return string.Empty;

            if (sb.Length > 0) sb.Remove(0, 1);

            return sb.ToString();
        }

        private void Button_cycle_Click(object sender, EventArgs e) // Handles Button_cycle.Click, Button_Region.Click
        {
            var buttonSender = (DevExpress.XtraEditors.SimpleButton) sender;

            // Translate the button to the associated control
            var ctl = ( sender == Button_cycle ) ? CheckedListBox_Cycle : CheckedListBox_Region;

            // Process the check status for the button
            var newState = DebtPlus.Utils.Nulls.DInt(buttonSender.Tag) == 0 ? CheckState.Unchecked : CheckState.Checked;

            // Select all of the items in the list
            for( var itemCount = 0; itemCount < ctl.ItemCount; ++itemCount)
            {
                ctl.Items[itemCount].CheckState = newState;
            }

            // Change the legend to match the tag
            if( newState == CheckState.Checked )
            {
                buttonSender.Tag = 0;
                buttonSender.Text = "&Clear All";
            }
            else
            {
                buttonSender.Tag = 1;
                buttonSender.Text = "&Set All";
            }
        }

        private void load_lst_regions()
        {
            // Ensure that the list is cleared
            CheckedListBox_Region.Items.Clear();

            var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            SqlDataReader rd;
            try
            {
                cn.Open();
                using( var cmd = new SqlCommand() )
                {
                    cmd.Connection = cn;
                    cmd.CommandText = "SELECT isnull(region,0) as region, isnull(description,'') as description FROM regions WITH (NOLOCK) ORDER BY description";
                    rd = cmd.ExecuteReader(CommandBehavior.CloseConnection | CommandBehavior.SingleResult);
                }

                while( rd.Read() )
                {
                    CheckedListBox_Region.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(Convert.ToString(rd[1]), Convert.ToInt32(rd[0])), CheckState.Unchecked, true);
                }
            }

            catch ( SqlException ex )
            {
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error Loading the region list", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            finally
            {
                cn.Dispose();
            }
        }

        private void BatchCreateForm_Load(object sender, EventArgs e) // Handles Me.Load
        {
            UnRegisterHandlers();
            try
            {
                load_lst_regions();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void Text_Label_KeyPress(object sender, KeyPressEventArgs e) // Handles Text_Label.KeyPress
        {
            if( string.IsNullOrEmpty( Text_Label.Text ) && System.Char.IsWhiteSpace( e.KeyChar ))
            {
                e.Handled = true;
            }
        }

        private void Text_Label_Enter(object sender, EventArgs e) // Handles Text_Label.Enter
        {
            Text_Label.SelectAll();
        }

        private void Button_Create_Click(object sender, EventArgs e) // Handles Button_Create.Click
        {
            var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            try
            {
                cn.Open();
                using( var cmd = new SqlCommand())
                {
                    cmd.CommandText = "xpr_disbursement_create";
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlCommandBuilder.DeriveParameters(cmd);

                    cmd.Parameters["@disbursement_date"].Value = CycleString();
                    cmd.Parameters["@region"].Value = RegionString();
                    cmd.Parameters["note"].Value = Convert.ToString(Text_Label.EditValue).Trim();

                    // This can take a while on some large systems so make the timeout longer.
                    cmd.CommandTimeout = System.Math.Max(2400, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                    
                    cmd.ExecuteNonQuery();
                    BatchID = Convert.ToInt32(cmd.Parameters[1].Value);
                }

                DebtPlus.Data.Forms.MessageBox.Show(string.Format("The disbursement batch {0:n0} was created successfully.", BatchID), "Operation Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
            }

            catch( SqlException ex )
            {
                using (var gdr = new Repository.GetDataResult())
                {
                    gdr.HandleException(ex);
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error Loading the region list");
                }
            }

            finally
            {
                cn.Dispose();
            }
        }
    }
}