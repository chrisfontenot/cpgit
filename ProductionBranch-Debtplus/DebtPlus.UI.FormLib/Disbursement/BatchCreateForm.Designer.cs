namespace DebtPlus.UI.FormLib.Disbursement
{
    partial class BatchCreateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Text_Label = new DevExpress.XtraEditors.TextEdit();
            this.Label1 = new DevExpress.XtraEditors.LabelControl();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Create = new DevExpress.XtraEditors.SimpleButton();
            this.GroupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.CheckedListBox_Region = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.Button_Region = new DevExpress.XtraEditors.SimpleButton();
            this.GroupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.CheckedListBox_Cycle = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.Button_cycle = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text_Label.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl2)).BeginInit();
            this.GroupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckedListBox_Region)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl1)).BeginInit();
            this.GroupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckedListBox_Cycle)).BeginInit();
            this.SuspendLayout();
            // 
            // Text_Label
            // 
            this.Text_Label.Location = new System.Drawing.Point(84, 12);
            this.Text_Label.Name = "Text_Label";
            this.Text_Label.Properties.NullText = "... The optional label for the batch goes here ...";
            this.Text_Label.Size = new System.Drawing.Size(272, 20);
            this.Text_Label.TabIndex = 7;
            this.Text_Label.ToolTipController = this.ToolTipController1;
            // 
            // Label1
            // 
            this.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.Label1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Label1.Location = new System.Drawing.Point(20, 12);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(29, 13);
            this.Label1.TabIndex = 6;
            this.Label1.Text = "Label:";
            this.Label1.ToolTipController = this.ToolTipController1;
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(244, 252);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 11;
            this.Button_Cancel.Text = "&Cancel";
            this.Button_Cancel.ToolTip = "Cancel this form and return to the previous condition";
            this.Button_Cancel.ToolTipController = this.ToolTipController1;
            this.Button_Cancel.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Hand;
            // 
            // Button_Create
            // 
            this.Button_Create.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Button_Create.Location = new System.Drawing.Point(60, 252);
            this.Button_Create.Name = "Button_Create";
            this.Button_Create.Size = new System.Drawing.Size(75, 23);
            this.Button_Create.TabIndex = 10;
            this.Button_Create.Text = "C&reate";
            this.Button_Create.ToolTip = "Click here to create the batch";
            this.Button_Create.ToolTipController = this.ToolTipController1;
            this.Button_Create.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Hand;
            // 
            // GroupControl2
            // 
            this.GroupControl2.Controls.Add(this.CheckedListBox_Region);
            this.GroupControl2.Controls.Add(this.Button_Region);
            this.GroupControl2.Location = new System.Drawing.Point(196, 44);
            this.GroupControl2.Name = "GroupControl2";
            this.GroupControl2.Size = new System.Drawing.Size(160, 200);
            this.GroupControl2.TabIndex = 9;
            this.GroupControl2.Text = " Region(s) ";
            // 
            // CheckedListBox_Region
            // 
            this.CheckedListBox_Region.ItemHeight = 16;
            this.CheckedListBox_Region.Location = new System.Drawing.Point(8, 25);
            this.CheckedListBox_Region.Name = "CheckedListBox_Region";
            this.CheckedListBox_Region.Size = new System.Drawing.Size(144, 135);
            this.CheckedListBox_Region.TabIndex = 0;
            this.CheckedListBox_Region.ToolTip = "Choose the region or regions that you wish to disburse in this batch";
            this.CheckedListBox_Region.ToolTipController = this.ToolTipController1;
            // 
            // Button_Region
            // 
            this.Button_Region.Location = new System.Drawing.Point(48, 168);
            this.Button_Region.Name = "Button_Region";
            this.Button_Region.Size = new System.Drawing.Size(75, 23);
            this.Button_Region.TabIndex = 1;
            this.Button_Region.Tag = "1";
            this.Button_Region.Text = "Select &All";
            this.Button_Region.ToolTip = "Click here to select all of the items in this list";
            this.Button_Region.ToolTipController = this.ToolTipController1;
            this.Button_Region.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Hand;
            // 
            // GroupControl1
            // 
            this.GroupControl1.Controls.Add(this.CheckedListBox_Cycle);
            this.GroupControl1.Controls.Add(this.Button_cycle);
            this.GroupControl1.Location = new System.Drawing.Point(12, 44);
            this.GroupControl1.Name = "GroupControl1";
            this.GroupControl1.Size = new System.Drawing.Size(160, 200);
            this.GroupControl1.TabIndex = 8;
            this.GroupControl1.Text = " Disbursement Cycle(s) ";
            // 
            // CheckedListBox_Cycle
            // 
            this.CheckedListBox_Cycle.ItemHeight = 16;
            this.CheckedListBox_Cycle.Items.AddRange(new DevExpress.XtraEditors.Controls.CheckedListBoxItem[] {
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("1"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("2"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("3"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("4"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("5"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("6"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("7"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("8"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("9"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("10"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("11"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("12"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("13"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("14"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("15"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("16"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("17"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("18"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("19"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("20"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("21"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("22"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("23"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("24"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("25"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("26"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("27"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("28"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("29"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("30"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem("31")});
            this.CheckedListBox_Cycle.Location = new System.Drawing.Point(8, 25);
            this.CheckedListBox_Cycle.Name = "CheckedListBox_Cycle";
            this.CheckedListBox_Cycle.Size = new System.Drawing.Size(144, 135);
            this.CheckedListBox_Cycle.TabIndex = 0;
            this.CheckedListBox_Cycle.ToolTip = "Choose the client\'s disbursement cycle for those clients that you wish to disburs" +
    "e in this batch";
            this.CheckedListBox_Cycle.ToolTipController = this.ToolTipController1;
            // 
            // Button_cycle
            // 
            this.Button_cycle.Location = new System.Drawing.Point(48, 168);
            this.Button_cycle.Name = "Button_cycle";
            this.Button_cycle.Size = new System.Drawing.Size(75, 23);
            this.Button_cycle.TabIndex = 1;
            this.Button_cycle.Tag = "1";
            this.Button_cycle.Text = "Select &All";
            this.Button_cycle.ToolTip = "Click here to select all of the items in this list";
            this.Button_cycle.ToolTipController = this.ToolTipController1;
            this.Button_cycle.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Hand;
            // 
            // BatchCreateForm
            // 
            this.AcceptButton = this.Button_Create;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(368, 286);
            this.Controls.Add(this.Text_Label);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_Create);
            this.Controls.Add(this.GroupControl2);
            this.Controls.Add(this.GroupControl1);
            this.Name = "BatchCreateForm";
            this.Text = "Create Auto Disbursement Batch";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Text_Label.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl2)).EndInit();
            this.GroupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CheckedListBox_Region)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl1)).EndInit();
            this.GroupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CheckedListBox_Cycle)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        internal DevExpress.XtraEditors.TextEdit Text_Label;
        internal DevExpress.XtraEditors.LabelControl Label1;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
        internal DevExpress.XtraEditors.SimpleButton Button_Create;
        internal DevExpress.XtraEditors.GroupControl GroupControl2;
        internal DevExpress.XtraEditors.CheckedListBoxControl CheckedListBox_Region;
        internal DevExpress.XtraEditors.SimpleButton Button_Region;
        internal DevExpress.XtraEditors.GroupControl GroupControl1;
        internal DevExpress.XtraEditors.CheckedListBoxControl CheckedListBox_Cycle;
        internal DevExpress.XtraEditors.SimpleButton Button_cycle;
    }
}