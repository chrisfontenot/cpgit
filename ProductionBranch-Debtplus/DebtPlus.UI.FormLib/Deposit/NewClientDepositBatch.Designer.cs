namespace DebtPlus.UI.FormLib.Deposit
{
    partial class NewClientDepositBatch
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

#region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonNew = new DevExpress.XtraEditors.SimpleButton();
            this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RepositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // LayoutControl1
            // 
            this.LayoutControl1.Controls.Add(this.ButtonNew);
            this.LayoutControl1.Controls.SetChildIndex(this.ButtonNew, 0);
            this.LayoutControl1.Controls.SetChildIndex(this.ButtonSelect, 0);
            this.LayoutControl1.Controls.SetChildIndex(this.ButtonCancel, 0);
            // 
            // ButtonCancel
            // 
            this.ButtonCancel.Location = new System.Drawing.Point(467, 64);
            // 
            // ButtonSelect
            // 
            this.ButtonSelect.Location = new System.Drawing.Point(467, 37);
            // 
            // LayoutControlGroup1
            // 
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem1});
            // 
            // LayoutControlItem2
            // 
            this.LayoutControlItem2.Text = "Please select the deposit batch to use or click new to create a new batch";
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(349, 13);
            // 
            // LayoutControlItem3
            // 
            this.LayoutControlItem3.Location = new System.Drawing.Point(455, 25);
            this.LayoutControlItem3.Text = "Select";
            // 
            // LayoutControlItem4
            // 
            this.LayoutControlItem4.Location = new System.Drawing.Point(455, 52);
            this.LayoutControlItem4.Text = "Cancel";
            // 
            // EmptySpaceItem1
            // 
            this.EmptySpaceItem1.Location = new System.Drawing.Point(455, 106);
            this.EmptySpaceItem1.Size = new System.Drawing.Size(79, 216);
            // 
            // EmptySpaceItem2
            // 
            this.EmptySpaceItem2.Size = new System.Drawing.Size(79, 25);
            // 
            // ButtonNew
            // 
            this.ButtonNew.Location = new System.Drawing.Point(467, 91);
            this.ButtonNew.MaximumSize = new System.Drawing.Size(75, 23);
            this.ButtonNew.MinimumSize = new System.Drawing.Size(75, 23);
            this.ButtonNew.Name = "ButtonNew";
            this.ButtonNew.Size = new System.Drawing.Size(75, 23);
            this.ButtonNew.StyleController = this.LayoutControl1;
            this.ButtonNew.TabIndex = 8;
            this.ButtonNew.Text = "&New...";
            // 
            // LayoutControlItem1
            // 
            this.LayoutControlItem1.Control = this.ButtonNew;
            this.LayoutControlItem1.CustomizationFormText = "new";
            this.LayoutControlItem1.Location = new System.Drawing.Point(455, 79);
            this.LayoutControlItem1.Name = "LayoutControlItem1";
            this.LayoutControlItem1.Size = new System.Drawing.Size(79, 27);
            this.LayoutControlItem1.Text = "new";
            this.LayoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem1.TextToControlDistance = 0;
            this.LayoutControlItem1.TextVisible = false;
            // 
            // NewClientDepositBatch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "NewClientDepositBatch";
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RepositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).EndInit();
            this.ResumeLayout(false);
        }
        public DevExpress.XtraEditors.SimpleButton ButtonNew;
        protected DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
#endregion
    }
}
