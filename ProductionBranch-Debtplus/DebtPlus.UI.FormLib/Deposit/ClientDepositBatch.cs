using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using DebtPlus.UI.Common;
using DebtPlus.Utils;

namespace DebtPlus.UI.FormLib.Deposit
{
    public partial class ClientDepositBatch : DevExpress.XtraEditors.XtraUserControl
    {
        protected DataSet ds = new DataSet("ds");

        /// <summary>
        /// The dialog was canceled.
        /// </summary>
        public event EventHandler Cancelled;
		
        /// <summary>
        /// Deposit batch selected
        /// </summary>
        public event EventHandler Selected;

        private Int32 _privateDepositBatchId;
        [Browsable(false)]
        public Int32 DepositBatchID
        {
            get { return _privateDepositBatchId; }
            protected set { _privateDepositBatchId = value; }
        }

        /// <summary>
        /// Type of batches to be displayed
        /// </summary>
        [Description("Deposit Batch Type. CL=Client, CR=Refund"), Category("DebtPlus"), Browsable(true), DefaultValue(typeof(String), "CL")]
        public string BatchType { get; set; }

        /// <summary>
        /// Batch status selection information
        /// </summary>
        public enum BatchStatusEnum
        {
            Open = 0,
            Closed,
            Posted,
            UnPosted
        };

        /// <summary>
        /// Status of the batches displayed in the list
        /// </summary>
        [Description("Status for batches listed in the group"), Category("DebtPlus"), Browsable(true), DefaultValue(typeof(BatchStatusEnum), "Open")]
        public BatchStatusEnum BatchStatus { get; set; }

        public ClientDepositBatch()
        {
            InitializeComponent();

            BatchType = "CL";
            BatchStatus = BatchStatusEnum.Open;

            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            ButtonCancel.Click += ButtonCancel_Click;
            ButtonSelect.Click += ButtonSelect_Click;
            GridView1.DoubleClick += GridView1_DoubleClick;
            GridView1.FocusedRowChanged += GridView1_FocusedRowChanged;
            GridView1.SelectionChanged += GridView1_SelectionChanged;
            GridView1.Layout += GridView1_Layout;
            GridView1.RowUpdated += GridView1_RowUpdated;
        }

        private void DeRegisterHandlers()
        {
            ButtonCancel.Click -= ButtonCancel_Click;
            ButtonSelect.Click -= ButtonSelect_Click;
            GridView1.DoubleClick -= GridView1_DoubleClick;
            GridView1.FocusedRowChanged -= GridView1_FocusedRowChanged;
            GridView1.SelectionChanged -= GridView1_SelectionChanged;
            GridView1.Layout -= GridView1_Layout;
            GridView1.RowUpdated -= GridView1_RowUpdated;
        }

        protected virtual void OnCancelled(EventArgs e)
        {
            if (Cancelled != null) Cancelled(this, e);
        }

        protected void RaiseCancelled()
        {
            OnCancelled(new EventArgs());
        }

        protected virtual void OnSelected(EventArgs e)
        {
            if (Selected != null) Selected(this, e);
        }

        protected void RaiseSelected()
        {
            OnSelected(new EventArgs());
        }

        /// <summary>
        /// Handle the update of the grid contorl row to reset the note
        /// </summary>
        private void GridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e) // Handles GridView1.RowUpdated
        {
            using (new CursorManager())
            {
                try
                {
                    using (var cmd = new SqlCommand())
                    {
                        cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        cmd.CommandText = "UPDATE deposit_batch_ids SET note=@note WHERE deposit_batch_id=@deposit_batch_id";
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.Add("@note", SqlDbType.VarChar, 50, "note");
                        cmd.Parameters.Add("@deposit_batch_id", SqlDbType.Int, 4, "deposit_batch_id");

                        var tbl = ((DataView) GridControl1.DataSource).Table;
                        using (var da = new SqlDataAdapter())
                        {
                            da.UpdateCommand = cmd;
                            da.Update(tbl);
                        }
                        tbl.AcceptChanges();
                    }
                }
                catch (SqlException ex)
                {
                    using (var gdr = new Repository.GetDataResult())
                    {
                        gdr.HandleException(ex);
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error rewriting notes for client deposit batch ids table");
                    }
                }
            }
        }

        public virtual void ReadForm()
        {
            DeRegisterHandlers();

            var commandText = "SELECT db.deposit_batch_id, db.bank, db.date_created, dbo.format_counselor_name(db.created_by) as created_by, db.note, b.description, isnull(SUM(dd.amount),0) AS amount FROM deposit_batch_ids db WITH (NOLOCK) LEFT OUTER JOIN banks b WITH (NOLOCK) ON db.bank = b.bank LEFT OUTER JOIN deposit_batch_details dd WITH (NOLOCK) ON db.deposit_batch_id = dd.deposit_batch_id AND 1 = dd.ok_to_post WHERE db.batch_type = @batch_type";
            switch (BatchStatus)
            {
                case BatchStatusEnum.Closed:
                    commandText += " AND db.date_closed IS NOT NULL AND db.date_posted IS NULL";
                    break;

                case BatchStatusEnum.Open:
                    commandText += " AND db.date_closed IS NULL AND db.date_posted IS NULL";
                    break;

                case BatchStatusEnum.Posted:
                    commandText += " AND db.date_posted IS NOT NULL";
                    break;

                case BatchStatusEnum.UnPosted:
                    commandText += " AND db.date_posted IS NULL AND (db.batch_type = 'CR' OR db.date_closed IS NOT NULL)";
                    break;
            }
            commandText += " GROUP BY db.deposit_batch_id, db.bank, db.date_created, db.created_by, db.note, b.description";
            
            using (new CursorManager())
            {
                try
                {
                    using (var cmd = new SqlCommand())
                    {
                        cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        cmd.CommandText = commandText;
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.Add("@batch_type", SqlDbType.VarChar, 10).Value = BatchType;

                        using (var da = new SqlDataAdapter(cmd))
                        {
                            da.Fill(ds, "deposit_batch_ids");
                        }
                    }
                }
                catch (SqlException ex)
                {
                    using (var gdr = new Repository.GetDataResult())
                    {
                        gdr.HandleException(ex);
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading client deposit batch ids table");
                    }
                }
            }

            var tbl = ds.Tables["deposit_batch_ids"];
            if (tbl.PrimaryKey.GetUpperBound(0) < 0)
            {
                tbl.PrimaryKey = new [] { tbl.Columns["deposit_batch_id"] };
            }

            // Bind the data to the control
            GridControl1.DataSource = tbl.DefaultView;
            GridControl1.RefreshDataSource();

            // Clear the selection and disable the select button
            GridView1.ClearSelection();
            GridView1.BestFitColumns();
            ReloadGridControlLayout();

            ButtonSelect.Enabled = false;
            if (GridView1.RowCount > 0)
            {
                GridView1.SelectRow(0);
                ButtonSelect.Enabled = true;
            }
            RegisterHandlers();
        }

        /// <summary>
        /// Retrieve the path to the saved configuration data. This is the path, not the file name.
        /// </summary>
        protected virtual string XMLBasePath()
        {
            return DesignMode ? string.Empty : System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "DebtPlus", "Deposit.Batch");
        }

        /// <summary>
        /// Reload the grid layout information from the datafile.
        /// </summary>
        protected void ReloadGridControlLayout()
        {
            // Find the base path to the saved file location
            var pathName = XMLBasePath();
            if (string.IsNullOrEmpty(pathName)) return;

            try
            {
                if (System.IO.File.Exists(pathName))
                    GridView1.RestoreLayoutFromXml(pathName);
            }
#pragma warning disable 168  // Ignore warning about "ex" not being used.
            catch( System.IO.DirectoryNotFoundException ex ) { }
            catch( System.IO.FileNotFoundException ex ) { }
            catch( System.Xml.XmlException ex ) { }
#pragma warning restore 168
        }

        private void GridView1_Layout(object sender, EventArgs e) // Handles GridView1.Layout
        {
            var pathName = XMLBasePath();
            if (string.IsNullOrEmpty(pathName)) return;

            if( ! System.IO.Directory.Exists ( pathName ))
            {
                System.IO.Directory.CreateDirectory( pathName );
            }
            var fileName = System.IO.Path.Combine(pathName, string.Format("{0}.Grid.xml", Name));
            GridView1.SaveLayoutToXml(fileName);
        }

        private void GridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e) 
        {
            ButtonSelect.Enabled = (GridView1.SelectedRowsCount > 0);
        }

        private void GridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e) 
        {
            var rowHandle = GridView1.FocusedRowHandle;

            GridView1.ClearSelection();
            GridView1.SelectRow(rowHandle);
        }

        protected Int32 GetRowBatchID(Int32 RowHandle)
        {
            var answer = -1;
            if( RowHandle >= 0 )
            {
                var row = GridView1.GetDataRow(RowHandle);
                if( row != null )
                {
                    if( row["deposit_batch_id"] != null && row["deposit_batch_id"] != DBNull.Value )
                    {
                        answer = Convert.ToInt32( row["deposit_batch_id"] );
                    }
                }
            }
            return answer;
        }

        private void GridView1_DoubleClick(object sender, EventArgs e) 
        {
            var hi = GridView1.CalcHitInfo((GridControl1.PointToClient(MousePosition)));
            if (!hi.IsValid || !hi.InRow) return;

            var rowHandle = hi.RowHandle;
            if (rowHandle < 0) return;

            GridView1.ClearSelection();
            GridView1.SelectRow(rowHandle);

            if (GridView1.SelectedRowsCount <= 0) return;

            DepositBatchID = GetRowBatchID(rowHandle);
            if (DepositBatchID > 0)
                RaiseSelected();
        }

        protected virtual void ButtonSelect_Click(object sender, EventArgs e) 
        {
            DepositBatchID = GetRowBatchID(GridView1.FocusedRowHandle);
            if (DepositBatchID > 0)
                RaiseSelected();
        }

        protected virtual void ButtonCancel_Click(object sender, EventArgs e) 
        {
            RaiseCancelled();
        }
    }
}
