using System;
using System.Data;
using System.Data.SqlClient;
using DebtPlus.UI.Common;
using DebtPlus.Utils;

namespace DebtPlus.UI.FormLib.Deposit
{
    public partial class NewClientDepositBatch_Form : DebtPlus.Data.Forms.DebtPlusForm
    {

        // Local storage
        private DataRowView drv;
        private DataSet ds = new DataSet("ds");

        /// <summary>
        /// Initialize the form class
        /// </summary>
        /// <param name="drv"></param>
        public NewClientDepositBatch_Form(DataRowView drv)
        {
            this.drv = drv;
            InitializeComponent();
            RegisterHandlers();
        }

        /// <summary>
        /// Initialize the form class
        /// </summary>
        public NewClientDepositBatch_Form()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load += NewClientDepositBatch_Form_Load;
            LookUpEdit_Bank.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            LookUpEdit_Bank.EditValueChanged += LookUpEdit_Bank_EditValueChanged;
        }

        /// <summary>
        /// Remove the event handler registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= NewClientDepositBatch_Form_Load;
            LookUpEdit_Bank.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            LookUpEdit_Bank.EditValueChanged -= LookUpEdit_Bank_EditValueChanged;
        }

        /// <summary>
        /// Load the form on the first processing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NewClientDepositBatch_Form_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Load the banks control
                BankListLookupEdit_Load();
                if( drv.IsNew )
                {
                    drv["bank"] = LookUpEdit_Bank.EditValue;
                }

                // Bind the controls to the data
                txt_label.DataBindings.Add("EditValue", drv, "note");
                LookUpEdit_Bank.DataBindings.Add("EditValue", drv, "bank");
                ButtonOK.Enabled = LookUpEdit_Bank.EditValue != null && LookUpEdit_Bank.EditValue != DBNull.Value && Convert.ToInt32(LookUpEdit_Bank.EditValue) > 0;
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Find the banks table
        /// </summary>
        /// <returns></returns>
        private DataTable GetBanksTable()
        {
            const string TableName = "banks";
            DataTable tbl = ds.Tables[TableName];
            if ( tbl == null )
            {
                using (new CursorManager())
                {
                    DebtPlus.Repository.GetDataResult rslt = DebtPlus.Repository.LookupTables.Banks.GetAll(ds, TableName);
                    if (rslt.Success)
                    {
                        tbl = ds.Tables[TableName];
                    }
                }
            }

            return tbl;
        }

        /// <summary>
        /// Load the list of banks into the lookup control.
        /// </summary>
        private void BankListLookupEdit_Load()
        {
            // Start with the banks table
            DataView vue = new DataView(GetBanksTable(), "[type] in ('D','C') AND [ActiveFlag]<>0", "description", DataViewRowState.CurrentRows);

            // Bind the office list to the data
            LookUpEdit_Bank.Properties.DataSource = vue;
            LookUpEdit_Bank.Properties.DisplayMember = "description";
            LookUpEdit_Bank.Properties.ValueMember = "bank";
            LookUpEdit_Bank.Properties.PopupWidth = LookUpEdit_Bank.Width + 140;

            // Set the default item if possible
            var rows = vue.Table.Select(vue.RowFilter + " AND [default]<>false", "bank");
            if (rows.GetUpperBound(0) >= 0)
            {
                LookUpEdit_Bank.EditValue = rows[0]["bank"];
            }
            else if (vue.Count > 0)
            {
                LookUpEdit_Bank.EditValue = vue[0]["bank"];
            }
        }

        /// <summary>
        /// Process a change in the banks table selection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LookUpEdit_Bank_EditValueChanged(object sender, System.EventArgs e)
        {
            ButtonOK.Enabled = LookUpEdit_Bank.EditValue != null && LookUpEdit_Bank.EditValue != System.DBNull.Value && Convert.ToInt32(LookUpEdit_Bank.EditValue) > 0;
        }
    }
}