namespace DebtPlus.UI.FormLib.Deposit
{
    partial class NewClientDepositBatch_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null)
                    {
                        components.Dispose();
                    }
                    ds.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.ButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            this.ButtonOK = new DevExpress.XtraEditors.SimpleButton();
            this.txt_label = new DevExpress.XtraEditors.TextEdit();
            this.LookUpEdit_Bank = new DevExpress.XtraEditors.LookUpEdit();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.EmptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.EmptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt_label.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_Bank.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // LayoutControl1
            // 
            this.LayoutControl1.Controls.Add(this.ButtonCancel);
            this.LayoutControl1.Controls.Add(this.ButtonOK);
            this.LayoutControl1.Controls.Add(this.txt_label);
            this.LayoutControl1.Controls.Add(this.LookUpEdit_Bank);
            this.LayoutControl1.Controls.Add(this.LabelControl1);
            this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(329, 190);
            this.LayoutControl1.TabIndex = 1;
            this.LayoutControl1.Text = "LayoutControl1";
            // 
            // ButtonCancel
            // 
            this.ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ButtonCancel.Location = new System.Drawing.Point(162, 116);
            this.ButtonCancel.MaximumSize = new System.Drawing.Size(75, 23);
            this.ButtonCancel.MinimumSize = new System.Drawing.Size(75, 23);
            this.ButtonCancel.Name = "ButtonCancel";
            this.ButtonCancel.Size = new System.Drawing.Size(75, 23);
            this.ButtonCancel.StyleController = this.LayoutControl1;
            this.ButtonCancel.TabIndex = 8;
            this.ButtonCancel.Text = "&Cancel";
            // 
            // ButtonOK
            // 
            this.ButtonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.ButtonOK.Location = new System.Drawing.Point(83, 116);
            this.ButtonOK.MaximumSize = new System.Drawing.Size(75, 23);
            this.ButtonOK.MinimumSize = new System.Drawing.Size(75, 23);
            this.ButtonOK.Name = "ButtonOK";
            this.ButtonOK.Size = new System.Drawing.Size(75, 23);
            this.ButtonOK.StyleController = this.LayoutControl1;
            this.ButtonOK.TabIndex = 7;
            this.ButtonOK.Text = "&OK";
            // 
            // txt_label
            // 
            this.txt_label.Location = new System.Drawing.Point(88, 53);
            this.txt_label.Name = "txt_label";
            this.txt_label.Properties.MaxLength = 50;
            this.txt_label.Size = new System.Drawing.Size(229, 20);
            this.txt_label.StyleController = this.LayoutControl1;
            this.txt_label.TabIndex = 6;
            this.txt_label.ToolTip = "If you wish to tag the deposit batch with an identiifer, enter the identifier her" +
    "e.";
            // 
            // LookUpEdit_Bank
            // 
            this.LookUpEdit_Bank.Location = new System.Drawing.Point(88, 29);
            this.LookUpEdit_Bank.Name = "LookUpEdit_Bank";
            this.LookUpEdit_Bank.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_Bank.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("bank", "ID", 5, DevExpress.Utils.FormatType.Numeric, "f0", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 30, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("formatted_check", "Type", 10, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.LookUpEdit_Bank.Properties.NullText = "... Please choose one ...";
            this.LookUpEdit_Bank.Size = new System.Drawing.Size(229, 20);
            this.LookUpEdit_Bank.StyleController = this.LayoutControl1;
            this.LookUpEdit_Bank.TabIndex = 5;
            this.LookUpEdit_Bank.ToolTip = "Choose the bank into which the deposit is to be made";
            // 
            // LabelControl1
            // 
            this.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LabelControl1.Location = new System.Drawing.Point(12, 12);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(294, 13);
            this.LabelControl1.StyleController = this.LayoutControl1;
            this.LabelControl1.TabIndex = 4;
            this.LabelControl1.Text = "Please enter the information needed to create the new batch";
            // 
            // LayoutControlGroup1
            // 
            this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem1,
            this.LayoutControlItem3,
            this.LayoutControlItem4,
            this.LayoutControlItem5,
            this.EmptySpaceItem1,
            this.LayoutControlItem2,
            this.EmptySpaceItem2,
            this.EmptySpaceItem4,
            this.EmptySpaceItem3});
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "LayoutControlGroup1";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(329, 190);
            this.LayoutControlGroup1.Text = "LayoutControlGroup1";
            this.LayoutControlGroup1.TextVisible = false;
            // 
            // LayoutControlItem1
            // 
            this.LayoutControlItem1.Control = this.LabelControl1;
            this.LayoutControlItem1.CustomizationFormText = "Please enter the information needed to create the new batch";
            this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem1.Name = "LayoutControlItem1";
            this.LayoutControlItem1.Size = new System.Drawing.Size(309, 17);
            this.LayoutControlItem1.Text = "Please enter the information needed to create the new batch";
            this.LayoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem1.TextToControlDistance = 0;
            this.LayoutControlItem1.TextVisible = false;
            // 
            // LayoutControlItem3
            // 
            this.LayoutControlItem3.Control = this.txt_label;
            this.LayoutControlItem3.CustomizationFormText = "Optional Label:";
            this.LayoutControlItem3.Location = new System.Drawing.Point(0, 41);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(309, 24);
            this.LayoutControlItem3.Text = "Optional Label:";
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(72, 13);
            // 
            // LayoutControlItem4
            // 
            this.LayoutControlItem4.Control = this.ButtonOK;
            this.LayoutControlItem4.CustomizationFormText = "LayoutControlItem4";
            this.LayoutControlItem4.FillControlToClientArea = false;
            this.LayoutControlItem4.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.LayoutControlItem4.Location = new System.Drawing.Point(71, 104);
            this.LayoutControlItem4.Name = "LayoutControlItem4";
            this.LayoutControlItem4.Size = new System.Drawing.Size(79, 27);
            this.LayoutControlItem4.Text = "LayoutControlItem4";
            this.LayoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem4.TextToControlDistance = 0;
            this.LayoutControlItem4.TextVisible = false;
            // 
            // LayoutControlItem5
            // 
            this.LayoutControlItem5.Control = this.ButtonCancel;
            this.LayoutControlItem5.CustomizationFormText = "LayoutControlItem5";
            this.LayoutControlItem5.FillControlToClientArea = false;
            this.LayoutControlItem5.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.LayoutControlItem5.Location = new System.Drawing.Point(150, 104);
            this.LayoutControlItem5.Name = "LayoutControlItem5";
            this.LayoutControlItem5.Size = new System.Drawing.Size(79, 27);
            this.LayoutControlItem5.Text = "LayoutControlItem5";
            this.LayoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem5.TextToControlDistance = 0;
            this.LayoutControlItem5.TextVisible = false;
            // 
            // EmptySpaceItem1
            // 
            this.EmptySpaceItem1.AllowHotTrack = false;
            this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
            this.EmptySpaceItem1.Location = new System.Drawing.Point(0, 65);
            this.EmptySpaceItem1.MinSize = new System.Drawing.Size(110, 30);
            this.EmptySpaceItem1.Name = "EmptySpaceItem1";
            this.EmptySpaceItem1.Size = new System.Drawing.Size(309, 39);
            this.EmptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.EmptySpaceItem1.Text = "EmptySpaceItem1";
            this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LayoutControlItem2
            // 
            this.LayoutControlItem2.Control = this.LookUpEdit_Bank;
            this.LayoutControlItem2.CustomizationFormText = "Bank Account";
            this.LayoutControlItem2.Location = new System.Drawing.Point(0, 17);
            this.LayoutControlItem2.Name = "LayoutControlItem2";
            this.LayoutControlItem2.Size = new System.Drawing.Size(309, 24);
            this.LayoutControlItem2.Text = "Bank Account";
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(72, 13);
            // 
            // EmptySpaceItem2
            // 
            this.EmptySpaceItem2.AllowHotTrack = false;
            this.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2";
            this.EmptySpaceItem2.Location = new System.Drawing.Point(0, 104);
            this.EmptySpaceItem2.Name = "EmptySpaceItem2";
            this.EmptySpaceItem2.Size = new System.Drawing.Size(71, 27);
            this.EmptySpaceItem2.Text = "EmptySpaceItem2";
            this.EmptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // EmptySpaceItem4
            // 
            this.EmptySpaceItem4.AllowHotTrack = false;
            this.EmptySpaceItem4.CustomizationFormText = "EmptySpaceItem4";
            this.EmptySpaceItem4.Location = new System.Drawing.Point(0, 131);
            this.EmptySpaceItem4.Name = "EmptySpaceItem4";
            this.EmptySpaceItem4.Size = new System.Drawing.Size(309, 39);
            this.EmptySpaceItem4.Text = "EmptySpaceItem4";
            this.EmptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // EmptySpaceItem3
            // 
            this.EmptySpaceItem3.AllowHotTrack = false;
            this.EmptySpaceItem3.CustomizationFormText = "EmptySpaceItem3";
            this.EmptySpaceItem3.Location = new System.Drawing.Point(229, 104);
            this.EmptySpaceItem3.Name = "EmptySpaceItem3";
            this.EmptySpaceItem3.Size = new System.Drawing.Size(80, 27);
            this.EmptySpaceItem3.Text = "EmptySpaceItem3";
            this.EmptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // NewClientDepositBatch_Form
            // 
            this.AcceptButton = this.ButtonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.ButtonCancel;
            this.ClientSize = new System.Drawing.Size(329, 190);
            this.Controls.Add(this.LayoutControl1);
            this.Name = "NewClientDepositBatch_Form";
            this.Text = "Create a new batch";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt_label.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_Bank.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected DevExpress.XtraLayout.LayoutControl LayoutControl1;
        protected DevExpress.XtraEditors.SimpleButton ButtonCancel;
        protected DevExpress.XtraEditors.SimpleButton ButtonOK;
        protected DevExpress.XtraEditors.TextEdit txt_label;
        protected DevExpress.XtraEditors.LookUpEdit LookUpEdit_Bank;
        protected DevExpress.XtraEditors.LabelControl LabelControl1;
        protected DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
        protected DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
        protected DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
        protected DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
        protected DevExpress.XtraLayout.LayoutControlItem LayoutControlItem5;
        protected DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
        protected DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
        protected DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem2;
        internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem4;
        internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem3;
    }
}