using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DebtPlus.UI.Common;

namespace DebtPlus.UI.FormLib.Deposit
{
    public partial class NewClientDepositBatch : ClientDepositBatch
    {
        public NewClientDepositBatch()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            ButtonNew.Click += ButtonNew_Click;
        }

        private void UnRegisterHandlers()
        {
            ButtonNew.Click -= ButtonNew_Click;
        }

        protected void ButtonNew_Click(object sender, EventArgs e) // Handles ButtonNew.Click
        {
            DataTable tbl = ds.Tables["deposit_batch_ids"];
            DataView vue = tbl.DefaultView;
            DataRowView drv = vue.AddNew();

            // Request the deposit batch information
            using (var frm = new NewClientDepositBatch_Form(drv))
            {
                if (frm.ShowDialog() == DialogResult.OK && SaveChanges(drv))
                {
                    drv.EndEdit();
                    GridControl1.RefreshDataSource();

                    // Find the row for the new batch
                    Int32 RowHandle = GridView1.LocateByValue(0, GridColumn_deposit_batch_id, drv["deposit_batch_id"]);
                    if (RowHandle >= 0)
                    {
                        GridView1.ClearSelection();
                        GridView1.SelectRow(RowHandle);
                        DepositBatchID = Convert.ToInt32(drv["deposit_batch_id"]);
                        RaiseSelected();
                    }
                }
                else
                {
                    drv.CancelEdit();
                }
            }
        }

        protected bool SaveChanges(DataRowView drv)
        {
            var answer = false;

            var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            using (new CursorManager())
            {
                try
                {
                    cn.Open();

                    using (var cmd = new SqlCommand())
                    {
                        cmd.Connection = cn;
                        cmd.CommandText = "xpr_deposit_batch_create";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@deposit_batch_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                        cmd.Parameters.Add("@note", SqlDbType.VarChar, 50).Value = drv["note"];
                        cmd.Parameters.Add("@batch_type", SqlDbType.VarChar, 10).Value = BatchType;
                        cmd.Parameters.Add("@bank", SqlDbType.Int).Value = drv["bank"];
                        cmd.ExecuteNonQuery();
                        drv["deposit_batch_id"] = cmd.Parameters[0].Value;

                        answer = true;
                    }
                }
                catch (SqlException ex)
                {
                    using (var gdr = new Repository.GetDataResult())
                    {
                        gdr.HandleException(ex);
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error creating deposit batch ID");
                    }
                }
                finally
                {
                    cn.Dispose();
                }
            }

            return answer;
        }
    }
}