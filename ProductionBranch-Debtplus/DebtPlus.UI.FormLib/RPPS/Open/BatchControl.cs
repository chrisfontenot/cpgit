using System;
using System.Data;
using System.IO;
using System.Xml;
using DebtPlus.UI.Common;
using DebtPlus.Utils;
using DevExpress.Data;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

namespace DebtPlus.UI.FormLib.RPPS.Open
{
    public partial class BatchControl : XtraUserControl
    {
        // Local dataset for the tables
        protected DataSet ds = new DataSet("ds");

        public BatchControl()
        {
            InitializeComponent();

            // If this is not the design mode, i.e. we are really running, then register the handlers.
            // If this is design mode then don't as they can cause problems with database accesses, etc.
            if (!DesignMode) RegisterHandlers();
        }

        /// <summary>
        ///     The selected batch from the input control
        /// </summary>
        public Int32 rpps_response_file
        {
            get
            {
                var answer = -1;

                if (gridView1.SelectedRowsCount > 0)
                {
                    var rowHandle = gridView1.GetSelectedRows()[0];
                    if (rowHandle >= 0)
                    {
                        var row = gridView1.GetDataRow(rowHandle);
                        if (row != null && row["rpps_response_file"] != null)
                            answer = Convert.ToInt32(row["rpps_response_file"]);
                    }
                }
                return answer;
            }
        }

        public event EventHandler Cancelled;
        public event EventHandler Selected;

        private void RPPSResponseBatch_Dispose(bool disposing)
        {
            if (DesignMode) return;
            ds.Dispose();
            UnRegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += OnFormLoad;

            Button_Cancel.Click += Button_Cancel_Click;
            Button_Select.Click += Button_Select_Click;

            gridView1.DoubleClick += gridView1_DoubleClick;
            gridView1.Layout += gridView1_Layout;
            gridView1.SelectionChanged += gridView1_SelectionChanged;
            gridView1.FocusedRowChanged += gridView1_FocusedRowChanged;
            gridView1.RowUpdated += gridView1_RowUpdated;
        }

        private void UnRegisterHandlers()
        {
            Load -= OnFormLoad;

            Button_Cancel.Click -= Button_Cancel_Click;
            Button_Select.Click -= Button_Select_Click;

            gridView1.DoubleClick -= gridView1_DoubleClick;
            gridView1.Layout -= gridView1_Layout;
            gridView1.SelectionChanged -= gridView1_SelectionChanged;
            gridView1.FocusedRowChanged -= gridView1_FocusedRowChanged;
            gridView1.RowUpdated -= gridView1_RowUpdated;
        }

        private void OnFormLoad(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                ReadForm();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        protected virtual void OnCancelled(EventArgs e)
        {
            RaiseCancelled(e);
        }

        protected void RaiseCancelled(EventArgs e)
        {
            var evt = Cancelled;
            if (evt != null) evt(this, e);
        }

        protected void Button_Cancel_Click(object sender, EventArgs e)
        {
            OnCancelled(EventArgs.Empty);
        }

        protected virtual void OnSelected(EventArgs e)
        {
            RaiseSelected(e);
        }

        protected void RaiseSelected(EventArgs e)
        {
            var evt = Selected;
            if (evt != null) evt(this, e);
        }

        protected void Button_Select_Click(object sender, EventArgs e)
        {
            OnSelected(EventArgs.Empty);
        }

        /// <summary>
        ///     Save any changes to the form
        /// </summary>
        private void gridView1_RowUpdated(object sender, RowObjectEventArgs e)
        {
            const string tableName = "rpps_response_files";
            var dt = ds.Tables[tableName];

            var ccr = Repository.General.CommitRPPSResponseFileChanges(dt);
            if (!ccr.Success)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ccr);
            }

            ds.AcceptChanges();
        }

        /// <summary>
        ///     Read the control information
        /// </summary>
        public virtual void ReadForm()
        {
            const string tableName = "rpps_response_files";
            UnRegisterHandlers();
            try
            {
                using (new CursorManager())
                {
                    var gdr = Repository.General.GetAllRPPSResponseFiles(ds, tableName);
                    if (!gdr.Success)
                    {
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "General GetAllRPPSResponseFiles error");
                        return;
                    }

                    var tbl = ds.Tables["rpps_response_files"];
                    tbl.PrimaryKey = new[] { tbl.Columns["rpps_response_file"] };
                    tbl.Columns["rpps_response_file"].AutoIncrement = true;
                    tbl.Columns["rpps_response_file"].AutoIncrementSeed = -1;
                    tbl.Columns["rpps_response_file"].AutoIncrementStep = -1;

                    // Bind the data to the control
                    GridControl1.DataSource = tbl.DefaultView;
                    GridControl1.RefreshDataSource();

                    // Clear the selection and disable the button
                    gridView1.ClearSelection();
                    gridView1.BestFitColumns();
                    ReloadGridControlLayout();

                    Button_Select.Enabled = false;
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        public int NumberOfRppsFilesInList()
        {
            return gridView1.DataRowCount;
        }

        /// Return the directory to the saved layout information
        protected virtual string XMLBasePath()
        {
            return DesignMode ? string.Empty : Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "DebtPlus", "RPPS.Response.File");
        }

        /// Reload the layout of the grid control if needed
        protected void ReloadGridControlLayout()
        {
            string PathName = XMLBasePath();
            if (! string.IsNullOrEmpty(PathName))
            {
                try
                {
                    string FileName = Path.Combine(PathName, string.Format("{0}.Grid.xml", Name));
                    if (File.Exists(FileName))
                    {
                        gridView1.RestoreLayoutFromXml(FileName);
                    }
                }

#pragma warning disable 168 // Yes, I know the "ex" is not used.
                catch (DirectoryNotFoundException ex)
                {
                }
                catch (FileNotFoundException ex)
                {
                }
                catch (XmlException ex)
                {
                }
#pragma warning restore 168
            }
        }

        /// If the layout is changed then update the file
        private void gridView1_Layout(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                string PathName = XMLBasePath();
                if (! string.IsNullOrEmpty(PathName))
                {
                    if (! Directory.Exists(PathName))
                    {
                        Directory.CreateDirectory(PathName);
                    }
                    string FileName = Path.Combine(PathName, string.Format("{0}.Grid.xml", Name));
                    gridView1.SaveLayoutToXml(FileName);
                }
            }

            finally
            {
                RegisterHandlers();
            }
        }

        /// Process a change in the selection items
        private void gridView1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Button_Select.Enabled = (gridView1.SelectedRowsCount > 0);
        }

        /// Move the selected row to the focused row
        private void gridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            Int32 RowHandle = gridView1.FocusedRowHandle;

            // Clear the selection and re-select the focused row. The selection will trip the handler for the button.
            gridView1.ClearSelection();
            gridView1.SelectRow(RowHandle);
        }

        /// Process a double-click event on the row
        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            // Find the row which was clicked
            GridHitInfo hi = gridView1.CalcHitInfo((GridControl1.PointToClient(MousePosition)));
            if (hi.IsValid && hi.InRow)
            {
                Int32 RowHandle = hi.RowHandle;
                if (RowHandle >= 0)
                {
                    gridView1.ClearSelection();
                    gridView1.SelectRow(RowHandle);

                    // If there is a selected row then trip the selected event
                    if (gridView1.SelectedRowsCount > 0)
                    {
                        OnSelected(EventArgs.Empty);
                    }
                }
            }
        }
    }
}
