using System;
using System.Windows.Forms;
using DebtPlus.Data.Forms;

namespace DebtPlus.UI.FormLib.RPPS.Open
{
    public partial class BatchForm : DebtPlusForm
    {
        public BatchForm()
        {
            InitializeComponent();
            if (!DesignMode) RegisterHandlers();
        }

        public Int32 rpps_response_file
        {
            get { return rppsResponseBatch1.rpps_response_file; }
        }

        private void RPPSResponseBatch_Form_Dispose(bool disposing)
        {
            if (!DesignMode) UnRegisterHandlers();
        }

        private void RegisterHandlers()
        {
            rppsResponseBatch1.Cancelled += rppsResponseBatch1_Cancelled;
            rppsResponseBatch1.Selected += rppsResponseBatch1_Selected;
        }

        private void UnRegisterHandlers()
        {
            rppsResponseBatch1.Cancelled -= rppsResponseBatch1_Cancelled;
            rppsResponseBatch1.Selected -= rppsResponseBatch1_Selected;
        }

        private void rppsResponseBatch1_Cancelled(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void rppsResponseBatch1_Selected(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}

