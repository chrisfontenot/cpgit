namespace DebtPlus.UI.FormLib.RPPS.New
{
    partial class CreateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            NewRPPSResponseBatch_Form_Dispose(disposing);
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Label3 = new DevExpress.XtraEditors.LabelControl();
            this.Label1 = new DevExpress.XtraEditors.LabelControl();
            this.txt_label = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_label.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(175, 76);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 24);
            this.Button_Cancel.TabIndex = 11;
            this.Button_Cancel.Text = "&Cancel";
            // 
            // Button_OK
            // 
            this.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Button_OK.Enabled = false;
            this.Button_OK.Location = new System.Drawing.Point(79, 76);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 24);
            this.Button_OK.TabIndex = 10;
            this.Button_OK.Text = "&OK";
            // 
            // Label3
            // 
            this.Label3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.Label3.Location = new System.Drawing.Point(11, 37);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(68, 13);
            this.Label3.TabIndex = 8;
            this.Label3.Text = "Optional Label";
            // 
            // Label1
            // 
            this.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Label1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Label1.Location = new System.Drawing.Point(11, 8);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(294, 13);
            this.Label1.TabIndex = 7;
            this.Label1.Text = "Please enter the information needed to create the new batch";
            this.Label1.ToolTipController = this.ToolTipController1;
            // 
            // txt_label
            // 
            this.txt_label.Location = new System.Drawing.Point(99, 34);
            this.txt_label.Name = "txt_label";
            this.txt_label.Properties.MaxLength = 50;
            this.txt_label.Size = new System.Drawing.Size(232, 20);
            this.txt_label.TabIndex = 9;
            this.txt_label.ToolTip = "If you wish to put a label on this batch, enter it here.";
            this.txt_label.ToolTipController = this.ToolTipController1;
            // 
            // CreateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(342, 108);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.txt_label);
            this.Name = "CreateForm";
            this.Text = "Create New Batch";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt_label.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
        internal DevExpress.XtraEditors.SimpleButton Button_OK;
        internal DevExpress.XtraEditors.LabelControl Label3;
        internal DevExpress.XtraEditors.LabelControl Label1;
        internal DevExpress.XtraEditors.TextEdit txt_label;

        #endregion
        
    }
}