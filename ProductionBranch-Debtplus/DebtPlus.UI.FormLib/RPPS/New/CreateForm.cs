using System;
using System.Data;
using System.Windows.Forms;

namespace DebtPlus.UI.FormLib.RPPS.New
{
    internal partial class CreateForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        private readonly DataRowView _drv;

        private void NewRPPSResponseBatch_Form_Dispose(bool disposing)
        {
            if (!DesignMode) UnRegisterHandlers();
        }

        public CreateForm(DataRowView drv)
        {
            InitializeComponent();
            _drv = drv;
            if (!DesignMode) RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += NewRPPSResponseBatch_Form_Load;
        }

        private void UnRegisterHandlers()
        {
            Load -= NewRPPSResponseBatch_Form_Load;
        }

        private void NewRPPSResponseBatch_Form_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                txt_label.DataBindings.Add("EditValue", _drv, "label", false, DataSourceUpdateMode.OnPropertyChanged);
                Button_OK.Enabled = true;
            }
            finally
            {
                RegisterHandlers();
            }
        }
    }
}