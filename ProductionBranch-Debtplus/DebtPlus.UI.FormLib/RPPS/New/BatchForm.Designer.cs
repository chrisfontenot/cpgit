namespace DebtPlus.UI.FormLib.RPPS.New
{
    partial class BatchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            RequestBatch_Form_Dispose(disposing);
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rppsResponseBatch1 = new BatchControl();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // rppsResponseBatch1
            // 
            this.rppsResponseBatch1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rppsResponseBatch1.Location = new System.Drawing.Point(0, 0);
            this.rppsResponseBatch1.Name = "rppsResponseBatch1";
            this.rppsResponseBatch1.Size = new System.Drawing.Size(582, 295);
            this.rppsResponseBatch1.TabIndex = 0;
            // 
            // BatchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(582, 295);
            this.Controls.Add(this.rppsResponseBatch1);
            this.Name = "BatchForm";
            this.Text = "Request RPPS Response Batch";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private BatchControl rppsResponseBatch1;
    }
}