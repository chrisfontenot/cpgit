using System;
using System.Windows.Forms;
using DebtPlus.Repository;

namespace DebtPlus.UI.FormLib.RPPS.New
{
    public partial class BatchControl : Open.BatchControl
    {
        public BatchControl()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void BatchControl_Dispose(bool disposing)
        {
            UnRegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += OnFormLoad;
            
            Button_New.Click += Button_New_Click;
        }

        private void UnRegisterHandlers()
        {
            Load -= OnFormLoad;
            
            Button_New.Click -= Button_New_Click;
        }

        private void OnFormLoad(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                ReadForm();
                if (gridView1.DataRowCount <= 0) ShowCreateRppsBatchForm();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void Button_New_Click(object sender, EventArgs e)
        {
            ShowCreateRppsBatchForm();
        }

        private void ShowCreateRppsBatchForm()
        {
            var tbl = ds.Tables["rpps_response_files"];
            var vue = tbl.DefaultView;
            var drv = vue.AddNew();

            using (var frm = new CreateForm(drv))
            {
                drv.BeginEdit();
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    drv.EndEdit();
                    SaveChanges();

                    // Find the row in the view.
                    var batch = Convert.ToInt32(drv["rpps_response_file"]);
                    var rowHandle = gridView1.LocateByValue(0, GridColumn_rpps_response_file, batch);
                    if (rowHandle >= 0)
                    {
                        gridView1.ClearSelection();
                        gridView1.SelectRow(rowHandle);
                        OnSelected(EventArgs.Empty);
                    }
                }
                else
                {
                    drv.CancelEdit();
                }
            }
        }

        private void SaveChanges()
        {
            const string tableName = "rpps_response_files";
            var dt = ds.Tables[tableName];

            var ccr = Repository.General.CommitRPPSResponseFileChanges2(dt);
            if (!ccr.Success)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ccr);
            }
        }
    }
}