﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using DebtPlus.LINQ;
using DebtPlus.LINQ.BusinessLayer;

namespace DebtPlus.OCS.Domain
{     
    //The Client is the synchronization boundary, so when we do a commit, we commit the whole object graph
    //also the entire object graph must be valid before a commit is allowed
    public class OCSClient
    {
        public class ValidationResult {
            //i.e. we cannot persist or continue to work with this object as-is
            public List<string> Errors { get; set; }
            //useful information that may or may not stop us from moving forward
            public List<string> Warnings { get; set; }
            //useful diagnostic information
            public List<string> PassedChecks { get; set; }
            //things that will happen when we commit this object
            public List<string> Actions { get; set; }

            public ValidationResult() {
                Errors = new List<string>();
                Warnings = new List<string>();
                PassedChecks = new List<string>();
                Actions = new List<string>();
            }

            public ValidationResult Add(ValidationResult other) {
                this.Errors = this.Errors.Concat(other.Errors).ToList();
                this.Warnings = this.Warnings.Concat(other.Warnings).ToList();
                this.PassedChecks = this.PassedChecks.Concat(other.PassedChecks).ToList();
                this.Actions = this.Actions.Concat(other.Actions).ToList();

                return this;
            }
        }

        public int? ID { get; set; }
        public int? ClientID { get; set; }
        public PROGRAM Program { get; set; }
        public string Servicer { get; set; }
        public string LoanNo { get; set; }
        public Address Address { get; set; }
        public Address PropertyAddress { get; set; }

        public bool UseHomeAddress { get; set; }

        public Person.LANGUAGE PreferredLanguage { get; set; }
        public Person Person1 { get; set; }
        public Person Person2 { get; set; }
        public bool ActiveFlag { get; set; }
        public bool Archive { get; set; }
        public bool IsDuplicate { get; set; }
        public Guid UploadAttempt { get; set; }

        public string InvestorLoanNo { get; set; }
        public DateTime ActualSaleDate { get; set; }
        public DateTime DueDate { get; set; }
        public DateTime LastChanceList { get; set; }

        public DateTime? ClaimedDate { get; set; }
        public string ClaimedBy { get; set; }


        [Obsolete("This property is only included to ease working with current schema.  Please try not to use it.")]
        public TelephoneNumber ClientHomePhone {get;set;}
        [Obsolete("This property is only included to ease working with current schema.  Please try not to use it.")]
        public TelephoneNumber ClientMsgPhone {get;set;}
        [Obsolete("This property is only included to ease working with current schema.  Please try not to use it.")]
        public TelephoneNumber ApplicantCell { get; set; }
        [Obsolete("This property is only included to ease working with current schema.  Please try not to use it.")]
        public TelephoneNumber ApplicantWork { get; set; }
        [Obsolete("This property is only included to ease working with current schema.  Please try not to use it.")]
        public TelephoneNumber CoApplicantCell { get; set; }
        [Obsolete("This property is only included to ease working with current schema.  Please try not to use it.")]
        public TelephoneNumber CoApplicantWork { get; set; }

        private List<TelephoneNumber> phoneNumbers = null;

        /// <summary>
        /// List of phone numbers
        /// </summary>
        /// <remarks>
        /// This implementation looks ugly, and it is.  In the currently D+ schema, phone numbers are referenced in both the client
        /// table and the people table.  We are unable to gather all of these together and turn them into a list during query time
        /// like is done in the list below.  LINQ to SQL does not support the type of constructor used below.  Our other option would
        /// be to go ahead and realize our initial clients results sets and then run another query to retrieve phone numbers.
        /// I am trying to avoid realizing result sets until the very last moment, as this provides us with more flexibility as well
        /// as should be more preferment (less round-trips, potentially less data coming back as well).  So while this is far from
        /// ideal, my judgment is that this is the least of all the evils.  -BW
        /// </remarks>
        public List<TelephoneNumber> PhoneNumbers {
            get {
                if (phoneNumbers == null) {
                    phoneNumbers = (new List<TelephoneNumber> {
#pragma warning disable 618
                        ClientHomePhone,
                        ClientMsgPhone,
                        ApplicantCell,
                        ApplicantWork,
                        CoApplicantCell,
                        CoApplicantWork
#pragma warning restore 618
                    }).Where(n => n != null).ToList();
                }

                return phoneNumbers;
            }
            set {
                phoneNumbers = value;
            }
        }
        public STATUS_CODE StatusCode { get; set; }
        
        public ContactAttempt CurrentAttempt { get; set; }
        public List<ContactAttempt> ContactAttempts { get; set; }
        public int ContactAttemptCount { get; set; }

        public List<client_note> Notes { get; set; }

        public void AddNote(String Source, String Subject, String Body, int NoteType)
        {
            if (Notes == null)
            {
                Notes = new List<client_note>();
            }

            var n = DebtPlus.LINQ.Factory.Manufacture_client_note();
            n.type = NoteType;
            n.subject = Subject;
            n.note = Body;
            Notes.Add(n);
        }
        public string ACH_Flag { get; set; }
        public string Agency_Name { get; set; }
        public string Backlog_Mod_Flag { get; set; }
        public string Mod_Conversion_Date { get; set; }
        public string UPB { get; set; }
        public string Reason_For_Default { get; set; }
        public string Comment_Description { get; set; }
        public string Trial_Mod { get; set; }
        public string Trial_Mod_Description { get; set; }
        public string Trial_Mod_Payment_Amount { get; set; }
        public string TrialPaymentsReceivedAmount { get; set; }
        public string TrialPaymentsReceivedCount { get; set; }
        public string Last_Payment_Applied_Amount { get; set; }
        public string Workout_Type { get; set; }

        public DateTime? FirstCounselDate { get; set; }
        public DateTime? SecondCounselDate { get; set; }
        public DateTime? LastRpcDate { get; set; }

        public int UploadRecordId { get; set; }

        #region New Fields for PostMod (need to read/write to db)

        public DateTime? LastRightPartyContact { get; set; }
        public int? PostModLastChanceFlag { get; set; }
        public DateTime? PostModLastChanceDate { get; set; }
        public OCS_QUEUE? Queue { get; set; }

        #endregion New Fields for PostMod (need to read/write to db)

        public static System.Collections.Generic.List<hecm_default_lookup> getPartner()
        {
            System.Collections.Generic.List<hecm_default_lookup> listRange = new System.Collections.Generic.List<hecm_default_lookup>();

            listRange.Add(new DebtPlus.LINQ.hecm_default_lookup { Id = (int) PROGRAM.HECMDefault, description = "Non-Partner" });
            listRange.Add(new DebtPlus.LINQ.hecm_default_lookup { Id = (int) PROGRAM.OneWestHECM, description = "OneWest" });
            listRange.Add(new DebtPlus.LINQ.hecm_default_lookup { Id = (int) PROGRAM.NationStarHECM, description = "Champion" });
            listRange.Add(new DebtPlus.LINQ.hecm_default_lookup { Id = (int) PROGRAM.RMSHECM, description = "RMS" });
            listRange.Add(new DebtPlus.LINQ.hecm_default_lookup { Id = (int) PROGRAM.WELLSHECM, description = "Wells Fargo" });
            return listRange;
        }
    }
}