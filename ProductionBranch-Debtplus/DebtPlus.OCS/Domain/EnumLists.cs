﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.OCS.Domain
{
    public enum TRIGGER
    {
        NoTrigger,//0
        New,//1
        LanguageChanged,//2
        Result_BN,//3
        Result_CB,//4
        Result_CC,//5
        Result_CO,//6
        Result_DS,//7
        Result_LM,//8
        Result_NS,//9
        Result_NC,//10
        Result_NI,//11
        Result_SA,//12
        Result_SK,//13
        Result_TR,//14
        Result_WL,//15

        //post mod
        Result_EX,//16
        Result_GQ,//17
        Result_IM,//18
        Result_IV,//19
        Result_RP,//20
        Result_ST,//second counsel, 21
        Result_CH
    }
    /*
        *Some useful queries to help generate enum code out of the database:
        *
        * 
select 'DEP_' + ShortCode + ' = ' + cast(Id as varchar(50)) + ',' from debtpluslatest.dbo.ocs_statuscode
order by Id      
        * 
        *
select '{ "' + ShortCode + ' - ' + [Description] + '", STATUS_CODE.' + 'DEP_' + ShortCode + ' },'
from debtpluslatest.dbo.ocs_statuscode
order by Id
        * 
        */
    public enum STATUS_CODE
    {
        NewRecord = 0,
        BadNumber = 1,//BN
        CounselorWillCallback1 = 2,//CB1
        CounselorWillCallback2 = 3,//CB2
        CounselorWillCallback3 = 4,//CB3
        CSRWillCallBack = 5,//CC
        Counseled = 6,//CO
        ContactMade = 7,//CT
        NoContact = 8,//NC
        NoShow = 9,//NS
        OptOut = 10,//OP
        PendingAppointment = 11,//PE        
        TransferToCounselor = 12,//TR
        ScheduledAppointment = 13,//SA

        //post mod
        ExclusionReport = 14,//EX
        GeneralQuestions = 15,//GQ
        ImmediateCounseling = 16,//IM
        InitialContactAttempt1 = 17,//C1
        InitialContactAttempt2 = 18,//C2
        InitialContactAttempt3 = 19,//C3
        InitialContactAttempt4 = 20,//C4
        InitialContactAttemptExtras = 43,//OT

        PaymentReminder1 = 21,//P1
        PaymentReminder2 = 22,//P2
        PaymentReminder3 = 23,//P3
        PaymentReminder4 = 24,//P4
        PaymentReminder5 = 25,//P5
        PaymentReminder6 = 26,//P6
        PaymentReminder7 = 27,//P7
        PaymentReminder8 = 28,//P8
        PaymentReminder9 = 29,//P9
        PaymentReminder10 = 30,//P10
        PaymentReminder11 = 31,//P11
        PaymentReminder12 = 32,//P12

        SecondCounseling = 33,//SCO

        /*All of the following come from the legacy CredAbility database*/
        DEP_BLANK = 34,
        DEP_MC = 35,
        DEP_VM = 36,
        DEP_VN = 37,
        DEP_WS = 38,
        DEP_MT = 39,
        DEP_SP = 40,
        DEP_MZ = 41,
        DEP_BZ = 42,

        Pending = 44,
        AppointmentConfirmed = 45,
        CounselHighTouch = 46,
        InitialContactAttempt5 = 47
    }
    public enum STAGE
    {
        NotSet
    }
    public enum PROGRAM : int
    {
        FreddieMacEI = 0,
        FreddieMac180 = 1,
        FreddieMac720 = 2,
        FreddieMacHamp = 3,
        FreddieMacPostMod = 4,
        NationStarSD1308 = 5,
        FannieMaePostMod = 6,
        PostModPilotCheckin = 7,
        PostModPilotCounseling = 8,
        HECMDefault = 9,
        OneWestHECM = 10,
        NationStarHECM = 11,
        RMSHECM = 12,
        WELLSHECM = 13,
        USBankSD1308 = 14,
        OneWestSD1308 = 15,
    }
    public enum CONTACT_TYPE
    {
        CSR_OutboundCall,//OB
        CSR_Incoming_ReceivedLetter,//IL
        CSR_Incoming_ReturningCall,//IR
        CSR_Internet,//NT
        CSR_ReferredByLender,//RL

        CO_OutboundCall,//OB
        CO_Incoming_ReceivedLetter,//IL
        CO_Incoming_ReturningCall,//IR
        CO_Internet,//NT
        CO_ReferredByLender//RL
    }
    public enum MTI_RESULT
    {
        Busy,
        Attempt,
        VoiceMail,
        ValidNumber
    }
    public enum OCS_QUEUE
    {
        HighTouch,
        Counselor
    }
}
