﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.LINQ;

namespace DebtPlus.OCS.Domain
{
    public class ContactAttempt
    {
        public class CallOutcome
        {
            //"Sub-results" such as the 'Not Interested Reasons' will be included in the overall RESULT_CODE enumeration
            public TelephoneNumber PhoneNumber { get; set; }
            public RESULT_CODE? Result { get; set; }
            public MTI_RESULT? MTIResult { get; set; }
            public bool IsAssigned { get; set; }
        }

        public CONTACT_TYPE? Type { get; set; }
        public DateTime Begin { get; private set; }
        public DateTime End { get; private set; }
        public bool IsClosed { 
            get {
                return Begin != DateTime.MinValue && End != DateTime.MinValue;
            } 
        }
        public List<CallOutcome> Outcomes { get; set; }

        public RESULT_CODE ResultCode { get; set; }
        public string SpecificReason { get; set; }

        public void StartAttempt()
        {
            StartedAttempt(DateTime.Now);
        }
        public void StartedAttempt(DateTime when)
        {
            Begin = when;
            Outcomes = new List<CallOutcome>();
            End = DateTime.MinValue;
        }

        public void EndAttempt()
        {
            EndedAttempt(DateTime.Now);
        }
        //useful when re-hydrating
        public void EndedAttempt(DateTime when)
        {
            End = when;
        }
    }
}