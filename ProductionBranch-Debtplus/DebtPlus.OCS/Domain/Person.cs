﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.OCS.Domain
{
    public class Person
    {
        public enum LANGUAGE
        {
            English = 1,
            Spanish = 2
        }

        public enum SALUTATION
        {
            Mr,
            Mrs,
            Dr,
            Ms,
            Sr,
            Miss
        }

        public enum SUFFIX
        {
            Jr,
            Sr,
            PhD,
            MD,
            II,
            III,
            IV,
            V
        }

        public SALUTATION? Salutation { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string SSN { get; set; }
        public string EmailAddress { get; set; }
        public SUFFIX? Suffix { get; set; }
    }
}
