﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.OCS.Domain
{
    public enum RESULT_CODE
    {
        NotSet = 0,//0
        BadNumber = 1, //1, BN
        CounselorWillCallBack = 2,//2, CB
        CSRWillCallBack = 3,//3, CC
        Counseled = 4,//4, CO
        Deceased = 5,//5, DS
        LeftMessage = 6, //6, LM
        NoContact = 7,//7, NC
        NoShow = 8,//8, NI
        ClientNotInterested = 9, //9, NI
        ScheduledAppointment = 10, //10, SA
        Skip = 11, //11, SK
        TransferredToCounselor = 12, //12, TR
        WorkingWithLender = 13, //13, WL

        /*new for fmac postmod*/
        ImmediateCounseling = 14,//IM
        RightParty = 15,//RP
        GeneralQuestions = 16,//GQ
        SecondCounsel = 17,//ST
        ExclusionReport = 18,
        InboundVoicemail = 19,

        PreviousBadNumber = 20,

        /*All of the following come from the legacy CredAbility database*/
        DEP_PBN = 21,
        DEP_CL = 22,
        DEP_MC = 23,
        DEP_VN = 24,
        DEP_VM = 25,
        DEP_BLANK = 26,
        DEP_MI = 27,
        DEP_M1 = 28,
        DEP_EM = 29,
        DEP_RI = 30,
        DEP_BZ = 31,
        DEP_CD = 32,
        DEP_DC = 33,
        CheckIn = 34
    }

    public static class ResultCodeExtensions
    {

        //Triggers and result codes are not exactly the same, so we need to do this mapping
        private static Dictionary<RESULT_CODE, TRIGGER> CodeToTriggerMap = new Dictionary<RESULT_CODE, TRIGGER> { 
            {RESULT_CODE.BadNumber,                 TRIGGER.Result_BN},
            {RESULT_CODE.CounselorWillCallBack,     TRIGGER.Result_CB},
            {RESULT_CODE.CSRWillCallBack,           TRIGGER.Result_CC},
            {RESULT_CODE.Counseled,                 TRIGGER.Result_CO},
            {RESULT_CODE.Deceased,                  TRIGGER.Result_DS},
            {RESULT_CODE.LeftMessage,               TRIGGER.Result_LM},
            {RESULT_CODE.NoShow,                    TRIGGER.Result_NS},
            {RESULT_CODE.NoContact,                 TRIGGER.Result_NC},
            {RESULT_CODE.ClientNotInterested,       TRIGGER.Result_NI},
            {RESULT_CODE.ScheduledAppointment,      TRIGGER.Result_SA},
            //skip is handled by other means
            {RESULT_CODE.TransferredToCounselor,    TRIGGER.Result_TR},
            {RESULT_CODE.WorkingWithLender,         TRIGGER.Result_WL},
        };

        //used for mapping from a closed contact attempt to a trigger
        public static TRIGGER MapToTrigger(this RESULT_CODE self)
        {
            if (CodeToTriggerMap.ContainsKey(self))
            {
                return CodeToTriggerMap[self];
            }

            return TRIGGER.NoTrigger;
        }
    }
}
