﻿using System;
using System.Linq;
using DebtPlus.OCS;
using DebtPlus.LINQ;

namespace DebtPlus.OCS.InMemory
{
    /// <summary>
    /// The collection of program types are of this class
    /// </summary>
    public class OCSProgramType
    {
        public Int32 Id { get; set; }

        public string description { get; set; }

        public Boolean ActiveFlag { get; set; }

        public Boolean @default { get; set; }

        public OCSProgramType()
        {
            this.ActiveFlag = true;
        }

        internal OCSProgramType(Int32 Id, string description, Boolean ActiveFlag, Boolean @default)
        {
            this.Id = Id;
            this.description = description;
            this.ActiveFlag = ActiveFlag;
            this.@default = @default;
        }
    }

    /// <summary>
    /// This list is used for items that are Yes/No/Ask status such as client's ElectronicStatements field.
    /// </summary>
    public static class OCSProgramTypes
    {
        private static System.Collections.Generic.List<OCSProgramType> col;

        /// <summary>
        /// Initialize the new class structure
        /// </summary>
        static OCSProgramTypes()
        {
            col = new System.Collections.Generic.List<OCSProgramType>();
            col.Add(new OCSProgramType((int)DebtPlus.OCS.Domain.PROGRAM.FreddieMacEI, "FreddieMacEI", true, true));
            col.Add(new OCSProgramType((int)DebtPlus.OCS.Domain.PROGRAM.FreddieMac180, "FreddieMac180", true, false));
            col.Add(new OCSProgramType((int)DebtPlus.OCS.Domain.PROGRAM.FreddieMac720, "FreddieMac720", true, false));
            col.Add(new OCSProgramType((int)DebtPlus.OCS.Domain.PROGRAM.FreddieMacHamp, "FreddieMacHamp", true, false));
            col.Add(new OCSProgramType((int)DebtPlus.OCS.Domain.PROGRAM.FreddieMacPostMod, "FreddieMacPostMod", true, false));
            col.Add(new OCSProgramType((int)DebtPlus.OCS.Domain.PROGRAM.FannieMaePostMod, "FannieMaePostMod", true, false));
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<OCSProgramType> getList()
        {
            return col;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public Int32 getDefault()
        {
            var q = (from l in getList() where l.@default = true select l).FirstOrDefault();
            return q.Id;
        }
    }
}