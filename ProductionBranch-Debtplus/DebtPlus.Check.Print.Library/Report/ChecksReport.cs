#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DevExpress.XtraReports.UI;

namespace DebtPlus.Check.Print.Library
{
    public partial class ChecksReport : DebtPlus.Reports.Template.BaseXtraReportClass
    {
        /// <summary>
        /// Create a new instance of our class
        /// </summary>
        /// <remarks></remarks>
        public ChecksReport() : base()
        {
            InitializeComponents();
        }

        private void InitializeComponents()
        {
            const string ReportName = "DebtPlus.Check.Print.repx";
            const string RootNameSpace = "DebtPlus.Check.Print.Library.Report";

            // See if there is a report reference that we can use
            string Fname = System.IO.Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName);
            bool UseDefault = true;

            // Load the overridden form definition from the reports share
            try
            {
                if (System.IO.File.Exists(Fname))
                {
                    LoadLayout(Fname);
                    UseDefault = false;
                }
            }
            catch { }

            // Load the standard report definition if we need a new item
            if (UseDefault)
            {
                System.Reflection.Assembly _assembly = System.Reflection.Assembly.GetExecutingAssembly();
                using (System.IO.Stream ios = _assembly.GetManifestResourceStream(string.Format("{0}.{1}", RootNameSpace, ReportName)))
                {
                    LoadLayout(ios);
                }

                // Set the watermark to say "VOID" in red letters.
                // This is not saved with the file if the customized item is loaded.
                Watermark.Font = new System.Drawing.Font("Arial", 80f, System.Drawing.FontStyle.Bold);
                Watermark.ForeColor = System.Drawing.Color.Red;
                Watermark.ShowBehind = false;
                Watermark.Text = "    VOID VOID VOID";
                Watermark.TextTransparency = 128;
                Watermark.TextDirection = DevExpress.XtraPrinting.Drawing.DirectionMode.Vertical;
            }

            // Set the script references to all of the loaded modules.
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies();

            // Copy the script references to the subreports as well
            foreach (DevExpress.XtraReports.UI.Band Band in Bands)
            {
                foreach (DevExpress.XtraReports.UI.XRControl ctl in Band.Controls)
                {
                    DevExpress.XtraReports.UI.XRSubreport subReport = ctl as DevExpress.XtraReports.UI.XRSubreport;
                    if (subReport != null)
                    {
                        DevExpress.XtraReports.UI.XtraReport rpt = subReport.ReportSource;
                        if (rpt != null)
                        {
                            rpt.ScriptReferences = ScriptReferences;
                        }
                    }
                }
            }

            // Clear the Parameter values
            SetParameter("ParameterMarker", typeof(string), string.Empty, "Marker", false);
            SetParameter("ParameterTrustRegister", typeof(Int32), -1, "Trust Register", false);
            SetParameter("ParameterBank", typeof(Int32), -1, "Bank #", false);

            // DISABLE THE SELECTION CRITERIA!!! Don't be tempted to enable it.
            ReportFilter.IsEnabled = false;
        }

        public string Parameter_Marker
        {
            get { return Convert.ToString(Parameters["ParameterMarker"].Value); }
            set { Parameters["ParameterMarker"].Value = value; }
        }

        public System.Int32 Parameter_TrustRegister
        {
            get { return Convert.ToInt32(Parameters["ParameterTrustRegister"].Value); }
            set { Parameters["ParameterTrustRegister"].Value = value; }
        }

        public override string ReportTitle
        {
            get { return "Checks"; }
        }

        public override void SetReportParameter(string Parameter, object Value)
        {
            switch (Parameter)
            {
                case "marker":
                    Parameter_Marker = Convert.ToString(Value);
                    break;

                case "trust_register":
                    Parameter_TrustRegister = Convert.ToInt32(Value);
                    break;
                default:
                    break;
            }
        }

        public System.Windows.Forms.DialogResult DisplayPreviewDialog(System.Windows.Forms.IWin32Window owner)
        {
            using (var frm = new DebtPlus.Reports.Template.PrintPreviewForm(this))
            {
                frm.CustomizeReport += CustomizeReportEvent;
                var answer = frm.ShowDialog(owner);
                frm.CustomizeReport -= CustomizeReportEvent;
                return answer;
            }
        }

        #region "Feedback"
        public class FeedbackArgs
        {
            public decimal Amount;
            public Int64 checknum;
            public string Payee;
            public Int32 CurrentRow;

            public Int32 MaxRow;
            public FeedbackArgs()
            {
            }

            public FeedbackArgs(string Payee, decimal Amount, Int64 checknum, Int32 CurrentRow, Int32 MaxRow)
		    {
                this.Payee = Payee;
                this.Amount = Amount;
                this.checknum = checknum;
                this.CurrentRow = CurrentRow;
                this.MaxRow = MaxRow;
		    }

            public override string ToString()
            {
                return string.Format("Check Number: {0:f0}, Amount: {1:c}, Payee: {2}", checknum, Amount, Payee);
            }
        }

        public event FormattingFeedbackEventHandler FormattingFeedback;
        public delegate void FormattingFeedbackEventHandler(object sender, FeedbackArgs e);
        protected virtual void OnFormattingFeedback(FeedbackArgs e)
        {
            if (FormattingFeedback != null)
            {
                FormattingFeedback(this, e);
            }
        }
        #endregion

        protected override void OnDataSourceRowChanged(DevExpress.XtraReports.UI.DataSourceRowEventArgs e)
        {
            base.OnDataSourceRowChanged(e);

            System.Data.DataView vue = (System.Data.DataView)this.DataSource;
            System.Data.DataRowView drv = vue[e.CurrentRow];
            OnFormattingFeedback(new FeedbackArgs(Convert.ToString(drv["payee"]), Convert.ToDecimal(drv["amount"]), Convert.ToInt64(drv["checknum"]), e.CurrentRow, e.RowCount));
        }
    }
}
