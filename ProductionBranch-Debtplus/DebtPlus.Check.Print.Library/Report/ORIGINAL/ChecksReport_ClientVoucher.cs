﻿#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

/* -- Do not use "using" statements since the script won't tolerate them
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.Utils;
using System.Data.SqlClient;
*/

namespace DebtPlus.Check.Print.Library
{
    public partial class ChecksReport_ClientVoucher
    {
        public ChecksReport_ClientVoucher()
            : base()
        {
            InitializeComponent();

            // The scripts handle the formatting code now
            // RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            XrLabel_refund_message.BeforePrint += XrLabel_refund_message_BeforePrint;
        }

        private void UnRegisterHandlers()
        {
            XrLabel_refund_message.BeforePrint -= XrLabel_refund_message_BeforePrint;
        }

        private void XrLabel_refund_message_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            DevExpress.XtraReports.UI.XRLabel lbl = (DevExpress.XtraReports.UI.XRLabel)sender;
            DevExpress.XtraReports.UI.XtraReport rpt = (DevExpress.XtraReports.UI.XtraReport)lbl.Report;
            DevExpress.XtraReports.UI.XtraReport MasterRpt = (DevExpress.XtraReports.UI.XtraReport)rpt.MasterReport;
            DebtPlus.LINQ.SQLInfoClass sqlInfo = DebtPlus.LINQ.SQLInfoClass.getDefault();
            System.Int32 TrustRegister = System.Convert.ToInt32(MasterRpt.GetCurrentColumnValue("trust_register"));

            // Retrieve the voucher information
            try
            {
                using (var cn = new System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString))
                {
                    cn.Open();
                    using (var cmd = new System.Data.SqlClient.SqlCommand())
                    {
                        cmd.Connection = cn;
                        cmd.CommandText = "rpt_PrintChecks_ClientVoucher";
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;

                        System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd);
                        cmd.Parameters[1].Value = TrustRegister;

                        using (var rd = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection | System.Data.CommandBehavior.SingleResult))
                        {
                            // Merge the strings together
                            System.Text.StringBuilder sb = new System.Text.StringBuilder();

                            while (rd.Read())
                            {
                                // If the string is present then add it to the result.
                                if (!rd.IsDBNull(0))
                                {
                                    sb.AppendFormat("{0}{1}", System.Environment.NewLine, System.Convert.ToString(rd.GetValue(0)).Trim());
                                }
                            }

                            // Set the text string
                            if (sb.Length > 0)
                            {
                                sb.Remove(0, 2);
                            }

                            // Add a canned message if there is none.
                            if (sb.Length == 0)
                            {
                                sb.Append("Please detach this detail sheet before depositing the check. Thank you.");
                            }

                            lbl.Text = sb.ToString();
                        }
                    }
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading client voucher");
            }
        }
    }
}