﻿namespace DebtPlus.Check.Print.Library
{
    partial class ChecksReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraPrinting.BarCode.PostNetGenerator postNetGenerator1 = new DevExpress.XtraPrinting.BarCode.PostNetGenerator();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChecksReport));
            this.XrLabel_amount = new DevExpress.XtraReports.UI.XRLabel();
            this.ParameterBank = new DevExpress.XtraReports.Parameters.Parameter();
            this.ParameterMarker = new DevExpress.XtraReports.Parameters.Parameter();
            this.XrLabel_checknum = new DevExpress.XtraReports.UI.XRLabel();
            this.ParameterTrustRegister = new DevExpress.XtraReports.Parameters.Parameter();
            this.XrLabel_address = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel_verbose_amount = new DevExpress.XtraReports.UI.XRLabel();
            this.XrPanel_address_block = new DevExpress.XtraReports.UI.XRPanel();
            this.XrBarCode1 = new DevExpress.XtraReports.UI.XRBarCode();
            this.XrLabel_payee = new DevExpress.XtraReports.UI.XRLabel();
            this.XrSubreport_ClientVoucher = new DevExpress.XtraReports.UI.XRSubreport();
            this.XrLabel_check_id = new DevExpress.XtraReports.UI.XRLabel();
            this.XrPictureBox_signature = new DevExpress.XtraReports.UI.XRPictureBox();
            this.XrSubreport_CreditorVoucher = new DevExpress.XtraReports.UI.XRSubreport();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.XrLabel_micr_code = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel_check_date = new DevExpress.XtraReports.UI.XRLabel();
            this.checksReport_CreditorVoucher1 = new DebtPlus.Check.Print.Library.ChecksReport_CreditorVoucher();
            this.checksReport_ClientVoucher1 = new DebtPlus.Check.Print.Library.ChecksReport_ClientVoucher();
            ((System.ComponentModel.ISupportInitialize)(this.checksReport_CreditorVoucher1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checksReport_ClientVoucher1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.XrLabel_payee,
            this.XrLabel_micr_code,
            this.XrLabel_check_date,
            this.XrLabel_check_id,
            this.XrPanel_address_block,
            this.XrPictureBox_signature,
            this.XrLabel_verbose_amount,
            this.XrLabel_amount,
            this.XrLabel_checknum,
            this.XrSubreport_CreditorVoucher,
            this.XrSubreport_ClientVoucher});
            this.Detail.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.Detail.HeightF = 346F;
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand;
            this.Detail.StylePriority.UseFont = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // XrLabel_amount
            // 
            this.XrLabel_amount.LocationFloat = new DevExpress.Utils.PointFloat(690F, 103.9583F);
            this.XrLabel_amount.Name = "XrLabel_amount";
            this.XrLabel_amount.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel_amount.Scripts.OnBeforePrint = "XrLabel_amount_BeforePrint";
            this.XrLabel_amount.SizeF = new System.Drawing.SizeF(100F, 16F);
            this.XrLabel_amount.Text = "amount";
            // 
            // ParameterBank
            // 
            this.ParameterBank.Description = "Bank ID";
            this.ParameterBank.Name = "ParameterBank";
            this.ParameterBank.Type = typeof(int);
            this.ParameterBank.Value = -1;
            this.ParameterBank.Visible = false;
            // 
            // ParameterMarker
            // 
            this.ParameterMarker.Description = "Batch ID Marker";
            this.ParameterMarker.Name = "ParameterMarker";
            this.ParameterMarker.Value = "";
            // 
            // XrLabel_checknum
            // 
            this.XrLabel_checknum.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "checknum")});
            this.XrLabel_checknum.LocationFloat = new DevExpress.Utils.PointFloat(379F, 10.00001F);
            this.XrLabel_checknum.Name = "XrLabel_checknum";
            this.XrLabel_checknum.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel_checknum.SizeF = new System.Drawing.SizeF(109F, 16F);
            this.XrLabel_checknum.StylePriority.UseTextAlignment = false;
            this.XrLabel_checknum.Text = "Check Number";
            this.XrLabel_checknum.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // ParameterTrustRegister
            // 
            this.ParameterTrustRegister.Description = "Trust register to print a single check";
            this.ParameterTrustRegister.Name = "ParameterTrustRegister";
            this.ParameterTrustRegister.Type = typeof(int);
            this.ParameterTrustRegister.Value = -1;
            // 
            // XrLabel_address
            // 
            this.XrLabel_address.LocationFloat = new DevExpress.Utils.PointFloat(0F, 15.99995F);
            this.XrLabel_address.Multiline = true;
            this.XrLabel_address.Name = "XrLabel_address";
            this.XrLabel_address.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel_address.SizeF = new System.Drawing.SizeF(342F, 84.00003F);
            this.XrLabel_address.Text = "address";
            // 
            // XrLabel_verbose_amount
            // 
            this.XrLabel_verbose_amount.LocationFloat = new DevExpress.Utils.PointFloat(37F, 103.9583F);
            this.XrLabel_verbose_amount.Name = "XrLabel_verbose_amount";
            this.XrLabel_verbose_amount.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel_verbose_amount.Scripts.OnBeforePrint = "XrLabel_verbose_amount_BeforePrint";
            this.XrLabel_verbose_amount.SizeF = new System.Drawing.SizeF(653F, 16F);
            this.XrLabel_verbose_amount.Text = "Verbose Amount";
            // 
            // XrPanel_address_block
            // 
            this.XrPanel_address_block.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.XrLabel_address,
            this.XrBarCode1});
            this.XrPanel_address_block.LocationFloat = new DevExpress.Utils.PointFloat(92.04166F, 142.2917F);
            this.XrPanel_address_block.Name = "XrPanel_address_block";
            this.XrPanel_address_block.Scripts.OnBeforePrint = "XrPanel_address_block_BeforePrint";
            this.XrPanel_address_block.SizeF = new System.Drawing.SizeF(342F, 100F);
            // 
            // XrBarCode1
            // 
            this.XrBarCode1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.XrBarCode1.Name = "XrBarCode1";
            this.XrBarCode1.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100F);
            this.XrBarCode1.ShowText = false;
            this.XrBarCode1.SizeF = new System.Drawing.SizeF(342F, 15.99997F);
            this.XrBarCode1.Symbology = postNetGenerator1;
            // 
            // XrLabel_payee
            // 
            this.XrLabel_payee.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "payee")});
            this.XrLabel_payee.LocationFloat = new DevExpress.Utils.PointFloat(37F, 11F);
            this.XrLabel_payee.Name = "XrLabel_payee";
            this.XrLabel_payee.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel_payee.SizeF = new System.Drawing.SizeF(265.9583F, 16F);
            this.XrLabel_payee.Text = "Payee";
            this.XrLabel_payee.Visible = false;
            // 
            // XrSubreport_ClientVoucher
            // 
            this.XrSubreport_ClientVoucher.LocationFloat = new DevExpress.Utils.PointFloat(0F, 323F);
            this.XrSubreport_ClientVoucher.Name = "XrSubreport_ClientVoucher";
            this.XrSubreport_ClientVoucher.ReportSource = this.checksReport_ClientVoucher1;
            this.XrSubreport_ClientVoucher.Scripts.OnBeforePrint = "XrSubreport_ClientVoucher_BeforePrint";
            this.XrSubreport_ClientVoucher.SizeF = new System.Drawing.SizeF(800F, 23F);
            // 
            // XrLabel_check_id
            // 
            this.XrLabel_check_id.LocationFloat = new DevExpress.Utils.PointFloat(681F, 57.99999F);
            this.XrLabel_check_id.Name = "XrLabel_check_id";
            this.XrLabel_check_id.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel_check_id.Scripts.OnBeforePrint = "XrLabel_check_id_BeforePrint";
            this.XrLabel_check_id.SizeF = new System.Drawing.SizeF(109F, 16F);
            this.XrLabel_check_id.StylePriority.UseTextAlignment = false;
            this.XrLabel_check_id.Text = "Check ID";
            this.XrLabel_check_id.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            // 
            // XrPictureBox_signature
            // 
            this.XrPictureBox_signature.LocationFloat = new DevExpress.Utils.PointFloat(575.0001F, 153.0833F);
            this.XrPictureBox_signature.Name = "XrPictureBox_signature";
            this.XrPictureBox_signature.Scripts.OnBeforePrint = "XrPictureBox_signature_BeforePrint";
            this.XrPictureBox_signature.SizeF = new System.Drawing.SizeF(214.9999F, 89.20834F);
            this.XrPictureBox_signature.Sizing = DevExpress.XtraPrinting.ImageSizeMode.AutoSize;
            // 
            // XrSubreport_CreditorVoucher
            // 
            this.XrSubreport_CreditorVoucher.LocationFloat = new DevExpress.Utils.PointFloat(0F, 300F);
            this.XrSubreport_CreditorVoucher.Name = "XrSubreport_CreditorVoucher";
            this.XrSubreport_CreditorVoucher.ReportSource = this.checksReport_CreditorVoucher1;
            this.XrSubreport_CreditorVoucher.Scripts.OnBeforePrint = "XrSubreport_CreditorVoucher_BeforePrint";
            this.XrSubreport_CreditorVoucher.SizeF = new System.Drawing.SizeF(800F, 23F);
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("checknum", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader1.HeightF = 0F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // XrLabel_micr_code
            // 
            this.XrLabel_micr_code.Font = new System.Drawing.Font("Arial Black", 9.75F);
            this.XrLabel_micr_code.LocationFloat = new DevExpress.Utils.PointFloat(670.8333F, 10.00001F);
            this.XrLabel_micr_code.Name = "XrLabel_micr_code";
            this.XrLabel_micr_code.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel_micr_code.Scripts.OnBeforePrint = "XrLabel_micr_code_BeforePrint";
            this.XrLabel_micr_code.SizeF = new System.Drawing.SizeF(109.7083F, 17F);
            this.XrLabel_micr_code.StylePriority.UseFont = false;
            this.XrLabel_micr_code.Text = "MICR Code";
            this.XrLabel_micr_code.Visible = false;
            // 
            // XrLabel_check_date
            // 
            this.XrLabel_check_date.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "date_created", "{0:d}")});
            this.XrLabel_check_date.LocationFloat = new DevExpress.Utils.PointFloat(468.4167F, 57F);
            this.XrLabel_check_date.Name = "XrLabel_check_date";
            this.XrLabel_check_date.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel_check_date.SizeF = new System.Drawing.SizeF(100F, 17F);
            this.XrLabel_check_date.Text = "Check Date";
            // 
            // ChecksReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.TopMarginBand1,
            this.Detail,
            this.BottomMarginBand1,
            this.GroupHeader1});
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.ParameterMarker,
            this.ParameterTrustRegister,
            this.ParameterBank});
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 0;
            this.RequestParameters = false;
            this.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString");
            this.Scripts.OnBeforePrint = "ChecksReport_BeforePrint";
            this.ScriptsSource = resources.GetString("$this.ScriptsSource");
            this.Version = "11.2";
            this.Controls.SetChildIndex(this.GroupHeader1, 0);
            this.Controls.SetChildIndex(this.BottomMarginBand1, 0);
            this.Controls.SetChildIndex(this.Detail, 0);
            this.Controls.SetChildIndex(this.TopMarginBand1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.checksReport_CreditorVoucher1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checksReport_ClientVoucher1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.XRLabel XrLabel_amount;
        private DevExpress.XtraReports.Parameters.Parameter ParameterBank;
        private DevExpress.XtraReports.Parameters.Parameter ParameterMarker;
        private DevExpress.XtraReports.UI.XRLabel XrLabel_checknum;
        private DevExpress.XtraReports.UI.XRLabel XrLabel_payee;
        private DevExpress.XtraReports.UI.XRLabel XrLabel_micr_code;
        private DevExpress.XtraReports.UI.XRLabel XrLabel_check_date;
        private DevExpress.XtraReports.UI.XRLabel XrLabel_check_id;
        private DevExpress.XtraReports.UI.XRPanel XrPanel_address_block;
        private DevExpress.XtraReports.UI.XRLabel XrLabel_address;
        private DevExpress.XtraReports.UI.XRBarCode XrBarCode1;
        private DevExpress.XtraReports.UI.XRPictureBox XrPictureBox_signature;
        private DevExpress.XtraReports.UI.XRLabel XrLabel_verbose_amount;
        private DevExpress.XtraReports.UI.XRSubreport XrSubreport_CreditorVoucher;
        private DevExpress.XtraReports.UI.XRSubreport XrSubreport_ClientVoucher;
        private DevExpress.XtraReports.Parameters.Parameter ParameterTrustRegister;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private ChecksReport_CreditorVoucher checksReport_CreditorVoucher1;
        private ChecksReport_ClientVoucher checksReport_ClientVoucher1;
    }
}
