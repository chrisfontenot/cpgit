﻿namespace DebtPlus.Check.Print.Library
{
    partial class ChecksReport_CreditorVoucher : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary4 = new DevExpress.XtraReports.UI.XRSummary();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChecksReport_CreditorVoucher));
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.XrLabel_gross = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel_net = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel_total_net = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel_checknum = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel_account_number = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.XrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel_total_gross = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.XrLabel_total_deducted = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel_total_billed = new DevExpress.XtraReports.UI.XRLabel();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.XrLabel_deducted = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel_billed = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel_client_name = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel_client_id = new DevExpress.XtraReports.UI.XRLabel();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 25F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // XrLabel_gross
            // 
            this.XrLabel_gross.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "gross", "{0:c}")});
            this.XrLabel_gross.LocationFloat = new DevExpress.Utils.PointFloat(486.3333F, 0F);
            this.XrLabel_gross.Name = "XrLabel_gross";
            this.XrLabel_gross.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.XrLabel_gross.SizeF = new System.Drawing.SizeF(84.8334F, 16F);
            this.XrLabel_gross.StylePriority.UsePadding = false;
            this.XrLabel_gross.StylePriority.UseTextAlignment = false;
            this.XrLabel_gross.Text = "$0.00";
            this.XrLabel_gross.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.XrLabel_gross.WordWrap = false;
            // 
            // XrLabel_net
            // 
            this.XrLabel_net.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "net", "{0:c}")});
            this.XrLabel_net.LocationFloat = new DevExpress.Utils.PointFloat(719F, 0F);
            this.XrLabel_net.Name = "XrLabel_net";
            this.XrLabel_net.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.XrLabel_net.SizeF = new System.Drawing.SizeF(81F, 16F);
            this.XrLabel_net.StylePriority.UsePadding = false;
            this.XrLabel_net.StylePriority.UseTextAlignment = false;
            this.XrLabel_net.Text = "$0.00";
            this.XrLabel_net.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.XrLabel_net.WordWrap = false;
            // 
            // XrLabel_total_net
            // 
            this.XrLabel_total_net.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "net")});
            this.XrLabel_total_net.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.XrLabel_total_net.LocationFloat = new DevExpress.Utils.PointFloat(716.5417F, 10.00001F);
            this.XrLabel_total_net.Name = "XrLabel_total_net";
            this.XrLabel_total_net.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.XrLabel_total_net.SizeF = new System.Drawing.SizeF(81F, 16F);
            this.XrLabel_total_net.StylePriority.UseFont = false;
            this.XrLabel_total_net.StylePriority.UsePadding = false;
            this.XrLabel_total_net.StylePriority.UseTextAlignment = false;
            xrSummary1.FormatString = "{0:c}";
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.XrLabel_total_net.Summary = xrSummary1;
            this.XrLabel_total_net.Text = "$0.00";
            this.XrLabel_total_net.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.XrLabel_total_net.WordWrap = false;
            // 
            // XrLabel_checknum
            // 
            this.XrLabel_checknum.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.XrLabel_checknum.LocationFloat = new DevExpress.Utils.PointFloat(227.4167F, 1.25F);
            this.XrLabel_checknum.Name = "XrLabel_checknum";
            this.XrLabel_checknum.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.XrLabel_checknum.Scripts.OnBeforePrint = "XrLabel_checknum_BeforePrint";
            this.XrLabel_checknum.SizeF = new System.Drawing.SizeF(102F, 15F);
            this.XrLabel_checknum.StylePriority.UseFont = false;
            this.XrLabel_checknum.StylePriority.UsePadding = false;
            this.XrLabel_checknum.StylePriority.UseTextAlignment = false;
            this.XrLabel_checknum.Text = "check";
            this.XrLabel_checknum.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.XrLabel_checknum.WordWrap = false;
            // 
            // XrLabel_account_number
            // 
            this.XrLabel_account_number.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "account_number")});
            this.XrLabel_account_number.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.XrLabel_account_number.Name = "XrLabel_account_number";
            this.XrLabel_account_number.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.XrLabel_account_number.SizeF = new System.Drawing.SizeF(192F, 16F);
            this.XrLabel_account_number.StylePriority.UsePadding = false;
            this.XrLabel_account_number.StylePriority.UseTextAlignment = false;
            this.XrLabel_account_number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.XrLabel_account_number.WordWrap = false;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.XrLabel1});
            this.ReportHeader.HeightF = 50F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // XrLabel1
            // 
            this.XrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(22.91667F, 10.00001F);
            this.XrLabel1.Name = "XrLabel1";
            this.XrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel1.SizeF = new System.Drawing.SizeF(100F, 23F);
            // 
            // XrLabel_total_gross
            // 
            this.XrLabel_total_gross.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "gross")});
            this.XrLabel_total_gross.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.XrLabel_total_gross.LocationFloat = new DevExpress.Utils.PointFloat(483.875F, 10.00001F);
            this.XrLabel_total_gross.Name = "XrLabel_total_gross";
            this.XrLabel_total_gross.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.XrLabel_total_gross.SizeF = new System.Drawing.SizeF(87.29172F, 16F);
            this.XrLabel_total_gross.StylePriority.UseFont = false;
            this.XrLabel_total_gross.StylePriority.UsePadding = false;
            this.XrLabel_total_gross.StylePriority.UseTextAlignment = false;
            xrSummary2.FormatString = "{0:c}";
            xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.XrLabel_total_gross.Summary = xrSummary2;
            this.XrLabel_total_gross.Text = "$0.00";
            this.XrLabel_total_gross.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.XrLabel_total_gross.WordWrap = false;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.XrLabel_checknum,
            this.XrLabel_total_deducted,
            this.XrLabel_total_billed,
            this.XrLabel_total_gross,
            this.XrLabel_total_net});
            this.ReportFooter.HeightF = 50F;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.PrintAtBottom = true;
            // 
            // XrLabel_total_deducted
            // 
            this.XrLabel_total_deducted.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "deducted")});
            this.XrLabel_total_deducted.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.XrLabel_total_deducted.LocationFloat = new DevExpress.Utils.PointFloat(571.1668F, 10.00001F);
            this.XrLabel_total_deducted.Name = "XrLabel_total_deducted";
            this.XrLabel_total_deducted.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.XrLabel_total_deducted.SizeF = new System.Drawing.SizeF(66.95825F, 16F);
            this.XrLabel_total_deducted.StylePriority.UseFont = false;
            this.XrLabel_total_deducted.StylePriority.UsePadding = false;
            this.XrLabel_total_deducted.StylePriority.UseTextAlignment = false;
            xrSummary3.FormatString = "{0:c}";
            xrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.XrLabel_total_deducted.Summary = xrSummary3;
            this.XrLabel_total_deducted.Text = "$0.00";
            this.XrLabel_total_deducted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.XrLabel_total_deducted.WordWrap = false;
            // 
            // XrLabel_total_billed
            // 
            this.XrLabel_total_billed.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "billed")});
            this.XrLabel_total_billed.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.XrLabel_total_billed.LocationFloat = new DevExpress.Utils.PointFloat(638.1251F, 10.00001F);
            this.XrLabel_total_billed.Name = "XrLabel_total_billed";
            this.XrLabel_total_billed.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.XrLabel_total_billed.SizeF = new System.Drawing.SizeF(78.41663F, 16F);
            this.XrLabel_total_billed.StylePriority.UseFont = false;
            this.XrLabel_total_billed.StylePriority.UsePadding = false;
            this.XrLabel_total_billed.StylePriority.UseTextAlignment = false;
            xrSummary4.FormatString = "{0:c}";
            xrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.XrLabel_total_billed.Summary = xrSummary4;
            this.XrLabel_total_billed.Text = "$0.00";
            this.XrLabel_total_billed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.XrLabel_total_billed.WordWrap = false;
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.XrLabel_deducted,
            this.XrLabel_billed,
            this.XrLabel_account_number,
            this.XrLabel_gross,
            this.XrLabel_net,
            this.XrLabel_client_name,
            this.XrLabel_client_id});
            this.Detail.HeightF = 16F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // XrLabel_deducted
            // 
            this.XrLabel_deducted.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "deducted", "{0:c}")});
            this.XrLabel_deducted.LocationFloat = new DevExpress.Utils.PointFloat(571.1667F, 0F);
            this.XrLabel_deducted.Name = "XrLabel_deducted";
            this.XrLabel_deducted.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.XrLabel_deducted.SizeF = new System.Drawing.SizeF(69.41656F, 16F);
            this.XrLabel_deducted.StylePriority.UsePadding = false;
            this.XrLabel_deducted.StylePriority.UseTextAlignment = false;
            this.XrLabel_deducted.Text = "$0.00";
            this.XrLabel_deducted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.XrLabel_deducted.WordWrap = false;
            // 
            // XrLabel_billed
            // 
            this.XrLabel_billed.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "billed", "{0:c}")});
            this.XrLabel_billed.LocationFloat = new DevExpress.Utils.PointFloat(640.5834F, 0F);
            this.XrLabel_billed.Name = "XrLabel_billed";
            this.XrLabel_billed.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.XrLabel_billed.SizeF = new System.Drawing.SizeF(78.41663F, 16F);
            this.XrLabel_billed.StylePriority.UsePadding = false;
            this.XrLabel_billed.StylePriority.UseTextAlignment = false;
            this.XrLabel_billed.Text = "$0.00";
            this.XrLabel_billed.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.XrLabel_billed.WordWrap = false;
            // 
            // XrLabel_client_name
            // 
            this.XrLabel_client_name.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "client_name")});
            this.XrLabel_client_name.LocationFloat = new DevExpress.Utils.PointFloat(203.1251F, 0F);
            this.XrLabel_client_name.Name = "XrLabel_client_name";
            this.XrLabel_client_name.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.XrLabel_client_name.SizeF = new System.Drawing.SizeF(165.9583F, 16F);
            this.XrLabel_client_name.StylePriority.UsePadding = false;
            this.XrLabel_client_name.StylePriority.UseTextAlignment = false;
            this.XrLabel_client_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.XrLabel_client_name.WordWrap = false;
            // 
            // XrLabel_client_id
            // 
            this.XrLabel_client_id.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "client", "{0:0000000}")});
            this.XrLabel_client_id.LocationFloat = new DevExpress.Utils.PointFloat(369.0834F, 0F);
            this.XrLabel_client_id.Name = "XrLabel_client_id";
            this.XrLabel_client_id.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.XrLabel_client_id.SizeF = new System.Drawing.SizeF(106.8333F, 16F);
            this.XrLabel_client_id.StylePriority.UsePadding = false;
            this.XrLabel_client_id.StylePriority.UseTextAlignment = false;
            this.XrLabel_client_id.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.XrLabel_client_id.WordWrap = false;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 25F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ChecksReport_CreditorVoucher
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportFooter,
            this.ReportHeader});
            this.Margins = new System.Drawing.Printing.Margins(25, 25, 25, 25);
            this.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString");
            this.Scripts.OnBeforePrint = "ChecksReport_CreditorVoucher_BeforePrint";
            this.ScriptsSource = resources.GetString("$this.ScriptsSource");
            this.Version = "11.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.XRLabel XrLabel_gross;
        private DevExpress.XtraReports.UI.XRLabel XrLabel_net;
        private DevExpress.XtraReports.UI.XRLabel XrLabel_total_net;
        private DevExpress.XtraReports.UI.XRLabel XrLabel_checknum;
        private DevExpress.XtraReports.UI.XRLabel XrLabel_account_number;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRLabel XrLabel1;
        private DevExpress.XtraReports.UI.XRLabel XrLabel_total_gross;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLabel XrLabel_total_deducted;
        private DevExpress.XtraReports.UI.XRLabel XrLabel_total_billed;
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.XRLabel XrLabel_deducted;
        private DevExpress.XtraReports.UI.XRLabel XrLabel_billed;
        private DevExpress.XtraReports.UI.XRLabel XrLabel_client_name;
        private DevExpress.XtraReports.UI.XRLabel XrLabel_client_id;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
    }
}
