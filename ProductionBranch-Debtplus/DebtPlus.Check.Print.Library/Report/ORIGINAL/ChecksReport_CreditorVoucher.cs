﻿#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

/* -- Do not use "using" statements since the scripts won't tolerate them
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.Utils;
using System.Data.SqlClient;
*/

namespace DebtPlus.Check.Print.Library
{
    public partial class ChecksReport_CreditorVoucher
    {
        public ChecksReport_CreditorVoucher()
            : base()
        {
            InitializeComponent();

            // The scripts handle the formatting code now
            // RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            this.BeforePrint += ChecksReport_CreditorVoucher_BeforePrint;
            this.XrLabel_checknum.BeforePrint += XrLabel_checknum_BeforePrint;
        }

        private void UnRegisterHandlers()
        {
            this.BeforePrint -= ChecksReport_CreditorVoucher_BeforePrint;
            this.XrLabel_checknum.BeforePrint -= XrLabel_checknum_BeforePrint;
        }

        System.Data.DataSet ds = new System.Data.DataSet("ds");
        private void ChecksReport_CreditorVoucher_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            const string TableName = "rpt_PrintChecks_CreditorVoucher";
            DebtPlus.LINQ.SQLInfoClass SqlInfo = DebtPlus.LINQ.SQLInfoClass.getDefault();
            DevExpress.XtraReports.UI.XtraReport rpt = (DevExpress.XtraReports.UI.XtraReport)sender;
            DevExpress.XtraReports.UI.XtraReport MasterRpt = (DevExpress.XtraReports.UI.XtraReport)rpt.MasterReport;
            System.Int32 TrustRegister = System.Convert.ToInt32(MasterRpt.GetCurrentColumnValue("trust_register"));

            // Retrieve the voucher information
            try
            {
                ds.Clear();
                using (var cn = new System.Data.SqlClient.SqlConnection(SqlInfo.ConnectionString))
                {
                    cn.Open();

                    using (var cmd = new System.Data.SqlClient.SqlCommand())
                    {
                        cmd.Connection = cn;
                        cmd.CommandText = "rpt_PrintChecks_CreditorVoucher";
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.Add("@check", System.Data.SqlDbType.Int, 4).Value = TrustRegister;

                        using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                        {
                            da.Fill(ds, TableName);
                        }
                    }
                }

                // Populate the table information for the sub-report
                rpt.DataSource = ds.Tables[TableName].DefaultView;

            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading creditor check voucher");
            }
        }

        private void XrLabel_checknum_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            DevExpress.XtraReports.UI.XRLabel lbl = (DevExpress.XtraReports.UI.XRLabel)sender;
            DevExpress.XtraReports.UI.XtraReport rpt = (DevExpress.XtraReports.UI.XtraReport)lbl.Report;
            DevExpress.XtraReports.UI.XtraReport MasterRpt = (DevExpress.XtraReports.UI.XtraReport)rpt.MasterReport;
            string checknum = System.Convert.ToString(MasterRpt.GetCurrentColumnValue("checknum"));
            lbl.Text = checknum;
        }
    }
}