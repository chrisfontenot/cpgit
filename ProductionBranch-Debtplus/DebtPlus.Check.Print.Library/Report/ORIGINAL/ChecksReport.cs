﻿#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

/* -- DO NOT use any "using" statements as the scripts won't tolerate them.
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.Reports.Template;
using DebtPlus.Utils;
using System.Data.SqlClient;
using DebtPlus.Utils.Format;
*/

namespace DebtPlus.Check.Print.Library
{
    public partial class ChecksReport : DebtPlus.Reports.Template.BaseXtraReportClass
    {
        public ChecksReport()
            : this(false)
        {
        }

        /// <summary>
        /// Create a new instance of our class
        /// </summary>
        /// <param name="ShowWaitingDialog">TRUE if we should show the waiting busy dialog</param>
        /// <remarks></remarks>
        public ChecksReport(bool ShowWaitingDialog)
            : base(ShowWaitingDialog)
        {
            InitializeComponent();

            // Clear the Parameter values
            Parameters["ParameterMarker"].Value = string.Empty;
            Parameters["ParameterMarker"].Visible = false;

            Parameters["ParameterTrustRegister"].Value = -1;
            Parameters["ParameterTrustRegister"].Visible = false;

            Parameters["ParameterBank"].Value = -1;
            Parameters["ParameterBank"].Visible = false;

            // The scripts handle the formatting code now
            // RegisterHandlers();

            // DISABLE THE SELECTION CRITERIA!!! Don't be tempted to enable it.
            ReportFilter.IsEnabled = false;
        }

        private void RegisterHandlers()
        {
            BeforePrint += ChecksReport_BeforePrint;
            XrLabel_check_id.BeforePrint += XrLabel_check_id_BeforePrint;
            XrLabel_amount.BeforePrint += XrLabel_amount_BeforePrint;
            XrLabel_micr_code.BeforePrint += XrLabel_micr_code_BeforePrint;
            XrLabel_verbose_amount.BeforePrint += XrLabel_verbose_amount_BeforePrint;
            XrSubreport_CreditorVoucher.BeforePrint += XrSubreport_CreditorVoucher_BeforePrint;
            XrSubreport_ClientVoucher.BeforePrint += XrSubreport_ClientVoucher_BeforePrint;
            XrPanel_address_block.BeforePrint += XrPanel_address_block_BeforePrint;
            XrPictureBox_signature.BeforePrint += XrPictureBox_signature_BeforePrint;
        }

        private void unRegisterHandlers()
        {
            BeforePrint -= ChecksReport_BeforePrint;
            XrLabel_check_id.BeforePrint -= XrLabel_check_id_BeforePrint;
            XrLabel_amount.BeforePrint -= XrLabel_amount_BeforePrint;
            XrLabel_micr_code.BeforePrint -= XrLabel_micr_code_BeforePrint;
            XrLabel_verbose_amount.BeforePrint -= XrLabel_verbose_amount_BeforePrint;
            XrSubreport_CreditorVoucher.BeforePrint -= XrSubreport_CreditorVoucher_BeforePrint;
            XrSubreport_ClientVoucher.BeforePrint -= XrSubreport_ClientVoucher_BeforePrint;
            XrPanel_address_block.BeforePrint -= XrPanel_address_block_BeforePrint;
            XrPictureBox_signature.BeforePrint -= XrPictureBox_signature_BeforePrint;
        }

        /// <summary>
        /// The report title
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks>This is the title for the preview form</remarks>
        public override string ReportTitle
        {
            get
            {
                return "Checks";
            }
        }

        /// <summary>
        /// Override the data source row being changed to send the feedback to the caller.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnDataSourceRowChanged(DevExpress.XtraReports.UI.DataSourceRowEventArgs e)
        {
            base.OnDataSourceRowChanged(e);

            System.Data.DataView vue = (System.Data.DataView)this.DataSource;
            System.Data.DataRowView drv = vue[e.CurrentRow];
            OnFormattingFeedback(new FeedbackArgs(System.Convert.ToString(drv["payee"]), System.Convert.ToDecimal(drv["amount"]), System.Convert.ToInt64(drv["checknum"]), e.CurrentRow, e.RowCount));
        }

        /// <summary>
        /// Set the report parameter values.
        /// </summary>
        public override void SetReportParameter(string Parameter, object Value)
        {
            switch (Parameter)
            {
                case "printMarker":
                    Parameter_Marker = System.Convert.ToString(Value);
                    break;

                case "trust_register":
                    Parameter_TrustRegister = System.Convert.ToInt32(Value);
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// Marker string for the checks that should be printed
        /// </summary>
        public string Parameter_Marker
        {
            get
            {
                return System.Convert.ToString(Parameters["ParameterMarker"].Value);
            }
            set
            {
                Parameters["ParameterMarker"].Value = value;
            }
        }

        /// <summary>
        /// Trust register if we are printing a single check. No printMarker is needed. It prints just the one check.
        /// </summary>
        public System.Int32 Parameter_TrustRegister
        {
            get
            {
                return System.Convert.ToInt32(Parameters["ParameterTrustRegister"].Value);
            }
            set
            {
                Parameters["ParameterTrustRegister"].Value = value;
            }
        }

        public new System.Windows.Forms.DialogResult ShowPreviewDialog()
        {
            return this.ShowPreviewDialog(null);
        }

        public System.Windows.Forms.DialogResult ShowPreviewDialog(System.Windows.Forms.IWin32Window owner)
        {
            using (var frm = new DebtPlus.Reports.Template.PrintPreviewForm(this))
            {
                return frm.ShowDialog(owner);
            }
        }

        public new void ShowPreview()
        {
            var frm = new DebtPlus.Reports.Template.PrintPreviewForm(this);
            frm.Show();
        }

        #region "Feedback"
        public class FeedbackArgs
        {
            public decimal Amount;
            public string Payee;
            public System.Int64 checknum;
            public System.Int32 CurrentRow;
            public System.Int32 MaxRow;

            public FeedbackArgs()
            {
            }

            public FeedbackArgs(string Payee, decimal Amount, System.Int64 checknum, System.Int32 CurrentRow, System.Int32 MaxRow)
            {
                this.Payee = Payee;
                this.Amount = Amount;
                this.checknum = checknum;
                this.CurrentRow = CurrentRow;
                this.MaxRow = MaxRow;
            }

            public override string ToString()
            {
                return string.Format("Check Number: {0:f0}, Amount: {1:c}, Payee: {2}", checknum, Amount, Payee);
            }
        }

        public event FormattingFeedbackEventHandler FormattingFeedback;
        public delegate void FormattingFeedbackEventHandler(object sender, FeedbackArgs e);
        protected virtual void OnFormattingFeedback(FeedbackArgs e)
        {
            if (FormattingFeedback != null)
            {
                FormattingFeedback(this, e);
            }
        }
        #endregion

        // Storage for the current check batch
        public System.Data.DataSet ds = new System.Data.DataSet("ds");

        private void ChecksReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            const string TableName = "rpt_PrintChecks";
            DevExpress.XtraReports.UI.XtraReport rpt = (DevExpress.XtraReports.UI.XtraReport)sender;
            DevExpress.XtraReports.UI.XtraReport masterRpt = rpt;
            DebtPlus.LINQ.SQLInfoClass sqlInfo = DebtPlus.LINQ.SQLInfoClass.getDefault();

            // Retrieve the information to print the report
            try
            {
                using (var cn = new System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString))
                {
                    cn.Open();

                    // Load the bank information (without signature)
                    using (var cmd = new System.Data.SqlClient.SqlCommand())
                    {
                        cmd.Connection = cn;
                        cmd.CommandText = "SELECT [bank], [aba], [account_number], [default] FROM [banks] WHERE [type] = 'C'";
                        cmd.CommandType = System.Data.CommandType.Text;

                        using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                        {
                            da.Fill(ds, "banks");
                            System.Data.DataTable banksTbl = ds.Tables["banks"];
                            banksTbl.PrimaryKey = new System.Data.DataColumn[] { banksTbl.Columns[0] };
                        }
                    }

                    // Read the information
                    using (var cmd = new System.Data.SqlClient.SqlCommand())
                    {
                        cmd.Connection = cn;

                        // If a printMarker was given then use it.
                        string MarkerString = System.Convert.ToString(rpt.Parameters["ParameterMarker"].Value);
                        if (MarkerString != string.Empty)
                        {
                            cmd.CommandText = "rpt_PrintChecks";
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.Add("@marker", System.Data.SqlDbType.VarChar, 50).Value = MarkerString;
                        }
                        else
                        {
                            // Otherwise, use the trust register
                            cmd.CommandText = "SELECT t.trust_register,case when t.creditor is not null then cr.creditor_name when t.client is not null then dbo.format_normal_name (pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) else '' end as payee, case when t.client is not null then 'CL' when t.creditor is null then 'UN' when cr.voucher_spacing = 2 then 'NV' when cr.voucher_spacing = 1 then 'CR2' else 'CR1' end as 'tran_type', t.client, t.creditor, t.date_created, t.checknum, t.amount, t.bank FROM registers_trust t LEFT OUTER JOIN creditors cr with (nolock) ON t.creditor = cr.creditor left outer join people p with (nolock) on t.client = p.client and 1 = p.relation LEFT OUTER JOIN names pn with (nolock) ON p.NameID = pn.Name WHERE t.trust_register=@trust_register";
                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.Parameters.Add("@trust_register", System.Data.SqlDbType.Int).Value = rpt.Parameters["ParameterTrustRegister"].Value;
                        }

                        // Read the dataset for the check list
                        using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                        {
                            da.Fill(ds, TableName);
                        }
                    }

                    // Ensure that there is a bank number
                    System.Data.DataTable tbl = ds.Tables[TableName];

                    // Force the printing sequence to be in check number order
                    System.Data.DataView vue = new System.Data.DataView(tbl, string.Empty, "checknum", System.Data.DataViewRowState.CurrentRows);
                    rpt.DataSource = vue;

                    // Find a trust register from the first check. The trust register has the bank number.
                    System.Int32 Bank = System.Convert.ToInt32(rpt.Parameters["ParameterBank"].Value);
                    if (Bank <= 0)
                    {
                        System.Int32 trust_register = -1;
                        if (vue.Count > 0)
                        {
                            System.Data.DataColumn col = tbl.Columns["trust_register"];
                            if (col != null && (!object.ReferenceEquals(vue[0][col.Ordinal], System.DBNull.Value)))
                            {
                                trust_register = System.Convert.ToInt32(vue[0][col.Ordinal]);
                            }

                            if (trust_register > 0)
                            {
                                using (var cmd = new System.Data.SqlClient.SqlCommand())
                                {
                                    cmd.Connection = cn;
                                    cmd.CommandText = "SELECT bank FROM registers_trust WITH (NOLOCK) WHERE trust_register = @trust_register";
                                    cmd.CommandType = System.Data.CommandType.Text;
                                    cmd.Parameters.Add("@trust_register", System.Data.SqlDbType.Int).Value = trust_register;

                                    object ObjAnswer = cmd.ExecuteScalar();
                                    if (!object.ReferenceEquals(ObjAnswer, System.DBNull.Value))
                                    {
                                        Bank = System.Convert.ToInt32(ObjAnswer);
                                    }
                                }

                                rpt.Parameters["ParameterBank"].Value = Bank;
                            }
                        }

                        // If there is no bank then use the default bank for the signature
                        if (Bank <= 0)
                        {
                            using (var cmd = new System.Data.SqlClient.SqlCommand())
                            {
                                cmd.Connection = cn;
                                cmd.CommandText = "SELECT bank FROM registers_trust WITH (NOLOCK) WHERE trust_register = @trust_register";
                                cmd.CommandType = System.Data.CommandType.Text;
                                cmd.Parameters.Add("@trust_register", System.Data.SqlDbType.Int).Value = trust_register;

                                object ObjAnswer = cmd.ExecuteScalar();
                                if (!object.ReferenceEquals(ObjAnswer, System.DBNull.Value))
                                {
                                    Bank = System.Convert.ToInt32(ObjAnswer);
                                }
                            }
                        }

                        if (Bank <= 0)
                        {
                            using (var cmd = new System.Data.SqlClient.SqlCommand())
                            {
                                cmd.Connection = cn;
                                cmd.CommandText = "SELECT [bank] FROM banks WHERE [type]='C' AND [Default] <> 0 UNION ALL SELECT min(bank) as bank FROM Banks WHERE type = 'C'";
                                cmd.CommandType = System.Data.CommandType.Text;
                                using (System.Data.SqlClient.SqlDataReader rd = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection | System.Data.CommandBehavior.SingleResult))
                                {
                                    if (rd.Read())
                                    {
                                        if (!rd.IsDBNull(0))
                                        {
                                            Bank = System.Convert.ToInt32(rd.GetValue(0));
                                        }
                                    }
                                }
                            }
                        }

                        rpt.Parameters["ParameterBank"].Value = Bank;
                    }
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading check information");
                e.Cancel = true;
            }
        }

        private void XrLabel_check_id_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            DevExpress.XtraReports.UI.XRLabel lbl = (DevExpress.XtraReports.UI.XRLabel)sender;
            DevExpress.XtraReports.UI.XtraReport rpt = (DevExpress.XtraReports.UI.XtraReport)lbl.Report;

            // Find the fields for the current row
            string tran_type = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue("tran_type")).ToUpper();
            System.Int32 TrustRegister = DebtPlus.Utils.Nulls.DInt(rpt.GetCurrentColumnValue("trust_register"));

            // Client refund checks have the client ID
            if (tran_type.StartsWith("CL"))
            {
                System.Int32 Client = DebtPlus.Utils.Nulls.DInt(rpt.GetCurrentColumnValue("client"));
                if (Client >= 0)
                {
                    lbl.Text = string.Format("{0:0000000}-{1:f0}", Client, TrustRegister);
                }
            }

            // Creditor checks have the creditor ID
            else if (tran_type.StartsWith("CR") || tran_type.StartsWith("NV"))
            {
                string Creditor = System.String.Empty;
                if ((!object.ReferenceEquals(rpt.GetCurrentColumnValue("creditor"), System.DBNull.Value)))
                {
                    Creditor = System.Convert.ToString(GetCurrentColumnValue("creditor")).Trim();
                }
                lbl.Text = string.Format("{0}-{1:f0}", Creditor, TrustRegister);
            }
            else
            {
                // Other unknown checks do not have either a client or creditor
                lbl.Text = TrustRegister.ToString();
            }
        }

        private void XrLabel_verbose_amount_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            DevExpress.XtraReports.UI.XRLabel lbl = (DevExpress.XtraReports.UI.XRLabel)sender;
            DevExpress.XtraReports.UI.XtraReport rpt = (DevExpress.XtraReports.UI.XtraReport)lbl.Report;

            // Retrieve the dollar amount of the check
            decimal Amount = 0;
            if ((!object.ReferenceEquals(rpt.GetCurrentColumnValue("amount"), System.DBNull.Value)))
            {
                Amount = System.Convert.ToDecimal(rpt.GetCurrentColumnValue("amount"));
            }

            // Update the amount field
            lbl.Text = ToWords(Amount);
        }

        private void XrLabel_amount_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            DevExpress.XtraReports.UI.XRLabel lbl = (DevExpress.XtraReports.UI.XRLabel)sender;
            DevExpress.XtraReports.UI.XtraReport rpt = (DevExpress.XtraReports.UI.XtraReport)lbl.Report;

            // Find the current amount field
            decimal CheckAmount = DebtPlus.Utils.Nulls.DDec(rpt.GetCurrentColumnValue("amount"));

            // Format the number with leading asterisk characters
            lbl.Text = CheckAmount.ToString("0.00").PadLeft(14, '*');
        }

        private void XrLabel_micr_code_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            DevExpress.XtraReports.UI.XRLabel lbl = (DevExpress.XtraReports.UI.XRLabel)sender;
            DevExpress.XtraReports.UI.XtraReport rpt = (DevExpress.XtraReports.UI.XtraReport)lbl.Report;
            System.Int32 Bank = System.Convert.ToInt32(rpt.Parameters["ParameterBank"].Value);

            // Find the check amount from the check
            long cents_amount = 0L;
            decimal CheckAmount = DebtPlus.Utils.Nulls.DDec(rpt.GetCurrentColumnValue("amount"));
            cents_amount = System.Convert.ToInt64(CheckAmount * 100m);

            // Find the check number
            string checknum = string.Empty;
            if ((!object.ReferenceEquals(rpt.GetCurrentColumnValue("checknum"), System.DBNull.Value)))
            {
                checknum = System.Convert.ToString(rpt.GetCurrentColumnValue("checknum"));
            }

            // Find the information about the current bank account
            System.Data.DataRow row = ds.Tables["banks"].Rows.Find(Bank);
            string aba = DebtPlus.Utils.Nulls.DStr(row["aba"]);
            string account_number = DebtPlus.Utils.Nulls.DStr(row["account_number"]);

            // Generate the output text line
            lbl.Text = string.Format("A{0:000000000}B{1}C{2:00000000}D", new object[] { aba, account_number, checknum, cents_amount });
        }

        private void XrPanel_address_block_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            DevExpress.XtraReports.UI.XRPanel pannel = (DevExpress.XtraReports.UI.XRPanel)sender;
            DevExpress.XtraReports.UI.XtraReport rpt = (DevExpress.XtraReports.UI.XtraReport)pannel.Report;
            DevExpress.XtraReports.UI.XtraReport masterRpt = rpt;
            DebtPlus.LINQ.SQLInfoClass sqlInfo = DebtPlus.LINQ.SQLInfoClass.getDefault();

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            string postalcode = string.Empty;

            // These are defaulted should the result be null
            System.Int32 TrustRegister = DebtPlus.Utils.Nulls.DInt(rpt.GetCurrentColumnValue("trust_register"));
            if (TrustRegister > 0)
            {
                System.Data.DataRow row = null;

                // Try to find this address in the list of known items that we have seen before
                System.Data.DataTable tbl = ds.Tables["addresses"];
                if (tbl != null)
                {
                    row = tbl.Rows.Find(TrustRegister);
                }

                // Read the address from the database
                if (row == null)
                {
                    using (var cn = new System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString))
                    {
                        cn.Open();
                        using (var cmd = new System.Data.SqlClient.SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.CommandText = "rpt_PrintChecks_Address";
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;

                            // Ask the system for the parameter name. Use the first paramter (parameter 0 is the return value)
                            System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd);
                            cmd.Parameters[1].Value = TrustRegister;

                            using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                            {
                                da.Fill(ds, "addresses");
                                tbl = ds.Tables["addresses"];

                                if (tbl.PrimaryKey.GetUpperBound(0) < 0)
                                {
                                    tbl.PrimaryKey = new System.Data.DataColumn[] { tbl.Columns["trust_register"] };
                                }
                            }
                        }
                    }

                    // Return the row to the caller
                    row = tbl.Rows.Find(TrustRegister);
                }

                // Retrieve the information from the datarow item
                if (row != null)
                {
                    string line_attn = string.Empty;
                    string line_1 = string.Empty;
                    string line_2 = string.Empty;
                    string line_3 = string.Empty;
                    string line_4 = string.Empty;
                    string line_5 = string.Empty;
                    string line_6 = string.Empty;
                    string line_7 = string.Empty;

                    // Extract the fields from the result set. The stored procedure changed between systems so we need to do it this way.
                    for (System.Int32 FldNo = 0; FldNo <= row.Table.Columns.Count - 1; FldNo++)
                    {
                        if (!object.ReferenceEquals(row[FldNo], System.DBNull.Value))
                        {
                            switch (row.Table.Columns[FldNo].ColumnName.ToLower())
                            {
                                case "postalcode":
                                case "zipcode":
                                    postalcode = DebtPlus.Utils.Format.Strings.DigitsOnly(System.Convert.ToString(row[FldNo]).Trim());
                                    break;

                                case "attn":
                                    line_attn = System.Convert.ToString(row[FldNo]).Trim();
                                    break;

                                case "addr_1":
                                case "line1":
                                    line_1 = System.Convert.ToString(row[FldNo]).Trim();
                                    break;

                                case "addr_2":
                                case "line2":
                                    line_2 = System.Convert.ToString(row[FldNo]).Trim();
                                    break;

                                case "addr_3":
                                case "line3":
                                    line_3 = System.Convert.ToString(row[FldNo]).Trim();
                                    break;

                                case "addr_4":
                                case "line4":
                                    line_4 = System.Convert.ToString(row[FldNo]).Trim();
                                    break;

                                case "addr_5":
                                case "line5":
                                    line_5 = System.Convert.ToString(row[FldNo]).Trim();
                                    break;

                                case "addr_6":
                                case "line6":
                                    line_6 = System.Convert.ToString(row[FldNo]).Trim();
                                    break;

                                case "addr_7":
                                case "line7":
                                    line_7 = System.Convert.ToString(row[FldNo]).Trim();
                                    break;
                            }
                        }
                    }

                    // Build the address block in the proper line sequence
                    foreach (string TempString in new string[] { line_attn, line_1, line_2, line_3, line_4, line_5, line_6, line_7 })
                    {
                        if (!string.IsNullOrEmpty(TempString))
                        {
                            sb.AppendFormat("\r\n{0}", TempString);
                        }
                    }

                    // Discard the leading cr/lf sequence
                    if (sb.Length > 0)
                    {
                        sb.Remove(0, 2);
                    }
                }

                // Update the fields with the new string
                DevExpress.XtraReports.UI.XRControl ctl = pannel.FindControl("XrBarCode1", true);
                if (ctl != null)
                {
                    ctl.Text = postalcode;
                }

                ctl = pannel.FindControl("XrLabel_address", true);
                if (ctl != null)
                {
                    ctl.Text = sb.ToString();
                }
            }
        }

        private void XrSubreport_ClientVoucher_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            DevExpress.XtraReports.UI.XRSubreport xrSub = (DevExpress.XtraReports.UI.XRSubreport)sender;
            DevExpress.XtraReports.UI.XtraReport rpt = (DevExpress.XtraReports.UI.XtraReport)xrSub.Report;
            string tran_type = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue("tran_type"));
            if (!tran_type.StartsWith("CL"))
            {
                e.Cancel = true;
            }
        }

        private void XrSubreport_CreditorVoucher_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            DevExpress.XtraReports.UI.XRSubreport xrSub = (DevExpress.XtraReports.UI.XRSubreport)sender;
            DevExpress.XtraReports.UI.XtraReport rpt = (DevExpress.XtraReports.UI.XtraReport)xrSub.Report;
            DevExpress.XtraReports.UI.XtraReport SubRpt = (DevExpress.XtraReports.UI.XtraReport)xrSub.ReportSource;

            string tran_type = DebtPlus.Utils.Nulls.DStr(rpt.GetCurrentColumnValue("tran_type"));

            DevExpress.XtraReports.UI.Band Band = SubRpt.Bands["Detail"];
            switch (tran_type)
            {
                case "CR":
                case "CR1":
                    // Use normal single line spacing
                    Band.HeightF = 16f;
                    break;

                case "CR2":
                    // Make the detail lines twice as high for double spacing
                    Band.HeightF = 32f;
                    break;

                default:
                    e.Cancel = true;
                    break;
            }
        }

        private void XrPictureBox_signature_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            DevExpress.XtraReports.UI.XRPictureBox pict = (DevExpress.XtraReports.UI.XRPictureBox)sender;
            System.Int32 LastBank = -1;
            if (pict.Tag != null && !object.ReferenceEquals(pict.Tag, System.DBNull.Value))
            {
                if (pict.Tag is string)
                {
                    System.Int32.TryParse(System.Convert.ToString(pict.Tag), out LastBank);
                }
                else
                {
                    LastBank = System.Convert.ToInt32(pict.Tag);
                }
            }

            DevExpress.XtraReports.UI.XtraReport rpt = (DevExpress.XtraReports.UI.XtraReport)pict.Report;
            System.Int32 NewBank = System.Convert.ToInt32(rpt.Parameters["ParameterBank"].Value);

            // If the bank has changed then load the signature information
            if (NewBank != LastBank)
            {
                pict.Tag = NewBank;
                System.Drawing.Image signature = ReadNewSignature(NewBank);

                // If unable to read the new one, try the old one.
                if (signature == null)
                {
                    signature = new System.Drawing.Bitmap(1, 1);
                }
                pict.Image = signature;
            }
        }

        #region "toWords"
        private const System.Int32 MAX_LEN = 80;
        private const System.Int32 CENTS_LEN = 19;
        private const System.Int32 MAX_DOLLARS = MAX_LEN - CENTS_LEN;

        private void SplitAmount(decimal Amount, ref System.Int64 Dollars, ref System.Int32 Cents)
        {
            // Split the amount into whole dollars and cents
            string TempString = System.String.Format("{0:00000000000000000000000.00}", System.Math.Floor(Amount * 100m) / 100m);

            // This is a bit of a hack. I would like to use "floor" but it does not work here. So, do it the hard way.
            string[] Parts = TempString.Split('.');
            Dollars = System.Int64.Parse(Parts[0]);
            Cents = System.Int32.Parse(Parts[1]);
        }

        private string ToWords(decimal Amount)
        {
            return ToWords(Amount, MAX_LEN);
        }

        private string ToWords(decimal Amount, System.Int32 MaxLength)
        {
            string answer = null;
            string CentsString = null;

            // Split the number into dollars and cents.
            System.Int64 Dollars = default(System.Int64);
            System.Int32 Cents = default(System.Int32);
            SplitAmount(Amount, ref Dollars, ref Cents);

            // Assume standard formatting for the cents string
            CentsString = System.String.Format(" AND {0:00}/100 DOLLARS", Cents);

            // If there are no dollars then use the term "zero" otherwise, convert it to a string
            if (Dollars == 0L)
            {
                answer = "ZERO" + CentsString;
            }
            else
            {
                answer = Format_Text_Numbers(Dollars) + CentsString;
            }

            // If the resulting string is too long then shorten it.
            if (MaxLength > 0 && answer.Length > MaxLength)
            {
                answer = string.Format("***{0:f0}*** DOLLARS AND ***{1:00}*** CENTS", Dollars, Cents);
            }

            return answer;
        }

        /// <summary>
        /// This class is used to translate specific values to their corresponding text string
        /// </summary>
        private class numberList
        {
            public System.Int64 Value { get; set; }
            public string text { get; set; }
            public numberList(System.Int64 Value, string text)
            {
                this.Value = Value;
                this.text = text;
            }
        }

        // This is a list of the specific values to be translated
        private System.Collections.Generic.List<numberList> numberCollection = new System.Collections.Generic.List<numberList>(new numberList[]
        {
            new numberList(0L,  ""),
            new numberList(1L,  "ONE"),
            new numberList(2L,  "TWO"),
            new numberList(3L,  "THREE"),
            new numberList(4L,  "FOUR"),
            new numberList(5L,  "FIVE"),
            new numberList(6L,  "SIX"),
            new numberList(7L,  "SEVEN"),
            new numberList(8L,  "EIGHT"),
            new numberList(9L,  "NINE"),
            new numberList(10L, "TEN"),
            new numberList(11L, "ELEVEN"),
            new numberList(12L, "TWELVE"),
            new numberList(13L, "THIRTEEN"),
            new numberList(14L, "FOURTEEN"),
            new numberList(15L, "FIFTEEN"),
            new numberList(16L, "SIXTEEN"),
            new numberList(17L, "SEVENTEEN"),
            new numberList(18L, "EIGHTEEN"),
            new numberList(19L, "NINETEEN"),
            new numberList(20L, "TWENTY"),
            new numberList(30L, "THIRTY"),
            new numberList(40L, "FORTY"),
            new numberList(50L, "FIFTY"),
            new numberList(60L, "SIXTY"),
            new numberList(70L, "SEVENTY"),
            new numberList(80L, "EIGHTY"),
            new numberList(90L, "NINETY")
        });

        /// <summary>
        /// Return the text for the corresponding input number.
        /// </summary>
        /// <param name="Amount">The whole amount. Fractions are handled separately.</param>
        /// <returns></returns>
        private string Format_Text_Numbers(System.Int64 Amount)
        {
            // Look for a specific amount and use the corresponding text string
            var q = numberCollection.Find(s => s.Value == Amount);
            if (q != null)
            {
                return q.text;
            }

            string Result = null;
            System.Int64 Quotient = 0L;
            System.Int64 Remainder = 0;

            // Handle amounts greater than 1 million dollars
            if (Amount >= 1000000L)
            {
                Quotient = System.Math.DivRem(Amount, 1000000L, out Remainder);

                Result = Format_Text_Numbers(Quotient) + " MILLION";
                string Extra = Format_Text_Numbers(Remainder);
                if (Extra != string.Empty)
                {
                    Result = string.Format("{0}, {1}", Result, Extra);
                }

                if (Result.Length > MAX_DOLLARS)
                {
                    return string.Format("{0:f0} MILLION", Quotient);
                }

                return Result.Trim();
            }

            // Handle amounts greater than 1 thousand dollars
            if (Amount >= 1000L)
            {
                Quotient = System.Math.DivRem(Amount, 1000L, out Remainder);

                Result = Format_Text_Numbers(Quotient) + " THOUSAND";
                string Extra = Format_Text_Numbers(Remainder);
                if (Extra != string.Empty)
                {
                    Result = string.Format("{0}, {1}", Result, Extra);
                }

                if (Result.Length > MAX_DOLLARS)
                {
                    return string.Format("{0:f0} THOUSAND", Quotient);
                }
                return Result.Trim();
            }

            // Handle amounts greater than 100 dollars
            if (Amount >= 100L)
            {
                Quotient = System.Math.DivRem(Amount, 100L, out Remainder);

                Result = Format_Text_Numbers(Quotient) + " HUNDRED";
                string Extra = Format_Text_Numbers(Remainder);
                if (Extra != string.Empty)
                {
                    Result = string.Format("{0}, {1}", Result, Extra);
                }

                if (Result.Length > MAX_DOLLARS)
                {
                    return string.Format("{0:f0} HUNDRED", Quotient);
                }
                return Result.Trim();
            }

            // Handle amounts less than 100 dollars (but greater than 20 dollars)
            Quotient = System.Math.DivRem(Amount, 10L, out Remainder);
            return string.Format("{0} {1}", Format_Text_Numbers(Quotient * 10L), Format_Text_Numbers(Remainder)).Trim();
        }
        #endregion

        #region "Signature Information"
        private System.Drawing.Image ReadNewSignature(System.Int32 Bank)
        {
            try
            {
                DebtPlus.LINQ.SQLInfoClass sqlInfo = DebtPlus.LINQ.SQLInfoClass.getDefault();
                using (var cn = new System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString))
                {
                    cn.Open();
                    using (var cmd = new System.Data.SqlClient.SqlCommand())
                    {
                        cmd.Connection = cn;
                        cmd.CommandText = "SELECT SignatureImage FROM banks WHERE bank = @bank";
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.Parameters.Add("@bank", System.Data.SqlDbType.Int).Value = Bank;

                        using (var rd = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection | System.Data.CommandBehavior.SequentialAccess))
                        {
                            if (rd.Read() && !rd.IsDBNull(0))
                            {
                                using (var memStream = new System.IO.MemoryStream())
                                {
                                    byte[] ab = new byte[4097];
                                    long ByteCount = 0;
                                    System.Int32 ByteOffset = 0;

                                    ByteCount = rd.GetBytes(0, 0, ab, 0, 4096);
                                    while (ByteCount == 4096)
                                    {
                                        memStream.Write(ab, 0, 4096);
                                        ByteOffset += 4096;
                                        ByteCount = rd.GetBytes(0, ByteOffset, ab, 0, 4096);
                                    }

                                    if (ByteCount > 0)
                                    {
                                        memStream.Write(ab, 0, System.Convert.ToInt32(ByteCount));
                                        ByteOffset += System.Convert.ToInt32(ByteCount);
                                    }

                                    // Convert the byte buffer to an image
                                    memStream.Seek(0, System.IO.SeekOrigin.Begin);
                                    return System.Drawing.Image.FromStream(memStream);
                                }
                            }
                        }
                    }
                }
            }

            catch (System.Exception ex)
            {
                DebtPlus.Data.Forms.MessageBox.Show(ex.Message, "Error reading signature");
            }

            return null;
        }
        #endregion
    }
}