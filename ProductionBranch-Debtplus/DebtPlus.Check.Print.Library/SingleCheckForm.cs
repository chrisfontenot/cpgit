#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.Utils;
using System.Data.SqlClient;
using System;
using DebtPlus.LINQ;
using System.Linq;

namespace DebtPlus.Check.Print.Library
{
    internal partial class SingleCheckForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        protected DebtPlus.Check.Print.Library.PrintChecksClass ParentClass = null;

        //------------------------------------------------------------------------------------------------------
        //          Trust register information about the check                                              --
        //------------------------------------------------------------------------------------------------------
        protected internal System.Int32 Parameter_TrustRegister = -1;
        protected internal System.Int32 bankID;

        public SingleCheckForm() : base()
        {
            InitializeComponent();
        }

        public SingleCheckForm(PrintChecksClass Parent, System.Int32 TrustRegister) : this()
        {
            ParentClass = Parent;
            Parameter_TrustRegister = TrustRegister;
            RegisterHandlers();

            // Prevent the form from closing normally until the OK button is pressed.
            FormClosing += SingleCheckForm_FormClosing;
        }

        private void RegisterHandlers()
        {
            Load += SingleCheckForm_Load;
            Button_Print.Click += Button_Print_Click;
        }

        private void UnRegisterHandlers()
        {
            Load -= SingleCheckForm_Load;
            Button_Print.Click -= Button_Print_Click;
        }

        /// <summary>
        /// Routine to handle the closing of the form.
        /// </summary>
        private void SingleCheckForm_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            // Do not hold up a system restart with this form.
            if (e.CloseReason == System.Windows.Forms.CloseReason.UserClosing)
            {
                // Ask the user if they really want to cancel this form.
                if (DebtPlus.Data.Forms.MessageBox.Show("If you close this form the check will be\r\nincluded in the next check print run.\r\n\r\nIs this what you want to do?", "Are you sure?", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Information, System.Windows.Forms.MessageBoxDefaultButton.Button2) != System.Windows.Forms.DialogResult.Yes)
                {
                    e.Cancel = true;
                }
            }
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        internal System.Windows.Forms.Panel Panel1;
        protected DevExpress.XtraEditors.SimpleButton Button_Print;
        protected DevExpress.XtraEditors.SimpleButton Button_Cancel;
        protected System.Windows.Forms.PictureBox PictureBox_Signature;
        protected DevExpress.XtraEditors.LabelControl Label5;
        protected DevExpress.XtraEditors.LabelControl address;
        protected DevExpress.XtraEditors.LabelControl check_amount;
        protected DevExpress.XtraEditors.LabelControl check_id;
        protected internal DevExpress.XtraEditors.TextEdit checknum;
        protected internal DevExpress.XtraEditors.DateEdit check_date;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(SingleCheckForm));
            this.Panel1 = new System.Windows.Forms.Panel();
            this.check_date = new DevExpress.XtraEditors.DateEdit();
            this.checknum = new DevExpress.XtraEditors.TextEdit();
            this.Label5 = new DevExpress.XtraEditors.LabelControl();
            this.address = new DevExpress.XtraEditors.LabelControl();
            this.PictureBox_Signature = new System.Windows.Forms.PictureBox();
            this.check_amount = new DevExpress.XtraEditors.LabelControl();
            this.check_id = new DevExpress.XtraEditors.LabelControl();
            this.Button_Print = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();

            this.Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.check_date.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.checknum.Properties).BeginInit();
            this.SuspendLayout();

            //
            //Panel1
            //
            this.Panel1.BackColor = System.Drawing.Color.Aquamarine;
            this.Panel1.Controls.Add(this.check_date);
            this.Panel1.Controls.Add(this.checknum);
            this.Panel1.Controls.Add(this.Label5);
            this.Panel1.Controls.Add(this.address);
            this.Panel1.Controls.Add(this.PictureBox_Signature);
            this.Panel1.Controls.Add(this.check_amount);
            this.Panel1.Controls.Add(this.check_id);
            this.Panel1.Location = new System.Drawing.Point(0, 0);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(368, 215);
            this.ToolTipController1.SetSuperTip(this.Panel1, null);
            this.Panel1.TabIndex = 0;
            //
            //check_date
            //
            this.check_date.EditValue = null;
            this.check_date.Location = new System.Drawing.Point(264, 59);
            this.check_date.Name = "check_date";
            //
            //check_date.Properties
            //
            this.check_date.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.check_date.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.check_date.Size = new System.Drawing.Size(88, 20);
            this.check_date.TabIndex = 4;
            this.check_date.ToolTip = "Enter the date for the check.";
            //
            //checknum
            //
            this.checknum.EditValue = "";
            this.checknum.Location = new System.Drawing.Point(240, 15);
            this.checknum.Name = "checknum";
            //
            //checknum.Properties
            //
            this.checknum.Properties.Appearance.Options.UseTextOptions = true;
            this.checknum.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.checknum.Properties.DisplayFormat.FormatString = "d0";
            this.checknum.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.checknum.Properties.EditFormat.FormatString = "d0";
            this.checknum.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.checknum.Properties.Mask.BeepOnError = true;
            this.checknum.Properties.Mask.EditMask = "f0";
            this.checknum.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.checknum.Size = new System.Drawing.Size(112, 20);
            this.checknum.TabIndex = 2;
            this.checknum.ToolTip = "Enter the new check number. Shown is the expected check number that is the next o" + "ne in sequence.";
            //
            //Label5
            //
            this.Label5.Location = new System.Drawing.Point(152, 17);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(88, 17);
            this.Label5.TabIndex = 1;
            this.Label5.Text = "Check &Number:";
            //
            //address
            //
            this.address.Location = new System.Drawing.Point(16, 103);
            this.address.Name = "address";
            this.address.Size = new System.Drawing.Size(224, 104);
            this.address.TabIndex = 5;
            this.address.Text = "Name and address Information";
            this.address.UseMnemonic = false;
            //
            //PictureBox_Signature
            //
            this.PictureBox_Signature.Image = (System.Drawing.Image)resources.GetObject("PictureBox_Signature.Image");
            this.PictureBox_Signature.Location = new System.Drawing.Point(248, 146);
            this.PictureBox_Signature.Name = "PictureBox_Signature";
            this.PictureBox_Signature.Size = new System.Drawing.Size(112, 30);
            this.ToolTipController1.SetSuperTip(this.PictureBox_Signature, null);
            this.PictureBox_Signature.TabIndex = 3;
            this.PictureBox_Signature.TabStop = false;
            //
            //check_amount
            //
            this.check_amount.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.check_amount.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            this.check_amount.Appearance.ForeColor = System.Drawing.Color.Crimson;
            this.check_amount.Appearance.Options.UseBackColor = true;
            this.check_amount.Appearance.Options.UseFont = true;
            this.check_amount.Appearance.Options.UseForeColor = true;
            this.check_amount.Appearance.Options.UseTextOptions = true;
            this.check_amount.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.check_amount.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.check_amount.Location = new System.Drawing.Point(8, 60);
            this.check_amount.Name = "check_amount";
            this.check_amount.ShowToolTips = false;
            this.check_amount.Size = new System.Drawing.Size(232, 20);
            this.check_amount.TabIndex = 3;
            this.check_amount.Text = "*XX* DOLLARS AND *XX* CENTS";
            //
            //check_id
            //
            this.check_id.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.check_id.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            this.check_id.Appearance.ForeColor = System.Drawing.Color.Crimson;
            this.check_id.Appearance.Options.UseBackColor = true;
            this.check_id.Appearance.Options.UseFont = true;
            this.check_id.Appearance.Options.UseForeColor = true;
            this.check_id.Location = new System.Drawing.Point(8, 17);
            this.check_id.Name = "check_id";
            this.check_id.Size = new System.Drawing.Size(136, 17);
            this.check_id.TabIndex = 0;
            this.check_id.Text = "XXXXX-XXXXXXX";
            //
            //Button_Print
            //
            this.Button_Print.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.Button_Print.Location = new System.Drawing.Point(376, 17);
            this.Button_Print.Name = "Button_Print";
            this.Button_Print.Size = new System.Drawing.Size(72, 26);
            this.Button_Print.TabIndex = 1;
            this.Button_Print.Text = "Record";
            this.Button_Print.ToolTip = "Press this button to print the check with these values";
            this.Button_Print.ToolTipController = this.ToolTipController1;
            //
            //Button_Cancel
            //
            this.Button_Cancel.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(376, 60);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(72, 26);
            this.Button_Cancel.TabIndex = 2;
            this.Button_Cancel.Text = "Cancel";
            this.Button_Cancel.ToolTip = "Press this button to cancel the print at this time. The check is left in the queu" + "e to be printed later.";
            this.Button_Cancel.ToolTipController = this.ToolTipController1;
            //
            //SingleCheckForm
            //
            this.AcceptButton = this.Button_Print;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(458, 215);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_Print);
            this.Controls.Add(this.Panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SingleCheckForm";
            this.ToolTipController1.SetSuperTip(this, null);
            this.Text = "Record Single Check";

            this.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)this.check_date.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.checknum.Properties).EndInit();
            this.ResumeLayout(false);
        }

        #endregion

        protected void SingleCheckForm_Load(object sender, System.EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                ReloadForm(true);
            }
            finally
            {
                RegisterHandlers();
            }
        }

        protected void ReloadForm()
        {
            ReloadForm(false);
        }

        protected void ReloadForm(bool includeCheckNumber)
        {
            try
            {
                using (var bc = new BusinessContext())
                {
                    var trustRecord = bc.registers_trusts.Where(s => s.Id == Parameter_TrustRegister).FirstOrDefault();
                    if (trustRecord != null)
                    {
                        FormatTrustRegister(includeCheckNumber, bc, trustRecord);
                    }
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading trust reference");
            }
        }

        /// <summary>
        /// Format the trust register into the display check image
        /// </summary>
        /// <param name="trustRecord"></param>
        protected void FormatTrustRegister(bool includeCheckNumber, registers_trust trustRecord)
        {
            using (var bc = new BusinessContext())
            {
                FormatTrustRegister(includeCheckNumber, bc, trustRecord);
            }
        }

        /// <summary>
        /// Format the trust register into the display check image
        /// </summary>
        /// <param name="includeCheckNumber">TRUE if the check number is to be shown</param>
        /// <param name="bc"></param>
        /// <param name="trustRecord"></param>
        protected void FormatTrustRegister(bool includeCheckNumber, BusinessContext bc, registers_trust trustRecord)
        {
            // Update the information corresponding to the trust record
            if ((trustRecord.client.HasValue) && (trustRecord.tran_type == "CR" || trustRecord.tran_type == "AR"))
            {
                check_id.Text = string.Format("{0:0000000}", trustRecord.client) + "-" + trustRecord.Id.ToString();
                address.Text = ReadClientNameAddress(bc, trustRecord.client.Value);
            }

            // Update the creditor information
            if (! string.IsNullOrEmpty(trustRecord.creditor) && (trustRecord.tran_type != "CR" && trustRecord.tran_type != "AR"))
            {
                check_id.Text = trustRecord.creditor + "-" + trustRecord.Id.ToString();
                address.Text = ReadCreditorNameAndAddress(bc, trustRecord.creditor);
            }

            if (includeCheckNumber)
            {
                // Find the default bank if there is one missing
                if (trustRecord.bank < 1)
                {
                    trustRecord.bank = DebtPlus.LINQ.Cache.bank.getList().Find(s => s.Default && s.type == "C").Id;
                }

                // Find the check number to be used on the check when it is printed
                if (trustRecord.checknum.GetValueOrDefault(0L) < 1L && trustRecord.bank > 0)
                {
                    trustRecord.checknum = bc.banks.Where(s => s.Id == trustRecord.bank).Select(s => s.checknum).FirstOrDefault();
                }

                // Default the check number if there still is not one present.
                if (! trustRecord.checknum.HasValue)
                {
                    trustRecord.checknum = 101L;
                }

                // Set the check number into the display
                checknum.EditValue = trustRecord.checknum;
            }
            checknum.Properties.ReadOnly = false;

            // Save the bank ID for the closing routine
            bankID = trustRecord.bank;

            // Set the date into the editing control
            check_date.DateTime = trustRecord.date_created;

            // The amount is a bit more tricky. We need to split the values without rounding.
            string temp = string.Format("{0:0.0000}", trustRecord.amount);
            string[] splitTemp = temp.Split('.');
            System.Int64 dollars = Int64.Parse(splitTemp[0]);
            check_amount.Text = string.Format("*{0:n0}* DOLLARS AND *{1}* CENTS", dollars, splitTemp[1].PadRight(2, '0').Substring(0,2));
        }

        protected string ReadCreditorNameAndAddress(BusinessContext bc, string CreditorID)
        {
            try
            {
                // Try to use the payment address for the creditor
                var q = (from ca in bc.creditor_addresses
                         join adr in bc.addresses on ca.AddressID equals adr.Id
                         where ca.creditor == CreditorID && ca.type == "P"
                         select adr).FirstOrDefault();

                if (q != null)
                {
                    return q.ToString();
                }

                // Look in the creditor record for the creditor name
                var creditorName = bc.creditors.Where(s => s.Id == CreditorID).Select(s => s.creditor_name).FirstOrDefault();
                return creditorName ?? string.Empty;
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading creditor name/address");
            }

            return string.Empty;
        }

        protected string ReadClientNameAddress(BusinessContext bc, Int32 ClientID)
        {
            if (ClientID > 0)
            {
                try
                {
                    var nameString = ReadClientName(bc, ClientID);
                    var addrString = ReadClientAddress(bc, ClientID);
                    if (!string.IsNullOrEmpty(nameString) && !string.IsNullOrEmpty(addrString))
                    {
                        return nameString + "\r\n" + addrString;
                    }

                    return nameString + addrString;
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading client name/address");
                }
            }
            return string.Empty;
        }

        private string ReadClientName(BusinessContext bc, Int32 ClientID)
        {
            var name = (from p in bc.peoples
                        join n in bc.Names on p.NameID equals n.Id
                        where p.Client == ClientID && p.Relation == 1
                        select n).FirstOrDefault();
            if (name != null)
            {
                return name.ToString();
            }
            return string.Empty;
        }

        private string ReadClientAddress(BusinessContext bc, Int32 ClientID)
        {
            var addr = (from cl in bc.clients
                        join a in bc.addresses on cl.AddressID equals a.Id
                        where cl.Id == ClientID
                        select a).FirstOrDefault();

            if (addr != null)
            {
                return addr.ToString();
            }
            return string.Empty;
        }

        protected virtual void Button_Print_Click(object sender, System.EventArgs e)
        {
            // Ignore the print button if the dialog is read-only.
            if (checknum.Properties.ReadOnly)
            {
                DialogResult = System.Windows.Forms.DialogResult.OK;
                return;
            }

            // Obtain the values in the proper form for the update statement.
            System.Int64 Newchecknum = Convert.ToInt64(checknum.EditValue);
            string NewCheckDate      = Convert.ToDateTime(check_date.EditValue).ToShortDateString();

            try
            {
                using (var bc = new BusinessContext())
                {
                    // This statement must be done differently and directly. It changes fields that are normally read-only to us.
                    string updateStatement = string.Format("UPDATE [registers_trust] SET [date_created]='{0} 00:00:00',[cleared]=' ',[checknum]={1:f0} WHERE [trust_register]={2:f0};",
                                                            NewCheckDate,
                                                            Newchecknum,
                                                            Parameter_TrustRegister);
                    bc.ExecuteCommand(updateStatement);

                    // If the check number is the same as the next available number then increment value
                    var bankRecord = bc.banks.Where(s => s.Id == bankID).FirstOrDefault();
                    if (bankRecord != null && bankRecord.checknum == Newchecknum)
                    {
                        bankRecord.checknum += 1L;
                        bc.SubmitChanges();
                    }

                    // All is good. Complete the dialog normally.
                    SetDialogResult(System.Windows.Forms.DialogResult.OK);
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        protected void SetDialogResult(System.Windows.Forms.DialogResult resultCode)
        {
            // Set the dialog result code
            DialogResult = resultCode;

            // Allow the form to close normally
            if (resultCode != System.Windows.Forms.DialogResult.None)
            {
                FormClosing -= SingleCheckForm_FormClosing;
            }
        }
    }
}
