﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.Check.Print.Library
{
    partial class BusyStatusForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl_payee = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl_amount = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl_checknum = new DevExpress.XtraEditors.LabelControl();
            this.ProgressBarControl1 = new DevExpress.XtraEditors.ProgressBarControl();
            this.LabelControl_Start = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl_End = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.ProgressBarControl1.Properties).BeginInit();
            this.SuspendLayout();
            //
            //LabelControl1
            //
            this.LabelControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl1.Location = new System.Drawing.Point(13, 13);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(267, 32);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "The system is busy formatting the check images. Please be patient. It will be fin" + "ished shortly.";
            this.LabelControl1.UseWaitCursor = true;
            //
            //LabelControl2
            //
            this.LabelControl2.Location = new System.Drawing.Point(13, 52);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(73, 13);
            this.LabelControl2.TabIndex = 2;
            this.LabelControl2.Text = "Check Number:";
            this.LabelControl2.UseWaitCursor = true;
            //
            //LabelControl3
            //
            this.LabelControl3.Location = new System.Drawing.Point(13, 71);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(41, 13);
            this.LabelControl3.TabIndex = 3;
            this.LabelControl3.Text = "Amount:";
            this.LabelControl3.UseWaitCursor = true;
            //
            //LabelControl4
            //
            this.LabelControl4.Location = new System.Drawing.Point(13, 90);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(34, 13);
            this.LabelControl4.TabIndex = 4;
            this.LabelControl4.Text = "Payee:";
            this.LabelControl4.UseWaitCursor = true;
            //
            //LabelControl_payee
            //
            this.LabelControl_payee.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Character;
            this.LabelControl_payee.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.LabelControl_payee.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl_payee.Location = new System.Drawing.Point(92, 90);
            this.LabelControl_payee.Name = "LabelControl_payee";
            this.LabelControl_payee.Size = new System.Drawing.Size(188, 13);
            this.LabelControl_payee.TabIndex = 7;
            this.LabelControl_payee.UseWaitCursor = true;
            //
            //LabelControl_amount
            //
            this.LabelControl_amount.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Character;
            this.LabelControl_amount.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.LabelControl_amount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl_amount.Location = new System.Drawing.Point(92, 71);
            this.LabelControl_amount.Name = "LabelControl_amount";
            this.LabelControl_amount.Size = new System.Drawing.Size(188, 13);
            this.LabelControl_amount.TabIndex = 6;
            this.LabelControl_amount.UseWaitCursor = true;
            //
            //LabelControl_checknum
            //
            this.LabelControl_checknum.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Character;
            this.LabelControl_checknum.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.LabelControl_checknum.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl_checknum.Location = new System.Drawing.Point(92, 52);
            this.LabelControl_checknum.Name = "LabelControl_checknum";
            this.LabelControl_checknum.Size = new System.Drawing.Size(188, 13);
            this.LabelControl_checknum.TabIndex = 5;
            this.LabelControl_checknum.UseWaitCursor = true;
            //
            //ProgressBarControl1
            //
            this.ProgressBarControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.ProgressBarControl1.Location = new System.Drawing.Point(13, 138);
            this.ProgressBarControl1.Name = "ProgressBarControl1";
            this.ProgressBarControl1.Properties.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
            this.ProgressBarControl1.Size = new System.Drawing.Size(267, 18);
            this.ProgressBarControl1.TabIndex = 1;
            this.ProgressBarControl1.UseWaitCursor = true;
            //
            //LabelControl_Start
            //
            this.LabelControl_Start.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
            this.LabelControl_Start.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LabelControl_Start.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl_Start.Location = new System.Drawing.Point(15, 121);
            this.LabelControl_Start.Name = "LabelControl_Start";
            this.LabelControl_Start.Size = new System.Drawing.Size(69, 13);
            this.LabelControl_Start.TabIndex = 8;
            this.LabelControl_Start.Text = "0";
            //
            //LabelControl_End
            //
            this.LabelControl_End.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
            this.LabelControl_End.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.LabelControl_End.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl_End.Location = new System.Drawing.Point(211, 121);
            this.LabelControl_End.Name = "LabelControl_End";
            this.LabelControl_End.Size = new System.Drawing.Size(69, 13);
            this.LabelControl_End.TabIndex = 9;
            this.LabelControl_End.Text = "0";
            //
            //BusyStatusForm
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 168);
            this.CloseBox = false;
            this.Controls.Add(this.LabelControl_End);
            this.Controls.Add(this.LabelControl_Start);
            this.Controls.Add(this.LabelControl_payee);
            this.Controls.Add(this.LabelControl_amount);
            this.Controls.Add(this.LabelControl_checknum);
            this.Controls.Add(this.LabelControl4);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.LabelControl1);
            this.Controls.Add(this.ProgressBarControl1);
            this.LookAndFeel.UseDefaultLookAndFeel = true;
            this.Name = "BusyStatusForm";
            this.Text = "Formatting Checks";
            this.UseWaitCursor = true;
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.ProgressBarControl1.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        internal DevExpress.XtraEditors.LabelControl LabelControl1;
        internal DevExpress.XtraEditors.LabelControl LabelControl2;
        internal DevExpress.XtraEditors.LabelControl LabelControl3;
        internal DevExpress.XtraEditors.LabelControl LabelControl4;
        public DevExpress.XtraEditors.LabelControl LabelControl_payee;
        public DevExpress.XtraEditors.LabelControl LabelControl_amount;
        public DevExpress.XtraEditors.LabelControl LabelControl_checknum;
        public DevExpress.XtraEditors.ProgressBarControl ProgressBarControl1;
        public DevExpress.XtraEditors.LabelControl LabelControl_Start;
        public DevExpress.XtraEditors.LabelControl LabelControl_End;
    }
}