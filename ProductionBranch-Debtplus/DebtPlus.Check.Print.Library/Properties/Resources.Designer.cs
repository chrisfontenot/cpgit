﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DebtPlus.Check.Print.Library.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("DebtPlus.Check.Print.Library.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CAUTION: Are you really sure?.
        /// </summary>
        internal static string CAUTIONAreYouReallySure {
            get {
                return ResourceManager.GetString("CAUTIONAreYouReallySure", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error marking checks for selection.
        /// </summary>
        internal static string ErrorMarkingChecksForSelection {
            get {
                return ResourceManager.GetString("ErrorMarkingChecksForSelection", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error marking checks to be printed.
        /// </summary>
        internal static string ErrorMarkingChecksToBePrinted {
            get {
                return ResourceManager.GetString("ErrorMarkingChecksToBePrinted", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error reading banks table.
        /// </summary>
        internal static string ErrorReadingBanksTable {
            get {
                return ResourceManager.GetString("ErrorReadingBanksTable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error reading check list.
        /// </summary>
        internal static string ErrorReadingCheckList {
            get {
                return ResourceManager.GetString("ErrorReadingCheckList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error reading trust register.
        /// </summary>
        internal static string ErrorReadingTrustRegister {
            get {
                return ResourceManager.GetString("ErrorReadingTrustRegister", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error setting check number.
        /// </summary>
        internal static string ErrorSettingCheckNumber {
            get {
                return ResourceManager.GetString("ErrorSettingCheckNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error setting trust register printMarker.
        /// </summary>
        internal static string ErrorSettingTrustRegisterMarker {
            get {
                return ResourceManager.GetString("ErrorSettingTrustRegisterMarker", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error updating banks table with check number.
        /// </summary>
        internal static string ErrorUpdatingBanksTableWithCheckNumber {
            get {
                return ResourceManager.GetString("ErrorUpdatingBanksTableWithCheckNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error updating the check number.
        /// </summary>
        internal static string ErrorUpdatingTheCheckNumber {
            get {
                return ResourceManager.GetString("ErrorUpdatingTheCheckNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error voiding destoryed checks.
        /// </summary>
        internal static string ErrorVoidingDestoryedChecks {
            get {
                return ResourceManager.GetString("ErrorVoidingDestoryedChecks", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to It is a critical condition..
        /// </summary>
        internal static string ItIsACriticalCondition {
            get {
                return ResourceManager.GetString("ItIsACriticalCondition", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Normally, the checks should be reprinted before you terminate this application..
        /// </summary>
        internal static string NormallyTheChecksShouldBeReprintedBeforeYouTerminateThisApplication {
            get {
                return ResourceManager.GetString("NormallyTheChecksShouldBeReprintedBeforeYouTerminateThisApplication", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please talk to your supervisor before answering this question..
        /// </summary>
        internal static string PleaseTalkToYourSupervisorBeforeAnsweringThisQuestion {
            get {
                return ResourceManager.GetString("PleaseTalkToYourSupervisorBeforeAnsweringThisQuestion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to reprinted. Are you really sure that you want to leave these checks in this status?.
        /// </summary>
        internal static string ReprintedAreYouReallySureThatYouWantToLeaveTheseChecksInThisStatus {
            get {
                return ResourceManager.GetString("ReprintedAreYouReallySureThatYouWantToLeaveTheseChecksInThisStatus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The check number was changed by someone else. Please review the information and correct it if needed..
        /// </summary>
        internal static string TheCheckNumberWasChangedBySomeoneElsePleaseReviewTheInformationAndCorrectItIfNeeded {
            get {
                return ResourceManager.GetString("TheCheckNumberWasChangedBySomeoneElsePleaseReviewTheInformationAndCorrectItIfNeed" +
                        "ed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The checks are no longer pending to be printed. Please review the list again..
        /// </summary>
        internal static string TheChecksAreNoLongerPendingToBePrintedPleaseReviewTheListAgain {
            get {
                return ResourceManager.GetString("TheChecksAreNoLongerPendingToBePrintedPleaseReviewTheListAgain", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to WARNING!!!.
        /// </summary>
        internal static string WARNING {
            get {
                return ResourceManager.GetString("WARNING", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Was the check printed correctly.
        /// </summary>
        internal static string WasTheCheckPrintedCorrectly {
            get {
                return ResourceManager.GetString("WasTheCheckPrintedCorrectly", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You have {0:d} checks to print totalling {1:c2}.
        /// </summary>
        internal static string YouHave0DChecksToPrintTotalling1C2 {
            get {
                return ResourceManager.GetString("YouHave0DChecksToPrintTotalling1C2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You have no checks to print. Choose a new bank or press CANCEL..
        /// </summary>
        internal static string YouHaveNoChecksToPrintChooseANewBankOrPressCANCEL {
            get {
                return ResourceManager.GetString("YouHaveNoChecksToPrintChooseANewBankOrPressCANCEL", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You indicated that the checks were destroyed. There are now pending checks to be.
        /// </summary>
        internal static string YouIndicatedThatTheChecksWereDestroyedThereAreNowPendingChecksToBe {
            get {
                return ResourceManager.GetString("YouIndicatedThatTheChecksWereDestroyedThereAreNowPendingChecksToBe", resourceCulture);
            }
        }
    }
}
