#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.Common;
using System.Data.SqlClient;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.Check.Print.Library
{
    public partial class PrintChecksClass : System.IDisposable
    {
        // Valid transaction types for checks
        internal static string[] checkTranTypes = new string[] { "AD", "MD", "CM", "CR", "AR" };

        /// <summary>
        /// Class for the parameters to print a sequence of checks.
        /// </summary>
        internal class PrintParameters
        {
            public Int32 BankID { get; set; }
            public string MarkerString { get; set; }

            public PrintParameters()
            {
                BankID       = 0;
                MarkerString = null;
            }

            public PrintParameters(Int32 bankID, string MarkerString)
                : this()
            {
                this.BankID       = bankID;
                this.MarkerString = MarkerString;
            }

            public PrintParameters(string MarkerString)
                : this()
            {
                this.MarkerString = MarkerString;
            }
        }

        /// <summary>
        /// Class for the parameters to void a sequence of checks.
        /// </summary>
        internal class VoidParameters : PrintParameters
        {
            public Int64? FirstCheck { get; set; }
            public Int64? LastCheck { get; set; }
            public bool GenerateVoids { get; set; }

            public VoidParameters() : base()
            {
                FirstCheck    = null;
                LastCheck     = null;
                GenerateVoids = true;
            }

            public VoidParameters(string MarkerString)
                : this()
            {
                this.MarkerString = MarkerString;
            }

            public VoidParameters(string MarkerString, bool GenerateVoids)
                : this()
            {
                this.MarkerString  = MarkerString;
                this.GenerateVoids = GenerateVoids;
            }
        }

        internal enum ModeEnum
        {
            Single     = 1,
            First      = 2,
            Additional = 3
        }

        private BusyStatusForm _busyForm = null;

        /// <summary>
        /// Print a single check
        /// </summary>
        public void PrintSingleCheck(Int32 trustRegister)
        {
            using (PrintSingleCheckForm frm = new PrintSingleCheckForm(this, trustRegister))
            {
                frm.ShowDialog();
            }
        }

        /// <summary>
        /// Record a check with the pre-printed check number
        /// </summary>
        public void RecordSingleCheck(Int32 trustRegister)
        {
            using (SingleCheckForm frm = new SingleCheckForm(this, trustRegister))
            {
                frm.ShowDialog();
            }
        }

        /// <summary>
        /// Print all pending checks
        /// </summary>
        public void PrintAllChecks()
        {
            using (PrintAllForm frm = new PrintAllForm(this))
            {
                frm.ShowDialog();
            }
        }

        /// <summary>
        /// Void the printed range of checks
        /// </summary>
        internal void VoidCheck(VoidParameters param)
        {
            System.Exception ex;
            do
            {
                ex = ProcessVoidCheck(param);
                if (ex == null)
                {
                    return;
                }

                // Handle sql exceptions
                System.Data.SqlClient.SqlException sqlError = ex as System.Data.SqlClient.SqlException;
                if (sqlError == null || sqlError.ErrorCode != 1205)
                {
                    break;
                }
            } while (true);

            // Process the standard error logic on a non-retryable error condition
            DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error voiding check list");
        }

        /// <summary>
        /// Void the printed range of checks
        /// </summary>
        private System.Exception ProcessVoidCheck(VoidParameters param)
        {
            try
            {
                // Generate a transaction for the void operation. We want it all to work correctly or forget it all.
                string connectionString = DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString;
                using (var cn = new System.Data.SqlClient.SqlConnection(connectionString))
                {
                    cn.Open();
                    System.Data.SqlClient.SqlTransaction txn = cn.BeginTransaction(IsolationLevel.RepeatableRead);
                    using (var bc = new BusinessContext(cn))
                    {
                        bc.Transaction = txn;

                        // Generate a collection of the checks to be voided.
                        System.Collections.Generic.List<registers_trust> colVoidList;

                        // If there is no starting check then void all of the checks.
                        if (param.FirstCheck.GetValueOrDefault(0L) <= 0L)
                        {
                            colVoidList = (from tr in bc.registers_trusts
                                           where tr.cleared == ' ' && tr.sequence_number == param.MarkerString && checkTranTypes.Contains(tr.tran_type) && tr.bank == param.BankID
                                           select tr).ToList();
                        }

                        // If there is an ending check then do not include the ending check
                        else if (param.LastCheck.GetValueOrDefault(0L) > 0L)
                        {
                                colVoidList = (from tr in bc.registers_trusts
                                               where tr.cleared == ' ' && tr.sequence_number == param.MarkerString && checkTranTypes.Contains(tr.tran_type) && tr.bank == param.BankID
                                               && tr.checknum >= param.FirstCheck.Value
                                               && tr.checknum < param.LastCheck.Value
                                               select tr).ToList();
                        }

                        // There is a starting check but no ending check. Void starting at the beginning check and do the rest of the list.
                        else
                        {
                                colVoidList = (from tr in bc.registers_trusts
                                               where tr.cleared == ' ' && tr.sequence_number == param.MarkerString && checkTranTypes.Contains(tr.tran_type) && tr.bank == param.BankID
                                               && tr.checknum >= param.FirstCheck.Value
                                               select tr).ToList();
                        }

                        // If there are no items then we can not do anything
                        if (colVoidList.Count < 1)
                        {
                            try { txn.Rollback(); }
                            catch { }
                            return null;
                        }

                        // Do we want to create a destroyed check image for these items?
                        if (param.GenerateVoids)
                        {
                            foreach (registers_trust destroyedItem in colVoidList)
                            {
                                var newTr = DebtPlus.LINQ.Factory.Manufacture_registers_trust();

                                newTr.amount = destroyedItem.amount;
                                newTr.bank = destroyedItem.bank;
                                newTr.check_order = destroyedItem.check_order;
                                newTr.checknum = destroyedItem.checknum;
                                newTr.cleared = 'D';
                                newTr.client = destroyedItem.client;
                                newTr.reconciled_date = DateTime.Now;
                                newTr.tran_type = destroyedItem.tran_type;

                                bc.registers_trusts.InsertOnSubmit(newTr);
                            }
                        }

                        // Now, go through the original entries and mark them ready to print again.
                        foreach (registers_trust destroyedItem in colVoidList)
                        {
                            destroyedItem.cleared = 'P';
                            destroyedItem.sequence_number = null;
                            destroyedItem.checknum = null;
                        }

                        // We are complete. Submit the changes to the database to complete the transaction.
                        bc.SubmitChanges(System.Data.Linq.ConflictMode.FailOnFirstConflict);
                        txn.Commit();
                        txn.Dispose();
                        txn = null;
                    }
                }
                return null;
            }

            catch (System.Exception ex)
            {
                return ex;
            }
        }

        /// <summary>
        /// Print all pending checks
        /// </summary>
        internal void PrintMarkedChecks(PrintParameters param)
        {
            // Create the busy form
            _busyForm = null;
            string printerName = string.Empty;

            using (var rpt = new ChecksReport())
            {
                // don't complain if the margins are outside the physical printer. Just print the data as it is given.
                rpt.PrintingSystem.ShowMarginsWarning = false;

                // Ask for the printer name if the operation is not to preview the checks.
                if (!DebtPlus.Configuration.Config.PreviewChecks)
                {
                    using (var frm = new System.Windows.Forms.PrintDialog() { AllowPrintToFile = false, AllowSelection = false, AllowSomePages = false, AllowCurrentPage = false, PrintToFile = false, ShowHelp = false, ShowNetwork = false, UseEXDialog = true })
                    {
                        if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                        {
                            DebtPlus.Data.Forms.MessageBox.Show("The checks must be printed so this dialog can't be canceled.\r\nWe will use the default printer for the checks.", "Sorry, but you can't do that");
                        }
                        else
                        {
                            printerName = frm.PrinterSettings.PrinterName;
                            rpt.PrintingSystem.PageSettings.PrinterName = printerName;
                        }
                    }
                }

                // Display a busy form now
                using (_busyForm = new BusyStatusForm())
                {
                    _busyForm.Show();
                    _busyForm.BringToFront();
                    System.Windows.Forms.Application.DoEvents();

                    // Pass along the printMarker for the record selections
                    rpt.Parameter_Marker = param.MarkerString;

                    // Create the document and find the number of pages
                    rpt.FormattingFeedback += ChecksReport_FormattingFeedback;
                    rpt.CreateDocument();
                    rpt.FormattingFeedback -= ChecksReport_FormattingFeedback;

                    _busyForm.Close();
                }
                _busyForm = null;

                // Set the name of the document for the spooler
                rpt.PrintingSystem.Document.Name = "Checks";

                // Preview or print the checks.
                if (DebtPlus.Configuration.Config.PreviewChecks)
                {
                    rpt.DisplayPreviewDialog();
                }
                else
                {
                    rpt.PrintingSystem.ShowPrintStatusDialog = true;
                    // Show the status dialog to appease the user
                    if (printerName != string.Empty)
                    {
                        rpt.PrintDocument(printerName);
                    }
                    else
                    {
                        rpt.PrintDocument();
                    }
                }
            }
        }

        /// <summary>
        /// Provide some feedback to the user when we are formatting the checks
        /// </summary>
        private void ChecksReport_FormattingFeedback(object sender, ChecksReport.FeedbackArgs e)
        {
            if (_busyForm != null)
            {

                // Update the status form with the new information
                _busyForm.LabelControl_amount.Text = string.Format("{0:c}", e.Amount);
                _busyForm.LabelControl_checknum.Text = e.checknum.ToString();
                _busyForm.LabelControl_payee.Text = e.Payee;

                Int32 startValue = e.CurrentRow;
                Int32 endValue = e.MaxRow - e.CurrentRow;

                if (e.MaxRow > 0)
                {
                    _busyForm.ProgressBarControl1.Properties.Maximum = e.MaxRow;
                    _busyForm.ProgressBarControl1.Properties.Minimum = 0;
                    _busyForm.ProgressBarControl1.EditValue = e.CurrentRow;
                }

                _busyForm.LabelControl_Start.Text = string.Format("{0:n0}", startValue);
                _busyForm.LabelControl_End.Text = string.Format("{0:n0}", endValue);

                // Invalidate the fields so that they are re-painted on the screen
                _busyForm.LabelControl_amount.Invalidate();
                _busyForm.LabelControl_checknum.Invalidate();
                _busyForm.LabelControl_payee.Invalidate();
                _busyForm.LabelControl_Start.Invalidate();
                _busyForm.LabelControl_End.Invalidate();

                System.Windows.Forms.Application.DoEvents();
            }
        }

        /// <summary>
        /// Generate a new printMarker string
        /// </summary>
        static internal string GenerateNewMarker()
        {
            return string.Format("[{0}]", Guid.NewGuid().ToString().ToUpper());
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}