#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.Utils;
using System.Data.SqlClient;

namespace DebtPlus.Check.Print.Library
{
    internal partial class SelectChecksForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        public string MarkedChecksIdentifier = System.Guid.NewGuid().ToString();
        private System.Int32 Bank = -1;
        private System.Data.DataSet ds = new System.Data.DataSet("ds");
        private System.Data.DataTable tbl = null;

        public SelectChecksForm() : base()
        {
            InitializeComponent();
        }

        public SelectChecksForm(System.Int32 Bank) : this()
        {
            this.Bank = Bank;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            gridView1.Layout                           += LayoutChanged;
            Load                                       += SelectChecksForm_Load;
            gridView1.Click                            += GridView1_Click;
            button_OK.Click                            += Button_OK_Click;
            menuItem_Select.Click                      += menuItem_Clear_Click;
            menuItem_Clear.Click                       += menuItem_Clear_Click;
            menuItem_Select_All.Click                  += menuItem_Clear_All_Click;
            menuItem_Clear_All.Click                   += menuItem_Clear_All_Click;
            RepositoryItemCheckEdit1.EditValueChanging += RepositoryItemCheckEdit1_EditValueChanging;
            if (tbl != null)
            {
                tbl.ColumnChanging                     += ColumnChanging;
            }
        }

        private void UnRegisterHandlers()
        {
            gridView1.Layout                           -= LayoutChanged;
            Load                                       -= SelectChecksForm_Load;
            gridView1.Click                            -= GridView1_Click;
            button_OK.Click                            -= Button_OK_Click;
            menuItem_Select.Click                      -= menuItem_Clear_Click;
            menuItem_Clear.Click                       -= menuItem_Clear_Click;
            menuItem_Select_All.Click                  -= menuItem_Clear_All_Click;
            menuItem_Clear_All.Click                   -= menuItem_Clear_All_Click;
            RepositoryItemCheckEdit1.EditValueChanging -= RepositoryItemCheckEdit1_EditValueChanging;
            if (tbl != null)
            {
                tbl.ColumnChanging                     -= ColumnChanging;
            }
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
                ds.Dispose();
            }
            base.Dispose(disposing);
        }

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        internal DevExpress.XtraGrid.GridControl gridControl1;
        internal DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        internal DevExpress.XtraGrid.Columns.GridColumn gridColumn_Checked;
        internal DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit RepositoryItemCheckEdit1;
        internal DevExpress.XtraGrid.Columns.GridColumn gridColumn_trust_register;
        internal DevExpress.XtraGrid.Columns.GridColumn gridColumn_checknum;
        internal DevExpress.XtraGrid.Columns.GridColumn gridColumn_Payee;
        internal DevExpress.XtraGrid.Columns.GridColumn gridColumn_Date_Created;
        internal DevExpress.XtraGrid.Columns.GridColumn gridColumn_Amount;
        internal DevExpress.XtraGrid.Columns.GridColumn gridColumn_tran_type;
        internal DevExpress.XtraEditors.SimpleButton button_OK;
        internal DevExpress.XtraEditors.SimpleButton button_Cancel;
        internal System.Windows.Forms.ContextMenu contextMenu1;
        internal System.Windows.Forms.MenuItem menuItem_Select;
        internal System.Windows.Forms.MenuItem menuItem_Clear;
        internal System.Windows.Forms.MenuItem MenuItem3;
        internal System.Windows.Forms.MenuItem menuItem_Select_All;
        internal DevExpress.XtraGrid.Columns.GridColumn gridColumn_creditor;
        internal System.Windows.Forms.MenuItem menuItem_Clear_All;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.contextMenu1 = new System.Windows.Forms.ContextMenu();
            this.menuItem_Select = new System.Windows.Forms.MenuItem();
            this.menuItem_Clear = new System.Windows.Forms.MenuItem();
            this.MenuItem3 = new System.Windows.Forms.MenuItem();
            this.menuItem_Select_All = new System.Windows.Forms.MenuItem();
            this.menuItem_Clear_All = new System.Windows.Forms.MenuItem();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn_Checked = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_Checked.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.RepositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn_trust_register = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_trust_register.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_checknum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_checknum.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_creditor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_creditor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_Payee = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_Payee.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_Date_Created = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_Date_Created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_Amount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_Amount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_tran_type = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_tran_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.gridControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.gridView1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.RepositoryItemCheckEdit1).BeginInit();
            this.SuspendLayout();
            //
            //gridControl1
            //
            this.gridControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.gridControl1.ContextMenu = this.contextMenu1;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] { this.RepositoryItemCheckEdit1 });
            this.gridControl1.Size = new System.Drawing.Size(480, 215);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ToolTipController = this.ToolTipController1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { this.gridView1 });
            //
            //contextMenu1
            //
            this.contextMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
			this.menuItem_Select,
			this.menuItem_Clear,
			this.MenuItem3,
			this.menuItem_Select_All,
			this.menuItem_Clear_All
		});
            //
            //menuItem_Select
            //
            this.menuItem_Select.Index = 0;
            this.menuItem_Select.Text = "&Select";
            //
            //menuItem_Clear
            //
            this.menuItem_Clear.Index = 1;
            this.menuItem_Clear.Text = "&Clear";
            //
            //MenuItem3
            //
            this.MenuItem3.Index = 2;
            this.MenuItem3.Text = "-";
            //
            //menuItem_Select_All
            //
            this.menuItem_Select_All.Index = 3;
            this.menuItem_Select_All.Text = "Select &All";
            //
            //menuItem_Clear_All
            //
            this.menuItem_Clear_All.Index = 4;
            this.menuItem_Clear_All.Text = "Clear All";
            //
            //gridView1
            //
            this.gridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.gridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.gridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.gridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.gridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.gridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.Empty.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
            this.gridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
            this.gridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.gridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.gridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.gridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(59), (Int32)Convert.ToByte(133), (Int32)Convert.ToByte(195));
            this.gridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(38), (Int32)Convert.ToByte(109), (Int32)Convert.ToByte(189));
            this.gridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(59), (Int32)Convert.ToByte(139), (Int32)Convert.ToByte(206));
            this.gridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.gridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.gridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.gridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.gridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.gridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.gridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.gridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.gridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.gridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(139), (Int32)Convert.ToByte(201), (Int32)Convert.ToByte(254));
            this.gridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(139), (Int32)Convert.ToByte(201), (Int32)Convert.ToByte(254));
            this.gridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(105), (Int32)Convert.ToByte(170), (Int32)Convert.ToByte(225));
            this.gridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
            this.gridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.gridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.gridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.gridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.gridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.gridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5f);
            this.gridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
            this.gridView1.Appearance.Preview.Options.UseFont = true;
            this.gridView1.Appearance.Preview.Options.UseForeColor = true;
            this.gridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
            this.gridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.Row.Options.UseBackColor = true;
            this.gridView1.Appearance.Row.Options.UseForeColor = true;
            this.gridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.gridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
            this.gridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.gridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.gridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
			this.gridColumn_Checked,
			this.gridColumn_trust_register,
			this.gridColumn_checknum,
			this.gridColumn_creditor,
			this.gridColumn_Payee,
			this.gridColumn_Date_Created,
			this.gridColumn_Amount,
			this.gridColumn_tran_type
		});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsSelection.InvertSelection = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsSelection.UseIndicatorForSelection = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            //
            //gridColumn_Checked
            //
            this.gridColumn_Checked.ColumnEdit = this.RepositoryItemCheckEdit1;
            this.gridColumn_Checked.CustomizationCaption = "Checked Indicator";
            this.gridColumn_Checked.FieldName = "checked";
            this.gridColumn_Checked.Name = "gridColumn_Checked";
            this.gridColumn_Checked.Visible = true;
            this.gridColumn_Checked.VisibleIndex = 0;
            this.gridColumn_Checked.Width = 25;
            //
            //RepositoryItemCheckEdit1
            //
            this.RepositoryItemCheckEdit1.AutoHeight = false;
            this.RepositoryItemCheckEdit1.Caption = "";
            this.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1";
            this.RepositoryItemCheckEdit1.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.RepositoryItemCheckEdit1.ValueChecked = 1;
            this.RepositoryItemCheckEdit1.ValueUnchecked = 0;
            //
            //gridColumn_trust_register
            //
            this.gridColumn_trust_register.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_trust_register.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_trust_register.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_trust_register.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_trust_register.Caption = "Record";
            this.gridColumn_trust_register.CustomizationCaption = "Record ID";
            this.gridColumn_trust_register.DisplayFormat.FormatString = "f0";
            this.gridColumn_trust_register.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_trust_register.FieldName = "trust_register";
            this.gridColumn_trust_register.GroupFormat.FormatString = "f0";
            this.gridColumn_trust_register.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_trust_register.Name = "gridColumn_trust_register";
            this.gridColumn_trust_register.OptionsColumn.AllowEdit = false;
            //
            //gridColumn_checknum
            //
            this.gridColumn_checknum.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_checknum.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_checknum.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_checknum.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_checknum.Caption = "Check Number";
            this.gridColumn_checknum.CustomizationCaption = "Check Number";
            this.gridColumn_checknum.FieldName = "checknum";
            this.gridColumn_checknum.Name = "gridColumn_checknum";
            this.gridColumn_checknum.OptionsColumn.AllowEdit = false;
            //
            //gridColumn_creditor
            //
            this.gridColumn_creditor.Caption = "ID";
            this.gridColumn_creditor.CustomizationCaption = "Creditor/Client ID";
            this.gridColumn_creditor.FieldName = "creditor";
            this.gridColumn_creditor.Name = "gridColumn_creditor";
            this.gridColumn_creditor.OptionsColumn.AllowEdit = false;
            this.gridColumn_creditor.Visible = true;
            this.gridColumn_creditor.VisibleIndex = 1;
            this.gridColumn_creditor.Width = 70;
            //
            //gridColumn_Payee
            //
            this.gridColumn_Payee.Caption = "Payee";
            this.gridColumn_Payee.CustomizationCaption = "Payee";
            this.gridColumn_Payee.FieldName = "payee";
            this.gridColumn_Payee.Name = "gridColumn_Payee";
            this.gridColumn_Payee.OptionsColumn.AllowEdit = false;
            this.gridColumn_Payee.Visible = true;
            this.gridColumn_Payee.VisibleIndex = 2;
            this.gridColumn_Payee.Width = 344;
            //
            //gridColumn_Date_Created
            //
            this.gridColumn_Date_Created.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_Date_Created.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_Date_Created.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_Date_Created.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_Date_Created.Caption = "Date";
            this.gridColumn_Date_Created.CustomizationCaption = "Date Written";
            this.gridColumn_Date_Created.DisplayFormat.FormatString = "d";
            this.gridColumn_Date_Created.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn_Date_Created.FieldName = "date_created";
            this.gridColumn_Date_Created.GroupFormat.FormatString = "d";
            this.gridColumn_Date_Created.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn_Date_Created.Name = "gridColumn_Date_Created";
            this.gridColumn_Date_Created.OptionsColumn.AllowEdit = false;
            this.gridColumn_Date_Created.Visible = true;
            this.gridColumn_Date_Created.VisibleIndex = 3;
            //
            //gridColumn_Amount
            //
            this.gridColumn_Amount.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_Amount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_Amount.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_Amount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_Amount.Caption = "Amount";
            this.gridColumn_Amount.CustomizationCaption = "Check Amount";
            this.gridColumn_Amount.DisplayFormat.FormatString = "c2";
            this.gridColumn_Amount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_Amount.FieldName = "amount";
            this.gridColumn_Amount.GroupFormat.FormatString = "c2";
            this.gridColumn_Amount.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_Amount.Name = "gridColumn_Amount";
            this.gridColumn_Amount.OptionsColumn.AllowEdit = false;
            this.gridColumn_Amount.Visible = true;
            this.gridColumn_Amount.VisibleIndex = 4;
            this.gridColumn_Amount.Width = 100;
            //
            //gridColumn_tran_type
            //
            this.gridColumn_tran_type.Caption = "Transaction";
            this.gridColumn_tran_type.CustomizationCaption = "Transaction Type";
            this.gridColumn_tran_type.FieldName = "tran_type";
            this.gridColumn_tran_type.Name = "gridColumn_tran_type";
            this.gridColumn_tran_type.OptionsColumn.AllowEdit = false;
            this.gridColumn_tran_type.Width = 137;
            //
            //button_OK
            //
            this.button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button_OK.Location = new System.Drawing.Point(147, 224);
            this.button_OK.Name = "button_OK";
            this.button_OK.Size = new System.Drawing.Size(75, 23);
            this.button_OK.TabIndex = 1;
            this.button_OK.Text = "&OK";
            this.button_OK.ToolTipController = this.ToolTipController1;
            //
            //button_Cancel
            //
            this.button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button_Cancel.Location = new System.Drawing.Point(259, 224);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.button_Cancel.TabIndex = 2;
            this.button_Cancel.Text = "&Cancel";
            this.button_Cancel.ToolTipController = this.ToolTipController1;
            //
            //SelectChecksForm
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.CancelButton = this.button_Cancel;
            this.ClientSize = new System.Drawing.Size(480, 256);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.button_OK);
            this.Controls.Add(this.gridControl1);
            this.Name = "SelectChecksForm";
            this.Text = "Select Checks to be Printed";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.gridControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.gridView1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.RepositoryItemCheckEdit1).EndInit();
            this.ResumeLayout(false);
        }

        #endregion

        /// <summary>
        /// Load a list of pending checks to be printed
        /// </summary>
        private void SelectChecksForm_Load(object sender, System.EventArgs e)
        {
            base.LoadPlacement("Check.Print.Library.SelectChecks");
            UnRegisterHandlers();

            try
            {
                using (var cm = new DebtPlus.UI.Common.CursorManager())
                {
                    using (var cmd = new SqlCommand())
                    {
                        cmd.CommandText = "SELECT tr.trust_register, tr.tran_type, tr.date_created, tr.amount, case when tr.client is not null then dbo.format_client_id(tr.client) else tr.creditor end as creditor, CASE WHEN tr.client IS NOT NULL THEN dbo.format_normal_name(pn.prefix, pn.first, pn.middle, pn.last, pn.suffix) ELSE cr.creditor_name END AS payee FROM registers_trust tr WITH (NOLOCK) LEFT OUTER JOIN people p WITH (NOLOCK) ON tr.client = p.client AND 1 = p.relation LEFT OUTER JOIN Names pn WITH (NOLOCK) ON p.NameID = pn.Name LEFT OUTER JOIN creditors cr WITH (NOLOCK) ON tr.creditor = cr.creditor WHERE tr.cleared = 'P' AND tr.tran_type IN ('AD','MD','CM','CR','AR') AND tr.amount > 0 AND tr.bank = @bank";
                        cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        cmd.Parameters.Add("@bank", SqlDbType.Int).Value = Bank;
                        cmd.CommandType = CommandType.Text;

                        using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                        {
                            da.Fill(ds, "selected_checks");
                        }
                    }
                }

                tbl = ds.Tables["selected_checks"];
                if (!tbl.Columns.Contains("checked"))
                {
                    tbl.Columns.Add("checked", typeof(System.Int32)).DefaultValue = false;
                }

                tbl.Columns["checked"].AllowDBNull = true;
                tbl.Columns["checked"].ReadOnly = false;
                tbl.Columns["checked"].DefaultValue = 0;
                tbl.PrimaryKey = new System.Data.DataColumn[] { tbl.Columns["trust_register"] };

                foreach (System.Data.DataRow row in tbl.Rows)
                {
                    row.BeginEdit();
                    row["checked"] = 0;
                    row.EndEdit();
                }
                gridControl1.DataSource = tbl;

                // Start with the button disabled.
                button_OK.Enabled = false;

                string PathName = XMLBasePath();
                string FileName = System.IO.Path.Combine(PathName, gridView1.Name + ".Grid.xml");
                if (System.IO.File.Exists(FileName))
                {
                    gridView1.RestoreLayoutFromXml(FileName);
                }
            }

#pragma warning disable 168
            catch (System.IO.DirectoryNotFoundException ex) { }
            catch (System.IO.FileNotFoundException ex) { }
#pragma warning restore 168

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }

            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Return the directory to the saved layout information
        /// </summary>
        protected virtual string XMLBasePath()
        {
            string BasePath = System.Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + System.IO.Path.DirectorySeparatorChar + "DebtPlus";
            return System.IO.Path.Combine(BasePath, "Checks.Print.Library.SelectChecksForm");
        }

        /// <summary>
        /// If the layout is changed then update the file
        /// </summary>
        private void LayoutChanged(object Sender, System.EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                string PathName = XMLBasePath();
                if (!System.IO.Directory.Exists(PathName))
                {
                    System.IO.Directory.CreateDirectory(PathName);
                }

                string FileName = System.IO.Path.Combine(PathName, gridView1.Name + ".Grid.xml");
                gridView1.SaveLayoutToXml(FileName);
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Handle the CLEAR OR SELECT menu items
        /// </summary>
        private void menuItem_Clear_Click(object sender, System.EventArgs e)
        {
            bool NewState = true;
            if (object.ReferenceEquals(sender, menuItem_Clear))
            {
                NewState = false;
            }

            // For the current selected rows, set the checked state accordingly
            foreach (System.Int32 SelectedRow in gridView1.GetSelectedRows())
            {
                System.Data.DataRow Row = gridView1.GetDataRow(SelectedRow);
                if (Row != null)
                {
                    Row.BeginEdit();
                    Row["checked"] = NewState;
                    Row.EndEdit();
                }
            }
        }

        /// <summary>
        /// Handle the CLEAR ALL button being pressed
        /// </summary>
        private void menuItem_Clear_All_Click(object sender, System.EventArgs e)
        {
            bool NewState = true;
            if (object.ReferenceEquals(sender, menuItem_Clear_All))
            {
                NewState = false;
            }

            // For the current selected rows, set the checked state accordingly
            foreach (System.Data.DataRow row in ds.Tables[0].Rows)
            {
                row.BeginEdit();
                row["checked"] = NewState;
                row.EndEdit();
            }
        }

        /// <summary>
        /// Enable/disable the ok button based upon the selection list
        /// </summary>
        private void RepositoryItemCheckEdit1_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            button_OK.Enabled = SelectedItems(e.NewValue);
        }

        private void ColumnChanging(object Sender, System.Data.DataColumnChangeEventArgs e)
        {
            button_OK.Enabled = SelectedItems(e.ProposedValue);
        }

        private bool SelectedItems(object ProposedValue)
        {
            // If the new value is selected then enable the ok button
            if (DebtPlus.Utils.Nulls.DInt(ProposedValue) != 0)
            {
                return true;
            }

            // If the new value is deselected then count the number of selected items.
            // we need to have at least two selected items (1 plus the one that we are deselecting)
            // to have a valid condition for the OK button.
            System.Int32 SelectedCount = 0;
            foreach (System.Data.DataRow row in tbl.Rows)
            {
                if (!object.ReferenceEquals(row["checked"], System.DBNull.Value) && Convert.ToInt32(row["checked"]) != 0)
                {
                    SelectedCount += 1;

                    // Stop when we find a valid situation. There is no need to examine all of the items.
                    if (SelectedCount > 1)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Process the OK button click event
        /// </summary>
        private void Button_OK_Click(object sender, System.EventArgs e)
        {
            var sb = new System.Text.StringBuilder();

            // Find the trust registers that are marked in the desired list
            using (System.Data.DataView vue = new System.Data.DataView(tbl, "[checked]<>0", "trust_register", DataViewRowState.CurrentRows))
            {
                foreach (System.Data.DataRowView drv in vue)
                {
                    sb.AppendFormat(",{0:d}", Convert.ToInt32(drv["trust_register"]));
                }
            }

            if (sb.Length <= 0)
            {
                DialogResult = System.Windows.Forms.DialogResult.Cancel;
                return;
            }
            sb.Remove(0, 1);

            sb.Insert(0, "UPDATE registers_trust SET sequence_number=@sequence_number WHERE trust_register in (");
            sb.Append(") AND cleared = 'P' and amount > 0 and tran_type in ('AD','MD','CM','CR','AR')");

            MarkedChecksIdentifier = PrintChecksClass.GenerateNewMarker();

            // Update the database to mark the checks with our identifier
            try
            {
                using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                {
                    System.Int32 rows_updated = 0;
                    using (var cm = new DebtPlus.UI.Common.CursorManager())
                    {
                        cn.Open();
                        using (var cmd = new System.Data.SqlClient.SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.CommandText = sb.ToString();
                            cmd.Parameters.Add("@sequence_number", SqlDbType.VarChar, 80).Value = MarkedChecksIdentifier;
                            rows_updated = cmd.ExecuteNonQuery();
                        }

                        // If there were no checks remarked for our selection then complain and cancel.
                        if (rows_updated == 0)
                        {
                            DebtPlus.Data.Forms.MessageBox.Show("The checks are no longer in the ready to be printed status. Please review the check numbers again.", "Sorry, but I can't do this", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                            DialogResult = System.Windows.Forms.DialogResult.Cancel;
                            return;
                        }

                        // We are terminating normally.
                        DialogResult = System.Windows.Forms.DialogResult.OK;
                        return;
                    }
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }

            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        /// <summary>
        /// Process the click event on the grid
        /// </summary>
        private void GridView1_Click(object sender, System.EventArgs e)
        {
            // Find where we clicked on the grid. I need the row and column.
            DevExpress.XtraGrid.Views.Grid.GridView gv = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            DevExpress.XtraGrid.GridControl ctl = (DevExpress.XtraGrid.GridControl)gv.GridControl;
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gv.CalcHitInfo((ctl.PointToClient(System.Windows.Forms.Control.MousePosition)));

            // We need to be validly in a row and not somewhere else to continue.
            if (hi.IsValid && hi.InRow)
            {
                System.Int32 ControlRow = hi.RowHandle;
                DevExpress.XtraGrid.Columns.GridColumn col = hi.Column;
                // Don't do this in the check-box column as you will toggle the checked status back to the original setting.
                if (!object.ReferenceEquals(col, gridColumn_Checked))
                {
                    System.Data.DataRow row = gv.GetDataRow(ControlRow);
                    row.BeginEdit();
                    row["checked"] = !DebtPlus.Utils.Nulls.DBool(row["checked"]);
                    row.EndEdit();
                }
            }
        }
    }
}
