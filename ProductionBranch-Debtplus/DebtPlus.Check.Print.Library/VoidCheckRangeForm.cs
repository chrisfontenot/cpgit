#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.Check.Print.Library
{
    internal partial class VoidCheckRangeForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        private PrintChecksClass.VoidParameters record;
        public VoidCheckRangeForm(PrintChecksClass.VoidParameters record)
            : base()
        {
            // This call is required by the Windows Form Designer.
            InitializeComponent();
            this.record = record;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += VoidCheckRangeForm_Load;
            Button_OK.Click += Button_OK_Click;
            first_number_edit.EditValueChanging += first_number_edit_EditValueChanging;
            last_number_edit.EditValueChanging += last_number_edit_EditValueChanging;
        }

        private void UnRegisterHandlers()
        {
            Load -= VoidCheckRangeForm_Load;
            Button_OK.Click -= Button_OK_Click;
            first_number_edit.EditValueChanging -= first_number_edit_EditValueChanging;
            last_number_edit.EditValueChanging -= last_number_edit_EditValueChanging;
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if ((components != null))
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.LabelControl LabelControl1;
        internal DevExpress.XtraEditors.LabelControl LabelControl2;
        internal DevExpress.XtraEditors.LabelControl LabelControl3;
        internal DevExpress.XtraEditors.LabelControl LabelControl4;
        internal DevExpress.XtraEditors.LabelControl LabelControl5;
        internal DevExpress.XtraEditors.LabelControl LabelControl6;
        internal DevExpress.XtraEditors.LabelControl LabelControl7;
        internal DevExpress.XtraEditors.TextEdit first_number_edit;
        internal DevExpress.XtraEditors.TextEdit last_number_edit;
        internal DevExpress.XtraEditors.LabelControl LabelControl8;
        internal DevExpress.XtraEditors.LabelControl LabelControl9;
        internal DevExpress.XtraEditors.SimpleButton Button_OK;
        internal DevExpress.XtraEditors.PanelControl PanelControl1;
        internal DevExpress.XtraEditors.LabelControl LabelControl10;
        internal DevExpress.XtraEditors.CheckEdit checkEdit_GenerateDestroyed;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VoidCheckRangeForm));
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.first_number_edit = new DevExpress.XtraEditors.TextEdit();
            this.last_number_edit = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.PanelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.LabelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.checkEdit_GenerateDestroyed = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.first_number_edit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.last_number_edit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelControl1)).BeginInit();
            this.PanelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_GenerateDestroyed.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // LabelControl1
            // 
            this.LabelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl1.Location = new System.Drawing.Point(24, 8);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(20, 29);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "1.";
            this.LabelControl1.ToolTipController = this.ToolTipController1;
            // 
            // LabelControl2
            // 
            this.LabelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LabelControl2.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.LabelControl2.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl2.Location = new System.Drawing.Point(50, 12);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(444, 35);
            this.LabelControl2.TabIndex = 1;
            this.LabelControl2.Text = "If the checks were printed without an error then press the ALL IS OK button at th" +
    "e bottom to update the database with the check numbers.";
            this.LabelControl2.ToolTipController = this.ToolTipController1;
            // 
            // LabelControl3
            // 
            this.LabelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LabelControl3.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.LabelControl3.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl3.Location = new System.Drawing.Point(50, 55);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(444, 32);
            this.LabelControl3.TabIndex = 3;
            this.LabelControl3.Text = "If you were able to restart the print function then enter the check numbers of th" +
    "e first and last check destroyed in the printing.";
            this.LabelControl3.ToolTipController = this.ToolTipController1;
            // 
            // LabelControl4
            // 
            this.LabelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.LabelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl4.Location = new System.Drawing.Point(24, 48);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(20, 29);
            this.LabelControl4.TabIndex = 2;
            this.LabelControl4.Text = "2.";
            this.LabelControl4.ToolTipController = this.ToolTipController1;
            // 
            // LabelControl5
            // 
            this.LabelControl5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelControl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LabelControl5.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
            this.LabelControl5.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LabelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl5.Location = new System.Drawing.Point(50, 95);
            this.LabelControl5.Name = "LabelControl5";
            this.LabelControl5.Size = new System.Drawing.Size(444, 32);
            this.LabelControl5.TabIndex = 5;
            this.LabelControl5.Text = "If the checks were jammed to the end of the batch then enter the check number of " +
    "the first check destroyed. Leave the last number field empty (or enter 0).";
            this.LabelControl5.ToolTipController = this.ToolTipController1;
            // 
            // LabelControl6
            // 
            this.LabelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.LabelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl6.Location = new System.Drawing.Point(24, 88);
            this.LabelControl6.Name = "LabelControl6";
            this.LabelControl6.Size = new System.Drawing.Size(20, 29);
            this.LabelControl6.TabIndex = 4;
            this.LabelControl6.Text = "3.";
            this.LabelControl6.ToolTipController = this.ToolTipController1;
            // 
            // LabelControl7
            // 
            this.LabelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelControl7.Appearance.ForeColor = System.Drawing.Color.DarkRed;
            this.LabelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl7.Location = new System.Drawing.Point(16, 152);
            this.LabelControl7.Name = "LabelControl7";
            this.LabelControl7.Size = new System.Drawing.Size(200, 13);
            this.LabelControl7.TabIndex = 6;
            this.LabelControl7.Text = "First Check Number to be Reprinted";
            this.LabelControl7.ToolTipController = this.ToolTipController1;
            // 
            // first_number_edit
            // 
            this.first_number_edit.Location = new System.Drawing.Point(232, 149);
            this.first_number_edit.Name = "first_number_edit";
            this.first_number_edit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.first_number_edit.Properties.Appearance.Options.UseTextOptions = true;
            this.first_number_edit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.first_number_edit.Properties.DisplayFormat.FormatString = "############0; ; ";
            this.first_number_edit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.first_number_edit.Properties.EditFormat.FormatString = "f0";
            this.first_number_edit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.first_number_edit.Properties.Mask.BeepOnError = true;
            this.first_number_edit.Properties.Mask.EditMask = "f0";
            this.first_number_edit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.first_number_edit.Size = new System.Drawing.Size(100, 20);
            this.first_number_edit.TabIndex = 7;
            this.first_number_edit.ToolTipController = this.ToolTipController1;
            // 
            // last_number_edit
            // 
            this.last_number_edit.Location = new System.Drawing.Point(232, 177);
            this.last_number_edit.Name = "last_number_edit";
            this.last_number_edit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.last_number_edit.Properties.Appearance.Options.UseTextOptions = true;
            this.last_number_edit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.last_number_edit.Properties.DisplayFormat.FormatString = "############0; ; ";
            this.last_number_edit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.last_number_edit.Properties.EditFormat.FormatString = "f0";
            this.last_number_edit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.last_number_edit.Properties.Mask.BeepOnError = true;
            this.last_number_edit.Properties.Mask.EditMask = "f0";
            this.last_number_edit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.last_number_edit.Size = new System.Drawing.Size(100, 20);
            this.last_number_edit.TabIndex = 9;
            this.last_number_edit.ToolTipController = this.ToolTipController1;
            // 
            // LabelControl8
            // 
            this.LabelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelControl8.Appearance.ForeColor = System.Drawing.Color.DarkRed;
            this.LabelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl8.Location = new System.Drawing.Point(16, 180);
            this.LabelControl8.Name = "LabelControl8";
            this.LabelControl8.Size = new System.Drawing.Size(199, 13);
            this.LabelControl8.TabIndex = 8;
            this.LabelControl8.Text = "Last Check Number to be Reprinted";
            this.LabelControl8.ToolTipController = this.ToolTipController1;
            // 
            // LabelControl9
            // 
            this.LabelControl9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelControl9.Appearance.ForeColor = System.Drawing.Color.DarkRed;
            this.LabelControl9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LabelControl9.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.LabelControl9.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LabelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl9.Location = new System.Drawing.Point(59, 3);
            this.LabelControl9.Name = "LabelControl9";
            this.LabelControl9.Size = new System.Drawing.Size(404, 68);
            this.LabelControl9.TabIndex = 10;
            this.LabelControl9.Text = resources.GetString("LabelControl9.Text");
            this.LabelControl9.ToolTipController = this.ToolTipController1;
            // 
            // Button_OK
            // 
            this.Button_OK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_OK.Location = new System.Drawing.Point(404, 144);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.TabIndex = 11;
            this.Button_OK.Text = "VOID CHKS";
            this.Button_OK.ToolTipController = this.ToolTipController1;
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(404, 176);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 12;
            this.Button_Cancel.Text = "ALL IS OK";
            this.Button_Cancel.ToolTipController = this.ToolTipController1;
            // 
            // PanelControl1
            // 
            this.PanelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.PanelControl1.Controls.Add(this.LabelControl9);
            this.PanelControl1.Controls.Add(this.LabelControl10);
            this.PanelControl1.Location = new System.Drawing.Point(13, 214);
            this.PanelControl1.Name = "PanelControl1";
            this.PanelControl1.Size = new System.Drawing.Size(466, 79);
            this.PanelControl1.TabIndex = 14;
            // 
            // LabelControl10
            // 
            this.LabelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelControl10.Appearance.ForeColor = System.Drawing.Color.DarkRed;
            this.LabelControl10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LabelControl10.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.LabelControl10.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LabelControl10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl10.Location = new System.Drawing.Point(3, 3);
            this.LabelControl10.Name = "LabelControl10";
            this.LabelControl10.Size = new System.Drawing.Size(72, 16);
            this.LabelControl10.TabIndex = 14;
            this.LabelControl10.Text = "CAUTION:";
            this.LabelControl10.ToolTipController = this.ToolTipController1;
            // 
            // checkEdit_GenerateDestroyed
            // 
            this.checkEdit_GenerateDestroyed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkEdit_GenerateDestroyed.EditValue = true;
            this.checkEdit_GenerateDestroyed.Location = new System.Drawing.Point(13, 304);
            this.checkEdit_GenerateDestroyed.Name = "checkEdit_GenerateDestroyed";
            this.checkEdit_GenerateDestroyed.Properties.Caption = "Mark the previous checks as destroyed in printing and void them at the bank";
            this.checkEdit_GenerateDestroyed.Size = new System.Drawing.Size(392, 19);
            this.checkEdit_GenerateDestroyed.TabIndex = 15;
            // 
            // VoidCheckRangeForm
            // 
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(508, 335);
            this.Controls.Add(this.checkEdit_GenerateDestroyed);
            this.Controls.Add(this.PanelControl1);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.last_number_edit);
            this.Controls.Add(this.LabelControl8);
            this.Controls.Add(this.first_number_edit);
            this.Controls.Add(this.LabelControl7);
            this.Controls.Add(this.LabelControl5);
            this.Controls.Add(this.LabelControl6);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.LabelControl4);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.LabelControl1);
            this.MaximizeBox = false;
            this.Name = "VoidCheckRangeForm";
            this.Text = "Check Printing Status";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.first_number_edit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.last_number_edit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelControl1)).EndInit();
            this.PanelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_GenerateDestroyed.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private void first_number_edit_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            Button_OK.Enabled = !HasErrors(DebtPlus.Utils.Nulls.v_Int64(e.NewValue), DebtPlus.Utils.Nulls.v_Int64(last_number_edit.EditValue));
        }

        private void last_number_edit_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            Button_OK.Enabled = !HasErrors(DebtPlus.Utils.Nulls.v_Int64(first_number_edit.EditValue), DebtPlus.Utils.Nulls.v_Int64(e.NewValue));
        }

        private bool HasErrors(Int64? firstItem, Int64? lastitem)
        {
            // A first item is always required.
            if (!firstItem.HasValue || firstItem.Value <= 0L)
            {
                return true;
            }

            // The last value may be missing here.
            if (!lastitem.HasValue)
            {
                return false;
            }

            // The ending check must have a value if it is given
            if (lastitem.Value < 0L)
            {
                return true;
            }

            // If there is no last item or it is greater to the first then we are good.
            if (lastitem.Value == 0L || firstItem.Value < lastitem.Value)
            {
                return false;
            }

            return true;
        }

        private void VoidCheckRangeForm_Load(System.Object sender, System.EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                checkEdit_GenerateDestroyed.Checked = record.GenerateVoids;
                first_number_edit.EditValue = record.FirstCheck;
                last_number_edit.EditValue = record.LastCheck;

                Button_OK.Enabled = !HasErrors(record.FirstCheck, record.LastCheck);

                CloseBox = false;
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void Button_OK_Click(System.Object sender, System.EventArgs e)
        {
            record.GenerateVoids = checkEdit_GenerateDestroyed.Checked;
            record.FirstCheck = DebtPlus.Utils.Nulls.v_Int64(first_number_edit.EditValue);
            record.LastCheck = DebtPlus.Utils.Nulls.v_Int64(last_number_edit.EditValue);

            DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}
