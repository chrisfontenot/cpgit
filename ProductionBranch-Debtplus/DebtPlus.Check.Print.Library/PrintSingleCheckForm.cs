#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.Check.Print.Library
{
    internal partial class PrintSingleCheckForm : SingleCheckForm
    {
        public PrintSingleCheckForm() : base()
        {
            InitializeComponent();
        }

        public PrintSingleCheckForm(PrintChecksClass Parent, System.Int32 TrustRegister) : base(Parent, TrustRegister)
        {
            InitializeComponent();
            Load += PrintSingleCheckForm_Load;
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)this.check_date.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.checknum.Properties).BeginInit();

            this.SuspendLayout();
            //
            //check_date
            //
            this.check_date.Name = "check_date";
            //
            //check_date.Properties
            //
            this.check_date.Properties.ReadOnly = true;
            //
            //checknum
            //
            this.checknum.Name = "checknum";
            //
            //checknum.Properties
            //
            this.checknum.Properties.Appearance.Options.UseTextOptions = true;
            this.checknum.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.checknum.Properties.DisplayFormat.FormatString = "d0";
            this.checknum.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.checknum.Properties.EditFormat.FormatString = "d0";
            this.checknum.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.checknum.Properties.Mask.BeepOnError = true;
            this.checknum.Properties.Mask.EditMask = "f0";
            this.checknum.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //
            //Button_Print
            //
            this.Button_Print.Name = "Button_Print";
            this.Button_Print.Text = "Print";

            //
            //PrintSingleCheckForm
            //
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(458, 215);
            this.Name = "PrintSingleCheckForm";
            this.Text = "Print Single Check";
            this.ToolTipController1.SetSuperTip(this, null);
            ((System.ComponentModel.ISupportInitialize)this.check_date.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.checknum.Properties).EndInit();

            this.ResumeLayout(false);
        }

        #endregion

        /// <summary>
        /// Process the form load event
        /// </summary>
        private void PrintSingleCheckForm_Load(object sender, System.EventArgs e)
        {
            ReloadForm(true);
        }

        /// <summary>
        /// Print button was pressed. Do the print operation now.
        /// </summary>
        protected override void Button_Print_Click(object sender, EventArgs e)
        {
            System.Int64 Newchecknum = Convert.ToInt64(checknum.EditValue);
            if (checknum.Properties.ReadOnly)
            {
                return;
            }

            PrintChecksClass.VoidParameters printParm = new PrintChecksClass.VoidParameters() { BankID = bankID, MarkerString = PrintChecksClass.GenerateNewMarker(), GenerateVoids = true };
            try
            {
                using (var bc = new BusinessContext())
                {
                    var tr = bc.registers_trusts.Where(s => s.Id == Parameter_TrustRegister).FirstOrDefault();
                    if (tr != null)
                    {
                        tr.checknum = Newchecknum;
                        tr.sequence_number = printParm.MarkerString;
                        tr.cleared = ' ';
                    }

                    var bnk = bc.banks.Where(s => s.Id == printParm.BankID).FirstOrDefault();
                    if (bnk != null)
                    {
                        bnk.checknum = Newchecknum + 1L;
                    }

                    bc.SubmitChanges();
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                ReloadForm();
                return;
            }

            // Generate the report for the check if there is no error to this point
            ParentClass.PrintMarkedChecks(printParm);
            if (DebtPlus.Data.Forms.MessageBox.Show("Was the check printed correctly?", "Successful Completion", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                SetDialogResult(System.Windows.Forms.DialogResult.OK);
                return;
            }

            // Void the check and try again.
            ParentClass.VoidCheck(printParm);
            ReloadForm(false);
        }
    }
}