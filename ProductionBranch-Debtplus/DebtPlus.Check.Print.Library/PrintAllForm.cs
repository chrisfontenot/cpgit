#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.Common;
using DebtPlus.Utils;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Globalization;
using DebtPlus.Data.Forms;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.Check.Print.Library
{
    public partial class PrintAllForm : DebtPlusForm
    {
        // Private dataset for the storage
        System.Data.DataSet ds = new System.Data.DataSet("ds");

        // Should the warning message be shown when the form is closed?
        private bool ShouldShowWarning = false;

        // Current list of checks to be processed
        private string MarkedChecksIdentifier = string.Empty;

        // Pointer to our parent check class
        private PrintChecksClass ParentClass = null;

        public PrintAllForm()
            : base()
        {
            InitializeComponent();
        }

        public PrintAllForm(PrintChecksClass ParentClass)
            : this()
        {
            this.ParentClass = ParentClass;
            RegisterHandlers();
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if ((components != null))
                {
                    components.Dispose();
                }
                ds.Dispose();
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.LabelControl LabelControl1;
        internal DevExpress.XtraEditors.LabelControl LabelControl2;
        internal DevExpress.XtraEditors.LookUpEdit BankLookup;
        internal DevExpress.XtraEditors.GroupControl GroupControl1;
        internal DevExpress.XtraEditors.LabelControl status;
        internal DevExpress.XtraEditors.TextEdit checknum;
        internal DevExpress.XtraEditors.GroupControl GroupControl2;
        internal DevExpress.XtraEditors.RadioGroup print_checks;
        internal DevExpress.XtraEditors.SimpleButton Button_Print;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;

        internal DevExpress.XtraEditors.SimpleButton Button_All;
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.BankLookup = new DevExpress.XtraEditors.LookUpEdit();
            this.GroupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.status = new DevExpress.XtraEditors.LabelControl();
            this.checknum = new DevExpress.XtraEditors.TextEdit();
            this.GroupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.print_checks = new DevExpress.XtraEditors.RadioGroup();
            this.Button_All = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Print = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.BankLookup.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GroupControl1).BeginInit();
            this.GroupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.checknum.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GroupControl2).BeginInit();
            this.GroupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.print_checks.Properties).BeginInit();
            this.SuspendLayout();
            //
            //LabelControl1
            //
            this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl1.Location = new System.Drawing.Point(16, 12);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(27, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Bank:";
            //
            //LabelControl2
            //
            this.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl2.Location = new System.Drawing.Point(16, 88);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(97, 13);
            this.LabelControl2.TabIndex = 3;
            this.LabelControl2.Text = "First Check Number:";
            //
            //BankLookup
            //
            this.BankLookup.Location = new System.Drawing.Point(56, 9);
            this.BankLookup.Name = "BankLookup";
            this.BankLookup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.BankLookup.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
			new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Bank", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
			new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description"),
		});
            this.BankLookup.Size = new System.Drawing.Size(320, 20);
            this.BankLookup.TabIndex = 1;
            this.BankLookup.Properties.DisplayMember = "description";
            this.BankLookup.Properties.ValueMember = "Id";
            this.BankLookup.Properties.NullText = string.Empty;
            //
            //GroupControl1
            //
            this.GroupControl1.Controls.Add(this.status);
            this.GroupControl1.Location = new System.Drawing.Point(8, 40);
            this.GroupControl1.Name = "GroupControl1";
            this.GroupControl1.ShowCaption = false;
            this.GroupControl1.Size = new System.Drawing.Size(368, 32);
            this.GroupControl1.TabIndex = 2;
            this.GroupControl1.Text = "GroupControl1";
            //
            //status
            //
            this.status.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.status.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.status.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.status.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.status.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.status.Location = new System.Drawing.Point(8, 9);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(355, 13);
            this.status.TabIndex = 0;
            this.status.Text = "You have 0 checks totaling $000,000,000.00";
            //
            //checknum
            //
            this.checknum.Location = new System.Drawing.Point(126, 83);
            this.checknum.Name = "checknum";
            this.checknum.Properties.Appearance.Options.UseTextOptions = true;
            this.checknum.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.checknum.Properties.Mask.BeepOnError = true;
            this.checknum.Properties.Mask.EditMask = "f0";
            this.checknum.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.checknum.Size = new System.Drawing.Size(88, 20);
            this.checknum.TabIndex = 4;
            //
            //GroupControl2
            //
            this.GroupControl2.Controls.Add(this.print_checks);
            this.GroupControl2.Location = new System.Drawing.Point(16, 120);
            this.GroupControl2.Name = "GroupControl2";
            this.GroupControl2.Size = new System.Drawing.Size(200, 72);
            this.GroupControl2.TabIndex = 5;
            this.GroupControl2.Text = "Print";
            //
            //print_checks
            //
            this.print_checks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.print_checks.EditValue = 1;
            this.print_checks.Location = new System.Drawing.Point(2, 22);
            this.print_checks.Name = "print_checks";
            this.print_checks.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
			new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "All Checks"),
			new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Selected Checks")
		});
            this.print_checks.Size = new System.Drawing.Size(196, 48);
            this.print_checks.TabIndex = 0;
            //
            //Button_All
            //
            this.Button_All.Location = new System.Drawing.Point(296, 80);
            this.Button_All.Name = "Button_All";
            this.Button_All.Size = new System.Drawing.Size(75, 23);
            this.Button_All.TabIndex = 6;
            this.Button_All.Text = "Print";
            //
            //Button_Print
            //
            this.Button_Print.Location = new System.Drawing.Point(296, 120);
            this.Button_Print.Name = "Button_Print";
            this.Button_Print.Size = new System.Drawing.Size(75, 23);
            this.Button_Print.TabIndex = 7;
            this.Button_Print.Text = "Print First";
            //
            //Button_Cancel
            //
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(296, 160);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 8;
            this.Button_Cancel.Text = "Cancel";
            //
            //PrintAllForm
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(384, 206);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_Print);
            this.Controls.Add(this.Button_All);
            this.Controls.Add(this.GroupControl2);
            this.Controls.Add(this.checknum);
            this.Controls.Add(this.GroupControl1);
            this.Controls.Add(this.BankLookup);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.LabelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "PrintAllForm";
            this.Text = "Print Checks";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.BankLookup.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GroupControl1).EndInit();
            this.GroupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)this.checknum.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GroupControl2).EndInit();
            this.GroupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)this.print_checks.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        /// <summary>
        /// Generate a warning that we have left checks pending
        /// </summary>
        private void Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ShouldShowWarning && DebtPlus.Data.Forms.MessageBox.Show(Properties.Resources.WARNING + Environment.NewLine + Environment.NewLine + Properties.Resources.YouIndicatedThatTheChecksWereDestroyedThereAreNowPendingChecksToBe + Environment.NewLine + Properties.Resources.ReprintedAreYouReallySureThatYouWantToLeaveTheseChecksInThisStatus + Environment.NewLine + Properties.Resources.NormallyTheChecksShouldBeReprintedBeforeYouTerminateThisApplication + Environment.NewLine + Environment.NewLine + Properties.Resources.PleaseTalkToYourSupervisorBeforeAnsweringThisQuestion + Environment.NewLine + Properties.Resources.ItIsACriticalCondition, Properties.Resources.CAUTIONAreYouReallySure, MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Register the event handlers as needed
        /// </summary>
        private void RegisterHandlers()
        {
            Load                          += PrintAllForm_Load;
            FormClosing                   += Form_FormClosing;
            BankLookup.EditValueChanging  += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            BankLookup.EditValueChanged   += BankLookup_EditValueChanged;
            Button_All.Click              += Button_Click;
            Button_Print.Click            += Button_Click;
            print_checks.EditValueChanged += print_checks_EditValueChanged;
        }

        /// <summary>
        /// Remove the event handler registration
        /// </summary>
        private void RemoveHandlers()
        {
            Load                          -= PrintAllForm_Load;
            FormClosing                   -= Form_FormClosing;
            BankLookup.EditValueChanging  -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            BankLookup.EditValueChanged   -= BankLookup_EditValueChanged;
            Button_All.Click              -= Button_Click;
            Button_Print.Click            -= Button_Click;
            print_checks.EditValueChanged -= print_checks_EditValueChanged;
        }

        /// <summary>
        /// Process the form load
        /// </summary>

        private void PrintAllForm_Load(object sender, EventArgs e)
        {
            RemoveHandlers();
            try
            {
                // Load the list of bank accounts
                BankListLookupEdit_Load();

                // Update the button status
                Button_Print.Tag = "First";
                Button_Print.Text = "Print First";

                Button_All.Tag = "Print";
                Button_All.Text = "Print";

                Button_Cancel.Text = "Cancel";
                Button_Cancel.Tag = "Cancel";

                // Go to the "PRINT ALL" mode
                print_checks.EditValue = 1;
                MarkedChecksIdentifier = string.Empty;
                SwitchModes();

                // Load the check information for the first check, etc.
                RefreshForm(MarkedChecksIdentifier, true);
            }
            finally
            {
                // Re-register the events
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Refresh the form information as needed
        /// </summary>
        private void RefreshForm(string checksIdentifier, bool resetchecknum)
        {
            Int32? bank = DebtPlus.Utils.Nulls.v_Int32(BankLookup.EditValue);

            // We need to have a valid bank reference.
            if (! bank.HasValue)
            {
                status.Text = "Please choose a bank from the list.";
                Button_All.Enabled = false;
                Button_Print.Enabled = false;
                checknum.Enabled = false;
                print_checks.Enabled = false;
                ShouldShowWarning = false;
                return;
            }

            try
            {
                using (var cm = new CursorManager())
                {
                    using (var bc = new BusinessContext())
                    {
                        // If the check number is to be replaced then do so now.
                        if (resetchecknum)
                        {
                            checknum.EditValue = bc.banks.Where(s => s.Id == bank.Value).Select(s => s.checknum).FirstOrDefault();
                        }

                        // Check list information
                        Int32 checkCount = 0;
                        decimal checkBalance = 0m;

                        // If there is a printMarker string then use it in the selection criteria.
                        if (!string.IsNullOrEmpty(checksIdentifier))
                        {
                            var col = (from tr in bc.registers_trusts
                                       where tr.cleared == 'P' && PrintChecksClass.checkTranTypes.Contains(tr.tran_type)
                                       && tr.bank == bank.Value
                                       && tr.sequence_number == checksIdentifier
                                       select new { tr.amount }).ToList();

                            checkCount = col.Count();
                            checkBalance = col.Where(s => s.amount > 0M).Sum(s => s.amount);
                        }
                        else
                        {
                            var col = (from tr in bc.registers_trusts
                                       where tr.cleared == 'P' && PrintChecksClass.checkTranTypes.Contains(tr.tran_type)
                                       && tr.bank == bank.Value
                                       select new { tr.amount }).ToList();

                            checkCount = col.Count();
                            checkBalance = col.Where(s => s.amount > 0M).Sum(s => s.amount);
                        }

                        // If there are no checks then indicate as much
                        if (checkCount <= 0 || checkBalance <= 0M)
                        {
                            status.Text = Properties.Resources.YouHaveNoChecksToPrintChooseANewBankOrPressCANCEL;
                            Button_All.Enabled = false;
                            Button_Print.Enabled = false;
                            checknum.Enabled = false;
                            print_checks.Enabled = false;
                            ShouldShowWarning = false;
                            return;
                        }

                        status.Text = string.Format(Properties.Resources.YouHave0DChecksToPrintTotalling1C2, checkCount, checkBalance);

                        // Enable the buttons
                        Button_All.Enabled = true;
                        Button_Print.Enabled = true;
                        checknum.Enabled = true;
                        print_checks.Enabled = true;
                        checknum.Enabled = true;
                    }
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading trust register");
            }
        }

        /// <summary>
        /// Load the bank information
        /// </summary>
        private void BankListLookupEdit_Load()
        {
            BankLookup.Properties.PopupWidth = 300;

            var colBanks = DebtPlus.LINQ.Cache.bank.getList().FindAll(s => s.type == "C" && s.ActiveFlag);
            BankLookup.Properties.DataSource = colBanks;

            var q = colBanks.Find(s => s.Default);
            if (q != null)
            {
                BankLookup.EditValue = q.Id;
            }
        }

        /// <summary>
        /// A change in the bank account refreshes the list of checks
        /// </summary>
        private void BankLookup_EditValueChanged(object sender, EventArgs e)
        {
            MarkedChecksIdentifier = string.Empty;
            RefreshForm(string.Empty, true);
        }

        /// <summary>
        /// Change the text of the button when the edit value changes
        /// </summary>
        private void print_checks_EditValueChanged(object sender, EventArgs e)
        {
            SwitchModes();
        }

        /// <summary>
        /// Switch the mode processing when the selection radio button is changed.
        /// </summary>

        private void SwitchModes()
        {
            // If the radio button is on "selected checks" then change the button
            if (Convert.ToInt32(print_checks.EditValue) == 0)
            {
                Button_Print.Text = "Select...";
                Button_Print.Tag = "Select";
            }
            else
            {
                Button_Print.Text = "Print First";
                Button_Print.Tag = "First";
            }
        }

        /// <summary>
        /// Process the request to select specific checks to be printed now
        /// </summary>
        private void Button_Click(object sender, EventArgs e)
        {
            switch (Convert.ToString(((SimpleButton)sender).Tag))
            {
                case "Select":
                    Button_Select_Click(sender, e);
                    break;

                case "First":
                    Button_First_Click(sender, e);
                    break;

                case "Print":
                    Button_Print_Click(sender, e);
                    break;
            }
        }

        /// <summary>
        /// Process the request to select specific checks to be printed now
        /// </summary>
        private void Button_Select_Click(object sender, EventArgs e)
        {
            MarkedChecksIdentifier = string.Empty;
            using (SelectChecksForm frm = new SelectChecksForm(Convert.ToInt32(BankLookup.EditValue)))
            {
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    MarkedChecksIdentifier = frm.MarkedChecksIdentifier;
                    Button_Print.Text = "Print First";
                    Button_Print.Tag = "First";
                }
            }

            // Refresh the information based upon the selected checks.
            RefreshForm(MarkedChecksIdentifier, false);
        }

        /// <summary>
        /// Print all of the indicated checks in the list
        /// </summary>
        private void Button_Print_Click(object sender, EventArgs e)
        {
            // Find the bank ID
            Int32? bank = DebtPlus.Utils.Nulls.v_Int32(BankLookup.EditValue);
            if (!bank.HasValue)
            {
                return;
            }

            // Obtain the check number that we are going to use
            Int64 startingCheckNum;
            if (! Int64.TryParse(checknum.Text.Trim(), out startingCheckNum) || startingCheckNum <= 0L)
            {
                return;
            }

            // Generate the parameters to print the checks.
            PrintChecksClass.VoidParameters printParam = new PrintChecksClass.VoidParameters() { BankID = bank.Value, MarkerString = PrintChecksClass.GenerateNewMarker() };

            // Generate a transaction for this sequence. We need to update the bank information without having someone else change it.
            string connectionString = DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString;
            using (var cn = new System.Data.SqlClient.SqlConnection(connectionString))
            {
                cn.Open();
                System.Data.SqlClient.SqlTransaction txn = cn.BeginTransaction(IsolationLevel.RepeatableRead);
                using (var bc = new BusinessContext(cn))
                {
                    bc.Transaction = txn;

                    // Read the bank record that we are going to update.
                    var bankRecord = bc.banks.Where(s => s.Id == printParam.BankID).FirstOrDefault();
                    if (bankRecord == null)
                    {
                        try { txn.Rollback(); } catch { }
                        return;
                    }

                    bankRecord.checknum   = startingCheckNum;

                    // If we wish to pre-fill the starting check number for the print run then do it here
                    // printParam.FirstCheck = startingCheckNum;

                    // Find the items that we want from the trust register to be printed
                    System.Collections.Generic.List<registers_trust> colRecords;

                    if (string.IsNullOrEmpty(MarkedChecksIdentifier))
                    {
                        colRecords = (from tr in bc.registers_trusts
                                      where tr.bank == printParam.BankID && tr.cleared == 'P' && PrintChecksClass.checkTranTypes.Contains(tr.tran_type)
                                      orderby tr.check_order, tr.creditor, tr.client, tr.Id
                                      select tr).ToList();
                    }
                    else
                    {
                        colRecords = (from tr in bc.registers_trusts
                                      where tr.bank == printParam.BankID && tr.cleared == 'P' && PrintChecksClass.checkTranTypes.Contains(tr.tran_type)
                                      && tr.sequence_number == MarkedChecksIdentifier
                                      orderby tr.check_order, tr.creditor, tr.client, tr.Id
                                      select tr).ToList();
                    }

                    // There needs to be some items to be printed
                    if (colRecords.Count < 1)
                    {
                        try { txn.Rollback(); } catch { }
                        DebtPlus.Data.Forms.MessageBox.Show(Properties.Resources.TheChecksAreNoLongerPendingToBePrintedPleaseReviewTheListAgain, "Data Synchronization", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }

                    // Process the record collection and assign check numbers to the items
                    foreach (registers_trust trItem in colRecords)
                    {
                        trItem.cleared         = ' ';
                        trItem.sequence_number = printParam.MarkerString;
                        trItem.checknum        = bankRecord.checknum;
                        bankRecord.checknum   += 1L;
                    }

                    // Submit the changes to the database and complete the transaction
                    bc.SubmitChanges(System.Data.Linq.ConflictMode.FailOnFirstConflict);

                    txn.Commit();
                    txn.Dispose();
                    txn = null;
                }
            }

            // Now, print the checks in the list
            ParentClass.PrintMarkedChecks(printParam);

            // Indicate that we no longer need the warning message
            ShouldShowWarning = false;

            // Ask if a check range is to be voided
            using (VoidCheckRangeForm frm = new VoidCheckRangeForm(printParam))
            {
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    // Void the items in the check run
                    ParentClass.VoidCheck(printParam);
                    ShouldShowWarning = true;
                }
            }

            // Remove event handling for now
            RemoveHandlers();
            try
            {
                // Go to the "PRINT ALL" mode
                print_checks.EditValue = 1;
                MarkedChecksIdentifier = string.Empty;
                SwitchModes();

                // Load the check information for the first check, etc.
                RefreshForm(MarkedChecksIdentifier, true);
            }
            finally
            {
                // Re-register the events
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Print the first check in the pending list
        /// </summary>
        private void Button_First_Click(object sender, EventArgs e)
        {
            string originalMarker;
            Int32 printedTrustRegister;

            // Find the bank ID
            Int32? bank = DebtPlus.Utils.Nulls.v_Int32(BankLookup.EditValue);
            if (!bank.HasValue)
            {
                return;
            }

            // Obtain the check number that we are going to use
            Int64 startingCheckNum;
            if (! Int64.TryParse(checknum.Text.Trim(), out startingCheckNum) || startingCheckNum <= 0L)
            {
                return;
            }

            // Generate a new printMarker for the first check that we are going to print.
            PrintChecksClass.VoidParameters printParam = new PrintChecksClass.VoidParameters() { BankID = bank.Value, MarkerString = PrintChecksClass.GenerateNewMarker() };
            try
            {
                string connectionString = DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString;
                using (var cn = new System.Data.SqlClient.SqlConnection(connectionString))
                {
                    cn.Open();
                    System.Data.SqlClient.SqlTransaction txn = cn.BeginTransaction(IsolationLevel.RepeatableRead);
                    using (var bc = new BusinessContext(cn))
                    {
                        bc.Transaction = txn;

                        // Read the bank record that we are going to update.
                        var bankRecord = bc.banks.Where(s => s.Id == printParam.BankID).FirstOrDefault();
                        if (bankRecord == null)
                        {
                            try { txn.Rollback(); } catch { }
                            return;
                        }

                        bankRecord.checknum = startingCheckNum;

                        // Determine the trust register entry that is the one that we are going to print first.
                        registers_trust q;
                        if (!string.IsNullOrEmpty(MarkedChecksIdentifier))
                        {
                            q = (from tr in bc.registers_trusts
                                 where tr.cleared == 'P' && PrintChecksClass.checkTranTypes.Contains(tr.tran_type) && tr.bank == printParam.BankID
                                 && tr.sequence_number == MarkedChecksIdentifier
                                 orderby tr.creditor, tr.client, tr.Id
                                 select tr).FirstOrDefault();
                        }
                        else
                        {
                            q = (from tr in bc.registers_trusts
                                 where tr.cleared == 'P' && PrintChecksClass.checkTranTypes.Contains(tr.tran_type) && tr.bank == printParam.BankID
                                 orderby tr.creditor, tr.client, tr.Id
                                 select tr).FirstOrDefault();
                        }

                        // If there is no check then we can not continue.
                        if (q == null)
                        {
                            try { txn.Rollback(); } catch { }
                            DebtPlus.Data.Forms.MessageBox.Show(Properties.Resources.TheChecksAreNoLongerPendingToBePrintedPleaseReviewTheListAgain, "Data Synchronization", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return;
                        }

                        // Save the ID of the record and the original (current) printMarker string. We may need to put it back later.
                        originalMarker       = q.sequence_number;
                        printedTrustRegister = q.Id;

                        // Change the printMarker to the new item that designates the check list to be printed now.
                        // There should only be one item in the list.
                        q.sequence_number    = printParam.MarkerString;
                        q.checknum           = bankRecord.checknum;
                        bankRecord.checknum += 1L;

                        // Submit the normal changes to the database at this time.
                        bc.SubmitChanges();
                        txn.Commit();
                        txn.Dispose();
                        txn = null;
                    }
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                return;
            }

            // Reset the warning flag
            ShouldShowWarning = false;

            // Print the checks in the printMarker list.
            ParentClass.PrintMarkedChecks(printParam);

            // Ask if a check range is to be voided
            if (DebtPlus.Data.Forms.MessageBox.Show(Properties.Resources.WasTheCheckPrintedCorrectly, "Successful Completion", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
            {
                // Set the warning that we voided checks
                ShouldShowWarning = true;

                // Since we marked the one entry that we printed before, we can just void that single entry now.
                ParentClass.VoidCheck(printParam);

                // We need to replace the original printMarker on the trust entry so that it is back in the original list.
                try
                {
                    using (var bc = new BusinessContext())
                    {
                        var q = bc.registers_trusts.Where(s => s.Id == printedTrustRegister).FirstOrDefault();
                        if (q != null)
                        {
                            q.sequence_number = originalMarker;
                            bc.SubmitChanges();
                        }
                    }
                }

                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error voiding print first check");
                }
            }

            // Under all cases, refresh the information being displayed to the user.
            RefreshForm(MarkedChecksIdentifier, true);
        }

#if false
        /// <summary>
        /// Update the check number in the database based upon the entry given.
        /// </summary>
        public void Updatechecknum()
        {
            // Do the update with the values as given
            Int64 newchecknum = Convert.ToInt64(checknum.EditValue, CultureInfo.InvariantCulture);
            Int32 bank = Convert.ToInt32(BankLookup.EditValue);

            if (newchecknum > 0 && bank > 0)
            {
                try
                {
                    // Update the check number in the banks table
                    using (var bc = new BusinessContext())
                    {
                        var q = bc.banks.Where(s => s.Id == bank).FirstOrDefault();
                        if (q != null)
                        {
                            q.checknum = newchecknum;
                            bc.SubmitChanges();
                        }
                    }
                }

                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
            }
        }
#endif
    }
}
