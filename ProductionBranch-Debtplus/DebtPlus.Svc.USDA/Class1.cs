﻿#pragma warning disable 1591

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Svc.USDA
{
    internal static class sharedFunctions
    {
        internal static System.Nullable<decimal> parseDecimal(string inputString)
        {
            if (!string.IsNullOrWhiteSpace(inputString))
            {
                // Decode the decimal number
                decimal result;
                if (decimal.TryParse(inputString, out result))
                {
                    return new decimal?(result);
                }
            }

            return new decimal?();
        }
    }

    /// <summary>
    /// Error messages are of this type
    /// </summary>
    public class ErrorResponseMessageType
    {
        /// <summary>
        /// Code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Type
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Text
        /// </summary>
        public string Text { get; set; }
    }

    /// <summary>
    /// Error responses are of this type
    /// </summary>
    public class ErrorResponseType
    {
        /// <summary>
        /// EngineId
        /// </summary>
        public string EngineId { get; set; }
        /// <summary>
        /// HostName
        /// </summary>
        public string HostName { get; set; }
        /// <summary>
        /// MaxSeverity
        /// </summary>
        public string MaxSeverity { get; set; }
        /// <summary>
        /// LogFile
        /// </summary>
        public string LogFile { get; set; }
        /// <summary>
        /// Class
        /// </summary>
        public string Class { get; set; }
        /// <summary>
        /// Module
        /// </summary>
        public string Module { get; set; }
        /// <summary>
        /// Severity
        /// </summary>
        public string Severity { get; set; }
        /// <summary>
        /// Time
        /// </summary>
        public string Time { get; set; }
        /// <summary>
        /// Message
        /// </summary>
        public ErrorResponseMessageType Message { get; private set; }

        /// <summary>
        /// ErrorResponseType
        /// </summary>
        public ErrorResponseType()
        {
            Message = new ErrorResponseMessageType();
        }

        /// <summary>
        /// AdjustedType
        /// </summary>
        public ErrorResponseType(ref System.Xml.XmlReader xmlRdr) : this()
        {
            if (xmlRdr.HasAttributes)
            {
                xmlRdr.MoveToFirstAttribute();

                // Process the attribute fields
                while (!xmlRdr.EOF)
                {
                    switch(xmlRdr.NodeType)
                    {
                        case System.Xml.XmlNodeType.Attribute:
                            switch(xmlRdr.Name)
                            {
                                case "EngineId":
                                    EngineId = xmlRdr.Value;
                                    break;

                                case "HostName":
                                    HostName = xmlRdr.Value;
                                    break;

                                case "MaxSeverity":
                                    MaxSeverity = xmlRdr.Value;
                                    break;

                                case "LogFile":
                                    LogFile = xmlRdr.Value;
                                    break;

                                case "Class":
                                    Class = xmlRdr.Value;
                                    break;

                                case "Module":
                                    Module = xmlRdr.Value;
                                    break;

                                case "Severity":
                                    Severity = xmlRdr.Value;
                                    break;

                                case "Time":
                                    Time = xmlRdr.Value;
                                    break;

                                default:
                                    break;
                            }
                            break;

                        default:
                            break;
                    }

                    if (!xmlRdr.MoveToNextAttribute())
                    {
                        break;
                    }
                }
            }
        }
    }

    /// <summary>
    /// A section 502 response looks like this.
    /// </summary>
    public class Section502Type
    {
        /// <summary>
        /// Section502Type
        /// </summary>
        public Section502Type()
        {
        }

        /// <summary>
        /// Section502Type
        /// </summary>
        public Section502Type(ref System.Xml.XmlReader xmlRdr)
            : this()
        {
            if (xmlRdr.HasAttributes)
            {
                xmlRdr.MoveToFirstAttribute();

                // Process the attribute fields
                while (!xmlRdr.EOF)
                {
                    switch(xmlRdr.NodeType)
                    {
                        case System.Xml.XmlNodeType.Attribute:
                            switch(xmlRdr.Name)
                            {
                                case "MaximumAdjusted":
                                    MaximumAdjusted = sharedFunctions.parseDecimal(xmlRdr.Value);
                                    break;

                                case "Eligible":
                                    Eligible = xmlRdr.Value;
                                    break;

                                default:
                                    break;
                            }
                            break;

                        default:
                            break;
                    }

                    if (!xmlRdr.MoveToNextAttribute())
                    {
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// MaximumAdjusted
        /// </summary>
        public System.Nullable<decimal> MaximumAdjusted { get; set; }
        /// <summary>
        /// Eligible
        /// </summary>
        public string Eligible { get; set; }
    }

    /// <summary>
    /// A Property response looks like this
    /// </summary>
    public class PropertyType
    {
        /// <summary>
        /// PropertyType
        /// </summary>
        public PropertyType()
        {
        }

        /// <summary>
        /// PropertyType
        /// </summary>
        public PropertyType(ref System.Xml.XmlReader xmlRdr) : this()
        {
            if (xmlRdr.HasAttributes)
            {
                xmlRdr.MoveToFirstAttribute();

                // Process the attribute fields
                while (!xmlRdr.EOF)
                {
                    switch (xmlRdr.NodeType)
                    {
                        case System.Xml.XmlNodeType.Attribute:
                            switch (xmlRdr.Name)
                            {
                                case "Eligibility":
                                    Eligibility = xmlRdr.Value;
                                    break;

                                case "MapURL":
                                    MapURL = xmlRdr.Value;
                                    break;

                                default:
                                    break;
                            }
                            break;

                        default:
                            break;
                    }

                    if (!xmlRdr.MoveToNextAttribute())
                    {
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Eligibility
        /// </summary>
        public string Eligibility { get; set; }
        /// <summary>
        /// MapURL
        /// </summary>
        public string MapURL { get; set; }
    }

    /// <summary>
    /// An income response looks like this
    /// </summary>
    public class AdjustedType
    {
        /// <summary>
        /// AdjustedType
        /// </summary>
        public AdjustedType()
        {
        }

        /// <summary>
        /// AdjustedType
        /// </summary>
        public AdjustedType(ref System.Xml.XmlReader xmlRdr)
            : this()
        {
            if (xmlRdr.HasAttributes)
            {
                xmlRdr.MoveToFirstAttribute();

                // Process the attribute fields
                while (!xmlRdr.EOF)
                {
                    switch(xmlRdr.NodeType)
                    {
                        case System.Xml.XmlNodeType.Attribute:
                            switch(xmlRdr.Name)
                            {
                                case "AnnualIncome":
                                    AnnualIncome = sharedFunctions.parseDecimal(xmlRdr.Value);
                                    break;
                                    
                                case "TotalDeduction":
                                    TotalDeduction = sharedFunctions.parseDecimal(xmlRdr.Value);
                                    break;
                                    
                                case "AdjustedIncome":
                                    AdjustedIncome = sharedFunctions.parseDecimal(xmlRdr.Value);
                                    break;
                                    
                                case "ElderlyDeduction":
                                    ElderlyDeduction = sharedFunctions.parseDecimal(xmlRdr.Value);
                                    break;

                                case "YoungDeduction":
                                    YoungDeduction = sharedFunctions.parseDecimal(xmlRdr.Value);
                                    break;

                                default:
                                    break;
                            }
                            break;

                        default:
                            break;
                    }

                    if (!xmlRdr.MoveToNextAttribute())
                    {
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// AnnualIncome
        /// </summary>
        public System.Nullable<decimal> AnnualIncome { get; private set; }
        /// <summary>
        /// TotalDeduction
        /// </summary>
        public System.Nullable<decimal> TotalDeduction { get; private set; }
        /// <summary>
        /// AdjustedIncome
        /// </summary>
        public System.Nullable<decimal> AdjustedIncome { get; private set; }
        /// <summary>
        /// ElderlyDeduction
        /// </summary>
        public System.Nullable<decimal> ElderlyDeduction { get; private set; }
        /// <summary>
        /// YoungDeduction
        /// </summary>
        public System.Nullable<decimal> YoungDeduction { get; private set; }
    }

    /// <summary>
    /// General submission and response processing
    /// </summary>
    public class Submission
    {
        /// <summary>
        /// Initialize the new class
        /// </summary>
        public Submission()
        {
        }

        /// <summary>
        /// Send the submission request to USDA and retrieve their response.
        /// </summary>
        /// <param name="requestType"></param>
        /// <param name="inputRequest"></param>
        /// <returns></returns>
        public virtual string Submit(string requestType, string inputRequest)
        {
            // Host for the remote server
            string hostAddress = "eligibility.sc.egov.usda.gov";

            // Correct the request string to include the proper header and footer information
            string webRequest = "<?xml version=\"1.0\"?>"
                                + "<Eligibility xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"/var/lib/tomcat5/webapps/eligibility/Eligibilitywsdl.xsd\">"
                                + inputRequest
                                + "</Eligibility>";

            // Append the server information to the request structure
            webRequest = string.Format("http://{0}/eligibility/eligibilityservice?eligibilityType={1}&requestString={2}", hostAddress, requestType, EncodeForWeb(webRequest));

            // Submit the request to the remote server
            System.Net.WebRequest req = System.Net.WebRequest.Create(webRequest);
            req.Credentials = System.Net.CredentialCache.DefaultCredentials;
            ((System.Net.HttpWebRequest)req).UserAgent = ".NET Framework Client";
            req.Method = "GET";
            req.ContentLength = 0;
            req.ContentType = "application/x-www-form-urlencoded";

            // Read the response from the remote system for our request
            System.Net.WebResponse response = req.GetResponse();
            System.IO.Stream dataStream = response.GetResponseStream();
            System.IO.StreamReader reader = new System.IO.StreamReader(dataStream);

            // Read the data into the local buffer
            string responseFromServer = reader.ReadToEnd();

            // At the end of the stream, close it.
            reader.Close();
            dataStream.Close();
            response.Close();

            // Return the server's response buffer
            return responseFromServer;
        }

        private string EncodeForWeb(string input)
        {
            var sb = new System.Text.StringBuilder();
            foreach (char ch in input)
            {
                if (char.IsLetterOrDigit(ch))
                {
                    sb.Append(ch);
                }
                else
                {
                    sb.AppendFormat("%{0:X2}", ((byte)ch) & 0xFF);
                }
            }
            return sb.ToString();
        }
    }

    /// <summary>
    /// Property submission and response processing
    /// </summary>
    public class Property : Submission
    {
        /// <summary>
        /// Request for the property submission
        /// </summary>
        public class PropertyRequest
        {
            /// <summary>
            /// PropertyRequest
            /// </summary>
            public PropertyRequest()
            {
                Program = "RBS";
                County = "";
            }

            /// <summary>
            /// PropertyRequest
            /// </summary>
            /// <param name="addr"></param>
            public PropertyRequest(DebtPlus.LINQ.address addr)
                : this()
            {
                StreetAddress1 = DebtPlus.LINQ.address.getAddressLine1(addr.house, addr.direction, addr.street, addr.suffix, null, null);
                StreetAddress2 = addr.address_line_2;
                StreetAddress3 = addr.address_line_3;

                City = addr.city;
                if (addr.PostalCode.Length > 5)
                {
                    Zip = addr.PostalCode.Substring(0, 5);
                }
                else
                {
                    Zip = addr.PostalCode;
                }

                // Find the state from the states table
                var q = DebtPlus.LINQ.Cache.state.getList().Find(s => s.Id == addr.state);
                if (q != null)
                {
                    State = q.MailingCode;
                }
            }

            /// <summary>
            /// StreetAddress1
            /// </summary>
            public string StreetAddress1 { get; set; }
            /// <summary>
            /// StreetAddress2
            /// </summary>
            public string StreetAddress2 { get; set; }
            /// <summary>
            /// StreetAddress3
            /// </summary>
            public string StreetAddress3 { get; set; }
            /// <summary>
            /// City
            /// </summary>
            public string City { get; set; }
            /// <summary>
            /// State
            /// </summary>
            public string State { get; set; }
            /// <summary>
            /// County
            /// </summary>
            public string County { get; set; }
            /// <summary>
            /// Zip
            /// </summary>
            public string Zip { get; set; }
            /// <summary>
            /// Program
            /// </summary>
            public string Program { get; set; }

            /// <summary>
            /// Serialize
            /// </summary>
            /// <returns></returns>
            public string Serialize()
            {
                return Serialize("PropertyRequest");
            }

            /// <summary>
            /// Serialize
            /// </summary>
            /// <param name="AttributeName"></param>
            /// <returns></returns>
            public string Serialize(string AttributeName)
            {
                // Extract the information from the structures into the XML document
                StringBuilder sb = new StringBuilder();

                using (var txt = new System.IO.StringWriter(sb))
                {
                    System.Xml.XmlTextWriter XmlWriter = new System.Xml.XmlTextWriter(txt);
                    try
                    {
                        XmlWriter.Formatting = System.Xml.Formatting.None;

                        // Generate the property request element
                        XmlWriter.WriteStartElement(AttributeName);
                        if (!string.IsNullOrEmpty(StreetAddress1)) XmlWriter.WriteAttributeString("StreetAddress1", StreetAddress1);
                        if (!string.IsNullOrEmpty(StreetAddress2)) XmlWriter.WriteAttributeString("StreetAddress2", StreetAddress2);
                        if (!string.IsNullOrEmpty(StreetAddress3)) XmlWriter.WriteAttributeString("StreetAddress3", StreetAddress3);
                        if (!string.IsNullOrEmpty(City)) XmlWriter.WriteAttributeString("City", City);
                        if (!string.IsNullOrEmpty(State)) XmlWriter.WriteAttributeString("State", State);
                        if (!string.IsNullOrEmpty(Zip)) XmlWriter.WriteAttributeString("Zip", Zip);
                        XmlWriter.WriteAttributeString("County", County);
                        if (!string.IsNullOrEmpty(Program)) XmlWriter.WriteAttributeString("Program", Program);
                        XmlWriter.WriteEndElement();
                    }

                    finally
                    {
                        XmlWriter.Close();
                        txt.Close();
                    }
                }

                // Return the resulting string buffer
                return sb.ToString();
            }
        }

        /// <summary>
        /// Response to the property submission
        /// </summary>
        public class PropertyResponse
        {
            /// <summary>
            /// Initialize the new class instance
            /// </summary>
            public PropertyResponse()
            {
                Adjusted = new AdjustedType();
                Property = new PropertyType();
                Section502Guaranted = new Section502Type();
                Section502Direct = new Section502Type();
                ErrorResponse = new ErrorResponseType();
            }

            /// <summary>
            /// Initialize the new class instance
            /// </summary>
            public PropertyResponse(string responseString)
                : this()
            {
                using (var fs = new System.IO.StringReader(responseString))
                {
                    var settings = new System.Xml.XmlReaderSettings();
                    settings.ConformanceLevel = System.Xml.ConformanceLevel.Document;
                    settings.IgnoreComments = true;
                    settings.IgnoreWhitespace = true;
                    settings.IgnoreProcessingInstructions = true;

                    var xmlRdr = System.Xml.XmlTextReader.Create(fs, settings);
                    while (xmlRdr.Read())
                    {
                        if (xmlRdr.NodeType == System.Xml.XmlNodeType.Element || xmlRdr.NodeType == System.Xml.XmlNodeType.EndElement)
                        {
                            if (xmlRdr.Name == "Eligibility")
                            {
                                xmlRdr.MoveToContent();
                                Deserialize(ref xmlRdr);
                                continue;
                            }
                        }
                    }
                }
            }

            /// <summary>
            /// De-serialize the input string
            /// </summary>
            /// <param name="xmlRdr"></param>
            public PropertyResponse(ref System.Xml.XmlReader xmlRdr)
                : this()
            {
                Deserialize(ref xmlRdr);
            }

            /// <summary>
            /// De-serialize the input string
            /// </summary>
            /// <param name="xmlRdr"></param>
            private void Deserialize(ref System.Xml.XmlReader xmlRdr)
            {
                while (xmlRdr.Read())
                {
                    if (xmlRdr.NodeType == System.Xml.XmlNodeType.Element)
                    {
                        switch (xmlRdr.Name)
                        {
                            case "Adjusted":
                                Adjusted = new AdjustedType(ref xmlRdr);
                                break;

                            case "Property":
                                Property = new PropertyType(ref xmlRdr);
                                break;

                            case "Section502Guaranted":
                                Section502Guaranted = new Section502Type(ref xmlRdr);
                                break;

                            case "Section502Direct":
                                Section502Direct = new Section502Type(ref xmlRdr);
                                break;

                            case "ErrorResponse":
                                ErrorResponse = new ErrorResponseType(ref xmlRdr);
                                break;

                            default:
                                break;
                        }
                    }
                }
            }

            /// <summary>
            /// Adjusted
            /// </summary>
            public AdjustedType Adjusted { get; private set; }
            /// <summary>
            /// Section502Guaranted
            /// </summary>
            public Section502Type Section502Guaranted { get; private set; }
            /// <summary>
            /// Section502Direct
            /// </summary>
            public Section502Type Section502Direct { get; private set; }
            /// <summary>
            /// Property
            /// </summary>
            public PropertyType Property { get; private set; }
            /// <summary>
            /// ErrorResponse
            /// </summary>
            public ErrorResponseType ErrorResponse { get; private set; }
        }

        /// <summary>
        /// Initialize the new class instance
        /// </summary>
        public Property()
            : base()
        {
        }

        /// <summary>
        /// Submit the request to the remote server and obtain the response
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public PropertyResponse Submit(PropertyRequest req)
        {
            // Return the response from the remote server
            return new PropertyResponse(base.Submit("Property", req.Serialize()));
        }
    }

    /// <summary>
    /// Income submission and response processing
    /// </summary>
    public class Income : Submission
    {
        /// <summary>
        /// Response structure for an income request
        /// </summary>
        public class IncomeResponse
        {
            public AdjustedType Adjusted { get; private set; }
            public Section502Type Section502Guaranted { get; private set; }
            public Section502Type Section502Direct { get; private set; }
            public PropertyType Property { get; private set; }
            public ErrorResponseType ErrorResponse { get; private set; }

            /// <summary>
            /// Initialize the new class
            /// </summary>
            public IncomeResponse()
            {
                Adjusted = new AdjustedType();
                Property = new PropertyType();
                Section502Guaranted = new Section502Type();
                Section502Direct = new Section502Type();
                ErrorResponse = new ErrorResponseType();
            }

            /// <summary>
            /// Initialize the new class
            /// </summary>
            /// <param name="responseString">XML Response string from the server at USDA</param>
            public IncomeResponse(string responseString)
                : this()
            {
            }
        }

        /// <summary>
        /// IncomeRequest
        /// </summary>
        public class IncomeRequest
        {
            public class FinancialRatesType
            {
                public FinancialRatesType()
                {
                    PaymentIncomeRatio = null;
                    DebtIncomeRatio = null;
                    ElderlyDeductionAmount = null;
                    YoungDeductionAmount = null;

                    FinancialRates = new FinancialRatesType();
                    MiscIncomeExpenses = new MiscIncomeExpensesType();
                    Income = new IncomeType[2];
                }

                public class IncomeType
                {
                    public IncomeType()
                    {
                        BaseIncome = null;
                        OverTime = null;
                        Bonus = null;
                        Commission = null;
                        Dividend = null;
                        NetRental = null;
                        OtherIncome = null;
                    }

                    public System.Nullable<decimal> BaseIncome { get; set; }
                    public System.Nullable<decimal> OverTime { get; set; }
                    public System.Nullable<decimal> Bonus { get; set; }
                    public System.Nullable<decimal> Commission { get; set; }
                    public System.Nullable<decimal> Dividend { get; set; }
                    public System.Nullable<decimal> NetRental { get; set; }
                    public System.Nullable<decimal> OtherIncome { get; set; }

                    /// <summary>
                    /// Serialize the class to an XML string
                    /// </summary>
                    /// <returns></returns>
                    public string Serialize()
                    {
                        return string.Empty;
                    }
                }

                public class MiscIncomeExpensesType
                {
                    public MiscIncomeExpensesType()
                    {
                        DisabilityExpenses = null;
                        ChildCareExpenses = null;
                        MedExpenses = null;
                        AllOtherIncome = null;
                    }

                    public System.Nullable<decimal> DisabilityExpenses { get; set; }
                    public System.Nullable<decimal> ChildCareExpenses { get; set; }
                    public System.Nullable<decimal> MedExpenses { get; set; }
                    public System.Nullable<decimal> AllOtherIncome { get; set; }

                    /// <summary>
                    /// Serialize the class to an XML string
                    /// </summary>
                    /// <returns></returns>
                    public string Serialize()
                    {
                        return string.Empty;
                    }
                }

                public System.Nullable<double> PaymentIncomeRatio { get; set; }
                public System.Nullable<double> DebtIncomeRatio { get; set; }
                public System.Nullable<decimal> ElderlyDeductionAmount { get; set; }
                public System.Nullable<decimal> YoungDeductionAmount { get; set; }
                public FinancialRatesType FinancialRates { get; private set; }
                public MiscIncomeExpensesType MiscIncomeExpenses { get; private set; }
                public IncomeType[] Income { get; private set; }

                /// <summary>
                /// Serialize the class to an XML string
                /// </summary>
                /// <returns></returns>
                public string Serialize()
                {
                    return string.Empty;
                }
            }

            public IncomeRequest()
            {
                State = null;
                County = null;
                MSA = null;
                NumberOfHousehold = null;
                ResidentUnder18 = null;
                ResidentOverAge62 = null;
                Disabled = null;
                FinancialRates = new FinancialRatesType();
            }

            public System.Nullable<int> State { get; set; }
            public System.Nullable<int> County { get; set; }
            public string MSA { get; set; }
            public System.Nullable<int> NumberOfHousehold { get; set; }
            public System.Nullable<int> ResidentUnder18 { get; set; }
            public System.Nullable<bool> ResidentOverAge62 { get; set; }
            public System.Nullable<bool> Disabled { get; set; }
            public FinancialRatesType FinancialRates { get; private set; }

            /// <summary>
            /// Serialize the class to an XML string
            /// </summary>
            /// <returns></returns>
            public string Serialize()
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Income
        /// </summary>
        public Income()
            : base()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Submit the request to the remote server and obtain the response
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public IncomeResponse Submit(IncomeRequest req)
        {
            // Return the response from the remote server
            return new IncomeResponse(base.Submit("Income", req.Serialize()));
        }
    }

    /// <summary>
    /// Income Property submission and response processing
    /// </summary>
    public class IncomeProperty : Submission
    {
        /// <summary>
        /// Income Property response structure from USDA
        /// </summary>
        public class IncomePropertyResponse
        {
            /// <summary>
            /// Initialize the empty class structure
            /// </summary>
            public IncomePropertyResponse()
            {
                throw new NotImplementedException();
            }

            /// <summary>
            /// De-serialize the input string to the class
            /// </summary>
            /// <param name="xmlString"></param>
            public IncomePropertyResponse(string xmlString) : this()
            {
            }
        }

        /// <summary>
        /// Request structure for the IncomeProperty operation
        /// </summary>
        public class IncomePropertyRequest
        {
            /// <summary>
            /// Initialize the new class
            /// </summary>
            public IncomePropertyRequest()
            {
                throw new NotImplementedException();
            }

            /// <summary>
            /// Serialize the class to an XML string
            /// </summary>
            /// <returns></returns>
            public string Serialize()
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Initialize the new class
        /// </summary>
        public IncomeProperty() : base()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Submit a request to the USDA system for a valid reply
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public IncomePropertyResponse Submit(IncomePropertyRequest req)
        {
            // Return the response from the remote server
            return new IncomePropertyResponse(base.Submit("IncomeProperty", req.Serialize()));
        }
    }
}
