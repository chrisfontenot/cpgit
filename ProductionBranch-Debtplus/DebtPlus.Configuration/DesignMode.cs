﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Configuration
{
    public static class DesignMode
    {
        /// <summary>
        /// Hack to make a valid test for the design mode.
        /// </summary>
        public static bool IsInDesignMode()
        {
            // Return TRUE if the process executing is VisualStudio
            return System.Reflection.Assembly.GetExecutingAssembly().Location.Contains("VisualStudio");
        }
    }
}
