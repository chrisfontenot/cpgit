﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Configuration
{
    public static class Constants
    {
        public readonly static string Company = @"DebtPlus, L.L.C.";
        public readonly static string Copyright = @"Copyright © 2000-2012 DebtPlus, L.L.C. -- All rights reserved";

        [Obsolete("Modules are not signed. Don't use.")]
        public readonly static string PublicKey = @"5745bf598a7493de";

        // Registry constants
        public readonly static string InstallationRegistryKey = @"Software\DebtPlus, L.L.C.\DebtPlus";
        public readonly static string Hkcu = @"HKEY_CURRENT_USER";
        public readonly static string Hklm = @"HKEY_LOCAL_MACHINE";
        
        //--------------------------------------------------------------------------------------------------------------
        // There are three different types of registry configuration types. Those in HKLM, those in HKCU and those
        // that case from HKLM to HKCU.  Please add new constants to the correct section below.
        // Please do NOT add leading or trailing slashes to these names as they confuse the system.

        internal const string OptionsFolderKey = @"Options";
        internal const string HistoryFolderKey = @"history";
        internal const string InstallationFolderKey = @"Installation";
        internal const string LocationsFolderKey = @"Locations";
        internal const string ValidationFolderKey = @"Validation";
        internal const string DatabaseFolderKey = @"Database";
        internal const string FormsFolderKey = @"Forms";
        internal const string PermissionsFolderKey = @"Permissions";
        internal const string SystemsFolderKey = @"Systems";
        internal const string DatabaseEnvVar = @"DEBTPLUS_KEY";

        //
        //   these are all in HKCU
        //

        public static string HKCU
        {
            get
            {
                return Hkcu;
            }
        }

        // Splash Screen registry constants
        internal readonly static string  ShowSplashHive = HKCU;
        internal readonly static string  ShowSplashFolder = OptionsFolderKey;
        internal readonly static string  ShowSplashKey = @"Show Splash At Startup";
        internal readonly static string  ShowSplashDefault = "True";

        // Startup Tips registry constants
        internal readonly static string  ShowTipsHive = HKCU;
        internal readonly static string  ShowTipsFolder = OptionsFolderKey;
        internal readonly static string  ShowTipsKey = @"Show Tips At Startup";
        internal readonly static string  ShowTipsDefault = "True";

        // Skin registry constants
        internal readonly static string  SkinNameHive = HKCU;
        internal readonly static string  SkinNameFolder = OptionsFolderKey;
        internal readonly static string  SkinNameKey = @"Default Skin";
        internal readonly static string  SkinNameDefault = @"DevExpress Style";

        // Tooltips registry constants
        internal readonly static string  ShowToolTipsHive = HKCU;
        internal readonly static string  ShowToolTipsFolder = OptionsFolderKey;
        internal readonly static string  ShowToolTipsKey = @"Enable Tool Tips";
        internal readonly static Boolean ShowToolTipsDefault = false ;

        // Date Selection registry constants
        internal readonly static string  DateSelectionHive = HKCU;
        internal readonly static string  DateSelectionFolder = OptionsFolderKey;
        internal readonly static string  DateSelectionKey = @"Date Selection";
        internal readonly static Int32  DateSelectionDefault = -1;

        // Client BottomLine Form Always On Top registry constants
        internal readonly static string  ClientBottomLineFormAlwaysOnTopHive = HKCU;
        internal readonly static string  ClientBottomLineFormAlwaysOnTopFolder = FormsFolderKey + @"\Client.BottomLine";
        internal readonly static string  ClientBottomLineFormAlwaysOnTopKey = @"AlwaysOnTop";
        internal readonly static Boolean  ClientBottomLineFormAlwaysOnTopDefault = false ;

        // Disbursement Options registry constants
        internal readonly static string  DisbursementLockedHive = HKCU;
        internal readonly static string  DisbursementLockedFolder = @"Options\Disbursement";
        internal readonly static string  DisbursementLockedKey = @"locked";
        internal readonly static Boolean  DisbursemenLockedDefault = false ;

        // Disbursement Default 1 registry constants
        internal readonly static string  DisbursementDefault1Hive = HKCU;
        internal readonly static string  DisbursementDefault1Folder = @"Options\Disbursement";
        internal readonly static string  DisbursementDefault1Key = @"1";
        internal readonly static Int64 DisbursementDefault1Default = 2; // normal-no

        // Disbursement Default 2 registry constants
        internal readonly static string  DisbursementDefault2Hive = HKCU;
        internal readonly static string  DisbursementDefault2Folder = @"Options\Disbursement";
        internal readonly static string  DisbursementDefault2Key = @"2";
        internal readonly static Int64 DisbursementDefault2Default = 5; // normal-yes

        // Disbursement Default 3 registry constants
        internal readonly static string  DisbursementDefault3Hive = HKCU;
        internal readonly static string  DisbursementDefault3Folder = @"Options\Disbursement";
        internal readonly static string  DisbursementDefault3Key = @"3";
        internal readonly static Int64 DisbursementDefault3Default = 2; // more-no

        // Disbursement Default 4 registry constants
        internal readonly static string  DisbursementDefault4Hive = HKCU;
        internal readonly static string  DisbursementDefault4Folder = @"Options\Disbursement";
        internal readonly static string  DisbursementDefault4Key = @"4";
        internal readonly static Int64 DisbursementDefault4Default = 5; // more-yes

        // Disbursement Default 5 registry constants
        internal readonly static string  DisbursementDefault5Hive = HKCU;
        internal readonly static string  DisbursementDefault5Folder = @"Options\Disbursement";
        internal readonly static string  DisbursementDefault5Key = @"5";
        internal readonly static Int64  DisbursementDefault5Default = 1; // less-no

        // Disbursement Default 6 registry constants
        internal readonly static string  DisbursementDefault6Hive = HKCU;
        internal readonly static string  DisbursementDefault6Folder = @"Options\Disbursement";
        internal readonly static string  DisbursementDefault6Key = @"6";
        internal readonly static Int64 DisbursementDefault6Default = 5; // less-yes

        // Connection Timeout registry constants
        internal readonly static string ConnectionTimeoutHive = HKCU;
        internal readonly static string ConnectionTimeoutFolder = DatabaseFolderKey;
        internal readonly static string ConnectionTimeoutKey = @"Connection Timeout";
        internal readonly static Int64 ConnectionTimeoutDefault = 4;

        // Command Timeout registry constants
        internal readonly static string CommandTimeoutHive = HKCU;
        internal readonly static string CommandTimeoutFolder = DatabaseFolderKey;
        internal readonly static string CommandTimeoutKey = @"Command Timeout";
        internal readonly static Int64 CommandTimeoutDefault = 30;

        // User name registry constants
        internal readonly static string UsernameHive = HKCU;
        internal readonly static string UsernameFolder = DatabaseFolderKey;
        internal readonly static string UsernameKey = @"User Name";
        internal readonly static string UsernameDefault = null;

        // Password registry constants
        internal readonly static string PasswordHive = HKCU;
        internal readonly static string PasswordFolder = DatabaseFolderKey;
        internal readonly static string PasswordKey = @"Password";
        internal readonly static string PasswordDefault = null;

        // Database registry constants
        internal readonly static string DatabaseHive = HKCU;
        internal readonly static string DatabaseFolder = DatabaseFolderKey;
        internal readonly static string DatabaseKey = @"Database";
        internal readonly static string DatabaseDefault = null;

        // Server registry constants
        internal readonly static string ServerHive = HKCU;
        internal readonly static string ServerFolder = DatabaseFolderKey;
        internal readonly static string ServerKey = @"Server";
        internal readonly static string ServerDefault = null;

        //
        //   these are all in HKLM
        //

        public static string HKLM
        {
            get
            {
                return Hklm;
            }
        }

        // Company registry constants
        internal readonly static string CompanyHive = HKLM;
        internal readonly static string CompanyFolder = InstallationFolderKey;
        internal readonly static string CompanyKey = @"company";
        internal readonly static string CompanyDefault = "";

        // Name registry constants
        internal readonly static string  NameHive = HKLM;
        internal readonly static string  NameFolder = InstallationFolderKey;
        internal readonly static string  NameKey = @"name";
        internal readonly static string  NameDefault = "";

        // Programs registry constants
        internal readonly static string  ProgramsHive = HKLM;
        internal readonly static string  ProgramsFolder = LocationsFolderKey;
        internal readonly static string  ProgramsKey = @"Programs";
        internal readonly static string  ProgramsDefault = "";

        // DotNet registry constants
        internal readonly static string  DotNetHive = HKLM;
        internal readonly static string  DotNetFolder = LocationsFolderKey;
        internal readonly static string  DotNetKey = @"DotNet";
        internal readonly static string  DotNetDefault = "";

        // RPPS Biller Id RegEx constants
        internal readonly static string  RppsBillerIdRegExHive = HKLM;
        internal readonly static string  RppsBillerIdRegExFolder = ValidationFolderKey;
        internal readonly static string  RppsBillerIdRegExKey = @"rpps_biller_ids.rpps_biller_id";
        internal readonly static string  RppsBillerIdRegExDefault = @"^[0-9]{10}$";

        // Letters constants
        internal readonly static string  LettersHive = HKLM;
        internal readonly static string  LettersFolder = LocationsFolderKey;
        internal readonly static string  LettersKey = @"letters";
        internal readonly static string  LettersDefault = "";

        // Reports constants
        internal readonly static string  ReportsHive = HKLM;
        internal readonly static string  ReportsFolder = LocationsFolderKey;
        internal readonly static string  ReportsKey = @"reports";
        internal readonly static string  ReportsDefault = "";

        // Log File constants
        internal readonly static string  LogFileHive = HKLM;
        internal readonly static string  LogFileFolder = LocationsFolderKey;
        internal readonly static string  LogFileKey = @"RxOfficeLogFile";
        internal readonly static string  LogFileDefault = "";

        // Log File constants
        internal readonly static string  RxOfficeCaseSubmissionHive = HKLM;
        internal readonly static string  RxOfficeCaseSubmissionFolder = LocationsFolderKey;
        internal readonly static string  RxOfficeCaseSubmissionKey = @"RxOfficeCaseSubmissionUrl";
        internal readonly static string  RxOfficeCaseSubmissionDefault = "";

        // ReportsLibrary constants
        internal readonly static string  ReportsLibraryHive = HKLM;
        internal readonly static string  ReportsLibraryFolder = LocationsFolderKey;
        internal readonly static string  ReportsLibraryKey = @"Reports Library";
        internal readonly static string  ReportsLibraryDefault = "";

        // EmailHeader constants
        internal readonly static string  EmailHeaderHive = HKLM;
        internal readonly static string  EmailHeaderFolder = OptionsFolderKey;
        internal readonly static string  EmailHeaderKey = @"email header";
        internal readonly static string  EmailHeaderDefault = @"Subject=Regarding your account number ?client?";

        public static string[] HKLM_HKCU
        {
            get
            {
                return new string[] { Hklm, Hkcu };
            }
        }

        //
        // these all cascade from HKLM, then HKCU
        //
        // Show Creditor Addresses registry constants
        internal readonly static string [] ShowCreditorAddressesHives = HKLM_HKCU;
        internal readonly static string  ShowCreditorAddressesFolder = OptionsFolderKey;
        internal readonly static string  ShowCreditorAddressesKey = @"show creditor addresses";
        internal readonly static Boolean ShowCreditorAddressesDefault = false ;

        // Creditor Search Limit registry constants
        internal readonly static string [] CreditorSearchLimitHives = HKLM_HKCU;
        internal readonly static string  CreditorSearchLimitFolder = OptionsFolderKey;
        internal readonly static string  CreditorSearchLimitKey = @"creditor search limit";
        internal readonly static Int64 CreditorSearchLimitDefault = 1000;

        // Show Client Address registry constants
        internal readonly static string [] ShowClientAddressHives = HKLM_HKCU;
        internal readonly static string  ShowClientAddressFolder = OptionsFolderKey;
        internal readonly static string  ShowClientAddressKey = @"Show Client Address";
        internal readonly static Boolean ShowClientAddressDefault = false;

        // Client Search Limit registry constants
        internal readonly static string [] ClientSearchLimitHives = HKLM_HKCU;
        internal readonly static string  ClientSearchLimitFolder = OptionsFolderKey;
        internal readonly static string  ClientSearchLimitKey = @"Client search limit";
        internal readonly static Int64 ClientSearchLimitDefault = 1000;

        // Preview Letters registry constants
        internal readonly static string [] PreviewLettersHives = HKLM_HKCU;
        internal readonly static string  PreviewLettersFolder = OptionsFolderKey;
        internal readonly static string  PreviewLettersKey = @"preview letters";
        internal readonly static Boolean PreviewLettersDefault = false;

        // Preview Checks registry constants
        internal readonly static string [] PreviewChecksHives = HKLM_HKCU;
        internal readonly static string  PreviewChecksFolder = OptionsFolderKey;
        internal readonly static string  PreviewChecksKey = @"preview checks";
        internal readonly static Boolean PreviewChecksDefault = false;

        // Reprint Proposals registry constants
        internal readonly static string [] ReprintProposalsHives = HKLM_HKCU;
        internal readonly static string  ReprintProposalsFolder = OptionsFolderKey;
        internal readonly static string  ReprintProposalsKey = @"Reprint Proposals";
        internal readonly static Boolean ReprintProposalsDefault = false;

        // Permissions - Update Creditor Interest
        internal readonly static string [] UpdateCreditorInterestHives = HKLM_HKCU;
        internal readonly static string  UpdateCreditorInterestFolder = PermissionsFolderKey;
        internal readonly static string  UpdateCreditorInterestKey = @"UpdateCreditorInterest";
        internal readonly static Boolean UpdateCreditorInterestDefault = false;

        // Permissions - Update Creditor General
        internal readonly static string [] UpdateCreditorGeneralHives = HKLM_HKCU;
        internal readonly static string  UpdateCreditorGeneralFolder = PermissionsFolderKey;
        internal readonly static string  UpdateCreditorGeneralKey = @"UpdateCreditorGeneral";
        internal readonly static Boolean UpdateCreditorGeneralDefault = false;

        // Permissions - Update Creditor Contact
        internal readonly static string [] UpdateCreditorContactHives = HKLM_HKCU;
        internal readonly static string  UpdateCreditorContactFolder = PermissionsFolderKey;
        internal readonly static string  UpdateCreditorContactKey = @"UpdateCreditorContact";
        internal readonly static Boolean UpdateCreditorContactDefault = false;

        // Permissions - Update Creditor Policy
        internal readonly static string [] UpdateCreditorPolicyHives = HKLM_HKCU;
        internal readonly static string  UpdateCreditorPolicyFolder = PermissionsFolderKey;
        internal readonly static string  UpdateCreditorPolicyKey = @"UpdateCreditorPolicy";
        internal readonly static Boolean UpdateCreditorPolicyDefault = false;

        // Permissions - Update Creditor Name
        internal readonly static string [] UpdateCreditorNameHives = HKLM_HKCU;
        internal readonly static string  UpdateCreditorNameFolder = PermissionsFolderKey;
        internal readonly static string  UpdateCreditorNameKey = @"UpdateCreditorName";
        internal readonly static Boolean UpdateCreditorNameDefault = false;

        // Permissions - Update Creditor EFT
        internal readonly static string [] UpdateCreditorEFTHives = HKLM_HKCU;
        internal readonly static string  UpdateCreditorEFTFolder = PermissionsFolderKey;
        internal readonly static string  UpdateCreditorEFTKey = @"UpdateCreditorEFT";
        internal readonly static Boolean UpdateCreditorEFTDefault = false;

        // Permissions - Update Creditor Contribution
        internal readonly static string [] UpdateCreditorContributionHives = HKLM_HKCU;
        internal readonly static string  UpdateCreditorContributionFolder = PermissionsFolderKey;
        internal readonly static string  UpdateCreditorContributionKey = @"UpdateCreditorContribution";
        internal readonly static Boolean UpdateCreditorContributionDefault = false;

        // Permissions - Note - Edit Existing Same User
        internal readonly static string [] EditExistingNoteSameUserHives = HKLM_HKCU;
        internal readonly static string  EditExistingNoteSameUserFolder = PermissionsFolderKey;
        internal readonly static string  EditExistingNoteSameUserKey = @"Notes_EditExistingSameUser";
        internal readonly static Boolean EditExistingNoteSameUserDefault = false;

        // Permissions - Note - Edit Existing Other User
        internal readonly static string [] EditExistingNoteOtherUserHives = HKLM_HKCU;
        internal readonly static string  EditExistingNoteOtherUserFolder = PermissionsFolderKey;
        internal readonly static string  EditExistingNoteOtherUserKey = @"Notes_EditExistingOtherUser";
        internal readonly static Boolean EditExistingNoteOtherUserDefault = false;

        // Permissions - Note - Delete
        internal readonly static string [] DeleteNotesHives = HKLM_HKCU;
        internal readonly static string  DeleteNotesFolder = PermissionsFolderKey;
        internal readonly static string  DeleteNotesKey = @"Notes_Delete";
        internal readonly static Boolean DeleteNotesDefault = false;
    }
}
