﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;

namespace DebtPlus.Configuration
{
    /// <summary>
    /// Master Runtime configuration singleton.  
    /// </summary>
    public static class Config
    {
        /// <summary>
        ///     // *.exe.config file - ConfigurationManager functions
        /// </summary>
        public enum ConfigSet
        {
            CheckExtract,
            CheckReconcileFile,
            ClientStatementsOutsourcedCsv,
            CounselorReassign,
            CreditorPolicyMatrixUpdate,
            CreditorRefunds,
            DepositInMotion,
            DepositLockbox,
            DepositManual,
            EmailOptions,
            ExtractArmV4,
            ExtractCsv,
            ExtractExcel,
            ExtractFixed,
            ExtractOpenOfficeCalc,
            FaqDetail,
            HousingContacts,
            ReportsPrint,
            HousingHPF,
            CreditorBalanceVerificationCapitalOne
        }

        /// <summary>
        /// Use local copy of the report (REPX) files only?
        /// </summary>
        public static Boolean LocalReportsOnly
        {
            get
            {
                var cnf = new System.Diagnostics.BooleanSwitch("LocalReportsOnly", "Use Local RPT files only");
                return cnf.Enabled;
            }
        }

        /// <summary>
        /// Path name to the Config directory for the application.
        /// </summary>
        public static string ConfigDirectory
        {
            get
            {
                return System.IO.Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "Config");
            }
        }

        /// <summary>
        /// Should the Splash screen be shown to the user?
        /// </summary>
        public static Boolean ShowSplash
        {
            get
            {
                // Look for the string being true/false.
                String s = DebtPlusRegistry.GetValue(Constants.ShowSplashHive, Constants.ShowSplashFolder, Constants.ShowSplashKey) ?? Constants.ShowSplashDefault;
                Boolean value;
                if (s != null && Boolean.TryParse(s, out value))
                {
                    return value;
                }

                // Look for the string being the current version information
                return System.String.Compare(AssemblyInfo.Version, s, false) != 0;
            }

            set
            {
                // Set the string as being the current version string or true. If it is the current version then it is false until the version is changed.
                // It is set to "TRUE" if the value is true.
                // It is set to the current version if the value is false.

                // To make it "FALSE", Constants.it must be manually edited as that does not cause the item to be shown forever and is never overwritten. (Constants.Well, Constants.that
                // is to say that the form is never displayed and if it is never displayed then there is no option to overwrite it
                // so it is never overwritten.)
                String v = value ? Boolean.TrueString : AssemblyInfo.Version;
                DebtPlusRegistry.SetValue(Constants.ShowSplashHive, Constants.ShowSplashFolder, Constants.ShowSplashKey, v);
            }
        }

        /// <summary>
        /// Should the Tips screen be shown to the user?
        /// </summary>
        public static Boolean ShowStartupTips
        {
            get
            {
                return translateToBool(DebtPlusRegistry.GetValue(Constants.ShowTipsHive, Constants.ShowTipsFolder, Constants.ShowTipsKey)).GetValueOrDefault(true);
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.ShowTipsHive, Constants.ShowTipsFolder, Constants.ShowTipsKey, value.ToString());
            }
        }

        /// <summary>
        /// Are tool tips enabled on the forms?
        /// </summary>
        public static Boolean ShowToolTips
        {
            get
            {
                string v = DebtPlusRegistry.GetValue(Constants.ShowToolTipsHive, Constants.ShowToolTipsFolder, Constants.ShowToolTipsKey);
                return translateToBool(v).GetValueOrDefault(Constants.ShowToolTipsDefault);
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.ShowToolTipsHive, Constants.ShowToolTipsFolder, Constants.ShowToolTipsKey, value.ToString());
            }
        }

        /// <summary>
        /// Name of the current skin for the form
        /// </summary>
        public static string SkinName
        {
            get
            {
                return DebtPlusRegistry.GetValue(Constants.SkinNameHive, Constants.SkinNameFolder, Constants.SkinNameKey) ?? Constants.SkinNameDefault;
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.SkinNameHive, Constants.SkinNameFolder, Constants.SkinNameKey, value.ToString());
            }
        }

        /// <summary>
        /// Selection type for a date parameter on the input form. This is a report parameter form field.
        /// </summary>
        public static Int32 DateSelection
        {
            get
            {
                return (Int32) translateToInt(DebtPlusRegistry.GetValue(Constants.DateSelectionHive, Constants.DateSelectionFolder, Constants.DateSelectionKey)).GetValueOrDefault(Constants.DateSelectionDefault);
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.DateSelectionHive, Constants.DateSelectionFolder, Constants.DateSelectionKey, value.ToString());
            }
        }

        /// <summary>
        /// Is the bottom line form always on top of the forms?
        /// </summary>
        public static Boolean ClientBottomLineFormAlwaysOnTop
        {
            get
            {
                return translateToBool(DebtPlusRegistry.GetValue(Constants.ClientBottomLineFormAlwaysOnTopHive, Constants.ClientBottomLineFormAlwaysOnTopFolder, Constants.ClientBottomLineFormAlwaysOnTopKey)).GetValueOrDefault(Constants.ClientBottomLineFormAlwaysOnTopDefault);
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.ClientBottomLineFormAlwaysOnTopHive, Constants.ClientBottomLineFormAlwaysOnTopFolder, Constants.ClientBottomLineFormAlwaysOnTopKey, value.ToString());
            }
        }

        /// <summary>
        /// Are the disbursement options locked (Constants.meaning can not be changed) for the disbursement?
        /// </summary>
        public static Boolean DisbursementLocked
        {
            get
            {
                return translateToBool(DebtPlusRegistry.GetValue(Constants.DisbursementLockedFolder, Constants.DisbursementLockedKey)).GetValueOrDefault(Constants.DisbursemenLockedDefault);
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.DisbursementLockedHive, Constants.DisbursementLockedFolder, Constants.DisbursementLockedKey, value.ToString());
            }
        }

        /// <summary>
        /// Current setting for the disbursement option #1
        /// </summary>
        public static Int32 DisbursementDefault1
        {
            get
            {
                Int32 v = (Int32) translateToInt(DebtPlusRegistry.GetValue(Constants.DisbursementDefault1Folder, Constants.DisbursementDefault1Key)).GetValueOrDefault(Constants.DisbursementDefault1Default);
                return (v < 0) ? 0 : v;
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.DisbursementDefault1Hive, Constants.DisbursementDefault1Folder, Constants.DisbursementDefault1Key, value.ToString());
            }
        }

        /// <summary>
        /// Current setting for the disbursement option #2
        /// </summary>
        public static Int32 DisbursementDefault2
        {
            get
            {
                Int32 v = (Int32) translateToInt(DebtPlusRegistry.GetValue(Constants.DisbursementDefault2Folder, Constants.DisbursementDefault2Key)).GetValueOrDefault(Constants.DisbursementDefault2Default);
                return (v < 0) ? 0 : v;
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.DisbursementDefault2Hive, Constants.DisbursementDefault2Folder, Constants.DisbursementDefault2Key, value.ToString());
            }
        }

        /// <summary>
        /// Current setting for the disbursement option #3
        /// </summary>
        public static Int32 DisbursementDefault3
        {
            get
            {
                Int32 v = (Int32) translateToInt(DebtPlusRegistry.GetValue(Constants.DisbursementDefault3Folder, Constants.DisbursementDefault3Key)).GetValueOrDefault(Constants.DisbursementDefault3Default);
                return (v < 0) ? 0 : v;
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.DisbursementDefault3Hive, Constants.DisbursementDefault3Folder, Constants.DisbursementDefault3Key, value.ToString());
            }
        }

        /// <summary>
        /// Current setting for the disbursement option #4
        /// </summary>
        public static Int32 DisbursementDefault4
        {
            get
            {
                Int32 v = (Int32) translateToInt(DebtPlusRegistry.GetValue(Constants.DisbursementDefault4Folder, Constants.DisbursementDefault4Key)).GetValueOrDefault(Constants.DisbursementDefault4Default);
                return (v < 0) ? 0 : v;
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.DisbursementDefault4Hive, Constants.DisbursementDefault4Folder, Constants.DisbursementDefault4Key, value.ToString());
            }
        }

        /// <summary>
        /// Current setting for the disbursement option #5
        /// </summary>
        public static Int32 DisbursementDefault5
        {
            get
            {
                Int32 v = (Int32) translateToInt(DebtPlusRegistry.GetValue(Constants.DisbursementDefault5Folder, Constants.DisbursementDefault5Key)).GetValueOrDefault(Constants.DisbursementDefault5Default);
                return (v < 0) ? 0 : v;
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.DisbursementDefault5Hive, Constants.DisbursementDefault5Folder, Constants.DisbursementDefault5Key, value.ToString());
            }
        }

        /// <summary>
        /// Current setting for the disbursement option #6
        /// </summary>
        public static Int32 DisbursementDefault6
        {
            get
            {
                Int32 v = (Int32)translateToInt(DebtPlusRegistry.GetValue(Constants.DisbursementDefault6Folder, Constants.DisbursementDefault6Key)).GetValueOrDefault(Constants.DisbursementDefault6Default);
                return (v < 0) ? 0 : v;
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.DisbursementDefault6Hive, Constants.DisbursementDefault6Folder, Constants.DisbursementDefault6Key, value.ToString());
            }
        }

        /// <summary>
        /// Company information
        /// </summary>
        public static String InstallationCompany
        {
            get
            {
                return DebtPlusRegistry.GetValue(Constants.CompanyHive, Constants.CompanyFolder, Constants.CompanyKey) ?? Constants.CompanyDefault;
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.CompanyHive, Constants.CompanyFolder, Constants.CompanyKey, value.ToString());
            }
        }

        /// <summary>
        /// User name information
        /// </summary>
        public static String InstallationName
        {
            get
            {
                return DebtPlusRegistry.GetValue(Constants.NameHive, Constants.NameFolder, Constants.NameKey) ?? Constants.NameDefault;
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.NameHive, Constants.NameFolder, Constants.NameKey, value.ToString());
            }
        }

        /// <summary>
        /// Location of the directory where the application is installed
        /// </summary>
        public static String ProgramsDirectory
        {
            get
            {
                return DebtPlusRegistry.GetValue(Constants.ProgramsHive, Constants.ProgramsFolder, Constants.ProgramsKey) ?? Constants.ProgramsDefault;
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.ProgramsHive, Constants.ProgramsFolder, Constants.ProgramsKey, value.ToString());
            }
        }

        /// <summary>
        /// Location where the .NET executables are installed. (Constants.OBSOLETE)
        /// </summary>
        public static String DotNetDirectory
        {
            get
            {
                return DebtPlusRegistry.GetValue(Constants.DotNetHive, Constants.DotNetFolder, Constants.DotNetKey) ?? Constants.DotNetDefault;
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.DotNetHive, Constants.DotNetFolder, Constants.DotNetKey, value.ToString());
            }
        }

        /// <summary>
        /// Regular expression for a RPPS biller ID number
        /// </summary>
        public static String RppsBillerIdRegEx
        {
            get
            {
                return DebtPlusRegistry.GetValue(Constants.RppsBillerIdRegExHive, Constants.RppsBillerIdRegExFolder, Constants.RppsBillerIdRegExKey) ?? Constants.RppsBillerIdRegExDefault;
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.RppsBillerIdRegExHive, Constants.RppsBillerIdRegExFolder, Constants.RppsBillerIdRegExKey, value.ToString());
            }
        }

        /// <summary>
        /// Location where the letters are to be located
        /// </summary>
        public static String LettersDirectory
        {
            get
            {
                return DebtPlusRegistry.GetValue(Constants.LettersHive, Constants.LettersFolder, Constants.LettersKey) ?? Constants.LettersDefault;
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.LettersHive, Constants.LettersFolder, Constants.LettersKey, value.ToString());
            }
        }

        /// <summary>
        /// Location where the reports are located
        /// </summary>
        public static String ReportsDirectory
        {
            get
            {
                return DebtPlusRegistry.GetValue(Constants.ReportsHive, Constants.ReportsFolder, Constants.ReportsKey) ?? Constants.ReportsDefault;
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.ReportsHive, Constants.ReportsFolder, Constants.ReportsKey, value.ToString());
            }
        }

        /// <summary>
        /// Location where the log files are located
        /// </summary>
        public static String LogFileDirectory
        {
            get
            {
                return DebtPlusRegistry.GetValue(Constants.LogFileHive, Constants.LogFileFolder, Constants.LogFileKey) ?? Constants.LogFileDefault;
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.LogFileHive, Constants.LogFileFolder, Constants.LogFileKey, value.ToString());
            }
        }

        /// <summary>
        /// Location where the log files are located
        /// </summary>
        public static String RxOfficeCaseSubmissionPath
        {
            get
            {
                return DebtPlusRegistry.GetValue(Constants.RxOfficeCaseSubmissionHive, Constants.RxOfficeCaseSubmissionFolder, Constants.RxOfficeCaseSubmissionKey) ?? Constants.RxOfficeCaseSubmissionDefault;
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.RxOfficeCaseSubmissionHive, Constants.RxOfficeCaseSubmissionFolder, Constants.RxOfficeCaseSubmissionKey, value.ToString());
            }
        }

        /// <summary>
        /// Header for email messages generated by clicking on the email address
        /// </summary>
        public static String EmailHeader
        {
            get
            {
                return DebtPlusRegistry.GetValue(Constants.EmailHeaderHive, Constants.EmailHeaderFolder, Constants.EmailHeaderKey) ?? Constants.EmailHeaderDefault;
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.EmailHeaderHive, Constants.EmailHeaderFolder, Constants.EmailHeaderKey, value.ToString());
            }
        }

        /// <summary>
        /// should creditor addresses be shown in the result list?
        /// </summary>
        public static Boolean ShowCreditorAddresses
        {
            get
            {
                return translateToBool(DebtPlusRegistry.GetValue(Constants.ShowCreditorAddressesHives, Constants.ShowCreditorAddressesFolder, Constants.ShowCreditorAddressesKey)).GetValueOrDefault(Constants.ShowCreditorAddressesDefault);
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.HKCU, Constants.ShowCreditorAddressesFolder, Constants.ShowCreditorAddressesKey, value.ToString());
            }
        }

        /// <summary>
        /// Number of items to be returned on the creditor search. "0" = infinite.
        /// </summary>
        public static Int32 CreditorSearchLimit
        {
            get
            {
                return (Int32) translateToInt(DebtPlusRegistry.GetValue(Constants.CreditorSearchLimitHives, Constants.CreditorSearchLimitFolder, Constants.CreditorSearchLimitKey)).GetValueOrDefault(Constants.CreditorSearchLimitDefault);
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.HKCU, Constants.CreditorSearchLimitFolder, Constants.CreditorSearchLimitKey, value.ToString());
            }
        }

        /// <summary>
        /// Should the address information be displayed on the search form?
        /// </summary>
        public static Boolean ShowClientAddress
        {
            get
            {
                return translateToBool(DebtPlusRegistry.GetValue(Constants.ShowClientAddressHives, Constants.ShowClientAddressFolder, Constants.ShowClientAddressKey)).GetValueOrDefault(Constants.ShowClientAddressDefault);
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.HKCU, Constants.ShowClientAddressFolder, Constants.ShowClientAddressKey, value.ToString());
            }
        }

        /// <summary>
        /// Number of items to be returned on the client search. "0" = infinite.
        /// </summary>
        public static Int32 ClientSearchLimit
        {
            get
            {
                return (Int32) translateToInt(DebtPlusRegistry.GetValue(Constants.ClientSearchLimitHives, Constants.ClientSearchLimitFolder, Constants.ClientSearchLimitKey)).GetValueOrDefault(Constants.ClientSearchLimitDefault);
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.HKCU, Constants.ClientSearchLimitFolder, Constants.ClientSearchLimitKey, value.ToString());
            }
        }

        /// <summary>
        /// Do we wish to preview all letters generated by the program?
        /// </summary>
        public static Boolean PreviewLetters
        {
            get
            {
                return translateToBool(DebtPlusRegistry.GetValue(Constants.PreviewLettersHives, Constants.PreviewLettersFolder, Constants.PreviewLettersKey)).GetValueOrDefault(Constants.PreviewLettersDefault);
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.HKCU, Constants.PreviewLettersFolder, Constants.PreviewLettersKey, value.ToString());
            }
        }

        /// <summary>
        /// Do we wish to preview all letters generated by the program?
        /// </summary>
        public static Boolean PreviewChecks
        {
            get
            {
                return translateToBool(DebtPlusRegistry.GetValue(Constants.PreviewChecksHives, Constants.PreviewChecksFolder, Constants.PreviewChecksKey)).GetValueOrDefault(Constants.PreviewChecksDefault);
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.HKCU, Constants.PreviewChecksFolder, Constants.PreviewChecksKey, value.ToString());
            }
        }

        /// <summary>
        /// Is the user allowed to reprint proposals?
        /// </summary>
        public static Boolean ReprintProposals
        {
            get
            {
                return translateToBool(DebtPlusRegistry.GetValue(Constants.ReprintProposalsHives, Constants.ReprintProposalsFolder, Constants.ReprintProposalsKey)).GetValueOrDefault(Constants.ReprintProposalsDefault);
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.HKCU, Constants.ReprintProposalsFolder, Constants.ReprintProposalsKey, value.ToString());
            }
        }

        /// <summary>
        /// Is the user allowed to update the creditor interest information?
        /// </summary>
        public static Boolean UpdateCreditorInterest
        {
            get
            {
                return translateToBool(DebtPlusRegistry.GetValue(Constants.UpdateCreditorInterestHives, Constants.UpdateCreditorInterestFolder, Constants.UpdateCreditorInterestKey)).GetValueOrDefault(Constants.UpdateCreditorContactDefault);
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.HKCU, Constants.UpdateCreditorInterestFolder, Constants.UpdateCreditorInterestKey, value.ToString());
            }
        }

        /// <summary>
        /// Is the user allowed to update the creditor general information?
        /// </summary>
        public static Boolean UpdateCreditorGeneral
        {
            get
            {
                return translateToBool(DebtPlusRegistry.GetValue(Constants.UpdateCreditorGeneralHives, Constants.UpdateCreditorGeneralFolder, Constants.UpdateCreditorGeneralKey)).GetValueOrDefault(Constants.UpdateCreditorGeneralDefault);
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.HKCU, Constants.UpdateCreditorGeneralFolder, Constants.UpdateCreditorGeneralKey, value.ToString());
            }
        }

        /// <summary>
        /// Is the user allowed to update the creditor contact information?
        /// </summary>
        public static Boolean UpdateCreditorContacts
        {
            get
            {
                return translateToBool(DebtPlusRegistry.GetValue(Constants.UpdateCreditorContactHives, Constants.UpdateCreditorContactFolder, Constants.UpdateCreditorContactKey)).GetValueOrDefault(Constants.UpdateCreditorContactDefault);
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.HKCU, Constants.UpdateCreditorContactFolder, Constants.UpdateCreditorContactKey, value.ToString());
            }
        }

        /// <summary>
        /// Is the user allowed to update the creditor policy information?
        /// </summary>
        public static Boolean UpdateCreditorPolicy
        {
            get
            {
                return translateToBool(DebtPlusRegistry.GetValue(Constants.UpdateCreditorPolicyHives, Constants.UpdateCreditorPolicyFolder, Constants.UpdateCreditorPolicyKey)).GetValueOrDefault();
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.HKCU, Constants.UpdateCreditorPolicyFolder, Constants.UpdateCreditorPolicyKey, value.ToString());
            }
        }

        /// <summary>
        /// Is the user allowed to update the creditor name information?
        /// </summary>
        public static Boolean UpdateCreditorName
        {
            get
            {
                return translateToBool(DebtPlusRegistry.GetValue(Constants.UpdateCreditorNameHives, Constants.UpdateCreditorNameFolder, Constants.UpdateCreditorNameKey)).GetValueOrDefault();
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.HKCU, Constants.UpdateCreditorNameFolder, Constants.UpdateCreditorNameKey, value.ToString());
            }
        }

        /// <summary>
        /// Is the user allowed to update the creditor EFT information?
        /// </summary>
        public static Boolean UpdateCreditorEFT
        {
            get
            {
                return translateToBool(DebtPlusRegistry.GetValue(Constants.UpdateCreditorEFTHives, Constants.UpdateCreditorEFTFolder, Constants.UpdateCreditorEFTKey)).GetValueOrDefault();
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.HKCU, Constants.UpdateCreditorEFTFolder, Constants.UpdateCreditorEFTKey, value.ToString());
            }
        }

        /// <summary>
        /// Is the user allowed to update the creditor contribution information?
        /// </summary>
        public static Boolean UpdateCreditorContribution
        {
            get
            {
                return translateToBool(DebtPlusRegistry.GetValue(Constants.UpdateCreditorContributionHives, Constants.UpdateCreditorContributionFolder, Constants.UpdateCreditorContributionKey)).GetValueOrDefault();
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.HKCU, Constants.UpdateCreditorContributionFolder, Constants.UpdateCreditorContributionKey, value.ToString());
            }
        }

        /// <summary>
        /// Is the user allowed to edit a note if the user is the same one who created it?
        /// </summary>
        public static Boolean EditExistingNoteSameUser
        {
            get
            {
                return translateToBool(DebtPlusRegistry.GetValue(Constants.EditExistingNoteSameUserHives, Constants.EditExistingNoteSameUserFolder, Constants.EditExistingNoteSameUserKey)).GetValueOrDefault();
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.HKCU, Constants.EditExistingNoteSameUserFolder, Constants.EditExistingNoteSameUserKey, value.ToString());
            }
        }

        /// <summary>
        /// Is the user allowed to edit a note if the user is not the same one who created it?
        /// </summary>
        public static Boolean EditExistingNoteOtherUser
        {
            get
            {
                return translateToBool(DebtPlusRegistry.GetValue(Constants.EditExistingNoteOtherUserHives, Constants.EditExistingNoteOtherUserFolder, Constants.EditExistingNoteOtherUserKey)).GetValueOrDefault();
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.HKCU, Constants.EditExistingNoteOtherUserFolder, Constants.EditExistingNoteOtherUserKey, value.ToString());
            }
        }

        /// <summary>
        /// Is the user allowed to delete a note?
        /// </summary>
        public static Boolean DeleteNotes
        {
            get
            {
                return translateToBool(DebtPlusRegistry.GetValue(Constants.DeleteNotesHives, Constants.DeleteNotesFolder, Constants.DeleteNotesKey)).GetValueOrDefault();
            }
            set
            {
                DebtPlusRegistry.SetValue(Constants.HKCU, Constants.DeleteNotesFolder, Constants.DeleteNotesKey, value.ToString());
            }
        }

        /// <summary>
        /// Should the log file be used for debugging purposes in LINQ queries.
        /// </summary>
        public static string EnableLINQLoggging
        {
            get
            {
                string elem = null;
                System.Collections.Specialized.NameValueCollection col = System.Configuration.ConfigurationManager.AppSettings;
                if (col != null)
                {
                    elem = col["linq.log"];
                    if (string.Compare(elem, System.Boolean.TrueString, true) == 0)
                    {
                        return "DEBUG";
                    }
                    else if (string.Compare(elem, System.Boolean.FalseString, true) == 0)
                    {
                        return "OFF";
                    }
                }
                return elem ?? "OFF";
            }
        }
       
        /////////////////////////////////////////////////////////////////////////////////////////
        //     Database access keys                                                            //
        /////////////////////////////////////////////////////////////////////////////////////////

        public static Int32? ConnectionTimeout
        {
            get
            {
                return (Int32?) translateToInt(GetDatabaseValue(Constants.DatabaseEnvVar, Constants.ConnectionTimeoutHive, Constants.ConnectionTimeoutFolder, Constants.ConnectionTimeoutKey));
            }
        }

        /// <summary>
        /// SQL Command timeout
        /// </summary>
        public static Int32? CommandTimeout
        {
            get
            {
                return (Int32?)translateToInt(GetDatabaseValue(Constants.DatabaseEnvVar, Constants.CommandTimeoutHive, Constants.CommandTimeoutFolder, Constants.CommandTimeoutKey));
            }
        }

        /// <summary>
        /// Database user name
        /// </summary>
        public static String Username
        {
            get
            {
                // highest precedence is the DEBTPLUS_USER env var
                String uname = EnvVar("DEBTPLUS_USER");
                if (!String.IsNullOrWhiteSpace(uname))
                {
                    return uname;
                }

                return GetDatabaseValue(Constants.DatabaseEnvVar, Constants.UsernameHive, Constants.UsernameFolder, Constants.UsernameKey);
            }
        }

        /// <summary>
        /// Database password
        /// </summary>
        public static String Password
        {
            get
            {
                // highest precedence is the DEBTPLUS_USER env var
                String pswd = EnvVar("DEBTPLUS_PASSWORD");
                if (!string.IsNullOrWhiteSpace(pswd))
                {
                    return pswd;
                }

                return GetDatabaseValue(Constants.DatabaseEnvVar, Constants.PasswordHive, Constants.PasswordFolder, Constants.PasswordKey);
            }
        }

        /// <summary>
        /// Database name (name of the database on the server)
        /// </summary>
        public static String Database
        {
            get
            {
                // highest precedence is the DEBTPLUS_DATABASE env var
                String db = EnvVar("DEBTPLUS_DATABASE");
                if (!string.IsNullOrWhiteSpace(db))
                {
                    return db;
                }

                return GetDatabaseValue("DEBTPLUS_KEY", Constants.DatabaseHive, Constants.DatabaseFolder, Constants.DatabaseKey);
            }
        }

        /// <summary>
        /// Name of the database server.
        /// </summary>
        public static String Server
        {
            get
            {
                // highest precedence is the DEBTPLUS_SERVER env var
                String svr = EnvVar("DEBTPLUS_SERVER");
                if (!string.IsNullOrWhiteSpace(svr))
                {
                    return svr;
                }

                return GetDatabaseValue(Constants.DatabaseEnvVar, Constants.ServerHive, Constants.ServerFolder, Constants.ServerKey);
            }
        }

        /// <summary>
        /// Utility function to convert the string to a boolean.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Boolean? translateToBool(string value)
        {
            try
            {
                if (value != null)
                {
                    bool answer;
                    if (Boolean.TryParse(value, out answer))
                    {
                        return answer;
                    }
                }
            }
            catch { }

            return null;
        }

        /// <summary>
        /// Utility function to convert the string to a fixed point number
        /// </summary>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static Int64? translateToInt(string value)
        {
            try
            {
                if (value != null)
                {
                    Int64 answer;
                    if (Int64.TryParse(value, out answer))
                    {
                        return answer;
                    }
                }
            }
            catch { }

            return null;
        }

        /// <summary>
        /// Special versions of GetValue for the database information. Allows for the system name to be modified.
        /// </summary>
        /// <param name="envVariable"></param>
        /// <param name="hives"></param>
        /// <param name="folder"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        private static String GetDatabaseValue(String envVariable, String hive, String folder, String key)
        {
            // highest precedence is the cascading override folder in the registry if the envVariable is set
            String overrideFolder = EnvVar(envVariable);
            if (!String.IsNullOrWhiteSpace(overrideFolder))
            {
                overrideFolder = System.IO.Path.Combine(Constants.DatabaseFolder.Trim('\\'), Constants.SystemsFolderKey.Trim('\\'), overrideFolder.Trim('\\'));
                string val = DebtPlusRegistry.GetValue(hive, overrideFolder, key);
                if (val != null)
                {
                    return val;
                }
            }

            // least precedence is the normal cascading registry value
            return DebtPlusRegistry.GetValue(hive, folder, key);
        }

        /// <summary>
        /// Retrieve the current environment variable from the system environment.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private static String EnvVar(String name)
        {
            return Environment.GetEnvironmentVariable(name, EnvironmentVariableTarget.Process);
        }

        /// <summary>
        /// Return the setting collection name in the app.config file
        /// </summary>
        /// <param name="configSet"></param>
        /// <returns></returns>
        public static string GetConfigSettingsName(ConfigSet configSet)
        {
            string setName = null;

            switch (configSet)
            {
                case ConfigSet.CreditorBalanceVerificationCapitalOne:
                    setName = "creditor.balance.verification.capone";
                    break;
                case ConfigSet.CheckExtract:
                    setName = "check.extract";
                    break;
                case ConfigSet.CheckReconcileFile:
                    setName = "check.reconcile.file";
                    break;
                case ConfigSet.ClientStatementsOutsourcedCsv:
                    setName = "client.statements.outsourced.csv";
                    break;
                case ConfigSet.CounselorReassign:
                    setName = "counselor.reassign";
                    break;
                case ConfigSet.CreditorPolicyMatrixUpdate:
                    setName = "creditor.policymatrix.update";
                    break;
                case ConfigSet.CreditorRefunds:
                    setName = "creditor.refunds";
                    break;
                case ConfigSet.DepositInMotion:
                    setName = "deposit.inmotion";
                    break;
                case ConfigSet.DepositLockbox:
                    setName = "deposit.lockbox.{0}";
                    break;
                case ConfigSet.DepositManual:
                    setName = "deposit.manual";
                    break;
                case ConfigSet.EmailOptions:
                    setName = "email.options";
                    break;
                case ConfigSet.ExtractArmV4:
                    setName = "extract.arm.v4";
                    break;
                case ConfigSet.ExtractCsv:
                    setName = "extract.csv";
                    break;
                case ConfigSet.ExtractExcel:
                    setName = "extract.excel";
                    break;
                case ConfigSet.ExtractFixed:
                    setName = "extract.fixed";
                    break;
                case ConfigSet.ExtractOpenOfficeCalc:
                    setName = "extract.openoffice.calc";
                    break;
                case ConfigSet.FaqDetail:
                    setName = "faq.detail";
                    break;
                case ConfigSet.HousingContacts:
                    setName = "housing.contacts";
                    break;
                case ConfigSet.ReportsPrint:
                    setName = "reports.print";
                    break;
                case ConfigSet.HousingHPF:
                    setName = "housing.hpf";
                    break;
            }

            return setName;
        }

        /// <summary>
        /// Get the configuration setting collection
        /// </summary>
        /// <param name="configSet"></param>
        /// <returns></returns>
        public static System.Collections.Specialized.NameValueCollection GetConfigSettings(ConfigSet configSet)
        {
            String setName = GetConfigSettingsName(configSet);
            if (string.IsNullOrEmpty(setName))
            {
                return null;
            }
            return GetConfigSettings(setName);
        }

        /// <summary>
        /// Get the configuration setting collection
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Specialized.NameValueCollection GetConfigSettings()
        {
            return GetConfigSettings(null);
        }

        /// <summary>
        /// Get the configuration setting collection
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        public static System.Collections.Specialized.NameValueCollection GetConfigSettings(String section)
        {
            if (section != null)
            {
                return System.Configuration.ConfigurationManager.GetSection(section) as System.Collections.Specialized.NameValueCollection;
            }
            return System.Configuration.ConfigurationManager.AppSettings;
        }

        /// <summary>
        /// Obtain a list of the regular expressions used in parsing the input files.
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        public static System.Collections.Specialized.ListDictionary GetRegexExprList(System.Collections.Specialized.NameValueCollection settings)
        {
            if (settings == null)
            {
                return null;
            }

            System.Collections.Specialized.ListDictionary list = new System.Collections.Specialized.ListDictionary();
            foreach (String key in settings.Keys)
            {
                if (key.StartsWith("expr_", true, System.Globalization.CultureInfo.InvariantCulture))
                {
                    string keyName = key.Substring(5);
                    if (!list.Contains(keyName))
                    {
                        list.Add(key, new RegexParser(keyName, settings[key]));
                    }
                }
            }
            return list;
        }

        /// <summary>
        /// Retrieve a value from the configuration setting collection.
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetConfigValueForKey(System.Collections.Specialized.NameValueCollection settings, string key)
        {
            return GetConfigValueForKey(settings, key, null);
        }

        /// <summary>
        /// Retrieve a value from the configuration setting collection.
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static String GetConfigValueForKey(System.Collections.Specialized.NameValueCollection settings, String key, String defaultValue)
        {
            try
            {
                String temp = settings[key];
                if (temp != null)
                {
                    return temp;
                }
            }
            catch { }

            return defaultValue;
        }
    }
}
