﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Configuration
{
    public static class DebtPlusRegistry
    {
        public static string RegistryBaseKey
        {
            get
            {
                return Constants.InstallationRegistryKey;
            }
        }

        /// <summary>
        /// Get an array of strings of all subkey names of folder
        /// </summary>
        public static string[] GetInstallationSubKeyNames(string folder)
        {
            try
            {
                string path = System.IO.Path.Combine(RegistryBaseKey, folder.Trim('\\'));
                using (Microsoft.Win32.RegistryKey newReg = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(path))
                {
                    if (newReg != null)
                    {
                        String[] result = newReg.GetSubKeyNames();
                        if (result != null)
                        {
                            return result;
                        }
                    }
                }
            }
            catch { }
            return null;
        }

        /// <summary>
        /// Retrieve the value from the Systems area for the database
        /// </summary>
        public static string GetInstallationValue(string folder, string key)
        {
            return GetValue(Constants.HKLM, folder, key);
        }

        public static String GetValue(string[] hives, string folder, string key)
        {
            for (Int32 indx = hives.GetUpperBound(0); indx >= 0; --indx)
            {
                string v = GetValue(hives[indx], folder, key);
                if (!string.IsNullOrEmpty(v))
                {
                    return v;
                }
            }
            return null;
        }

        public static String GetValue(string folder, string key)
        {
            return GetValue(Constants.HKCU, folder, key);
        }

        public static String GetValue(String hive, String folder, String key)
        {
            try
            {
                string path = System.IO.Path.Combine(hive, RegistryBaseKey, folder.Trim('\\'));
                object obj = Microsoft.Win32.Registry.GetValue(path, key, null);
                if (obj != null)
                {
                    return Convert.ToString(obj);
                }
            }
            catch { }
            return null;
        }

        public static Boolean SetValue(string folder, string key, string value)
        {
            return SetValue(Constants.HKCU, folder, key, value);
        }

        public static Boolean SetValue(String hive, String folder, String key, String value)
        {
            try
            {
                string path = System.IO.Path.Combine(hive, RegistryBaseKey, folder.Trim('\\'));
                Microsoft.Win32.Registry.SetValue(path, key, value);
                return GetValue(hive, folder, key) == value;
            }
            catch
            {
                return false;
            }
        }
    }
}
