﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace DebtPlus.Configuration
{
    public class RegexParser
    {
        public RegexParser()
        {
            rx = null;
        }

        public RegexParser(string subType, string expr) : this()
        {
            this.subtype = subType;
            this.expr = expr;
        }

        public Regex rx { get; set; }

        private string _expr = null;
        public string expr
        {
            get
            {
                return _expr;
            }

            set
            {
                _expr = value;
                rx = new Regex(expr, RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.CultureInvariant);
            }
        }

        public string subtype { get; set; }
    }
}
