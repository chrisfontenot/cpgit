﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Configuration
{
    public static class AssemblyInfo
    {
        /// <summary>
        /// return the installation registry key setting. It is typically "Software\DebtPlus, L.L.C.\DebtPlus\".
        /// </summary>
        public static string InstallationRegistryKey
        {
            get
            {
                return Constants.InstallationRegistryKey;
            }
        }

        /// <summary>
        /// return the current version string for the program that is being executed
        /// </summary>
        public static string Version
        {
            get
            {
                System.Diagnostics.FileVersionInfo fi = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileVersionInfo;
                return fi.ProductVersion;
            }
        }
    }
}

