#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the f_ollowing
' set of attributes. Change these attribute values to modify the information
' associated with an assembly

<Assembly: AssemblyCompany("DebtPlus, L.L.C.")> 
<Assembly: AssemblyProduct("DebtPlus Debt Management System")> 
<Assembly: AssemblyCopyright("Copyright © 2000-2012 DebtPlus, L.L.C. -- All rights reserved")> 
<Assembly: AssemblyTrademark("DebtPlus is an trademark of DebtPlus, L.L.C., a California Company.")> 
'<Assembly: System.Security.AllowPartiallyTrustedCallers()> 
<Assembly: CLSCompliant(True)> 
<Assembly: ComVisible(False)> 
<Assembly: AssemblyCulture("")> 

' Set the configuration to either debug or release depending upon how the program is built.
#If DEBUG Then
<Assembly: AssemblyConfiguration("net-2.0.win32; Debug")> 
#Else
<Assembly: AssemblyConfiguration("net-2.0.win32; Release")>
#End If

'
' In order to sign your assembly you must specify a key to use. Refer to the
' Microsoft .NET f_ramework documentation for more information on assembly signing.
'
' Use the attributes below to control which key is used for signing.
'
' Notes:
'   (*) If no key is specified, the assembly is not signed.
'   (*) KeyName refers to a key that has been installed in the Crypto Service
'       Provider (CSP) on your machine. KeyFile refers to a f_ile which contains
'       a key.
'   (*) If the KeyFile and the KeyName values are both specified, the
'       f_ollowing processing occurs:
'       (1) If the KeyName can be f_ound in the CSP, that key is used.
'       (2) If the KeyName does not exist and the KeyFile does exist, the key
'           in the KeyFile is installed into the CSP and used.
'   (*) In order to create a KeyFile, you can use the sn.exe (Strong Name) utility.
'       When specifying the KeyFile, the location of the KeyFile should be
'       relative to the project output directory which is
'       %Project Directory%\obj\<configuration>. for example, if your KeyFile is
'       located in the project directory, you would specify the AssemblyKeyFile
'       attribute as <assembly: AssemblyKeyFile("..\\..\\mykey.snk")>
'   (*) Delay Signing is an advanced option - see the Microsoft .NET f_ramework
'       documentation for more information on this.
'
'<Assembly: AssemblyDelaySign(False)>
'<Assembly: AssemblyKeyFile("C:\Documents and Settings\Programming\My Documents\Visual Studio 2008\Projects\DebtPlus\DebtPlus.Key\StrongKey.snk")>
'<Assembly: AssemblyKeyName("DebtPlus")>

' Version information for an assembly consists of the f_ollowing f_our values:
'
'	Major version
'	Minor Version
'	Revision
'	Build Number
'
<Assembly: AssemblyVersion("17.4.20.0227")> 
<Assembly: AssemblyInformationalVersion("17.4.20.0227")> 
<Assembly: AssemblyFileVersion("17.4.20.0227")> 
