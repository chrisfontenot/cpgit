﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainMenu
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ACHRejectCodesBtn = New System.Windows.Forms.Button()
        Me.ActionItemsBtn = New System.Windows.Forms.Button()
        Me.AppointmentConfirmationTypesBtn = New System.Windows.Forms.Button()
        Me.CounselorsBtn = New System.Windows.Forms.Button()
        Me.AppointmentReservedTimesBtn = New System.Windows.Forms.Button()
        Me.AppointmentTypesBtn = New System.Windows.Forms.Button()
        Me.ApptTypesBtn = New System.Windows.Forms.Button()
        Me.AssetIdsBtn = New System.Windows.Forms.Button()
        Me.AttributeTypesBtn = New System.Windows.Forms.Button()
        Me.BankruptcyDistrictsBtn = New System.Windows.Forms.Button()
        Me.BankruptcyClassTypesBtn = New System.Windows.Forms.Button()
        Me.BudgetCategoriesBtn = New System.Windows.Forms.Button()
        Me.ContactMethodTypesBtn = New System.Windows.Forms.Button()
        Me.CountiesBtn = New System.Windows.Forms.Button()
        Me.CountriesBtn = New System.Windows.Forms.Button()
        Me.CreditorTypesBtn = New System.Windows.Forms.Button()
        Me.CreditorContactTypesBtn = New System.Windows.Forms.Button()
        Me.CalendarBtn = New System.Windows.Forms.Button()
        Me.ConfigBtn = New System.Windows.Forms.Button()
        Me.DropReasonsBtn = New System.Windows.Forms.Button()
        Me.EducationTypesBtn = New System.Windows.Forms.Button()
        Me.EmailTemplatesBtn = New System.Windows.Forms.Button()
        Me.EthnicityTypesBtn = New System.Windows.Forms.Button()
        Me.FaqItemsBtn = New System.Windows.Forms.Button()
        Me.FinancialProblemsBtn = New System.Windows.Forms.Button()
        Me.FirstContactTypesBtn = New System.Windows.Forms.Button()
        Me.GenderTypesBtn = New System.Windows.Forms.Button()
        Me.HousingArmHcsIdsBtn = New System.Windows.Forms.Button()
        Me.HousingARMRefInfoBtn = New System.Windows.Forms.Button()
        Me.HousingCounselorAttributeTypesBtn = New System.Windows.Forms.Button()
        Me.HousingTypesBtn = New System.Windows.Forms.Button()
        Me.LetterTypesBtn = New System.Windows.Forms.Button()
        Me.MaritalTypesBtn = New System.Windows.Forms.Button()
        Me.BanksBtn1 = New System.Windows.Forms.Button()
        Me.RaceTypesBtn = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.HousingTrainingCoursesBtn = New System.Windows.Forms.Button()
        Me.HousingTerminationReasonTypesBtn = New System.Windows.Forms.Button()
        Me.HousingStatusTypesBtn = New System.Windows.Forms.Button()
        Me.HousingPurposeOfVisitTypesBtn = New System.Windows.Forms.Button()
        Me.HousingMortgageTypesBtn = New System.Windows.Forms.Button()
        Me.HousingLoanTypesBtn = New System.Windows.Forms.Button()
        Me.HousingLoanPositionTypesBtn = New System.Windows.Forms.Button()
        Me.HousingHUDAssistanceTypesBtn = New System.Windows.Forms.Button()
        Me.HousingGrantTypesBtn = New System.Windows.Forms.Button()
        Me.HousingFinancingTypesBtn = New System.Windows.Forms.Button()
        Me.HousingFICONotIncludedReasonsBtn = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.IndicatorsBtn = New System.Windows.Forms.Button()
        Me.JobDescriptionsBtn = New System.Windows.Forms.Button()
        Me.LetterFieldsBtn = New System.Windows.Forms.Button()
        Me.LetterTablesBtn = New System.Windows.Forms.Button()
        Me.OfficeTypesBtn = New System.Windows.Forms.Button()
        Me.RegionsBtn = New System.Windows.Forms.Button()
        Me.PayFrequencyTypesBtn = New System.Windows.Forms.Button()
        Me.ProposalStatusTypesBtn = New System.Windows.Forms.Button()
        Me.ReferredByBtn = New System.Windows.Forms.Button()
        Me.MenusBtn = New System.Windows.Forms.Button()
        Me.MessagesBtn = New System.Windows.Forms.Button()
        Me.ReferredToBtn = New System.Windows.Forms.Button()
        Me.RelationshipTypesBtn = New System.Windows.Forms.Button()
        Me.ReportsBtn = New System.Windows.Forms.Button()
        Me.ResourcesBtn = New System.Windows.Forms.Button()
        Me.RetentionActionsBtn = New System.Windows.Forms.Button()
        Me.RetentionEventsBtn = New System.Windows.Forms.Button()
        Me.RPPSErrorCodesBtn = New System.Windows.Forms.Button()
        Me.RPPSAssetTypesBtn = New System.Windows.Forms.Button()
        Me.RPPSBillerTypesBtn = New System.Windows.Forms.Button()
        Me.StatesBtn = New System.Windows.Forms.Button()
        Me.SubjectsBtn = New System.Windows.Forms.Button()
        Me.TicklerTypesBtn = New System.Windows.Forms.Button()
        Me.TranTypesBtn = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'ACHRejectCodesBtn
        '
        Me.ACHRejectCodesBtn.Location = New System.Drawing.Point(12, 12)
        Me.ACHRejectCodesBtn.Name = "ACHRejectCodesBtn"
        Me.ACHRejectCodesBtn.Size = New System.Drawing.Size(144, 23)
        Me.ACHRejectCodesBtn.TabIndex = 0
        Me.ACHRejectCodesBtn.Text = "ACH Reject Codes"
        Me.ACHRejectCodesBtn.UseVisualStyleBackColor = True
        '
        'ActionItemsBtn
        '
        Me.ActionItemsBtn.Location = New System.Drawing.Point(12, 41)
        Me.ActionItemsBtn.Name = "ActionItemsBtn"
        Me.ActionItemsBtn.Size = New System.Drawing.Size(144, 23)
        Me.ActionItemsBtn.TabIndex = 1
        Me.ActionItemsBtn.Text = "Action Items"
        Me.ActionItemsBtn.UseVisualStyleBackColor = True
        '
        'AppointmentConfirmationTypesBtn
        '
        Me.AppointmentConfirmationTypesBtn.Location = New System.Drawing.Point(22, 78)
        Me.AppointmentConfirmationTypesBtn.Name = "AppointmentConfirmationTypesBtn"
        Me.AppointmentConfirmationTypesBtn.Size = New System.Drawing.Size(144, 23)
        Me.AppointmentConfirmationTypesBtn.TabIndex = 2
        Me.AppointmentConfirmationTypesBtn.Text = "Conference Types"
        Me.AppointmentConfirmationTypesBtn.UseVisualStyleBackColor = True
        '
        'CounselorsBtn
        '
        Me.CounselorsBtn.Location = New System.Drawing.Point(12, 70)
        Me.CounselorsBtn.Name = "CounselorsBtn"
        Me.CounselorsBtn.Size = New System.Drawing.Size(144, 23)
        Me.CounselorsBtn.TabIndex = 3
        Me.CounselorsBtn.Text = "Counselors"
        Me.CounselorsBtn.UseVisualStyleBackColor = True
        '
        'AppointmentReservedTimesBtn
        '
        Me.AppointmentReservedTimesBtn.Location = New System.Drawing.Point(22, 49)
        Me.AppointmentReservedTimesBtn.Name = "AppointmentReservedTimesBtn"
        Me.AppointmentReservedTimesBtn.Size = New System.Drawing.Size(144, 23)
        Me.AppointmentReservedTimesBtn.TabIndex = 4
        Me.AppointmentReservedTimesBtn.Text = "Reserved Times"
        Me.AppointmentReservedTimesBtn.UseVisualStyleBackColor = True
        '
        'AppointmentTypesBtn
        '
        Me.AppointmentTypesBtn.Location = New System.Drawing.Point(22, 20)
        Me.AppointmentTypesBtn.Name = "AppointmentTypesBtn"
        Me.AppointmentTypesBtn.Size = New System.Drawing.Size(144, 23)
        Me.AppointmentTypesBtn.TabIndex = 5
        Me.AppointmentTypesBtn.Text = "Types"
        Me.AppointmentTypesBtn.UseVisualStyleBackColor = True
        '
        'ApptTypesBtn
        '
        Me.ApptTypesBtn.Location = New System.Drawing.Point(22, 107)
        Me.ApptTypesBtn.Name = "ApptTypesBtn"
        Me.ApptTypesBtn.Size = New System.Drawing.Size(144, 23)
        Me.ApptTypesBtn.TabIndex = 6
        Me.ApptTypesBtn.Text = "Appt Types"
        Me.ApptTypesBtn.UseVisualStyleBackColor = True
        '
        'AssetIdsBtn
        '
        Me.AssetIdsBtn.Location = New System.Drawing.Point(12, 99)
        Me.AssetIdsBtn.Name = "AssetIdsBtn"
        Me.AssetIdsBtn.Size = New System.Drawing.Size(144, 23)
        Me.AssetIdsBtn.TabIndex = 7
        Me.AssetIdsBtn.Text = "Asset Ids"
        Me.AssetIdsBtn.UseVisualStyleBackColor = True
        '
        'AttributeTypesBtn
        '
        Me.AttributeTypesBtn.Location = New System.Drawing.Point(162, 302)
        Me.AttributeTypesBtn.Name = "AttributeTypesBtn"
        Me.AttributeTypesBtn.Size = New System.Drawing.Size(144, 23)
        Me.AttributeTypesBtn.TabIndex = 8
        Me.AttributeTypesBtn.Text = "Attribute Types"
        Me.AttributeTypesBtn.UseVisualStyleBackColor = True
        '
        'BankruptcyDistrictsBtn
        '
        Me.BankruptcyDistrictsBtn.Location = New System.Drawing.Point(12, 128)
        Me.BankruptcyDistrictsBtn.Name = "BankruptcyDistrictsBtn"
        Me.BankruptcyDistrictsBtn.Size = New System.Drawing.Size(144, 23)
        Me.BankruptcyDistrictsBtn.TabIndex = 9
        Me.BankruptcyDistrictsBtn.Text = "Bankruptcy Districts"
        Me.BankruptcyDistrictsBtn.UseVisualStyleBackColor = True
        '
        'BankruptcyClassTypesBtn
        '
        Me.BankruptcyClassTypesBtn.Location = New System.Drawing.Point(12, 157)
        Me.BankruptcyClassTypesBtn.Name = "BankruptcyClassTypesBtn"
        Me.BankruptcyClassTypesBtn.Size = New System.Drawing.Size(144, 23)
        Me.BankruptcyClassTypesBtn.TabIndex = 10
        Me.BankruptcyClassTypesBtn.Text = "Bankruptcy Class Types"
        Me.BankruptcyClassTypesBtn.UseVisualStyleBackColor = True
        '
        'BudgetCategoriesBtn
        '
        Me.BudgetCategoriesBtn.Location = New System.Drawing.Point(12, 186)
        Me.BudgetCategoriesBtn.Name = "BudgetCategoriesBtn"
        Me.BudgetCategoriesBtn.Size = New System.Drawing.Size(144, 23)
        Me.BudgetCategoriesBtn.TabIndex = 11
        Me.BudgetCategoriesBtn.Text = "Budget Categories"
        Me.BudgetCategoriesBtn.UseVisualStyleBackColor = True
        '
        'ContactMethodTypesBtn
        '
        Me.ContactMethodTypesBtn.Location = New System.Drawing.Point(12, 215)
        Me.ContactMethodTypesBtn.Name = "ContactMethodTypesBtn"
        Me.ContactMethodTypesBtn.Size = New System.Drawing.Size(144, 23)
        Me.ContactMethodTypesBtn.TabIndex = 12
        Me.ContactMethodTypesBtn.Text = "Contact Method Types"
        Me.ContactMethodTypesBtn.UseVisualStyleBackColor = True
        '
        'CountiesBtn
        '
        Me.CountiesBtn.Location = New System.Drawing.Point(12, 244)
        Me.CountiesBtn.Name = "CountiesBtn"
        Me.CountiesBtn.Size = New System.Drawing.Size(144, 23)
        Me.CountiesBtn.TabIndex = 13
        Me.CountiesBtn.Text = "Counties"
        Me.CountiesBtn.UseVisualStyleBackColor = True
        '
        'CountriesBtn
        '
        Me.CountriesBtn.Location = New System.Drawing.Point(12, 273)
        Me.CountriesBtn.Name = "CountriesBtn"
        Me.CountriesBtn.Size = New System.Drawing.Size(144, 23)
        Me.CountriesBtn.TabIndex = 14
        Me.CountriesBtn.Text = "Countries"
        Me.CountriesBtn.UseVisualStyleBackColor = True
        '
        'CreditorTypesBtn
        '
        Me.CreditorTypesBtn.Location = New System.Drawing.Point(12, 302)
        Me.CreditorTypesBtn.Name = "CreditorTypesBtn"
        Me.CreditorTypesBtn.Size = New System.Drawing.Size(144, 23)
        Me.CreditorTypesBtn.TabIndex = 15
        Me.CreditorTypesBtn.Text = "CreditorTypes"
        Me.CreditorTypesBtn.UseVisualStyleBackColor = True
        '
        'CreditorContactTypesBtn
        '
        Me.CreditorContactTypesBtn.Location = New System.Drawing.Point(12, 331)
        Me.CreditorContactTypesBtn.Name = "CreditorContactTypesBtn"
        Me.CreditorContactTypesBtn.Size = New System.Drawing.Size(144, 23)
        Me.CreditorContactTypesBtn.TabIndex = 16
        Me.CreditorContactTypesBtn.Text = "Creditor Contact Types"
        Me.CreditorContactTypesBtn.UseVisualStyleBackColor = True
        '
        'CalendarBtn
        '
        Me.CalendarBtn.Location = New System.Drawing.Point(12, 360)
        Me.CalendarBtn.Name = "CalendarBtn"
        Me.CalendarBtn.Size = New System.Drawing.Size(144, 23)
        Me.CalendarBtn.TabIndex = 17
        Me.CalendarBtn.Text = "Calendar"
        Me.CalendarBtn.UseVisualStyleBackColor = True
        '
        'ConfigBtn
        '
        Me.ConfigBtn.Location = New System.Drawing.Point(12, 389)
        Me.ConfigBtn.Name = "ConfigBtn"
        Me.ConfigBtn.Size = New System.Drawing.Size(144, 23)
        Me.ConfigBtn.TabIndex = 18
        Me.ConfigBtn.Text = "Config"
        Me.ConfigBtn.UseVisualStyleBackColor = True
        '
        'DropReasonsBtn
        '
        Me.DropReasonsBtn.Location = New System.Drawing.Point(12, 418)
        Me.DropReasonsBtn.Name = "DropReasonsBtn"
        Me.DropReasonsBtn.Size = New System.Drawing.Size(144, 23)
        Me.DropReasonsBtn.TabIndex = 19
        Me.DropReasonsBtn.Text = "Drop Reasons"
        Me.DropReasonsBtn.UseVisualStyleBackColor = True
        '
        'EducationTypesBtn
        '
        Me.EducationTypesBtn.Location = New System.Drawing.Point(12, 447)
        Me.EducationTypesBtn.Name = "EducationTypesBtn"
        Me.EducationTypesBtn.Size = New System.Drawing.Size(144, 23)
        Me.EducationTypesBtn.TabIndex = 20
        Me.EducationTypesBtn.Text = "Education Types"
        Me.EducationTypesBtn.UseVisualStyleBackColor = True
        '
        'EmailTemplatesBtn
        '
        Me.EmailTemplatesBtn.Location = New System.Drawing.Point(12, 476)
        Me.EmailTemplatesBtn.Name = "EmailTemplatesBtn"
        Me.EmailTemplatesBtn.Size = New System.Drawing.Size(144, 23)
        Me.EmailTemplatesBtn.TabIndex = 21
        Me.EmailTemplatesBtn.Text = "Email Templates"
        Me.EmailTemplatesBtn.UseVisualStyleBackColor = True
        '
        'EthnicityTypesBtn
        '
        Me.EthnicityTypesBtn.Location = New System.Drawing.Point(162, 12)
        Me.EthnicityTypesBtn.Name = "EthnicityTypesBtn"
        Me.EthnicityTypesBtn.Size = New System.Drawing.Size(144, 23)
        Me.EthnicityTypesBtn.TabIndex = 22
        Me.EthnicityTypesBtn.Text = "Ethnicity Types"
        Me.EthnicityTypesBtn.UseVisualStyleBackColor = True
        '
        'FaqItemsBtn
        '
        Me.FaqItemsBtn.Location = New System.Drawing.Point(162, 41)
        Me.FaqItemsBtn.Name = "FaqItemsBtn"
        Me.FaqItemsBtn.Size = New System.Drawing.Size(144, 23)
        Me.FaqItemsBtn.TabIndex = 23
        Me.FaqItemsBtn.Text = "FAQ Items"
        Me.FaqItemsBtn.UseVisualStyleBackColor = True
        '
        'FinancialProblemsBtn
        '
        Me.FinancialProblemsBtn.Location = New System.Drawing.Point(162, 70)
        Me.FinancialProblemsBtn.Name = "FinancialProblemsBtn"
        Me.FinancialProblemsBtn.Size = New System.Drawing.Size(144, 23)
        Me.FinancialProblemsBtn.TabIndex = 24
        Me.FinancialProblemsBtn.Text = "Financial Problems"
        Me.FinancialProblemsBtn.UseVisualStyleBackColor = True
        '
        'FirstContactTypesBtn
        '
        Me.FirstContactTypesBtn.Location = New System.Drawing.Point(162, 331)
        Me.FirstContactTypesBtn.Name = "FirstContactTypesBtn"
        Me.FirstContactTypesBtn.Size = New System.Drawing.Size(144, 23)
        Me.FirstContactTypesBtn.TabIndex = 25
        Me.FirstContactTypesBtn.Text = "First Contact Types"
        Me.FirstContactTypesBtn.UseVisualStyleBackColor = True
        '
        'GenderTypesBtn
        '
        Me.GenderTypesBtn.Location = New System.Drawing.Point(162, 273)
        Me.GenderTypesBtn.Name = "GenderTypesBtn"
        Me.GenderTypesBtn.Size = New System.Drawing.Size(144, 23)
        Me.GenderTypesBtn.TabIndex = 26
        Me.GenderTypesBtn.Text = "Gender Types"
        Me.GenderTypesBtn.UseVisualStyleBackColor = True
        '
        'HousingArmHcsIdsBtn
        '
        Me.HousingArmHcsIdsBtn.Location = New System.Drawing.Point(22, 54)
        Me.HousingArmHcsIdsBtn.Name = "HousingArmHcsIdsBtn"
        Me.HousingArmHcsIdsBtn.Size = New System.Drawing.Size(144, 23)
        Me.HousingArmHcsIdsBtn.TabIndex = 27
        Me.HousingArmHcsIdsBtn.Text = "ARM HCS IDs"
        Me.HousingArmHcsIdsBtn.UseVisualStyleBackColor = True
        '
        'HousingARMRefInfoBtn
        '
        Me.HousingARMRefInfoBtn.Location = New System.Drawing.Point(22, 83)
        Me.HousingARMRefInfoBtn.Name = "HousingARMRefInfoBtn"
        Me.HousingARMRefInfoBtn.Size = New System.Drawing.Size(144, 23)
        Me.HousingARMRefInfoBtn.TabIndex = 28
        Me.HousingARMRefInfoBtn.Text = "ARM Ref Info"
        Me.HousingARMRefInfoBtn.UseVisualStyleBackColor = True
        '
        'HousingCounselorAttributeTypesBtn
        '
        Me.HousingCounselorAttributeTypesBtn.Location = New System.Drawing.Point(22, 112)
        Me.HousingCounselorAttributeTypesBtn.Name = "HousingCounselorAttributeTypesBtn"
        Me.HousingCounselorAttributeTypesBtn.Size = New System.Drawing.Size(144, 23)
        Me.HousingCounselorAttributeTypesBtn.TabIndex = 29
        Me.HousingCounselorAttributeTypesBtn.Text = "Counselor Attributes"
        Me.HousingCounselorAttributeTypesBtn.UseVisualStyleBackColor = True
        '
        'HousingTypesBtn
        '
        Me.HousingTypesBtn.Location = New System.Drawing.Point(22, 25)
        Me.HousingTypesBtn.Name = "HousingTypesBtn"
        Me.HousingTypesBtn.Size = New System.Drawing.Size(144, 23)
        Me.HousingTypesBtn.TabIndex = 30
        Me.HousingTypesBtn.Text = "Types"
        Me.HousingTypesBtn.UseVisualStyleBackColor = True
        '
        'LetterTypesBtn
        '
        Me.LetterTypesBtn.Location = New System.Drawing.Point(162, 186)
        Me.LetterTypesBtn.Name = "LetterTypesBtn"
        Me.LetterTypesBtn.Size = New System.Drawing.Size(144, 23)
        Me.LetterTypesBtn.TabIndex = 31
        Me.LetterTypesBtn.Text = "Letter Types"
        Me.LetterTypesBtn.UseVisualStyleBackColor = True
        '
        'MaritalTypesBtn
        '
        Me.MaritalTypesBtn.Location = New System.Drawing.Point(162, 215)
        Me.MaritalTypesBtn.Name = "MaritalTypesBtn"
        Me.MaritalTypesBtn.Size = New System.Drawing.Size(144, 23)
        Me.MaritalTypesBtn.TabIndex = 32
        Me.MaritalTypesBtn.Text = "Marital Types"
        Me.MaritalTypesBtn.UseVisualStyleBackColor = True
        '
        'BanksBtn1
        '
        Me.BanksBtn1.Location = New System.Drawing.Point(312, 12)
        Me.BanksBtn1.Name = "BanksBtn1"
        Me.BanksBtn1.Size = New System.Drawing.Size(144, 23)
        Me.BanksBtn1.TabIndex = 33
        Me.BanksBtn1.Text = "Banks"
        Me.BanksBtn1.UseVisualStyleBackColor = True
        '
        'RaceTypesBtn
        '
        Me.RaceTypesBtn.Location = New System.Drawing.Point(162, 244)
        Me.RaceTypesBtn.Name = "RaceTypesBtn"
        Me.RaceTypesBtn.Size = New System.Drawing.Size(144, 23)
        Me.RaceTypesBtn.TabIndex = 34
        Me.RaceTypesBtn.Text = "Race Types"
        Me.RaceTypesBtn.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.HousingTrainingCoursesBtn)
        Me.GroupBox1.Controls.Add(Me.HousingTerminationReasonTypesBtn)
        Me.GroupBox1.Controls.Add(Me.HousingStatusTypesBtn)
        Me.GroupBox1.Controls.Add(Me.HousingPurposeOfVisitTypesBtn)
        Me.GroupBox1.Controls.Add(Me.HousingMortgageTypesBtn)
        Me.GroupBox1.Controls.Add(Me.HousingLoanTypesBtn)
        Me.GroupBox1.Controls.Add(Me.HousingLoanPositionTypesBtn)
        Me.GroupBox1.Controls.Add(Me.HousingHUDAssistanceTypesBtn)
        Me.GroupBox1.Controls.Add(Me.HousingGrantTypesBtn)
        Me.GroupBox1.Controls.Add(Me.HousingFinancingTypesBtn)
        Me.GroupBox1.Controls.Add(Me.HousingFICONotIncludedReasonsBtn)
        Me.GroupBox1.Controls.Add(Me.HousingArmHcsIdsBtn)
        Me.GroupBox1.Controls.Add(Me.HousingARMRefInfoBtn)
        Me.GroupBox1.Controls.Add(Me.HousingCounselorAttributeTypesBtn)
        Me.GroupBox1.Controls.Add(Me.HousingTypesBtn)
        Me.GroupBox1.Location = New System.Drawing.Point(500, 16)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(187, 469)
        Me.GroupBox1.TabIndex = 35
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Housing"
        '
        'HousingTrainingCoursesBtn
        '
        Me.HousingTrainingCoursesBtn.Location = New System.Drawing.Point(22, 431)
        Me.HousingTrainingCoursesBtn.Name = "HousingTrainingCoursesBtn"
        Me.HousingTrainingCoursesBtn.Size = New System.Drawing.Size(144, 23)
        Me.HousingTrainingCoursesBtn.TabIndex = 41
        Me.HousingTrainingCoursesBtn.TabStop = False
        Me.HousingTrainingCoursesBtn.Text = "Training Courses"
        Me.HousingTrainingCoursesBtn.UseVisualStyleBackColor = True
        '
        'HousingTerminationReasonTypesBtn
        '
        Me.HousingTerminationReasonTypesBtn.Location = New System.Drawing.Point(22, 402)
        Me.HousingTerminationReasonTypesBtn.Name = "HousingTerminationReasonTypesBtn"
        Me.HousingTerminationReasonTypesBtn.Size = New System.Drawing.Size(144, 23)
        Me.HousingTerminationReasonTypesBtn.TabIndex = 40
        Me.HousingTerminationReasonTypesBtn.TabStop = False
        Me.HousingTerminationReasonTypesBtn.Text = "Termination Reason Types"
        Me.HousingTerminationReasonTypesBtn.UseVisualStyleBackColor = True
        '
        'HousingStatusTypesBtn
        '
        Me.HousingStatusTypesBtn.Location = New System.Drawing.Point(22, 373)
        Me.HousingStatusTypesBtn.Name = "HousingStatusTypesBtn"
        Me.HousingStatusTypesBtn.Size = New System.Drawing.Size(144, 23)
        Me.HousingStatusTypesBtn.TabIndex = 39
        Me.HousingStatusTypesBtn.TabStop = False
        Me.HousingStatusTypesBtn.Text = "Status Types"
        Me.HousingStatusTypesBtn.UseVisualStyleBackColor = True
        '
        'HousingPurposeOfVisitTypesBtn
        '
        Me.HousingPurposeOfVisitTypesBtn.Location = New System.Drawing.Point(22, 344)
        Me.HousingPurposeOfVisitTypesBtn.Name = "HousingPurposeOfVisitTypesBtn"
        Me.HousingPurposeOfVisitTypesBtn.Size = New System.Drawing.Size(144, 23)
        Me.HousingPurposeOfVisitTypesBtn.TabIndex = 38
        Me.HousingPurposeOfVisitTypesBtn.TabStop = False
        Me.HousingPurposeOfVisitTypesBtn.Text = "Purpose of Visit Types"
        Me.HousingPurposeOfVisitTypesBtn.UseVisualStyleBackColor = True
        '
        'HousingMortgageTypesBtn
        '
        Me.HousingMortgageTypesBtn.Location = New System.Drawing.Point(22, 315)
        Me.HousingMortgageTypesBtn.Name = "HousingMortgageTypesBtn"
        Me.HousingMortgageTypesBtn.Size = New System.Drawing.Size(144, 23)
        Me.HousingMortgageTypesBtn.TabIndex = 37
        Me.HousingMortgageTypesBtn.TabStop = False
        Me.HousingMortgageTypesBtn.Text = "Mortgage Types"
        Me.HousingMortgageTypesBtn.UseVisualStyleBackColor = True
        '
        'HousingLoanTypesBtn
        '
        Me.HousingLoanTypesBtn.Location = New System.Drawing.Point(22, 286)
        Me.HousingLoanTypesBtn.Name = "HousingLoanTypesBtn"
        Me.HousingLoanTypesBtn.Size = New System.Drawing.Size(144, 23)
        Me.HousingLoanTypesBtn.TabIndex = 36
        Me.HousingLoanTypesBtn.TabStop = False
        Me.HousingLoanTypesBtn.Text = "Loan Types"
        Me.HousingLoanTypesBtn.UseVisualStyleBackColor = True
        '
        'HousingLoanPositionTypesBtn
        '
        Me.HousingLoanPositionTypesBtn.Location = New System.Drawing.Point(22, 257)
        Me.HousingLoanPositionTypesBtn.Name = "HousingLoanPositionTypesBtn"
        Me.HousingLoanPositionTypesBtn.Size = New System.Drawing.Size(144, 23)
        Me.HousingLoanPositionTypesBtn.TabIndex = 35
        Me.HousingLoanPositionTypesBtn.TabStop = False
        Me.HousingLoanPositionTypesBtn.Text = "Loan Position Types"
        Me.HousingLoanPositionTypesBtn.UseVisualStyleBackColor = True
        '
        'HousingHUDAssistanceTypesBtn
        '
        Me.HousingHUDAssistanceTypesBtn.Location = New System.Drawing.Point(22, 228)
        Me.HousingHUDAssistanceTypesBtn.Name = "HousingHUDAssistanceTypesBtn"
        Me.HousingHUDAssistanceTypesBtn.Size = New System.Drawing.Size(144, 23)
        Me.HousingHUDAssistanceTypesBtn.TabIndex = 34
        Me.HousingHUDAssistanceTypesBtn.TabStop = False
        Me.HousingHUDAssistanceTypesBtn.Text = "HUD Assistance Types"
        Me.HousingHUDAssistanceTypesBtn.UseVisualStyleBackColor = True
        '
        'HousingGrantTypesBtn
        '
        Me.HousingGrantTypesBtn.Location = New System.Drawing.Point(22, 199)
        Me.HousingGrantTypesBtn.Name = "HousingGrantTypesBtn"
        Me.HousingGrantTypesBtn.Size = New System.Drawing.Size(144, 23)
        Me.HousingGrantTypesBtn.TabIndex = 33
        Me.HousingGrantTypesBtn.TabStop = False
        Me.HousingGrantTypesBtn.Text = "Grant Types"
        Me.HousingGrantTypesBtn.UseVisualStyleBackColor = True
        '
        'HousingFinancingTypesBtn
        '
        Me.HousingFinancingTypesBtn.Location = New System.Drawing.Point(22, 170)
        Me.HousingFinancingTypesBtn.Name = "HousingFinancingTypesBtn"
        Me.HousingFinancingTypesBtn.Size = New System.Drawing.Size(144, 23)
        Me.HousingFinancingTypesBtn.TabIndex = 32
        Me.HousingFinancingTypesBtn.TabStop = False
        Me.HousingFinancingTypesBtn.Text = "Financing Types"
        Me.HousingFinancingTypesBtn.UseVisualStyleBackColor = True
        '
        'HousingFICONotIncludedReasonsBtn
        '
        Me.HousingFICONotIncludedReasonsBtn.Location = New System.Drawing.Point(22, 141)
        Me.HousingFICONotIncludedReasonsBtn.Name = "HousingFICONotIncludedReasonsBtn"
        Me.HousingFICONotIncludedReasonsBtn.Size = New System.Drawing.Size(144, 23)
        Me.HousingFICONotIncludedReasonsBtn.TabIndex = 31
        Me.HousingFICONotIncludedReasonsBtn.TabStop = False
        Me.HousingFICONotIncludedReasonsBtn.Text = "FICO Not Incl'd Reasons"
        Me.HousingFICONotIncludedReasonsBtn.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.AppointmentTypesBtn)
        Me.GroupBox3.Controls.Add(Me.AppointmentReservedTimesBtn)
        Me.GroupBox3.Controls.Add(Me.AppointmentConfirmationTypesBtn)
        Me.GroupBox3.Controls.Add(Me.ApptTypesBtn)
        Me.GroupBox3.Location = New System.Drawing.Point(500, 509)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(187, 144)
        Me.GroupBox3.TabIndex = 37
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Appointments"
        '
        'IndicatorsBtn
        '
        Me.IndicatorsBtn.Location = New System.Drawing.Point(312, 41)
        Me.IndicatorsBtn.Name = "IndicatorsBtn"
        Me.IndicatorsBtn.Size = New System.Drawing.Size(144, 23)
        Me.IndicatorsBtn.TabIndex = 38
        Me.IndicatorsBtn.Text = "Indicators"
        Me.IndicatorsBtn.UseVisualStyleBackColor = True
        '
        'JobDescriptionsBtn
        '
        Me.JobDescriptionsBtn.Location = New System.Drawing.Point(312, 70)
        Me.JobDescriptionsBtn.Name = "JobDescriptionsBtn"
        Me.JobDescriptionsBtn.Size = New System.Drawing.Size(144, 23)
        Me.JobDescriptionsBtn.TabIndex = 39
        Me.JobDescriptionsBtn.Text = "Job Descriptions"
        Me.JobDescriptionsBtn.UseVisualStyleBackColor = True
        '
        'LetterFieldsBtn
        '
        Me.LetterFieldsBtn.Location = New System.Drawing.Point(162, 99)
        Me.LetterFieldsBtn.Name = "LetterFieldsBtn"
        Me.LetterFieldsBtn.Size = New System.Drawing.Size(144, 23)
        Me.LetterFieldsBtn.TabIndex = 40
        Me.LetterFieldsBtn.Text = "Letter Fields"
        Me.LetterFieldsBtn.UseVisualStyleBackColor = True
        '
        'LetterTablesBtn
        '
        Me.LetterTablesBtn.Location = New System.Drawing.Point(162, 128)
        Me.LetterTablesBtn.Name = "LetterTablesBtn"
        Me.LetterTablesBtn.Size = New System.Drawing.Size(144, 23)
        Me.LetterTablesBtn.TabIndex = 41
        Me.LetterTablesBtn.Text = "Letter Tables"
        Me.LetterTablesBtn.UseVisualStyleBackColor = True
        '
        'OfficeTypesBtn
        '
        Me.OfficeTypesBtn.Location = New System.Drawing.Point(162, 157)
        Me.OfficeTypesBtn.Name = "OfficeTypesBtn"
        Me.OfficeTypesBtn.Size = New System.Drawing.Size(144, 23)
        Me.OfficeTypesBtn.TabIndex = 42
        Me.OfficeTypesBtn.Text = "Office Types"
        Me.OfficeTypesBtn.UseVisualStyleBackColor = True
        '
        'RegionsBtn
        '
        Me.RegionsBtn.Location = New System.Drawing.Point(312, 99)
        Me.RegionsBtn.Name = "RegionsBtn"
        Me.RegionsBtn.Size = New System.Drawing.Size(144, 23)
        Me.RegionsBtn.TabIndex = 43
        Me.RegionsBtn.Text = "Regions"
        Me.RegionsBtn.UseVisualStyleBackColor = True
        '
        'PayFrequencyTypesBtn
        '
        Me.PayFrequencyTypesBtn.Location = New System.Drawing.Point(312, 128)
        Me.PayFrequencyTypesBtn.Name = "PayFrequencyTypesBtn"
        Me.PayFrequencyTypesBtn.Size = New System.Drawing.Size(144, 23)
        Me.PayFrequencyTypesBtn.TabIndex = 44
        Me.PayFrequencyTypesBtn.Text = "Pay Frequency Types"
        Me.PayFrequencyTypesBtn.UseVisualStyleBackColor = True
        '
        'ProposalStatusTypesBtn
        '
        Me.ProposalStatusTypesBtn.Location = New System.Drawing.Point(312, 157)
        Me.ProposalStatusTypesBtn.Name = "ProposalStatusTypesBtn"
        Me.ProposalStatusTypesBtn.Size = New System.Drawing.Size(144, 23)
        Me.ProposalStatusTypesBtn.TabIndex = 45
        Me.ProposalStatusTypesBtn.Text = "Proposal Status Types"
        Me.ProposalStatusTypesBtn.UseVisualStyleBackColor = True
        '
        'ReferredByBtn
        '
        Me.ReferredByBtn.Location = New System.Drawing.Point(312, 186)
        Me.ReferredByBtn.Name = "ReferredByBtn"
        Me.ReferredByBtn.Size = New System.Drawing.Size(144, 23)
        Me.ReferredByBtn.TabIndex = 46
        Me.ReferredByBtn.Text = "Referred By"
        Me.ReferredByBtn.UseVisualStyleBackColor = True
        '
        'MenusBtn
        '
        Me.MenusBtn.Location = New System.Drawing.Point(162, 360)
        Me.MenusBtn.Name = "MenusBtn"
        Me.MenusBtn.Size = New System.Drawing.Size(144, 23)
        Me.MenusBtn.TabIndex = 47
        Me.MenusBtn.Text = "Menus"
        Me.MenusBtn.UseVisualStyleBackColor = True
        '
        'MessagesBtn
        '
        Me.MessagesBtn.Location = New System.Drawing.Point(162, 389)
        Me.MessagesBtn.Name = "MessagesBtn"
        Me.MessagesBtn.Size = New System.Drawing.Size(144, 23)
        Me.MessagesBtn.TabIndex = 48
        Me.MessagesBtn.Text = "Messages"
        Me.MessagesBtn.UseVisualStyleBackColor = True
        '
        'ReferredToBtn
        '
        Me.ReferredToBtn.Location = New System.Drawing.Point(312, 215)
        Me.ReferredToBtn.Name = "ReferredToBtn"
        Me.ReferredToBtn.Size = New System.Drawing.Size(144, 23)
        Me.ReferredToBtn.TabIndex = 49
        Me.ReferredToBtn.Text = "Referred To"
        Me.ReferredToBtn.UseVisualStyleBackColor = True
        '
        'RelationshipTypesBtn
        '
        Me.RelationshipTypesBtn.Location = New System.Drawing.Point(312, 244)
        Me.RelationshipTypesBtn.Name = "RelationshipTypesBtn"
        Me.RelationshipTypesBtn.Size = New System.Drawing.Size(144, 23)
        Me.RelationshipTypesBtn.TabIndex = 50
        Me.RelationshipTypesBtn.Text = "RelationshipTypes"
        Me.RelationshipTypesBtn.UseVisualStyleBackColor = True
        '
        'ReportsBtn
        '
        Me.ReportsBtn.Location = New System.Drawing.Point(312, 273)
        Me.ReportsBtn.Name = "ReportsBtn"
        Me.ReportsBtn.Size = New System.Drawing.Size(144, 23)
        Me.ReportsBtn.TabIndex = 51
        Me.ReportsBtn.Text = "Reports"
        Me.ReportsBtn.UseVisualStyleBackColor = True
        '
        'ResourcesBtn
        '
        Me.ResourcesBtn.Location = New System.Drawing.Point(312, 302)
        Me.ResourcesBtn.Name = "ResourcesBtn"
        Me.ResourcesBtn.Size = New System.Drawing.Size(144, 23)
        Me.ResourcesBtn.TabIndex = 52
        Me.ResourcesBtn.Text = "Resources"
        Me.ResourcesBtn.UseVisualStyleBackColor = True
        '
        'RetentionActionsBtn
        '
        Me.RetentionActionsBtn.Location = New System.Drawing.Point(312, 331)
        Me.RetentionActionsBtn.Name = "RetentionActionsBtn"
        Me.RetentionActionsBtn.Size = New System.Drawing.Size(144, 23)
        Me.RetentionActionsBtn.TabIndex = 53
        Me.RetentionActionsBtn.Text = "RetentionActions"
        Me.RetentionActionsBtn.UseVisualStyleBackColor = True
        '
        'RetentionEventsBtn
        '
        Me.RetentionEventsBtn.Location = New System.Drawing.Point(312, 360)
        Me.RetentionEventsBtn.Name = "RetentionEventsBtn"
        Me.RetentionEventsBtn.Size = New System.Drawing.Size(144, 23)
        Me.RetentionEventsBtn.TabIndex = 54
        Me.RetentionEventsBtn.Text = "RetentionEvents"
        Me.RetentionEventsBtn.UseVisualStyleBackColor = True
        '
        'RPPSErrorCodesBtn
        '
        Me.RPPSErrorCodesBtn.Location = New System.Drawing.Point(162, 418)
        Me.RPPSErrorCodesBtn.Name = "RPPSErrorCodesBtn"
        Me.RPPSErrorCodesBtn.Size = New System.Drawing.Size(144, 23)
        Me.RPPSErrorCodesBtn.TabIndex = 55
        Me.RPPSErrorCodesBtn.Text = "RPPS Error Codes"
        Me.RPPSErrorCodesBtn.UseVisualStyleBackColor = True
        '
        'RPPSAssetTypesBtn
        '
        Me.RPPSAssetTypesBtn.Location = New System.Drawing.Point(162, 447)
        Me.RPPSAssetTypesBtn.Name = "RPPSAssetTypesBtn"
        Me.RPPSAssetTypesBtn.Size = New System.Drawing.Size(144, 23)
        Me.RPPSAssetTypesBtn.TabIndex = 56
        Me.RPPSAssetTypesBtn.Text = "RPPS Asset Types"
        Me.RPPSAssetTypesBtn.UseVisualStyleBackColor = True
        '
        'RPPSBillerTypesBtn
        '
        Me.RPPSBillerTypesBtn.Location = New System.Drawing.Point(162, 476)
        Me.RPPSBillerTypesBtn.Name = "RPPSBillerTypesBtn"
        Me.RPPSBillerTypesBtn.Size = New System.Drawing.Size(144, 23)
        Me.RPPSBillerTypesBtn.TabIndex = 57
        Me.RPPSBillerTypesBtn.Text = "RPPS Biller Types"
        Me.RPPSBillerTypesBtn.UseVisualStyleBackColor = True
        '
        'StatesBtn
        '
        Me.StatesBtn.Location = New System.Drawing.Point(312, 389)
        Me.StatesBtn.Name = "StatesBtn"
        Me.StatesBtn.Size = New System.Drawing.Size(144, 23)
        Me.StatesBtn.TabIndex = 58
        Me.StatesBtn.Text = "States"
        Me.StatesBtn.UseVisualStyleBackColor = True
        '
        'SubjectsBtn
        '
        Me.SubjectsBtn.Location = New System.Drawing.Point(312, 418)
        Me.SubjectsBtn.Name = "SubjectsBtn"
        Me.SubjectsBtn.Size = New System.Drawing.Size(144, 23)
        Me.SubjectsBtn.TabIndex = 59
        Me.SubjectsBtn.Text = "Subjects"
        Me.SubjectsBtn.UseVisualStyleBackColor = True
        '
        'TicklerTypesBtn
        '
        Me.TicklerTypesBtn.Location = New System.Drawing.Point(312, 447)
        Me.TicklerTypesBtn.Name = "TicklerTypesBtn"
        Me.TicklerTypesBtn.Size = New System.Drawing.Size(144, 23)
        Me.TicklerTypesBtn.TabIndex = 60
        Me.TicklerTypesBtn.Text = "Tickler Types"
        Me.TicklerTypesBtn.UseVisualStyleBackColor = True
        '
        'TranTypesBtn
        '
        Me.TranTypesBtn.Location = New System.Drawing.Point(312, 476)
        Me.TranTypesBtn.Name = "TranTypesBtn"
        Me.TranTypesBtn.Size = New System.Drawing.Size(144, 23)
        Me.TranTypesBtn.TabIndex = 61
        Me.TranTypesBtn.Text = "Tran Types"
        Me.TranTypesBtn.UseVisualStyleBackColor = True
        '
        'MainMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(697, 656)
        Me.Controls.Add(Me.TranTypesBtn)
        Me.Controls.Add(Me.TicklerTypesBtn)
        Me.Controls.Add(Me.SubjectsBtn)
        Me.Controls.Add(Me.StatesBtn)
        Me.Controls.Add(Me.RPPSBillerTypesBtn)
        Me.Controls.Add(Me.RPPSAssetTypesBtn)
        Me.Controls.Add(Me.RPPSErrorCodesBtn)
        Me.Controls.Add(Me.RetentionEventsBtn)
        Me.Controls.Add(Me.RetentionActionsBtn)
        Me.Controls.Add(Me.ResourcesBtn)
        Me.Controls.Add(Me.ReportsBtn)
        Me.Controls.Add(Me.RelationshipTypesBtn)
        Me.Controls.Add(Me.ReferredToBtn)
        Me.Controls.Add(Me.MessagesBtn)
        Me.Controls.Add(Me.MenusBtn)
        Me.Controls.Add(Me.ReferredByBtn)
        Me.Controls.Add(Me.ProposalStatusTypesBtn)
        Me.Controls.Add(Me.PayFrequencyTypesBtn)
        Me.Controls.Add(Me.RegionsBtn)
        Me.Controls.Add(Me.OfficeTypesBtn)
        Me.Controls.Add(Me.LetterTablesBtn)
        Me.Controls.Add(Me.LetterFieldsBtn)
        Me.Controls.Add(Me.JobDescriptionsBtn)
        Me.Controls.Add(Me.IndicatorsBtn)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.RaceTypesBtn)
        Me.Controls.Add(Me.BanksBtn1)
        Me.Controls.Add(Me.MaritalTypesBtn)
        Me.Controls.Add(Me.LetterTypesBtn)
        Me.Controls.Add(Me.GenderTypesBtn)
        Me.Controls.Add(Me.FirstContactTypesBtn)
        Me.Controls.Add(Me.FinancialProblemsBtn)
        Me.Controls.Add(Me.FaqItemsBtn)
        Me.Controls.Add(Me.EthnicityTypesBtn)
        Me.Controls.Add(Me.EmailTemplatesBtn)
        Me.Controls.Add(Me.EducationTypesBtn)
        Me.Controls.Add(Me.DropReasonsBtn)
        Me.Controls.Add(Me.ConfigBtn)
        Me.Controls.Add(Me.CalendarBtn)
        Me.Controls.Add(Me.CreditorContactTypesBtn)
        Me.Controls.Add(Me.CreditorTypesBtn)
        Me.Controls.Add(Me.CountriesBtn)
        Me.Controls.Add(Me.CountiesBtn)
        Me.Controls.Add(Me.ContactMethodTypesBtn)
        Me.Controls.Add(Me.BudgetCategoriesBtn)
        Me.Controls.Add(Me.BankruptcyClassTypesBtn)
        Me.Controls.Add(Me.BankruptcyDistrictsBtn)
        Me.Controls.Add(Me.AttributeTypesBtn)
        Me.Controls.Add(Me.AssetIdsBtn)
        Me.Controls.Add(Me.CounselorsBtn)
        Me.Controls.Add(Me.ActionItemsBtn)
        Me.Controls.Add(Me.ACHRejectCodesBtn)
        Me.Name = "MainMenu"
        Me.Text = "Lookup Table Administration and Configuration"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Public WithEvents ACHRejectCodesBtn As System.Windows.Forms.Button
    Public WithEvents ActionItemsBtn As System.Windows.Forms.Button
    Friend WithEvents AppointmentConfirmationTypesBtn As System.Windows.Forms.Button
    Friend WithEvents CounselorsBtn As System.Windows.Forms.Button
    Friend WithEvents AppointmentReservedTimesBtn As System.Windows.Forms.Button
    Friend WithEvents AppointmentTypesBtn As System.Windows.Forms.Button
    Friend WithEvents ApptTypesBtn As System.Windows.Forms.Button
    Friend WithEvents AssetIdsBtn As System.Windows.Forms.Button
    Friend WithEvents AttributeTypesBtn As System.Windows.Forms.Button
    Friend WithEvents BankruptcyDistrictsBtn As System.Windows.Forms.Button
    Friend WithEvents BankruptcyClassTypesBtn As System.Windows.Forms.Button
    Friend WithEvents BudgetCategoriesBtn As System.Windows.Forms.Button
    Friend WithEvents ContactMethodTypesBtn As System.Windows.Forms.Button
    Friend WithEvents CountiesBtn As System.Windows.Forms.Button
    Friend WithEvents CountriesBtn As System.Windows.Forms.Button
    Friend WithEvents CreditorTypesBtn As System.Windows.Forms.Button
    Friend WithEvents CreditorContactTypesBtn As System.Windows.Forms.Button
    Friend WithEvents CalendarBtn As System.Windows.Forms.Button
    Friend WithEvents ConfigBtn As System.Windows.Forms.Button
    Friend WithEvents DropReasonsBtn As System.Windows.Forms.Button
    Friend WithEvents EducationTypesBtn As System.Windows.Forms.Button
    Friend WithEvents EmailTemplatesBtn As System.Windows.Forms.Button
    Friend WithEvents EthnicityTypesBtn As System.Windows.Forms.Button
    Friend WithEvents FaqItemsBtn As System.Windows.Forms.Button
    Friend WithEvents FinancialProblemsBtn As System.Windows.Forms.Button
    Friend WithEvents FirstContactTypesBtn As System.Windows.Forms.Button
    Friend WithEvents GenderTypesBtn As System.Windows.Forms.Button
    Friend WithEvents HousingArmHcsIdsBtn As System.Windows.Forms.Button
    Friend WithEvents HousingARMRefInfoBtn As System.Windows.Forms.Button
    Friend WithEvents HousingCounselorAttributeTypesBtn As System.Windows.Forms.Button
    Friend WithEvents HousingTypesBtn As System.Windows.Forms.Button
    Friend WithEvents LetterTypesBtn As System.Windows.Forms.Button
    Friend WithEvents MaritalTypesBtn As System.Windows.Forms.Button
    Friend WithEvents BanksBtn1 As System.Windows.Forms.Button
    Friend WithEvents RaceTypesBtn As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents HousingFICONotIncludedReasonsBtn As System.Windows.Forms.Button
    Friend WithEvents HousingFinancingTypesBtn As System.Windows.Forms.Button
    Friend WithEvents HousingGrantTypesBtn As System.Windows.Forms.Button
    Friend WithEvents HousingHUDAssistanceTypesBtn As System.Windows.Forms.Button
    Friend WithEvents HousingLoanPositionTypesBtn As System.Windows.Forms.Button
    Friend WithEvents HousingLoanTypesBtn As System.Windows.Forms.Button
    Friend WithEvents HousingMortgageTypesBtn As System.Windows.Forms.Button
    Friend WithEvents HousingPurposeOfVisitTypesBtn As System.Windows.Forms.Button
    Friend WithEvents HousingStatusTypesBtn As System.Windows.Forms.Button
    Friend WithEvents HousingTerminationReasonTypesBtn As System.Windows.Forms.Button
    Friend WithEvents HousingTrainingCoursesBtn As System.Windows.Forms.Button
    Friend WithEvents IndicatorsBtn As System.Windows.Forms.Button
    Friend WithEvents JobDescriptionsBtn As System.Windows.Forms.Button
    Friend WithEvents LetterFieldsBtn As System.Windows.Forms.Button
    Friend WithEvents LetterTablesBtn As System.Windows.Forms.Button
    Friend WithEvents OfficeTypesBtn As System.Windows.Forms.Button
    Friend WithEvents RegionsBtn As System.Windows.Forms.Button
    Friend WithEvents PayFrequencyTypesBtn As System.Windows.Forms.Button
    Friend WithEvents ProposalStatusTypesBtn As System.Windows.Forms.Button
    Friend WithEvents ReferredByBtn As System.Windows.Forms.Button
    Friend WithEvents MenusBtn As System.Windows.Forms.Button
    Friend WithEvents MessagesBtn As System.Windows.Forms.Button
    Friend WithEvents ReferredToBtn As System.Windows.Forms.Button
    Friend WithEvents RelationshipTypesBtn As System.Windows.Forms.Button
    Friend WithEvents ReportsBtn As System.Windows.Forms.Button
    Friend WithEvents ResourcesBtn As System.Windows.Forms.Button
    Friend WithEvents RetentionActionsBtn As System.Windows.Forms.Button
    Friend WithEvents RetentionEventsBtn As System.Windows.Forms.Button
    Friend WithEvents RPPSErrorCodesBtn As System.Windows.Forms.Button
    Friend WithEvents RPPSAssetTypesBtn As System.Windows.Forms.Button
    Friend WithEvents RPPSBillerTypesBtn As System.Windows.Forms.Button
    Friend WithEvents StatesBtn As System.Windows.Forms.Button
    Friend WithEvents SubjectsBtn As System.Windows.Forms.Button
    Friend WithEvents TicklerTypesBtn As System.Windows.Forms.Button
    Friend WithEvents TranTypesBtn As System.Windows.Forms.Button

End Class
