#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Explicit On 
Option Strict On

Imports DebtPlus.UI.Common

Public Module Program
    <STAThread()> _
    Public Sub Main(ByVal arguments() As String)
        Startup.Register(arguments)

        AddHandler Application.ThreadException, AddressOf ThreadExceptionHandler

        Dim ap As New ArgParser()
        If ap.Parse(arguments) Then

            ' Do Some Dependency Injection
            Using Sql As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
                Repository.Setup.ConnectionString = Sql.ConnectionString
                Repository.Setup.CommandTimeout = Sql.CommandTimeout
            End Using

            Using mainMenu As New MainMenu()
                mainMenu.ShowDialog()
            End Using
        End If
    End Sub

    ''' <summary>
    ''' Thread Exception Handler
    ''' </summary>
    Private Sub ThreadExceptionHandler(ByVal Sender As Object, ByVal e As System.Threading.ThreadExceptionEventArgs)
        Dim ex As Exception = DirectCast(e.Exception, Exception)
        DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Thread Exception Occurred", MessageBoxButtons.OK, MessageBoxIcon.Stop)
    End Sub
End Module
