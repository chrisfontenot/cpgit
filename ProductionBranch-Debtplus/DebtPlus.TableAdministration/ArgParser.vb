Option Compare Binary
Option Explicit On 
Option Strict On
Friend Class ArgParser
    Inherits DebtPlus.Utils.ArgParserBase

    ''' <summary>
    ''' Create an instance of our class
    ''' </summary>
    Public Sub New()
        MyBase.New(New String() {})
    End Sub

    ''' <summary>
    ''' Generate the command usage information
    ''' </summary>
    Protected Overrides Sub OnUsage(ByVal errorInfo As String)
        If errorInfo IsNot Nothing Then
            AppendErrorLine(String.Format("Invalid parameter: {0}" + Environment.NewLine, errorInfo))
        End If

        Dim _asm As System.Reflection.Assembly = System.Reflection.Assembly.GetExecutingAssembly
        Dim fname As String = System.IO.Path.GetFileName(_asm.CodeBase)
        AppendErrorLine("Usage: " + fname)
    End Sub
End Class
