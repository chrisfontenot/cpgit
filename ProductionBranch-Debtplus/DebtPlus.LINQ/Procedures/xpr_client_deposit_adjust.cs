﻿using System;
using System.Data.Linq;
using System.Reflection;

namespace DebtPlus.LINQ
{
    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_client_deposit_adjust")]
        public Int32 xpr_client_deposit_adjust(Int32 client, Decimal new_amount)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, (MethodInfo)(MethodInfo.GetCurrentMethod()), new object[] { client, new_amount });
            return (int)result.ReturnValue;
        }
    }
}