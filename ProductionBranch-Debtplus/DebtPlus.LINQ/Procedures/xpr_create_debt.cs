﻿namespace DebtPlus.LINQ
{
    using System.Data.Linq;
    using System.Linq;
    using System.Reflection;

    [global::System.Data.Linq.Mapping.TableAttribute(Name = "dbo.view_debt_creditor_list")]
    public partial class view_debt_creditor_list
    {
        private int _creditor_id;
        private string _creditor;
        private string _sic;
        private System.Nullable<double> _fairshare_pct_eft;
        private System.Nullable<double> _fairshare_pct_check;
        private string _name;
        private string _comment;
        private string _fairshare;
        private string _address;
        private bool _prohibit_use;

        public view_debt_creditor_list()
        {
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_creditor_id", DbType = "Int NOT NULL")]
        public int creditor_id
        {
            get
            {
                return this._creditor_id;
            }
            set
            {
                if ((this._creditor_id != value))
                {
                    this._creditor_id = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_creditor", DbType = "VarChar(10) NOT NULL", CanBeNull = false)]
        public string creditor
        {
            get
            {
                return this._creditor;
            }
            set
            {
                if ((this._creditor != value))
                {
                    this._creditor = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_sic", DbType = "VarChar(20)")]
        public string sic
        {
            get
            {
                return this._sic;
            }
            set
            {
                if ((this._sic != value))
                {
                    this._sic = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_fairshare_pct_eft", DbType = "Float")]
        public System.Nullable<double> fairshare_pct_eft
        {
            get
            {
                return this._fairshare_pct_eft;
            }
            set
            {
                if ((this._fairshare_pct_eft != value))
                {
                    this._fairshare_pct_eft = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_fairshare_pct_check", DbType = "Float")]
        public System.Nullable<double> fairshare_pct_check
        {
            get
            {
                return this._fairshare_pct_check;
            }
            set
            {
                if ((this._fairshare_pct_check != value))
                {
                    this._fairshare_pct_check = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_name", DbType = "VarChar(511)")]
        public string name
        {
            get
            {
                return this._name;
            }
            set
            {
                if ((this._name != value))
                {
                    this._name = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_comment", DbType = "VarChar(80)")]
        public string comment
        {
            get
            {
                return this._comment;
            }
            set
            {
                if ((this._comment != value))
                {
                    this._comment = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_fairshare", DbType = "VarChar(10)")]
        public string fairshare
        {
            get
            {
                return this._fairshare;
            }
            set
            {
                if ((this._fairshare != value))
                {
                    this._fairshare = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_address", DbType = "VarChar(800)")]
        public string address
        {
            get
            {
                return this._address;
            }
            set
            {
                if ((this._address != value))
                {
                    this._address = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_prohibit_use", DbType = "Bit NOT NULL")]
        public bool prohibit_use
        {
            get
            {
                return this._prohibit_use;
            }
            set
            {
                if ((this._prohibit_use != value))
                {
                    this._prohibit_use = value;
                }
            }
        }
    }

    partial class DebtPlusDataContext
    {
        public System.Data.Linq.Table<view_debt_creditor_list> view_debt_creditor_lists
        {
            get
            {
                return this.GetTable<view_debt_creditor_list>();
            }
        }

        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_debt_creation_byName")]
        private ISingleResult<view_debt_creditor_list> private_xpr_debt_creation_byName([global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "VarChar(80)")] string searchName)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), searchName);
            return (ISingleResult<view_debt_creditor_list>)(result.ReturnValue);
        }

        public System.Collections.Generic.List<view_debt_creditor_list> xpr_debt_creation_ByName(string searchName)
        {
            ISingleResult<view_debt_creditor_list> result = private_xpr_debt_creation_byName(searchName);
            if (result != null)
            {
                return result.ToList<view_debt_creditor_list>();
            }
            return null;
        }

        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_debt_creation_byAccount")]
        private ISingleResult<view_debt_creditor_list> private_xpr_debt_creation_byAccount([global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "VarChar(80)")] string accountNumber)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), accountNumber);
            return ((ISingleResult<view_debt_creditor_list>)(result.ReturnValue));
        }

        public System.Collections.Generic.List<view_debt_creditor_list> xpr_debt_creation_byAccount(string accountNumber)
        {
            ISingleResult<view_debt_creditor_list> result = private_xpr_debt_creation_byAccount(accountNumber);
            if (result != null)
            {
                return result.ToList<view_debt_creditor_list>();
            }
            return null;
        }

        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_create_debt")]
        public int xpr_create_debt([global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> client, [global::System.Data.Linq.Mapping.ParameterAttribute(Name = "Creditor", DbType = "VarChar(10)")] string creditor, [global::System.Data.Linq.Mapping.ParameterAttribute(Name = "Creditor_Name", DbType = "VarChar(50)")] string creditor_Name, [global::System.Data.Linq.Mapping.ParameterAttribute(Name = "Account_Number", DbType = "VarChar(50)")] string account_Number)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), client, creditor, creditor_Name, account_Number);
            return ((int)(result.ReturnValue));
        }
    }
}