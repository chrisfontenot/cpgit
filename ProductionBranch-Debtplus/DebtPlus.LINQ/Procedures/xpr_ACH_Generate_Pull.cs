﻿#pragma warning disable 1591

namespace DebtPlus.LINQ
{
    using System.Reflection;

    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_ACH_Generate_Pull", IsComposable = true)]
        public int xpr_ACH_Generate_Pull([global::System.Data.Linq.Mapping.ParameterAttribute(Name = "ACH_Pull_Date", DbType = "DateTime")] System.Nullable<System.DateTime> aCH_Pull_Date, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> ach_file, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> bank)
        {
            return ((int)(this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), aCH_Pull_Date, ach_file, bank).ReturnValue));
        }
    }
}