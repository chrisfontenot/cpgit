﻿namespace DebtPlus.LINQ
{
    using System.Data.Linq;
    using System.Linq;
    using System.Reflection;

    public partial class xpr_housing_arm_v4_client
    {
        public xpr_housing_arm_v4_client()
        {
        }

        private int _is_hud_client;
        private System.Nullable<int> _Ethnicity_Clients_Counseling_Hispanic;
        private System.Nullable<int> _Ethnicity_Clients_Counseling_No_Response;
        private System.Nullable<int> _Ethnicity_Clients_Counseling_Non_Hispanic;
        private System.Nullable<int> _Household_Lives_In_Rural_Area;
        private System.Nullable<int> _Rural_Area_No_Response;
        private System.Nullable<int> _Household_Does_Not_Live_In_Rural_Area;
        private System.Nullable<int> _Household_Is_Limited_English_Proficient;
        private System.Nullable<int> _Limited_English_Proficient_No_Response;
        private System.Nullable<int> _Household_Is_Not_Limited_English_Proficient;
        private System.Nullable<int> _Less30_AMI_Level;
        private System.Nullable<int> _a30_49_AMI_Level;
        private System.Nullable<int> _a50_79_AMI_Level;
        private System.Nullable<int> _a80_100_AMI_Level;
        private System.Nullable<int> _Greater100_AMI_Level;
        private System.Nullable<int> _AMI_No_Response;
        private System.Nullable<int> _MultiRace_Clients_Counseling_AMINDWHT;
        private System.Nullable<int> _MultiRace_Clients_Counseling_AMRCINDBLK;
        private System.Nullable<int> _MultiRace_Clients_Counseling_ASIANWHT;
        private System.Nullable<int> _MultiRace_Clients_Counseling_BLKWHT;
        private System.Nullable<int> _MultiRace_Clients_Counseling_NoResponse;
        private System.Nullable<int> _MultiRace_Clients_Counseling_OtherMLTRC;
        private System.Nullable<int> _Race_Clients_Counseling_American_Indian_Alaskan_Native;
        private System.Nullable<int> _Race_Clients_Counseling_Asian;
        private System.Nullable<int> _Race_Clients_Counseling_Black_AfricanAmerican;
        private System.Nullable<int> _Race_Clients_Counseling_Pacific_Islanders;
        private System.Nullable<int> _Race_Clients_Counseling_White;
        private System.Nullable<int> _Compl_HomeMaint_FinMngt;
        private System.Nullable<int> _Compl_Workshop_Predatory_Lend;
        private System.Nullable<int> _Compl_Help_FairHousing_Workshop;
        private System.Nullable<int> _Compl_Resolv_Prevent_Mortg_Deliq;
        private System.Nullable<int> _Counseling_Rental_Workshop;
        private System.Nullable<int> _Compl_HomeBuyer_Educ_Workshop;
        private System.Nullable<int> _Compl_NonDelinqency_PostPurchase_Workshop;
        private System.Nullable<int> _Compl_Other_Workshop;
        private System.Nullable<int> _One_Homeless_Assistance_Counseling;
        private System.Nullable<int> _One_Rental_Topics_Counseling;
        private System.Nullable<int> _One_PrePurchase_HomeBuying_Counseling;
        private System.Nullable<int> _One_Home_Maintenance_Fin_Management_Counseling;
        private System.Nullable<int> _One_Reverse_Mortgage_Counseling;
        private System.Nullable<int> _One_Resolv_Prevent_Mortg_Delinq_Counseling;
        private System.Nullable<int> _Impact_One_On_One_And_Group;
        private System.Nullable<int> _Impact_Received_Info_Fair_Housing;
        private System.Nullable<int> _Impact_Developed_Sustainable_Budget;
        private System.Nullable<int> _Impact_Improved_Financial_Capacity;
        private System.Nullable<int> _Impact_Gained_Access_Resources_Improve_Housing;
        private System.Nullable<int> _Impact_Gained_Access_NonHousing_Resources;
        private System.Nullable<int> _Impact_Homeless_Obtained_Housing;
        private System.Nullable<int> _Impact_Received_Rental_Counseling_Avoided_Eviction;
        private System.Nullable<int> _Impact_Received_Rental_Counseling_Improved_Living_Conditions;
        private System.Nullable<int> _Impact_Received_PrePurchase_Counseling_Purchased_Housing;
        private System.Nullable<int> _Impact_Received_Reverse_Mortgage_Counseling_Obtained_HECM;
        private System.Nullable<int> _Impact_Received_NonDelinquency_PostPurchase_Counseling_Improve_Conditions_Affordability;
        private System.Nullable<int> _Impact_Prevented_Resolved_Mortgage_Default;
        private System.Nullable<int> _Client_ID_Num;
        private System.Nullable<int> _Client_Purpose_Of_Visit;
        private System.Nullable<int> _Client_Outcome_Of_Visit;
        private System.Nullable<int> _Client_Activity_Type;
        private System.Nullable<int> _Client_Case_Num;
        private System.Nullable<int> _Client_Counselor_ID;
        private System.Nullable<int> _Client_Head_Of_Household_Type;
        private System.Nullable<int> _Client_Credit_Score;
        private System.Nullable<int> _Client_Credit_Score_Source;
        private System.Nullable<int> _Client_No_Credit_Score_Reason;
        private string _Client_First_Name;
        private string _Client_Last_Name;
        private string _Client_Middle_Name;
        private string _Client_Street_Address_1;
        private string _Client_Street_Address_2;
        private string _Client_City;
        private System.Nullable<int> _Client_State;
        private string _Client_ZipCode;
        private string _Client_New_Street_Address_1;
        private string _Client_New_Street_Address_2;
        private string _Client_New_City;
        private System.Nullable<int> _Client_New_State;
        private string _Client_New_ZipCode;
        private string _Client_Spouse_First_Name;
        private string _Client_Spouse_Last_Name;
        private string _Client_Spouse_Middle_Name;
        private System.Nullable<bool> _Client_Farm_Worker;
        private System.Nullable<bool> _Client_Colonias_Resident;
        private System.Nullable<bool> _Client_Disabled;
        private System.Nullable<bool> _Client_HECM_Certificate;
        private System.Nullable<bool> _Client_Predatory_Lending;
        private System.Nullable<bool> _Client_FirstTime_Home_Buyer;
        private System.Nullable<bool> _Client_Discrimination_Victim;
        private System.Nullable<bool> _Client_Mortgage_Deliquency;
        private System.Nullable<bool> _Client_Second_Loan_Exists;
        private System.Nullable<bool> _Client_Intake_Loan_Type_Is_Hybrid_ARM;
        private System.Nullable<bool> _Client_Intake_Loan_Type_Is_Option_ARM;
        private System.Nullable<bool> _Client_Intake_Loan_Type_Is_Interest_Only;
        private System.Nullable<bool> _Client_Intake_Loan_Type_Is_FHA_Or_VA_Insured;
        private System.Nullable<bool> _Client_Intake_Loan_Type_Is_Privately_Held;
        private System.Nullable<bool> _Client_Intake_Loan_Type_Has_Interest_Rate_Reset;
        private System.Nullable<System.DateTime> _Client_Counsel_Session_DT_Start;
        private System.Nullable<System.DateTime> _Client_Counsel_Session_DT_End;
        private System.Nullable<System.DateTime> _Client_Intake_DT;
        private System.Nullable<System.DateTime> _Client_Birth_DT;
        private System.Nullable<System.DateTime> _Client_HECM_Certificate_Issue_Date;
        private System.Nullable<System.DateTime> _Client_HECM_Certificate_Expiration_Date;
        private string _Client_HECM_Certificate_ID;
        private System.Nullable<System.DateTime> _Client_Sales_Contract_Signed;
        private System.Nullable<decimal> _Client_Grant_Amount_Used;
        private System.Nullable<decimal> _Client_Mortgage_Closing_Cost;
        private System.Nullable<double> _Client_Mortgage_Interest_Rate;
        private System.Nullable<int> _Client_Counseling_Termination;
        private System.Nullable<int> _Client_Income_Level;
        private System.Nullable<int> _Client_Highest_Educ_Grade;
        private System.Nullable<int> _Client_HUD_Assistance;
        private System.Nullable<int> _Client_Dependents_Num;
        private System.Nullable<int> _Client_Language_Spoken;
        private System.Nullable<int> _Client_Session_Duration;
        private System.Nullable<int> _Client_Counseling_Type;
        private System.Nullable<decimal> _Client_Counseling_Fee;
        private System.Nullable<int> _Client_Attribute_HUD_Grant;
        private System.Nullable<int> _Client_Finance_Type_Before;
        private System.Nullable<int> _Client_Finance_Type_After;
        private System.Nullable<int> _Client_Mortgage_Type;
        private System.Nullable<int> _Client_Mortgage_Type_After;
        private System.Nullable<int> _Client_Referred_By;
        private System.Nullable<int> _Client_Job_Duration;
        private System.Nullable<int> _Client_Household_Debt;
        private string _Client_Loan_Being_Reported;
        private System.Nullable<int> _Client_Intake_Loan_Type;
        private System.Nullable<int> _Client_Family_Size;
        private System.Nullable<int> _Client_Marital_Status;
        private System.Nullable<int> _Client_Race_ID;
        private System.Nullable<int> _Client_Ethnicity_ID;
        private System.Nullable<decimal> _Client_Household_Gross_Monthly_Income;
        private string _Client_Gender;
        private string _Client_Spouse_SSN;
        private string _Client_SSN1;
        private string _Client_SSN2;
        private string _Client_Mobile_Phone_Num;
        private string _Client_Phone_Num;
        private string _Client_Fax;
        private string _Client_Email;
        private System.Nullable<int> _language;
        private System.Nullable<int> _office;
        private System.Nullable<int> _hcs_id;
        private System.Nullable<int> _HUD_Grant;
        private System.Nullable<int> _group_session_id;
        private System.Nullable<int> _person_1;
        private System.Nullable<int> _person_2;
        private System.Nullable<int> _hud_interview;
        private System.Nullable<int> _interview_type;
        private System.Nullable<int> _hud_result;
        private System.Nullable<System.DateTime> _interview_date;
        private System.Nullable<System.DateTime> _result_date;
        private System.Nullable<System.DateTime> _termination_date;
        private string _interview_result;
        private System.Nullable<decimal> _ami;
        private System.Nullable<int> _client_appointment;
        private System.Nullable<int> _housing_property;
        private System.Nullable<int> _primary_loan;
        private System.Nullable<int> _secondary_loan;
        private int _is_client;
        private int _is_workshop;

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_is_hud_client", DbType = "Int NOT NULL")]
        public int is_hud_client
        {
            get
            {
                return this._is_hud_client;
            }
            set
            {
                if ((this._is_hud_client != value))
                {
                    this._is_hud_client = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Ethnicity_Clients_Counseling_Hispanic", DbType = "Int")]
        public System.Nullable<int> Ethnicity_Clients_Counseling_Hispanic
        {
            get
            {
                return this._Ethnicity_Clients_Counseling_Hispanic;
            }
            set
            {
                if ((this._Ethnicity_Clients_Counseling_Hispanic != value))
                {
                    this._Ethnicity_Clients_Counseling_Hispanic = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Ethnicity_Clients_Counseling_No_Response", DbType = "Int")]
        public System.Nullable<int> Ethnicity_Clients_Counseling_No_Response
        {
            get
            {
                return this._Ethnicity_Clients_Counseling_No_Response;
            }
            set
            {
                if ((this._Ethnicity_Clients_Counseling_No_Response != value))
                {
                    this._Ethnicity_Clients_Counseling_No_Response = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Ethnicity_Clients_Counseling_Non_Hispanic", DbType = "Int")]
        public System.Nullable<int> Ethnicity_Clients_Counseling_Non_Hispanic
        {
            get
            {
                return this._Ethnicity_Clients_Counseling_Non_Hispanic;
            }
            set
            {
                if ((this._Ethnicity_Clients_Counseling_Non_Hispanic != value))
                {
                    this._Ethnicity_Clients_Counseling_Non_Hispanic = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Household_Lives_In_Rural_Area", DbType = "Int")]
        public System.Nullable<int> Household_Lives_In_Rural_Area
        {
            get
            {
                return this._Household_Lives_In_Rural_Area;
            }
            set
            {
                if ((this._Household_Lives_In_Rural_Area != value))
                {
                    this._Household_Lives_In_Rural_Area = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Rural_Area_No_Response", DbType = "Int")]
        public System.Nullable<int> Rural_Area_No_Response
        {
            get
            {
                return this._Rural_Area_No_Response;
            }
            set
            {
                if ((this._Rural_Area_No_Response != value))
                {
                    this._Rural_Area_No_Response = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Household_Does_Not_Live_In_Rural_Area", DbType = "Int")]
        public System.Nullable<int> Household_Does_Not_Live_In_Rural_Area
        {
            get
            {
                return this._Household_Does_Not_Live_In_Rural_Area;
            }
            set
            {
                if ((this._Household_Does_Not_Live_In_Rural_Area != value))
                {
                    this._Household_Does_Not_Live_In_Rural_Area = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Household_Is_Limited_English_Proficient", DbType = "Int")]
        public System.Nullable<int> Household_Is_Limited_English_Proficient
        {
            get
            {
                return this._Household_Is_Limited_English_Proficient;
            }
            set
            {
                if ((this._Household_Is_Limited_English_Proficient != value))
                {
                    this._Household_Is_Limited_English_Proficient = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Limited_English_Proficient_No_Response", DbType = "Int")]
        public System.Nullable<int> Limited_English_Proficient_No_Response
        {
            get
            {
                return this._Limited_English_Proficient_No_Response;
            }
            set
            {
                if ((this._Limited_English_Proficient_No_Response != value))
                {
                    this._Limited_English_Proficient_No_Response = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Household_Is_Not_Limited_English_Proficient", DbType = "Int")]
        public System.Nullable<int> Household_Is_Not_Limited_English_Proficient
        {
            get
            {
                return this._Household_Is_Not_Limited_English_Proficient;
            }
            set
            {
                if ((this._Household_Is_Not_Limited_English_Proficient != value))
                {
                    this._Household_Is_Not_Limited_English_Proficient = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Less30_AMI_Level", DbType = "Int")]
        public System.Nullable<int> Less30_AMI_Level
        {
            get
            {
                return this._Less30_AMI_Level;
            }
            set
            {
                if ((this._Less30_AMI_Level != value))
                {
                    this._Less30_AMI_Level = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_a30_49_AMI_Level", DbType = "Int")]
        public System.Nullable<int> a30_49_AMI_Level
        {
            get
            {
                return this._a30_49_AMI_Level;
            }
            set
            {
                if ((this._a30_49_AMI_Level != value))
                {
                    this._a30_49_AMI_Level = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_a50_79_AMI_Level", DbType = "Int")]
        public System.Nullable<int> a50_79_AMI_Level
        {
            get
            {
                return this._a50_79_AMI_Level;
            }
            set
            {
                if ((this._a50_79_AMI_Level != value))
                {
                    this._a50_79_AMI_Level = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_a80_100_AMI_Level", DbType = "Int")]
        public System.Nullable<int> a80_100_AMI_Level
        {
            get
            {
                return this._a80_100_AMI_Level;
            }
            set
            {
                if ((this._a80_100_AMI_Level != value))
                {
                    this._a80_100_AMI_Level = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Greater100_AMI_Level", DbType = "Int")]
        public System.Nullable<int> Greater100_AMI_Level
        {
            get
            {
                return this._Greater100_AMI_Level;
            }
            set
            {
                if ((this._Greater100_AMI_Level != value))
                {
                    this._Greater100_AMI_Level = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_AMI_No_Response", DbType = "Int")]
        public System.Nullable<int> AMI_No_Response
        {
            get
            {
                return this._AMI_No_Response;
            }
            set
            {
                if ((this._AMI_No_Response != value))
                {
                    this._AMI_No_Response = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_MultiRace_Clients_Counseling_AMINDWHT", DbType = "Int")]
        public System.Nullable<int> MultiRace_Clients_Counseling_AMINDWHT
        {
            get
            {
                return this._MultiRace_Clients_Counseling_AMINDWHT;
            }
            set
            {
                if ((this._MultiRace_Clients_Counseling_AMINDWHT != value))
                {
                    this._MultiRace_Clients_Counseling_AMINDWHT = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_MultiRace_Clients_Counseling_AMRCINDBLK", DbType = "Int")]
        public System.Nullable<int> MultiRace_Clients_Counseling_AMRCINDBLK
        {
            get
            {
                return this._MultiRace_Clients_Counseling_AMRCINDBLK;
            }
            set
            {
                if ((this._MultiRace_Clients_Counseling_AMRCINDBLK != value))
                {
                    this._MultiRace_Clients_Counseling_AMRCINDBLK = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_MultiRace_Clients_Counseling_ASIANWHT", DbType = "Int")]
        public System.Nullable<int> MultiRace_Clients_Counseling_ASIANWHT
        {
            get
            {
                return this._MultiRace_Clients_Counseling_ASIANWHT;
            }
            set
            {
                if ((this._MultiRace_Clients_Counseling_ASIANWHT != value))
                {
                    this._MultiRace_Clients_Counseling_ASIANWHT = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_MultiRace_Clients_Counseling_BLKWHT", DbType = "Int")]
        public System.Nullable<int> MultiRace_Clients_Counseling_BLKWHT
        {
            get
            {
                return this._MultiRace_Clients_Counseling_BLKWHT;
            }
            set
            {
                if ((this._MultiRace_Clients_Counseling_BLKWHT != value))
                {
                    this._MultiRace_Clients_Counseling_BLKWHT = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_MultiRace_Clients_Counseling_NoResponse", DbType = "Int")]
        public System.Nullable<int> MultiRace_Clients_Counseling_NoResponse
        {
            get
            {
                return this._MultiRace_Clients_Counseling_NoResponse;
            }
            set
            {
                if ((this._MultiRace_Clients_Counseling_NoResponse != value))
                {
                    this._MultiRace_Clients_Counseling_NoResponse = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_MultiRace_Clients_Counseling_OtherMLTRC", DbType = "Int")]
        public System.Nullable<int> MultiRace_Clients_Counseling_OtherMLTRC
        {
            get
            {
                return this._MultiRace_Clients_Counseling_OtherMLTRC;
            }
            set
            {
                if ((this._MultiRace_Clients_Counseling_OtherMLTRC != value))
                {
                    this._MultiRace_Clients_Counseling_OtherMLTRC = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Race_Clients_Counseling_American_Indian_Alaskan_Native", DbType = "Int")]
        public System.Nullable<int> Race_Clients_Counseling_American_Indian_Alaskan_Native
        {
            get
            {
                return this._Race_Clients_Counseling_American_Indian_Alaskan_Native;
            }
            set
            {
                if ((this._Race_Clients_Counseling_American_Indian_Alaskan_Native != value))
                {
                    this._Race_Clients_Counseling_American_Indian_Alaskan_Native = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Race_Clients_Counseling_Asian", DbType = "Int")]
        public System.Nullable<int> Race_Clients_Counseling_Asian
        {
            get
            {
                return this._Race_Clients_Counseling_Asian;
            }
            set
            {
                if ((this._Race_Clients_Counseling_Asian != value))
                {
                    this._Race_Clients_Counseling_Asian = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Race_Clients_Counseling_Black_AfricanAmerican", DbType = "Int")]
        public System.Nullable<int> Race_Clients_Counseling_Black_AfricanAmerican
        {
            get
            {
                return this._Race_Clients_Counseling_Black_AfricanAmerican;
            }
            set
            {
                if ((this._Race_Clients_Counseling_Black_AfricanAmerican != value))
                {
                    this._Race_Clients_Counseling_Black_AfricanAmerican = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Race_Clients_Counseling_Pacific_Islanders", DbType = "Int")]
        public System.Nullable<int> Race_Clients_Counseling_Pacific_Islanders
        {
            get
            {
                return this._Race_Clients_Counseling_Pacific_Islanders;
            }
            set
            {
                if ((this._Race_Clients_Counseling_Pacific_Islanders != value))
                {
                    this._Race_Clients_Counseling_Pacific_Islanders = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Race_Clients_Counseling_White", DbType = "Int")]
        public System.Nullable<int> Race_Clients_Counseling_White
        {
            get
            {
                return this._Race_Clients_Counseling_White;
            }
            set
            {
                if ((this._Race_Clients_Counseling_White != value))
                {
                    this._Race_Clients_Counseling_White = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Compl_HomeMaint_FinMngt", DbType = "Int")]
        public System.Nullable<int> Compl_HomeMaint_FinMngt
        {
            get
            {
                return this._Compl_HomeMaint_FinMngt;
            }
            set
            {
                if ((this._Compl_HomeMaint_FinMngt != value))
                {
                    this._Compl_HomeMaint_FinMngt = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Compl_Workshop_Predatory_Lend", DbType = "Int")]
        public System.Nullable<int> Compl_Workshop_Predatory_Lend
        {
            get
            {
                return this._Compl_Workshop_Predatory_Lend;
            }
            set
            {
                if ((this._Compl_Workshop_Predatory_Lend != value))
                {
                    this._Compl_Workshop_Predatory_Lend = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Compl_Help_FairHousing_Workshop", DbType = "Int")]
        public System.Nullable<int> Compl_Help_FairHousing_Workshop
        {
            get
            {
                return this._Compl_Help_FairHousing_Workshop;
            }
            set
            {
                if ((this._Compl_Help_FairHousing_Workshop != value))
                {
                    this._Compl_Help_FairHousing_Workshop = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Compl_Resolv_Prevent_Mortg_Deliq", DbType = "Int")]
        public System.Nullable<int> Compl_Resolv_Prevent_Mortg_Deliq
        {
            get
            {
                return this._Compl_Resolv_Prevent_Mortg_Deliq;
            }
            set
            {
                if ((this._Compl_Resolv_Prevent_Mortg_Deliq != value))
                {
                    this._Compl_Resolv_Prevent_Mortg_Deliq = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Counseling_Rental_Workshop", DbType = "Int")]
        public System.Nullable<int> Counseling_Rental_Workshop
        {
            get
            {
                return this._Counseling_Rental_Workshop;
            }
            set
            {
                if ((this._Counseling_Rental_Workshop != value))
                {
                    this._Counseling_Rental_Workshop = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Compl_HomeBuyer_Educ_Workshop", DbType = "Int")]
        public System.Nullable<int> Compl_HomeBuyer_Educ_Workshop
        {
            get
            {
                return this._Compl_HomeBuyer_Educ_Workshop;
            }
            set
            {
                if ((this._Compl_HomeBuyer_Educ_Workshop != value))
                {
                    this._Compl_HomeBuyer_Educ_Workshop = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Compl_NonDelinqency_PostPurchase_Workshop", DbType = "Int")]
        public System.Nullable<int> Compl_NonDelinqency_PostPurchase_Workshop
        {
            get
            {
                return this._Compl_NonDelinqency_PostPurchase_Workshop;
            }
            set
            {
                if ((this._Compl_NonDelinqency_PostPurchase_Workshop != value))
                {
                    this._Compl_NonDelinqency_PostPurchase_Workshop = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Compl_Other_Workshop", DbType = "Int")]
        public System.Nullable<int> Compl_Other_Workshop
        {
            get
            {
                return this._Compl_Other_Workshop;
            }
            set
            {
                if ((this._Compl_Other_Workshop != value))
                {
                    this._Compl_Other_Workshop = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_One_Homeless_Assistance_Counseling", DbType = "Int")]
        public System.Nullable<int> One_Homeless_Assistance_Counseling
        {
            get
            {
                return this._One_Homeless_Assistance_Counseling;
            }
            set
            {
                if ((this._One_Homeless_Assistance_Counseling != value))
                {
                    this._One_Homeless_Assistance_Counseling = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_One_Rental_Topics_Counseling", DbType = "Int")]
        public System.Nullable<int> One_Rental_Topics_Counseling
        {
            get
            {
                return this._One_Rental_Topics_Counseling;
            }
            set
            {
                if ((this._One_Rental_Topics_Counseling != value))
                {
                    this._One_Rental_Topics_Counseling = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_One_PrePurchase_HomeBuying_Counseling", DbType = "Int")]
        public System.Nullable<int> One_PrePurchase_HomeBuying_Counseling
        {
            get
            {
                return this._One_PrePurchase_HomeBuying_Counseling;
            }
            set
            {
                if ((this._One_PrePurchase_HomeBuying_Counseling != value))
                {
                    this._One_PrePurchase_HomeBuying_Counseling = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_One_Home_Maintenance_Fin_Management_Counseling", DbType = "Int")]
        public System.Nullable<int> One_Home_Maintenance_Fin_Management_Counseling
        {
            get
            {
                return this._One_Home_Maintenance_Fin_Management_Counseling;
            }
            set
            {
                if ((this._One_Home_Maintenance_Fin_Management_Counseling != value))
                {
                    this._One_Home_Maintenance_Fin_Management_Counseling = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_One_Reverse_Mortgage_Counseling", DbType = "Int")]
        public System.Nullable<int> One_Reverse_Mortgage_Counseling
        {
            get
            {
                return this._One_Reverse_Mortgage_Counseling;
            }
            set
            {
                if ((this._One_Reverse_Mortgage_Counseling != value))
                {
                    this._One_Reverse_Mortgage_Counseling = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_One_Resolv_Prevent_Mortg_Delinq_Counseling", DbType = "Int")]
        public System.Nullable<int> One_Resolv_Prevent_Mortg_Delinq_Counseling
        {
            get
            {
                return this._One_Resolv_Prevent_Mortg_Delinq_Counseling;
            }
            set
            {
                if ((this._One_Resolv_Prevent_Mortg_Delinq_Counseling != value))
                {
                    this._One_Resolv_Prevent_Mortg_Delinq_Counseling = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Impact_One_On_One_And_Group", DbType = "Int")]
        public System.Nullable<int> Impact_One_On_One_And_Group
        {
            get
            {
                return this._Impact_One_On_One_And_Group;
            }
            set
            {
                if ((this._Impact_One_On_One_And_Group != value))
                {
                    this._Impact_One_On_One_And_Group = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Impact_Received_Info_Fair_Housing", DbType = "Int")]
        public System.Nullable<int> Impact_Received_Info_Fair_Housing
        {
            get
            {
                return this._Impact_Received_Info_Fair_Housing;
            }
            set
            {
                if ((this._Impact_Received_Info_Fair_Housing != value))
                {
                    this._Impact_Received_Info_Fair_Housing = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Impact_Developed_Sustainable_Budget", DbType = "Int")]
        public System.Nullable<int> Impact_Developed_Sustainable_Budget
        {
            get
            {
                return this._Impact_Developed_Sustainable_Budget;
            }
            set
            {
                if ((this._Impact_Developed_Sustainable_Budget != value))
                {
                    this._Impact_Developed_Sustainable_Budget = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Impact_Improved_Financial_Capacity", DbType = "Int")]
        public System.Nullable<int> Impact_Improved_Financial_Capacity
        {
            get
            {
                return this._Impact_Improved_Financial_Capacity;
            }
            set
            {
                if ((this._Impact_Improved_Financial_Capacity != value))
                {
                    this._Impact_Improved_Financial_Capacity = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Impact_Gained_Access_Resources_Improve_Housing", DbType = "Int")]
        public System.Nullable<int> Impact_Gained_Access_Resources_Improve_Housing
        {
            get
            {
                return this._Impact_Gained_Access_Resources_Improve_Housing;
            }
            set
            {
                if ((this._Impact_Gained_Access_Resources_Improve_Housing != value))
                {
                    this._Impact_Gained_Access_Resources_Improve_Housing = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Impact_Gained_Access_NonHousing_Resources", DbType = "Int")]
        public System.Nullable<int> Impact_Gained_Access_NonHousing_Resources
        {
            get
            {
                return this._Impact_Gained_Access_NonHousing_Resources;
            }
            set
            {
                if ((this._Impact_Gained_Access_NonHousing_Resources != value))
                {
                    this._Impact_Gained_Access_NonHousing_Resources = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Impact_Homeless_Obtained_Housing", DbType = "Int")]
        public System.Nullable<int> Impact_Homeless_Obtained_Housing
        {
            get
            {
                return this._Impact_Homeless_Obtained_Housing;
            }
            set
            {
                if ((this._Impact_Homeless_Obtained_Housing != value))
                {
                    this._Impact_Homeless_Obtained_Housing = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Impact_Received_Rental_Counseling_Avoided_Eviction", DbType = "Int")]
        public System.Nullable<int> Impact_Received_Rental_Counseling_Avoided_Eviction
        {
            get
            {
                return this._Impact_Received_Rental_Counseling_Avoided_Eviction;
            }
            set
            {
                if ((this._Impact_Received_Rental_Counseling_Avoided_Eviction != value))
                {
                    this._Impact_Received_Rental_Counseling_Avoided_Eviction = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Impact_Received_Rental_Counseling_Improved_Living_Conditions", DbType = "Int")]
        public System.Nullable<int> Impact_Received_Rental_Counseling_Improved_Living_Conditions
        {
            get
            {
                return this._Impact_Received_Rental_Counseling_Improved_Living_Conditions;
            }
            set
            {
                if ((this._Impact_Received_Rental_Counseling_Improved_Living_Conditions != value))
                {
                    this._Impact_Received_Rental_Counseling_Improved_Living_Conditions = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Impact_Received_PrePurchase_Counseling_Purchased_Housing", DbType = "Int")]
        public System.Nullable<int> Impact_Received_PrePurchase_Counseling_Purchased_Housing
        {
            get
            {
                return this._Impact_Received_PrePurchase_Counseling_Purchased_Housing;
            }
            set
            {
                if ((this._Impact_Received_PrePurchase_Counseling_Purchased_Housing != value))
                {
                    this._Impact_Received_PrePurchase_Counseling_Purchased_Housing = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Impact_Received_Reverse_Mortgage_Counseling_Obtained_HECM", DbType = "Int")]
        public System.Nullable<int> Impact_Received_Reverse_Mortgage_Counseling_Obtained_HECM
        {
            get
            {
                return this._Impact_Received_Reverse_Mortgage_Counseling_Obtained_HECM;
            }
            set
            {
                if ((this._Impact_Received_Reverse_Mortgage_Counseling_Obtained_HECM != value))
                {
                    this._Impact_Received_Reverse_Mortgage_Counseling_Obtained_HECM = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Impact_Received_NonDelinquency_PostPurchase_Counseling_Improve_Conditions_Afford" +
            "ability", DbType = "Int")]
        public System.Nullable<int> Impact_Received_NonDelinquency_PostPurchase_Counseling_Improve_Conditions_Affordability
        {
            get
            {
                return this._Impact_Received_NonDelinquency_PostPurchase_Counseling_Improve_Conditions_Affordability;
            }
            set
            {
                if ((this._Impact_Received_NonDelinquency_PostPurchase_Counseling_Improve_Conditions_Affordability != value))
                {
                    this._Impact_Received_NonDelinquency_PostPurchase_Counseling_Improve_Conditions_Affordability = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Impact_Prevented_Resolved_Mortgage_Default", DbType = "Int")]
        public System.Nullable<int> Impact_Prevented_Resolved_Mortgage_Default
        {
            get
            {
                return this._Impact_Prevented_Resolved_Mortgage_Default;
            }
            set
            {
                if ((this._Impact_Prevented_Resolved_Mortgage_Default != value))
                {
                    this._Impact_Prevented_Resolved_Mortgage_Default = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_ID_Num", DbType = "Int")]
        public System.Nullable<int> Client_ID_Num
        {
            get
            {
                return this._Client_ID_Num;
            }
            set
            {
                if ((this._Client_ID_Num != value))
                {
                    this._Client_ID_Num = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Purpose_Of_Visit", DbType = "Int")]
        public System.Nullable<int> Client_Purpose_Of_Visit
        {
            get
            {
                return this._Client_Purpose_Of_Visit;
            }
            set
            {
                if ((this._Client_Purpose_Of_Visit != value))
                {
                    this._Client_Purpose_Of_Visit = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Outcome_Of_Visit", DbType = "Int")]
        public System.Nullable<int> Client_Outcome_Of_Visit
        {
            get
            {
                return this._Client_Outcome_Of_Visit;
            }
            set
            {
                if ((this._Client_Outcome_Of_Visit != value))
                {
                    this._Client_Outcome_Of_Visit = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Activity_Type", DbType = "Int")]
        public System.Nullable<int> Client_Activity_Type
        {
            get
            {
                return this._Client_Activity_Type;
            }
            set
            {
                if ((this._Client_Activity_Type != value))
                {
                    this._Client_Activity_Type = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Case_Num", DbType = "Int")]
        public System.Nullable<int> Client_Case_Num
        {
            get
            {
                return this._Client_Case_Num;
            }
            set
            {
                if ((this._Client_Case_Num != value))
                {
                    this._Client_Case_Num = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Counselor_ID", DbType = "Int")]
        public System.Nullable<int> Client_Counselor_ID
        {
            get
            {
                return this._Client_Counselor_ID;
            }
            set
            {
                if ((this._Client_Counselor_ID != value))
                {
                    this._Client_Counselor_ID = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Head_Of_Household_Type", DbType = "Int")]
        public System.Nullable<int> Client_Head_Of_Household_Type
        {
            get
            {
                return this._Client_Head_Of_Household_Type;
            }
            set
            {
                if ((this._Client_Head_Of_Household_Type != value))
                {
                    this._Client_Head_Of_Household_Type = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Credit_Score", DbType = "Int")]
        public System.Nullable<int> Client_Credit_Score
        {
            get
            {
                return this._Client_Credit_Score;
            }
            set
            {
                if ((this._Client_Credit_Score != value))
                {
                    this._Client_Credit_Score = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Credit_Score_Source", DbType = "Int")]
        public System.Nullable<int> Client_Credit_Score_Source
        {
            get
            {
                return this._Client_Credit_Score_Source;
            }
            set
            {
                if ((this._Client_Credit_Score_Source != value))
                {
                    this._Client_Credit_Score_Source = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_No_Credit_Score_Reason", DbType = "Int")]
        public System.Nullable<int> Client_No_Credit_Score_Reason
        {
            get
            {
                return this._Client_No_Credit_Score_Reason;
            }
            set
            {
                if ((this._Client_No_Credit_Score_Reason != value))
                {
                    this._Client_No_Credit_Score_Reason = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_First_Name", DbType = "VarChar(80)")]
        public string Client_First_Name
        {
            get
            {
                return this._Client_First_Name;
            }
            set
            {
                if ((this._Client_First_Name != value))
                {
                    this._Client_First_Name = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Last_Name", DbType = "VarChar(80)")]
        public string Client_Last_Name
        {
            get
            {
                return this._Client_Last_Name;
            }
            set
            {
                if ((this._Client_Last_Name != value))
                {
                    this._Client_Last_Name = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Middle_Name", DbType = "VarChar(80)")]
        public string Client_Middle_Name
        {
            get
            {
                return this._Client_Middle_Name;
            }
            set
            {
                if ((this._Client_Middle_Name != value))
                {
                    this._Client_Middle_Name = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Street_Address_1", DbType = "VarChar(256)")]
        public string Client_Street_Address_1
        {
            get
            {
                return this._Client_Street_Address_1;
            }
            set
            {
                if ((this._Client_Street_Address_1 != value))
                {
                    this._Client_Street_Address_1 = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Street_Address_2", DbType = "VarChar(256)")]
        public string Client_Street_Address_2
        {
            get
            {
                return this._Client_Street_Address_2;
            }
            set
            {
                if ((this._Client_Street_Address_2 != value))
                {
                    this._Client_Street_Address_2 = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_City", DbType = "VarChar(256)")]
        public string Client_City
        {
            get
            {
                return this._Client_City;
            }
            set
            {
                if ((this._Client_City != value))
                {
                    this._Client_City = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_State", DbType = "Int")]
        public System.Nullable<int> Client_State
        {
            get
            {
                return this._Client_State;
            }
            set
            {
                if ((this._Client_State != value))
                {
                    this._Client_State = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_ZipCode", DbType = "VarChar(256)")]
        public string Client_ZipCode
        {
            get
            {
                return this._Client_ZipCode;
            }
            set
            {
                if ((this._Client_ZipCode != value))
                {
                    this._Client_ZipCode = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_New_Street_Address_1", DbType = "VarChar(256)")]
        public string Client_New_Street_Address_1
        {
            get
            {
                return this._Client_New_Street_Address_1;
            }
            set
            {
                if ((this._Client_New_Street_Address_1 != value))
                {
                    this._Client_New_Street_Address_1 = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_New_Street_Address_2", DbType = "VarChar(256)")]
        public string Client_New_Street_Address_2
        {
            get
            {
                return this._Client_New_Street_Address_2;
            }
            set
            {
                if ((this._Client_New_Street_Address_2 != value))
                {
                    this._Client_New_Street_Address_2 = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_New_City", DbType = "VarChar(256)")]
        public string Client_New_City
        {
            get
            {
                return this._Client_New_City;
            }
            set
            {
                if ((this._Client_New_City != value))
                {
                    this._Client_New_City = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_New_State", DbType = "Int")]
        public System.Nullable<int> Client_New_State
        {
            get
            {
                return this._Client_New_State;
            }
            set
            {
                if ((this._Client_New_State != value))
                {
                    this._Client_New_State = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_New_ZipCode", DbType = "VarChar(256)")]
        public string Client_New_ZipCode
        {
            get
            {
                return this._Client_New_ZipCode;
            }
            set
            {
                if ((this._Client_New_ZipCode != value))
                {
                    this._Client_New_ZipCode = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Spouse_First_Name", DbType = "VarChar(80)")]
        public string Client_Spouse_First_Name
        {
            get
            {
                return this._Client_Spouse_First_Name;
            }
            set
            {
                if ((this._Client_Spouse_First_Name != value))
                {
                    this._Client_Spouse_First_Name = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Spouse_Last_Name", DbType = "VarChar(80)")]
        public string Client_Spouse_Last_Name
        {
            get
            {
                return this._Client_Spouse_Last_Name;
            }
            set
            {
                if ((this._Client_Spouse_Last_Name != value))
                {
                    this._Client_Spouse_Last_Name = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Spouse_Middle_Name", DbType = "VarChar(80)")]
        public string Client_Spouse_Middle_Name
        {
            get
            {
                return this._Client_Spouse_Middle_Name;
            }
            set
            {
                if ((this._Client_Spouse_Middle_Name != value))
                {
                    this._Client_Spouse_Middle_Name = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Farm_Worker", DbType = "Bit")]
        public System.Nullable<bool> Client_Farm_Worker
        {
            get
            {
                return this._Client_Farm_Worker;
            }
            set
            {
                if ((this._Client_Farm_Worker != value))
                {
                    this._Client_Farm_Worker = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Colonias_Resident", DbType = "Bit")]
        public System.Nullable<bool> Client_Colonias_Resident
        {
            get
            {
                return this._Client_Colonias_Resident;
            }
            set
            {
                if ((this._Client_Colonias_Resident != value))
                {
                    this._Client_Colonias_Resident = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Disabled", DbType = "Bit")]
        public System.Nullable<bool> Client_Disabled
        {
            get
            {
                return this._Client_Disabled;
            }
            set
            {
                if ((this._Client_Disabled != value))
                {
                    this._Client_Disabled = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_HECM_Certificate", DbType = "Bit")]
        public System.Nullable<bool> Client_HECM_Certificate
        {
            get
            {
                return this._Client_HECM_Certificate;
            }
            set
            {
                if ((this._Client_HECM_Certificate != value))
                {
                    this._Client_HECM_Certificate = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Predatory_Lending", DbType = "Bit")]
        public System.Nullable<bool> Client_Predatory_Lending
        {
            get
            {
                return this._Client_Predatory_Lending;
            }
            set
            {
                if ((this._Client_Predatory_Lending != value))
                {
                    this._Client_Predatory_Lending = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_FirstTime_Home_Buyer", DbType = "Bit")]
        public System.Nullable<bool> Client_FirstTime_Home_Buyer
        {
            get
            {
                return this._Client_FirstTime_Home_Buyer;
            }
            set
            {
                if ((this._Client_FirstTime_Home_Buyer != value))
                {
                    this._Client_FirstTime_Home_Buyer = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Discrimination_Victim", DbType = "Bit")]
        public System.Nullable<bool> Client_Discrimination_Victim
        {
            get
            {
                return this._Client_Discrimination_Victim;
            }
            set
            {
                if ((this._Client_Discrimination_Victim != value))
                {
                    this._Client_Discrimination_Victim = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Mortgage_Deliquency", DbType = "Bit")]
        public System.Nullable<bool> Client_Mortgage_Deliquency
        {
            get
            {
                return this._Client_Mortgage_Deliquency;
            }
            set
            {
                if ((this._Client_Mortgage_Deliquency != value))
                {
                    this._Client_Mortgage_Deliquency = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Second_Loan_Exists", DbType = "Bit")]
        public System.Nullable<bool> Client_Second_Loan_Exists
        {
            get
            {
                return this._Client_Second_Loan_Exists;
            }
            set
            {
                if ((this._Client_Second_Loan_Exists != value))
                {
                    this._Client_Second_Loan_Exists = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Intake_Loan_Type_Is_Hybrid_ARM", DbType = "Bit")]
        public System.Nullable<bool> Client_Intake_Loan_Type_Is_Hybrid_ARM
        {
            get
            {
                return this._Client_Intake_Loan_Type_Is_Hybrid_ARM;
            }
            set
            {
                if ((this._Client_Intake_Loan_Type_Is_Hybrid_ARM != value))
                {
                    this._Client_Intake_Loan_Type_Is_Hybrid_ARM = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Intake_Loan_Type_Is_Option_ARM", DbType = "Bit")]
        public System.Nullable<bool> Client_Intake_Loan_Type_Is_Option_ARM
        {
            get
            {
                return this._Client_Intake_Loan_Type_Is_Option_ARM;
            }
            set
            {
                if ((this._Client_Intake_Loan_Type_Is_Option_ARM != value))
                {
                    this._Client_Intake_Loan_Type_Is_Option_ARM = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Intake_Loan_Type_Is_Interest_Only", DbType = "Bit")]
        public System.Nullable<bool> Client_Intake_Loan_Type_Is_Interest_Only
        {
            get
            {
                return this._Client_Intake_Loan_Type_Is_Interest_Only;
            }
            set
            {
                if ((this._Client_Intake_Loan_Type_Is_Interest_Only != value))
                {
                    this._Client_Intake_Loan_Type_Is_Interest_Only = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Intake_Loan_Type_Is_FHA_Or_VA_Insured", DbType = "Bit")]
        public System.Nullable<bool> Client_Intake_Loan_Type_Is_FHA_Or_VA_Insured
        {
            get
            {
                return this._Client_Intake_Loan_Type_Is_FHA_Or_VA_Insured;
            }
            set
            {
                if ((this._Client_Intake_Loan_Type_Is_FHA_Or_VA_Insured != value))
                {
                    this._Client_Intake_Loan_Type_Is_FHA_Or_VA_Insured = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Intake_Loan_Type_Is_Privately_Held", DbType = "Bit")]
        public System.Nullable<bool> Client_Intake_Loan_Type_Is_Privately_Held
        {
            get
            {
                return this._Client_Intake_Loan_Type_Is_Privately_Held;
            }
            set
            {
                if ((this._Client_Intake_Loan_Type_Is_Privately_Held != value))
                {
                    this._Client_Intake_Loan_Type_Is_Privately_Held = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Intake_Loan_Type_Has_Interest_Rate_Reset", DbType = "Bit")]
        public System.Nullable<bool> Client_Intake_Loan_Type_Has_Interest_Rate_Reset
        {
            get
            {
                return this._Client_Intake_Loan_Type_Has_Interest_Rate_Reset;
            }
            set
            {
                if ((this._Client_Intake_Loan_Type_Has_Interest_Rate_Reset != value))
                {
                    this._Client_Intake_Loan_Type_Has_Interest_Rate_Reset = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Counsel_Session_DT_Start", DbType = "DateTime")]
        public System.Nullable<System.DateTime> Client_Counsel_Session_DT_Start
        {
            get
            {
                return this._Client_Counsel_Session_DT_Start;
            }
            set
            {
                if ((this._Client_Counsel_Session_DT_Start != value))
                {
                    this._Client_Counsel_Session_DT_Start = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Counsel_Session_DT_End", DbType = "DateTime")]
        public System.Nullable<System.DateTime> Client_Counsel_Session_DT_End
        {
            get
            {
                return this._Client_Counsel_Session_DT_End;
            }
            set
            {
                if ((this._Client_Counsel_Session_DT_End != value))
                {
                    this._Client_Counsel_Session_DT_End = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Intake_DT", DbType = "DateTime")]
        public System.Nullable<System.DateTime> Client_Intake_DT
        {
            get
            {
                return this._Client_Intake_DT;
            }
            set
            {
                if ((this._Client_Intake_DT != value))
                {
                    this._Client_Intake_DT = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Birth_DT", DbType = "DateTime")]
        public System.Nullable<System.DateTime> Client_Birth_DT
        {
            get
            {
                return this._Client_Birth_DT;
            }
            set
            {
                if ((this._Client_Birth_DT != value))
                {
                    this._Client_Birth_DT = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_HECM_Certificate_Issue_Date", DbType = "DateTime")]
        public System.Nullable<System.DateTime> Client_HECM_Certificate_Issue_Date
        {
            get
            {
                return this._Client_HECM_Certificate_Issue_Date;
            }
            set
            {
                if ((this._Client_HECM_Certificate_Issue_Date != value))
                {
                    this._Client_HECM_Certificate_Issue_Date = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_HECM_Certificate_Expiration_Date", DbType = "DateTime")]
        public System.Nullable<System.DateTime> Client_HECM_Certificate_Expiration_Date
        {
            get
            {
                return this._Client_HECM_Certificate_Expiration_Date;
            }
            set
            {
                if ((this._Client_HECM_Certificate_Expiration_Date != value))
                {
                    this._Client_HECM_Certificate_Expiration_Date = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_HECM_Certificate_ID", DbType = "VarChar(80)")]
        public string Client_HECM_Certificate_ID
        {
            get
            {
                return this._Client_HECM_Certificate_ID;
            }
            set
            {
                if ((this._Client_HECM_Certificate_ID != value))
                {
                    this._Client_HECM_Certificate_ID = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Sales_Contract_Signed", DbType = "DateTime")]
        public System.Nullable<System.DateTime> Client_Sales_Contract_Signed
        {
            get
            {
                return this._Client_Sales_Contract_Signed;
            }
            set
            {
                if ((this._Client_Sales_Contract_Signed != value))
                {
                    this._Client_Sales_Contract_Signed = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Grant_Amount_Used", DbType = "Money")]
        public System.Nullable<decimal> Client_Grant_Amount_Used
        {
            get
            {
                return this._Client_Grant_Amount_Used;
            }
            set
            {
                if ((this._Client_Grant_Amount_Used != value))
                {
                    this._Client_Grant_Amount_Used = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Mortgage_Closing_Cost", DbType = "Money")]
        public System.Nullable<decimal> Client_Mortgage_Closing_Cost
        {
            get
            {
                return this._Client_Mortgage_Closing_Cost;
            }
            set
            {
                if ((this._Client_Mortgage_Closing_Cost != value))
                {
                    this._Client_Mortgage_Closing_Cost = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Mortgage_Interest_Rate", DbType = "Float")]
        public System.Nullable<double> Client_Mortgage_Interest_Rate
        {
            get
            {
                return this._Client_Mortgage_Interest_Rate;
            }
            set
            {
                if ((this._Client_Mortgage_Interest_Rate != value))
                {
                    this._Client_Mortgage_Interest_Rate = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Counseling_Termination", DbType = "Int")]
        public System.Nullable<int> Client_Counseling_Termination
        {
            get
            {
                return this._Client_Counseling_Termination;
            }
            set
            {
                if ((this._Client_Counseling_Termination != value))
                {
                    this._Client_Counseling_Termination = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Income_Level", DbType = "Int")]
        public System.Nullable<int> Client_Income_Level
        {
            get
            {
                return this._Client_Income_Level;
            }
            set
            {
                if ((this._Client_Income_Level != value))
                {
                    this._Client_Income_Level = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Highest_Educ_Grade", DbType = "Int")]
        public System.Nullable<int> Client_Highest_Educ_Grade
        {
            get
            {
                return this._Client_Highest_Educ_Grade;
            }
            set
            {
                if ((this._Client_Highest_Educ_Grade != value))
                {
                    this._Client_Highest_Educ_Grade = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_HUD_Assistance", DbType = "Int")]
        public System.Nullable<int> Client_HUD_Assistance
        {
            get
            {
                return this._Client_HUD_Assistance;
            }
            set
            {
                if ((this._Client_HUD_Assistance != value))
                {
                    this._Client_HUD_Assistance = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Dependents_Num", DbType = "Int")]
        public System.Nullable<int> Client_Dependents_Num
        {
            get
            {
                return this._Client_Dependents_Num;
            }
            set
            {
                if ((this._Client_Dependents_Num != value))
                {
                    this._Client_Dependents_Num = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Language_Spoken", DbType = "Int")]
        public System.Nullable<int> Client_Language_Spoken
        {
            get
            {
                return this._Client_Language_Spoken;
            }
            set
            {
                if ((this._Client_Language_Spoken != value))
                {
                    this._Client_Language_Spoken = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Session_Duration", DbType = "Int")]
        public System.Nullable<int> Client_Session_Duration
        {
            get
            {
                return this._Client_Session_Duration;
            }
            set
            {
                if ((this._Client_Session_Duration != value))
                {
                    this._Client_Session_Duration = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Counseling_Type", DbType = "Int")]
        public System.Nullable<int> Client_Counseling_Type
        {
            get
            {
                return this._Client_Counseling_Type;
            }
            set
            {
                if ((this._Client_Counseling_Type != value))
                {
                    this._Client_Counseling_Type = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Counseling_Fee", DbType = "Money")]
        public System.Nullable<decimal> Client_Counseling_Fee
        {
            get
            {
                return this._Client_Counseling_Fee;
            }
            set
            {
                if ((this._Client_Counseling_Fee != value))
                {
                    this._Client_Counseling_Fee = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Attribute_HUD_Grant", DbType = "Int")]
        public System.Nullable<int> Client_Attribute_HUD_Grant
        {
            get
            {
                return this._Client_Attribute_HUD_Grant;
            }
            set
            {
                if ((this._Client_Attribute_HUD_Grant != value))
                {
                    this._Client_Attribute_HUD_Grant = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Finance_Type_Before", DbType = "Int")]
        public System.Nullable<int> Client_Finance_Type_Before
        {
            get
            {
                return this._Client_Finance_Type_Before;
            }
            set
            {
                if ((this._Client_Finance_Type_Before != value))
                {
                    this._Client_Finance_Type_Before = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Finance_Type_After", DbType = "Int")]
        public System.Nullable<int> Client_Finance_Type_After
        {
            get
            {
                return this._Client_Finance_Type_After;
            }
            set
            {
                if ((this._Client_Finance_Type_After != value))
                {
                    this._Client_Finance_Type_After = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Mortgage_Type", DbType = "Int")]
        public System.Nullable<int> Client_Mortgage_Type
        {
            get
            {
                return this._Client_Mortgage_Type;
            }
            set
            {
                if ((this._Client_Mortgage_Type != value))
                {
                    this._Client_Mortgage_Type = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Mortgage_Type_After", DbType = "Int")]
        public System.Nullable<int> Client_Mortgage_Type_After
        {
            get
            {
                return this._Client_Mortgage_Type_After;
            }
            set
            {
                if ((this._Client_Mortgage_Type_After != value))
                {
                    this._Client_Mortgage_Type_After = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Referred_By", DbType = "Int")]
        public System.Nullable<int> Client_Referred_By
        {
            get
            {
                return this._Client_Referred_By;
            }
            set
            {
                if ((this._Client_Referred_By != value))
                {
                    this._Client_Referred_By = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Job_Duration", DbType = "Int")]
        public System.Nullable<int> Client_Job_Duration
        {
            get
            {
                return this._Client_Job_Duration;
            }
            set
            {
                if ((this._Client_Job_Duration != value))
                {
                    this._Client_Job_Duration = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Household_Debt", DbType = "Int")]
        public System.Nullable<int> Client_Household_Debt
        {
            get
            {
                return this._Client_Household_Debt;
            }
            set
            {
                if ((this._Client_Household_Debt != value))
                {
                    this._Client_Household_Debt = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Loan_Being_Reported", DbType = "VarChar(1)")]
        public string Client_Loan_Being_Reported
        {
            get
            {
                return this._Client_Loan_Being_Reported;
            }
            set
            {
                if ((this._Client_Loan_Being_Reported != value))
                {
                    this._Client_Loan_Being_Reported = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Intake_Loan_Type", DbType = "Int")]
        public System.Nullable<int> Client_Intake_Loan_Type
        {
            get
            {
                return this._Client_Intake_Loan_Type;
            }
            set
            {
                if ((this._Client_Intake_Loan_Type != value))
                {
                    this._Client_Intake_Loan_Type = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Family_Size", DbType = "Int")]
        public System.Nullable<int> Client_Family_Size
        {
            get
            {
                return this._Client_Family_Size;
            }
            set
            {
                if ((this._Client_Family_Size != value))
                {
                    this._Client_Family_Size = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Marital_Status", DbType = "Int")]
        public System.Nullable<int> Client_Marital_Status
        {
            get
            {
                return this._Client_Marital_Status;
            }
            set
            {
                if ((this._Client_Marital_Status != value))
                {
                    this._Client_Marital_Status = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Race_ID", DbType = "Int")]
        public System.Nullable<int> Client_Race_ID
        {
            get
            {
                return this._Client_Race_ID;
            }
            set
            {
                if ((this._Client_Race_ID != value))
                {
                    this._Client_Race_ID = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Ethnicity_ID", DbType = "Int")]
        public System.Nullable<int> Client_Ethnicity_ID
        {
            get
            {
                return this._Client_Ethnicity_ID;
            }
            set
            {
                if ((this._Client_Ethnicity_ID != value))
                {
                    this._Client_Ethnicity_ID = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Household_Gross_Monthly_Income", DbType = "Money")]
        public System.Nullable<decimal> Client_Household_Gross_Monthly_Income
        {
            get
            {
                return this._Client_Household_Gross_Monthly_Income;
            }
            set
            {
                if ((this._Client_Household_Gross_Monthly_Income != value))
                {
                    this._Client_Household_Gross_Monthly_Income = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Gender", DbType = "VarChar(10)")]
        public string Client_Gender
        {
            get
            {
                return this._Client_Gender;
            }
            set
            {
                if ((this._Client_Gender != value))
                {
                    this._Client_Gender = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Spouse_SSN", DbType = "VarChar(20)")]
        public string Client_Spouse_SSN
        {
            get
            {
                return this._Client_Spouse_SSN;
            }
            set
            {
                if ((this._Client_Spouse_SSN != value))
                {
                    this._Client_Spouse_SSN = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_SSN1", DbType = "VarChar(20)")]
        public string Client_SSN1
        {
            get
            {
                return this._Client_SSN1;
            }
            set
            {
                if ((this._Client_SSN1 != value))
                {
                    this._Client_SSN1 = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_SSN2", DbType = "VarChar(20)")]
        public string Client_SSN2
        {
            get
            {
                return this._Client_SSN2;
            }
            set
            {
                if ((this._Client_SSN2 != value))
                {
                    this._Client_SSN2 = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Mobile_Phone_Num", DbType = "VarChar(80)")]
        public string Client_Mobile_Phone_Num
        {
            get
            {
                return this._Client_Mobile_Phone_Num;
            }
            set
            {
                if ((this._Client_Mobile_Phone_Num != value))
                {
                    this._Client_Mobile_Phone_Num = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Phone_Num", DbType = "VarChar(80)")]
        public string Client_Phone_Num
        {
            get
            {
                return this._Client_Phone_Num;
            }
            set
            {
                if ((this._Client_Phone_Num != value))
                {
                    this._Client_Phone_Num = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Fax", DbType = "VarChar(80)")]
        public string Client_Fax
        {
            get
            {
                return this._Client_Fax;
            }
            set
            {
                if ((this._Client_Fax != value))
                {
                    this._Client_Fax = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Client_Email", DbType = "VarChar(256)")]
        public string Client_Email
        {
            get
            {
                return this._Client_Email;
            }
            set
            {
                if ((this._Client_Email != value))
                {
                    this._Client_Email = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_language", DbType = "Int")]
        public System.Nullable<int> language
        {
            get
            {
                return this._language;
            }
            set
            {
                if ((this._language != value))
                {
                    this._language = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_office", DbType = "Int")]
        public System.Nullable<int> office
        {
            get
            {
                return this._office;
            }
            set
            {
                if ((this._office != value))
                {
                    this._office = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_hcs_id", DbType = "Int")]
        public System.Nullable<int> hcs_id
        {
            get
            {
                return this._hcs_id;
            }
            set
            {
                if ((this._hcs_id != value))
                {
                    this._hcs_id = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_HUD_Grant", DbType = "Int")]
        public System.Nullable<int> HUD_Grant
        {
            get
            {
                return this._HUD_Grant;
            }
            set
            {
                if ((this._HUD_Grant != value))
                {
                    this._HUD_Grant = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_group_session_id", DbType = "Int")]
        public System.Nullable<int> group_session_id
        {
            get
            {
                return this._group_session_id;
            }
            set
            {
                if ((this._group_session_id != value))
                {
                    this._group_session_id = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_person_1", DbType = "Int")]
        public System.Nullable<int> person_1
        {
            get
            {
                return this._person_1;
            }
            set
            {
                if ((this._person_1 != value))
                {
                    this._person_1 = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_person_2", DbType = "Int")]
        public System.Nullable<int> person_2
        {
            get
            {
                return this._person_2;
            }
            set
            {
                if ((this._person_2 != value))
                {
                    this._person_2 = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_hud_interview", DbType = "Int")]
        public System.Nullable<int> hud_interview
        {
            get
            {
                return this._hud_interview;
            }
            set
            {
                if ((this._hud_interview != value))
                {
                    this._hud_interview = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_interview_type", DbType = "Int")]
        public System.Nullable<int> interview_type
        {
            get
            {
                return this._interview_type;
            }
            set
            {
                if ((this._interview_type != value))
                {
                    this._interview_type = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_hud_result", DbType = "Int")]
        public System.Nullable<int> hud_result
        {
            get
            {
                return this._hud_result;
            }
            set
            {
                if ((this._hud_result != value))
                {
                    this._hud_result = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_interview_date", DbType = "DateTime")]
        public System.Nullable<System.DateTime> interview_date
        {
            get
            {
                return this._interview_date;
            }
            set
            {
                if ((this._interview_date != value))
                {
                    this._interview_date = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_result_date", DbType = "DateTime")]
        public System.Nullable<System.DateTime> result_date
        {
            get
            {
                return this._result_date;
            }
            set
            {
                if ((this._result_date != value))
                {
                    this._result_date = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_termination_date", DbType = "DateTime")]
        public System.Nullable<System.DateTime> termination_date
        {
            get
            {
                return this._termination_date;
            }
            set
            {
                if ((this._termination_date != value))
                {
                    this._termination_date = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_interview_result", DbType = "VarChar(80)")]
        public string interview_result
        {
            get
            {
                return this._interview_result;
            }
            set
            {
                if ((this._interview_result != value))
                {
                    this._interview_result = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_ami", DbType = "Money")]
        public System.Nullable<decimal> ami
        {
            get
            {
                return this._ami;
            }
            set
            {
                if ((this._ami != value))
                {
                    this._ami = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_client_appointment", DbType = "Int")]
        public System.Nullable<int> client_appointment
        {
            get
            {
                return this._client_appointment;
            }
            set
            {
                if ((this._client_appointment != value))
                {
                    this._client_appointment = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_housing_property", DbType = "Int")]
        public System.Nullable<int> housing_property
        {
            get
            {
                return this._housing_property;
            }
            set
            {
                if ((this._housing_property != value))
                {
                    this._housing_property = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_primary_loan", DbType = "Int")]
        public System.Nullable<int> primary_loan
        {
            get
            {
                return this._primary_loan;
            }
            set
            {
                if ((this._primary_loan != value))
                {
                    this._primary_loan = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_secondary_loan", DbType = "Int")]
        public System.Nullable<int> secondary_loan
        {
            get
            {
                return this._secondary_loan;
            }
            set
            {
                if ((this._secondary_loan != value))
                {
                    this._secondary_loan = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_is_client", DbType = "Int NOT NULL")]
        public int is_client
        {
            get
            {
                return this._is_client;
            }
            set
            {
                if ((this._is_client != value))
                {
                    this._is_client = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_is_workshop", DbType = "Int NOT NULL")]
        public int is_workshop
        {
            get
            {
                return this._is_workshop;
            }
            set
            {
                if ((this._is_workshop != value))
                {
                    this._is_workshop = value;
                }
            }
        }
    }

    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_housing_arm_v4_client_fetch")]
        private ISingleResult<xpr_housing_arm_v4_client> private_xpr_housing_arm_v4_client_fetch()
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())));
            return (ISingleResult<xpr_housing_arm_v4_client>)(result.ReturnValue);
        }

        public System.Collections.Generic.List<xpr_housing_arm_v4_client> xpr_housing_arm_v4_client_fetch()
        {
            ISingleResult<xpr_housing_arm_v4_client> result = private_xpr_housing_arm_v4_client_fetch();
            if (result != null)
            {
                return result.ToList();
            }
            return null;
        }
    }
}