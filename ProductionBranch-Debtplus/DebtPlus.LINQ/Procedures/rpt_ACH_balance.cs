﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.1008
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DebtPlus.LINQ
{
    using System;
    using System.Data.Linq;
    using System.Linq;
    using System.Reflection;

    public partial class rpt_ACH_balanceResult
    {
        private decimal _extra_amount;
        private int _client;
        private string _name;
        private int _status_group;
        private decimal _deposit_amount;
        private decimal _normal_amount;
        private string _reason;
        private decimal _difference;

        public rpt_ACH_balanceResult()
        {
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_extra_amount", DbType = "Money NOT NULL")]
        public decimal extra_amount
        {
            get
            {
                return this._extra_amount;
            }
            set
            {
                this._extra_amount = value;
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_client", DbType = "int")]
        public int client
        {
            get
            {
                return this._client;
            }
            set
            {
                this._client = value;
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_name", DbType = "VarChar(80) NULL")]
        public string name
        {
            get
            {
                return this._name;
            }
            set
            {
                this._name = value;
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_status_group", DbType = "int")]
        public int status_group
        {
            get
            {
                return this._status_group;
            }
            set
            {
                this._status_group = value;
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_deposit_amount", DbType = "Money NOT NULL")]
        public decimal deposit_amount
        {
            get
            {
                return this._deposit_amount;
            }
            set
            {
                this._deposit_amount = value;
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_normal_amount", DbType = "Money NOT NULL")]
        public decimal normal_amount
        {
            get
            {
                return this._normal_amount;
            }
            set
            {
                this._normal_amount = value;
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_reason", DbType = "Varchar(80) NULL", CanBeNull = true)]
        public string reason
        {
            get
            {
                return this._reason;
            }
            set
            {
                this._reason = value;
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_difference", DbType = "Money NOT NULL")]
        public decimal difference
        {
            get
            {
                return this._difference;
            }
            set
            {
                this._difference = value;
            }
        }
    }

    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.rpt_ACH_balance")]
        private ISingleResult<rpt_ACH_balanceResult> pvt_rpt_ACH_balance([global::System.Data.Linq.Mapping.ParameterAttribute(Name = "ACH_Pull_Date", DbType = "DateTime NOT NULL")] DateTime ACH_Pull_Date)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), ACH_Pull_Date);
            return ((ISingleResult<rpt_ACH_balanceResult>)(result.ReturnValue));
        }

        public System.Collections.Generic.List<DebtPlus.LINQ.rpt_ACH_balanceResult> rpt_ACH_balance(DateTime ACH_Pull_Date)
        {
            return pvt_rpt_ACH_balance(ACH_Pull_Date).ToList<DebtPlus.LINQ.rpt_ACH_balanceResult>();
        }
    }
}