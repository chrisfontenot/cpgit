﻿#pragma warning disable 1591

namespace DebtPlus.LINQ
{
    using System.Data.Linq;
    using System.Linq;
    using System.Reflection;

    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_ACH_Generate_File")]
        private ISingleResult<xpr_ACH_Generate_FileResult> private_xpr_ACH_Generate_File(System.Nullable<int> bankID)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), new object[] { bankID } );
            return ((ISingleResult<xpr_ACH_Generate_FileResult>)(result.ReturnValue));
        }

        public System.Collections.Generic.List<xpr_ACH_Generate_FileResult> xpr_ACH_Generate_File()
        {
            ISingleResult<xpr_ACH_Generate_FileResult> rslt = private_xpr_ACH_Generate_File(null);
            if (rslt != null)
            {
                return rslt.ToList();
            }
            return null;
        }

        public System.Collections.Generic.List<xpr_ACH_Generate_FileResult> xpr_ACH_Generate_File(System.Int32 bankID)
        {
            ISingleResult<xpr_ACH_Generate_FileResult> rslt = private_xpr_ACH_Generate_File(bankID);
            if (rslt != null)
            {
                return rslt.ToList();
            }
            return null;
        }
    }

    public partial class xpr_ACH_Generate_FileResult : IACH_Generate_File
    {
        private System.Int32 _bank;
        private int _ach_transaction;
        private int _ach_file;
        private string _transaction_code;
        private string _routing_number;
        private string _account_number;
        private decimal _amount;
        private int _client;
        private System.Nullable<int> _discretionary_data;
        private System.Nullable<int> _client_product;
        private string _trace_number;
        private string _client_name;

        public xpr_ACH_Generate_FileResult()
        {
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_ach_transaction", DbType = "Int NOT NULL")]
        public int ach_transaction
        {
            get
            {
                return this._ach_transaction;
            }
            set
            {
                if ((this._ach_transaction != value))
                {
                    this._ach_transaction = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_ach_file", DbType = "Int NOT NULL")]
        public int ach_file
        {
            get
            {
                return this._ach_file;
            }
            set
            {
                if ((this._ach_file != value))
                {
                    this._ach_file = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_transaction_code", DbType = "VarChar(2)")]
        public string transaction_code
        {
            get
            {
                return this._transaction_code;
            }
            set
            {
                if ((this._transaction_code != value))
                {
                    this._transaction_code = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_routing_number", DbType = "VarChar(9) NOT NULL", CanBeNull = false)]
        public string routing_number
        {
            get
            {
                return this._routing_number;
            }
            set
            {
                if ((this._routing_number != value))
                {
                    this._routing_number = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_account_number", DbType = "VarChar(17) NOT NULL", CanBeNull = false)]
        public string account_number
        {
            get
            {
                return this._account_number;
            }
            set
            {
                if ((this._account_number != value))
                {
                    this._account_number = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_amount", DbType = "Money NOT NULL")]
        public decimal amount
        {
            get
            {
                return this._amount;
            }
            set
            {
                if ((this._amount != value))
                {
                    this._amount = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_client", DbType = "Int NOT NULL")]
        public int client
        {
            get
            {
                return this._client;
            }
            set
            {
                if ((this._client != value))
                {
                    this._client = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_discretionary_data", DbType = "Int")]
        public System.Nullable<int> discretionary_data
        {
            get
            {
                return this._discretionary_data;
            }
            set
            {
                if ((this._discretionary_data != value))
                {
                    this._discretionary_data = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_client_product", DbType = "Int")]
        public System.Nullable<int> client_product
        {
            get
            {
                return this._client_product;
            }
            set
            {
                if ((this._client_product != value))
                {
                    this._client_product = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_bank", DbType = "Int NOT NULL")]
        public int bank
        {
            get
            {
                return this._bank;
            }
            set
            {
                if ((this._bank != value))
                {
                    this._bank = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_trace_number", DbType = "VarChar(50)")]
        public string trace_number
        {
            get
            {
                return this._trace_number;
            }
            set
            {
                if ((this._trace_number != value))
                {
                    this._trace_number = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_client_name", DbType = "VarChar(22)")]
        public string client_name
        {
            get
            {
                return this._client_name;
            }
            set
            {
                if ((this._client_name != value))
                {
                    this._client_name = value;
                }
            }
        }
    }
}