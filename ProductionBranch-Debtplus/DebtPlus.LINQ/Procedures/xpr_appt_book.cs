﻿#pragma warning disable 1591

namespace DebtPlus.LINQ
{
    using System.Data.Linq;
    using System.Reflection;

    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_appt_book")]
        private int pvt_xpr_appt_book([global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> client, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> appt_time, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> priority, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> appt_type, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> old_appointment, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> counselor, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> bankruptcy_class, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "VarChar(50) NULL")] string partner)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), new object[] { client, appt_time, priority, appt_type, old_appointment, counselor, bankruptcy_class, partner });
            return ((int)(result.ReturnValue));
        }

        /// <summary>
        /// Book the client appointment if possible.
        /// </summary>
        /// <param name="client">Client ID</param>
        /// <param name="appt_time">Appointment Time ID</param>
        /// <param name="priority">Priority status</param>
        /// <param name="appt_type">Appointment Type ID</param>
        /// <param name="old_appointment">Pointer to rescheduled appointment if used</param>
        /// <param name="counselor">Counselor ID</param>
        /// <param name="bankruptcy_class">Bankruptcy Class ID</param>
        /// <param name="partner">Partner code</param>
        /// <returns></returns>
        public int xpr_appt_book(System.Nullable<int> client, System.Nullable<int> appt_time, System.Nullable<int> priority, System.Nullable<int> appt_type, System.Nullable<int> old_appointment, System.Nullable<int> counselor, System.Nullable<int> bankruptcy_class, string partner)
        {
            return pvt_xpr_appt_book(client, appt_time, priority, appt_type, old_appointment, counselor, bankruptcy_class, partner);
        }
    }
}