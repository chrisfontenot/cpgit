﻿using System.Data.Linq;
using System.Linq;
using System.Reflection;

namespace DebtPlus.LINQ
{
    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_sales_debt_export")]
        private object private_xpr_sales_debt_export([global::System.Data.Linq.Mapping.ParameterAttribute(Name = "ClientCreditor", DbType = "Int")] System.Nullable<int> salesFileID)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), salesFileID);
            return result.ReturnValue;
        }

        public System.Nullable<int> xpr_sales_debt_export(sales_file salesFileRecord)
        {
            object rslt = private_xpr_sales_debt_export(salesFileRecord.Id);
            if (rslt != null)
            {
                return new System.Nullable<int>((int)rslt);
            }
            return null;
        }
    }
}