﻿using System;
using System.Data.Linq;
using System.Reflection;

namespace DebtPlus.LINQ
{
    partial class DebtPlusDataContext
    {
        public Int32 xpr_drop_client(int client, Int32 drop_reason)
        {
            return xpr_drop_client(client, drop_reason, null);
        }

        public Int32 xpr_drop_client(int client, string drop_reason_other)
        {
            return xpr_drop_client(client, null, drop_reason_other);
        }

        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_drop_client")]
        public Int32 xpr_drop_client([global::System.Data.Linq.Mapping.ParameterAttribute(Name = "client", DbType = "int")] Int32 client, [global::System.Data.Linq.Mapping.ParameterAttribute(Name = "drop_reason", DbType = "int")]  System.Nullable<Int32> drop_reason, [global::System.Data.Linq.Mapping.ParameterAttribute(Name = "drop_reason_other", DbType = "varchar NULL")] string drop_reason_other)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, (MethodInfo)(MethodInfo.GetCurrentMethod()), new object[] { client, drop_reason, drop_reason_other });
            return (int)result.ReturnValue;
        }
    }
}