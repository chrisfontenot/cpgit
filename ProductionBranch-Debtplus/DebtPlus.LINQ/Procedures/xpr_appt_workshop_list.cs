﻿#pragma warning disable 1591

namespace DebtPlus.LINQ
{
    using System;
    using System.Data.Linq;
    using System.Linq;
    using System.Reflection;

    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_appt_workshop_list")]
        private ISingleResult<xpr_appt_workshop_listResult> private_xpr_appt_workshop_list()
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())));
            return ((ISingleResult<xpr_appt_workshop_listResult>)(result.ReturnValue));
        }

        public System.Collections.Generic.List<xpr_appt_workshop_listResult> xpr_appt_workshop_list()
        {
            ISingleResult<xpr_appt_workshop_listResult> result = private_xpr_appt_workshop_list();
            if (result != null)
            {
                return result.ToList();
            }
            return null;
        }
    }

    public partial class xpr_appt_workshop_listResult
    {
        private int? _workshop;
        private DateTime? _start_time;
        private int? _duration;
        private string _workshop_location;
        private int? _seats_available;
        private string _counselor;
        private string _description;
        private string _HUD_Grant;
        private decimal? _HousingFeeAmount;
        private int? _booked;
        private int? _location;
        private int _seats_left;

        public xpr_appt_workshop_listResult()
        {
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_workshop", DbType = "Int NULL")]
        public int? workshop
        {
            get
            {
                return this._workshop;
            }
            set
            {
                if ((this._workshop != value))
                {
                    this._workshop = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_start_time", DbType = "DateTime NULL")]
        public DateTime? start_time
        {
            get
            {
                return this._start_time;
            }
            set
            {
                if ((this._start_time != value))
                {
                    this._start_time = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_duration", DbType = "Int NULL")]
        public int? duration
        {
            get
            {
                return this._duration;
            }
            set
            {
                if ((this._duration != value))
                {
                    this._duration = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_workshop_location", DbType = "varchar(80) NULL")]
        public string workshop_location
        {
            get
            {
                return this._workshop_location;
            }
            set
            {
                if ((this._workshop_location != value))
                {
                    this._workshop_location = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_seats_available", DbType = "Int NULL")]
        public int? seats_available
        {
            get
            {
                return this._seats_available;
            }
            set
            {
                if ((this._seats_available != value))
                {
                    this._seats_available = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_counselor", DbType = "varchar(80) NULL")]
        public string counselor
        {
            get
            {
                return this._counselor;
            }
            set
            {
                if ((this._counselor != value))
                {
                    this._counselor = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_description", DbType = "Varchar(80) NOT NULL")]
        public string description
        {
            get
            {
                return this._description;
            }
            set
            {
                if ((this._description != value))
                {
                    this._description = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_booked", DbType = "Int NULL")]
        public int? booked
        {
            get
            {
                return this._booked;
            }
            set
            {
                if ((this._booked != value))
                {
                    this._booked = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_seats_left", DbType = "Int NOT NULL")]
        public int seats_left
        {
            get
            {
                return this._seats_left;
            }
            set
            {
                if ((this._seats_left != value))
                {
                    this._seats_left = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_HUD_Grant", DbType = "varchar(50) NULL")]
        public string HUD_Grant
        {
            get
            {
                return this._HUD_Grant;
            }
            set
            {
                if ((this._HUD_Grant != value))
                {
                    this._HUD_Grant = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_HousingFeeAmount", DbType = "money NULL")]
        public Decimal? HousingFeeAmount
        {
            get
            {
                return this._HousingFeeAmount;
            }
            set
            {
                if ((this._HousingFeeAmount != value))
                {
                    this._HousingFeeAmount = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_location", DbType = "Int NULL")]
        public int? location
        {
            get
            {
                return this._location;
            }
            set
            {
                if ((this._location != value))
                {
                    this._location = value;
                }
            }
        }
    }
}