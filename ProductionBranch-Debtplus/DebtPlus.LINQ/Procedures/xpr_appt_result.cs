﻿#pragma warning disable 1591

namespace DebtPlus.LINQ
{
    using System;
    using System.Data.Linq;
    using System.Reflection;

    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_appt_result")]
        private int private_xpr_appt_result([global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> client_appointment, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "varchar(4) NULL")] string client_status, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> appt_type, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> referred_to, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> duration, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> office, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "bit NULL")] System.Nullable<Boolean> credit, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> counselor, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> @referred_by, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "DateTime NULL")] System.Nullable<DateTime> end_time, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "varchar(50) NULL")] string partner)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, (MethodInfo)(MethodInfo.GetCurrentMethod()), new object[] { client_appointment, client_status, appt_type, referred_to, duration, office, credit, counselor, referred_by, end_time, partner });
            return ((int)(result.ReturnValue));
        }

        /// <summary>
        /// Mark the appointment as having been "kept" or counseled
        /// </summary>
        public int xpr_appt_result(client_appointment record)
        {
            Int32 duration = record.end_time.HasValue ? Convert.ToInt32(record.end_time.Value.Subtract(record.start_time).TotalMinutes) : 0;
            return private_xpr_appt_result(record.Id, record.result, record.appt_type, record.referred_to, duration, record.office, record.credit, record.counselor, record.referred_by, record.end_time, record.partner);
        }
    }
}