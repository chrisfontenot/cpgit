﻿/*
<Function Name="dbo.xpr_disbursement_creditor_info" Method="xpr_disbursement_creditor_info">
    <Parameter Name="disbursement_register" Type="System.Int32" DbType="Int" />
    <Parameter Name="client" Type="System.Int32" DbType="Int" />
    <ElementType Name="xpr_disbursement_creditor_infoResult">
        <Column Name="creditor" Type="System.String" DbType="VarChar(10) NOT NULL" CanBeNull="false" />
        <Column Name="client_creditor" Type="System.Int32" DbType="Int NOT NULL" CanBeNull="false" />
        <Column Name="sched_payment" Type="System.Decimal" DbType="Money NOT NULL" CanBeNull="false" />
        <Column Name="disbursement_factor" Type="System.Decimal" DbType="Money NOT NULL" CanBeNull="false" />
        <Column Name="current_month_disbursement" Type="System.Decimal" DbType="Money NOT NULL" CanBeNull="false" />
        <Column Name="orig_balance" Type="System.Decimal" DbType="Money NOT NULL" CanBeNull="false" />
        <Column Name="orig_dmp_payment" Type="System.Decimal" DbType="Money" CanBeNull="true" />
        <Column Name="current_balance" Type="System.Decimal" DbType="Money NOT NULL" CanBeNull="false" />
        <Column Name="apr" Type="System.Double" DbType="Float NOT NULL" CanBeNull="false" />
        <Column Name="debit_amt" Type="System.Decimal" DbType="Money NOT NULL" CanBeNull="false" />
        <Column Name="priority" Type="System.Int32" DbType="Int NOT NULL" CanBeNull="false" />
        <Column Name="creditor_name" Type="System.String" DbType="VarChar(255) NOT NULL" CanBeNull="false" />
        <Column Name="account_number" Type="System.String" DbType="VarChar(22) NOT NULL" CanBeNull="false" />
        <Column Name="line_number" Type="System.Int32" DbType="Int NOT NULL" CanBeNull="false" />
        <Column Name="zero_balance" Type="System.Boolean" DbType="Bit NOT NULL" CanBeNull="false" />
        <Column Name="prorate" Type="System.Boolean" DbType="Bit NOT NULL" CanBeNull="false" />
        <Column Name="creditor_type" Type="System.String" DbType="VarChar(1) NOT NULL" CanBeNull="false" />
        <Column Name="held" Type="System.Boolean" DbType="Bit NOT NULL" CanBeNull="false" />
        <Column Name="oid" Type="System.Int32" DbType="Int NOT NULL" CanBeNull="false" />
    </ElementType>
</Function>
*/

#pragma warning disable 1591

using System;
using System.Data.Linq;
using System.Linq;
using System.Reflection;

namespace DebtPlus.LINQ
{
    public partial class DebtPlusDataContext : System.Data.Linq.DataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_disbursement_creditor_info")]
        private ISingleResult<xpr_disbursement_creditor_infoResult> pvt_xpr_disbursement_creditor_info([global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> disbursement_register, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> client)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, (MethodInfo)(MethodInfo.GetCurrentMethod()), disbursement_register, client);
            return (ISingleResult<xpr_disbursement_creditor_infoResult>)(result.ReturnValue);
        }

        public System.Collections.Generic.List<xpr_disbursement_creditor_infoResult> xpr_disbursement_creditor_info(int disbursement_register, int client)
        {
            ISingleResult<xpr_disbursement_creditor_infoResult> result = pvt_xpr_disbursement_creditor_info(disbursement_register, client);
            if (result != null)
            {
                return result.ToList();
            }
            return null;
        }
    }

    public partial class xpr_disbursement_creditor_infoResult
    {
        private string _creditor;
        private int _client_creditor;
        private decimal _sched_payment;
        private decimal _disbursement_factor;
        private decimal _current_month_disbursement;
        private decimal _orig_balance;
        private System.Nullable<decimal> _orig_dmp_payment;
        private decimal _current_balance;
        private double _apr;
        private decimal _debit_amt;
        private int _priority;
        private string _creditor_name;
        private string _account_number;
        private int _line_number;
        private bool _zero_balance;
        private bool _prorate;
        private string _creditor_type;
        private bool _held;
        private int _oid;

        public xpr_disbursement_creditor_infoResult()
        {
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_creditor", DbType = "VarChar(10) NOT NULL", CanBeNull = false)]
        public string creditor
        {
            get
            {
                return this._creditor;
            }
            set
            {
                if (this._creditor != value)
                {
                    this._creditor = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_client_creditor", DbType = "Int NOT NULL")]
        public int client_creditor
        {
            get
            {
                return this._client_creditor;
            }
            set
            {
                if (this._client_creditor != value)
                {
                    this._client_creditor = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_sched_payment", DbType = "Money NOT NULL")]
        public decimal sched_payment
        {
            get
            {
                return this._sched_payment;
            }
            set
            {
                if (this._sched_payment != value)
                {
                    this._sched_payment = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_disbursement_factor", DbType = "Money NOT NULL")]
        public decimal disbursement_factor
        {
            get
            {
                return this._disbursement_factor;
            }
            set
            {
                if (this._disbursement_factor != value)
                {
                    this._disbursement_factor = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_current_month_disbursement", DbType = "Money NOT NULL")]
        public decimal current_month_disbursement
        {
            get
            {
                return this._current_month_disbursement;
            }
            set
            {
                if (this._current_month_disbursement != value)
                {
                    this._current_month_disbursement = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_orig_balance", DbType = "Money NOT NULL")]
        public decimal orig_balance
        {
            get
            {
                return this._orig_balance;
            }
            set
            {
                if (this._orig_balance != value)
                {
                    this._orig_balance = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_orig_dmp_payment", DbType = "Money")]
        public System.Nullable<decimal> orig_dmp_payment
        {
            get
            {
                return this._orig_dmp_payment;
            }
            set
            {
                if (this._orig_dmp_payment != value)
                {
                    this._orig_dmp_payment = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_current_balance", DbType = "Money NOT NULL")]
        public decimal current_balance
        {
            get
            {
                return this._current_balance;
            }
            set
            {
                if (this._current_balance != value)
                {
                    this._current_balance = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_apr", DbType = "Float NOT NULL")]
        public double apr
        {
            get
            {
                return this._apr;
            }
            set
            {
                if (this._apr != value)
                {
                    this._apr = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_debit_amt", DbType = "Money NOT NULL")]
        public decimal debit_amt
        {
            get
            {
                return this._debit_amt;
            }
            set
            {
                if (this._debit_amt != value)
                {
                    this._debit_amt = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_priority", DbType = "Int NOT NULL")]
        public int priority
        {
            get
            {
                return this._priority;
            }
            set
            {
                if (this._priority != value)
                {
                    this._priority = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_creditor_name", DbType = "VarChar(255) NOT NULL", CanBeNull = false)]
        public string creditor_name
        {
            get
            {
                return this._creditor_name;
            }
            set
            {
                if (this._creditor_name != value)
                {
                    this._creditor_name = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_account_number", DbType = "VarChar(22) NOT NULL", CanBeNull = false)]
        public string account_number
        {
            get
            {
                return this._account_number;
            }
            set
            {
                if (this._account_number != value)
                {
                    this._account_number = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_line_number", DbType = "Int NOT NULL")]
        public int line_number
        {
            get
            {
                return this._line_number;
            }
            set
            {
                if (this._line_number != value)
                {
                    this._line_number = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_zero_balance", DbType = "Bit NOT NULL")]
        public bool zero_balance
        {
            get
            {
                return this._zero_balance;
            }
            set
            {
                if (this._zero_balance != value)
                {
                    this._zero_balance = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_prorate", DbType = "Bit NOT NULL")]
        public bool prorate
        {
            get
            {
                return this._prorate;
            }
            set
            {
                if (this._prorate != value)
                {
                    this._prorate = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_creditor_type", DbType = "VarChar(1) NOT NULL", CanBeNull = false)]
        public string creditor_type
        {
            get
            {
                return this._creditor_type;
            }
            set
            {
                if (this._creditor_type != value)
                {
                    this._creditor_type = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_held", DbType = "Bit NOT NULL")]
        public bool held
        {
            get
            {
                return this._held;
            }
            set
            {
                if (this._held != value)
                {
                    this._held = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_oid", DbType = "Int NOT NULL")]
        public int oid
        {
            get
            {
                return this._oid;
            }
            set
            {
                if (this._oid != value)
                {
                    this._oid = value;
                }
            }
        }
    }

    public partial class xpr_disbursement_creditor_infoResult : IDebtInfoSourceCollection
    {
        string IDebtInfoSourceCollection.account_number
        {
            get
            {
                return this.account_number;
            }
        }

        int? IDebtInfoSourceCollection.balance_client_creditor
        {
            get
            {
                return this.client_creditor;
            }
        }

        bool IDebtInfoSourceCollection.balance_verification_release
        {
            get
            {
                return false;
            }
        }

        string IDebtInfoSourceCollection.balance_verify_by
        {
            get
            {
                return null;
            }
        }

        DateTime? IDebtInfoSourceCollection.balance_verify_date
        {
            get
            {
                return null;
            }
        }

        bool? IDebtInfoSourceCollection.ccl_agency_account
        {
            get
            {
                return null;
            }
        }

        bool? IDebtInfoSourceCollection.ccl_always_disburse
        {
            get
            {
                return null;
            }
        }

        bool? IDebtInfoSourceCollection.ccl_prorate
        {
            get
            {
                return null;
            }
        }

        bool? IDebtInfoSourceCollection.ccl_zero_balance
        {
            get
            {
                return null;
            }
        }

        int IDebtInfoSourceCollection.check_payments
        {
            get
            {
                return 0;
            }
        }

        int IDebtInfoSourceCollection.client
        {
            get
            {
                return 0;
            }
        }

        int? IDebtInfoSourceCollection.client_creditor_balance
        {
            get
            {
                return null;
            }
        }

        int? IDebtInfoSourceCollection.client_creditor_proposal
        {
            get
            {
                return null;
            }
        }

        string IDebtInfoSourceCollection.client_name
        {
            get
            {
                return null;
            }
        }

        string IDebtInfoSourceCollection.contact_name
        {
            get
            {
                return null;
            }
        }

        string IDebtInfoSourceCollection.cr_creditor_name
        {
            get
            {
                return this.creditor_name;
            }
        }

        string IDebtInfoSourceCollection.cr_division_name
        {
            get
            {
                return string.Empty;
            }
        }

        decimal? IDebtInfoSourceCollection.cr_min_accept_amt
        {
            get
            {
                return 0m;
            }
        }

        double? IDebtInfoSourceCollection.cr_min_accept_pct
        {
            get
            {
                return 0.0D;
            }
        }

        char? IDebtInfoSourceCollection.cr_payment_balance
        {
            get
            {
                return 'B';
            }
        }

        string IDebtInfoSourceCollection.created_by
        {
            get
            {
                return string.Empty;
            }
        }

        string IDebtInfoSourceCollection.creditor
        {
            get
            {
                return this.creditor;
            }
        }

        double? IDebtInfoSourceCollection.creditor_interest
        {
            get
            {
                return this.apr;
            }
        }

        string IDebtInfoSourceCollection.creditor_name
        {
            get
            {
                return this.creditor_name;
            }
        }

        decimal? IDebtInfoSourceCollection.current_sched_payment
        {
            get
            {
                return this.sched_payment;
            }
        }

        DateTime IDebtInfoSourceCollection.date_created
        {
            get
            {
                return DateTime.MinValue;
            }
        }

        DateTime? IDebtInfoSourceCollection.date_disp_changed
        {
            get
            {
                return null;
            }
        }

        decimal IDebtInfoSourceCollection.disbursement_factor
        {
            get
            {
                return this.disbursement_factor;
            }
        }

        double? IDebtInfoSourceCollection.dmp_interest
        {
            get
            {
                return this.apr;
            }
        }

        double? IDebtInfoSourceCollection.dmp_payout_interest
        {
            get
            {
                return this.apr;
            }
        }

        DateTime? IDebtInfoSourceCollection.drop_date
        {
            get
            {
                return null;
            }
        }

        int? IDebtInfoSourceCollection.drop_reason
        {
            get
            {
                return null;
            }
        }

        DateTime? IDebtInfoSourceCollection.drop_reason_sent
        {
            get
            {
                return null;
            }
        }

        DateTime? IDebtInfoSourceCollection.expected_payout_date
        {
            get
            {
                return null;
            }
        }

        double? IDebtInfoSourceCollection.fairshare_pct_check
        {
            get
            {
                return null;
            }
        }

        double? IDebtInfoSourceCollection.fairshare_pct_eft
        {
            get
            {
                return null;
            }
        }

        int? IDebtInfoSourceCollection.first_payment
        {
            get
            {
                return null;
            }
        }

        decimal? IDebtInfoSourceCollection.first_payment_amt
        {
            get
            {
                return null;
            }
        }

        DateTime? IDebtInfoSourceCollection.first_payment_date
        {
            get
            {
                return null;
            }
        }

        string IDebtInfoSourceCollection.first_payment_type
        {
            get
            {
                return null;
            }
        }

        bool IDebtInfoSourceCollection.hold_disbursements
        {
            get
            {
                return this.held;
            }
        }

        int IDebtInfoSourceCollection.Id
        {
            get
            {
                return this.client_creditor;
            }
        }

        decimal IDebtInfoSourceCollection.interest_this_creditor
        {
            get
            {
                return 0m;
            }
        }

        bool IDebtInfoSourceCollection.irs_form_on_file
        {
            get
            {
                return false;
            }
        }

        int IDebtInfoSourceCollection.IsActive
        {
            get
            {
                return 1;
            }
        }

        string IDebtInfoSourceCollection.last_communication
        {
            get
            {
                return null;
            }
        }

        int? IDebtInfoSourceCollection.last_payment
        {
            get
            {
                return null;
            }
        }

        decimal? IDebtInfoSourceCollection.last_payment_amt
        {
            get
            {
                return null;
            }
        }

        DateTime? IDebtInfoSourceCollection.last_payment_date
        {
            get
            {
                return null;
            }
        }

        DateTime? IDebtInfoSourceCollection.last_payment_date_b4_dmp
        {
            get
            {
                return null;
            }
        }

        string IDebtInfoSourceCollection.last_payment_type
        {
            get
            {
                return null;
            }
        }

        DateTime? IDebtInfoSourceCollection.last_stmt_date
        {
            get
            {
                return null;
            }
        }

        int IDebtInfoSourceCollection.line_number
        {
            get
            {
                return this.line_number;
            }
        }

        string IDebtInfoSourceCollection.message
        {
            get
            {
                return null;
            }
        }

        int IDebtInfoSourceCollection.months_delinquent
        {
            get
            {
                return 0;
            }
        }

        double IDebtInfoSourceCollection.non_dmp_interest
        {
            get
            {
                return 0.0D;
            }
        }

        decimal IDebtInfoSourceCollection.non_dmp_payment
        {
            get
            {
                return 0m;
            }
        }

        decimal? IDebtInfoSourceCollection.orig_balance
        {
            get
            {
                return this.orig_balance;
            }
        }

        decimal? IDebtInfoSourceCollection.orig_balance_adjustment
        {
            get
            {
                return null;
            }
        }

        decimal? IDebtInfoSourceCollection.orig_dmp_payment
        {
            get
            {
                return this.orig_dmp_payment;
            }
        }

        decimal? IDebtInfoSourceCollection.payments_month_0
        {
            get
            {
                return this.current_month_disbursement;
            }
        }

        decimal? IDebtInfoSourceCollection.payments_month_1
        {
            get
            {
                return null;
            }
        }

        decimal IDebtInfoSourceCollection.payments_this_creditor
        {
            get
            {
                return 0m;
            }
        }

        double? IDebtInfoSourceCollection.percent_balance
        {
            get
            {
                return null;
            }
        }

        int? IDebtInfoSourceCollection.person
        {
            get
            {
                return null;
            }
        }

        DateTime? IDebtInfoSourceCollection.prenote_date
        {
            get
            {
                return null;
            }
        }

        int IDebtInfoSourceCollection.priority
        {
            get
            {
                return this.priority;
            }
        }

        decimal? IDebtInfoSourceCollection.proposal_balance
        {
            get
            {
                return null;
            }
        }

        int? IDebtInfoSourceCollection.proposal_status
        {
            get
            {
                return null;
            }
        }

        decimal IDebtInfoSourceCollection.returns_this_creditor
        {
            get
            {
                return 0m;
            }
        }

        string IDebtInfoSourceCollection.rpps_client_type_indicator
        {
            get
            {
                return " ";
            }
        }

        decimal IDebtInfoSourceCollection.sched_payment
        {
            get
            {
                return this.disbursement_factor;
            }
        }

        int IDebtInfoSourceCollection.send_bal_verify
        {
            get
            {
                return 0;
            }
        }

        bool IDebtInfoSourceCollection.send_drop_notice
        {
            get
            {
                return false;
            }
        }

        DateTime? IDebtInfoSourceCollection.start_date
        {
            get
            {
                return null;
            }
        }

        bool IDebtInfoSourceCollection.student_loan_release
        {
            get
            {
                return false;
            }
        }

        int? IDebtInfoSourceCollection.terms
        {
            get
            {
                return 60;
            }
        }

        decimal? IDebtInfoSourceCollection.total_interest
        {
            get
            {
                return 0m;
            }
        }

        decimal? IDebtInfoSourceCollection.total_payments
        {
            get
            {
                return 0m;
            }
        }

        decimal? IDebtInfoSourceCollection.total_sched_payment
        {
            get
            {
                return 0m;
            }
        }

        DateTime? IDebtInfoSourceCollection.verify_request_date
        {
            get
            {
                return null;
            }
        }

        decimal IDebtInfoSourceCollection.current_balance
        {
            get
            {
                return this.current_balance;
            }
        }

        string IDebtInfoSourceCollection.creditor_type
        {
            get
            {
                return this.creditor_type;
            }
        }

        decimal IDebtInfoSourceCollection.debit_amt
        {
            get
            {
                return this.debit_amt;
            }
        }

        int? IDebtInfoSourceCollection.oID
        {
            get
            {
                return this.oid;
            }
        }
    }
}