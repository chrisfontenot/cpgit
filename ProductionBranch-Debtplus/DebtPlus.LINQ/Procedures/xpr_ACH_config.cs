﻿#pragma warning disable 1591

namespace DebtPlus.LINQ
{
    using System.Data.Linq;
    using System.Linq;
    using System.Reflection;

    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_ACH_config")]
        private ISingleResult<xpr_ACH_configResult> private_xpr_ACH_config([global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> bank)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), bank);
            return ((ISingleResult<xpr_ACH_configResult>)(result.ReturnValue));
        }

        public xpr_ACH_configResult xpr_ACH_config(System.Nullable<int> bank)
        {
            ISingleResult<xpr_ACH_configResult> result = private_xpr_ACH_config(bank);
            if (result != null)
            {
                return result.FirstOrDefault();
            }
            return null;
        }
    }

    public partial class xpr_ACH_configResult
    {
        private string _priority_code;
        private string _company_identification;
        private string _immediate_destination;
        private string _immediate_destination_name;
        private string _immediate_origin_name;
        private string _immediate_origin;
        private System.Nullable<int> _reference_code;
        private string _originating_dfi_identification;
        private string _standard_entry_class;
        private string _company_entry_description;
        private string _prefix;
        private string _suffix;
        private int _batch_number;
        private int _transaction_number;
        private string _ach_message_authentication;
        private string _ach_company_identification;

        public xpr_ACH_configResult()
        {
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_priority_code", DbType = "VarChar(2)")]
        public string priority_code
        {
            get
            {
                return this._priority_code;
            }
            set
            {
                if ((this._priority_code != value))
                {
                    this._priority_code = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_company_identification", DbType = "VarChar(10)")]
        public string company_identification
        {
            get
            {
                return this._company_identification;
            }
            set
            {
                if ((this._company_identification != value))
                {
                    this._company_identification = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_immediate_destination", DbType = "VarChar(12)")]
        public string immediate_destination
        {
            get
            {
                return this._immediate_destination;
            }
            set
            {
                if ((this._immediate_destination != value))
                {
                    this._immediate_destination = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_immediate_destination_name", DbType = "VarChar(30)")]
        public string immediate_destination_name
        {
            get
            {
                return this._immediate_destination_name;
            }
            set
            {
                if ((this._immediate_destination_name != value))
                {
                    this._immediate_destination_name = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_immediate_origin_name", DbType = "VarChar(30)")]
        public string immediate_origin_name
        {
            get
            {
                return this._immediate_origin_name;
            }
            set
            {
                if ((this._immediate_origin_name != value))
                {
                    this._immediate_origin_name = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_immediate_origin", DbType = "VarChar(12)")]
        public string immediate_origin
        {
            get
            {
                return this._immediate_origin;
            }
            set
            {
                if ((this._immediate_origin != value))
                {
                    this._immediate_origin = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_reference_code", DbType = "Int")]
        public System.Nullable<int> reference_code
        {
            get
            {
                return this._reference_code;
            }
            set
            {
                if ((this._reference_code != value))
                {
                    this._reference_code = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_originating_dfi_identification", DbType = "VarChar(12)")]
        public string originating_dfi_identification
        {
            get
            {
                return this._originating_dfi_identification;
            }
            set
            {
                if ((this._originating_dfi_identification != value))
                {
                    this._originating_dfi_identification = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_standard_entry_class", DbType = "VarChar(3) NOT NULL", CanBeNull = false)]
        public string standard_entry_class
        {
            get
            {
                return this._standard_entry_class;
            }
            set
            {
                if ((this._standard_entry_class != value))
                {
                    this._standard_entry_class = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_company_entry_description", DbType = "VarChar(10) NOT NULL", CanBeNull = false)]
        public string company_entry_description
        {
            get
            {
                return this._company_entry_description;
            }
            set
            {
                if ((this._company_entry_description != value))
                {
                    this._company_entry_description = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_prefix", DbType = "VarChar(256)")]
        public string prefix
        {
            get
            {
                return this._prefix;
            }
            set
            {
                if ((this._prefix != value))
                {
                    this._prefix = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_suffix", DbType = "VarChar(256)")]
        public string suffix
        {
            get
            {
                return this._suffix;
            }
            set
            {
                if ((this._suffix != value))
                {
                    this._suffix = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_batch_number", DbType = "Int NOT NULL")]
        public int batch_number
        {
            get
            {
                return this._batch_number;
            }
            set
            {
                if ((this._batch_number != value))
                {
                    this._batch_number = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_transaction_number", DbType = "Int NOT NULL")]
        public int transaction_number
        {
            get
            {
                return this._transaction_number;
            }
            set
            {
                if ((this._transaction_number != value))
                {
                    this._transaction_number = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_ach_message_authentication", DbType = "VarChar(19)")]
        public string ach_message_authentication
        {
            get
            {
                return this._ach_message_authentication;
            }
            set
            {
                if ((this._ach_message_authentication != value))
                {
                    this._ach_message_authentication = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_ach_company_identification", DbType = "VarChar(10)")]
        public string ach_company_identification
        {
            get
            {
                return this._ach_company_identification;
            }
            set
            {
                if ((this._ach_company_identification != value))
                {
                    this._ach_company_identification = value;
                }
            }
        }
    }
}