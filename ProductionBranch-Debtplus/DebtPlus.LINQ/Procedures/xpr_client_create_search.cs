﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.1008
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DebtPlus.LINQ
{
    using System;
    using System.Data.Linq;
    using System.Linq;
    using System.Reflection;

    public partial class xpr_client_create_searchResult
    {
        public xpr_client_create_searchResult()
        {
            _clientID = 0;
            _name = string.Empty;
            _active_status = string.Empty;
            _ssn = string.Empty;
            _homeTelephone = string.Empty;
        }

        private Int32 _clientID;
        private string _name;
        private string _active_status;
        private string _ssn;
        private string _homeTelephone;

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_homeTelephone", DbType = "varchar(80) NULL")]
        public string homeTelephone
        {
            get
            {
                return _homeTelephone;
            }
            set
            {
                if (value != _homeTelephone)
                {
                    value = _homeTelephone;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_ssn", DbType = "varchar(80) NULL")]
        public string ssn
        {
            get
            {
                return _ssn;
            }
            set
            {
                if (value != _ssn)
                {
                    value = _ssn;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_active_status", DbType = "varchar(80) NULL")]
        public string active_status
        {
            get
            {
                return _active_status;
            }
            set
            {
                if (value != _active_status)
                {
                    value = _active_status;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_clientID", DbType = "Int NOT NULL")]
        public Int32 clientID
        {
            get
            {
                return _clientID;
            }
            set
            {
                if (value != _clientID)
                {
                    value = _clientID;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_name", DbType = "varchar(80) NULL")]
        public string name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value != _name)
                {
                    value = _name;
                }
            }
        }
    }

    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_client_create_search")]
        private ISingleResult<xpr_client_create_searchResult> pvt_xpr_client_create_search([global::System.Data.Linq.Mapping.ParameterAttribute(Name = "TelephoneNumberKey", DbType = "VarChar(10)")] string TelephoneNumberKey)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), TelephoneNumberKey);
            return ((ISingleResult<xpr_client_create_searchResult>)(result.ReturnValue));
        }

        public System.Collections.Generic.List<xpr_client_create_searchResult> xpr_client_create_search(string TelephoneNumberKey)
        {
            Int32 priorTimeout = this.CommandTimeout;
            this.CommandTimeout = System.Math.Max(600, priorTimeout);
            try
            {
                ISingleResult<xpr_client_create_searchResult> result = pvt_xpr_client_create_search(TelephoneNumberKey);
                if (result != null)
                {
                    return result.ToList();
                }
                return null;
            }
            finally
            {
                this.CommandTimeout = priorTimeout;
            }
        }
    }
}