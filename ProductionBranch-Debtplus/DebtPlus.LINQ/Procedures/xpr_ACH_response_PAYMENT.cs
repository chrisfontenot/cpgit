﻿#pragma warning disable 1591

namespace DebtPlus.LINQ
{
    using System.Data.Linq;
    using System.Reflection;

    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_ACH_response_PAYMENT")]
        public int xpr_ACH_response_PAYMENT([global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> deposit_batch_detail, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "VarChar(4)")] string reason, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "VarChar(50)")] string ach_response_batch_id)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), deposit_batch_detail, reason, ach_response_batch_id);
            return ((int)(result.ReturnValue));
        }
    }
}