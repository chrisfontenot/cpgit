﻿#pragma warning disable 1591

namespace DebtPlus.LINQ
{
    using System.Data.Linq;
    using System.Linq;
    using System.Reflection;

    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_ACH_Reject_Letters")]
        private ISingleResult<xpr_ACH_Reject_LettersResult> private_xpr_ACH_Reject_Letters([global::System.Data.Linq.Mapping.ParameterAttribute(Name = "ACHFile", DbType = "Int")] System.Nullable<int> ACHFile)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), ACHFile);
            return ((ISingleResult<xpr_ACH_Reject_LettersResult>)(result.ReturnValue));
        }

        public System.Collections.Generic.List<xpr_ACH_Reject_LettersResult> xpr_ACH_Reject_Letters([global::System.Data.Linq.Mapping.ParameterAttribute(Name = "ACHFile", DbType = "Int")] System.Nullable<int> ACHFile)
        {
            ISingleResult<xpr_ACH_Reject_LettersResult> result = private_xpr_ACH_Reject_Letters(ACHFile);
            if (result != null)
            {
                return result.ToList();
            }
            return null;
        }
    }

    public partial class xpr_ACH_Reject_LettersResult
    {
        private System.Nullable<int> _client;
        private string _letter_code;

        public xpr_ACH_Reject_LettersResult()
        {
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_client", DbType = "Int")]
        public System.Nullable<int> client
        {
            get
            {
                return this._client;
            }
            set
            {
                if ((this._client != value))
                {
                    this._client = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_letter_code", DbType = "VarChar(10)")]
        public string letter_code
        {
            get
            {
                return this._letter_code;
            }
            set
            {
                if ((this._letter_code != value))
                {
                    this._letter_code = value;
                }
            }
        }
    }
}