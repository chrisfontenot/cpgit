﻿#pragma warning disable 1591
namespace DebtPlus.LINQ
{
    using System;
    using System.Data.Linq;
    using System.Reflection;

    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_creditor_grandfather")]
        private int private_xpr_creditor_grandfather([global::System.Data.Linq.Mapping.ParameterAttribute(Name = "creditor", DbType = "varchar(10)")] string CreditorId, [global::System.Data.Linq.Mapping.ParameterAttribute(Name = "old_rate", DbType = "float")] double OldRate)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), CreditorId, OldRate);
            return ((int)(result.ReturnValue));
        }

        /// <summary>
        /// Grandfather the interest rates to old debts for this creditor
        /// </summary>
        public int xpr_creditor_grandfather(string CreditorId, double OldRate)
        {
            return private_xpr_creditor_grandfather(CreditorId, OldRate);
        }
    }
}

#pragma warning restore 1591