﻿using System.Reflection;

namespace DebtPlus.LINQ
{
    partial class DebtPlusDataContext
    {
        /// <summary>
        /// Translate the client ID to an office appointment if possible. We only look at the most recently completed office appointments.
        /// </summary>
        /// <param name="client">Client ID</param>
        /// <returns>The ID of the appointment if possible or NULL if there are no appointments</returns>
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.map_client_to_last_completed_office_appt", IsComposable = true)]
        public System.Nullable<int> map_client_to_last_completed_office_appt([global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> client)
        {
            return ((System.Nullable<int>)(this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), client).ReturnValue));
        }
    }
}