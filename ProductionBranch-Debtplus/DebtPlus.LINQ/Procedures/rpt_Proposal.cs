﻿using System.Data.Linq;
using System.Linq;
using System.Reflection;

namespace DebtPlus.LINQ
{
    public partial class rpt_Proposal_AccountNumberResult
    {
        private int _client_creditor;
        private string _account_number;
        private string _ssn;
        private string _referred_by;
        private string _ssn2;

        public rpt_Proposal_AccountNumberResult()
        {
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_client_creditor", DbType = "Int NOT NULL")]
        public int client_creditor
        {
            get
            {
                return this._client_creditor;
            }
            set
            {
                if ((this._client_creditor != value))
                {
                    this._client_creditor = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_account_number", DbType = "VarChar(22)")]
        public string account_number
        {
            get
            {
                return this._account_number;
            }
            set
            {
                if ((this._account_number != value))
                {
                    this._account_number = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_ssn", DbType = "VarChar(9)")]
        public string ssn
        {
            get
            {
                return this._ssn;
            }
            set
            {
                if ((this._ssn != value))
                {
                    this._ssn = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_referred_by", DbType = "VarChar(50)")]
        public string referred_by
        {
            get
            {
                return this._referred_by;
            }
            set
            {
                if ((this._referred_by != value))
                {
                    this._referred_by = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_ssn2", DbType = "VarChar(9)")]
        public string ssn2
        {
            get
            {
                return this._ssn2;
            }
            set
            {
                if ((this._ssn2 != value))
                {
                    this._ssn2 = value;
                }
            }
        }
    }

    public partial class rpt_Proposal_AddressInformationResult
    {
        private string _spouse;
        private string _name;
        private string _address1;
        private string _address2;
        private string _address3;
        private string _zipcode;
        private string _applicant;
        private int _client;
        private System.Nullable<int> _client_creditor_proposal;

        public rpt_Proposal_AddressInformationResult()
        {
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_spouse", DbType = "VarChar(256)")]
        public string spouse
        {
            get
            {
                return this._spouse;
            }
            set
            {
                if ((this._spouse != value))
                {
                    this._spouse = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_name", DbType = "VarChar(256)")]
        public string name
        {
            get
            {
                return this._name;
            }
            set
            {
                if ((this._name != value))
                {
                    this._name = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_address1", DbType = "VarChar(256)")]
        public string address1
        {
            get
            {
                return this._address1;
            }
            set
            {
                if ((this._address1 != value))
                {
                    this._address1 = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_address2", DbType = "VarChar(256)")]
        public string address2
        {
            get
            {
                return this._address2;
            }
            set
            {
                if ((this._address2 != value))
                {
                    this._address2 = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_address3", DbType = "VarChar(256)")]
        public string address3
        {
            get
            {
                return this._address3;
            }
            set
            {
                if ((this._address3 != value))
                {
                    this._address3 = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_zipcode", DbType = "VarChar(256)")]
        public string zipcode
        {
            get
            {
                return this._zipcode;
            }
            set
            {
                if ((this._zipcode != value))
                {
                    this._zipcode = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_applicant", DbType = "VarChar(256)")]
        public string applicant
        {
            get
            {
                return this._applicant;
            }
            set
            {
                if ((this._applicant != value))
                {
                    this._applicant = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_client", DbType = "Int NOT NULL")]
        public int client
        {
            get
            {
                return this._client;
            }
            set
            {
                if ((this._client != value))
                {
                    this._client = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_client_creditor_proposal", DbType = "Int")]
        public System.Nullable<int> client_creditor_proposal
        {
            get
            {
                return this._client_creditor_proposal;
            }
            set
            {
                if ((this._client_creditor_proposal != value))
                {
                    this._client_creditor_proposal = value;
                }
            }
        }
    }

    public partial class rpt_Proposal_AssetsResult
    {
        private string _auto_make;
        private string _auto_model;
        private int _auto_year;
        private System.Nullable<decimal> _balance;
        private decimal _value;
        private System.Nullable<int> _arrears_months;

        public rpt_Proposal_AssetsResult()
        {
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_auto_make", DbType = "VarChar(101) NOT NULL", CanBeNull = false)]
        public string auto_make
        {
            get
            {
                return this._auto_make;
            }
            set
            {
                if ((this._auto_make != value))
                {
                    this._auto_make = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_auto_model", DbType = "VarChar(50) NOT NULL", CanBeNull = false)]
        public string auto_model
        {
            get
            {
                return this._auto_model;
            }
            set
            {
                if ((this._auto_model != value))
                {
                    this._auto_model = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_auto_year", DbType = "Int NOT NULL")]
        public int auto_year
        {
            get
            {
                return this._auto_year;
            }
            set
            {
                if ((this._auto_year != value))
                {
                    this._auto_year = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_balance", DbType = "Money")]
        public System.Nullable<decimal> balance
        {
            get
            {
                return this._balance;
            }
            set
            {
                if ((this._balance != value))
                {
                    this._balance = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_value", DbType = "Money NOT NULL")]
        public decimal value
        {
            get
            {
                return this._value;
            }
            set
            {
                if ((this._value != value))
                {
                    this._value = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_arrears_months", DbType = "Int")]
        public System.Nullable<int> arrears_months
        {
            get
            {
                return this._arrears_months;
            }
            set
            {
                if ((this._arrears_months != value))
                {
                    this._arrears_months = value;
                }
            }
        }
    }

    public partial class rpt_Proposal_ContributionResult
    {
        private System.Nullable<int> _client;
        private string _creditor;
        private System.Nullable<int> _client_creditor;
        private string _counselor_name;
        private string _message;

        public rpt_Proposal_ContributionResult()
        {
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_client", DbType = "Int")]
        public System.Nullable<int> client
        {
            get
            {
                return this._client;
            }
            set
            {
                if ((this._client != value))
                {
                    this._client = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_creditor", DbType = "VarChar(10)")]
        public string creditor
        {
            get
            {
                return this._creditor;
            }
            set
            {
                if ((this._creditor != value))
                {
                    this._creditor = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_client_creditor", DbType = "Int")]
        public System.Nullable<int> client_creditor
        {
            get
            {
                return this._client_creditor;
            }
            set
            {
                if ((this._client_creditor != value))
                {
                    this._client_creditor = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_counselor_name", DbType = "VarChar(80)")]
        public string counselor_name
        {
            get
            {
                return this._counselor_name;
            }
            set
            {
                if ((this._counselor_name != value))
                {
                    this._counselor_name = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_message", DbType = "VarChar(114)")]
        public string message
        {
            get
            {
                return this._message;
            }
            set
            {
                if ((this._message != value))
                {
                    this._message = value;
                }
            }
        }
    }

    public partial class rpt_Proposal_CreditorListResult
    {
        private System.Nullable<decimal> _balance;
        private decimal _disbursement_factor;
        private string _marker;
        private string _creditor_name;
        private System.Nullable<int> _client_creditor_proposal;

        public rpt_Proposal_CreditorListResult()
        {
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_balance", DbType = "Money")]
        public System.Nullable<decimal> balance
        {
            get
            {
                return this._balance;
            }
            set
            {
                if ((this._balance != value))
                {
                    this._balance = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_disbursement_factor", DbType = "Money NOT NULL")]
        public decimal disbursement_factor
        {
            get
            {
                return this._disbursement_factor;
            }
            set
            {
                if ((this._disbursement_factor != value))
                {
                    this._disbursement_factor = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_marker", DbType = "VarChar(1) NOT NULL", CanBeNull = false)]
        public string marker
        {
            get
            {
                return this._marker;
            }
            set
            {
                if ((this._marker != value))
                {
                    this._marker = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_creditor_name", DbType = "VarChar(255) NOT NULL", CanBeNull = false)]
        public string creditor_name
        {
            get
            {
                return this._creditor_name;
            }
            set
            {
                if ((this._creditor_name != value))
                {
                    this._creditor_name = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_client_creditor_proposal", DbType = "Int")]
        public System.Nullable<int> client_creditor_proposal
        {
            get
            {
                return this._client_creditor_proposal;
            }
            set
            {
                if ((this._client_creditor_proposal != value))
                {
                    this._client_creditor_proposal = value;
                }
            }
        }
    }

    public partial class rpt_Proposal_EmployerResult
    {
        private string _employer;
        private string _financial_problem;

        public rpt_Proposal_EmployerResult()
        {
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_employer", DbType = "VarChar(50) NOT NULL", CanBeNull = false)]
        public string employer
        {
            get
            {
                return this._employer;
            }
            set
            {
                if ((this._employer != value))
                {
                    this._employer = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_financial_problem", DbType = "VarChar(50) NOT NULL", CanBeNull = false)]
        public string financial_problem
        {
            get
            {
                return this._financial_problem;
            }
            set
            {
                if ((this._financial_problem != value))
                {
                    this._financial_problem = value;
                }
            }
        }
    }

    public partial class rpt_Proposal_ProposalResult
    {
        private int _client;
        private string _creditor;
        private int _client_creditor;
        private System.Nullable<System.DateTime> _print_date;
        private string _message;
        private bool _proposal_budget_info;
        private int _client_creditor_proposal;
        private bool _proposal_income_info;

        public rpt_Proposal_ProposalResult()
        {
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_client", DbType = "Int NOT NULL")]
        public int client
        {
            get
            {
                return this._client;
            }
            set
            {
                if ((this._client != value))
                {
                    this._client = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_creditor", DbType = "VarChar(10)")]
        public string creditor
        {
            get
            {
                return this._creditor;
            }
            set
            {
                if ((this._creditor != value))
                {
                    this._creditor = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_client_creditor", DbType = "Int NOT NULL")]
        public int client_creditor
        {
            get
            {
                return this._client_creditor;
            }
            set
            {
                if ((this._client_creditor != value))
                {
                    this._client_creditor = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_print_date", DbType = "DateTime")]
        public System.Nullable<System.DateTime> print_date
        {
            get
            {
                return this._print_date;
            }
            set
            {
                if ((this._print_date != value))
                {
                    this._print_date = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_message", DbType = "VarChar(256)")]
        public string message
        {
            get
            {
                return this._message;
            }
            set
            {
                if ((this._message != value))
                {
                    this._message = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_proposal_budget_info", DbType = "Bit NOT NULL")]
        public bool proposal_budget_info
        {
            get
            {
                return this._proposal_budget_info;
            }
            set
            {
                if ((this._proposal_budget_info != value))
                {
                    this._proposal_budget_info = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_client_creditor_proposal", DbType = "Int NOT NULL")]
        public int client_creditor_proposal
        {
            get
            {
                return this._client_creditor_proposal;
            }
            set
            {
                if ((this._client_creditor_proposal != value))
                {
                    this._client_creditor_proposal = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_proposal_income_info", DbType = "Bit NOT NULL")]
        public bool proposal_income_info
        {
            get
            {
                return this._proposal_income_info;
            }
            set
            {
                if ((this._proposal_income_info != value))
                {
                    this._proposal_income_info = value;
                }
            }
        }
    }

    public partial class rpt_ProposalAddressResult
    {
        private string _spouse;
        private string _name;
        private string _address1;
        private string _address2;
        private string _address3;
        private string _zipcode;
        private string _client;

        public rpt_ProposalAddressResult()
        {
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_spouse", DbType = "VarChar(256)")]
        public string spouse
        {
            get
            {
                return this._spouse;
            }
            set
            {
                if ((this._spouse != value))
                {
                    this._spouse = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_name", DbType = "VarChar(256)")]
        public string name
        {
            get
            {
                return this._name;
            }
            set
            {
                if ((this._name != value))
                {
                    this._name = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_address1", DbType = "VarChar(80)")]
        public string address1
        {
            get
            {
                return this._address1;
            }
            set
            {
                if ((this._address1 != value))
                {
                    this._address1 = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_address2", DbType = "VarChar(256)")]
        public string address2
        {
            get
            {
                return this._address2;
            }
            set
            {
                if ((this._address2 != value))
                {
                    this._address2 = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_address3", DbType = "VarChar(256)")]
        public string address3
        {
            get
            {
                return this._address3;
            }
            set
            {
                if ((this._address3 != value))
                {
                    this._address3 = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_zipcode", DbType = "VarChar(256)")]
        public string zipcode
        {
            get
            {
                return this._zipcode;
            }
            set
            {
                if ((this._zipcode != value))
                {
                    this._zipcode = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_client", DbType = "VarChar(10)")]
        public string client
        {
            get
            {
                return this._client;
            }
            set
            {
                if ((this._client != value))
                {
                    this._client = value;
                }
            }
        }
    }

    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.rpt_Proposal_AccountNumber")]
        private ISingleResult<rpt_Proposal_AccountNumberResult> private_rpt_Proposal_AccountNumber([global::System.Data.Linq.Mapping.ParameterAttribute(Name = "ClientCreditor", DbType = "Int")] System.Nullable<int> clientCreditor)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), clientCreditor);
            return ((ISingleResult<rpt_Proposal_AccountNumberResult>)(result.ReturnValue));
        }

        public System.Collections.Generic.List<rpt_Proposal_AccountNumberResult> rpt_Proposal_AccountNumber(System.Nullable<int> clientCreditor)
        {
            ISingleResult<rpt_Proposal_AccountNumberResult> rslt = private_rpt_Proposal_AccountNumber(clientCreditor);
            if (rslt != null)
            {
                return rslt.ToList();
            }
            return null;
        }

        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.rpt_Proposal_AddressInformation")]
        private ISingleResult<rpt_Proposal_AddressInformationResult> private_rpt_Proposal_AddressInformation([global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> client_creditor_proposal)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), client_creditor_proposal);
            return ((ISingleResult<rpt_Proposal_AddressInformationResult>)(result.ReturnValue));
        }

        public System.Collections.Generic.List<rpt_Proposal_AddressInformationResult> rpt_Proposal_AddressInformation(System.Nullable<int> client_creditor_proposal)
        {
            ISingleResult<rpt_Proposal_AddressInformationResult> rslt = private_rpt_Proposal_AddressInformation(client_creditor_proposal);
            if (rslt != null)
            {
                return rslt.ToList();
            }
            return null;
        }

        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.rpt_Proposal_Assets")]
        private ISingleResult<rpt_Proposal_AssetsResult> private_rpt_Proposal_Assets([global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> client)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), client);
            return ((ISingleResult<rpt_Proposal_AssetsResult>)(result.ReturnValue));
        }

        public System.Collections.Generic.List<rpt_Proposal_AssetsResult> rpt_Proposal_Assets(System.Nullable<int> client)
        {
            ISingleResult<rpt_Proposal_AssetsResult> rslt = private_rpt_Proposal_Assets(client);
            if (rslt != null)
            {
                return rslt.ToList();
            }
            return null;
        }

        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.rpt_Proposal_Contribution")]
        private ISingleResult<rpt_Proposal_ContributionResult> private_rpt_Proposal_Contribution([global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> client_creditor)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), client_creditor);
            return ((ISingleResult<rpt_Proposal_ContributionResult>)(result.ReturnValue));
        }

        public System.Collections.Generic.List<rpt_Proposal_ContributionResult> rpt_Proposal_Contribution(System.Nullable<int> client_creditor)
        {
            ISingleResult<rpt_Proposal_ContributionResult> rslt = private_rpt_Proposal_Contribution(client_creditor);
            if (rslt != null)
            {
                return rslt.ToList();
            }
            return null;
        }

        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.rpt_Proposal_CreditorList")]
        private ISingleResult<rpt_Proposal_CreditorListResult> private_rpt_Proposal_CreditorList([global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> client_creditor_proposal)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), client_creditor_proposal);
            return ((ISingleResult<rpt_Proposal_CreditorListResult>)(result.ReturnValue));
        }

        public System.Collections.Generic.List<rpt_Proposal_CreditorListResult> rpt_Proposal_CreditorList(System.Nullable<int> client_creditor_proposal)
        {
            ISingleResult<rpt_Proposal_CreditorListResult> rslt = private_rpt_Proposal_CreditorList(client_creditor_proposal);
            if (rslt != null)
            {
                return rslt.ToList();
            }
            return null;
        }

        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.rpt_Proposal_Employer")]
        private ISingleResult<rpt_Proposal_EmployerResult> private_rpt_Proposal_Employer([global::System.Data.Linq.Mapping.ParameterAttribute(Name = "Client", DbType = "Int")] System.Nullable<int> client)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), client);
            return ((ISingleResult<rpt_Proposal_EmployerResult>)(result.ReturnValue));
        }

        public System.Collections.Generic.List<rpt_Proposal_EmployerResult> rpt_Proposal_Employer(System.Nullable<int> client_creditor_proposal)
        {
            ISingleResult<rpt_Proposal_EmployerResult> rslt = private_rpt_Proposal_Employer(client_creditor_proposal);
            if (rslt != null)
            {
                return rslt.ToList();
            }
            return null;
        }

        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.rpt_Proposal_Proposal")]
        private ISingleResult<rpt_Proposal_ProposalResult> private_rpt_Proposal_Proposal([global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> client_creditor_proposal, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> proposal_mode, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "VarChar(256)")] string proposal_batch_label)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), client_creditor_proposal, proposal_mode, proposal_batch_label);
            return ((ISingleResult<rpt_Proposal_ProposalResult>)(result.ReturnValue));
        }

        public System.Collections.Generic.List<rpt_Proposal_ProposalResult> rpt_Proposal_Proposal(System.Nullable<int> client_creditor_proposal, System.Nullable<int> proposal_mode, string proposal_batch_label)
        {
            ISingleResult<rpt_Proposal_ProposalResult> rslt = private_rpt_Proposal_Proposal(client_creditor_proposal, proposal_mode, proposal_batch_label);
            if (rslt != null)
            {
                return rslt.OrderBy(s => s.creditor).ThenBy(s => s.client).ThenBy(s => s.client_creditor).ToList<rpt_Proposal_ProposalResult>();
            }
            return null;
        }

        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.rpt_ProposalAddress")]
        private ISingleResult<rpt_ProposalAddressResult> private_rpt_ProposalAddress([global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> client_creditor_proposal)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), client_creditor_proposal);
            return ((ISingleResult<rpt_ProposalAddressResult>)(result.ReturnValue));
        }

        public System.Collections.Generic.List<rpt_ProposalAddressResult> rpt_ProposalAddress(System.Nullable<int> client_creditor_proposal)
        {
            ISingleResult<rpt_ProposalAddressResult> rslt = private_rpt_ProposalAddress(client_creditor_proposal);
            if (rslt != null)
            {
                return rslt.ToList();
            }
            return null;
        }
    }
}