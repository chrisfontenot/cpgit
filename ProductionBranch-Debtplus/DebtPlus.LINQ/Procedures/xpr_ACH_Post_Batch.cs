﻿#pragma warning disable 1591

namespace DebtPlus.LINQ
{
    using System.Reflection;

    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_ACH_Post_Batch", IsComposable = true)]
        public int xpr_ACH_Post_Batch([global::System.Data.Linq.Mapping.ParameterAttribute(Name = "ACHFile", DbType = "Int")] System.Nullable<int> aCHFile)
        {
            return ((int)(this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), aCHFile).ReturnValue));
        }
    }
}