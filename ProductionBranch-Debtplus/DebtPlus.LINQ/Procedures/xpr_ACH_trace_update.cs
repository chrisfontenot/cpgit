﻿#pragma warning disable 1591

namespace DebtPlus.LINQ
{
    using System.Data.Linq;
    using System.Reflection;

    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_ACH_trace_update")]
        public int xpr_ACH_trace_update([global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> ach_file, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> batch_number, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> trace_number)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), ach_file, batch_number, trace_number);
            return ((int)(result.ReturnValue));
        }
    }
}