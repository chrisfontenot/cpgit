﻿#pragma warning disable 1591

namespace DebtPlus.LINQ
{
    using System;
    using System.Data.Linq;
    using System.Reflection;

    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_insert_policy_matrix_policies")]
        private int private_xpr_insert_policy_matrix_policies([global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "VarChar(10)")] string creditor, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "VarChar(50)")] string policy_matrix_policy_type, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "VarChar(1024)")] string item_value, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "VarChar(1024)")] string message, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> use_sic)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), creditor, policy_matrix_policy_type, item_value, message, use_sic);
            return ((int)(result.ReturnValue));
        }

        /// <summary>
        /// Update the creditor policy matrix record(s)
        /// </summary>
        /// <param name="policyRecord">Pointer to the record with the values to be updated</param>
        /// <returns>Status for the update</returns>
        public int xpr_insert_policy_matrix_policies(DebtPlus.LINQ.policy_matrix_policy policyRecord)
        {
            return xpr_insert_policy_matrix_policies(policyRecord, false);
        }

        /// <summary>
        /// Update the creditor policy matrix record(s)
        /// </summary>
        /// <param name="policyRecord">Pointer to the record with the values to be updated</param>
        /// <param name="useSIC">TRUE if the change applies to all creditors of the creditor's SIC code</param>
        /// <returns>Status for the update</returns>
        public int xpr_insert_policy_matrix_policies(DebtPlus.LINQ.policy_matrix_policy policyRecord, Boolean useSIC)
        {
            return private_xpr_insert_policy_matrix_policies(policyRecord.creditor, policyRecord.policy_matrix_policy_type, policyRecord.item_value, policyRecord.message, new System.Nullable<Int32>(useSIC ? 1 : 0));
        }
    }
}