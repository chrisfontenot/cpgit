﻿namespace DebtPlus.LINQ
{
    using System.Reflection;

    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_housing_arm_v4_cleanup")]
        public int xpr_housing_arm_v4_cleanup()
        {
            return (int)(this.ExecuteMethodCall(this, (MethodInfo)(MethodInfo.GetCurrentMethod()))).ReturnValue;
        }
    }
}