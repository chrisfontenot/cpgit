﻿using System.Data.Linq;
using System.Linq;
using System.Reflection;

namespace DebtPlus.LINQ
{
    public partial class xpr_Creditor_StatisticsResult
    {
        private System.Nullable<int> _clients;
        private System.Nullable<int> _active_clients;
        private System.Nullable<int> _clients_balance;
        private System.Nullable<int> _zero_bal_clients;
        private System.Nullable<decimal> _total_disbursement;
        private System.Nullable<decimal> _total_balance;
        private System.Nullable<decimal> _avg_disbursement;

        public xpr_Creditor_StatisticsResult()
        {
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_clients", DbType = "Int")]
        public System.Nullable<int> clients
        {
            get
            {
                return this._clients;
            }
            set
            {
                if ((this._clients != value))
                {
                    this._clients = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_active_clients", DbType = "Int")]
        public System.Nullable<int> active_clients
        {
            get
            {
                return this._active_clients;
            }
            set
            {
                if ((this._active_clients != value))
                {
                    this._active_clients = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_clients_balance", DbType = "Int")]
        public System.Nullable<int> clients_balance
        {
            get
            {
                return this._clients_balance;
            }
            set
            {
                if ((this._clients_balance != value))
                {
                    this._clients_balance = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_zero_bal_clients", DbType = "Int")]
        public System.Nullable<int> zero_bal_clients
        {
            get
            {
                return this._zero_bal_clients;
            }
            set
            {
                if ((this._zero_bal_clients != value))
                {
                    this._zero_bal_clients = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_total_disbursement", DbType = "Money")]
        public System.Nullable<decimal> total_disbursement
        {
            get
            {
                return this._total_disbursement;
            }
            set
            {
                if ((this._total_disbursement != value))
                {
                    this._total_disbursement = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_total_balance", DbType = "Money")]
        public System.Nullable<decimal> total_balance
        {
            get
            {
                return this._total_balance;
            }
            set
            {
                if ((this._total_balance != value))
                {
                    this._total_balance = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_avg_disbursement", DbType = "Money")]
        public System.Nullable<decimal> avg_disbursement
        {
            get
            {
                return this._avg_disbursement;
            }
            set
            {
                if ((this._avg_disbursement != value))
                {
                    this._avg_disbursement = value;
                }
            }
        }
    }

    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_Creditor_Statistics")]
        private ISingleResult<xpr_Creditor_StatisticsResult> pvt_xpr_Creditor_Statistics([global::System.Data.Linq.Mapping.ParameterAttribute(Name = "Creditor", DbType = "VarChar(10)")] string creditor, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> force_calculation)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), creditor, force_calculation);
            return ((ISingleResult<xpr_Creditor_StatisticsResult>)(result.ReturnValue));
        }

        public xpr_Creditor_StatisticsResult xpr_Creditor_Statistics(string creditor, bool ForceCalculation)
        {
            ISingleResult<xpr_Creditor_StatisticsResult> result = pvt_xpr_Creditor_Statistics(creditor, ForceCalculation ? 1 : 0);
            if (result != null)
            {
                System.Collections.Generic.List<xpr_Creditor_StatisticsResult> lst = result.ToList<xpr_Creditor_StatisticsResult>();
                if (lst != null && lst.Count > 0)
                {
                    return lst[0];
                }
            }

            return null;
        }
    }
}