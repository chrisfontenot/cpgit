﻿using System;
using System.Data.Linq;
using System.Reflection;

namespace DebtPlus.LINQ
{
    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_client_reserved_in_trust_update")]
        public Int32 xpr_client_reserved_in_trust_update([global::System.Data.Linq.Mapping.ParameterAttribute(Name = "client", DbType = "int")] int client, [global::System.Data.Linq.Mapping.ParameterAttribute(Name = "amount", DbType = "money")] decimal amount, [global::System.Data.Linq.Mapping.ParameterAttribute(Name = "cutoff_date", DbType = "datetime NULL")] System.Nullable<DateTime> cutoff_date)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, (MethodInfo)(MethodInfo.GetCurrentMethod()), new object[] { client, amount, cutoff_date });
            return (int)result.ReturnValue;
        }
    }
}