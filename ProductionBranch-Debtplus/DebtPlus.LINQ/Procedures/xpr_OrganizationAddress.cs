﻿#pragma warning disable 1591

namespace DebtPlus.LINQ
{
    using System.Data.Linq;
    using System.Reflection;

    partial class DebtPlusDataContext
    {
        public partial class xpr_OrganizationAddressResult
        {
            private string _name;
            private string _addr1;
            private string _addr2;
            private string _addr3;
            private string _telephone;
            private string _zipcode;

            public xpr_OrganizationAddressResult()
            {
            }

            [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_name", DbType = "VarChar(256)")]
            public string name
            {
                get
                {
                    return this._name;
                }
                set
                {
                    if ((this._name != value))
                    {
                        this._name = value;
                    }
                }
            }

            [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_addr1", DbType = "VarChar(256)")]
            public string addr1
            {
                get
                {
                    return this._addr1;
                }
                set
                {
                    if ((this._addr1 != value))
                    {
                        this._addr1 = value;
                    }
                }
            }

            [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_addr2", DbType = "VarChar(256)")]
            public string addr2
            {
                get
                {
                    return this._addr2;
                }
                set
                {
                    if ((this._addr2 != value))
                    {
                        this._addr2 = value;
                    }
                }
            }

            [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_addr3", DbType = "VarChar(256)")]
            public string addr3
            {
                get
                {
                    return this._addr3;
                }
                set
                {
                    if ((this._addr3 != value))
                    {
                        this._addr3 = value;
                    }
                }
            }

            [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_telephone", DbType = "VarChar(256)")]
            public string telephone
            {
                get
                {
                    return this._telephone;
                }
                set
                {
                    if ((this._telephone != value))
                    {
                        this._telephone = value;
                    }
                }
            }

            [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_zipcode", DbType = "VarChar(10)")]
            public string zipcode
            {
                get
                {
                    return this._zipcode;
                }
                set
                {
                    if ((this._zipcode != value))
                    {
                        this._zipcode = value;
                    }
                }
            }
        }

        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_OrganizationAddress")]
        public ISingleResult<xpr_OrganizationAddressResult> xpr_OrganizationAddress()
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())));
            return ((ISingleResult<xpr_OrganizationAddressResult>)(result.ReturnValue));
        }
    }
}