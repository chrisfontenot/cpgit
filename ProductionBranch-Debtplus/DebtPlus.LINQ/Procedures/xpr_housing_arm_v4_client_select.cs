﻿namespace DebtPlus.LINQ
{
    using System;
    using System.Data.Linq;
    using System.Reflection;

    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_housing_arm_v4_client_select")]
        private int private_xpr_housing_arm_v4_client_select([global::System.Data.Linq.Mapping.ParameterAttribute(Name = "period_start", DbType = "DateTime")] DateTime periodStart, [global::System.Data.Linq.Mapping.ParameterAttribute(Name = "period_end", DbType = "DateTime")] DateTime periodEnd, [global::System.Data.Linq.Mapping.ParameterAttribute(Name = "hcs_id", DbType = "int")] System.Nullable<int> hcsID)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), periodStart, periodEnd, hcsID);
            return ((int)(result.ReturnValue));
        }

        /// <summary>
        /// perform the client selection operation on the HUD submission
        /// </summary>
        /// <param name="periodStart">Starting date for the selection</param>
        /// <param name="periodEnd">Ending date for the selection</param>
        /// <param name="hcsID">Agency ID for the selection</param>
        /// <returns></returns>
        public int xpr_housing_arm_v4_client_select(DateTime periodStart, DateTime periodEnd, int hcsID)
        {
            return private_xpr_housing_arm_v4_client_select(periodStart, periodEnd, hcsID);
        }

        /// <summary>
        /// perform the client selection operation on the HUD submission
        /// </summary>
        /// <param name="periodStart">Starting date for the selection</param>
        /// <param name="periodEnd">Ending date for the selection</param>
        /// <returns></returns>
        public int xpr_housing_arm_v4_client_select(DateTime periodStart, DateTime periodEnd)
        {
            return private_xpr_housing_arm_v4_client_select(periodStart, periodEnd, null);
        }
    }
}