﻿namespace DebtPlus.LINQ
{
    using System.Data.Linq;
    using System.Linq;
    using System.Reflection;

    public partial class xpr_debt_balanceResult
    {
        private decimal _orig_balance;
        private decimal _orig_balance_adjustment;
        private decimal _total_interest;
        private decimal _total_payments;

        public xpr_debt_balanceResult()
        {
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_orig_balance", DbType = "Money NOT NULL")]
        public decimal orig_balance
        {
            get
            {
                return this._orig_balance;
            }
            set
            {
                if ((this._orig_balance != value))
                {
                    this._orig_balance = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_orig_balance_adjustment", DbType = "Money NOT NULL")]
        public decimal orig_balance_adjustment
        {
            get
            {
                return this._orig_balance_adjustment;
            }
            set
            {
                if ((this._orig_balance_adjustment != value))
                {
                    this._orig_balance_adjustment = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_total_interest", DbType = "Money NOT NULL")]
        public decimal total_interest
        {
            get
            {
                return this._total_interest;
            }
            set
            {
                if ((this._total_interest != value))
                {
                    this._total_interest = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_total_payments", DbType = "Money NOT NULL")]
        public decimal total_payments
        {
            get
            {
                return this._total_payments;
            }
            set
            {
                if ((this._total_payments != value))
                {
                    this._total_payments = value;
                }
            }
        }
    }

    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_debt_balance")]
        private ISingleResult<xpr_debt_balanceResult> private_xpr_debt_balance([global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> client_creditor, [global::System.Data.Linq.Mapping.ParameterAttribute(Name = "Value", DbType = "Money")] System.Nullable<decimal> value)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), client_creditor, value);
            return (ISingleResult<xpr_debt_balanceResult>)(result.ReturnValue);
        }

        public xpr_debt_balanceResult xpr_debt_balance(System.Nullable<int> client_creditor, System.Nullable<decimal> value)
        {
            ISingleResult<xpr_debt_balanceResult> result = private_xpr_debt_balance(client_creditor, value);
            if (result != null)
            {
                return result.FirstOrDefault<xpr_debt_balanceResult>();
            }
            return null;
        }
    }
}