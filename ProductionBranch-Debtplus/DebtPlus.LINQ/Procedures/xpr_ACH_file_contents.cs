﻿#pragma warning disable 1591

namespace DebtPlus.LINQ
{
    using System.Data.Linq;
    using System.Linq;
    using System.Reflection;

    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_ACH_file_contents")]
        private ISingleResult<xpr_ACH_file_contentsResult> private_xpr_ACH_file_contents([global::System.Data.Linq.Mapping.ParameterAttribute(Name = "FileID", DbType = "Int")] System.Nullable<int> fileID)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), fileID);
            return ((ISingleResult<xpr_ACH_file_contentsResult>)(result.ReturnValue));
        }

        public System.Collections.Generic.List<xpr_ACH_file_contentsResult> xpr_ACH_file_contents(System.Nullable<int> fileID)
        {
            ISingleResult<xpr_ACH_file_contentsResult> result = private_xpr_ACH_file_contents(fileID);
            if (result != null)
            {
                return result.ToList();
            }
            return null;
        }
    }

    public partial class xpr_ACH_file_contentsResult
    {
        private int _item_key;
        private int _deposit_batch_id;
        private string _transaction_code;
        private System.Nullable<int> _client;
        private decimal _amount;
        private string _trace_number;
        private string _authentication_code;

        public xpr_ACH_file_contentsResult()
        {
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_item_key", DbType = "Int NOT NULL")]
        public int item_key
        {
            get
            {
                return this._item_key;
            }
            set
            {
                if ((this._item_key != value))
                {
                    this._item_key = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_deposit_batch_id", DbType = "Int NOT NULL")]
        public int deposit_batch_id
        {
            get
            {
                return this._deposit_batch_id;
            }
            set
            {
                if ((this._deposit_batch_id != value))
                {
                    this._deposit_batch_id = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_transaction_code", DbType = "VarChar(2)")]
        public string transaction_code
        {
            get
            {
                return this._transaction_code;
            }
            set
            {
                if ((this._transaction_code != value))
                {
                    this._transaction_code = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_client", DbType = "Int")]
        public System.Nullable<int> client
        {
            get
            {
                return this._client;
            }
            set
            {
                if ((this._client != value))
                {
                    this._client = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_amount", DbType = "Money NOT NULL")]
        public decimal amount
        {
            get
            {
                return this._amount;
            }
            set
            {
                if ((this._amount != value))
                {
                    this._amount = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_trace_number", DbType = "VarChar(50)")]
        public string trace_number
        {
            get
            {
                return this._trace_number;
            }
            set
            {
                if ((this._trace_number != value))
                {
                    this._trace_number = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_authentication_code", DbType = "VarChar(3)")]
        public string authentication_code
        {
            get
            {
                return this._authentication_code;
            }
            set
            {
                if ((this._authentication_code != value))
                {
                    this._authentication_code = value;
                }
            }
        }
    }
}