﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.1008
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DebtPlus.LINQ
{
    using System.Data.Linq;
    using System.Reflection;

    public partial class xpr_letter_fetchResult
    {
        private double _top_margin;
        private double _left_margin;
        private double _right_margin;
        private double _bottom_margin;
        private string _description;
        private string _filename;
        private int _language;
        private string _letter_code;
        private int _letter_type;
        private int _display_mode;
        private string _queue_name;

        public xpr_letter_fetchResult()
        {
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_top_margin", DbType = "Float NOT NULL")]
        public double top_margin
        {
            get
            {
                return this._top_margin;
            }
            set
            {
                if ((this._top_margin != value))
                {
                    this._top_margin = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_left_margin", DbType = "Float NOT NULL")]
        public double left_margin
        {
            get
            {
                return this._left_margin;
            }
            set
            {
                if ((this._left_margin != value))
                {
                    this._left_margin = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_right_margin", DbType = "Float NOT NULL")]
        public double right_margin
        {
            get
            {
                return this._right_margin;
            }
            set
            {
                if ((this._right_margin != value))
                {
                    this._right_margin = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_bottom_margin", DbType = "Float NOT NULL")]
        public double bottom_margin
        {
            get
            {
                return this._bottom_margin;
            }
            set
            {
                if ((this._bottom_margin != value))
                {
                    this._bottom_margin = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_description", DbType = "VarChar(256) NOT NULL", CanBeNull = false)]
        public string description
        {
            get
            {
                return this._description;
            }
            set
            {
                if ((this._description != value))
                {
                    this._description = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_filename", DbType = "VarChar(256) NOT NULL", CanBeNull = false)]
        public string filename
        {
            get
            {
                return this._filename;
            }
            set
            {
                if ((this._filename != value))
                {
                    this._filename = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_language", DbType = "Int NOT NULL")]
        public int language
        {
            get
            {
                return this._language;
            }
            set
            {
                if ((this._language != value))
                {
                    this._language = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_letter_code", DbType = "VarChar(10) NOT NULL", CanBeNull = false)]
        public string letter_code
        {
            get
            {
                return this._letter_code;
            }
            set
            {
                if ((this._letter_code != value))
                {
                    this._letter_code = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_letter_type", DbType = "Int NOT NULL")]
        public int letter_type
        {
            get
            {
                return this._letter_type;
            }
            set
            {
                if ((this._letter_type != value))
                {
                    this._letter_type = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_display_mode", DbType = "Int NOT NULL")]
        public int display_mode
        {
            get
            {
                return this._display_mode;
            }
            set
            {
                if ((this._display_mode != value))
                {
                    this._display_mode = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_queue_name", DbType = "VarChar(50) NOT NULL", CanBeNull = false)]
        public string queue_name
        {
            get
            {
                return this._queue_name;
            }
            set
            {
                if ((this._queue_name != value))
                {
                    this._queue_name = value;
                }
            }
        }
    }

    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_letter_fetch")]
        public ISingleResult<xpr_letter_fetchResult> xpr_letter_fetch([global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "VarChar(10)")] string letter_code, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> client, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> creditor_id, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "VarChar(10)")] string creditor, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> debt)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), letter_code, client, creditor_id, creditor, debt);
            return ((ISingleResult<xpr_letter_fetchResult>)(result.ReturnValue));
        }
    }
}