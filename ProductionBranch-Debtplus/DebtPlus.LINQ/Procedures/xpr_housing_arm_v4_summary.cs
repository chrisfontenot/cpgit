﻿namespace DebtPlus.LINQ
{
    using System;
    using System.Data.Linq;
    using System.Linq;
    using System.Reflection;

    public partial class xpr_housing_arm_v4_summaryResult
    {
        public xpr_housing_arm_v4_summaryResult()
        {
        }

        private Int32 _hcs_id = default(Int32);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_hcs_id", DbType = "Int NOT NULL")]
        public Int32 hcs_id
        {
            get
            {
                return this._hcs_id;
            }
            set
            {
                if ((this._hcs_id != value))
                {
                    this._hcs_id = value;
                }
            }
        }

        private String _tag = "";

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_tag", DbType = "varchar(256) NOT NULL")]
        public string tag
        {
            get
            {
                return this._tag;
            }
            set
            {
                if ((this._tag != value))
                {
                    this._tag = value;
                }
            }
        }

        private Int32 _hud_count = default(Int32);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_hud_count", DbType = "int NOT NULL")]
        public Int32 hud_count
        {
            get
            {
                return this._hud_count;
            }
            set
            {
                if ((this._hud_count != value))
                {
                    this._hud_count = value;
                }
            }
        }

        private Int32 _all_count = default(Int32);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_all_count", DbType = "int NOT NULL")]
        public Int32 all_count
        {
            get
            {
                return this._all_count;
            }
            set
            {
                if ((this._all_count != value))
                {
                    this._all_count = value;
                }
            }
        }
    }

    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_housing_arm_v4_summary")]
        private ISingleResult<xpr_housing_arm_v4_summaryResult> private_xpr_housing_arm_v4_summary()
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())));
            return (ISingleResult<xpr_housing_arm_v4_summaryResult>)(result.ReturnValue);
        }

        public System.Collections.Generic.List<xpr_housing_arm_v4_summaryResult> xpr_housing_arm_v4_summary()
        {
            ISingleResult<xpr_housing_arm_v4_summaryResult> result = private_xpr_housing_arm_v4_summary();
            if (result != null)
            {
                return result.ToList<xpr_housing_arm_v4_summaryResult>();
            }
            return null;
        }
    }
}