﻿namespace DebtPlus.LINQ
{
    using System.Data.Linq;
    using System.Linq;
    using System.Reflection;

    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_void_check")]
        private System.Int32 private_xpr_void_check([global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "int", Name = "trust_register")] int trustRegisterID, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "varchar(50)")] string message)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), trustRegisterID, message);
            return ((int)(result.ReturnValue));
        }

        public System.Int32 xpr_void_check(System.Int32 TrustRegisterID, string Message)
        {
            return private_xpr_void_check(TrustRegisterID, Message);
        }
    }
}