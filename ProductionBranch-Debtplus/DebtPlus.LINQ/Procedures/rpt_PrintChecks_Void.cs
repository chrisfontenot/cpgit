﻿using System;
using System.Data.Linq;
using System.Reflection;

namespace DebtPlus.LINQ
{
    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.rpt_PrintChecks_Void")]
        public Int32 rpt_PrintChecks_Void([global::System.Data.Linq.Mapping.ParameterAttribute(Name = "bank", DbType = "Int")] int BankId, [global::System.Data.Linq.Mapping.ParameterAttribute(Name = "marker")] string marker)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, (MethodInfo)(MethodInfo.GetCurrentMethod()), new object[] { BankId, marker });
            return (int)result.ReturnValue;
        }
    }
}