﻿#pragma warning disable 1591

namespace DebtPlus.LINQ
{
    using System;
    using System.Data.Linq;
    using System.Linq;
    using System.Reflection;

    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_housing_arm_v4_group_sessions")]
        private ISingleResult<xpr_housing_arm_v4_group_sessionsResult> private_xpr_housing_arm_v4_group_sessions()
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())));
            return ((ISingleResult<xpr_housing_arm_v4_group_sessionsResult>)(result.ReturnValue));
        }

        public System.Collections.Generic.List<xpr_housing_arm_v4_group_sessionsResult> xpr_housing_arm_v4_group_sessions()
        {
            ISingleResult<xpr_housing_arm_v4_group_sessionsResult> result = private_xpr_housing_arm_v4_group_sessions();
            if (result != null)
            {
                return result.ToList();
            }
            return null;
        }
    }

    public partial class xpr_housing_arm_v4_group_sessionsResult
    {
        private int _group_session_id;
        private int _group_session_counselor_id;
        private int _group_session_duration;
        private string _group_session_title;
        private DateTime? _group_session_date;
        private int? _hcs_id;
        private int? _group_session_type;
        private int? _group_session_attribute_hud_grant;
        private int? _workshop_type;

        public xpr_housing_arm_v4_group_sessionsResult()
        {
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_group_session_id", DbType = "Int NOT NULL")]
        public int group_session_id
        {
            get
            {
                return this._group_session_id;
            }
            set
            {
                if ((this._group_session_id != value))
                {
                    this._group_session_id = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_group_session_duration", DbType = "Int NOT NULL")]
        public int group_session_duration
        {
            get
            {
                return this._group_session_duration;
            }
            set
            {
                if ((this._group_session_duration != value))
                {
                    this._group_session_duration = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_group_session_date", DbType = "DateTime NULL")]
        public DateTime? group_session_date
        {
            get
            {
                return this._group_session_date;
            }
            set
            {
                if ((this._group_session_date != value))
                {
                    this._group_session_date = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_group_session_title", DbType = "varchar(80) NULL")]
        public string group_session_title
        {
            get
            {
                return this._group_session_title;
            }
            set
            {
                if ((this._group_session_title != value))
                {
                    this._group_session_title = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_group_session_counselor_id", DbType = "Int NOT NULL")]
        public int group_session_counselor_id
        {
            get
            {
                return this._group_session_counselor_id;
            }
            set
            {
                if ((this._group_session_counselor_id != value))
                {
                    this._group_session_counselor_id = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_group_session_type", DbType = "Int")]
        public int? group_session_type
        {
            get
            {
                return this._group_session_type;
            }
            set
            {
                if ((this._group_session_type != value))
                {
                    this._group_session_type = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_hcs_id", DbType = "Int")]
        public int? hcs_id
        {
            get
            {
                return this._hcs_id;
            }
            set
            {
                if ((this._hcs_id != value))
                {
                    this._hcs_id = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_group_session_attribute_hud_grant", DbType = "Int")]
        public int? group_session_attribute_hud_grant
        {
            get
            {
                return this._group_session_attribute_hud_grant;
            }
            set
            {
                if ((this._group_session_attribute_hud_grant != value))
                {
                    this._group_session_attribute_hud_grant = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_workshop_type", DbType = "Int")]
        public int? workshop_type
        {
            get
            {
                return this._workshop_type;
            }
            set
            {
                if ((this._workshop_type != value))
                {
                    this._workshop_type = value;
                }
            }
        }
    }
}