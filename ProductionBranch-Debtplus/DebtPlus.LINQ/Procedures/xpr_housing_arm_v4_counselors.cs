﻿namespace DebtPlus.LINQ
{
    using System.Data.Linq;
    using System.Linq;
    using System.Reflection;

    /// <summary>
    /// Results from the xpr_housing_arm_v4_counselors procedure call
    /// </summary>
    public partial class xpr_housing_arm_v4_counselorsResult
    {
        private System.Nullable<int> _cms_counselor_id;
        private string _cnslor_fname;
        private string _cnslor_lname;
        private string _cnslor_mname;
        private System.Nullable<System.DateTime> _cnslor_emp_start_date;
        private System.Nullable<System.DateTime> _cnslor_emp_end_date;
        private System.Nullable<decimal> _cnslor_rate;
        private string _cnslor_billing_method;
        private System.Nullable<int> _cnslor_HUD_id;
        private string _cnslor_SSN;
        private string _cnslor_phone;
        private string _cnslor_email;
        private System.Nullable<int> _training_ID;
        private System.Nullable<System.DateTime> _training_date;
        private System.Nullable<bool> _training_certificate;
        private string _training_title;
        private System.Nullable<int> _training_duration;
        private System.Nullable<int> _training_training_org;
        private string _training_training_org_other;
        private System.Nullable<int> _training_sponsor;
        private string _training_sponsor_other;

        public xpr_housing_arm_v4_counselorsResult()
        {
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_cms_counselor_id", DbType = "Int")]
        public System.Nullable<int> cms_counselor_id
        {
            get
            {
                return this._cms_counselor_id;
            }
            set
            {
                if ((this._cms_counselor_id != value))
                {
                    this._cms_counselor_id = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_cnslor_fname", DbType = "VarChar(80)")]
        public string cnslor_fname
        {
            get
            {
                return this._cnslor_fname;
            }
            set
            {
                if ((this._cnslor_fname != value))
                {
                    this._cnslor_fname = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_cnslor_lname", DbType = "VarChar(80)")]
        public string cnslor_lname
        {
            get
            {
                return this._cnslor_lname;
            }
            set
            {
                if ((this._cnslor_lname != value))
                {
                    this._cnslor_lname = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_cnslor_mname", DbType = "VarChar(80)")]
        public string cnslor_mname
        {
            get
            {
                return this._cnslor_mname;
            }
            set
            {
                if ((this._cnslor_mname != value))
                {
                    this._cnslor_mname = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_cnslor_emp_start_date", DbType = "DateTime")]
        public System.Nullable<System.DateTime> cnslor_emp_start_date
        {
            get
            {
                return this._cnslor_emp_start_date;
            }
            set
            {
                if ((this._cnslor_emp_start_date != value))
                {
                    this._cnslor_emp_start_date = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_cnslor_emp_end_date", DbType = "DateTime")]
        public System.Nullable<System.DateTime> cnslor_emp_end_date
        {
            get
            {
                return this._cnslor_emp_end_date;
            }
            set
            {
                if ((this._cnslor_emp_end_date != value))
                {
                    this._cnslor_emp_end_date = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_cnslor_rate", DbType = "Decimal(18,0)")]
        public System.Nullable<decimal> cnslor_rate
        {
            get
            {
                return this._cnslor_rate;
            }
            set
            {
                if ((this._cnslor_rate != value))
                {
                    this._cnslor_rate = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_cnslor_billing_method", DbType = "VarChar(20)")]
        public string cnslor_billing_method
        {
            get
            {
                return this._cnslor_billing_method;
            }
            set
            {
                if ((this._cnslor_billing_method != value))
                {
                    this._cnslor_billing_method = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_cnslor_HUD_id", DbType = "Int")]
        public System.Nullable<int> cnslor_HUD_id
        {
            get
            {
                return this._cnslor_HUD_id;
            }
            set
            {
                if ((this._cnslor_HUD_id != value))
                {
                    this._cnslor_HUD_id = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_cnslor_SSN", DbType = "VarChar(20)")]
        public string cnslor_SSN
        {
            get
            {
                return this._cnslor_SSN;
            }
            set
            {
                if ((this._cnslor_SSN != value))
                {
                    this._cnslor_SSN = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_cnslor_phone", DbType = "VarChar(20)")]
        public string cnslor_phone
        {
            get
            {
                return this._cnslor_phone;
            }
            set
            {
                if ((this._cnslor_phone != value))
                {
                    this._cnslor_phone = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_cnslor_email", DbType = "VarChar(80)")]
        public string cnslor_email
        {
            get
            {
                return this._cnslor_email;
            }
            set
            {
                if ((this._cnslor_email != value))
                {
                    this._cnslor_email = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_training_ID", DbType = "Int")]
        public System.Nullable<int> training_ID
        {
            get
            {
                return this._training_ID;
            }
            set
            {
                if ((this._training_ID != value))
                {
                    this._training_ID = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_training_date", DbType = "DateTime")]
        public System.Nullable<System.DateTime> training_date
        {
            get
            {
                return this._training_date;
            }
            set
            {
                if ((this._training_date != value))
                {
                    this._training_date = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_training_certificate", DbType = "Bit")]
        public System.Nullable<bool> training_certificate
        {
            get
            {
                return this._training_certificate;
            }
            set
            {
                if ((this._training_certificate != value))
                {
                    this._training_certificate = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_training_title", DbType = "VarChar(50)")]
        public string training_title
        {
            get
            {
                return this._training_title;
            }
            set
            {
                if ((this._training_title != value))
                {
                    this._training_title = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_training_duration", DbType = "Int")]
        public System.Nullable<int> training_duration
        {
            get
            {
                return this._training_duration;
            }
            set
            {
                if ((this._training_duration != value))
                {
                    this._training_duration = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_training_training_org", DbType = "Int")]
        public System.Nullable<int> training_training_org
        {
            get
            {
                return this._training_training_org;
            }
            set
            {
                if ((this._training_training_org != value))
                {
                    this._training_training_org = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_training_training_org_other", DbType = "VarChar(50)")]
        public string training_training_org_other
        {
            get
            {
                return this._training_training_org_other;
            }
            set
            {
                if ((this._training_training_org_other != value))
                {
                    this._training_training_org_other = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_training_sponsor", DbType = "Int")]
        public System.Nullable<int> training_sponsor
        {
            get
            {
                return this._training_sponsor;
            }
            set
            {
                if ((this._training_sponsor != value))
                {
                    this._training_sponsor = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_training_sponsor_other", DbType = "VarChar(50)")]
        public string training_sponsor_other
        {
            get
            {
                return this._training_sponsor_other;
            }
            set
            {
                if ((this._training_sponsor_other != value))
                {
                    this._training_sponsor_other = value;
                }
            }
        }
    }

    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_housing_arm_v4_counselors")]
        private ISingleResult<xpr_housing_arm_v4_counselorsResult> private_xpr_housing_arm_v4_counselors()
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())));
            return (ISingleResult<xpr_housing_arm_v4_counselorsResult>)(result.ReturnValue);
        }

        public System.Collections.Generic.List<xpr_housing_arm_v4_counselorsResult> xpr_housing_arm_v4_counselors()
        {
            ISingleResult<xpr_housing_arm_v4_counselorsResult> result = private_xpr_housing_arm_v4_counselors();
            if (result != null)
            {
                return result.ToList<xpr_housing_arm_v4_counselorsResult>();
            }
            return null;
        }
    }
}