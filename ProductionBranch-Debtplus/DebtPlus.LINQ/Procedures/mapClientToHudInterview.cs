﻿using System;
using System.Linq;

namespace DebtPlus.LINQ
{
    public partial class Procedures
    {
        /// <summary>
        /// Map the client to the latest HUD interview record for that client
        /// </summary>
        /// <returns>NULL if the record could not be located</returns>
        public static hud_interview MapClientToInterview(BusinessContext bc, Int32? clientID)
        {
            return MapClientToInterview(bc, clientID, null);
        }

        /// <summary>
        /// Map the client to the latest HUD interview record for that client
        /// </summary>
        /// <returns>NULL if the record could not be located</returns>
        public static hud_interview MapClientToInterview(BusinessContext bc, Int32? clientID, Housing_PurposeOfVisitType povType)
        {
            // There must be a client to have a record
            if (!clientID.HasValue)
            {
                return null;
            }

            // If no type is entered then just take the first record
            if (povType == null)
            {
                return bc.hud_interviews.Where(hi => hi.client == clientID).OrderByDescending(hi => hi.interview_date).Take(1).FirstOrDefault();
            }

            // Look for that specific type of record
            return bc.hud_interviews.Where(hi => hi.client == clientID && hi.interview_type == povType.Id).OrderByDescending(hi => hi.interview_date).Take(1).FirstOrDefault();
        }

        /// <summary>
        /// Map the client to the open HUD case
        /// </summary>
        /// <returns>NULL if the record could not be located</returns>
        public static hud_interview MapClientToOpenInterview(BusinessContext bc, Int32? clientID)
        {
            // There must be a client to have a record
            if (!clientID.HasValue)
            {
                return null;
            }
            return bc.hud_interviews.Where(hi => hi.client == clientID && hi.hud_result == null && hi.termination_reason == null).OrderByDescending(hi => hi.interview_date).Take(1).FirstOrDefault();
        }
    }

    public partial class Procedures
    {
        /// <summary>
        /// Find the POV type record that corresponds to the Mortgage Delinquency Prevention case type
        /// </summary>
        public static Housing_PurposeOfVisitType getMortgageInterviewType()
        {
            // Retrieve the mortgage POV type
            return DebtPlus.LINQ.Cache.Housing_PurposeOfVisitType.getList()
                .Find(s => s.hud_9902_section == "Resolving or Preventing Mortgage Delinquency");
        }
    }
}