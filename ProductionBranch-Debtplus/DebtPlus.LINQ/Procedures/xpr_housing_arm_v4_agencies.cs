﻿namespace DebtPlus.LINQ
{
    using System;
    using System.Data.Linq;
    using System.Linq;
    using System.Reflection;

    public partial class xpr_housing_arm_v4_agenciesResult
    {
        public xpr_housing_arm_v4_agenciesResult()
        {
        }

        private Int32 _office = default(Int32);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_office", DbType = "int NOT NULL")]
        public Int32 office
        {
            get
            {
                return this._office;
            }
            set
            {
                if ((this._office != value))
                {
                    this._office = value;
                }
            }
        }

        private string _agc_name = default(string);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_agc_name", DbType = "varchar(50) NOT NULL")]
        public string agc_name
        {
            get
            {
                return this._agc_name;
            }
            set
            {
                if ((this._agc_name != value))
                {
                    this._agc_name = value;
                }
            }
        }

        private Int32 _agc_cms_type = default(Int32);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_agc_cms_type", DbType = "int NOT NULL")]
        public Int32 agc_cms_type
        {
            get
            {
                return this._agc_cms_type;
            }
            set
            {
                if ((this._agc_cms_type != value))
                {
                    this._agc_cms_type = value;
                }
            }
        }

        private string _agc_ein = default(string);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_agc_ein", DbType = "varchar(50) NOT NULL")]
        public string agc_ein
        {
            get
            {
                return this._agc_ein;
            }
            set
            {
                if ((this._agc_ein != value))
                {
                    this._agc_ein = value;
                }
            }
        }

        private string _agc_dun_nbr = default(string);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_agc_dun_nbr", DbType = "varchar(50) NOT NULL")]
        public string agc_dun_nbr
        {
            get
            {
                return this._agc_dun_nbr;
            }
            set
            {
                if ((this._agc_dun_nbr != value))
                {
                    this._agc_dun_nbr = value;
                }
            }
        }

        private string _agc_address1 = default(string);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_agc_address1", DbType = "varchar(50) NOT NULL")]
        public string agc_address1
        {
            get
            {
                return this._agc_address1;
            }
            set
            {
                if ((this._agc_address1 != value))
                {
                    this._agc_address1 = value;
                }
            }
        }

        private string _agc_address2 = default(string);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_agc_address2", DbType = "varchar(50) NOT NULL")]
        public string agc_address2
        {
            get
            {
                return this._agc_address2;
            }
            set
            {
                if ((this._agc_address2 != value))
                {
                    this._agc_address2 = value;
                }
            }
        }

        private string _agc_address3 = default(string);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_agc_address3", DbType = "varchar(50) NOT NULL")]
        public string agc_address3
        {
            get
            {
                return this._agc_address3;
            }
            set
            {
                if ((this._agc_address3 != value))
                {
                    this._agc_address3 = value;
                }
            }
        }

        private string _agc_address4 = default(string);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_agc_address4", DbType = "varchar(50) NOT NULL")]
        public string agc_address4
        {
            get
            {
                return this._agc_address4;
            }
            set
            {
                if ((this._agc_address4 != value))
                {
                    this._agc_address4 = value;
                }
            }
        }

        private string _agc_city = default(string);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_agc_city", DbType = "varchar(50) NOT NULL")]
        public string agc_city
        {
            get
            {
                return this._agc_city;
            }
            set
            {
                if ((this._agc_city != value))
                {
                    this._agc_city = value;
                }
            }
        }

        private string _agc_state = default(string);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_agc_state", DbType = "varchar(50) NOT NULL")]
        public string agc_state
        {
            get
            {
                return this._agc_state;
            }
            set
            {
                if ((this._agc_state != value))
                {
                    this._agc_state = value;
                }
            }
        }

        private string _agc_zip = default(string);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_agc_zip", DbType = "varchar(50) NOT NULL")]
        public string agc_zip
        {
            get
            {
                return this._agc_zip;
            }
            set
            {
                if ((this._agc_zip != value))
                {
                    this._agc_zip = value;
                }
            }
        }

        private string _agc_web_site = default(string);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_agc_web_site", DbType = "varchar(50) NOT NULL")]
        public string agc_web_site
        {
            get
            {
                return this._agc_web_site;
            }
            set
            {
                if ((this._agc_web_site != value))
                {
                    this._agc_web_site = value;
                }
            }
        }

        private string _agc_phone_nbr = default(string);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_agc_phone_nbr", DbType = "varchar(50) NOT NULL")]
        public string agc_phone_nbr
        {
            get
            {
                return this._agc_phone_nbr;
            }
            set
            {
                if ((this._agc_phone_nbr != value))
                {
                    this._agc_phone_nbr = value;
                }
            }
        }

        private string _alternate_phone_nbr = default(string);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_alternate_phone_nbr", DbType = "varchar(50) NOT NULL")]
        public string alternate_phone_nbr
        {
            get
            {
                return this._alternate_phone_nbr;
            }
            set
            {
                if ((this._alternate_phone_nbr != value))
                {
                    this._alternate_phone_nbr = value;
                }
            }
        }

        private string _agc_fax_nbr = default(string);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_agc_fax_nbr", DbType = "varchar(50) NOT NULL")]
        public string agc_fax_nbr
        {
            get
            {
                return this._agc_fax_nbr;
            }
            set
            {
                if ((this._agc_fax_nbr != value))
                {
                    this._agc_fax_nbr = value;
                }
            }
        }

        private string _agc_email = default(string);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_agc_email", DbType = "varchar(50) NOT NULL")]
        public string agc_email
        {
            get
            {
                return this._agc_email;
            }
            set
            {
                if ((this._agc_email != value))
                {
                    this._agc_email = value;
                }
            }
        }

        private Boolean _agc_faith_based_ind = default(Boolean);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_agc_faith_based_ind", DbType = "bit NOT NULL")]
        public Boolean agc_faith_based_ind
        {
            get
            {
                return this._agc_faith_based_ind;
            }
            set
            {
                if ((this._agc_faith_based_ind != value))
                {
                    this._agc_faith_based_ind = value;
                }
            }
        }

        private Boolean _agc_colonias_ind = default(Boolean);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_agc_colonias_ind", DbType = "bit NOT NULL")]
        public Boolean agc_colonias_ind
        {
            get
            {
                return this._agc_colonias_ind;
            }
            set
            {
                if ((this._agc_colonias_ind != value))
                {
                    this._agc_colonias_ind = value;
                }
            }
        }

        private Boolean _agc_migrfarm_worker_ind = default(Boolean);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_agc_migrfarm_worker_ind", DbType = "bit NOT NULL")]
        public Boolean agc_migrfarm_worker_ind
        {
            get
            {
                return this._agc_migrfarm_worker_ind;
            }
            set
            {
                if ((this._agc_migrfarm_worker_ind != value))
                {
                    this._agc_migrfarm_worker_ind = value;
                }
            }
        }

        private decimal _agc_counselin_budget_amt = default(decimal);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_agc_counselin_budget_amt", DbType = "money NOT NULL")]
        public decimal agc_counselin_budget_amt
        {
            get
            {
                return this._agc_counselin_budget_amt;
            }
            set
            {
                if ((this._agc_counselin_budget_amt != value))
                {
                    this._agc_counselin_budget_amt = value;
                }
            }
        }

        private Boolean _Send9902 = default(Boolean);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Send9902", DbType = "bit NOT NULL")]
        public Boolean Send9902
        {
            get
            {
                return this._Send9902;
            }
            set
            {
                if ((this._Send9902 != value))
                {
                    this._Send9902 = value;
                }
            }
        }

        private Boolean _SendCounselors = default(Boolean);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_SendCounselors", DbType = "bit NOT NULL")]
        public Boolean SendCounselors
        {
            get
            {
                return this._SendCounselors;
            }
            set
            {
                if ((this._SendCounselors != value))
                {
                    this._SendCounselors = value;
                }
            }
        }

        private Boolean _SendClients = default(Boolean);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_SendClients", DbType = "bit NOT NULL")]
        public Boolean SendClients
        {
            get
            {
                return this._SendClients;
            }
            set
            {
                if ((this._SendClients != value))
                {
                    this._SendClients = value;
                }
            }
        }

        private Boolean _SendAttendees = default(Boolean);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_SendAttendees", DbType = "bit NOT NULL")]
        public Boolean SendAttendees
        {
            get
            {
                return this._SendAttendees;
            }
            set
            {
                if ((this._SendAttendees != value))
                {
                    this._SendAttendees = value;
                }
            }
        }

        private Boolean _SendClientSSN = default(Boolean);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_SendClientSSN", DbType = "bit NOT NULL")]
        public Boolean SendClientSSN
        {
            get
            {
                return this._SendClientSSN;
            }
            set
            {
                if ((this._SendClientSSN != value))
                {
                    this._SendClientSSN = value;
                }
            }
        }

        private Boolean _SendClientCount = default(Boolean);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_SendClientCount", DbType = "bit NOT NULL")]
        public Boolean SendClientCount
        {
            get
            {
                return this._SendClientCount;
            }
            set
            {
                if ((this._SendClientCount != value))
                {
                    this._SendClientCount = value;
                }
            }
        }

        private string _UserName = default(string);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_UserName", DbType = "varchar(50) NOT NULL")]
        public string UserName
        {
            get
            {
                return this._UserName;
            }
            set
            {
                if ((this._UserName != value))
                {
                    this._UserName = value;
                }
            }
        }

        private string _Password = default(string);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Password", DbType = "varchar(50) NOT NULL")]
        public string Password
        {
            get
            {
                return this._Password;
            }
            set
            {
                if ((this._Password != value))
                {
                    this._Password = value;
                }
            }
        }

        private string _Description = default(string);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Description", DbType = "varchar(50) NOT NULL")]
        public string Description
        {
            get
            {
                return this._Description;
            }
            set
            {
                if ((this._Description != value))
                {
                    this._Description = value;
                }
            }
        }

        private Int32 _agc_hcs_id = default(Int32);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_agc_hcs_id", DbType = "int NOT NULL")]
        public Int32 agc_hcs_id
        {
            get
            {
                return this._agc_hcs_id;
            }
            set
            {
                if ((this._agc_hcs_id != value))
                {
                    this._agc_hcs_id = value;
                }
            }
        }

        private Boolean _Default = default(Boolean);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Default", DbType = "bit NOT NULL")]
        public Boolean Default
        {
            get
            {
                return this._Default;
            }
            set
            {
                if ((this._Default != value))
                {
                    this._Default = value;
                }
            }
        }

        private Boolean _ActiveFlag = default(Boolean);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_ActiveFlag", DbType = "bit NOT NULL")]
        public Boolean ActiveFlag
        {
            get
            {
                return this._ActiveFlag;
            }
            set
            {
                if ((this._ActiveFlag != value))
                {
                    this._ActiveFlag = value;
                }
            }
        }
    }

    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_housing_arm_v4_agencies")]
        private ISingleResult<xpr_housing_arm_v4_agenciesResult> private_xpr_housing_arm_v4_agencies()
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())));
            return (ISingleResult<xpr_housing_arm_v4_agenciesResult>)(result.ReturnValue);
        }

        public xpr_housing_arm_v4_agenciesResult xpr_housing_arm_v4_agencies()
        {
            ISingleResult<xpr_housing_arm_v4_agenciesResult> result = private_xpr_housing_arm_v4_agencies();
            if (result != null)
            {
                return result.FirstOrDefault<xpr_housing_arm_v4_agenciesResult>();
            }
            return null;
        }
    }
}