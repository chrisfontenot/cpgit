﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.1008
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DebtPlus.LINQ
{
    using System;
    using System.Data.Linq;
    using System.Reflection;

    partial class DebtPlusDataContext
    {
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.xpr_create_client_fhlb")]
        private int private_xpr_create_client_fhlb([global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> client, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "VarChar(22)")] string account_number, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "VarChar(10)")] string creditor, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Money")] System.Nullable<decimal> amount)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), client, account_number, creditor, amount);
			return ((int)(result.ReturnValue));
		}

        /// <summary>
        /// Create a new client for the system
        /// </summary>
        /// <param name="clientID">IF of the client that was created</param>
        /// <returns>The ID of the client record. It is left to the caller to retrieve the client record if desired.</returns>
        public int xpr_create_client_fhlb(Int32 clientID)
        {
            return private_xpr_create_client_fhlb(clientID, null, null, null);
        }

        /// <summary>
        /// Create a new client for the system
        /// </summary>
        /// <param name="clientID">IF of the client that was created</param>
        /// <param name="account_number">Account number for the debt</param>
        /// <param name="creditor">Creditor to be used for the debt</param>
        /// <returns></returns>
        public int xpr_create_client_fhlb(Int32 clientID, string account_number, string creditor)
        {
            return private_xpr_create_client_fhlb(clientID, account_number, creditor, null);
        }

        /// <summary>
        /// Create a new client for the system
        /// </summary>
        /// <param name="clientID">IF of the client that was created</param>
        /// <param name="account_number">Account number for the debt</param>
        /// <returns></returns>
        public int xpr_create_client_fhlb(Int32 clientID, string account_number)
        {
            return private_xpr_create_client_fhlb(clientID, account_number, null, null);
        }

        /// <summary>
        /// Create a new client for the system
        /// </summary>
        /// <param name="clientID">IF of the client that was created</param>
        /// <param name="account_number">Account number for the debt</param>
        /// <param name="creditor">Creditor to be used for the debt</param>
        /// <param name="amount">Starting account balance</param>
        /// <returns></returns>
        public int xpr_create_client_fhlb(Int32 clientID, string account_number, string creditor, System.Nullable<decimal> amount)
        {
            return private_xpr_create_client_fhlb(clientID, account_number, creditor, amount);
        }

        /// <summary>
        /// Create a new client for the system
        /// </summary>
        /// <param name="clientID">IF of the client that was created</param>
        /// <param name="account_number">Account number for the debt</param>
        /// <param name="amount">Starting account balance</param>
        /// <returns></returns>
        public int xpr_create_client_fhlb(Int32 clientID, string account_number, System.Nullable<decimal> amount)
        {
            return private_xpr_create_client_fhlb(clientID, account_number, null, amount);
        }
    }
}

#pragma warning restore 1591