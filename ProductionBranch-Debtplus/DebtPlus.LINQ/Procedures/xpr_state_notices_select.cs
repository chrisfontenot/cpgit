﻿#pragma warning disable 1591

using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Reflection;

namespace DebtPlus.LINQ
{
    public class xpr_StateNotices_SelectResult
    {
        public Int32 oId;
        public Int32 state;
        public Int32 StateMessageType;
        public DateTime? effective;
        public string reason;
        public DateTime? date_updated;
        public string updated_by;
        public DateTime date_created;
        public string created_by;
        public string Details;
    }

    partial class DebtPlusDataContext
    {
        [Function(Name = "dbo.xpr_StateNotices_Select")]
        public ISingleResult<xpr_StateNotices_SelectResult> xpr_StateNotices_Select([Parameter(DbType = "int")] Int32 state, [Parameter(DbType = "datetime")] System.Nullable<DateTime> Effective)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), new object[] { state, Effective });
            return ((ISingleResult<xpr_StateNotices_SelectResult>)(result.ReturnValue));
        }
    }
}