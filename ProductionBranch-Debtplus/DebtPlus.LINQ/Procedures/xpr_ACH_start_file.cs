﻿#pragma warning disable 1591

namespace DebtPlus.LINQ
{
    using System.Data.Linq;
    using System.Reflection;

    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_ACH_start_file")]
        public int xpr_ACH_start_file([global::System.Data.Linq.Mapping.ParameterAttribute(Name = "PullDate", DbType = "DateTime")] System.Nullable<System.DateTime> pullDate, [global::System.Data.Linq.Mapping.ParameterAttribute(Name = "FileName", DbType = "VarChar(80)")] string fileName, [global::System.Data.Linq.Mapping.ParameterAttribute(Name = "EffectiveDate", DbType = "DateTime")] System.Nullable<System.DateTime> effectiveDate, [global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> bank, [global::System.Data.Linq.Mapping.ParameterAttribute(Name = "Note", DbType = "VarChar(50)")] string note)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), pullDate, fileName, effectiveDate, bank, note);
            return ((int)(result.ReturnValue));
        }
    }
}