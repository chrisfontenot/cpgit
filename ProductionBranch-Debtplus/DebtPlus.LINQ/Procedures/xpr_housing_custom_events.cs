﻿#pragma warning disable 1591

namespace DebtPlus.LINQ
{
    using System.Data.Linq;
    using System.Reflection;

    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_housing_custom_events")]
        private int private_xpr_housing_custom_events([global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int NOT NULL")] int hud_interview)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, (MethodInfo)(MethodInfo.GetCurrentMethod()), hud_interview);
            return ((int)(result.ReturnValue));
        }

        /// <summary>
        /// Process the new housing Purpose Of Visit transaction
        /// </summary>
        public int xpr_housing_custom_events(hud_interview record)
        {
            System.Diagnostics.Debug.Assert(record != null);
            System.Diagnostics.Debug.Assert(record.Id > 0);

            try
            {
                return private_xpr_housing_custom_events(record.Id);
            }

            // Handle the "missing stored procedure" error
            catch (System.Data.SqlClient.SqlException ex)
            {
                if (ex.Number != 2812 || !ex.Message.Contains("Could not find stored procedure"))
                {
                    throw;
                }
            }

            // Return the "did not execute" status code
            return 0;
        }
    }
}