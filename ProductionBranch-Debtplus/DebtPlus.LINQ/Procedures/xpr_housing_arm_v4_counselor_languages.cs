﻿namespace DebtPlus.LINQ
{
    using System;
    using System.Data.Linq;
    using System.Linq;
    using System.Reflection;

    public partial class xpr_housing_arm_v4_counselor_languagesResult
    {
        public xpr_housing_arm_v4_counselor_languagesResult()
        {
        }

        private Int32 _cms_counselor_id = default(Int32);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_cms_counselor_id", DbType = "Int NOT NULL")]
        public Int32 cms_counselor_id
        {
            get
            {
                return this._cms_counselor_id;
            }
            set
            {
                if ((this._cms_counselor_id != value))
                {
                    this._cms_counselor_id = value;
                }
            }
        }

        private Int32 _language = default(Int32);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_language", DbType = "int NOT NULL")]
        public Int32 language
        {
            get
            {
                return this._language;
            }
            set
            {
                if ((this._language != value))
                {
                    this._language = value;
                }
            }
        }

        private Int32 _InputLanguage = default(Int32);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_InputLanguage", DbType = "int NOT NULL")]
        public Int32 InputLanguage
        {
            get
            {
                return this._InputLanguage;
            }
            set
            {
                if ((this._InputLanguage != value))
                {
                    this._InputLanguage = value;
                }
            }
        }

        private Int32 _Language = default(Int32);

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_Language", DbType = "int NOT NULL")]
        public Int32 Language
        {
            get
            {
                return this._Language;
            }
            set
            {
                if ((this._Language != value))
                {
                    this._Language = value;
                }
            }
        }
    }

    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.xpr_housing_arm_v4_counselor_languages")]
        private ISingleResult<xpr_housing_arm_v4_counselor_languagesResult> private_xpr_housing_arm_v4_counselor_languages()
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())));
            return (ISingleResult<xpr_housing_arm_v4_counselor_languagesResult>)(result.ReturnValue);
        }

        public System.Collections.Generic.List<xpr_housing_arm_v4_counselor_languagesResult> xpr_housing_arm_v4_counselor_languages()
        {
            ISingleResult<xpr_housing_arm_v4_counselor_languagesResult> result = private_xpr_housing_arm_v4_counselor_languages();
            if (result != null)
            {
                return result.ToList<xpr_housing_arm_v4_counselor_languagesResult>();
            }
            return null;
        }
    }
}