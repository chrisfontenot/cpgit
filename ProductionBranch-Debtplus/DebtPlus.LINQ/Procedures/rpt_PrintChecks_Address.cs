﻿using System;
using System.Data.Linq;
using System.Linq;
using System.Reflection;

namespace DebtPlus.LINQ
{
    public partial class rpt_PrintChecks_AddressResult
    {
        private System.Nullable<int> _trust_register;
        private string _attn;
        private string _addr_1;
        private string _addr_2;
        private string _addr_3;
        private string _addr_4;
        private string _addr_5;
        private string _zipcode;

        public rpt_PrintChecks_AddressResult()
        {
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_attn", DbType = "VarChar(80)")]
        public string attn
        {
            get
            {
                return this._attn;
            }
            set
            {
                if ((this._attn != value))
                {
                    this._attn = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_addr_1", DbType = "VarChar(256)")]
        public string addr_1
        {
            get
            {
                return this._addr_1;
            }
            set
            {
                if ((this._addr_1 != value))
                {
                    this._addr_1 = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_addr_2", DbType = "VarChar(256)")]
        public string addr_2
        {
            get
            {
                return this._addr_2;
            }
            set
            {
                if ((this._addr_2 != value))
                {
                    this._addr_2 = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_addr_3", DbType = "VarChar(256)")]
        public string addr_3
        {
            get
            {
                return this._addr_3;
            }
            set
            {
                if ((this._addr_3 != value))
                {
                    this._addr_3 = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_addr_4", DbType = "VarChar(256)")]
        public string addr_4
        {
            get
            {
                return this._addr_4;
            }
            set
            {
                if ((this._addr_4 != value))
                {
                    this._addr_4 = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_addr_5", DbType = "VarChar(256)")]
        public string addr_5
        {
            get
            {
                return this._addr_5;
            }
            set
            {
                if ((this._addr_5 != value))
                {
                    this._addr_5 = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_zipcode", DbType = "VarChar(80)")]
        public string zipcode
        {
            get
            {
                return this._zipcode;
            }
            set
            {
                if ((this._zipcode != value))
                {
                    this._zipcode = value;
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_trust_register", DbType = "Int")]
        public System.Nullable<int> trust_register
        {
            get
            {
                return this._trust_register;
            }
            set
            {
                if ((this._trust_register != value))
                {
                    this._trust_register = value;
                }
            }
        }

        public override string ToString()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            if (!string.IsNullOrEmpty(attn))
            {
                sb.AppendFormat("{0}ATTN: {1}", Environment.NewLine, attn);
            }

            foreach (string adr in new string[] { addr_1, addr_2, addr_3, addr_4, addr_5 })
            {
                if (!string.IsNullOrEmpty(adr))
                {
                    sb.AppendFormat("{0}{1}", Environment.NewLine, adr);
                }
            }

            if (sb.Length > 0)
            {
                sb.Remove(0, 2);
            }
            return sb.ToString();
        }
    }

    partial class DebtPlusDataContext
    {
        [global::System.Data.Linq.Mapping.FunctionAttribute(Name = "dbo.rpt_PrintChecks_Address")]
        private ISingleResult<rpt_PrintChecks_AddressResult> pvt_rpt_PrintChecks_Address([global::System.Data.Linq.Mapping.ParameterAttribute(DbType = "Int")] System.Nullable<int> trust_register)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), trust_register);
            return (ISingleResult<rpt_PrintChecks_AddressResult>)(result.ReturnValue);
        }

        public rpt_PrintChecks_AddressResult rpt_PrintChecks_Address(Int32 trust_register)
        {
            ISingleResult<rpt_PrintChecks_AddressResult> result = pvt_rpt_PrintChecks_Address(trust_register);
            if (result != null)
            {
                return result.FirstOrDefault();
            }
            return null;
        }
    }
}