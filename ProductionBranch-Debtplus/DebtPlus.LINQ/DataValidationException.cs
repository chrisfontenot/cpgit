﻿namespace DebtPlus.LINQ
{
    public sealed class DataValidationException : System.Exception
    {
        public DataValidationException()
            : base()
        { }

        public DataValidationException(string ErrorMessage)
            : base(ErrorMessage)
        {
        }

        public DataValidationException(string ErrorMessage, System.Exception innerException)
            : base(ErrorMessage, innerException)
        {
        }

        public DataValidationException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        {
        }
    }
}
