namespace DebtPlus.LINQ
{
    partial class DebtPlusDataContext
    {
        /// <summary>
        /// Return the connection string associated with the database
        /// </summary>
        /// <returns></returns>
        public static string ConnectionString()
        {
            return SQLInfoClass.getDefault().ConnectionString;
        }

        /// <summary>
        /// Create a specific telephone number in the table. The ID is assumed to not exist or an error will be returned.
        /// </summary>
        /// <param name="Id">The primary key to the table</param>
        /// <returns></returns>
        public System.Int32 CreateSpecificTelephoneNumber(System.Int32 Id)
        {
            System.String stmt = string.Format("SET IDENTITY_INSERT [TelephoneNumbers] ON; INSERT INTO [TelephoneNumbers]([TelephoneNumber]) VALUES ({0:f0}); SET IDENTITY_INSERT [TelephoneNumbers] OFF;", Id);
            ExecuteCommand(stmt);

            // Return the ID of the telephone number record
            return Id;
        }

        /// <summary>
        /// Create a specific address in the table. The ID is assumed to not exist or an error will be returned.
        /// </summary>
        /// <param name="Id">The primary key to the table</param>
        /// <returns></returns>
        public System.Int32 CreateSpecificAddress(System.Int32 Id)
        {
            System.String stmt = string.Format("SET IDENTITY_INSERT [Addresses] ON; INSERT INTO [Addresses]([Address]) VALUES ({0:f0}); SET IDENTITY_INSERT [Addresses] OFF;", Id);
            ExecuteCommand(stmt);

            // Return the ID of the address number record
            return Id;
        }

        /// <summary>
        /// Create a specific E-Mail address in the table. The ID is assumed to not exist or an error will be returned.
        /// </summary>
        /// <param name="Id">The primary key to the table</param>
        /// <returns></returns>
        public System.Int32 CreateSpecificEmailAddress(System.Int32 Id)
        {
            System.String stmt = string.Format("SET IDENTITY_INSERT [EmailAddresses] ON; INSERT INTO [EmailAddresses]([Email]) VALUES ({0:f0}); SET IDENTITY_INSERT [EmailAddresses] OFF;", Id);
            ExecuteCommand(stmt);

            // Return the ID of the Email number record
            return Id;
        }
    }
}
