﻿#pragma warning disable 1591

namespace DebtPlus.LINQ
{
    using System;

    public interface IACH_Generate_File
    {
        Int32   bank { get; set; }
        string  transaction_code { get; set; }
        string  routing_number { get; set; }
        string  account_number { get; set; }
        decimal amount { get; set; }
        int     client { get; set; }
        string  client_name { get; set; }
        Int32?  client_product { get; set; }
    }
}
