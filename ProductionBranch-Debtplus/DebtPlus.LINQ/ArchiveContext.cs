﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Text;
using System.Configuration;

namespace DebtPlus.LINQ
{
    public partial class ArchiveContext : DebtPlusDataContext
    {
        /// <summary>
        /// Raised before we do the SubmitChanges function to the database
        /// </summary>
        public event EventHandler BeforeSubmitChanges;

        /// <summary>
        /// Called to raise the BeforeSubmitChanges function
        /// </summary>
        /// <param name="e">Pointer to the event arguments</param>
        protected void RaiseBeforeSubmitChanges(EventArgs e)
        {
            var evt = BeforeSubmitChanges;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        /// <summary>
        /// Called to raise the BeforeSubmitChanges function
        /// </summary>
        /// <param name="e">Pointer to the event arguments</param>
        protected virtual void OnBeforeSubmitChanges(EventArgs e)
        {
            RaiseBeforeSubmitChanges(e);
        }

        /// <summary>
        /// Routine to handle the ConfictException when it occurs.
        /// </summary>
        /// <param name="ex">Exception event that was raised</param>
        partial void OnConflictException(System.Data.Linq.ChangeConflictException ex);

        private DebtPlus.LINQ.Logging.Log4NetLogger log4Net = null;
        private void EnableLogging()
        {
            string flg = DebtPlus.Configuration.Config.EnableLINQLoggging;
            if (string.Compare(flg, "OFF", true) != 0)
            {
                log4Net = new DebtPlus.LINQ.Logging.Log4NetLogger();
                log4Net.CreateLogger(typeof(DebtPlus.LINQ.ArchiveContext));
                this.Log = new DebtPlus.LINQ.Logging.LINQLogger(log4Net, flg);
            }
        }

        /// <summary>
        /// Initialize the new class
        /// </summary>
        public ArchiveContext()
            : base(ConfigurationManager.ConnectionStrings["DebtPlusArchive"].ConnectionString)
        {
            EnableLogging();
        }

        /// <summary>
        /// Initialize the new class
        /// </summary>
        /// <param name="connection">String for the connection object</param>
        public ArchiveContext(string connection)
            : base(connection)
        {
            EnableLogging();
        }

        /// <summary>
        /// Initialize the new class
        /// </summary>
        /// <param name="connection">Connection object to be used</param>
        public ArchiveContext(System.Data.IDbConnection connection)
            : base(connection)
        {
            EnableLogging();
        }

        /// <summary>
        /// Initialize the new class
        /// </summary>
        /// <param name="connection">String for the connection object</param>
        /// <param name="mappingSource">Map tables for the translations</param>
        public ArchiveContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource)
            : base(connection, mappingSource)
        {
            EnableLogging();
        }

        /// <summary>
        /// Initialize the new class
        /// </summary>
        /// <param name="connection">Connection object to be used</param>
        /// <param name="mappingSource">Map tables for the translations</param>
        public ArchiveContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource)
            : base(connection, mappingSource)
        {
            EnableLogging();
        }

        /// <summary>
        /// Submit modifications to the database
        /// </summary>
        public new void SubmitChanges()
        {
            OnBeforeSubmitChanges(EventArgs.Empty);
            try
            {
                base.SubmitChanges(ConflictMode.FailOnFirstConflict);
            }
            catch (ChangeConflictException ex)
            {
                // Notify someone else that the data conflict occurred
                OnConflictException(ex);

                // Just replace the values with our copies. We run in "Last Change Wins" mode.
                foreach (ObjectChangeConflict occ in ChangeConflicts)
                {
                    occ.Resolve(RefreshMode.KeepChanges);
                }

                // Finally do the database update again once the conflicts have been resolved
                base.SubmitChanges(ConflictMode.ContinueOnConflict);
            }
        }

        /// <summary>
        /// Submit modifications to the database
        /// </summary>
        /// <param name="ConflictMode"></param>
        public new void SubmitChanges(ConflictMode Mode)
        {
            // Generate the event to process the submissions and pass along the argument
            OnBeforeSubmitChanges(EventArgs.Empty);
            base.SubmitChanges(Mode);
        }
    }
}
