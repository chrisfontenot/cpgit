﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace DebtPlus.LINQ.Logging
{
    public class LINQLogger : TextWriter
    {
        // The ILogger Instance
        public ILogger Logger { get; set; }

        //The Log level enum
        public LogLevelType LogLevel { get; set; }

        private Encoding _encoding;
        public override Encoding Encoding
        {
            get
            {
                if (_encoding == null)
                {
                    _encoding = new UnicodeEncoding(false, false);
                }
                return _encoding;
            }
        }

        public LINQLogger()
        {
        }

        public LINQLogger(ILogger logger, string loggingLevel) : this()
        {
            if (logger == null)
            {
                throw new System.ArgumentNullException("logger is null");
            }
            this.Logger = logger;

            LogLevelType tempLevel = LogLevelType.Debug;
            if (!System.Enum.TryParse<LogLevelType>(loggingLevel, true, out tempLevel))
            {
                throw new System.ArgumentException("logLevel invalid");
            }
            this.LogLevel = tempLevel;
        }

        public LINQLogger(ILogger logger, LogLevelType logLevel) : this()
        {
            this.Logger = logger;
            this.LogLevel = LogLevel;
        }

        public override void Write(string value)
        {
            if (Logger == null)
            {
                return;
            }

            switch (LogLevel)
            {
                case LogLevelType.Fatal:
                    Logger.Fatal(value);
                    break;

                case LogLevelType.Error:
                    Logger.Error(value);
                    break;

                case LogLevelType.Warn:
                    Logger.Warn(value);
                    break;

                case LogLevelType.Info:
                    Logger.Info(value);
                    break;

                case LogLevelType.Debug:
                    Logger.Debug(value);
                    break;
            }
        }

        public override void Write(char[] buffer, int index, int count)
        {
            Write(new string(buffer, index, count));
        }
    }

    public enum LogLevelType
    {
        Fatal,
        Error,
        Warn,
        Info,
        Debug
    }
}
