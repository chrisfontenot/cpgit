﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;

namespace DebtPlus.LINQ.Logging
{
    /// <summary>
    /// This is an implementation of ILogger interface which uses log4net for logging.
    /// </summary>
    public class Log4NetLogger : ILogger
    {
        private ILog log;

        public void CreateLogger()
        {
            CreateLogger(typeof(DebtPlus.LINQ.BusinessContext));
        }

        public void CreateLogger(Type loggerType)
        {
            log = LogManager.GetLogger(loggerType);
        }

        // An implementation of interface indexer for setting logging properties.
        public string this[string propertyName]
        {
            set { ThreadContext.Properties[propertyName] = value; }
        }

        // An implementation of interface method for logging Debug messages
        public void Debug(string message)
        {
            if (log == null)
            {
                CreateLogger();
            }

            if (log.IsDebugEnabled)
            {
                log.Debug(message);
            }
        }

        // An implementation of interface method for logging Debug messages
        public void Debug(string message, System.Exception ex)
        {
            if (log == null)
            {
                CreateLogger();
            }

            if (log.IsDebugEnabled)
            {
                log.Debug(message, ex);
            }
        }

        // An implementation of interface method for logging Fatal messages
        public void Fatal(string message)
        {
            if (log == null)
            {
                CreateLogger();
            }

            if (log.IsFatalEnabled)
            {
                log.Fatal(message);
            }
        }

        // An implementation of interface method for logging Fatal messages
        public void Fatal(string message, System.Exception ex)
        {
            if (log == null)
            {
                CreateLogger();
            }

            if (log.IsFatalEnabled)
            {
                log.Fatal(message, ex);
            }
        }

        // An implementation of interface method for logging Info messages
        public void Info(string message)
        {
            if (log == null)
            {
                CreateLogger();
            }

            if (log.IsInfoEnabled)
            {
                log.Info(message);
            }
        }

        // An implementation of interface method for logging Info messages
        public void Info(string message, System.Exception ex)
        {
            if (log == null)
            {
                CreateLogger();
            }

            if (log.IsInfoEnabled)
            {
                log.Info(message, ex);
            }
        }

        // An implementation of interface method for logging Error messages
        public void Error(string message)
        {
            if (log == null)
            {
                CreateLogger();
            }

            if (log.IsErrorEnabled)
            {
                log.Error(message);
            }
        }

        // An implementation of interface method for logging Error messages
        public void Error(string message, System.Exception ex)
        {
            if (log == null)
            {
                CreateLogger();
            }

            if (log.IsErrorEnabled)
            {
                log.Error(message, ex);
            }
        }

        // An implementation of interface method for logging Warning messages
        public void Warn(string message)
        {
            if (log == null)
            {
                CreateLogger();
            }

            if (log.IsWarnEnabled)
            {
                log.Warn(message);
            }
        }

        // An implementation of interface method for logging Warning messages
        public void Warn(string message, System.Exception ex)
        {
            if (log == null)
            {
                CreateLogger();
            }

            if (log.IsWarnEnabled)
            {
                log.Warn(message, ex);
            }
        }
    }
}
