﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ.Logging
{
    // An interface for loggers.
    public interface ILogger
    {
        // This method creates a logger for specified class
        void CreateLogger();
        void CreateLogger(Type loggerType);

        // Indexer for setting properties for logging
        string this[string propertyName]
        {
            set;
        }

        //These methods log in various logging levels
        void Debug(string message);
        void Debug(string message, System.Exception ex);
        void Fatal(string message);
        void Fatal(string message, System.Exception ex);
        void Info(string message);
        void Info(string message, System.Exception ex);
        void Error(string message);
        void Error(string message, System.Exception ex);
        void Warn(string message);
        void Warn(string message, System.Exception ex);
    }
}
