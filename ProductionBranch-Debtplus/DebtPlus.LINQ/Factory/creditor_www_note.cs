﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static creditor_www_note Manufacture_creditor_www_note()
        {
            var ans = new creditor_www_note()
            {
                creditor = null,
                date_shown = null,
                date_viewed = null,
                message = string.Empty
            };

            return ans;
        }

        public static creditor_www_note Manufacture_creditor_www_note(string CreditorLabel)
        {
            var ans = Manufacture_creditor_www_note();
            ans.creditor = CreditorLabel;
            return ans;
        }
    }
}
