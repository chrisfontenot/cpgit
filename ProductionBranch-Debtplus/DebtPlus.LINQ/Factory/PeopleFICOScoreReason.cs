﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank PeopleFICOScoreReason record.
        /// </summary>
        /// <returns></returns>
        public static PeopleFICOScoreReason Manufacture_PeopleFICOScoreReason()
        {
            var ans = new PeopleFICOScoreReason()
            {
                Id            = default(Int32),
                ScoreReasonID = 0
            };
            return ans;
        }
    }
}
