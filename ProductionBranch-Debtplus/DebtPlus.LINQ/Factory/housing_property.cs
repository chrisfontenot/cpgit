﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a new Housing_property record
        /// </summary>
        /// <returns></returns>
        public static Housing_property Manufacture_Housing_property()
        {
            var ans = new DebtPlus.LINQ.Housing_property()
            {
                Id                           = 0,
                HousingID                    = 0,
                annual_ins_amount            = 0M,
                annual_property_tax          = 0M,
                OwnerID                      = null,
                CoOwnerID                    = null,
                FcNoticeReceivedIND          = false,
                FcSaleDT                     = null,
                ForSaleIND                   = false,
                HPF_counselor                = null,
                HPF_foreclosureCaseID        = null,
                HPF_interviewID              = null,
                HPF_Program                  = null,
                HPF_SubProgram               = null,
                HPF_worked_with_other_agency = false,
                HPF_ReasonForCallCD          = null,
                ImprovementsValue            = 0M,
                ins_delinq_state             = null,
                ins_due_date                 = null,
                LandValue                    = 0M,
                Occupancy                    = 0,
                PreviousAddress              = null,
                PropertyAddress              = null,
                PurchasePrice                = 0M,
                PurchaseYear                 = 0,
                RealityCompany               = null,
                SalePrice                    = 0M,
                sales_contract_date          = null,
                tax_delinq_state             = null,
                tax_due_date                 = null,
                TrustName                    = null,
                UseHomeAddress               = true,
                UseInReports                 = false,
                Appraisal_Date               = null,
                Appraisal_Type               = null,
                HOA_delinq_state             = null,
                HOA_delinq_amount            = 0M,
                ins_due_amount               = 0M,
                tax_due_amount               = 0M,

                homeowner_ins    = DebtPlus.LINQ.InMemory.YesNos.getDefault(),
                pmi_insurance    = DebtPlus.LINQ.InMemory.YesNos.getDefault(),
                property_tax     = DebtPlus.LINQ.InMemory.YesNos.getDefault(),
                PlanToKeepHomeCD = DebtPlus.LINQ.Cache.Housing_RetentionType.getDefault(),
                PropertyType     = DebtPlus.LINQ.Cache.HousingType.getDefault().GetValueOrDefault(),
                Residency        = DebtPlus.LINQ.Cache.Housing_ResidencyType.getDefault().GetValueOrDefault()
            };

            return ans;
        }

        /// <summary>
        /// Manufacture a new Housing_property record
        /// </summary>
        /// <returns></returns>
        public static Housing_property Manufacture_Housing_property(Int32 HousingID)
        {
            var ans = Manufacture_Housing_property();
            ans.HousingID = HousingID;
            return ans;
        }
    }
}
