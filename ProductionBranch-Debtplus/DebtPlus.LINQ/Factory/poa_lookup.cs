﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank poa_lookup object
        /// </summary>
        /// <returns>The new poa_lookup structure</returns>
        public static poa_lookup Manufacture_poa_lookup()
        {
            var ans = new poa_lookup()
            {
                Id = default(Int32),
                lookup_type = default(Int32),
                description = null
            };
            return ans;
        }
    }
}
