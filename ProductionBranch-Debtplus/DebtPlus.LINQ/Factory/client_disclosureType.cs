﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Create an empty client_DisclosureType record
        /// </summary>
        /// <returns></returns>
        public static client_DisclosureType Manufacture_client_DisclosureType()
        {
            var ans = new client_DisclosureType()
            {
                Id                  = default(Int32),
                ActiveFlag          = true,
                applies_applicant   = true,
                applies_coapplicant = true,
                Assembly            = string.Empty,
                Default             = false,
                description         = string.Empty,
                required_propertyID = false
            };
            return ans;
        }
    }
}
