﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static Housing_CounselorAttributeType Manufacture_Housing_CounselorAttributeType()
        {
            var ans = new Housing_CounselorAttributeType()
            {
                Id               = default(Int32),
                description      = string.Empty,
                ActiveFlag       = true,
                hud_9902_section = null
            };
            return ans;
        }
    }
}
