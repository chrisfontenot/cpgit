﻿using System;
using System.Data.Linq;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank vendor_product object
        /// </summary>
        /// <returns>The new vendor_product structure</returns>
        public static DebtPlus.LINQ.vendor_product Manufacture_vendor_product()
        {
            var ans = new DebtPlus.LINQ.vendor_product()
            {
                Id             = default(Int32),
                escrowProBono  = DebtPlus.LINQ.InMemory.EscrowProBonoTypes.getDefault().GetValueOrDefault(),
                effective_date = DateTime.Now.Date
            };

            return ans;
        }
    }
}
