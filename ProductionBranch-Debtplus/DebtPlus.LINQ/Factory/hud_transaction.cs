﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static hud_transaction Manufacture_hud_transaction()
        {
            var ans = new hud_transaction()
            {
                client_appointment = null,
                GrantAmountUsed    = 0M,
                Id                 = default(Int32),
                minutes            = 0
            };
            return ans;
        }
    }
}
