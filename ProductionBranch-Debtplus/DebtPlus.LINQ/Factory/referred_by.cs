﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank referred_by object
        /// </summary>
        /// <returns>The new referred_by structure</returns>
        public static DebtPlus.LINQ.referred_by Manufacture_referred_by()
        {
            var ans = new DebtPlus.LINQ.referred_by()
            {
                Id = default(Int32),
                ActiveFlag = true,
                Default = false,
                description = string.Empty,
                hud_9902_section = string.Empty,
                note = string.Empty,
                partnerValidation = string.Empty,
                referred_by_type = null
            };

            return ans;
        }
    }
}
