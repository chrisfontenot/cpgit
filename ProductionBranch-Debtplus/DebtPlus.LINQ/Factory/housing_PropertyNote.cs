﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a new Housing_propertyNote record
        /// </summary>
        /// <returns></returns>
        public static Housing_PropertyNote Manufacture_Housing_PropertyNote()
        {
            var ans = new Housing_PropertyNote()
            {
                Id = 0,
                PropertyID = 0,
                NoteBuffer = string.Empty
            };
            return ans;
        }

        /// <summary>
        /// Manufacture a new Housing_propertyNote record
        /// </summary>
        /// <returns></returns>
        public static Housing_PropertyNote Manufacture_Housing_PropertyNote(Int32 propertyID)
        {
            var ans = Manufacture_Housing_PropertyNote();
            ans.PropertyID = propertyID;
            return ans;
        }
    }
}
