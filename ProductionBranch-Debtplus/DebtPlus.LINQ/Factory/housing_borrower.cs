﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a new housing_borrower record
        /// </summary>
        /// <returns></returns>
        public static Housing_borrower Manufacture_housing_borrower()
        {
            var ans = new DebtPlus.LINQ.Housing_borrower()
            {
                Birthdate           = null,
                bkAuthLetterDate    = null,
                bkchapter           = null,
                bkdischarge         = null,
                bkfileddate         = null,
                bkPaymentCurrent    = null,
                Disabled            = false,
                EmailID             = null,
                emp_end_date        = null,
                emp_start_date      = null,
                FICO_Score          = 0,
                Former              = null,
                job                 = null,
                NameID              = null,
                other_job           = null,
                PersonID            = 0,
                SSN                 = null,
                Race                = DebtPlus.LINQ.Cache.RaceType.getDefault().GetValueOrDefault(10),
                CreditAgency        = DebtPlus.LINQ.InMemory.CreditAgencyTypes.getDefault(),
                Education           = DebtPlus.LINQ.Cache.EducationType.getDefault().GetValueOrDefault(1),
                Ethnicity           = DebtPlus.LINQ.Cache.EthnicityType.getDefault().GetValueOrDefault(),
                Gender              = DebtPlus.LINQ.Cache.GenderType.getDefault().GetValueOrDefault(1),
                Language            = DebtPlus.LINQ.Cache.AttributeType.getDefaultLanguage(),
                marital_status      = DebtPlus.LINQ.Cache.MaritalType.getDefault(),
                MilitaryDependentID = DebtPlus.LINQ.Cache.MilitaryDependentType.getDefault().GetValueOrDefault(),
                MilitaryGradeID     = DebtPlus.LINQ.Cache.MilitaryGradeType.getDefault().GetValueOrDefault(),
                MilitaryServiceID   = DebtPlus.LINQ.Cache.MilitaryServiceType.getDefault().GetValueOrDefault(),
                MilitaryStatusID    = DebtPlus.LINQ.Cache.MilitaryStatusType.getDefault().GetValueOrDefault()
            };

            return ans;
        }
    }
}
