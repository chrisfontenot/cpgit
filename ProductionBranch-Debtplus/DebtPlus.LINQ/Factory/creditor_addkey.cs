﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static creditor_addkey Manufacture_creditor_addkey()
        {
            var ans = new creditor_addkey()
            {
                Id = default(Int32),
                creditor = null,
                type = InMemory.addkeyTypes.getDefault()
            };

            return ans;
        }

        public static creditor_addkey Manufacture_creditor_addkey(string creditorLabel)
        {
            var ans = Manufacture_creditor_addkey();
            ans.creditor = creditorLabel;
            return ans;
        }
    }
}
