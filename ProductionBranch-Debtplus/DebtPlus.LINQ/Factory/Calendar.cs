﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static Calendar Manufacture_Calendar()
        {
            var ans = new Calendar()
            {
                Id                 = DateTime.Now.Date,
                D                  = 0,
                dayname            = null,
                DayNumber          = 0,
                DOY                = 0,
                DW                 = 0,
                FM                 = 0,
                FY                 = 0,
                HolidayDescription = null,
                isBankHoliday      = false,
                isHoliday          = false,
                isWeekday          = false,
                M                  = 0,
                monthname          = null,
                Q                  = 0,
                UTCOffset          = -5,
                W                  = 0,
                Y                  = 0
            };
            return ans;
        }
    }
}
