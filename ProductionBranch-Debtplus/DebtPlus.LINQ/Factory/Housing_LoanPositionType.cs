﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static Housing_LoanPositionType Manufacture_Housing_LoanPositionType()
        {
            var ans = new Housing_LoanPositionType()
            {
                Id               = default(Int32),
                ActiveFlag       = true,
                Default          = false,
                description      = null,
                hpf              = string.Empty
            };
            return ans;
        }
    }
}
