﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank client_product object
        /// </summary>
        /// <returns>The new client_product structure</returns>
        public static DebtPlus.LINQ.client_product Manufacture_client_product()
        {
            var ans = new DebtPlus.LINQ.client_product()
            {
                Id              = default(Int32),
                client          = default(Int32),
                cost            = 0M,
                discount        = 0M,
                disputed_amount = 0M,
                tendered        = 0M,
                invoice_status  = DebtPlus.LINQ.client_product.invoice_status_none,
                invoice_date    = null,
                disputed_status = null,
                disputed_note   = null,
                resolution_note = null
            };
            return ans;
        }
    }
}
