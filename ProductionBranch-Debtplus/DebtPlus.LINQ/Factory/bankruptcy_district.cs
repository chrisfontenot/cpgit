﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static bankruptcy_district Manufacture_bankruptcy_district()
        {
            var ans = new bankruptcy_district()
            {
                ActiveFlag  = true,
                Default     = false,
                Id          = default(Int32),
                Description = null
            };
            return ans;
        }
    }
}
