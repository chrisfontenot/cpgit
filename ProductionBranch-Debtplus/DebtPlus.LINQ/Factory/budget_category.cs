﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank budget_category object
        /// </summary>
        /// <returns>The new budget_category structure</returns>
        public static DebtPlus.LINQ.budget_category Manufacture_budget_category()
        {
            var ans = new DebtPlus.LINQ.budget_category()
            {
                Id                     = default(Int32),
                description            = "unspecified category",
                spanish_description    = "categoría no especificada",
                budget_category_group  = null,
                budget_category_group1 = null,
                ExpenseCode            = "L",
                detail                 = true,
                heading                = false,
                housing_expense        = false,
                living_expense         = true,
                hpf                    = null,
                auto_factor            = 0M,
                maximum_factor         = 0M,
                person_factor          = 0M,
                rpps_code              = DebtPlus.LINQ.InMemory.rpps_asset_types.getDefault().GetValueOrDefault(6)
            };

            return ans;
        }
    }
}
