﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank referred_by_type object
        /// </summary>
        /// <returns>The new referred_by_type structure</returns>
        public static DebtPlus.LINQ.referred_by_type Manufacture_referred_by_type()
        {
            var ans = new DebtPlus.LINQ.referred_by_type()
            {
                Id          = default(Int32),
                description = string.Empty
            };

            return ans;
        }
    }
}
