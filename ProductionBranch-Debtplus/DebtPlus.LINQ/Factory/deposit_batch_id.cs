﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static deposit_batch_id Manufacture_deposit_batch_id()
        {
            var ans = new deposit_batch_id()
            {
                Id                  = default(Int32),
                bank                = default(Int32),
                ach_effective_date  = null,
                ach_file            = null,
                ach_pull_date       = null,
                ach_settlement_date = null,
                note                = null,
                batch_type          = "CL",
                date_closed         = null,
                date_posted         = null,
                trust_register      = null
            };
            return ans;
        }
    }
}
