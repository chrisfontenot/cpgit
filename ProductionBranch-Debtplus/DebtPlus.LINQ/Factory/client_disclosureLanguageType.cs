﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Create an empty client_DisclosureLanguageType record
        /// </summary>
        /// <returns></returns>
        public static client_DisclosureLanguageType Manufacture_client_DisclosureLanguageType()
        {
            var ans = new client_DisclosureLanguageType()
            {
                Id                     = default(Int32),
                clientDisclosureTypeID = DebtPlus.LINQ.Cache.client_DisclosureType.getDefault().GetValueOrDefault(),
                languageID             = DebtPlus.LINQ.Cache.AttributeType.getDefaultLanguage(),
                expiration_date        = null,
                effective_date         = DateTime.Now,
                revision_date          = DateTime.Now,
                template_url           = string.Empty
            };
            return ans;
        }
    }
}
