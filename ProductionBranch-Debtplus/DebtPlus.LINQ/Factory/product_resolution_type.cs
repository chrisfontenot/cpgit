﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static product_resolution_type Manufacture_product_resolution_type()
        {
            var ans = new product_resolution_type()
            {
                Description   = string.Empty,
                Id            = default(Int32),
                ActiveFlag    = true,
                Default       = false,
                clear_dispute = true
            };
            return ans;
        }
    }
}
