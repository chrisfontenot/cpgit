﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank vendor object
        /// </summary>
        /// <returns>The new vendor structure</returns>
        public static DebtPlus.LINQ.vendor Manufacture_vendor()
        {
            var ans = new DebtPlus.LINQ.vendor()
            {
                Id                       = default(Int32),
                ACH_AccountNumber        = null,
                ACH_CheckingSavings      = DebtPlus.LINQ.InMemory.ACHAccountTypes.getDefault().GetValueOrDefault('C'),
                ACH_RoutingNumber        = null,
                ActiveFlag               = true,
                BillingMode              = DebtPlus.LINQ.InMemory.vendorBillingModes.getDefault(),
                CC_CVV                   = null,
                CC_ExpirationDate        = null,
                Label                    = String.Empty,
                Name                     = "NEW VENDOR",
                SupressInvoices          = false,
                SupressPayments          = false,
                MonthlyBillingDay        = 10,
                NameOnCard               = string.Empty,
                SupressPrintingPayments  = false,
                Comment                  = string.Empty,
                st_BilledMTDAmt          = 0,
                st_BilledYTDAmt          = 0,
                st_BillingAdjustmentsAmt = 0,
                st_ClientCountTotalCount = 0,
                st_DisputeCnt            = 0,
                st_PaymentsReceivedAmt   = 0,
                st_ProductsOfferedCount  = 0,
                st_ProductsSoldCnt       = 0,
                st_ReceivedMTDAmt        = 0,
                st_ReceivedYTDAmt        = 0,
                st_FirstBillingDate      = null,
                st_ReturnedMailDate      = null,
                website                  = null
            };

            return ans;
        }
    }
}
