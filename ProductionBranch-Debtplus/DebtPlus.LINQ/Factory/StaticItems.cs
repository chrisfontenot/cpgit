﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Encrypt the password with MD5 encryption that is used in the client_www record.
        /// </summary>
        /// <param name="Password">Input password string</param>
        /// <remarks>
        /// Do **NOT** use the cryptographic MD5 hashing algorithm directly. The return values are not the same.
        /// </remarks>
        /// <returns></returns>
        public static string MD5(string Password)
        {
            // Do not allow for blank passwords.
            if (string.IsNullOrWhiteSpace(Password))
            {
                return string.Empty;
            }

            // Encrypt the password for the entry
            var TempSource = System.Text.ASCIIEncoding.ASCII.GetBytes(Password);

            // Next, encode the password stream
            var MD5_Class = new System.Security.Cryptography.MD5CryptoServiceProvider();
            var TempHash = MD5_Class.ComputeHash(TempSource);

            // Finally, merge the byte stream to a string
            var sbPasswd = new System.Text.StringBuilder();
            for (Int32 index = 0; index <= TempHash.GetUpperBound(0); ++index)
            {
                sbPasswd.Append(TempHash[index].ToString("X2"));
            }

            // The encoded password is the combined string
            return sbPasswd.ToString().ToUpper();
        }

        /// <summary>
        /// Invent a new password for the web user
        /// </summary>
        public static string CreatePassword()
        {
            Int32 seedValue = (Int32) (DateTime.Now.Ticks & ((long) Int32.MaxValue));
            System.Random rnd = new System.Random(seedValue);
            return Color(rnd) + Special(rnd) + word(rnd);
        }

        /// <summary>
        /// Choose a random color sequence
        /// </summary>
        private static string Color(System.Random rnd)
        {
            // Generate a list of the colors
            string[] colors = new string[]
            {
				"blue",
				"green",
				"pink",
				"red",
				"purple",
				"magenta",
				"cyan",
				"brown",
				"yellow",
				"black",
				"silver",
				"orange",
				"white",
				"tan"
			};

            // Return a random color
            double rnd_value = Convert.ToDouble(colors.GetUpperBound(0)) * rnd.NextDouble();
            Int32 index = Convert.ToInt32(rnd_value);
            return colors[index];
        }

        /// <summary>
        /// Choose a random special character
        /// </summary>
        private static string Special(System.Random rnd)
        {
            // Generate a list of the special characters
            string specials = "!@#$%^&*()-_+=[]{}:;\";><,.?/";

            // Return a random special character
            double rnd_value = Convert.ToDouble(specials.Length) * rnd.NextDouble();
            Int32 index = Convert.ToInt32(rnd_value);
            return specials.Substring(index, 1);
        }

        /// <summary>
        /// Choose a random noun
        /// </summary>
        private static string word(System.Random rnd)
        {
            // Generate a list of the words
            string[] words = new string[]
            {
				"hawk",
				"bird",
				"worm",
				"mouse",
				"automobile",
				"telephone",
				"radio",
				"lamp",
				"stove",
				"carpet",
				"towel",
				"window",
				"tree",
				"face",
				"monkey",
				"watch",
				"bush",
				"word",
				"ground",
				"food"
			};

            // Return a random word
            double rnd_value = Convert.ToDouble(words.GetUpperBound(0)) * rnd.NextDouble();
            Int32 index = Convert.ToInt32(rnd_value);
            return words[index];
        }
    }
}
