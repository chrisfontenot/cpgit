﻿using System;

namespace DebtPlus.LINQ
{
    /// <summary>
    /// Factor class to generate new object records as needed for the LINQ system. This
    /// module contains all of the needed properties to generate the default (blank) class
    /// records. All defaults should be supplied.
    /// </summary>
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank action_items_goal object
        /// </summary>
        /// <returns>The new action_items_goal structure</returns>
        public static action_items_goal Manufacture_action_items_goal()
        {
            var ans  = new action_items_goal();
            ans.text = @"{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fnil\fcharset0 Calibri;}}\viewkind4\uc1\pard\sa200\sl276\slmult1\lang9\f0\fs24\par}";
            return ans;
        }
    }
}
