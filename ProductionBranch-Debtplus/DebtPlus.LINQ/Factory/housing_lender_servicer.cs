﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a new Housing_lender_servicer record
        /// </summary>
        /// <returns></returns>
        public static Housing_lender_servicer Manufacture_Housing_lender_servicer()
        {
            // From the record marked "IsOther", get the ServiceID field.
            Int32? serviceId = DebtPlus.LINQ.Cache.Housing_lender_servicer.getOther();
            if (serviceId != null)
            {
                serviceId = DebtPlus.LINQ.Cache.Housing_lender_servicer.getList().Find(s => s.Id == serviceId.Value).ServiceID;
            }

            // Return the answer to the question
            var ans = new DebtPlus.LINQ.Housing_lender_servicer()
            {
                ActiveFlag  = true,
                Comment     = null,
                Default     = false,
                IsOther     = false,
                description = string.Empty,
                Id          = default(Int32),
                RXOffice    = 763,
                ServiceID   = serviceId
            };

            return ans;
        }
    }
}
