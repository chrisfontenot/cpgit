﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Generate a blank creditor_register entry
        /// </summary>
        /// <returns>A pointer to the new creditor_register entry</returns>
        public static registers_creditor Manufacture_registers_creditor()
        {
            var ans = new DebtPlus.LINQ.registers_creditor()
            {
                Id = default(Int32),
                credit_amt = 0m,
                creditor = string.Empty,
                debit_amt = 0m,
                disbursement_register = null,
                fairshare_pct = null,
                invoice_register = null,
                item_date = null,
                message = null,
                tran_type = string.Empty,
                trust_register = null
            };
            return ans;
        }

        /// <summary>
        /// Generate a blank creditor_register entry
        /// </summary>
        /// <returns>A pointer to the new creditor_register entry</returns>
        public static registers_creditor Manufacture_registers_creditor(string creditorID, string tran_type)
        {
            var ans = Manufacture_registers_creditor();
            ans.creditor = creditorID;
            ans.tran_type = tran_type;
            return ans;
        }
    }
}
