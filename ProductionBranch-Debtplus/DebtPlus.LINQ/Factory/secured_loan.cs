﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static secured_loan Manufacture_secured_loan()
        {
            var ans = new secured_loan()
            {
                Id                      = default(Int32),
                account_number          = null,
                arm                     = false,
                arm_reset               = false,
                balance                 = 0M,
                case_number             = string.Empty,
                fdic_number             = string.Empty,
                fha_va_insured          = false,
                hybrid_arm              = false,
                interest_only           = false,
                interest_rate           = 0D,
                lender                  = string.Empty,
                loan_type               = DebtPlus.LINQ.Cache.Housing_LoanType.getDefault().GetValueOrDefault(),
                loan_type_description   = string.Empty,
                option_arm              = false,
                original_account_number = string.Empty,
                original_amount         = 0M,
                original_fdic_number    = string.Empty,
                original_lender         = string.Empty,
                past_due_amount         = 0M,
                past_due_periods        = 0,
                payment                 = 0M,
                periods                 = 0,
                priority                = 1,
                privately_held          = false
            };
            return ans;
        }
    }
}
