﻿namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static ContactMethodType Manufacture_ContactMethodType()
        {
            var ans = new ContactMethodType()
            {
                Id               = default(System.Int32),
                ActiveFlag       = true,
                Default          = false,
                description      = string.Empty
            };
            return ans;
        }
    }
}
