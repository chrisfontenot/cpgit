﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static workshop_type Manufacture_workshop_type()
        {
            var ans = new workshop_type()
            {
                ActiveFlag       = true,
                description      = null,
                Id               = default(Int32),
                duration         = 0,
                HousingFeeAmount = 0M,
                HUD_Grant        = DebtPlus.LINQ.Cache.Housing_GrantType.getDefault()
            };
            return ans;
        }
    }
}
