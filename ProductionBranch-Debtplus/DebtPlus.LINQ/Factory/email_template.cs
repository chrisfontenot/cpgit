﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank email_template object
        /// </summary>
        /// <returns>The new email_template structure</returns>
        public static email_template Manufacture_email_template()
        {
            var ans = new email_template()
            {
                Id           = default(Int32),
                description  = null,
                html_URI     = null,
                language     = DebtPlus.LINQ.Cache.AttributeType.getDefaultLanguage(),
                sender_email = null,
                sender_name  = null,
                subject      = null,
                text_URI     = null,
                attachments  = null
            };
            return ans;
        }
    }
}
