﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static secured_type_group Manufacture_secured_type_group()
        {
            var ans = new secured_type_group()
            {
                Id                  = default(Int32),
                description         = string.Empty,
                spanish_description = string.Empty,
                ActiveFlag          = true,
                Default             = false,
                KeyType             = string.Empty
            };
            return ans;
        }
    }
}
