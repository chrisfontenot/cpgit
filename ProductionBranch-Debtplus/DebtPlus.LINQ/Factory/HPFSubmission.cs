﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank HPFSubmission object
        /// </summary>
        /// <returns>The new HPFSubmission structure</returns>
        public static DebtPlus.LINQ.HPFSubmission Manufacture_HPFSubmission()
        {
            var ans = new DebtPlus.LINQ.HPFSubmission()
            {
                Id = default(Int32),
                responseStatus = null,
                FcId = null,
                propertyID = null,
                requestType = null,
                requestXML = null,
                responseXML = null
            };

            return ans;
        }
    }
}
