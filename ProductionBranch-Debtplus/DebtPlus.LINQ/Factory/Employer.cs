﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank employer object
        /// </summary>
        /// <returns>The new employer structure</returns>
        public static employer Manufacture_employer()
        {
            var ans = new employer()
            {
                AddressID = null,
                FAXID = null,
                Id = 0,
                name = string.Empty,
                TelephoneID = null,
                industry = DebtPlus.LINQ.Cache.industry.getDefault()
            };

            return ans;
        }
    }
}
