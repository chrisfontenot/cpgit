﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank action_items_other object
        /// </summary>
        /// <returns>The new action_items_other structure</returns>
        public static action_items_other Manufacture_action_items_other()
        {
            var ans = new action_items_other()
            {
                action_plan = default(Int32),
                description = null,
                item_group = default(Int32)
            };
            return ans;
        }

        /// <summary>
        /// Manufacture a blank action_items_other object
        /// </summary>
        /// <param name="PlanID">Associated Plan ID</param>
        /// <param name="GroupID">Associated Group ID</param>
        /// <param name="description">Associated description</param>
        /// <returns>The new action_items_other structure</returns>
        public static action_items_other Manufacture_action_items_other(Int32 PlanID, Int32 GroupID, string description)
        {
            var ans = Manufacture_action_items_other();
            ans.action_plan = PlanID;
            ans.item_group = GroupID;
            ans.description = description;

            return ans;
        }
    }
}
