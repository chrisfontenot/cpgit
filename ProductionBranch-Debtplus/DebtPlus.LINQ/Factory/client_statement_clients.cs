﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank client_statement_client object
        /// </summary>
        /// <returns>The new client_statement_client structure</returns>
        public static client_statement_client Manufacture_client_statement_client()
        {
            var ans = new client_statement_client()
            {
                active_debts           = 0,
                active_status          = string.Empty,
                client                 = default(Int32),
                client_statement_batch = default(Int32),
                counselor              = default(Int32),
                delivery_date          = null,
                delivery_method        = null,
                deposit_amt            = 0M,
                disbursement_amt       = 0M,
                expected_deposit_amt   = null,
                expected_deposit_date  = null,
                held_in_trust          = 0M,
                Id                     = System.Guid.NewGuid(),
                isACH                  = 0,
                isFinal                = 0,
                isMultiPage            = 0,
                last_deposit_amt       = 0M,
                last_deposit_date      = null,
                office                 = 0,
                refund_amt             = 0M,
                reserved_in_trust      = 0M,
                starting_trust_balance = 0M
            };
            return ans;
        }
    }
}
