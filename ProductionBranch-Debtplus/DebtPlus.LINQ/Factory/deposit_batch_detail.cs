﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static deposit_batch_detail Manufacture_deposit_batch_detail()
        {
            var ans = new deposit_batch_detail()
            {
                Id                      = default(Int32),
                amount                  = 0M,
                item_date               = System.DateTime.Now.Date,
                ach_account_number      = null,
                ach_authentication_code = null,
                ach_response_batch_id   = null,
                ach_routing_number      = null,
                ach_transaction_code    = null,
                client                  = null,
                client_creditor         = null,
                creditor                = null,
                creditor_type           = null,
                date_posted             = null,
                fairshare_amt           = null,
                fairshare_pct           = null,
                message                 = null,
                ok_to_post              = true,
                reference               = null,
                scanned_client          = null,
                tran_subtype            = null
            };
            return ans;
        }
    }
}
