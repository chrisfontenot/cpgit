﻿using System;
using System.Linq;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture and empty HPFSession object
        /// </summary>
        /// <returns>The HPFSession object</returns>
        public static HPFSession Manufacture_HPFSession()
        {
            var answer = new HPFSession()
            {
                completionEvent            = null,
                @event                     = default(Int32),
                fcID                       = null,
                programStageID             = DebtPlus.LINQ.Cache.HPFProgramStage.getDefault(),
                Id                         = 0,
                sessionID                  = null,
                actionItemNote             = default(string),
                budgetSetID                = null,
                CompletionEvent            = null,
                followupNote               = default(string),
                loanDfltReasonNote         = default(string),
                debtLoadCalculation        = default(decimal),
                householdGrossAnnualIncome = default(decimal),
                householdNetAnnualIncome   = default(decimal),
                dtiRatio                   = default(double)
            };

            return answer;
        }

        /// <summary>
        /// Manufacture and empty HPFSession object
        /// </summary>
        /// <param name="propertyRecord">The property record associated with the session</param>
        /// <returns>The HPFSession object</returns>
        public static HPFSession Manufacture_HPFSession(HPFEvent eventRecord)
        {
            var answer             = Manufacture_HPFSession();
            answer.fcID            = eventRecord.fcID;
            answer.@event          = eventRecord.Id;
            answer.completionEvent = eventRecord.Id;

            // Set the other fields in the record based upon the property information
            using (var bc = new BusinessContext())
            {
                // Find the property record from the event record that was passed as the parameter.
                var propertyRecord = bc.Housing_properties.Where(s => s.Id == eventRecord.property).FirstOrDefault();
                if (propertyRecord == null)
                {
                    return answer;
                }

                // Find the latest property note
                var pn = (from n in bc.Housing_PropertyNotes where n.PropertyID == propertyRecord.Id orderby n.date_created descending select n).FirstOrDefault();
                if (pn != null)
                {
                    answer.loanDfltReasonNote = pn.NoteBuffer;
                }

                // Find the housing record for this property
                if (propertyRecord.HousingID <= 0)
                {
                    return answer;
                }

                var housingRecord = bc.client_housings.Where(s => s.Id == propertyRecord.HousingID).FirstOrDefault();
                if (housingRecord == null)
                {
                    return answer;
                }

                // Set the fields in the housing record
                answer.dtiRatio = housingRecord.BackEnd_DTI;

                // From the property record, get the client id
                System.Int32 clientID = propertyRecord.HousingID;
                if (clientID <= 0)
                {
                    return answer;
                }

                // People information
                var colPeople = (from p in bc.peoples
                                 where p.Client == clientID
                                 select new { p.net_income, p.gross_income, p.Frequency }).ToList();

                // Other income source
                System.Collections.Generic.List<DebtPlus.LINQ.asset> colAssets = bc.assets.Where(s => s.client == clientID).ToList();
                decimal asset_amount = colAssets.Sum(s => DebtPlus.LINQ.BusinessContext.PayAmount(s.asset_amount, s.frequency));

                // Non-managed debt
                System.Collections.Generic.List<DebtPlus.LINQ.client_other_debt> colOtherDebt = bc.client_other_debts.Where(s => s.client == clientID).ToList();
                decimal other_debt = colOtherDebt.Sum(s => s.balance);

                // Unsecured debt
                var col_unsecured_debt = (from cc in bc.client_creditors
                                          join b in bc.client_creditor_balances on cc.client_creditor_balance equals b.Id
                                          where (!cc.reassigned_debt) && (cc.client == clientID)
                                          select new { amount = b.orig_balance + b.orig_balance_adjustment + b.total_interest - b.total_payments }).ToList();
                decimal unsecured_debt = col_unsecured_debt.Sum(s => s.amount);

                // Secured debt
                var col_secured_debt = (from pr in bc.secured_properties
                                        join sl in bc.secured_loans on pr.Id equals sl.secured_property
                                        where pr.client == clientID
                                        select new { amount = sl.balance }).ToList();
                decimal secured_debt = col_unsecured_debt.Sum(s => s.amount);

                // Find the total debt figure
                answer.debtLoadCalculation = unsecured_debt + secured_debt + other_debt;

                // Find the annual gross and net amounts
                var grossIncome = (from i in colPeople select i.gross_income > 0M ? DebtPlus.LINQ.BusinessContext.PayAmount(i.gross_income, i.Frequency) : DebtPlus.LINQ.BusinessContext.PayAmount(i.net_income, i.Frequency)).Sum();
                var netIncome = (from i in colPeople select i.net_income > 0M ? DebtPlus.LINQ.BusinessContext.PayAmount(i.net_income, i.Frequency) : DebtPlus.LINQ.BusinessContext.PayAmount(i.gross_income, i.Frequency)).Sum();
                answer.householdGrossAnnualIncome = (grossIncome + asset_amount) * 12M;
                answer.householdNetAnnualIncome   = (netIncome + asset_amount) * 12M;
            }

            return answer;
        }
    }
}
