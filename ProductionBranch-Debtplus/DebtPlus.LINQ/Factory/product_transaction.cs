﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank product_transaction object
        /// </summary>
        /// <returns>The new product_transaction structure</returns>
        public static DebtPlus.LINQ.product_transaction Manufacture_product_transaction()
        {
            var ans = new DebtPlus.LINQ.product_transaction()
            {
                Id               = default(Int32),
                transaction_type = null,
                client_product   = null,
                vendor           = null,
                product          = null,
                cost             = 0M,
                discount         = 0M,
                disputed         = 0M,
                payment          = 0M,
                note             = null
            };
            return ans;
        }
    }
}
