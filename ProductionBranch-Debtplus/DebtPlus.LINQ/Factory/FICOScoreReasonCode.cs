﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank FICOScoreReasonCode object
        /// </summary>
        /// <returns>The new FICOScoreReasonCode structure</returns>
        public static FICOScoreReasonCode Manufacture_FICOScoreReasonCode()
        {
            var ans = new DebtPlus.LINQ.FICOScoreReasonCode()
            {
                Id               = default(Int32),
                ActiveFlag       = true,
                Default          = false,
                description      = string.Empty,
                long_description = string.Empty,
                tip              = string.Empty,
                Equifax          = null
            };

            return ans;
        }
    }
}
