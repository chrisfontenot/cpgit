﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a new Housing_Loan record
        /// </summary>
        /// <returns></returns>
        public static Housing_loan Manufacture_Housing_loan()
        {
            var ans = new DebtPlus.LINQ.Housing_loan()
            {
                CurrentDetailID = null,
                CurrentLenderID = null,
                CurrentLoanBalanceAmt = 0M,
                DsaDetailID = null,
                IntakeDetailID = null,
                IntakeSameAsCurrent = true,
                Loan1st2nd = DebtPlus.LINQ.Cache.Housing_LoanPositionType.getDefault(),
                LoanDelinquencyAmount = 0M,
                LoanDelinquencyMonths = 0,
                LoanOriginationDate = null,
                MortgageClosingCosts = 0M,
                OrigLenderID = null,
                OrigLoanAmt = 0M,
                PropertyID = 0,
                UseInReports = false,
                UseOrigLenderID = true
            };

            return ans;
        }

        /// <summary>
        /// Manufacture a new Housing_loan record
        /// </summary>
        /// <returns></returns>
        public static Housing_loan Manufacture_Housing_loan(Int32 PropertyID)
        {
            var ans = Manufacture_Housing_loan();
            ans.PropertyID = PropertyID;
            return ans;
        }
    }
}
