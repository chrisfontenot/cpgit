﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a new Housing_training_course record
        /// </summary>
        /// <returns></returns>
        public static Housing_training_course Manufacture_Housing_training_course()
        {
            var ans = new DebtPlus.LINQ.Housing_training_course()
            {
                ActiveFlag = true,
                Certificate = false,
                Id = default(Int32),
                TrainingDuration = default(Int32),
                TrainingOrganization = null,
                TrainingOrganizationOther = null,
                TrainingSponsor = null,
                TrainingSponsorOther = null,
                TrainingTitle = string.Empty
            };

            return ans;
        }
    }
}
