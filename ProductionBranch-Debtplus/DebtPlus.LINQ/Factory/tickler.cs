﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank tickler object
        /// </summary>
        /// <returns>The new tickler structure</returns>
        public static tickler Manufacture_tickler()
        {
            var ans = new tickler()
            {
                Id                 = default(Int32),
                client             = default(Int32),
                counselor          = null,
                date_deleted       = null,
                date_effective     = System.DateTime.Now.AddDays(7).Date,
                note               = string.Empty,
                original_counselor = null,
                priority           = 9,
                ShowInGrid         = true,
                tickler_type       = DebtPlus.LINQ.Cache.TicklerType.getDefault().GetValueOrDefault()
            };
            return ans;
        }

        /// <summary>
        /// Manufacture a blank tickler object
        /// </summary>
        /// <returns>The new tickler structure</returns>
        public static tickler Manufacture_tickler(Int32 ClientID)
        {
            var ans       = Manufacture_tickler();
            ans.client    = ClientID;
            ans.counselor = DebtPlus.LINQ.BusinessContext.GetCurrentCounselorID();
            return ans;
        }

        /// <summary>
        /// Manufacture a blank tickler object
        /// </summary>
        /// <returns>The new tickler structure</returns>
        public static tickler Manufacture_tickler(Int32 ClientID, Int32 CounselorID)
        {
            var ans       = Manufacture_tickler();
            ans.counselor = CounselorID;
            ans.client    = ClientID;

            // Find the counselor. We need the logon id for the counselor.
            var co        = DebtPlus.LINQ.Cache.counselor.getList().Find(s => s.Id == CounselorID);
            if (co != null)
            {
                ans.original_counselor = co.Person;
            }
            return ans;
        }
    }
}
