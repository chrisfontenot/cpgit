﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Create an empty client_disclosure record
        /// </summary>
        /// <returns></returns>
        public static client_disclosure Manufacture_client_disclosure()
        {
            var ans = new client_disclosure()
            {
                Id                = default(Int32),
                disclosureID      = default(Int32),
                client            = default(Int32),
                applicant         = default(Int32),
                applicant_value   = null,
                coapplicant       = default(Int32),
                coapplicant_value = null,
                propertyID        = null
            };
            return ans;
        }

        /// <summary>
        /// Create an empty client_disclosure record
        /// </summary>
        /// <param name="clientID">ID for the client</param>
        /// <returns></returns>
        public static client_disclosure Manufacture_client_disclosure(Int32 clientID)
        {
            var ans    = Manufacture_client_disclosure();
            ans.client = clientID;
            return ans;
        }

        /// <summary>
        /// Create an empty client_disclosure record
        /// </summary>
        /// <param name="clientID">ID for the client</param>
        /// <param name="disclosureID">ID for the disclosure</param>
        /// <returns></returns>
        public static client_disclosure Manufacture_client_disclosure(Int32 clientID, Int32 disclosureID)
        {
            var ans          = Manufacture_client_disclosure();
            ans.client       = clientID;
            ans.disclosureID = disclosureID;
            return ans;
        }
    }
}
