﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static RelationType Manufacture_RelationType()
        {
            var ans = new RelationType()
            {
                ActiveFlag       = true,
                Default          = false,
                description      = null,
                hud_9902_section = string.Empty,
                Id               = default(Int32),
                note             = string.Empty,
                rpps             = string.Empty
            };
            return ans;
        }
    }
}
