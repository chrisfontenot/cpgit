﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static rpps_biller_id Manufacture_rpps_biller_id()
        {
            var ans = new rpps_biller_id()
            {
                biller_aba = null,
                biller_class = null,
                biller_name = null,
                biller_type = 1,
                blroldname = null,
                checkdigit = false,
                currency = null,
                dmppayonly = false,
                effdate = DateTime.Now.Date,
                guaranteed_funds = true,
                Id = string.Empty,
                livedate = DateTime.Now.Date,
                Mastercard_Key = null,
                note = null,
                payment_prenote = false,
                proposal_prenote = false,
                pvt_biller_id = false,
                reqAdndaRev = false,
                returns_cda = false,
                returns_cdc = false,
                returns_cdm = false,
                returns_cdr = false,
                returns_cdt = false,
                returns_cdv = false,
                reversal_biller_id = null,
                send_cdd = true,
                send_cdf = true,
                send_cdn = true,
                send_cdp = true,
                send_cdv = true,
                send_exception_pay = false,
                send_fbc = false,
                send_fbd = false
            };

            return ans;
        }

        public static rpps_biller_id Manufacture_rpps_biller_id(string Id)
        {
            var ans = Manufacture_rpps_biller_id();
            ans.Id = (Id ?? string.Empty).PadLeft(9, '0').Substring(0, 9);
            return ans;
        }

        public static rpps_biller_id Manufacture_rpps_biller_id(string Id, string biller_name)
        {
            var ans = Manufacture_rpps_biller_id(Id);
            ans.biller_name = biller_name;
            return ans;
        }
    }
}
