﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static Housing_CounselorContactReasonType Manufacture_Housing_CounselorContactReasonType()
        {
            var ans = new Housing_CounselorContactReasonType()
            {
                Id          = default(Int32),
                ActiveFlag  = true,
                Default     = false,
                description = null,
                hpf         = string.Empty
            };
            return ans;
        }
    }
}
