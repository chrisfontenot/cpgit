﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank vendor_note object
        /// </summary>
        /// <returns>The new vendor_note structure</returns>
        public static DebtPlus.LINQ.vendor_note Manufacture_vendor_note()
        {
            var ans = new DebtPlus.LINQ.vendor_note()
            {
                Id          = default(Int32),
                dont_delete = false,
                dont_edit   = false,
                dont_print  = false,
                expires     = null,
                subject     = null,
                note        = DebtPlus.LINQ.InMemory.Notes.Default_Note,
                type        = (int)DebtPlus.LINQ.InMemory.Notes.NoteTypes.Permanent
            };
            return ans;
        }

        /// <summary>
        /// Manufacture a blank creditor_note object
        /// </summary>
        /// <returns>The new creditor_note structure</returns>
        public static vendor_note Manufacture_vendor_note(int VendorLabel)
        {
            //if (string.IsNullOrWhiteSpace(VendorLabel))
            //{
            //    throw new ArgumentOutOfRangeException("VendorLabel");
            //}
            return Manufacture_vendor_note(VendorLabel, 1);
        }

        /// <summary>
        /// Manufacture a blank creditor_note object
        /// </summary>
        /// <returns>The new creditor_note structure</returns>
        public static vendor_note Manufacture_vendor_note(int vendorLabel, Int32 NoteType)
        {
            var ans = Manufacture_vendor_note();
            ans.vendor = vendorLabel;
            ans.type = NoteType;

            // Temporary notes expire in 30 days
            if (NoteType == 2)
            {
                ans.expires = DateTime.Now.Date.AddDays(30);
            }

            // Alert notes expire in 90 days
            else if (NoteType == 4)
            {
                ans.expires = DateTime.Now.Date.AddDays(90);
            }

            // System notes may not be edited
            else if (NoteType == 3)
            {
                ans.dont_delete = true;
                ans.dont_edit = true;
            }
            return ans;
        }
    }
}
