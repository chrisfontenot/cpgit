﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank vendor_address object
        /// </summary>
        /// <returns>The new vendor_address structure</returns>
        public static DebtPlus.LINQ.vendor_address Manufacture_vendor_address()
        {
            var ans = new DebtPlus.LINQ.vendor_address()
            {
                Id           = default(Int32),
                address_type = vendor_address.AddressType_General,
                addressID    = null,
                attn         = string.Empty,
                line_1       = string.Empty,
                line_2       = string.Empty
            };

            return ans;
        }


        /// <summary>
        /// Manufacture a blank vendor_address object
        /// </summary>
        /// <param name="creditorLabel">Associated Creditor ID</param>
        /// <param name="addressType">Type of address</param>
        /// <returns>The new vendor_address structure</returns>
        public static vendor_address Manufacture_vendor_address(int vendorLabel, char addressType)
        {
            var ans = Manufacture_vendor_address();
            ans.vendor = vendorLabel;
            ans.address_type = addressType;

            return ans;
        }
    }
}
