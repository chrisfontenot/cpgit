﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static RPPSAssetType Manufacture_RPPSAssetType()
        {
            var ans = new RPPSAssetType()
            {
                ActiveFlag  = true,
                Default     = false,
                description = string.Empty,
                Id          = default(Int32)
            };
            return ans;
        }
    }
}
