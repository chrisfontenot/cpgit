﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank client_other_debt object
        /// </summary>
        /// <returns>The new client_other_debt structure</returns>
        public static client_other_debt Manufacture_client_other_debt()
        {
            var ans = new client_other_debt()
            {
                Id = default(Int32),
                account_number = string.Empty,
                balance = 0M,
                client = default(Int32),
                creditor_name = string.Empty,
                interest_rate = 0D,
                payment = 0M
            };
            return ans;
        }

        /// <summary>
        /// Manufacture a blank client_other_debt object
        /// </summary>
        /// <returns>The new client_other_debt structure</returns>
        public static client_other_debt Manufacture_client_other_debt(Int32 ClientID)
        {
            var ans = Manufacture_client_other_debt();
            ans.client = ClientID;
            return ans;
        }
    }
}
