﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a new housing_loan_detail record
        /// </summary>
        /// <returns></returns>
        public static Housing_loan_detail Manufacture_housing_loan_detail()
        {
            var ans = new DebtPlus.LINQ.Housing_loan_detail()
            {
                ARM_Reset = false,
                FHA_VA_Insured_Loan = false,
                Hybrid_ARM_Loan = false,
                Interest_Only_Loan = false,
                Option_ARM_Loan = false,
                Privately_Held_Loan = false,
                InterestRate = default(double),
                FinanceTypeCD = DebtPlus.LINQ.Cache.Housing_FinancingType.getDefault(),
                LoanTypeCD = DebtPlus.LINQ.Cache.Housing_LoanType.getDefault(),
                MortgageTypeCD = DebtPlus.LINQ.Cache.Housing_MortgageType.getDefault(),
                Payment = 0M
            };

            return ans;
        }
    }
}
