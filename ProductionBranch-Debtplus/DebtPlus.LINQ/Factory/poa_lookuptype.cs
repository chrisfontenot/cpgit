﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank poa_lookuptype object
        /// </summary>
        /// <returns>The new poa_lookuptype structure</returns>
        public static poa_lookuptype Manufacture_poa_lookuptype()
        {
            var ans = new poa_lookuptype()
            {
                description = null,
                Id = default(Int32)
            };
            return ans;
        }
    }
}
