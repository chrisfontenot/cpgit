﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a new people record
        /// </summary>
        /// <returns></returns>
        public static people Manufacture_people()
        {
            var ans = new DebtPlus.LINQ.people()
            {
                Id                   = default(Int32),
                Client               = default(Int32),
                CreditAgency         = DebtPlus.LINQ.InMemory.CreditAgencyTypes.getDefault(),
                Education            = DebtPlus.LINQ.Cache.EducationType.getDefault().GetValueOrDefault(),
                Ethnicity            = DebtPlus.LINQ.Cache.EthnicityType.getDefault().GetValueOrDefault(),
                Frequency            = DebtPlus.LINQ.Cache.PayFrequencyType.getDefault().GetValueOrDefault(4),
                Gender               = DebtPlus.LINQ.Cache.GenderType.getDefault().GetValueOrDefault(1),
                MilitaryDependentID  = DebtPlus.LINQ.Cache.MilitaryDependentType.getDefault().GetValueOrDefault(),
                MilitaryGradeID      = DebtPlus.LINQ.Cache.MilitaryGradeType.getDefault().GetValueOrDefault(),
                MilitaryServiceID    = DebtPlus.LINQ.Cache.MilitaryServiceType.getDefault().GetValueOrDefault(),
                MilitaryStatusID     = DebtPlus.LINQ.Cache.MilitaryStatusType.getDefault().GetValueOrDefault(0),
                no_fico_score_reason = DebtPlus.LINQ.Cache.Housing_FICONotIncludedReason.getDefault(),
                Race                 = DebtPlus.LINQ.Cache.RaceType.getDefault().GetValueOrDefault(10),
                Relation             = DebtPlus.LINQ.Cache.RelationType.getDefault().GetValueOrDefault(2),
                bkPaymentCurrent     = DebtPlus.LINQ.InMemory.YesNos.getDefault(),
                gross_income         = 0M,
                net_income           = 0M,
                Disabled             = false,
                Birthdate            = null,
                bkAuthLetterDate     = null,
                bkchapter            = null,
                bkdischarge          = null,
                bkfileddate          = null,
                bkDistrict           = null,
                CellTelephoneID      = null,
                EmailID              = null,
                employer             = null,
                emp_end_date         = null,
                emp_start_date       = null,
                FICO_Score           = null,
                FICO_pull_date       = null,
                Former               = null,
                job                  = null,
                NameID               = null,
                other_job            = null,
                SSN                  = null,
                WorkTelephoneID      = null
            };
            return ans;
        }

        /// <summary>
        /// Manufacture a new person record
        /// </summary>
        /// <returns></returns>
        public static people Manufacture_people(Int32 ClientID)
        {
            var ans = Manufacture_people();
            ans.Client = ClientID;
            return ans;
        }
    }
}
