﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a new hecm_default record
        /// </summary>
        /// <returns></returns>
        public static hecm_default Manufacture_hecm_default()
        {
            var ans = new DebtPlus.LINQ.hecm_default()
            {
                Id                   = default(Int32),
                bdt_bec              = null,
                checkup              = null,
                client               = default(Int32),
                file_status          = null,
                file_status_date     = null,
                Hardest_Hit_Ref_Date = null,
                ref_car_ins          = 0,
                ref_food             = 0,
                ref_home_repair      = 0,
                ref_income           = 0,
                ref_medical          = 0,
                ref_prescription     = 0,
                ref_property_taxes   = 0,
                ref_social_services  = 0,
                ref_ssi              = 0,
                ref_utility          = 0,
                save_car_ins         = 0M,
                save_food            = 0M,
                save_home_repair     = 0M,
                save_income          = 0M,
                save_medical         = 0M,
                save_prescription    = 0M,
                save_property_taxes  = 0M,
                save_social_services = 0M,
                save_ssi             = 0M,
                save_utility         = 0M,
                referral_option      = DebtPlus.LINQ.Cache.HECM_Lookup.getDefault(),
                tier                 = DebtPlus.LINQ.Cache.HECM_Lookup.getDefault()
            };

            return ans;
        }

        /// <summary>
        /// Manufacture a new hecm_default record
        /// </summary>
        /// <returns></returns>
        public static hecm_default Manufacture_hecm_default(Int32 ClientID)
        {
            var ans    = Manufacture_hecm_default();
            ans.client = ClientID;
            return ans;
        }
    }
}
