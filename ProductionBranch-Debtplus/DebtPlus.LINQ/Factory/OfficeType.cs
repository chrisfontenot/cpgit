﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static OfficeType Manufacture_OfficeType()
        {
            var ans = new OfficeType()
            {
                Id               = default(Int32),
                ActiveFlag       = true,
                Default          = false,
                description      = string.Empty
            };
            return ans;
        }
    }
}
