﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture an empty creditor_contribution_pct record
        /// </summary>
        public static creditor_contribution_pct Manufacture_creditor_contribution_pct()
        {
            var ans = new creditor_contribution_pct()
            {
                creditor = null,
                creditor_type_check = 'N',
                creditor_type_eft = 'N',
                date_updated = null,
                effective_date = DateTime.Now.AddDays(1).Date,
                fairshare_pct_check = 0,
                fairshare_pct_eft = 0,
                Id = default(Int32)
            };

            return ans;
        }

        /// <summary>
        /// Manufacture an empty creditor_contribution_pct record
        /// </summary>
        public static creditor_contribution_pct Manufacture_creditor_contribution_pct(string creditorLabel)
        {
            var ans = Manufacture_creditor_contribution_pct();
            ans.creditor = creditorLabel;
            return ans;
        }
    }
}
