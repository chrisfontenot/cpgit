﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static sales_file Manufacture_sales_file()
        {
            var ans = new sales_file()
            {
                Id                      = default(Int32),
                client                  = default(Int32),
                client_monthly_interest = 0M,
                client_months           = 0,
                client_total_interest   = 0M,
                date_exported           = null,
                exported_by             = null,
                extra_amount            = 0M,
                note                    = null,
                plan_monthly_interest   = 0M,
                plan_months             = 0,
                plan_total_interest     = 0M
            };
            return ans;
        }

        public static sales_file Manufacture_sales_file(Int32 ClientID)
        {
            var ans    = Manufacture_sales_file();
            ans.client = ClientID;
            return ans;
        }
    }
}
