﻿using System;

namespace DebtPlus.LINQ
{
    /// <summary>
    /// Factor class to generate new object records as needed for the LINQ system. This
    /// module contains all of the needed properties to generate the default (blank) class
    /// records. All defaults should be supplied.
    /// </summary>
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank action_plan object
        /// </summary>
        /// <returns>The new action_plan structure</returns>
        public static action_plan Manufacture_action_plan()
        {
            var ans = new action_plan();
            return ans;
        }

        /// <summary>
        /// Manufacture a blank action_plan object
        /// </summary>
        /// <param name="ClientID">Associated Client ID</param>
        /// <returns>The new action_plan structure</returns>
        public static action_plan Manufacture_action_plan(Int32 ClientID)
        {
            var ans = Manufacture_action_plan();
            ans.client = ClientID;

            return ans;
        }
    }
}
