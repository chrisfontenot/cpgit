﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static Housing_LoanType Manufacture_Housing_LoanType()
        {
            var ans = new Housing_LoanType()
            {
                Id               = default(Int32),
                ActiveFlag       = true,
                Default          = false,
                description      = null,
                modification     = false,
                morgageType      = string.Empty,
                nfmcp_section    = string.Empty,
                rateType         = string.Empty,
                hpf              = string.Empty,
                hud_9902_section = string.Empty
            };
            return ans;
        }
    }
}
