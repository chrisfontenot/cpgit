﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static creditor_contact_type Manufacture_creditor_contact_type()
        {
            var ans = new creditor_contact_type()
            {
                ActiveFlag   = true,
                contact_type = null,
                Default      = false,
                description  = null,
                Id           = default(Int32),
                name         = null
            };
            return ans;
        }
    }
}
