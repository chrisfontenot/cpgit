﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static rpps_file Manufacture_rpps_file()
        {
            var ans = new rpps_file()
            {
                bank = default(Int32),
                file_type = string.Empty,
                filename = string.Empty,
                Id = default(Int32)
            };
            return ans;
        }

        public static rpps_file Manufacture_rpps_file(string file_type, Int32 bankID)
        {
            var ans = Manufacture_rpps_file();
            ans.bank = bankID;
            ans.file_type = file_type;
            return ans;
        }
    }
}
