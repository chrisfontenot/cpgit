﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static client_indicator Manufacture_client_indicator()
        {
            var ans = new client_indicator()
            {
                client    = default(Int32),
                indicator = default(Int32),
                Id        = default(Int32)
            };
            return ans;
        }
    }
}
