﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static county Manufacture_county()
        {
            var ans = new county()
            {
                Id                  = default(Int32),
                ActiveFlag          = true,
                Default             = false,
                name                = null,
                median_income       = 0M,
                bankruptcy_district = DebtPlus.LINQ.Cache.bankruptcy_district.getDefault(),
                state               = DebtPlus.LINQ.Cache.state.getDefault().GetValueOrDefault()
            };
            return ans;
        }
    }
}
