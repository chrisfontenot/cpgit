﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static Housing_ClientContactOutcomeType Manufacture_Housing_ClientContactOutcomeType()
        {
            var ans = new Housing_ClientContactOutcomeType()
            {
                Id          = default(Int32),
                ActiveFlag  = true,
                Default     = false,
                description = null,
                hpf         = string.Empty
            };
            return ans;
        }
    }
}
