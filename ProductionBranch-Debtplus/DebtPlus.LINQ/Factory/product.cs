﻿using System;
using System.Linq;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        private static object objLock = new object();

        private static System.Collections.Generic.List<DebtPlus.LINQ.product> colItems;
        /// <summary>
        /// Manufacture a blank product object
        /// </summary>
        /// <returns>The new product structure</returns>
        public static DebtPlus.LINQ.product Manufacture_product()
        {
            var ans = new DebtPlus.LINQ.product()
            {
                Id          = default(Int32),
                ActiveFlag  = true,
                Amount      = 0M,
                Default     = false,
                description = null
            };

            return ans;
        }

        /// <summary>
        /// Manufacture an empty vendor_contribution_pct record
        /// </summary>
        public static vendor_product Manufacture_product(int vendorLabel)
        {
            var ans = new DebtPlus.LINQ.vendor_product();
            ans.vendor = vendorLabel;
            return ans;
        }

        
        /// <summary>
        /// Return the static copy of the RelationTypes table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.product> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.products.ToList();
                        }
                    }
                }
            }
            return colItems;
        }
    }
}
