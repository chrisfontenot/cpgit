﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static hud_interview Manufacture_hud_interview()
        {
            var ans = new hud_interview()
            {
                Id                    = default(Int32),
                client                = default(Int32),
                HUD_Grant             = DebtPlus.LINQ.Cache.Housing_GrantType.getDefault().GetValueOrDefault(),
                interview_type        = DebtPlus.LINQ.Cache.Housing_PurposeOfVisitType.getDefault().GetValueOrDefault(),
                HousingFeeAmount      = 0M,
                hud_result            = null,
                result_counselor      = null,
                result_date           = null,
                termination_counselor = null,
                termination_date      = null,
                termination_reason    = null
            };
            return ans;
        }

        public static hud_interview Manufacture_hud_interview(Int32 clientID)
        {
            var ans    = Manufacture_hud_interview();
            ans.client = clientID;
            return ans;
        }
    }
}
