﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static client_situation Manufacture_client_situation()
        {
            var ans = new client_situation()
            {
                ActiveFlag       = true,
                @default         = false,
                description      = string.Empty,
                oID              = default(Int32),
                sortorder        = default(Int32)
            };
            return ans;
        }
    }
}
