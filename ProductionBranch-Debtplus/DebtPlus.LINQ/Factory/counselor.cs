﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank counselor object
        /// </summary>
        /// <returns>The new counselor structure</returns>
        public static counselor Manufacture_counselor()
        {
            var ans = new counselor()
            {
                Id              = default(Int32),
                ActiveFlag      = true,
                Menu_Level      = 9,
                Office          = DebtPlus.LINQ.Cache.office.getDefault().GetValueOrDefault(1),
                rate            = default(decimal),
                SSN             = string.Empty,
                Note            = string.Empty,
                emp_start_date  = DateTime.Now.Date,
                billing_mode    = "FIXED",
                CaseCounter     = 0,
                Default         = false,
                Color           = null,
                emp_end_date    = null,
                HUD_id          = null,
                Image           = null,
                Person          = null,
                TelephoneID     = null,
                TelephoneNumber = null
            };
            return ans;
        }
    }
}
