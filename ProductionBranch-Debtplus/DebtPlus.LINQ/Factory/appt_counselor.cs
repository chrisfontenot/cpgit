﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static appt_counselor Manufacture_appt_counselor()
        {
            var ans = new appt_counselor()
            {
                Id              = default(Int32),
                appt_time       = default(Int32),
                inactive        = 0,
                inactive_reason = null,
                counselor       = default(Int32)
            };
            return ans;
        }
    }
}
