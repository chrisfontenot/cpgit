﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static rpps_transaction Manufacture_rpps_transaction()
        {
            var ans = new rpps_transaction()
            {
                bank = null,
                biller_id = null,
                client = null,
                client_creditor = null,
                client_creditor_proposal = null,
                client_creditor_register = null,
                creditor = null,
                death_date = System.DateTime.Now.AddDays(120).Date,
                debt_note = null,
                Id = default(Int32),
                new_account_number = null,
                old_account_number = null,
                old_balance = null,
                prenote_status = null,
                response_batch_id = null,
                return_code = null,
                rpps_batch = null,
                rpps_file = null,
                service_class_or_purpose = null,
                trace_number_first = null,
                trace_number_last = null,
                transaction_code = null
            };
            return ans;
        }
    }
}
