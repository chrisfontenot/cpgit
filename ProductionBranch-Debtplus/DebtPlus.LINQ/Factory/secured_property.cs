﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static secured_property Manufacture_secured_property()
        {
            var ans = new secured_property()
            {
                Id                  = default(Int32),
                secured_type        = DebtPlus.LINQ.Cache.secured_type.getDefault().GetValueOrDefault(),
                description         = string.Empty,
                sub_description     = string.Empty,
                @class              = default(Int32),
                client              = default(Int32),
                current_value       = 0M,
                housing_type        = DebtPlus.LINQ.Cache.HousingType.getDefault().GetValueOrDefault(),
                original_price      = 0M,
                primary_residence   = true,
                school_name         = null,
                general_outcome_id  = DebtPlus.LINQ.Cache.studentLoan_generalOutcome.getDefault(),
                studentLoan_type_id = DebtPlus.LINQ.Cache.studentLoan_type.getDefault(),
                type_of_workout_id  = DebtPlus.LINQ.Cache.studentLoan_workoutType.getDefault(),
                year_acquired       = 0,
                year_mfg            = 0
            };
            return ans;
        }
    }
}
