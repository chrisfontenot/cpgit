﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Create a creditor_note record
        /// </summary>
        public static creditor_note Manufacture_creditor_note()
        {
            var ans = new creditor_note()
            {
                creditor    = null,
                dont_delete = false,
                dont_edit   = false,
                dont_print  = false,
                expires     = null,
                note        = string.Empty,
                prior_note  = null,
                subject     = string.Empty,
                type        = 1,
                Id          = default(Int32)
            };

            return ans;
        }

        /// <summary>
        /// Manufacture a blank creditor_note object
        /// </summary>
        /// <returns>The new creditor_note structure</returns>
        public static creditor_note Manufacture_creditor_note(string CreditorLabel)
        {
            if (string.IsNullOrWhiteSpace(CreditorLabel))
            {
                throw new ArgumentOutOfRangeException("CreditorLabel");
            }
            return Manufacture_creditor_note(CreditorLabel, 1);
        }

        /// <summary>
        /// Manufacture a blank creditor_note object
        /// </summary>
        /// <returns>The new creditor_note structure</returns>
        public static creditor_note Manufacture_creditor_note(string creditorLabel, Int32 NoteType)
        {
            var ans      = Manufacture_creditor_note();
            ans.creditor = creditorLabel;
            ans.type     = NoteType;

            // Temporary notes expire in 30 days
            if (NoteType == 2)
            {
                ans.expires = DateTime.Now.Date.AddDays(30);
            }

            // Alert notes expire in 90 days
            else if (NoteType == 4)
            {
                ans.expires = DateTime.Now.Date.AddDays(90);
            }

            // System notes may not be edited
            else if (NoteType == 3)
            {
                ans.dont_delete = true;
                ans.dont_edit = true;
            }
            return ans;
        }
    }
}
