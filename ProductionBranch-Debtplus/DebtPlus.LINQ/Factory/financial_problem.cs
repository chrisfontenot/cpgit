﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static financial_problem Manufacture_financial_problem()
        {
            var ans = new financial_problem()
            {
                ActiveFlag    = true,
                Default       = false,
                description   = null,
                hpf           = string.Empty,
                Id            = default(Int32),
                nfcc          = string.Empty,
                nfmcp_section = string.Empty
            };
            return ans;
        }
    }
}
