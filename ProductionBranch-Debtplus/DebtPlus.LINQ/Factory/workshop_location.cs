﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static workshop_location Manufacture_workshop_location()
        {
            var ans = new workshop_location()
            {
                ActiveFlag   = true,
                Id           = default(Int32),
                AddressID    = null,
                directions   = null,
                hcs_id       = null,
                milage       = 0D,
                name         = null,
                organization = null,
                TelephoneID  = null,
                type         = null
            };
            return ans;
        }
    }
}
