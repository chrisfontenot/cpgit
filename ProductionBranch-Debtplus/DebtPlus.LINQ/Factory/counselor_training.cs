﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a new counselor_training record
        /// </summary>
        /// <returns></returns>
        public static counselor_training Manufacture_counselor_training()
        {
            var ans = new DebtPlus.LINQ.counselor_training()
            {
                Counselor = default(Int32),
                Id = default(Int32),
                TrainingCertificateDate = null,
                TrainingDate = DateTime.Now.Date,
                TrainingDuration = null,
                TrainingOrganization = null,
                TrainingOrganizationOther = null,
                TrainingSponsor = null,
                TrainingSponsorOther = null,
                TrainingTitle = string.Empty
            };

            return ans;
        }

        /// <summary>
        /// Manufacture a new counselor_training record
        /// </summary>
        /// <returns></returns>
        public static counselor_training Manufacture_counselor_training(Int32 CounselorID)
        {
            var ans = Manufacture_counselor_training();
            ans.Counselor = CounselorID;
            return ans;
        }
    }
}
