﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static client_www_note Manufacture_client_www_note()
        {
            var ans = new client_www_note()
            {
                date_shown  = null,
                date_viewed = null,
                Id          = default(Int32),
                message     = string.Empty,
            };
            return ans;
        }
    }
}
