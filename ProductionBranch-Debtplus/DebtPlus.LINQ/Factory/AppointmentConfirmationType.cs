﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static AppointmentConfirmationType Manufacture_AppointmentConfirmationType()
        {
            var ans = new AppointmentConfirmationType()
            {
                Id               = default(Int32),
                Default          = false,
                ActiveFlag       = true,
                description      = string.Empty,
                note             = string.Empty,
                rpps             = string.Empty
            };
            return ans;
        }
    }
}
