﻿using System;
using System.Collections;
using System.Linq;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Create a client web reference user
        /// </summary>
        public static client_www Manufacture_client_www()
        {
            var ans = new DebtPlus.LINQ.client_www()
            {
                ApplicationName                        = "/",
                Comment                                = null,
                Email                                  = null,
                FailedPasswordAnswerAttemptCount       = 0,
                FailedPasswordAnswerAttemptWindowStart = DateTime.UtcNow,
                FailedPasswordAttemptCount             = 0,
                FailedPasswordAttemptWindowStart       = DateTime.UtcNow,
                Id                                     = System.Guid.NewGuid(),
                IsApproved                             = true,
                IsLockedOut                            = false,
                LastActivityDate                       = DateTime.UtcNow,
                LastLockoutDate                        = null,
                LastLoginDate                          = DateTime.UtcNow,
                LastPasswordChangeDate                 = DateTime.UtcNow,
                Password                               = string.Empty,
                PasswordAnswer                         = string.Empty,
                PasswordQuestion                       = string.Empty,
                UserName                               = null,
                DatabaseKeyID                          = null
            };

            return ans;
        }

        /// <summary>
        /// Create a client web reference user
        /// </summary>
        public static client_www Manufacture_client_www(Int32 ClientID)
        {
            var ans = Manufacture_client_www();
            ans.UserName = ClientID.ToString("0000000");
            return ans;
        }
    }
}
