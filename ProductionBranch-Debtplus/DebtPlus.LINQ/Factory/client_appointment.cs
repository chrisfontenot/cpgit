﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank client_appointment object
        /// </summary>
        /// <returns>The new client_appointment structure</returns>
        public static client_appointment Manufacture_client_appointment()
        {
            DateTime currentTime = DateTime.Now;
            var ans = new client_appointment()
            {
                Id                   = default(Int32),
                appt_time            = null,
                appt_type            = DebtPlus.LINQ.Cache.appt_type.getDefault(),
                bankruptcy_class     = DebtPlus.LINQ.Cache.BankruptcyClassType.getDefault().GetValueOrDefault(),
                callback_ph          = null,
                client               = default(Int32),
                confirmation_status  = DebtPlus.LINQ.Cache.AppointmentConfirmationType.getDefault().GetValueOrDefault(),
                counselor            = DebtPlus.LINQ.BusinessContext.GetCurrentCounselorID(),
                credit               = false,
                date_confirmed       = null,
                date_updated         = currentTime,
                start_time           = currentTime,
                end_time             = currentTime,
                housing              = false,
                HousingFeeAmount     = 0M,
                office               = null,
                partner              = null,
                post_purchase        = false,
                previous_appointment = null,
                priority             = false,
                referred_by          = null,
                referred_to          = null,
                result               = null,
                status               = 'P' /* Pending */,
                date_uploaded        = null,
                workshop             = null,
                workshop_people      = 0
            };
            return ans;
        }

        /// <summary>
        /// Manufacture a blank client_appointment object
        /// </summary>
        /// <param name="ClientID">Associated Client ID</param>
        /// <returns>The new client_appointment structure</returns>
        public static client_appointment Manufacture_client_appointment(Int32 ClientID)
        {
            var ans    = Manufacture_client_appointment();
            ans.client = ClientID;
            return ans;
        }

        /// <summary>
        /// Manufacture a blank client_appointment object
        /// </summary>
        /// <param name="ClientID">Associated Client ID</param>
        /// <param name="AppointmentTimeRecord">Associated appt_time record</param>
        /// <returns>The new client_appointment structure</returns>
        public static client_appointment Manufacture_client_appointment(Int32 ClientID, DebtPlus.LINQ.appt_time appointmentTimeRecord)
        {
            var ans        = Manufacture_client_appointment(ClientID);

            ans.appt_time  = appointmentTimeRecord.Id;
            ans.appt_type  = appointmentTimeRecord.appt_type;
            ans.start_time = appointmentTimeRecord.start_time;
            ans.end_time   = appointmentTimeRecord.start_time;
            ans.office     = appointmentTimeRecord.office;

            return ans;
        }

        /// <summary>
        /// Manufacture a blank client_appointment object
        /// </summary>
        /// <param name="ClientID">Associated Client ID</param>
        /// <param name="appointmentTypeRecord">Pointer to the appointment type record</param>
        /// <returns></returns>
        public static client_appointment Manufacture_client_appointment(Int32 ClientID, DebtPlus.LINQ.appt_type appointmentTypeRecord)
        {
            var ans        = Manufacture_client_appointment(ClientID);
            ans.appt_type  = appointmentTypeRecord.Id;
            ans.end_time   = ans.start_time.AddMinutes(appointmentTypeRecord.appt_duration);
            return ans;
        }
    }
}
