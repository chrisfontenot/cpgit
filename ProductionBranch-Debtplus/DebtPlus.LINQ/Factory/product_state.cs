﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank product_state object
        /// </summary>
        /// <returns>The new product_state structure</returns>
        public static DebtPlus.LINQ.product_state Manufacture_product_state()
        {
            var ans = new DebtPlus.LINQ.product_state()
            {
                Id      = default(Int32),
                Amount  = 0M,
                stateID = DebtPlus.LINQ.Cache.state.getDefault().GetValueOrDefault()
            };

            return ans;
        }
    }
}
