﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static letter_field Manufacture_letter_field()
        {
            var ans = new letter_field()
            {
                field_name = string.Empty,
                formatting = null,
                HasValue = false,
                Id = string.Empty,
                letter_table = Cache.letter_table.getDefault(),
                referenced = false,
                type = default(Int32),
                value = null
            };
            return ans;
        }
    }
}
