﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static HousingType Manufacture_HousingType()
        {
            var ans = new HousingType()
            {
                ActiveFlag          = true,
                Default             = false,
                description         = null,
                hpf                 = string.Empty,
                Id                  = default(Int32),
                SingleFamilyHomeIND = false
            };
            return ans;
        }
    }
}
