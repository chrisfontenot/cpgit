﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static Housing_CounselorContactOutcomeType Manufacture_Housing_CounselorContactOutcomeType()
        {
            var ans = new Housing_CounselorContactOutcomeType()
            {
                Id          = default(Int32),
                ActiveFlag  = true,
                Default     = false,
                description = null,
                hpf         = string.Empty
            };
            return ans;
        }
    }
}
