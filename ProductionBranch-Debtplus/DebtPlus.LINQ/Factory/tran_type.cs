﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static tran_type Manufacture_tran_type()
        {
            var ans = new tran_type()
            {
                Id              = string.Empty,
                sort_order      = 0,
                note            = null,
                description     = string.Empty,
                deposit_subtype = false
            };
            return ans;
        }
    }
}
