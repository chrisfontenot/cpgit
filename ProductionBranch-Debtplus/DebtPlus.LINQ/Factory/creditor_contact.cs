﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank creditor_contact object
        /// </summary>
        /// <returns>The new creditor_contact structure</returns>
        public static creditor_contact Manufacture_creditor_contact()
        {
            var ans = new creditor_contact()
            {
                AddressID = null,
                EmailID = null,
                TelephoneID = null,
                AltTelephoneID = null,
                FAXID = null,
                NameID = null,
                Id = default(Int32),
                creditor = null,
                Notes = null,
                Title = null,
                creditor_contact_type = DebtPlus.LINQ.Cache.creditor_contact_type.getDefault().GetValueOrDefault(),
            };

            return ans;
        }
    }
}
