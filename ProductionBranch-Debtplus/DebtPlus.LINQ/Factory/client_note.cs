﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {

        /// <summary>
        /// Manufacture a blank client_note object
        /// </summary>
        /// <returns>The new client_note structure</returns>
        public static client_note Manufacture_client_note()
        {
            return Manufacture_client_note(-1, (int)DebtPlus.LINQ.InMemory.Notes.NoteTypes.Permanent);
        }

        /// <summary>
        /// Manufacture a blank client_note object
        /// </summary>
        /// <param name="ClientID">Associated Client ID</param>
        /// <returns>The new client_note structure</returns>
        public static client_note Manufacture_client_note(Int32 ClientID)
        {
            if (ClientID < 0)
            {
                throw new ArgumentOutOfRangeException("ClientID");
            }
            return Manufacture_client_note(ClientID, 1);
        }

        /// <summary>
        /// Manufacture a blank client_note object
        /// </summary>
        /// <param name="ClientID">Associated Client ID</param>
        /// <param name="NoteType">Associated Note Type</param>
        /// <returns>The new client_note structure</returns>
        public static client_note Manufacture_client_note(Int32 ClientID, Int32 NoteType)
        {
            var ans = new client_note()
            {
                client          = ClientID,
                client_creditor = null,
                dont_delete     = false,
                dont_edit       = false,
                dont_print      = false,
                expires         = null,
                Id              = 0,
                note            = DebtPlus.LINQ.InMemory.Notes.Default_Note,
                prior_note      = null,
                subject         = string.Empty,
                type            = NoteType
            };

            // Temporary notes expire in 30 days
            if (NoteType == (int)DebtPlus.LINQ.InMemory.Notes.NoteTypes.Temporary)
            {
                ans.expires = DateTime.Now.Date.AddDays(30);
            }

            // Alert notes expire in 90 days
            else if (NoteType == (int)DebtPlus.LINQ.InMemory.Notes.NoteTypes.Alert)
            {
                ans.expires = DateTime.Now.Date.AddDays(90);
            }

            // System notes may not be edited
            else if (NoteType == (int)DebtPlus.LINQ.InMemory.Notes.NoteTypes.System)
            {
                ans.dont_delete = true;
                ans.dont_edit   = true;
            }
            return ans;
        }
    }
}
