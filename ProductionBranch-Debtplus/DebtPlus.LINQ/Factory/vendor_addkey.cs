﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank vendor_addkey object
        /// </summary>
        /// <returns>The new vendor_addkey structure</returns>
        public static DebtPlus.LINQ.vendor_addkey Manufacture_vendor_addkey()
        {
            var ans = new DebtPlus.LINQ.vendor_addkey()
            {
                Id         = default(Int32),
                additional = null
            };

            return ans;
        }

        public static vendor_addkey Manufacture_vendor_addkey(int vendorLabel)
        {
            var ans = Manufacture_vendor_addkey();
            ans.vendor = vendorLabel;
            return ans;
        }
    }
}
