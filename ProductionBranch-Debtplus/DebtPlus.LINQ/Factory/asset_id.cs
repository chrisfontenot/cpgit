﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static asset_id Manufacture_asset_id()
        {
            var ans = new asset_id()
            {
                Id          = default(Int32),
                description = string.Empty,
                hpf         = null,
                maximum     = 0M,
                rpps_code   = 6
            };
            return ans;
        }
    }
}
