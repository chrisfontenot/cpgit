﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static rpps_batch Manufacture_rpps_batch()
        {
            var ans = new rpps_batch()
            {
                biller_name = null,
                death_date = System.DateTime.Now.AddDays(120).Date,
                entry_class = null,
                rpps_biller_id = null,
                rpps_file = null,
                service_class = null,
                trace_number = null,
                Id = default(Int32)
            };

            return ans;
        }

        public static rpps_batch Manufacture_rpps_batch(Int32 fileID)
        {
            var ans = Manufacture_rpps_batch();
            ans.rpps_file = fileID;
            return ans;
        }
    }
}
