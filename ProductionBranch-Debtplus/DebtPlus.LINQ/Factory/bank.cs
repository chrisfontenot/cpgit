﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank bank object
        /// </summary>
        /// <returns>The new bank structure</returns>
        public static bank Manufacture_bank()
        {
            var ans = new DebtPlus.LINQ.bank()
            {
                aba = null,
                account_number = null,
                ach_batch_company_id = null,
                ach_company_id = null,
                ach_company_identification = null,
                ach_enable_offset = false,
                ach_message_authentication = null,
                ach_origin_dfi = null,
                ach_priority = null,
                ActiveFlag = true,
                bank_name = string.Empty,
                BankAddressID = null,
                batch_number = 0,
                checknum = 101,
                ContactNameID = null,
                Default = false,
                description = string.Empty,
                Id = default(Int32),
                immediate_destination = null,
                immediate_destination_name = null,
                immediate_origin = null,
                immediate_origin_name = null,
                max_amt_per_check = 0M,
                max_clients_per_check = 40,
                output_directory = null,
                prefix_line = null,
                SignatureImage = null,
                suffix_line = null,
                transaction_number = 0,
                type = DebtPlus.LINQ.InMemory.bankTypes.getDefault()
            };

            return ans;
        }
    }
}
