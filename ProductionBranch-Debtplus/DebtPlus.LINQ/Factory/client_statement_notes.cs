﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank client_statement_note object
        /// </summary>
        /// <returns>The new client_statement_note structure</returns>
        public static client_statement_note Manufacture_client_statement_note()
        {
            var ans = new client_statement_note()
            {
                client           = default(Int32),
                note             = null,
                Id               = default(Int32),
                client_statement = null
            };
            return ans;
        }
    }
}
