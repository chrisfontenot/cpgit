﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank client_poa object
        /// </summary>
        /// <returns>The new client_poa structure</returns>
        public static client_poa Manufacture_client_poa()
        {
            var ans = new client_poa()
            {
                Id             = default(Int32),
                Financial      = DebtPlus.LINQ.InMemory.validInvalidTypes.getDefault(),
                Other          = DebtPlus.LINQ.InMemory.validInvalidTypes.getDefault(),
                RealEstate     = DebtPlus.LINQ.InMemory.validInvalidTypes.getDefault(),
                ApprovalBy     = string.Empty,
                NameID         = null,
                TelephoneID    = null,
                POA_Expires    = null,
                POA_ID         = string.Empty,
                POA_ID_Expires = null,
                POA_ID_Issued  = null,
                POAType        = DebtPlus.LINQ.Cache.poa_type.getDefault(),
                Stage          = DebtPlus.LINQ.Cache.poa_lookup.getDefault(),
                TelephoneType  = DebtPlus.LINQ.Cache.TelephoneType.getDefault(),

                // These columns are crap! They should be removed ASAP.
                // This table needs to be linked by the "person" field, not by a relation.
                // Even better -- Fold the fields into the people table where they belong and get rid of the whole table!!!
                Client         = default(Int32),
                relation       = default(Int32)
            };
            return ans;
        }
    }
}
