﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Create an initialize an empty HPFOutcome object
        /// </summary>
        /// <returns>The new HPFOutcome object</returns>
        public static HPFOutcome Manufacture_HPFOutcome()
        {
            var answer = new HPFOutcome()
            {
                Id = default(Int32),
                extRefOtherName = null,
                NonProfitReferralName = null,
                propertyID = default(Int32),
                outcomeTypeID = DebtPlus.LINQ.Cache.HPFOutcomeType.getDefault()
            };

            return answer;
        }

        /// <summary>
        /// Create an initialize an empty HPFOutcome object
        /// </summary>
        /// <param name="sessionRecord">The corresponding session for this Outcome</param>
        /// <returns>The new HPFOutcome object</returns>
        public static HPFOutcome Manufacture_HPFOutcome(Int32 propertyID)
        {
            var answer = Manufacture_HPFOutcome();
            answer.propertyID = propertyID;

            return answer;
        }
    }
}
