﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank client_creditor_balance object
        /// </summary>
        /// <returns>The new client_creditor_balance structure</returns>
        public static client_creditor_balance Manufacture_client_creditor_balance()
        {
            var ans = new DebtPlus.LINQ.client_creditor_balance()
            {
                 client_creditor = default(Int32),
                 current_sched_payment = 0M,
                 orig_balance = 0M,
                 orig_balance_adjustment = 0M,
                 payments_month_0 = 0M,
                 payments_month_1 = 0M,
                 total_interest = 0M,
                 total_payments = 0M,
                 total_sched_payment = 0M,
                 zero_bal_status = 0,
                 zero_bal_letter_date = null,
                 Id = default(Int32)
            };
            return ans;
        }
    }
}
