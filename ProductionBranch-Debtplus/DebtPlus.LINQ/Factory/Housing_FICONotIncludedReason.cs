﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static Housing_FICONotIncludedReason Manufacture_Housing_FICONotIncludedReason()
        {
            var ans = new Housing_FICONotIncludedReason()
            {
                Id               = default(Int32),
                description      = string.Empty,
                ActiveFlag       = true,
                Default          = false
            };
            return ans;
        }
    }
}
