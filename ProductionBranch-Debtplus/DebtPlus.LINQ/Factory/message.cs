﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static message Manufacture_message()
        {
            var ans = new message()
            {
                ActiveFlag       = true,
                additional       = null,
                item_type        = null,
                item_value       = default(Int32),
                Default          = false,
                description      = null,
                hud_9902_section = string.Empty,
                Id               = default(Int32),
                rpps             = string.Empty
            };
            return ans;
        }
    }
}
