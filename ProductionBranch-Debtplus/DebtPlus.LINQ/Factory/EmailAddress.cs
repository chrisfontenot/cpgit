﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank EmailAddress object
        /// </summary>
        /// <returns>The new EmailAddress structure</returns>
        public static EmailAddress Manufacture_EmailAddress()
        {
            var ans = new EmailAddress()
            {
                Address = string.Empty,
                ValidationCode = EmailAddress.EmailValidationEnum.Missing
            };

            return ans;
        }

        /// <summary>
        /// Manufacture a blank EmailAddress object
        /// </summary>
        /// <returns>The new EmailAddress structure</returns>
        public static EmailAddress Manufacture_EmailAddress(EmailAddress.EmailValidationEnum Validation)
        {
            // The validation must not be VALID nor INVALID here.
            if (Validation == EmailAddress.EmailValidationEnum.Valid || Validation == EmailAddress.EmailValidationEnum.Invalid)
            {
                throw new System.ArgumentException("Validation can not be VALID nor INVALID without an address");
            }

            var ans = new EmailAddress()
            {
                Address = string.Empty,
                ValidationCode = Validation
            };

            return ans;
        }

        /// <summary>
        /// Manufacture a blank EmailAddress object
        /// </summary>
        /// <returns>The new EmailAddress structure</returns>
        public static EmailAddress Manufacture_EmailAddress(string Address, EmailAddress.EmailValidationEnum Validation)
        {
            // The validation must not be VALID nor INVALID here.
            if (string.IsNullOrWhiteSpace(Address) && (Validation == EmailAddress.EmailValidationEnum.Valid || Validation == EmailAddress.EmailValidationEnum.Invalid))
            {
                throw new System.ArgumentException("Validation can not be VALID nor INVALID without an address");
            }

            var ans = new EmailAddress()
            {
                Address = Address,
                ValidationCode = Validation
            };

            return ans;
        }
    }
}
