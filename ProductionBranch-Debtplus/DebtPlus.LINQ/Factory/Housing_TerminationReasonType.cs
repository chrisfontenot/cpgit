﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static Housing_TerminationReasonType Manufacture_Housing_TerminationReasonType()
        {
            var ans = new Housing_TerminationReasonType()
            {
                ActiveFlag       = true,
                Default          = false,
                description      = null,
                hpf              = string.Empty,
                hud_9902_section = string.Empty,
                Id               = default(Int32)
            };
            return ans;
        }
    }
}
