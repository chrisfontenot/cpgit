﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static asset Manufacture_asset()
        {
            var ans = new asset()
            {
                Id        = default(Int32),
                amount    = 0m,
                client    = default(Int32),
                frequency = DebtPlus.LINQ.Cache.PayFrequencyType.getDefault().GetValueOrDefault(4)
            };
            return ans;
        }
    }
}
