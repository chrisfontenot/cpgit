﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank action_item object
        /// </summary>
        /// <returns>The new action_item structure</returns>
        public static DebtPlus.LINQ.action_item Manufacture_action_item()
        {
            var ans = new DebtPlus.LINQ.action_item()
            {
                Id            = default(Int32),
                item_group    = DebtPlus.LINQ.InMemory.action_itemTypes.getDefault().GetValueOrDefault(1),
                description   = string.Empty
            };

            return ans;
        }
    }
}
