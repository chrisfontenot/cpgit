﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank ach_reject_code object
        /// </summary>
        /// <returns>The new ach_reject_code structure</returns>
        public static DebtPlus.LINQ.ach_reject_code Manufacture_ach_reject_code()
        {
            var ans = new DebtPlus.LINQ.ach_reject_code()
            {
                Id            = null,
                serious_error = false,
                letter_code   = null,
                description   = string.Empty
            };

            return ans;
        }

        /// <summary>
        /// Manufacture a blank ach_reject_code object
        /// </summary>
        /// <param name="Id">Primary key for the record</param>
        /// <returns>The new ach_reject_code structure</returns>
        public static DebtPlus.LINQ.ach_reject_code Manufacture_ach_reject_code(string Id)
        {
            var ans = Manufacture_ach_reject_code();
            ans.Id  = Id;
            return ans;
        }
    }
}
