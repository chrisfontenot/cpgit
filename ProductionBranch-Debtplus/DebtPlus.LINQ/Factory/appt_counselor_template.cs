﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static appt_counselor_template Manufacture_appt_counselor_template()
        {
            var ans = new appt_counselor_template()
            {
                Id                 = default(Int32),
                appt_time_template = default(Int32),
                counselor          = default(Int32)
            };
            return ans;
        }
    }
}
