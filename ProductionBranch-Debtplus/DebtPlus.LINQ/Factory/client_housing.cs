﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank client_housing object
        /// </summary>
        /// <returns>The new client_housing structure</returns>
        public static client_housing Manufacture_client_housing()
        {
            var ans = new client_housing()
            {
                Id = default(Int32),
                BackEnd_DTI = 0D,
                FrontEnd_DTI = 0D,
                DSA_CaseID = null,
                DSA_NoDsaReason = DebtPlus.LINQ.Cache.NoDsaProgramReason.getDefault(),
                DSA_Program = null,
                DSA_ProgramBenefit = false,
                DSA_Specialist = null,
                HECM_Certificate_Date = null,
                HECM_Certificate_Expires = null,
                HECM_Certificate_ID = string.Empty,
                housing_status = DebtPlus.LINQ.Cache.Housing_StatusType.getDefault().GetValueOrDefault(),
                HUD_Assistance = DebtPlus.LINQ.Cache.Housing_HUDAssistanceType.getDefault(),
                HUD_colonias = false,
                HUD_DiscriminationVictim = false,
                HUD_fair_housing = false,
                HUD_FirstTimeHomeBuyer = false,
                HUD_grant = DebtPlus.LINQ.Cache.Housing_GrantType.getDefault().GetValueOrDefault(),
                HUD_home_ownership_voucher = false,
                HUD_housing_voucher = false,
                HUD_LoanScam = false,
                HUD_migrant_farm_worker = false,
                HUD_predatory_lending = false,
                NFMCP_decline_authorization = false,
                NFMCP_level = string.Empty,
                NFMCP_privacy_policy = false,
                OtherHUDFunding = 0M
            };

            return ans;
        }

        /// <summary>
        /// Manufacture a blank client_housing object
        /// </summary>
        /// <param name="ClientID">Client ID (primary key to the table)</param>
        /// <returns>The new client_housing structure</returns>
        public static client_housing Manufacture_client_housing(Int32 ClientID)
        {
            var ans = Manufacture_client_housing();
            ans.Id = ClientID;
            return ans;
        }
    }
}
