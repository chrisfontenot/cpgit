﻿using System;

namespace DebtPlus.LINQ
{
    /// <summary>
    /// Factor class to generate new object records as needed for the LINQ system. This
    /// module contains all of the needed properties to generate the default (blank) class
    /// records. All defaults should be supplied.
    /// </summary>
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank client_disclosures_RMC object
        /// </summary>
        /// <returns>The new client_disclosures_RMC structure</returns>
        public static client_disclosures_RMC Manufacture_client_disclosures_RMC()
        {
            var ans               = new client_disclosures_RMC();
            ans.Id                = default(Int32);
            ans.q1_answer         = 0;
            ans.q2_answer         = 0;
            ans.q3_answer         = 0;
            ans.q4_answer         = 0;
            ans.q5_answer         = 0;
            ans.q6_answer         = 0;
            ans.q7_answer         = 0;
            ans.q8_answer         = 0;
            ans.q9_answer         = 0;
            ans.q10_answer        = 0;
            ans.q11_answer        = 0;
            ans.q12_answer        = 0;
            ans.q13_answer        = 0;
            ans.q14_answer        = 0;
            ans.q15_answer        = 0;
            ans.q16_answer        = 0;
            ans.q17_answer        = 0;
            ans.q18_answer        = 0;
            ans.q19_answer        = 0;

            ans.q15_comments      = string.Empty;
            ans.q20_missing_info  = string.Empty;
            ans.q10_contact       = string.Empty;
            ans.q10_fax           = string.Empty;
            ans.q10_name          = string.Empty;
            ans.q10_telephone     = string.Empty;
            ans.q1_name           = string.Empty;
            ans.q1_relation       = string.Empty;

            return ans;
        }
    }
}
