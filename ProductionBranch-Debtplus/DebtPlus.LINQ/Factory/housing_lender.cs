﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a new housing_lender record
        /// </summary>
        /// <returns></returns>
        public static Housing_lender Manufacture_housing_lender()
        {
            var ans = new DebtPlus.LINQ.Housing_lender()
            {
                AcctNum                   = null,
                AttemptContactDate        = null,
                CaseNumber                = null,
                ContactLenderDate         = null,
                ContactLenderSuccess      = false,
                FdicNcusNum               = null,
                InvestorAccountNumber     = null,
                InvestorID                = DebtPlus.LINQ.Cache.Investor.getDefault(),
                ServicerID                = DebtPlus.LINQ.Cache.Housing_lender_servicer.getDefault(),
                ServicerName              = null,
                WorkoutPlanDate           = null,
                last_updated              = null,
                corporate_advance         = 0M,
                monthly_desired_repay_amt = 0M,
                repay_month               = 0
            };

            return ans;
        }
    }
}
