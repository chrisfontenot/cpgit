﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static secured_type Manufacture_secured_type()
        {
            var ans = new secured_type()
            {
                Id                  = default(Int32),
                description         = string.Empty,
                spanish_description = string.Empty,
                secured_type_group  = DebtPlus.LINQ.Cache.secured_type_group.getDefault().GetValueOrDefault(0)
            };
            return ans;
        }
    }
}
