﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank debt_note object
        /// </summary>
        /// <returns>The new debt_note structure</returns>
        public static debt_note Manufacture_debt_note()
        {
            var ans = new DebtPlus.LINQ.debt_note()
            {
                Id                       = default(Int32),
                bank                     = null,
                client                   = null,
                client_creditor          = null,
                client_creditor_proposal = null,
                creditor                 = null,
                creditor_method          = null,
                drop_reason              = null,
                note_amount              = 0M,
                note_date                = DateTime.Now,
                note_text                = string.Empty,
                output_batch             = null,
                rpps_biller_id           = null,
                rpps_transaction         = null,
                type                     = DebtPlus.LINQ.debt_note.Type_AccountNote
            };

            return ans;
        }

        /// <summary>
        /// Manufacture a blank debt_note object
        /// </summary>
        /// <returns>The new debt_note structure</returns>
        public static debt_note Manufacture_debt_note(Int32 ClientID, Int32 Client_Creditor)
        {
            var ans = Manufacture_debt_note();
            ans.client = ClientID;
            ans.client_creditor = Client_Creditor;
            return ans;
        }
    }
}
