﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static NoDsaProgramReason Manufacture_NoDsaProgramReason()
        {
            var ans = new NoDsaProgramReason()
            {
                Id               = default(Int32),
                ActiveFlag       = true,
                @default         = false,
                description      = string.Empty,
                hpf              = null
            };
            return ans;
        }
    }
}
