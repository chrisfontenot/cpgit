﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a new rpps_reject_code record
        /// </summary>
        /// <returns></returns>
        public static rpps_reject_code Manufacture_rpps_reject_code()
        {
            var ans = new DebtPlus.LINQ.rpps_reject_code()
            {
                Id           = null,
                resend_funds = false,
                description  = string.Empty
            };
            return ans;
        }
    }
}
