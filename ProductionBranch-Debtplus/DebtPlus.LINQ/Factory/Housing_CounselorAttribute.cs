﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a new Housing_CounselorAttribute record
        /// </summary>
        /// <returns></returns>
        public static Housing_CounselorAttribute Manufacture_Housing_CounselorAttribute()
        {
            var ans = new DebtPlus.LINQ.Housing_CounselorAttribute()
            {
                Id               = default(Int32),
                Counselor        = default(Int32),
                HousingAttribute = default(Int32)
            };

            return ans;
        }

        /// <summary>
        /// Manufacture a new Housing_CounselorAttribute record
        /// </summary>
        /// <returns></returns>
        public static Housing_CounselorAttribute Manufacture_Housing_CounselorAttribute(Int32 CounselorID, Int32 HousingAttributeID)
        {
            var ans              = Manufacture_Housing_CounselorAttribute();
            ans.Counselor        = CounselorID;
            ans.HousingAttribute = HousingAttributeID;
            return ans;
        }
    }
}
