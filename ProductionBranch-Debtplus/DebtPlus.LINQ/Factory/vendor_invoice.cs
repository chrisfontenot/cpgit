﻿using System;

namespace DebtPlus.LINQ
{
    partial class Factory
    {
        /// <summary>
        /// Manufacture a blank vendor_invoice object
        /// </summary>
        /// <returns>The new vendor_invoice structure</returns>
        public DebtPlus.LINQ.vendor_invoice Manufacture_vendor_invoice()
        {
            var ans = new DebtPlus.LINQ.vendor_invoice()
            {
                Id          = default(Int32),
                vendor      = default(Int32),
                date_posted = null,
                batchID     = null
            };
            return ans;
        }
    }
}
