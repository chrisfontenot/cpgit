﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank client_creditor_proposal object
        /// </summary>
        /// <returns>The new client_creditor_proposal structure</returns>
        public static client_creditor_proposal Manufacture_client_creditor_proposal()
        {
            var ans = new DebtPlus.LINQ.client_creditor_proposal()
            {
                bank                            = null,
                client_creditor                 = default(Int32),
                counter_amount                  = 0M,
                debt_notes                      = null,
                full_disclosure                 = false,
                missing_item                    = null,
                percent_balance                 = null,

                proposal_accepted_by            = null,
                proposal_print_date             = null,
                proposal_reject_disp            = null,
                proposal_reject_reason          = null,
                proposal_status                 = 1,
                proposal_status_date            = null,
                proposed_amount                 = null,
                proposed_balance                = null,
                proposed_date                   = DateTime.Now.AddDays(30).Date,
                proposed_start_date             = null,

                rpps_biller_id                  = null,
                rpps_client_type_indicator      = null,
                rpps_cycle_months_remaining     = null,
                rpps_first_payment_date         = null,
                rpps_forgiven_pct               = null,
                rpps_good_thru_date             = null,
                rpps_ineligible_reason          = null,
                rpps_interest_rate              = null,
                rpps_internal_program_ends_date = null,
                rpps_resubmit_date              = null,
                rpps_third_party_contact        = null,
                rpps_third_party_detail         = null,

                terms                           = null,
                Id                              = default(Int32)
            };

            return ans;
        }

        /// <summary>
        /// Manufacture a blank client_creditor_proposal object
        /// </summary>
        /// <returns>The new client_creditor_proposal structure</returns>
        public static client_creditor_proposal Manufacture_client_creditor_proposal(Int32 client_creditor)
        {
            var ans = Manufacture_client_creditor_proposal();
            ans.client_creditor = client_creditor;
            return ans;
        }

        /// <summary>
        /// Manufacture a blank client_creditor_proposal object
        /// </summary>
        /// <returns>The new client_creditor_proposal structure</returns>
        public static client_creditor_proposal Manufacture_client_creditor_proposal(Int32 client_creditor, Int32 proposal_status)
        {
            var ans = Manufacture_client_creditor_proposal(client_creditor);
            ans.proposal_status = proposal_status;
            return ans;
        }
    }
}
