﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank BankruptcyClassType object
        /// </summary>
        /// <returns>The new BankruptcyClassType structure</returns>
        public static DebtPlus.LINQ.BankruptcyClassType Manufacture_BankruptcyClassType()
        {
            var ans = new DebtPlus.LINQ.BankruptcyClassType()
            {
                Id          = default(Int32),
                description = string.Empty,
                ActiveFlag  = true,
                Default     = false
            };

            return ans;
        }
    }
}
