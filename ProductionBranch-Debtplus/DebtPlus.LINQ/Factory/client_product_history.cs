﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank client_product object
        /// </summary>
        /// <returns>The new client_product structure</returns>
        public static DebtPlus.LINQ.client_products_history Manufacture_client_products_history()
        {
            var ans = new DebtPlus.LINQ.client_products_history()
            {
                Id                = default(Int32),
                cost              = 0M,
                discount          = 0M,
                disputed_amount   = 0M,
                tendered          = 0M,
                disputed_status   = null,
                disputed_note     = null,
                resolution_note   = null,
                disputed_date     = null,
                resolution_date   = null,
                resolution_status = null,
                invoice_status    = DebtPlus.LINQ.client_product.invoice_status_none,
                invoice_date      = null,
                vendor_label      = string.Empty,
                vendor            = default(Int32)
            };
            return ans;
        }
    }
}
