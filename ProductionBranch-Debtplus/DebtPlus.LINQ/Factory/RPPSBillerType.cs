﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static RPPSBillerType Manufacture_RPPSBillerType()
        {
            var ans = new RPPSBillerType()
            {
                ActiveFlag       = true,
                Default          = false,
                description      = null,
                Id               = default(Int32)
            };
            return ans;
        }
    }
}
