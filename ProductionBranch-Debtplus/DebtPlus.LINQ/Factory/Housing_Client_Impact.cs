﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static Housing_Client_Impact Manufacture_Housing_Client_Impact()
        {
            var ans = new Housing_Client_Impact()
            {
                Id                     = default(Int32),
                ImpactId               = default(Int32),
                POVId                  = default(Int32),
                Housing_HUD_ImpactType = null,
                hud_interview          = null
            };
            return ans;
        }
    }
}
