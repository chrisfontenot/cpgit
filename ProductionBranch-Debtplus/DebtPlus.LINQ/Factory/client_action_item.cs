﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank client_action_item object
        /// </summary>
        /// <returns>The new client_action_item structure</returns>
        public static client_action_item Manufacture_client_action_item()
        {
            var ans = new client_action_item()
            {
                action_item = default(Int32),
                @checked    = true,
                Id          = default(Int32)
            };
            return ans;
        }
    }
}
