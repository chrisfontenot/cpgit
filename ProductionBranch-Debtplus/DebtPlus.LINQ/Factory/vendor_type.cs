﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank vendor_type object
        /// </summary>
        /// <returns>The new vendor_type structure</returns>
        public static DebtPlus.LINQ.vendor_type Manufacture_vendor_type()
        {
            var ans = new DebtPlus.LINQ.vendor_type()
            {
                Id          = string.Empty,
                ActiveFlag  = true,
                Default     = false,
                description = string.Empty
            };
            return ans;
        }
    }
}
