﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static Housing_ReasonForCallType Manufacture_Housing_ReasonForCallType()
        {
            var ans = new Housing_ReasonForCallType()
            {
                ActiveFlag  = true,
                Default     = false,
                description = string.Empty,
                hpf         = null,
                Id          = default(Int32)
            };
            return ans;
        }
    }
}
