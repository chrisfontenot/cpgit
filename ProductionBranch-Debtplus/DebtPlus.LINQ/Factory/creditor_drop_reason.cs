﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank creditor_drop_reason object
        /// </summary>
        /// <returns>The new creditor_drop_reason structure</returns>
        public static DebtPlus.LINQ.creditor_drop_reason Manufacture_creditor_drop_reason()
        {
            var ans = new DebtPlus.LINQ.creditor_drop_reason()
            {
                creditor = null,
                rpps_code = null,
                Id = default(Int32)
            };

            return ans;
        }

        public static DebtPlus.LINQ.creditor_drop_reason Manufacture_creditor_drop_reason(string creditorLabel, string RPPSCode)
        {
            var ans = Manufacture_creditor_drop_reason();
            ans.creditor = creditorLabel;
            ans.rpps_code = RPPSCode;
            return ans;
        }
    }
}
