﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a new hecm_default_lookup record
        /// </summary>
        /// <returns></returns>
        public static hecm_default_lookup Manufacture_hecm_default_lookup()
        {
            var ans = new DebtPlus.LINQ.hecm_default_lookup()
            {
                Id = default(Int32),
                Default = false,
                description = string.Empty,
                LookupType = 1
            };

            return ans;
        }
    }
}
