﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static drop_reason Manufacture_drop_reason()
        {
            var ans = new drop_reason()
            {
                ActiveFlag  = true,
                Default     = false,
                description = string.Empty,
                Id          = default(Int32)
            };
            return ans;
        }
    }
}
