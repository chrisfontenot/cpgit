﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static config_fee Manufacture_config_fee()
        {
            var ans = new config_fee()
            {
                ActiveFlag        = true,
                Default           = false,
                description       = string.Empty,
                preferred         = false,
                fee_balance_inc   = null,
                fee_balance_max   = false,
                fee_base          = default(decimal),
                fee_disb_max      = null,
                fee_maximum       = null,
                fee_monthly_max   = null,
                fee_per_creditor  = null,
                fee_percent       = null,
                fee_sched_payment = null,
                fee_type          = 1,
                Id                = default(Int32),
                state             = DebtPlus.LINQ.Cache.state.getDefault()
            };
            return ans;
        }
    }
}
