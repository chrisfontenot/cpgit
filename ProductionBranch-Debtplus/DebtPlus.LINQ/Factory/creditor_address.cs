﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank creditor_address object
        /// </summary>
        /// <returns>The new creditor_address structure</returns>
        public static creditor_address Manufacture_creditor_address()
        {
            var ans = new creditor_address()
                {
                    AddressID = null,
                    attn      = string.Empty,
                    Id        = default(Int32),
                    creditor  = null,
                    type      = DebtPlus.LINQ.InMemory.creditorAddressTypes.PaymentTypeKey
                };
            return ans;
        }

        /// <summary>
        /// Manufacture a blank creditor_address object
        /// </summary>
        /// <param name="creditorLabel">Associated Creditor ID</param>
        /// <param name="addressType">Type of address</param>
        /// <returns>The new creditor_address structure</returns>
        public static creditor_address Manufacture_creditor_address(string creditorLabel, string addressType)
        {
            var ans = Manufacture_creditor_address();
            ans.creditor = creditorLabel;
            ans.type = addressType;

            return ans;
        }
    }
}
