﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank client_retention_event object
        /// </summary>
        /// <returns>The new client_retention_event structure</returns>
        public static client_retention_event Manufacture_client_retention_event()
        {
            var ans = new client_retention_event()
            {
                amount          = 0m,
                client          = default(Int32),
                date_expired    = null,
                effective_date  = DateTime.Now.Date,
                expire_date     = null,
                expire_type     = 1,
                hud_interview   = null,
                Id              = default(Int32),
                message         = string.Empty,
                priority        = 9,
                retention_event = DebtPlus.LINQ.Cache.retention_event.getDefault().GetValueOrDefault()
            };
            return ans;
        }

        /// <summary>
        /// Manufacture a blank client_retention_event object
        /// </summary>
        /// <param name="ClientId">Client ID to be associated with the event</param>
        /// <returns>The new client_retention_event structure</returns>
        public static client_retention_event Manufacture_client_retention_event(Int32 ClientId)
        {
            var ans    = Manufacture_client_retention_event();
            ans.client = ClientId;
            return ans;
        }
    }
}
