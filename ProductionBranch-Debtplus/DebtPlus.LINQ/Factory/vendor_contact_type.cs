﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank vendor_contact_type object
        /// </summary>
        /// <returns>The new vendor_contact_type structure</returns>
        public static DebtPlus.LINQ.vendor_contact_type Manufacture_vendor_contact_type()
        {
            var ans = new DebtPlus.LINQ.vendor_contact_type()
            {
                Id          = default(Int32),
                ActiveFlag  = true,
                Default     = false,
                description = null
            };

            return ans;
        }
    }
}
