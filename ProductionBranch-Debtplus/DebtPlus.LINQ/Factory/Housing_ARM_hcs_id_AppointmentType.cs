﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static Housing_ARM_hcs_id_AppointmentType Manufacture_Housing_ARM_hcs_id_AppointmentType()
        {
            var ans = new Housing_ARM_hcs_id_AppointmentType()
            {
                Id              = default(Int32),
                AppointmentType = "F"
            };
            return ans;
        }
    }
}
