﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a new resource_region record
        /// </summary>
        /// <returns></returns>
        public static resource_region Manufacture_resource_region()
        {
            var ans = new DebtPlus.LINQ.resource_region()
            {
                Id       = default(Int32),
                resource = default(Int32),
                office   = default(Int32)
            };
            return ans;
        }
    }
}
