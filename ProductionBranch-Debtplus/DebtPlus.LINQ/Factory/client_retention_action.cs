﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank client_retention_action object
        /// </summary>
        /// <returns>The new client_retention_action structure</returns>
        public static client_retention_action Manufacture_client_retention_action()
        {
            var ans = new client_retention_action()
            {
                amount                 = 0m,
                client_retention_event = default(Int32),
                message                = string.Empty,
                retention_action       = DebtPlus.LINQ.Cache.retention_action.getDefault().GetValueOrDefault()
            };
            return ans;
        }
    }
}
