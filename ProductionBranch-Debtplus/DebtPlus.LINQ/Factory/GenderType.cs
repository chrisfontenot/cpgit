﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static GenderType Manufacture_GenderType()
        {
            var ans = new GenderType()
            {
                ActiveFlag       = true,
                Default          = false,
                description      = null,
                hpf              = string.Empty,
                Id               = default(Int32),
                nfmcp_section    = string.Empty
            };
            return ans;
        }
    }
}
