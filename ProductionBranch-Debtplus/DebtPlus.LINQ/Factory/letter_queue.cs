﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static letter_queue Manufacture_letter_queue()
        {
            var ans = new letter_queue()
            {
                client           = default(Int32),
                Id               = default(Int32),
                bottom_margin    = 0D,
                creditor         = null,
                date_printed     = null,
                death_date       = null,
                formatted_letter = null,
                left_margin      = 0D,
                letter_fields    = null,
                letter_type      = DebtPlus.LINQ.Cache.letter_type.getDefault().GetValueOrDefault(),
                priority         = 9,
                queue_name       = null,
                right_margin     = 0D,
                sort_order       = string.Empty,
                top_margin       = 0D
            };
            return ans;
        }
    }
}
