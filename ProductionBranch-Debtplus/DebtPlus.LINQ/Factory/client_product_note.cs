﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank client_product_note object
        /// </summary>
        /// <returns>The new client_product_note structure</returns>
        public static client_product_note Manufacture_client_product_note()
        {
            var ans = new DebtPlus.LINQ.client_product_note()
            {
                client_product  = default(Int32),
                dont_delete     = false,
                dont_edit       = false,
                dont_print      = false,
                Id              = default(Int32),
                note            = DebtPlus.LINQ.InMemory.Notes.Default_Note,
                subject         = string.Empty,
                type            = (System.Int32) DebtPlus.LINQ.InMemory.Notes.NoteTypes.System
            };

            return ans;
        }
    }
}
