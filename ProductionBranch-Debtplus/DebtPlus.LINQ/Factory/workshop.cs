﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static workshop Manufacture_workshop()
        {
            var ans = new workshop()
            {
                Id                = default(Int32),
                counselor         = null,
                CounselorID       = null,
                Guest             = null,
                HousingFeeAmount  = 0M,
                HUD_Grant         = DebtPlus.LINQ.Cache.Housing_GrantType.getDefault(),
                seats_available   = 30,
                start_time        = DateTime.Now.Date.AddDays(7),
                workshop_location = DebtPlus.LINQ.Cache.workshop_location.getDefault().GetValueOrDefault(),
                workshop_type     = DebtPlus.LINQ.Cache.workshop_type.getDefault().GetValueOrDefault()
            };
            return ans;
        }
    }
}
