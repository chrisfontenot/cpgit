﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static creditor_type Manufacture_creditor_type()
        {
            // Find the default contribution cycle
            var default_cycleID = DebtPlus.LINQ.InMemory.contribCycleTypes.getDefault();
            var default_cycle   = DebtPlus.LINQ.InMemory.contribCycleTypes.getList().Find(s => s.Id == default_cycleID);

            var ans = new creditor_type()
            {
                Id                       = '\0',
                contribution_pct         = 0D,
                nfcc_type                = null,
                proposals                = DebtPlus.LINQ.InMemory.ProposalStatusTypes.getDefault(),
                send_car_letter          = null,
                send_contribution_letter = null,
                send_pledge_letter       = null,
                sic_prefix               = null,

                // The cycle and status come from the default contribution record.
                cycle                    = default_cycle == null ? 'M' : default_cycle.Cycle,
                status                   = default_cycle == null ? 'D' : default_cycle.Status
            };
            return ans;
        }
    }
}
