﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Create a creditor web reference user
        /// </summary>
        public static creditor_www Manufacture_creditor_www()
        {
            var ans = new DebtPlus.LINQ.creditor_www()
            {
                Id = null,
                password = MD5(CreatePassword())
            };
            return ans;
        }

        /// <summary>
        /// Create a creditor web reference user
        /// </summary>
        public static creditor_www Manufacture_creditor_www(string creditorID)
        {
            var ans = Manufacture_creditor_www();
            ans.Id = creditorID;
            return ans;
        }
    }
}
