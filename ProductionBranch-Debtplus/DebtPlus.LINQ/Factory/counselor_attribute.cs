﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a new counselor_attribute record
        /// </summary>
        /// <returns></returns>
        public static counselor_attribute Manufacture_counselor_attribute()
        {
            var ans = new DebtPlus.LINQ.counselor_attribute()
            {
                Id        = default(Int32),
                Counselor = default(Int32),
                Attribute = default(Int32)
            };

            return ans;
        }

        /// <summary>
        /// Manufacture a new counselor_attribute record
        /// </summary>
        /// <returns></returns>
        public static counselor_attribute Manufacture_counselor_attribute(Int32 CounselorID, Int32 AttributeID)
        {
            var ans       = Manufacture_counselor_attribute();
            ans.Counselor = CounselorID;
            ans.Attribute = AttributeID;
            return ans;
        }
    }
}
