﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank HPFPrivacyPolicy object
        /// </summary>
        /// <returns>The new HPFPrivacyPolicy structure</returns>
        public static DebtPlus.LINQ.HPFPrivacyPolicy Manufacture_HPFPrivacyPolicy()
        {
            var ans = new DebtPlus.LINQ.HPFPrivacyPolicy()
            {
                Id = default(Int32),
				client = default(Int32),
				address = null,
				print_queue = null,
				print_date = null,
				death_date = null,
				email_queueID = null
            };

            return ans;
        }
		
        /// <summary>
        /// Manufacture a blank HPFPrivacyPolicy object
        /// </summary>
        /// <returns>The new HPFPrivacyPolicy structure</returns>
        public static DebtPlus.LINQ.HPFPrivacyPolicy Manufacture_HPFPrivacyPolicy(Int32 ClientID)
        {
            var ans = Manufacture_HPFPrivacyPolicy();
			ans.client = ClientID;
			return ans;
		}
    }
}
