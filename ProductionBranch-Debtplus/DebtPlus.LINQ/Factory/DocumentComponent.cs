﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static DocumentComponent Manufacture_DocumentComponent()
        {
            var ans = new DocumentComponent()
            {
                Id                   = default(Int32),
                DocumentID           = default(Int32),
                ComponentLocationURI = "FILE://",
                ComponentType        = "RTF",
                Document             = null,
                EffectiveExpireDate  = null,
                EffectiveStartDate   = DateTime.Now.Date,
                RevisionDate         = DateTime.Now.Date,
                PageEjectBeforeFLG   = false,
                SectionID            = "DETAIL",
                SequenceID           = default(Int32)
            };
            return ans;
        }
    }
}
