﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static letter_type Manufacture_letter_type()
        {
            var ans = new letter_type()
            {
                Id            = default(Int32),
                display_mode  = DebtPlus.LINQ.InMemory.letterDisplayModes.getDefault().GetValueOrDefault(),
                language      = DebtPlus.LINQ.Cache.AttributeType.getDefaultLanguage(),
                letter_group  = DebtPlus.LINQ.InMemory.LetterGroupTypes.getDefault(),
                region        = DebtPlus.LINQ.Cache.region.getDefault().GetValueOrDefault(),
                Default       = false,
                description   = string.Empty,
                filename      = string.Empty,
                letter_code   = null,
                menu_name     = null,
                queue_name    = null,
                bottom_margin = 0.25D,
                left_margin   = 0.25D,
                right_margin  = 0.25D,
                top_margin    = 0.25D
            };
            return ans;
        }
    }
}
