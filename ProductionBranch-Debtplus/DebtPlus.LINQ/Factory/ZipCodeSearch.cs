﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank ZipCodeSearch object
        /// </summary>
        /// <returns>The new ZipCodeSearch structure</returns>
        public static ZipCodeSearch Manufacture_ZipCodeSearch()
        {
            var ans = new DebtPlus.LINQ.ZipCodeSearch()
            {
                ActiveFlag = false,
                Id         = default(Int32),
                CityName   = string.Empty,
                ZIPCode    = string.Empty,
                MSACode    = null,
                AreaCode   = null,
                County     = null,
                Latitude   = null,
                Longitude = null,
                TimeZone   = null,
                ZIPType    = 'S',
                CityType   = 'N'
            };
            return ans;
        }
    }
}
