﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static letter_table Manufacture_letter_table()
        {
            var ans = new letter_table()
            {
                Id         = default(Int32),
                query      = null,
                query_type = DebtPlus.LINQ.InMemory.LetterQueryTypes.getDefault().Value
            };
            return ans;
        }
    }
}
