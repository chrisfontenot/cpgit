﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank client_statement_batch object
        /// </summary>
        /// <returns>The new client_statement_batch structure</returns>
        public static client_statement_batch Manufacture_client_statement_batch()
        {
            var ans = new client_statement_batch()
            {
                disbursement_date = default(Int32),
                Id                = default(Int32),
                note              = null,
                period_end        = default(DateTime),
                period_start      = default(DateTime),
                statement_date    = System.DateTime.Now.Date
            };
            return ans;
        }
    }
}
