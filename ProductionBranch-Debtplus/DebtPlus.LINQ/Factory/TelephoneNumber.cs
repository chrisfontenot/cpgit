﻿using System;
using System.Linq;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture and empty TelephoneNumber object
        /// </summary>
        /// <param name="propertyRecord">The new TelephoneNumber record</param>
        /// <returns>The Name object</returns>
        public static TelephoneNumber Manufacture_TelephoneNumber()
        {
            // Find the default area code for the item
            var q = DebtPlus.LINQ.Cache.config.getList().FirstOrDefault();
            if (q == null)
            {
                q = Manufacture_config();
            }

            // Generate the empty telephone number
            var answer = new DebtPlus.LINQ.TelephoneNumber()
            {
                Id = default(Int32),
                Country = DebtPlus.LINQ.Cache.country.getDefault(),
                Acode = q.areacode ?? string.Empty,
                Ext = string.Empty,
                Number = string.Empty
            };

            return answer;
        }
    }
}
