﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static product_dispute_type Manufacture_product_dispute_type()
        {
            var ans = new product_dispute_type()
            {
                Description = string.Empty,
                Id          = default(Int32),
                ActiveFlag  = true
            };
            return ans;
        }
    }
}
