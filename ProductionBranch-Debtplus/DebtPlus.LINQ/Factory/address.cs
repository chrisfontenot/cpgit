﻿using System;
using System.Linq;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank address object
        /// </summary>
        /// <returns>The new address structure</returns>
        public static address Manufacture_address()
        {
            return Manufacture_address(false, false, false, false);
        }

        /// <summary>
        /// Manufacture a blank address object
        /// </summary>
        /// <returns>The new address structure</returns>
        public static address Manufacture_address(bool ShowAttn, bool ShowCreditor1, bool ShowCreditor2, bool ShowLine3)
        {
            // Set the default modifier value from the tables.
            var abrev = (from p in DebtPlus.LINQ.Cache.postal_abbreviation.getList() where p.type == 3 && p.Default select p).FirstOrDefault();

            // Generate the address record
            var ans = new address(ShowAttn, ShowCreditor1, ShowCreditor2, ShowLine3)
            {
                Id                = default(Int32),
                creditor_prefix_1 = string.Empty,
                creditor_prefix_2 = string.Empty,
                attn              = string.Empty,
                house             = string.Empty,
                direction         = string.Empty,
                street            = string.Empty,
                suffix            = string.Empty,
                modifier          = abrev != null ? abrev.abbreviation : string.Empty,
                modifier_value    = string.Empty,
                address_line_2    = string.Empty,
                address_line_3    = string.Empty,
                city              = string.Empty,
                state             = DebtPlus.LINQ.Cache.state.getDefault().GetValueOrDefault(),
                PostalCode        = string.Empty,
                USDA_Status       = '?',

                // These are items which should not be in the address record.
                // They should be looked up with each needed reference.
                GMTOffset         = -5,
                StateProvince     = null,
                TzDescriptor      = string.Empty
            };
            return ans;
        }
    }
}
