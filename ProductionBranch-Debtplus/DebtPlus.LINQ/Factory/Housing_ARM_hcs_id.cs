﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static Housing_ARM_hcs_id Manufacture_Housing_ARM_hcs_id()
        {
            var ans = new Housing_ARM_hcs_id()
            {
                Id                  = default(Int32),
                ActiveFlag          = true,
                colonias            = false,
                counseling_amount   = 0M,
                Default             = false,
                description         = string.Empty,
                faith_based         = false,
                migrant_farm_worker = false,
                Password            = null,
                SendClients         = false,
                SendWorkshops       = false,
                Username            = null,
                validation_date     = null
            };
            return ans;
        }
    }
}
