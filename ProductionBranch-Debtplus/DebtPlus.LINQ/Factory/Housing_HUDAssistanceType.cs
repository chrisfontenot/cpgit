﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Create a new Housing_HUDAssistanceType record type
        /// </summary>
        /// <returns></returns>
        public static Housing_HUDAssistanceType Manufacture_Housing_HUDAssistanceType()
        {
            var ans = new Housing_HUDAssistanceType()
            {
                Id               = default(Int32),
                ActiveFlag       = true,
                Default          = false,
                description      = string.Empty,
                hud_9902_section = null
            };
            return ans;
        }
    }
}
