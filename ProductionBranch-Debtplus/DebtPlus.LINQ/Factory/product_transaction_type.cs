﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank product_transaction_type object
        /// </summary>
        /// <returns>The new product_transaction_type structure</returns>
        public static DebtPlus.LINQ.product_transaction_type Manufacture_product_transaction_type()
        {
            var ans = new DebtPlus.LINQ.product_transaction_type()
            {
                Id          = string.Empty,
                Description = string.Empty
            };
            return ans;
        }
    }
}
