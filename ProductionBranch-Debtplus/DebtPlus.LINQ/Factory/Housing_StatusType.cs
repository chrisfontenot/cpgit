﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static Housing_StatusType Manufacture_Housing_StatusType()
        {
            var ans = new Housing_StatusType()
            {
                ActiveFlag       = true,
                Default          = false,
                description      = null,
                Id               = default(Int32)
            };
            return ans;
        }
    }
}
