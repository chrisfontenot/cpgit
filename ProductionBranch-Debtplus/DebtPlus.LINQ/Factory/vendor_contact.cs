﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank vendor_contact object
        /// </summary>
        /// <returns>The new vendor_contact structure</returns>
        public static DebtPlus.LINQ.vendor_contact Manufacture_vendor_contact()
        {
            var ans = new DebtPlus.LINQ.vendor_contact()
            {
                Id           = default(Int32),
                addressID    = null,
                contact_type = DebtPlus.LINQ.Cache.vendor_contact_type.getDefault().GetValueOrDefault(),
                emailID      = null,
                faxID        = null,
                nameID       = null,
                note         = null,
                telephoneID  = null
            };

            return ans;
        }

      
    }
}
