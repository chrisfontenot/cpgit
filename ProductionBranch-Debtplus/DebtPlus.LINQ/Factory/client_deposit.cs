﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a new client_deposit record
        /// </summary>
        /// <returns></returns>
        public static client_deposit Manufacture_client_deposit()
        {
            DateTime now = DateTime.Now.AddDays(10);
            var ans = new DebtPlus.LINQ.client_deposit()
            {
                ach_pull       = true,
                one_time       = false,
                deposit_amount = 0M,
                deposit_date   = new DateTime(now.Year, now.Month, 1).AddMonths(1)
            };
            return ans;
        }

        /// <summary>
        /// Manufacture a new client_deposit record
        /// </summary>
        /// <returns></returns>
        public static client_deposit Manufacture_client_deposit(Int32 clientID)
        {
            var ans    = Manufacture_client_deposit();
            ans.client = clientID;
            return ans;
        }
    }
}
