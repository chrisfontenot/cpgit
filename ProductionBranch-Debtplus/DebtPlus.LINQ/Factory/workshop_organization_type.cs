﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static workshop_organization_type Manufacture_workshop_organization_type()
        {
            var ans = new workshop_organization_type()
            {
                ActiveFlag = true,
                Id         = default(Int32),
                name       = null
            };
            return ans;
        }
    }
}
