﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static TicklerType Manufacture_TicklerType()
        {
            var ans = new TicklerType()
            {
                ActiveFlag       = true,
                Default          = false,
                description      = null,
                Id               = default(Int32)
            };
            return ans;
        }
    }
}
