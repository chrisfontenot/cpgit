﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static appt_time Manufacture_appt_time()
        {
            var ans = new appt_time()
            {
                Id         = default(Int32),
                appt_type  = null,
                start_time = new DateTime(1980,1,1),
                office     = default(Int32)
            };
            return ans;
        }
    }
}
