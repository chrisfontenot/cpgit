﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static appt_time_template Manufacture_appt_time_template()
        {
            var ans = new appt_time_template()
            {
                Id         = default(Int32),
                appt_type  = null,
                start_time = 8 * 60,
                dow        = default(Int32),
                office     = default(Int32)
            };
            return ans;
        }
    }
}
