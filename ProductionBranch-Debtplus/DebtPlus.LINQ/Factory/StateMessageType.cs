﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank StateMessageType object
        /// </summary>
        /// <returns>The new StateMessageType structure</returns>
        public static StateMessageType Manufacture_StateMessageType()
        {
            var ans = new StateMessageType()
            {
                Id          = default(Int32),
                Description = string.Empty,
                ActiveFlag  = true,
                Default     = false
            };
            return ans;
        }
    }
}
