﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static Housing_PurposeOfVisitType Manufacture_Housing_PurposeOfVisitType()
        {
            var ans = new Housing_PurposeOfVisitType()
            {
                ActiveFlag            = true,
                Default               = false,
                description           = null,
                hud_9902_section      = string.Empty,
                Id                    = default(Int32),
                HourlyGrantAmountUsed = 0M,
                HousingFeeAmount      = 0M
            };
            return ans;
        }
    }
}
