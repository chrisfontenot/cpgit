﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static Housing_GrantType Manufacture_Housing_GrantType()
        {
            var ans = new Housing_GrantType()
            {
                Id               = default(Int32),
                description      = string.Empty,
                ActiveFlag       = true,
                Default          = false,
                hud_9902_section = null
            };
            return ans;
        }
    }
}
