﻿using System;

namespace DebtPlus.LINQ
{
    partial class Factory
    {
        public static readonly string DEFAULT_ACH_BATCH_TYPE = "TEL";

        public static ach_onetime Manufacture_ach_onetime()
        {
            var ans = new ach_onetime()
            {
                Id                   = default(Int32),
                ABA                  = null,
                AccountNumber        = null,
                Amount               = 0m,
                CheckingSavings      = 'C',
                client               = default(Int32),
                deposit_batch_date   = null,
                deposit_batch_id     = null,
                batch_type           = DEFAULT_ACH_BATCH_TYPE,
                bank                 = DebtPlus.LINQ.Cache.bank.getDefault("A").GetValueOrDefault(4),
                client_product       = null,
                temp_fhlb_email      = null,
                temp_fhlb_firm_name  = null,
                temp_fhlb_firm_phone = null,
                EffectiveDate        = DateTime.Now.Date.AddDays(1)
            };
            return ans;
        }
    }
}
