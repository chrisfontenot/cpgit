﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a new resource record
        /// </summary>
        /// <returns></returns>
        public static resource Manufacture_resource()
        {
            var ans = new DebtPlus.LINQ.resource()
            {
                Id            = default(Int32),
                Resource_type = default(Int32),
                Name          = string.Empty,
                WWW           = null,
                AddressID     = null,
                EmailID       = null,
                FAXID         = null,
                TelephoneID   = null
            };
            return ans;
        }
    }
}
