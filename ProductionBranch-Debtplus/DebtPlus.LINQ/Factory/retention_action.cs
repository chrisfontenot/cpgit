﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a new retention_action record
        /// </summary>
        /// <returns></returns>
        public static retention_action Manufacture_retention_action()
        {
            var ans = new DebtPlus.LINQ.retention_action()
            {
                Id = default(Int32),
                letter_code = null,
                description = string.Empty
            };
            return ans;
        }
    }
}
