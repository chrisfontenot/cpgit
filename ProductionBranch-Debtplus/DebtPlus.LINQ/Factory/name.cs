﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture and empty Name object
        /// </summary>
        /// <param name="propertyRecord">The new name record</param>
        /// <returns>The Name object</returns>
        public static Name Manufacture_Name()
        {
            var answer = new DebtPlus.LINQ.Name()
            {
                Id = default(Int32),
                First = string.Empty,
                Last = string.Empty,
                Middle = string.Empty,
                Prefix = string.Empty,
                Suffix = string.Empty
            };

            return answer;
        }
    }
}
