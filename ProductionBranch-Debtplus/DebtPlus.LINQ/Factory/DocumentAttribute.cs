﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static DocumentAttribute Manufacture_DocumentAttribute()
        {
            var ans = new DocumentAttribute()
            {
                Id              = default(Int32),
                AllLanguagesFLG = true,
                AllStatesFLG    = true,
                DocumentID      = default(Int32),
                LanguageID      = default(Int32),
                State           = default(Int32)
            };
            return ans;
        }
    }
}
