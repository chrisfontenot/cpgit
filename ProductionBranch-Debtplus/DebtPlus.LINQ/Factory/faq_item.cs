﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static faq_item Manufacture_faq_item()
        {
            var ans = new faq_item()
            {
                description = string.Empty,
                attributes  = null,
                grouping    = null,
                Id          = default(Int32)
            };
            return ans;
        }
    }
}
