﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a new retention_event record
        /// </summary>
        /// <returns></returns>
        public static retention_event Manufacture_retention_event()
        {
            var ans = new DebtPlus.LINQ.retention_event()
            {
                Id                       = default(Int32),
                expire_type              = DebtPlus.LINQ.InMemory.retention_expire_types.getDefault(),
                initial_retention_action = DebtPlus.LINQ.Cache.retention_action.getDefault(),
                priority                 = 9,
                description              = string.Empty
            };
            return ans;
        }
    }
}
