﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Create an initialize an empty HPFEvent object
        /// </summary>
        /// <returns>The new HPFEvent object</returns>
        public static HPFEvent Manufacture_HPFEvent()
        {
            var answer = new HPFEvent()
            {
                property = default(Int32),
                fcID = null,
                eventID = null,
                optOutReasonCD = null,
                programRefusalDT = null,
                comments = default(string),
                completedIND = default(bool),
                rpcIND = default(bool),
                programStageID = Cache.HPFProgramStage.getDefault(),
                eventOutcomeCD = Cache.HPFEventOutcomeType.getDefault(),
                eventTypeCD = Cache.HPFEventType.getDefault()
            };

            return answer;
        }

        /// <summary>
        /// Create an initialize an empty HPFEvent object
        /// </summary>
        /// <param name="sessionRecord">The corresponding session for this event</param>
        /// <returns>The new HPFEvent object</returns>
        public static HPFEvent Manufacture_HPFEvent(Int32 propertyID)
        {
            var answer = Manufacture_HPFEvent();
            answer.property = propertyID;

            return answer;
        }
    }
}
