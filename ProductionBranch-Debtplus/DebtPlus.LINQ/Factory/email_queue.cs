﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank email_queue object
        /// </summary>
        /// <returns>The new email_queue structure</returns>
        public static email_queue Manufacture_email_queue()
        {
            var ans = new email_queue()
            {
                Id            = default(Int32),
                attempts      = 0,
                date_sent     = null,
                death_date    = null,
                email_address = null,
                email_name    = null,
                last_attempt  = null,
                last_error    = null,
                next_attempt  = null,
                status        = "PENDING",
                substitutions = null,
                template      = DebtPlus.LINQ.Cache.email_template.getDefault().GetValueOrDefault()
            };
            return ans;
        }
    }
}
