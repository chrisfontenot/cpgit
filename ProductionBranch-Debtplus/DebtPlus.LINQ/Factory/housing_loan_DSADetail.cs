﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a new Housing_Loan_DSADetail record
        /// </summary>
        /// <returns></returns>
        public static Housing_Loan_DSADetail Manufacture_Housing_Loan_DSADetail()
        {
            var ans = new DebtPlus.LINQ.Housing_Loan_DSADetail()
            {
                Id = default(Int32),
                ClientSituation = DebtPlus.LINQ.Cache.Clientsituation.getDefault(),
                NoDsaReason = DebtPlus.LINQ.Cache.NoDsaProgramReason.getDefault(),
                CaseID = null,
                Program = null,
                ProgramBenefit = null,
                Specialist = null
            };

            return ans;
        }
    }
}
