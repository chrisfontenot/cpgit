﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static report Manufacture_report()
        {
            var ans = new report()
            {
                Argument    = null,
                ClassName   = null,
                description = null,
                filename    = null,
                Id          = default(Int32),
                label       = null,
                menu_name   = null,
                Type        = "OTHER"
            };
            return ans;
        }
    }
}
