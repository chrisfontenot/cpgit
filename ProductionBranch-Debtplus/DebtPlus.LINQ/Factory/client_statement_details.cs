﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank client_statement_detail object
        /// </summary>
        /// <returns>The new client_statement_detail structure</returns>
        public static client_statement_detail Manufacture_client_statement_detail()
        {
            var ans = new client_statement_detail()
            {
                client_creditor  = default(Int32),
                client_statement = default(System.Guid),
                credits          = 0M,
                current_balance  = 0M,
                debits           = 0M,
                Id               = System.Guid.NewGuid(),
                interest         = 0M,
                interest_rate    = 0.0,
                last_payment     = default(Int32),
                original_balance = 0M,
                payments_to_date = 0M,
                sched_payment    = 0M
            };
            return ans;
        }
    }
}
