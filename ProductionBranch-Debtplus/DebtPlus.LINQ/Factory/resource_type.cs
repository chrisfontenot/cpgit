﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a new resource_type record
        /// </summary>
        /// <returns></returns>
        public static resource_type Manufacture_resource_type()
        {
            var ans = new DebtPlus.LINQ.resource_type()
            {
                Id          = default(Int32),
                label       = string.Empty,
                description = string.Empty
            };
            return ans;
        }
    }
}
