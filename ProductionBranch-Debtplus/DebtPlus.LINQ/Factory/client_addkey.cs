﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static client_addkey Manufacture_client_addkey()
        {
            var ans = new client_addkey()
            {
                additional = string.Empty,
                client     = default(Int32),
                Id         = default(Int32)
            };
            return ans;
        }
    }
}
