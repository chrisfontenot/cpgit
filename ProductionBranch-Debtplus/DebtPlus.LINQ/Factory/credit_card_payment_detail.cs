﻿using System;

namespace DebtPlus.LINQ
{
    partial class Factory
    {
        public static credit_card_payment_detail Manufacture_credit_card_payment_detail()
        {
            var ans = new credit_card_payment_detail()
            {
                account_number             = null,
                amount                     = 0M,
                avs_address                = null,
                avs_city                   = null,
                avs_full_name              = null,
                avs_state                  = null,
                avs_zipcode                = null,
                card_status_flag           = null,
                client                     = null,
                client_product             = default(Int32),
                confirmation_number        = null,
                credit_card_type           = null,
                cvv                        = null,
                payment_medium             = null,
                request                    = null,
                response                   = null,
                result_message             = null,
                return_code                = null,
                settlement_submission_date = null,
                submission_batch           = System.Guid.NewGuid(),
                submission_date            = null
            };
            return ans;
        }
    }
}
