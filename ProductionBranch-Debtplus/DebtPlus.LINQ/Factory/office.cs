﻿using System;
using System.Linq;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank office object
        /// </summary>
        /// <returns>The new office structure</returns>
        public static office Manufacture_office()
        {
            using (var bc = new BusinessContext())
            {
                // Find the current config entry to obtain the defaults for some of the fields.
                var q = DebtPlus.LINQ.Cache.config.getList().FirstOrDefault();
                if (q == null)
                {
                    q = Manufacture_config();
                }

                var ans = new DebtPlus.LINQ.office()
                {
                    ActiveFlag = true,
                    AddressID = null,
                    AltTelephoneID = null,
                    counselor = null,
                    Default = false,
                    default_till_missed = q.default_till_missed,
                    directions = string.Empty,
                    district = DebtPlus.LINQ.Cache.district.getDefault().GetValueOrDefault(1),
                    EmailID = null,
                    FAXID = null,
                    hud_hcs_id = null,
                    Id = default(Int32),
                    name = string.Empty,
                    nfcc = null,
                    region = DebtPlus.LINQ.Cache.region.getDefault().GetValueOrDefault(1),
                    TelephoneID = null,
                    TimeZoneID = DebtPlus.LINQ.Cache.TimeZone.getDefault().GetValueOrDefault(1),
                    type = DebtPlus.LINQ.InMemory.officeTypes.getDefault()
                };

                return ans;
            }
        }
    }
}
