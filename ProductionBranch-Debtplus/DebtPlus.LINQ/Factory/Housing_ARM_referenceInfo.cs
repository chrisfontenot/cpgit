﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static Housing_ARM_referenceInfo Manufacture_Housing_ARM_referenceInfo()
        {
            var ans = new Housing_ARM_referenceInfo()
            {
                Id         = default(Int32),
                arm_id     = default(Int32),
                groupId    = default(Int32),
                longDesc   = null,
                name       = null,
                Referenced = false,
                shortDesc  = null
            };
            return ans;
        }
    }
}
