﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank client_creditor object
        /// </summary>
        /// <returns>The new client_creditor structure</returns>
        public static client_creditor Manufacture_client_creditor()
        {
            var ans = new DebtPlus.LINQ.client_creditor()
            {
                account_number = string.Empty,
                balance_verification_release = false,
                balance_verify_by = null,
                balance_verify_date = null,
                check_payments = 0,
                client = default(Int32),
                client_creditor_balance = 'B',
                client_creditor_proposal = null,
                client_name = null,
                contact_name = null,
                creditor = null,
                creditor_name = null,
                date_disp_changed = null,
                disbursement_factor = 0M,
                dmp_interest = null,
                dmp_payout_interest = null,
                drop_date = null,
                drop_reason = null,
                drop_reason_sent = null,
                expected_payout_date = null,
                fairshare_pct_check = null,
                fairshare_pct_eft = null,
                first_payment = null,
                hold_disbursements = false,
                interest_this_creditor = 0M,
                irs_form_on_file = false,
                last_communication = null,
                last_payment = null,
                last_payment_date_b4_dmp = null,
                last_stmt_date = null,
                line_number = 0,
                message = null,
                months_delinquent = 0,
                non_dmp_interest = 0D,
                non_dmp_payment = 0M,
                orig_dmp_payment = null,
                payment_rpps_mask = null,
                payment_rpps_mask_timestamp = null,
                payments_this_creditor = 0M,
                percent_balance = null,
                person = null,
                prenote_date = null,
                priority = 9,
                proposal_prenote_date = null,
                reassigned_debt = false,
                returns_this_creditor = 0M,
                rpps_client_type_indicator = null,
                rpps_mask = null,
                rpps_mask_timestamp = null,
                sched_payment = 0M,
                send_bal_verify = 0,
                send_drop_notice = false,
                start_date = null,
                student_loan_release = false,
                terms = null,
                verify_request_date = null,
                Id = default(Int32)
            };

            return ans;
        }
    }
}
