﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank postal_abbreviation object
        /// </summary>
        /// <returns>The new postal_abbreviation structure</returns>
        public static postal_abbreviation Manufacture_postal_abbreviation()
        {
            var ans = new DebtPlus.LINQ.postal_abbreviation()
            {
                abbreviation = string.Empty,
                Default = false,
                description = string.Empty,
                Id = default(Int32),
                require_modifier = false,
                type = default(Int32)
            };
            return ans;
        }

        /// <summary>
        /// Manufacture a blank postal_abbreviation object
        /// </summary>
        /// <returns>The new postal_abbreviation structure</returns>
        public static postal_abbreviation Manufacture_postal_abbreviation(Int32 Type, string Abbreviation, string Description)
        {
            var ans = new DebtPlus.LINQ.postal_abbreviation()
            {
                abbreviation = Abbreviation,
                Default = false,
                description = Description,
                Id = default(Int32),
                require_modifier = false,
                type = Type
            };
            return ans;
        }
    }
}
