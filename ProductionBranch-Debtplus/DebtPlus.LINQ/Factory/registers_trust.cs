﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Generate a blank trust_register entry
        /// </summary>
        /// <returns>A pointer to the new trust_register entry</returns>
        public static registers_trust Manufacture_registers_trust()
        {
            var ans = new DebtPlus.LINQ.registers_trust()
            {
                Id = default(Int32),
                amount = 0M,
                bank = default(Int32),
                bank_xmit_date = null,
                checknum = null,
                check_order = default(Int32),
                cleared = ' ',
                client = null,
                creditor = null,
                invoice_register = null,
                reconciled_date = null,
                Selected = false,
                sequence_number = null,
                tran_type = null
            };
            return ans;
        }

        /// <summary>
        /// Generate a blank trust_register entry
        /// </summary>
        /// <param name="tranType">Transaction Type</param>
        /// <returns>A pointer to the new trust_register entry</returns>
        public static registers_trust Manufacture_registers_trust(string tranType)
        {
            var ans = Manufacture_registers_trust();
            ans.tran_type = tranType;
            return ans;
        }
    }
}
