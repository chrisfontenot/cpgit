﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static InboundTelephoneNumber Manufacture_InboundTelephoneNumber()
        {
            var ans = new InboundTelephoneNumber()
            {
                ActiveFlag      = true,
                Default         = false,
                Description     = string.Empty,
                Id              = default(Int32),
                TelephoneNumber = string.Empty
            };
            return ans;
        }
    }
}
