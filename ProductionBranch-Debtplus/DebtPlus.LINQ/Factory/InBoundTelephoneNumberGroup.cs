﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static InBoundTelephoneNumberGroup Manufacture_InboundTelephoneNumberGroup()
        {
            var ans = new InBoundTelephoneNumberGroup()
            {
                ActiveFlag      = true,
                Default         = false,
                Description     = string.Empty,
                Id              = default(Int32)
            };
            return ans;
        }
    }
}
