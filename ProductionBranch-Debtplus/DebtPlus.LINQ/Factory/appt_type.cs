﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static appt_type Manufacture_appt_type()
        {
            var ans = new appt_type()
            {
                Id                     = default(Int32),
                appt_duration          = 60,
                appt_name              = string.Empty,
                bankruptcy             = false,
                bankruptcy_class       = DebtPlus.LINQ.Cache.BankruptcyClassType.getDefault().GetValueOrDefault(),
                cancel_letter          = null,
                Default                = false,
                contact_type           = DebtPlus.LINQ.Cache.ContactMethodType.getDefault().GetValueOrDefault(),
                create_letter          = null,
                credit                 = false,
                housing                = false,
                initial_appt           = false,
                missed_retention_event = null,
                reschedule_letter      = null
            };
            return ans;
        }
    }
}
