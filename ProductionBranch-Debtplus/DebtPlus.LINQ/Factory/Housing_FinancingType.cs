﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static Housing_FinancingType Manufacture_Housing_FinancingType()
        {
            var ans = new Housing_FinancingType()
            {
                Id               = default(Int32),
                ActiveFlag       = true,
                Default          = false,
                description      = null,
                hud_9902_section = string.Empty,
                hpf              = string.Empty
            };
            return ans;
        }
    }
}
