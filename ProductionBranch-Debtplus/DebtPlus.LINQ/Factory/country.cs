﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static country Manufacture_country()
        {
            var ans = new country()
            {
                Id           = default(Int32),
                description  = string.Empty,
                ActiveFlag   = true,
                country_code = 1,
                Default      = false
            };
            return ans;
        }
    }
}
