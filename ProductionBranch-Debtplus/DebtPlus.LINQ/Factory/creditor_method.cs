﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Create a creditor_method record
        /// </summary>
        public static creditor_method Manufacture_creditor_method()
        {
            var ans = new creditor_method()
            {
                Id             = default(Int32),
                bank           = DebtPlus.LINQ.Cache.bank.getDefault("C").GetValueOrDefault(0),
                region         = DebtPlus.LINQ.Cache.region.getDefault().GetValueOrDefault(0),
                creditor       = default(Int32),
                rpps_biller_id = null,
                type           = null,
                validated      = false
            };

            return ans;
        }

        /// <summary>
        /// Create a creditor_method record
        /// </summary>
        public static creditor_method Manufacture_creditor_method(Int32 CreditorID, string typeKey)
        {
            var ans      = Manufacture_creditor_method();
            ans.bank     = DebtPlus.LINQ.Cache.bank.getDefault("R").GetValueOrDefault(0);
            ans.creditor = CreditorID;
            ans.type     = typeKey;
            return ans;
        }
    }
}
