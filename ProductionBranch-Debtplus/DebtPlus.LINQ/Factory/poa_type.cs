﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank poa_type object
        /// </summary>
        /// <returns>The new poa_type structure</returns>
        public static poa_type Manufacture_poa_type()
        {
            var ans = new poa_type()
            {
                Id = default(Int32),
                description = null
            };
            return ans;
        }
    }
}
