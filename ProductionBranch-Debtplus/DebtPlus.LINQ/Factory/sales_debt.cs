﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static sales_debt Manufacture_sales_debt()
        {
            var ans = new sales_debt()
            {
                Id                        = default(Int32),
                account_number            = string.Empty,
                balance                   = 0m,
                client_creditor           = null,
                cosigner_fees             = 0m,
                Creditor                  = default(Int32),
                creditor_name             = null,
                creditor_type             = DebtPlus.LINQ.Cache.creditor_type.getDefault(),
                DMP_EnteredPayment        = 0m,
                DMP_GoingToPrincipal      = 0m,
                DMP_InterestFees          = 0m,
                DMP_InterestRate          = 0D,
                DMP_Length_Months         = 0,
                DMP_MinimumPayment        = 0m,
                DMP_MinimumProrate        = 0D,
                DMP_Payment               = 0M,
                DMP_PayoutInterest        = 0M,
                DMP_PayoutPrincipal       = 0M,
                finance_charge            = 0M,
                interest_rate             = 0D,
                late_fees                 = 0M,
                minimum_payment           = 0M,
                overlimit_fees            = 0M,
                payout_interest           = 0M,
                payout_percent            = 0D,
                plan_finance_charge       = 0M,
                plan_interest_rate        = 0D,
                plan_min_payment          = 0M,
                plan_min_prorate          = 0D,
                plan_payment              = 0M,
                plan_principal            = 0M,
                plan_total_fees           = 0M,
                plan_total_interest_fees  = 0M,
                SELF_Length_Months        = 0,
                principal                 = 0M,
                sales_file                = default(Int32),
                sales_file1               = null,
                SELF_CosignerFees         = 0M,
                SELF_GoingToPrincipal     = 0M,
                SELF_InterestAndFees      = 0M,
                SELF_InterestRate         = 0D,
                SELF_LateFees             = 0M,
                SELF_MonthlyFinanceCharge = 0M,
                SELF_MonthlyPayment       = 0M,
                SELF_OverLimitFees        = 0M,
                SELF_PaymentPercent       = 0D,
                SELF_PayoutInterest       = 0M,
                SELF_PayoutPrincipal      = 0M,
                total_interest_fees       = 0M
            };
            return ans;
        }

        public static sales_debt Manufacture_sales_debt(Int32 SalesFileId)
        {
            var ans = Manufacture_sales_debt();
            ans.sales_file = SalesFileId;
            return ans;
        }
    }
}
