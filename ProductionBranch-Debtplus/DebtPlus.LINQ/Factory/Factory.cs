﻿using System;

namespace DebtPlus.LINQ
{
    /// <summary>
    /// Factor class to generate new object records as needed for the LINQ system. This
    /// module contains all of the needed properties to generate the default (blank) class
    /// records. All defaults should be supplied.
    /// </summary>
    public static partial class Factory
    {
        /// <summary>
        /// Initialize the class
        /// </summary>
        static Factory()
        {
        }
    }
}
