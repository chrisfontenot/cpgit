﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        /// <summary>
        /// Manufacture a blank config object
        /// </summary>
        /// <returns>The new config structure</returns>
        public static DebtPlus.LINQ.config Manufacture_config()
        {
            var ans = new DebtPlus.LINQ.config()
            {
                acceptance_days            = 30,
                AddressID                  = null,
                AltAddressID               = null,
                AltTelephoneID             = null,
                areacode                   = string.Empty,
                bal_verify_months          = 3,
                chks_per_invoice           = 0,
                client_statement_agency    = string.Empty,
                ComparisonAmount           = 0,
                ComparisonPercent          = 0,
                ComparisonRate             = 0,
                counseling_creditor        = string.Empty,
                deduct_creditor            = "Z9999",
                default_till_missed        = 3,
                dun_nbr                    = string.Empty,
                FAXID                      = null,
                fed_tax_id                 = string.Empty,
                myriad_client_statement_id = string.Empty,
                name                       = string.Empty,
                nfcc_unit_no               = 0,
                paf_creditor               = "X0001",
                payment_minimum            = 10,
                payments_pad_greater       = 0,
                payments_pad_less          = 0,
                rpps_resend_msg            = false,
                setup_creditor             = string.Empty,
                TelephoneID                = null,
                threshold                  = 0.20,
                use_sched_pay              = true,
                Id                         = default(Int32),
                web_site                   = string.Empty
            };

            return ans;
        }
    }
}
