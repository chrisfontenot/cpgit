﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static AttributeType Manufacture_AttributeType()
        {
            var ans = new AttributeType()
            {
                Id             = default(Int32),
                ActiveFlag     = true,
                Attribute      = string.Empty,
                Default        = false,
                Grouping       = null,
                hpf            = null,
                HUDLanguage    = null,
                HUDServiceType = null,
                LanguageID     = "enUS"
            };
            return ans;
        }
    }
}
