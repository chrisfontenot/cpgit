﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static client_ach Manufacture_client_ach()
        {
            var ans = new client_ach()
            {
                Id              = default(Int32),
                ABA             = string.Empty,
                AccountNumber   = string.Empty,
                CheckingSavings = DebtPlus.LINQ.InMemory.ACHAccountTypes.getDefault().GetValueOrDefault('C'),
                ContractDate    = System.DateTime.Now.Date,
                EnrollDate      = System.DateTime.Now.Date,
                StartDate       = System.DateTime.Now.Date,
                EndDate         = null,
                ErrorDate       = null,
                PrenoteDate     = null,
                isActive        = false
            };
            return ans;
        }
    }
}
