﻿using System;

namespace DebtPlus.LINQ
{
    static partial class Factory
    {
        public static StateSetupFee Manufacture_StateSetupFee()
        {
            var ans = new StateSetupFee()
            {
                Id             = default(Int32),
                amount         = 0M,
                cutoff_date    = null,
                effective_date = DateTime.Now.Date,
                state          = default(Int32)
            };
            return ans;
        }
    }
}
