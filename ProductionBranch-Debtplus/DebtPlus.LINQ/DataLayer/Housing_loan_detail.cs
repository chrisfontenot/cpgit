﻿namespace DebtPlus.LINQ
{
    using System;

    partial class Housing_loan_detail : IEquatable<Housing_loan_detail>
    {
        /// <summary>
        /// Test the equality between the current object and a different item
        /// </summary>
        /// <param name="other">The other comparison object</param>
        /// <returns>TRUE if the objects are logically equal and contain the same data, FALSE otherwise</returns>
        public bool Equals(Housing_loan_detail other)
        {
            // Compare all of the comparable fields. We don't compare those fields that change automatically or are the primary key.
            if (!ARM_Reset.Equals(other.ARM_Reset)) return false;
            if (!FHA_VA_Insured_Loan.Equals(other.FHA_VA_Insured_Loan)) return false;
            if (!Hybrid_ARM_Loan.Equals(other.Hybrid_ARM_Loan)) return false;
            if (!Interest_Only_Loan.Equals(other.Interest_Only_Loan)) return false;
            if (!Option_ARM_Loan.Equals(other.Option_ARM_Loan)) return false;
            if (!Privately_Held_Loan.Equals(other.Privately_Held_Loan)) return false;
            if (!Payment.Equals(other.Payment)) return false;

            if (!InterestRate.GetValueOrDefault().Equals(other.InterestRate.GetValueOrDefault())) return false;
            if (!FinanceTypeCD.GetValueOrDefault().Equals(other.FinanceTypeCD.GetValueOrDefault())) return false;
            if (!LoanTypeCD.GetValueOrDefault().Equals(other.LoanTypeCD.GetValueOrDefault())) return false;
            if (!MortgageTypeCD.GetValueOrDefault().Equals(other.MortgageTypeCD.GetValueOrDefault())) return false;

            return true;
        }
    }
}