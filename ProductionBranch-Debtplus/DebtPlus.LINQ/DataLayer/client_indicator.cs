﻿namespace DebtPlus.LINQ
{
    partial class client_indicator
    {
        /// <summary>
        /// Detach the item from a list. Use with caution.
        /// </summary>
        public virtual void Detach()
        {
            if (null == PropertyChanging)
            {
                return;
            }

            PropertyChanging = null;
            PropertyChanged = null;
        }
    }
}