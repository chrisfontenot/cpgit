﻿using System.Linq;

namespace DebtPlus.LINQ
{
    partial class BusinessContext
    {
        /// <summary>
        /// Results from the suser_sname call. There is only one string value used here.
        /// </summary>
        private class suser_sname_result
        {
            public string name { get; set; }
        }

        private static string cached_suser_sname = null;
        private static object lock_suser_sname = new object();

        /// <summary>
        /// Find the ID of the signed on user. This is a database ID. It is not the mapped user name but
        /// reflects the value that was used in the login.
        /// </summary>
        /// <returns></returns>
        public static string suser_sname()
        {
            if (cached_suser_sname == null)
            {
                lock (lock_suser_sname)
                {
                    if (cached_suser_sname == null)
                    {
                        using (var bc = new BusinessContext())
                        {
                            var r = bc.ExecuteQuery<suser_sname_result>(@"select suser_sname() as name").FirstOrDefault();
                            cached_suser_sname = r.name;
                        }
                    }
                }
            }
            return cached_suser_sname;
        }
    }
}