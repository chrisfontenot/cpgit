﻿// -----------------------------------------------------------------------
// <copyright file="housing_PropertyNote.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace DebtPlus.LINQ
{
    partial class Housing_PropertyNote
    {
        partial void OnCreated()
        {
            // Default the note creation to now since that is all that is listed in the grid.
            this._created_by = string.Empty;
            this._date_created = System.DateTime.Now;
        }
    }
}