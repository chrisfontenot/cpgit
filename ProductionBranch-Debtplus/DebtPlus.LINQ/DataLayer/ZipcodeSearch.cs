﻿namespace DebtPlus.LINQ
{
    partial class ZipCodeSearch
    {
        /// <summary>
        /// Interface to the new record creation.
        /// </summary>
        partial void OnCreated()
        {
            _ZIPType                    = 'S';
            _CityType                   = 'N';
            UpdateUtilityStateReference = default(DebtPlus.LINQ.state);
        }

        /// <summary>
        /// Used in the update utility to point to the current state. Not used otherwise.
        /// </summary>
        public DebtPlus.LINQ.state UpdateUtilityStateReference { get; set; }
    }
}