﻿// -----------------------------------------------------------------------
// <copyright file="housing_lender.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace DebtPlus.LINQ
{
    using System;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    partial class Housing_lender_servicer : IEquatable<Housing_lender_servicer>
    {
        public bool Equals(Housing_lender_servicer other)
        {
            // Compare all of the comparable fields. We don't compare those fields that change automatically or are the primary key.
            if (!ActiveFlag.Equals(other.ActiveFlag)) return false;
            if (!Default.Equals(other.Default)) return false;
            if (!RXOffice.GetValueOrDefault(0).Equals(other.RXOffice.GetValueOrDefault(0))) return false;
            if (!ServiceID.GetValueOrDefault(0).Equals(other.ServiceID.GetValueOrDefault(0))) return false;
            if (string.Compare(Comment ?? string.Empty, other.Comment ?? string.Empty, true) != 0) return false;
            if (string.Compare(description ?? string.Empty, other.description ?? string.Empty, true) != 0) return false;

            return true;
        }
    }
}