﻿using System;
using System.Linq;

namespace DebtPlus.LINQ
{
    partial class BusinessContext
    {
        /// <summary>
        /// Results from the getdate call. There is only one datetime value used here.
        /// </summary>
        private class getdate_result
        {
            public DateTime date { get; set; }
        }

        // Information about the last attempt for getting the system date
        private static DateTime last_getdate = DateTime.MinValue;

        private static DateTime system_getdate = DateTime.MinValue;
        private static object lock_getdate = new object();

        /// <summary>
        /// Find the current local time at the server.
        /// </summary>
        /// <remarks>
        /// The timestamps are based upon local civil time
        /// since they are directly used in reports, other selections, etc.
        ///
        /// The problem is that when a workstation is no longer at the local time zone for the server, you
        /// can not use the workstation's local time.
        /// </remarks>
        /// <returns>The DateTime value for the server's local time clock</returns>
        public static DateTime getdate()
        {
            // Take the workstation's current UTC time as a base value.
            DateTime currentDate = DateTime.UtcNow;
            TimeSpan ts = currentDate.Subtract(last_getdate);

            // Allow the time to be off by at most 4 hours. This is to prevent problems with the LCT shifts.
            if (ts.TotalMinutes > 240.0D)
            {
                lock (lock_getdate)
                {
                    if (currentDate.Subtract(last_getdate).TotalMinutes > 240.0D)
                    {
                        using (var bc = new BusinessContext())
                        {
                            var r = bc.ExecuteQuery<getdate_result>(@"select getdate() as date").FirstOrDefault();
                            system_getdate = r.date;
                        }
                        // Do not use currentDate. That is too old.
                        last_getdate = DateTime.UtcNow;
                    }
                }
                currentDate = DateTime.UtcNow;
                ts = new TimeSpan(0);
            }

            // The current time is the delta from when it was read and now plus the base time when it was read.
            return system_getdate.Add(ts);
        }
    }
}