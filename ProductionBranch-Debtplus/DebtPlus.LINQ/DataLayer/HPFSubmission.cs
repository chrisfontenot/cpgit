﻿namespace DebtPlus.LINQ
{
    using System;

    partial class HPFSubmission
    {
        /// <summary>
        /// Value for requestType in a foreclosure case submission.
        /// </summary>
        public static readonly string requestType_SUBMIT = "SUBMIT";

        /// <summary>
        /// Value for requestType in a session submission.
        /// </summary>
        public static readonly string requestType_SESSION = "SESSION";

        /// <summary>
        /// Value for requestType in a search operation
        /// </summary>
        public static readonly string requestType_SEARCH = "SEARCH";

        /// <summary>
        /// Value for requestType in a call log search operation
        /// </summary>
        public static readonly string requestType_CALLLOG_SEARCH = "CALLLOG_SEARCH";

        /// <summary>
        /// Value for requestType in a call log retrieve operation
        /// </summary>
        public static readonly string requestType_CALLLOG_RETRIEVE = "CALLLOG_RETRIEVE";

        /// <summary>
        /// Handle the creation of the class
        /// </summary>
        partial void OnCreated()
        {
            // This is the default value for the requestType. It may be changed before submitting.
            this.requestType = requestType_SUBMIT;

            // The property ID is defaulted. It is optional, but we should have a value.
            this.propertyID = default(Int32);
        }
    }
}