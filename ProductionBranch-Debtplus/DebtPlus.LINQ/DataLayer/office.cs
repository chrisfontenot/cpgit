﻿using System.Linq;

namespace DebtPlus.LINQ
{
    partial class office
    {
        /// <summary>
        /// Return the string associated with the type of office. This comes from a constant type list.
        /// </summary>
        public string formatted_type
        {
            get
            {
                var q = DebtPlus.LINQ.InMemory.officeTypes.getList().Find(s => s.Id == this.type);
                if (q != null)
                {
                    return q.description;
                }
                return "Unknown";
            }
        }
    }
}
