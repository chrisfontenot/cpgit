﻿namespace DebtPlus.LINQ
{
    using System.Linq;

    partial class HPFSession
    {
        private string _actionItemNote;

        public string actionItemNote
        {
            get
            {
                return _actionItemNote;
            }
            set
            {
                _actionItemNote = value;
                SendPropertyChanged("actionItemNote");
            }
        }

        private HPFEvent _CompletionEvent;

        public HPFEvent CompletionEvent
        {
            get
            {
                return _CompletionEvent;
            }
            set
            {
                _CompletionEvent = value;
                SendPropertyChanged("CompletionEvent");
            }
        }

        private double? _dtiRatio;

        public double? dtiRatio
        {
            get
            {
                return _dtiRatio;
            }
            set
            {
                _dtiRatio = value;
                SendPropertyChanged("dtiRatio");
            }
        }

        private string _followupNote;

        public string followupNote
        {
            get
            {
                return _followupNote;
            }
            set
            {
                _followupNote = value;
                SendPropertyChanged("followupNote");
            }
        }

        private string _loanDfltReasonNote;

        public string loanDfltReasonNote
        {
            get
            {
                return _loanDfltReasonNote;
            }
            set
            {
                _loanDfltReasonNote = value;
                SendPropertyChanged("loanDfltReasonNote");
            }
        }
    }
}