﻿namespace DebtPlus.LINQ
{
    partial class client_product
    {
        /// <summary>
        /// The invoice has not been generated. The work was not performed.
        /// </summary>
        public static readonly System.Int32 invoice_status_none = 0;

        /// <summary>
        /// The invoice has not been generated. The work was performed.
        /// </summary>
        public static readonly System.Int32 invoice_status_pending = 1;

        /// <summary>
        /// The invoice was generated but has not been reviewed.
        /// </summary>
        public static readonly System.Int32 invoice_status_preliminary = 2;

        /// <summary>
        /// THe invoice was generated and has been reviewed. It has been sent out.
        /// </summary>
        public static readonly System.Int32 invoice_status_generated = 3;

        /// <summary>
        /// Current balance for the item. Used in the grid control.
        /// </summary>
        public decimal balance
        {
            get
            {
                return this.cost - this.discount - this.disputed_amount - this.tendered;
            }
        }
    }
}