﻿using System.Linq;

namespace DebtPlus.LINQ
{
    partial class Housing_property
    {
        /// <summary>
        /// Convert the object to a string representation for the menus.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            try
            {
                using (var bc = new BusinessContext())
                {
                    // Retrieve the property address if there is one to be used
                    if (!this.UseHomeAddress && this.PropertyAddress.HasValue)
                    {
                        var adr = bc.addresses.Where(s => s.Id == PropertyAddress.Value).FirstOrDefault();
                        if (adr != null)
                        {
                            return adr.AddressLine1;
                        }
                    }

                    // Use the client's home address
                    var clientAdr = (from c in bc.clients
                                     join adr in bc.addresses on c.AddressID equals adr.Id
                                     where c.Id == this.HousingID
                                     select adr).FirstOrDefault();

                    if (clientAdr != null)
                    {
                        return clientAdr.AddressLine1;
                    }
                }
            }

            // We can't do anything about the error so just ignore it. It is better than trapping
            // with the error condition in the caller since we don't know where the caller is.
            catch { }

            // There is no address for this property.
            return string.Empty;
        }
    }
}