﻿// -----------------------------------------------------------------------
// <copyright file="housing_lender.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace DebtPlus.LINQ
{
    using System;

    partial class Housing_lender : IEquatable<Housing_lender>
    {
        /// <summary>
        /// Test the equality between the current object and a different item
        /// </summary>
        /// <param name="other">The other comparison object</param>
        /// <returns>TRUE if the objects are logically equal and contain the same data, FALSE otherwise</returns>
        public bool Equals(Housing_lender other)
        {
            // Compare all of the comparable fields. We don't compare those fields that change automatically or are the primary key.
            if (!ContactLenderSuccess.Equals(other.ContactLenderSuccess)) return false;
            if (!InvestorID.GetValueOrDefault(0).Equals(other.InvestorID.GetValueOrDefault(0))) return false;
            if (!ServicerID.GetValueOrDefault(0).Equals(other.ServicerID.GetValueOrDefault(0))) return false;

            if (!AttemptContactDate.GetValueOrDefault().Equals(other.AttemptContactDate.GetValueOrDefault())) return false;
            if (!ContactLenderDate.GetValueOrDefault().Equals(other.AttemptContactDate.GetValueOrDefault())) return false;
            if (!WorkoutPlanDate.GetValueOrDefault().Equals(other.AttemptContactDate.GetValueOrDefault())) return false;

            if (string.Compare(AcctNum ?? string.Empty, other.AcctNum ?? string.Empty, true) != 0) return false;
            if (string.Compare(CaseNumber ?? string.Empty, other.CaseNumber ?? string.Empty, true) != 0) return false;
            if (string.Compare(FdicNcusNum ?? string.Empty, other.FdicNcusNum ?? string.Empty, true) != 0) return false;
            if (string.Compare(InvestorAccountNumber ?? string.Empty, other.InvestorAccountNumber ?? string.Empty, true) != 0) return false;
            if (string.Compare(ServicerName ?? string.Empty, other.ServicerName ?? string.Empty, true) != 0) return false;

            return true;
        }
    }
}