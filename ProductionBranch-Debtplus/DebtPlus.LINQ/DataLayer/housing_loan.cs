﻿using System;
using System.Linq;

namespace DebtPlus.LINQ
{
    partial class Housing_loan : IEquatable<Housing_loan>
    {
        /// <summary>
        /// Convert the object to a string representation for the menus.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            // Convert the loan to a suitable string
            var sb = new System.Text.StringBuilder();

            // Start with the loan position
            if (this.Loan1st2nd.HasValue)
            {
                var q = DebtPlus.LINQ.Cache.Housing_LoanPositionType.getList().Find(s => s.Id == this.Loan1st2nd.Value);
                if (q != null)
                {
                    sb.AppendFormat(" {0}", q.description);
                }
            }

            try
            {
                // Take the lender ID next
                using (var bc = new BusinessContext())
                {
                    if (this.CurrentLenderID.HasValue)
                    {
                        var q = bc.Housing_lenders.Where(s => s.Id == this.CurrentLenderID.Value).FirstOrDefault();
                        if (q != null)
                        {
                            if (q.ServicerID.HasValue)
                            {
                                var svcr = bc.Housing_lender_servicers.Where(s => s.Id == q.ServicerID.Value).FirstOrDefault();
                                if (svcr != null)
                                {
                                    q.ServicerName = svcr.description;
                                }
                            }
                            sb.AppendFormat(" {0}", q.ServicerName);
                        }

                        // Append the account number
                        if (!string.IsNullOrEmpty(q.AcctNum))
                        {
                            sb.AppendFormat(" {0}", q.AcctNum);
                        }
                    }
                }
            }

            // We can't do anything about the error so just ignore it. It is better than trapping
            // with the error condition in the caller since we don't know where the caller is.
            catch { }

            // Return the resulting string as the function
            if (sb.Length > 0)
            {
                sb.Remove(0, 1);
            }

            return sb.ToString();
        }

        /// <summary>
        /// Test the equality between the current object and a different item
        /// </summary>
        /// <param name="other">The other comparison object</param>
        /// <returns>TRUE if the objects are logically equal and contain the same data, FALSE otherwise</returns>
        public bool Equals(Housing_loan other)
        {
            if (!CurrentDetailID.GetValueOrDefault(0).Equals(other.CurrentDetailID.GetValueOrDefault(0))) return false;
            if (!CurrentLenderID.GetValueOrDefault(0).Equals(other.CurrentLenderID.GetValueOrDefault(0))) return false;
            if (!CurrentLoanBalanceAmt.GetValueOrDefault(0M).Equals(other.CurrentLoanBalanceAmt.GetValueOrDefault(0M))) return false;
            if (!IntakeDetailID.GetValueOrDefault(0).Equals(other.IntakeDetailID.GetValueOrDefault(0))) return false;
            if (!IntakeSameAsCurrent.Equals(other.IntakeSameAsCurrent)) return false;
            if (!Loan1st2nd.GetValueOrDefault(0).Equals(other.Loan1st2nd.GetValueOrDefault(0))) return false;
            if (!LoanDelinquencyMonths.GetValueOrDefault(0).Equals(other.LoanDelinquencyMonths.GetValueOrDefault(0))) return false;
            if (!LoanOriginationDate.GetValueOrDefault().Equals(other.LoanOriginationDate.GetValueOrDefault())) return false;
            if (!MortgageClosingCosts.GetValueOrDefault(0M).Equals(other.MortgageClosingCosts.GetValueOrDefault(0M))) return false;
            if (!PropertyID.Equals(other.PropertyID)) return false;
            if (!UseInReports.GetValueOrDefault(false).Equals(other.UseInReports.GetValueOrDefault(false))) return false;
            if (!UseOrigLenderID.Equals(other.UseOrigLenderID)) return false;

            return true;
        }
    }
}