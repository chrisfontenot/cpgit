﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    /// <summary>
    /// A collection of changed fields is defined by this structure.
    /// </summary>
    public class ChangedFieldItem
    {
        public string FieldName { get; set; }
        public object OldValue { get; set; }
        public object NewValue { get; set; }

        public ChangedFieldItem()
        {
        }

        public ChangedFieldItem(string FieldName)
        {
            this.FieldName = FieldName;
            this.OldValue = null;
            this.NewValue = null;
        }
    }
}
