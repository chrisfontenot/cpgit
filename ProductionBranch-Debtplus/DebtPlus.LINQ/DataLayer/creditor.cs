﻿using System.Linq;

namespace DebtPlus.LINQ
{
    partial class BusinessContext
    {
        internal static partial class Helpers
        {
            internal static string Getvoucher_spacingText(System.Nullable<System.Int32> key)
            {
                if (key.HasValue)
                {
                    var q = DebtPlus.LINQ.InMemory.VoucherSpacings.getList().Find(s => s.Id == key.Value);
                    if (q != null)
                    {
                        return q.description;
                    }
                }
                return string.Empty;
            }

            internal static string Getpayment_balanceText(char key)
            {
                if (key != '\0')
                {
                    switch (key)
                    {
                        case 'P': return "Payment";
                        case 'B': return "Balance";
                        default: break;
                    }
                }
                return string.Empty;
            }

            internal static string GetCycleText(char key)
            {
                if (key != '\0')
                {
                    var q = DebtPlus.LINQ.InMemory.contribCycleTypes.getList().Find(s => s.Id == key);
                    if (q != null)
                    {
                        return q.description;
                    }
                }
                return string.Empty;
            }
        }
    }

    partial class BusinessContext
    {
        /// <summary>
        /// Write the system note for the creditor record.
        /// </summary>
        /// <param name="creditorRecord">Pointer to the current creditor record.</param>
        void WriteCreditorSystemNote(creditor creditorRecord)
        {
            System.Collections.Generic.List<DebtPlus.LINQ.ChangedFieldItem> colItemsChanged = creditorRecord.getChangedFieldItems;

            // If the rates are changed then grandfather the old rates to the existing debts.
            var q = colItemsChanged.Find(s => s.FieldName == "lowest_apr_pct");
            if (q != null && q.OldValue is double)
            {
                int oldCommandTimeout = CommandTimeout;
                try
                {
                    CommandTimeout = 600 * 1000; // This can take a while to do the operation.
                    xpr_creditor_grandfather(creditorRecord.Id, (double) q.OldValue);
                }
                finally
                {
                    CommandTimeout = oldCommandTimeout;
                }
            }

            // Gather the information for the system note.
            string noteSubject = creditor_SystemNoteSubject(colItemsChanged);
            string noteText    = creditor_SystemNoteText(colItemsChanged);

            // Create the system note if there is something to say
            if (!string.IsNullOrEmpty(noteSubject) && !string.IsNullOrEmpty(noteText))
            {
                var n     = DebtPlus.LINQ.Factory.Manufacture_creditor_note(creditorRecord.Id, 3);
                n.subject = noteSubject;
                n.note    = noteText;

                creditor_notes.InsertOnSubmit(n);
            }

            // Indicate that no fields have been changed.
            creditorRecord.ClearChangedFieldItems();
        }

        /// <summary>
        /// Get the subject line for the system note
        /// </summary>
        /// <returns></returns>
        private string creditor_SystemNoteSubject(System.Collections.Generic.List<DebtPlus.LINQ.ChangedFieldItem> colChanges)
        {
            var sbSystemNoteSubject = new System.Text.StringBuilder(string.Join(",", colChanges.Select(s => s.FieldName).ToArray()));
            if (sbSystemNoteSubject.Length > 0)
            {
                sbSystemNoteSubject.Insert(0, "Changed ");
                if (sbSystemNoteSubject.Length > 80)
                {
                    sbSystemNoteSubject.Remove(80, sbSystemNoteSubject.Length - 79);
                    sbSystemNoteSubject.Append("...");
                }
                return sbSystemNoteSubject.ToString();
            }
            return string.Empty;
        }

        /// <summary>
        /// Get the field change information for the system note
        /// </summary>
        /// <returns></returns>
        private string creditor_SystemNoteText(System.Collections.Generic.List<DebtPlus.LINQ.ChangedFieldItem> colChanges)
        {
            // Generate the change note
            var sbSystemNoteText = new System.Text.StringBuilder();
            colChanges.ForEach(s => sbSystemNoteText.Append(Helpers.FormatString(s.FieldName, s.OldValue, s.NewValue)));

            if (sbSystemNoteText.Length > 0)
            {
                sbSystemNoteText.Insert(0, "Changed the following creditor fields\r\n");
                return sbSystemNoteText.ToString();
            }
            return string.Empty;
        }
    }

    partial class creditor
    {
        /// <summary>
        /// List of the fields that have been changed
        /// </summary>
        private System.Collections.Generic.Dictionary<string, ChangedFieldItem> colChangedFieldItems = new System.Collections.Generic.Dictionary<string, ChangedFieldItem>();

        /// <summary>
        /// Retrieve the list of changed field items
        /// </summary>
        public System.Collections.Generic.List<ChangedFieldItem> getChangedFieldItems
        {
            get
            {
                return colChangedFieldItems.Values.ToList();
            }
        }

        /// <summary>
        /// Reset the list of changed field items
        /// </summary>
        public void ClearChangedFieldItems()
        {
            colChangedFieldItems.Clear();
        }

        /// <summary>
        /// Handle the change in a data field for the client record
        /// </summary>
        /// <param name="fieldname">Name of the field that is being changed</param>
        /// <param name="oldvalue">The current (old) value before the change</param>
        /// <param name="newvalue">The new value after the change</param>
        private void field_changing(string fieldname, object oldvalue, object newvalue)
        {
            var q = colChangedFieldItems.Where(s => s.Key == fieldname).Select(s => s.Value).FirstOrDefault();
            if (q == null)
            {
                lock (colChangedFieldItems)
                {
                    q = colChangedFieldItems.Where(s => s.Key == fieldname).Select(s => s.Value).FirstOrDefault();
                    if (q == null)
                    {
                        q = new ChangedFieldItem()
                        {
                            FieldName = fieldname,
                            OldValue  = oldvalue,
                            NewValue  = newvalue
                        };
                        colChangedFieldItems.Add(fieldname, q);
                        return;
                    }
                }
            }
            q.NewValue = newvalue;
        }

        // Look for the changes in the fields that we report in the creditor record. All of the other field changes we can ignore.
        partial void Onpledge_bill_monthChanging(int value)
        {
            field_changing("pledge_bill_month", _pledge_bill_month, value);
        }

        partial void OnsicChanging(string value)
        {
            field_changing("sic", _sic, value);
        }

        partial void Oncreditor_nameChanging(string value)
        {
            field_changing("creditor_name", _creditor_name, value);
        }

        partial void OncommentChanging(string value)
        {
            field_changing("comment", _comment, value);
        }

        partial void OndivisionChanging(string value)
        {
            field_changing("division", _division, value);
        }

        partial void Oncreditor_classChanging(int value)
        {
            field_changing("creditor_class", _creditor_class, value);
        }

        partial void Onvoucher_spacingChanging(int value)
        {
            field_changing("voucher_spacing", DebtPlus.LINQ.BusinessContext.Helpers.Getvoucher_spacingText(_voucher_spacing), DebtPlus.LINQ.BusinessContext.Helpers.Getvoucher_spacingText(value));
        }

        partial void Onmail_priorityChanging(int value)
        {
            field_changing("mail_priority", _mail_priority, value);
        }

        partial void Onpayment_balanceChanging(char value)
        {
            field_changing("payment_balance", DebtPlus.LINQ.BusinessContext.Helpers.Getpayment_balanceText(_payment_balance), DebtPlus.LINQ.BusinessContext.Helpers.Getpayment_balanceText(value));
        }

        partial void Onprohibit_useChanging(bool value)
        {
            field_changing("prohibit_use", _prohibit_use, value);
        }

        partial void Onfull_disclosureChanging(bool value)
        {
            field_changing("full_disclosure", _full_disclosure, value);
        }

        partial void Onsuppress_invoiceChanging(bool value)
        {
            field_changing("suppress_invoice", _suppress_invoice, value);
        }

        partial void Onproposal_budget_infoChanging(bool value)
        {
            field_changing("proposal_budget_info", _proposal_budget_info, value);
        }

        partial void Onproposal_income_infoChanging(bool value)
        {
            field_changing("proposal_income_info", _proposal_income_info, value);
        }

        partial void Oncontrib_cycleChanging(char value)
        {
            field_changing("contrib_cycle", DebtPlus.LINQ.BusinessContext.Helpers.GetCycleText(_contrib_cycle), DebtPlus.LINQ.BusinessContext.Helpers.GetCycleText(value));
        }

        partial void Oncontrib_bill_monthChanging(int value)
        {
            field_changing("contrib_bill_month", _contrib_bill_month, value);
        }

        partial void Onpledge_amtChanging(System.Nullable<decimal> value)
        {
            field_changing("pledge_amt", _pledge_amt, value);
        }

        partial void Onpledge_cycleChanging(char value)
        {
            field_changing("pledge_cycle", DebtPlus.LINQ.BusinessContext.Helpers.GetCycleText(_pledge_cycle), DebtPlus.LINQ.BusinessContext.Helpers.GetCycleText(value));
        }

        partial void Onmin_accept_amtChanging(System.Nullable<decimal> value)
        {
            field_changing("min_accept_amt", _min_accept_amt, value);
        }

        partial void Onmin_accept_pctChanging(System.Nullable<double> value)
        {
            field_changing("min_accept_pct", _min_accept_pct, value);
        }

        partial void Onmin_accept_per_billChanging(System.Nullable<decimal> value)
        {
            field_changing("min_accept_per_bill", _min_accept_per_bill, value);
        }

        partial void Onlowest_apr_pctChanging(double value)
        {
            field_changing("lowest_apr_pct", _lowest_apr_pct, value);
        }

        partial void Onmedium_apr_pctChanging(double value)
        {
            field_changing("medium_apr_pct", _medium_apr_pct, value);
        }

        partial void Onhighest_apr_pctChanging(double value)
        {
            field_changing("highest_apr_pct", _highest_apr_pct, value);
        }

        partial void Onmedium_apr_amtChanging(decimal value)
        {
            field_changing("medium_apr_amt", _medium_apr_amt, value);
        }

        partial void Onhighest_apr_amtChanging(decimal value)
        {
            field_changing("highest_apr_amt", _highest_apr_amt, value);
        }

        partial void Onmax_clients_per_checkChanging(System.Nullable<int> value)
        {
            field_changing("max_clients_per_check", _max_clients_per_check, value);
        }

        partial void Onmax_amt_per_checkChanging(System.Nullable<decimal> value)
        {
            field_changing("max_amt_per_check", _max_amt_per_check, value);
        }

        partial void Onmax_fairshare_per_debtChanging(System.Nullable<decimal> value)
        {
            field_changing("max_fairshare_per_debt", _max_fairshare_per_debt, value);
        }

        partial void Onchks_per_invoiceChanging(System.Nullable<int> value)
        {
            field_changing("chks_per_invoice", _chks_per_invoice, value);
        }

        partial void Onreturned_mailChanging(System.Nullable<System.DateTime> value)
        {
            field_changing("returned_mail", _returned_mail, value);
        }

        partial void Onpo_numberChanging(string value)
        {
            field_changing("po_number", _po_number, value);
        }

        partial void Onusual_priorityChanging(int value)
        {
            field_changing("usual_priority", _usual_priority, value);
        }

        partial void Onpercent_balanceChanging(double value)
        {
            field_changing("percent_balance", _percent_balance, value);
        }

        partial void Onacceptance_daysChanging(System.Nullable<int> value)
        {
            field_changing("acceptance_days", _acceptance_days, value);
        }

        partial void Oncheck_paymentsChanging(int value)
        {
            field_changing("check_payments", _check_payments, value);
        }
    }
}