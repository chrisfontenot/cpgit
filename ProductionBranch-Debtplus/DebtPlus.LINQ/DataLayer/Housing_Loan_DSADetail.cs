﻿namespace DebtPlus.LINQ
{
    using System;

    partial class Housing_Loan_DSADetail : IEquatable<Housing_Loan_DSADetail>
    {
        /// <summary>
        /// Test the equality between the current object and a different item
        /// </summary>
        /// <param name="other">The other comparison object</param>
        /// <returns>TRUE if the objects are logically equal and contain the same data, FALSE otherwise</returns>
        public bool Equals(Housing_Loan_DSADetail other)
        {
            if (!CaseID.GetValueOrDefault().Equals(other.CaseID.GetValueOrDefault())) return false;
            if (!ClientSituation.GetValueOrDefault().Equals(other.ClientSituation.GetValueOrDefault())) return false;
            if (!NoDsaReason.GetValueOrDefault().Equals(other.NoDsaReason.GetValueOrDefault())) return false;
            if (!Program.GetValueOrDefault().Equals(other.Program.GetValueOrDefault())) return false;
            if (!ProgramBenefit.GetValueOrDefault().Equals(other.ProgramBenefit.GetValueOrDefault())) return false;
            if (!Specialist.GetValueOrDefault().Equals(other.Specialist.GetValueOrDefault())) return false;

            return true;
        }
    }
}