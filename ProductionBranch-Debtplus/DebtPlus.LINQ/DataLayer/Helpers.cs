﻿using System.Linq;

namespace DebtPlus.LINQ
{
    partial class BusinessContext
    {
        internal static partial class Helpers
        {
            /// <summary>
            /// Routine to format a string to the proper text for a system note.
            /// </summary>
            /// <param name="fieldname">Name of the field</param>
            /// <param name="oldValue">Value for the old item</param>
            /// <param name="newvalue">Value for the new item</param>
            /// <returns></returns>
            /// <remarks></remarks>
            static internal string FormatString(string fieldName, object oldValue, object newValue)
            {
                return System.String.Format("\r\nChanged {0} from \"{1}\" to \"{2}\"", fieldName, getStringFormat(oldValue), getStringFormat(newValue));
            }

            /// <summary>
            /// Helping function to convert the object to a suitable string. This is a private function.
            /// Please do not change it to be externally accessible. It is only called from the FormatString
            /// function.
            /// </summary>
            /// <param name="value">Value to be converted</param>
            /// <returns>The suitable string representation of the value</returns>
            /// <remarks></remarks>
            static internal string getStringFormat(object value)
            {
                // A null value is empty
                if (value == null)
                {
                    return string.Empty;
                }

                // A decimal value is currency
                if (value is decimal)
                {
                    return System.String.Format("{0:c}", value);
                }

                // A date value does not have the time.
                if (value is System.DateTime)
                {
                    return System.String.Format("{0:d}", value);
                }

                // All others are simply the suitable string value
                return value.ToString();
            }
        }
    }
}
