﻿#pragma warning disable 1591

/*
  <Table Name="dbo.view_debt_info" Member="view_debt_infos">
    <Type Name="view_debt_info">
      <Column Name="account_number" Type="System.String" DbType="VarChar(22)" CanBeNull="true" />
      <Column Name="balance_verify_by" Type="System.String" DbType="VarChar(50)" CanBeNull="true" />
      <Column Name="balance_verify_date" Type="System.DateTime" DbType="DateTime" CanBeNull="true" />
      <Column Name="check_payments" Type="System.Int32" DbType="Int NOT NULL" CanBeNull="false" />
      <Column Name="client" Type="System.Int32" DbType="Int NOT NULL" CanBeNull="false" />
      <Column Name="client_creditor" Member="Id" Type="System.Int32" DbType="Int NOT NULL" CanBeNull="false" />
      <Column Name="client_name" Type="System.String" DbType="VarChar(255)" CanBeNull="true" />
      <Column Name="creditor_name" Type="System.String" DbType="VarChar(80)" CanBeNull="true" />
      <Column Name="contact_name" Type="System.String" DbType="VarChar(255)" CanBeNull="true" />
      <Column Name="created_by" Type="System.String" DbType="VarChar(80) NOT NULL" CanBeNull="false" />
      <Column Name="date_created" Type="System.DateTime" DbType="DateTime NOT NULL" CanBeNull="false" />
      <Column Name="date_disp_changed" Type="System.DateTime" DbType="DateTime" CanBeNull="true" />
      <Column Name="disbursement_factor" Type="System.Decimal" DbType="Money NOT NULL" CanBeNull="false" />
      <Column Name="dmp_interest" Type="System.Double" DbType="Float" CanBeNull="true" />
      <Column Name="dmp_payout_interest" Type="System.Double" DbType="Float" CanBeNull="true" />
      <Column Name="drop_date" Type="System.DateTime" DbType="DateTime" CanBeNull="true" />
      <Column Name="drop_reason" Type="System.Int32" DbType="Int" CanBeNull="true" />
      <Column Name="drop_reason_sent" Type="System.DateTime" DbType="DateTime" CanBeNull="true" />
      <Column Name="expected_payout_date" Type="System.DateTime" DbType="DateTime" CanBeNull="true" />
      <Column Name="fairshare_pct_check" Type="System.Double" DbType="Float" CanBeNull="true" />
      <Column Name="fairshare_pct_eft" Type="System.Double" DbType="Float" CanBeNull="true" />
      <Column Name="hold_disbursements" Type="System.Boolean" DbType="Bit NOT NULL" CanBeNull="false" />
      <Column Name="interest_this_creditor" Type="System.Decimal" DbType="Money NOT NULL" CanBeNull="false" />
      <Column Name="irs_form_on_file" Type="System.Boolean" DbType="Bit NOT NULL" CanBeNull="false" />
      <Column Name="last_communication" Type="System.String" DbType="VarChar(15)" CanBeNull="true" />
      <Column Name="last_payment_date_b4_dmp" Type="System.DateTime" DbType="DateTime" CanBeNull="true" />
      <Column Name="last_stmt_date" Type="System.DateTime" DbType="DateTime" CanBeNull="true" />
      <Column Name="line_number" Type="System.Int32" DbType="Int NOT NULL" CanBeNull="false" />
      <Column Name="message" Type="System.String" DbType="VarChar(256)" CanBeNull="true" />
      <Column Name="months_delinquent" Type="System.Int32" DbType="Int NOT NULL" CanBeNull="false" />
      <Column Name="non_dmp_interest" Type="System.Double" DbType="Float NOT NULL" CanBeNull="false" />
      <Column Name="non_dmp_payment" Type="System.Decimal" DbType="Money NOT NULL" CanBeNull="false" />
      <Column Name="orig_dmp_payment" Type="System.Decimal" DbType="Money" CanBeNull="true" />
      <Column Name="payments_this_creditor" Type="System.Decimal" DbType="Money NOT NULL" CanBeNull="false" />
      <Column Name="percent_balance" Type="System.Double" DbType="Float" CanBeNull="true" />
      <Column Name="person" Type="System.Int32" DbType="Int" CanBeNull="true" />
      <Column Name="prenote_date" Type="System.DateTime" DbType="DateTime" CanBeNull="true" />
      <Column Name="priority" Type="System.Int32" DbType="Int NOT NULL" CanBeNull="false" />
      <Column Name="IsActive" Type="System.Int32" DbType="Int NOT NULL" CanBeNull="false" />
      <Column Name="returns_this_creditor" Type="System.Decimal" DbType="Money NOT NULL" CanBeNull="false" />
      <Column Name="sched_payment" Type="System.Decimal" DbType="Money NOT NULL" CanBeNull="false" />
      <Column Name="send_bal_verify" Type="System.Int32" DbType="Int NOT NULL" CanBeNull="false" />
      <Column Name="send_drop_notice" Type="System.Boolean" DbType="Bit NOT NULL" CanBeNull="false" />
      <Column Name="start_date" Type="System.DateTime" DbType="DateTime" CanBeNull="true" />
      <Column Name="student_loan_release" Type="System.Boolean" DbType="Bit NOT NULL" CanBeNull="false" />
      <Column Name="rpps_client_type_indicator" Type="System.String" DbType="VarChar(1)" CanBeNull="true" />
      <Column Name="terms" Type="System.Int32" DbType="Int" CanBeNull="true" />
      <Column Name="verify_request_date" Type="System.DateTime" DbType="DateTime" CanBeNull="true" />
      <Column Name="balance_verification_release" Type="System.Boolean" DbType="Bit NOT NULL" CanBeNull="false" />
      <Column Name="client_creditor_proposal" Type="System.Int32" DbType="Int" CanBeNull="true" />
      <Column Name="proposal_balance" Type="System.Decimal" DbType="Money" CanBeNull="true" />
      <Column Name="proposal_status" Type="System.Int32" DbType="Int" CanBeNull="true" />
      <Column Name="client_creditor_balance" Type="System.Int32" DbType="Int" CanBeNull="true" />
      <Column Name="balance_client_creditor" Type="System.Int32" DbType="Int" CanBeNull="true" />
      <Column Name="current_sched_payment" Type="System.Decimal" DbType="Money" CanBeNull="true" />
      <Column Name="orig_balance" Type="System.Decimal" DbType="Money" CanBeNull="true" />
      <Column Name="orig_balance_adjustment" Type="System.Decimal" DbType="Money" CanBeNull="true" />
      <Column Name="payments_month_0" Type="System.Decimal" DbType="Money" CanBeNull="true" />
      <Column Name="payments_month_1" Type="System.Decimal" DbType="Money" CanBeNull="true" />
      <Column Name="total_interest" Type="System.Decimal" DbType="Money" CanBeNull="true" />
      <Column Name="total_payments" Type="System.Decimal" DbType="Money" CanBeNull="true" />
      <Column Name="total_sched_payment" Type="System.Decimal" DbType="Money" CanBeNull="true" />
      <Column Name="creditor_interest" Type="System.Double" DbType="Float" CanBeNull="true" />
      <Column Name="creditor" Type="System.String" DbType="VarChar(10)" CanBeNull="true" />
      <Column Name="cr_creditor_name" Type="System.String" DbType="VarChar(255)" CanBeNull="true" />
      <Column Name="cr_division_name" Type="System.String" DbType="VarChar(255)" CanBeNull="true" />
      <Column Name="cr_payment_balance" Type="System.Char" DbType="Char(1)" CanBeNull="true" />
      <Column Name="cr_min_accept_amt" Type="System.Decimal" DbType="Money" CanBeNull="true" />
      <Column Name="cr_min_accept_pct" Type="System.Double" DbType="Float" CanBeNull="true" />
      <Column Name="ccl_zero_balance" Type="System.Boolean" DbType="Bit" CanBeNull="true" />
      <Column Name="ccl_prorate" Type="System.Boolean" DbType="Bit" CanBeNull="true" />
      <Column Name="ccl_always_disburse" Type="System.Boolean" DbType="Bit" CanBeNull="true" />
      <Column Name="ccl_agency_account" Type="System.Boolean" DbType="Bit" CanBeNull="true" />
      <Column Name="first_payment" Type="System.Int32" DbType="Int" CanBeNull="true" />
      <Column Name="first_payment_date" Type="System.DateTime" DbType="DateTime" CanBeNull="true" />
      <Column Name="first_payment_amt" Type="System.Decimal" DbType="Money" CanBeNull="true" />
      <Column Name="first_payment_type" Type="System.String" DbType="VarChar(2)" CanBeNull="true" />
      <Column Name="last_payment" Type="System.Int32" DbType="Int" CanBeNull="true" />
      <Column Name="last_payment_date" Type="System.DateTime" DbType="DateTime" CanBeNull="true" />
      <Column Name="last_payment_amt" Type="System.Decimal" DbType="Money" CanBeNull="true" />
      <Column Name="last_payment_type" Type="System.String" DbType="VarChar(2)" CanBeNull="true" />
    </Type>
  </Table>
*/

namespace DebtPlus.LINQ
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;

    public partial class DebtPlusDataContext
    {
        public System.Data.Linq.Table<view_debt_info> view_debt_infos
        {
            get
            {
                return this.GetTable<view_debt_info>();
            }
        }
    }

    [global::System.Data.Linq.Mapping.TableAttribute(Name = "dbo.view_debt_info")]
    public partial class view_debt_info
    {
        private string _account_number;
        private string _balance_verify_by;
        private System.Nullable<System.DateTime> _balance_verify_date;
        private int _check_payments;
        private int _client;
        private int _Id = default(int);
        private string _client_name;
        private string _creditor_name;
        private string _contact_name;
        private string _created_by = default(string);
        private System.DateTime _date_created = default(DateTime);
        private System.Nullable<System.DateTime> _date_disp_changed;
        private decimal _disbursement_factor;
        private System.Nullable<double> _dmp_interest;
        private System.Nullable<double> _dmp_payout_interest;
        private System.Nullable<System.DateTime> _drop_date;
        private System.Nullable<int> _drop_reason;
        private System.Nullable<System.DateTime> _drop_reason_sent;
        private System.Nullable<System.DateTime> _expected_payout_date;
        private System.Nullable<double> _fairshare_pct_check;
        private System.Nullable<double> _fairshare_pct_eft;
        private bool _hold_disbursements;
        private decimal _interest_this_creditor = default(decimal);
        private bool _irs_form_on_file;
        private string _last_communication;
        private System.Nullable<System.DateTime> _last_payment_date_b4_dmp;
        private System.Nullable<System.DateTime> _last_stmt_date;
        private int _line_number;
        private string _message;
        private int _months_delinquent;
        private double _non_dmp_interest;
        private decimal _non_dmp_payment;
        private System.Nullable<decimal> _orig_dmp_payment;
        private decimal _payments_this_creditor = default(decimal);
        private System.Nullable<double> _percent_balance;
        private System.Nullable<int> _person;
        private System.Nullable<System.DateTime> _prenote_date;
        private int _priority;
        private int _IsActive;
        private decimal _returns_this_creditor = default(decimal);
        private decimal _sched_payment;
        private int _send_bal_verify;
        private bool _send_drop_notice;
        private System.Nullable<System.DateTime> _start_date;
        private bool _student_loan_release;
        private string _rpps_client_type_indicator;
        private System.Nullable<int> _terms;
        private System.Nullable<System.DateTime> _verify_request_date;
        private bool _balance_verification_release;
        private System.Nullable<int> _client_creditor_proposal;
        private System.Nullable<decimal> _proposal_balance;
        private System.Nullable<int> _proposal_status;
        private System.Nullable<int> _client_creditor_balance;
        private System.Nullable<int> _balance_client_creditor;
        private System.Nullable<decimal> _current_sched_payment;
        private System.Nullable<decimal> _orig_balance;
        private System.Nullable<decimal> _orig_balance_adjustment;
        private System.Nullable<decimal> _payments_month_0 = default(decimal);
        private System.Nullable<decimal> _payments_month_1 = default(decimal);
        private System.Nullable<decimal> _total_interest = default(decimal);
        private System.Nullable<decimal> _total_payments = default(decimal);
        private System.Nullable<decimal> _total_sched_payment = default(decimal);
        private System.Nullable<double> _creditor_interest = default(double);
        private string _creditor = default(string);
        private string _cr_creditor_name = default(string);
        private string _cr_division_name = default(string);
        private System.Nullable<char> _cr_payment_balance = default(char);
        private System.Nullable<decimal> _cr_min_accept_amt = default(decimal);
        private System.Nullable<double> _cr_min_accept_pct = default(double);
        private System.Nullable<bool> _ccl_zero_balance = default(Boolean);
        private System.Nullable<bool> _ccl_prorate = default(Boolean);
        private System.Nullable<bool> _ccl_always_disburse = default(Boolean);
        private System.Nullable<bool> _ccl_agency_account = default(Boolean);
        private System.Nullable<int> _first_payment;
        private System.Nullable<System.DateTime> _first_payment_date = default(DateTime);
        private System.Nullable<decimal> _first_payment_amt = default(decimal);
        private string _first_payment_type = default(string);
        private System.Nullable<int> _last_payment;
        private System.Nullable<System.DateTime> _last_payment_date = default(DateTime);
        private System.Nullable<decimal> _last_payment_amt = default(decimal);
        private string _last_payment_type = default(string);

        #region Extensibility Method Definitions
        partial void OnLoaded();
        partial void OnValidate(System.Data.Linq.ChangeAction action);
        partial void OnCreated();
        partial void field_changing(string fieldname, object oldvalue, object newvalue);
        partial void Onaccount_numberChanged();
        partial void Onaccount_numberChanging(string value);
        partial void Onbalance_client_creditorChanged();
        partial void Onbalance_client_creditorChanging(System.Nullable<int> value);
        partial void Onbalance_verification_releaseChanged();
        partial void Onbalance_verification_releaseChanging(bool value);
        partial void Onbalance_verify_byChanged();
        partial void Onbalance_verify_byChanging(string value);
        partial void Onbalance_verify_dateChanged();
        partial void Onbalance_verify_dateChanging(System.Nullable<System.DateTime> value);
        partial void Oncheck_paymentsChanged();
        partial void Oncheck_paymentsChanging(int value);
        partial void Onclient_creditor_balanceChanged();
        partial void Onclient_creditor_balanceChanging(System.Nullable<int> value);
        partial void Onclient_creditor_proposalChanged();
        partial void Onclient_creditor_proposalChanging(System.Nullable<int> value);
        partial void Onclient_nameChanged();
        partial void Onclient_nameChanging(string value);
        partial void OnclientChanged();
        partial void OnclientChanging(int value);
        partial void Oncontact_nameChanged();
        partial void Oncontact_nameChanging(string value);
        partial void Oncreditor_interestChanged();
        partial void Oncreditor_interestChanging(System.Nullable<double> value);
        partial void Oncreditor_nameChanged();
        partial void Oncreditor_nameChanging(string value);
        partial void OncreditorChanged();
        partial void OncreditorChanging(string value);
        partial void Oncurrent_sched_paymentChanged();
        partial void Oncurrent_sched_paymentChanging(System.Nullable<decimal> value);
        partial void Ondate_disp_changedChanged();
        partial void Ondate_disp_changedChanging(System.Nullable<System.DateTime> value);
        partial void Ondisbursement_factorChanged();
        partial void Ondisbursement_factorChanging(decimal value);
        partial void Ondmp_interestChanged();
        partial void Ondmp_interestChanging(System.Nullable<double> value);
        partial void Ondmp_payout_interestChanged();
        partial void Ondmp_payout_interestChanging(System.Nullable<double> value);
        partial void Ondrop_dateChanged();
        partial void Ondrop_dateChanging(System.Nullable<System.DateTime> value);
        partial void Ondrop_reason_sentChanged();
        partial void Ondrop_reason_sentChanging(System.Nullable<System.DateTime> value);
        partial void Ondrop_reasonChanged();
        partial void Ondrop_reasonChanging(System.Nullable<int> value);
        partial void Onexpected_payout_dateChanged();
        partial void Onexpected_payout_dateChanging(System.Nullable<System.DateTime> value);
        partial void Onfairshare_pct_checkChanged();
        partial void Onfairshare_pct_checkChanging(System.Nullable<double> value);
        partial void Onfairshare_pct_eftChanged();
        partial void Onfairshare_pct_eftChanging(System.Nullable<double> value);
        partial void Onfirst_paymentChanged();
        partial void Onfirst_paymentChanging(System.Nullable<int> value);
        partial void Onhold_disbursementsChanged();
        partial void Onhold_disbursementsChanging(bool value);
        partial void OnIdChanged();
        partial void OnIdChanging(System.Int32 value);
        partial void Onirs_form_on_fileChanged();
        partial void Onirs_form_on_fileChanging(bool value);
        partial void OnIsActiveChanged();
        partial void OnIsActiveChanging(int value);
        partial void Onlast_communicationChanged();
        partial void Onlast_communicationChanging(string value);
        partial void Onlast_payment_date_b4_dmpChanged();
        partial void Onlast_payment_date_b4_dmpChanging(System.Nullable<System.DateTime> value);
        partial void Onlast_paymentChanged();
        partial void Onlast_paymentChanging(System.Nullable<int> value);
        partial void Onlast_stmt_dateChanged();
        partial void Onlast_stmt_dateChanging(System.Nullable<System.DateTime> value);
        partial void Online_numberChanged();
        partial void Online_numberChanging(int value);
        partial void OnmessageChanged();
        partial void OnmessageChanging(string value);
        partial void Onmonths_delinquentChanged();
        partial void Onmonths_delinquentChanging(int value);
        partial void Onnon_dmp_interestChanged();
        partial void Onnon_dmp_interestChanging(double value);
        partial void Onnon_dmp_paymentChanged();
        partial void Onnon_dmp_paymentChanging(decimal value);
        partial void Onorig_balance_adjustmentChanged();
        partial void Onorig_balance_adjustmentChanging(System.Nullable<decimal> value);
        partial void Onorig_balanceChanged();
        partial void Onorig_balanceChanging(System.Nullable<decimal> value);
        partial void Onorig_dmp_paymentChanged();
        partial void Onorig_dmp_paymentChanging(System.Nullable<decimal> value);
        partial void Onpercent_balanceChanged();
        partial void Onpercent_balanceChanging(System.Nullable<double> value);
        partial void OnpersonChanged();
        partial void OnpersonChanging(System.Nullable<int> value);
        partial void Onprenote_dateChanged();
        partial void Onprenote_dateChanging(System.Nullable<System.DateTime> value);
        partial void OnpriorityChanged();
        partial void OnpriorityChanging(int value);
        partial void Onproposal_balanceChanged();
        partial void Onproposal_balanceChanging(System.Nullable<decimal> value);
        partial void Onproposal_statusChanged();
        partial void Onproposal_statusChanging(System.Nullable<int> value);
        partial void Onrpps_client_type_indicatorChanged();
        partial void Onrpps_client_type_indicatorChanging(string value);
        partial void Onsched_paymentChanged();
        partial void Onsched_paymentChanging(decimal value);
        partial void Onsend_bal_verifyChanged();
        partial void Onsend_bal_verifyChanging(int value);
        partial void Onsend_drop_noticeChanged();
        partial void Onsend_drop_noticeChanging(bool value);
        partial void Onstart_dateChanged();
        partial void Onstart_dateChanging(System.Nullable<System.DateTime> value);
        partial void Onstudent_loan_releaseChanged();
        partial void Onstudent_loan_releaseChanging(bool value);
        partial void OntermsChanged();
        partial void OntermsChanging(System.Nullable<int> value);
        partial void Onverify_request_dateChanged();
        partial void Onverify_request_dateChanging(System.Nullable<System.DateTime> value);
        #endregion

        public view_debt_info()
        {
            OnCreated();
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_account_number", DbType = "VarChar(22)")]
        public string account_number
        {
            get
            {
                return this._account_number;
            }
            set
            {
                if ((this._account_number != value))
                {
                    string fn = "account_number";
                    field_changing(fn, this._account_number, value);
                    this.Onaccount_numberChanging(value);
                    this.SendPropertyChanging(fn);
                    this._account_number = value;
                    this.SendPropertyChanged(fn);
                    this.Onaccount_numberChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_balance_verify_by", DbType = "VarChar(50)")]
        public string balance_verify_by
        {
            get
            {
                return this._balance_verify_by;
            }
            set
            {
                if ((this._balance_verify_by != value))
                {
                    string fn = "balance_verify_by";
                    field_changing(fn, this._balance_verify_by, value);
                    this.Onbalance_verify_byChanging(value);
                    this.SendPropertyChanging(fn);
                    this._balance_verify_by = value;
                    this.SendPropertyChanged(fn);
                    this.Onbalance_verify_byChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_balance_verify_date", DbType = "DateTime")]
        public System.Nullable<System.DateTime> balance_verify_date
        {
            get
            {
                return this._balance_verify_date;
            }
            set
            {
                if ((this._balance_verify_date != value))
                {
                    string fn = "balance_verify_date";
                    field_changing(fn, this._balance_verify_date, value);
                    this.Onbalance_verify_dateChanging(value);
                    this.SendPropertyChanging(fn);
                    this._balance_verify_date = value;
                    this.SendPropertyChanged(fn);
                    this.Onbalance_verify_dateChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_check_payments", DbType = "Int NOT NULL")]
        public int check_payments
        {
            get
            {
                return this._check_payments;
            }
            set
            {
                if ((this._check_payments != value))
                {
                    string fn = "check_payments";
                    field_changing(fn, this._check_payments, value);
                    this.Oncheck_paymentsChanging(value);
                    this.SendPropertyChanging(fn);
                    this._check_payments = value;
                    this.SendPropertyChanged(fn);
                    this.Oncheck_paymentsChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_client", DbType = "Int NOT NULL")]
        public int client
        {
            get
            {
                return this._client;
            }
            set
            {
                if ((this._client != value))
                {
                    string fn = "client";
                    this.OnclientChanging(value);
                    this.SendPropertyChanging(fn);
                    this._client = value;
                    this.SendPropertyChanged(fn);
                    this.OnclientChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Name = "Id", Storage = "_Id", AutoSync = AutoSync.OnInsert, DbType = "Int NOT NULL IDENTITY", IsPrimaryKey = true, IsDbGenerated = true, UpdateCheck = UpdateCheck.Never)]
        public int Id
        {
            get
            {
                return this._Id;
            }
            set
            {
                if ((this._Id != value))
                {
                    string fn = "Id";
                    this.OnIdChanging(value);
                    this.SendPropertyChanging(fn);
                    this._Id = value;
                    this.SendPropertyChanged(fn);
                    this.OnIdChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_client_name", DbType = "VarChar(255)")]
        public string client_name
        {
            get
            {
                return this._client_name;
            }
            set
            {
                if ((this._client_name != value))
                {
                    string fn = "client_name";
                    field_changing(fn, this._client_name, value);
                    this.Onclient_nameChanging(value);
                    this.SendPropertyChanging(fn);
                    this._client_name = value;
                    this.SendPropertyChanged(fn);
                    this.Onclient_nameChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_creditor_name", DbType = "VarChar(80)")]
        public string creditor_name
        {
            get
            {
                return this._creditor_name;
            }
            set
            {
                if ((this._creditor_name != value))
                {
                    string fn = "creditor_name";
                    this.Oncreditor_nameChanging(value);
                    this.SendPropertyChanging(fn);
                    this._creditor_name = value;
                    this.SendPropertyChanged(fn);
                    this.Oncreditor_nameChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_contact_name", DbType = "VarChar(255)")]
        public string contact_name
        {
            get
            {
                return this._contact_name;
            }
            set
            {
                if ((this._contact_name != value))
                {
                    string fn = "contact_name";
                    field_changing(fn, this._contact_name, value);
                    this.Oncontact_nameChanging(value);
                    this.SendPropertyChanging(fn);
                    this._contact_name = value;
                    this.SendPropertyChanged(fn);
                    this.Oncontact_nameChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_created_by", AutoSync = AutoSync.OnInsert, DbType = "VarChar(80) NOT NULL", CanBeNull = false, IsDbGenerated = true, UpdateCheck = UpdateCheck.Never)]
        public string created_by
        {
            get
            {
                return this._created_by;
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_date_created", AutoSync = AutoSync.OnInsert, DbType = "DateTime NOT NULL", IsDbGenerated = true, UpdateCheck = UpdateCheck.Never)]
        public System.DateTime date_created
        {
            get
            {
                return this._date_created;
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_date_disp_changed", DbType = "DateTime")]
        public System.Nullable<System.DateTime> date_disp_changed
        {
            get
            {
                return this._date_disp_changed;
            }
            set
            {
                if ((this._date_disp_changed != value))
                {
                    string fn = "date_disb_changed";
                    field_changing(fn, this._date_disp_changed, value);
                    this.Ondate_disp_changedChanging(value);
                    this.SendPropertyChanging(fn);
                    this._date_disp_changed = value;
                    this.SendPropertyChanged(fn);
                    this.Ondate_disp_changedChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_disbursement_factor", DbType = "Money NOT NULL")]
        public decimal disbursement_factor
        {
            get
            {
                return this._disbursement_factor;
            }
            set
            {
                if ((this._disbursement_factor != value))
                {
                    string fn = "disbursement_factor";
                    field_changing(fn, this._disbursement_factor, value);
                    this.Ondisbursement_factorChanging(value);
                    this.SendPropertyChanging(fn);
                    this._disbursement_factor = value;
                    this.SendPropertyChanged(fn);
                    this.Ondisbursement_factorChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_dmp_interest", DbType = "Float")]
        public System.Nullable<double> dmp_interest
        {
            get
            {
                return this._dmp_interest;
            }
            set
            {
                if ((this._dmp_interest != value))
                {
                    string fn = "dmp_interest";
                    field_changing(fn, this._dmp_interest, value);
                    this.Ondmp_interestChanging(value);
                    this.SendPropertyChanging(fn);
                    this._dmp_interest = value;
                    this.SendPropertyChanged(fn);
                    this.Ondmp_interestChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_dmp_payout_interest", DbType = "Float")]
        public System.Nullable<double> dmp_payout_interest
        {
            get
            {
                return this._dmp_payout_interest;
            }
            set
            {
                if ((this._dmp_payout_interest != value))
                {
                    string fn = "dmp_payout_interest";
                    field_changing(fn, this._dmp_payout_interest, value);
                    this.Ondmp_payout_interestChanging(value);
                    this.SendPropertyChanging(fn);
                    this._dmp_payout_interest = value;
                    this.SendPropertyChanged(fn);
                    this.Ondmp_payout_interestChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_drop_date", DbType = "DateTime")]
        public System.Nullable<System.DateTime> drop_date
        {
            get
            {
                return this._drop_date;
            }
            set
            {
                if ((this._drop_date != value))
                {
                    string fn = "drop_date";
                    field_changing(fn, this._drop_date, value);
                    this.Ondrop_dateChanging(value);
                    this.SendPropertyChanging(fn);
                    this._drop_date = value;
                    this.SendPropertyChanged(fn);
                    this.Ondrop_dateChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_drop_reason", DbType = "Int")]
        public System.Nullable<int> drop_reason
        {
            get
            {
                return this._drop_reason;
            }
            set
            {
                if ((this._drop_reason != value))
                {
                    string fn = "drop_reason";
                    field_changing(fn, this._drop_reason, value);
                    this.Ondrop_reasonChanging(value);
                    this.SendPropertyChanging(fn);
                    this._drop_reason = value;
                    this.SendPropertyChanged(fn);
                    this.Ondrop_reasonChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_drop_reason_sent", DbType = "DateTime")]
        public System.Nullable<System.DateTime> drop_reason_sent
        {
            get
            {
                return this._drop_reason_sent;
            }
            set
            {
                if ((this._drop_reason_sent != value))
                {
                    string fn = "drop_reason_sent";
                    field_changing(fn, this._drop_reason_sent, value);
                    this.Ondrop_reason_sentChanging(value);
                    this.SendPropertyChanging(fn);
                    this._drop_reason_sent = value;
                    this.SendPropertyChanged(fn);
                    this.Ondrop_reason_sentChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_expected_payout_date", DbType = "DateTime")]
        public System.Nullable<System.DateTime> expected_payout_date
        {
            get
            {
                return this._expected_payout_date;
            }
            set
            {
                if ((this._expected_payout_date != value))
                {
                    string fn = "expected_payout_date";
                    field_changing(fn, this._expected_payout_date, value);
                    this.Onexpected_payout_dateChanging(value);
                    this.SendPropertyChanging(fn);
                    this._expected_payout_date = value;
                    this.SendPropertyChanged(fn);
                    this.Onexpected_payout_dateChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_fairshare_pct_check", DbType = "Float")]
        public System.Nullable<double> fairshare_pct_check
        {
            get
            {
                return this._fairshare_pct_check;
            }
            set
            {
                if ((this._fairshare_pct_check != value))
                {
                    string fn = "fairshare_pct_check";
                    field_changing(fn, this._fairshare_pct_check, value);
                    this.Onfairshare_pct_checkChanging(value);
                    this.SendPropertyChanging(fn);
                    this._fairshare_pct_check = value;
                    this.SendPropertyChanged(fn);
                    this.Onfairshare_pct_checkChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_fairshare_pct_eft", DbType = "Float")]
        public System.Nullable<double> fairshare_pct_eft
        {
            get
            {
                return this._fairshare_pct_eft;
            }
            set
            {
                if ((this._fairshare_pct_eft != value))
                {
                    string fn = "fairshare_pct_eft";
                    field_changing(fn, this._fairshare_pct_eft, value);
                    this.Onfairshare_pct_eftChanging(value);
                    this.SendPropertyChanging(fn);
                    this._fairshare_pct_eft = value;
                    this.SendPropertyChanged(fn);
                    this.Onfairshare_pct_eftChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_hold_disbursements", DbType = "Bit NOT NULL")]
        public bool hold_disbursements
        {
            get
            {
                return this._hold_disbursements;
            }
            set
            {
                if ((this._hold_disbursements != value))
                {
                    string fn = "hold_disbursements";
                    field_changing(fn, this._hold_disbursements, value);
                    this.Onhold_disbursementsChanging(value);
                    this.SendPropertyChanging(fn);
                    this._hold_disbursements = value;
                    this.SendPropertyChanged(fn);
                    this.Onhold_disbursementsChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_interest_this_creditor", DbType = "Money NOT NULL")]
        public decimal interest_this_creditor
        {
            get
            {
                return this._interest_this_creditor;
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_irs_form_on_file", DbType = "Bit NOT NULL")]
        public bool irs_form_on_file
        {
            get
            {
                return this._irs_form_on_file;
            }
            set
            {
                if ((this._irs_form_on_file != value))
                {
                    string fn = "irs_form_on_file";
                    field_changing(fn, this._irs_form_on_file, value);
                    this.Onirs_form_on_fileChanging(value);
                    this.SendPropertyChanging(fn);
                    this._irs_form_on_file = value;
                    this.SendPropertyChanged(fn);
                    this.Onirs_form_on_fileChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_last_communication", DbType = "VarChar(15)")]
        public string last_communication
        {
            get
            {
                return this._last_communication;
            }
            set
            {
                if ((this._last_communication != value))
                {
                    string fn = "last_communication";
                    field_changing(fn, this._last_communication, value);
                    this.Onlast_communicationChanging(value);
                    this.SendPropertyChanging(fn);
                    this._last_communication = value;
                    this.SendPropertyChanged(fn);
                    this.Onlast_communicationChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_last_payment_date_b4_dmp", DbType = "DateTime")]
        public System.Nullable<System.DateTime> last_payment_date_b4_dmp
        {
            get
            {
                return this._last_payment_date_b4_dmp;
            }
            set
            {
                if ((this._last_payment_date_b4_dmp != value))
                {
                    string fn = "last_payment_date_b4_dmp";
                    field_changing(fn, this._last_payment_date_b4_dmp, value);
                    this.Onlast_payment_date_b4_dmpChanging(value);
                    this.SendPropertyChanging(fn);
                    this._last_payment_date_b4_dmp = value;
                    this.SendPropertyChanged(fn);
                    this.Onlast_payment_date_b4_dmpChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_last_stmt_date", DbType = "DateTime")]
        public System.Nullable<System.DateTime> last_stmt_date
        {
            get
            {
                return this._last_stmt_date;
            }
            set
            {
                if ((this._last_stmt_date != value))
                {
                    string fn = "last_stmt_date";
                    field_changing(fn, this._last_stmt_date, value);
                    this.Onlast_stmt_dateChanging(value);
                    this.SendPropertyChanging(fn);
                    this._last_stmt_date = value;
                    this.SendPropertyChanged(fn);
                    this.Onlast_stmt_dateChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_line_number", DbType = "Int NOT NULL")]
        public int line_number
        {
            get
            {
                return this._line_number;
            }
            set
            {
                if ((this._line_number != value))
                {
                    string fn = "line_number";
                    field_changing(fn, this._line_number, value);
                    this.Online_numberChanging(value);
                    this.SendPropertyChanging(fn);
                    this._line_number = value;
                    this.SendPropertyChanged(fn);
                    this.Online_numberChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_message", DbType = "VarChar(256)")]
        public string message
        {
            get
            {
                return this._message;
            }
            set
            {
                if ((this._message != value))
                {
                    string fn = "message";
                    field_changing(fn, this._message, value);
                    this.OnmessageChanging(value);
                    this.SendPropertyChanging(fn);
                    this._message = value;
                    this.SendPropertyChanged(fn);
                    this.OnmessageChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_months_delinquent", DbType = "Int NOT NULL")]
        public int months_delinquent
        {
            get
            {
                return this._months_delinquent;
            }
            set
            {
                if ((this._months_delinquent != value))
                {
                    string fn = "months_delinquent";
                    field_changing(fn, this._months_delinquent, value);
                    this.Onmonths_delinquentChanging(value);
                    this.SendPropertyChanging(fn);
                    this._months_delinquent = value;
                    this.SendPropertyChanged(fn);
                    this.Onmonths_delinquentChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_non_dmp_interest", DbType = "Float NOT NULL")]
        public double non_dmp_interest
        {
            get
            {
                return this._non_dmp_interest;
            }
            set
            {
                if ((this._non_dmp_interest != value))
                {
                    string fn = "non_dmp_interest";
                    field_changing(fn, this._non_dmp_interest, value);
                    this.Onnon_dmp_interestChanging(value);
                    this.SendPropertyChanging(fn);
                    this._non_dmp_interest = value;
                    this.SendPropertyChanged(fn);
                    this.Onnon_dmp_interestChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_non_dmp_payment", DbType = "Money NOT NULL")]
        public decimal non_dmp_payment
        {
            get
            {
                return this._non_dmp_payment;
            }
            set
            {
                if ((this._non_dmp_payment != value))
                {
                    string fn = "non_dmp_payment";
                    field_changing(fn, this._non_dmp_payment, value);
                    this.Onnon_dmp_paymentChanging(value);
                    this.SendPropertyChanging(fn);
                    this._non_dmp_payment = value;
                    this.SendPropertyChanged(fn);
                    this.Onnon_dmp_paymentChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_orig_dmp_payment", DbType = "Money")]
        public System.Nullable<decimal> orig_dmp_payment
        {
            get
            {
                return this._orig_dmp_payment;
            }
            set
            {
                if ((this._orig_dmp_payment != value))
                {
                    string fn = "orig_dmp_payment";
                    field_changing(fn, this._orig_dmp_payment, value);
                    this.Onorig_dmp_paymentChanging(value);
                    this.SendPropertyChanging(fn);
                    this._orig_dmp_payment = value;
                    this.SendPropertyChanged(fn);
                    this.Onorig_dmp_paymentChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_payments_this_creditor", DbType = "Money NOT NULL")]
        public decimal payments_this_creditor
        {
            get
            {
                return this._payments_this_creditor;
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_percent_balance", DbType = "Float")]
        public System.Nullable<double> percent_balance
        {
            get
            {
                return this._percent_balance;
            }
            set
            {
                if ((this._percent_balance != value))
                {
                    string fn = "percent_balance";
                    field_changing(fn, this._percent_balance, value);
                    this.Onpercent_balanceChanging(value);
                    this.SendPropertyChanging(fn);
                    this._percent_balance = value;
                    this.SendPropertyChanged(fn);
                    this.Onpercent_balanceChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_person", DbType = "Int")]
        public System.Nullable<int> person
        {
            get
            {
                return this._person;
            }
            set
            {
                if ((this._person != value))
                {
                    string fn = "person";
                    field_changing(fn, this._person, value);
                    this.OnpersonChanging(value);
                    this.SendPropertyChanging(fn);
                    this._person = value;
                    this.SendPropertyChanged(fn);
                    this.OnpersonChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_prenote_date", DbType = "DateTime")]
        public System.Nullable<System.DateTime> prenote_date
        {
            get
            {
                return this._prenote_date;
            }
            set
            {
                if ((this._prenote_date != value))
                {
                    string fn = "prenote_date";
                    field_changing(fn, this._prenote_date, value);
                    this.Onprenote_dateChanging(value);
                    this.SendPropertyChanging(fn);
                    this._prenote_date = value;
                    this.SendPropertyChanged(fn);
                    this.Onprenote_dateChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_priority", DbType = "Int NOT NULL")]
        public int priority
        {
            get
            {
                return this._priority;
            }
            set
            {
                if ((this._priority != value))
                {
                    string fn = "priority";
                    field_changing(fn, this._priority, value);
                    this.OnpriorityChanging(value);
                    this.SendPropertyChanging(fn);
                    this._priority = value;
                    this.SendPropertyChanged(fn);
                    this.OnpriorityChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_IsActive", DbType = "Int NOT NULL")]
        public int IsActive
        {
            get
            {
                return this._IsActive;
            }
            set
            {
                if ((this._IsActive != value))
                {
                    string fn = "IsActive";
                    field_changing(fn, this._IsActive, value);
                    this.OnIsActiveChanging(value);
                    this.SendPropertyChanging(fn);
                    this._IsActive = value;
                    this.SendPropertyChanged(fn);
                    this.OnIsActiveChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_returns_this_creditor", DbType = "Money NOT NULL", CanBeNull=false, AutoSync = AutoSync.Never, UpdateCheck = UpdateCheck.Never)]
        public decimal returns_this_creditor
        {
            get
            {
                return this._returns_this_creditor;
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_sched_payment", DbType = "Money NOT NULL")]
        public decimal sched_payment
        {
            get
            {
                return this._sched_payment;
            }
            set
            {
                if ((this._sched_payment != value))
                {
                    string fn = "sched_payment";
                    field_changing(fn, this._sched_payment, value);
                    this.Onsched_paymentChanging(value);
                    this.SendPropertyChanging(fn);
                    this._sched_payment = value;
                    this.SendPropertyChanged(fn);
                    this.Onsched_paymentChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_send_bal_verify", DbType = "Int NOT NULL")]
        public int send_bal_verify
        {
            get
            {
                return this._send_bal_verify;
            }
            set
            {
                if ((this._send_bal_verify != value))
                {
                    string fn = "send_bal_verify";
                    field_changing(fn, this._send_bal_verify, value);
                    this.Onsend_bal_verifyChanging(value);
                    this.SendPropertyChanging(fn);
                    this._send_bal_verify = value;
                    this.SendPropertyChanged(fn);
                    this.Onsend_bal_verifyChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_send_drop_notice", DbType = "Bit NOT NULL")]
        public bool send_drop_notice
        {
            get
            {
                return this._send_drop_notice;
            }
            set
            {
                if ((this._send_drop_notice != value))
                {
                    string fn = "send_drop_notice";
                    field_changing(fn, this._send_drop_notice, value);
                    this.Onsend_drop_noticeChanging(value);
                    this.SendPropertyChanging(fn);
                    this._send_drop_notice = value;
                    this.SendPropertyChanged(fn);
                    this.Onsend_drop_noticeChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_start_date", DbType = "DateTime")]
        public System.Nullable<System.DateTime> start_date
        {
            get
            {
                return this._start_date;
            }
            set
            {
                if ((this._start_date != value))
                {
                    string fn = "start_date";
                    field_changing(fn, this._start_date, value);
                    this.Onstart_dateChanging(value);
                    this.SendPropertyChanging(fn);
                    this._start_date = value;
                    this.SendPropertyChanged(fn);
                    this.Onstart_dateChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_student_loan_release", DbType = "Bit NOT NULL")]
        public bool student_loan_release
        {
            get
            {
                return this._student_loan_release;
            }
            set
            {
                if ((this._student_loan_release != value))
                {
                    string fn = "student_loan_release";
                    field_changing(fn, this._student_loan_release, value);
                    this.Onstudent_loan_releaseChanging(value);
                    this.SendPropertyChanging(fn);
                    this._student_loan_release = value;
                    this.SendPropertyChanged(fn);
                    this.Onstudent_loan_releaseChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_rpps_client_type_indicator", DbType = "VarChar(1)")]
        public string rpps_client_type_indicator
        {
            get
            {
                return this._rpps_client_type_indicator;
            }
            set
            {
                if ((this._rpps_client_type_indicator != value))
                {
                    string fn = "rpps_client_type_indicator";
                    field_changing(fn, this._rpps_client_type_indicator, value);
                    this.Onrpps_client_type_indicatorChanging(value);
                    this.SendPropertyChanging(fn);
                    this._rpps_client_type_indicator = value;
                    this.SendPropertyChanged(fn);
                    this.Onrpps_client_type_indicatorChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_terms", DbType = "Int")]
        public System.Nullable<int> terms
        {
            get
            {
                return this._terms;
            }
            set
            {
                if ((this._terms != value))
                {
                    string fn = "terms";
                    field_changing(fn, this._terms, value);
                    this.OntermsChanging(value);
                    this.SendPropertyChanging(fn);
                    this._terms = value;
                    this.SendPropertyChanged(fn);
                    this.OntermsChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_verify_request_date", DbType = "DateTime")]
        public System.Nullable<System.DateTime> verify_request_date
        {
            get
            {
                return this._verify_request_date;
            }
            set
            {
                if ((this._verify_request_date != value))
                {
                    string fn = "verify_request_date";
                    field_changing(fn, this._verify_request_date, value);
                    this.Onverify_request_dateChanging(value);
                    this.SendPropertyChanging(fn);
                    this._verify_request_date = value;
                    this.SendPropertyChanged(fn);
                    this.Onverify_request_dateChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_balance_verification_release", DbType = "Bit NOT NULL")]
        public bool balance_verification_release
        {
            get
            {
                return this._balance_verification_release;
            }
            set
            {
                if ((this._balance_verification_release != value))
                {
                    string fn = "balance_verification_release";
                    field_changing(fn, this._balance_verification_release, value);
                    this.Onbalance_verification_releaseChanging(value);
                    this.SendPropertyChanging(fn);
                    this._balance_verification_release = value;
                    this.SendPropertyChanged(fn);
                    this.Onbalance_verification_releaseChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_client_creditor_proposal", DbType = "Int")]
        public System.Nullable<int> client_creditor_proposal
        {
            get
            {
                return this._client_creditor_proposal;
            }
            set
            {
                if ((this._client_creditor_proposal != value))
                {
                    string fn = "client_creditor_proposal";
                    field_changing(fn, this._client_creditor_proposal, value);
                    this.Onclient_creditor_proposalChanging(value);
                    this.SendPropertyChanging(fn);
                    this._client_creditor_proposal = value;
                    this.SendPropertyChanged(fn);
                    this.Onclient_creditor_proposalChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_proposal_balance", DbType = "Money")]
        public System.Nullable<decimal> proposal_balance
        {
            get
            {
                return this._proposal_balance;
            }
            set
            {
                if ((this._proposal_balance != value))
                {
                    string fn = "proposal_balance";
                    field_changing(fn, this._proposal_balance, value);
                    this.Onproposal_balanceChanging(value);
                    this.SendPropertyChanging(fn);
                    this._proposal_balance = value;
                    this.SendPropertyChanged(fn);
                    this.Onproposal_balanceChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_proposal_status", DbType = "Int")]
        public System.Nullable<int> proposal_status
        {
            get
            {
                return this._proposal_status;
            }
            set
            {
                if ((this._proposal_status != value))
                {
                    string fn = "proposal_status";
                    field_changing(fn, this._proposal_status, value);
                    this.Onproposal_statusChanging(value);
                    this.SendPropertyChanging(fn);
                    this._proposal_status = value;
                    this.SendPropertyChanged(fn);
                    this.Onproposal_statusChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_client_creditor_balance", DbType = "Int")]
        public System.Nullable<int> client_creditor_balance
        {
            get
            {
                return this._client_creditor_balance;
            }
            set
            {
                if ((this._client_creditor_balance != value))
                {
                    string fn = "client_creditor_balance";
                    field_changing(fn, this._client_creditor_balance, value);
                    this.Onclient_creditor_balanceChanging(value);
                    this.SendPropertyChanging(fn);
                    this._client_creditor_balance = value;
                    this.SendPropertyChanged(fn);
                    this.Onclient_creditor_balanceChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_balance_client_creditor", DbType = "Int")]
        public System.Nullable<int> balance_client_creditor
        {
            get
            {
                return this._balance_client_creditor;
            }
            set
            {
                if ((this._balance_client_creditor != value))
                {
                    string fn = "balance_client_creditor";
                    field_changing(fn, this._balance_client_creditor, value);
                    this.Onbalance_client_creditorChanging(value);
                    this.SendPropertyChanging(fn);
                    this._balance_client_creditor = value;
                    this.SendPropertyChanged(fn);
                    this.Onbalance_client_creditorChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_current_sched_payment", DbType = "Money")]
        public System.Nullable<decimal> current_sched_payment
        {
            get
            {
                return this._current_sched_payment;
            }
            set
            {
                if ((this._current_sched_payment != value))
                {
                    string fn = "current_sched_payment";
                    field_changing(fn, this._current_sched_payment, value);
                    this.Oncurrent_sched_paymentChanging(value);
                    this.SendPropertyChanging(fn);
                    this._current_sched_payment = value;
                    this.SendPropertyChanged(fn);
                    this.Oncurrent_sched_paymentChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_orig_balance", DbType = "Money")]
        public System.Nullable<decimal> orig_balance
        {
            get
            {
                return this._orig_balance;
            }
            set
            {
                if ((this._orig_balance != value))
                {
                    string fn = "orig_balance";
                    field_changing(fn, this._orig_balance, value);
                    this.Onorig_balanceChanging(value);
                    this.SendPropertyChanging(fn);
                    this._orig_balance = value;
                    this.SendPropertyChanged(fn);
                    this.Onorig_balanceChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_orig_balance_adjustment", DbType = "Money")]
        public System.Nullable<decimal> orig_balance_adjustment
        {
            get
            {
                return this._orig_balance_adjustment;
            }
            set
            {
                if ((this._orig_balance_adjustment != value))
                {
                    string fn = "orig_balance_adjustment";
                    field_changing(fn, this._orig_balance_adjustment, value);
                    this.Onorig_balance_adjustmentChanging(value);
                    this.SendPropertyChanging(fn);
                    this._orig_balance_adjustment = value;
                    this.SendPropertyChanged(fn);
                    this.Onorig_balance_adjustmentChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_payments_month_0", DbType = "Money")]
        public System.Nullable<decimal> payments_month_0
        {
            get
            {
                return this._payments_month_0;
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_payments_month_1", DbType = "Money")]
        public System.Nullable<decimal> payments_month_1
        {
            get
            {
                return this._payments_month_1;
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_total_interest", DbType = "Money")]
        public System.Nullable<decimal> total_interest
        {
            get
            {
                return this._total_interest;
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_total_payments", DbType = "Money")]
        public System.Nullable<decimal> total_payments
        {
            get
            {
                return this._total_payments;
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_total_sched_payment", DbType = "Money")]
        public System.Nullable<decimal> total_sched_payment
        {
            get
            {
                return this._total_sched_payment;
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_creditor_interest", DbType = "Float")]
        public System.Nullable<double> creditor_interest
        {
            get
            {
                return this._creditor_interest;
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_creditor", DbType = "VarChar(10)")]
        public string creditor
        {
            get
            {
                return this._creditor;
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_cr_creditor_name", DbType = "VarChar(255)")]
        public string cr_creditor_name
        {
            get
            {
                return this._cr_creditor_name;
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_cr_division_name", DbType = "VarChar(255)")]
        public string cr_division_name
        {
            get
            {
                return this._cr_division_name;
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_cr_payment_balance", DbType = "Char(1)")]
        public System.Nullable<char> cr_payment_balance
        {
            get
            {
                return this._cr_payment_balance;
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_cr_min_accept_amt", DbType = "Money")]
        public System.Nullable<decimal> cr_min_accept_amt
        {
            get
            {
                return this._cr_min_accept_amt;
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_cr_min_accept_pct", DbType = "Float")]
        public System.Nullable<double> cr_min_accept_pct
        {
            get
            {
                return this._cr_min_accept_pct;
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_ccl_zero_balance", DbType = "Bit")]
        public System.Nullable<bool> ccl_zero_balance
        {
            get
            {
                return this._ccl_zero_balance;
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_ccl_prorate", DbType = "Bit")]
        public System.Nullable<bool> ccl_prorate
        {
            get
            {
                return this._ccl_prorate;
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_ccl_always_disburse", DbType = "Bit")]
        public System.Nullable<bool> ccl_always_disburse
        {
            get
            {
                return this._ccl_always_disburse;
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_ccl_agency_account", DbType = "Bit")]
        public System.Nullable<bool> ccl_agency_account
        {
            get
            {
                return this._ccl_agency_account;
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_first_payment", DbType = "Int")]
        public System.Nullable<int> first_payment
        {
            get
            {
                return this._first_payment;
            }
            set
            {
                if ((this._first_payment != value))
                {
                    string fn = "first_payment";
                    field_changing(fn, this._first_payment, value);
                    this.Onfirst_paymentChanging(value);
                    this.SendPropertyChanging(fn);
                    this._first_payment = value;
                    this.SendPropertyChanged(fn);
                    this.Onfirst_paymentChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_first_payment_date", DbType = "DateTime")]
        public System.Nullable<System.DateTime> first_payment_date
        {
            get
            {
                return this._first_payment_date;
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_first_payment_amt", DbType = "Money")]
        public System.Nullable<decimal> first_payment_amt
        {
            get
            {
                return this._first_payment_amt;
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_first_payment_type", DbType = "VarChar(2)")]
        public string first_payment_type
        {
            get
            {
                return this._first_payment_type;
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_last_payment", DbType = "Int")]
        public System.Nullable<int> last_payment
        {
            get
            {
                return this._last_payment;
            }
            set
            {
                if ((this._last_payment != value))
                {
                    string fn = "last_payment";
                    field_changing(fn, this._last_payment, value);
                    this.Onlast_paymentChanging(value);
                    this.SendPropertyChanging(fn);
                    this._last_payment = value;
                    this.SendPropertyChanged(fn);
                    this.Onlast_paymentChanged();
                }
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_last_payment_date", DbType = "DateTime")]
        public System.Nullable<System.DateTime> last_payment_date
        {
            get
            {
                return this._last_payment_date;
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_last_payment_amt", DbType = "Money")]
        public System.Nullable<decimal> last_payment_amt
        {
            get
            {
                return this._last_payment_amt;
            }
        }

        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_last_payment_type", DbType = "VarChar(2)")]
        public string last_payment_type
        {
            get
            {
                return this._last_payment_type;
            }
        }

        public event PropertyChangingEventHandler PropertyChanging;
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void SendPropertyChanging(String propertyName)
        {
            if ((this.PropertyChanging != null))
            {
                this.PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        protected virtual void SendPropertyChanged(String propertyName)
        {
            if ((this.PropertyChanged != null))
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
