﻿namespace DebtPlus.LINQ
{
    using System;

    partial class HPFEvent : IEquatable<HPFEvent>
    {
        // Return the ID of the counselor given the logon name
        private string getCounselorID(string person)
        {
            if (!string.IsNullOrWhiteSpace(person))
            {
                var rx = new System.Text.RegularExpressions.Regex(@"(\\|/){.*}$", System.Text.RegularExpressions.RegexOptions.Singleline | System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.RightToLeft);
                var m = rx.Match(person, person.Length);
                if (m.Success)
                {
                    return m.Groups[1].Captures[0].Value;
                }
            }
            return person;
        }

        /// <summary>
        /// Last person who changed this record
        /// </summary>
        public string chgLastUserID
        {
            get
            {
                return getCounselorID(changed_by);
            }
        }

        /// <summary>
        /// ID of the person who created the event
        /// </summary>
        public string counselorIdREF
        {
            get
            {
                return getCounselorID(created_by);
            }
        }

        /// <summary>
        /// Date/Time of the event
        /// </summary>
        public DateTime eventDT
        {
            get
            {
                return date_created;
            }
        }

        /// <summary>
        /// Is the class instance the same as another item?
        /// </summary>
        /// <param name="other">Pointer to the "other" item for comparison.</param>
        /// <returns>TRUE if the two structures reflect the same information. FALSE otherwise.</returns>
        public bool Equals(HPFEvent other)
        {
            // Compare the items in the structure that should be compared. Ignore the items that don't have meaning.
            if (!(comments ?? string.Empty).Equals((other.comments ?? string.Empty))) return false;
            if (!completedIND.Equals(other.completedIND)) return false;
            if (!eventID.Equals(other.eventID)) return false;
            if (!eventOutcomeCD.Equals(other.eventOutcomeCD)) return false;
            if (!eventTypeCD.Equals(other.eventTypeCD)) return false;
            if (!optOutReasonCD.Equals(other.optOutReasonCD)) return false;
            if (!programRefusalDT.Equals(other.programRefusalDT)) return false;
            if (!programStageID.Equals(other.programStageID)) return false;
            if (!rpcIND.Equals(other.rpcIND)) return false;
            if (!property.Equals(other.property)) return false;

            return true;
        }
    }
}