﻿#pragma warning disable 1591

namespace DebtPlus.LINQ
{
    using System;

    /// <summary>
    /// Housing borrower table class definition
    /// </summary>
    partial class Housing_borrower : DebtPlus.Interfaces.Client.IPeople, System.IEquatable<DebtPlus.LINQ.Housing_borrower>
    {
        /// <summary>
        /// Job ID number from the system. Not defined but needed for the IPeople interface.
        /// </summary>
        public System.Nullable<Int32> job
        {
            get
            {
                return null;
            }
            set
            {
            }
        }

        /// <summary>
        /// Look for equality to the passed object
        /// </summary>
        /// <param name="other">Pointer to the other housing_borrower structure</param>
        /// <returns>TRUE if the records are equal. FALSE if not.</returns>
        public bool Equals(Housing_borrower other)
        {
            // Compare the fields in the current record
            if (!Birthdate.Equals(other.Birthdate)) return false;
            if (!bkAuthLetterDate.Equals(other.bkAuthLetterDate)) return false;
            if (!bkchapter.Equals(other.bkchapter)) return false;
            if (!bkdischarge.Equals(other.bkdischarge)) return false;
            if (!bkfileddate.Equals(other.bkfileddate)) return false;
            if (!bkPaymentCurrent.Equals(other.bkPaymentCurrent)) return false;
            if (!(CreditAgency ?? string.Empty).Equals((other.CreditAgency ?? string.Empty))) return false;
            if (!Disabled.Equals(other.Disabled)) return false;
            if (!Education.Equals(other.Education)) return false;
            if (!EmailID.Equals(other.EmailID)) return false;
            if (!emp_end_date.Equals(other.emp_end_date)) return false;
            if (!emp_start_date.Equals(other.emp_start_date)) return false;
            if (!Ethnicity.Equals(other.Ethnicity)) return false;
            if (!FICO_Score.Equals(other.FICO_Score)) return false;
            if (!(Former ?? string.Empty).Equals((other.Former ?? string.Empty))) return false;
            if (!Gender.Equals(other.Gender)) return false;
            if (!job.Equals(other.job)) return false;
            if (!Language.Equals(other.Language)) return false;
            if (!marital_status.Equals(other.marital_status)) return false;
            if (!MilitaryDependentID.Equals(other.MilitaryDependentID)) return false;
            if (!MilitaryGradeID.Equals(other.MilitaryGradeID)) return false;
            if (!MilitaryServiceID.Equals(other.MilitaryServiceID)) return false;
            if (!MilitaryStatusID.Equals(other.MilitaryStatusID)) return false;
            if (!NameID.Equals(other.NameID)) return false;
            if (!(other_job ?? string.Empty).Equals((other.other_job ?? string.Empty))) return false;
            if (!PersonID.Equals(other.PersonID)) return false;
            if (!Race.Equals(other.Race)) return false;
            if (!(SSN ?? string.Empty).Equals((other.SSN ?? string.Empty))) return false;

            return true;
        }
    }
}