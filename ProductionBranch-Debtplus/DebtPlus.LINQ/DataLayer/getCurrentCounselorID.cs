﻿using System;

namespace DebtPlus.LINQ
{
    partial class BusinessContext
    {
        /// <summary>
        /// Get the ID of the current counselor. This is not the same as the name. It is the ID in the counselors table.
        /// </summary>
        /// <returns>The ID or NULL if it can not be found</returns>
        public static Int32? GetCurrentCounselorID()
        {
            // Obtain the current counselor information
            string name = suser_sname();
            if (!string.IsNullOrEmpty(name))
            {
                // Find the counselor in the list of counselor IDs
                var q = DebtPlus.LINQ.Cache.counselor.getList().Find(s => string.Compare(s.Person, name, true) == 0);
                if (q != null)
                {
                    return q.Id;
                }
            }

            // Can not find the counselor
            return null;
        }
    }
}