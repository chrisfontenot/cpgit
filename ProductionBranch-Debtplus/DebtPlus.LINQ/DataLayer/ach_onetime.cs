﻿namespace DebtPlus.LINQ
{
    partial class ach_onetime
    {
        public override string ToString()
        {
            var sb = new System.Text.StringBuilder();
            if (Id > 0)
            {
                sb.AppendFormat(" #{0:f0}", Id);
            }

            sb.AppendFormat(" on {0:d}", EffectiveDate.Date);
            sb.AppendFormat(" for {0:c}", Amount);

            // Include the banking information
            sb.AppendFormat(" from {0} account", this.CheckingSavings == 'S' ? "Savings" : "Checking");
            sb.AppendFormat(" # {0}/{1}", ABA, AccountNumber);

            // Discard the leading space and return the resulting string.
            if (sb.Length > 0)
            {
                sb.Remove(0, 1);
            }

            return sb.ToString();
        }
    }
}