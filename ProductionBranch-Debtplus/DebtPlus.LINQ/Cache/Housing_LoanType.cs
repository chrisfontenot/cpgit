﻿using System;
using System.Linq;

namespace DebtPlus.LINQ.Cache
{
    public static class Housing_LoanType
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the Housing_LoanTypes table
        private static System.Collections.Generic.List<DebtPlus.LINQ.Housing_LoanType> colItems;

        /// <summary>
        /// Return the static copy of the Housing_LoanTypes table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.Housing_LoanType> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.Housing_LoanTypes.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default then NULL is returned.
        /// </summary>
        /// <returns></returns>
        public static System.Nullable<Int32> getDefault()
        {
            // Take the item marked "Default"
            var q = getList().Where(s => s.Default).FirstOrDefault();
            if (q != null)
            {
                return q.Id;
            }

            // Otherwise, return null.
            return null;
        }
    }
}