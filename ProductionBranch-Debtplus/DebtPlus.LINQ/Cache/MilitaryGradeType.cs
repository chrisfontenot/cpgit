﻿using System;
using System.Linq;

namespace DebtPlus.LINQ.Cache
{
    public static class MilitaryGradeType
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the militaryGradeTypes table
        private static System.Collections.Generic.List<DebtPlus.LINQ.militaryGradeType> colItems;

        /// <summary>
        /// Return the static copy of the militaryGradeTypes table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.militaryGradeType> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.militaryGradeTypes.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default, a suitable item is chosen.
        /// </summary>
        /// <returns></returns>
        public static Int32? getDefault()
        {
            var q = getList().Where(s => s.Default).FirstOrDefault();
            if (q == null)
            {
                q = getList().FirstOrDefault();
            }

            if (q != null)
            {
                return q.Id;
            }

            return null;
        }
    }
}