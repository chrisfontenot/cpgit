﻿using System;
using System.Linq;

namespace DebtPlus.LINQ.Cache
{
    public static class proposal_message
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the proposal_messages table
        private static System.Collections.Generic.List<DebtPlus.LINQ.proposal_message> colItems;

        /// <summary>
        /// Return the static copy of the proposal_messages table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.proposal_message> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.proposal_messages.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default then NULL is returned.
        /// </summary>
        /// <returns></returns>
        public static System.Nullable<Int32> getDefault()
        {
            // Take the first item in the list as the default. If the list is empty then null is returned.
            return getList().OrderBy(s => s.Id).Take(1).Select(s => s.Id).SingleOrDefault();
        }
    }
}