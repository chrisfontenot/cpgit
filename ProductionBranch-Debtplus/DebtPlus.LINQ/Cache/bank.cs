﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ.Cache
{
    public static class bank
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the banks table
        private static System.Collections.Generic.List<DebtPlus.LINQ.bank> colItems;

        /// <summary>
        /// Return the static copy of the banks table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.bank> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.banks.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default, a suitable item is chosen.
        /// </summary>
        /// <returns>There is no default item. This is a collection of checked items.</returns>
        public static System.Nullable<Int32> getDefault(string TypeId)
        {
            DebtPlus.LINQ.bank defaultItem = getList().Find(b => b.Default && b.type == TypeId);

            if (defaultItem == null && getList().Count > 0)
            {
                defaultItem = getList().Where(b => b.type == TypeId).OrderBy(b => b.Id).First();
            }

            if (defaultItem != null)
            {
                return defaultItem.Id;
            }
            return null;
        }
    }
}
