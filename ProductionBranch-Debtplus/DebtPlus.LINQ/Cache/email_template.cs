﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ.Cache
{
    public static class email_template
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the email_templates table
        private static System.Collections.Generic.List<DebtPlus.LINQ.email_template> colItems;

        /// <summary>
        /// Return the static copy of the email_templates table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.email_template> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.email_templates.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default then NULL is returned.
        /// </summary>
        /// <returns></returns>
        public static System.Nullable<Int32> getDefault()
        {
            return getList().OrderBy(s => s.Id).Take(1).Select(s => s.Id).SingleOrDefault();
        }
    }
}
