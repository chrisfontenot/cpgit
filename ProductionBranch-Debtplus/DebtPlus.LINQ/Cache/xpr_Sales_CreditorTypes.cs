﻿using System;
using System.Linq;

namespace DebtPlus.LINQ.Cache
{
    public static class xpr_Sales_CreditorTypes
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the xpr_Sales_CreditorTypes results table
        private static System.Collections.Generic.List<DebtPlus.LINQ.xpr_Sales_CreditorTypesResult> colItems;

        /// <summary>
        /// Return the static copy of the xpr_Sales_CreditorTypes results table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.xpr_Sales_CreditorTypesResult> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.xpr_Sales_CreditorTypes().ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default then NULL is returned.
        /// </summary>
        /// <returns></returns>
        public static System.Nullable<Int32> getDefault()
        {
            return new System.Nullable<Int32>();
        }
    }
}