﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ.Cache
{
    public static class config
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the configs table
        private static System.Collections.Generic.List<DebtPlus.LINQ.config> colItems;

        /// <summary>
        /// Return the static copy of the configs table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.config> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.configs.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the config record from the cached config table
        /// </summary>
        /// <param name="config"></param>
        /// <returns></returns>
        public static DebtPlus.LINQ.config ConfigById(Int32 config)
        {
            return DebtPlus.LINQ.Cache.config.getList().Where(s => s.Id == config).FirstOrDefault();
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default then NULL is returned.
        /// </summary>
        /// <returns></returns>
        public static System.Nullable<Int32> getDefault()
        {
            // The default is simply the first item in the list of companies. This is normally "1".
            return getList().OrderBy(s => s.Id).Take(1).Select(s => s.Id).SingleOrDefault();
        }
    }
}
