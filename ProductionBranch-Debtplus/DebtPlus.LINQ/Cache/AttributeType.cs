﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ.Cache
{
    public static class AttributeType
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the attributeTypes table
        private static System.Collections.Generic.List<DebtPlus.LINQ.AttributeType> colItems;

        /// <summary>
        /// Return the static copy of the attributeTypes table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.AttributeType> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.AttributeTypes.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return a list of the attributes that are for the spoken languages only.
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.AttributeType> getLanguageList()
        {
            return getList().FindAll(l => l.Grouping == "LANGUAGE");
        }

        /// <summary>
        /// Return the default spoken language from the cached list of languages
        /// </summary>
        /// <returns></returns>
        public static Int32 getDefaultLanguage()
        {
            System.Collections.Generic.List<DebtPlus.LINQ.AttributeType> colLanguages = getLanguageList();

            // Try for the ACTIVE DEFAULT item.
            DebtPlus.LINQ.AttributeType a = (from atr in colLanguages where atr.Default == true && atr.ActiveFlag == true select atr).FirstOrDefault();

            // If there is no default than take any marked DEFAULT
            if (a == null)
            {
                a = (from atr in colLanguages where atr.Default == true select atr).FirstOrDefault();

                // If still no default then take the first item
                if (a == null)
                {
                    a = (from atr in colLanguages orderby atr.Id select atr).FirstOrDefault();
                }
            }

            // If there is a default marked then return its key
            if (a != null)
            {
                return a.Id;
            }

            // Just punt and assume that this is the proper value. It should be US English.
            return 1;
        }

        /// <summary>
        /// Return a list of the attributes that are Dsa Partners only.
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.AttributeType> getDsaPartnerList()
        {
            return getList().FindAll(l => l.Grouping == "DSAPARTNER");
        }

        /// <summary>
        /// Return the default dsa partner from the cached list of partners
        /// </summary>
        /// <returns></returns>
        public static Int32 getDefaultDsaPartner()
        {
            System.Collections.Generic.List<DebtPlus.LINQ.AttributeType> dsaPartners = getDsaPartnerList();

            // Try for the ACTIVE DEFAULT item.
            DebtPlus.LINQ.AttributeType a = (from atr in dsaPartners where atr.Default == true && atr.ActiveFlag == true select atr).FirstOrDefault();

            // If there is no default than take any marked DEFAULT
            if (a == null)
            {
                a = (from atr in dsaPartners where atr.Default == true select atr).FirstOrDefault();

                // If still no default then take the first item
                if (a == null)
                {
                    a = (from atr in dsaPartners orderby atr.Id select atr).FirstOrDefault();
                }
            }

            // If there is a default marked then return its key
            if (a != null)
            {
                return a.Id;
            }

            // Just punt and assume that this is the proper value. It should be Freddie Mac.
            return 1;
        }
    }
}
