﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ.Cache
{
    public static class poa_lookup
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the poa_lookups table
        private static System.Collections.Generic.List<DebtPlus.LINQ.poa_lookup> colItems;

        /// <summary>
        /// Return the static copy of the poa_lookups table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.poa_lookup> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.poa_lookups.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the ID of the default entry for this table or null if there is not one.
        /// </summary>
        /// <returns></returns>
        public static System.Nullable<System.Int32> getDefault()
        {
            return null;        // There is no default.
        }

        private enum Lookup_Type
        {
            RealEstate = 1,
            Financial  = 1,
            Other      = 2,
            Stage      = 3
        }
    }
}
