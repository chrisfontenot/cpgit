﻿using System;
using System.Linq;

namespace DebtPlus.LINQ.Cache
{
    public static class vendor_type
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the vendor_type table
        private static System.Collections.Generic.List<DebtPlus.LINQ.vendor_type> colItems;

        /// <summary>
        /// Return the static copy of the vendor_type table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.vendor_type> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.vendor_types.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default, a suitable item is chosen.
        /// </summary>
        /// <returns></returns>
        public static string getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}