﻿using System;
using System.Collections.Generic;

namespace DebtPlus.LINQ.Cache
{
    [Obsolete("Use DebtPlus.LINQ.InMemory.YesNo")]
    public static class yesno_type
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the job_descriptions table
        private static List<DebtPlus.LINQ.yesno_type> colItems;

        public static List<DebtPlus.LINQ.yesno_type> getList()
        {
            if (colItems == null)
            {
                lock (objLock)
                {
                    colItems = new List<DebtPlus.LINQ.yesno_type>();
                    //colItems.Add(new DebtPlus.LINQ.yesno_type() { Value = null, description = string.Empty });
                    colItems.Add(new DebtPlus.LINQ.yesno_type() { Id = true, description = "Yes" });
                    colItems.Add(new DebtPlus.LINQ.yesno_type() { Id = false, description = "No" });
                }
            }
            return colItems;
        }
    }
}

namespace DebtPlus.LINQ
{
    /// <summary>
    /// Used in yes/no lists
    /// </summary>
    [Obsolete("Use DebtPlus.LINQ.InMemory.YesNo")]
    public class yesno_type
    {
        public bool Id { get; set; }

        public string description { get; set; }
    }
}