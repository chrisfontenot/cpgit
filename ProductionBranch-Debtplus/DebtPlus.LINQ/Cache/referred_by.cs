﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DebtPlus.LINQ.Cache
{
    public static class referred_by
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the referred_by table
        private static System.Collections.Generic.List<DebtPlus.LINQ.referred_by> colItems;

        /// <summary>
        /// Return the static copy of the referred_by table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.referred_by> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.referred_bies.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default then NULL is returned.
        /// </summary>
        /// <returns></returns>
        public static System.Nullable<Int32> getDefault()
        {
            return getList().Where(s => s.Default).Select(s => s.Id).FirstOrDefault();
        }

        public static List<DebtPlus.LINQ.referred_by> getHECMList()
        {
            int hecmType = DebtPlus.LINQ.Cache.referred_by_type.getList().Where(x => x.description.StartsWith("HECM")).Select(x => x.Id).FirstOrDefault();
            return getList().OrderBy(x => x.description).Where(r => r.referred_by_type == hecmType).ToList();
        }
    }
}