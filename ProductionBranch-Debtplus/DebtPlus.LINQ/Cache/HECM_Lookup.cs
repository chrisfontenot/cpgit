﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ.Cache
{
    public static class HECM_Lookup
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the militaryStatusTypes table
        private static System.Collections.Generic.List<DebtPlus.LINQ.hecm_default_lookup> colItems;

        /// <summary>
        /// Return the static copy of the militaryStatusTypes table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.hecm_default_lookup> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.hecm_default_lookups.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        public static System.Nullable<Int32> getDefault()
        {
            return getList().Where(s => s.Default).Select(s => s.Id).FirstOrDefault();
        }

        public static System.Collections.Generic.List<DebtPlus.LINQ.hecm_default_lookup> getOptionList()
        {
            return getList().Where(s => s.LookupType == 1).ToList();
        }

        public static System.Collections.Generic.List<hecm_default_lookup> getTierList()
        {
            List<hecm_default_lookup> listRange = new List<hecm_default_lookup>();

            listRange.Add(new hecm_default_lookup { Id = 1, description = "1" });
            listRange.Add(new hecm_default_lookup { Id = 2, description = "2" });
            listRange.Add(new hecm_default_lookup { Id = 3, description = "3" });

            return listRange;
        }

        public static System.Collections.Generic.List<hecm_default_lookup> getYesNoList()
        {
            List<hecm_default_lookup> listRange = new List<hecm_default_lookup>();

            listRange.Add(new hecm_default_lookup { Id = 0, description = "No" });
            listRange.Add(new hecm_default_lookup { Id = 1, description = "Yes" });

            return listRange;
        }

        public static System.Collections.Generic.List<hecm_default_lookup> getFileStatus()
        {
            List<hecm_default_lookup> listRange = new List<hecm_default_lookup>();

            listRange.Add(new hecm_default_lookup { Id = 1, description = "Completed" });
            listRange.Add(new hecm_default_lookup { Id = 2, description = "Disengaged" });
            listRange.Add(new hecm_default_lookup { Id = 3, description = "Pending" });

            return listRange;
        }

        public static System.Collections.Generic.List<hecm_default_lookup> getDelinqencyType()
        {
            List<hecm_default_lookup> listRange = new List<hecm_default_lookup>();

            listRange.Add(new hecm_default_lookup { Id = 1, description = "Taxes" });
            listRange.Add(new hecm_default_lookup { Id = 2, description = "Insurance" });
            listRange.Add(new hecm_default_lookup { Id = 3, description = "Taxes & Insurance" });

            return listRange;
        }

        public static int? getDelinqencyType(int? ins, int? tax)
        {
            if (!ins.HasValue && !tax.HasValue)
            {
                return null;    // For now, this trap is occurring when you re-enter an existing client. Prevent the trap from
                                // happening and just return an empty condition to the caller.
                // throw new ArgumentNullException("ins or tax may not be null");
            }

            if (ins == 2)
            {
                return HECM_Lookup.getDelinqencyType().Where(x => x.description == "Insurance").Select(x => x.Id).FirstOrDefault();
            }

            if (tax == 2)
            {
                return HECM_Lookup.getDelinqencyType().Where(x => x.description == "Taxes").Select(x => x.Id).FirstOrDefault();
            }

            if (ins == 2 && tax == 2)
            {
                return HECM_Lookup.getDelinqencyType().Where(x => x.description == "Taxes & Insurance").Select(x => x.Id).FirstOrDefault();
            }

            throw new ArgumentException("ins and tax are undefined for the collection");
        }
    }
}
