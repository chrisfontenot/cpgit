﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DebtPlus.LINQ.Cache
{
    [Obsolete("Use DebtPlus.LINQ.InMemory.YesNo")]
    public static class YesNoOption
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the studentLoan_borrowers table
        private static System.Collections.Generic.List<YesNo> colItems;

        /// <summary>
        /// Return the static copy of the studentLoan_borrowers table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<YesNo> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        colItems = new List<YesNo>()
                        {                           
                            new YesNo() {Name = "Yes", Value = 0},
                            new YesNo() {Name = "No", Value = 1},
                        };
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default, a suitable item is chosen.
        /// </summary>
        /// <returns></returns>
        public static Int32 getDefault()
        {
            if (getList().Count > 0)
            {
                return getList().OrderBy(b => b.Value).First().Value;
            }
            return 0;
        }
    }

    [Obsolete("Use DebtPlus.LINQ.InMemory.YesNo")]
    public class YesNo
    {
        public string Name { get; set; }
        public int Value { get; set; }
    }
}
