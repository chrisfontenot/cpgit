﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ.Cache
{
    public static class InboundTelephoneNumberGroup
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the ethnicityTypes table
        private static System.Collections.Generic.List<DebtPlus.LINQ.InBoundTelephoneNumberGroup> colItems;

        /// <summary>
        /// Return the static copy of the InboundTelephoneNumberGroupes table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.InBoundTelephoneNumberGroup> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var bc = new BusinessContext())
                        {
                            bc.DeferredLoadingEnabled = false;

                            // Load the list of telephone numbers associated with the groups as well.
                            var dl = new System.Data.Linq.DataLoadOptions();
                            dl.LoadWith<DebtPlus.LINQ.InBoundTelephoneNumberGroup>(s => s.InboundTelephoneNumbers);
                            bc.LoadOptions = dl;

                            colItems = bc.InBoundTelephoneNumberGroups.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default then NULL is returned.
        /// </summary>
        /// <returns></returns>
        public static System.Nullable<Int32> getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}
