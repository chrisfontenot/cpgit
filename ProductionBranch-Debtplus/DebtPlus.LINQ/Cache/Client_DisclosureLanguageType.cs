﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ.Cache
{
    public static class client_DisclosureLanguageType
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the client_DisclosureTypes table
        private static System.Collections.Generic.List<DebtPlus.LINQ.client_DisclosureLanguageType> colItems;

        /// <summary>
        /// Return the static copy of the client_DisclosureTypes table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.client_DisclosureLanguageType> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.client_DisclosureLanguageTypes.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default then NULL is returned.
        /// </summary>
        /// <returns></returns>
        public static Int32? getDefault()
        {
            // There is no default in this table. It is a many-to-many linkage table.
            return new Int32?();
        }
    }
}
