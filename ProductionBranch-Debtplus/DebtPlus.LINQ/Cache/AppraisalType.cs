﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ.Cache
{
    public static class AppraisalType
    {

        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the militaryStatusTypes table
        private static System.Collections.Generic.List<DebtPlus.LINQ.AppraisalType> colItems;

        /// <summary>
        /// Return the static copy of the militaryStatusTypes table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.AppraisalType> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.AppraisalTypes.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

    }
}
