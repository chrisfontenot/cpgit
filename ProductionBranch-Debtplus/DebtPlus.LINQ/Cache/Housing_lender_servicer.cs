﻿using System;
using System.Linq;

namespace DebtPlus.LINQ.Cache
{
    public static class Housing_lender_servicer
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the Housing_lender_servicers table
        private static System.Collections.Generic.List<DebtPlus.LINQ.Housing_lender_servicer> colItems;

        /// <summary>
        /// Return the static copy of the Housing_lender_servicers table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.Housing_lender_servicer> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.Housing_lender_servicers.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default then NULL is returned.
        /// </summary>
        /// <returns></returns>
        public static System.Nullable<Int32> getDefault()
        {
            var q = getList().Where(s => s.Default).FirstOrDefault();
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default then NULL is returned.
        /// </summary>
        /// <returns></returns>
        public static System.Nullable<Int32> getOther()
        {
            var q = getList().Where(s => s.IsOther).FirstOrDefault();
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}