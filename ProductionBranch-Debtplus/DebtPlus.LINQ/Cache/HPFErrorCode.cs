﻿using System;
using System.Linq;

namespace DebtPlus.LINQ.Cache
{
    public static class HPFErrorCode
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the HPFErrorCodes table
        private static System.Collections.Generic.List<DebtPlus.LINQ.HPFErrorCode> colItems;

        /// <summary>
        /// Return the static copy of the HPFErrorCodes table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.HPFErrorCode> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.HPFErrorCodes.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default then NULL is returned.
        /// Since there is no default, the answer is always null.
        /// </summary>
        /// <returns></returns>
        public static System.Nullable<Int32> getDefault()
        {
            return null;
        }
    }
}