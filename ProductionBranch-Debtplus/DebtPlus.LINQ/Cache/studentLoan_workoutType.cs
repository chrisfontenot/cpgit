﻿using System;
using System.Linq;

namespace DebtPlus.LINQ.Cache
{
    public static class studentLoan_workoutType
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the studentLoan_workoutTypes table
        private static System.Collections.Generic.List<DebtPlus.LINQ.studentLoan_workoutType> colItems;

        /// <summary>
        /// Return the static copy of the studentLoan_workoutTypes table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.studentLoan_workoutType> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.studentLoan_workoutTypes.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default then NULL is returned.
        /// </summary>
        /// <returns></returns>
        public static System.Nullable<Int32> getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}