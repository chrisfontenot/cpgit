﻿using System;
using System.Linq;

namespace DebtPlus.LINQ.Cache
{
    public static class state
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the states table
        private static System.Collections.Generic.List<DebtPlus.LINQ.state> colItems;

        /// <summary>
        /// Return the static copy of the states table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.state> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            // Load the country information along with the state. We need that to map the controls
                            // correctly.
                            System.Data.Linq.DataLoadOptions dl = new System.Data.Linq.DataLoadOptions();
                            dl.LoadWith<DebtPlus.LINQ.state>(s => s.country1);
                            dc.LoadOptions = dl;
                            dc.DeferredLoadingEnabled = false;

                            colItems = dc.states.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the state record from the cached state table
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        public static DebtPlus.LINQ.state StateRecordById(Int32 state)
        {
            return getList().Find(s => s.Id == state);
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default then NULL is returned.
        /// </summary>
        /// <returns></returns>
        public static Int32? getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}