﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ.Cache
{
    public static class Housing_FICONotIncludedReason
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the Housing_FICONotIncludedReasons table
        private static System.Collections.Generic.List<DebtPlus.LINQ.Housing_FICONotIncludedReason> colItems;

        /// <summary>
        /// Return the static copy of the Housing_FICONotIncludedReasons table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.Housing_FICONotIncludedReason> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.Housing_FICONotIncludedReasons.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default then NULL is returned.
        /// </summary>
        /// <returns></returns>
        public static System.Nullable<Int32> getDefault()
        {
            return getList().Where(s => s.Default).Select(s => s.Id).FirstOrDefault();
        }
    }
}
