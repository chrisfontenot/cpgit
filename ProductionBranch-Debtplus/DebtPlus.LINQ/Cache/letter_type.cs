﻿using System;
using System.Linq;

namespace DebtPlus.LINQ.Cache
{
    public static class letter_type
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the letter_types table
        private static System.Collections.Generic.List<DebtPlus.LINQ.letter_type> colItems;

        /// <summary>
        /// Return the static copy of the letter_types table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.letter_type> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.letter_types.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return a list of the letter types that are just for the default language. These should be distinct
        /// letters rather than a collection of duplicates that are relative to each language.
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.letter_type> getDistinctList()
        {
            Int32 defaultLanguage = 1; // Just a canned value. We could use the value from the Attributes table, but it is easier for now.
            return getList().FindAll(l => l.language == defaultLanguage).ToList();
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default then NULL is returned.
        /// </summary>
        /// <returns></returns>
        public static System.Nullable<Int32> getDefault()
        {
            return getList().Where(s => s.Default).Select(s => s.Id).FirstOrDefault();
        }
    }
}