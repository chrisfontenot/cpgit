﻿using System;
using System.Linq;

namespace DebtPlus.LINQ.Cache
{
    public static class region
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the regions table
        private static System.Collections.Generic.List<DebtPlus.LINQ.region> colItems;

        /// <summary>
        /// Return the static copy of the regions table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.region> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.regions.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default then NULL is returned.
        /// </summary>
        /// <returns></returns>
        public static System.Nullable<Int32> getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}