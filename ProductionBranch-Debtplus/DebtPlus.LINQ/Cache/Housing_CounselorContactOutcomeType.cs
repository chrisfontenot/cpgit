﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ.Cache
{
    public static class Housing_CounselorContactOutcomeType
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the Housing_CounselorContactOutcomeTypes table
        private static System.Collections.Generic.List<DebtPlus.LINQ.Housing_CounselorContactOutcomeType> colItems;

        /// <summary>
        /// Return the static copy of the Housing_CounselorContactOutcomeTypes table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.Housing_CounselorContactOutcomeType> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.Housing_CounselorContactOutcomeTypes.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default, a suitable item is chosen.
        /// </summary>
        /// <returns>There is no default item. This is a collection of checked items.</returns>
        public static System.Nullable<Int32> getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}
