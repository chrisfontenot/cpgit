﻿using System;
using System.Linq;

namespace DebtPlus.LINQ.Cache
{
    public static class RelationType
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the RelationTypes table
        private static System.Collections.Generic.List<DebtPlus.LINQ.RelationType> colItems;

        /// <summary>
        /// Return the static copy of the RelationTypes table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.RelationType> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.RelationTypes.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default then NULL is returned.
        /// </summary>
        /// <returns></returns>
        public static System.Nullable<Int32> getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }

        /// <summary>
        /// This is the only relation that the applicant can use. It is known to be "SELF".
        /// </summary>
        public static Int32 Self
        {
            get
            {
                return 1;
            }
        }

        /// <summary>
        /// This relation indicates that the co-applicant is not present. It is never used for the applicant.
        /// </summary>
        public static Int32 None
        {
            get
            {
                return 0;
            }
        }
    }
}