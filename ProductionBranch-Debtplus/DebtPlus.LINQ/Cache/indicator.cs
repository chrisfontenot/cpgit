﻿using System;
using System.Linq;

namespace DebtPlus.LINQ.Cache
{
    public static class indicator
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the indicators table
        private static System.Collections.Generic.List<DebtPlus.LINQ.indicator> colItems;

        /// <summary>
        /// Return the static copy of the indicators table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.indicator> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.indicators.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. There is no default. Return the NULL value.
        /// </summary>
        /// <returns></returns>
        public static System.Nullable<Int32> getDefault()
        {
            return new Int32?();
        }
    }
}