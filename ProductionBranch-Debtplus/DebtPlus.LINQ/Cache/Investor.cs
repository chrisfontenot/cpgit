﻿using System;
using System.Linq;

namespace DebtPlus.LINQ.Cache
{
    public static class Investor
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the Investor table
        private static System.Collections.Generic.List<DebtPlus.LINQ.Investor> colItems;

        /// <summary>
        /// Return the static copy of the Investor table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.Investor> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            try
                            {
                                colItems = dc.Investors.ToList();
                            }
                            catch (Exception ex)
                            {
                                String message = ex.Message;
                            }
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list.
        /// </summary>
        /// <returns></returns>
        public static System.Nullable<Int32> getDefault()
        {
            var q = getList().Where(s => s.Default).FirstOrDefault();
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}