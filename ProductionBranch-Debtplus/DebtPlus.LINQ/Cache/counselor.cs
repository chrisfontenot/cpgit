﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ.Cache
{
    public static class counselor
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the counselors table
        private static System.Collections.Generic.List<DebtPlus.LINQ.counselor> colItems;

        /// <summary>
        /// Return the static copy of the counselors table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.counselor> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            // Ask Linq to read the names and telephone numbers for the counselors as well...
                            // Use "LoadWith" and not "AssociateWith" since we want a greedy load of all of the data.
                            System.Data.Linq.DataLoadOptions dl = new System.Data.Linq.DataLoadOptions();
                            dl.LoadWith<DebtPlus.LINQ.counselor>(s => s.Name);              // load the name references
                            dl.LoadWith<DebtPlus.LINQ.counselor>(s => s.TelephoneNumber);   // load the telephone number references
                            dc.LoadOptions = dl;

                            dc.DeferredLoadingEnabled = false;
                            colItems = dc.counselors.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default, a suitable item is chosen.
        /// </summary>
        /// <returns></returns>
        public static System.Nullable<Int32> getDefault()
        {
            return getDefault(null);
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default, a suitable item is chosen.
        /// </summary>
        /// <param name="officeId">The desired office</param>
        /// <returns></returns>
        public static System.Nullable<Int32> getDefault(Int32? officeId)
        {
            // If the list is empty then just punt and return a "future" value for the first item.
            if (getList().Count <= 0)
            {
                return 1;
            }

            // If an office was requested, find it for the specified office. A counselor works by default in an office and we
            // should be asking for the default counselor in a specific office, not just a general default counselor.
            if (officeId.HasValue)
            {
                DebtPlus.LINQ.counselor coOff = (from c in getList() where c.Office == officeId.Value && c.Default select c).FirstOrDefault();
                if (coOff != null)
                {
                    return coOff.Id;
                }
            }

            // Look for a default office where the office is not specified.
            DebtPlus.LINQ.counselor coReg = (from c in getList() where c.Default orderby c.Id select c).FirstOrDefault();
            if (coReg != null)
            {
                return coReg.Id;
            }

            // Find the first item in the non-empty list.
            return getList().Min(b => b.Id);
        }

        public static System.Collections.Generic.List<DebtPlus.LINQ.counselor> CounselorsList()
        {
            return CounselorsList(null);
        }

        /// <summary>
        /// Return the list of jsut counselors. These are the people who see clients.
        /// </summary>
        /// <param name="CounselorID"></param>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.counselor> CounselorsList(Int32? CounselorID)
        {
            return CounselorsList(CounselorID, "COUNSELOR");
        }

        public static System.Collections.Generic.List<DebtPlus.LINQ.counselor> CSRsList()
        {
            return CSRsList(null);
        }

        /// <summary>
        /// Return the list of just customer service reps. These are the people who maintain the clients but don't necessarily counsel them.
        /// </summary>
        /// <param name="CounselorID"></param>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.counselor> CSRsList(Int32? CounselorID)
        {
            return CounselorsList(CounselorID, "CSR");
        }

        /// <summary>
        /// Return the list of just educators. These are the people who conduct a workshop.
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.counselor> EducatorsList()
        {
            return EducatorsList(null);
        }

        /// <summary>
        /// Return the list of just educators. These are the people who conduct a workshop. No other items are included.
        /// </summary>
        /// <param name="CounselorID">Include this ID in the list even if it is not active</param>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.counselor> EducatorsList(Int32? CounselorID)
        {
            return CounselorsList(CounselorID, "EDUCATOR");
        }

        /// <summary>
        /// Return a sub-set list of a specific role.
        /// </summary>
        /// <returns></returns>
        private static System.Collections.Generic.List<DebtPlus.LINQ.counselor> CounselorsList(Int32? CounselorID, string RoleName)
        {
            // Map the name to an attribute
            DebtPlus.LINQ.AttributeType AttributeID = DebtPlus.LINQ.Cache.AttributeType.getList().Find(s => s.Attribute == RoleName && s.Grouping == "ROLE");
            if (AttributeID != null)
            {

                // If there is a current counselor then return that counselor in the list even if it is not active
                if (CounselorID.HasValue)
                {
                    return (from co in Cache.counselor.getList()
                            join a in Cache.counselor_attribute.getList() on co.Id equals a.Counselor
                            where a.Attribute == AttributeID.Id && (co.ActiveFlag || co.Id == CounselorID.Value)
                            select co).ToList();
                }
                else
                {
                    return (from co in Cache.counselor.getList()
                            join a in Cache.counselor_attribute.getList() on co.Id equals a.Counselor
                            where a.Attribute == AttributeID.Id && co.ActiveFlag
                            select co).ToList();
                }
            }

            // The attribute is not located. Return an empty list.
            return new System.Collections.Generic.List<DebtPlus.LINQ.counselor>();
        }
    }
}
