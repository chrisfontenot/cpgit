﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ.Cache
{
    public static class MSACodeType
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the MSACodeTypes table
        private static System.Collections.Generic.List<DebtPlus.LINQ.MSACodeType> colItems;

        /// <summary>
        /// Return the static copy of the MSACodeTypes table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.MSACodeType> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.MSACodeTypes.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default then NULL is returned.
        /// </summary>
        /// <returns></returns>
        public static string getDefault()
        {
            return null;
        }
    }
}
