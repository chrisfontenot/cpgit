﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ.Cache
{
    public static class product_dispute_type
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the product_dispute_types table
        private static System.Collections.Generic.List<DebtPlus.LINQ.product_dispute_type> colItems;

        /// <summary>
        /// Return the static copy of the product_dispute_types table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.product_dispute_type> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.product_dispute_types.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default then NULL is returned.
        /// </summary>
        /// <returns></returns>
        public static System.Nullable<Int32> getDefault()
        {
            return null;
        }
    }
}
