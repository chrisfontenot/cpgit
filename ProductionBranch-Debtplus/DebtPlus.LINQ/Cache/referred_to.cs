﻿using System;
using System.Linq;

namespace DebtPlus.LINQ.Cache
{
    public static class referred_to
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the referred_by table
        private static System.Collections.Generic.List<DebtPlus.LINQ.referred_to> colItems;

        /// <summary>
        /// Return the static copy of the referred_to table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.referred_to> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.referred_tos.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default then NULL is returned.
        /// </summary>
        /// <returns></returns>
        public static System.Nullable<Int32> getDefault()
        {
            // There is no default. Just return NULL.
            return new System.Nullable<Int32>();
        }
    }
}