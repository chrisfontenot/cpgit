﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ.Cache
{
    public static class GenderType
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the genderTypes table
        private static System.Collections.Generic.List<DebtPlus.LINQ.GenderType> colItems;

        /// <summary>
        /// Return the static copy of the genderTypes table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.GenderType> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.GenderTypes.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default then NULL is returned.
        /// </summary>
        /// <returns></returns>
        public static System.Nullable<Int32> getDefault()
        {
            // Take the item marked "Default"
            var q = getList().Where(s => s.Default).FirstOrDefault();

            // If there is no item then take the first item
            if (q == null)
            {
                q = getList().FirstOrDefault();
            }

            // If we have an item now then return its id.
            if (q != null)
            {
                return q.Id;
            }

            // Otherwise, return null.
            return null;
        }
    }
}
