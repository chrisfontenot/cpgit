﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ.Cache
{
    public static class InboundTelephoneNumber
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the ethnicityTypes table
        private static System.Collections.Generic.List<DebtPlus.LINQ.InboundTelephoneNumber> colItems;

        /// <summary>
        /// Return the static copy of the InboundTelephoneNumberGroupes table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.InboundTelephoneNumber> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        colItems = new System.Collections.Generic.List<DebtPlus.LINQ.InboundTelephoneNumber>();
                        foreach (var grp in DebtPlus.LINQ.Cache.InboundTelephoneNumberGroup.getList())
                        {
                            colItems.AddRange(grp.InboundTelephoneNumbers);
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default then NULL is returned.
        /// </summary>
        /// <returns></returns>
        public static System.Nullable<Int32> getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}
