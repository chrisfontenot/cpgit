﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ.Cache
{
    public static class Document
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the Documents table
        private static System.Collections.Generic.List<DebtPlus.LINQ.Document> colItems;

        /// <summary>
        /// Return the static copy of the Documents table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.Document> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            // Load the corresponding attribute list with the component items
                            var dl = new System.Data.Linq.DataLoadOptions();
                            dl.LoadWith<DebtPlus.LINQ.Document>(s => s.DocumentComponents);
                            dc.LoadOptions = dl;

                            colItems = dc.Documents.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default then NULL is returned.
        /// </summary>
        /// <returns></returns>
        public static System.Nullable<Int32> getDefault()
        {
            return null;
        }
    }
}
