﻿using System;
using System.Linq;

namespace DebtPlus.LINQ.Cache
{
    public static class postal_abbreviation
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the postal_abbreviations table
        private static System.Collections.Generic.List<DebtPlus.LINQ.postal_abbreviation> colItems;

        /// <summary>
        /// Return the static copy of the postal_abbreviations table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.postal_abbreviation> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.postal_abbreviations.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default then NULL is returned.
        /// </summary>
        /// <returns></returns>
        public static System.Nullable<Int32> getDefault()
        {
            return getList().Where(s => s.Default).Select(s => s.Id).FirstOrDefault();
        }
    }
}