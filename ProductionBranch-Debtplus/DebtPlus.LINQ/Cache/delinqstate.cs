﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ.Cache
{
    public static class DelinqState
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the DelinqState table
        private static System.Collections.Generic.List<DebtPlus.LINQ.DelinqStateType> colItems;

        /// <summary>
        /// Return the static copy of the DelinqState table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.DelinqStateType> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.DelinqStateTypes.OrderBy(b => b.Sortorder).ThenBy(b => b.description).ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default, a suitable item is chosen.
        /// </summary>
        /// <returns></returns>
        public static System.Nullable<Int32> getDefault()
        {
            return getList().Where(s => s.Default).Select(s => s.Id).FirstOrDefault();
        }

        public static List<DelinqStateType> getHOA()
        {
            List<DelinqStateType> listRange = new List<DelinqStateType>();
            var lst = getList();
            if (lst.Count > 0)
            {
                listRange.AddRange(lst);
            }

            listRange.Add(new DelinqStateType { Id = 3, description = "No HOA Dues" });

            return listRange;
        }
    }
}
