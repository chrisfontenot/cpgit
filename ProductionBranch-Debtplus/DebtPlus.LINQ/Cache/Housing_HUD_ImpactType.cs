﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ.Cache
{
    public static class Housing_HUD_ImpactType
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the Housing_HUD_ImpactTypes table
        private static System.Collections.Generic.List<DebtPlus.LINQ.Housing_HUD_ImpactType> colItems;

        /// <summary>
        /// Return the static copy of the Housing_HUD_ImpactTypes table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.Housing_HUD_ImpactType> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.Housing_HUD_ImpactTypes.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default then NULL is returned.
        /// Since there is no default, the answer is always null.
        /// </summary>
        /// <returns></returns>
        public static System.Nullable<Int32> getDefault()
        {
            return null;
        }
    }
}
