﻿using System;
using System.Linq;

namespace DebtPlus.LINQ.Cache
{
    public static class retention_event
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the countries table
        private static System.Collections.Generic.List<DebtPlus.LINQ.retention_event> colItems;

        /// <summary>
        /// Return the static copy of the countries table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.retention_event> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.retention_events.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default, a suitable item is chosen.
        /// </summary>
        /// <returns></returns>
        public static Int32? getDefault()
        {
            // There is no default
            return null;
        }
    }
}