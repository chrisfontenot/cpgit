﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ.Cache
{
    public static class creditor_type
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the creditor_types table
        private static System.Collections.Generic.List<DebtPlus.LINQ.creditor_type> colItems;

        /// <summary>
        /// Return the static copy of the creditor_types table
        /// </summary>
        public static System.Collections.Generic.List<DebtPlus.LINQ.creditor_type> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.creditor_types.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. There isn't one so NULL is returned.
        /// </summary>
        public static string getDefault()
        {
            return null;
        }
    }
}
