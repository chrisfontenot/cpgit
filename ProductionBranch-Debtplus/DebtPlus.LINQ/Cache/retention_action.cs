﻿using System;
using System.Linq;

namespace DebtPlus.LINQ.Cache
{
    public static class retention_action
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the retention_actions table
        private static System.Collections.Generic.List<DebtPlus.LINQ.retention_action> colItems;

        /// <summary>
        /// Return the static copy of the retention_actions table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.retention_action> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.retention_actions.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default then NULL is returned.
        /// </summary>
        /// <returns></returns>
        public static System.Nullable<Int32> getDefault()
        {
            // There is no column marked "default" so there is no default item.
            return null;
        }
    }
}