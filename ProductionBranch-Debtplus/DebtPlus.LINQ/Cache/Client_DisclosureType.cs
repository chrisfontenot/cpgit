﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ.Cache
{
    public static class client_DisclosureType
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the client_DisclosureTypes table
        private static System.Collections.Generic.List<DebtPlus.LINQ.client_DisclosureType> colItems;

        /// <summary>
        /// Return the static copy of the client_DisclosureTypes table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.client_DisclosureType> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            // Merge the items with the languages suitable for the types
                            System.Data.Linq.DataLoadOptions dl = new System.Data.Linq.DataLoadOptions();
                            dl.LoadWith<DebtPlus.LINQ.client_DisclosureType>(s => s.client_DisclosureLanguageTypes);
                            dc.LoadOptions = dl;

                            colItems = dc.client_DisclosureTypes.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default then NULL is returned.
        /// </summary>
        /// <returns></returns>
        public static Int32? getDefault()
        {
            return getList().Where(s => s.Default).Select(s => s.Id).FirstOrDefault();
        }
    }
}
