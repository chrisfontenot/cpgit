﻿using System;
using System.Linq;

namespace DebtPlus.LINQ.Cache
{
    public static class HPFEventOutcomeType
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the HPFEventOutcomeTypes table
        private static System.Collections.Generic.List<DebtPlus.LINQ.HPFEventOutcomeType> colItems;

        /// <summary>
        /// Return the static copy of the HPFEventOutcomeTypes table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.HPFEventOutcomeType> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.HPFEventOutcomeTypes.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default then NULL is returned.
        /// </summary>
        /// <returns></returns>
        public static System.Nullable<Int32> getDefault()
        {
            var item = getList().Where(s => s.Default).FirstOrDefault();
            if (item == null)
            {
                item = getList().FirstOrDefault();
            }

            if (item != null)
            {
                return item.Id;
            }

            return null;
        }
    }
}