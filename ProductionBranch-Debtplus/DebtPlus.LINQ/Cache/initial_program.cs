﻿using System;
using System.Linq;

namespace DebtPlus.LINQ.Cache
{
    public static class InitialProgram
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the client_situation table
        private static System.Collections.Generic.List<DebtPlus.LINQ.initial_program> colItems;

        /// <summary>
        /// Return the static copy of the client_situation table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.initial_program> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.initial_programs.OrderBy(b => b.sortorder).ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default, a suitable item is chosen.
        /// </summary>
        /// <returns></returns>
        public static Int32 getDefault()
        {
            DebtPlus.LINQ.initial_program defaultItem = null;
            if (getList().Count > 0)
            {
                defaultItem = getList().OrderBy(b => b.sortorder).First();
            }
            return (int)defaultItem.Id;
        }
    }
}