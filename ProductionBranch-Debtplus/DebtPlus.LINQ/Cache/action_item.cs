﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ.Cache
{
    public static class action_item
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the action_items table
        private static System.Collections.Generic.List<DebtPlus.LINQ.action_item> colItems;

        /// <summary>
        /// Return the static copy of the action_items table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.action_item> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.action_items.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default, a suitable item is chosen.
        /// </summary>
        /// <returns>There is no default item. This is a collection of checked items.</returns>
        public static System.Nullable<Int32> getDefault()
        {
            throw new NotImplementedException("getDefault() is not implemented");
        }
    }
}
