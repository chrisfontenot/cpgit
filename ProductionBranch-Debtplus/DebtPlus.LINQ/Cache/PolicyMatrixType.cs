﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ.Cache
{
    public static class policy_matrix_policy_type
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the attributeTypes table
        private static System.Collections.Generic.List<DebtPlus.LINQ.policy_matrix_policy_type> colItems;

        /// <summary>
        /// Return the static copy of the attributeTypes table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.policy_matrix_policy_type> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.policy_matrix_policy_types.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Get the ID of the default for the field.
        /// </summary>
        /// <returns></returns>
        public static string getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}
