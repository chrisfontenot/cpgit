﻿using System;
using System.Linq;

namespace DebtPlus.LINQ.Cache
{
    public static class job_description
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the job_descriptions table
        private static System.Collections.Generic.List<DebtPlus.LINQ.job_description> colItems;

        /// <summary>
        /// Return the static copy of the job_descriptions table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.job_description> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.job_descriptions.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        public static System.Collections.Generic.List<DebtPlus.LINQ.job_description> getAarpList()
        {
            return getList().Where(x => !string.IsNullOrWhiteSpace(x.aarp)).ToList();
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default then NULL is returned.
        /// </summary>
        /// <returns></returns>
        public static System.Nullable<Int32> getDefault()
        {
            // Take the first item in the list as the default. If the list is empty then null is returned.
            return getList().OrderBy(s => s.Id).Take(1).Select(s => s.Id).SingleOrDefault();
        }
    }
}