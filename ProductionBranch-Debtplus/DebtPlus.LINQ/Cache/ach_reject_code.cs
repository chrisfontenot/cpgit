﻿using System.Linq;

namespace DebtPlus.LINQ.Cache
{
    public static class ach_reject_code
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the ach_reject_codes table
        private static System.Collections.Generic.List<DebtPlus.LINQ.ach_reject_code> colItems;

        /// <summary>
        /// Return the static copy of the ach_reject_codes table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.ach_reject_code> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var bc = new BusinessContext())
                        {
                            colItems = bc.ach_reject_codes.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default then NULL is returned.
        /// </summary>
        /// <returns></returns>
        public static string getDefault()
        {
            // Take the first item in the list as the default. If the list is empty then null is returned.
            return getList().OrderBy(s => s.Id).Take(1).Select(s => s.Id).SingleOrDefault();
        }
    }
}