﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ.Cache
{
    public static class TelephoneType
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the TelephoneTypes table
        private static System.Collections.Generic.List<DebtPlus.LINQ.TelephoneType> colItems;

        /// <summary>
        /// Return the static copy of the TelephoneTypes table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.TelephoneType> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.TelephoneTypes.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        public static System.Nullable<Int32> getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}
