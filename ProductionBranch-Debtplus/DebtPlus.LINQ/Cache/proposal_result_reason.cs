﻿using System;
using System.Linq;

namespace DebtPlus.LINQ.Cache
{
    public static class proposal_result_reason
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the proposal_result_reasons table
        private static System.Collections.Generic.List<DebtPlus.LINQ.proposal_result_reason> colItems;

        /// <summary>
        /// Return the static copy of the proposal_result_reasons table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.proposal_result_reason> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.proposal_result_reasons.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. The default is simply "none".
        /// </summary>
        /// <returns></returns>
        public static Int32? getDefault()
        {
            return new System.Int32?();
        }
    }
}