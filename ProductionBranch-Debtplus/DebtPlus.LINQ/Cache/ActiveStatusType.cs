﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ.Cache
{
    public static class ActiveStatusType
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the active_status table
        private static System.Collections.Generic.List<DebtPlus.LINQ.active_status> colItems;

        /// <summary>
        /// Return the static copy of the active_status table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.active_status> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.active_status.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default, a suitable item is chosen.
        /// </summary>
        /// <returns></returns>
        public static System.String getDefault()
        {
            return "CRE";
        }
    }
}
