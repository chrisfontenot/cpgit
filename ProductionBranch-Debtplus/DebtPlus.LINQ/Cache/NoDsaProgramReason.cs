﻿using System;
using System.Linq;

namespace DebtPlus.LINQ.Cache
{
    public static class NoDsaProgramReason
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the NoDsaProgramReason table
        private static System.Collections.Generic.List<DebtPlus.LINQ.NoDsaProgramReason> colItems;

        /// <summary>
        /// Return the static copy of the NoDsaProgramReason table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.NoDsaProgramReason> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.NoDsaProgramReasons.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default, a suitable item is chosen.
        /// </summary>
        /// <returns></returns>
        public static Int32? getDefault()
        {
            var q = getList().Find(s => s.@default);
            if (q != null)
            {
                return q.Id;
            }

            return null;
        }
    }
}