﻿using System;
using System.Linq;

namespace DebtPlus.LINQ.Cache
{
    public static class Housing_StatusType
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the Housing_StatusTypes table
        private static System.Collections.Generic.List<DebtPlus.LINQ.Housing_StatusType> colItems;

        /// <summary>
        /// Return the static copy of the Housing_StatusTypes table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.Housing_StatusType> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.Housing_StatusTypes.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the static copy of the Housing_StatusTypes table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.Housing_StatusType> getAarpList()
        {
            return getList().Where(x => !string.IsNullOrWhiteSpace(x.aarp)).ToList();
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default then NULL is returned.
        /// </summary>
        /// <returns></returns>
        public static System.Nullable<Int32> getDefault()
        {
            return getList().Where(s => s.Default).Select(s => s.Id).FirstOrDefault();
        }
    }
}