﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ.Cache
{
    public static class country
    {
        // Lock object to prevent threading problems
        private static object objLock = new object();

        // Cached list of the countries table
        private static System.Collections.Generic.List<DebtPlus.LINQ.country> colItems;

        /// <summary>
        /// Return the static copy of the countries table
        /// </summary>
        /// <returns></returns>
        public static System.Collections.Generic.List<DebtPlus.LINQ.country> getList()
        {
            // Look to determine if the table has already been read
            if (colItems == null)
            {
                lock (objLock)
                {
                    if (colItems == null)
                    {
                        // Retrieve the table from the database
                        using (var dc = new BusinessContext())
                        {
                            colItems = dc.countries.ToList();
                        }
                    }
                }
            }
            return colItems;
        }

        /// <summary>
        /// Return the country record from the cached country table
        /// </summary>
        /// <param name="country"></param>
        /// <returns></returns>
        public static DebtPlus.LINQ.country countryRecordById(Int32 country)
        {
            return DebtPlus.LINQ.Cache.country.getList().Where(s => s.Id == country).FirstOrDefault();
        }

        /// <summary>
        /// Return the default item from the list. If none are marked default then NULL is returned.
        /// </summary>
        /// <returns></returns>
        public static System.Nullable<Int32> getDefault()
        {
            return getList().Where(s => s.Default).Select(s => s.Id).FirstOrDefault();
        }
    }
}
