﻿
namespace DebtPlus.LINQ
{
    partial class credit_card_payment_detail
    {
        partial void Onaccount_numberChanging(string value)
        {
            max_length(value, "account_number", 50);
        }

        partial void Onavs_addressChanging(string value)
        {
            max_length(value, "avs_address", 50);
        }

        partial void Onavs_cityChanging(string value)
        {
            max_length(value, "avs_city", 50);
        }

        partial void Onavs_full_nameChanging(string value)
        {
            max_length(value, "avs_full_name", 50);
        }

        partial void Onavs_stateChanging(string value)
        {
            max_length(value, "avs_state", 2);
        }

        partial void Onavs_zipcodeChanging(string value)
        {
            max_length(value, "avs_zipcode", 10);
        }

        partial void Oncard_status_flagChanging(string value)
        {
            max_length(value, "card_status_flag", 10);
        }

        partial void Onconfirmation_numberChanging(string value)
        {
            max_length(value, "confirmation_number", 50);
        }

        partial void Oncredit_card_typeChanging(string value)
        {
            max_length(value, "credit_card_type", 10);
        }

        partial void Onpayment_mediumChanging(string value)
        {
            max_length(value, "payment_medium", 20);
        }

        partial void Onresult_messageChanging(string value)
        {
            max_length(value, "result_message", 256);
        }

        partial void Onreturn_codeChanging(string value)
        {
            max_length(value, "return_code", 50);
        }

        private static void max_length(string value, string name, int maximum)
        {
            if (value != null)
            {
                if (value.Length > maximum)
                {
                    throw new System.ArgumentOutOfRangeException(string.Format("{0} allows only {1:f0} characters", name, maximum));
                }
            }
        }
    }
}
