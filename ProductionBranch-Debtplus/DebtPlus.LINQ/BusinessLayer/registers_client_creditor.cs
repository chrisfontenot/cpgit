﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class registers_client_creditor
    {
        /// <summary>
        /// Amount deducted from the check before payment is made
        /// </summary>
        public decimal deducted
        {
            get
            {
                if (creditor_type == 'D' && fairshare_amt.HasValue)
                {
                    return fairshare_amt.Value;
                }
                return 0M;
            }
        }

        /// <summary>
        /// Amount to be generated on a subsequent invoice
        /// </summary>
        public decimal billed
        {
            get
            {
                if (creditor_type != 'D' && creditor_type != 'N')
                {
                    if (fairshare_amt.HasValue)
                    {
                        return fairshare_amt.Value;
                    }
                }

                return 0M;
            }
        }

        /// <summary>
        /// Net amount of the transaction
        /// </summary>
        public decimal net
        {
            get
            {
                if (debit_amt.HasValue)
                {
                    return debit_amt.Value - deducted;
                }
                return 0M;
            }
        }
    }
}
