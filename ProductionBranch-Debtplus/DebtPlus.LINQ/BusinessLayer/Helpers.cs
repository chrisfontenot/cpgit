﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    internal static class Helpers
    {
        /// <summary>
        /// Compare two strings for equality. Either may be missing.
        /// </summary>
        /// <param name="a">String 1 to compare</param>
        /// <param name="b">String 2 to compare</param>
        /// <returns></returns>
        internal static Int32 Comparison(string a, string b)
        {
            // Handle the condition where one or both are not specified
            if (string.IsNullOrEmpty(b))
            {
                return string.IsNullOrEmpty(a) ? 0 : 1;
            }

            if (string.IsNullOrEmpty(a))
            {
                return -1;
            }

            // Compare the strings without regard to case or language
            return System.String.Compare(a, b, true, System.Globalization.CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Compare two integers for equality. Either may be missing.
        /// </summary>
        /// <param name="a">Integer 1 to compare</param>
        /// <param name="b">Integer 2 to compare</param>
        /// <returns></returns>
        internal static Int32 Comparison(System.Nullable<System.Int32> a, System.Nullable<System.Int32> b)
        {
            // Handle the condition where one or both are not specified.
            if (b == null || !b.HasValue)
            {
                return (a == null || !a.HasValue) ? 0 : 1;
            }

            if (a == null || !a.HasValue)
            {
                return -1;
            }

            // Finally, both are present. Compare the fields
            return a.Value.CompareTo(b.Value);
        }
    }
}
