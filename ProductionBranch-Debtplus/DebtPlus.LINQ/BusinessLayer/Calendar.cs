﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class Calendar
    {
        /// <summary>
        /// Detach the object from a Data Context
        /// </summary>
        public void Detach()
        {
            // These must be null for Attach() to work.
            this.PropertyChanged = null;
            this.PropertyChanging = null;
        }
    }
}
