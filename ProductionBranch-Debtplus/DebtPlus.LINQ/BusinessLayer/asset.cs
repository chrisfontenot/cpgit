﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class asset
    {
        /// <summary>
        /// Return the monthly amount figure corresponding to the pay frequency and dollar amount
        /// </summary>
        public decimal asset_amount
        {
            get
            {
                // If the pay frequency matches a value in the table then use the periods per year to find the yearly amount.
                // then convert the annual amount to a monthly figure.
                PayFrequencyType freq = DebtPlus.LINQ.Cache.PayFrequencyType.getList().Find(s => s.Id == frequency);
                if (freq != null)
                {
                    return System.Math.Floor((Convert.ToDecimal(freq.PeriodsPerYear) * amount / 12m) * 100M) / 100M;
                }

                // There is no matching frequency. Assume monthly and that makes it the same as the amount figure.
                return amount;
            }
        }
    }
}
