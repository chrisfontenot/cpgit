﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class vendor
    {
        public static readonly string BillingMode_None       = "NONE";
        public static readonly string BillingMode_ACH        = "ACH";
        public static readonly string BillingMode_CreditCard = "CREDITCARD";
    }
}
