﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class client_disclosure
    {
        /// <summary>
        /// Counselor name suitable for the display grid
        /// </summary>
        public string display_counselor
        {
            get
            {
                if (created_by != null)
                {
                    // Try to find the name in the counselor list. It is possible that it is no longer there.
                    var q = DebtPlus.LINQ.Cache.counselor.getList().Find(s => string.Compare(s.Person, created_by, true) == 0);
                    if (q != null && q.Name != null)
                    {
                        return q.Name.ToString();
                    }

                    // Use the counselor label otherwise
                    String answer = created_by.Trim();
                    Int32 SlashMaker = answer.LastIndexOfAny(new Char[] { '/', '\\' });
                    if (SlashMaker > 0)
                    {
                        answer = answer.Substring(SlashMaker + 1);
                    }
                    return answer;
                }
                return string.Empty;
            }
        }
    }
}
