﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class ZipCodeSearch
    {
        /// <summary>
        /// Convert the ZIPType field to a suitable display string
        /// </summary>
        public string Formatted_ZIPType
        {
            get
            {
                var q = DebtPlus.LINQ.InMemory.ZipcodeSearchZipTypes.getList().Find(s => s.Id == ZIPType);
                if (q != null)
                {
                    return q.description;
                }

                // When it fails, just us the type string which may be NULL
                return string.Empty;
            }
        }

        /// <summary>
        /// Convert the CityType field to a suitable display string
        /// </summary>
        public string Formatted_CityType
        {
            get
            {
                var q = DebtPlus.LINQ.InMemory.ZipcodeSearchCityTypes.getList().Find(s => s.Id == CityType);
                if (q != null)
                {
                    return q.description;
                }
                return string.Empty;
            }
        }

        /// <summary>
        /// Find the name of the corresponding state record
        /// </summary>
        public string Formatted_StateMailingCode
        {
            get
            {
                if (state1 != null && !string.IsNullOrEmpty(state1.MailingCode))
                {
                    return state1.MailingCode.Substring(0, 2);
                }
                return null;
            }
        }

        /// <summary>
        /// Find the name of the corresponding state record
        /// </summary>
        public string Formatted_StateName
        {
            get
            {
                if (state1 != null)
                {
                    return state1.Name;
                }
                return null;
            }
        }
    }
}
