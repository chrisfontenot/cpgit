﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class creditor_contribution_pct
    {
        /// <summary>
        /// Create an empty record with the proper values. These are "no contribution" settings.
        /// </summary>
        partial void OnCreated()
        {
            _creditor_type_check = 'N';
            _fairshare_pct_check = 0.0;

            _creditor_type_eft = 'N';
            _fairshare_pct_eft = 0.0;

            // The effective date is not nullable. Supply a default for "tomorrow".
            _effective_date = DateTime.Now.AddDays(1).Date;
        }

        /// <summary>
        /// Format the rate and contribution field
        /// </summary>
        /// <param name="rate"></param>
        /// <param name="cycle"></param>
        /// <returns></returns>
        public static string FormatRateAndContribution(double? rate, char cycle)
        {
            // It is none if there is no value for a rate or cycle
            if (rate.GetValueOrDefault() <= 0.0D || cycle == 'N')
            {
                return "None";
            }

            // It is deduct if so indicated
            if (cycle == 'D')
            {
                return string.Format("D({0:p})", rate.Value);
            }

            // Otherwise all other types are billed
            return string.Format("B({0:p})", rate.Value);
        }

        /// <summary>
        /// Used only in the list of contribution figures for the creditor update.
        /// </summary>
        public string status
        {
            get
            {
                char eft = (fairshare_pct_eft.GetValueOrDefault() <= 0.0D || this.creditor_type_eft == 'N') ? 'N' : creditor_type_eft;
                char check = (fairshare_pct_check <= 0.0D || this.creditor_type_check == 'N') ? 'N' : creditor_type_check;
                if (eft == check)
                {
                    return Convert.ToString(eft);
                }
                return eft + "/" + check;
            }
        }

        /// <summary>
        /// Convert the item to a string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string eftMode = FormatRateAndContribution(this.fairshare_pct_eft, this.creditor_type_eft);
            string chkMode = FormatRateAndContribution(this.fairshare_pct_check, this.creditor_type_check);
            if (string.Compare(eftMode, chkMode, true) == 0)
            {
                return eftMode;
            }
            return eftMode + "/" + chkMode;
        }
    }
}
