﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class Housing_ARM_referenceInfo
    {
        /// <summary>
        /// Handle the creation logic for the class instance
        /// </summary>
        partial void OnCreated()
        {
            Referenced = false;            
        }                

        /// <summary>
        /// Is the item referenced in the download section? Unreferenced items are deleted.
        /// </summary>
        public bool Referenced { get; set; }
    }
}
