﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ.BusinessLayer
{
    /// <summary>
    /// This class is necessary for use during mapping.  
    /// LINQ will not let us instantiate and populate the base class as part of our initial query.
    /// </summary>
    public class Address : DebtPlus.LINQ.address
    {
    }
}
