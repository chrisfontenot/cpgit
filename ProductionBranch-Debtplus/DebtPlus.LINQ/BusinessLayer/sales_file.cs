﻿using System;
using System.Linq;

namespace DebtPlus.LINQ
{
    partial class sales_file
    {
        /// <summary>
        /// Magic value when the value can not be determined
        /// </summary>
        public static readonly double CAN_NOT_BE_DETERMINED = -1F;

        private object RecalculateSelfPayoutLock = new object();

        /// <summary>
        /// Length of the plan in months
        /// </summary>
        public double DMP_LengthMonths
        {
            get
            {
                // Calculate the plan length based upon the total balance, the average interest rate, and the sum(debt payments) + extra amount as a payment figure
                // We can do this because the amount is fixed and does not reduce over time based upon the balances.
                // Also, we want to reflect the reduction in time as you add "extra amount" to the base line figure.

                // Handle the fringe cases where someone may be trying to fool us.
                decimal CurrentAmount = TotalBalance;
                if (CurrentAmount <= 0M)
                {
                    return 0F;
                }

                decimal CurrentPayment = DMP_TotalPayment;
                if (CurrentAmount <= CurrentPayment)
                {
                    return 1F;
                }

                try
                {
                    double CurrentRate = DMP_TotalInterestRate;
                    return System.Math.Ceiling(Microsoft.VisualBasic.Financial.NPer(CurrentRate / 12F, Convert.ToDouble(0M - CurrentPayment), Convert.ToDouble(CurrentAmount), 0F, Microsoft.VisualBasic.DueDate.BegOfPeriod));
                }
                catch
                {
                    return CAN_NOT_BE_DETERMINED;
                }
            }
        }

        /// <summary>
        /// Should the Plan payout schedule be recalculated because something changed?
        /// </summary>
        public Boolean DMP_NeedToRecalculate { get; set; }

        /// <summary>
        /// Total plan payments
        /// </summary>
        public decimal DMP_TotalAmountPaid
        {
            get
            {
                return TotalBalance + DMP_TotalInterestPaid;
            }
        }

        /// <summary>
        /// Total amount paid on the plan
        /// </summary>
        public decimal DMP_TotalEnteredPayment
        {
            get
            {
                return this.sales_debts.Sum(d => d.DMP_Payment);
            }
        }

        /// <summary>
        /// Total plan fees
        /// </summary>
        public decimal DMP_TotalFees
        {
            get
            {
                return this.sales_debts.Sum(d => d.DMP_Fees);
            }
        }

        /// <summary>
        /// Total plan finance charges
        /// </summary>
        public decimal DMP_TotalFinanceCharge
        {
            get
            {
                return this.sales_debts.Sum(d => d.DMP_FinanceCharge);
            }
        }

        /// <summary>
        /// Total dollars going to principal for plan
        /// </summary>
        public decimal DMP_TotalGoingToPrincipal
        {
            get
            {
                return this.sales_debts.Sum(d => d.DMP_GoingToPrincipal);
            }
        }

        /// <summary>
        /// Total plan interest and fees
        /// </summary>
        public decimal DMP_TotalInterestFees
        {
            get
            {
                return this.sales_debts.Sum(d => d.DMP_InterestFees);
            }
        }

        /// <summary>
        /// Total plan interest paid for all debts
        /// </summary>
        public decimal DMP_TotalInterestPaid
        {
            get
            {
                // Compute the total interest paid as the simple formula of:
                // interest = (payment amount * plan length) - original balance

                try
                {
                    // First, calculate the length of the plan in fractional months.
                    decimal CurrentAmount = TotalBalance;
                    double CurrentRate = DMP_TotalInterestRate;
                    decimal CurrentPayment = DMP_TotalPayment;
                    double PlanLength = Microsoft.VisualBasic.Financial.NPer((CurrentRate / 12.0F), Convert.ToDouble(0M - CurrentPayment), Convert.ToDouble(CurrentAmount), 0F, Microsoft.VisualBasic.DueDate.BegOfPeriod);

                    // Then take the monthly payment times the number of months less the principal amount
                    decimal answer = Convert.ToDecimal(Convert.ToDouble(CurrentPayment) * PlanLength) - CurrentAmount;
                    if (answer > 0M)
                    {
                        return answer;
                    }
                }
                catch { }

                return 0M;
            }
        }

        /// <summary>
        /// Total plan interest rate
        /// </summary>
        public double DMP_TotalInterestRate
        {
            get
            {
                try
                {
                    // Total interest rate = totalfinancecharge / totalbalance
                    double result = Convert.ToDouble(DMP_TotalFinanceCharge) / Convert.ToDouble(TotalBalance) * 12.0F;
                    if (Double.IsNaN(result))
                    {
                        result = 0.0F;
                    }
                    return System.Math.Truncate(result * 100000F) / 100000F;
                }
                catch
                {
                    return 0.0F;
                }
            }
        }

        /// <summary>
        /// Total plan payment
        /// </summary>
        public decimal DMP_TotalMinimumPayment
        {
            get
            {
                return this.sales_debts.Sum(d => d.DMP_MinimumPayment);
            }
        }

        /// <summary>
        /// Total plan payment
        /// </summary>
        public decimal DMP_TotalPayment
        {
            get
            {
                return DMP_TotalEnteredPayment + extra_amount;
            }
        }

        /// <summary>
        /// Total percentage going to principal for plan
        /// </summary>
        public double DMP_TotalPercentGoingToprincipal
        {
            get
            {
                try
                {
                    double Result = Convert.ToDouble(DMP_TotalGoingToPrincipal) / Convert.ToDouble(DMP_TotalEnteredPayment);
                    return System.Math.Truncate(Result * 100000F) / 100000F;
                }
                catch
                {
                    return 0.0F;
                }
            }
        }

        /// <summary>
        /// Monthly savings in interest rate
        /// </summary>
        public double MonthlySavings_EstimatedInterestRate
        {
            get
            {
                return SELF_TotalInterestRate - DMP_TotalInterestRate;
            }
        }

        /// <summary>
        /// Monthly savings amount for finance charges
        /// </summary>
        public decimal MonthlySavings_FinanceCharge
        {
            get
            {
                return SELF_TotalFinanceCharge - DMP_TotalFinanceCharge;
            }
        }

        /// <summary>
        /// Monthly savings amount
        /// </summary>
        public decimal MonthlySavings_Payments
        {
            get
            {
                return SELF_TotalPayoutPayment - DMP_TotalEnteredPayment;
            }
        }

        /// <summary>
        /// Monthly savings for interest and fees
        /// </summary>
        public decimal MonthlySavings_TotalInterestAndFees
        {
            get
            {
                return SELF_TotalInterestAndFees - DMP_TotalInterestFees;
            }
        }

        /// <summary>
        /// Savings amount
        /// </summary>
        public decimal Savings
        {
            get
            {
                return SELF_InterestSavings + SELF_LateOverlimitFeeSavings;
            }
        }

        /// <summary>
        /// Self-administered savings amount
        /// </summary>
        public decimal SELF_InterestSavings
        {
            get
            {
                return SELF_TotalInterestPaid - DMP_TotalInterestPaid;
            }
        }

        /// <summary>
        /// Self-administered over-limit fees
        /// </summary>
        public decimal SELF_LateOverlimitFeeSavings
        {
            get
            {
                return SELF_TotalLateFees + SELF_TotalOverLimitFees;
            }
        }

        /// <summary>
        /// Length of the self-administered plan
        /// </summary>
        public double SELF_LengthMonths
        {
            get
            {
                PerformSelfPayoutRecaluation();

                double SELF_LastPayoutMonths = CAN_NOT_BE_DETERMINED;

                // Calculate the figures for each debt. Take the longer item.
                foreach (sales_debt d in sales_debts)
                {
                    d.SELF_Length_Months = 0F;
                    double months = d.SELF_Length_Months;
                    if (months > SELF_LastPayoutMonths)
                    {
                        SELF_LastPayoutMonths = months;
                    }
                }

                return SELF_LastPayoutMonths;
            }
        }

        /// <summary>
        /// Should the Client payout schedule be recalculated because something changed?
        /// </summary>
        public Boolean SELF_NeedToRecalculate { get; set; }

        /// <summary>
        /// Total balance for all debts when self-paid
        /// </summary>
        public decimal SELF_TotalBalance
        {
            get
            {
                return TotalBalance;
            }
        }

        /// <summary>
        /// Total co-signer fees for all debts
        /// </summary>
        public decimal SELF_TotalCosignerFees
        {
            get
            {
                return this.sales_debts.Sum(d => d.cosigner_fees);
            }
        }

        /// <summary>
        /// Total finance charges for all debts
        /// </summary>
        public decimal SELF_TotalFinanceCharge
        {
            get
            {
                return this.sales_debts.Sum(d => d.SELF_MonthlyFinanceCharge);
            }
        }

        /// <summary>
        /// Total amount going to principal for all debts when self-paid
        /// </summary>
        public decimal SELF_TotalGoingToPrincipal
        {
            get
            {
                return this.sales_debts.Sum(d => d.SELF_GoingToPrincipal);
            }
        }

        /// <summary>
        /// Total interest and fees for all debts
        /// </summary>
        public decimal SELF_TotalInterestAndFees
        {
            get
            {
                return this.sales_debts.Sum(d => d.SELF_InterestAndFees);
            }
        }

        /// <summary>
        /// Total self-administered interest paid
        /// </summary>
        public decimal SELF_TotalInterestPaid
        {
            get
            {
                PerformSelfPayoutRecaluation();
                try
                {
                    decimal answer = this.sales_debts.Sum(d => d.SELF_PayoutInterest);
                    if (answer > 0M)
                    {
                        return System.Math.Truncate(answer * 100M) / 100M;
                    }
                }
                catch { }

                return 0M;
            }
        }

        /// <summary>
        ///     Return the total interest rate information. This is not really a total, but a recomputation of the average for the totals as finance charges / balance.
        /// </summary>
        public double SELF_TotalInterestRate
        {
            get
            {
                double Result;

                // Total interest rate = totalfinancecharge / totalbalance
                try
                {
                    Result = Convert.ToDouble(SELF_TotalFinanceCharge) / Convert.ToDouble(TotalBalance) * 12.0F;
                    if (Double.IsNaN(Result))
                    {
                        Result = 0.0F;
                    }
                    Result = System.Math.Truncate((Result * 100.0F) * 1000F) / 100000F;
                }
                catch
                {
                    Result = 0F;
                }

                return Result;
            }
        }

        /// <summary>
        /// Total late fees for all debts
        /// </summary>
        public decimal SELF_TotalLateFees
        {
            get
            {
                return this.sales_debts.Sum(d => d.late_fees);
            }
        }

        /// <summary>
        /// Total over-limit fees for all debts
        /// </summary>
        public decimal SELF_TotalOverLimitFees
        {
            get
            {
                return this.sales_debts.Sum(d => d.overlimit_fees);
            }
        }

        /// <summary>
        /// Total self-administered amount paid for all debts
        /// </summary>
        public decimal SELF_TotalPaid
        {
            get
            {
                return TotalBalance + SELF_TotalInterestPaid;
            }
        }

        /// <summary>
        /// Total payout payment amount
        /// </summary>
        public decimal SELF_TotalPayoutPayment
        {
            get
            {
                return this.sales_debts.Sum(d => d.SELF_MonthlyPayment);
            }
        }

        /// <summary>
        /// Total percentage going to principal for all debts when self-paid
        /// </summary>
        public double SELF_TotalPercentGoingToPrincipal
        {
            get
            {
                try
                {
                    double Result = Convert.ToDouble(SELF_TotalGoingToPrincipal) / Convert.ToDouble(SELF_TotalPayoutPayment);
                    return System.Math.Truncate(Result * 100000F) / 100000F;
                }
                catch
                {
                    return 0.0F;
                }
            }
        }

        /// <summary>
        /// Total balance of all of the debts in the plan
        /// </summary>
        public decimal TotalBalance
        {
            get
            {
                return this.sales_debts.Sum(d => d.balance);
            }
        }

        /// <summary>
        /// Total interest savings on the plan
        /// </summary>
        public decimal TotalInterestSavings
        {
            get
            {
                return 0M;      // There is no "real" way to calculate this.
                                // It might be possible to compute the difference in self interest amount (assuming minimum payments) vs. plan interest amounts, but that is stretching the boundaries of credibility.
            }
        }

        /// <summary>
        /// Total savings for late fees
        /// </summary>
        public decimal TotalLateFeeSavings
        {
            get
            {
                return 0M;  // This is not possible at this point.
            }
        }

        /// <summary>
        /// Handle the creation of the class
        /// </summary>
        partial void OnCreated()
        {
            DMP_NeedToRecalculate = true;
            SELF_NeedToRecalculate = true;
        }

        /// <summary>
        /// Recalculate the self-paid payout length
        /// </summary>
        private void PerformSelfPayoutRecaluation()
        {
            if (SELF_NeedToRecalculate)
            {
                lock (RecalculateSelfPayoutLock)
                {
                    if (SELF_NeedToRecalculate)
                    {
                        foreach (sales_debt d in sales_debts)
                        {
                            d.SELF_CalculatePayout();
                        }
                        SELF_NeedToRecalculate = false;
                    }
                }
            }
        }
    }
}