﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class zipcode
    {
        public string PostalCode
        {
            get
            {
				string zip = zip_lower.ToString();  // Make null values an empty string.
                if (zip.Length < 5)
                {
                    return zip_lower;
                }
                return zip.Substring(0, 5);
            }
            set
            {
                zip_lower = value;
            }
        }
    }
}
