﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class Housing_HUD_ImpactType
    {
        /// <summary>
        /// Convert the object to a suitable string
        /// </summary>
        public override string ToString()
        {
            return this.description ?? string.Empty;
        }
    }
}
