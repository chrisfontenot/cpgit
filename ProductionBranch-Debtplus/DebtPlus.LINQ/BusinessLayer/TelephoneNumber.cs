﻿#pragma warning disable 414

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq.Mapping;

namespace DebtPlus.LINQ
{
    partial class TelephoneNumber : System.IComparable<TelephoneNumber>, DebtPlus.Interfaces.TelephoneNumbers.ITelephoneNumber
    {
        /// <summary>
        /// Try to parse the input value to a suitable structure
        /// </summary>
        /// <param name="InputString">String to be parsed into the current structure.</param>
        /// <returns></returns>
        public static TelephoneNumber TryParse(string InputString)
        {
            // if there is no number then empty the fields
            if (InputString == string.Empty)
            {
                return null;
            }

            // Try to use the number as a telephone number if possible. if the field is 7 digits or 10 digits then
            // accept it as the telephone number. 10 digits would include the area code field.
            System.Text.RegularExpressions.Match matches = System.Text.RegularExpressions.Regex.Match(InputString, @"^(?<number>([0-9]{1,7})|([0-9]{3}-[0-9]{4}))(\s*(extension\.|extension|x\.|x|ext\.|ext)\s*(?<ext>\d+))?$", System.Text.RegularExpressions.RegexOptions.CultureInvariant | System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            if (!matches.Success)
            {
                matches = System.Text.RegularExpressions.Regex.Match(InputString, @"^(\+(?<country>\d+))?\s*(?<acode>\d\d\d)?(?<number>[-0-9]+)(\s*(extension\.|extension|x\.|x|ext\.|ext)\s*(?<ext>\d+))?$", System.Text.RegularExpressions.RegexOptions.CultureInvariant | System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                if (!matches.Success)
                {
                    matches = System.Text.RegularExpressions.Regex.Match(InputString, @"^(\+(?<country>\d+))?\s*(\((?<acode>\d+)\)\s*?|(?<acode>\d+)\-|(?<acode>\d+)\s|(?<acode>\d+)\.)?(?<number>[-0-9]+)(\s*(extension\.|extension|x\.|x|ext\.|ext)\s*(?<ext>\d+))?$", System.Text.RegularExpressions.RegexOptions.CultureInvariant | System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                }
            }

            // if there is no match then the parse is not valid
            if (!matches.Success)
            {
                return null;
            }

            // Allocate a structure to hold the result if all goes well.
            var answer = DebtPlus.LINQ.Factory.Manufacture_TelephoneNumber();

            // Try to find the dialing code from the input string.
            string DialingCode;
            if (! string.IsNullOrWhiteSpace(matches.Groups["country"].Value))
            {
                DialingCode = matches.Groups["country"].Value.Trim();
            }
            else
            {
                DialingCode = string.Empty;
            }

            // If there is a dialing code then try to find the associated country for it.
            // There are duplicates in the map from dialing code to country as several
            // countries have the same dialing code, but we will take a shot at finding at
            // least one that is a "good guess".
            if (DialingCode != string.Empty)
            {
                Int32 intDialingCode;
                if (!Int32.TryParse(DialingCode, out intDialingCode) && intDialingCode > 0)
                {
                    return null;
                }

                var ctry = DebtPlus.LINQ.Cache.country.getList().Find(s => s.country_code == intDialingCode);
                if (ctry != null)
                {
                    answer.Country = ctry.Id;
                }
            }

            // Find the area code entry
            if (! string.IsNullOrWhiteSpace(matches.Groups["acode"].Value))
            {
                answer.Acode = matches.Groups["acode"].Value.Trim();
            }

            // Obtain the fields from the expression search. An extension is invalid if there is no number.
            answer.Number = matches.Groups["number"].Value;

            // Format the number if possible
            if (answer.Country == 1 && answer.Number.Length == 7)
            {
                answer.Number = answer.Number.Substring(0, 3) + "-" + answer.Number.Substring(3);
            }

            // Generate the extension field
            answer.Ext = (answer.Number.Length == 0) ? string.Empty : matches.Groups["ext"].Value;

            // Finally, the parse is valid.
            return answer;
        }

        /// <summary>
        /// Format the number with the simple values. We don't do anything complicated such as look up the country
        /// code for the proper dialing sequence, but it should work for most USA format telephone numbers.
        /// </summary>
        /// <returns></returns>
        public string FormattedNumber()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            DebtPlus.LINQ.country c = null;

            // Find the local number
            if (!string.IsNullOrEmpty(Number))
            {
                sb.Append(Number);

                // Find the extension
                if (sb.Length > 0 && !string.IsNullOrEmpty(Ext))
                {
                    sb.AppendFormat(" x {0}", Ext);
                }
            }

            // Find the country code
            string temp_Country = string.Empty;
            if (Country.HasValue && Country.Value > 0)
            {
                c = DebtPlus.LINQ.Cache.country.getList().Find(s => s.Id == Country.Value);
                if (c != null && !c.Default)
                {
                    temp_Country = string.Format("+{0:f0} ", c.country_code);
                }
            }

            // If there is a number then add the area code to the number
            if (sb.Length > 0 || temp_Country.Length > 0)
            {
                if (!string.IsNullOrEmpty(Acode) && Acode != "000")
                {
                    sb.Insert(0, string.Format("({0}) ", Acode));
                }

                // Add the country to the item
                if (temp_Country.Length > 0)
                {
                    sb.Insert(0, temp_Country);
                }
            }

            return sb.ToString().Trim();
        }

        /// <summary>
        /// Search value for doing a database search on the telephone number. This is a database generated field
        /// and is the combination of the area code and local telephone number, without any formatting.
        /// </summary>
        public string SearchValue
        {
            get
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                if (!string.IsNullOrEmpty(Acode))
                {
                    sb.Append(Acode);
                }

                if (!string.IsNullOrEmpty(BaseNumber))
                {
                    sb.Append(BaseNumber);
                }

                return sb.ToString().Trim();
            }
        }

        /// <summary>
        /// Create a new instance of the class
        /// </summary>
        /// <param name="Country">Country ID</param>
        /// <param name="Acode">Area code for the county</param>
        /// <param name="Number">Local number for the area code</param>
        /// <param name="Ext">Extension for the local number</param>
        public TelephoneNumber(Int32 Country, string Acode, string Number, string Ext)
            : this()
        {
            _Country = Country;
            _Acode   = Acode;
            _Number  = Number;
            _Ext     = Ext;
        }

        /// <summary>
        /// Is the current object a new telephone record?
        /// </summary>
        /// <returns></returns>
        public bool IsNew()
        {
            return Id <= 0;
        }

        /// <summary>
        /// Is the current object empty?
        /// </summary>
        /// <returns></returns>
        public bool IsEmpty()
        {
            if (Country == null || string.IsNullOrWhiteSpace(Acode) || string.IsNullOrWhiteSpace(Number))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Is the number a valid telephone number or not?
        /// </summary>
        /// <returns>TRUE if the number is valid. FALSE if not.</returns>
        public bool IsValid
        {
            get
            {
                // Accept a blank telephone number
                if (IsEmpty())
                {
                    return true;
                }

                // The number is not blank. Then all required fields must be present.
                if (Country == null || string.IsNullOrWhiteSpace(Acode) || string.IsNullOrWhiteSpace(Number))
                {
                    return false;
                }

                // If the country is "USA" then validate the telephone number to be 7 digits and the area code to be 3
                if (Country.Value == 1)
                {
                    System.Text.RegularExpressions.Regex rx = new System.Text.RegularExpressions.Regex(@"^\d\d\d$");
                    if (!rx.IsMatch(Acode))
                    {
                        return false;
                    }

                    rx = new System.Text.RegularExpressions.Regex(@"^\d\d\d(-?)\d\d\d\d$");
                    if (!rx.IsMatch(Number))
                    {
                        return false;
                    }
                }

                // The number is valid.
                return true;
            }
        }

        /// <summary>
        /// Compare the two telephone number objects to determine relationship
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(TelephoneNumber other)
        {
            return string.Compare(ToString(), other.ToString(), true, System.Globalization.CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// String representation of the telephone number
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return FormattedNumber();
        }

        private static System.Text.RegularExpressions.Regex sanitizePhone = new System.Text.RegularExpressions.Regex(@"\D");

        /// <summary>
        /// Local routine to return the number without any special formatting.
        /// </summary>
        private string BaseNumber
        {
            get
            {
                if (!string.IsNullOrEmpty(Number))
                {
                    return sanitizePhone.Replace(Number, String.Empty);
                }
                return null;
            }
        }

/*
 * These are+ needed so that LINQ can map to the correct column in the database.  
 * Normally we would let the LINQ tooling (via .dbml file) generate this for us.
 * However, when that was done, it completely broke the TelephoneNumber entity.  
 * So instead, we are manually adding here what would normally be generated.
 */
        private string _user_selected_type;
        partial void Onuser_selected_typeChanging(string value);
        partial void Onuser_selected_typeChanged();
        [global::System.Data.Linq.Mapping.ColumnAttribute(Storage = "_user_selected_type", DbType = "VarChar(50)", UpdateCheck = UpdateCheck.Never)]
        public string user_selected_type
        {
            get
            {
                return this._user_selected_type;
            }
            set
            {
                if ((this._user_selected_type != value))
                {
                    this.Onuser_selected_typeChanging(value);
                    this.SendPropertyChanging();
                    this._user_selected_type = value;
                    this.SendPropertyChanged("user_selected_type");
                    this.Onuser_selected_typeChanged();
                }
            }
        }


        public string SetNumber
        {
            set
            {
                var result = sanitizePhone.Replace(value, "");
                /* example: 1-555-555-5555, so valid lengths are: 7, 10, 11 */
                if (result.Length != 11 && result.Length != 10 && result.Length != 7)
                {
                    throw new ArgumentOutOfRangeException("Passed phone number may only contain 7, 10, or 11 digits.  Passed was " + value);
                }
                this.Number = result;
            }
        }
        public PHONE_TYPE AutoAssignedType { get; set; }
        public PHONE_TYPE UserAssignedType { get; set; }
    }
}
#pragma warning restore 414
