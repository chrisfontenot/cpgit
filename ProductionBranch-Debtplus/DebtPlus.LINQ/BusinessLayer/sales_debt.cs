﻿using System;
using System.Linq;

namespace DebtPlus.LINQ
{
    partial class sales_debt
    {
        /// <summary>
        /// Event raised when the payout information should be recorded for the report if desired.
        /// </summary>
        public event DebtPlus.LINQ.Events.SalesPayoutEventHandler SelfPayout;

        /// <summary>
        /// Event raised when the payout information should be recorded for the report if desired.
        /// </summary>
        public event DebtPlus.LINQ.Events.SalesPayoutEventHandler PlanPayout;

        private config cnf       = null;
        private Int32 m_Creditor = -1;

        // Length of the plan in months
        private double private_DMP_Length_Months = DebtPlus.LINQ.sales_file.CAN_NOT_BE_DETERMINED;
        private double private_SELF_Length_Months = DebtPlus.LINQ.sales_file.CAN_NOT_BE_DETERMINED;

        /// <summary>
        /// Raise the payment reference
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnSelfPayout(DebtPlus.LINQ.Events.SalesPayoutEventArgs e)
        {
            var evt = SelfPayout;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        /// <summary>
        /// Raise the payment reference
        /// </summary>
        /// <param name="e"></param>
        protected void RaiseSelfPayout(DebtPlus.LINQ.Events.SalesPayoutEventArgs e)
        {
            OnSelfPayout(e);
        }

        /// <summary>
        /// Raise the payment reference
        /// </summary>
        /// <param name="MonthNum"></param>
        /// <param name="PaymentAmt"></param>
        /// <param name="InterestAmt"></param>
        /// <param name="PrincipalAmt"></param>
        /// <param name="BalanceAmt"></param>
        protected void RaiseSelfPayout(Int32 MonthNum, decimal PaymentAmt, decimal InterestAmt, decimal PrincipalAmt, decimal BalanceAmt)
        {
            RaiseSelfPayout(new DebtPlus.LINQ.Events.SalesPayoutEventArgs(MonthNum, PaymentAmt, InterestAmt, PrincipalAmt, BalanceAmt));
        }

        /// <summary>
        /// Raise the payment reference
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnPlanPayout(DebtPlus.LINQ.Events.SalesPayoutEventArgs e)
        {
            var evt = PlanPayout;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        /// <summary>
        /// Raise the payment reference
        /// </summary>
        /// <param name="e"></param>
        protected void RaisePlanPayout(DebtPlus.LINQ.Events.SalesPayoutEventArgs e)
        {
            OnPlanPayout(e);
        }

        /// <summary>
        /// Raise the payment reference
        /// </summary>
        /// <param name="MonthNum"></param>
        /// <param name="PaymentAmt"></param>
        /// <param name="InterestAmt"></param>
        /// <param name="PrincipalAmt"></param>
        /// <param name="BalanceAmt"></param>
        protected void RaisePlanPayout(Int32 MonthNum, decimal PaymentAmt, decimal InterestAmt, decimal PrincipalAmt, decimal BalanceAmt)
        {
            RaisePlanPayout(new DebtPlus.LINQ.Events.SalesPayoutEventArgs(MonthNum, PaymentAmt, InterestAmt, PrincipalAmt, BalanceAmt));
        }

        /// <summary>
        /// Handle the initialization of the class. Load all non-defaulted items with the proper defaults.
        /// </summary>
        partial void OnCreated()
        {
            _payout_percent = 0.01F;    // The payout percent is "interest + 1%".
            cnf = null;
        }

        /// <summary>
        /// Return a pointer to agency configuration record
        /// </summary>
        private config agency
        {
            get
            {
                if (cnf == null)
                {
                    cnf = DebtPlus.LINQ.Cache.config.getList().FirstOrDefault();
                    if (cnf == null)
                    {
                        throw new ApplicationException("Unable to find the config table entry");
                    }
                }
                return cnf;
            }
        }

        /// <summary>
        /// Record number for the creditor ID when matched to the name
        /// </summary>
        public Int32 Creditor
        {
            get
            {
                return m_Creditor;
            }
            set
            {
                if (m_Creditor != value)
                {
                    m_Creditor = value;
                    if (m_Creditor >= 0)
                    {
                        var q = DebtPlus.LINQ.Cache.xpr_sales_creditor.getList().Find(c => c.Id == value);
                        if (q != null)
                        {
                            this.creditor_type = q.Type;
                            return;
                        }
                    }
                    creditor_type = null;
                }
            }
        }

        /// <summary>
        /// Payout interest amount
        /// </summary>
        public decimal SELF_PayoutInterest { get; set; }

        /// <summary>
        /// Payout principal amount
        /// </summary>
        public decimal SELF_PayoutPrincipal { get; set; }

        /// <summary>
        /// Late fee amount
        /// </summary>
        public decimal SELF_LateFees
        {
            get
            {
                return late_fees;
            }
            set
            {
                late_fees = value;
            }
        }

        /// <summary>
        /// Over limit fee amount
        /// </summary>
        public decimal SELF_OverLimitFees
        {
            get
            {
                return overlimit_fees;
            }
            set
            {
                overlimit_fees = value;
            }
        }

        /// <summary>
        /// Co-signer fee amount
        /// </summary>
        public decimal SELF_CosignerFees
        {
            get
            {
                return cosigner_fees;
            }
            set
            {
                cosigner_fees = value;
            }
        }

        /// <summary>
        /// Interest rate for the non-plan figure
        /// </summary>
        public double SELF_InterestRate
        {
            get
            {
                return interest_rate;
            }
            set
            {
                interest_rate = value;
            }
        }

        /// <summary>
        /// Payout percentage figure for the non-plan
        /// </summary>
        public double SELF_PaymentPercent
        {
            get
            {
                return payout_percent;
            }
            set
            {
                payout_percent = value;
            }
        }

        public decimal SELF_MonthlyPayment
        {
            get
            {
                decimal value;

                if (SELF_PaymentPercent == 0.01F)
                {
                    value = Decimal.Round(Convert.ToDecimal(Convert.ToDouble(balance) * (SELF_InterestRate / 12.0F)) + Convert.ToDecimal(Convert.ToDouble(balance) * 0.01F), 2);
                }
                else
                {
                    value = Decimal.Round(Convert.ToDecimal(Convert.ToDouble(balance) * SELF_PaymentPercent));
                }

                if (value < agency.ComparisonAmount)
                {
                    value = agency.ComparisonAmount;
                }

                if (value > balance)
                {
                    value = balance;
                }

                return minimum_payment = value;
            }
            set
            {
                minimum_payment = value;
            }
        }
#if false
        /// <summary>
        /// Handle a change in the creditor name.
        /// </summary>
        partial void Oncreditor_nameChanged()
        {
            // When the creditor name is changed, attempt to find it in the list of known
            // creditors. If found, change the type to the value indicated in the creditor.
            var q = DebtPlus.LINQ.Cache.xpr_sales_creditor.getList().Find(s => string.Compare(s.Name, this.creditor_name, true) == 0);
            if (q != null)
            {
                this.Creditor      = q.Id;
                this.creditor_type = q.Type;
                return;
            }
            Creditor      = -1;
            creditor_type = null;
        }
#endif
        public decimal SELF_MonthlyFinanceCharge
        {
            get
            {
                return finance_charge = Decimal.Round(Convert.ToDecimal(Convert.ToDouble(balance) * (SELF_InterestRate / 12.0F)), 2);
            }
            set
            {
                finance_charge = value;
            }
        }

        public decimal SELF_InterestAndFees
        {
            get
            {
                return total_interest_fees = SELF_MonthlyFinanceCharge + late_fees + overlimit_fees + cosigner_fees;
            }
            set
            {
                total_interest_fees = value;
            }
        }

        public decimal SELF_GoingToPrincipal
        {
            get
            {
                return principal = SELF_MonthlyPayment - SELF_MonthlyFinanceCharge;
            }
            set
            {
                principal = value;
            }
        }

        public void SELF_CalculatePayout()
        {
            // Clear the interest amount paid
            SELF_PayoutInterest = 0M;

            // If there is no balance then there can't be a need to repay. Force the value to be zero.
            if (balance <= 0M)
            {
                SELF_Length_Months = 0F;
                RaiseSelfPayout(1, 0, 0, 0, 0);
                return;
            }

            decimal PrincipalPaid = 0M;
            SELF_Length_Months = 1.0F;

            // Put a limit so that we just don't do it forever.
            while (SELF_Length_Months < 999.0F)
            {
                // Calculate the interest figure from the balance.
                decimal NewBalance = balance - PrincipalPaid;
                decimal InterestAmount = Decimal.Round(Convert.ToDecimal(Convert.ToDouble(NewBalance) * (SELF_InterestRate / 12.0F)), 2);

                // To that add the additional amount for payout
                decimal MinimumPayment;

                // If the amount is the magic 1% then do it as "interest + 1%". Otherwise, do it as a percentage of the balance.
                if (payout_percent == 0.01F)
                {
                    MinimumPayment = InterestAmount + Decimal.Round(Convert.ToDecimal(Convert.ToDouble(NewBalance) * 0.01), 2);
                }
                else // Otherwise, it is the percent of the balance.
                {
                    MinimumPayment = Decimal.Round(Convert.ToDecimal(Convert.ToDouble(NewBalance) * SELF_PaymentPercent), 2);
                }

                // If the minimum is less than $15 then make it $15 so that there is a chance that we will pay off the amount
                if (MinimumPayment < agency.ComparisonAmount)
                {
                    MinimumPayment = agency.ComparisonAmount;
                }

                // If the minimum is is now more than the balance then the debt is paid off and there is no interest on the last month.
                if (MinimumPayment >= NewBalance)
                {
                    RaiseSelfPayout(Convert.ToInt32(SELF_Length_Months), NewBalance, 0, NewBalance, 0);
                    return;
                }

                // Calculate the figures for the amount of principal and interest
                decimal PrincipalAmount = MinimumPayment - InterestAmount;
                SELF_PayoutInterest    += InterestAmount;
                PrincipalPaid          += PrincipalAmount;

                // Increment the number of months to pay off the debt
                RaiseSelfPayout(Convert.ToInt32(SELF_Length_Months), MinimumPayment, InterestAmount, PrincipalAmount, balance - PrincipalPaid);
                SELF_Length_Months += 1.0F;
            }

            // Sorry, the figures went too far. Indicate that it will never be paid off.
            SELF_Length_Months = DebtPlus.LINQ.sales_file.CAN_NOT_BE_DETERMINED;
        }

        public double SELF_Length_Months
        {
            get
            {
                if (private_SELF_Length_Months <= 0F)
                {
                    SELF_CalculatePayout();
                }
                return private_SELF_Length_Months;
            }

            set
            {
                private_SELF_Length_Months = value;
            }
        }

        /// <summary>
        /// Payment amount for the DMP plan
        /// </summary>
        public decimal DMP_Payment
        {
            get
            {
                return plan_payment;
            }
            set
            {
                plan_payment = value;
            }
        }

        /// <summary>
        /// DMP (plan) payout interest amount
        /// </summary>
        public decimal DMP_PayoutInterest { get; set; }

        /// <summary>
        /// DMP (plan) payout principal
        /// </summary>
        public decimal DMP_PayoutPrincipal { get; set; }

        /// <summary>
        /// This is a more specific name for the "plan_payment" column in the record.
        /// </summary>
        public decimal DMP_EnteredPayment
        {
            get
            {
                return plan_payment;
            }
            set
            {
                plan_payment = value;
            }
        }

        /// <summary>
        /// Find the PLAN Minimum Prorate
        /// </summary>
        public double DMP_MinimumProrate
        {
            get
            {
                // First, look at the defined creditor for the figure
                if (Creditor > 0)
                {
                    var q = DebtPlus.LINQ.Cache.xpr_sales_creditor.getList().Find(cr => cr.Id == Creditor);
                    if (q != null)
                    {
                        return plan_min_prorate = q.ReducedRate;
                    }
                }

                // Then look at the creditor type for the figure
                if (!string.IsNullOrEmpty(creditor_type))
                {
                    var q = DebtPlus.LINQ.Cache.xpr_Sales_CreditorTypes.getList().Find(t => t.Id == creditor_type);
                    if (q != null)
                    {
                        return plan_min_prorate = q.DefaultPercentPayment;
                    }
                }

                // Finally, there is no figure
                return plan_min_prorate = 0F;
            }
            set
            {
                plan_min_prorate = value;
            }
        }

        /// <summary>
        /// Calculate the PLAN Minimum Payment amount
        /// </summary>
        public decimal DMP_MinimumPayment
        {
            get
            {
                decimal CalculatedValue;
                double MinProrate = DMP_MinimumProrate;

                if (MinProrate > 0.40F)
                {
                    CalculatedValue = (Convert.ToDecimal(Convert.ToDouble(SELF_MonthlyPayment) * MinProrate) * 100M) / 100M;   // Yes, SELF_MonthlyPayment is correct for finance companies!
                }
                else
                {
                    CalculatedValue = (Convert.ToDecimal(Convert.ToDouble(balance) * MinProrate) * 100M) / 100M;

                    // If there is an agency minimum amount then we need to use that
                    if (agency.ComparisonRate > 0F && agency.ComparisonRate < 1F)
                    {
                        decimal minAgencyThreshold = System.Math.Truncate(Convert.ToDecimal(agency.ComparisonRate * Convert.ToDouble(balance)) * 100M) / 100M;
                        if (minAgencyThreshold > CalculatedValue)
                        {
                            CalculatedValue = minAgencyThreshold;
                        }
                    }
                }

                // If the creditor has a floor value then we need to use that value.
                if (Creditor > 0)
                {
                    var q = DebtPlus.LINQ.Cache.xpr_sales_creditor.getList().Find(cr => cr.Id == Creditor);
                    if (q != null)
                    {
                        decimal MinLimit = q.MinPayment;
                        if (MinLimit > 0M && MinLimit > CalculatedValue)
                        {
                            CalculatedValue = MinLimit;
                        }
                    }
                }

                // If there is a minimum dollar amount for the agency then use it
                if (agency.ComparisonAmount > 0M)
                {
                    if (agency.ComparisonAmount > CalculatedValue)
                    {
                        CalculatedValue = agency.ComparisonAmount;
                    }
                }

                return CalculatedValue;
            }
            set
            {
                this.plan_min_payment = value;
            }
        }

        /// <summary>
        /// Calculate the PLAN interest rate
        /// </summary>
        public double DMP_InterestRate
        {
            get
            {
                if (plan_interest_rate >= 0.0F && plan_interest_rate < 1.0F)
                {
                    return plan_interest_rate;
                }

                if (Creditor > 0)
                {
                    var cr = DebtPlus.LINQ.Cache.xpr_sales_creditor.getList().Find(c => c.Id == Creditor);
                    if (cr != null)
                    {
                        if (cr.ReducedRate >= 0.0F && cr.ReducedRate < 1.0F)
                        {
                            return plan_interest_rate = cr.ReducedRate;
                        }
                    }
                }

                if (SELF_InterestRate >= 0.0F && SELF_InterestRate < 1.0F)
                {
                    return plan_interest_rate = SELF_InterestRate;
                }
                return plan_interest_rate = 0.0F;
            }
            set
            {
                plan_interest_rate = value;
            }
        }

        /// <summary>
        /// Calculate the PLAN fees. This is always $0.00.
        /// </summary>
        public decimal DMP_Fees
        {
            get
            {
                return 0M;
            }
        }

        /// <summary>
        /// Calculate the PLAN Finance charge
        /// </summary>
        public decimal DMP_FinanceCharge
        {
            get
            {
                return Decimal.Truncate(Convert.ToDecimal(Convert.ToDouble(balance) * (DMP_InterestRate / 12.0F)) * 100M) / 100M;
            }
        }

        /// <summary>
        /// Calculate the PLAN Interest plus Fees amount
        /// </summary>
        public decimal DMP_InterestFees
        {
            get
            {
                plan_total_interest_fees = DMP_Fees + DMP_FinanceCharge;
                return plan_total_interest_fees;
            }
            set
            {
                plan_total_interest_fees = value;
            }
        }

        public decimal DMP_GoingToPrincipal
        {
            get
            {
                plan_principal = DMP_EnteredPayment - DMP_InterestFees;
                return plan_principal;
            }
            set
            {
                plan_principal = value;
            }
        }

        /// <summary>
        /// Calculate the payout schedule for the plan
        /// </summary>
        public void DMP_CalculatePayout()
        {
            DMP_Length_Months = DMP_ComputePayout();
        }

        /// <summary>
        /// Determine the length of the plan in months and calculate the payout schedule for it.
        /// </summary>
        public double DMP_Length_Months
        {
            get
            {
                if (private_DMP_Length_Months <= 0F)
                {
                    private_DMP_Length_Months = DMP_ComputePayout();
                }
                return private_DMP_Length_Months;
            }
            set
            {
                private_DMP_Length_Months = value;
            }
        }

        private double DMP_ComputePayout()
        {
            // Clear the interest amount paid
            DMP_PayoutInterest = 0M;

            // If there is no balance then there can't be a need to repay. Force the value to be zero.
            if (balance <= 0M)
            {
                RaisePlanPayout(0, 0, 0, 0, 0);
                return 0F;
            }

            decimal PrincipalPaid = 0M;
            Int32 months = 1;

            // Put a limit so that we just don't do it forever.
            while (months < 999)
            {
                // Calculate the interest figure from the balance.
                decimal NewBalance = balance - PrincipalPaid;
                decimal InterestAmount = Decimal.Truncate(Convert.ToDecimal(Convert.ToDouble(NewBalance) * (DMP_InterestRate / 12.0F)) * 100M) / 100M;

                // To that add the additional amount for payout
                decimal MinimumPayment = Decimal.Truncate(DMP_EnteredPayment * 100M) / 100M;

                // If the minimum is less than $15 then make it $15 so that there is a chance that we will pay off the amount
                if (MinimumPayment < agency.ComparisonAmount)
                {
                    MinimumPayment = agency.ComparisonAmount;
                }

                // If the minimum is is now more than the balance then the debt is paid off and there is no interest on the last month.
                if (MinimumPayment >= NewBalance)
                {
                    RaisePlanPayout(months, NewBalance, 0, NewBalance, 0);
                    return Convert.ToDouble(months);
                }

                // Calculate the figures for the amount of principal and interest
                decimal PrincipalAmount = MinimumPayment - InterestAmount;
                DMP_PayoutInterest += InterestAmount;
                PrincipalPaid += PrincipalAmount;

                // Increment the number of months to pay off the debt
                RaisePlanPayout(months, InterestAmount + PrincipalAmount, InterestAmount, PrincipalAmount, balance - PrincipalPaid);
                months += 1;
            }

            // Sorry, the figures went too far. Indicate that it will never be paid off.
            return DebtPlus.LINQ.sales_file.CAN_NOT_BE_DETERMINED;
        }
    }
}