﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class client_www
    {
        partial void OnCreated()
        {
            // Define the empty values for the new class object. These may be replaced by the database
            // read routine, or they may not.
            _Id = System.Guid.NewGuid();
            _ApplicationName = "/";
            _FailedPasswordAnswerAttemptCount = 0;
            _FailedPasswordAttemptCount = 0;
            _IsLockedOut = false;
            _PasswordAnswer = string.Empty;
            _Password = string.Empty;
            _UserName = string.Empty;
            _PasswordQuestion = string.Empty;
            _Email = string.Empty;

            _FailedPasswordAnswerAttemptWindowStart = System.DateTime.UtcNow;
            _FailedPasswordAttemptWindowStart = this._FailedPasswordAnswerAttemptWindowStart;
            _LastActivityDate = this._FailedPasswordAttemptWindowStart;
            _LastLockoutDate = this._FailedPasswordAttemptWindowStart;
            _LastLoginDate = this._FailedPasswordAttemptWindowStart;
            _LastPasswordChangeDate = this._FailedPasswordAttemptWindowStart;
        }
    }
}
