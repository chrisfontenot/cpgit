﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class registers_invoice
    {
        /// <summary>
        /// Balance on the invoice
        /// </summary>
        public decimal balance
        {
            get
            {
                if (inv_amount > pmt_amount + adj_amount)
                {
                    return Decimal.Truncate((inv_amount - pmt_amount - adj_amount) * 100M) / 100M;
                }
                return 0M;
            }
        }

        /// <summary>
        /// Number of months that the invoice is outstanding
        /// </summary>
        public int months
        {
            get
            {
                // If the current balance is zero then the invoice is "current"
                if (balance <= 0m)
                {
                    return 0;
                }

                // Find the number of months
                DateTime current = DateTime.Now;
                int answer = ((current.Year - inv_date.Year) * 12) + (current.Month - inv_date.Month) - 1;
                if (answer < 0)
                {
                    answer = 0;
                }
                return answer;
            }
        }
    }
}
