﻿namespace DebtPlus.LINQ
{
    using System;

    /// <summary>
    /// Class of items in the addresses table
    /// </summary>
    partial class address : System.IComparable<address>, DebtPlus.Interfaces.Addresses.IAddress
    {
        /// <summary>
        /// Default value for the address record modifier field.
        /// </summary>
        public const string Defaultmodifier = "APT";

        public bool showattn { get; private set; }         // Is the attention line displayed?
        public bool showCreditor1 { get; private set; }    // Is the creditor line 1 valid?
        public bool showCreditor2 { get; private set; }    // Is the creditor line 2 valid?
        public bool showLine3 { get; private set; }        // Is the third line of the address valid?

        /// <summary>
        /// Attention line
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public string attn { get; set; }        // not used in the general case and not part of the database row

        /// <summary>
        /// Create the instance of the data class
        /// </summary>
        /// <param name="ShowAttn">Should the creditor attn line be shown?</param>
        /// <param name="ShowCreditor1">Should the creditor line 1 be shown?</param>
        /// <param name="ShowCreditor2">Should the creditor line 2 be shown?</param>
        /// <param name="ShowLine3">Should the address line 3 be shown?</param>
        public address(bool ShowAttn, bool ShowCreditor1, bool ShowCreditor2, bool ShowLine3)
            : this()
        {
            showattn = ShowAttn;
            showCreditor1 = ShowCreditor1;
            showCreditor2 = ShowCreditor2;
            showLine3 = ShowLine3;
        }

        /// <summary>
        /// Is the current record to be newly created?
        /// </summary>
        /// <returns></returns>
        public bool IsNew()
        {
            return Id <= 0;     // New records don't have a key.
        }

        /// <summary>
        /// Is the current record empty? Does it have any data to be saved?
        /// </summary>
        public bool IsEmpty()
        {
            // Look at the standardly empty strings
            foreach (string str in new string[] { creditor_prefix_1, creditor_prefix_2, house, direction, street, suffix, modifier_value, address_line_2, address_line_3, city, PostalCode })
            {
                if (!string.IsNullOrWhiteSpace(str))
                {
                    return false;
                }
            }

            // Look at the state identifier
            if (state > 0)
            {
                if (state != DebtPlus.LINQ.Cache.state.getDefault())
                {
                    return false;
                }
            }

            // Look for the modifier
            if (string.IsNullOrWhiteSpace(modifier) || string.Compare(modifier, Defaultmodifier, true, System.Globalization.CultureInfo.InvariantCulture) != 0)
            {
                return false;
            }

            // The record has no changes.
            return true;
        }

        /// <summary>
        /// Compare the class contents to determine relative order
        /// </summary>
        /// <param name="other">Pointer to the other object to be used in the comparison</param>
        /// <returns></returns>
        public int CompareTo(address other)
        {
            // If there is no "other" then we are greater (can't be equal since we exist or we wouldn't be here)
            if (other == null)
            {
                return 1;
            }

            // Compare the fields to determine the order. We need to be careful about missing fields.
            int Relation = Helpers.Comparison(creditor_prefix_1, other.creditor_prefix_1);
            if (Relation == 0)
            {
                Relation = Helpers.Comparison(creditor_prefix_2, other.creditor_prefix_2);
                if (Relation == 0)
                {
                    Relation = Helpers.Comparison(house, other.house);
                    if (Relation == 0)
                    {
                        Relation = Helpers.Comparison(direction, other.direction);
                        if (Relation == 0)
                        {
                            Relation = Helpers.Comparison(street, other.street);
                            if (Relation == 0)
                            {
                                Relation = Helpers.Comparison(suffix, other.suffix);
                                if (Relation == 0)
                                {
                                    Relation = Helpers.Comparison(modifier, other.modifier);
                                    if (Relation == 0)
                                    {
                                        Relation = Helpers.Comparison(modifier_value, other.modifier_value);
                                        if (Relation == 0)
                                        {
                                            Relation = Helpers.Comparison(address_line_2, other.address_line_2);
                                            if (Relation == 0)
                                            {
                                                Relation = Helpers.Comparison(address_line_3, other.address_line_3);
                                                if (Relation == 0)
                                                {
                                                    Relation = Helpers.Comparison(city, other.city);
                                                    if (Relation == 0)
                                                    {
                                                        Relation = Helpers.Comparison(state, other.state);
                                                        if (Relation == 0)
                                                        {
                                                            Relation = Helpers.Comparison(PostalCode, other.PostalCode);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return Relation;
        }

        /// <summary>
        /// Convert the first line address to a string
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public string AddressLine1
        {
            get
            {
                return getAddressLine1(house, direction, street, suffix, modifier, modifier_value);
            }
        }

        /// <summary>
        /// Retrieve the first line of the address given the component pieces
        /// </summary>
        /// <param name="house"></param>
        /// <param name="direction"></param>
        /// <param name="street"></param>
        /// <param name="suffix"></param>
        /// <param name="modifier"></param>
        /// <param name="modifier_value"></param>
        /// <returns></returns>
        public static string getAddressLine1(string house, string direction, string street, string suffix, string modifier, string modifier_value)
        {
            var sb = new System.Text.StringBuilder();

            // Generate the initial line with a single space between the fields
            foreach (string str in new string[] { house, direction, street, suffix })
            {
                if (!string.IsNullOrWhiteSpace(str))
                {
                    sb.AppendFormat(" {0}", str.Trim());
                }
            }

            // Determine if the modifier and value is defined. Ignore "APT" without a modifier.
            if (!string.IsNullOrWhiteSpace(modifier))
            {
                // Look up the modifier in the list to determine if a value is required. If a value is required but it is not there
                // then there is no modifier either, even if one is given.
                var q = DebtPlus.LINQ.Cache.postal_abbreviation.getList().Find(s => s.abbreviation == modifier && s.type == 3);
                if (q != null)
                {
                    // We want the field if there is a modifier value or a modifier value is not required
                    if (!string.IsNullOrWhiteSpace(modifier_value) || !q.require_modifier)
                    {
                        sb.AppendFormat(" {0}", modifier);

                        // Include the modifier value if there is one. It is optional at this point.
                        if (!string.IsNullOrWhiteSpace(modifier_value))
                        {
                            sb.AppendFormat(" {0}", modifier_value);
                        }
                    }
                }
            }

            // The answer is the combined result.
            return sb.ToString().Trim(new char[] { ' ', ',' });
        }

        /// <summary>
        /// Convert the city, state, and zip-code to an address line
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public string CityStateZip
        {
            get
            {
                string formatting = null;
                Int32? currentCountry = new Int32?();
                string countryName = string.Empty;

                // Find the state definition for the address
                DebtPlus.LINQ.state st = DebtPlus.LINQ.Cache.state.getList().Find(s => s.Id == state);
                if (st != null)
                {
                    formatting = st.AddressFormat;
                    currentCountry = st.Country;
                }

                // Find the country name
                if (currentCountry.HasValue && currentCountry.Value > 0)
                {
                    DebtPlus.LINQ.country c = DebtPlus.LINQ.Cache.country.getList().Find(s => s.Id == currentCountry);
                    if (c != null)
                    {
                        countryName = c.description;
                    }
                }

                // Format the address line using the values as given. We need to translate \r and \n to the proper escape codes.
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.AppendFormat(formatting, new object[] {city, st.MailingCode, PostalCode, countryName});
                sb.Replace(@"\r", "\r");
                sb.Replace(@"\n", "\n");
                return sb.ToString().Trim();
            }
        }

        /// <summary>
        /// Convert the current object to a string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return ConvertToString(showattn, showCreditor1, showCreditor2, showLine3);
        }

        /// <summary>
        /// Return the string format for the address record.
        /// </summary>
        /// <param name="showAttn">Should the attention line be included?</param>
        /// <param name="showCreditor1">Should the creditor1 line be included?</param>
        /// <param name="showCreditor2">Should the creditor2 line be included?</param>
        /// <param name="showLine3">Should the addressline3 line be included?</param>
        /// <returns></returns>
        internal string ConvertToString(bool showAttn, bool showCreditor1, bool showCreditor2, bool showLine3)
        {
            var sb = new System.Text.StringBuilder();

            // Include the attention line
            if (showattn && !string.IsNullOrWhiteSpace(attn))
            {
                sb.Append(Environment.NewLine);
                sb.AppendFormat("ATTN: {0}", attn.Trim());
            }

            // Include the creditor prefix line 1
            if (showCreditor1 && !string.IsNullOrWhiteSpace(creditor_prefix_1))
            {
                sb.Append(Environment.NewLine);
                sb.Append(creditor_prefix_1.Trim());
            }

            // Include the creditor prefix line 2
            if (showCreditor2 && !string.IsNullOrWhiteSpace(creditor_prefix_2))
            {
                sb.Append(Environment.NewLine);
                sb.Append(creditor_prefix_2.Trim());
            }

            // Add the standard line1 and 2 of the address block
            var str = AddressLine1;
            if (!string.IsNullOrWhiteSpace(str))
            {
                sb.Append(Environment.NewLine);
                sb.Append(str);
            }

            if (!string.IsNullOrWhiteSpace(address_line_2))
            {
                sb.Append(Environment.NewLine);
                sb.Append(address_line_2.Trim());
            }

            // Include the address line 3 if needed
            if (showLine3 && !string.IsNullOrWhiteSpace(address_line_3))
            {
                sb.Append(Environment.NewLine);
                sb.Append(address_line_3.Trim());
            }

            // Follow it with the city / state / zip-code
            str = CityStateZip;
            if (!string.IsNullOrWhiteSpace(str))
            {
                sb.Append(Environment.NewLine);
                sb.Append(str);
            }

            return sb.ToString().Trim();
        }

        private US_STATE? _us_state = null;
        /// <summary>
        /// State of the address as in 'CA.'  Enumeration that is the same as the base.State int.
        /// </summary>
        public US_STATE? StateProvince
        {
            get
            {
                var toReturn = _us_state;
                if (!toReturn.HasValue)
                {
                    US_STATE parsed;
                    if (Enum.TryParse<US_STATE>(this.state.ToString(), out parsed))
                    {
                        toReturn = parsed;
                    }
                }

                return toReturn;
            }
            set
            {
                _us_state = value;
                if (value.HasValue)
                {
                    this.state = (Int32)value.Value;
                }
            }
        }
        public string TzDescriptor { get; set; }
        public int GMTOffset { get; set; }
        public TIMEZONE TimeZone
        {
            get
            {
                if (String.IsNullOrWhiteSpace(TzDescriptor))
                {
                    return TIMEZONE.Eastern;
                }
                return (TIMEZONE)Enum.Parse(typeof(TIMEZONE), TzDescriptor, true);
            }
        }         
    }
}
