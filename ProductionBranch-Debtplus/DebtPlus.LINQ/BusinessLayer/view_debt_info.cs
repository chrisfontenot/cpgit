﻿#pragma warning disable 1591

namespace DebtPlus.LINQ
{
    using System.Linq;

    partial class view_debt_info : IDebtInfoSourceCollection
    {
        /// <summary>
        /// Current debt balance
        /// </summary>
        public decimal current_balance
        {
            get
            {
                return orig_balance.GetValueOrDefault(0m) + orig_balance_adjustment.GetValueOrDefault(0m) + total_interest.GetValueOrDefault(0m) - total_payments.GetValueOrDefault(0m);
            }
        }

        /// <summary>
        /// Type of the creditor
        /// </summary>
        public string creditor_type
        {
            get
            {
                return "N";
            }
        }

        /// <summary>
        /// Dollar amount for the disbursement
        /// </summary>
        public decimal debit_amt
        {
            get
            {
                return 0m;
            }
        }

        /// <summary>
        /// Current disbursement record pointer
        /// </summary>
        public System.Nullable<System.Int32> oID
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// List of the fields that have been changed
        /// </summary>
        private System.Collections.Generic.Dictionary<string, ChangedFieldItem> colChangedFieldItems = new System.Collections.Generic.Dictionary<string, ChangedFieldItem>();

        /// <summary>
        /// Retrieve the list of changed field items
        /// </summary>
        public System.Collections.Generic.List<ChangedFieldItem> getChangedFieldItems
        {
            get
            {
                return colChangedFieldItems.Values.ToList();
            }
        }

        /// <summary>
        /// Reset the list of changed field items
        /// </summary>
        public void ClearChangedFieldItems()
        {
            colChangedFieldItems.Clear();
        }

        /// <summary>
        /// Handle the change in a data field for the client record
        /// </summary>
        /// <param name="fieldname">Name of the field that is being changed</param>
        /// <param name="oldvalue">The current (old) value before the change</param>
        /// <param name="newvalue">The new value after the change</param>
        partial void field_changing(string fieldname, object oldvalue, object newvalue)
        {
            if (!colChangedFieldItems.ContainsKey(fieldname))
            {
                lock (colChangedFieldItems)
                {
                    if (!colChangedFieldItems.ContainsKey(fieldname))
                    {
                        ChangedFieldItem i = new ChangedFieldItem(fieldname) { OldValue = oldvalue, NewValue = newvalue };
                        colChangedFieldItems.Add(fieldname, i);
                    }
                }
            }
        }
    }
}
