﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class menus
    {
        /// <summary>
        /// Define the initial values for a new structure
        /// </summary>
        partial void OnCreated()
        {
            this._attributes = 0;
            this._argument = string.Empty;
            this._menu_level = 9;
            this._text = string.Empty;
            this._type = string.Empty;
        }

        /// <summary>
        /// Validate the value for the argument field. It is allowed to be either 0 or 1.
        /// </summary>
        /// <param name="value"></param>
        partial void OnattributesChanging(int? value)
        {
            if (value != null && (value.Value < 0 || value.Value > 1))
            {
                throw new ArgumentOutOfRangeException("attributes");
            }
        }
    }
}
