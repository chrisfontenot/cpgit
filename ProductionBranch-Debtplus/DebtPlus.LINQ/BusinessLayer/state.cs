﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class state
    {
        partial void OnCreated()
        {
            _ActiveFlag = true;
            _Default = false;
            _DoingBusiness = true;
            _MailingCode = string.Empty;
            _Name = string.Empty;
            _USAFormat = 1;
            _TimeZoneID = 1;
            _NegativeDebtWarning = false;
        }

        /// <summary>
        /// Name of the country from the country record.
        /// </summary>
        public string CountryName
        {
            get
            {
                if (country1 != null)
                {
                    return country1.description;
                }
                return string.Empty;
            }
        }
    }
}
