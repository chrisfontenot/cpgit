﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    /// <summary>
    /// Class to be used for creditor addresses. The defaults are set for the creditor
    /// configuration as opposed to the client configurations.
    /// </summary>
    public class CreditorAddressEntry : address
    {
        /// <summary>
        /// Initialize the structure
        /// </summary>
        public CreditorAddressEntry() : base(true, true, true, true)
        {
        }

        /// <summary>
        /// Initialize the structure
        /// </summary>
        /// <param name="adr">Pointer to the current address entry</param>
        public CreditorAddressEntry(address adr) : this()
        {
            this.address_line_2    = adr.address_line_2;
            this.address_line_3    = adr.address_line_3;
            this.attn              = adr.attn;
            this.city              = adr.city;
            this.creditor_prefix_1 = adr.creditor_prefix_1;
            this.creditor_prefix_2 = adr.creditor_prefix_2;
            this.direction         = adr.direction;
            this.house             = adr.house;
            this.modifier          = adr.modifier;
            this.modifier_value    = adr.modifier_value;
            this.PostalCode        = adr.PostalCode;
            this.state             = adr.state;
            this.street            = adr.street;
            this.suffix            = adr.suffix;
            this.USDA_Status       = adr.USDA_Status;
        }

        /// <summary>
        /// Initialize the structure
        /// </summary>
        /// <param name="adr">Pointer to the current address entry</param>
        /// <param name="attn">String for the attention line. It comes from the creditor_addresses table.</param>
        public CreditorAddressEntry(address adr, string attn) : this(adr)
        {
            this.attn = attn;
        }
    }
}
