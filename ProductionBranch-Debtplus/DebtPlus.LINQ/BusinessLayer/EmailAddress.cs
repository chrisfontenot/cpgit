﻿namespace DebtPlus.LINQ
{
    using System;

    /// <summary>
    /// Class of items in the EmailAddresses table
    /// </summary>
    partial class EmailAddress : System.IComparable<EmailAddress>, DebtPlus.Interfaces.Email.IEmailAddress
    {
        // Strings for the various special modes
        public const string cREFUSED = "DECLINED";
        public const string cNONE = "NONE";
        public const string cMISSING = "NOT SPECIFIED";

        /// <summary>
        /// List of items that are stored in the database "validation" field for the various settings.
        /// Internally, "valid" and "invalid" are the same thing. It is only when the email is validated
        /// and proven to be invalid that we change the type to invalid.
        /// </summary>
        /// <remarks></remarks>
        public enum EmailValidationEnum
        {
            Missing = 0,        // The address is not specified. No assumption for the address should be made.
            Valid = 1,          // The address is specified and is a valid Email address.
            Invalid = 2,        // THe address is specified but is not a valid Email address. It is not to be used.
            Refused = 3,        // The Email address was requested by was refused by the client. Don't ask for it again.
            None = 4            // The Email address was requested but the answer was "we don't have one yet". Ask later.
        }

        /// <summary>
        /// Validation code from the enum list
        /// </summary>
        public EmailValidationEnum ValidationCode
        {
            get
            {
                return (EmailValidationEnum)Validation;
            }
            set
            {
                Validation = (Int32)value;
            }
        }

        /// <summary>
        /// Is the current record to be newly created?
        /// </summary>
        /// <returns></returns>
        public bool IsNew()
        {
            return Id <= 0;     // New records don't have a key.
        }

        /// <summary>
        /// Is the current record empty? Does it have any data to be saved?
        /// </summary>
        public bool IsEmpty()
        {
            switch (ValidationCode)
            {
                case EmailValidationEnum.Missing:
                    return true;

                case EmailValidationEnum.Valid:
                case EmailValidationEnum.Invalid:
                    return string.IsNullOrWhiteSpace(Address);

                default:
                    return false;
            }
        }

        /// <summary>
        /// Compare the class contents to determine relative order
        /// </summary>
        /// <param name="other">Pointer to the other object to be used in the comparison</param>
        /// <returns></returns>
        public int CompareTo(EmailAddress other)
        {
            // If there is no "other" then we are greater (can't be equal since we exist or we wouldn't be here)
            if (other == null)
            {
                return 1;
            }

            // Both "invalid" and "valid" are the same for our comparison at this time since the actual status has not been determined.
            // It won't be determined until it is run though the EmailValidation procedure.
            Int32 a_Validation = Convert.ToInt32(ValidationCode == EmailValidationEnum.Invalid || ValidationCode == EmailValidationEnum.Valid ? EmailValidationEnum.Valid : ValidationCode);
            Int32 b_Validation = Convert.ToInt32(other.ValidationCode == EmailValidationEnum.Invalid || other.ValidationCode == EmailValidationEnum.Valid ? EmailValidationEnum.Valid : other.ValidationCode);

            Int32 Relation = a_Validation.CompareTo(b_Validation);
            if (Relation == 0)
            {
                if (a_Validation == (Int32)EmailValidationEnum.Valid)
                {
                    Relation = Helpers.Comparison(Address, other.Address);
                }
            }
            return Relation;
        }

        /// <summary>
        /// Convert the object to a string.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            switch (ValidationCode)
            {
                case EmailValidationEnum.Missing:
                    return cMISSING;

                case EmailValidationEnum.Refused:
                    return cREFUSED;

                case EmailValidationEnum.None:
                    return cNONE;

                default:
                    return Address;
            }
        }
    }
}
