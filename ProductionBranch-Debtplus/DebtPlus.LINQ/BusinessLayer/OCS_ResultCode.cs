﻿// -----------------------------------------------------------------------
// <copyright file="ocsResults.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace DebtPlus.LINQ
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    partial class OCS_ResultCode
    {
        public string display_description
        {
            get
            {
                return string.Format("{0} - {1}", this.ShortCode ?? string.Empty, this.Description);
            }
        }
    }
}
