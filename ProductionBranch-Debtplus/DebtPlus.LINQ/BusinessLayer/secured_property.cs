﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class secured_property
    {
        public void Detach()
        {
            // These must be null for Attach() to work.
            this.PropertyChanged = null;
            this.PropertyChanging = null;

            // Similarly set child objects to default as well
            this._secured_loans = new System.Data.Linq.EntitySet<secured_loan>(new Action<secured_loan>(this.attach_secured_loans), new Action<secured_loan>(this.detach_secured_loans));
        }

        /// <summary>
        /// Define the contents of the class when it is first created.
        /// </summary>
        partial void OnCreated()
        {
            this._sub_description = string.Empty;
            this._description = string.Empty;
            this._current_value = 0M;
            this._original_price = 0M;
            this._primary_residence = false;
            this._school_name = string.Empty;
            this._year_acquired = 0;
            this._year_mfg = 0;
        }

        /// <summary>
        /// Return the total balance on the loans associated with the property.
        /// </summary>
        public decimal balance
        {
            get
            {
                return secured_loans.Sum(s => s.balance);
            }
        }
    }
}
