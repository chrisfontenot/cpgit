﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class AttributeType
    {
        /// <summary>
        /// Return the culture information structure from the specific language ID
        /// </summary>
        /// <param name="LanguageID">ID of the AttributeTypes table for the language</param>
        /// <returns></returns>
        public static System.Globalization.CultureInfo SpecificCulture(System.Nullable<System.Int32> LanguageID)
        {
            string cultureID = getLanguageString(LanguageID);
            if (!string.IsNullOrWhiteSpace(cultureID))
            {
                return System.Globalization.CultureInfo.CreateSpecificCulture(cultureID);
            }

            // Use the current culture for the default
            return System.Globalization.CultureInfo.CurrentCulture;
        }

        /// <summary>
        /// Return the specific language Culture information string from the language ID
        /// </summary>
        /// <param name="LanguageID">The desired language ID</param>
        /// <returns></returns>
        public static string getLanguageString(System.Nullable<System.Int32> LanguageID)
        {
            // Translate the specified language key to the culture string if a key was given.
            if (LanguageID.HasValue)
            {
                var qLocal = DebtPlus.LINQ.Cache.AttributeType.getLanguageList().Find(s => s.Id == LanguageID.Value);
                if (qLocal != null && !string.IsNullOrWhiteSpace(qLocal.LanguageID))
                {
                    return qLocal.LanguageID;
                }
            }

            // Find the default language
            var qGlobal = DebtPlus.LINQ.Cache.AttributeType.getLanguageList().Find(s => s.Default);
            if (qGlobal != null && !string.IsNullOrWhiteSpace(qGlobal.LanguageID))
            {
                return qGlobal.LanguageID;
            }

            // This is just a "canned" known value. We use US English by default.
            return "en_US";
        }

        /// <summary>
        /// Return a string suitable for a single list.
        /// </summary>
        public string description
        {
            get
            {
                if (string.IsNullOrEmpty(Grouping))
                {
                    return Attribute;
                }
                if (string.IsNullOrEmpty(Attribute))
                {
                    return Grouping;
                }
                return Grouping.Trim() + ":" + Attribute.Trim();
            }
        }
    }
}
