﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class client_ach
    {
        /// <summary>
        /// List of the fields that have been changed
        /// </summary>
        private System.Collections.Generic.Dictionary<string, ChangedFieldItem> colChangedFieldItems = new System.Collections.Generic.Dictionary<string, ChangedFieldItem>();

        /// <summary>
        /// Retrieve the list of changed field items
        /// </summary>
        public System.Collections.Generic.List<ChangedFieldItem> getChangedFieldItems
        {
            get
            {
                return colChangedFieldItems.Values.ToList();
            }
        }

        /// <summary>
        /// Reset the list of changed field items
        /// </summary>
        public void ClearChangedFieldItems()
        {
            colChangedFieldItems.Clear();
        }

        /// <summary>
        /// Handle the change in a data field for the client record
        /// </summary>
        /// <param name="fieldname">Name of the field that is being changed</param>
        /// <param name="oldvalue">The current (old) value before the change</param>
        /// <param name="newvalue">The new value after the change</param>
        void field_changing(string fieldname, object oldvalue, object newvalue)
        {
            var q = colChangedFieldItems.Where(s => s.Key == fieldname).Select(s => s.Value).FirstOrDefault();
            if (q == null)
            {
                lock (colChangedFieldItems)
                {
                    q = colChangedFieldItems.Where(s => s.Key == fieldname).Select(s => s.Value).FirstOrDefault();
                    if (q == null)
                    {
                        q = new ChangedFieldItem()
                        {
                            FieldName = fieldname,
                            OldValue = oldvalue,
                            NewValue = newvalue
                        };
                        colChangedFieldItems.Add(fieldname, q);
                        return;
                    }
                }
            }
            q.NewValue = newvalue;
        }

        partial void OnCheckingSavingsChanging(char value)
        {
            field_changing("CheckingSavings", _CheckingSavings, value);
        }

        partial void OnABAChanging(string value)
        {
            field_changing("ABA", _ABA, value);
        }

        partial void OnAccountNumberChanging(string value)
        {
            field_changing("AccountNumber", _AccountNumber, value);
        }

        partial void OnStartDateChanging(DateTime value)
        {
            field_changing("StartDate", _StartDate, value);
        }

        partial void OnEndDateChanging(DateTime? value)
        {
            field_changing("EndDate", _EndDate, value);
        }

        partial void OnErrorDateChanging(DateTime? value)
        {
            field_changing("ErrorDate", _ErrorDate, value);
        }

        partial void OnPrenoteDateChanging(DateTime? value)
        {
            field_changing("PrenoteDate", _PrenoteDate, value);
        }

        partial void OnContractDateChanging(DateTime? value)
        {
            field_changing("ContractDate", _ContractDate, value);
        }

        partial void OnEnrollDateChanging(DateTime value)
        {
            field_changing("EnrollDate", _EnrollDate, value);
        }

        partial void OnisActiveChanging(bool value)
        {
            field_changing("isActive", _isActive, value);
        }
    }
}
