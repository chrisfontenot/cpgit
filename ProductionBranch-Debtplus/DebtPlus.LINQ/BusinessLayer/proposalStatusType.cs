﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class ProposalStatusType
    {
        public static readonly Int32 Type_Created = 0;
        public static readonly Int32 Type_Pending = 1;
        public static readonly Int32 Type_AcceptedByDefault = 2;
        public static readonly Int32 Type_Accepted = 3;
        public static readonly Int32 Type_Rejected = 4;
    }
}
