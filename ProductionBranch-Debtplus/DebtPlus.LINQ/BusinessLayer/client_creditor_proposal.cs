﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class client_creditor_proposal
    {
        /// <summary>
        /// Handle the creation of the new class object
        /// </summary>
        partial void OnCreated()
        {
            _created_by = string.Empty;
            _proposal_status = 1;
            _date_created = DateTime.Now;
            _proposed_date = DateTime.Now.Date;
        }

        public client_creditor_proposal(DateTime date_created, string created_by) : this()
        {
            _date_created = date_created;
            _created_by = created_by;
        }

        /// <summary>
        /// Used in display only.
        /// </summary>
        public DateTime? letter_date
        {
            get
            {
                // If the proposal is in a batch then look at the batch
                if (proposal_batch_id1 != null)
                {

                    // Use the date transmitted if there is one.
                    if (proposal_batch_id1.date_transmitted.HasValue)
                    {
                        return proposal_batch_id1.date_transmitted;
                    }

                    // Use the date closed if there is one
                    if (proposal_batch_id1.date_closed.HasValue)
                    {
                        return proposal_batch_id1.date_closed;
                    }
                }

                // Otherwise the date is empty.
                return new DateTime?();
            }
        }

        /// <summary>
        /// Used in display only.
        /// </summary>
        public DateTime? display_proposal_status_date
        {
            get
            {
                // If there is a status date then return it
                if (proposal_status_date.HasValue)
                {
                    return proposal_status_date;
                }

                // If there is no created date then return an empty date
                if (date_created <= minimum_date)
                {
                    return new DateTime?();
                }

                // Otherwise, return the date the proposal was created
                return new DateTime? (date_created);
            }
        }

        private static DateTime minimum_date = new DateTime(1900, 1, 1);
    }
}
