﻿// -----------------------------------------------------------------------
// <copyright file="client_DisclosureTypes.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace DebtPlus.LINQ
{
    partial class client_DisclosureType
    {
        public static readonly string description_HPF_CBR = "HPF CBR-Credit Bureau Release";
        public static readonly string description_HPF_SHARE = "HPF Auth. to Share Data (Housing)";
        public static readonly string description_HPF_PRIVACY = "HPF Privacy Policy";
        public static readonly string description_MHA = "MHA-Making Home Affordable (Housing)";
    }
}