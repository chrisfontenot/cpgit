﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.LINQ;

namespace DebtPlus.LINQ
{
    partial class resource_type : System.IComparable<resource_type>, System.IComparable
    {
        /// <summary>
        /// Override the toString to return the description field.
        /// </summary>
        public override string ToString()
        {
            return this.description ?? string.Empty;
        }

        /// <summary>
        /// Implementation of the comparison function to allow items to be sorted
        /// </summary>
        public int CompareTo(resource_type other)
        {
            if (other == null)
            {
                return 1;
            }
            return this.ToString().CompareTo(other.ToString());
        }

        /// <summary>
        /// Implementation of the comparison function to allow items to be sorted
        /// </summary>
        public int CompareTo(object obj)
        {
            return CompareTo(obj as resource_type); 
        }
    }
}