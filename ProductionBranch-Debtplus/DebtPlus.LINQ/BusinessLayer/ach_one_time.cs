﻿using System;
using System.Linq;

namespace DebtPlus.LINQ
{
    partial class ach_onetime : IACH_Generate_File
    {
        partial void OnCreated()
        {
            _CheckingSavings = 'C';
            _ABA             = "000000000";
            _AccountNumber   = string.Empty;
            _Amount          = 0M;
            _client          = 0;
            _date_created    =
            _EffectiveDate   = DateTime.Now;
            _created_by      = string.Empty;
            _client_product  = null;
        }

        private string _transaction_code = null;
        string IACH_Generate_File.transaction_code
        {
            get
            {
                if (string.IsNullOrEmpty(_transaction_code))
                {
                    return CheckingSavings == 'S' ? "37" : "27";
                }
                return _transaction_code;
            }
            set
            {
                _transaction_code = value;
            }
        }

        string IACH_Generate_File.routing_number
        {
            get
            {
                return ABA;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        string IACH_Generate_File.account_number
        {
            get
            {
                return AccountNumber;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        decimal IACH_Generate_File.amount
        {
            get
            {
                return Amount;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        int IACH_Generate_File.client
        {
            get
            {
                return this.client;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        string IACH_Generate_File.client_name
        {
            get
            {
                using (var bc = new BusinessContext())
                {
                    // Return an empty string (not null) for the invalid client.
                    return (from n in bc.view_client_addresses where n.client == this.client select n.name).FirstOrDefault<string>().ToString().ToUpper();
                }
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
}
