﻿namespace DebtPlus.LINQ
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;

    partial class address
    {
        /// <summary>
        /// Try to parse the first line of the address block
        /// </summary>
        /// <param name="input">Pointer to the input string for the first address line</param>
        public void ParseCityStateZip(string input)
        {
            // First, clear the address fields
            city = string.Empty;
            state = 0;
            PostalCode = string.Empty;

            // Find the default country for the address records. This should be the USA.
            Int32 defaultCountry = DebtPlus.LINQ.Cache.country.getDefault().GetValueOrDefault(1);

            // First, attempt to remove the postalcode from the input
            var rxPostal = new Regex(@"^\s*(.*)(\d{5}(-\d{4})?)\s*$");
            var mPostal = rxPostal.Match(input);
            if (mPostal.Success)
            {
                PostalCode = mPostal.Groups[2].Captures[0].Value;
                input = mPostal.Groups[1].Captures[0].Value;
            }

            // Next, remove the state name from the city
            var rxState = new Regex(@"^\s*(.*)\s+(\S+)\.?\s*$");
            var mState = rxState.Match(input);
            if (mState.Success)
            {
                var stateName = mState.Groups[2].Captures[0].Value;
                var q = DebtPlus.LINQ.Cache.state.getList().Find(s => (s.Country == defaultCountry) && (string.Compare(s.MailingCode.PadRight(2).Substring(0, 2), stateName, true) == 0 || string.Compare(s.Name, stateName, true) == 0));
                if (q != null)
                {
                    input = mState.Groups[1].Captures[0].Value;
                    state = q.Id;
                }
            }

            // What is left is the city name. Remove any punctuation and stop.
            city = input.Trim(new char[] { ' ', ',', '.' });
        }

        /// <summary>
        /// Try to parse the first line of the address block
        /// </summary>
        /// <param name="input">Pointer to the input string for the first address line</param>
        public void ParseAddress1(string input)
        {
            // Clear the storage fields
            house = string.Empty;
            direction = string.Empty;
            street = string.Empty;
            suffix = string.Empty;
            modifier = "APT";
            modifier_value = string.Empty;

            // Split the house number
            var rxHouse = new Regex(@"^\s*([0-9]+[a-z]*)\s+(.*)\s*$", RegexOptions.Singleline | RegexOptions.IgnoreCase);
            var mHouse = rxHouse.Match(input);
            if (mHouse.Success)
            {
                house = mHouse.Groups[1].Captures[0].Value;
                input = mHouse.Groups[2].Captures[0].Value;
            }

            do
            {
                // Look for the magic string of "#" to mean "APT"
                var rxApt = new Regex(@"^\s*(.*)\s+\#\s*(\S+)\s*$");
                var mApt = rxApt.Match(input);
                if (mApt.Success)
                {
                    input = mApt.Groups[1].Captures[0].Value;
                    modifier = "APT";
                    modifier_value = mApt.Groups[2].Captures[0].Value;
                    break;
                }

                // Find items that do not have a number. These may be like "REAR" or "BASEMENT"
                rxApt = new Regex(@"^\s*(.+)\s+(\S+)\s*$", RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.RightToLeft);
                mApt = rxApt.Match(input, input.Length);
                if (mApt.Success)
                {
                    var remainder = mApt.Groups[1].Captures[0].Value;
                    var type = mApt.Groups[2].Captures[0].Value;

                    // Determine if there is a match to the type field
                    var q = DebtPlus.LINQ.Cache.postal_abbreviation.getList().Find(s => s.type == 3 && (! s.require_modifier) && (string.Compare(s.abbreviation, type, true) == 0 || string.Compare(s.description, type, true) == 0));
                    if (q != null)
                    {
                        input = remainder;
                        modifier = q.abbreviation;
                        break;
                    }
                }

                // Look at the items that take a modifier number
                rxApt = new Regex(@"^\s*(.+)\s+(\S+)\s+(\S+)\s*$", RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.RightToLeft);
                mApt = rxApt.Match(input, input.Length);
                if (mApt.Success)
                {
                    var remainder = mApt.Groups[1].Captures[0].Value;
                    var type = mApt.Groups[2].Captures[0].Value;
                    var number = mApt.Groups[3].Captures[0].Value;

                    // Determine if there is a match to the type field
                    var q = DebtPlus.LINQ.Cache.postal_abbreviation.getList().Find(s => s.type == 3 && s.require_modifier && (string.Compare(s.abbreviation, type, true) == 0 || string.Compare(s.description, type, true) == 0));
                    if (q != null)
                    {
                        input = remainder;
                        modifier = q.abbreviation;
                        modifier_value = number;
                        break;
                    }
                }
            } while (false);

            // Split the suffix
            var rxSuffix = new Regex(@"^\s*(.*)\s+(\S+)\s*$");
            var mSuffix = rxSuffix.Match(input);
            if (mSuffix.Success)
            {
                var str_rest = mSuffix.Groups[1].Captures[0].Value;
                var str_suffix = mSuffix.Groups[2].Captures[0].Value;
                var q = DebtPlus.LINQ.Cache.postal_abbreviation.getList().Find(s => s.type == 2 && (string.Compare(s.abbreviation, str_suffix, true) == 0 || string.Compare(s.description, str_suffix, true) == 0));
                if (q != null)
                {
                    suffix = q.abbreviation;
                    input = str_rest;
                }
            }

            // Split the direction from the first part of the street name
            var rxDir = new Regex(@"^\s*(\S.*)\s+(.+)\s*$", RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.RightToLeft);
            var mDir = rxDir.Match(input, input.Length);
            if (mDir.Success)
            {
                var strDir = mDir.Groups[1].Captures[0].Value;
                var strRest = mDir.Groups[2].Captures[0].Value;
                var q = DebtPlus.LINQ.Cache.postal_abbreviation.getList().Find(s => s.type == 1 && (string.Compare(s.abbreviation, strDir, true) == 0 || string.Compare(s.description, strDir, true) == 0));
                if (q != null)
                {
                    direction = q.abbreviation;
                    input = strRest;
                }
            }

            // Finally, the street name is the input string
            street = input.Trim(new char[] { ' ', ',', '.' });
        }
    }
}
