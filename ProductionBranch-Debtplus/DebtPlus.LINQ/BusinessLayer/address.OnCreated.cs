﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class address
    {
        /// <summary>
        /// Default the object to the proper values as defined for the business layer. These represent the "empty" object.
        /// </summary>
        partial void OnCreated()
        {
            _modifier = Defaultmodifier;

            // Set the other text fields to an empty string.
            _address_line_2 = string.Empty;
            _address_line_3 = string.Empty;
            _city = string.Empty;
            _creditor_prefix_1 = string.Empty;
            _creditor_prefix_2 = string.Empty;
            _direction = string.Empty;
            _house = string.Empty;
            _modifier = string.Empty;
            _modifier_value = string.Empty;
            _PostalCode = string.Empty;
            _street = string.Empty;
            _suffix = string.Empty;

            // The USDA status is required. It can not be null.
            _USDA_Status = '?';
        }
    }
}
