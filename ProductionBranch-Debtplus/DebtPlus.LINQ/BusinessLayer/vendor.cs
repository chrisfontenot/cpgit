﻿using System.Linq;

namespace DebtPlus.LINQ
{
    partial class vendor
    {
        /// <summary>
        /// List of the fields that have been changed
        /// </summary>
        private System.Collections.Generic.Dictionary<string, ChangedFieldItem> colChangedFieldItems = new System.Collections.Generic.Dictionary<string, ChangedFieldItem>();

        /// <summary>
        /// Retrieve the list of changed field items
        /// </summary>
        public System.Collections.Generic.List<ChangedFieldItem> getChangedFieldItems
        {
            get
            {
                return colChangedFieldItems.Values.ToList();
            }
        }

        /// <summary>
        /// Reset the list of changed field items
        /// </summary>
        public void ClearChangedFieldItems()
        {
            colChangedFieldItems.Clear();
        }

        /// <summary>
        /// Handle the change in a data field for the client record
        /// </summary>
        /// <param name="fieldname">Name of the field that is being changed</param>
        /// <param name="oldvalue">The current (old) value before the change</param>
        /// <param name="newvalue">The new value after the change</param>
        void field_changing(string fieldname, object oldvalue, object newvalue)
        {
            var q = colChangedFieldItems.Where(s => s.Key == fieldname).Select(s => s.Value).FirstOrDefault();
            if (q == null)
            {
                lock (colChangedFieldItems)
                {
                    q = colChangedFieldItems.Where(s => s.Key == fieldname).Select(s => s.Value).FirstOrDefault();
                    if (q == null)
                    {
                        q = new ChangedFieldItem()
                        {
                            FieldName = fieldname,
                            OldValue = oldvalue,
                            NewValue = newvalue
                        };
                        colChangedFieldItems.Add(fieldname, q);
                        return;
                    }
                }
            }
            q.NewValue = newvalue;
        }

        partial void OnACH_AccountNumberChanging(string value)
        {
            field_changing("ACH_AccountNumber", _ACH_AccountNumber, value);
        }

        partial void OnACH_RoutingNumberChanging(string value)
        {
            field_changing("ACH_RoutingNumber", _ACH_RoutingNumber, value);
        }

        partial void OnActiveFlagChanging(bool value)
        {
            field_changing("ActiveFlag", _ActiveFlag, value);
        }

        partial void OnBillingModeChanging(string value)
        {
            field_changing("BillingMode", _BillingMode, value);
        }

        partial void OnCC_ExpirationDateChanging(System.DateTime? value)
        {
            field_changing("CC_ExpirationDate", _CC_ExpirationDate, value);
        }

        partial void OnCC_CVVChanging(string value)
        {
            field_changing("CC_CVV", _CC_CVV, value);
        }

        partial void OnLabelChanging(string value)
        {
            field_changing("Label", _Label, value);
        }

        partial void OnMonthlyBillingDayChanging(int? value)
        {
            field_changing("MonthlyBillingDay", _MonthlyBillingDay, value);
        }

        partial void OnNameChanging(string value)
        {
            field_changing("Name", _Name, value);
        }

        partial void OnSupressInvoicesChanging(bool value)
        {
            field_changing("SupressInvoices", _SupressInvoices, value);
        }

        partial void OnSupressPaymentsChanging(bool value)
        {
            field_changing("SupressPayments", _SupressPayments, value);
        }

        partial void OnSupressPrintingPaymentsChanging(bool value)
        {
            field_changing("SupressPrintingPayments", _SupressPrintingPayments, value);
        }

        partial void OnTypeChanging(string value)
        {
            field_changing("Type", _Type, value);
        }

        partial void OnwebsiteChanging(string value)
        {
            field_changing("website", _website, value);
        }

        partial void OnCommentChanging(string value)
        {
            field_changing("comment", _Comment, value);
        }
    }
}