﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class Name : System.IComparable<Name>, System.IComparable, DebtPlus.Interfaces.Names.IName
    {
        /// <summary>
        /// Is the current structure "new" or has it been read from the database?
        /// </summary>
        /// <returns></returns>
        public bool IsNew()
        {
            return Id < 1;
        }

        /// <summary>
        /// Is the current structure empty or does it have values that need to be saved?
        /// </summary>
        /// <returns></returns>
        public bool IsEmpty()
        {
            foreach (string item in new string[] { Prefix, First, Middle, Last, Suffix })
            {
                if (!string.IsNullOrWhiteSpace(item))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Convert the current item to a string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return FormatNormalName();
        }

        /// <summary>
        /// Compare the current item against another to determine if the values are the same
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            return CompareTo(obj as Name);
        }

        /// <summary>
        /// Compare the current item against another to determine if the values are the same
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(Name other)
        {
            // If the other item is missing then the current is always greater.
            if (other == null)
            {
                return 1;
            }

            // Compare the last name
            Int32 Relation = Helpers.Comparison(Last, other.Last);

            // Compare the other parts of the name until we find the first non-match
            if (Relation == 0)
            {
                Relation = Helpers.Comparison(Suffix, other.Suffix);
                if (Relation == 0)
                {
                    Relation = Helpers.Comparison(First, other.First);
                    if (Relation == 0)
                    {
                        Relation = Helpers.Comparison(Middle, other.Middle);
                        if (Relation == 0)
                        {
                            Relation = Helpers.Comparison(Prefix, other.Prefix);
                        }
                    }
                }
            }

            return Relation;
        }

        /// <summary>
        /// Convert the current item to a string using the values defined
        /// </summary>
        /// <returns></returns>
        public string FormatNormalName()
        {
            return FormatNormalName(Prefix, First, Middle, Last, Suffix);
        }

        /// <summary>
        /// Format the person's name from the component pieces
        /// </summary>
        /// <param name="prefix">Prefix to the name</param>
        /// <param name="first">Christian name (first name)</param>
        /// <param name="middle">Middle name</param>
        /// <param name="last">Sir name (last name)</param>
        /// <param name="suffix">suffix to the name</param>
        public static String FormatNormalName(Object prefix, Object first, Object middle, Object last, Object suffix)
        {
			return FormatNormalName(DStr(prefix).Trim(), DStr(first).Trim(), DStr(middle).Trim(), DStr(last).Trim(), DStr(suffix).Trim());
        }

        /// <summary>
        /// Format the person's name from the component pieces
        /// </summary>
        /// <param name="prefix">Prefix to the name</param>
        /// <param name="first">Christian name (first name)</param>
        /// <param name="middle">Middle name</param>
        /// <param name="last">Sir name (last name)</param>
        /// <param name="suffix">suffix to the name</param>
        public static String FormatNormalName(string prefix, string first, string middle, string last, string suffix)
        {
            StringBuilder sb = new StringBuilder();

            // Prefix
            if (! string.IsNullOrEmpty(prefix))
            {
                sb.AppendFormat(" {0}", prefix);
            }

            // First
            if (! string.IsNullOrEmpty(first))
            {
                sb.AppendFormat(" {0}", first);
            }

            // Middle
            if (! string.IsNullOrEmpty(middle))
            {
                sb.AppendFormat(" {0}", middle);
                if (middle.Length == 1)
                {
                    sb.Append(".");
                }
            }

            // Last
            if (! string.IsNullOrEmpty(last))
            {
                sb.AppendFormat(" {0}", last);
            }

            // suffix
            if (! string.IsNullOrEmpty(suffix))
            {
                sb.AppendFormat(", {0}", suffix);
            }

            return sb.ToString().Trim(new char[] {' ', ','});
        }

        /// <summary>
        /// Format the name in last name first order
        /// </summary>
        /// <returns>The string formatted as "last [ suffix], first [, middle] [, prefix]</returns>
        public String FormatReverseName()
        {
            return FormatReverseName(Prefix, First, Middle, Last, Suffix);
        }

        /// <summary>
        /// Format the name in last name first order
        /// </summary>
        /// <param name="prefix">Prefix to the name</param>
        /// <param name="first">Christian name (first name)</param>
        /// <param name="middle">Middle name</param>
        /// <param name="last">Sir name (last name)</param>
        /// <param name="suffix">suffix to the name</param>
        /// <returns>The string formatted as "last [ suffix], first [, middle] [, prefix]</returns>
        public static String FormatReverseName(Object prefix, Object first, Object middle, Object last, Object suffix)
        {
            return FormatReverseName(DStr(prefix).Trim(), DStr(first).Trim(), DStr(middle).Trim(), DStr(last).Trim(), DStr(suffix).Trim());
        }

        /// <summary>
        /// Format the name in last name first order
        /// </summary>
        /// <param name="prefix">Prefix to the name</param>
        /// <param name="first">Christian name (first name)</param>
        /// <param name="middle">Middle name</param>
        /// <param name="last">Sir name (last name)</param>
        /// <param name="suffix">suffix to the name</param>
        /// <returns>The string formatted as "last [ suffix], first [, middle] [, prefix]</returns>
        public static String FormatReverseName(string prefix, string first, string middle, string last, string suffix)
        {
            StringBuilder sb = new StringBuilder();

            // Start with the last name
            if (!string.IsNullOrEmpty(last))
            {
                sb.Append(last);
            }

            // Add the suffix if needed
            if (!string.IsNullOrEmpty(suffix))
            {
                if (sb.Length > 0)
                {
                    sb.Append(" ");
                }
                sb.Append(suffix);
            }

            // Add the first name
            if (!string.IsNullOrEmpty(first))
            {
                if (sb.Length > 0)
                {
                    sb.Append(", ");
                }
                sb.Append(first);
            }

            // Add the middle name
            if (!string.IsNullOrEmpty(middle))
            {
                if (sb.Length > 0)
                {
                    sb.Append(", ");
                }
                sb.Append(middle);
            }

            // Finally, add the prefix to the name
            if (!string.IsNullOrEmpty(prefix))
            {
                if (sb.Length > 0)
                {
                    sb.Append(", ");
                }
                sb.Append(prefix);
            }

            return sb.ToString();
        }

        /// <summary>
        /// Convert the input object value to a suitable string
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private static string DStr(object obj)
        {
            if (obj != null && !System.Object.Equals(obj, System.DBNull.Value))
            {
                try
                {
                    return Convert.ToString(obj);
                }
                catch { }
            }
            return string.Empty;
        }
    }
}
