﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class client_DisclosureLanguageType
    {
        /// <summary>
        /// Counselor name suitable for the display grid
        /// </summary>
        public string display_language
        {
            get
            {
                if (this.languageID > 0)
                {
                    var q = DebtPlus.LINQ.Cache.AttributeType.getLanguageList().Find(s => s.Id == languageID);
                    if (q != null)
                    {
                        return q.Attribute;
                    }
                }

                return string.Empty;
            }
        }
    }
}
