﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    public enum PHONE_TYPE
    {
        None,
        ClientMsg,
        ClientHome,
        ApplicantWork,
        ApplicantWork2,
        ApplicantCell,
        ApplicantCell2,
        CoApplicantHome,
        CoApplicantMsg,
        CoApplicantWork,
        CoApplicantWork2,
        CoApplicantCell,
        CoApplicantCell2,
        BadNumber
    }

    public enum US_STATE
    {
        None, //0, None
        AL, //1, Alabama
        AK, //2, Alaska
        AS, //3, American Samoa
        AZ, //4, Arizona
        AR, //5, Arkansas
        AEA, //6, Armed Forces Africa
        AA, //7, Armed Forces Americas
        AEC, //8, Armed Forces Canada
        AE, //9, Armed Forces Europe
        AEE, //10, Armed Forces Middle East
        AP, //11, Armed Forces Pacific
        CA, //12, California
        CO, //13, Colorado
        CT, //14, Connecticut
        DE, //15, Delaware
        DC, //16, District of Columbia
        FM, //17, Federated States of Micronesia
        FL, //18, Florida
        GA, //19, Georgia
        GU, //20, Guam
        HI, //21, Hawaii
        ID, //22, Idaho
        IL, //23, Illinois
        IN, //24, Indiana
        IA, //25, Iowa
        KS, //26, Kansas
        KY, //27, Kentucky
        LA, //28, Louisiana
        ME, //29, Maine
        MH, //30, Marshall Islands
        MD, //31, Maryland
        MA, //32, Massachusetts
        MI, //33, Michigan
        MN, //34, Minnesota
        MS, //35, Mississippi
        MO, //36, Missouri
        MT, //37, Montana
        NE, //38, Nebraska
        NV, //39, Nevada
        NH, //40, New Hampshire
        NJ, //41, New Jersey
        NM, //42, New Mexico
        NY, //43, New York
        NC, //44, North Carolina
        ND, //45, North Dakota
        MP, //46, Northern Mariana Islands
        OH, //47, Ohio
        OK, //48, Oklahoma
        OR, //49, Oregon
        PW, //50, Palau
        PA, //51, Pennsylvania
        PR, //52, Puerto Rico
        RI, //53, Rhode Island
        SC, //54, South Carolina
        SD, //55, South Dakota
        TN, //56, Tennessee
        TX, //57, Texas
        UT, //58, Utah
        VT, //59, Vermont
        VI, //60, Virgin Islands
        VA, //61, Virginia
        WA, //62, Washington
        WV, //63, West Virginia
        WI, //64, Wisconsin
        WY //65, Wyoming
    }

    public enum TIMEZONE
    {
        Chamorro,
        Atlantic,
        Eastern,
        Central,
        Mountain,
        Pacific,
        Alaska,
        Hawaii,
        Indeterminate
    }

}