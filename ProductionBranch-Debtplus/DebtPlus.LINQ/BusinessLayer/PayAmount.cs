﻿using System;
using System.Linq;

namespace DebtPlus.LINQ
{
    partial class DebtPlusDataContext
    {
        /// <summary>
        /// Calculate the monthly pay amount given the payment frequency and amount. We compute
        /// a monthly figure from the number of periods per year and then reduce it to a monthly
        /// dollar amount.
        /// </summary>
        /// <param name="Amount">The payment amount</param>
        /// <param name="Frequency">Primary key to the PayFrequencyTypes table</param>
        /// <returns>The corresponding monthly amount</returns>
        /// <remarks>This uses the cached copy of the PayFrequenctyTypes table</remarks>
        public static Decimal PayAmount(Decimal Amount, Int32 Frequency)
        {
            // Calculate the annual figure and divide it by 12 to get a monthly amount.
            var q = DebtPlus.LINQ.Cache.PayFrequencyType.getList().Find(s => s.Id == Frequency);
            if (q != null && q.PeriodsPerYear > 0)
            {
                return System.Decimal.Floor(((Amount * Convert.ToDecimal(q.PeriodsPerYear)) / 12M) * 100M) / 100M;
            }

            return Amount;
        }
    }
}