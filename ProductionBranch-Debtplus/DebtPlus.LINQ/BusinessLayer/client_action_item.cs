﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class client_action_item
    {
        /// <summary>
        /// Validate the record before the entry is updated or inserted into the database. We can ignore "delete".
        /// </summary>
        /// <param name="action">Action to be performed on the database</param>
        partial void OnValidate(System.Data.Linq.ChangeAction action)
        {
            // If the operation is not to update or insert then we can just ignore the request
            if (action != System.Data.Linq.ChangeAction.Insert && action != System.Data.Linq.ChangeAction.Update)
            {
                return;
            }

            // Ensure that we have a valid pointer to he action record. We don't check to see if it exists, just that it is not empty.
            if (action_item <= 0)
            {
                throw new DataValidationException("action_item is not a valid key pointer");
            }
        }
    }
}
