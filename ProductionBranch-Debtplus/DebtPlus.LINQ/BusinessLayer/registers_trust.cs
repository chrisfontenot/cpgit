﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class registers_trust
    {
        partial void OnCreated()
        {
            // Default the date that the item was created to "Now".
            _date_created = DateTime.Now;
        }

        /// <summary>
        /// Used in the check printing logic to determine if the check is to be printed or not.
        /// </summary>
        public bool Selected { get; set; }
    }
}
