﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class vendor_address
    {
        public static readonly char AddressType_Dispute = 'D';
        public static readonly char AddressType_Invoice = 'I';
        public static readonly char AddressType_General = 'G';
    }
}
