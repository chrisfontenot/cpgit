﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class creditor_method
    {
        /// <summary>
        /// Return the type of the bank account.
        /// </summary>
        public string bank_type
        {
            get
            {
                if (bank > 0)
                {
                    var q = DebtPlus.LINQ.Cache.bank.getList().Find(s => s.Id == bank);
                    if (q != null)
                    {
                        return q.type;
                    }
                }
                return "C";
            }
        }

        /// <summary>
        /// The type of the bank
        /// </summary>
        public string display_bank
        {
            get
            {
                switch (bank_type)
                {
                    case "C":
                        return (this.type == "EFT") ? "Checks" : "Paper";

                    case "R":
                        return "RPPS";

                    default:
                        return bank_type;
                }
            }
        }

        /// <summary>
        /// Region description for this method
        /// </summary>
        public string display_region
        {
            get
            {
                if (this.region > 0)
                {
                    var q = DebtPlus.LINQ.Cache.region.getList().Find(s => s.Id == region);
                    if (q != null)
                    {
                        return q.description;
                    }
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// The distinct biller ID for this method. It is RPPS.
        /// </summary>
        public string display_biller_id
        {
            get
            {
                switch (bank_type)
                {
                    case "R":
                        return rpps_biller_id;

                    default:
                        return null;
                }
            }
        }
    }
}
