﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class tickler
    {
        private bool _ShowInGrid = false;
        /// <summary>
        /// Should the item be shown in the grid of ticklers. Set on newly created items
        /// before and after they have been saved. This is not saved to the database.
        /// </summary>
        public bool ShowInGrid
        {
            get
            {
                return _ShowInGrid;
            }
            set
            {
                if (_ShowInGrid != value)
                {
                    SendPropertyChanging();
                    _ShowInGrid = value;
                    SendPropertyChanged("ShowInGrid");
                }
            }
        }
    }
}
