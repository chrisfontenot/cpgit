﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class letter_field : System.ICloneable
    {
        // Fields that are not stored in the database. They are referenced in the program
        // for each item in the database. We don't track changes to these fields.
        public string value { get; set; }
        public bool referenced { get; set; }
        public bool HasValue { get; set; }

        object ICloneable.Clone()
        {
            return MemberwiseClone();
        }
    }
}
