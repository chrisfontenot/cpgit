﻿using System;
using System.Linq;

namespace DebtPlus.LINQ
{
    partial class client
    {
        /// <summary>
        /// List of the fields that have been changed
        /// </summary>
        private System.Collections.Generic.Dictionary<string, ChangedFieldItem> colChangedFieldItems = new System.Collections.Generic.Dictionary<string, ChangedFieldItem>();

        /// <summary>
        /// Retrieve the list of changed field items
        /// </summary>
        public System.Collections.Generic.List<ChangedFieldItem> getChangedFieldItems
        {
            get
            {
                return colChangedFieldItems.Values.ToList();
            }
        }

        /// <summary>
        /// Reset the list of changed field items
        /// </summary>
        public void ClearChangedFieldItems()
        {
            colChangedFieldItems.Clear();
        }

        /// <summary>
        /// Handle the change in a data field for the client record
        /// </summary>
        /// <param name="fieldname">Name of the field that is being changed</param>
        /// <param name="oldvalue">The current (old) value before the change</param>
        /// <param name="newvalue">The new value after the change</param>
        void field_changing(string fieldname, object oldvalue, object newvalue)
        {
            var q = colChangedFieldItems.Where(s => s.Key == fieldname).Select(s => s.Value).FirstOrDefault();
            if (q == null)
            {
                lock (colChangedFieldItems)
                {
                    q = colChangedFieldItems.Where(s => s.Key == fieldname).Select(s => s.Value).FirstOrDefault();
                    if (q == null)
                    {
                        q = new ChangedFieldItem()
                        {
                            FieldName = fieldname,
                            OldValue = oldvalue,
                            NewValue = newvalue
                        };
                        colChangedFieldItems.Add(fieldname, q);
                        return;
                    }
                }
            }
            q.NewValue = newvalue;
        }

        partial void OnInboundTelephoneGroupIDChanging(Int32? value)
        {
            field_changing("InboundTelephoneGroupID", _InboundTelephoneGroupID, value);
        }

        partial void OnsalutationChanging(string value)
        {
            field_changing("salutation", _salutation, value);
        }

        partial void OncountyChanging(int value)
        {
            field_changing("county", _county, value);
        }

        partial void OnregionChanging(int value)
        {
            field_changing("region", _region, value);
        }

        partial void Onactive_statusChanging(string value)
        {
            _active_status_date = DateTime.Now;
            if ((value == "I") && (_active_status != "I" && _active_status != "EX"))
            {
                _drop_date = _active_status_date;
            }
            field_changing("active_status", _active_status, value);
        }

        partial void Onclient_statusChanging(string value)
        {
            _client_status_date = DateTime.Now;
            if (_client_status != "DMP" && value == "DMP")
            {
                _dmp_status_date = _client_status_date;
            }
            field_changing("client_status", _client_status, value);
        }

        partial void Ondisbursement_dateChanging(int value)
        {
            field_changing("disbursement_date", _disbursement_date, value);
        }

        partial void Onmail_error_dateChanging(System.Nullable<System.DateTime> value)
        {
            field_changing("mail_error_date", _mail_error_date, value);
        }

        partial void Onstart_dateChanging(System.Nullable<System.DateTime> value)
        {
            field_changing("start_date", _start_date, value);
        }

        partial void Ondrop_reasonChanging(System.Nullable<int> value)
        {
            if (value.HasValue)
            {
                _drop_date = DateTime.Now;
            }
            field_changing("drop_reason", _drop_reason, value);
        }

        partial void Ondrop_reason_otherChanging(string value)
        {
            field_changing("drop_reason_other", _drop_reason_other, value);
        }

        partial void Onreferred_byChanging(System.Nullable<int> value)
        {
            field_changing("referred_by", _referred_by, value);
        }

        partial void Oncause_fin_problem1Changing(System.Nullable<int> value)
        {
            field_changing("cause_fin_problem1", _cause_fin_problem1, value);
        }

        partial void Oncause_fin_problem2Changing(System.Nullable<int> value)
        {
            field_changing("cause_fin_problem2", _cause_fin_problem2, value);
        }

        partial void Oncause_fin_problem3Changing(System.Nullable<int> value)
        {
            field_changing("cause_fin_problem3", _cause_fin_problem3, value);
        }

        partial void Oncause_fin_problem4Changing(System.Nullable<int> value)
        {
            field_changing("cause_fin_problem4", _cause_fin_problem4, value);
        }

        partial void OnofficeChanging(System.Nullable<int> value)
        {
            field_changing("office", _office, value);
        }

        partial void OncounselorChanging(System.Nullable<int> value)
        {
            field_changing("counselor", _counselor, value);
        }

        partial void OncsrChanging(System.Nullable<int> value)
        {
            field_changing("csr", _csr, value);
        }

        partial void Onhold_disbursementsChanging(bool value)
        {
            field_changing("hold_disbursements", _hold_disbursements, value);
        }

        partial void Onpersonal_checksChanging(bool value)
        {
            field_changing("personal_checks", _personal_checks, value);
        }

        partial void Onach_activeChanging(bool value)
        {
            field_changing("ach_active", _ach_active, value);
        }

        partial void Onstack_prorationChanging(bool value)
        {
            field_changing("stack_proration", _stack_proration, value);
        }

        partial void OnElectronicStatementsChanging(int value)
        {
            field_changing("ElectronicStatements", _ElectronicStatements, value);
        }

        partial void OnElectronicCorrespondenceChanging(int value)
        {
            field_changing("ElectronicCorrespondence", _ElectronicCorrespondence, value);
        }

        partial void Onbankruptcy_classChanging(int value)
        {
            field_changing("bankruptcy_class", _bankruptcy_class, value);
        }

        partial void Onsatisfaction_scoreChanging(int value)
        {
            field_changing("satisfaction_score", _satisfaction_score, value);
        }

        partial void Onfed_tax_owedChanging(decimal value)
        {
            field_changing("fed_tax_owed", _fed_tax_owed, value);
        }

        partial void Onstate_tax_owedChanging(decimal value)
        {
            field_changing("state_tax_owed", _state_tax_owed, value);
        }

        partial void Onlocal_tax_owedChanging(decimal value)
        {
            field_changing("local_tax_owed", _local_tax_owed, value);
        }

        partial void Onfed_tax_monthsChanging(int value)
        {
            field_changing("fed_tax_months", _fed_tax_months, value);
        }

        partial void Onstate_tax_monthsChanging(int value)
        {
            field_changing("state_tax_months", _state_tax_months, value);
        }

        partial void Onlocal_tax_monthsChanging(int value)
        {
            field_changing("local_tax_months", _local_tax_months, value);
        }

        partial void Ondeposit_in_trustChanging(decimal value)
        {
            field_changing("deposit_in_trust", _deposit_in_trust, value);
        }

        partial void Onreserved_in_trustChanging(decimal value)
        {
            field_changing("reserved_in_trust", _reserved_in_trust, value);
        }

        partial void Onreserved_in_trust_cutoffChanging(System.Nullable<System.DateTime> value)
        {
            field_changing("reserved_in_trust_cutoff", _reserved_in_trust_cutoff, value);
        }

        partial void Onmarital_statusChanging(System.Nullable<int> value)
        {
            field_changing("marital_status", _marital_status, value);
        }

        partial void OnlanguageChanging(System.Nullable<int> value)
        {
            field_changing("language", _language, value);
        }

        partial void OndependentsChanging(int value)
        {
            field_changing("dependents", _dependents, value);
        }

        partial void OnhouseholdChanging(int value)
        {
            field_changing("household", _household, value);
        }

        partial void Onmethod_first_contactChanging(int value)
        {
            field_changing("method_first_contact", _method_first_contact, value);
        }

        partial void Onconfig_feeChanging(int value)
        {
            field_changing("config_fee", _config_fee, value);
        }

        partial void Onpreferred_contactChanging(int value)
        {
            field_changing("preferred_contact", _preferred_contact, value);
        }
    }
}