﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class secured_loan
    {
        /// <summary>
        /// Define the contents of the class when it is first created.
        /// </summary>
        partial void OnCreated()
        {
            this.arm = false;
            this.arm_reset = false;
            this.fha_va_insured = false;
            this.hybrid_arm = false;
            this.interest_only = false;
            this.option_arm = false;
            this.privately_held = false;
            this.interest_rate = 0D;
            this.balance = 0M;
            this.original_amount = 0M;
            this.past_due_amount = 0M;
            this.past_due_periods = 0;
            this.payment = 0M;
            this.periods = 0;
            this.priority = 1;
            this.lender = string.Empty;
            this.case_number = string.Empty;
            this.fdic_number = string.Empty;
            this.loan_type_description = string.Empty;
            this.original_account_number = string.Empty;
            this.original_fdic_number = string.Empty;
            this.original_lender = string.Empty;
        }
    }
}
