﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class workshop_content_type
    {
        /// <summary>
        /// Retrieve the string value of the class
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.name ?? string.Empty;
        }
    }
}
