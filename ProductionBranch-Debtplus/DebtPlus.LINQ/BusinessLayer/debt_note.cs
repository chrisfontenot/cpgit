﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class debt_note : ICloneable
    {
        /// <summary>
        /// CLone the current object into a new item with a shallow copy.
        /// </summary>
        object ICloneable.Clone()
        {
            return MemberwiseClone();
        }

        /// <summary>
        /// For account notes (general messages to the creditor)
        /// </summary>
        public static readonly string Type_AccountNote = "AN";

        /// <summary>
        /// For client notes (not used)
        /// </summary>
        [Obsolete("Use Type_AccountNote instead")]
        public static readonly string Type_ClientNote = "CN";

        /// <summary>
        /// For proposal notes
        /// </summary>
        public static readonly string Type_ProposalNote = "PR";

        /// <summary>
        /// For drop notices
        /// </summary>
        public static readonly string Type_DropNote = "DR";
    }
}
