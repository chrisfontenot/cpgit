﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class rpps_mask
    {
        /// <summary>
        /// Return the mask type. It is RPPS
        /// </summary>
        public string display_type
        {
            get
            {
                return "RPPS";
            }
        }
    }
}
