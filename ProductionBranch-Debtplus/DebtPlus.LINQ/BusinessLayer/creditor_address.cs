﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class creditor_address
    {
        /// <summary>
        /// Convert the address to a suitable string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            // If there is an attention line then include it.
            if (!string.IsNullOrEmpty(attn))
            {
                sb.AppendLine();
                sb.AppendFormat("Attn: {0}", attn.Trim());
            }

            if (AddressID.HasValue)
            {
                using (var bc = new BusinessContext())
                {
                    var adr = bc.addresses.Where<address>(s => s.Id == AddressID.Value).FirstOrDefault();
                    if (adr != null)
                    {
                        sb.AppendLine();
                        sb.Append(adr.ConvertToString(false, true, true, true));
                    }
                }
            }

            if (sb.Length > 0)
            {
                sb.Remove(0, 2);
            }

            return sb.ToString();
        }
    }
}
