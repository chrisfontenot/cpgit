﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class counselor : IComparable
    {
        public int CompareTo(object obj)
        {
            counselor other = obj as counselor;
            if (other == null)
            {
                return 1;
            }

            DebtPlus.LINQ.Name this_Name = this.Name;
            DebtPlus.LINQ.Name other_Name = other.Name;

            if (this_Name == null && other_Name == null)
            {
                return 0;
            }

            if (this_Name == null)
            {
                return -1;
            }

            if (other_Name == null)
            {
                return 1;
            }

            return this_Name.CompareTo(other_Name);
        }
    }
}
