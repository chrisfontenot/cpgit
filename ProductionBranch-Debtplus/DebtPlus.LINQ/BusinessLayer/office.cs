﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.LINQ;

namespace DebtPlus.LINQ
{
    partial class office : System.IComparable<office>
    {
        /// <summary>
        /// Override the toString to return the description field.
        /// </summary>
        public override string ToString()
        {
            return this.name ?? string.Empty;
        }

        public int CompareTo(office other)
        {
            return this.ToString().CompareTo(other.ToString());
        }
    }
}