﻿using System.Linq;

namespace DebtPlus.LINQ
{
    partial class people : DebtPlus.Interfaces.Client.IPeople
    {
        /// <summary>
        /// List of the fields that have been changed
        /// </summary>
        private System.Collections.Generic.Dictionary<string, ChangedFieldItem> colChangedFieldItems = new System.Collections.Generic.Dictionary<string, ChangedFieldItem>();

        /// <summary>
        /// Retrieve the list of changed field items
        /// </summary>
        public System.Collections.Generic.List<ChangedFieldItem> getChangedFieldItems
        {
            get
            {
                return colChangedFieldItems.Values.ToList();
            }
        }

        /// <summary>
        /// Reset the list of changed field items
        /// </summary>
        public void ClearChangedFieldItems()
        {
            colChangedFieldItems.Clear();
        }

        /// <summary>
        /// Handle the change in a data field for the client record
        /// </summary>
        /// <param name="fieldname">Name of the field that is being changed</param>
        /// <param name="oldvalue">The current (old) value before the change</param>
        /// <param name="newvalue">The new value after the change</param>
        void field_changing(string fieldname, object oldvalue, object newvalue)
        {
            var q = colChangedFieldItems.Where(s => s.Key == fieldname).Select(s => s.Value).FirstOrDefault();
            if (q == null)
            {
                lock (colChangedFieldItems)
                {
                    q = colChangedFieldItems.Where(s => s.Key == fieldname).Select(s => s.Value).FirstOrDefault();
                    if (q == null)
                    {
                        q = new ChangedFieldItem()
                        {
                            FieldName = fieldname,
                            OldValue = oldvalue,
                            NewValue = newvalue
                        };
                        colChangedFieldItems.Add(fieldname, q);
                        return;
                    }
                }
            }
            q.NewValue = newvalue;
        }

        partial void OnRelationChanging(int value)
        {
            field_changing("Relation", _Relation, value);
        }

        partial void OnFormerChanging(string value)
        {
            field_changing("Former", _Former, value);
        }

        partial void OnSSNChanging(string value)
        {
            field_changing("SSN", _SSN, value);
        }

        partial void OnGenderChanging(int value)
        {
            field_changing("Gender", _Gender, value);
        }

        partial void OnRaceChanging(int value)
        {
            field_changing("Race", _Race, value);
        }

        partial void OnEthnicityChanging(int value)
        {
            field_changing("Ethnicity", _Ethnicity, value);
        }

        partial void OnDisabledChanging(bool value)
        {
            field_changing("Disabled", _Disabled, value);
        }

        partial void OnMilitaryServiceIDChanging(int value)
        {
            field_changing("MilitaryServiceID", _MilitaryServiceID, value);
        }

        partial void OnMilitaryStatusIDChanging(int value)
        {
            field_changing("MilitaryStatusID", _MilitaryStatusID, value);
        }

        partial void OnMilitaryGradeIDChanging(int value)
        {
            field_changing("MilitaryGradeID", _MilitaryGradeID, value);
        }

        partial void OnMilitaryDependentIDChanging(int value)
        {
            field_changing("MilitaryDependentID", _MilitaryDependentID, value);
        }

        partial void OnEducationChanging(int value)
        {
            field_changing("Education", _Education, value);
        }

        partial void OnBirthdateChanging(System.Nullable<System.DateTime> value)
        {
            field_changing("Birthdate", _Birthdate, value);
        }

        partial void OnFICO_ScoreChanging(System.Nullable<int> value)
        {
            field_changing("FICO_Score", _FICO_Score, value);
        }

        partial void OnCreditAgencyChanging(string value)
        {
            field_changing("CreditAgency", _CreditAgency, value);
        }

        partial void Onno_fico_score_reasonChanging(System.Nullable<int> value)
        {
            field_changing("no_fico_score_reason", _no_fico_score_reason, value);
        }

        partial void Ongross_incomeChanging(decimal value)
        {
            field_changing("gross_income", _gross_income, value);
        }

        partial void Onnet_incomeChanging(decimal value)
        {
            field_changing("net_income", _net_income, value);
        }

        partial void OnFrequencyChanging(int value)
        {
            field_changing("Frequency", _Frequency, value);
        }

        partial void Onemp_start_dateChanging(System.Nullable<System.DateTime> value)
        {
            field_changing("emp_start_date", _emp_start_date, value);
        }

        partial void Onemp_end_dateChanging(System.Nullable<System.DateTime> value)
        {
            field_changing("emp_end_date", _emp_end_date, value);
        }

        partial void OnemployerChanging(System.Nullable<int> value)
        {
            field_changing("employer", _employer, value);
        }

        partial void OnjobChanging(System.Nullable<int> value)
        {
            field_changing("job", _job, value);
        }

        partial void Onother_jobChanging(string value)
        {
            field_changing("other_job", _other_job, value);
        }

        partial void OnbkfileddateChanging(System.Nullable<System.DateTime> value)
        {
            field_changing("bkfileddate", _bkfileddate, value);
        }

        partial void OnbkdischargeChanging(System.Nullable<System.DateTime> value)
        {
            field_changing("bkdischarge", _bkdischarge, value);
        }

        partial void OnbkchapterChanging(System.Nullable<int> value)
        {
            field_changing("bkchapter", _bkchapter, value);
        }

        partial void OnbkPaymentCurrentChanging(System.Nullable<int> value)
        {
            field_changing("bkPaymentCurrent", _bkPaymentCurrent, value);
        }

        partial void OnbkAuthLetterDateChanging(System.Nullable<System.DateTime> value)
        {
            field_changing("bkAuthLetterDate", _bkAuthLetterDate, value);
        }

        partial void OnbkCertificateNoChanging(string value)
        {
            field_changing("bkCertificateNo", _bkCertificateNo, value);
        }

        partial void OnbkCertificateExpiresChanging(System.Nullable<System.DateTime> value)
        {
            field_changing("bkCertificateExpires", _bkCertificateExpires, value);
        }

        partial void OnbkCertificateIssuedChanging(System.Nullable<System.DateTime> value)
        {
            field_changing("bkCertificateIssued", _bkCertificateIssued, value);
        }

        partial void OnbkDistrictChanging(System.Nullable<System.Int32> value)
        {
            field_changing("bkDistrict", _bkDistrict, value);
        }
    }
}