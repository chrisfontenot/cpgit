﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    partial class client_poa
    {
        /// <summary>
        /// List of the fields that have been changed
        /// </summary>
        private System.Collections.Generic.Dictionary<string, ChangedFieldItem> colChangedFieldItems = new System.Collections.Generic.Dictionary<string, ChangedFieldItem>();

        /// <summary>
        /// Retrieve the list of changed field items
        /// </summary>
        public System.Collections.Generic.List<ChangedFieldItem> getChangedFieldItems
        {
            get
            {
                return colChangedFieldItems.Values.ToList();
            }
        }

        /// <summary>
        /// Reset the list of changed field items
        /// </summary>
        public void ClearChangedFieldItems()
        {
            colChangedFieldItems.Clear();
        }

        /// <summary>
        /// Handle the change in a data field for the client record
        /// </summary>
        /// <param name="fieldname">Name of the field that is being changed</param>
        /// <param name="oldvalue">The current (old) value before the change</param>
        /// <param name="newvalue">The new value after the change</param>
        private void field_changing(string fieldname, object oldvalue, object newvalue)
        {
            var q = colChangedFieldItems.Where(s => s.Key == fieldname).Select(s => s.Value).FirstOrDefault();
            if (q == null)
            {
                lock (colChangedFieldItems)
                {
                    q = colChangedFieldItems.Where(s => s.Key == fieldname).Select(s => s.Value).FirstOrDefault();
                    if (q == null)
                    {
                        q = new ChangedFieldItem()
                        {
                            FieldName = fieldname,
                            OldValue = oldvalue,
                            NewValue = newvalue
                        };
                        colChangedFieldItems.Add(fieldname, q);
                        return;
                    }
                }
            }
            q.NewValue = newvalue;
        }

        partial void OnApprovalByChanging(string value)
        {
            field_changing("ApprovalBy", _ApprovalBy, value);
        }

        partial void OnFinancialChanging(int? value)
        {
            field_changing("Financial", _Financial, value);
        }

        partial void OnOtherChanging(int? value)
        {
            field_changing("Other", _Other, value);
        }

        partial void OnPOA_ExpiresChanging(DateTime? value)
        {
            field_changing("POA_Expires", _POA_Expires, value);
        }

        partial void OnPOA_ID_ExpiresChanging(DateTime? value)
        {
            field_changing("POA_ID_Expires", _POA_ID_Expires, value);
        }

        partial void OnPOA_ID_IssuedChanging(DateTime? value)
        {
            field_changing("POA_ID_Issued", _POA_ID_Issued, value);
        }

        partial void OnPOATypeChanging(int? value)
        {
            field_changing("POAType", _POAType, value);
        }

        partial void OnRealEstateChanging(int? value)
        {
            field_changing("RealEstate", _RealEstate, value);
        }

        partial void OnStageChanging(int? value)
        {
            field_changing("Stage", _Stage, value);
        }

        partial void OnTelephoneTypeChanging(int? value)
        {
            field_changing("TelephoneType", _TelephoneType, value);
        }

        partial void OnPOA_IDChanging(string value)
        {
            field_changing("POA_ID", _POA_ID, value);
        }
    }
}
