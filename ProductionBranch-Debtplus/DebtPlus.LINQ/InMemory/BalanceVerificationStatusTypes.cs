﻿using System;

namespace DebtPlus.LINQ.InMemory
{
    public class BalanceVerificationStatusType
    {
        public Int32 Id { get; set; }

        public string description { get; set; }

        public bool SuspendProposals { get; set; }

        public string SuspendProposals_description
        {
            get
            {
                return SuspendProposals ? "Yes" : "No";
            }
        }

        public BalanceVerificationStatusType()
        {
        }

        internal BalanceVerificationStatusType(Int32 Id, string description, bool SuspendProposals)
        {
            this.Id = Id;
            this.description = description;
            this.SuspendProposals = SuspendProposals;
        }
    }

    public static class BalanceVerificationStatusTypes
    {
        private static System.Collections.Generic.List<BalanceVerificationStatusType> col = null;

        public static System.Collections.Generic.List<BalanceVerificationStatusType> getList()
        {
            if (col == null)
            {
                col = new System.Collections.Generic.List<BalanceVerificationStatusType>();
                col.Add(new BalanceVerificationStatusType(0, "Normal", false));
                col.Add(new BalanceVerificationStatusType(1, "Request Balance Update", false));
                col.Add(new BalanceVerificationStatusType(2, "Must Send Prenote BAL", true));
                col.Add(new BalanceVerificationStatusType(3, "Prenote BAL Sent", true));
                col.Add(new BalanceVerificationStatusType(4, "Prenote BAL Failed", true));
                col.Add(new BalanceVerificationStatusType(5, "Prenote BAL Success", false));
            }

            return col;
        }

        public static Int32 getDefault()
        {
            return 0;
        }
    }
}