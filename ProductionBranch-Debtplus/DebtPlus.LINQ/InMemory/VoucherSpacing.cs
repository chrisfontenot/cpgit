﻿using System;
using System.Linq;

namespace DebtPlus.LINQ.InMemory
{
    /// <summary>
    /// The collection if items in the list are of this type.
    /// </summary>
    public class VoucherSpacing
    {
        /// <summary>
        /// ID for the record
        /// </summary>
        public Int32 Id { get; set; }

        /// <summary>
        /// Descriptive text for the record
        /// </summary>
        public string description { get; set; }

        /// <summary>
        /// Is this item the default item?
        /// </summary>
        public Boolean Default { get; set; }

        /// <summary>
        /// Is this item Active? Can it be used for new designations?
        /// </summary>
        public Boolean ActiveFlag { get; set; }

        public VoucherSpacing()
        {
            Default = false;
            ActiveFlag = true;
            description = string.Empty;
            Id = default(Int32);
        }

        internal VoucherSpacing(Int32 Id, string description, Boolean Default) : this()
        {
            this.Id = Id;
            this.description = description;
            this.Default = Default;
        }
    }

    /// <summary>
    /// This list is used for items that are Yes/No/Ask status such as client's ElectronicStatements field.
    /// </summary>
    public static class VoucherSpacings
    {
        private static System.Collections.Generic.List<VoucherSpacing> col;

        /// <summary>
        /// Initialize the new class structure
        /// </summary>
        static VoucherSpacings()
        {
            col = new System.Collections.Generic.List<VoucherSpacing>();
            col.Add(new VoucherSpacing(0, "Single", true));
            col.Add(new VoucherSpacing(1, "Double", false));
            col.Add(new VoucherSpacing(2, "None", false));
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<VoucherSpacing> getList()
        {
            return col;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public Int32? getDefault()
        {
            // We know that there is a default item as we set it. So, just find it.
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}