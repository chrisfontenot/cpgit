﻿using System;

namespace DebtPlus.LINQ.InMemory
{
    /// <summary>
    /// Type of the dateRange account
    /// </summary>
    public class dateRangeType : System.IComparable
    {
        /// <summary>
        /// Code list for the various items if needed
        /// </summary>
        public enum DateItems
        {
            DateItem_All               = 0,
            DateItem_Today             = 1,
            DateItem_Tomorrow          = 2,
            DateItem_Yesterday         = 3,
            DateItem_ThisWeek          = 4,
            DateItme_LastWeek          = 5,
            DateItem_QuarterToDate     = 6,
            DateItem_LastQuarter       = 7,
            DateItem_AllToDate         = 8,
            DateItem_CurrentMonth      = 9,
            DateItem_CurrentYear       = 10,
            DateItem_MonthToDate       = 11,
            DateItem_YearToDate        = 12,
            DateItem_Last30Days        = 13,
            DateItem_Last3Months       = 14,
            DateItem_Last6Months       = 15,
            DateItem_Last12Months      = 16,
            DateItem_PreviousMonth     = 17,
            DateItem_PreviousYear      = 18,
            DateItem_SpecificDateRange = 19
        };

        /// <summary>
        /// Primary key to the record.
        /// </summary>
        public Int32 Id                  { get; set; }

        /// <summary>
        /// Description for the record type
        /// </summary>
        public System.String description { get; set; }

        /// <summary>
        /// True if the item is the default value
        /// </summary>
        public Boolean Default           { get; set; }

        /// <summary>
        /// True if the item is still active
        /// </summary>
        public Boolean ActiveFlag        { get; set; }

        /// <summary>
        /// Initialize a new context
        /// </summary>
        public dateRangeType()
        {
            ActiveFlag = true;
        }

        /// <summary>
        /// Starting date for the date range
        /// </summary>
        public DateTime? startingDate
        {
            get
            {
                return getStartingValue((DateItems) Id);
            }
        }

        /// <summary>
        /// Is the item variable (Do we ask the user for the date range?)
        /// </summary>
        public bool isVariable
        {
            get
            {
                return (DateItems)Id == DateItems.DateItem_SpecificDateRange;
            }
        }

        /// <summary>
        /// Ending date for the date range.
        /// </summary>
        public DateTime? endingDate
        {
            get
            {
                return getEndingDate((DateItems)Id);
            }
        }

        /// <summary>
        /// Look at the type and determine the starting date value
        /// </summary>
        private DateTime? getStartingValue(DateItems idValue)
        {
            switch (idValue)
            {
                case DateItems.DateItem_All:
                case DateItems.DateItem_AllToDate:
                case DateItems.DateItem_SpecificDateRange:
                    return null;

                case DateItems.DateItem_Today:
                    return System.DateTime.Now.Date;

                case DateItems.DateItem_Tomorrow:
                    return getStartingValue(DateItems.DateItem_Today).Value.AddDays(1);

                case DateItems.DateItem_Yesterday:
                    return getStartingValue(DateItems.DateItem_Today).Value.AddDays(-1);

                case DateItems.DateItem_ThisWeek:
                    {
                        var now = DateTime.Now.Date;
                        return now.AddDays(0 - (int)now.DayOfWeek);
                    }

                case DateItems.DateItme_LastWeek:
                    return getStartingValue(DateItems.DateItem_ThisWeek).Value.AddDays(-7);

                case DateItems.DateItem_QuarterToDate:
                    {
                        var ThisMonth = DateTime.Now.Date.AddMonths(-4);
                        ThisMonth = new DateTime(System.DateTime.Now.Year, System.DateTime.Now.Month, 1);
                        switch(ThisMonth.Month)
                        {
                            case 1: case 4: case 7: case 10:
                                break;

                            case 2: case 5: case 8: case 11:
                                ThisMonth = ThisMonth.AddMonths(-1);
                                break;

                            case 3: case 6: case 9: case 12:
                                ThisMonth = ThisMonth.AddMonths(-3);
                                break;
                        }
                        return ThisMonth;
                    }

                case DateItems.DateItem_LastQuarter:
                    return getStartingValue(DateItems.DateItem_QuarterToDate).Value.AddMonths(-3);

                case DateItems.DateItem_MonthToDate:
                case DateItems.DateItem_CurrentMonth:
                    {
                        var now = DateTime.Now.Date;
                        return new DateTime(now.Year, now.Month, 1);
                    }

                case DateItems.DateItem_YearToDate:
                case DateItems.DateItem_CurrentYear:
                    {
                        var now = DateTime.Now.Date;
                        return new DateTime(now.Year, 1, 1);
                    }

                case DateItems.DateItem_Last30Days:
                    return DateTime.Now.Date.AddDays(-30);

                case DateItems.DateItem_Last3Months:
                    return DateTime.Now.Date.AddMonths(-2);

                case DateItems.DateItem_Last6Months:
                    return DateTime.Now.Date.AddMonths(-5);

                case DateItems.DateItem_PreviousMonth:
                    return getStartingValue(DateItems.DateItem_CurrentMonth).Value.AddMonths(-1);

                case DateItems.DateItem_PreviousYear:
                    {
                        var now = DateTime.Now.Date.AddYears(-1);
                        return new DateTime(now.Year, 1, 1);
                    }

                case DateItems.DateItem_Last12Months:
                    return DateTime.Now.Date.AddMonths(-11).Date;
            }

            return null;
        }

        /// <summary>
        /// Look at the type and determine the starting date value
        /// </summary>
        private DateTime? getEndingDate(DateItems IdValue)
        {
            switch (IdValue)
            {
                case DateItems.DateItem_AllToDate:
                case DateItems.DateItem_MonthToDate:
                case DateItems.DateItem_YearToDate:
                case DateItems.DateItem_Last30Days:
                case DateItems.DateItem_Last3Months:
                case DateItems.DateItem_Last6Months:
                case DateItems.DateItem_Last12Months:
                case DateItems.DateItem_CurrentMonth:
                case DateItems.DateItem_QuarterToDate:
                    return System.DateTime.Now.Date;

                case DateItems.DateItem_PreviousMonth:
                    return getStartingValue(IdValue).Value.AddMonths(1).AddDays(-1);

                case DateItems.DateItem_PreviousYear:
                case DateItems.DateItem_CurrentYear:
                    return getStartingValue(IdValue).Value.AddYears(1).AddDays(-1);

                case DateItems.DateItem_SpecificDateRange:
                case DateItems.DateItem_All:
                    return null;

                case DateItems.DateItem_Today:
                case DateItems.DateItem_Tomorrow:
                case DateItems.DateItem_Yesterday:
                    return getStartingValue(IdValue).Value;

                case DateItems.DateItem_ThisWeek:
                case DateItems.DateItme_LastWeek:
                    return getStartingValue(IdValue).Value.AddDays(6);

                case DateItems.DateItem_LastQuarter:
                    return getStartingValue(DateItems.DateItem_QuarterToDate).Value.AddDays(-1);
            }

            return null;
        }

        /// <summary>
        /// Initialize a new context
        /// </summary>
        public dateRangeType(Int32 Id, string description, Boolean Default)
            : this()
        {
            this.Id           = Id;
            this.description  = description;
            this.Default      = Default;
        }

        /// <summary>
        /// Convert the item to a displayable string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return description ?? string.Empty;
        }

        /// <summary>
        /// Compare the value for sorting
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            return ToString().CompareTo((obj as dateRangeType).ToString());
        }
    }

    /// <summary>
    /// This list is used for items that reflect the dateRange type
    /// </summary>
    public static class dateRangeTypes
    {
        private static System.Collections.Generic.List<dateRangeType> coldateRangeTypes;
        static dateRangeTypes()
        {
            coldateRangeTypes = new System.Collections.Generic.List<dateRangeType>();

            coldateRangeTypes.Add(new dateRangeType((int) dateRangeType.DateItems.DateItem_All, "All Items", false));
            coldateRangeTypes.Add(new dateRangeType((int) dateRangeType.DateItems.DateItem_AllToDate, "All To Date", false));
            coldateRangeTypes.Add(new dateRangeType((int) dateRangeType.DateItems.DateItem_CurrentMonth, "Current Month", false));
            coldateRangeTypes.Add(new dateRangeType((int) dateRangeType.DateItems.DateItem_CurrentYear, "Current Year", false));
            coldateRangeTypes.Add(new dateRangeType((int) dateRangeType.DateItems.DateItem_Last12Months, "Last 12 Months", false));
            coldateRangeTypes.Add(new dateRangeType((int) dateRangeType.DateItems.DateItem_Last30Days, "Last 30 Days", false));
            coldateRangeTypes.Add(new dateRangeType((int) dateRangeType.DateItems.DateItem_Last3Months, "Last 3 Months", true));
            coldateRangeTypes.Add(new dateRangeType((int) dateRangeType.DateItems.DateItem_Last6Months, "Last 6 Months", false));
            coldateRangeTypes.Add(new dateRangeType((int) dateRangeType.DateItems.DateItem_LastQuarter, "Last Quarter", false));
            coldateRangeTypes.Add(new dateRangeType((int) dateRangeType.DateItems.DateItem_MonthToDate, "Month To Date", false));
            coldateRangeTypes.Add(new dateRangeType((int) dateRangeType.DateItems.DateItem_PreviousMonth, "Previous Month", false));
            coldateRangeTypes.Add(new dateRangeType((int) dateRangeType.DateItems.DateItem_PreviousYear, "Previous Year", false));
            coldateRangeTypes.Add(new dateRangeType((int) dateRangeType.DateItems.DateItem_QuarterToDate, "Quarter To Date", false));
            coldateRangeTypes.Add(new dateRangeType((int) dateRangeType.DateItems.DateItem_SpecificDateRange, "Specific Date Range...", false));
            coldateRangeTypes.Add(new dateRangeType((int) dateRangeType.DateItems.DateItem_ThisWeek, "This Week", false));
            coldateRangeTypes.Add(new dateRangeType((int) dateRangeType.DateItems.DateItem_Today, "Today", false));
            coldateRangeTypes.Add(new dateRangeType((int) dateRangeType.DateItems.DateItem_Tomorrow, "Tomorrow", false));
            coldateRangeTypes.Add(new dateRangeType((int) dateRangeType.DateItems.DateItem_YearToDate, "Year To Date", false));
            coldateRangeTypes.Add(new dateRangeType((int) dateRangeType.DateItems.DateItem_YearToDate, "Year To Date", false));
            coldateRangeTypes.Add(new dateRangeType((int) dateRangeType.DateItems.DateItem_Yesterday, "Yesterday", false));
            coldateRangeTypes.Add(new dateRangeType((int) dateRangeType.DateItems.DateItme_LastWeek, "Last Week", false));
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<dateRangeType> getList()
        {
            return coldateRangeTypes;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public System.Nullable<Int32> getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}