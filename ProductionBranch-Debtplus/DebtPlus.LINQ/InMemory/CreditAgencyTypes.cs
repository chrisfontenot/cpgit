﻿using System;

namespace DebtPlus.LINQ.InMemory
{
    /// <summary>
    /// The collection if items in the list are of this type.
    /// </summary>
    public class CreditAgencyType
    {
        public string Id { get; set; }

        public string description { get; set; }

        public Boolean @default { get; set; }

        public Boolean ActiveFlag { get; set; }

        public CreditAgencyType()
        {
        }

        internal CreditAgencyType(string Id, string description, Boolean @default, Boolean ActiveFlag)
        {
            this.Id = Id;
            this.description = description;
            this.@default = @default;
            this.ActiveFlag = ActiveFlag;
        }
    }

    /// <summary>
    /// This list is used for items that are Yes/No/Ask status such as client's ElectronicStatements field.
    /// </summary>
    public static class CreditAgencyTypes
    {
        private static System.Collections.Generic.List<CreditAgencyType> col;

        /// <summary>
        /// Initialize the new class structure
        /// </summary>
        static CreditAgencyTypes()
        {
            col = new System.Collections.Generic.List<CreditAgencyType>();
            col.Add(new CreditAgencyType("EXPERIAN", "Experian", false, true));
            col.Add(new CreditAgencyType("TRANSUNION", "TransUnion", false, true));
            col.Add(new CreditAgencyType("EQUIFAX", "EquiFax", true, true));
            col.Add(new CreditAgencyType("TRI-MERGE", "Tri-Merge", false, true));
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<CreditAgencyType> getList()
        {
            return col;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public string getDefault()
        {
            // We know that there is a default item as we set it. So, just find it.
            return getList().Find(l => l.@default == true).Id;
        }
    }
}