﻿using System;

namespace DebtPlus.LINQ.InMemory
{
    /// <summary>
    /// Type of the counselor billing mode
    /// </summary>
    public class counselorBillingType
    {
        public System.String Id          { get; set; }
        public System.String description { get; set; }
        public Boolean Default           { get; set; }
        public Boolean ActiveFlag        { get; set; }

        public counselorBillingType()
        {
            ActiveFlag = true;
        }

        public counselorBillingType(string Id, string description, Boolean Default)
            : this()
        {
            this.Id          = Id;
            this.description = description;
            this.Default     = Default;
        }
    }

    /// <summary>
    /// This list is used for items that reflect the counselorBilling type
    /// </summary>
    public static class counselorBillingTypes
    {
        private static System.Collections.Generic.List<counselorBillingType> colcounselorBillingTypes;
        static counselorBillingTypes()
        {
            colcounselorBillingTypes = new System.Collections.Generic.List<counselorBillingType>();

            colcounselorBillingTypes.Add(new counselorBillingType("Hourly", "Hourly", false));
            colcounselorBillingTypes.Add(new counselorBillingType("Fixed", "Fixed", true));
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<counselorBillingType> getList()
        {
            return colcounselorBillingTypes;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public string getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}