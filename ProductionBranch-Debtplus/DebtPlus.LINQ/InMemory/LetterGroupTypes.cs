﻿using System;
using System.Linq;

namespace DebtPlus.LINQ.InMemory
{
    /// <summary>
    /// The collection if items in the list are of this type.
    /// </summary>
    public class LetterGroupType
    {
        public string Id { get; set; }
        public bool Default { get; set; }
        public string description { get; set; }

        public LetterGroupType()
        {
        }

        internal LetterGroupType(string Id, string description, bool Default)
        {
            this.Id = Id;
            this.description = description;
            this.Default = Default;
        }
    }

    /// <summary>
    /// This list is used for items that are Yes/No/Ask status such as client's ElectronicStatements field.
    /// </summary>
    public static class LetterGroupTypes
    {
        private static System.Collections.Generic.List<LetterGroupType> col;

        /// <summary>
        /// Initialize the new class structure
        /// </summary>
        static LetterGroupTypes()
        {
            col = new System.Collections.Generic.List<LetterGroupType>();
            col.Add(new LetterGroupType("OTHER", "Other", true));
            col.Add(new LetterGroupType("CL", "Client", false));
            col.Add(new LetterGroupType("CR", "Creditor", false));
            col.Add(new LetterGroupType("DEBT", "Debt", false));
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<LetterGroupType> getList()
        {
            return col;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public string getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}