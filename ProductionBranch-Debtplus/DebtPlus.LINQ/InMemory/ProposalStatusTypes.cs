﻿using System;

namespace DebtPlus.LINQ.InMemory
{
    /// <summary>
    /// Type of the counselor billing mode
    /// </summary>
    public class ProposalStatusType
    {
        public Int32 Id                  { get; set; }
        public System.String description { get; set; }
        public Boolean Default           { get; set; }
        public Boolean ActiveFlag        { get; set; }

        public ProposalStatusType()
        {
            ActiveFlag = true;
        }

        public ProposalStatusType(Int32 Id, string description, Boolean Default)
            : this()
        {
            this.Id          = Id;
            this.description = description;
            this.Default     = Default;
        }
    }

    /// <summary>
    /// This list is used for items that reflect the ProposalStatus type
    /// </summary>
    public static class ProposalStatusTypes
    {
        private static System.Collections.Generic.List<ProposalStatusType> colProposalStatusTypes;
        static ProposalStatusTypes()
        {
            colProposalStatusTypes = new System.Collections.Generic.List<ProposalStatusType>();
            colProposalStatusTypes.Add(new ProposalStatusType(0, "No Proposals", true));
            colProposalStatusTypes.Add(new ProposalStatusType(1, "Standard", false));
            colProposalStatusTypes.Add(new ProposalStatusType(2, "Full Disclosure", false));
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<ProposalStatusType> getList()
        {
            return colProposalStatusTypes;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public Int32? getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}