﻿using System;
using System.Linq;

namespace DebtPlus.LINQ.InMemory
{
    /// <summary>
    /// The collection if items in the list are of this type.
    /// </summary>
    public class EscrowProBonoType
    {
        public Int32 Id { get; set; }
        public string description { get; set; }
        public bool Default { get; set; }
        public bool ActiveFlag { get; set; }

        public EscrowProBonoType()
        {
            ActiveFlag = true;
            Default = false;
        }

        internal EscrowProBonoType(Int32 Id, string description, Boolean Default) : this()
        {
            this.Id = Id;
            this.description = description;
            this.Default = Default;
        }
    }

    /// <summary>
    /// This list is used for items that are Yes/No/Ask status such as client's ElectronicStatements field.
    /// </summary>
    public static class EscrowProBonoTypes
    {
        private static System.Collections.Generic.List<EscrowProBonoType> col;

        /// <summary>
        /// Initialize the new class structure
        /// </summary>
        static EscrowProBonoTypes()
        {
            col = new System.Collections.Generic.List<EscrowProBonoType>();
            col.Add(new EscrowProBonoType(0, "non-Escrow", true));
            col.Add(new EscrowProBonoType(1, "Escrow", false));
            col.Add(new EscrowProBonoType(2, "pro Bono", false));
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<EscrowProBonoType> getList()
        {
            return col;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public System.Nullable<System.Int32> getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}