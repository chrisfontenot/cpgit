﻿using System;

namespace DebtPlus.LINQ.InMemory
{
    /// <summary>
    /// The collection if items in the list are of this type.
    /// </summary>
    public class TristateType
    {
        public Int32 Id { get; set; }

        public string description { get; set; }

        public Boolean @default { get; set; }

        public TristateType()
        {
        }

        internal TristateType(Int32 Id, string description, Boolean @default)
        {
            this.Id = Id;
            this.description = description;
            this.@default = @default;
        }
    }

    /// <summary>
    /// This list is used for items that are Yes/No/Ask status such as client's ElectronicStatements field.
    /// </summary>
    public static class TristateTypes
    {
        private static System.Collections.Generic.List<TristateType> col;

        /// <summary>
        /// Initialize the new class structure
        /// </summary>
        static TristateTypes()
        {
            col = new System.Collections.Generic.List<TristateType>();
            col.Add(new TristateType(2, "Ask", true));
            col.Add(new TristateType(0, "No", false));
            col.Add(new TristateType(1, "Yes", false));
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<TristateType> getList()
        {
            return col;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public Int32 getDefault()
        {
            // We know that there is a default item as we set it. So, just find it.
            return getList().Find(l => l.@default == true).Id;
        }
    }
}