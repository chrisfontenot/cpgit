﻿using System;
using System.Linq;

namespace DebtPlus.LINQ.InMemory
{
    /// <summary>
    /// The collection if items in the list are of this type.
    /// </summary>
    public class LetterFieldType
    {
        public Int32 Id { get; set; }

        public string description { get; set; }

        public bool need_table { get; set; }

        public LetterFieldType()
        {
        }

        internal LetterFieldType(Int32 Id, string description, Boolean need_table)
        {
            this.Id = Id;
            this.description = description;
            this.need_table = need_table;
        }
    }

    /// <summary>
    /// This list is used for items that are Yes/No/Ask status such as client's ElectronicStatements field.
    /// </summary>
    public static class LetterFieldTypes
    {
        private static System.Collections.Generic.List<LetterFieldType> col;

        /// <summary>
        /// Initialize the new class structure
        /// </summary>
        static LetterFieldTypes()
        {
            col = new System.Collections.Generic.List<LetterFieldType>();
            col.Add(new LetterFieldType(1, "Normal", true));
            col.Add(new LetterFieldType(2, "letter date", false));
            col.Add(new LetterFieldType(3, "client", false));
            col.Add(new LetterFieldType(4, "creditor", false));
            col.Add(new LetterFieldType(5, "debt", false));
            col.Add(new LetterFieldType(6, "amount", false));
            col.Add(new LetterFieldType(7, "followup date", false));
            col.Add(new LetterFieldType(8, "name", false));
            col.Add(new LetterFieldType(9, "address", false));
            col.Add(new LetterFieldType(10, "callback", false));
            col.Add(new LetterFieldType(11, "proc items", true));
            col.Add(new LetterFieldType(12, "proc text", true));
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<LetterFieldType> getList()
        {
            return col;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public Int32 getDefault()
        {
            // Just take the first item in the list
            return getList().Min(s => s.Id);
        }
    }
}