﻿using System;

namespace DebtPlus.LINQ.InMemory
{
    public class officeType
    {
        public System.Int32 Id           { get; set; }
        public System.String description { get; set; }
        public Boolean Default           { get; set; }
        public Boolean ActiveFlag        { get; set; }

        public officeType()
        {
            ActiveFlag = true;
        }

        public officeType(Int32 Id, string description, Boolean Default)
            : this()
        {
            this.Id          = Id;
            this.description = description;
            this.Default     = Default;
        }
    }

    /// <summary>
    /// This list is used for items that reflect the RPPS proposal type
    /// </summary>
    public static class officeTypes
    {
        private static System.Collections.Generic.List<officeType> colOfficeTypes;
        static officeTypes()
        {
            colOfficeTypes = new System.Collections.Generic.List<officeType>();
            colOfficeTypes.Add(new officeType(1, "Main", false));
            colOfficeTypes.Add(new officeType(2, "Satellite", false));
            colOfficeTypes.Add(new officeType(3, "Desk", false));
            colOfficeTypes.Add(new officeType(4, "Other", true));
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<officeType> getList()
        {
            return colOfficeTypes;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public Int32? getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}