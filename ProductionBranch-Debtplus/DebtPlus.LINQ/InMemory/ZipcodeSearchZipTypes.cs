﻿using System;

namespace DebtPlus.LINQ.InMemory
{
    public class ZipcodeSearchZipType
    {
        public Char Id                   { get; set; }
        public System.String description { get; set; }
        public Boolean Default           { get; set; }
        public Boolean ActiveFlag        { get; set; }

        public ZipcodeSearchZipType()
        {
            ActiveFlag = true;
        }

        public ZipcodeSearchZipType(Char Id, string description, Boolean Default)
            : this()
        {
            this.Id          = Id;
            this.description = description;
            this.Default     = Default;
        }
    }

    /// <summary>
    /// This list is used for items that reflect the RPPS proposal type
    /// </summary>
    public static class ZipcodeSearchZipTypes
    {
        private static System.Collections.Generic.List<ZipcodeSearchZipType> colZipcodeSearchZipTypes;
        static ZipcodeSearchZipTypes()
        {
            colZipcodeSearchZipTypes = new System.Collections.Generic.List<ZipcodeSearchZipType>();
            colZipcodeSearchZipTypes.Add(new ZipcodeSearchZipType('M', "Military", false));
            colZipcodeSearchZipTypes.Add(new ZipcodeSearchZipType('P', "PO BOX Only", false));
            colZipcodeSearchZipTypes.Add(new ZipcodeSearchZipType('S', "Standard", true));
            colZipcodeSearchZipTypes.Add(new ZipcodeSearchZipType('U', "Un-incorporated", false));
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<ZipcodeSearchZipType> getList()
        {
            return colZipcodeSearchZipTypes;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public Char getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return 'S';
        }
    }
}