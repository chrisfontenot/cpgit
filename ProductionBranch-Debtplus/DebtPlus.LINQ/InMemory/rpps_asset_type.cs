﻿using System;

namespace DebtPlus.LINQ.InMemory
{
    /// <summary>
    /// The collection if items in the list are of this type.
    /// </summary>
    public class rpps_asset_type
    {
        public Int32 Id { get; set; }
        public string description { get; set; }
        public Boolean Default { get; set; }
        public Boolean ActiveFlag { get; set; }

        public rpps_asset_type()
        {
            Default = false;
            ActiveFlag = true;
            Id = default(Int32);
            description = string.Empty;
        }

        internal rpps_asset_type(Int32 Id, string description, Boolean ActiveFlag, Boolean Default)
        {
            this.Id          = Id;
            this.description = description;
            this.Default     = Default;
            this.ActiveFlag  = ActiveFlag;
        }
    }

    /// <summary>
    /// This list is used for items that reflect the RPPS proposal type
    /// </summary>
    public static class rpps_asset_types
    {
        private static System.Collections.Generic.List<rpps_asset_type> col;

        /// <summary>
        /// Initialize the new class structure
        /// </summary>
        static rpps_asset_types()
        {
            col = new System.Collections.Generic.List<rpps_asset_type>();

            // Populate the collection once it has been created
            col.Add(new rpps_asset_type(1, "Applicant Employment", true, false));
            col.Add(new rpps_asset_type(2, "Co-Applicant Employment", true, false));
            col.Add(new rpps_asset_type(3, "Child Support", true, false));
            col.Add(new rpps_asset_type(4, "Social Security/Disability", true, false));
            col.Add(new rpps_asset_type(5, "Pension/Retirement", true, false));
            col.Add(new rpps_asset_type(6, "Other Income", true, true));
            col.Add(new rpps_asset_type(7, "Other Government Support", true, false));
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<rpps_asset_type> getList()
        {
            return col;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public System.Nullable<Int32> getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}