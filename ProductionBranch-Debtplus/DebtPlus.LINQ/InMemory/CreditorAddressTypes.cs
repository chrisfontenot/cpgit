﻿using System;

namespace DebtPlus.LINQ.InMemory
{
    public class creditorAddressType
    {
        public System.String Id          { get; set; }
        public System.String description { get; set; }
        public Boolean Default           { get; set; }
        public Boolean ActiveFlag        { get; set; }

        public creditorAddressType()
        {
            ActiveFlag = true;
        }

        public creditorAddressType(string Id, string description, Boolean Default)
            : this()
        {
            this.Id          = Id;
            this.description = description;
            this.Default     = Default;
        }
    }

    /// <summary>
    /// This list is used for items that reflect the RPPS proposal type
    /// </summary>
    public static class creditorAddressTypes
    {
        // Key for the payment address entry. It is not necessarily the "default" item.
        // We need it for the address "copy payment" operation.
        public static readonly string PaymentTypeKey = "P";

        private static System.Collections.Generic.List<creditorAddressType> colcreditorAddressTypes;
        static creditorAddressTypes()
        {
            colcreditorAddressTypes = new System.Collections.Generic.List<creditorAddressType>();
            colcreditorAddressTypes.Add(new creditorAddressType("P", "Payment",         true));
            colcreditorAddressTypes.Add(new creditorAddressType("I", "Invoice",         false));
            colcreditorAddressTypes.Add(new creditorAddressType("L", "Proposal/Letter", false));
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<creditorAddressType> getList()
        {
            return colcreditorAddressTypes;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public string getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}