﻿using System;
using System.Linq;

namespace DebtPlus.LINQ.InMemory
{
    /// <summary>
    /// The collection if items in the list are of this type.
    /// </summary>
    public class ACHAccountType
    {
        public char Id { get; set; }

        public string description { get; set; }

        public Boolean @default { get; set; }

        public ACHAccountType()
        {
        }

        internal ACHAccountType(char Id, string description, Boolean @default)
        {
            this.Id = Id;
            this.description = description;
            this.@default = @default;
        }
    }

    /// <summary>
    /// This list is used for items that are Yes/No/Ask status such as client's ElectronicStatements field.
    /// </summary>
    public static class ACHAccountTypes
    {
        private static System.Collections.Generic.List<ACHAccountType> col;

        /// <summary>
        /// Initialize the new class structure
        /// </summary>
        static ACHAccountTypes()
        {
            col = new System.Collections.Generic.List<ACHAccountType>();
            col.Add(new ACHAccountType('C', "Checking", true));
            col.Add(new ACHAccountType('S', "Savings", false));
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<ACHAccountType> getList()
        {
            return col;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public char? getDefault()
        {
            // We know that there is a default item as we set it. So, just find it.
            var q = getList().Find(s => s.@default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}