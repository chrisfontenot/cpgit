﻿using System;

namespace DebtPlus.LINQ.InMemory
{
    /// <summary>
    /// Type of the contribCycle account
    /// </summary>
    public class contribCycleType
    {
        public char Id                   { get; set; }
        public System.String description { get; set; }
        public Boolean Default           { get; set; }
        public Boolean ActiveFlag        { get; set; }
        public char Status               { get; set; }
        public char Cycle                { get; set; }

        public contribCycleType()
        {
            ActiveFlag = true;
        }

        public contribCycleType(char Id, string description, Boolean Default, char Status, char Cycle)
            : this()
        {
            this.Id          = Id;
            this.description = description;
            this.Default     = Default;
            this.Status      = Status;
            this.Cycle       = Cycle;
        }
    }

    /// <summary>
    /// This list is used for items that reflect the contribCycle type
    /// </summary>
    public static class contribCycleTypes
    {
        private static System.Collections.Generic.List<contribCycleType> colContribCycleTypes;
        static contribCycleTypes()
        {
            colContribCycleTypes = new System.Collections.Generic.List<contribCycleType>();

            colContribCycleTypes.Add(new contribCycleType('B', "Bill",          false, 'B', ' '));
            colContribCycleTypes.Add(new contribCycleType('D', "Deduct",        false, 'D', ' '));
            colContribCycleTypes.Add(new contribCycleType('I', "Immediate",     false, 'B', 'I'));
            colContribCycleTypes.Add(new contribCycleType('N', "None",          true,  'N', ' '));
            colContribCycleTypes.Add(new contribCycleType('M', "Monthly",       false, 'B', 'M'));
            colContribCycleTypes.Add(new contribCycleType('Q', "Quarterly",     false, 'B', 'Q'));
            colContribCycleTypes.Add(new contribCycleType('S', "Semi-Annually", false, 'B', 'S'));
            colContribCycleTypes.Add(new contribCycleType('A', "Annually",      false, 'B', 'A'));
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<contribCycleType> getList()
        {
            return colContribCycleTypes;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public char? getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}