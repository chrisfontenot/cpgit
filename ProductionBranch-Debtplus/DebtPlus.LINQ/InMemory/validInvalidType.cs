﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DebtPlus.LINQ.InMemory
{
    public class validInvalidType
    {
        public System.Int32 Id { get; set; }
        public System.String description { get; set; }
        public Boolean Default { get; set; }
        public Boolean ActiveFlag { get; set; }
        public Boolean IsOther { get; set; }

        public validInvalidType()
        {
            ActiveFlag = true;
        }

        public validInvalidType(Int32 Id, string description, Boolean Default, Boolean IsOther)
            : this()
        {
            this.Id = Id;
            this.description = description;
            this.Default = Default;
            this.IsOther = IsOther;
        }
    }

    public static class validInvalidTypes
    {
        private static System.Collections.Generic.List<validInvalidType> colItems;
        static validInvalidTypes()
        {
            colItems = new System.Collections.Generic.List<validInvalidType>();
            colItems.Add(new validInvalidType(0, "Not Valid", false, false));
            colItems.Add(new validInvalidType(1, "Valid", false, false));
            colItems.Add(new validInvalidType(2, "See Note*", false, true));
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<validInvalidType> getList()
        {
            return colItems;
        }

        /// <summary>
        /// Find the default item in the list
        /// </summary>
        /// <returns>The primary key to the list that corresponds to the default item</returns>
        public static System.Nullable<System.Int32> getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}
