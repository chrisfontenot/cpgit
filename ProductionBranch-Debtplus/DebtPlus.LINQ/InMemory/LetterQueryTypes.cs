﻿using System;
using System.Linq;

namespace DebtPlus.LINQ.InMemory
{
    /// <summary>
    /// The collection if items in the list are of this type.
    /// </summary>
    public class LetterQueryType
    {
        public Int32 Id { get; set; }
        public bool Default { get; set; }
        public string description { get; set; }

        public LetterQueryType()
        {
        }

        internal LetterQueryType(Int32 Id, string description, bool Default)
        {
            this.Id = Id;
            this.description = description;
            this.Default = Default;
        }
    }

    /// <summary>
    /// This list is used for items that are Yes/No/Ask status such as client's ElectronicStatements Query.
    /// </summary>
    public static class LetterQueryTypes
    {
        private static System.Collections.Generic.List<LetterQueryType> col;

        /// <summary>
        /// Initialize the new class structure
        /// </summary>
        static LetterQueryTypes()
        {
            col = new System.Collections.Generic.List<LetterQueryType>();
            col.Add(new LetterQueryType(1, "Text", true));
            col.Add(new LetterQueryType(2, "Function", false));
            col.Add(new LetterQueryType(4, "Stored Procedure", false));
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<LetterQueryType> getList()
        {
            return col;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public System.Nullable<Int32> getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}