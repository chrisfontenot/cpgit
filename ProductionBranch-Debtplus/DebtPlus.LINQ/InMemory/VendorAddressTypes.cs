using System;

namespace DebtPlus.LINQ.InMemory
{
    public class vendorAddressType
    {
        public Char Id { get; set; }
        public System.String description { get; set; }
        public Boolean Default { get; set; }
        public Boolean ActiveFlag { get; set; }

        public vendorAddressType()
        {
            ActiveFlag = true;
        }

        public vendorAddressType(Char Id, string description, Boolean Default)
            : this()
        {
            this.Id = Id;
            this.description = description;
            this.Default = Default;
        }
    }

    /// <summary>
    /// This list is used for items that reflect the RPPS proposal type
    /// </summary>
    public static class vendorAddressTypes
    {
        // Key for the payment address entry. It is not necessarily the "default" item.
        // We need it for the address "copy payment" operation.
        public static readonly string PaymentTypeKey = "P";

        private static System.Collections.Generic.List<vendorAddressType> colvendorAddressTypes;
        static vendorAddressTypes()
        {
            colvendorAddressTypes = new System.Collections.Generic.List<vendorAddressType>();
            colvendorAddressTypes.Add(new vendorAddressType(DebtPlus.LINQ.vendor_address.AddressType_Dispute, "Dispute", false));
            colvendorAddressTypes.Add(new vendorAddressType(DebtPlus.LINQ.vendor_address.AddressType_Invoice, "Invoice", false));
            colvendorAddressTypes.Add(new vendorAddressType(DebtPlus.LINQ.vendor_address.AddressType_General, "General", true));
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<vendorAddressType> getList()
        {
            return colvendorAddressTypes;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public char? getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}