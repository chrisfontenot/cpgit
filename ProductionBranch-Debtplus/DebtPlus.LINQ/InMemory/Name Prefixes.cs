﻿using System;

namespace DebtPlus.LINQ.InMemory
{
    /// <summary>
    /// Type of the Prefix account
    /// </summary>
    public class NamePrefix : System.IComparable
    {
        public System.String Id { get; set; }
        public System.String description { get; set; }
        public Boolean Default { get; set; }
        public Boolean ActiveFlag { get; set; }

        public NamePrefix()
        {
            ActiveFlag = true;
        }

        public NamePrefix(string Id, string description, Boolean Default)
            : this()
        {
            this.Id = Id;
            this.description = description;
            this.Default = Default;
        }

        public override string ToString()
        {
            return description;
        }

        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                return 1;
            }

            return description.CompareTo((obj as NamePrefix).description);
        }
    }

    /// <summary>
    /// This list is used for items that reflect the Prefix type
    /// </summary>
    public static class NamePrefixes
    {
        private static System.Collections.Generic.List<NamePrefix> colPrefixes;
        static NamePrefixes()
        {
            colPrefixes = new System.Collections.Generic.List<NamePrefix>();

            colPrefixes.Add(new NamePrefix("MR", "Mr.", false));
            colPrefixes.Add(new NamePrefix("MS", "Ms.", false));
            colPrefixes.Add(new NamePrefix("DR", "Dr.", true));
            colPrefixes.Add(new NamePrefix("SR", "Sr.", true));
            colPrefixes.Add(new NamePrefix("MRS", "Mrs.", true));
            colPrefixes.Add(new NamePrefix("MISS", "Miss", true));
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<NamePrefix> getList()
        {
            return colPrefixes;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public string getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}