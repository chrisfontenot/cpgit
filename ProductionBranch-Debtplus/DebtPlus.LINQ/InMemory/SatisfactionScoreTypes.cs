﻿using System;

namespace DebtPlus.LINQ.InMemory
{
    public class SatisfactionScoreType
    {
        public System.Int32 Id           { get; set; }
        public System.String description { get; set; }
        public Boolean Default           { get; set; }
        public Boolean ActiveFlag        { get; set; }

        public SatisfactionScoreType()
        {
            ActiveFlag = true;
        }

        public SatisfactionScoreType(Int32 Id, string description, Boolean Default)
            : this()
        {
            this.Id          = Id;
            this.description = description;
            this.Default     = Default;
        }
    }

    /// <summary>
    /// This list is used for items that reflect the RPPS proposal type
    /// </summary>
    public static class SatisfactionScoreTypes
    {
        private static System.Collections.Generic.List<SatisfactionScoreType> colSatisfactionScoreTypes;
        static SatisfactionScoreTypes()
        {
            colSatisfactionScoreTypes = new System.Collections.Generic.List<SatisfactionScoreType>();
            colSatisfactionScoreTypes.Add(new SatisfactionScoreType(-1, "Declined", false));
            colSatisfactionScoreTypes.Add(new SatisfactionScoreType(0,  "Unspecified", true));
            colSatisfactionScoreTypes.Add(new SatisfactionScoreType(1,  " 1 - Least", false));
            colSatisfactionScoreTypes.Add(new SatisfactionScoreType(2,  " 2", false));
            colSatisfactionScoreTypes.Add(new SatisfactionScoreType(3,  " 3", false));
            colSatisfactionScoreTypes.Add(new SatisfactionScoreType(4,  " 4", false));
            colSatisfactionScoreTypes.Add(new SatisfactionScoreType(5,  " 5", false));
            colSatisfactionScoreTypes.Add(new SatisfactionScoreType(6,  " 6", false));
            colSatisfactionScoreTypes.Add(new SatisfactionScoreType(7,  " 7", false));
            colSatisfactionScoreTypes.Add(new SatisfactionScoreType(8,  " 8", false));
            colSatisfactionScoreTypes.Add(new SatisfactionScoreType(9,  " 9", false));
            colSatisfactionScoreTypes.Add(new SatisfactionScoreType(10, "10 - Most", false));
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<SatisfactionScoreType> getList()
        {
            return colSatisfactionScoreTypes;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public Int32? getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}