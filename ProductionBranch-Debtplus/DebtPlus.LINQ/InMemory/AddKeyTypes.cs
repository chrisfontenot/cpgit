﻿using System;

namespace DebtPlus.LINQ.InMemory
{
    /// <summary>
    /// Type of the additional keys
    /// </summary>
    public class addkeyType
    {
        public System.String Id          { get; set; }
        public System.String description { get; set; }
        public Boolean Default           { get; set; }
        public Boolean ActiveFlag        { get; set; }

        public addkeyType()
        {
            ActiveFlag = true;
        }

        public addkeyType(string Id, string description, Boolean Default)
            : this()
        {
            this.Id          = Id;
            this.description = description;
            this.Default     = Default;
        }
    }

    /// <summary>
    /// This list is used for items that reflect the addkey type
    /// </summary>
    public static class addkeyTypes
    {
        private static System.Collections.Generic.List<addkeyType> colAddkeyTypes;
        static addkeyTypes()
        {
            colAddkeyTypes = new System.Collections.Generic.List<addkeyType>();

            colAddkeyTypes.Add(new addkeyType("A", "Alias",   true));
            colAddkeyTypes.Add(new addkeyType("P", "Prefix",  false));
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<addkeyType> getList()
        {
            return colAddkeyTypes;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public string getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}