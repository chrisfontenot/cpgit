﻿using System;

namespace DebtPlus.LINQ.InMemory
{
    public class action_itemType
    {
        public Int32 Id { get; private set; }
        public string description { get; private set; }
        public bool Default { get; private set; }

        public action_itemType()
        {
        }

        internal action_itemType(Int32 Id, string description, bool Default)
            : this()
        {
            this.Id = Id;
            this.description = description;
            this.Default = Default;
        }
    }

    /// <summary>
    /// This list is used for items that are Yes/No/Ask status such as client's ElectronicStatements field.
    /// </summary>
    public static class action_itemTypes
    {
        private static System.Collections.Generic.List<action_itemType> col;

        /// <summary>
        /// Initialize the new class structure
        /// </summary>
        static action_itemTypes()
        {
            col = new System.Collections.Generic.List<action_itemType>();
            col.Add(new action_itemType(1, "Things To Do", true));
            col.Add(new action_itemType(2, "Increase Income", false));
            col.Add(new action_itemType(3, "Reduce Expenses", false));
            col.Add(new action_itemType(4, "Required for Program", false));
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<action_itemType> getList()
        {
            return col;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public System.Nullable<Int32> getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}