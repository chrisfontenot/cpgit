﻿using System;

namespace DebtPlus.LINQ.InMemory
{
    /// <summary>
    /// Type of the bank account
    /// </summary>
    public class vendorBillingMode : System.IComparable
    {
        public System.String Id          { get; set; }
        public System.String description { get; set; }
        public Boolean Default           { get; set; }
        public Boolean ActiveFlag        { get; set; }

        // Strings known to the system. Others may be used, but these are the known items.
        public static readonly string TypeCreditCard = "CreditCard";
        public static readonly string TypeACH        = "ACH";
        public static readonly string TypeNone       = "None";

        public vendorBillingMode()
        {
            ActiveFlag = true;
        }

        public vendorBillingMode(string Id, string description, Boolean Default)
            : this()
        {
            this.Id          = Id;
            this.description = description;
            this.Default     = Default;
        }

        public override string ToString()
        {
            return description ?? string.Empty;
        }

        public int CompareTo(object obj)
        {
            return ToString().CompareTo((obj as bankType).ToString());
        }
    }

    /// <summary>
    /// This list is used for items that reflect the bank type
    /// </summary>
    public static class vendorBillingModes
    {
        private static System.Collections.Generic.List<vendorBillingMode> colvendorBillingModes;
        static vendorBillingModes()
        {
            colvendorBillingModes = new System.Collections.Generic.List<vendorBillingMode>();

            colvendorBillingModes.Add(new vendorBillingMode(vendorBillingMode.TypeCreditCard, "Credit Card", false));
            colvendorBillingModes.Add(new vendorBillingMode(vendorBillingMode.TypeACH,        "ACH",         false));
            colvendorBillingModes.Add(new vendorBillingMode(vendorBillingMode.TypeNone,       "None",        true));
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<vendorBillingMode> getList()
        {
            return colvendorBillingModes;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public string getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}