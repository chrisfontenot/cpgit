﻿using System;

namespace DebtPlus.LINQ.InMemory
{
    /// <summary>
    /// Type of the bank account
    /// </summary>
    public class bankType : System.IComparable
    {
        public System.String Id          { get; set; }
        public System.String description { get; set; }
        public Boolean Default           { get; set; }
        public Boolean ActiveFlag        { get; set; }

        public bankType()
        {
            ActiveFlag = true;
        }

        public bankType(string Id, string description, Boolean Default)
            : this()
        {
            this.Id          = Id;
            this.description = description;
            this.Default     = Default;
        }

        public override string ToString()
        {
            return description ?? string.Empty;
        }

        public int CompareTo(object obj)
        {
            return ToString().CompareTo((obj as bankType).ToString());
        }
    }

    /// <summary>
    /// This list is used for items that reflect the bank type
    /// </summary>
    public static class bankTypes
    {
        private static System.Collections.Generic.List<bankType> colBankTypes;
        static bankTypes()
        {
            colBankTypes = new System.Collections.Generic.List<bankType>();

            colBankTypes.Add(new bankType("A", "ACH",     false));
            colBankTypes.Add(new bankType("C", "Check",   false));
            colBankTypes.Add(new bankType("D", "Deposit", true));
            colBankTypes.Add(new bankType("R", "RPPS",    true));
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<bankType> getList()
        {
            return colBankTypes;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public string getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}