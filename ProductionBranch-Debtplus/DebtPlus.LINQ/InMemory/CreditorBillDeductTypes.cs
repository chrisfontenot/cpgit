﻿using System;
using System.Linq;

namespace DebtPlus.LINQ.InMemory
{
    /// <summary>
    /// The collection if items in the list are of this type.
    /// </summary>
    public class CreditorBillDeductType
    {
        /// <summary>
        /// ID for the record
        /// </summary>
        public Int32 Id { get; set; }

        /// <summary>
        /// Descriptive text for the record
        /// </summary>
        public string description { get; set; }

        /// <summary>
        /// Bill / Deduct / None designation
        /// </summary>
        public char bdn { get; set; }

        /// <summary>
        /// If billing, this is the billing cycle
        /// </summary>
        public char period { get; set; }

        /// <summary>
        /// Is this item the default item?
        /// </summary>
        public Boolean @default { get; set; }

        /// <summary>
        /// Is this item Active? Can it be used for new designations?
        /// </summary>
        public Boolean ActiveFlag { get; set; }

        public CreditorBillDeductType()
        {
        }

        internal CreditorBillDeductType(Int32 Id, char bdn, char period, string description, Boolean @default, Boolean ActiveFlag)
        {
            this.Id = Id;
            this.description = description;
            this.bdn = bdn;
            this.period = period;
            this.@default = @default;
            this.ActiveFlag = ActiveFlag;
        }

        /// <summary>
        /// Return the "new" designation for billing mode which is a combination of the older
        /// bill/deduct/none with the billing cycle information.
        /// </summary>
        public char combined
        {
            get
            {
                if (bdn != 'B')
                {
                    return bdn;
                }
                return period;
            }
        }
    }

    /// <summary>
    /// This list is used for items that are Yes/No/Ask status such as client's ElectronicStatements field.
    /// </summary>
    public static class CreditorBillDeductTypes
    {
        private static System.Collections.Generic.List<CreditorBillDeductType> col;

        /// <summary>
        /// Initialize the new class structure
        /// </summary>
        static CreditorBillDeductTypes()
        {
            col = new System.Collections.Generic.List<CreditorBillDeductType>();
            col.Add(new CreditorBillDeductType(1, 'N', 'N', "None", true, true));
            col.Add(new CreditorBillDeductType(2, 'D', 'D', "Deduct", false, true));
            col.Add(new CreditorBillDeductType(3, 'B', 'M', "Bill Monthly", false, true));
            col.Add(new CreditorBillDeductType(4, 'B', 'I', "Bill Immediately", false, true));
            col.Add(new CreditorBillDeductType(5, 'B', 'Q', "Bill Quarterly", false, true));
            col.Add(new CreditorBillDeductType(6, 'B', 'S', "Bill Semi-Annually", false, true));
            col.Add(new CreditorBillDeductType(7, 'B', 'A', "Bill Annually", false, true));
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<CreditorBillDeductType> getList()
        {
            return col.FindAll(s => (new Int32[] { 1, 2, 3 }).Contains(s.Id)).ToList();
        }

        static public System.Collections.Generic.List<CreditorBillDeductType> getAll()
        {
            return col;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public Int32 getDefault()
        {
            // We know that there is a default item as we set it. So, just find it.
            return getAll().Find(l => l.@default == true).Id;
        }
    }
}