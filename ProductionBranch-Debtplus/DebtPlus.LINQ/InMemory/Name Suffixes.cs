﻿using System;

namespace DebtPlus.LINQ.InMemory
{
    /// <summary>
    /// Type of the Suffix account
    /// </summary>
    public class NameSuffix : System.IComparable
    {
        public System.String Id          { get; set; }
        public System.String description { get; set; }
        public Boolean Default           { get; set; }
        public Boolean ActiveFlag        { get; set; }

        public NameSuffix()
        {
            ActiveFlag = true;
        }

        public NameSuffix(string Id, string description, Boolean Default)
            : this()
        {
            this.Id          = Id;
            this.description = description;
            this.Default     = Default;
        }

        public override string ToString()
        {
            return description;
        }

        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                return 1;
            }

            return description.CompareTo((obj as NameSuffix).description);
        }
    }

    /// <summary>
    /// This list is used for items that reflect the Suffix type
    /// </summary>
    public static class NameSuffixes
    {
        private static System.Collections.Generic.List<NameSuffix> colSuffixes;
        static NameSuffixes()
        {
            colSuffixes = new System.Collections.Generic.List<NameSuffix>();

            colSuffixes.Add(new NameSuffix("PHD", "Ph.D.", false));
            colSuffixes.Add(new NameSuffix("MD", "M.D.", false));
            colSuffixes.Add(new NameSuffix("JR", "Jr.", true));
            colSuffixes.Add(new NameSuffix("SR", "Sr.", true));
            colSuffixes.Add(new NameSuffix("II", "II", true));
            colSuffixes.Add(new NameSuffix("III", "III", true));
            colSuffixes.Add(new NameSuffix("IV", "IV", true));
            colSuffixes.Add(new NameSuffix("V", "V", true));
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<NameSuffix> getList()
        {
            return colSuffixes;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public string getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}