﻿using System;

namespace DebtPlus.LINQ.InMemory
{
    /// <summary>
    /// The collection if items in the list are of this type.
    /// </summary>
    public class retention_expire_type
    {
        public Int32 Id { get; set; }
        public string description { get; set; }
        public Boolean Default { get; set; }
        public Boolean ActiveFlag { get; set; }

        public retention_expire_type()
        {
            Default = false;
            ActiveFlag = true;
            Id = default(Int32);
            description = string.Empty;
        }

        internal retention_expire_type(Int32 Id, string description, Boolean ActiveFlag, Boolean Default)
        {
            this.Id          = Id;
            this.description = description;
            this.Default     = Default;
            this.ActiveFlag  = ActiveFlag;
        }
    }

    /// <summary>
    /// This list is used for items that reflect the RPPS proposal type
    /// </summary>
    public static class retention_expire_types
    {
        private static System.Collections.Generic.List<retention_expire_type> col;

        /// <summary>
        /// Initialize the new class structure
        /// </summary>
        static retention_expire_types()
        {
            col = new System.Collections.Generic.List<retention_expire_type>();

            // Populate the collection once it has been created
            col.Add(new retention_expire_type(1, "Until Cancelled", true, true));
            col.Add(new retention_expire_type(2, "On the next deposit", true, false));
            col.Add(new retention_expire_type(3, "Until the specified date value", true, false));
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<retention_expire_type> getList()
        {
            return col;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public System.Nullable<Int32> getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}