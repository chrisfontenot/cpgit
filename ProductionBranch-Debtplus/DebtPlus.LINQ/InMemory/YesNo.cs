﻿using System;

namespace DebtPlus.LINQ.InMemory
{
    /// <summary>
    /// The collection if items in the list are of this type.
    /// </summary>
    public class YesNo
    {
        /// <summary>
        /// Primary key to the table
        /// </summary>
        public Int32 Id { get; set; }

        /// <summary>
        /// Description associated with the entry
        /// </summary>
        public string description { get; set; }

        /// <summary>
        /// TRUE if the item is the default in the list.
        /// </summary>
        public Boolean @default { get; set; }

        /// <summary>
        /// Initialize the class definition
        /// </summary>
        public YesNo()
        {
        }

        /// <summary>
        /// Initialize the class definition
        /// </summary>
        internal YesNo(Int32 Id, string description, Boolean @default)
        {
            this.Id = Id;
            this.description = description;
            this.@default = @default;
        }

        /// <summary>
        /// Boolean ID for the item
        /// </summary>
        public Boolean boolId
        {
            get
            {
                return Id != 0;
            }
        }
    }

    /// <summary>
    /// This list is used for items that are Yes/No/Ask status such as client's ElectronicStatements field.
    /// </summary>
    public static class YesNos
    {
        private static System.Collections.Generic.List<YesNo> col;

        /// <summary>
        /// Initialize the new class structure
        /// </summary>
        static YesNos()
        {
            col = new System.Collections.Generic.List<YesNo>();
            col.Add(new YesNo(0, "No", false));
            col.Add(new YesNo(1, "Yes", false));
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<YesNo> getList()
        {
            return col;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public Int32? getDefault()
        {
            // There is no default.
            return null;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public Boolean? getBoolDefault()
        {
            // There is no default.
            return null;
        }
    }
}