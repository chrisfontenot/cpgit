﻿using System;

namespace DebtPlus.LINQ.InMemory
{
    public class DaysOfWeek
    {
        public System.Int32 Id           { get; set; }
        public System.String description { get; set; }
        public Boolean Default           { get; set; }
        public Boolean ActiveFlag        { get; set; }

        public DaysOfWeek()
        {
            ActiveFlag = true;
        }

        public DaysOfWeek(Int32 Id, string description, Boolean Default)
            : this()
        {
            this.Id          = Id;
            this.description = description;
            this.Default     = Default;
        }
    }

    /// <summary>
    /// This list is used for items that reflect the RPPS proposal type
    /// </summary>
    public static class DaysOfWeeks
    {
        private static System.Collections.Generic.List<DaysOfWeek> colDaysOfWeeks;
        static DaysOfWeeks()
        {
            colDaysOfWeeks = new System.Collections.Generic.List<DaysOfWeek>();
            Int32 dow = 1;
            foreach(var dowItem in new System.DayOfWeek[] {System.DayOfWeek.Sunday, System.DayOfWeek.Monday, System.DayOfWeek.Tuesday, System.DayOfWeek.Wednesday, System.DayOfWeek.Thursday, System.DayOfWeek.Friday, System.DayOfWeek.Saturday})
            {
                colDaysOfWeeks.Add(new DaysOfWeek(dow, System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.DayNames[(int) dowItem], false));
                dow += 1;
            }
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<DaysOfWeek> getList()
        {
            return colDaysOfWeeks;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public Int32? getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}