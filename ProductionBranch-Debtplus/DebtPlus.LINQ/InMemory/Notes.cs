﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ.InMemory
{
    public static class Notes
    {
        /// <summary>
        /// List of the types of a single note in most of the note tables. Some tables have only a subset of this
        /// list. The client notes do not include creditor nor disbursement notes, disbursement notes do not have alerts, etc.
        /// </summary>
        public enum NoteTypes
        {
            Permanent    = 1,       // Permanent text note of a general nature
            Temporary    = 2,       // Note that when it expires is deleted from the system
            System       = 3,       // System note used to track changes to the database
            Alert        = 4,       // Alert information that is shown before the main record
            Disbursement = 5,       // Disbursement related information.
            Research     = 6,       // General creditor research information. Rarely used.
            HUD          = 7        // Housing related information
        };

        /// <summary>
        /// Default note text for a new note. A new note is given this text to set the font information.
        /// We use RTF to set the default not as that allows the font to be specified. A simple blank string
        /// would generate the default font, and that is one that we don't want for notes.
        /// 
        /// Font: Times New Roman, 12 point, normal
        /// </summary>
        public static readonly string Default_Note = @"{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fnil\fcharset0 Times New Roman;}}\viewkind4\uc1\pard\slmult1\lang9\f0\fs24}";
    }
}
