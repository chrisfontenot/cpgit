﻿using System;

namespace DebtPlus.LINQ.InMemory
{
    /// <summary>
    /// Type of the contribCycle account
    /// </summary>
    public class housing_arm_hcs_id_AppointmentType
    {
        public string Id                 { get; set; }
        public System.String description { get; set; }
        public Boolean Default           { get; set; }
        public Boolean ActiveFlag        { get; set; }

        public housing_arm_hcs_id_AppointmentType()
        {
            ActiveFlag = true;
        }

        public housing_arm_hcs_id_AppointmentType(string Id, string description)
            : this(Id, description, false)
        {
        }

        public housing_arm_hcs_id_AppointmentType(string Id, string description, Boolean Default)
            : this()
        {
            this.Id = Id;
            this.description = description;
            this.Default     = Default;
        }
    }

    /// <summary>
    /// This list is used for items that reflect the contribCycle type
    /// </summary>
    public static class housing_arm_hcs_id_AppointmentTypes
    {
        private static System.Collections.Generic.List<housing_arm_hcs_id_AppointmentType> colhousing_arm_hcs_id_AppointmentTypes;
        static housing_arm_hcs_id_AppointmentTypes()
        {
            colhousing_arm_hcs_id_AppointmentTypes = new System.Collections.Generic.List<housing_arm_hcs_id_AppointmentType>();

            colhousing_arm_hcs_id_AppointmentTypes.Add(new housing_arm_hcs_id_AppointmentType("E", "E-Mail Counseling"));
            colhousing_arm_hcs_id_AppointmentTypes.Add(new housing_arm_hcs_id_AppointmentType("F", "Face-to-Face Counseling"));
            colhousing_arm_hcs_id_AppointmentTypes.Add(new housing_arm_hcs_id_AppointmentType("I", "Internet/WEB Counseling"));
            colhousing_arm_hcs_id_AppointmentTypes.Add(new housing_arm_hcs_id_AppointmentType("P", "Phone Counseling"));
            colhousing_arm_hcs_id_AppointmentTypes.Add(new housing_arm_hcs_id_AppointmentType("M", "Postal Mail Counseling"));
            colhousing_arm_hcs_id_AppointmentTypes.Add(new housing_arm_hcs_id_AppointmentType("1", "Group Counseling"));
            colhousing_arm_hcs_id_AppointmentTypes.Add(new housing_arm_hcs_id_AppointmentType("2", "Video Conference"));
            colhousing_arm_hcs_id_AppointmentTypes.Add(new housing_arm_hcs_id_AppointmentType("3", "Other Counseling"));
            colhousing_arm_hcs_id_AppointmentTypes.Add(new housing_arm_hcs_id_AppointmentType("4", "Phone Counseling Only"));
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<housing_arm_hcs_id_AppointmentType> getList()
        {
            return colhousing_arm_hcs_id_AppointmentTypes;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public string getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}