﻿using System;

namespace DebtPlus.LINQ.InMemory
{
    public class disclosure_applicantType
    {
        public System.Int32 Id           { get; set; }
        public System.String description { get; set; }
        public Boolean Default           { get; set; }
        public Boolean ActiveFlag        { get; set; }

        public disclosure_applicantType()
        {
            ActiveFlag = true;
        }

        public disclosure_applicantType(Int32 Id, string description, Boolean Default)
            : this()
        {
            this.Id          = Id;
            this.description = description;
            this.Default     = Default;
        }

        public disclosure_applicantType(Int32 Id, string description) : this(Id, description, false)
        {
        }
    }

    /// <summary>
    /// This list is used for items that reflect the RPPS proposal type
    /// </summary>
    public static class disclosure_applicantTypes
    {
        private static System.Collections.Generic.List<disclosure_applicantType> coldisclosure_applicantTypes;
        static disclosure_applicantTypes()
        {
            coldisclosure_applicantTypes = new System.Collections.Generic.List<disclosure_applicantType>();
            coldisclosure_applicantTypes.Add(new disclosure_applicantType(1, "No"));
            coldisclosure_applicantTypes.Add(new disclosure_applicantType(2, "Yes"));
            coldisclosure_applicantTypes.Add(new disclosure_applicantType(3, "N/A"));
            coldisclosure_applicantTypes.Add(new disclosure_applicantType(4, "UnAvail"));
            coldisclosure_applicantTypes.Add(new disclosure_applicantType(5, "Yes once"));
            coldisclosure_applicantTypes.Add(new disclosure_applicantType(6, "Yes ltd."));
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<disclosure_applicantType> getList()
        {
            return coldisclosure_applicantTypes;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public Int32? getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return null;
        }
    }
}