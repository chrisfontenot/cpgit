﻿using System;

namespace DebtPlus.LINQ.InMemory
{
    /// <summary>
    /// The collection if items in the list are of this type.
    /// </summary>
    public class rpps_client_type_indicator
    {
        public string Id { get; set; }

        public string description { get; set; }

        public double rate { get; set; }

        public Boolean enabled { get; set; }

        public Boolean Default { get; set; }

        public rpps_client_type_indicator()
        {
        }

        internal rpps_client_type_indicator(string Id, string description, double rate, Boolean enabled, Boolean Default)
        {
            this.Id = Id;
            this.description = description;
            this.rate = rate;
            this.enabled = enabled;
            this.Default = Default;
        }
    }

    /// <summary>
    /// This list is used for items that reflect the RPPS proposal type
    /// </summary>
    public static class rpps_client_type_indicators
    {
        private static System.Collections.Generic.List<rpps_client_type_indicator> col;

        /// <summary>
        /// Initialize the new class structure
        /// </summary>
        static rpps_client_type_indicators()
        {
            col = new System.Collections.Generic.List<rpps_client_type_indicator>();

            // Populate the collection once it has been created
            col.Add(new rpps_client_type_indicator(" ", "Standard Full Balance DMP", 1.0D, false, true));
            col.Add(new rpps_client_type_indicator("A", "Standard Less-Than-Full-Balance DMP", 0.0D, true, false));
            col.Add(new rpps_client_type_indicator("B", "Pre-Bankruptcy Full Balance DMP", 1.0D, false, false));
            col.Add(new rpps_client_type_indicator("C", "Pre-Bankruptcy Less-Than-Full-Balance DMP", 0.0D, true, false));
            col.Add(new rpps_client_type_indicator("D", "Call to Action Standard", 1.0D, false, false));
            col.Add(new rpps_client_type_indicator("E", "Call to Action Hardship", 1.0D, false, false));
            col.Add(new rpps_client_type_indicator("F", "CSP Tier 1 - 2.25%", 1.0D, false, false));
            col.Add(new rpps_client_type_indicator("G", "CSP Tier 2 - 2.00%", 1.0D, false, false));
            col.Add(new rpps_client_type_indicator("H", "CSP Tier 3 - 1.75%", 1.0D, false, false));
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<rpps_client_type_indicator> getList()
        {
            return col;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public string getDefault()
        {
            return " ";
        }
    }
}