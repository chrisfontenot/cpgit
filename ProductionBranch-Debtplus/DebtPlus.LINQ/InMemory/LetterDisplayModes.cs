﻿using System;

namespace DebtPlus.LINQ.InMemory
{
    /// <summary>
    /// Type of the letter display mode. This is what is done with the letter after it has been imaged.
    /// </summary>
    public class letterDisplayMode
    {
        public Int32 Id                  { get; set; }
        public System.String description { get; set; }
        public Boolean Default           { get; set; }
        public Boolean ActiveFlag        { get; set; }

        public letterDisplayMode()
        {
            ActiveFlag = true;
        }

        public letterDisplayMode(Int32 Id, string description, Boolean Default)
            : this()
        {
            this.Id          = Id;
            this.description = description;
            this.Default     = Default;
        }
    }

    /// <summary>
    /// This list is used for items that reflect the letter display modes
    /// </summary>
    public static class letterDisplayModes
    {
        private static System.Collections.Generic.List<letterDisplayMode> colletterDisplayModes;
        static letterDisplayModes()
        {
            colletterDisplayModes = new System.Collections.Generic.List<letterDisplayMode>();

            colletterDisplayModes.Add(new letterDisplayMode(0, "Default", true));
            colletterDisplayModes.Add(new letterDisplayMode(1, "Always print", false));
            colletterDisplayModes.Add(new letterDisplayMode(2, "Always view", false));
            colletterDisplayModes.Add(new letterDisplayMode(3, "Always queue", false));
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<letterDisplayMode> getList()
        {
            return colletterDisplayModes;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public System.Nullable<Int32> getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }

            return null;
        }
    }
}