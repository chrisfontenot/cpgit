﻿using System;

namespace DebtPlus.LINQ.InMemory
{
    public class ZipcodeSearchCityType
    {
        public Char Id                   { get; set; }
        public System.String description { get; set; }
        public Boolean Default           { get; set; }
        public Boolean ActiveFlag        { get; set; }

        public ZipcodeSearchCityType()
        {
            ActiveFlag = true;
        }

        public ZipcodeSearchCityType(char Id, string description, Boolean Default)
            : this()
        {
            this.Id          = Id;
            this.description = description;
            this.Default     = Default;
        }
    }

    /// <summary>
    /// This list is used for items that reflect the RPPS proposal type
    /// </summary>
    public static class ZipcodeSearchCityTypes
    {
        private static System.Collections.Generic.List<ZipcodeSearchCityType> colZipcodeSearchCityTypes;
        static ZipcodeSearchCityTypes()
        {
            colZipcodeSearchCityTypes = new System.Collections.Generic.List<ZipcodeSearchCityType>();
            colZipcodeSearchCityTypes.Add(new ZipcodeSearchCityType('D', "Preferred", false));
            colZipcodeSearchCityTypes.Add(new ZipcodeSearchCityType('A', "Acceptable", false));
            colZipcodeSearchCityTypes.Add(new ZipcodeSearchCityType('N', "Not Acceptable", true));
        }

        /// <summary>
        /// Return the static list of the collection
        /// </summary>
        /// <returns></returns>
        static public System.Collections.Generic.List<ZipcodeSearchCityType> getList()
        {
            return colZipcodeSearchCityTypes;
        }

        /// <summary>
        /// Return the default item for this collection
        /// </summary>
        /// <returns></returns>
        static public char getDefault()
        {
            var q = getList().Find(s => s.Default);
            if (q != null)
            {
                return q.Id;
            }
            return 'N';
        }
    }
}