using System;
using System.Collections.Generic;
using System.Linq;

namespace DebtPlus.LINQ.Events
{
    /// <summary>
    /// Storage for the event
    /// </summary>
    /// <remarks></remarks>
    public sealed class SalesPayoutEventArgs : System.EventArgs
    {
        public Int32 Month { get; private set; }
        public decimal Payment { get; private set; }
        public decimal Principal { get; private set; }
        public decimal Interest { get; private set; }
        public decimal Balance { get; private set; }

        public SalesPayoutEventArgs()
        {
        }

        public SalesPayoutEventArgs(Int32 Month, decimal Payment, decimal Interest, decimal Principal, decimal Balance) : this()
        {
            this.Month = Month;
            this.Payment = Payment;
            this.Principal = Principal;
            this.Interest = Interest;
            this.Balance = Balance;
        }
    }

    /// <summary>
    /// Delegate to record the payout information for a sales debt
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void SalesPayoutEventHandler(object sender, SalesPayoutEventArgs e);
}
