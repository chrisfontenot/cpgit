﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.LINQ
{
    public class SQLInfoClass : IDisposable
    {
        private enum tokenValues
        {
            CommandTimeout,
            ConnectionTimeout,
            Server,
            Database,
            Workstation,
            UserId,
            Password,
            TrustedSecurity,
            TrustedConnection,
            FalloverPartner,
            PacketSize,
            NetworkLibrary,
            AttachDbFile,
            Encrypt,
            MARS
        };

        private class token
        {
            public string Id { get; set; }
            public tokenValues tokenValue { get; set; }
            public token(string Id, tokenValues tokenValue)
            {
                this.Id = Id;
                this.tokenValue = tokenValue;
            }
        }

        private static token[] tokenList = new token[]
        {
                new token("CommandTimeout", tokenValues.CommandTimeout),
                new token("Timeout", tokenValues.CommandTimeout),
                new token("ConnectionTimeout", tokenValues.ConnectionTimeout),
                new token("Database", tokenValues.Database),
                new token("Catalog", tokenValues.Database),
                new token("InitialCatalog", tokenValues.Database),
                new token("FalloverPartner", tokenValues.FalloverPartner),
                new token("Password", tokenValues.Password),
                new token("Pwd", tokenValues.Password),
                new token("Server", tokenValues.Server),
                new token("DataSource", tokenValues.Server),
                new token("TrustedSecurity", tokenValues.TrustedSecurity),
                new token("IntegratedSecurity", tokenValues.TrustedSecurity),
                new token("TrustedConnection", tokenValues.TrustedConnection),
                new token("UId", tokenValues.UserId),
                new token("UserId", tokenValues.UserId),
                new token("UserName", tokenValues.UserId),
                new token("Workstation", tokenValues.Workstation),
                new token("PacketSize", tokenValues.PacketSize),
                new token("NetworkLibrary", tokenValues.NetworkLibrary),
                new token("Library", tokenValues.NetworkLibrary),
                new token("AttachDbFile", tokenValues.AttachDbFile),
                new token("Encrypt", tokenValues.Encrypt),
                new token("MultipleActiveResultSets", tokenValues.MARS),
                new token("MARS", tokenValues.MARS)
        };

        /// <summary>
        /// Static copy of the connection information
        /// </summary>
        private static SQLInfoClass privateConnectionInfo = null;
        private static object lockingObject = new object();

        /// <summary>
        /// Get the default value for the database connection object. This is only valid the first
        /// time. If the underlying parameters are changed, this object will not be updated.
        /// </summary>
        /// <returns>The static copy of the SQLInfoClass object</returns>
        public static SQLInfoClass getDefault()
        {
            if (privateConnectionInfo == null)
            {
                lock (lockingObject)
                {
                    if (privateConnectionInfo == null)
                    {
                        privateConnectionInfo = new SQLInfoClass(true);
                    }
                }
            }
            return privateConnectionInfo;
        }

        /// <summary>
        /// Dispose of the cached object. This is only called when the connection information is
        /// changed and the caller wants to toss any cached item in favor of new data.
        /// </summary>
        public static void Reset()
        {
            lock (lockingObject)
            {
                privateConnectionInfo = new SQLInfoClass(true);
            }
        }

        /// <summary>
        /// String value for the connection object
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return ConnectionString;
        }

        /// <summary>
        /// Initialize the instance of the new class
        /// </summary>
        /// <param name="suppressDispose"></param>
        private SQLInfoClass(bool suppressDispose) : this()
        {
            this.suppressDispose = suppressDispose;
        }

        /// <summary>
        /// Initialize the instance of the new class
        /// </summary>
        public SQLInfoClass()
        {
            string defaultSection = System.Configuration.ConfigurationManager.AppSettings["DefaultConnection"] ?? "DebtPlus";
            ReadConnectionSection(defaultSection);
        }

        /// <summary>
        /// Initialize the instance of the new class
        /// </summary>
        /// <param name="ConnectionStringName">Name of the entry in the ConnectionString section</param>
        public SQLInfoClass(string ConnectionStringName)
        {
            ReadConnectionSection(ConnectionStringName);
        }

        private void ReadConnectionSection(string sectionName)
        {
            // Default the values should they be missing
            Workstation = System.Environment.MachineName;
            ConnectionTimeout = 4;
            CommandTimeout = 30;
            Database = null;
            Server = null;
            UserName = null;
            Password = null;
            PacketSize = 0;
            NetworkLibrary = null;
            AttachDbFilename = null;
            FalloverPartner = null;
            IntegratedSecurity = false;
            Encrypt = false;
            TrustedConnection = false;
            Mars = null;

            // Get the settings item from the config filer. If missing then use the defaults 
            var connection = System.Configuration.ConfigurationManager.ConnectionStrings[sectionName].ConnectionString;
            if (!string.IsNullOrEmpty(connection))
            {
                ReadConnectionString(connection);
            }

            // Use the registry to override the config strings
            var registryUserName = DebtPlus.Configuration.Config.Username;
            if (!string.IsNullOrWhiteSpace(registryUserName))
            {
                UserName = registryUserName;
            }

            var registryPassword = DebtPlus.Configuration.Config.Password;
            if (!string.IsNullOrWhiteSpace(registryPassword))
            {
                Password = registryPassword;
            }

            // If there is no user name then the connection is trusted, regardless of the value
            if (string.IsNullOrWhiteSpace(UserName))
            {
                IntegratedSecurity = true;
            }

            // If a user name and password was supplied then we don't have a trusted connection
            else if (!string.IsNullOrWhiteSpace(UserName) && !string.IsNullOrWhiteSpace(Password))
            {
                IntegratedSecurity = false;
            }

            MakeConnectionString();

            // Default the flag to false so that Dispose will work.
            suppressDispose = false;
        }

        private void ReadConnectionString(string ConnectionString)
        {
            // Split the string appropriately
            string[] items = ConnectionString.Split(';');
            foreach(string itemString in items)
            {
                if (! string.IsNullOrWhiteSpace(itemString))
                {
                    string[] splitKey = itemString.Split('=');
                    if (splitKey.GetUpperBound(0) > 0)
                    {
                        ProcessItemKey(splitKey[0], splitKey[1]);
                    }
                    else if (splitKey.GetUpperBound(0) == 0)
                    {
                        ProcessItemKey(splitKey[0], string.Empty);
                    }
                }
            }
        }

        private void ProcessItemKey(string keyID, string keyValue)
        {
            keyID = keyID.Replace(" ", "");
            var tokenItem = tokenList.Where(s => string.Compare(s.Id, keyID, true) == 0).FirstOrDefault();
            if (tokenItem != null)
            {
                switch (tokenItem.tokenValue)
                {
                    case tokenValues.CommandTimeout:
                        CommandTimeout = (int) DebtPlus.Configuration.Config.translateToInt(keyValue).GetValueOrDefault(4);
                        break;
                    case tokenValues.ConnectionTimeout:
                        ConnectionTimeout = (int) DebtPlus.Configuration.Config.translateToInt(keyValue).GetValueOrDefault(30);
                        break;
                    case tokenValues.PacketSize:
                        PacketSize = (int) DebtPlus.Configuration.Config.translateToInt(keyValue).GetValueOrDefault(0);
                        break;
                    case tokenValues.Database:
                        Database = keyValue;
                        break;
                    case tokenValues.FalloverPartner:
                        FalloverPartner = keyValue;
                        break;
                    case tokenValues.Password:
                        Password = keyValue;
                        break;
                    case tokenValues.Server:
                        Server = keyValue;
                        break;
                    case tokenValues.TrustedConnection:
                        TrustedConnection = DebtPlus.Configuration.Config.translateToBool(keyValue).GetValueOrDefault(false);
                        goto case tokenValues.TrustedSecurity;
                    case tokenValues.TrustedSecurity:
                        IntegratedSecurity = DebtPlus.Configuration.Config.translateToBool(keyValue).GetValueOrDefault(false);
                        break;
                    case tokenValues.UserId:
                        UserName = keyValue;
                        break;
                    case tokenValues.Workstation:
                        Workstation = keyValue;
                        break;
                    case tokenValues.NetworkLibrary:
                        NetworkLibrary = keyValue;
                        break;
                    case tokenValues.AttachDbFile:
                        AttachDbFilename = keyValue;
                        break;
                    case tokenValues.Encrypt:
                        Encrypt = DebtPlus.Configuration.Config.translateToBool(keyValue).GetValueOrDefault(false);
                        break;
                    case tokenValues.MARS:
                        Mars = DebtPlus.Configuration.Config.translateToBool(keyValue).GetValueOrDefault(false);
                        break;
                    default:
                        break;
                }
            }
        }

        public string ConnectionString { get; private set; }
        public Int32 CommandTimeout { get; private set; }
        public string UserName { get; private set; }
        public string Password { get; private set; }
        public string Server { get; private set; }
        public string Database { get; private set; }
        public string Workstation { get; private set; }
        public Int32 PacketSize { get; private set; }
        public string NetworkLibrary { get; private set; }
        public string AttachDbFilename { get; private set; }
        public string FalloverPartner { get; private set; }
        public Boolean IntegratedSecurity { get; private set; }
        public Boolean TrustedConnection { get; set; }
        public Boolean Encrypt { get; private set; }
        public Int32 ConnectionTimeout { get; private set; }
        public Boolean? Mars { get; set; }

        private void MakeConnectionString()
        {
            System.Data.SqlClient.SqlConnectionStringBuilder sb = new System.Data.SqlClient.SqlConnectionStringBuilder();

            // Set the server and database entries
            sb.DataSource = Server;
            sb.InitialCatalog = Database;
            
            // This is the name of our workstation
            if (Workstation.Length > 15)
            {
                Workstation = Workstation.Substring(0, 15);
            }
            sb.WorkstationID = Workstation;

            if (Mars.HasValue)
            {
                sb.MultipleActiveResultSets = Mars.Value;
            }

            // Enable connection pooling so that we don't re-open connections all of the time.
            sb.Pooling = true;
            sb.MaxPoolSize = 10;
            sb.LoadBalanceTimeout = 300;

            // Our application name
            sb.ApplicationName = "DebtPlus";

            // If there is a partner, include it
            if (!string.IsNullOrWhiteSpace(FalloverPartner))
            {
                sb.FailoverPartner = FalloverPartner;
            }

            // Include other fields that we use
            if (! string.IsNullOrWhiteSpace(NetworkLibrary))
            {
                sb.NetworkLibrary = NetworkLibrary;
            }

            // Location of the MDB file (not really used)
            if (! String.IsNullOrWhiteSpace(AttachDbFilename))
            {
                sb.AttachDBFilename = AttachDbFilename;
            }

            // If a packet size was specified then include it
            if (PacketSize > 0)
            {
                sb.PacketSize = PacketSize;
            }

            // If there is a connection timeout then specify it
            if (ConnectionTimeout > 0)
            {
                sb.ConnectTimeout = ConnectionTimeout;
            }

            // If this is a trusted connection then so indicate
            if (IntegratedSecurity || TrustedConnection)
            {
                sb.IntegratedSecurity = true;
            }
            else
            {
                sb.IntegratedSecurity = false;
                sb.UserID = UserName;
                sb.Password = Password;
            }

            // Obtain the resulting connection string with the current values.
            ConnectionString = sb.ConnectionString;
        }

        /// <summary>
        /// Should the Dispose routine be active?
        /// </summary>
        private bool suppressDispose { get; set; }

#region IDisposable Support
        private System.Boolean disposedValue; // To detect redundant calls

        /// <summary>
        /// Internal procedure to handle Dispose and Finalize events
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(System.Boolean disposing)
        {
            if (! this.disposedValue)
            {
                if (disposing)
                {
                }
            }
            this.disposedValue = true;
        }

        /// <summary>
        /// Dispose routine for the class
        /// </summary>
        public void Dispose()
        {
            if (!suppressDispose)
            {
                try
                {
                    Dispose(true);
                    GC.SuppressFinalize(this);
                }
                catch { }
            }
        }

        /// <summary>
        /// Finalization routine for the class
        /// </summary>
        ~SQLInfoClass()
        {
            if (!suppressDispose)
            {
                Dispose(false);
            }
        }
#endregion
    }
}
