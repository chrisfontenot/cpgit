﻿namespace DebtPlus.LINQ
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Return the list of records for the privacy policy report
    /// </summary>
    public static partial class Reports
    {
        public static System.Collections.Generic.List<HPFPrivacyPolicy> rpt_HPF_PrivacyPolicy(string languageType)
        {
            using (var bc = new DebtPlus.LINQ.BusinessContext())
            {
                // Current date (without time) for printing items
                DateTime currentDate = System.DateTime.Now.Date;

                // Mark the items that were printed today as "not printed"
                foreach (var item in bc.HPFPrivacyPolicies.Where(s => s.print_date.HasValue && s.print_date.Value == currentDate))
                {
                    item.print_date = null;
                    item.print_queue = null;
                }

                // Expire obsolete entries
                foreach (var item in bc.HPFPrivacyPolicies.Where(s => s.death_date.HasValue && s.death_date.Value <= currentDate))
                {
                    bc.HPFPrivacyPolicies.DeleteOnSubmit(item);
                }
                bc.SubmitChanges();

                // Mark the items that we want to print today
                var guidMarker = System.Guid.NewGuid();
                foreach(var item in bc.HPFPrivacyPolicies.Where(s => s.print_queue == null))
                {
                    item.print_queue = guidMarker;
                    item.print_date = currentDate;
                    item.death_date = currentDate.AddYears(1).Date;
                }

                // Retrieve the collection of items that we are going to print today
                return bc.HPFPrivacyPolicies.Where(s => s.print_queue == guidMarker && ! s.email_queueID.HasValue).ToList();
            }
        }
    }
}
