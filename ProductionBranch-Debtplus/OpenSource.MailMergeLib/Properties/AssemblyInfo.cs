using System.Reflection;
using System.Runtime.InteropServices;
using System.Security;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("MailMergeLib")]
[assembly: AssemblyDescription(".NET library for sending emails")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Norbert Bietsch")]
[assembly: AssemblyProduct("MailMergeLib 4.1")]
[assembly: AssemblyCopyright("Copyright © 2007-2010")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// [assembly: AllowPartiallyTrustedCallers]
[assembly: SecurityTransparent]
[assembly: SecurityRules(SecurityRuleSet.Level1)]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("493782f8-2007-4316-b72c-28fe39c3180f")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("17.4.20.0227")]
[assembly: AssemblyFileVersion("17.4.20.0227")]
[assembly: AssemblyInformationalVersion("17.4.20.0227")]
