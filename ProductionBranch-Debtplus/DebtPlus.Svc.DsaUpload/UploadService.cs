﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.LINQ;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Security;
using System.Security.Permissions;

namespace DebtPlus.Svc.DsaUpload
{
    public class UploadService
    {
        log4net.ILog logger = null;

        //DSA program Values
        const int FreddieMacProgram = 301;
        const int FannieMaeMhnProgram = 302;
        const int FannieMaeHpfProgram = 303;
        const int WellsFargoProgram = 304;
        const int HPFfmacProgram = 305;
        const int HLPProgram = 306;

        //Referral Codes for the DSA specialist
        const int CCRCHopeReferralCode = 824;
        const int HopeFannieMaeReferralCode = 825;
        const int HopeMilitaryAssistanceReferralCode = 838;

        const string DateFormat = "MM/dd/yyyy";
        const string BirthDateFormat = "yyyy-MM-dd";
        const string DefaultDate = "1901-01-02";

        string message = string.Empty;
        StringBuilder requiredFieldsMessage = new StringBuilder();
        StringBuilder referredByMessage = new StringBuilder();
        StringBuilder invalidDsaProgramMessage = new StringBuilder();
        StringBuilder rxOfficeMessage = new StringBuilder();

        public UploadService()
        {
            logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        }

        public string UploadCase(int clientID, int propertyID, int housingLoan, int currentLenderID, int intakeDetailID)
        {
            // Binding Name of the RxOffice Service Name
            string bindingName = "RxImportV1_1Soap";
            string result = "";

            string rxOfficeCaseSubmissionUrl = DebtPlus.Configuration.Config.RxOfficeCaseSubmissionPath;

            using (var bc = new BusinessContext())
            {
                try
                {
                    var hasDsaProgram = bc.has_dsa_program(housingLoan);

                    if (hasDsaProgram == 0)
                    {
                        return "This property does not qualify under dsa program";
                    }

                    var canDisclose = bc.get_dsa_disclosure_detail(clientID);

                    if (canDisclose == -1)
                    {
                        return "Need Authorization to Share Data Disclosure";
                    }           
                    else if (canDisclose > 0)
                    {
                        return "Authorization to Share Data was not authorized";
                    }
                }
                catch (Exception ex)
                {
                    logger.Error("Error occurred while calling the function get_dsa_disclosure_detail. Exception Details: " + ex.Message);
                    return "Error occurred while getting the client disclosure detail.";
                }
            }

            if (!string.IsNullOrWhiteSpace(rxOfficeCaseSubmissionUrl) && Uri.IsWellFormedUriString(rxOfficeCaseSubmissionUrl, UriKind.Absolute))
            {
                RxOffice.CaseInfo info = new RxOffice.CaseInfo();

                // Populate Header information 
                info.Header = GetHeaderInformation();

                info.Details = GetLoanInformation(clientID, propertyID, housingLoan, currentLenderID, intakeDetailID);

                //CreateXMLLog(info);
                var filename = "clientid_" + clientID.ToString();
                if (referredByMessage.Length == 0 && requiredFieldsMessage.Length == 0 && invalidDsaProgramMessage.Length == 0)
                {
                    string caseID = string.Empty;

                    logger.Debug("Initializing RxOffice Service...");
                    using (RxOffice.RxImportV1_1SoapClient client = new RxOffice.RxImportV1_1SoapClient(bindingName, rxOfficeCaseSubmissionUrl))
                    {
                        try
                        {
                            var serviceResult = client.RxLoadNewCase(info);

                            result = ProcessServiceResult(clientID,housingLoan, serviceResult, out caseID);
                        }
                        catch (Exception ex)
                        {
                            logger.Error("Exception on calling LoadNewCase of RxOffice. Exception Details: " + ex.Message);
                            throw;
                        }
                        finally
                        {
                            filename += (string.IsNullOrWhiteSpace(caseID)) ? "" : "_caseid_" + caseID;
                            CreateXMLLog(info, filename);
                        }
                    }
                }
                else
                {
                    if (requiredFieldsMessage.Length > 0)
                    {
                        result = "Following fields are required for the request to be sent to RxOffice. Please enter these details and upload the case again:" + Environment.NewLine + requiredFieldsMessage.ToString();
                    }
                    if (invalidDsaProgramMessage.Length > 0)
                    {
                        result = invalidDsaProgramMessage.ToString();
                    }

                    if (referredByMessage.Length > 0)
                    {
                        result = referredByMessage.ToString();
                    }

                    CreateXMLLog(info, filename);
                }
            }
            else
            {
                result = "Incorrect configuration." + Environment.NewLine + "The RxOffice Case Submission Url is incorrect.";
                logger.Error("RxOffice Case Submission Url Configured is incorrect. Current Setting: " + rxOfficeCaseSubmissionUrl);
            }

            return result;
        }

        private void CreateXMLLog(RxOffice.CaseInfo caseObject, string filename)
        {
            string logFilePath = DebtPlus.Configuration.Config.LogFileDirectory;

            if (Directory.Exists(logFilePath))
            {
                var caseFileName = Path.Combine(logFilePath, "casefile_" + filename + "_" + DateTime.Now.ToString("MMddyyyyhhmmss") + ".xml");

                var permissionSet = new PermissionSet(PermissionState.None);
                var writePermission = new FileIOPermission(FileIOPermissionAccess.Write, caseFileName);
                permissionSet.AddPermission(writePermission);

                if (permissionSet.IsSubsetOf(AppDomain.CurrentDomain.PermissionSet))
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    //Represents an XML document, 
                    // Initializes a new instance of the XmlDocument class.          
                    XmlSerializer xmlSerializer = new XmlSerializer(caseObject.GetType());
                    // Creates a stream whose backing store is memory. 
                    using (MemoryStream xmlStream = new MemoryStream())
                    {
                        try
                        {
                            xmlSerializer.Serialize(xmlStream, caseObject);
                            xmlStream.Position = 0;
                            //Loads the XML document from the specified string.
                            xmlDoc.Load(xmlStream);
                            xmlDoc.Save(caseFileName);
                        }
                        catch (Exception ex)
                        {
                            logger.Error("Error occurred while trying to create the xml log file. Message: " + ex.Message);
                        }
                    }
                }
                else
                {
                    logger.Error("User does not have permission to write to the path " + logFilePath);
                }
            }
            else
            {
                logger.Error("Folder to create log file does not exist. Path: " + logFilePath);
            }
        }

        private string ProcessServiceResult(int clientID,int housingLoanID, RxOffice.CaseResult serviceResult, out string caseID)
        {
            string result = "";

            caseID = string.Empty;

            if (serviceResult != null && serviceResult.Result != null && serviceResult.Result.Length > 0)
            {
                // Get the error details
                var caseResult = serviceResult.Result[0];
                if (caseResult.Error != null && caseResult.Error.Length > 0)
                {
                    var error = caseResult.Error[0];
                    string errorDetails = string.Format("Error Message: {0}, Error Description: {1}", error.Message, error.Description);

                    result = "Following error occurred on submitting the request to RxOffice: " + Environment.NewLine + error.Description;
                    logger.Error("Error occurred while loading a new case. " + errorDetails);
                    GenerateSystemNote(clientID, false, "");
                }
                else
                {
                    // Get the success massage
                    int newCaseID = caseResult.CaseID;
                    caseID = caseResult.CaseID.ToString();
                    GenerateSystemNote(clientID, true, caseID);
                    using (var bc = new BusinessContext())
                    {
                        try
                        {
                            bc.xpr_update_dsa_case_details(housingLoanID, newCaseID);
                        }
                        catch (Exception ex)
                        {
                            logger.Error("Error occurred while trying to update the case id details. Message: " + ex.Message);
                        }
                    }
                }
            }
            return result;
        }

        private void GenerateSystemNote(int clientID, bool UploadSuccess, string caseID)
        {
            string subjectText = string.Empty;
            string noteText =  String.Empty;

            if (UploadSuccess)
            {
                subjectText = "Dsa Upload Success";
                noteText = "DSA uploaded successfully. DSA CaseID = " + caseID;
            }
            else
            {
                subjectText = "Dsa Upload Error";
                noteText = "DSA unable to upload at this time";
            }

            using (var bc = new BusinessContext())
            {
                // If there is a client then write a client note to the system
                var cn = DebtPlus.LINQ.Factory.Manufacture_client_note(clientID, 3);
                cn.subject = subjectText;
                cn.note = noteText;
                bc.client_notes.InsertOnSubmit(cn);

                bc.SubmitChanges();
            }
        }

        /// <summary>
        /// This method is used to populate the load information for rxoffice
        /// </summary>
        /// <returns></returns>
        private RxOffice.ArrayOfImportCaseLoanInformationLoanInformation[] GetLoanInformation(int clientID, int propertyID, int housingLoan, int currentLenderID, int intakeDetailID)
        {
            var details = new RxOffice.ArrayOfImportCaseLoanInformationLoanInformation[1];

            xpr_get_dsa_case_income_detailsResult income = null;
            xpr_get_dsa_case_expense_detailsResult expense = null;
            xpr_get_dsa_case_asset_detailsResult asset = null;
            xpr_get_dsa_case_liability_detailsResult liability = null;
            // xpr_get_dsa_case_liens_detailsResult liens = null;

            using (var bc = new BusinessContext())
            {
                try
                {
                    var result = bc.xpr_get_dsa_loan_details(clientID, propertyID, housingLoan, currentLenderID, intakeDetailID).ToList();

                    if (result != null && result.Count > 0)
                    {
                        // get the case details
                        var cd = result[0];

                        string message = bc.validate_dsa_referral(clientID, cd.DsaProgram);

                        if (!string.IsNullOrWhiteSpace(message))
                        {
                            referredByMessage.Append(message);
                            return null;
                        }

                        var incomeResult = bc.xpr_get_dsa_case_income_details(clientID).ToList();
                        var expenseResult = bc.xpr_get_dsa_case_expense_details(clientID).ToList();
                        var assetResult = bc.xpr_get_dsa_case_asset_details(clientID, propertyID).ToList();
                        var liabilityResult = bc.xpr_get_dsa_case_liability_details(clientID).ToList();
                        var lienResult = bc.xpr_get_dsa_case_liens_details(housingLoan, propertyID).ToList();

                        RxOffice.CaseTypeEnumType caseType = GetCaseType(cd.DsaProgram, cd.ReferralCode);

                        string loanNumber = (cd.AccountNumber != null) ? cd.AccountNumber.ToString() : "";

                        if (string.IsNullOrWhiteSpace(loanNumber))
                        {
                            requiredFieldsMessage.AppendLine("Account Number field in the Lender tab of the loan");
                        }

                        string mortgageSourceId = (cd.MortgageCompanySourceID != null) ? cd.MortgageCompanySourceID.ToString().PadLeft(6, '0') : "";

                        if (string.IsNullOrWhiteSpace(mortgageSourceId))
                        {
                            requiredFieldsMessage.AppendLine("MortgageCompanySourceId aka RXOfficeServicerID in the database");
                        }

                        string investorSourceID = (cd.InvestorCode != null) ? cd.InvestorCode.ToString() : "";

                        if (string.IsNullOrWhiteSpace(investorSourceID))
                        {
                            requiredFieldsMessage.AppendLine("Investor in the Lender tab of the loan");
                        }

                        double balance = (cd.Balance != null) ? (double)cd.Balance : 0.00;
                        double delinquencyAmount = (cd.DelinquencyAmount != null) ? (double)cd.DelinquencyAmount : 0.00;
                        double InstallmentAmount = (cd.MonthlyPayment != null) ? (double)cd.MonthlyPayment : 0.00;
                        double interestRate = 0.00;
                        if (cd.InterestRate.HasValue)
                        {
                            // Convert to Percentage
                            interestRate = (double)cd.InterestRate * 100;
                        }

                        details[0] = new RxOffice.ArrayOfImportCaseLoanInformationLoanInformation()
                        {
                            CaseStatusCode = "New",
                            CaseStatus = "New Case",
                            CaseOpenDate = DateTime.Now.ToString(DateFormat),
                            CaseType = caseType,
                            LoanType = cd.LoanType.ToString(),
                            LoanNumber = loanNumber,
                            ModifiedInLastSixMonths = cd.ModifiedInLastSixMonths,
                            InterestRate = interestRate,
                            InterestRateSpecified = true,
                            MonthlyPayment = InstallmentAmount,
                            MonthlyPaymentSpecified = true,
                            MonthsBehind = (cd.MonthsBehind != null) ? (int)cd.MonthsBehind : 0,
                            MonthsBehindSpecified = true,
                            IfForeclosure = cd.IfForeclosure,
                            SaleDate = (cd.SaleDate != null) ? ((DateTime)cd.SaleDate).ToString(DateFormat) : "",
                            DelinquencyAmount = delinquencyAmount,
                            DelinquencyAmountSpecified = true,
                            InvestorSourceID = investorSourceID,
                            InvestorName = cd.InvestorName,
                            //InvestorLoanNumber must be 10 digit
                            InvestorLoanNumber = cd.InvestorLoanNumber,
                            MortgageCompanySourceId = mortgageSourceId,
                            Balance = balance,
                            BalanceSpecified = true,
                            InstallmentAmount = InstallmentAmount,
                            InstallmentAmountSpecified = true,
                            AgencyInformation = new RxOffice.AgencyInformationType()
                            {
                                InternalRef = clientID.ToString()
                            },
                            PropertyValuation = new RxOffice.ArrayOfImportCaseLoanInformationLoanInformationPropertyValuation()
                            {
                                EstimatedCurrentValue = (cd.EstimatedCurrentValue != null) ? (double)cd.EstimatedCurrentValue : 0
                            }
                        };

                        details[0].Borrower = GetBorrowerDetails(cd);
                        details[0].CoBorrower = GetCoBorrowerDetails(cd);
                        details[0].PropertyInformation = GetPropertyInformation(cd);
                        details[0].BorrowerMailAddress = new RxOffice.AddressInformationType()
                        {
                            AddressLine1 = cd.BorrowerAddress1,
                            AddressLine2 = (string.IsNullOrWhiteSpace(cd.BorrowerAddress2)) ? "NA" : cd.BorrowerAddress2,
                            AddressLine3 = (string.IsNullOrWhiteSpace(cd.BorrowerAddress3)) ? "NA" : cd.BorrowerAddress3,
                            City = cd.BorrowerCity,
                            StateCode = cd.BorrowerStateCode,
                            Zip = cd.BorrowerZip
                        };

                        // Get Conversation details
                        details[0].Conversation = GetConversationDetails(cd);

                        if (incomeResult != null && incomeResult.Count > 0)
                        {
                            income = incomeResult[0];
                        }

                        if (expenseResult != null && expenseResult.Count > 0)
                        {
                            expense = expenseResult[0];
                        }

                        if (assetResult != null && assetResult.Count > 0)
                        {
                            asset = assetResult[0];
                        }

                        if (liabilityResult != null && liabilityResult.Count > 0)
                        {
                            liability = liabilityResult[0];
                        }

                        //if (lienResult != null && lienResult.Count > 0)
                        //{
                        //    liens = lienResult[0];
                        //}

                        // Get financial details
                        details[0].Financials = GetFinancialDetails(cd, income, expense, asset, liability);

                        details[0].HardshipInformation = GetHardshipInformation(cd);

                        details[0].LiensInformation = GetLiensInformation(lienResult);

                        details[0].CounselorInformation = GetCounselorInformation(cd);

                    }
                }
                catch (Exception ex)
                {
                    logger.Error("Error occurred while calling the stored procedure xpr_get_loan_case_details. Exception Details: " + ex.Message);
                }
            }

            return details;
        }

        private RxOffice.ArrayOfImportCaseLoanInformationLoanInformationCounselorInformation GetCounselorInformation(xpr_get_dsa_loan_detailsResult cd)
        {
            var counselorInfo = new RxOffice.ArrayOfImportCaseLoanInformationLoanInformationCounselorInformation();

            counselorInfo.CounselingAgency = cd.CounselorAgency;
            counselorInfo.CounselorDesignatedUser = cd.CounselorDesignatedUser;
            counselorInfo.CounselorName = cd.CounselorName;
            counselorInfo.CounselorSourceID = "cccsat";

            return counselorInfo;
        }

        private RxOffice.ArrayOfImportCaseLoanInformationLiensLiens[] GetLiensInformation(List<xpr_get_dsa_case_liens_detailsResult> liens)
        {
            var liensList = new List<RxOffice.ArrayOfImportCaseLoanInformationLiensLiens>();

            if (liens != null && liens.Count > 0)
            {
                foreach (var item in liens)
                {
                    var lien = new RxOffice.ArrayOfImportCaseLoanInformationLiensLiens();
                    lien.Balance = (double)item.Balance.GetValueOrDefault(0M); // (item.Balance != null) ? (double)item.Balance : 0.00;
                    //lien.BalanceSpecified = item.Balance != null;   // True if the value was specified. False if it was originally null.
                    lien.BalanceSpecified = true;
                    lien.InterestRate = item.InterestRate.GetValueOrDefault(0.0D); // (item.InterestRate != null) ? (double)item.InterestRate : 0.00;
                    //lien.InterestRateSpecified = item.InterestRate != null;  // True if the value was specified. False if it was originally null.
                    lien.InterestRateSpecified = true;
                    lien.MonthsBehind = item.MonthsBehind.GetValueOrDefault(0);
                    //lien.MonthsBehindSpecified = item.MonthsBehind != null;  // True if the value is specified. False if it was originally null.
                    lien.MonthsBehindSpecified = true;
                    lien.SecondMortgageCompany = item.SecondaryMortgageCompany;
                    lien.SecondMortgageCompanySourceID = (item.SecondaryMortgageCompanySourceID != null) ? item.SecondaryMortgageCompanySourceID.ToString() : string.Empty;
                    lien.SecondMortgageLoanNumber = (item.SecondaryMortgageLoanNumber != null) ? item.SecondaryMortgageLoanNumber.ToString() : string.Empty;

                    liensList.Add(lien);
                }
            }

            return liensList.ToArray();
        }

        private RxOffice.BorrowerDetailsType[] GetCoBorrowerDetails(xpr_get_dsa_loan_detailsResult cd)
        {
            if (string.IsNullOrWhiteSpace(cd.CoborrowerFirstName) && string.IsNullOrWhiteSpace(cd.CoborrowerLastName))
            {
                return null;
            }

            var coborrowers = new List<RxOffice.BorrowerDetailsType>();

            var cb = new RxOffice.BorrowerDetailsType();

            //Field mandated by RXOffice, set default value of 2
            cb.DebtorSequenceNo = 2;

            cb.FirstName = cd.CoborrowerFirstName;
            cb.MiddleName = cd.CoborrowerMiddleName;
            cb.LastName = cd.CoborrowerLastName;
            cb.Category = RxOffice.BorrowerType.CoBorrower;
            cb.SSN = (string.IsNullOrWhiteSpace(cd.CoborrowerSSN)) ? "000000000" : cd.CoborrowerSSN;
            cb.BirthDate = (cd.CoborrowerBirthdate != null) ? ((DateTime)cd.CoborrowerBirthdate).ToString(BirthDateFormat) : DefaultDate;

            if (string.Compare(cd.CoborrowerGender, "Male", true) == 0)
            {
                cb.Gender = "M";
            }
            if (string.Compare(cd.CoborrowerGender, "Female", true) == 0)
            {
                cb.Gender = "F";
            }
            cb.MaritalStatus = cd.CoborrowerMaritalStatus;
            cb.Ethnicity = cd.CoborrowerEthnicity;
            cb.Race = cd.CoborrowerRace;
            cb.WorkPhone = (string.IsNullOrWhiteSpace(cd.CoborrowerWorkphone)) ? "" : cd.CoborrowerWorkphone;
            cb.CellPhone = (string.IsNullOrWhiteSpace(cd.CoborrowerCellphone)) ? "" : cd.CoborrowerCellphone; 
            cb.EmailID = cd.CoborrowerEmail;
            cb.Zip = (string.IsNullOrWhiteSpace(cd.BorrowerZip)) ? "00000" : cd.BorrowerZip;

            // Hard code values as per ticket DP-340
            cb.PrimaryContactType = RxOffice.ContactType.Home;
            cb.SecondaryContactType = RxOffice.ContactType.Cell;
            cb.AlternateContactType = RxOffice.ContactType.Work;

            var ei = new RxOffice.BorrowerDetailsTypeEmployerInformation();
            if (!string.IsNullOrWhiteSpace(cd.CoborrowerCurrentlyEmployed))
            {
                ei.CurrentlyEmployed = cd.CoborrowerCurrentlyEmployed;
            }
            ei.EmployerName = cd.CoborrowerEmployer;

            var employerInfoList = new List<RxOffice.BorrowerDetailsTypeEmployerInformation>();
            employerInfoList.Add(ei);

            cb.EmployerInformation = employerInfoList.ToArray();

            coborrowers.Add(cb);

            return coborrowers.ToArray();
        }

        private RxOffice.PrimaryBorrowerDetailsType GetBorrowerDetails(xpr_get_dsa_loan_detailsResult cd)
        {
            var b = new RxOffice.PrimaryBorrowerDetailsType();

            if (string.IsNullOrWhiteSpace(cd.BorrowerFirstName))
            {
                requiredFieldsMessage.AppendLine("Borrower > FirstName");
            }
            if (string.IsNullOrWhiteSpace(cd.BorrowerLastName))
            {
                requiredFieldsMessage.AppendLine("Borrower > LastName");
            }

            string ssn = (string.IsNullOrWhiteSpace(cd.BorrowerSSN)) ? "000000000" : cd.BorrowerSSN;
            string birthdate = (cd.BorrowerBirthdate != null) ? ((DateTime)cd.BorrowerBirthdate).ToString(BirthDateFormat) : DefaultDate;

            string numberOfPeopleLiving = (cd.NoOfPersonLiving != null) ? cd.NoOfPersonLiving.ToString() : "0";
            string numberOfDependents = (cd.Dependents != null) ? cd.Dependents.ToString() : "0";

            b.FirstName = cd.BorrowerFirstName;
            b.MiddleName = cd.BorrowerMiddleName;
            b.LastName = cd.BorrowerLastName;
            b.SSN = ssn;
            b.Category = RxOffice.BorrowerType.Borrower;
            b.Language = cd.BorrowerLanguage;
            b.BirthDate = birthdate;
            if (string.Compare(cd.BorrowerGender, "Male", true) == 0)
            {
                b.Gender = "M";
            }
            if (string.Compare(cd.BorrowerGender, "Female", true) == 0)
            {
                b.Gender = "F";
            }
            b.MaritalStatus = cd.BorrowerMaritalStatus;
            b.Ethnicity = cd.BorrowerEthnicity;
            b.Race = cd.BorrowerRace;
            b.HomePhone = (string.IsNullOrWhiteSpace(cd.BorrowerHomePhone)) ? "" : cd.BorrowerHomePhone;
            b.WorkPhone = (string.IsNullOrWhiteSpace(cd.BorrowerWorkPhone)) ? "" : cd.BorrowerWorkPhone;
            b.CellPhone = (string.IsNullOrWhiteSpace(cd.BorrowerCellPhone)) ? "" : cd.BorrowerCellPhone; 
            b.EmailID = cd.BorrowerEmail;
            b.Address1 = cd.BorrowerAddress1;
            b.Address2 = (string.IsNullOrWhiteSpace(cd.BorrowerAddress2)) ? "NA" : cd.BorrowerAddress2;
            b.Address3 = (string.IsNullOrWhiteSpace(cd.BorrowerAddress3)) ? "NA" : cd.BorrowerAddress3;
            b.City = cd.BorrowerCity;
            b.County = (string.IsNullOrWhiteSpace(cd.BorrowerCounty)) ? "NA" : cd.BorrowerCounty;
            b.StateCode = cd.BorrowerStateCode;
            b.Zip = (string.IsNullOrWhiteSpace(cd.BorrowerZip)) ? "00000" : cd.BorrowerZip;
            b.HowDidHearAboutUs = cd.HowDidYouHearAboutUs;
            
            b.NoOfPersonsLiving = numberOfPeopleLiving;
            b.NoOfDependent = numberOfDependents;

            switch(cd.BorrowerPreferredContact)
            {
                case "email":
                    b.BestMethodToContact = RxOffice.BestContactMethodType.Email;
                    break;
                case "phone":
                    b.BestMethodToContact = RxOffice.BestContactMethodType.Phone;
                    break;
                case "letter":
                    b.BestMethodToContact = RxOffice.BestContactMethodType.Letter;
                    break;
                default:
                    b.BestMethodToContact = RxOffice.BestContactMethodType.Phone;
                    break;
            }

            // Hard code values as per ticket DP-307
            b.PrimaryContactType = RxOffice.ContactType.Home;
            b.SecondaryContactType = RxOffice.ContactType.Cell;
            b.AlternateContactType = RxOffice.ContactType.Work;

            var ei = new RxOffice.PrimaryBorrowerDetailsTypeEmployerInformation();
            if (!string.IsNullOrWhiteSpace(cd.BorrowerCurrentlyEmployed))
            {
                ei.CurrentlyEmployed = cd.BorrowerCurrentlyEmployed;
            }
            ei.EmployerName = cd.BorrowerEmployer;

            var employerInfoList = new List<RxOffice.PrimaryBorrowerDetailsTypeEmployerInformation>();
            employerInfoList.Add(ei);

            b.EmployerInformation = employerInfoList.ToArray();

            return b;
        }

        private RxOffice.ArrayOfImportCaseLoanInformationLoanInformationHardshipInformation GetHardshipInformation(xpr_get_dsa_loan_detailsResult cd)
        {
            var hardshipInfo = new RxOffice.ArrayOfImportCaseLoanInformationLoanInformationHardshipInformation();

            hardshipInfo.Reason1 = cd.CauseOfFinancialProblem1;
            hardshipInfo.Reason2 = cd.CauseOfFinancialProblem2;
            hardshipInfo.Reason3 = cd.CauseOfFinancialProblem3;

            return hardshipInfo;
        }

        /// <summary>
        /// Gets the Borrower and Coborrower Income data
        /// </summary>
        /// <param name="cd"></param>
        /// <returns></returns>
        private RxOffice.FinancialType GetFinancialDetails(xpr_get_dsa_loan_detailsResult cd,
                                                           xpr_get_dsa_case_income_detailsResult income,
                                                           xpr_get_dsa_case_expense_detailsResult expense,
                                                           xpr_get_dsa_case_asset_detailsResult asset,
                                                           xpr_get_dsa_case_liability_detailsResult liability)
        {
            var financials = new RxOffice.FinancialType();

            #region "Borrower Income List"

            var biList = new List<RxOffice.ArrayOfFinancialTypeIncomeDetailsIncomeIncome>();
            biList.Add(AddBorrowerIncomeItem("mnth_gross_inc", "Total Gross Monthly Income", cd.BorrowerTotalGrossMonthlyIncome));
            biList.Add(AddBorrowerIncomeItem("mnth_income", "Total Monthly Take Home (Net Income)", cd.BorrowerTotalMonthlyTakeHome));

            if (income != null)
            {
                biList.Add(AddBorrowerIncomeItem("unemp_inc", "Unemployment Income", income.UnemploymentIncome));
                biList.Add(AddBorrowerIncomeItem("chld_suprt_almny", "Child Support/ Alimony/ Separation", income.ChildSupportAlimonyIncome));
                biList.Add(AddBorrowerIncomeItem("disabl_inc_ssi", "Social Security/ SSDI", income.SocialSecurityIncome));
                biList.Add(AddBorrowerIncomeItem("rent_rcvd", "Rents Received", income.RentReceivedIncome));
                biList.Add(AddBorrowerIncomeItem("bonus_inc", "Tip's, commission, bonus and self-employed income", income.BonusIncome));
                biList.Add(AddBorrowerIncomeItem("other", "Other (Investment income, royalties, Interest, dividends etc)", income.OtherIncome));
                //biList.Add(AddBorrowerIncomeItem("Overtime", "Overtime", income.OvertimeIncome));
                biList.Add(AddBorrowerIncomeItem("oth_month_inc", "Other monthly income from pensions, annuities or retirement plans", income.OtherMonthlyIncome));
                biList.Add(AddBorrowerIncomeItem("food_welfare", "Food stamps/Welfare", income.FoodWelfareIncome));
            }

            #endregion

            #region "Coborrower Income List"
            var ciList = new List<RxOffice.FinancialTypeIncomeDetailsCoBorrowerIncome>();
            if (!string.IsNullOrWhiteSpace(cd.CoborrowerFirstName) && !string.IsNullOrWhiteSpace(cd.CoborrowerLastName))
            {
                // CoBorrower Income List
                var incomeList = new List<RxOffice.FinancialTypeIncomeDetailsCoBorrowerIncomeIncome>();
                incomeList.Add(AddCoborrowerIncomeItem("mnth_gross_inc", "Total Gross Monthly Income", cd.CoborrowerTotalGrossMonthlyIncome));
                incomeList.Add(AddCoborrowerIncomeItem("mnth_income", "Total Monthly Take Home (Net Income)", cd.CoborrowerTotalMonthlyTakeHome));


                var ci = new RxOffice.FinancialTypeIncomeDetailsCoBorrowerIncome()
                {
                    //Field mandated by RXOffice, set default value of 2
                    DebtorSeqNo = 2,
                    Income = incomeList.ToArray()
                };
                ciList.Add(ci);
            }

                    
            #endregion

            financials.IncomeDetails = new RxOffice.FinancialTypeIncomeDetails()
            {
                BorrowerIncome = biList.ToArray(),
                CoBorrowerIncome = ciList.ToArray()
            };

            if (expense != null)
            {
                var expenses = new List<RxOffice.ArrayOfFinancialTypeExpensesExpenses>();
                expenses.Add(AddExpenseItem("mnth_mrgs", "First Mortgage Payment", expense.FirstMortgagePayment));
                expenses.Add(AddExpenseItem("lien", "Second Mortgage Payment", expense.SecondMortgagePayment));
                expenses.Add(AddExpenseItem("mnth_owner_ins_pay", "Monthly Homeowner's Insurance Payment", expense.MonthlyHomeOwnerPayment));
                expenses.Add(AddExpenseItem("prop_tax", "Property Taxes", expense.PropertyTax));
                expenses.Add(AddExpenseItem("auto_loan", "Car Payments", expense.AutoLoan));
                expenses.Add(AddExpenseItem("crd_crdothrs", "Credit Cards/ Installment Loan(s)", expense.CreditCardInstallment));
                expenses.Add(AddExpenseItem("chld_spport", "Alimony, child support payments", expense.ChildSupport));
                //expenses.Add(AddExpenseItem("medical_exp", "Total Monthly Medical Expenses", expense.TotalMonthlyMedicalExpenses));
                expenses.Add(AddExpenseItem("health_sub_pre", "Prescription/Co-Payments", expense.PrescriptionCoPayments));
                expenses.Add(AddExpenseItem("ins_sub_med_life", "Medical Life Insurance", expense.MedicalLifeInsurance));
                expenses.Add(AddExpenseItem("food", "Total Monthly Grocery Expenses", expense.TotalMonthlyGroceryExpense));
                expenses.Add(AddExpenseItem("ins_sub_car", "Insurance, Tags, Taxes", expense.Insurance));
                expenses.Add(AddExpenseItem("utilities_sub_Elec", "Electricity", expense.UtilityElectricity));
                expenses.Add(AddExpenseItem("utilities_sub_Water", "Water and Sewer", expense.UtilityWater));
                expenses.Add(AddExpenseItem("entrtmt_sub_cell", "Phone/Cell", expense.PhoneExpense));
                expenses.Add(AddExpenseItem("entrtmt_sub_cable", "Internet/Cable", expense.EntertainmentExpense));
                expenses.Add(AddExpenseItem("trp_sub_gas", "Gas/Public Trans/Toll/Maint", expense.GasExpense));
                expenses.Add(AddExpenseItem("oth_eating_exp", "Eating Out", expense.EatingOutExpense));
                expenses.Add(AddExpenseItem("pers_sub_cloth", "Clothing/Accessories", expense.ClothingExpense));
                expenses.Add(AddExpenseItem("life_sub_clen", "Dry Cleaning /Housecleaning", expense.DryCleaningExpense));
                expenses.Add(AddExpenseItem("pers_sub_nail", "Nail/Barber/Salon", expense.BarberExpense));
                expenses.Add(AddExpenseItem("life_sub_alco", "Alcohol/Tobacco", expense.AlcoholExpense));
                expenses.Add(AddExpenseItem("oth_sub_tithes", "Tithes/Charity", expense.CharityExpense));
                expenses.Add(AddExpenseItem("oth_sub_gym", "Rec/Clubs/Entertainment", expense.ClubExpense));
                expenses.Add(AddExpenseItem("pers_sub_gift", "Gift", expense.GiftExpense));
                expenses.Add(AddExpenseItem("oth_sub_exp", "Other", expense.OtherSubExpense));
                expenses.Add(AddExpenseItem("oth_sub_exp1", "Other1", expense.OtherSub1Expense));
                //expenses.Add(AddExpenseItem("oth_sub_exp2", "Other2", expense.OtherSub2Expense));
                expenses.Add(AddExpenseItem("hoa_fees", "HOA/ Condo Fees/ Property Maintenance", expense.PropertyMaintenance));
                //expenses.Add(AddExpenseItem("oth_mnth_exp", "Other", expense.OtherMonthlyExpense));
                expenses.Add(AddExpenseItem("dpnd_care_exp", "Dependent Care Expenses", expense.OtherDayExpense));
                expenses.Add(AddExpenseItem("finan_sub_stud", "Student Loan", expense.StudentLoan));
                expenses.Add(AddExpenseItem("finan_sub_loan", "Other Installment Loans", expense.OtherInstallmentLoans));
                //expenses.Add(AddExpenseItem("rent", "Net Rental Expenses", expense.NetRentalExpense));
                expenses.Add(AddExpenseItem("fed_state_tax", "Federal and State Tax, FICA", expense.FederalTaxStateTax));

                financials.ExpensesDetails = expenses.ToArray();
            }

            if (asset != null)
            {
                var assets = new List<RxOffice.ArrayOfFinancialTypeAssetsAssets>();
                assets.Add(AddAssetItem("home_cd", "Home", asset.Home));
                assets.Add(AddAssetItem("chk_act_cd", "Primary Checking Account", asset.PrimaryCheckingAccount));
                assets.Add(AddAssetItem("svgs_mkt", "Savings/Money Market", asset.SavingsMoneyMarket));
                assets.Add(AddAssetItem("stock_cd", "CDs (Certificates of Deposit)", asset.CertificateOfDeposit));
                assets.Add(AddAssetItem("stocks_bonds", "Stocks/ Bonds", asset.StockBonds));
                assets.Add(AddAssetItem("ira_mkt", "IRA / Keogh Accounts", asset.IRAMarket));
                assets.Add(AddAssetItem("esop_act", "401k/ESOP Accounts ,SEP or other retirement accounts", asset.ESOPAccount));
                assets.Add(AddAssetItem("life_ins", "Life Insurance  (Whole Life, not Term)", asset.LifeInsurance));
                assets.Add(AddAssetItem("oth_invst", "Other Asset Balance", asset.OtherAssetBalance));
                assets.Add(AddAssetItem("other_new", "Any Other Asset Balance", asset.AnyOtherAssetBalance));
                assets.Add(AddAssetItem("real_est_cd", "Other Real Estate (estimated value)", asset.OtherRealEstate));
                assets.Add(AddAssetItem("cars", "Cars", asset.Cars));
                assets.Add(AddAssetItem("other1_Ass", "Other1", asset.Other1));
                assets.Add(AddAssetItem("other2_Ass", "Other2", asset.Other2));
                assets.Add(AddAssetItem("other3_Ass", "Other3", asset.Other3));
                assets.Add(AddAssetItem("other4_Ass", "Other4", asset.Other4));

                financials.AssetDetails = assets.ToArray();
            }

            if (liability != null)
            {
                var liabilities = new List<RxOffice.ArrayOfFinancialTypeLiabilityLiability>();
                liabilities.Add(AddLiabilityItem("auto_loan_balance", "Auto Loan Balance", liability.AutoLoanBalance));
                liabilities.Add(AddLiabilityItem("credit_card_balance", "Credit Cards/ Installment Loan(s) ( total minimum payment per month )", liability.CreditCardInstallmentLoan));
                liabilities.Add(AddLiabilityItem("other1_li", "Other1", liability.Other1));
                liabilities.Add(AddLiabilityItem("other2_li", "Other2", liability.Other2));
                liabilities.Add(AddLiabilityItem("other3_li", "Other3", liability.Other3));
                liabilities.Add(AddLiabilityItem("other4_li", "Other4", liability.Other4));

                financials.LiabilityDetails = liabilities.ToArray();
            }

            return financials;
        }

        private RxOffice.ArrayOfFinancialTypeIncomeDetailsIncomeIncome AddBorrowerIncomeItem(string code, string type, decimal? value)
        {
            var item = new RxOffice.ArrayOfFinancialTypeIncomeDetailsIncomeIncome();

            item.Code = code;
            item.Type = type;
            item.MonthlyIncome = (value != null) ? (double)value : 0.00;

            return item;
        }

        private RxOffice.FinancialTypeIncomeDetailsCoBorrowerIncomeIncome AddCoborrowerIncomeItem(string code, string type, decimal? value)
        {
            var item = new RxOffice.FinancialTypeIncomeDetailsCoBorrowerIncomeIncome();

            item.Code = code;
            item.Type = type;
            item.MonthlyIncome = (value != null) ? (double)value : 0.00;

            return item;
        }

        private RxOffice.ArrayOfFinancialTypeExpensesExpenses AddExpenseItem(string code, string type, decimal? value)
        {
            var item = new RxOffice.ArrayOfFinancialTypeExpensesExpenses();

            item.Code = code;
            item.Type = type;
            item.MonthlyExpense = (value != null) ? (double)value : 0.00;

            return item;
        }

        private RxOffice.ArrayOfFinancialTypeAssetsAssets AddAssetItem(string code, string type, decimal? value)
        {
            var item = new RxOffice.ArrayOfFinancialTypeAssetsAssets();

            item.Code = code;
            item.Type = type;
            item.Amount = (value != null) ? (double)value : 0.00;

            return item;
        }

        private RxOffice.ArrayOfFinancialTypeLiabilityLiability AddLiabilityItem(string code, string type, decimal? value)
        {
            var item = new RxOffice.ArrayOfFinancialTypeLiabilityLiability();

            item.Code = code;
            item.Type = type;
            item.Amount = (value != null) ? (double)value : 0.00;

            return item;
        }

        /// <summary>
        /// Gets property information
        /// </summary>
        /// <param name="cd"></param>
        /// <returns></returns>
        private RxOffice.PropertyInformationType GetPropertyInformation(xpr_get_dsa_loan_detailsResult cd)
        {
            if (string.IsNullOrWhiteSpace(cd.AddressLine1))
            {
                requiredFieldsMessage.AppendLine("PropertyInformation > AddressLine1");
            }
            if (string.IsNullOrWhiteSpace(cd.City))
            {
                requiredFieldsMessage.AppendLine("PropertyInformation > City");
            }
            if (string.IsNullOrWhiteSpace(cd.StateCode))
            {
                requiredFieldsMessage.AppendLine("PropertyInformation > StateCode");
            }
            if (string.IsNullOrWhiteSpace(cd.ZipCode))
            {
                requiredFieldsMessage.AppendLine("PropertyInformation > Zip");
            }

            var propertyInformation = new RxOffice.PropertyInformationType()
            {
                AddressLine1 = cd.AddressLine1,
                AddressLine2 = (string.IsNullOrWhiteSpace(cd.AddressLine2)) ? "NA" : cd.AddressLine2,
                AddressLine3 = (string.IsNullOrWhiteSpace(cd.AddressLine3)) ? "NA" : cd.AddressLine3,
                City = cd.City,
                County = (string.IsNullOrWhiteSpace(cd.BorrowerCounty)) ? "NA" : cd.BorrowerCounty,
                StateCode = cd.StateCode,
                Zip = cd.ZipCode
            };
            return propertyInformation;
        }

        /// <summary>
        /// Set the questions and answers 
        /// </summary>
        /// <param name="cd"></param>
        /// <returns></returns>
        private RxOffice.ConversationType GetConversationDetails(xpr_get_dsa_loan_detailsResult cd)
        {
            var conversation = new RxOffice.ConversationType();

            string troublePayingMortgage = null;

            if (cd.DelinquencyAmount != null && (double)cd.DelinquencyAmount > 0)
            {
                troublePayingMortgage = "Yes";
            }
            else
            {
                troublePayingMortgage = "No";
            }

            var qaList = new List<RxOffice.QuenAnsType>();

            if (!string.IsNullOrWhiteSpace(cd.PropertyTaxPaidBy))
            {
                qaList.Add(GetQuestionAnswer(66, "Do you receive, and pay, the Real Estate Tax bill on your Home or does your lender pay it for you?", 16, cd.PropertyTaxPaidBy));
            }
            if (!string.IsNullOrWhiteSpace(cd.TaxesCurrent))
            {
                qaList.Add(GetQuestionAnswer(67, "Are the taxes current?", 2, cd.TaxesCurrent));
            }
            if (!string.IsNullOrWhiteSpace(cd.PolicyCurrent))
            {
                qaList.Add(GetQuestionAnswer(69, "Is the policy current?", 2, cd.PolicyCurrent));
            }
            if (!string.IsNullOrWhiteSpace(cd.ClientSituation))
            {
                qaList.Add(GetQuestionAnswer(78, "I believe that my situation is", 49, cd.ClientSituation));
            }
            if (!string.IsNullOrWhiteSpace(cd.BankruptcyFiled))
            {
                qaList.Add(GetQuestionAnswer(11, "Have you filed for bankruptcy?", 2, cd.BankruptcyFiled));
            }
            if (!string.IsNullOrWhiteSpace(cd.PlanToKeepHome))
            {
                qaList.Add(GetQuestionAnswer(20, "Do you want to keep or sell the property ?", 8, cd.PlanToKeepHome));
            }
            if (!string.IsNullOrWhiteSpace(cd.AmountClientOwe))
            {
                qaList.Add(GetQuestionAnswer(45, "Is the amount you owe on your first mortgage equal to or less than $729,750?", 2, cd.AmountClientOwe));
            }
            if (!string.IsNullOrWhiteSpace(cd.LoanOriginationDate))
            {
                qaList.Add(GetQuestionAnswer(46, "Did you get your current mortgage before January 1, 2009?", 2, cd.LoanOriginationDate));
            }
            if (!string.IsNullOrWhiteSpace(troublePayingMortgage))
            {
                qaList.Add(GetQuestionAnswer(51, "Are you having trouble paying your mortgage?", 2, troublePayingMortgage));
            }
            if (!string.IsNullOrWhiteSpace(cd.ForecloseNoticeReceived))
            {
                qaList.Add(GetQuestionAnswer(54, "Have you received a foreclosure notice from an attorney?", 2, cd.ForecloseNoticeReceived));
            }
            //qaList.Add(GetQuestionAnswer(55, "Is your property value less than your loan amount?", 2, cd));
            if (!string.IsNullOrWhiteSpace(cd.PropertyForSale))
            {
                qaList.Add(GetQuestionAnswer(59, "Is the property listed for sale?", 2, cd.PropertyForSale));
            }
            if (!string.IsNullOrWhiteSpace(cd.ForeclosureSaleScheduled))
            {
                qaList.Add(GetQuestionAnswer(60, "Is foreclosure sale scheduled?", 2, cd.ForeclosureSaleScheduled));
            }
            if (cd.ForeclosureSaleDate != null)
            {
                qaList.Add(GetQuestionAnswer(61, "What is foreclosure sale schedule date?", 18, ((DateTime)cd.ForeclosureSaleDate).ToString(DateFormat)));
            }
            if (!string.IsNullOrWhiteSpace(cd.BankruptcyChapter))
            {
                qaList.Add(GetQuestionAnswer(63, "What is the Bankruptcy chapter?", 15, cd.BankruptcyChapter));
            }
            if (cd.BankruptcyFileDate != null)
            {
                qaList.Add(GetQuestionAnswer(64, "What is the bankruptcy filed Date?", 18, ((DateTime)cd.BankruptcyFileDate).ToString(DateFormat)));
            }
            if (!string.IsNullOrWhiteSpace(cd.BankruptcyDischarged))
            {
                qaList.Add(GetQuestionAnswer(65, "Has your bankruptcy been discharged?", 2, cd.BankruptcyDischarged));
            }
            if (!string.IsNullOrWhiteSpace(cd.HoaFee))
            {
                qaList.Add(GetQuestionAnswer(73, "Do you pay Condominium or HOA Fee?", 2, cd.HoaFee));
            }
            if (!string.IsNullOrWhiteSpace(cd.PropertyType))
            {
                qaList.Add(GetQuestionAnswer(76, "Is the property your:", 20, cd.PropertyType));
            }
            if (!string.IsNullOrWhiteSpace(cd.WhoPaysHazardInsurance))
            {
                qaList.Add(GetQuestionAnswer(77, "Who Pays the hazard insurance policy for your property?", 25, cd.WhoPaysHazardInsurance));
            }

            conversation.ConversationDetails = qaList.ToArray();

            return conversation;
        }

        private RxOffice.QuenAnsType GetQuestionAnswer(int questionID, string question, int serialNumber, string answer)
        {
            var qa = new RxOffice.QuenAnsType()
            {
                QuestionID = questionID,
                Question = question,
                OptionSrNo = serialNumber,
                Answer = answer
            };
            return qa;
        }

        /// <summary>
        /// Sets the case type based on the dsa program and the referral code
        /// </summary>
        /// <param name="dsaProgram"></param>
        /// <param name="referralCode"></param>
        /// <returns></returns>
        private RxOffice.CaseTypeEnumType GetCaseType(int? dsaProgram, int? referralCode)
        {
            RxOffice.CaseTypeEnumType caseType = RxOffice.CaseTypeEnumType.doc;

            if (dsaProgram != null)
            {
                switch (dsaProgram)
                {
                    case FreddieMacProgram:
                        caseType = RxOffice.CaseTypeEnumType.frmdocmgmt;
                        break;
                    case WellsFargoProgram:
                        caseType = RxOffice.CaseTypeEnumType.weldocmgmt;
                        break;
                    case HPFfmacProgram:
                        //caseType = RxOffice.CaseTypeEnumType.doc; // ?? unable to find docmgmt
                        invalidDsaProgramMessage.AppendLine("For HPF-FMAC, please submit the document to Springboard.");
                        break;
                    case FannieMaeMhnProgram:
                        caseType = RxOffice.CaseTypeEnumType.fnmmhn;
                        break;
                    //case FannieMaeHpfProgram:
                      //  caseType = RxOffice.CaseTypeEnumType.fnmdocmgmt;
                       // break;
                    case HLPProgram:
                        caseType = RxOffice.CaseTypeEnumType.docmgmt;
                        break;
                      default:
                        requiredFieldsMessage.AppendLine("CaseType");
                        break;
                }
                return caseType;
            }
            return caseType;
        }

        /// <summary>
        /// This method is used to populate the header details for rxoffice service
        /// </summary>
        /// <returns></returns>
        private RxOffice.CaseInfoHeader GetHeaderInformation()
        {
            var header = new RxOffice.CaseInfoHeader()
            {
                HostSourceID = "cccsat",
                LoginID = "cccsatuser",
                SourceID = "cccsat",
                Password = "qIuVj1yyjcoW5W/fwBTUtg==",
                RecordCount = 1,
                RecordCountSpecified = true,
                TransactionID = "99"
            };
            return header;
        }
    }
}
