﻿#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DevExpress.XtraGrid.Views.Base
Imports DebtPlus.Svc.DataSets
Imports DebtPlus.Data.Forms
Imports DevExpress.XtraEditors
Imports DevExpress.XtraLayout
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo
Imports DebtPlus.LINQ
Imports System.Linq

Namespace Ticklers
    Public Class Form_Ticklers_ClientList
        Inherits Form_Ticklers_List

        Private ClientID As Int32

        ''' <summary>
        ''' Initialize the blank form
        ''' </summary>
        Public Sub New(ByVal ClientID As Int32)
            MyClass.New()
            RegisterEventHandlers()
            Me.ClientID = ClientID
        End Sub

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Private Sub RegisterEventHandlers()
            AddHandler Load, AddressOf Form_Ticklers_ClientList_Load
        End Sub

        Private Sub UnRegisterEventHandlers()
            RemoveHandler Load, AddressOf Form_Ticklers_ClientList_Load
        End Sub

        ''' <summary>
        ''' Handle the loading of the form
        ''' </summary>
        Private Sub Form_Ticklers_ClientList_Load(ByVal sender As Object, ByVal e As EventArgs)
            UnRegisterEventHandlers()

            Try
                chk_deleted.Checked = False
                LoadItems(False)
            Finally
                RegisterEventHandlers()
            End Try
        End Sub

        Protected Overrides Sub LoadItems(ByVal LoadDeleted As Boolean)

            ' Select the list of tickler items based upon today's date
            Using bc As New DebtPlus.LINQ.BusinessContext()
                Dim qry As System.Linq.IQueryable(Of tickler) = bc.ticklers.Where(Function(tk) tk.client = ClientID)
                If Not LoadDeleted Then
                    qry = qry.Where(Function(s) s.date_deleted Is Nothing)
                End If

                colItems = qry.ToList()
                GridControl1.DataSource = colItems
            End Using
        End Sub

        ''' <summary>
        ''' Edit the record
        ''' </summary>
        Protected Overrides Function EditRecord(ByVal record As tickler) As System.Windows.Forms.DialogResult
            Using frm As New Form_Tickler_Item(record)
                Return frm.ShowDialog()
            End Using
        End Function

        ''' <summary>
        ''' Process the CREATE menu click event
        ''' </summary>
        Protected Overrides Sub DoCreate()
            Dim record As tickler = DebtPlus.LINQ.Factory.Manufacture_tickler(ClientID)
            MyBase.DoCreate(record)
        End Sub

    End Class
End Namespace
