#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Explicit On
Option Strict On
Option Compare Binary

Imports DebtPlus.Data.Forms

Namespace Ticklers

    Friend Class Form_Ticklers_NewDate
        Inherits DebtPlusForm

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            RegisterEventHandlers()
        End Sub

        Public Property selection_date As DateTime

        Private Sub RegisterEventHandlers()
            AddHandler Load, AddressOf Form_Ticklers_NewDate_Load
            AddHandler MonthCalendar1.DateSelected, AddressOf MonthCalendar1_DateSelected
        End Sub

        Private Sub UnRegisterEventHandlers()
            RemoveHandler Load, AddressOf Form_Ticklers_NewDate_Load
            RemoveHandler MonthCalendar1.DateSelected, AddressOf MonthCalendar1_DateSelected
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents MonthCalendar1 As System.Windows.Forms.MonthCalendar
        Friend WithEvents Label1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton

        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager =
                    New System.ComponentModel.ComponentResourceManager(GetType(Form_Ticklers_NewDate))
            Me.MonthCalendar1 = New System.Windows.Forms.MonthCalendar()
            Me.Label1 = New DevExpress.XtraEditors.LabelControl()
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()

            Me.SuspendLayout()
            '
            'MonthCalendar1
            '
            Me.MonthCalendar1.Anchor = CType((((AnchorStyles.Top Or AnchorStyles.Bottom) _
                Or AnchorStyles.Left) _
                Or AnchorStyles.Right), AnchorStyles)
            Me.MonthCalendar1.Location = New System.Drawing.Point(7, 64)
            Me.MonthCalendar1.MaxSelectionCount = 1
            Me.MonthCalendar1.Name = "MonthCalendar1"
            Me.MonthCalendar1.TabIndex = 0
            '
            'Label1
            '
            Me.Label1.Anchor = CType(((AnchorStyles.Top Or AnchorStyles.Left) _
               Or AnchorStyles.Right), AnchorStyles)
            Me.Label1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.Label1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.Label1.Location = New System.Drawing.Point(10, 12)
            Me.Label1.Name = "Label1"
            Me.Label1.Size = New System.Drawing.Size(220, 40)
            Me.Label1.TabIndex = 1
            Me.Label1.Text = "Choose a new date for the cutoff of the ticklers. Ticklers prior to this date wil" & _
                             "l be shown."
            '
            'Button_OK
            '
            Me.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.Button_OK.Location = New System.Drawing.Point(83, 231)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.Size = New System.Drawing.Size(75, 24)
            Me.Button_OK.TabIndex = 2
            Me.Button_OK.Text = "&OK"
            Me.Button_OK.ToolTip = "Click here to accept the date"
            Me.Button_OK.ToolTipController = Me.ToolTipController1
            '
            'Form_Ticklers_NewDate
            '
            Me.AcceptButton = Me.Button_OK
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
            Me.CancelButton = Me.Button_OK
            Me.ClientSize = New System.Drawing.Size(240, 267)
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.Label1)
            Me.Controls.Add(Me.MonthCalendar1)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
            Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
            Me.MaximizeBox = False
            Me.MinimizeBox = False
            Me.Name = "Form_Ticklers_NewDate"
            Me.Text = "Cutoff Date"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()

            Me.ResumeLayout(False)
        End Sub

#End Region

        Private Sub Form_Ticklers_NewDate_Load(ByVal sender As Object, ByVal e As EventArgs)

            UnRegisterEventHandlers()
            Try
                selection_date = DateTime.Now.Date
                MonthCalendar1.TodayDate = selection_date
                MonthCalendar1.SelectionStart = selection_date
            Finally
                RegisterEventHandlers()
            End Try
        End Sub

        Private Sub MonthCalendar1_DateSelected(ByVal sender As Object, ByVal e As DateRangeEventArgs)
            selection_date = MonthCalendar1.SelectionStart().Date
        End Sub
    End Class
End NameSpace
