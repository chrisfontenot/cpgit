#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.Svc.DataSets
Imports DebtPlus.Data.Forms
Imports DevExpress.XtraEditors
Imports DebtPlus.LINQ
Imports System.Linq

Namespace Ticklers

    Friend Class Form_Tickler_Item
        Inherits DebtPlusForm

        Private record As DebtPlus.LINQ.tickler = Nothing

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(ByVal record As tickler)
            MyClass.New()
            Me.record = record

            ' Load Lookup Entries
            lk_Ticker_Type.Properties.DataSource = DebtPlus.LINQ.Cache.TicklerType.getList()
            lk_Counselor.Properties.DataSource = DebtPlus.LINQ.Cache.counselor.getList().FindAll(Function(s) s.ActiveFlag).ToList()

            RegisterEventHandlers()
        End Sub

        Private Sub RegisterEventHandlers()
            AddHandler Button_Delete.Click, AddressOf Button_Delete_Click
            AddHandler Button_OK.Click, AddressOf Button_OK_Click
            AddHandler ClientID1.EditValueChanged, AddressOf FormChanged
            AddHandler dt_effective.EditValueChanged, AddressOf FormChanged
            AddHandler dt_effective.EditValueChanging, AddressOf dt_effective_EditValueChanging
            AddHandler lk_Counselor.EditValueChanged, AddressOf FormChanged
            AddHandler lk_Counselor.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler lk_Ticker_Type.EditValueChanged, AddressOf FormChanged
            AddHandler lk_Ticker_Type.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler Load, AddressOf Form_Tickler_Item_Load
            AddHandler med_note.EditValueChanged, AddressOf FormChanged
            AddHandler spn_Priority.EditValueChanged, AddressOf FormChanged
        End Sub

        Private Sub UnRegisterEventHandlers()
            RemoveHandler Button_Delete.Click, AddressOf Button_Delete_Click
            RemoveHandler Button_OK.Click, AddressOf Button_OK_Click
            RemoveHandler ClientID1.EditValueChanged, AddressOf FormChanged
            RemoveHandler dt_effective.EditValueChanged, AddressOf FormChanged
            RemoveHandler dt_effective.EditValueChanging, AddressOf dt_effective_EditValueChanging
            RemoveHandler lk_Counselor.EditValueChanged, AddressOf FormChanged
            RemoveHandler lk_Counselor.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler lk_Ticker_Type.EditValueChanged, AddressOf FormChanged
            RemoveHandler lk_Ticker_Type.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler Load, AddressOf Form_Tickler_Item_Load
            RemoveHandler med_note.EditValueChanged, AddressOf FormChanged
            RemoveHandler spn_Priority.EditValueChanged, AddressOf FormChanged
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then
                        components.Dispose()
                    End If
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer = Nothing

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents Label1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Label2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Label3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents spn_Priority As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents Label4 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Label5 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Label6 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents med_note As DevExpress.XtraEditors.MemoEdit
        Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Client As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Delete As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents dt_effective As DevExpress.XtraEditors.DateEdit
        Friend WithEvents lbl_deleted As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lk_Counselor As LookUpEdit
        Friend WithEvents ClientID1 As DebtPlus.UI.Client.Widgets.Controls.ClientID
        Friend WithEvents lk_Ticker_Type As LookUpEdit

        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_Tickler_Item))
            Me.Label1 = New DevExpress.XtraEditors.LabelControl()
            Me.Label2 = New DevExpress.XtraEditors.LabelControl()
            Me.dt_effective = New DevExpress.XtraEditors.DateEdit()
            Me.Label3 = New DevExpress.XtraEditors.LabelControl()
            Me.spn_Priority = New DevExpress.XtraEditors.SpinEdit()
            Me.Label4 = New DevExpress.XtraEditors.LabelControl()
            Me.Label5 = New DevExpress.XtraEditors.LabelControl()
            Me.Label6 = New DevExpress.XtraEditors.LabelControl()
            Me.med_note = New DevExpress.XtraEditors.MemoEdit()
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton()
            Me.Button_Client = New DevExpress.XtraEditors.SimpleButton()
            Me.Button_Delete = New DevExpress.XtraEditors.SimpleButton()
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton()
            Me.lbl_deleted = New DevExpress.XtraEditors.LabelControl()
            Me.lk_Counselor = New DevExpress.XtraEditors.LookUpEdit()
            Me.lk_Ticker_Type = New DevExpress.XtraEditors.LookUpEdit()
            Me.ClientID1 = New DebtPlus.UI.Client.Widgets.Controls.ClientID()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_effective.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_effective.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.spn_Priority.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.med_note.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lk_Counselor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lk_Ticker_Type.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ClientID1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'Label1
            '
            Me.Label1.Location = New System.Drawing.Point(8, 9)
            Me.Label1.Name = "Label1"
            Me.Label1.Size = New System.Drawing.Size(45, 13)
            Me.Label1.TabIndex = 0
            Me.Label1.Text = "Client ID:"
            Me.Label1.ToolTipController = Me.ToolTipController1
            '
            'Label2
            '
            Me.Label2.Location = New System.Drawing.Point(8, 34)
            Me.Label2.Name = "Label2"
            Me.Label2.Size = New System.Drawing.Size(73, 13)
            Me.Label2.TabIndex = 4
            Me.Label2.Text = "Effective Date:"
            Me.Label2.ToolTipController = Me.ToolTipController1
            '
            'dt_effective
            '
            Me.dt_effective.EditValue = New Date(2003, 10, 31, 0, 0, 0, 0)
            Me.dt_effective.Location = New System.Drawing.Point(88, 33)
            Me.dt_effective.Name = "dt_effective"
            Me.dt_effective.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.dt_effective.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.dt_effective.Size = New System.Drawing.Size(100, 20)
            Me.dt_effective.TabIndex = 5
            Me.dt_effective.ToolTipController = Me.ToolTipController1
            '
            'Label3
            '
            Me.Label3.Location = New System.Drawing.Point(267, 36)
            Me.Label3.Name = "Label3"
            Me.Label3.Size = New System.Drawing.Size(38, 13)
            Me.Label3.TabIndex = 6
            Me.Label3.Text = "Priority:"
            Me.Label3.ToolTipController = Me.ToolTipController1
            '
            'spn_Priority
            '
            Me.spn_Priority.EditValue = New Decimal(New Integer() {9, 0, 0, 0})
            Me.spn_Priority.Location = New System.Drawing.Point(311, 33)
            Me.spn_Priority.Name = "spn_Priority"
            Me.spn_Priority.Properties.Appearance.Options.UseTextOptions = True
            Me.spn_Priority.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.spn_Priority.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.spn_Priority.Properties.IsFloatValue = False
            Me.spn_Priority.Properties.MaxLength = 1
            Me.spn_Priority.Properties.MaxValue = New Decimal(New Integer() {9, 0, 0, 0})
            Me.spn_Priority.Size = New System.Drawing.Size(32, 20)
            Me.spn_Priority.TabIndex = 7
            Me.spn_Priority.ToolTipController = Me.ToolTipController1
            '
            'Label4
            '
            Me.Label4.Location = New System.Drawing.Point(8, 60)
            Me.Label4.Name = "Label4"
            Me.Label4.Size = New System.Drawing.Size(52, 13)
            Me.Label4.TabIndex = 8
            Me.Label4.Text = "Counselor:"
            Me.Label4.ToolTipController = Me.ToolTipController1
            '
            'Label5
            '
            Me.Label5.Location = New System.Drawing.Point(8, 86)
            Me.Label5.Name = "Label5"
            Me.Label5.Size = New System.Drawing.Size(28, 13)
            Me.Label5.TabIndex = 10
            Me.Label5.Text = "Type:"
            Me.Label5.ToolTipController = Me.ToolTipController1
            '
            'Label6
            '
            Me.Label6.Location = New System.Drawing.Point(8, 112)
            Me.Label6.Name = "Label6"
            Me.Label6.Size = New System.Drawing.Size(27, 13)
            Me.Label6.TabIndex = 12
            Me.Label6.Text = "Note:"
            Me.Label6.ToolTipController = Me.ToolTipController1
            '
            'med_note
            '
            Me.med_note.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.med_note.EditValue = ""
            Me.med_note.Location = New System.Drawing.Point(88, 112)
            Me.med_note.Name = "med_note"
            Me.med_note.Properties.MaxLength = 256
            Me.med_note.Size = New System.Drawing.Size(255, 103)
            Me.med_note.TabIndex = 13
            Me.med_note.ToolTipController = Me.ToolTipController1
            '
            'Button_OK
            '
            Me.Button_OK.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.Button_OK.Location = New System.Drawing.Point(351, 9)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.Size = New System.Drawing.Size(75, 24)
            Me.Button_OK.TabIndex = 14
            Me.Button_OK.Text = "&OK"
            Me.Button_OK.ToolTipController = Me.ToolTipController1
            '
            'Button_Client
            '
            Me.Button_Client.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_Client.Location = New System.Drawing.Point(351, 146)
            Me.Button_Client.Name = "Button_Client"
            Me.Button_Client.Size = New System.Drawing.Size(75, 25)
            Me.Button_Client.TabIndex = 0
            Me.Button_Client.Text = "Client..."
            Me.Button_Client.ToolTip = "Show the indicated Client"
            Me.Button_Client.ToolTipController = Me.ToolTipController1
            Me.Button_Client.Visible = False
            '
            'Button_Delete
            '
            Me.Button_Delete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_Delete.Location = New System.Drawing.Point(351, 112)
            Me.Button_Delete.Name = "Button_Delete"
            Me.Button_Delete.Size = New System.Drawing.Size(75, 25)
            Me.Button_Delete.TabIndex = 16
            Me.Button_Delete.Text = "&Delete"
            Me.Button_Delete.ToolTipController = Me.ToolTipController1
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Button_Cancel.Location = New System.Drawing.Point(351, 43)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 25)
            Me.Button_Cancel.TabIndex = 15
            Me.Button_Cancel.Text = "&Cancel"
            Me.Button_Cancel.ToolTipController = Me.ToolTipController1
            '
            'lbl_deleted
            '
            Me.lbl_deleted.Appearance.BackColor = System.Drawing.Color.Red
            Me.lbl_deleted.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.lbl_deleted.Appearance.ForeColor = System.Drawing.Color.White
            Me.lbl_deleted.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.lbl_deleted.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
            Me.lbl_deleted.Location = New System.Drawing.Point(210, 13)
            Me.lbl_deleted.Name = "lbl_deleted"
            Me.lbl_deleted.Size = New System.Drawing.Size(133, 12)
            Me.lbl_deleted.TabIndex = 3
            Me.lbl_deleted.Text = " DELETED ON 88/88/8888 "
            Me.lbl_deleted.UseMnemonic = False
            Me.lbl_deleted.Visible = False
            '
            'lk_Counselor
            '
            Me.lk_Counselor.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lk_Counselor.Location = New System.Drawing.Point(88, 60)
            Me.lk_Counselor.Name = "lk_Counselor"
            Me.lk_Counselor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lk_Counselor.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Note", 10, "Note")})
            Me.lk_Counselor.Properties.DisplayMember = "Name"
            Me.lk_Counselor.Properties.NullText = ""
            Me.lk_Counselor.Properties.ShowFooter = False
            Me.lk_Counselor.Properties.SortColumnIndex = 1
            Me.lk_Counselor.Properties.ValueMember = "Id"
            Me.lk_Counselor.Size = New System.Drawing.Size(255, 20)
            Me.lk_Counselor.TabIndex = 9
            '
            'lk_Ticker_Type
            '
            Me.lk_Ticker_Type.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lk_Ticker_Type.Location = New System.Drawing.Point(88, 87)
            Me.lk_Ticker_Type.Name = "lk_Ticker_Type"
            Me.lk_Ticker_Type.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lk_Ticker_Type.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.lk_Ticker_Type.Properties.DisplayMember = "description"
            Me.lk_Ticker_Type.Properties.NullText = ""
            Me.lk_Ticker_Type.Properties.ShowFooter = False
            Me.lk_Ticker_Type.Properties.ShowHeader = False
            Me.lk_Ticker_Type.Properties.SortColumnIndex = 1
            Me.lk_Ticker_Type.Properties.ValueMember = "Id"
            Me.lk_Ticker_Type.Size = New System.Drawing.Size(255, 20)
            Me.lk_Ticker_Type.TabIndex = 11
            '
            'ClientID1
            '
            Me.ClientID1.AllowDrop = True
            Me.ClientID1.Location = New System.Drawing.Point(88, 9)
            Me.ClientID1.Name = "ClientID1"
            Me.ClientID1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.ClientID1.Properties.Appearance.Options.UseTextOptions = True
            Me.ClientID1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.ClientID1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DebtPlus.UI.Common.Controls.SearchButton()})
            Me.ClientID1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.ClientID1.Properties.DisplayFormat.FormatString = "0000000"
            Me.ClientID1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.ClientID1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.ClientID1.Properties.Mask.BeepOnError = True
            Me.ClientID1.Properties.Mask.EditMask = "\d*"
            Me.ClientID1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.ClientID1.Properties.ValidateOnEnterKey = True
            Me.ClientID1.Size = New System.Drawing.Size(100, 20)
            Me.ClientID1.TabIndex = 1
            '
            'Form_Tickler_Item
            '
            Me.AcceptButton = Me.Button_OK
            Me.CancelButton = Me.Button_Cancel
            Me.ClientSize = New System.Drawing.Size(431, 230)
            Me.Controls.Add(Me.ClientID1)
            Me.Controls.Add(Me.lk_Ticker_Type)
            Me.Controls.Add(Me.lk_Counselor)
            Me.Controls.Add(Me.lbl_deleted)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_Delete)
            Me.Controls.Add(Me.Button_Client)
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.med_note)
            Me.Controls.Add(Me.Label6)
            Me.Controls.Add(Me.Label5)
            Me.Controls.Add(Me.Label4)
            Me.Controls.Add(Me.spn_Priority)
            Me.Controls.Add(Me.Label3)
            Me.Controls.Add(Me.dt_effective)
            Me.Controls.Add(Me.Label2)
            Me.Controls.Add(Me.Label1)
            Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
            Me.Name = "Form_Tickler_Item"
            Me.Text = "Edit Tickler Item"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_effective.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_effective.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.spn_Priority.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.med_note.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lk_Counselor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lk_Ticker_Type.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ClientID1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

#End Region

        ''' <summary>
        ''' Process the form load event
        ''' </summary>
        Private Sub Form_Tickler_Item_Load(ByVal sender As Object, ByVal e As EventArgs)

            UnRegisterEventHandlers()
            Try
                ClientID1.EditValue = record.client
                lk_Counselor.EditValue = record.counselor
                lk_Ticker_Type.EditValue = record.tickler_type
                spn_Priority.EditValue = record.priority
                dt_effective.EditValue = record.date_effective
                med_note.EditValue = record.note

                ' If there is a client then do not permit it to be changed
                ClientID1.Enabled = record.client <= 0

                ' Process a record that has not been deleted
                If Not record.date_deleted.HasValue Then
                    lbl_deleted.Visible = False
                    Button_Delete.Enabled = record.Id > 0
                    Button_OK.Enabled = Not HasErrors()

                Else
                    ' Deleted records are for display only.
                    lbl_deleted.Text = String.Format("DELETED ON {0:d}", record.date_deleted.Value)
                    lbl_deleted.Visible = True

                    spn_Priority.Enabled = False
                    med_note.Enabled = False
                    dt_effective.Enabled = False
                    lk_Counselor.Enabled = False
                    lk_Ticker_Type.Enabled = False
                    ClientID1.Enabled = False

                    Button_Delete.Enabled = False
                    Button_OK.Enabled = False
                End If

            Finally
                RegisterEventHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Process the delete button
        ''' </summary>
        Private Sub Button_Delete_Click(ByVal sender As Object, ByVal e As EventArgs)
            record.date_deleted = BusinessContext.getdate()
            DialogResult = Windows.Forms.DialogResult.OK
        End Sub

        ''' <summary>
        ''' Process the OK button CLICK event
        ''' </summary>
        Private Sub Button_OK_Click(ByVal sender As Object, ByVal e As EventArgs)

            ' Retrieve the values when the OK button is clicked
            record.client = DebtPlus.Utils.Nulls.v_Int32(ClientID1.EditValue).GetValueOrDefault()
            record.counselor = DebtPlus.Utils.Nulls.v_Int32(lk_Counselor.EditValue)
            record.tickler_type = Convert.ToInt32(lk_Ticker_Type.EditValue)
            record.priority = Convert.ToInt32(spn_Priority.EditValue)
            record.date_effective = Convert.ToDateTime(dt_effective.EditValue)
            record.note = DebtPlus.Utils.Nulls.v_String(med_note.EditValue)

        End Sub

        ''' <summary>
        ''' Ensure that the effective date is valid
        ''' </summary>
        Private Sub dt_effective_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)
            If e.NewValue IsNot Nothing Then
                If Convert.ToDateTime(e.NewValue).Date < DateTime.Now.Date Then
                    e.Cancel = True
                End If
            End If
        End Sub

        ''' <summary>
        ''' When fields are changed ENABLE/DISABLE the OK button
        ''' </summary>
        Private Sub FormChanged(ByVal Sender As Object, ByVal e As EventArgs)
            Button_OK.Enabled = Not HasErrors()
        End Sub

        ''' <summary>
        ''' Determine if there is an error pending on the form
        ''' </summary>
        Private Function HasErrors() As Boolean

            If DebtPlus.Utils.Nulls.v_Int32(lk_Ticker_Type.EditValue).GetValueOrDefault() <= 0 Then
                lk_Ticker_Type.ErrorText = "A value is required"
                Return True
            End If
            lk_Ticker_Type.ErrorText = String.Empty

            If DebtPlus.Utils.Nulls.v_Int32(lk_Counselor.EditValue).GetValueOrDefault() <= 0 Then
                lk_Counselor.ErrorText = "A value is required"
                Return True
            End If
            lk_Counselor.ErrorText = String.Empty

            If DebtPlus.Utils.Nulls.v_Int32(ClientID1.EditValue).GetValueOrDefault() <= 0 Then
                ClientID1.ErrorText = "A value is required"
                Return True
            End If
            ClientID1.ErrorText = String.Empty

            ' There is no error. Return the status correctly.
            Return False
        End Function
    End Class
End NameSpace
