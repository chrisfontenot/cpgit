#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DevExpress.XtraGrid.Views.Base
Imports DebtPlus.Svc.DataSets
Imports DebtPlus.Data.Forms
Imports DevExpress.XtraEditors
Imports DevExpress.XtraLayout
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo
Imports DebtPlus.LINQ
Imports System.Linq

Namespace Ticklers

    Public Class Form_Ticklers_List
        Inherits DebtPlusForm

        Protected colItems As System.Collections.Generic.List(Of DebtPlus.LINQ.tickler)

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            col_counselor_name.DisplayFormat.Format = New CounselorFormatter()
            col_counselor_name.GroupFormat.Format = New CounselorFormatter()
            col_description.DisplayFormat.Format = New TicklerDescriptionFormatter()
            col_description.GroupFormat.Format = New TicklerDescriptionFormatter()

            RegisterEventHandlers()
        End Sub

        Private Sub RegisterEventHandlers()
            AddHandler Button_OK.Click, AddressOf Button_OK_Click
            AddHandler GridView1.Click, AddressOf GridView1_Click
            AddHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
            AddHandler GridView1.FocusedRowChanged, AddressOf GridView1_FocusedRowChanged
            AddHandler chk_deleted.CheckedChanged, AddressOf chk_deleted_CheckedChanged
            AddHandler ContextMenu1.Popup, AddressOf ContextMenu1_Popup
            AddHandler mnu_edit.Click, AddressOf mnu_edit_Click
            AddHandler mnuCreate.Click, AddressOf mnuCreate_Click
            AddHandler mnuDelete.Click, AddressOf mnuDelete_Click
            AddHandler MenuItem_Exit.Click, AddressOf MenuItem_Exit_Click
            AddHandler MenuItem_Print.Click, AddressOf MenuItem_Print_Click
        End Sub

        Private Sub UnRegisterEventHandlers()
            RemoveHandler Button_OK.Click, AddressOf Button_OK_Click
            RemoveHandler GridView1.Click, AddressOf GridView1_Click
            RemoveHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
            RemoveHandler GridView1.FocusedRowChanged, AddressOf GridView1_FocusedRowChanged
            RemoveHandler chk_deleted.CheckedChanged, AddressOf chk_deleted_CheckedChanged
            RemoveHandler ContextMenu1.Popup, AddressOf ContextMenu1_Popup
            RemoveHandler mnu_edit.Click, AddressOf mnu_edit_Click
            RemoveHandler mnuCreate.Click, AddressOf mnuCreate_Click
            RemoveHandler mnuDelete.Click, AddressOf mnuDelete_Click
            RemoveHandler MenuItem_Exit.Click, AddressOf MenuItem_Exit_Click
            RemoveHandler MenuItem_Print.Click, AddressOf MenuItem_Print_Click
        End Sub

        Protected Overridable Function EditRecord(ByVal record As tickler) As System.Windows.Forms.DialogResult
            Return Windows.Forms.DialogResult.Cancel
        End Function

        Protected Overridable Sub LoadItems(ByVal LoadDeleted As Boolean)
        End Sub

        ''' <summary>
        ''' Create a new tickler for the client
        ''' </summary>
        Protected Overridable Sub DoCreate()
        End Sub

        ''' <summary>
        ''' Do a creation event for the new record
        ''' </summary>
        Protected Overridable Sub DoCreate(ByVal record As tickler)

            ' Edit the new record
            If EditRecord(record) <> Windows.Forms.DialogResult.OK Then
                Return
            End If

            ' Don't bother to create items that are marked "DELETED"
            If record.date_deleted.HasValue Then
                Return
            End If

            ' Find the original counselor in the item
            If record.counselor.HasValue Then
                Dim q As DebtPlus.LINQ.counselor = DebtPlus.LINQ.Cache.counselor.getList().Find(Function(s) s.Id = record.counselor.Value)
                If q IsNot Nothing Then
                    record.original_counselor = q.Person
                End If
            End If

            ' Force the listing in the grid even if the dates are incorrect or for a different counselor
            record.ShowInGrid = True

            ' Update the database with the new tickler
            Using bc As New BusinessContext()
                bc.ticklers.InsertOnSubmit(record)
                bc.SubmitChanges()

                ' Update the list
                colItems.Add(record)
                GridControl1.RefreshDataSource()
                GridView1.RefreshData()
            End Using
        End Sub

        ''' <summary>
        ''' Find the counselor ID for the current counselor
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function DefaultCounselor() As Int32
            Dim counselorName As String = DebtPlus.LINQ.BusinessContext.suser_sname
            Dim counselorRecord As DebtPlus.LINQ.counselor = DebtPlus.LINQ.Cache.counselor.getList().Find(Function(s) String.Compare(s.Person, counselorName, True) = 0)
            If counselorRecord IsNot Nothing Then
                Return counselorRecord.Id
            End If

            Return -1
        End Function

        ''' <summary>
        ''' Edit the current item from the grid
        ''' </summary>
        Protected Sub DoEdit(ByVal obj As Object)
            Dim record As tickler = TryCast(obj, tickler)
            If record Is Nothing Then
                Return
            End If

            If EditRecord(record) <> Windows.Forms.DialogResult.OK Then
                Return
            End If

            Using bc As New BusinessContext()
                Dim q As tickler = bc.ticklers.Where(Function(tk) tk.Id = record.Id).FirstOrDefault()
                If q IsNot Nothing Then
                    q.counselor = record.counselor
                    q.client = record.client
                    q.date_deleted = record.date_deleted
                    q.date_effective = record.date_effective
                    q.note = record.note
                    q.original_counselor = record.original_counselor
                    q.priority = record.priority
                    q.ShowInGrid = record.ShowInGrid
                    q.tickler_type = record.tickler_type

                    ' Post the changes to the database
                    bc.SubmitChanges()

                    ' If the item is now deleted then remove it from the list of displayed items.
                    If record.date_deleted.HasValue Then
                        colItems.Remove(record)
                    End If

                    GridControl1.RefreshDataSource()
                    GridView1.RefreshData()
                End If
            End Using
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.

        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_Ticklers_List))
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
            Me.ContextMenu1 = New System.Windows.Forms.ContextMenu()
            Me.mnuCreate = New System.Windows.Forms.MenuItem()
            Me.mnu_edit = New System.Windows.Forms.MenuItem()
            Me.mnuDelete = New System.Windows.Forms.MenuItem()
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
            Me.col_date_effective = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.col_counselor_name = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.col_priority = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.col_description = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.col_date_created = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.col_counselor = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.col_date_deleted = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.col_id = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.col_tickler_type = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.col_client = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.chk_deleted = New DevExpress.XtraEditors.CheckEdit()
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton()
            Me.Button_Show = New DevExpress.XtraEditors.SimpleButton()
            Me.Button_Preview = New DevExpress.XtraEditors.SimpleButton()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
            Me.MenuItem1 = New System.Windows.Forms.MenuItem()
            Me.MenuItem_Print = New System.Windows.Forms.MenuItem()
            Me.MenuItem_Exit = New System.Windows.Forms.MenuItem()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.chk_deleted.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridControl1
            '
            Me.GridControl1.ContextMenu = Me.ContextMenu1
            Me.GridControl1.Location = New System.Drawing.Point(12, 12)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(440, 384)
            Me.GridControl1.TabIndex = 0
            Me.GridControl1.ToolTipController = Me.ToolTipController1
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'ContextMenu1
            '
            Me.ContextMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuCreate, Me.mnu_edit, Me.mnuDelete})
            '
            'mnuCreate
            '
            Me.mnuCreate.Index = 0
            Me.mnuCreate.Text = "&Add..."
            '
            'mnu_edit
            '
            Me.mnu_edit.Index = 1
            Me.mnu_edit.Text = "&Change..."
            '
            'mnuDelete
            '
            Me.mnuDelete.Index = 2
            Me.mnuDelete.Text = "&Delete..."
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(195, Byte), Integer))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(189, Byte), Integer))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(206, Byte), Integer))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(225, Byte), Integer))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(215, Byte), Integer))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(215, Byte), Integer))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(215, Byte), Integer))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.col_date_effective, Me.col_counselor_name, Me.col_priority, Me.col_description, Me.col_date_created, Me.col_counselor, Me.col_date_deleted, Me.col_id, Me.col_tickler_type, Me.col_client})
            Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsView.AutoCalcPreviewLineCount = True
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            Me.GridView1.OptionsView.ShowPreview = True
            Me.GridView1.PreviewFieldName = "note"
            Me.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedRow
            '
            'col_date_effective
            '
            Me.col_date_effective.Caption = "Date"
            Me.col_date_effective.CustomizationCaption = "Effective Date"
            Me.col_date_effective.DisplayFormat.FormatString = "d"
            Me.col_date_effective.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.col_date_effective.FieldName = "date_effective"
            Me.col_date_effective.Name = "col_date_effective"
            Me.col_date_effective.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'col_counselor_name
            '
            Me.col_counselor_name.AppearanceCell.Options.UseTextOptions = True
            Me.col_counselor_name.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.col_counselor_name.AppearanceHeader.Options.UseTextOptions = True
            Me.col_counselor_name.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.col_counselor_name.Caption = "Counselor"
            Me.col_counselor_name.CustomizationCaption = "Counselor Name"
            Me.col_counselor_name.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.col_counselor_name.FieldName = "counselor"
            Me.col_counselor_name.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.col_counselor_name.Name = "col_counselor_name"
            Me.col_counselor_name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.col_counselor_name.Width = 119
            '
            'col_priority
            '
            Me.col_priority.Caption = "P"
            Me.col_priority.CustomizationCaption = "Priority Level"
            Me.col_priority.DisplayFormat.FormatString = "f0"
            Me.col_priority.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_priority.FieldName = "priority"
            Me.col_priority.GroupFormat.FormatString = "f0"
            Me.col_priority.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_priority.Name = "col_priority"
            Me.col_priority.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.col_priority.Width = 47
            '
            'col_description
            '
            Me.col_description.AppearanceCell.Options.UseTextOptions = True
            Me.col_description.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.col_description.AppearanceHeader.Options.UseTextOptions = True
            Me.col_description.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.col_description.Caption = "Description"
            Me.col_description.CustomizationCaption = "Item Description"
            Me.col_description.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.col_description.FieldName = "tickler_type"
            Me.col_description.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.col_description.Name = "col_description"
            Me.col_description.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.col_description.Width = 280
            '
            'col_date_created
            '
            Me.col_date_created.Caption = "Created"
            Me.col_date_created.CustomizationCaption = "Date Created"
            Me.col_date_created.DisplayFormat.FormatString = "d"
            Me.col_date_created.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.col_date_created.FieldName = "date_created"
            Me.col_date_created.Name = "col_date_created"
            Me.col_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.col_date_created.Width = 88
            '
            'col_counselor
            '
            Me.col_counselor.AppearanceCell.Options.UseTextOptions = True
            Me.col_counselor.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.col_counselor.AppearanceHeader.Options.UseTextOptions = True
            Me.col_counselor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.col_counselor.Caption = "Counselor ID"
            Me.col_counselor.CustomizationCaption = "Counselor ID"
            Me.col_counselor.DisplayFormat.FormatString = "f0"
            Me.col_counselor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_counselor.FieldName = "counselor"
            Me.col_counselor.GroupFormat.FormatString = "f0"
            Me.col_counselor.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_counselor.Name = "col_counselor"
            Me.col_counselor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'col_date_deleted
            '
            Me.col_date_deleted.Caption = "Deleted"
            Me.col_date_deleted.CustomizationCaption = "Date Deleted"
            Me.col_date_deleted.DisplayFormat.FormatString = "d"
            Me.col_date_deleted.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.col_date_deleted.FieldName = "date_deleted"
            Me.col_date_deleted.Name = "col_date_deleted"
            Me.col_date_deleted.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'col_id
            '
            Me.col_id.Caption = "ID"
            Me.col_id.CustomizationCaption = "Record ID"
            Me.col_id.DisplayFormat.FormatString = "f0"
            Me.col_id.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_id.FieldName = "tickler"
            Me.col_id.Name = "col_id"
            Me.col_id.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'col_tickler_type
            '
            Me.col_tickler_type.AppearanceCell.Options.UseTextOptions = True
            Me.col_tickler_type.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.col_tickler_type.AppearanceHeader.Options.UseTextOptions = True
            Me.col_tickler_type.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.col_tickler_type.Caption = "Type Code"
            Me.col_tickler_type.CustomizationCaption = "Tickler Type Code"
            Me.col_tickler_type.DisplayFormat.FormatString = "f0"
            Me.col_tickler_type.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_tickler_type.FieldName = "tickler_type"
            Me.col_tickler_type.Name = "col_tickler_type"
            Me.col_tickler_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'col_client
            '
            Me.col_client.Caption = "Client"
            Me.col_client.CustomizationCaption = "Client ID"
            Me.col_client.DisplayFormat.FormatString = "0000000"
            Me.col_client.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_client.FieldName = "client"
            Me.col_client.Name = "col_client"
            Me.col_client.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.col_client.Width = 97
            '
            'Button_OK
            '
            Me.Button_OK.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_OK.Location = New System.Drawing.Point(456, 12)
            Me.Button_OK.MaximumSize = New System.Drawing.Size(75, 25)
            Me.Button_OK.MinimumSize = New System.Drawing.Size(75, 25)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.Size = New System.Drawing.Size(75, 25)
            Me.Button_OK.StyleController = Me.LayoutControl1
            Me.Button_OK.TabIndex = 1
            Me.Button_OK.Text = "&Edit"
            Me.Button_OK.ToolTipController = Me.ToolTipController1
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.chk_deleted)
            Me.LayoutControl1.Controls.Add(Me.GridControl1)
            Me.LayoutControl1.Controls.Add(Me.Button_OK)
            Me.LayoutControl1.Controls.Add(Me.Button_Cancel)
            Me.LayoutControl1.Controls.Add(Me.Button_Show)
            Me.LayoutControl1.Controls.Add(Me.Button_Preview)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(543, 408)
            Me.LayoutControl1.TabIndex = 6
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'chk_deleted
            '
            Me.chk_deleted.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.chk_deleted.Location = New System.Drawing.Point(456, 377)
            Me.chk_deleted.Name = "chk_deleted"
            Me.chk_deleted.Properties.Caption = "Deleted"
            Me.chk_deleted.Size = New System.Drawing.Size(75, 19)
            Me.chk_deleted.StyleController = Me.LayoutControl1
            Me.chk_deleted.TabIndex = 7
            Me.chk_deleted.ToolTipController = Me.ToolTipController1
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Button_Cancel.Location = New System.Drawing.Point(456, 41)
            Me.Button_Cancel.MaximumSize = New System.Drawing.Size(75, 25)
            Me.Button_Cancel.MinimumSize = New System.Drawing.Size(75, 25)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 25)
            Me.Button_Cancel.StyleController = Me.LayoutControl1
            Me.Button_Cancel.TabIndex = 2
            Me.Button_Cancel.Text = "&Cancel"
            Me.Button_Cancel.ToolTipController = Me.ToolTipController1
            '
            'Button_Show
            '
            Me.Button_Show.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_Show.Location = New System.Drawing.Point(456, 99)
            Me.Button_Show.MaximumSize = New System.Drawing.Size(75, 25)
            Me.Button_Show.MinimumSize = New System.Drawing.Size(75, 25)
            Me.Button_Show.Name = "Button_Show"
            Me.Button_Show.Size = New System.Drawing.Size(75, 25)
            Me.Button_Show.StyleController = Me.LayoutControl1
            Me.Button_Show.TabIndex = 3
            Me.Button_Show.Text = "Show..."
            Me.Button_Show.ToolTipController = Me.ToolTipController1
            Me.Button_Show.Visible = False
            '
            'Button_Preview
            '
            Me.Button_Preview.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_Preview.Location = New System.Drawing.Point(456, 70)
            Me.Button_Preview.MaximumSize = New System.Drawing.Size(75, 25)
            Me.Button_Preview.MinimumSize = New System.Drawing.Size(75, 25)
            Me.Button_Preview.Name = "Button_Preview"
            Me.Button_Preview.Size = New System.Drawing.Size(75, 25)
            Me.Button_Preview.StyleController = Me.LayoutControl1
            Me.Button_Preview.TabIndex = 4
            Me.Button_Preview.Text = "Preview..."
            Me.Button_Preview.ToolTipController = Me.ToolTipController1
            Me.Button_Preview.Visible = False
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "Root"
            Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup1.GroupBordersVisible = False
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem5, Me.LayoutControlItem4, Me.LayoutControlItem6, Me.EmptySpaceItem1})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "Root"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(543, 408)
            Me.LayoutControlGroup1.Text = "Root"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.GridControl1
            Me.LayoutControlItem1.CustomizationFormText = "Grid Control"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(444, 388)
            Me.LayoutControlItem1.Text = "Grid Control"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem1.TextToControlDistance = 0
            Me.LayoutControlItem1.TextVisible = False
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.Button_OK
            Me.LayoutControlItem2.CustomizationFormText = "EDIT Button"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(444, 0)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(79, 29)
            Me.LayoutControlItem2.Text = "EDIT Button"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem2.TextToControlDistance = 0
            Me.LayoutControlItem2.TextVisible = False
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.Button_Cancel
            Me.LayoutControlItem3.CustomizationFormText = "CANCEL Button"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(444, 29)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(79, 29)
            Me.LayoutControlItem3.Text = "CANCEL Button"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem3.TextToControlDistance = 0
            Me.LayoutControlItem3.TextVisible = False
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Control = Me.Button_Preview
            Me.LayoutControlItem5.CustomizationFormText = "PREVIEW Button"
            Me.LayoutControlItem5.Location = New System.Drawing.Point(444, 58)
            Me.LayoutControlItem5.Name = "LayoutControlItem5"
            Me.LayoutControlItem5.Size = New System.Drawing.Size(79, 29)
            Me.LayoutControlItem5.Text = "PREVIEW Button"
            Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem5.TextToControlDistance = 0
            Me.LayoutControlItem5.TextVisible = False
            Me.LayoutControlItem5.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.OnlyInCustomization
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.Button_Show
            Me.LayoutControlItem4.CustomizationFormText = "SHOW Button"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(444, 87)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(79, 29)
            Me.LayoutControlItem4.Text = "SHOW Button"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem4.TextToControlDistance = 0
            Me.LayoutControlItem4.TextVisible = False
            Me.LayoutControlItem4.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.OnlyInCustomization
            '
            'LayoutControlItem6
            '
            Me.LayoutControlItem6.Control = Me.chk_deleted
            Me.LayoutControlItem6.CustomizationFormText = "LayoutControlItem6"
            Me.LayoutControlItem6.Location = New System.Drawing.Point(444, 365)
            Me.LayoutControlItem6.Name = "LayoutControlItem6"
            Me.LayoutControlItem6.Size = New System.Drawing.Size(79, 23)
            Me.LayoutControlItem6.Text = "LayoutControlItem6"
            Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem6.TextToControlDistance = 0
            Me.LayoutControlItem6.TextVisible = False
            '
            'EmptySpaceItem1
            '
            Me.EmptySpaceItem1.AllowHotTrack = False
            Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Location = New System.Drawing.Point(444, 116)
            Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Size = New System.Drawing.Size(79, 249)
            Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
            Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
            '
            'MainMenu1
            '
            Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1})
            '
            'MenuItem1
            '
            Me.MenuItem1.Index = 0
            Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem_Print, Me.MenuItem_Exit})
            Me.MenuItem1.Text = "&File"
            '
            'MenuItem_Print
            '
            Me.MenuItem_Print.Index = 0
            Me.MenuItem_Print.Text = "Print"
            '
            'MenuItem_Exit
            '
            Me.MenuItem_Exit.Index = 1
            Me.MenuItem_Exit.Text = "Exit"
            '
            'Form_Ticklers_List
            '
            Me.CancelButton = Me.Button_Cancel
            Me.ClientSize = New System.Drawing.Size(543, 408)
            Me.Controls.Add(Me.LayoutControl1)
            Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
            Me.Menu = Me.MainMenu1
            Me.Name = "Form_Ticklers_List"
            Me.Text = "Pending Ticklers"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.chk_deleted.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

        Protected WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Protected WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Protected WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Protected WithEvents Button_Show As DevExpress.XtraEditors.SimpleButton
        Protected WithEvents Button_Preview As DevExpress.XtraEditors.SimpleButton
        Protected WithEvents col_client As DevExpress.XtraGrid.Columns.GridColumn
        Protected WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Protected WithEvents ContextMenu1 As System.Windows.Forms.ContextMenu
        Protected WithEvents mnu_edit As System.Windows.Forms.MenuItem
        Protected WithEvents mnuDelete As System.Windows.Forms.MenuItem
        Protected WithEvents mnuCreate As System.Windows.Forms.MenuItem
        Protected WithEvents MainMenu1 As System.Windows.Forms.MainMenu
        Protected WithEvents MenuItem1 As System.Windows.Forms.MenuItem
        Protected WithEvents MenuItem_Print As System.Windows.Forms.MenuItem
        Protected WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Protected WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Protected WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Protected WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Protected WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Protected WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Protected WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
        Protected WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
        Protected WithEvents MenuItem_Exit As System.Windows.Forms.MenuItem
        Protected WithEvents chk_deleted As CheckEdit
        Protected WithEvents LayoutControlItem6 As LayoutControlItem
        Protected WithEvents col_id As DevExpress.XtraGrid.Columns.GridColumn
        Protected WithEvents col_date_created As DevExpress.XtraGrid.Columns.GridColumn
        Protected WithEvents col_counselor As DevExpress.XtraGrid.Columns.GridColumn
        Protected WithEvents col_counselor_name As DevExpress.XtraGrid.Columns.GridColumn
        Protected WithEvents col_priority As DevExpress.XtraGrid.Columns.GridColumn
        Protected WithEvents col_date_deleted As DevExpress.XtraGrid.Columns.GridColumn
        Protected WithEvents col_tickler_type As DevExpress.XtraGrid.Columns.GridColumn
        Protected WithEvents col_description As DevExpress.XtraGrid.Columns.GridColumn
        Protected WithEvents col_date_effective As DevExpress.XtraGrid.Columns.GridColumn

#End Region

        ''' <summary>
        ''' Double-Click event on the grid
        ''' </summary>
        Private Sub GridView1_DoubleClick(ByVal sender As Object, ByVal e As EventArgs)
            Dim hi As GridHitInfo = GridView1.CalcHitInfo(GridControl1.PointToClient(MousePosition))
            Dim RowHandle As Int32 = hi.RowHandle

            If RowHandle >= 0 Then
                Dim obj As Object = GridView1.GetRow(RowHandle)
                If obj IsNot Nothing Then
                    DoEdit(obj)
                End If
            End If
        End Sub

        ''' <summary>
        ''' Click event on the grid
        ''' </summary>
        Protected Sub GridView1_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim hi As GridHitInfo = GridView1.CalcHitInfo(GridControl1.PointToClient(MousePosition))
            Dim RowHandle As Int32 = hi.RowHandle

            ' If the item is clicked then set the focused row to it.
            GridView1.FocusedRowHandle = RowHandle
        End Sub

        ''' <summary>
        ''' Process the OK button click event
        ''' </summary>
        Private Sub Button_OK_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim RowHandle As Int32 = GridView1.FocusedRowHandle

            If RowHandle >= 0 Then
                Dim obj As Object = GridView1.GetRow(RowHandle)
                If obj IsNot Nothing Then
                    DoEdit(obj)
                End If
            End If
        End Sub

        ''' <summary>
        ''' Process the change in the focus row
        ''' </summary>
        Private Sub GridView1_FocusedRowChanged(ByVal sender As Object, ByVal e As FocusedRowChangedEventArgs)
            Button_OK.Enabled = (e.FocusedRowHandle >= 0)
        End Sub

        ''' <summary>
        ''' Refresh the list when the checkbox is changed
        ''' </summary>
        Private Sub chk_deleted_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
            LoadItems(chk_deleted.Checked)
            GridControl1.RefreshDataSource()
            GridView1.RefreshData()
        End Sub

        ''' <summary>
        ''' Do a popup event on the menu
        ''' </summary>
        Private Sub ContextMenu1_Popup(ByVal sender As Object, ByVal e As EventArgs)
            Dim hi As GridHitInfo = GridView1.CalcHitInfo(GridControl1.PointToClient(MousePosition))
            Dim RowHandle As Int32 = hi.RowHandle

            ' Pass along the focused row handle to the grid
            GridView1.FocusedRowHandle = RowHandle

            ' If there is a row then enable the edit/delete operation.
            If RowHandle >= 0 Then
                mnu_edit.Enabled = True
                mnuDelete.Enabled = True
            Else
                mnu_edit.Enabled = False
                mnuDelete.Enabled = False
            End If

            mnuCreate.Enabled = True
        End Sub

        ''' <summary>
        ''' Process the EDIT menu click event
        ''' </summary>
        Private Sub mnu_edit_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim RowHandle As Int32 = GridView1.FocusedRowHandle

            If RowHandle >= 0 Then
                Dim obj As Object = GridView1.GetRow(RowHandle)
                If obj IsNot Nothing Then
                    DoEdit(obj)
                End If
            End If
        End Sub

        ''' <summary>
        ''' Process the CREATE menu click event
        ''' </summary>
        Private Sub mnuCreate_Click(ByVal sender As Object, ByVal e As EventArgs)
            DoCreate()
        End Sub

        ''' <summary>
        ''' Menu's DELETE item
        ''' </summary>
        Private Sub mnuDelete_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim RowHandle As Int32 = GridView1.FocusedRowHandle

            If RowHandle >= 0 Then
                Dim obj As Object = GridView1.GetRow(RowHandle)
                Dim record As tickler = TryCast(obj, tickler)
                If record IsNot Nothing Then
                    Using bc As New BusinessContext()
                        Dim q As tickler = bc.ticklers.Where(Function(tk) tk.Id = record.Id).FirstOrDefault()

                        ' Deleting the item is setting the "date_deleted" to non-null
                        If q IsNot Nothing Then
                            q.date_deleted = DebtPlus.LINQ.BusinessContext.getdate()
                            bc.SubmitChanges()

                            ' Remove the item from the tickler list and redraw the list
                            colItems.Remove(record)
                            GridControl1.RefreshDataSource()
                            GridView1.RefreshData()
                        End If
                    End Using
                End If
            End If
        End Sub

        ''' <summary>
        ''' Menu's EDIT item
        ''' </summary>
        Private Sub MenuItem_Exit_Click(ByVal sender As Object, ByVal e As EventArgs)
            Button_Cancel.PerformClick()
        End Sub

        ''' <summary>
        ''' Menu's PRINT item
        ''' </summary>
        Private Sub MenuItem_Print_Click(ByVal sender As Object, ByVal e As EventArgs)

            Using cm As New DebtPlus.UI.Common.CursorManager()
                If GridControl1.IsPrintingAvailable Then
                    GridControl1.ShowPrintPreview()
                Else
                    MessageBox.Show(Me, "Sorry, that function is not implemented at this time. It will be shortly.", "Function not available", MessageBoxButtons.OK)
                End If
            End Using

        End Sub

        Protected Class TicklerDescriptionFormatter
            Implements System.IFormatProvider
            Implements System.ICustomFormatter

            Public Function GetFormat(formatType As System.Type) As Object Implements System.IFormatProvider.GetFormat
                Return Me
            End Function

            Public Function Format(format1 As String, arg As Object, formatProvider As System.IFormatProvider) As String Implements System.ICustomFormatter.Format
                Dim key As System.Nullable(Of Int32) = DebtPlus.Utils.Nulls.v_Int32(arg)
                Return Helpers.GetTicklerTypeText(key)
            End Function
        End Class

        Protected Class CounselorFormatter
            Implements System.IFormatProvider
            Implements System.ICustomFormatter

            Public Function GetFormat(formatType As System.Type) As Object Implements System.IFormatProvider.GetFormat
                Return Me
            End Function

            Public Function Format(format1 As String, arg As Object, formatProvider As System.IFormatProvider) As String Implements System.ICustomFormatter.Format
                Dim key As System.Nullable(Of Int32) = DebtPlus.Utils.Nulls.v_Int32(arg)
                Return Helpers.GetCounselorNameText(key)
            End Function
        End Class
    End Class
End Namespace
