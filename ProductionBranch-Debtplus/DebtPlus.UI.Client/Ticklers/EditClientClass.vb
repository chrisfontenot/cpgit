﻿Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Interfaces.Client
Imports DebtPlus.Svc.DataSets
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports DevExpress.XtraLayout.Utils
Imports DebtPlus.UI.Client.Widgets.Search
Imports DebtPlus.Data.Forms
Imports System.Threading
Imports System.Reflection
Imports System.IO

Namespace Ticklers
    ''' <summary>
    ''' Class to perform the edit of the client on a new thread
    ''' </summary>
    Friend Class EditClientClass
        Inherits Object
        Private m_client As Int32 = -1

        ''' <summary>
        ''' Create a new instance of our class
        ''' </summary>
        Public Sub New(ByVal Client As Int32)
            MyBase.new()
            m_client = Client
        End Sub

        ''' <summary>
        ''' Edit the client passed to us
        ''' </summary>
        Friend Shared Sub ClientEdit(ByVal Client As Int32)
            With New EditClientClass(Client)
                Dim thrd As New Thread(AddressOf .EditRoutine)
                With thrd
                    .Name = "ClientEdit"
                    .SetApartmentState(ApartmentState.STA)
                    .Start()
                End With
            End With
        End Sub

        ''' <summary>
        ''' Edit the client information once we have found the ClientId
        ''' </summary>
        Public Sub EditRoutine()

            ' Display the alert notes for this client
            Using noteClass As New DebtPlus.Notes.AlertNotes.Client()
                noteClass.ShowAlerts(m_client)
            End Using

            ' Follow it up with the edit operation
            Using editClass As New DebtPlus.UI.Client.Service.ClientUpdateClass()
                editClass.ShowEditDialog(m_client, False)
            End Using
        End Sub
    End Class
End Namespace
