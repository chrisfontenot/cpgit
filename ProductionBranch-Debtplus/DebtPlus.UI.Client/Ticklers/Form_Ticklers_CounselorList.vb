#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Interfaces.Client
Imports DebtPlus.Svc.DataSets
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports DevExpress.XtraLayout.Utils
Imports DebtPlus.UI.Client.Widgets.Search
Imports DebtPlus.Data.Forms
Imports System.Threading
Imports System.Reflection
Imports System.IO
Imports System.Linq
Imports DebtPlus.LINQ

Namespace Ticklers

    Public Class Form_Ticklers_CounselorList
        Inherits Form_Ticklers_List
        Protected cutoffDate As DateTime

        Private CounselorID As Int32

        ''' <summary>
        ''' Create a new instance of our class with the parent pointer
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(ByVal CounselorID As Int32)
            MyClass.New()
            cutoffDate = DebtPlus.LINQ.BusinessContext.getdate().Date
            Me.CounselorID = CounselorID
            RegisterEventHandlers()
        End Sub

        Private Sub RegisterEventHandlers()
            AddHandler Load, AddressOf Form_Load
            AddHandler Timer1.Tick, AddressOf Timer1_Tick
            AddHandler Button_Preview.Click, AddressOf Button_Preview_Click
            AddHandler Button_Show.Click, AddressOf Button_Show_Click
        End Sub

        Private Sub UnRegisterEventHandlers()
            RemoveHandler Load, AddressOf Form_Load
            RemoveHandler Timer1.Tick, AddressOf Timer1_Tick
            RemoveHandler Button_Preview.Click, AddressOf Button_Preview_Click
            RemoveHandler Button_Show.Click, AddressOf Button_Show_Click
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then
                        components.Dispose()
                    End If
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer = Nothing

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents Timer1 As System.Windows.Forms.Timer

        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.chk_deleted.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridControl1
            '
            Me.GridControl1.Size = New System.Drawing.Size(401, 262)
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(195, Byte), Integer))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(189, Byte), Integer))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(206, Byte), Integer))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(225, Byte), Integer))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(215, Byte), Integer))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(215, Byte), Integer))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(215, Byte), Integer))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsView.AutoCalcPreviewLineCount = True
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            Me.GridView1.OptionsView.ShowPreview = True
            '
            'Button_OK
            '
            Me.Button_OK.Location = New System.Drawing.Point(417, 12)
            '
            'Button_Show
            '
            Me.Button_Show.Location = New System.Drawing.Point(417, 99)
            '
            'Button_Preview
            '
            Me.Button_Preview.Location = New System.Drawing.Point(417, 70)
            '
            'col_client
            '
            Me.col_client.DisplayFormat.FormatString = "0000000"
            Me.col_client.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_client.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.col_client.Visible = True
            Me.col_client.VisibleIndex = 1
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Location = New System.Drawing.Point(417, 41)
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Size = New System.Drawing.Size(504, 286)
            Me.LayoutControl1.Controls.SetChildIndex(Me.Button_Preview, 0)
            Me.LayoutControl1.Controls.SetChildIndex(Me.Button_Show, 0)
            Me.LayoutControl1.Controls.SetChildIndex(Me.Button_Cancel, 0)
            Me.LayoutControl1.Controls.SetChildIndex(Me.Button_OK, 0)
            Me.LayoutControl1.Controls.SetChildIndex(Me.GridControl1, 0)
            Me.LayoutControl1.Controls.SetChildIndex(Me.chk_deleted, 0)
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(504, 286)
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Size = New System.Drawing.Size(405, 266)
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Location = New System.Drawing.Point(405, 0)
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Location = New System.Drawing.Point(405, 29)
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Location = New System.Drawing.Point(405, 87)
            Me.LayoutControlItem4.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Location = New System.Drawing.Point(405, 58)
            Me.LayoutControlItem5.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
            '
            'EmptySpaceItem1
            '
            Me.EmptySpaceItem1.Location = New System.Drawing.Point(405, 116)
            Me.EmptySpaceItem1.Size = New System.Drawing.Size(79, 127)
            '
            'chk_deleted
            '
            Me.chk_deleted.Location = New System.Drawing.Point(417, 255)
            '
            'LayoutControlItem6
            '
            Me.LayoutControlItem6.Location = New System.Drawing.Point(405, 243)
            '
            'col_id
            '
            Me.col_id.DisplayFormat.FormatString = "f0"
            Me.col_id.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_id.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'col_date_created
            '
            Me.col_date_created.DisplayFormat.FormatString = "d"
            Me.col_date_created.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.col_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'col_counselor
            '
            Me.col_counselor.AppearanceCell.Options.UseTextOptions = True
            Me.col_counselor.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.col_counselor.AppearanceHeader.Options.UseTextOptions = True
            Me.col_counselor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.col_counselor.DisplayFormat.FormatString = "f0"
            Me.col_counselor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_counselor.GroupFormat.FormatString = "f0"
            Me.col_counselor.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_counselor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'col_counselor_name
            '
            Me.col_counselor_name.AppearanceCell.Options.UseTextOptions = True
            Me.col_counselor_name.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.col_counselor_name.AppearanceHeader.Options.UseTextOptions = True
            Me.col_counselor_name.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.col_counselor_name.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.col_counselor_name.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.col_counselor_name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'col_priority
            '
            Me.col_priority.DisplayFormat.FormatString = "f0"
            Me.col_priority.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_priority.GroupFormat.FormatString = "f0"
            Me.col_priority.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_priority.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.col_priority.Visible = True
            Me.col_priority.VisibleIndex = 2
            '
            'col_date_deleted
            '
            Me.col_date_deleted.DisplayFormat.FormatString = "d"
            Me.col_date_deleted.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.col_date_deleted.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'col_tickler_type
            '
            Me.col_tickler_type.AppearanceCell.Options.UseTextOptions = True
            Me.col_tickler_type.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.col_tickler_type.AppearanceHeader.Options.UseTextOptions = True
            Me.col_tickler_type.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.col_tickler_type.DisplayFormat.FormatString = "f0"
            Me.col_tickler_type.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.col_tickler_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'col_description
            '
            Me.col_description.AppearanceCell.Options.UseTextOptions = True
            Me.col_description.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.col_description.AppearanceHeader.Options.UseTextOptions = True
            Me.col_description.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.col_description.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.col_description.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.col_description.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.col_description.Visible = True
            Me.col_description.VisibleIndex = 3
            '
            'col_date_effective
            '
            Me.col_date_effective.DisplayFormat.FormatString = "d"
            Me.col_date_effective.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.col_date_effective.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.col_date_effective.Visible = True
            Me.col_date_effective.VisibleIndex = 0
            '
            'Timer1
            '
            Me.Timer1.Interval = 600000
            '
            'Form_Ticklers_CounselorList
            '
            Me.ClientSize = New System.Drawing.Size(504, 286)
            Me.Name = "Form_Ticklers_CounselorList"
            Me.Text = "List of pending ticklers for you"
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.chk_deleted.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        Protected Overrides Sub LoadItems(ByVal LoadDeleted As Boolean)

            Using bc As New DebtPlus.LINQ.BusinessContext()

                If CounselorID > 0 Then
                    Dim tomorrow As DateTime = cutoffDate.Date.AddDays(1)
                    Dim qry As System.Linq.IQueryable(Of tickler) = bc.ticklers.Where(Function(tk) tk.counselor.Value = CounselorID AndAlso tk.date_effective < tomorrow)
                    If Not LoadDeleted Then
                        qry = qry.Where(Function(s) s.date_deleted Is Nothing)
                    End If

                    colItems = qry.ToList()
                    GridControl1.DataSource = colItems
                End If
            End Using
        End Sub

        ' Current editing form for the operation
        Private EditItemRecord As tickler = Nothing

        ''' <summary>
        ''' Edit the record
        ''' </summary>
        Protected Overrides Function EditRecord(ByVal record As tickler) As System.Windows.Forms.DialogResult
            Dim frm As New Form_Tickler_Item(record)
            Try
                ' Save the form should the SHOW be clicked.
                EditItemRecord = record

                AddHandler frm.Button_Client.Click, AddressOf ShowClientButton_Click
                frm.Button_Client.Enabled = True
                frm.Button_Client.Visible = True

                Return frm.ShowDialog()

            Finally

                RemoveHandler frm.Button_Client.Click, AddressOf ShowClientButton_Click
                EditItemRecord = Nothing
                frm.Dispose()
            End Try

        End Function

        ''' <summary>
        ''' Process the PREVIEW button click event
        ''' </summary>
        Private Sub Button_Preview_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim frm As Form_Ticklers_NewDate = New Form_Ticklers_NewDate()
            Try
                frm.selection_date = cutoffDate
                If frm.ShowDialog() <> Windows.Forms.DialogResult.OK Then
                    Return
                End If

                cutoffDate = frm.selection_date()
                Text = String.Format("Tickler events as of {0:d}", cutoffDate)

                LoadItems(chk_deleted.Checked)
                GridControl1.RefreshDataSource()
                GridView1.RefreshData()

            Finally
                frm.Dispose()
            End Try
        End Sub

        ''' <summary>
        ''' Handle the click event for the show button on the edit form
        ''' </summary>
        Private Sub ShowClientButton_Click(ByVal Sender As Object, ByVal e As EventArgs)
            If EditItemRecord IsNot Nothing Then
                EditClientClass.ClientEdit(EditItemRecord.client)
            End If
        End Sub

        ''' <summary>
        ''' Do the form load operation for this dialog
        ''' </summary>
        Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs)

            UnRegisterEventHandlers()
            Try
                chk_deleted.Checked = False
                LoadItems(False)

                ' Run the timer
                Timer1.Enabled = True
                Timer1.Start()

            Finally
                RegisterEventHandlers()
            End Try

        End Sub

        ''' <summary>
        ''' Process a timer tick event every 10 minutes
        ''' </summary>
        Private Sub Timer1_Tick(ByVal sender As Object, ByVal e As EventArgs)
            UnRegisterEventHandlers()
            Try
                LoadItems(chk_deleted.Checked)
                GridControl1.RefreshDataSource()
                GridView1.RefreshData()

            Finally
                RegisterEventHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Intercept the create to ensure that we have a client
        ''' </summary>
        Protected Overrides Sub DoCreate()

            ' Create the new tickler
            Dim record As DebtPlus.LINQ.tickler = DebtPlus.LINQ.Factory.Manufacture_tickler(0, CounselorID)
            MyBase.DoCreate(record)
        End Sub

        ''' <summary>
        ''' Menu's SHOW item
        ''' </summary>
        Private Sub Button_Show_Click(ByVal sender As Object, ByVal e As EventArgs)
            ProcessShowClickEvent()
        End Sub

        ''' <summary>
        ''' Menu's SHOW item
        ''' </summary>
        Protected Sub ProcessShowClickEvent()

            Using cm As New DebtPlus.UI.Common.CursorManager()
                Dim RowHandle As Int32 = GridView1.FocusedRowHandle
                If RowHandle >= 0 Then
                    Dim obj As Object = GridView1.GetRow(RowHandle)
                    Dim record As tickler = TryCast(obj, tickler)
                    If record IsNot Nothing Then
                        If record.client > 0 Then
                            EditClientClass.ClientEdit(record.client)
                        End If
                    End If
                End If
            End Using
        End Sub
    End Class
End Namespace
