#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Collections.Generic
Imports System.Xml.Serialization

Namespace Service

    Friend Module Filters

#Region " clients "

        ''' <summary>
        ''' Filter for the clients table
        ''' </summary>
        Friend Function ClientsFilter(ByVal FilterName As String) As Boolean
            Select Case FilterName
                Case "client", "timestamp", "date_created", "created_by", "active_status_date", "drop_date",
                    "reserved_in_trust", "reserved_in_trust_cutoff"
                    Return False
            End Select
            Return True
        End Function

#End Region

#Region " People "

        ''' <summary>
        ''' Filter to prevent updating columns for the People table
        ''' </summary>
        Friend Function PeopleFilter(ByVal FieldName As String) As Boolean
            Select Case FieldName
                Case "timestamp", "person", "client", "date_created", "created_by"
                    Return False
            End Select
            Return True
        End Function

#End Region
    End Module

    ''' <summary>
    ''' Known filter for the debt records
    ''' </summary>
    Public Class RowFilter
        Private privateName As String = String.Empty
        Private privateFilter As String = String.Empty
        Private isDefault As Boolean = False

        ' Raised when the default item is changed
        Public Event DefaultChanged As EventHandler

        Public Property [Default]() As Boolean
            Get
                Return isDefault
            End Get
            Set(ByVal value As Boolean)
                If value <> isDefault Then
                    isDefault = value
                    RaiseEvent DefaultChanged(Me, EventArgs.Empty)
                End If
            End Set
        End Property

        Public Property Name() As String
            Get
                Return privateName
            End Get
            Set(ByVal value As String)
                privateName = value
            End Set
        End Property

        Public Property Filter() As String
            Get
                Return privateFilter
            End Get
            Set(ByVal value As String)
                privateFilter = value
            End Set
        End Property

        Public Sub New()
        End Sub

        Public Sub New(ByVal Name As String, ByVal Filter As String, ByVal isDefault As Boolean)
            Me.Name = Name
            Me.Filter = Filter
            Me.[Default] = isDefault
        End Sub
    End Class

    ''' <summary>
    ''' List of known filters for the debt records
    ''' </summary>
    <Serializable()>
    Public Class RowFilterList
        Inherits List(Of RowFilter)
        Private Const ExpectedVersion As Int32 = 2
        Private privateVersion As Int32 = ExpectedVersion

        <XmlElement("version")>
        Public Property Version() As Int32
            Get
                Return privateVersion
            End Get
            Set(ByVal value As Int32)
                privateVersion = value
            End Set
        End Property

        Public Function IsValidVersion() As Boolean
            Return Version = ExpectedVersion
        End Function

        Public Sub SetDefault(ByVal Item As RowFilter)
            Item.Default = True
        End Sub

        Public Function DefaultFilter() As RowFilter
            Dim Item As RowFilter = Nothing
            For Each row As RowFilter In Me
                If row.Default Then
                    Item = row
                    Exit For
                End If
            Next
            Return Item
        End Function

        Public Sub HookHandlers()
            UnHookHandlers()
            For Each row As RowFilter In Me
                AddHandler row.DefaultChanged, AddressOf DefaultChanged
            Next
        End Sub

        Public Sub UnHookHandlers()
            For Each row As RowFilter In Me
                RemoveHandler row.DefaultChanged, AddressOf DefaultChanged
            Next
        End Sub

        Private Sub DefaultChanged(ByVal Sender As Object, ByVal e As EventArgs)
            Dim item As RowFilter = CType(Sender, RowFilter)

            If item.Default Then
                UnHookHandlers()
                For Each row As RowFilter In Me
                    row.Default = False
                Next
                CType(item, RowFilter).Default = True
                HookHandlers()
            End If
        End Sub
    End Class
End NameSpace
