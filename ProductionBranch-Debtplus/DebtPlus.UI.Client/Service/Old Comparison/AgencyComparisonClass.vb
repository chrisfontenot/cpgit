Imports System.Data.SqlClient

Namespace Service
    ''' <summary>
    ''' Agency information for the comparison functions.
    ''' </summary>
    Public Class AgencyComparisonClass
        Inherits ComparisonBaseClass

        Public SalesFile As SalesFileInfoComparisonClass = Nothing

        Public Sub New(ByVal SalesFile As SalesFileInfoComparisonClass)
            MyClass.SalesFile = SalesFile

            ' Read the agency information
            Dim cn As SqlConnection = New SqlConnection(SqlInfo.ConnectionString)
            Dim rd As SqlDataReader = Nothing
            Try
                cn.Open()
                With New SqlCommand
                    .Connection = cn
                    .CommandText = "SELECT TOP 1 convert(money,isnull(payment_minimum,10)) AS payment_minimum, convert(float,isnull(threshold,0.021)) AS threshold, isnull(ComparisonAmount,0) as ComparisonAmount, isnull(ComparisonRate,0.03) as ComparisonRate, isnull(ComparisonPercent,0.03) as ComparisonPercent FROM config"
                    .CommandType = CommandType.Text
                    rd = .ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleRow)
                End With

                If rd IsNot Nothing AndAlso rd.Read Then
                    If Not rd.IsDBNull(0) Then privateMinimumPayment = rd.GetDecimal(0)
                    If Not rd.IsDBNull(1) Then privateMinimumRate = rd.GetDouble(1)
                    If Not rd.IsDBNull(2) Then privateComparisonPayment = rd.GetDecimal(2)
                    If Not rd.IsDBNull(3) Then privateComparisonRate = rd.GetDouble(3)
                    If Not rd.IsDBNull(4) Then privateComparisonPercent = rd.GetDouble(4)
                End If

            Catch ex As SqlException
                Using gdr As New Repository.GetDataResult()
                    gdr.HandleException(ex)
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading agency information")
                End Using

            Finally
                If rd IsNot Nothing AndAlso Not rd.IsClosed Then rd.Close()
                If cn IsNot Nothing AndAlso cn.State <> ConnectionState.Closed Then cn.Close()
            End Try
        End Sub

        ''' <summary>
        ''' Minimum rate for calculating the payout data on a DMP plan
        ''' </summary>
        Private privateMinimumRate As Double = 0.02#
        Public ReadOnly Property MinimumRate() As Double
            Get
                Return privateMinimumRate
            End Get
        End Property

        ''' <summary>
        ''' Minimum dollar amount for a SELF plan
        ''' </summary>
        Private privateComparisonPayment As Decimal = 15D
        Public ReadOnly Property ComparisonPayment() As Decimal
            Get
                Return privateComparisonPayment
            End Get
        End Property

        ''' <summary>
        ''' Payout rate for a SELF plan
        ''' </summary>
        Private privateComparisonRate As Double = 0.01#
        Public ReadOnly Property ComparisonRate() As Double
            Get
                Return privateComparisonRate
            End Get
        End Property

        ''' <summary>
        ''' Plan rate for unknown creditors
        ''' </summary>
        Private privateComparisonPercent As Double = 0.01#
        Public ReadOnly Property ComparisonPercent() As Double
            Get
                Return privateComparisonPercent
            End Get
        End Property

        ''' <summary>
        ''' Minimum dollar amount for a DMP plan
        ''' </summary>
        Private privateMinimumPayment As Decimal = 10D
        Public ReadOnly Property MinimumPayment() As Decimal
            Get
                Return privateMinimumPayment
            End Get
        End Property
    End Class
End NameSpace
