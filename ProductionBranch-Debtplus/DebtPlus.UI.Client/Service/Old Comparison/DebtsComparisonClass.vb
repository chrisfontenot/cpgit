Imports System.Collections.Generic
Imports DebtPlus.Utils
Imports System.Data.SqlClient

Namespace Service
    Public Class DebtsComparisonClass
        Inherits List(Of DebtComparisonClass)

        '' temporary hack
        Public Property SqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()

        Private SalesFile As SalesFileInfoComparisonClass = Nothing

        Public Sub New(ByVal SalesFile As SalesFileInfoComparisonClass)
            MyClass.SalesFile = SalesFile
        End Sub

        Public Overloads Sub Add(ByVal row As DataRow)
            Dim NewDebt As New DebtComparisonClass(SalesFile, row)
            MyBase.Add(NewDebt)
            AddHandler NewDebt.RecalculatePayout, AddressOf Debt_NeedToRecalculate
        End Sub

        Public Overloads Function Find(ByVal Key As Int32) As DebtComparisonClass
            Dim ie As IEnumerator = MyBase.GetEnumerator
            Do While ie.MoveNext
                Dim SearchItem As DebtComparisonClass = CType(ie.Current, DebtComparisonClass)
                If SearchItem.Key = Key Then
                    Return SearchItem
                End If
            Loop
            Return Nothing
        End Function

        Public Sub LoadDebts()
            Const TableName As String = "sales_debts"
            Dim tbl As DataTable = SalesFile.ds.Tables(TableName)
            If tbl Is Nothing Then

                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    Using _
                        cmd As SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(SqlInfo.ConnectionString)
                            .CommandText = "SELECT [sales_debt],[sales_file],[creditor_type],[creditor_name],[account_number],[client_creditor],[balance],[interest_rate],[minimum_payment],[late_fees],[overlimit_fees],[cosigner_fees],[finance_charge],[total_interest_fees],[principal],[plan_min_prorate],[plan_min_payment],[plan_payment],[plan_interest_rate],[plan_total_fees],[plan_finance_charge],[plan_total_interest_fees],[plan_principal],[payout_percent],[payout_interest] FROM sales_debts WHERE sales_file=@sales_file"
                            .Parameters.Add("@sales_file", SqlDbType.Int).Value = SalesFile.ID
                            .CommandType = CommandType.Text
                        End With

                        Using da As New SqlDataAdapter(cmd)
                            da.Fill(SalesFile.ds, TableName)
                        End Using
                    End Using
                    tbl = SalesFile.ds.Tables(TableName)

                Catch ex As SqlException
                    Using gdr As New Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading comparison debts")
                    End Using

                Finally
                    Cursor.Current = current_cursor
                End Try
            End If

            If tbl IsNot Nothing Then
                For Each row As DataRow In tbl.Rows
                    Add(row)
                Next
            End If
        End Sub

        Public Sub Save(ByRef cn As SqlConnection, ByVal txn As SqlTransaction)
            Dim ie As IEnumerator = MyBase.GetEnumerator
            Do While ie.MoveNext
                Dim CurrentDebt As DebtComparisonClass = CType(ie.Current, DebtComparisonClass)
                CurrentDebt.Save(cn, txn)
            Loop
        End Sub

        Private Sub Debt_NeedToRecalculate(ByVal sender As Object, ByVal e As EventArgs)
            SalesFile.DMP_NeedToRecalculate = True
            SalesFile.Self_NeedToRecalculate = True
        End Sub
    End Class
End NameSpace
