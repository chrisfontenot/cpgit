Imports System.Collections.Generic
Imports DebtPlus.Utils
Imports System.Data.SqlClient

Namespace Service
    Public Class KnownCreditorsComparisonClass
        Inherits List(Of KnownCreditorComparisonClass)

        '' temporary hack
        Public Property SqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()

        Private SalesFile As SalesFileInfoComparisonClass = Nothing

        Public Sub New(ByVal SalesFile As SalesFileInfoComparisonClass)
            MyClass.SalesFile = SalesFile
        End Sub

        Public Overloads Sub Add(ByVal row As DataRow)
            MyBase.Add(New KnownCreditorComparisonClass(row))
        End Sub

        Public Overloads Function Find(ByVal Name As String) As KnownCreditorComparisonClass
            Dim ie As IEnumerator = MyBase.GetEnumerator
            Do While ie.MoveNext
                Dim SearchItem As KnownCreditorComparisonClass = CType(ie.Current, KnownCreditorComparisonClass)
                If String.Compare(SearchItem.Name, Name, True) = 0 Then
                    Return SearchItem
                End If
            Loop
            Return Nothing
        End Function

        Public Overloads Function Find(ByVal Key As Int32) As KnownCreditorComparisonClass
            Dim ie As IEnumerator = MyBase.GetEnumerator
            Do While ie.MoveNext
                Dim SearchItem As KnownCreditorComparisonClass = CType(ie.Current, KnownCreditorComparisonClass)
                If Key = SearchItem.Key Then
                    Return SearchItem
                End If
            Loop
            Return Nothing
        End Function

        Public Sub LoadCreditors()
            Const TableName As String = "comparison_creditors"
            Dim tbl As DataTable = SalesFile.ds.Tables(TableName)
            If tbl Is Nothing Then
                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    Using _
                        cmd As SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(SqlInfo.ConnectionString)
                            .CommandText = "xpr_Sales_Creditors"
                            .CommandType = CommandType.StoredProcedure
                        End With
                        Using da As New SqlDataAdapter(cmd)
                            da.Fill(SalesFile.ds, TableName)
                        End Using
                    End Using
                    tbl = SalesFile.ds.Tables(TableName)

                Catch ex As SqlException
                    Using gdr As New Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading comparison creditor types") : End Using
                Finally
                    Cursor.Current = current_cursor
                End Try
            End If

            ' Load the list of creditors into our list of classes
            For Each row As DataRow In tbl.Rows
                Add(row)
            Next
        End Sub
    End Class
End NameSpace
