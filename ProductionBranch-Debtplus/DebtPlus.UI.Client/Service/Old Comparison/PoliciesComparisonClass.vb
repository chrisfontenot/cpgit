Imports System.Collections.Generic
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Text

Namespace Service
    Public Class PoliciesComparisonClass
        Inherits List(Of PolicyComparisonClass)

        '' temporary hack
        Public Property SqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()

        Public Overloads Sub Add(ByVal row As DataRow)
            MyBase.Add(New PolicyComparisonClass(row))
        End Sub

        Public Sub LoadPolicies(ByVal salescreditor As Int32)

            If MyBase.Count <= 0 Then
                Const TableName As String = "policies"

                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    Dim ds As New DataSet("ds")

                    Using cmd As SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(SqlInfo.ConnectionString)
                            .CommandText = "xpr_sales_policies"
                            .Parameters.Add("@sales_creditor", SqlDbType.Int).Value = salescreditor
                            .CommandType = CommandType.StoredProcedure
                        End With

                        Using da As New SqlDataAdapter(cmd)
                            da.Fill(ds, TableName)
                        End Using
                    End Using

                    Dim tbl As DataTable = ds.Tables(0)
                    For Each row As DataRow In tbl.Rows
                        Add(row)
                    Next

                Catch ex As SqlException
                    Using gdr As New Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading comparison creditor types")
                    End Using

                Finally
                    Cursor.Current = current_cursor
                End Try
            End If
        End Sub

        Public Shadows Function toString() As String
            Dim sb As New StringBuilder
            sb.Append(
                "{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\froman\fcharset0 Times New Roman;}}\viewkind4\uc1\pard")

            Dim ie As IEnumerator = GetEnumerator()
            Do While ie.MoveNext
                Dim PolicyItem As PolicyComparisonClass = CType(ie.Current, PolicyComparisonClass)
                sb.Append(PolicyItem.ToString())
            Loop

            sb.Append("}")
            Return sb.ToString()
        End Function
    End Class
End NameSpace
