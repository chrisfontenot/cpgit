
Namespace Service
    Public Class KnownCreditorComparisonClass
        Inherits ComparisonBaseClass

        Public Sub New()
        End Sub

        Public Sub New(ByVal row As DataRow)
            MyClass.row = row
        End Sub

        Private privateRow As DataRow = Nothing

        Public Property row() As DataRow
            Get
                Return privateRow
            End Get
            Set(ByVal value As DataRow)
                privateRow = value
            End Set
        End Property

        Public ReadOnly Property Key() As Int32
            Get
                Dim answer As Int32 = -1
                If row IsNot Nothing AndAlso row("sales_creditor") IsNot Nothing AndAlso row("sales_creditor") IsNot DBNull.Value Then
                    answer = Convert.ToInt32(row("sales_creditor"))
                End If
                Return answer
            End Get
        End Property

        Public ReadOnly Property Name() As String
            Get
                Dim answer As String = String.Empty
                If row IsNot Nothing AndAlso row("Name") IsNot Nothing AndAlso row("Name") IsNot DBNull.Value Then _
                    answer = Convert.ToString(row("Name"))
                Return answer
            End Get
        End Property

        Public Shadows Function toString() As String
            Return Name
        End Function

        Public ReadOnly Property Type() As String
            Get
                Dim answer As String = String.Empty
                If row IsNot Nothing AndAlso row("type") IsNot Nothing AndAlso row("type") IsNot DBNull.Value Then _
                    answer = Convert.ToString(row("type"))
                Return answer
            End Get
        End Property

        Public ReadOnly Property Creditor() As String
            Get
                Dim answer As String = String.Empty
                If row IsNot Nothing AndAlso row("creditor") IsNot Nothing AndAlso row("creditor") IsNot DBNull.Value _
                    Then answer = Convert.ToString(row("creditor"))
                Return answer
            End Get
        End Property

        Public ReadOnly Property MinPayment() As Decimal
            Get
                Dim answer As Decimal = 0D
                If row IsNot Nothing AndAlso row("MinPayment") IsNot Nothing AndAlso row("MinPayment") IsNot DBNull.Value Then answer = Convert.ToDecimal(row("MinPayment"))
                Return answer
            End Get
        End Property

        Public ReadOnly Property Prorate() As Double
            Get
                Dim answer As Double = 0.0#
                If row IsNot Nothing AndAlso row("Prorate") IsNot Nothing AndAlso row("Prorate") IsNot DBNull.Value _
                    Then answer = Convert.ToDecimal(row("Prorate"))

                ' Reduce the value to a legal percentage
                If answer < 0.0# Then answer = 0.0#
                Do While answer >= 1.0#
                    answer /= 100.0#
                Loop

                Return answer
            End Get
        End Property

        Public ReadOnly Property ReducedRate() As Double
            Get
                Dim answer As Double = 0.0#
                If row IsNot Nothing AndAlso row("ReducedRate") IsNot Nothing AndAlso row("ReducedRate") IsNot DBNull.Value Then answer = Convert.ToDecimal(row("ReducedRate"))

                ' Reduce the value to a legal percentage
                If answer < 0.0# Then answer = 0.0#
                Do While answer >= 1.0#
                    answer /= 100.0#
                Loop

                Return answer
            End Get
        End Property

        Private privatePolicy As New PoliciesComparisonClass

        Public Function PolicyMatrix() As String
            If privatePolicy.Count <= 0 Then privatePolicy.LoadPolicies(Key)
            Return privatePolicy.ToString()
        End Function
    End Class
End NameSpace