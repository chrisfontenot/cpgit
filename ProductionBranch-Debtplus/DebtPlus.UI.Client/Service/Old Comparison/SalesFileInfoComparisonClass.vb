Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports DebtPlus.Utils

Namespace Service
    ''' <summary>
    ''' Sales File information
    ''' </summary>
    Public Class SalesFileInfoComparisonClass
        Inherits ComparisonBaseClass
        Implements IDisposable

        Public drv As DataRowView = Nothing
        Public ds As New DataSet("ds")

        'Private mReadonlyStatus As Boolean
        Friend DMP_NeedToRecalculate As Boolean = True
        Friend Self_NeedToRecalculate As Boolean = True
        Public DateCreated As Date = Now.Date

        Private privateContext As ClientUpdateClass = Nothing

        ''' <summary>
        ''' Context to access the client update class data
        ''' </summary>
        Public Property Context() As ClientUpdateClass
            Get
                Return privateContext
            End Get
            Set(ByVal value As ClientUpdateClass)
                privateContext = value
            End Set
        End Property

        ' List of creditors and the types
        Private privateKnownCreditorList As KnownCreditorsComparisonClass

        ''' <summary>
        ''' List of known creditors for the sales information
        ''' </summary>
        Public ReadOnly Property KnownCreditorList() As KnownCreditorsComparisonClass
            Get
                Return privateKnownCreditorList
            End Get
        End Property

        Private privateKnownTypeList As CreditorTypesComparisonClass

        ''' <summary>
        ''' List of known creditor types for the sales information
        ''' </summary>
        Public ReadOnly Property KnownTypeList() As CreditorTypesComparisonClass
            Get
                Return privateKnownTypeList
            End Get
        End Property

        Private privateDebtList As DebtsComparisonClass

        ''' <summary>
        ''' Return the list of known debts for the sales file
        ''' </summary>
        Public ReadOnly Property DebtList() As DebtsComparisonClass
            Get
                Return privateDebtList
            End Get
        End Property

        Private privateAgency As AgencyComparisonClass

        ''' <summary>
        ''' Return the pointer to the agency information for this sales file.
        ''' </summary>
        Public ReadOnly Property Agency() As AgencyComparisonClass
            Get
                Return privateAgency
            End Get
        End Property

        Private privateExtraDMPAmount As Decimal

        ''' <summary>
        ''' Additional deposit amount for the DMP program
        ''' </summary>
        Public Property DMP_ExtraAmount() As Decimal
            Get
                Return privateExtraDMPAmount
            End Get
            Set(ByVal value As Decimal)
                SetProperty("ExtraDMPAmount", privateExtraDMPAmount, value)
            End Set
        End Property

        Private privateComments As String

        ''' <summary>
        ''' Comments for the sales file.
        ''' </summary>
        Public Property Comments() As String
            Get
                Return privateComments
            End Get
            Set(ByVal value As String)
                SetProperty("Comments", privateComments, value)
            End Set
        End Property

        Public Sub New(ByRef Context As ClientUpdateClass, ByRef drv As DataRowView)
            MyClass.Context = Context
            MyClass.drv = drv

            ' If this is not a create function then save the sales file number
            If Not drv.IsNew Then
                privateID = Convert.ToInt32(drv("sales_file"))
                DateCreated = Convert.ToDateTime(drv("date_created"))
            End If

            ' Add the standard classes
            privateKnownCreditorList = New KnownCreditorsComparisonClass(Me)
            privateKnownTypeList = New CreditorTypesComparisonClass(Me)
            privateDebtList = New DebtsComparisonClass(Me)
            privateAgency = New AgencyComparisonClass(Me)

            ' Read the data for the various lists
            KnownTypeList.LoadTypes()
            KnownCreditorList.LoadCreditors()
            DebtList.LoadDebts()

            ' Read the information about the sales file itself
            Dim current_cursor As Cursor = Cursor.Current
            Dim cn As SqlConnection = New SqlConnection(SqlInfo.ConnectionString)
            Dim rd As SqlDataReader = Nothing
            Try
                Cursor.Current = Cursors.WaitCursor
                cn.Open()

                ' Read the sales file information from the database
                With New SqlCommand
                    .Connection = cn
                    .CommandText = "xpr_sales_info_common"
                    .CommandType = CommandType.StoredProcedure
                    .Parameters.Add("@sales_file", SqlDbType.Int).Value = ID
                    .Parameters.Add("@client", SqlDbType.Int).Value = Context.ClientId
                    rd = .ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleRow)
                End With

                If rd IsNot Nothing AndAlso rd.Read Then
                    If Not rd.IsDBNull(rd.GetOrdinal("note")) Then Comments = rd.GetString(rd.GetOrdinal("note"))
                    If Not rd.IsDBNull(rd.GetOrdinal("extra_amount")) Then _
                        DMP_ExtraAmount = rd.GetDecimal(rd.GetOrdinal("extra_amount"))
                    If _
                        Not rd.IsDBNull(rd.GetOrdinal("read_only")) AndAlso
                        Convert.ToInt32(rd.GetValue(rd.GetOrdinal("read_only"))) <> 0 Then privateReadonlyStatus = True
                End If

            Catch ex As SqlException
                Using gdr As New Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading comparison info common items") : End Using

            Finally
                If rd IsNot Nothing Then rd.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
                Cursor.Current = current_cursor
            End Try
        End Sub

        Private privateID As Int32 = -1

        ''' <summary>
        ''' The ID for the sales file
        ''' </summary>
        Public ReadOnly Property ID() As Int32
            Get
                Return privateID
            End Get
        End Property

        Private privateReadonlyStatus As Boolean = False

        ''' <summary>
        ''' Is the file "read-only". It is marked read-only when it is exported.
        ''' </summary>
        Public ReadOnly Property ReadOnlyStatus() As Boolean
            Get
                Return privateReadonlyStatus
            End Get
        End Property

        ''' <summary>
        ''' Delete the list of known debts associated with this file
        ''' </summary>
        Private Sub DeleteDebtList(ByRef cn As SqlConnection, ByVal txn As SqlTransaction)
            With New SqlCommand
                .Connection = cn
                .Transaction = txn
                .CommandText = "DELETE FROM sales_debts WHERE sales_file=@sales_file"
                .CommandType = CommandType.Text
                .Parameters.Add("@sales_file", SqlDbType.Int).Value = ID
                .ExecuteNonQuery()
            End With
        End Sub

        ''' <summary>
        ''' Write the common information for the sales file
        ''' </summary>
        Private Sub WriteCommonInfo(ByRef cn As SqlConnection, ByVal txn As SqlTransaction)

            With New SqlCommand
                .Connection = cn
                .Transaction = txn
                .CommandText = "UPDATE sales_files SET note=@note, extra_amount=@extra_amount, client_months=@client_months, plan_months=@plan_months, client_monthly_interest=@client_monthly_interest, plan_monthly_interest=@plan_monthly_interest, client_total_interest=@client_total_interest, plan_total_interest=@plan_total_interest WHERE sales_file=@sales_file"
                .CommandType = CommandType.Text

                With .Parameters
                    If Comments Is Nothing Then
                        .Add("@note", SqlDbType.VarChar, 4096).Value = DBNull.Value
                    Else
                        .Add("@note", SqlDbType.VarChar, 4096).Value = Comments
                    End If

                    .Add("@extra_amount", SqlDbType.Decimal).Value = DMP_ExtraAmount
                    .Add("@client_months", SqlDbType.Float).Value = Self_LengthMonths()
                    .Add("@plan_months", SqlDbType.Float).Value = DMP_LengthMonths()
                    .Add("@client_monthly_interest", SqlDbType.Decimal).Value = Self_TotalInterestAndFees
                    .Add("@plan_monthly_interest", SqlDbType.Decimal).Value = DMP_TotalInterestFees
                    .Add("@client_total_interest", SqlDbType.Decimal).Value = Self_TotalInterestPaid
                    .Add("@plan_total_interest", SqlDbType.Decimal).Value = DMP_TotalInterestPaid
                    .Add("@sales_file", SqlDbType.Int).Value = ID
                End With

                .ExecuteNonQuery()
            End With
        End Sub

        ''' <summary>
        ''' Export the sales debts to the DMP debts table
        ''' </summary>
        Private Sub UpdateDebtInfo(ByRef cn As SqlConnection, ByVal txn As SqlTransaction)
            With New SqlCommand
                .Connection = cn
                .Transaction = txn
                .CommandText = "xpr_sales_debt_export"
                .CommandType = CommandType.StoredProcedure
                .Parameters.Add("@sales_file", SqlDbType.Int).Value = ID
                .ExecuteNonQuery()
            End With

            ' Set the date that the file was exported into the sales_file row. Try to keep the unchanged status if possible.
            If drv.Row.RowState = DataRowState.Unchanged Then
                drv.BeginEdit()
                drv("date_exported") = Now
                drv.EndEdit()
                drv.Row.AcceptChanges()
            Else
                drv.BeginEdit()
                drv("date_exported") = Now
                drv.EndEdit()
            End If
        End Sub

        ''' <summary>
        ''' Just save the debts to the sales_debts table. No export is performed.
        ''' </summary>
        Public Sub WriteDebts()
            WriteDebts(False)
        End Sub

        ''' <summary>
        ''' Save and optionally export the debts to the DMP debts.
        ''' </summary>
        Public Sub WriteDebts(ByVal Export As Boolean)

            ' If the item is readonly then ignore the save
            If Not ReadOnlyStatus Then
                Dim cn As SqlConnection = New SqlConnection(SqlInfo.ConnectionString)
                Dim current_cursor As Cursor = Cursor.Current
                Dim txn As SqlTransaction = Nothing

                Try
                    Cursor.Current = Cursors.WaitCursor
                    cn.Open()
                    txn = cn.BeginTransaction(IsolationLevel.RepeatableRead)

                    ' Write the list of debts to the system
                    DeleteDebtList(cn, txn)
                    DebtList.Save(cn, txn)
                    WriteCommonInfo(cn, txn)

                    ' Do the export function
                    If Export Then
                        UpdateDebtInfo(cn, txn)
                    End If

                    ' Commit the changes to the database at this point.
                    txn.Commit()
                    txn.Dispose()
                    txn = Nothing

                Catch ex As System.Data.SqlClient.SqlException
                    Using gdr As New Repository.GetDataResult()
                        gdr.HandleException(ex)
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error saving comparison information")
                    End Using

                Finally
                    If txn IsNot Nothing Then
                        Try
                            txn.Rollback()
                        Catch exRollback As System.Data.SqlClient.SqlException
                        End Try
                        txn.Dispose()
                        txn = Nothing
                    End If

                    If cn IsNot Nothing Then cn.Dispose()
                    Cursor.Current = current_cursor
                End Try
            End If
        End Sub

        ''' <summary>
        ''' Return the total balance information
        ''' </summary>
        Public ReadOnly Property TotalBalance() As Decimal
            Get
                Dim answer As Decimal = 0D
                For Each item As DebtComparisonClass In DebtList
                    answer += item.Balance
                Next
                Return answer
            End Get
        End Property

        ''' <summary>
        ''' Return the total late fee savings
        ''' </summary>
        Public ReadOnly Property TotalLateFeeSavings() As Decimal
            Get
                Return 0D
            End Get
        End Property

        ''' <summary>
        ''' Return the total interest amount savings
        ''' </summary>
        Public ReadOnly Property TotalInterestSavings() As Decimal
            Get
                Return 0D
            End Get
        End Property

        ''' <summary>
        ''' Return the total interest rate information. This is not really a total, but a recomputation of the average for the totals as finance charges / balance.
        ''' </summary>
        Public ReadOnly Property Self_TotalInterestRate() As Double
            Get
                Dim Result As Double

                ' Total interest rate = totalfinancecharge / totalbalance
                Try
                    Result = Convert.ToDecimal(Self_TotalFinanceCharge) / Convert.ToDecimal(TotalBalance) * 12.0#
                    If Double.IsNaN(Result) Then Result = 0.0
                    Result = Math.Truncate(Result * 100.0#, 3) / 100.0#
                Catch ex As Exception
                    Result = 0.0
                End Try

                Return Result
            End Get
        End Property

        ''' <summary>
        ''' Return the total payment amount, a dollar figure.
        ''' </summary>
        Public ReadOnly Property Self_TotalPayoutPayment() As Decimal
            Get
                Dim answer As Decimal = 0D
                For Each item As DebtComparisonClass In DebtList
                    answer += item.Self_MonthlyPayment
                Next
                Return answer
            End Get
        End Property

        ''' <summary>
        ''' Return the total late fees amount
        ''' </summary>
        Public ReadOnly Property Self_TotalLateFees() As Decimal
            Get
                Dim answer As Decimal = 0D
                For Each item As DebtComparisonClass In DebtList
                    answer += item.Self_LateFees
                Next
                Return answer
            End Get
        End Property

        ''' <summary>
        ''' Return the total over-limit fees amount
        ''' </summary>
        Public ReadOnly Property Self_TotalOverLimitFees() As Decimal
            Get
                Dim answer As Decimal = 0D
                For Each item As DebtComparisonClass In DebtList
                    answer += item.Self_OverLimitFees
                Next
                Return answer
            End Get
        End Property

        ''' <summary>
        ''' Return the total co-signer fees amount
        ''' </summary>
        Public ReadOnly Property Self_TotalCosignerFees() As Decimal
            Get
                Dim answer As Decimal = 0D
                For Each item As DebtComparisonClass In DebtList
                    answer += item.Self_CosignerFees
                Next
                Return answer
            End Get
        End Property

        ''' <summary>
        ''' Return the total finance charge amount
        ''' </summary>

        Public ReadOnly Property Self_TotalFinanceCharge() As Decimal
            Get
                Dim answer As Decimal = 0D
                For Each Item As DebtComparisonClass In DebtList
                    answer += Item.Self_MonthlyFinanceCharge
                Next
                Return answer
            End Get
        End Property

        ''' <summary>
        ''' Return the total interest and fees amount
        ''' </summary>
        Public ReadOnly Property Self_TotalInterestAndFees() As Decimal
            Get
                Dim answer As Decimal = 0D
                For Each Item As DebtComparisonClass In DebtList
                    answer += Item.Self_InterestAndFees
                Next
                Return answer
            End Get
        End Property

        ''' <summary>
        ''' Return the total dollar amount going to the principal
        ''' </summary>
        Public ReadOnly Property Self_TotalGoingToPrincipal() As Decimal
            Get
                Dim answer As Decimal = 0D
                For Each Item As DebtComparisonClass In DebtList
                    answer += Item.Self_GoingToPrincipal
                Next
                Return answer
            End Get
        End Property

        ''' <summary>
        ''' Calculate the effective "total" percentage of money going to the principal.
        ''' </summary>
        Public ReadOnly Property Self_TotalPercentGoingToPrincipal() As Double
            Get
                Dim Result As Double
                Try
                    Result = Self_TotalGoingToPrincipal / Self_TotalPayoutPayment
                    Result = Math.Truncate(Result * 100.0#, 3) / 100.0#

                Catch ex As DivideByZeroException
                    Result = 0.0
                End Try

                Return Result
            End Get
        End Property

        ''' <summary>
        ''' Return the total initial balance information
        ''' </summary>
        Public ReadOnly Property Self_TotalBalance() As Decimal
            Get
                Dim answer As Decimal = 0D
                For Each Item As DebtComparisonClass In DebtList
                    answer += Item.Balance
                Next
                Return answer
            End Get
        End Property

        ''' <summary>
        ''' Return the total DMP Minimum payment information
        ''' </summary>
        Public ReadOnly Property DMP_TotalMinimumPayment() As Decimal
            Get
                Dim answer As Decimal = 0D
                For Each Item As DebtComparisonClass In DebtList
                    answer += Item.DMP_MinimumPayment
                Next
                Return answer
            End Get
        End Property

        ''' <summary>
        ''' Return the total DMP entered payment information
        ''' </summary>
        Public ReadOnly Property DMP_TotalEnteredPayment() As Decimal
            Get
                Dim answer As Decimal = 0D
                For Each Item As DebtComparisonClass In DebtList
                    answer += Item.DMP_EnteredPayment
                Next
                Return answer
            End Get
        End Property

        ''' <summary>
        ''' Return the total DMP fees information
        ''' </summary>
        Public ReadOnly Property DMP_TotalFees() As Decimal
            Get
                Dim answer As Decimal = 0D
                For Each Item As DebtComparisonClass In DebtList
                    answer += Item.DMP_Fees
                Next
                Return answer
            End Get
        End Property

        ''' <summary>
        ''' Return the total DMP Finance charge information
        ''' </summary>
        Public ReadOnly Property DMP_TotalFinanceCharge() As Decimal
            Get
                Dim answer As Decimal = 0D
                For Each Item As DebtComparisonClass In DebtList
                    answer += Item.DMP_FinanceCharge
                Next
                Return answer
            End Get
        End Property

        ''' <summary>
        ''' Return the total DMP Interst and Fees information
        ''' </summary>
        Public ReadOnly Property DMP_TotalInterestFees() As Decimal
            Get
                Dim answer As Decimal = 0D
                For Each Item As DebtComparisonClass In DebtList
                    answer += Item.DMP_InterestFees
                Next
                Return answer
            End Get
        End Property

        ''' <summary>
        ''' Return the total DMP amount going to the principal
        ''' </summary>
        Public ReadOnly Property DMP_TotalGoingToPrincipal() As Decimal
            Get
                Dim answer As Decimal = 0D
                For Each Item As DebtComparisonClass In DebtList
                    answer += Item.DMP_GoingToPrincipal
                Next
                Return answer
            End Get
        End Property

        ''' <summary>
        ''' Calculate a total "percent" of the money going to the principal
        ''' </summary>
        Public ReadOnly Property DMP_TotalPercentGoingToprincipal() As Double
            Get
                Dim Result As Double
                Try
                    Result = DMP_TotalGoingToPrincipal / DMP_TotalEnteredPayment
                    Result = Math.Truncate(Result * 100.0#, 3) / 100.0#
                Catch ex As DivideByZeroException
                    Result = 0.0
                End Try

                Return Result
            End Get
        End Property

        ''' <summary>
        ''' Calculate the total DMP Interest rate figure.
        ''' </summary>
        Public ReadOnly Property DMP_TotalInterestRate() As Double
            Get
                Dim Result As Double

                ' Total interest rate = totalfinancecharge / totalbalance
                Try
                    Result = Convert.ToDecimal(DMP_TotalFinanceCharge) / Convert.ToDecimal(TotalBalance) * 12.0#
                    If Double.IsNaN(Result) Then Result = 0.0
                    Result = Math.Truncate(Result * 100.0#, 3) / 100.0#
                Catch ex As Exception
                    Result = 0.0
                End Try

                Return Result
            End Get
        End Property

        ''' <summary>
        ''' Return the monthly savings for the payments
        ''' </summary>
        Public ReadOnly Property MonthlySavings_Payments() As Decimal
            Get
                Return Self_TotalPayoutPayment - DMP_TotalEnteredPayment
            End Get
        End Property

        ''' <summary>
        ''' Return the monthly savings or the finance charges
        ''' </summary>
        Public ReadOnly Property MonthlySavings_FinanceCharge() As Decimal
            Get
                Return Self_TotalFinanceCharge - DMP_TotalFinanceCharge
            End Get
        End Property

        ''' <summary>
        ''' Return the monthly savings for the estimated interest rate
        ''' </summary>
        Public ReadOnly Property MonthlySavings_EstimatedInterestRate() As Double
            Get
                Return Self_TotalInterestRate - DMP_TotalInterestRate
            End Get
        End Property

        ''' <summary>
        ''' Return the monthly savings for interest and fees
        ''' </summary>
        Public ReadOnly Property MonthlySavings_TotalInterestAndFees() As Decimal
            Get
                Return Self_TotalInterestAndFees - DMP_TotalInterestFees
            End Get
        End Property

        ''' <summary>
        ''' Return the total of the client interest paid
        ''' </summary>
        Public ReadOnly Property Self_TotalInterestPaid() As Decimal
            Get
                ' Ensure that the items are calculated correctly.
                If Self_NeedToRecalculate Then
                    For Each item As DebtComparisonClass In DebtList
                        item.Self_CalculatePayout()
                    Next
                    Self_NeedToRecalculate = False
                End If

                Dim answer As Decimal = 0D

                For Each Item As DebtComparisonClass In DebtList
                    answer += Item.Self_PayoutInterest
                Next

                If answer < 0D Then answer = 0D
                Return Math.Truncate(answer, 2)
            End Get
        End Property

        ' Total amount paid at end
        Public ReadOnly Property Self_TotalPaid() As Decimal
            Get
                Return TotalBalance + Self_TotalInterestPaid
            End Get
        End Property

        ' Total Interest paid
        Public ReadOnly Property DMP_TotalInterestPaid() As Decimal
            Get
                ' Compute the total interest paid as the simple formula of:
                ' interest = (payment amount * plan length) - original balance

                Try
                    ' First, calculate the length of the plan in fractional months.
                    Dim CurrentAmount As Decimal = TotalBalance
                    Dim CurrentRate As Double = DMP_TotalInterestRate
                    Dim CurrentPayment As Decimal = DMP_TotalPayment
                    Dim PlanLength As Double = NPer((CurrentRate / 12.0#), 0D - CurrentPayment, CurrentAmount, 0.0#, DueDate.BegOfPeriod)

                    ' Then take the monthly payment times the number of months less the principal amount
                    Dim answer As Decimal = Convert.ToDecimal(Convert.ToDouble(CurrentPayment * PlanLength)) - CurrentAmount

                    ' Do not allow the result to be negative.
                    If answer < 0D Then answer = 0D
                    Return answer

                Catch ex As Exception
                    Return 0D
                    ' The "magic" value for un-calculable information.

                End Try
            End Get
        End Property

        ' Total amount paid at end
        Public ReadOnly Property DMP_TotalAmountPaid() As Decimal
            Get
                Return TotalBalance + DMP_TotalInterestPaid
            End Get
        End Property

        Public Function DMP_LengthMonths() As Double

            ' Calculate the plan length based upon the total balance, the average interest rate, and the sum(debt payments) + extra amount as a payment figure
            ' We can do this because the amount is fixed and does not reduce over time based upon the balances.
            ' Also, we want to reflect the reduction in time as you add "extra amount" to the base line figure.

            ' The figure is rounded UP to the next month on a partial month payout since we pay in whole months we don't want 4.3 months, we want 5.

            Dim CurrentAmount As Decimal = TotalBalance
            Dim CurrentRate As Double = DMP_TotalInterestRate
            Dim CurrentPayment As Decimal = DMP_TotalPayment

            ' Handle the fringe cases where someone may be trying to fool us.
            If CurrentAmount <= 0D Then Return 0.0#
            If CurrentAmount <= CurrentPayment Then Return 1.0#

            Try
                Return System.Math.Ceiling(NPer((CurrentRate / 12.0#), 0D - CurrentPayment, CurrentAmount, 0.0#, DueDate.BegOfPeriod))

            Catch ex As Exception
                Return -1.0#
                ' The "magic" value for un-calculable information.

            End Try
        End Function

        ' Savings
        Public ReadOnly Property Savings() As Decimal
            Get
                Return Self_InterestSavings + Self_LateOverlimitFeeSavings
            End Get
        End Property

        Public ReadOnly Property Self_InterestSavings() As Decimal
            Get
                Return Self_TotalInterestPaid - DMP_TotalInterestPaid
            End Get
        End Property

        Public ReadOnly Property Self_LateOverlimitFeeSavings() As Decimal
            Get
                Return Self_TotalLateFees + Self_TotalOverLimitFees
            End Get
        End Property

        ' Total DMP Payment
        Public ReadOnly Property DMP_TotalPayment() As Decimal
            Get
                Return DMP_TotalEnteredPayment + DMP_ExtraAmount
            End Get
        End Property

        Public Function Self_LengthMonths() As Double
            Dim Self_LastPayoutMonths As Double = 0.0

            ' Ensure that the items are calculated correctly.
            If Self_NeedToRecalculate Then
                For Each item As DebtComparisonClass In DebtList
                    item.DMP_CalculatePayout()
                Next
                Self_NeedToRecalculate = False
            End If

            ' Calculate the figures for each debt. Take the longer item.
            For Each item As DebtComparisonClass In DebtList
                item.Self_Length_Months = 0.0#
                Dim Months As Double = item.Self_Length_Months
                If Months > Self_LastPayoutMonths Then
                    Self_LastPayoutMonths = Months
                End If
            Next

            Return Self_LastPayoutMonths
        End Function

#Region " IDisposable Support "

        Private disposedValue As Boolean = False
        ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not disposedValue Then
                If disposing Then
                    ds.Dispose()
                End If
            End If
            disposedValue = True
        End Sub

        ' This code added by Visual Basic to correctly implement the disposable pattern.
        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

#End Region
    End Class
End Namespace
