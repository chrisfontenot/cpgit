Imports System.Collections.Generic
Imports DebtPlus.Utils
Imports System.Data.SqlClient

Namespace Service
    Public Class CreditorTypesComparisonClass
        Inherits List(Of CreditorTypeComparisonClass)

        '' temporary hack
        Public Property SqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()

        Private SalesFile As SalesFileInfoComparisonClass = Nothing

        Public Sub New(ByVal SalesFile As SalesFileInfoComparisonClass)
            MyClass.SalesFile = SalesFile
        End Sub

        Dim NewKey As Int32 = 0

        Public Overloads Sub Add(ByVal row As DataRow)
            NewKey += 1
            MyBase.Add(New CreditorTypeComparisonClass(row, NewKey))
        End Sub

        Public Overloads Function Find(ByVal Label As String) As CreditorTypeComparisonClass
            Dim ie As IEnumerator = MyBase.GetEnumerator
            Do While ie.MoveNext
                Dim SearchItem As CreditorTypeComparisonClass = CType(ie.Current, CreditorTypeComparisonClass)
                If String.Compare(SearchItem.TypeLabel, Label, True) = 0 Then
                    Return SearchItem
                End If
            Loop
            Return Nothing
        End Function

        Public Overloads Function Find(ByVal Key As Int32) As CreditorTypeComparisonClass
            Dim ie As IEnumerator = MyBase.GetEnumerator
            Do While ie.MoveNext
                Dim SearchItem As CreditorTypeComparisonClass = CType(ie.Current, CreditorTypeComparisonClass)
                If Key = SearchItem.Key Then
                    Return SearchItem
                End If
            Loop
            Return Nothing
        End Function

        Public Sub LoadTypes()
            Const TableName As String = "comparison_creditor_types"
            Dim tbl As DataTable = SalesFile.ds.Tables(TableName)
            If tbl Is Nothing Then

                Dim current_cursor As Cursor = Cursor.Current
                Try
                    Cursor.Current = Cursors.WaitCursor
                    Using _
                        cmd As SqlCommand = New SqlCommand
                        With cmd
                            .Connection = New SqlConnection(SqlInfo.ConnectionString)
                            .CommandText = "xpr_Sales_CreditorTypes"
                            .CommandType = CommandType.StoredProcedure
                        End With

                        Using da As New SqlDataAdapter(cmd)
                            da.Fill(SalesFile.ds, TableName)
                        End Using
                    End Using
                    tbl = SalesFile.ds.Tables(TableName)

                Catch ex As SqlException
                    Using gdr As New Repository.GetDataResult() : gdr.HandleException(ex) : DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading comparison creditor types") : End Using
                Finally
                    Cursor.Current = current_cursor
                End Try
            End If

            If tbl IsNot Nothing Then
                For Each row As DataRow In tbl.Rows
                    Add(row)
                Next
            End If
        End Sub
    End Class
End NameSpace
