Imports System.ComponentModel
Imports System.Data.SqlClient
Imports DebtPlus.Utils

Namespace Service
    Public Class DebtComparisonClass
        Inherits ComparisonBaseClass

        Public Class PayoutEventArgs
            Private privateMonth As Int32

            Public ReadOnly Property Month As Int32
                Get
                    Return privateMonth
                End Get
            End Property

            Private privatePayment As Decimal

            Public ReadOnly Property Payment As Decimal
                Get
                    Return privatePayment
                End Get
            End Property

            Private privatePrincipal As Decimal

            Public ReadOnly Property Principal As Decimal
                Get
                    Return privatePrincipal
                End Get
            End Property

            Private privateInterest As Decimal

            Public ReadOnly Property Interest As Decimal
                Get
                    Return privateInterest
                End Get
            End Property

            Private privateBalance As Decimal

            Public ReadOnly Property Balance As Decimal
                Get
                    Return privateBalance
                End Get
            End Property

            Public Sub New(ByVal Month As Int32, ByVal Payment As Decimal, ByVal Interest As Decimal,
                           ByVal Principal As Decimal, ByVal Balance As Decimal)
                privateMonth = Month
                privatePayment = Payment
                privateInterest = Interest
                privatePrincipal = Principal
                privateBalance = Balance
            End Sub
        End Class

        ''' <summary>
        ''' Event handler for the payout schedule
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>

        Public Delegate Sub PayoutEventHandler(ByVal sender As Object, ByVal e As PayoutEventArgs)

        ''' <summary>
        ''' client payout event as the payment schedule is being run
        ''' </summary>
        Public Event ClientPayout As PayoutEventHandler

        Protected Overridable Sub OnClientPayout(ByVal e As PayoutEventArgs)
            RaiseEvent ClientPayout(Me, e)
        End Sub

        Protected Sub RaiseClientPayout(ByVal Month As Int32, ByVal Payment As Decimal, ByVal Interest As Decimal, ByVal Principal As Decimal, ByVal Balance As Decimal)
            OnClientPayout(New PayoutEventArgs(Month, Payment, Interest, Principal, Balance))
        End Sub

        ''' <summary>
        ''' Plan payout event as the payment schedule is being run
        ''' </summary>
            Public Event PlanPayout As PayoutEventHandler

        Protected Overridable Sub OnPlanPayout(ByVal e As PayoutEventArgs)
            RaiseEvent PlanPayout(Me, e)
        End Sub

        Protected Sub RaisePlanPayout(ByVal Month As Int32, ByVal Payment As Decimal, ByVal Interest As Decimal, ByVal Principal As Decimal, ByVal Balance As Decimal)
            OnPlanPayout(New PayoutEventArgs(Month, Payment, Interest, Principal, Balance))
        End Sub

        Public Event RecalculatePayout As EventHandler

        Protected Overridable Sub OnRecalculatePayout(ByVal e As EventArgs)
            RaiseEvent RecalculatePayout(Me, e)
        End Sub

        Protected Sub RaiseRecalculatePayout()
            OnRecalculatePayout(EventArgs.Empty)
        End Sub

        Protected Overrides Sub OnPropertyChanged(e As PropertyChangedEventArgs)
            MyBase.OnPropertyChanged(e)
            RaiseRecalculatePayout()
        End Sub

        Private privateSalesFile As SalesFileInfoComparisonClass = Nothing

        Public ReadOnly Property SalesFile() As SalesFileInfoComparisonClass
            Get
                Return privateSalesFile
            End Get
        End Property

        Public Sub New(ByVal SalesFile As SalesFileInfoComparisonClass)
            privateSalesFile = SalesFile

            ' Set the default values for a new record
            Self_InterestRate = SalesFile.Agency.ComparisonRate
            Self_PaymentPercent = SalesFile.Agency.ComparisonPercent
        End Sub

        Public Sub New(ByVal SalesFile As SalesFileInfoComparisonClass, ByVal row As DataRow)
            MyClass.New(SalesFile)

            ' Process the items in the row
            For ItemNo As Int32 = 0 To row.Table.Columns.Count - 1
                If Not row.IsNull(ItemNo) Then
                    Select Case row.Table.Columns(ItemNo).ColumnName
                        Case "sales_debt" : Key = Convert.ToInt32(row(ItemNo))
                        Case "client_creditor" : ClientCreditor = Convert.ToInt32(row(ItemNo))
                        Case "creditor_type" : CreditorType = Convert.ToString(row(ItemNo))
                        Case "creditor_name" : Name = Convert.ToString(row(ItemNo))
                        Case "creditor" : Creditor = Convert.ToInt32(row(ItemNo))
                        Case "balance" : Balance = Convert.ToDecimal(row(ItemNo))
                        Case "interest_rate" : Self_InterestRate = Convert.ToDouble(row(ItemNo))
                        Case "late_fees" : Self_LateFees = Convert.ToDecimal(row(ItemNo))
                        Case "overlimit_fees" : Self_OverLimitFees = Convert.ToDecimal(row(ItemNo))
                        Case "account_number" : AccountNumber = Convert.ToString(row(ItemNo))
                        Case "payout_interest" : DMP_PayoutInterest = Convert.ToDecimal(row(ItemNo))
                        Case "payout_percent" : Self_PaymentPercent = Convert.ToDouble(row(ItemNo))
                        Case "plan_interest_rate" : DMP_InterestRate = Convert.ToDouble(row(ItemNo))
                        Case "plan_payment" : DMP_EnteredPayment = Convert.ToDecimal(row(ItemNo))
                    End Select
                End If
            Next

            ' If there is a creditor but not name then find the creditor name
            If Creditor > 0 AndAlso SalesFile.KnownCreditorList IsNot Nothing Then
                Dim cr As KnownCreditorComparisonClass = SalesFile.KnownCreditorList.Find(Creditor)
                If cr IsNot Nothing Then Name = cr.Name
            End If
        End Sub

        Public Sub Save(ByVal cn As SqlConnection, ByVal txn As SqlTransaction)
            Using _
                cmd As SqlCommand = New SqlCommand
                With cmd
                    .Connection = cn
                    .Transaction = txn
                    .CommandText = "INSERT INTO sales_debts(client_creditor,sales_file,creditor_type,creditor_name,balance,interest_rate,minimum_payment,late_fees,overlimit_fees,finance_charge,total_interest_fees,principal,plan_min_prorate,plan_min_payment,plan_payment,plan_interest_rate,plan_total_interest_fees,plan_principal,account_number,payout_interest,payout_percent) VALUES (@client_creditor,@sales_file,@creditor_type,@creditor_name,@balance,@interest_rate,@minimum_payment,@late_fees,@over_limit_fees,@finance_charge,@total_interest_fees,@principal,@plan_min_prorate,@plan_min_payment,@plan_payment,@plan_interest_rate,@plan_total_interest_fees,@plan_principal,@account_number,@payout_interest,@payout_percent)"
                    .CommandType = CommandType.Text

                    With .Parameters
                        .Add("@client_creditor", SqlDbType.Int).Value = ClientCreditor
                        .Add("@sales_file", SqlDbType.Int).Value = SalesFile.ID
                        .Add("@creditor_type", SqlDbType.VarChar, 80).Value = CreditorType
                        .Add("@creditor_name", SqlDbType.VarChar, 80).Value = Name
                        .Add("@balance", SqlDbType.Decimal).Value = Balance

                        .Add("@interest_rate", SqlDbType.Float).Value = Self_InterestRate
                        .Add("@minimum_payment", SqlDbType.Decimal).Value = Self_MonthlyPayment
                        .Add("@late_fees", SqlDbType.Decimal).Value = Self_LateFees
                        .Add("@over_limit_fees", SqlDbType.Decimal).Value = Self_OverLimitFees
                        .Add("@finance_charge", SqlDbType.Decimal).Value = Self_MonthlyFinanceCharge
                        .Add("@total_interest_fees", SqlDbType.Decimal).Value = Self_InterestAndFees
                        .Add("@principal", SqlDbType.Decimal).Value = Self_GoingToPrincipal

                        .Add("@plan_min_prorate", SqlDbType.Float).Value = DMP_MinimumProrate
                        .Add("@plan_min_payment", SqlDbType.Decimal).Value = DMP_MinimumPayment
                        .Add("@plan_payment", SqlDbType.Decimal).Value = DMP_EnteredPayment
                        .Add("@plan_interest_rate", SqlDbType.Float).Value = DMP_InterestRate
                        .Add("@plan_total_interest_fees", SqlDbType.Decimal).Value = DMP_InterestFees
                        .Add("@plan_principal", SqlDbType.Decimal).Value = DMP_GoingToPrincipal

                        .Add("@account_number", SqlDbType.VarChar, 80).Value = AccountNumber
                        .Add("@payout_interest", SqlDbType.Decimal).Value = DMP_PayoutInterest
                        .Add("@payout_percent", SqlDbType.Float).Value = Self_PaymentPercent
                    End With

                    .ExecuteNonQuery()
                End With
            End Using
        End Sub

        Private privateKey As Int32 = 0

        Public Property Key() As Int32
            Get
                Return privateKey
            End Get
            Set(ByVal value As Int32)
                SetProperty("Key", privateKey, value)
            End Set
        End Property

        Private privateClientCreditor As Object = DBNull.Value

        Public Property ClientCreditor() As Object
            Get
                Return privateClientCreditor
            End Get
            Set(ByVal value As Object)
                If value Is Nothing Then value = DBNull.Value

                If value IsNot privateClientCreditor Then
                    Debug.Assert(TypeOf value Is Int32)
                    privateClientCreditor = value
                    OnPropertyChanged(New PropertyChangedEventArgs("ClientCreditor"))
                End If
            End Set
        End Property

        Private privateCreditor As Int32 = -1

        Public Property Creditor() As Int32
            Get
                Return privateCreditor
            End Get
            Set(ByVal value As Int32)
                SetProperty("Creditor", privateCreditor, value)

                ' Look up the type based upon the creditor id
                If value > 0 Then
                    Dim cr As KnownCreditorComparisonClass = SalesFile.KnownCreditorList.Find(value)
                    If cr IsNot Nothing Then
                        CreditorType = cr.Type
                    End If
                End If
            End Set
        End Property

        Private privateCreditorType As String = String.Empty
        Public Property CreditorType() As String
            Get
                Return privateCreditorType
            End Get
            Set(ByVal value As String)
                SetProperty("CreditorType", privateCreditorType, value)
            End Set
        End Property

        Private privateName As String = String.Empty
        Public Property Name() As String
            Get
                Return privateName
            End Get
            Set(ByVal value As String)
                value = value.Trim()
                SetProperty("Name", privateName, value)

                ' Try to find the creditor in the standard list of creditors
                If privateName = String.Empty Then
                    Creditor = -1
                Else
                    Dim cr As KnownCreditorComparisonClass = SalesFile.KnownCreditorList.Find(privateName)
                    If cr IsNot Nothing Then
                        Creditor = cr.Key
                    Else
                        Creditor = -1
                    End If
                End If
            End Set
        End Property

        Private privateAccountNumber As String = String.Empty
        Public Property AccountNumber() As String
            Get
                Return privateAccountNumber
            End Get
            Set(ByVal value As String)
                value = value.Trim()
                SetProperty("AccountNumber", privateAccountNumber, value)
            End Set
        End Property

        Private privateBalance As Decimal = 0D
        Public Property Balance() As Decimal
            Get
                Return privateBalance
            End Get
            Set(ByVal value As Decimal)
                value = Math.Truncate(value, 2)
                SetProperty("Balance", privateBalance, value)
            End Set
        End Property

        Public Property Self_PayoutInterest As Decimal = 0D
        Public Property Self_PayoutPrincipal As Decimal = 0D

        Private privateSelf_LateFees As Decimal = 0D

        Public Property Self_LateFees() As Decimal
            Get
                Return privateSelf_LateFees
            End Get
            Set(ByVal value As Decimal)
                value = Math.Truncate(value, 2)
                SetProperty("Self_LateFees", privateSelf_LateFees, value)
            End Set
        End Property

        Private privateSelf_OverLimitFees As Decimal = 0D
        Public Property Self_OverLimitFees() As Decimal
            Get
                Return privateSelf_OverLimitFees
            End Get
            Set(ByVal value As Decimal)
                value = Math.Truncate(value, 2)
                SetProperty("Self_OverLimitFees", privateSelf_OverLimitFees, value)
            End Set
        End Property

        Private privateSelf_CosignerFees As Decimal = 0D
        Public Property Self_CosignerFees() As Decimal
            Get
                Return privateSelf_CosignerFees
            End Get
            Set(ByVal value As Decimal)
                value = Math.Truncate(value, 2)
                SetProperty("Self_CosignerFees", privateSelf_CosignerFees, value)
            End Set
        End Property

        Private privateSelf_InterestRate As Double = 0.0#
        Public Property Self_InterestRate() As Double
            Get
                Return privateSelf_InterestRate
            End Get
            Set(ByVal value As Double)

                If value < 0.0# Then value = 0.0#
                While value >= 1.0#
                    value /= 100.0#
                End While

                SetProperty("Self_InterestRate", privateSelf_InterestRate, value)
            End Set
        End Property

        Private privateSelf_PaymentPercent As Double = 0.01#
        Public Property Self_PaymentPercent() As Double
            Get
                Return privateSelf_PaymentPercent
            End Get
            Set(ByVal value As Double)

                ' Reduce the upper limit to a legal percentage
                Do While value >= 1.0#
                    value /= 100.0#
                Loop

                ' Increase the lower limit to the valid range
                If value < 0.01# Then value = 0.01#

                ' Update the property
                SetProperty("Self_PaymentPercent", privateSelf_PaymentPercent, value)
            End Set
        End Property

        Public ReadOnly Property Self_MonthlyPayment() As Decimal
            Get
                Dim value As Decimal

                If Self_PaymentPercent = 0.01# Then
                    value =
                        Decimal.Round(
                            Convert.ToDecimal(Convert.ToDouble(Balance) * (Self_InterestRate / 12.0)) +
                            Convert.ToDecimal(Convert.ToDouble(Balance) * 0.01#), 2)
                Else
                    value = Decimal.Round(Convert.ToDecimal(Convert.ToDouble(Balance) * Self_PaymentPercent))
                End If

                If value < SalesFile.Agency.MinimumPayment Then
                    value = SalesFile.Agency.MinimumPayment
                    If value > Balance Then value = Balance
                End If

                Return value
            End Get
        End Property

        Public ReadOnly Property Self_MonthlyFinanceCharge() As Decimal
            Get
                Return Decimal.Round(Convert.ToDecimal(Convert.ToDouble(Balance) * (Self_InterestRate / 12.0#)), 2)
            End Get
        End Property

        Public ReadOnly Property Self_InterestAndFees() As Decimal
            Get
                Return Self_MonthlyFinanceCharge + Self_LateFees + Self_OverLimitFees + Self_CosignerFees
            End Get
        End Property

        Public ReadOnly Property Self_GoingToPrincipal() As Decimal
            Get
                Return Self_MonthlyPayment - Self_MonthlyFinanceCharge
            End Get
        End Property

        Public Sub Self_CalculatePayout()
            Self_CalculatePayout_MinimumPayment()
        End Sub

        Private Sub Self_CalculatePayout_MinimumPayment()

            ' Clear the interest amount paid
            Self_PayoutInterest = 0D

            ' If there is no balance then there can't be a need to repay. Force the value to be zero.
            If Balance <= 0D Then
                private_Self_Length_Months = 0.0
                RaiseClientPayout(1, 0, 0, 0, 0)
                Return
            End If

            Dim PrincipalPaid As Decimal = 0D
            private_Self_Length_Months = 1.0

            ' Put a limit so that we just don't do it forever.
            Do While private_Self_Length_Months < 999.0

                ' Calculate the interest figure from the balance.
                Dim NewBalance As Decimal = Balance - PrincipalPaid
                Dim InterestAmount As Decimal =
                        Decimal.Round(Convert.ToDecimal(Convert.ToDouble(NewBalance) * (Self_InterestRate / 12.0#)), 2)

                ' To that add the additional amount for payout
                Dim MinimumPayment As Decimal

                ' If the amount is the magic 1% then do it as "interest + 1%". Otherwise, do it as a percentage of the balance.
                If Self_PaymentPercent = 0.01# Then
                    MinimumPayment = InterestAmount +
                                     Decimal.Round(Convert.ToDecimal(Convert.ToDouble(NewBalance) * 0.01), 2)
                Else ' Otherwise, it is the percent of the balance.
                    MinimumPayment = Decimal.Round(Convert.ToDecimal(Convert.ToDouble(NewBalance) * Self_PaymentPercent),
                                                   2)
                End If

                ' If the minimum is less than $15 then make it $15 so that there is a chance that we will pay off the amount
                If MinimumPayment < SalesFile.Agency.ComparisonPayment Then
                    MinimumPayment = SalesFile.Agency.ComparisonPayment
                End If

                ' If the minimum is is now more than the balance then the debt is paid off and there is no interest on the last month.
                If MinimumPayment >= NewBalance Then
                    RaiseClientPayout(Convert.ToInt32(private_Self_Length_Months), NewBalance, 0, NewBalance, 0)
                    Return
                End If

                ' Calculate the figures for the amount of prinipal and interest
                Dim PrincipalAmount As Decimal = MinimumPayment - InterestAmount
                Self_PayoutInterest += InterestAmount
                PrincipalPaid += PrincipalAmount

                ' Increment the number of months to pay off the debt
                RaiseClientPayout(Convert.ToInt32(private_Self_Length_Months), MinimumPayment, InterestAmount,
                                  PrincipalAmount, Balance - PrincipalPaid)
                private_Self_Length_Months += 1.0
            Loop

            ' Sorry, the figures went too far. Indicate that it will never be paid off.
            private_Self_Length_Months = -1.0#
        End Sub

        Private private_Self_Length_Months As Double = 0.0

        Public Property Self_Length_Months As Double
            Get
                If private_Self_Length_Months <= 0.0 Then
                    Self_CalculatePayout()
                End If
                Return private_Self_Length_Months
            End Get

            Set(ByVal Value As Double)
                private_Self_Length_Months = Value
            End Set
        End Property

        '------------------------------------------------------------------------------------------------------
        '                       PLAN INFORMATION                                                           --
        '------------------------------------------------------------------------------------------------------

        Public Property DMP_PayoutInterest As Decimal = 0D
        Public Property DMP_PayoutPrincipal As Decimal = 0D

        Private privateDMP_EnteredPayment As Decimal = 0D

        Public Property DMP_EnteredPayment() As Decimal
            Get
                Return privateDMP_EnteredPayment
            End Get
            Set(ByVal value As Decimal)
                value = Math.Truncate(value, 2)
                SetProperty("DMP_EnteredPayment", privateDMP_EnteredPayment, value)
            End Set
        End Property

        Public ReadOnly Property DMP_MinimumProrate() As Double
            Get
                If Creditor > 0 Then
                    Return SalesFile.KnownCreditorList.Find(Creditor).Prorate
                ElseIf CreditorType <> String.Empty Then
                    Return Convert.ToDecimal(SalesFile.KnownTypeList.Find(CreditorType).Creditor_PercentPayment)
                Else
                    Return 0.0#
                End If
            End Get
        End Property

        Public ReadOnly Property DMP_MinimumPayment() As Decimal
            Get
                Dim CalculatedValue As Decimal

                ' Find the minimum prorate information for this debt
                Dim MinProrate As Double = DMP_MinimumProrate
                If MinProrate > 0.4 Then
                    CalculatedValue = Math.Truncate(Convert.ToDecimal(Convert.ToDecimal(Self_MonthlyPayment) * MinProrate),
                                                    2)
                    ' Yes, Self_MonthlyPayment is correct for finance companies!
                Else
                    CalculatedValue = Math.Truncate(Convert.ToDecimal(Convert.ToDecimal(Balance) * MinProrate), 2)
                End If

                ' use the minimum value for the creditor if there is one
                If Creditor > 0 Then
                    Dim MinLimit As Decimal = SalesFile.KnownCreditorList.Find(Creditor).MinPayment
                    If MinLimit > 0D AndAlso MinLimit > CalculatedValue Then CalculatedValue = MinLimit

                    ' No creditor. Look at the type for minimum levels.
                ElseIf CreditorType <> String.Empty Then
                    Dim MinLimit As Decimal =
                            Math.Truncate(
                                Convert.ToDecimal(
                                    SalesFile.KnownTypeList.Find(CreditorType).Creditor_PercentPayment *
                                    Convert.ToDecimal(Balance)), 2)
                    If MinLimit > 0D AndAlso MinLimit > CalculatedValue Then CalculatedValue = MinLimit

                    ' Use the minimum value for the agency threashold
                ElseIf SalesFile.Agency.MinimumRate > 0.0# AndAlso SalesFile.Agency.MinimumRate < 1.0# Then
                    Dim MinAgencyThreashold As Decimal =
                            Math.Truncate(Convert.ToDecimal(SalesFile.Agency.MinimumRate * Convert.ToDecimal(Balance)), 2)
                    If CalculatedValue < MinAgencyThreashold Then CalculatedValue = MinAgencyThreashold

                    ' Use the minimum amount for the agency
                    If SalesFile.Agency.MinimumPayment >= 0D Then
                        If SalesFile.Agency.MinimumPayment > CalculatedValue Then
                            CalculatedValue = SalesFile.Agency.MinimumPayment
                        End If
                    End If
                End If

                Return CalculatedValue
            End Get
        End Property

        ' DMP Interest rate
        Private privateDMP_InterestRate As Double = -1.0#

        Public Property DMP_InterestRate() As Double
            Get
                If privateDMP_InterestRate >= 0.0# AndAlso privateDMP_InterestRate < 1.0# Then
                    Return privateDMP_InterestRate
                Else
                    If Creditor <= 0 Then
                        If Self_InterestRate >= 0.0# AndAlso Self_InterestRate < 1.0# Then
                            Return Self_InterestRate
                        Else
                            Return 0.0#
                        End If
                    Else
                        With SalesFile.KnownCreditorList.Find(Creditor)
                            If .ReducedRate >= 0.0# AndAlso .ReducedRate < 1.0# Then
                                Return .ReducedRate
                            Else
                                Return Self_InterestRate
                            End If
                        End With
                    End If
                End If
            End Get
            Set(ByVal value As Double)

                If value < 0.0# Then
                    value = -1.0#
                Else
                    While value >= 1.0#
                        value /= 100.0#
                    End While
                End If

                SetProperty("DMP_InterestRate", privateDMP_InterestRate, value)
            End Set
        End Property

        Public ReadOnly Property DMP_Fees() As Decimal
            Get
                Return 0D
            End Get
        End Property

        Public ReadOnly Property DMP_FinanceCharge() As Decimal
            Get
                Return Decimal.Round(Convert.ToDecimal(Convert.ToDouble(Balance) * (DMP_InterestRate / 12.0#)), 2)
            End Get
        End Property

        Public ReadOnly Property DMP_InterestFees() As Decimal
            Get
                Return DMP_Fees + DMP_FinanceCharge
            End Get
        End Property

        Public ReadOnly Property DMP_GoingToPrincipal() As Decimal
            Get
                Return DMP_EnteredPayment - DMP_InterestFees
            End Get
        End Property

        Public Sub DMP_CalculatePayout()
            DMP_CalculatePayout_FixedAmount()
        End Sub

        Private Sub DMP_CalculatePayout_FixedAmount()

            ' Clear the interest amount paid
            DMP_PayoutInterest = 0D

            ' If there is no balance then there can't be a need to repay. Force the value to be zero.
            If Balance <= 0D Then
                private_DMP_Length_Months = 0.0
                RaisePlanPayout(0, 0, 0, 0, 0)
                Return
            End If

            Dim PrincipalPaid As Decimal = 0D
            private_DMP_Length_Months = 1.0

            ' Put a limit so that we just don't do it forever.
            Do While private_DMP_Length_Months < 999.0

                ' Calculate the interest figure from the balance.
                Dim NewBalance As Decimal = Balance - PrincipalPaid
                Dim InterestAmount As Decimal =
                        Decimal.Round(Convert.ToDecimal(Convert.ToDouble(NewBalance) * (DMP_InterestRate / 12.0#)), 2)

                ' To that add the additional amount for payout
                Dim MinimumPayment As Decimal = Decimal.Round(DMP_EnteredPayment, 2)

                ' If the minimum is less than $15 then make it $15 so that there is a chance that we will pay off the amount
                If MinimumPayment < SalesFile.Agency.MinimumPayment Then
                    MinimumPayment = SalesFile.Agency.MinimumPayment
                End If

                ' If the minimum is is now more than the balance then the debt is paid off and there is no interest on the last month.
                If MinimumPayment >= NewBalance Then
                    RaisePlanPayout(Convert.ToInt32(private_DMP_Length_Months), NewBalance, 0, NewBalance, 0)
                    Return
                End If

                ' Calculate the figures for the amount of prinipal and interest
                Dim PrincipalAmount As Decimal = MinimumPayment - InterestAmount
                DMP_PayoutInterest += InterestAmount
                PrincipalPaid += PrincipalAmount

                ' Increment the number of months to pay off the debt
                RaisePlanPayout(Convert.ToInt32(private_DMP_Length_Months), MinimumPayment, InterestAmount,
                                PrincipalAmount, Balance - PrincipalPaid)
                private_DMP_Length_Months += 1.0
            Loop

            ' Sorry, the figures went too far. Indicate that it will never be paid off.
            private_DMP_Length_Months = -1.0#
        End Sub

        Private private_DMP_Length_Months As Double = 0.0

        Public Property DMP_Length_Months As Double
            Get
                If private_DMP_Length_Months <= 0.0 Then
                    DMP_CalculatePayout()
                End If
                Return private_DMP_Length_Months
            End Get

            Set(ByVal Value As Double)
                private_DMP_Length_Months = Value
            End Set
        End Property
    End Class
End NameSpace
