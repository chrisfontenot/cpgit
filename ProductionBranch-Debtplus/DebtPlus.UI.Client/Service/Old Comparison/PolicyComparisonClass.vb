
Namespace Service
    Public Class PolicyComparisonClass
        Inherits ComparisonBaseClass

        Public Sub New()
        End Sub

        Public Sub New(ByVal row As DataRow)
            MyClass.row = row
        End Sub

        Private privateRow As DataRow = Nothing

        Public Property row() As DataRow
            Get
                Return privateRow
            End Get
            Set(ByVal value As DataRow)
                privateRow = value
            End Set
        End Property

        Public ReadOnly Property policydescription() As String
            Get
                Dim answer As String = String.Empty
                If _
                    row IsNot Nothing AndAlso row("policy_description") IsNot Nothing AndAlso
                    row("policy_description") IsNot DBNull.Value Then _
                    answer = Convert.ToString(row("policy_description"))
                Return answer
            End Get
        End Property

        Public ReadOnly Property policymessage() As String
            Get
                Dim answer As String = String.Empty
                If _
                    row IsNot Nothing AndAlso row("policy_message") IsNot Nothing AndAlso
                    row("policy_message") IsNot DBNull.Value Then answer = Convert.ToString(row("policy_message"))
                Return answer
            End Get
        End Property

        Private Function RTFString(ByVal InputString As String) As String
            Dim Result As String = InputString.Trim()

            ' Replace the special sequences with their RTF counterpart
            Result = Result.Replace("\", "\\")
            Result = Result.Replace("{", "\{")
            Result = Result.Replace("}", "\}")

            Return Result
        End Function

        Public Shadows Function toString() As String
            Return _
                String.Format("\b\f0\fs20 {0}\par\pard\li360\b0\fs18 {1}\par\pard\par",
                              RTFString(policydescription.ToUpper), RTFString(policymessage))
        End Function
    End Class
End NameSpace