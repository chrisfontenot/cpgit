
Namespace Service
    Public Class CreditorTypeComparisonClass
        Inherits ComparisonBaseClass

        Private privateKey As Int32 = -1

        Public Property Key() As Int32
            Get
                Return privateKey
            End Get
            Set(ByVal value As Int32)
                privateKey = value
            End Set
        End Property

        Public Sub New(ByVal row As DataRow, ByVal Key As Int32)
            MyClass.row = row
            MyClass.Key = Key
        End Sub

        Public Sub New()
        End Sub

        Public Sub New(ByVal row As DataRow)
            MyClass.row = row
        End Sub

        Private privateRow As DataRow = Nothing

        Public Property row() As DataRow
            Get
                Return privateRow
            End Get
            Set(ByVal value As DataRow)
                privateRow = value
            End Set
        End Property

        Public ReadOnly Property TypeLabel() As String
            Get
                Dim answer As String = String.Empty
                If row IsNot Nothing AndAlso row("TypeLabel") IsNot Nothing AndAlso row("TypeLabel") IsNot DBNull.Value _
                    Then answer = Convert.ToString(row("TypeLabel"))
                Return answer
            End Get
        End Property

        Public ReadOnly Property Description() As String
            Get
                Dim answer As String = String.Empty
                If _
                    row IsNot Nothing AndAlso row("description") IsNot Nothing AndAlso row("description") IsNot DBNull.Value Then answer = Convert.ToString(row("description"))
                Return answer
            End Get
        End Property

        Public Shadows Function toString() As String
            Return Description
        End Function

        Public ReadOnly Property Creditor_PercentPayment() As Object
            Get
                Dim answer As Object = Nothing
                If _
                    row IsNot Nothing AndAlso row("DefaultPercentPayment") IsNot Nothing AndAlso
                    row("DefaultPercentPayment") IsNot DBNull.Value Then _
                    answer = Convert.ToDecimal(row("DefaultPercentPayment"))
                Return answer
            End Get
        End Property

        Public ReadOnly Property Creditor_PercentDisb() As Object
            Get
                Dim answer As Object = Nothing
                If _
                    row IsNot Nothing AndAlso row("DefaultPercentDisb") IsNot Nothing AndAlso
                    row("DefaultPercentDisb") IsNot DBNull.Value Then _
                    answer = Convert.ToDecimal(row("DefaultPercentDisb"))
                Return answer
            End Get
        End Property

        Public ReadOnly Property Creditor_Amount() As Object
            Get
                Dim answer As Object = Nothing
                If row IsNot Nothing AndAlso row("DefaultAmount") IsNot Nothing AndAlso row("DefaultAmount") IsNot DBNull.Value Then answer = Convert.ToDecimal(row("DefaultAmount"))
                Return answer
            End Get
        End Property
    End Class
End NameSpace