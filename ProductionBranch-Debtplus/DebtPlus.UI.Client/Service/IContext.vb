
Namespace Service
    ''' <summary>
    ''' Forms for this assembly implement this interface for context info.
    ''' </summary>
    Friend Interface IContext
        ReadOnly Property Context() As ClientUpdateClass
    End Interface
End Namespace
