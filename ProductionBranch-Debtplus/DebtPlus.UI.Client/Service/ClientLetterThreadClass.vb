#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.Letters.Compose
Imports DebtPlus.Letters
Imports DebtPlus.Events
Imports DebtPlus.Utils.Format

Namespace Service

    ''' <summary>
    ''' Generate the appointment letters thread items
    ''' </summary>
    Friend Class ClientLetterThread
        Private context As ClientUpdateClass
        Private LetterType As String
        Private ClientAppointment As Int32

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        Public Sub New(ByVal context As ClientUpdateClass, ByVal LetterType As String, ByVal ClientAppointment As Int32)
            Me.context = context
            Me.LetterType = LetterType
            Me.ClientAppointment = ClientAppointment
        End Sub

        ''' <summary>
        ''' Create a new instance of the letter writing class
        ''' </summary>
        Private Function CreateNewLetterClass() As DebtPlus.Interfaces.Letters.ICompose
            Return New Composition(LetterType)
        End Function

        ''' <summary>
        ''' Generate the letter
        ''' </summary>
        Public Sub LetterThread()
            Using ltr As DebtPlus.Interfaces.Letters.ICompose = CreateNewLetterClass()
                AddHandler ltr.GetValue, AddressOf LetterThread_GetValue
                ltr.PrintLetter()
                RemoveHandler ltr.GetValue, AddressOf LetterThread_GetValue
            End Using
        End Sub

        ''' <summary>
        ''' Supply values for the parameters on the letter
        ''' </summary>
        Private Sub LetterThread_GetValue(ByVal Sender As Object, ByVal e As DebtPlus.Events.ParameterValueEventArgs)

            ' Return the client ID to the letter thread if needed
            If e.Name = ParameterValueEventArgs.name_client Then
                e.Value = context.ClientId

                ' Return the appointment ID to the letter thread if needed
            ElseIf e.Name = ParameterValueEventArgs.name_appointment Then
                If ClientAppointment > 0 Then
                    e.Value = ClientAppointment
                End If
            Else

                ' Return other specific items to the letter thread if needed
                Select Case e.Name.ToLower()
                    Case "client" ' Sometimes this is lower case.
                        e.Value = context.ClientId
                        Return

                    Case "salutation", "clients.salutation"
                        If Not String.IsNullOrWhiteSpace(context.ClientDs.clientRecord.salutation) Then
                            e.Value = context.ClientDs.clientRecord.salutation
                            Return
                        End If

                        ' The salutation is empty. Use the client's name record
                        e.Value = FindName(True)
                        Return

                    Case Else
                End Select
            End If
        End Sub

        ''' <summary>
        ''' Determine the current name setting for the client
        ''' </summary>
        Private Function FindName(ByVal UseApplicant As Boolean) As String

            ' Look for the appropriate person in the collection
            Dim q As DebtPlus.LINQ.people = context.ClientDs.colPeople.Find(Function(s) If(UseApplicant, s.Relation = 1, s.Relation <> 1))
            If q IsNot Nothing Then
                Dim nameRecord As DebtPlus.Interfaces.Names.IName = context.ClientDs.GetNameByID(q.NameID)
                If nameRecord IsNot Nothing Then
                    Return nameRecord.ToString()
                End If
            End If

            Return String.Empty
        End Function
    End Class
End NameSpace
