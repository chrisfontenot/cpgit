Imports DebtPlus.UI.Common
Imports DebtPlus.Svc.Debt
Imports DebtPlus.Utils
Imports DevExpress.Utils
Imports System.Data.SqlClient
Imports System.Linq

Namespace Service
    Public Class ClientUpdateDebtRecordList
        Inherits DebtRecordList

        ''' <summary>
        ''' Create a new instance of our class
        ''' </summary>
        Public Sub New()
            MyBase.New(GetType(ClientUpdateDebtRecord))
        End Sub

        Public Sub New(ByVal Context As ClientUpdateClass)
            MyBase.New(GetType(ClientUpdateDebtRecord))
        End Sub

        ''' <summary>
        ''' Delete the current debt
        ''' </summary>
        Public Overrides Sub RemoveDebt(ByVal obj As Object)

            Using dlg As New WaitDialogForm("Removing all references to the debt.", "Deleting debt")
                dlg.Show()

                Try
                    Using cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        cn.Open()
                        Using cmd As SqlCommand = New SqlCommand
                            cmd.Connection = cn
                            cmd.CommandText = "xpr_delete_debt"
                            cmd.CommandType = CommandType.StoredProcedure

                            ' This can take a while.
                            cmd.CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout)
                            cmd.Parameters.Add("@RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                            cmd.Parameters.Add("@client_creditor", SqlDbType.Int).Value = CType(obj, ClientUpdateDebtRecord).client_creditor
                            cmd.ExecuteNonQuery()
                        End Using
                    End Using

                    ' Remove the debt from the list of displayed debts.
                    MyBase.RemoveDebt(obj)

                Catch ex As SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error deleting debt information")

                Finally
                    If dlg IsNot Nothing Then
                        dlg.Close()
                    End If
                End Try
            End Using
        End Sub

        ''' <summary>
        ''' Reload the list of debts for the client
        ''' </summary>
        Public Overrides Sub RefreshData()
            Dim ClientID As Int32 = DebtListCurrentStorage.ClientId
            Dim waiting As New WaitDialogForm("Please Wait", "Reloading Debt Records")
            Using cm As New CursorManager()
                waiting.Show()

                Try
                    Clear()
                    Using cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        cn.Open()
                        Using txn As SqlTransaction = cn.BeginTransaction(IsolationLevel.ReadUncommitted)
                            ReadDebtInfo(cn, txn, ClientID)
                            txn.Commit()
                        End Using
                    End Using

                    ' Indicate that the debt list is different and has a new ID
                    DebtListCurrentStorage.PrivateID = DebtListStorage.GenerateID()
                    Sort()

                Finally
                    waiting.Close()
                End Try
            End Using
        End Sub

        ''' <summary>
        ''' Add the total of the disbursement factors to the list of possible answers
        ''' </summary>
        Public Overrides Function QueryFeeValue(ByVal item As String) As Object
            If item = "total_disbursement_factors" Then
                Return TotalDisbursementFactors()
            ElseIf item = "limit_monthly_max" Then
                Return True
            End If
            Return MyBase.QueryFeeValue(item)
        End Function

        ''' <summary>
        ''' Calculate the total of the disbursement factors for active creditors
        ''' </summary>
        Public Overridable Function TotalDisbursementFactors() As Decimal
            Return Me.Cast(Of ClientUpdateDebtRecord).Where(Function(s) s.IsProratable AndAlso Not s.IsFee AndAlso Not s.IsHeld).Sum(Function(s) s.display_disbursement_factor)
        End Function
    End Class
End Namespace
