#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Security.Cryptography
Imports DebtPlus.Data.Forms
Imports System.Text
Imports System.Linq
Imports DebtPlus.LINQ

Namespace Service
    Friend Class www_class
        Inherits Object

        Public Sub New()
            MyBase.new()
        End Sub

#Region "MD5"

        Public Shared Function MD5(ByVal InputString As String) As String

            ' Handle the missing password condition as a blank string.
            If String.IsNullOrWhiteSpace(InputString) Then
                Return String.Empty
            End If

            ' First, convert the string to an array of bytes
            Dim TempSource As Byte() = ASCIIEncoding.ASCII.GetBytes(InputString.ToLower())

            ' Next, encode the password stream
            Dim MD5_Class As New MD5CryptoServiceProvider()
            Dim TempHash As Byte() = MD5_Class.ComputeHash(TempSource)

            ' Finally, merge the byte stream to a string
            Dim sbPasswd As New StringBuilder()
            For Index As Int32 = 0 To TempHash.GetUpperBound(0)
                sbPasswd.Append(TempHash(Index).ToString("X2"))
            Next Index

            ' The encoded password is the combined string
            Return sbPasswd.ToString().ToUpper()
        End Function

#End Region

#Region "CreatePassword"

        Public Shared Function CreatePassword() As String

            ' Seed the random number generator with the time
            Randomize()

            ' The password is simple. It is a color followed by a special character and then a object
            Return Color() + Special() + word()
        End Function

        Private Shared Function Color() As String
            Dim colors() As String
            Dim index As Int32
            Dim rnd_value As Double

            ' Generate a list of the colors
            colors = New String() _
                {"blue", "green", "pink", "red", "purple", "magenta", "cyan", "brown", "yellow", "black", "silver",
                 "orange", "white", "tan"}

            ' Return a random color
            rnd_value = 3000 * Rnd()
            index = Convert.ToInt32(rnd_value) Mod colors.GetUpperBound(0)
            Return colors(index)
        End Function

        Private Shared Function Special() As String
            Dim specials As String
            Dim index As Int32
            Dim rnd_value As Double

            ' Generate a list of the special characters
            specials = "!@#$%^&*()-_+=[]{}:;"";><,.?/"

            ' Return a random special character
            rnd_value = 3000 * Rnd()
            index = Convert.ToInt32(rnd_value) Mod specials.Length
            Return specials.Substring(index, 1)
        End Function

        Private Shared Function word() As String
            Dim words() As String
            Dim index As Int32
            Dim rnd_value As Double

            ' Generate a list of the words
            words = New String() _
                {"hawk", "bird", "worm", "mouse", "automobile", "telephone", "radio", "lamp", "stove", "carpet", "towel",
                 "window", "tree", "face", "monkey", "watch", "bush", "word", "ground", "food"}

            ' Return a random word
            rnd_value = 3000 * Rnd()
            index = Convert.ToInt32(rnd_value) Mod words.GetUpperBound(0)
            Return words(index)
        End Function

#End Region
    End Class
End Namespace
