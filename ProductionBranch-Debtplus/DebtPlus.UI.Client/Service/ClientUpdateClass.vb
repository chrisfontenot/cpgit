#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Data.SqlClient
Imports System.Transactions
Imports DebtPlus.Interfaces.Debt
Imports DebtPlus.Interfaces.Client
Imports DebtPlus.Svc.Common
Imports DebtPlus.Svc.Debt
Imports DebtPlus.Svc.Client.Appointments
Imports DebtPlus.Svc.DataSets
Imports DebtPlus.Utils
Imports DebtPlus.Events
Imports DebtPlus.UI.Client.forms
Imports System.Threading
Imports System.Linq
Imports DebtPlus.LINQ

Namespace Service
    Public Class ClientUpdateClass
        Inherits Object
        Implements IClientUpdate
        Implements IClient
        Implements IContext
        Implements IDisposable

        ''' <summary>
        ''' Data storage for the client update class is kept in a different assembly
        ''' </summary>
        Friend ClientDs As ClientDataSet = Nothing

        ''' <summary>
        ''' Initialize the new instance of the class
        ''' </summary>
        Public Sub New()
            MyBase.New()
            ClientDs = New ClientDataSet()
            ClientDs.bc = New DebtPlus.LINQ.BusinessContext()
        End Sub

        Public Sub New(ByVal clientId As Int32, ByVal isCreated As Boolean)
            MyClass.New()
            Me.ClientDs.ClientId = clientId
            Me.isCreated = isCreated
        End Sub

        Private privateDebtRecords As ClientUpdateDebtRecordList = Nothing

        ''' <summary>
        ''' True if the client was created and the records are new. False if editing existing record.
        ''' </summary>
        Public Property isCreated As Boolean = False

        ''' <summary>
        ''' Return the pointer to the thread local storage area
        ''' </summary>
        Public ReadOnly Property Context() As ClientUpdateClass Implements IContext.Context
            Get
                Return Me
            End Get
        End Property

        ''' <summary>
        ''' List of debts for the client
        ''' </summary>
        Public Property DebtRecords() As ClientUpdateDebtRecordList
            Get
                Return privateDebtRecords
            End Get

            Set(ByVal value As ClientUpdateDebtRecordList)
                If privateDebtRecords IsNot Nothing Then
                    RemoveHandler privateDebtRecords.RecalculateMonthlyFee, AddressOf RecalculateMonthlyFee
                    RemoveHandler privateDebtRecords.QueryConfigFeeValue, AddressOf QueryConfigFeeHandler
                End If

                privateDebtRecords = value

                If privateDebtRecords IsNot Nothing Then
                    AddHandler privateDebtRecords.RecalculateMonthlyFee, AddressOf RecalculateMonthlyFee
                    AddHandler privateDebtRecords.QueryConfigFeeValue, AddressOf QueryConfigFeeHandler
                End If
            End Set
        End Property

        ''' <summary>
        ''' Client record information
        ''' </summary>
        Public Property ClientId() As Int32 Implements IClient.ClientId
            Get
                Return ClientDs.ClientId
            End Get
            Set(ByVal value As Int32)
                ClientDs.ClientId = value
            End Set
        End Property

        ''' <summary>
        ''' Display the dialog within the current thread
        ''' </summary>
        ''' <returns>The completion dialog result from the form</returns>
        Public Function ShowEditDialog(ClientId As Int32) As DialogResult Implements IClientUpdate.ShowEditDialog
            Return ShowEditDialog(ClientId, False)
        End Function

        ''' <summary>
        ''' Display the dialog within the current thread
        ''' </summary>
        ''' <returns>The completion dialog result from the form</returns>
        Public Function ShowEditDialog(ClientId As Int32, isCreated As Boolean) As DialogResult Implements IClientUpdate.ShowEditDialog
            Me.ClientId = ClientId
            Me.isCreated = isCreated

            Try
                ' Find the client information from the database
                Context.ClientDs.clientRecord = Context.ClientDs.bc.clients.Where(Function(s) s.Id = Context.ClientDs.ClientId).FirstOrDefault()
                If Context.ClientDs.clientRecord Is Nothing Then
                    DebtPlus.Data.Forms.MessageBox.Show("The client is no longer defined in the system. The edit operation has been cancelled.", "Clientd no longer present", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Return DialogResult.Cancel
                End If

                ' Load the people list.
                Context.ClientDs.colPeople = Context.ClientDs.bc.peoples.Where(Function(s) s.Client = Context.ClientDs.clientRecord.Id).ToList()

                ' Find the applicant. This is required since it is created when the client is created.
                Context.ClientDs.applicantRecord = Context.ClientDs.colPeople.Find(Function(s) s.Relation = 1)

                ' We require an applicant. We can not continue if there is no applicant. The client creation procedure creates the applicant and it can not be deleted.
                ' So, abort if there is no applicant.
                If Context.ClientDs.applicantRecord Is Nothing Then
                    DebtPlus.Data.Forms.MessageBox.Show("The applicant information is missing", "Can not continue", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Return DialogResult.Cancel
                End If

                ' Display the client information now
                Using frm As New Form_ClientUpdate(Context)
                    frm.ShowDialog()
                    SaveData()
                End Using

                ' The answer is always OK since there is no cancel button
                Return DialogResult.OK

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
                Return DialogResult.Cancel
            End Try
        End Function

        ''' <summary>
        ''' Save the data when the form is complete
        ''' </summary>
        Friend Sub SaveData()

            ' Save the debt records with a transaction to ensure that all goes out
            Dim retry As Boolean = True
            Do While retry
                retry = False

                Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                Dim txn As SqlTransaction = Nothing
                Try
                    cn.Open()
                    txn = cn.BeginTransaction()

                    ' Do the debt updates first
                    If DebtRecords IsNot Nothing Then
                        DebtRecords.SaveData(txn)
                    End If

                    txn.Commit()
                    txn.Dispose()
                    txn = Nothing

                Catch ex As SqlException
                    If ex.ErrorCode = 1205 Then ' this is the deadlock condition where the transaction was canceled
                        retry = True
                    Else
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating client information")
                    End If

                Finally
                    If txn IsNot Nothing Then
                        txn.Rollback()
                        txn.Dispose()
                    End If
                End Try
            Loop

            ' Find the list of changes to the client record.
            If Context.ClientDs.clientRecord.getChangedFieldItems.Count >= 1 Then
                Dim noteRecord As client_note = DebtPlus.LINQ.Factory.Manufacture_client_note()
                noteRecord.client = Context.ClientDs.ClientId
                noteRecord.type = 3

                ' Get a list of the fields that we are changing
                Dim sbNote As New System.Text.StringBuilder()
                Context.ClientDs.clientRecord.getChangedFieldItems.ForEach(Sub(s) sbNote.Append(FormatNoteField(s)))
                noteRecord.note = sbNote.ToString()

                Dim subjectText As String = String.Format("Changed {0}", String.Join(",", Context.ClientDs.clientRecord.getChangedFieldItems.Select(Function(s) s.FieldName)))
                If subjectText.Length > 77 Then
                    subjectText = subjectText.Substring(0, 77) + "..."
                End If
                noteRecord.subject = subjectText

                Context.ClientDs.bc.client_notes.InsertOnSubmit(noteRecord)
                Context.ClientDs.clientRecord.ClearChangedFieldItems()
            End If

            ' Find the list of changes to the ACH information
            If Context.ClientDs.clientACHRecord IsNot Nothing Then
                If Context.ClientDs.clientACHRecord.getChangedFieldItems.Count >= 1 Then
                    Dim noteRecord As client_note = DebtPlus.LINQ.Factory.Manufacture_client_note()
                    noteRecord.client = Context.ClientDs.ClientId
                    noteRecord.type = 3

                    ' Get a list of the fields that we are changing
                    Dim sbNote As New System.Text.StringBuilder()
                    Context.ClientDs.clientACHRecord.getChangedFieldItems.ForEach(Sub(s) sbNote.Append(FormatNoteField(s)))
                    noteRecord.note = sbNote.ToString()

                    Dim subjectText As String = String.Format("Changed ACH {0}", String.Join(",", Context.ClientDs.clientACHRecord.getChangedFieldItems.Select(Function(s) s.FieldName)))
                    If subjectText.Length > 77 Then
                        subjectText = subjectText.Substring(0, 77) + "..."
                    End If
                    noteRecord.subject = subjectText

                    Context.ClientDs.bc.client_notes.InsertOnSubmit(noteRecord)
                    Context.ClientDs.clientACHRecord.ClearChangedFieldItems()
                End If
            End If

            retry = True
            Do While retry
                retry = False

                Try
                    Context.ClientDs.bc.SubmitChanges()

                Catch ex As SqlException
                    If ex.ErrorCode = 1205 Then ' this is the deadlock condition where the transaction was canceled
                        retry = True
                    Else
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating client information")
                    End If
                End Try
            Loop
        End Sub

        ''' <summary>
        ''' Update the monthly fee when the figures change
        ''' </summary>
        Private Sub RecalculateMonthlyFee(ByVal sender As Object, ByVal e As BaseDebtList.RecalculateMonthlyFeeEventArgs)

            ' Find the X0001 debt
            Dim monthlyFeeDebt As ClientUpdateDebtRecord = CType(Context.DebtRecords.MonthlyFeeDebt, ClientUpdateDebtRecord)
            If monthlyFeeDebt IsNot Nothing AndAlso e.Debt IsNot monthlyFeeDebt Then

                Try
                    ' Get our shared instance of the fee pointer for later use. It is our own "private" copy.
                    Using fee As New MonthlyFeeCalculation()
                        AddHandler fee.QueryValue, AddressOf DisbursementFactorFeeQuery

                        Dim newFeeAmount As Decimal = fee.FeeAmount()
                        If newFeeAmount <> DebtPlus.Utils.Nulls.DDec(monthlyFeeDebt.disbursement_factor) Then
                            monthlyFeeDebt.disbursement_factor = newFeeAmount
                        End If

                        RemoveHandler fee.QueryValue, AddressOf DisbursementFactorFeeQuery
                    End Using

                Catch ex As System.Data.SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error calculating fee information")
                End Try
            End If
        End Sub

        ''' <summary>
        ''' Determine the value if we know for the requested item
        ''' </summary>
        Private Sub DisbursementFactorFeeQuery(Sender As Object, e As DebtPlus.Events.ParameterValueEventArgs)

            ' Translate the names that we want to see for this particular instance of the fee calculation
            With CType(Context.DebtRecords, IFeeable)
                Select Case e.Name
                    Case "sched_payment"
                        e.Value = .QueryFeeValue("disbursement_factor")
                        Exit Select

                    Case "dollars_disbursed" ' Total disbursements = total(disbursement_factor) since we don't have disbursements here.
                        e.Value = .QueryFeeValue("total_disbursement_factors")
                        Exit Select

                    Case Else ' All others simply pass through un-changed
                        e.Value = .QueryFeeValue(e.Name)
                End Select
            End With
        End Sub

        ''' <summary>
        ''' Handle requests from prorate for current values
        ''' </summary>
        Private Sub QueryConfigFeeHandler(ByVal sender As Object, e As DebtPlus.Events.ParameterValueEventArgs)
            Select Case e.Name
                Case "config_fee"
                    e.Value = Context.ClientDs.clientRecord.config_fee
                    Exit Select
            End Select
        End Sub

#Region " IDisposable Support "

        ''' <summary>
        ''' Handle the disposition of the class object
        ''' </summary>
        Private disposedValue As Boolean
        ' To detect redundant calls

        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not disposedValue Then
                disposedValue = True
                If disposing Then

                    ' Toss the debt records
                    If privateDebtRecords IsNot Nothing Then
                        With privateDebtRecords
                            RemoveHandler .RecalculateMonthlyFee, AddressOf RecalculateMonthlyFee
                            RemoveHandler .QueryConfigFeeValue, AddressOf QueryConfigFeeHandler
                            .Dispose()
                        End With
                        privateDebtRecords = Nothing
                    End If

                    ' Toss the appointments and the class' data storage item
                    CType(ClientDs, IDisposable).Dispose()
                End If
            End If
        End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

#End Region

        ''' <summary>
        ''' Local routine to translate the code numbers to suitable strings for the client record
        ''' </summary>
        Private Function FormatNoteField(item As DebtPlus.LINQ.ChangedFieldItem) As String

            ' Translate the fields which have code values to the suitable string. All others use the standard format routine.
            Select Case item.FieldName
                Case "counselor", "csr"
                    Return Helpers.FormatString(item.FieldName, Helpers.GetCounselorNameText(DebtPlus.Utils.Nulls.v_Int32(item.OldValue)), Helpers.GetCounselorNameText(DebtPlus.Utils.Nulls.v_Int32(item.NewValue)))

                Case "office"
                    Return Helpers.FormatString(item.FieldName, Helpers.GetOfficeText(DebtPlus.Utils.Nulls.v_Int32(item.OldValue)), Helpers.GetOfficeText(DebtPlus.Utils.Nulls.v_Int32(item.NewValue)))

                Case "referred_by"
                    Return Helpers.FormatString(item.FieldName, Helpers.GetReferredByText(DebtPlus.Utils.Nulls.v_Int32(item.OldValue)), Helpers.GetReferredByText(DebtPlus.Utils.Nulls.v_Int32(item.NewValue)))

                Case "ElectronicCorrespondence", "ElectronicStatements"
                    Return Helpers.FormatString(item.FieldName, Helpers.GetTriStateText(DebtPlus.Utils.Nulls.v_Int32(item.OldValue)), Helpers.GetTriStateText(DebtPlus.Utils.Nulls.v_Int32(item.NewValue)))

                Case "drop_reason"
                    Return Helpers.FormatString(item.FieldName, Helpers.GetDropReasonText(DebtPlus.Utils.Nulls.v_Int32(item.OldValue)), Helpers.GetDropReasonText(DebtPlus.Utils.Nulls.v_Int32(item.NewValue)))

                Case "language"
                    Return Helpers.FormatString(item.FieldName, Helpers.GetLanguageText(DebtPlus.Utils.Nulls.v_Int32(item.OldValue)), Helpers.GetLanguageText(DebtPlus.Utils.Nulls.v_Int32(item.NewValue)))

                Case "marital_status"
                    Return Helpers.FormatString(item.FieldName, Helpers.GetMarital_statusText(DebtPlus.Utils.Nulls.v_Int32(item.OldValue)), Helpers.GetMarital_statusText(DebtPlus.Utils.Nulls.v_Int32(item.NewValue)))

                Case "county"
                    Return Helpers.FormatString(item.FieldName, Helpers.GetCountyText(DebtPlus.Utils.Nulls.v_Int32(item.OldValue)), Helpers.GetCountyText(DebtPlus.Utils.Nulls.v_Int32(item.NewValue)))

                Case "config_fee"
                    Return Helpers.FormatString(item.FieldName, Helpers.GetConfigFeeText(DebtPlus.Utils.Nulls.v_Int32(item.OldValue)), Helpers.GetConfigFeeText(DebtPlus.Utils.Nulls.v_Int32(item.NewValue)))

                Case "cause_fin_problem1", "cause_fin_problem2", "cause_fin_problem3", "cause_fin_problem4"
                    Return Helpers.FormatString(item.FieldName, Helpers.GetFinancialProblemText(DebtPlus.Utils.Nulls.v_Int32(item.OldValue)), Helpers.GetFinancialProblemText(DebtPlus.Utils.Nulls.v_Int32(item.NewValue)))

                Case "bankruptcy_class"
                    Return Helpers.FormatString(item.FieldName, Helpers.GetBankruptcyClassTypeText(DebtPlus.Utils.Nulls.v_Int32(item.OldValue)), Helpers.GetBankruptcyClassTypeText(DebtPlus.Utils.Nulls.v_Int32(item.NewValue)))

                Case "method_first_contact"
                    Return Helpers.FormatString(item.FieldName, Helpers.GetContactMethodText(DebtPlus.Utils.Nulls.v_Int32(item.OldValue)), Helpers.GetContactMethodText(DebtPlus.Utils.Nulls.v_Int32(item.NewValue)))

                Case "current_program", "initial_program"
                    Return Helpers.FormatString(item.FieldName, Helpers.GetProgramParticipationText(DebtPlus.Utils.Nulls.v_Int32(item.OldValue)), Helpers.GetProgramParticipationText(DebtPlus.Utils.Nulls.v_Int32(item.NewValue)))

                Case Else
                    ' Fall out of the select statement to return the standard formatting routine.
            End Select

            ' Simply punt and record the item as a string. This works for strings, boolean and simple numbers
            Return Helpers.FormatString(item.FieldName, item.OldValue, item.NewValue)
        End Function
    End Class
End NameSpace
