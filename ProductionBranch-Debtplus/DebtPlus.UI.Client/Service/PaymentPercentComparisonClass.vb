Namespace Service

    ''' <summary>
    ''' Class that lists the information on the Payment Percentage lookup edit control
    ''' </summary>
    Public Class PaymentPercentComparisonClass
        Public Sub New()
        End Sub

        ''' <summary>
        ''' Percentage rate
        ''' </summary>
        Public Property rate As Double

        ''' <summary>
        ''' Description used for this percentage rate
        ''' </summary>
        Public Property description As String

        Public Sub New(ByVal rate As Double, ByVal description As String)
            MyClass.New()
            Me.rate = rate
            Me.description = description
        End Sub
    End Class
End Namespace
