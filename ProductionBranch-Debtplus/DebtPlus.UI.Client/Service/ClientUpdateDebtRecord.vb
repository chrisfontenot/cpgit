#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.ComponentModel
Imports DebtPlus.Interfaces.Debt
Imports DebtPlus.Svc.Debt
Imports System.Data.SqlClient
Imports System.Reflection

Namespace Service
    Friend Class ClientUpdateDebtRecord
        Inherits DebtRecord
        Implements ICloneable

        ''' <summary>
        ''' display_current_balance
        ''' </summary>
        Public Overrides ReadOnly Property display_current_balance() As Decimal
            Get
                ' If the creditor is marked as having a zero balance then keep it zero.
                If ccl_zero_balance Then Return 0D

                ' Otherwise, do the normal logic.
                If IsActive Then Return MyBase.display_current_balance
                Return 0D
            End Get
        End Property

        ''' <summary>
        ''' display_orig_balance
        ''' </summary>
        Public Overrides ReadOnly Property display_orig_balance() As Decimal
            Get
                If MyBase.IsActive Then Return MyBase.display_orig_balance
                Return 0D
            End Get
        End Property

        ''' <summary>
        ''' Is the debt just added to the list of debts?
        ''' </summary>
        Private privateAlwaysShowInDebtList As Boolean

        Public Overridable Property AlwaysShowInDebtList() As Boolean
            Get
                Return privateAlwaysShowInDebtList
            End Get
            Set(ByVal value As Boolean)
                SetProperty("AlwaysShowInDebtList", privateAlwaysShowInDebtList, value)
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub New(ByVal CloneCopy As ClientUpdateDebtRecord)
            MyClass.New()

            ' Make a copy of our variables
            With CloneCopy
                currentStorage = CType(CType(.currentStorage, ICloneable).Clone, RecordStorage)
                previousStorage = CType(CType(.previousStorage, ICloneable).Clone, RecordStorage)
                DebtCurrentStorage = CType(CType(.DebtCurrentStorage, ICloneable).Clone, DebtRecordStorage)
            End With

            ' Clear the flags and accept the changes to the current row
            privateIsEdit = False
            privateAlwaysShowInDebtList = CloneCopy.AlwaysShowInDebtList
        End Sub

        Public Sub New(ByVal AlwaysShowInDebtList As Boolean)
            MyClass.New()
            privateAlwaysShowInDebtList = AlwaysShowInDebtList
        End Sub

        ''' <summary>
        ''' Reload the current record from the database
        ''' </summary>
        Public Sub ReloadFromDataStore(ByVal cn As SqlClient.SqlConnection)
            ReloadFromDataStore(cn, Nothing)
        End Sub

        Public Sub ReloadFromDataStore(ByVal cn As SqlClient.SqlConnection, ByVal txn As SqlTransaction)

            ' Ensure that there is a record to be read. This is a serious error condition.
            If client_creditor <= 0 Then
                Throw New ApplicationException("debt record is not specified for the ReloadFromDataStore() operation")
            End If

            ' Process the new record. Tell it that we are loading the fields and not to trip events.
            BeginInit()
            Try
                Using cmd As New SqlClient.SqlCommand()
                    cmd.Connection = cn
                    cmd.Transaction = txn
                    cmd.CommandText = "SELECT [account_number],[balance_verify_by],[balance_verify_date],[check_payments],[client],[client_creditor],[client_name],[creditor_name],[contact_name],[created_by],[date_created],[date_disp_changed],[disbursement_factor],[dmp_interest],[dmp_payout_interest],[drop_date],[drop_reason],[drop_reason_sent],[expected_payout_date],[fairshare_pct_check],[fairshare_pct_eft],[hold_disbursements],[interest_this_creditor],[irs_form_on_file],[last_communication],[last_payment_date_b4_dmp],[last_stmt_date],[line_number],[message],[months_delinquent],[non_dmp_interest],[non_dmp_payment],[orig_dmp_payment],[payments_this_creditor],[percent_balance],[person],[prenote_date],[priority],[IsActive],[returns_this_creditor],[sched_payment],[send_bal_verify],[send_drop_notice],[start_date],[student_loan_release],[rpps_client_type_indicator],[terms],[verify_request_date],[balance_verification_release],[client_creditor_proposal],[proposal_balance],[proposal_status],[client_creditor_balance],[balance_client_creditor],[current_sched_payment],[orig_balance],[orig_balance_adjustment],[payments_month_0],[payments_month_1],[total_interest],[total_payments],[total_sched_payment],[creditor_interest],[creditor],[cr_creditor_name],[cr_division_name],[cr_payment_balance],[cr_min_accept_amt],[cr_min_accept_pct],[ccl_zero_balance],[ccl_prorate],[ccl_always_disburse],[ccl_agency_account],[first_payment],[first_payment_date],[first_payment_amt],[first_payment_type],[last_payment],[last_payment_date],[last_payment_amt],[last_payment_type],[payment_rpps_mask],[rpps_mask] FROM [view_debt_info] WHERE [client_creditor]=@client_creditor"
                    cmd.CommandType = CommandType.Text
                    cmd.Parameters.Add("@client_creditor", SqlDbType.Int).Value = client_creditor
                    Using rd As SqlClient.SqlDataReader = cmd.ExecuteReader(CommandBehavior.SingleRow) ' do NOT close the connection since we didn't create it.

                        ' Replace the data from the database
                        If rd.Read() Then

                            ' Save the debt type. It does not change if we reload the values.
                            Dim CurrentDebtType As DebtTypeEnum = DebtType

                            ' Toss the current storage
                            currentStorage = New RecordStorage()
                            previousStorage = New RecordStorage()

                            ' Process the properties in the current row
                            Dim Properties() As System.Reflection.PropertyInfo = Me.GetType.GetProperties()
                            For Each field As System.Reflection.PropertyInfo In Properties

                                ' Find the attributes for the property
                                Dim attributeList() As Object = field.GetCustomAttributes(GetType(DatabaseFieldAttribute), True)
                                If attributeList IsNot Nothing AndAlso attributeList.Length = 1 Then
                                    Dim customAttr As DatabaseFieldAttribute = TryCast(attributeList(0), DatabaseFieldAttribute)

                                    ' we need a custom attribute of the required type
                                    If customAttr IsNot Nothing Then
                                        Dim fieldname As String = customAttr.ColumnName
                                        Dim TypeOfField As DatabaseFieldAttribute.FieldType = customAttr.TypeOfField

                                        ' Find the field's column in the database table.
                                        For colNo As Int32 = 0 To rd.FieldCount - 1
                                            If Not rd.IsDBNull(colNo) Then
                                                Dim objValue As Object = rd.GetValue(colNo)
                                                Dim colName As String = rd.GetName(colNo)

                                                ' If the column matches the field name then we have found the field for this column.
                                                If String.Compare(colName, fieldname, True, System.Globalization.CultureInfo.InvariantCulture) = 0 Then

                                                    ' Convert the input data to the format that is expected by the object.
                                                    Select Case TypeOfField
                                                        Case DatabaseFieldAttribute.FieldType.TypeObject
                                                            field.SetValue(Me, objValue, Nothing)

                                                        Case DatabaseFieldAttribute.FieldType.TypeInt32
                                                            objValue = Convert.ToInt32(objValue)
                                                            field.SetValue(Me, objValue, Nothing)

                                                        Case DatabaseFieldAttribute.FieldType.TypeBoolean
                                                            objValue = Convert.ToBoolean(objValue)
                                                            field.SetValue(Me, objValue, Nothing)

                                                        Case DatabaseFieldAttribute.FieldType.TypeDate
                                                            objValue = Convert.ToDateTime(objValue)
                                                            field.SetValue(Me, objValue, Nothing)

                                                        Case DatabaseFieldAttribute.FieldType.TypeDecimal
                                                            objValue = Convert.ToDecimal(objValue)
                                                            field.SetValue(Me, objValue, Nothing)

                                                        Case DatabaseFieldAttribute.FieldType.TypeDouble
                                                            objValue = Convert.ToDouble(objValue)
                                                            field.SetValue(Me, objValue, Nothing)

                                                        Case DatabaseFieldAttribute.FieldType.TypeString
                                                            objValue = Convert.ToString(objValue)
                                                            field.SetValue(Me, objValue, Nothing)

                                                        Case Else
                                                            field.SetValue(Me, objValue, Nothing)
                                                    End Select
                                                End If
                                            End If
                                        Next
                                    End If
                                End If
                            Next

                            ' Restore the type of the debt and do any post-init processing
                            DebtType = CurrentDebtType
                        End If
                    End Using
                End Using

            Finally
                EndInit() ' Events would be nice from this point.
            End Try
        End Sub

#Region "IClonable"

        ''' <summary>
        ''' Make a copy of the current record
        ''' </summary>
        Public Function Clone() As Object Implements ICloneable.Clone
            Return New ClientUpdateDebtRecord(Me)
        End Function

#End Region

        ''' <summary>
        ''' Reassign the current debt
        ''' </summary>
        Public Function ReassignDebt(ByVal newCreditor As String, ByVal proposalStatus As Int32, ByVal unused As SqlConnection) As Int32

            Try
                Using cn As New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    cn.Open()

                    ' First reassign the debt and create a new debt with the new creditor reference
                    Using cmd As SqlCommand = New SqlCommand()
                        cmd.CommandText = "xpr_sell_debt"
                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Connection = cn

                        SqlCommandBuilder.DeriveParameters(cmd)
                        cmd.Parameters(1).Value = client_creditor
                        cmd.Parameters(2).Value = newCreditor
                        cmd.Parameters(3).Value = proposalStatus

                        cmd.ExecuteNonQuery()
                        Return Convert.ToInt32(cmd.Parameters(0).Value)
                    End Using
                End Using

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reassigning the debt")
            End Try

            Return -1
        End Function

        Protected Overrides Sub OnDataChanged(e As DebtPlus.Events.DataChangedEventArgs)
            MyBase.OnDataChanged(e)

            ' Look for additional fields
            Select Case e.PropertyName
                Case "ccl_zero_balance"
                    OnDataChanged(New DebtPlus.Events.DataChangedEventArgs("display_current_balance"))
                Case "IsActive"
                    OnDataChanged(New DebtPlus.Events.DataChangedEventArgs("display_current_balance"))
                    OnDataChanged(New DebtPlus.Events.DataChangedEventArgs("display_orig_balance"))
                Case Else
            End Select
        End Sub

    End Class
End Namespace
