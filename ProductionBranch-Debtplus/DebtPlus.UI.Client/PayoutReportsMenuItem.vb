#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Interfaces.Client
Imports DebtPlus.UI.Client.Service
Imports DevExpress.XtraBars
Imports DebtPlus.Interfaces.Reports
Imports DebtPlus.Reports

Friend Class PayoutReportsMenuItem
    Inherits ReportsMenuItem

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByVal ContextInfo As ClientUpdateClass, ByVal Name As String, ByVal rpt As DebtPlus.LINQ.report)
        MyBase.New(ContextInfo, Name, rpt)
    End Sub

    Public Sub New(ByVal text As String)
        MyBase.New(text)
    End Sub

    Public Sub New(ByVal text As String, ByVal OnClickHandler As ItemClickEventHandler)
        MyBase.New(text, OnClickHandler)
    End Sub

    Public Sub New(ByVal text As String, ByVal OnClickHandler As ItemClickEventHandler, ByVal Shortcut As Shortcut)
        MyBase.New(text, OnClickHandler, Shortcut)
    End Sub

    Protected Overrides Sub ReportThread()
        Dim rpt As DebtPlus.Reports.Client.PayoutDetail.ClientPayoutDetail = New DebtPlus.Reports.Client.PayoutDetail.ClientPayoutDetail(Context.DebtRecords)
        rpt.ClientId = Context.ClientId
        rpt.DisplayPreviewDialog()
        RaiseComplete()
    End Sub
End Class
