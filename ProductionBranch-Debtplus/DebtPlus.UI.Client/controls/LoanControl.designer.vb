Namespace controls

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class LoanControl
        Inherits ControlBaseClient

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.LoanDSADetailsControl1 = New DebtPlus.UI.Client.controls.LoanDSADetailsControl()
            Me.CheckEdit_UseInReports = New DevExpress.XtraEditors.CheckEdit()
            Me.CheckEdit_IntakeSameAsCurrent = New DevExpress.XtraEditors.CheckEdit()
            Me.CheckEdit_UseOrigLenderID = New DevExpress.XtraEditors.CheckEdit()
            Me.LenderControl_Origional = New DebtPlus.UI.Client.controls.LenderControl()
            Me.LoanDetailsControl_IntakeID = New DebtPlus.UI.Client.controls.LoanDetailsControl()
            Me.LenderControl_Intake = New DebtPlus.UI.Client.controls.LenderControl()
            Me.LookUpEdit_Position = New DevExpress.XtraEditors.LookUpEdit()
            Me.LoanDetailsControl_CurrentDetailID = New DebtPlus.UI.Client.controls.LoanDetailsControl()
            Me.TabbedControlGroup_Tabs = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem_Position = New DevExpress.XtraLayout.LayoutControlItem()
            Me.TabbedControlGroup1 = New DevExpress.XtraLayout.TabbedControlGroup()
            Me.LayoutControlGroup_CurrentLender = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.LayoutControlItem_CurrentLender = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlGroup_General = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlGroup_OriginalLender = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem_OriginalLender = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlGroup_Dsa = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.CheckEdit_UseInReports.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_IntakeSameAsCurrent.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_UseOrigLenderID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LenderControl_Origional, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LoanDetailsControl_IntakeID, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LenderControl_Intake, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_Position.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LoanDetailsControl_CurrentDetailID, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TabbedControlGroup_Tabs, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_Position, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TabbedControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup_CurrentLender, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_CurrentLender, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup_General, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup_OriginalLender, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_OriginalLender, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup_Dsa, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.LoanDSADetailsControl1)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_UseInReports)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_IntakeSameAsCurrent)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_UseOrigLenderID)
            Me.LayoutControl1.Controls.Add(Me.LenderControl_Origional)
            Me.LayoutControl1.Controls.Add(Me.LoanDetailsControl_IntakeID)
            Me.LayoutControl1.Controls.Add(Me.LenderControl_Intake)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_Position)
            Me.LayoutControl1.Controls.Add(Me.LoanDetailsControl_CurrentDetailID)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(515, 242, 250, 350)
            Me.LayoutControl1.Root = Me.TabbedControlGroup_Tabs
            Me.LayoutControl1.Size = New System.Drawing.Size(586, 435)
            Me.LayoutControl1.TabIndex = 0
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'LoanDSADetailsControl1
            '
            Me.LoanDSADetailsControl1.Location = New System.Drawing.Point(24, 70)
            Me.LoanDSADetailsControl1.Name = "LoanDSADetailsControl1"
            Me.LoanDSADetailsControl1.Size = New System.Drawing.Size(538, 341)
            Me.LoanDSADetailsControl1.TabIndex = 32
            '
            'CheckEdit_UseInReports
            '
            Me.CheckEdit_UseInReports.Location = New System.Drawing.Point(296, 12)
            Me.CheckEdit_UseInReports.Name = "CheckEdit_UseInReports"
            Me.CheckEdit_UseInReports.Properties.Caption = "Report on this loan for housing"
            Me.CheckEdit_UseInReports.Size = New System.Drawing.Size(278, 19)
            Me.CheckEdit_UseInReports.StyleController = Me.LayoutControl1
            Me.CheckEdit_UseInReports.TabIndex = 28
            '
            'CheckEdit_IntakeSameAsCurrent
            '
            Me.CheckEdit_IntakeSameAsCurrent.Location = New System.Drawing.Point(24, 70)
            Me.CheckEdit_IntakeSameAsCurrent.Name = "CheckEdit_IntakeSameAsCurrent"
            Me.CheckEdit_IntakeSameAsCurrent.Properties.Caption = "Same as Intake mortgage information"
            Me.CheckEdit_IntakeSameAsCurrent.Size = New System.Drawing.Size(538, 19)
            Me.CheckEdit_IntakeSameAsCurrent.StyleController = Me.LayoutControl1
            Me.CheckEdit_IntakeSameAsCurrent.TabIndex = 27
            Me.CheckEdit_IntakeSameAsCurrent.ToolTip = "If you want the current values to be the same as the intake items, then check thi" & _
        "s. Otherwise, specify new items."
            '
            'CheckEdit_UseOrigLenderID
            '
            Me.CheckEdit_UseOrigLenderID.EditValue = True
            Me.CheckEdit_UseOrigLenderID.Location = New System.Drawing.Point(24, 70)
            Me.CheckEdit_UseOrigLenderID.Name = "CheckEdit_UseOrigLenderID"
            Me.CheckEdit_UseOrigLenderID.Properties.Caption = "Same as Current Lender"
            Me.CheckEdit_UseOrigLenderID.Size = New System.Drawing.Size(538, 19)
            Me.CheckEdit_UseOrigLenderID.StyleController = Me.LayoutControl1
            Me.CheckEdit_UseOrigLenderID.TabIndex = 24
            Me.CheckEdit_UseOrigLenderID.ToolTip = "Check this to use the same information from the ""Current Lender"""
            '
            'LenderControl_Origional
            '
            Me.LenderControl_Origional.Enabled = False
            Me.LenderControl_Origional.IsHECMVisible = True
            Me.LenderControl_Origional.IsHPFVisible = True
            Me.LenderControl_Origional.Location = New System.Drawing.Point(24, 93)
            Me.LenderControl_Origional.Name = "LenderControl_Origional"
            Me.LenderControl_Origional.Size = New System.Drawing.Size(538, 318)
            Me.LenderControl_Origional.TabIndex = 16
            '
            'LoanDetailsControl_IntakeID
            '
            Me.LoanDetailsControl_IntakeID.DelinquencyMonths = Nothing
            Me.LoanDetailsControl_IntakeID.Location = New System.Drawing.Point(24, 70)
            Me.LoanDetailsControl_IntakeID.Name = "LoanDetailsControl_IntakeID"
            Me.LoanDetailsControl_IntakeID.Size = New System.Drawing.Size(538, 341)
            Me.LoanDetailsControl_IntakeID.TabIndex = 30
            '
            'LenderControl_Intake
            '
            Me.LenderControl_Intake.IsHECMVisible = True
            Me.LenderControl_Intake.IsHPFVisible = True
            Me.LenderControl_Intake.Location = New System.Drawing.Point(24, 99)
            Me.LenderControl_Intake.Name = "LenderControl_Intake"
            Me.LenderControl_Intake.Size = New System.Drawing.Size(538, 312)
            Me.LenderControl_Intake.TabIndex = 15
            '
            'LookUpEdit_Position
            '
            Me.LookUpEdit_Position.Location = New System.Drawing.Point(54, 12)
            Me.LookUpEdit_Position.Name = "LookUpEdit_Position"
            Me.LookUpEdit_Position.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.LookUpEdit_Position.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_Position.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_Position.Properties.DisplayMember = "description"
            Me.LookUpEdit_Position.Properties.NullText = ""
            Me.LookUpEdit_Position.Properties.ShowFooter = False
            Me.LookUpEdit_Position.Properties.ShowHeader = False
            Me.LookUpEdit_Position.Properties.SortColumnIndex = 1
            Me.LookUpEdit_Position.Properties.ValueMember = "Id"
            Me.LookUpEdit_Position.Size = New System.Drawing.Size(238, 20)
            Me.LookUpEdit_Position.StyleController = Me.LayoutControl1
            Me.LookUpEdit_Position.TabIndex = 4
            Me.LookUpEdit_Position.ToolTip = "Is this a first or second mortgage on the property? (A line of credit is a type o" & _
        "f ""second mortgage"" for reporting purposes.)"
            '
            'LoanDetailsControl_CurrentDetailID
            '
            Me.LoanDetailsControl_CurrentDetailID.DelinquencyMonths = Nothing
            Me.LoanDetailsControl_CurrentDetailID.Location = New System.Drawing.Point(24, 93)
            Me.LoanDetailsControl_CurrentDetailID.Name = "LoanDetailsControl_CurrentDetailID"
            Me.LoanDetailsControl_CurrentDetailID.Size = New System.Drawing.Size(538, 318)
            Me.LoanDetailsControl_CurrentDetailID.TabIndex = 31
            '
            'TabbedControlGroup_Tabs
            '
            Me.TabbedControlGroup_Tabs.CustomizationFormText = "Loan Information"
            Me.TabbedControlGroup_Tabs.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem_Position, Me.TabbedControlGroup1, Me.LayoutControlItem11})
            Me.TabbedControlGroup_Tabs.Location = New System.Drawing.Point(0, 0)
            Me.TabbedControlGroup_Tabs.Name = "Root"
            Me.TabbedControlGroup_Tabs.Size = New System.Drawing.Size(586, 435)
            Me.TabbedControlGroup_Tabs.Text = "Root"
            Me.TabbedControlGroup_Tabs.TextVisible = False
            '
            'LayoutControlItem_Position
            '
            Me.LayoutControlItem_Position.Control = Me.LookUpEdit_Position
            Me.LayoutControlItem_Position.CustomizationFormText = "Loan Position 1st/2nd"
            Me.LayoutControlItem_Position.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem_Position.Name = "LayoutControlItem_Position"
            Me.LayoutControlItem_Position.Size = New System.Drawing.Size(284, 24)
            Me.LayoutControlItem_Position.Text = "Position"
            Me.LayoutControlItem_Position.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
            Me.LayoutControlItem_Position.TextSize = New System.Drawing.Size(37, 13)
            Me.LayoutControlItem_Position.TextToControlDistance = 5
            '
            'TabbedControlGroup1
            '
            Me.TabbedControlGroup1.CustomizationFormText = "Lender Tabs"
            Me.TabbedControlGroup1.Location = New System.Drawing.Point(0, 24)
            Me.TabbedControlGroup1.Name = "TabbedControlGroup1"
            Me.TabbedControlGroup1.SelectedTabPage = Me.LayoutControlGroup_OriginalLender
            Me.TabbedControlGroup1.SelectedTabPageIndex = 3
            Me.TabbedControlGroup1.Size = New System.Drawing.Size(566, 391)
            Me.TabbedControlGroup1.TabPages.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup_General, Me.LayoutControlGroup1, Me.LayoutControlGroup_CurrentLender, Me.LayoutControlGroup_OriginalLender, Me.LayoutControlGroup_Dsa})
            Me.TabbedControlGroup1.Text = "Lenders"
            Me.TabbedControlGroup1.TextLocation = DevExpress.Utils.Locations.[Default]
            '
            'LayoutControlGroup_CurrentLender
            '
            Me.LayoutControlGroup_CurrentLender.CustomizationFormText = "Intake Lender"
            Me.LayoutControlGroup_CurrentLender.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.EmptySpaceItem2, Me.LayoutControlItem_CurrentLender})
            Me.LayoutControlGroup_CurrentLender.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup_CurrentLender.Name = "LayoutControlGroup_CurrentLender"
            Me.LayoutControlGroup_CurrentLender.Size = New System.Drawing.Size(542, 345)
            Me.LayoutControlGroup_CurrentLender.Text = "Intake Lender"
            '
            'EmptySpaceItem2
            '
            Me.EmptySpaceItem2.AllowHotTrack = False
            Me.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Location = New System.Drawing.Point(0, 0)
            Me.EmptySpaceItem2.MaxSize = New System.Drawing.Size(0, 29)
            Me.EmptySpaceItem2.MinSize = New System.Drawing.Size(1, 19)
            Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Size = New System.Drawing.Size(542, 29)
            Me.EmptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
            Me.EmptySpaceItem2.Text = "EmptySpaceItem2"
            Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
            '
            'LayoutControlItem_CurrentLender
            '
            Me.LayoutControlItem_CurrentLender.Control = Me.LenderControl_Intake
            Me.LayoutControlItem_CurrentLender.CustomizationFormText = "Current Lender Record"
            Me.LayoutControlItem_CurrentLender.Location = New System.Drawing.Point(0, 29)
            Me.LayoutControlItem_CurrentLender.Name = "LayoutControlItem_CurrentLender"
            Me.LayoutControlItem_CurrentLender.Size = New System.Drawing.Size(542, 316)
            Me.LayoutControlItem_CurrentLender.Text = "Current Lender"
            Me.LayoutControlItem_CurrentLender.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem_CurrentLender.TextToControlDistance = 0
            Me.LayoutControlItem_CurrentLender.TextVisible = False
            '
            'LayoutControlGroup_General
            '
            Me.LayoutControlGroup_General.CustomizationFormText = "General Information Group"
            Me.LayoutControlGroup_General.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem2})
            Me.LayoutControlGroup_General.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup_General.Name = "LayoutControlGroup_General"
            Me.LayoutControlGroup_General.Size = New System.Drawing.Size(542, 345)
            Me.LayoutControlGroup_General.Text = "Intake"
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.LoanDetailsControl_IntakeID
            Me.LayoutControlItem2.CustomizationFormText = "LayoutControlItem2"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem2.MinSize = New System.Drawing.Size(204, 172)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(542, 345)
            Me.LayoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
            Me.LayoutControlItem2.Text = "Intake Information"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem2.TextToControlDistance = 0
            Me.LayoutControlItem2.TextVisible = False
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "Post"
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem3, Me.LayoutControlItem10})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(542, 345)
            Me.LayoutControlGroup1.Text = "Post"
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.LoanDetailsControl_CurrentDetailID
            Me.LayoutControlItem3.CustomizationFormText = "LayoutControlItem3"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 23)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(542, 322)
            Me.LayoutControlItem3.Text = "LayoutControlItem3"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem3.TextToControlDistance = 0
            Me.LayoutControlItem3.TextVisible = False
            '
            'LayoutControlItem10
            '
            Me.LayoutControlItem10.Control = Me.CheckEdit_IntakeSameAsCurrent
            Me.LayoutControlItem10.CustomizationFormText = "Intake Same As Current"
            Me.LayoutControlItem10.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem10.Name = "LayoutControlItem10"
            Me.LayoutControlItem10.Size = New System.Drawing.Size(542, 23)
            Me.LayoutControlItem10.Text = "Same As Current"
            Me.LayoutControlItem10.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem10.TextToControlDistance = 0
            Me.LayoutControlItem10.TextVisible = False
            '
            'LayoutControlGroup_OriginalLender
            '
            Me.LayoutControlGroup_OriginalLender.CustomizationFormText = "Original Lender"
            Me.LayoutControlGroup_OriginalLender.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem_OriginalLender, Me.LayoutControlItem1})
            Me.LayoutControlGroup_OriginalLender.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup_OriginalLender.Name = "LayoutControlGroup_OriginalLender"
            Me.LayoutControlGroup_OriginalLender.Size = New System.Drawing.Size(542, 345)
            Me.LayoutControlGroup_OriginalLender.Text = "Original Lender"
            '
            'LayoutControlItem_OriginalLender
            '
            Me.LayoutControlItem_OriginalLender.Control = Me.LenderControl_Origional
            Me.LayoutControlItem_OriginalLender.CustomizationFormText = "Original Lender Record"
            Me.LayoutControlItem_OriginalLender.Location = New System.Drawing.Point(0, 23)
            Me.LayoutControlItem_OriginalLender.Name = "LayoutControlItem_OriginalLender"
            Me.LayoutControlItem_OriginalLender.Size = New System.Drawing.Size(542, 322)
            Me.LayoutControlItem_OriginalLender.Text = "Original Lender"
            Me.LayoutControlItem_OriginalLender.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem_OriginalLender.TextToControlDistance = 0
            Me.LayoutControlItem_OriginalLender.TextVisible = False
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.CheckEdit_UseOrigLenderID
            Me.LayoutControlItem1.CustomizationFormText = "LayoutControlItem1"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(542, 23)
            Me.LayoutControlItem1.Text = "LayoutControlItem1"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem1.TextToControlDistance = 0
            Me.LayoutControlItem1.TextVisible = False
            '
            'LayoutControlGroup_Dsa
            '
            Me.LayoutControlGroup_Dsa.CustomizationFormText = "DSA"
            Me.LayoutControlGroup_Dsa.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem4})
            Me.LayoutControlGroup_Dsa.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup_Dsa.Name = "LayoutControlGroup_Dsa"
            Me.LayoutControlGroup_Dsa.Size = New System.Drawing.Size(542, 345)
            Me.LayoutControlGroup_Dsa.Text = "DSA"
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.LoanDSADetailsControl1
            Me.LayoutControlItem4.CustomizationFormText = "LayoutControlItem4"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(542, 345)
            Me.LayoutControlItem4.Text = "LayoutControlItem4"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem4.TextToControlDistance = 0
            Me.LayoutControlItem4.TextVisible = False
            '
            'LayoutControlItem11
            '
            Me.LayoutControlItem11.Control = Me.CheckEdit_UseInReports
            Me.LayoutControlItem11.CustomizationFormText = "LayoutControlItem11"
            Me.LayoutControlItem11.Location = New System.Drawing.Point(284, 0)
            Me.LayoutControlItem11.Name = "LayoutControlItem11"
            Me.LayoutControlItem11.Size = New System.Drawing.Size(282, 24)
            Me.LayoutControlItem11.Text = "LayoutControlItem11"
            Me.LayoutControlItem11.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem11.TextToControlDistance = 0
            Me.LayoutControlItem11.TextVisible = False
            '
            'LoanControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.LayoutControl1)
            Me.Name = "LoanControl"
            Me.Size = New System.Drawing.Size(586, 435)
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.CheckEdit_UseInReports.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_IntakeSameAsCurrent.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_UseOrigLenderID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LenderControl_Origional, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LoanDetailsControl_IntakeID, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LenderControl_Intake, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_Position.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LoanDetailsControl_CurrentDetailID, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TabbedControlGroup_Tabs, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_Position, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TabbedControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup_CurrentLender, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_CurrentLender, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup_General, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup_OriginalLender, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_OriginalLender, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup_Dsa, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Protected Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Protected Friend WithEvents LookUpEdit_Position As DevExpress.XtraEditors.LookUpEdit
        Protected Friend WithEvents TabbedControlGroup_Tabs As DevExpress.XtraLayout.LayoutControlGroup
        Protected Friend WithEvents LayoutControlItem_Position As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LenderControl_Origional As LenderControl
        Friend WithEvents LenderControl_Intake As LenderControl
        Protected Friend WithEvents TabbedControlGroup1 As DevExpress.XtraLayout.TabbedControlGroup
        Protected Friend WithEvents LayoutControlGroup_General As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents CheckEdit_UseOrigLenderID As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents LayoutControlGroup_OriginalLender As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem_OriginalLender As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlGroup_CurrentLender As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem_CurrentLender As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents CheckEdit_IntakeSameAsCurrent As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents CheckEdit_UseInReports As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LoanDetailsControl_IntakeID As LoanDetailsControl
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LoanDetailsControl_CurrentDetailID As LoanDetailsControl
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlGroup_Dsa As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LoanDSADetailsControl1 As DebtPlus.UI.Client.controls.LoanDSADetailsControl
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    End Class
End Namespace