#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.ComponentModel
Imports DevExpress.XtraEditors.Controls
Imports System.Linq
Imports DebtPlus.LINQ
Imports DebtPlus.Utils

Namespace controls

    Friend Class LoanDetailsControl

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler LookUpEdit_FinanceTypeCD.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler LookUpEdit_LoanTypeCD.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler LookUpEdit_MortgageTypeCD.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler comboBox_DelinquencyMonths.Validated, AddressOf comboBox_DelinquencyMonths_Validated
            AddHandler comboBox_DelinquencyMonths.Validating, AddressOf comboBox_DelinquencyMonths_Validating
        End Sub

        Private Sub UnRegisterHandlers()
            AddHandler LookUpEdit_FinanceTypeCD.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler LookUpEdit_LoanTypeCD.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler LookUpEdit_MortgageTypeCD.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler comboBox_DelinquencyMonths.Validated, AddressOf comboBox_DelinquencyMonths_Validated
            AddHandler comboBox_DelinquencyMonths.Validating, AddressOf comboBox_DelinquencyMonths_Validating
        End Sub

        Private OriginalData As DebtPlus.LINQ.Housing_loan_detail

        ''' <summary>
        ''' This is the ID of the record in the database
        ''' </summary>
        <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), DefaultValue(DirectCast(Nothing, System.Object))> _
        Public Property EditValue As System.Nullable(Of Int32)
            Get
                Return SaveRecord()
            End Get
            Set(ByVal value As System.Nullable(Of Int32))
                UnRegisterHandlers()
                Try

                    ' Load the delinquency control items
                    comboBox_DelinquencyMonths.Properties.Items.Clear()
                    comboBox_DelinquencyMonths.Properties.Items.Add(New DebtPlus.Data.Controls.ComboboxItem("Current", 0))
                    comboBox_DelinquencyMonths.Properties.Items.Add(New DebtPlus.Data.Controls.ComboboxItem("Current but Struggling", -1))

                    LookUpEdit_FinanceTypeCD.Properties.DataSource = DebtPlus.LINQ.Cache.Housing_FinancingType.getList()
                    LookUpEdit_MortgageTypeCD.Properties.DataSource = DebtPlus.LINQ.Cache.Housing_MortgageType.getList()
                    LookUpEdit_LoanTypeCD.Properties.DataSource = DebtPlus.LINQ.Cache.Housing_LoanType.getList()

                OriginalData = ReadRecord(value)
                SetCurrentValues(OriginalData)
                Finally
                    RegisterHandlers()
                End Try
            End Set
        End Property

        ''' <summary>
        ''' Load the editing controls from the current record
        ''' </summary>
        Private Sub SetCurrentValues(ByVal values As DebtPlus.LINQ.Housing_loan_detail)

            UnRegisterHandlers()
            Try
                CheckEdit_ARM_Reset.Checked = values.ARM_Reset
                CheckEdit_FHA_VA_Insured_Loan.Checked = values.FHA_VA_Insured_Loan
                LookUpEdit_FinanceTypeCD.EditValue = values.FinanceTypeCD
                CheckEdit_Hybrid_ARM_Loan.Checked = values.Hybrid_ARM_Loan
                CheckEdit_Interest_Only_Loan.Checked = values.Interest_Only_Loan
                PercentEdit_InterestRate.EditValue = values.InterestRate
                LookUpEdit_LoanTypeCD.EditValue = values.LoanTypeCD
                LookUpEdit_MortgageTypeCD.EditValue = values.MortgageTypeCD
                CheckEdit_Option_ARM_Loan.Checked = values.Option_ARM_Loan
                CalcEdit_Payment.EditValue = values.Payment
                CheckEdit_Privately_Held_Loan.Checked = values.Privately_Held_Loan

            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Retrieve the desired records.
        ''' </summary>
        Private Function ReadRecord(ByVal RecordID As System.Nullable(Of Int32)) As DebtPlus.LINQ.Housing_loan_detail
            Dim NewValues As DebtPlus.LINQ.Housing_loan_detail = Nothing
            If RecordID.GetValueOrDefault(0) > 0 Then
                Using bc As New DebtPlus.LINQ.BusinessContext()
                    Dim id As Int32 = RecordID.Value
                    NewValues = bc.Housing_loan_details.Where(Function(b) b.Id = id).FirstOrDefault()
                End Using
            End If

            If NewValues Is Nothing Then
                NewValues = DebtPlus.LINQ.Factory.Manufacture_housing_loan_detail()

                ' Do not set the ID value. Leave it as zero to indicate that the record is "new".
            End If

            Return NewValues
        End Function

        ''' <summary>
        ''' Save the current information to the database
        ''' </summary>
        Private Function SaveRecord() As System.Nullable(Of Int32)
            UnRegisterHandlers()
            Try
                Dim values As DebtPlus.LINQ.Housing_loan_detail = GetCurrentValues()
                Using bc As New DebtPlus.LINQ.BusinessContext()

                    ' If the record can be located then update the record.
                    If values.Id > 0 Then
                        Dim q As DebtPlus.LINQ.Housing_loan_detail = bc.Housing_loan_details.Where(Function(s) s.Id = values.Id).FirstOrDefault()
                        If q IsNot Nothing Then
                            q.ARM_Reset = values.ARM_Reset
                            q.FHA_VA_Insured_Loan = values.FHA_VA_Insured_Loan
                            q.FinanceTypeCD = values.FinanceTypeCD
                            q.Hybrid_ARM_Loan = values.Hybrid_ARM_Loan
                            q.Interest_Only_Loan = values.Interest_Only_Loan
                            q.InterestRate = values.InterestRate
                            q.LoanTypeCD = values.LoanTypeCD
                            q.MortgageTypeCD = values.MortgageTypeCD
                            q.Option_ARM_Loan = values.Option_ARM_Loan
                            q.Payment = values.Payment
                            q.Privately_Held_Loan = values.Privately_Held_Loan

                            ' Submit the changed dataset to the database
                            bc.SubmitChanges()

                            ' Update the current copy with the database settings
                            OriginalData = q
                            Return OriginalData.Id
                        End If
                    End If

                    ' Add the record to the database if it is not the same as an empty record
                    ' THis record is never NULL. Our pointer must exist if we are needed. The only
                    ' time that something is null is when we are not used and the parent takes care
                    ' of that condition.
                    bc.Housing_loan_details.InsertOnSubmit(values)
                    bc.SubmitChanges()
                    OriginalData = values
                    Return OriginalData.Id
                End Using

            Finally
                RegisterHandlers()
            End Try

            ' Return the value as "null".
            Return New Int32()
        End Function

        Private Function GetCurrentValues() As DebtPlus.LINQ.Housing_loan_detail

            ' Get a new (blank) record and initialize it with the current default values.
            Dim record As DebtPlus.LINQ.Housing_loan_detail = DebtPlus.LINQ.Factory.Manufacture_housing_loan_detail()
            record.Id = OriginalData.Id ' Here we do want the ID to be the original value.

            record.ARM_Reset = CheckEdit_ARM_Reset.Checked
            record.FHA_VA_Insured_Loan = CheckEdit_FHA_VA_Insured_Loan.Checked
            record.FinanceTypeCD = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_FinanceTypeCD.EditValue)
            record.Hybrid_ARM_Loan = CheckEdit_Hybrid_ARM_Loan.Checked
            record.Interest_Only_Loan = CheckEdit_Interest_Only_Loan.Checked
            record.InterestRate = DebtPlus.Utils.Nulls.v_Double(PercentEdit_InterestRate.EditValue)
            record.LoanTypeCD = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_LoanTypeCD.EditValue)
            record.MortgageTypeCD = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_MortgageTypeCD.EditValue)
            record.Option_ARM_Loan = CheckEdit_Option_ARM_Loan.Checked
            record.Payment = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_Payment.EditValue).GetValueOrDefault(0D)
            record.Privately_Held_Loan = CheckEdit_Privately_Held_Loan.Checked

            Return record
        End Function

        Private Sub Percent_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)
            Dim NewValue As Double = 0.0#
            If e.NewValue IsNot Nothing AndAlso e.NewValue IsNot DBNull.Value Then
                If Not Double.TryParse(Convert.ToString(e.NewValue), NewValue) Then
                    e.Cancel = True
                    Return
                End If
            End If
            e.Cancel = (NewValue < 0.0#)
        End Sub

        Public Shared Function FormatDelinquency(ByVal input As System.Nullable(Of System.Int32)) As String

            If Not input.HasValue Then
                Return String.Empty
            End If

            If input.Value = 0 Then
                Return "Current"
            End If

            If input.Value = -1 Then
                Return "Struggling"
            End If

            Return String.Format("{0:n0}", input.Value)
        End Function

        Public Property DelinquencyMonths As System.Nullable(Of Int32)
            Get
                Return getDelinquencyMonths()
            End Get
            Set(value As System.Nullable(Of Int32))
                setDelinquencyMonths(value)
            End Set
        End Property

        ''' <summary>
        ''' Return the value for the delinquency months
        ''' </summary>
        Private Function getDelinquencyMonths() As System.Nullable(Of Int32)

            ' If there is something selected then use the value from the entry
            If comboBox_DelinquencyMonths.SelectedIndex >= 0 Then
                Return DirectCast(DirectCast(comboBox_DelinquencyMonths.SelectedItem, DebtPlus.Data.Controls.ComboboxItem).value, Int32)
            End If

            Dim strItem As String = DebtPlus.Utils.Format.Strings.DigitsOnly(comboBox_DelinquencyMonths.Text)
            Dim intValue As Int32
            If Int32.TryParse(strItem, intValue) Then
                If intValue > 0 Then
                    Return intValue
                End If
            End If

            ' The setting is invalid
            Return New System.Nullable(Of Int32)()
        End Function

        Private Sub setDelinquencyMonths(ByVal value As System.Nullable(Of Int32))
            If Not value.HasValue Then
                setDelinquencyMonths(0)
                Return
            End If

            If value.Value = -1 Then
                comboBox_DelinquencyMonths.SelectedIndex = 1
                Return
            End If

            If value.Value > 0 Then
                comboBox_DelinquencyMonths.SelectedIndex = -1
                comboBox_DelinquencyMonths.Text = String.Format("{0:n0}", value.Value)
                Return
            End If

            comboBox_DelinquencyMonths.SelectedIndex = 0
        End Sub

        ''' <summary>
        ''' Handle the validation event for the delinquency status
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub comboBox_DelinquencyMonths_Validated(sender As Object, e As System.EventArgs)

            ' If an item is chosen then it is complete
            If comboBox_DelinquencyMonths.SelectedIndex >= 0 Then
                Return
            End If

            UnRegisterHandlers()
            Try

                ' Retrieve the numerical value from the text buffer
                Dim strValue As String = DebtPlus.Utils.Format.Strings.DigitsOnly(comboBox_DelinquencyMonths.Text.Trim())
                If strValue <> String.Empty Then
                    Dim intValue As Int32
                    If Int32.TryParse(strValue, intValue) Then
                        comboBox_DelinquencyMonths.Text = String.Format("{0:n0}", intValue)
                        Return
                    End If
                End If

                ' There is no month entry. Use "Current"
                comboBox_DelinquencyMonths.SelectedIndex = 0
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Sub comboBox_DelinquencyMonths_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs)
            comboBox_DelinquencyMonths.ToolTip = String.Empty

            ' Accept any value that is listed in the list
            If comboBox_DelinquencyMonths.SelectedIndex >= 0 Then
                Return
            End If

            ' Accept a missing value
            If String.IsNullOrWhiteSpace(comboBox_DelinquencyMonths.Text) Then
                Return
            End If

            ' The value must be numeric at this point
            Dim intValue As Int32
            If Not Int32.TryParse(comboBox_DelinquencyMonths.Text.Trim(), intValue) Then
                comboBox_DelinquencyMonths.SelectedIndex = 0
                Return
            End If

            ' Limit the value to being suitable
            If intValue < 0 Then
                comboBox_DelinquencyMonths.SelectedIndex = 0
                Return
            End If

            comboBox_DelinquencyMonths.SelectedIndex = -1
            comboBox_DelinquencyMonths.Text = String.Format("{0:f0}", intValue)

            Dim formattedString As String = String.Empty
            Dim formattedYears As String = String.Empty
            Dim formattedMonths As String = String.Empty

            Dim years As Int32 = intValue \ 12
            Dim months As Int32 = intValue Mod 12

            If years > 0 Then
                If years = 1 Then
                    formattedYears = "1 year"
                Else
                    formattedYears = String.Format("{0:n0} years", years)
                End If
            End If

            If months > 0 Then
                If months = 1 Then
                    formattedMonths = "1 month"
                Else
                    formattedMonths = String.Format("{0:n0} months", years)
                End If
            End If

            If String.IsNullOrEmpty(formattedMonths) Then
                formattedString = formattedYears
            Else
                If Not String.IsNullOrEmpty(formattedYears) Then
                    formattedString = formattedYears + " and " + formattedMonths
                Else
                    formattedString = formattedMonths
                End If
            End If

            comboBox_DelinquencyMonths.ToolTip = formattedString
        End Sub
    End Class
End Namespace