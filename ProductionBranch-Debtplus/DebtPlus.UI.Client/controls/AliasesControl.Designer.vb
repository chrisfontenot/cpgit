Namespace controls
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class AliasesControl
        Inherits MyGridControlClient

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then components.Dispose()
                End If
                components = Nothing
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.GridColumn_Name = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_Alias = New DevExpress.XtraGrid.Columns.GridColumn()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PopupMenu_Items, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridView1
            '
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_Alias, Me.GridColumn_Name})
            '
            'GridColumn_Name
            '
            Me.GridColumn_Name.Caption = "Name"
            Me.GridColumn_Name.CustomizationCaption = "Item Name"
            Me.GridColumn_Name.FieldName = "additional"
            Me.GridColumn_Name.Name = "GridColumn_Name"
            Me.GridColumn_Name.OptionsColumn.AllowEdit = False
            Me.GridColumn_Name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_Name.OptionsColumn.ReadOnly = True
            Me.GridColumn_Name.Visible = True
            Me.GridColumn_Name.VisibleIndex = 0
            Me.GridColumn_Name.Width = 518
            '
            'GridColumn_Alias
            '
            Me.GridColumn_Alias.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Alias.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Alias.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Alias.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Alias.Caption = "ID"
            Me.GridColumn_Alias.CustomizationCaption = "Record ID"
            Me.GridColumn_Alias.FieldName = "Client_addkey"
            Me.GridColumn_Alias.Name = "GridColumn_Alias"
            Me.GridColumn_Alias.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_Alias.OptionsColumn.ReadOnly = True
            '
            'AliasesControl
            '
            Me.Name = "AliasesControl"
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PopupMenu_Items, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
        End Sub
        Friend WithEvents GridColumn_Name As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Alias As DevExpress.XtraGrid.Columns.GridColumn
    End Class
End Namespace