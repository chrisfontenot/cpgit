#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.Interfaces.Debt
Imports DebtPlus.Svc.Debt
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.UI.Client.Service
Imports DevExpress.Data.Filtering.Exceptions
Imports System.IO
Imports System.Linq
Imports DebtPlus.LINQ

Namespace controls

    Friend Class AdjustDepositDebtControl

        ''' <summary>
        ''' Initialize the new class instance
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            RegisterHandlers()
        End Sub

        ''' <summary>
        ''' Register the event handlers
        ''' </summary>
        Private Sub RegisterHandlers()
            AddHandler GridView1.ValidatingEditor, AddressOf GridView1_ValidatingEditor
            AddHandler GridView1.Layout, AddressOf LayoutChanged
            AddHandler Load, AddressOf Control_Load
            AddHandler GridView1.Layout, AddressOf LayoutChanged
        End Sub

        ''' <summary>
        ''' Remove the event handler registration
        ''' </summary>
        Private Sub UnRegisterHandlers()
            RemoveHandler GridView1.ValidatingEditor, AddressOf GridView1_ValidatingEditor
            RemoveHandler GridView1.Layout, AddressOf LayoutChanged
            RemoveHandler Load, AddressOf Control_Load
            RemoveHandler GridView1.Layout, AddressOf LayoutChanged
        End Sub

        ''' <summary>
        ''' Process the read of the form information
        ''' </summary>
        Public Overrides Sub ReadForm(bc As DebtPlus.LINQ.BusinessContext)

            UnRegisterHandlers()
            Try

                ' Copy the disbursement factor to the working column called "debit_amt"
                ' We want to allow the CANCEL button to "not do anything" so it does nothing
                ' It takes the OK button to copy the debit_amt back to the disbursement factor.
                For Each Debt As ClientUpdateDebtRecord In Context.DebtRecords
                    Debt.debit_amt = Debt.disbursement_factor
                Next

                ' Bind the grid to the debts table but take only the active records
                GridControl1.DataSource = Context.DebtRecords

                ' Bind the control to the items which are only active
                GridView1.BeginUpdate()
                GridView1.OptionsView.ShowFilterPanelMode = ShowFilterPanelMode.Never
                If Context.DebtRecords.Count > 0 Then
                    Try
                        GridView1.ActiveFilter.Clear()
                        GridView1.ActiveFilterString = "([IsActive] <> False) And (([current_balance] > 0.0) OR ([ccl_zero_balance] <> False))"

                    Catch ex As InvalidPropertyPathException
                        DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

                    Finally
                        GridView1.EndUpdate()
                    End Try
                End If

                ' Make the columns the proper width
                GridView1.BestFitColumns()

            Finally
                RegisterHandlers()
            End Try
        End Sub

        Public Overrides Sub SaveForm(bc As DebtPlus.LINQ.BusinessContext)

            For Each Debt As ClientUpdateDebtRecord In Context.DebtRecords
                Debt.disbursement_factor = Debt.debit_amt
            Next
        End Sub

        ''' <summary>
        ''' Validate the data entry on the grid
        ''' </summary>
        Private Sub GridView1_ValidatingEditor(ByVal sender As Object, ByVal e As BaseContainerValidateEditorEventArgs)

            ' Find the new value for the item
            e.Valid = True
            If e.Value IsNot Nothing AndAlso e.Value IsNot DBNull.Value Then
                Dim NewValue As Decimal = Convert.ToDecimal(e.Value)
                If NewValue < 0D Then
                    e.ErrorText = "Value may not be less than zero"
                    e.Valid = False
                End If
            End If
        End Sub

        ''' <summary>
        ''' Do the debt proration of the amount
        ''' </summary>
        Public Sub ProrateDebts(ByVal Amount As Decimal)

            ' Do the prorate operation
            GridView1.BeginUpdate()
            Try

                ' Clear the amounts to be prorated for all of the debt records
                For Each Debt As ClientUpdateDebtRecord In Context.DebtRecords
                    Debt.debit_amt = 0
                Next

                ' Go through the list and update the non-zero amounts correctly
                For RowHandle As Int32 = 0 To GridView1.RowCount - 1
                    Dim Debt As ClientUpdateDebtRecord = CType(GridView1.GetRow(RowHandle), ClientUpdateDebtRecord)
                    Debt.debit_amt = Debt.display_disbursement_factor
                Next

                ' Prorate the debts
                Using cls As New ProrateDebts(CType(Context.DebtRecords, IProratableList))
                    cls.Prorate(Amount, True)
                End Using

                ' Put the disbursement factor back for the debt
                For RowHandle As Int32 = 0 To GridView1.RowCount - 1
                    Dim Debt As ClientUpdateDebtRecord = CType(GridView1.GetRow(RowHandle), ClientUpdateDebtRecord)
                    Debt.disbursement_factor = Debt.debit_amt
                Next

                GridView1.RefreshData()
                GridView1.UpdateSummary()

            Finally
                GridView1.EndUpdate()
            End Try
        End Sub

        ''' <summary>
        ''' Return the directory to the saved layout information
        ''' </summary>
        Protected Overridable Function XMLBasePath() As String
            If DesignMode OrElse ParentForm Is Nothing Then Return String.Empty
            Return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "DebtPlus", "Client.Update", ParentForm.Name)
        End Function

        ''' <summary>
        ''' When loading the control, restore the layout from the file
        ''' </summary>
        Private Sub Control_Load(ByVal sender As Object, ByVal e As EventArgs)

            UnRegisterHandlers()
            Try
                ' Find the base path to the saved file location
                Dim PathName As String = XMLBasePath()
                If Not String.IsNullOrEmpty(PathName) Then
                    Try
                        Dim FileName As String = Path.Combine(PathName, String.Format("{0}.Grid.xml", Name))
                        If File.Exists(FileName) Then
                            GridView1.RestoreLayoutFromXml(FileName)
                        End If
                    Catch ex As DirectoryNotFoundException
                    Catch ex As FileNotFoundException
                    End Try
                End If

            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' If the layout is changed then update the file
        ''' </summary>
        Private Sub LayoutChanged(ByVal Sender As Object, ByVal e As EventArgs)
            Dim PathName As String = XMLBasePath()
            If Not String.IsNullOrEmpty(PathName) Then
                If Not Directory.Exists(PathName) Then
                    Directory.CreateDirectory(PathName)
                End If

                Dim FileName As String = Path.Combine(PathName, String.Format("{0}.Grid.xml", Name))
                GridView1.SaveLayoutToXml(FileName)
            End If
        End Sub
    End Class
End Namespace
