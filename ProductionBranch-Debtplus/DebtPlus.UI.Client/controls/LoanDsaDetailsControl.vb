﻿#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.ComponentModel
Imports DevExpress.XtraEditors.Controls
Imports System.Linq
Imports System.Collections.Generic
Imports DebtPlus.LINQ
Imports DebtPlus.Utils

Namespace controls

    Friend Class LoanDSADetailsControl

        Private yesNoOptionsList As New List(Of DebtPlus.LINQ.InMemory.YesNo)
        Private noDsaProgramList As New List(Of DebtPlus.LINQ.NoDsaProgramReason)
        Private DsaProgramList As New List(Of DebtPlus.LINQ.AttributeType)
        Private clientsituationlist As New List(Of DebtPlus.LINQ.client_situation)

        Private OriginalData As New DebtPlus.LINQ.Housing_Loan_DSADetail()

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            If Not InDesignMode Then
                RegisterHandlers()
            End If
        End Sub

        ''' <summary>
        ''' Register the event handlers
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub RegisterHandlers()
            AddHandler luClientBelieveSituation.EditValueChanged, AddressOf lookupEdit_EditValueChanged
            AddHandler luDsaHousingProgramBenefit.EditValueChanged, AddressOf lookupEdit_EditValueChanged
            AddHandler luDsaNoProgramReason.EditValueChanged, AddressOf lookupEdit_EditValueChanged
            AddHandler luDsaProgram.EditValueChanged, AddressOf lookupEdit_EditValueChanged
            AddHandler luDsaSpecialist.EditValueChanged, AddressOf lookupEdit_EditValueChanged
            AddHandler luDsaProgram.EditValueChanged, AddressOf luDsaProgram_EditValueChanged
            AddHandler luClientBelieveSituation.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler luDsaHousingProgramBenefit.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler luDsaNoProgramReason.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler luDsaProgram.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler luDsaSpecialist.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler luDsaHousingProgramBenefit.EditValueChanged, AddressOf luDsaHousingProgramBenefit_EditValueChanged
        End Sub

        ''' <summary>
        ''' Remove the event handler registrations
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub UnRegisterHandlers()
            RemoveHandler luClientBelieveSituation.EditValueChanged, AddressOf lookupEdit_EditValueChanged
            RemoveHandler luDsaHousingProgramBenefit.EditValueChanged, AddressOf lookupEdit_EditValueChanged
            RemoveHandler luDsaNoProgramReason.EditValueChanged, AddressOf lookupEdit_EditValueChanged
            RemoveHandler luDsaProgram.EditValueChanged, AddressOf lookupEdit_EditValueChanged
            RemoveHandler luDsaSpecialist.EditValueChanged, AddressOf lookupEdit_EditValueChanged
            RemoveHandler luDsaProgram.EditValueChanged, AddressOf luDsaProgram_EditValueChanged
            RemoveHandler luClientBelieveSituation.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler luDsaHousingProgramBenefit.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler luDsaNoProgramReason.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler luDsaProgram.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler luDsaSpecialist.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler luDsaHousingProgramBenefit.EditValueChanged, AddressOf luDsaHousingProgramBenefit_EditValueChanged
        End Sub

        ''' <summary>
        ''' Client's language to find a default specialist
        ''' </summary>
        <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), DefaultValue(1)> _
        Public Property Language As System.Nullable(Of Int32)

        ''' <summary>
        ''' This is the ID of the record in the database
        ''' </summary>
        <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), DefaultValue(DirectCast(Nothing, System.Object))> _
        Public Property EditValue As System.Nullable(Of Int32)
            Get
                ' Retrieve the current values and either overwrite the current record or create a new one.
                Return SaveRecord()
            End Get

            Set(ByVal value As System.Nullable(Of Int32))
                UnRegisterHandlers()
                Try
                    ' Default the language to US English
                    Language = DebtPlus.LINQ.Cache.AttributeType.getDefaultLanguage()

                    ' Create a list for the items that are "Yes/No". The code values are swapped from the usual meaning so we need to create
                    ' the list here and now.
                    yesNoOptionsList.AddRange(New DebtPlus.LINQ.InMemory.YesNo() _
                                          {
                                              New DebtPlus.LINQ.InMemory.YesNo() With {.default = True, .description = "", .Id = -1},
                                              New DebtPlus.LINQ.InMemory.YesNo() With {.default = False, .description = "Yes", .Id = 0},
                                              New DebtPlus.LINQ.InMemory.YesNo() With {.default = False, .description = "No", .Id = 1}
                                          })

                    ' Insert blank in NoDsa Programs
                    Dim rec As DebtPlus.LINQ.NoDsaProgramReason = DebtPlus.LINQ.Factory.Manufacture_NoDsaProgramReason()
                    rec.ActiveFlag = True
                    rec.default = False
                    rec.description = String.Empty
                    rec.Id = -1
                    noDsaProgramList.Add(rec)
                    noDsaProgramList.AddRange(DebtPlus.LINQ.Cache.NoDsaProgramReason.getList())

                    ' Insert blank in Dsa Program list
                    Dim typ As DebtPlus.LINQ.AttributeType = DebtPlus.LINQ.Factory.Manufacture_AttributeType()
                    typ.ActiveFlag = True
                    typ.Default = False
                    typ.Id = -1
                    typ.Attribute = String.Empty
                    DsaProgramList.Add(typ)
                    DsaProgramList.AddRange(DebtPlus.LINQ.Cache.AttributeType.getDsaPartnerList())

                    ' Insert blank to Clientsituation
                    Dim sit As DebtPlus.LINQ.client_situation = DebtPlus.LINQ.Factory.Manufacture_client_situation()
                    sit.ActiveFlag = True
                    sit.default = False
                    sit.description = String.Empty
                    sit.oID = -1

                    clientsituationlist.Add(sit)
                    clientsituationlist.AddRange(DebtPlus.LINQ.Cache.Clientsituation.getList())

                    luDsaHousingProgramBenefit.Properties.DataSource = yesNoOptionsList
                    luDsaNoProgramReason.Properties.DataSource = noDsaProgramList
                    luDsaProgram.Properties.DataSource = DsaProgramList
                    luClientBelieveSituation.Properties.DataSource = clientsituationlist

                    OriginalData = ReadRecord(value)
                    SetCurrentValues(OriginalData)
                Finally
                    RegisterHandlers()
                End Try
            End Set
        End Property

        ''' <summary>
        ''' Correct the list of counselors for this program and language
        ''' </summary>
        Private Sub UpdateCounselorList(ByVal Language As System.Nullable(Of Int32), ByVal ProgramId As System.Nullable(Of Int32))

            ' Clear the specialist list if there are missing items
            If Language Is Nothing OrElse ProgramId Is Nothing Then
                luDsaSpecialist.Properties.DataSource = Nothing
                luDsaSpecialist.EditValue = Nothing
                Return
            End If

            ' Find the list of possible specialists for the language/program
            Using bc As New BusinessContext()
                Dim colSpecialists As System.Collections.Generic.List(Of DebtPlus.LINQ.xpr_get_dsa_counselorResult) = bc.xpr_get_dsa_counselor(ProgramId.GetValueOrDefault(-1), Language.GetValueOrDefault(-1)).ToList()
                luDsaSpecialist.Properties.DataSource = colSpecialists
                If colSpecialists.Count > 0 Then
                    luDsaSpecialist.EditValue = colSpecialists(0).CounselorID
                Else
                    luDsaSpecialist.EditValue = Nothing
                End If
            End Using
        End Sub

        ''' <summary>
        ''' Load the editing controls from the current record
        ''' </summary>
        Private Sub SetCurrentValues(ByVal values As DebtPlus.LINQ.Housing_Loan_DSADetail)
            luDsaProgram.EditValue = values.Program

            If Not values.Specialist.HasValue() Then
                UpdateCounselorList(Language, values.Program)
            Else
                '' If a DSA is already selected, load the counselor details for display
                Using bc As New BusinessContext()
                    Dim colSpecialists As System.Collections.Generic.List(Of DebtPlus.LINQ.xpr_get_dsa_counselorResult) = Nothing
                    Dim specialistID As Integer = Convert.ToInt32(values.Specialist)
                    Dim counselorName As String = bc.counselors.Where(Function(c) c.Id = specialistID).Select(Function(c) c.RxOfficeCounselorID).FirstOrDefault()

                    colSpecialists = New System.Collections.Generic.List(Of DebtPlus.LINQ.xpr_get_dsa_counselorResult)()
                    Dim counselor As DebtPlus.LINQ.xpr_get_dsa_counselorResult = New DebtPlus.LINQ.xpr_get_dsa_counselorResult()
                    counselor.Counselor = counselorName
                    counselor.CounselorID = specialistID
                    colSpecialists.Add(counselor)

                    luDsaSpecialist.Properties.DataSource = colSpecialists
                    If colSpecialists.Count > 0 Then
                        luDsaSpecialist.EditValue = colSpecialists(0).CounselorID
                    Else
                        luDsaSpecialist.EditValue = Nothing
                    End If
                End Using
            End If
            luDsaHousingProgramBenefit.EditValue = values.ProgramBenefit

            If values.ProgramBenefit Is Nothing Then
                luDsaProgram.Enabled = False
                luDsaNoProgramReason.Enabled = False
            ElseIf values.ProgramBenefit = 0 Then
                luDsaProgram.Enabled = True
                luDsaNoProgramReason.Enabled = False
            Else
                luDsaProgram.Enabled = False
                luDsaNoProgramReason.Enabled = True
            End If

            luClientBelieveSituation.EditValue = values.ClientSituation
            luDsaNoProgramReason.EditValue = values.NoDsaReason
            luDsaSpecialist.EditValue = values.Specialist

            If values.CaseID Is Nothing Then
                txtDsaCaseID.Text = String.Empty
                luDsaHousingProgramBenefit.Properties.ReadOnly = False
                luDsaProgram.Properties.ReadOnly = False
                luDsaNoProgramReason.Properties.ReadOnly = False
                luClientBelieveSituation.Properties.ReadOnly = False
                Return
            End If

            txtDsaCaseID.Text = values.CaseID.ToString()
            luDsaHousingProgramBenefit.Properties.ReadOnly = True
            luDsaProgram.Properties.ReadOnly = True
            luDsaNoProgramReason.Properties.ReadOnly = True
            luClientBelieveSituation.Properties.ReadOnly = True

            ' Finally update the housing benefit information
            ControlProgramHousingBenefit(values.ProgramBenefit)
        End Sub

        ''' <summary>
        ''' Retrieve the desired records.
        ''' </summary>
        Private Function ReadRecord(ByVal RecordID As System.Nullable(Of Int32)) As DebtPlus.LINQ.Housing_Loan_DSADetail
            Dim NewValues As DebtPlus.LINQ.Housing_Loan_DSADetail = Nothing

            If RecordID.GetValueOrDefault(0) > 0 Then
                Using bc As New DebtPlus.LINQ.BusinessContext()
                    Dim id As Int32 = RecordID.Value
                    NewValues = bc.Housing_Loan_DSADetails.Where(Function(b) b.Id = id).FirstOrDefault()
                End Using
            End If

            If NewValues Is Nothing Then
                NewValues = DebtPlus.LINQ.Factory.Manufacture_Housing_Loan_DSADetail()

                ' Do not set the ID value. Leave it as zero to indicate that the record is "new".
            End If

            Return NewValues
        End Function

        ''' <summary>
        ''' Save the current information to the database
        ''' </summary>
        Private Function SaveRecord() As System.Nullable(Of Int32)
            UnRegisterHandlers()
            Dim txn As System.Data.SqlClient.SqlTransaction = Nothing

            Try
                Dim values As DebtPlus.LINQ.Housing_Loan_DSADetail = GetCurrentValues()
                Using cn As New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault.ConnectionString())
                    cn.Open()
                    txn = cn.BeginTransaction(System.Data.IsolationLevel.RepeatableRead)
                    Using bc As New DebtPlus.LINQ.BusinessContext(cn)
                        bc.Transaction = txn

                        ' If the record can be located then update the record.
                        If values.Id > 0 Then
                            Dim q As DebtPlus.LINQ.Housing_Loan_DSADetail = bc.Housing_Loan_DSADetails.Where(Function(s) s.Id = values.Id).FirstOrDefault()
                            If q IsNot Nothing Then

                                ' Compensate the DSA Specialist counter. Do this before we change "q"!
                                AdjustSpecialistCount(bc, q.Specialist, values.Specialist)

                                q.CaseID = values.CaseID
                                q.ClientSituation = values.ClientSituation
                                q.NoDsaReason = values.NoDsaReason
                                q.Program = values.Program
                                q.ProgramBenefit = values.ProgramBenefit
                                q.Specialist = values.Specialist

                                ' Submit the changed dataset to the database
                                bc.SubmitChanges()

                                txn.Commit()
                                txn.Dispose()
                                txn = Nothing

                                ' Update the current copy with the database settings
                                OriginalData = q
                                Return OriginalData.Id
                            End If
                        End If

                        ' If the current record is not empty then we need to insert it into the database
                        ' and return the pointer to the new record.
                        ' If it was empty then we just want NULL back. (This is handled later.)
                        If Not DebtPlus.LINQ.Factory.Manufacture_Housing_Loan_DSADetail().Equals(values) Then

                            ' The previous version is nothing since it is a new record.
                            AdjustSpecialistCount(bc, Nothing, values.Specialist)

                            bc.Housing_Loan_DSADetails.InsertOnSubmit(values)
                            bc.SubmitChanges()

                            txn.Commit()
                            txn.Dispose()
                            txn = Nothing

                            OriginalData = values
                            Return OriginalData.Id
                        End If
                    End Using

                    ' Just complete the transaction at this point. There was nothing done but it is nice to do it
                    ' with a complete rather than the rollback later.
                    txn.Commit()
                    txn.Dispose()
                    txn = Nothing
                End Using

            Finally
                If txn IsNot Nothing Then
                    Try
                        txn.Rollback()
                    Catch ex As Exception
                    End Try
                    txn.Dispose()
                    txn = Nothing
                End If

                RegisterHandlers()
            End Try

            ' Return the value as "null".
            Return New Int32()
        End Function

        ''' <summary>
        ''' Correct the count assigned to a specialist
        ''' </summary>
        Private Sub AdjustSpecialistCount(ByVal bc As DebtPlus.LINQ.BusinessContext, ByVal oldSpecialist As System.Nullable(Of Int32), ByVal newSpecialist As System.Nullable(Of Int32))

            ' Determine the relative order between the two counselors. We need to do the update in the proper sequence
            ' to avoid a deadlock situation. We always do the smaller value first!
            Dim order As Int32 = System.Nullable.Compare(Of Int32)(oldSpecialist, newSpecialist)

            ' Update the entries based upon the order relationships.
            If order < 0 Then
                BumpSpecialist(bc, oldSpecialist, -1)
                BumpSpecialist(bc, newSpecialist, 1)
            ElseIf order > 0 Then
                BumpSpecialist(bc, newSpecialist, 1)
                BumpSpecialist(bc, oldSpecialist, -1)
            End If
        End Sub

        ''' <summary>
        ''' Small routine to ensure that the counselor is updated correctly
        ''' </summary>
        Private Sub BumpSpecialist(ByVal bc As DebtPlus.LINQ.BusinessContext, ByVal specialistID As System.Nullable(Of Int32), ByVal increment As Int32)
            If specialistID IsNot Nothing Then
                Dim co As DebtPlus.LINQ.counselor = bc.counselors.Where(Function(s) s.Id = specialistID.Value).FirstOrDefault()
                If co IsNot Nothing Then
                    co.CaseCounter += increment
                    If co.CaseCounter < 0 Then
                        co.CaseCounter = 0
                    End If
                End If
            End If
        End Sub

        ''' <summary>
        ''' Retrieve the current values as a new record
        ''' </summary>
        Private Function GetCurrentValues() As DebtPlus.LINQ.Housing_Loan_DSADetail

            ' Get a new (blank) record and initialize it with the current default values.
            Dim record As DebtPlus.LINQ.Housing_Loan_DSADetail = DebtPlus.LINQ.Factory.Manufacture_Housing_Loan_DSADetail()

            record.Id = OriginalData.Id ' Here we do want the ID to be the original value.
            record.CaseID = OriginalData.CaseID
            record.ClientSituation = DebtPlus.Utils.Nulls.v_Int32(luClientBelieveSituation.EditValue)
            record.NoDsaReason = DebtPlus.Utils.Nulls.v_Int32(luDsaNoProgramReason.EditValue)
            record.Program = DebtPlus.Utils.Nulls.v_Int32(luDsaProgram.EditValue)
            record.ProgramBenefit = DebtPlus.Utils.Nulls.v_Int32(luDsaHousingProgramBenefit.EditValue)
            record.Specialist = DebtPlus.Utils.Nulls.v_Int32(luDsaSpecialist.EditValue)

            Return record
        End Function

        ''' <summary>
        ''' Check the values for the required information before processing the save operation
        ''' </summary>
        Public Function ValidateItems(ByVal ServicerID As System.Nullable(Of Int32), ByVal InvestorID As System.Nullable(Of Int32)) As String
            Dim record As DebtPlus.LINQ.Housing_Loan_DSADetail = GetCurrentValues()

            ' A case means that the values could not be changed so we are not accountable for errors at this point.
            If record.CaseID IsNot Nothing Then
                Return String.Empty
            End If

            ' Do static testing of the values
            Dim sb As New System.Text.StringBuilder()
            If record.ProgramBenefit.HasValue() Then
                ''sb.AppendFormat("{0}- Housing DSA Program benefit", Environment.NewLine)
                ''Else
                If record.ProgramBenefit = 0 AndAlso record.Program Is Nothing Then
                    sb.AppendFormat("{0}- DSA program", Environment.NewLine)
                End If

                If record.ProgramBenefit = 1 AndAlso record.NoDsaReason Is Nothing Then
                    sb.AppendFormat("{0}- Reason for No DSA Program benefit", Environment.NewLine)
                End If

                ' If there is a pending error then return the error message
                If sb.Length > 0 Then
                    sb.Insert(0, Environment.NewLine)
                    sb.Insert(0, "The following information is required.")
                    Return sb.ToString()
                End If

                ' If there is a no benefit then we are complete
                If record.ProgramBenefit = 1 Then
                    Return String.Empty
                End If

                ' Ask the database to do the validation if there is no benefit
                Using bc As New BusinessContext()
                    ' Use -1 to mean "null" because when the function was written the person did not handle things correctly and did not really want null even
                    ' through it was declare as such. The person really wanted -1 to mean "null". So, do it now.
                    Return bc.validate_dsa_details(record.Program.GetValueOrDefault(-1), ServicerID.GetValueOrDefault(-1), InvestorID.GetValueOrDefault(-1))
                End Using
            Else
                Return String.Empty
            End If

        End Function

        ''' <summary>
        ''' Translate the magic value for blank to be null
        ''' </summary>
        Private Sub lookupEdit_EditValueChanged(sender As Object, e As System.EventArgs)
            UnRegisterHandlers()

            Try
                Dim ctl As DevExpress.XtraEditors.LookUpEdit = TryCast(sender, DevExpress.XtraEditors.LookUpEdit)
                If ctl IsNot Nothing AndAlso ctl.EditValue IsNot Nothing Then
                    Dim v As Int32 = DebtPlus.Utils.Nulls.DInt(ctl.EditValue)
                    If v < 0 Then
                        ctl.EditValue = Nothing
                    End If
                End If

            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Control the list of counselors when the program ID changes
        ''' </summary>
        Private Sub luDsaProgram_EditValueChanged(sender As Object, e As System.EventArgs)
            UnRegisterHandlers()
            Try
                Dim value As System.Nullable(Of Int32) = DebtPlus.Utils.Nulls.v_Int32(luDsaProgram.EditValue)
                If value.GetValueOrDefault() > 0 Then
                    UpdateCounselorList(Language, value)
                    Return
                End If
                UpdateCounselorList(Nothing, Nothing)
            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Control the lists when the program benefit option changes
        ''' </summary>
        Private Sub luDsaHousingProgramBenefit_EditValueChanged(sender As Object, e As System.EventArgs)
            ControlProgramHousingBenefit(DebtPlus.Utils.Nulls.v_Int32(luDsaHousingProgramBenefit.EditValue))
        End Sub

        ''' <summary>
        ''' Enable or disable the lookup controls based upon the program benefit option
        ''' </summary>
        Private Sub ControlProgramHousingBenefit(ByVal value As System.Nullable(Of Int32))

            If value.HasValue Then
                luDsaProgram.Enabled = value.Value = 0
                luDsaNoProgramReason.Enabled = value.Value <> 0
                If luDsaProgram.Enabled = False Then
                    luDsaProgram.EditValue = Nothing
                End If
                If luDsaNoProgramReason.Enabled = False Then
                    luDsaNoProgramReason.EditValue = Nothing
                End If
                Return
            End If

            luDsaNoProgramReason.Enabled = False
            luDsaProgram.Enabled = False
        End Sub
    End Class
End Namespace