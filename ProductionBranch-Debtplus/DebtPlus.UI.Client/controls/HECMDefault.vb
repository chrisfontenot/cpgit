#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Svc.DataSets
Imports DebtPlus.UI.Client.forms.Form_HECMDefault
Imports DebtPlus.UI.Client.controls
Imports DebtPlus.Utils
Imports System.Threading
Imports DebtPlus.Data.Forms
Imports DebtPlus.Events
Imports System.Data.SqlClient
Imports DebtPlus.LINQ
Imports System.Linq

Namespace controls
    Friend Class HECMDefault
        Inherits MyGridControlClient

        ''' <summary>
        ''' Initialize a new class instance
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            EnableMenus = True
        End Sub

        ' Collection of records shown in the grid control
        Private colRecords As System.Collections.Generic.List(Of DebtPlus.LINQ.hecm_default) = Nothing
        Private clientHousingRecord As client_housing = Nothing

        ''' <summary>
        ''' Read the grid control records
        ''' </summary>
        ''' <remarks></remarks>
        Public Shadows Sub ReadForm(record As client_housing)
            clientHousingRecord = record
            If colRecords Is Nothing Then
                Using bc As New BusinessContext()
                    colRecords = bc.hecm_defaults.Where(Function(s) s.client = clientHousingRecord.Id).ToList()
                End Using
                GridControl1.DataSource = colRecords
            End If
        End Sub

        ''' <summary>
        ''' Create a new record
        ''' </summary>
        ''' <remarks></remarks>
        Protected Overrides Sub OnCreateItem()
            Dim record As DebtPlus.LINQ.hecm_default = DebtPlus.LINQ.Factory.Manufacture_hecm_default(clientHousingRecord.Id)
            EditHecmRecord(record)
        End Sub

        ''' <summary>
        ''' Delete the desired record
        ''' </summary>
        ''' <param name="obj"></param>
        ''' <remarks></remarks>
        Protected Overrides Sub OnDeleteItem(obj As Object)
            Dim record As hecm_default = TryCast(obj, hecm_default)
            If record Is Nothing Then
                Return
            End If

            If DebtPlus.Data.Prompts.RequestConfirmation_Delete() <> Windows.Forms.DialogResult.Yes Then
                Return
            End If

            ' Delete the record
            Using bc As New BusinessContext()
                Dim q As hecm_default = bc.hecm_defaults.Where(Function(s) s.Id = record.Id).FirstOrDefault()
                If q IsNot Nothing Then
                    bc.hecm_defaults.DeleteOnSubmit(q)
                    bc.SubmitChanges()

                    ' Remove the record from the grid control
                    colRecords.Remove(record)
                    GridControl1.RefreshDataSource()
                End If
            End Using
        End Sub

        ''' <summary>
        ''' Edit the record desired
        ''' </summary>
        ''' <param name="obj"></param>
        ''' <remarks></remarks>
        Protected Overrides Sub OnEditItem(ByVal obj As Object)
            Dim record As hecm_default = TryCast(obj, hecm_default)
            If record Is Nothing Then
                Return
            End If

            EditHecmRecord(record)
        End Sub

        ''' <summary>
        ''' Do the edit operation on the record
        ''' </summary>
        ''' <param name="record"></param>
        ''' <remarks></remarks>
        Private Sub EditHecmRecord(ByVal record As hecm_default)
            Dim thrd As New Thread(New ParameterizedThreadStart(AddressOf EditThread))
            thrd.Name = "HECM"
            thrd.SetApartmentState(ApartmentState.STA)
            thrd.IsBackground = True
            thrd.Start(record)
        End Sub

        ''' <summary>
        ''' Edit the record as a separate thread
        ''' </summary>
        ''' <param name="obj"></param>
        ''' <remarks></remarks>
        Private Sub EditThread(ByVal obj As Object)

            ' Find the record to be updated
            Dim record As hecm_default = TryCast(obj, hecm_default)
            If record Is Nothing Then
                Return
            End If

            ' Edit the record
            Using frm As New forms.Form_HECMDefault(record)
                If frm.ShowDialog() = DialogResult.OK Then

                    Try
                        Dim q As hecm_default = Nothing
                        Using bc As New BusinessContext()
                            If record.Id > 0 Then
                                q = bc.hecm_defaults.Where(Function(s) s.Id = record.Id).FirstOrDefault()
                            End If

                            If q IsNot Nothing Then
                                q.bdt_bec = record.bdt_bec
                                q.checkup = record.checkup
                                q.file_status = record.file_status
                                q.file_status_date = record.file_status_date
                                q.Hardest_Hit_Ref_Date = record.Hardest_Hit_Ref_Date
                                q.ref_car_ins = record.ref_car_ins
                                q.ref_food = record.ref_food
                                q.ref_home_repair = record.ref_home_repair
                                q.ref_income = record.ref_income
                                q.ref_medical = record.ref_medical
                                q.ref_prescription = record.ref_prescription
                                q.ref_property_taxes = record.ref_property_taxes
                                q.ref_social_services = record.ref_social_services
                                q.ref_ssi = record.ref_ssi
                                q.ref_utility = record.ref_utility
                                q.referral_option = record.referral_option
                                q.save_car_ins = record.save_car_ins
                                q.save_food = record.save_food
                                q.save_home_repair = record.save_home_repair
                                q.save_income = record.save_income
                                q.save_medical = record.save_medical
                                q.save_prescription = record.save_prescription
                                q.save_property_taxes = record.save_property_taxes
                                q.save_social_services = record.save_social_services
                                q.save_ssi = record.save_ssi
                                q.save_utility = record.save_utility
                                q.tier = record.tier
                                bc.SubmitChanges()

                            Else
                                bc.hecm_defaults.InsertOnSubmit(record)
                                bc.SubmitChanges()

                                ' Add the record to the list of items in the grid
                                UpdateGridControl(record)
                            End If
                        End Using

                    Catch ex As System.Data.SqlClient.SqlException
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error processing the HECM record update")
                    End Try
                End If
            End Using
        End Sub

        Private Delegate Sub UpdateGridControlDelegate(record As hecm_default)

        Private Sub UpdateGridControl(record As hecm_default)
            If GridControl1.InvokeRequired Then
                Dim ia As System.IAsyncResult = GridControl1.BeginInvoke(New UpdateGridControlDelegate(AddressOf UpdateGridControl), New Object() {record})
                GridControl1.EndInvoke(ia)
            Else
                colRecords.Add(record)
                GridView1.RefreshData()
                ' GridControl1.RefreshDataSource()
            End If
        End Sub
    End Class
End Namespace
