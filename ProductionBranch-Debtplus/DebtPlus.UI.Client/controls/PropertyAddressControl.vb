﻿Namespace controls
    Friend Class PropertyAddressControl

        Public Event AddressChanged As DebtPlus.Events.AddressChangedEventHandler

        ''' <summary>
        ''' Create an instance of our class
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Load, AddressOf PropertyAddressControl_Load
            AddHandler AddressRecordControl_AddressID.AddressChanged, AddressOf AddressRecordControl_AddressID_AddressChanged
            AddHandler AddressRecordControl_PreviousAddressID.AddressChanged, AddressOf AddressRecordControl_AddressID_AddressChanged
            AddHandler CheckEdit_UseHomeAddress.CheckStateChanged, AddressOf CheckEdit_UseHomeAddress_CheckStateChanged
            AddHandler CheckEdit_ViewPrevious.CheckStateChanged, AddressOf CheckEdit_ViewPrevious_CheckStateChanged
        End Sub

        Public Event AddressEnabled(ByVal Sendere As Object, ByVal isEnabled As Boolean)

        ''' <summary>
        ''' Handle the check event for the two address control radio buttons
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub CheckEdit_ViewPrevious_CheckStateChanged(sender As Object, e As EventArgs)
            ControlVisibility()
        End Sub

        ''' <summary>
        ''' Handle the LOAD event on the control
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub PropertyAddressControl_Load(sender As Object, e As EventArgs)

            ' Ensure that only one address record is shown
            ControlVisibility()
        End Sub

        ''' <summary>
        ''' Correct the visibility of the two address controls. We want only one shown at one time.
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub ControlVisibility()
            If CheckEdit_ViewCurrent.Checked Then
                AddressRecordControl_AddressID.BringToFront()
                AddressRecordControl_AddressID.Visible = True
                AddressRecordControl_PreviousAddressID.Visible = False
                CheckEdit_UseHomeAddress.Enabled = True
            Else
                AddressRecordControl_PreviousAddressID.BringToFront()
                AddressRecordControl_AddressID.Visible = False
                AddressRecordControl_PreviousAddressID.Visible = True
                CheckEdit_UseHomeAddress.Enabled = False
            End If
        End Sub

        Private Sub CheckEdit_UseHomeAddress_CheckStateChanged(sender As Object, e As EventArgs)
            RaiseEvent AddressEnabled(Me, Not CheckEdit_UseHomeAddress.Checked)
            AddressRecordControl_AddressID.Enabled = Not CheckEdit_UseHomeAddress.Checked
        End Sub

        Private Sub AddressRecordControl_AddressID_AddressChanged(sender As Object, e As Events.AddressChangedEventArgs)
            RaiseEvent AddressChanged(Me, e)
        End Sub
    End Class
End Namespace