#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.Events
Imports DebtPlus.UI.Client.Service
Imports DebtPlus.UI.Client.forms.Housing
Imports DebtPlus.LINQ
Imports DevExpress.Utils
Imports System.Globalization
Imports System.Linq

Namespace controls
    Friend Class LoanListControl
        Private bc As BusinessContext = Nothing
        Private propertyRecord As Housing_property = Nothing
        Private colLoanRecords As System.Collections.Generic.List(Of Housing_loan) = Nothing

        ''' <summary>
        ''' Initialize the storage for the control class
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            EnableMenus = True

            AddHandler GridView1.CustomColumnDisplayText, AddressOf GridView1_CustomColumnDisplayText
        End Sub

        ''' <summary>
        ''' Translate the specific columns in the grid to a text string
        ''' </summary>
        Private Sub GridView1_CustomColumnDisplayText(sender As Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs)

            ' We need a valid row index to find the record.
            If e.ListSourceRowIndex < 0 OrElse e.ListSourceRowIndex >= colLoanRecords.Count Then
                e.DisplayText = String.Empty
                Return
            End If

            ' Find the housing loan record that is desired
            Dim item As Housing_loan = colLoanRecords(e.ListSourceRowIndex)

            ' Handle the loan delinquency months
            If e.Column Is GridColumn_LoanDelinquencyMonths Then
                e.DisplayText = LoanDetailsControl.FormatDelinquency(item.LoanDelinquencyMonths)
                Return
            End If

            ' Handle the loan position
            If e.Column Is GridColumn_Loan1st2nd Then
                Dim position As System.Nullable(Of Int32) = DebtPlus.Utils.Nulls.v_Int32(item.Loan1st2nd)
                If position.HasValue Then
                    Dim q As Housing_LoanPositionType = DebtPlus.LINQ.Cache.Housing_LoanPositionType.getList().Find(Function(s) s.Id = position.Value)
                    If q IsNot Nothing Then
                        e.DisplayText = q.description
                        Return
                    End If
                End If

                e.DisplayText = String.Empty
                Return
            End If

            ' Find the detail information for the next set of columns
            If e.Column Is GridColumn_MortgageTypeCD OrElse e.Column Is GridColumn_FinanceTypeCD Then
                Dim detailID As System.Nullable(Of Int32) = item.IntakeDetailID
                If Not detailID.HasValue Then
                    e.DisplayText = String.Empty
                    Return
                End If

                ' Find the detail record. If none then we can't translate.
                Dim detailRecord As Housing_loan_detail = bc.Housing_loan_details.Where(Function(s) s.Id = detailID.Value).FirstOrDefault()
                If detailRecord Is Nothing Then
                    e.DisplayText = String.Empty
                    Return
                End If

                ' Translate the mortgage type
                If e.Column Is GridColumn_MortgageTypeCD Then
                    If detailRecord.MortgageTypeCD.HasValue Then
                        Dim q As DebtPlus.LINQ.Housing_MortgageType = DebtPlus.LINQ.Cache.Housing_MortgageType.getList().Find(Function(s) s.Id = detailRecord.MortgageTypeCD.Value)
                        If q IsNot Nothing Then
                            e.DisplayText = q.description
                            Return
                        End If
                    End If
                End If

                ' Translate the financing type
                If e.Column Is GridColumn_FinanceTypeCD Then
                    If detailRecord.FinanceTypeCD.HasValue Then
                        Dim q As DebtPlus.LINQ.Housing_FinancingType = DebtPlus.LINQ.Cache.Housing_FinancingType.getList().Find(Function(s) s.Id = detailRecord.FinanceTypeCD.Value)
                        If q IsNot Nothing Then
                            e.DisplayText = q.description
                            Return
                        End If
                    End If
                End If

                e.DisplayText = String.Empty
            End If
        End Sub

        ''' <summary>
        ''' Handle the EDIT function on a row
        ''' </summary>
        Protected Overrides Sub OnEditItem(obj As Object)

            ' A loan record needs to be defined before it may be edited
            Dim loanRecord As Housing_loan = TryCast(obj, Housing_loan)
            If loanRecord Is Nothing Then
                Return
            End If

            Dim previousUse As Boolean = loanRecord.UseInReports.GetValueOrDefault(False)
            Using frm As New LoanInformationForm(Context, loanRecord)
                If frm.ShowDialog() <> DialogResult.OK Then
                    Return
                End If
            End Using

            ' Turn off the UseInReports for all items in the database that are for this property
            If (Not previousUse) AndAlso loanRecord.UseInReports.GetValueOrDefault(False) Then
                colLoanRecords.ForEach(Sub(s) s.UseInReports = False)
                loanRecord.UseInReports = True
            End If

            Try
                bc.SubmitChanges()
                GridView1.RefreshData()

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
            End Try
        End Sub

        ''' <summary>
        ''' Create a new row
        ''' </summary>
        Protected Overrides Sub OnCreateItem()

            Dim loanRecord As Housing_loan = DebtPlus.LINQ.Factory.Manufacture_Housing_loan(propertyRecord.Id)

            ' Override the "UseInReports" to set it if there are no loans
            If colLoanRecords.Count() = 0 Then
                loanRecord.UseInReports = True
            Else ' set the status if there are no other items marked "use in reports"
                Dim q As Housing_loan = colLoanRecords.Find(Function(s) s.UseInReports.GetValueOrDefault(False))
                loanRecord.UseInReports = (q Is Nothing)
            End If

            Using frm As New LoanInformationForm(Context, loanRecord)
                If frm.ShowDialog() <> DialogResult.OK Then
                    Return
                End If
            End Using

            ' Turn off the UseInReports for all items in the database that are for this property
            If loanRecord.UseInReports.GetValueOrDefault(False) Then
                colLoanRecords.ForEach(Sub(s) s.UseInReports = False)
                loanRecord.UseInReports = True
            End If

            bc.Housing_loans.InsertOnSubmit(loanRecord)
            bc.SubmitChanges()

            ' Add the loan to the loan list once it is fully complete
            colLoanRecords.Add(loanRecord)
            GridView1.RefreshData()
        End Sub

        Protected Overrides Sub OnDeleteItem(obj As Object)

            ' A loan record needs to be defined before it may be edited
            Dim loanRecord As Housing_loan = TryCast(obj, Housing_loan)
            If loanRecord Is Nothing Then
                Return
            End If

            If DebtPlus.Data.Prompts.RequestConfirmation_Delete() <> DialogResult.Yes Then
                Return
            End If

            ' Update the loan information in the database
            Try
                bc.Housing_loans.DeleteOnSubmit(loanRecord)
                bc.SubmitChanges()

                colLoanRecords.Remove(loanRecord)
                GridView1.RefreshData()

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
            End Try
        End Sub

        ''' <summary>
        ''' Read the loans table
        ''' </summary>
        Public Shadows Sub ReadForm(bc As BusinessContext, ByVal propertyRecord As Housing_property)
            Me.propertyRecord = propertyRecord
            Me.bc = bc

            ' Retrieve the list of loan items
            colLoanRecords = bc.Housing_loans.Where(Function(s) s.PropertyID = propertyRecord.Id).ToList()
            GridControl1.DataSource = colLoanRecords
            GridView1.BestFitColumns()
            GridControl1.RefreshDataSource()
        End Sub
    End Class
End Namespace
