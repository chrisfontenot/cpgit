Namespace controls
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class BorrowerControl
        Inherits ControlBaseClient

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
            Dim SerializableAppearanceObject5 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
            Dim SerializableAppearanceObject2 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.PopupContainerControl_EverFiledBK = New DevExpress.XtraEditors.PopupContainerControl()
            Me.PopupContainerEdit_EverFiledBK = New DevExpress.XtraEditors.PopupContainerEdit()
            Me.PopupContainerControl1 = New DevExpress.XtraEditors.PopupContainerControl()
            Me.milDependent = New DevExpress.XtraEditors.LookUpEdit()
            Me.milGrade = New DevExpress.XtraEditors.LookUpEdit()
            Me.milService = New DevExpress.XtraEditors.LookUpEdit()
            Me.milStatus = New DevExpress.XtraEditors.LookUpEdit()
            Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
            Me.ComboBoxEdit_Person = New DevExpress.XtraEditors.ComboBoxEdit()
            Me.DateEdit_emp_end_date = New DevExpress.XtraEditors.DateEdit()
            Me.DateEdit_emp_start_date = New DevExpress.XtraEditors.DateEdit()
            Me.TextEdit_Occupation = New DevExpress.XtraEditors.TextEdit()
            Me.LookUpEdit_PreferredLanguageCD = New DevExpress.XtraEditors.LookUpEdit()
            Me.LookUpEdit_MaritalStatusCd = New DevExpress.XtraEditors.LookUpEdit()
            Me.LookUpEdit_Sync = New DevExpress.XtraEditors.LookUpEdit()
            Me.EmailRecordControl_EmailID = New DebtPlus.Data.Controls.EmailRecordControl()
            Me.NameRecordControl_NameID = New DebtPlus.Data.Controls.NameRecordControl()
            Me.LookUpEdit_IntakeCreditBureauCD = New DevExpress.XtraEditors.LookUpEdit()
            Me.LookUpEdit_GenderCD = New DevExpress.XtraEditors.LookUpEdit()
            Me.LookUpEdit_EducLevelCompletedCD = New DevExpress.XtraEditors.LookUpEdit()
            Me.DateEdit_DOB = New DevExpress.XtraEditors.DateEdit()
            Me.Ssn1 = New DebtPlus.Data.Controls.ssn()
            Me.LookUpEdit_RaceCD = New DevExpress.XtraEditors.LookUpEdit()
            Me.TextEdit_MotherMaidenLName = New DevExpress.XtraEditors.TextEdit()
            Me.CheckEdit_DisabledIND = New DevExpress.XtraEditors.CheckEdit()
            Me.SpinEdit_CreditScore = New DevExpress.XtraEditors.SpinEdit()
            Me.PopupContainerEdit_Military = New DevExpress.XtraEditors.PopupContainerEdit()
            Me.LookUpEdit_Ethnicity = New DevExpress.XtraEditors.LookUpEdit()
            Me.LayoutControlItem_Sync = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem_MotherMaidenLName = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_DisabledIND = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_NameID = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_DOB = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_IntakeCreditBureauCD = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_Occupation = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_EmailID = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_MaritalStatusCd = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_RaceCD = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_GenderCD = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_EducLevelCompletedCD = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_PreferredLanguageCD = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_IntakeCreditScore = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_SSN = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_MilitaryServiceCD = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_EthnicityIND = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
            Me.dtAuthLetter = New DevExpress.XtraEditors.DateEdit()
            Me.luPmtCurrent = New DevExpress.XtraEditors.LookUpEdit()
            Me.dtBkDischargedDate = New DevExpress.XtraEditors.DateEdit()
            Me.luEverFiledBKChapter = New DevExpress.XtraEditors.LookUpEdit()
            Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
            Me.dtEverFiledBKFiledDate = New DevExpress.XtraEditors.DateEdit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.PopupContainerControl_EverFiledBK, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.PopupContainerControl_EverFiledBK.SuspendLayout()
            CType(Me.PopupContainerEdit_EverFiledBK.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PopupContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.PopupContainerControl1.SuspendLayout()
            CType(Me.milDependent.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.milGrade.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.milService.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.milStatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Person.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit_emp_end_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit_emp_end_date.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit_emp_start_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit_emp_start_date.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_Occupation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_PreferredLanguageCD.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_MaritalStatusCd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_Sync.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmailRecordControl_EmailID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_IntakeCreditBureauCD.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_GenderCD.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_EducLevelCompletedCD.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit_DOB.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit_DOB.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Ssn1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_RaceCD.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_MotherMaidenLName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_DisabledIND.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.SpinEdit_CreditScore.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PopupContainerEdit_Military.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_Ethnicity.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_Sync, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_MotherMaidenLName, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_DisabledIND, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_NameID, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_DOB, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_IntakeCreditBureauCD, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_Occupation, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_EmailID, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_MaritalStatusCd, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_RaceCD, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_GenderCD, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_EducLevelCompletedCD, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_PreferredLanguageCD, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_IntakeCreditScore, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_SSN, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_MilitaryServiceCD, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_EthnicityIND, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dtAuthLetter.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dtAuthLetter.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.luPmtCurrent.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dtBkDischargedDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dtBkDischargedDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.luEverFiledBKChapter.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dtEverFiledBKFiledDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dtEverFiledBKFiledDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.PopupContainerControl_EverFiledBK)
            Me.LayoutControl1.Controls.Add(Me.PopupContainerEdit_EverFiledBK)
            Me.LayoutControl1.Controls.Add(Me.PopupContainerControl1)
            Me.LayoutControl1.Controls.Add(Me.ComboBoxEdit_Person)
            Me.LayoutControl1.Controls.Add(Me.DateEdit_emp_end_date)
            Me.LayoutControl1.Controls.Add(Me.DateEdit_emp_start_date)
            Me.LayoutControl1.Controls.Add(Me.TextEdit_Occupation)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_PreferredLanguageCD)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_MaritalStatusCd)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_Sync)
            Me.LayoutControl1.Controls.Add(Me.EmailRecordControl_EmailID)
            Me.LayoutControl1.Controls.Add(Me.NameRecordControl_NameID)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_IntakeCreditBureauCD)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_GenderCD)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_EducLevelCompletedCD)
            Me.LayoutControl1.Controls.Add(Me.DateEdit_DOB)
            Me.LayoutControl1.Controls.Add(Me.Ssn1)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_RaceCD)
            Me.LayoutControl1.Controls.Add(Me.TextEdit_MotherMaidenLName)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_DisabledIND)
            Me.LayoutControl1.Controls.Add(Me.SpinEdit_CreditScore)
            Me.LayoutControl1.Controls.Add(Me.PopupContainerEdit_Military)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_Ethnicity)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.HiddenItems.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem_Sync})
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(514, 74, 250, 350)
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(472, 301)
            Me.LayoutControl1.TabIndex = 0
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'PopupContainerControl_EverFiledBK
            '
            Me.PopupContainerControl_EverFiledBK.Controls.Add(Me.LabelControl9)
            Me.PopupContainerControl_EverFiledBK.Controls.Add(Me.LabelControl8)
            Me.PopupContainerControl_EverFiledBK.Controls.Add(Me.dtAuthLetter)
            Me.PopupContainerControl_EverFiledBK.Controls.Add(Me.luPmtCurrent)
            Me.PopupContainerControl_EverFiledBK.Controls.Add(Me.dtBkDischargedDate)
            Me.PopupContainerControl_EverFiledBK.Controls.Add(Me.luEverFiledBKChapter)
            Me.PopupContainerControl_EverFiledBK.Controls.Add(Me.LabelControl7)
            Me.PopupContainerControl_EverFiledBK.Controls.Add(Me.LabelControl6)
            Me.PopupContainerControl_EverFiledBK.Controls.Add(Me.LabelControl5)
            Me.PopupContainerControl_EverFiledBK.Controls.Add(Me.dtEverFiledBKFiledDate)
            Me.PopupContainerControl_EverFiledBK.Location = New System.Drawing.Point(254, 270)
            Me.PopupContainerControl_EverFiledBK.Name = "PopupContainerControl_EverFiledBK"
            Me.PopupContainerControl_EverFiledBK.Size = New System.Drawing.Size(200, 133)
            Me.PopupContainerControl_EverFiledBK.TabIndex = 25
            '
            'PopupContainerEdit_EverFiledBK
            '
            Me.PopupContainerEdit_EverFiledBK.Location = New System.Drawing.Point(346, 54)
            Me.PopupContainerEdit_EverFiledBK.Name = "PopupContainerEdit_EverFiledBK"
            Me.PopupContainerEdit_EverFiledBK.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.PopupContainerEdit_EverFiledBK.Properties.PopupControl = Me.PopupContainerControl_EverFiledBK
            Me.PopupContainerEdit_EverFiledBK.Properties.ShowPopupCloseButton = False
            Me.PopupContainerEdit_EverFiledBK.Size = New System.Drawing.Size(121, 20)
            Me.PopupContainerEdit_EverFiledBK.StyleController = Me.LayoutControl1
            Me.PopupContainerEdit_EverFiledBK.TabIndex = 24
            '
            'PopupContainerControl1
            '
            Me.PopupContainerControl1.Controls.Add(Me.milDependent)
            Me.PopupContainerControl1.Controls.Add(Me.milGrade)
            Me.PopupContainerControl1.Controls.Add(Me.milService)
            Me.PopupContainerControl1.Controls.Add(Me.milStatus)
            Me.PopupContainerControl1.Controls.Add(Me.LabelControl4)
            Me.PopupContainerControl1.Controls.Add(Me.LabelControl3)
            Me.PopupContainerControl1.Controls.Add(Me.LabelControl2)
            Me.PopupContainerControl1.Controls.Add(Me.LabelControl1)
            Me.PopupContainerControl1.Location = New System.Drawing.Point(6, 287)
            Me.PopupContainerControl1.Name = "PopupContainerControl1"
            Me.PopupContainerControl1.Size = New System.Drawing.Size(224, 112)
            Me.PopupContainerControl1.TabIndex = 23
            '
            'milDependent
            '
            Me.milDependent.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.milDependent.Location = New System.Drawing.Point(62, 81)
            Me.milDependent.Name = "milDependent"
            Me.milDependent.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.milDependent.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.milDependent.Properties.DisplayMember = "description"
            Me.milDependent.Properties.NullText = ""
            Me.milDependent.Properties.ShowFooter = False
            Me.milDependent.Properties.ShowHeader = False
            Me.milDependent.Properties.SortColumnIndex = 1
            Me.milDependent.Properties.ValueMember = "Id"
            Me.milDependent.Size = New System.Drawing.Size(159, 20)
            Me.milDependent.TabIndex = 15
            '
            'milGrade
            '
            Me.milGrade.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.milGrade.Location = New System.Drawing.Point(62, 55)
            Me.milGrade.Name = "milGrade"
            Me.milGrade.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.milGrade.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.milGrade.Properties.DisplayMember = "description"
            Me.milGrade.Properties.NullText = ""
            Me.milGrade.Properties.ShowFooter = False
            Me.milGrade.Properties.ShowHeader = False
            Me.milGrade.Properties.SortColumnIndex = 1
            Me.milGrade.Properties.ValueMember = "Id"
            Me.milGrade.Size = New System.Drawing.Size(159, 20)
            Me.milGrade.TabIndex = 13
            '
            'milService
            '
            Me.milService.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.milService.Location = New System.Drawing.Point(62, 29)
            Me.milService.Name = "milService"
            Me.milService.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.milService.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.milService.Properties.DisplayMember = "description"
            Me.milService.Properties.NullText = ""
            Me.milService.Properties.ShowFooter = False
            Me.milService.Properties.ShowHeader = False
            Me.milService.Properties.SortColumnIndex = 1
            Me.milService.Properties.ValueMember = "Id"
            Me.milService.Size = New System.Drawing.Size(159, 20)
            Me.milService.TabIndex = 11
            '
            'milStatus
            '
            Me.milStatus.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.milStatus.Location = New System.Drawing.Point(62, 3)
            Me.milStatus.Name = "milStatus"
            Me.milStatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.milStatus.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.milStatus.Properties.DisplayMember = "description"
            Me.milStatus.Properties.NullText = ""
            Me.milStatus.Properties.ShowFooter = False
            Me.milStatus.Properties.ShowHeader = False
            Me.milStatus.Properties.SortColumnIndex = 1
            Me.milStatus.Properties.ValueMember = "Id"
            Me.milStatus.Size = New System.Drawing.Size(159, 20)
            Me.milStatus.TabIndex = 9
            '
            'LabelControl4
            '
            Me.LabelControl4.Location = New System.Drawing.Point(4, 84)
            Me.LabelControl4.Name = "LabelControl4"
            Me.LabelControl4.Size = New System.Drawing.Size(53, 13)
            Me.LabelControl4.TabIndex = 14
            Me.LabelControl4.Text = "Dependent"
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(4, 58)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(24, 13)
            Me.LabelControl3.TabIndex = 12
            Me.LabelControl3.Text = "Rank"
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(4, 32)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(33, 13)
            Me.LabelControl2.TabIndex = 10
            Me.LabelControl2.Text = "Branch"
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(4, 6)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(31, 13)
            Me.LabelControl1.TabIndex = 8
            Me.LabelControl1.Text = "Status"
            '
            'ComboBoxEdit_Person
            '
            Me.ComboBoxEdit_Person.Location = New System.Drawing.Point(104, 5)
            Me.ComboBoxEdit_Person.Name = "ComboBoxEdit_Person"
            Me.ComboBoxEdit_Person.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.ComboBoxEdit_Person.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Person.Properties.Items.AddRange(New Object() {"Other", "Applicant", "Co-Applicant"})
            Me.ComboBoxEdit_Person.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
            Me.ComboBoxEdit_Person.Size = New System.Drawing.Size(363, 20)
            Me.ComboBoxEdit_Person.StyleController = Me.LayoutControl1
            Me.ComboBoxEdit_Person.TabIndex = 22
            '
            'DateEdit_emp_end_date
            '
            Me.DateEdit_emp_end_date.EditValue = Nothing
            Me.DateEdit_emp_end_date.Location = New System.Drawing.Point(346, 222)
            Me.DateEdit_emp_end_date.Name = "DateEdit_emp_end_date"
            Me.DateEdit_emp_end_date.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.DateEdit_emp_end_date.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.DateEdit_emp_end_date.Properties.DisplayFormat.FormatString = "MMM yyyy"
            Me.DateEdit_emp_end_date.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.DateEdit_emp_end_date.Properties.NullValuePrompt = "Still Employed"
            Me.DateEdit_emp_end_date.Properties.NullValuePromptShowForEmptyValue = True
            Me.DateEdit_emp_end_date.Properties.ValidateOnEnterKey = True
            Me.DateEdit_emp_end_date.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.[False]
            Me.DateEdit_emp_end_date.Properties.VistaEditTime = DevExpress.Utils.DefaultBoolean.[False]
            Me.DateEdit_emp_end_date.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.DateEdit_emp_end_date.Size = New System.Drawing.Size(121, 20)
            Me.DateEdit_emp_end_date.StyleController = Me.LayoutControl1
            Me.DateEdit_emp_end_date.TabIndex = 16
            Me.DateEdit_emp_end_date.ToolTip = "If the person no longer works at this job, what was the date that the employment " & _
        "was terminated?"
            '
            'DateEdit_emp_start_date
            '
            Me.DateEdit_emp_start_date.EditValue = Nothing
            Me.DateEdit_emp_start_date.Location = New System.Drawing.Point(346, 198)
            Me.DateEdit_emp_start_date.Name = "DateEdit_emp_start_date"
            Me.DateEdit_emp_start_date.Properties.ActionButtonIndex = 1
            Me.DateEdit_emp_start_date.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.DateEdit_emp_start_date.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Up, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1, "Click here to enter number of years, months of employment to date", "Edit", Nothing, True), New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject5, "", "DropDown", Nothing, True)})
            Me.DateEdit_emp_start_date.Properties.DisplayFormat.FormatString = "MMM yyyy"
            Me.DateEdit_emp_start_date.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.DateEdit_emp_start_date.Properties.NullValuePrompt = "Should be given"
            Me.DateEdit_emp_start_date.Properties.NullValuePromptShowForEmptyValue = True
            Me.DateEdit_emp_start_date.Properties.ValidateOnEnterKey = True
            Me.DateEdit_emp_start_date.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.[False]
            Me.DateEdit_emp_start_date.Properties.VistaEditTime = DevExpress.Utils.DefaultBoolean.[False]
            Me.DateEdit_emp_start_date.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.DateEdit_emp_start_date.Size = New System.Drawing.Size(121, 20)
            Me.DateEdit_emp_start_date.StyleController = Me.LayoutControl1
            Me.DateEdit_emp_start_date.TabIndex = 17
            Me.DateEdit_emp_start_date.ToolTip = "Starting date for the current employer. We need to determine how long the person " & _
        "has been ""on the job"" for HUD reports and others."
            '
            'TextEdit_Occupation
            '
            Me.TextEdit_Occupation.Location = New System.Drawing.Point(104, 78)
            Me.TextEdit_Occupation.Name = "TextEdit_Occupation"
            Me.TextEdit_Occupation.Properties.MaxLength = 50
            Me.TextEdit_Occupation.Size = New System.Drawing.Size(139, 20)
            Me.TextEdit_Occupation.StyleController = Me.LayoutControl1
            Me.TextEdit_Occupation.TabIndex = 20
            '
            'LookUpEdit_PreferredLanguageCD
            '
            Me.LookUpEdit_PreferredLanguageCD.Location = New System.Drawing.Point(104, 150)
            Me.LookUpEdit_PreferredLanguageCD.Name = "LookUpEdit_PreferredLanguageCD"
            Me.LookUpEdit_PreferredLanguageCD.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_PreferredLanguageCD.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Attribute", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_PreferredLanguageCD.Properties.DisplayMember = "Attribute"
            Me.LookUpEdit_PreferredLanguageCD.Properties.DropDownRows = 6
            Me.LookUpEdit_PreferredLanguageCD.Properties.NullText = ""
            Me.LookUpEdit_PreferredLanguageCD.Properties.ShowFooter = False
            Me.LookUpEdit_PreferredLanguageCD.Properties.ShowHeader = False
            Me.LookUpEdit_PreferredLanguageCD.Properties.SortColumnIndex = 1
            Me.LookUpEdit_PreferredLanguageCD.Properties.ValueMember = "Id"
            Me.LookUpEdit_PreferredLanguageCD.Size = New System.Drawing.Size(139, 20)
            Me.LookUpEdit_PreferredLanguageCD.StyleController = Me.LayoutControl1
            Me.LookUpEdit_PreferredLanguageCD.TabIndex = 19
            '
            'LookUpEdit_MaritalStatusCd
            '
            Me.LookUpEdit_MaritalStatusCd.Location = New System.Drawing.Point(346, 102)
            Me.LookUpEdit_MaritalStatusCd.Name = "LookUpEdit_MaritalStatusCd"
            Me.LookUpEdit_MaritalStatusCd.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_MaritalStatusCd.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_MaritalStatusCd.Properties.DisplayMember = "description"
            Me.LookUpEdit_MaritalStatusCd.Properties.DropDownRows = 5
            Me.LookUpEdit_MaritalStatusCd.Properties.NullText = ""
            Me.LookUpEdit_MaritalStatusCd.Properties.ShowFooter = False
            Me.LookUpEdit_MaritalStatusCd.Properties.ShowHeader = False
            Me.LookUpEdit_MaritalStatusCd.Properties.SortColumnIndex = 1
            Me.LookUpEdit_MaritalStatusCd.Properties.ValueMember = "Id"
            Me.LookUpEdit_MaritalStatusCd.Size = New System.Drawing.Size(121, 20)
            Me.LookUpEdit_MaritalStatusCd.StyleController = Me.LayoutControl1
            Me.LookUpEdit_MaritalStatusCd.TabIndex = 18
            '
            'LookUpEdit_Sync
            '
            Me.LookUpEdit_Sync.Location = New System.Drawing.Point(110, 202)
            Me.LookUpEdit_Sync.Name = "LookUpEdit_Sync"
            Me.LookUpEdit_Sync.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_Sync.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("oID", "ID", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_Sync.Properties.DisplayMember = "Description"
            Me.LookUpEdit_Sync.Properties.NullText = "None"
            Me.LookUpEdit_Sync.Properties.ValueMember = "oID"
            Me.LookUpEdit_Sync.Size = New System.Drawing.Size(359, 20)
            Me.LookUpEdit_Sync.StyleController = Me.LayoutControl1
            Me.LookUpEdit_Sync.TabIndex = 17
            '
            'EmailRecordControl_EmailID
            '
            Me.EmailRecordControl_EmailID.ClickedColor = System.Drawing.Color.Maroon
            Me.EmailRecordControl_EmailID.Location = New System.Drawing.Point(104, 54)
            Me.EmailRecordControl_EmailID.Name = "EmailRecordControl_EmailID"
            Me.EmailRecordControl_EmailID.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Underline)
            Me.EmailRecordControl_EmailID.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
            Me.EmailRecordControl_EmailID.Properties.Appearance.Options.UseFont = True
            Me.EmailRecordControl_EmailID.Properties.Appearance.Options.UseForeColor = True
            Me.EmailRecordControl_EmailID.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.EmailRecordControl_EmailID.Size = New System.Drawing.Size(139, 20)
            Me.EmailRecordControl_EmailID.StyleController = Me.LayoutControl1
            Me.EmailRecordControl_EmailID.TabIndex = 16
            '
            'NameRecordControl_NameID
            '
            Me.NameRecordControl_NameID.Location = New System.Drawing.Point(104, 29)
            Me.NameRecordControl_NameID.Margin = New System.Windows.Forms.Padding(0)
            Me.NameRecordControl_NameID.Name = "NameRecordControl_NameID"
            Me.NameRecordControl_NameID.Size = New System.Drawing.Size(363, 20)
            Me.NameRecordControl_NameID.TabIndex = 15
            '
            'LookUpEdit_IntakeCreditBureauCD
            '
            Me.LookUpEdit_IntakeCreditBureauCD.Location = New System.Drawing.Point(104, 222)
            Me.LookUpEdit_IntakeCreditBureauCD.Name = "LookUpEdit_IntakeCreditBureauCD"
            Me.LookUpEdit_IntakeCreditBureauCD.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.LookUpEdit_IntakeCreditBureauCD.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_IntakeCreditBureauCD.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_IntakeCreditBureauCD.Properties.DisplayMember = "description"
            Me.LookUpEdit_IntakeCreditBureauCD.Properties.DropDownRows = 4
            Me.LookUpEdit_IntakeCreditBureauCD.Properties.NullText = ""
            Me.LookUpEdit_IntakeCreditBureauCD.Properties.ShowFooter = False
            Me.LookUpEdit_IntakeCreditBureauCD.Properties.ShowHeader = False
            Me.LookUpEdit_IntakeCreditBureauCD.Properties.SortColumnIndex = 1
            Me.LookUpEdit_IntakeCreditBureauCD.Properties.ValueMember = "Id"
            Me.LookUpEdit_IntakeCreditBureauCD.Size = New System.Drawing.Size(139, 20)
            Me.LookUpEdit_IntakeCreditBureauCD.StyleController = Me.LayoutControl1
            Me.LookUpEdit_IntakeCreditBureauCD.TabIndex = 14
            Me.LookUpEdit_IntakeCreditBureauCD.ToolTip = "Agency where the credit score was retrieved"
            '
            'LookUpEdit_GenderCD
            '
            Me.LookUpEdit_GenderCD.Location = New System.Drawing.Point(104, 126)
            Me.LookUpEdit_GenderCD.Name = "LookUpEdit_GenderCD"
            Me.LookUpEdit_GenderCD.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.LookUpEdit_GenderCD.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_GenderCD.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_value", "ID", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_GenderCD.Properties.DisplayMember = "description"
            Me.LookUpEdit_GenderCD.Properties.DropDownRows = 2
            Me.LookUpEdit_GenderCD.Properties.NullText = ""
            Me.LookUpEdit_GenderCD.Properties.ShowFooter = False
            Me.LookUpEdit_GenderCD.Properties.ShowHeader = False
            Me.LookUpEdit_GenderCD.Properties.SortColumnIndex = 1
            Me.LookUpEdit_GenderCD.Properties.ValueMember = "Id"
            Me.LookUpEdit_GenderCD.Size = New System.Drawing.Size(139, 20)
            Me.LookUpEdit_GenderCD.StyleController = Me.LayoutControl1
            Me.LookUpEdit_GenderCD.TabIndex = 12
            Me.LookUpEdit_GenderCD.ToolTip = "Gender for the person"
            '
            'LookUpEdit_EducLevelCompletedCD
            '
            Me.LookUpEdit_EducLevelCompletedCD.Location = New System.Drawing.Point(346, 126)
            Me.LookUpEdit_EducLevelCompletedCD.Name = "LookUpEdit_EducLevelCompletedCD"
            Me.LookUpEdit_EducLevelCompletedCD.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.LookUpEdit_EducLevelCompletedCD.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_EducLevelCompletedCD.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_value", "ID", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_EducLevelCompletedCD.Properties.DisplayMember = "description"
            Me.LookUpEdit_EducLevelCompletedCD.Properties.NullText = ""
            Me.LookUpEdit_EducLevelCompletedCD.Properties.ShowFooter = False
            Me.LookUpEdit_EducLevelCompletedCD.Properties.ShowHeader = False
            Me.LookUpEdit_EducLevelCompletedCD.Properties.SortColumnIndex = 1
            Me.LookUpEdit_EducLevelCompletedCD.Properties.ValueMember = "Id"
            Me.LookUpEdit_EducLevelCompletedCD.Size = New System.Drawing.Size(121, 20)
            Me.LookUpEdit_EducLevelCompletedCD.StyleController = Me.LayoutControl1
            Me.LookUpEdit_EducLevelCompletedCD.TabIndex = 7
            Me.LookUpEdit_EducLevelCompletedCD.ToolTip = "Highest level of formal education received"
            '
            'DateEdit_DOB
            '
            Me.DateEdit_DOB.EditValue = Nothing
            Me.DateEdit_DOB.Location = New System.Drawing.Point(346, 174)
            Me.DateEdit_DOB.Name = "DateEdit_DOB"
            Me.DateEdit_DOB.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.DateEdit_DOB.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject2, "", "Edit", Nothing, True)})
            Me.DateEdit_DOB.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.DateEdit_DOB.Size = New System.Drawing.Size(121, 20)
            Me.DateEdit_DOB.StyleController = Me.LayoutControl1
            Me.DateEdit_DOB.TabIndex = 6
            Me.DateEdit_DOB.ToolTip = "Person's birthdate (Use ""1"" for month or day if you don't know it)"
            '
            'Ssn1
            '
            Me.Ssn1.Location = New System.Drawing.Point(104, 174)
            Me.Ssn1.Name = "Ssn1"
            Me.Ssn1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.Ssn1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.Ssn1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.Ssn1.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong
            Me.Ssn1.Properties.Mask.BeepOnError = True
            Me.Ssn1.Properties.Mask.EditMask = "000-00-0000"
            Me.Ssn1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
            Me.Ssn1.Properties.Mask.SaveLiteral = False
            Me.Ssn1.Properties.Mask.UseMaskAsDisplayFormat = True
            Me.Ssn1.Size = New System.Drawing.Size(139, 20)
            Me.Ssn1.StyleController = Me.LayoutControl1
            Me.Ssn1.TabIndex = 5
            Me.Ssn1.ToolTip = "This is the person's social security number."
            '
            'LookUpEdit_RaceCD
            '
            Me.LookUpEdit_RaceCD.Location = New System.Drawing.Point(104, 102)
            Me.LookUpEdit_RaceCD.Name = "LookUpEdit_RaceCD"
            Me.LookUpEdit_RaceCD.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.LookUpEdit_RaceCD.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_RaceCD.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_RaceCD.Properties.DisplayMember = "description"
            Me.LookUpEdit_RaceCD.Properties.NullText = ""
            Me.LookUpEdit_RaceCD.Properties.ShowFooter = False
            Me.LookUpEdit_RaceCD.Properties.ShowHeader = False
            Me.LookUpEdit_RaceCD.Properties.SortColumnIndex = 1
            Me.LookUpEdit_RaceCD.Properties.ValueMember = "Id"
            Me.LookUpEdit_RaceCD.Size = New System.Drawing.Size(139, 20)
            Me.LookUpEdit_RaceCD.StyleController = Me.LayoutControl1
            Me.LookUpEdit_RaceCD.TabIndex = 11
            Me.LookUpEdit_RaceCD.ToolTip = "Race for the person"
            '
            'TextEdit_MotherMaidenLName
            '
            Me.TextEdit_MotherMaidenLName.Location = New System.Drawing.Point(346, 78)
            Me.TextEdit_MotherMaidenLName.Name = "TextEdit_MotherMaidenLName"
            Me.TextEdit_MotherMaidenLName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.TextEdit_MotherMaidenLName.Properties.MaxLength = 30
            Me.TextEdit_MotherMaidenLName.Size = New System.Drawing.Size(121, 20)
            Me.TextEdit_MotherMaidenLName.StyleController = Me.LayoutControl1
            Me.TextEdit_MotherMaidenLName.TabIndex = 4
            Me.TextEdit_MotherMaidenLName.ToolTip = "Mother's Maiden LAST name (last name only please)"
            '
            'CheckEdit_DisabledIND
            '
            Me.CheckEdit_DisabledIND.Location = New System.Drawing.Point(247, 246)
            Me.CheckEdit_DisabledIND.Name = "CheckEdit_DisabledIND"
            Me.CheckEdit_DisabledIND.Properties.Caption = "Disabled"
            Me.CheckEdit_DisabledIND.Size = New System.Drawing.Size(220, 19)
            Me.CheckEdit_DisabledIND.StyleController = Me.LayoutControl1
            Me.CheckEdit_DisabledIND.TabIndex = 0
            Me.CheckEdit_DisabledIND.ToolTip = "Is the person disabled (federal standards)"
            '
            'SpinEdit_CreditScore
            '
            Me.SpinEdit_CreditScore.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
            Me.SpinEdit_CreditScore.Location = New System.Drawing.Point(104, 198)
            Me.SpinEdit_CreditScore.Name = "SpinEdit_CreditScore"
            Me.SpinEdit_CreditScore.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.SpinEdit_CreditScore.Properties.Appearance.Options.UseTextOptions = True
            Me.SpinEdit_CreditScore.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.SpinEdit_CreditScore.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.SpinEdit_CreditScore.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.[Default]
            Me.SpinEdit_CreditScore.Properties.IsFloatValue = False
            Me.SpinEdit_CreditScore.Properties.Mask.BeepOnError = True
            Me.SpinEdit_CreditScore.Properties.Mask.EditMask = "\d{0,3}"
            Me.SpinEdit_CreditScore.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.SpinEdit_CreditScore.Properties.Mask.SaveLiteral = False
            Me.SpinEdit_CreditScore.Properties.Mask.ShowPlaceHolders = False
            Me.SpinEdit_CreditScore.Properties.MaxLength = 3
            Me.SpinEdit_CreditScore.Size = New System.Drawing.Size(139, 20)
            Me.SpinEdit_CreditScore.StyleController = Me.LayoutControl1
            Me.SpinEdit_CreditScore.TabIndex = 21
            Me.SpinEdit_CreditScore.ToolTip = "Intake credit score (from 300 to 850)"
            '
            'PopupContainerEdit_Military
            '
            Me.PopupContainerEdit_Military.Location = New System.Drawing.Point(346, 150)
            Me.PopupContainerEdit_Military.Name = "PopupContainerEdit_Military"
            Me.PopupContainerEdit_Military.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.PopupContainerEdit_Military.Properties.PopupControl = Me.PopupContainerControl1
            Me.PopupContainerEdit_Military.Properties.ShowPopupCloseButton = False
            Me.PopupContainerEdit_Military.Size = New System.Drawing.Size(121, 20)
            Me.PopupContainerEdit_Military.StyleController = Me.LayoutControl1
            Me.PopupContainerEdit_Military.TabIndex = 10
            Me.PopupContainerEdit_Military.ToolTip = "Level of military service"
            '
            'LookUpEdit_Ethnicity
            '
            Me.LookUpEdit_Ethnicity.EditValue = False
            Me.LookUpEdit_Ethnicity.Location = New System.Drawing.Point(104, 246)
            Me.LookUpEdit_Ethnicity.Name = "LookUpEdit_Ethnicity"
            Me.LookUpEdit_Ethnicity.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_Ethnicity.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "description"), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default])})
            Me.LookUpEdit_Ethnicity.Properties.DisplayMember = "description"
            Me.LookUpEdit_Ethnicity.Properties.DropDownRows = 5
            Me.LookUpEdit_Ethnicity.Properties.NullText = ""
            Me.LookUpEdit_Ethnicity.Properties.ShowFooter = False
            Me.LookUpEdit_Ethnicity.Properties.ShowHeader = False
            Me.LookUpEdit_Ethnicity.Properties.ValueMember = "Id"
            Me.LookUpEdit_Ethnicity.Size = New System.Drawing.Size(139, 20)
            Me.LookUpEdit_Ethnicity.StyleController = Me.LayoutControl1
            Me.LookUpEdit_Ethnicity.TabIndex = 9
            Me.LookUpEdit_Ethnicity.ToolTip = "Is the person Hispanic (Latin American)?"
            '
            'LayoutControlItem_Sync
            '
            Me.LayoutControlItem_Sync.Control = Me.LookUpEdit_Sync
            Me.LayoutControlItem_Sync.CustomizationFormText = "Synchronize with client information"
            Me.LayoutControlItem_Sync.Location = New System.Drawing.Point(0, 198)
            Me.LayoutControlItem_Sync.Name = "LayoutControlItem_Sync"
            Me.LayoutControlItem_Sync.Size = New System.Drawing.Size(470, 29)
            Me.LayoutControlItem_Sync.Text = "Synchronize"
            Me.LayoutControlItem_Sync.TextSize = New System.Drawing.Size(101, 20)
            Me.LayoutControlItem_Sync.TextToControlDistance = 5
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem_MotherMaidenLName, Me.LayoutControlItem_DisabledIND, Me.LayoutControlItem_NameID, Me.LayoutControlItem_DOB, Me.LayoutControlItem_IntakeCreditBureauCD, Me.LayoutControlItem_Occupation, Me.LayoutControlItem_EmailID, Me.LayoutControlItem_MaritalStatusCd, Me.LayoutControlItem_RaceCD, Me.LayoutControlItem_GenderCD, Me.LayoutControlItem_EducLevelCompletedCD, Me.LayoutControlItem_PreferredLanguageCD, Me.LayoutControlItem_IntakeCreditScore, Me.LayoutControlItem_SSN, Me.LayoutControlItem_MilitaryServiceCD, Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem_EthnicityIND})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "Root"
            Me.LayoutControlGroup1.Padding = New DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2)
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(472, 301)
            Me.LayoutControlGroup1.Text = "Root"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem_MotherMaidenLName
            '
            Me.LayoutControlItem_MotherMaidenLName.Control = Me.TextEdit_MotherMaidenLName
            Me.LayoutControlItem_MotherMaidenLName.CustomizationFormText = "Mother's Maiden Last Name"
            Me.LayoutControlItem_MotherMaidenLName.Location = New System.Drawing.Point(242, 73)
            Me.LayoutControlItem_MotherMaidenLName.Name = "LayoutControlItem_MotherMaidenLName"
            Me.LayoutControlItem_MotherMaidenLName.Size = New System.Drawing.Size(224, 24)
            Me.LayoutControlItem_MotherMaidenLName.Text = "Former"
            Me.LayoutControlItem_MotherMaidenLName.TextSize = New System.Drawing.Size(96, 13)
            '
            'LayoutControlItem_DisabledIND
            '
            Me.LayoutControlItem_DisabledIND.Control = Me.CheckEdit_DisabledIND
            Me.LayoutControlItem_DisabledIND.CustomizationFormText = "Disabled Yes/No"
            Me.LayoutControlItem_DisabledIND.Location = New System.Drawing.Point(242, 241)
            Me.LayoutControlItem_DisabledIND.Name = "LayoutControlItem_DisabledIND"
            Me.LayoutControlItem_DisabledIND.Size = New System.Drawing.Size(224, 54)
            Me.LayoutControlItem_DisabledIND.Text = "Disabled"
            Me.LayoutControlItem_DisabledIND.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem_DisabledIND.TextToControlDistance = 0
            Me.LayoutControlItem_DisabledIND.TextVisible = False
            '
            'LayoutControlItem_NameID
            '
            Me.LayoutControlItem_NameID.AppearanceItemCaption.Options.UseTextOptions = True
            Me.LayoutControlItem_NameID.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LayoutControlItem_NameID.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
            Me.LayoutControlItem_NameID.Control = Me.NameRecordControl_NameID
            Me.LayoutControlItem_NameID.CustomizationFormText = "Name"
            Me.LayoutControlItem_NameID.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem_NameID.MaxSize = New System.Drawing.Size(0, 25)
            Me.LayoutControlItem_NameID.MinSize = New System.Drawing.Size(212, 25)
            Me.LayoutControlItem_NameID.Name = "LayoutControlItem_NameID"
            Me.LayoutControlItem_NameID.Size = New System.Drawing.Size(466, 25)
            Me.LayoutControlItem_NameID.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
            Me.LayoutControlItem_NameID.Text = "Name"
            Me.LayoutControlItem_NameID.TextSize = New System.Drawing.Size(96, 13)
            '
            'LayoutControlItem_DOB
            '
            Me.LayoutControlItem_DOB.Control = Me.DateEdit_DOB
            Me.LayoutControlItem_DOB.CustomizationFormText = "DOB"
            Me.LayoutControlItem_DOB.Location = New System.Drawing.Point(242, 169)
            Me.LayoutControlItem_DOB.Name = "LayoutControlItem_DOB"
            Me.LayoutControlItem_DOB.Size = New System.Drawing.Size(224, 24)
            Me.LayoutControlItem_DOB.Text = "DOB"
            Me.LayoutControlItem_DOB.TextSize = New System.Drawing.Size(96, 13)
            '
            'LayoutControlItem_IntakeCreditBureauCD
            '
            Me.LayoutControlItem_IntakeCreditBureauCD.Control = Me.LookUpEdit_IntakeCreditBureauCD
            Me.LayoutControlItem_IntakeCreditBureauCD.CustomizationFormText = "Score Agency"
            Me.LayoutControlItem_IntakeCreditBureauCD.Location = New System.Drawing.Point(0, 217)
            Me.LayoutControlItem_IntakeCreditBureauCD.Name = "LayoutControlItem_IntakeCreditBureauCD"
            Me.LayoutControlItem_IntakeCreditBureauCD.Size = New System.Drawing.Size(242, 24)
            Me.LayoutControlItem_IntakeCreditBureauCD.Text = "Score Agency"
            Me.LayoutControlItem_IntakeCreditBureauCD.TextSize = New System.Drawing.Size(96, 13)
            '
            'LayoutControlItem_Occupation
            '
            Me.LayoutControlItem_Occupation.Control = Me.TextEdit_Occupation
            Me.LayoutControlItem_Occupation.CustomizationFormText = "Occupation"
            Me.LayoutControlItem_Occupation.Location = New System.Drawing.Point(0, 73)
            Me.LayoutControlItem_Occupation.Name = "LayoutControlItem_Occupation"
            Me.LayoutControlItem_Occupation.Size = New System.Drawing.Size(242, 24)
            Me.LayoutControlItem_Occupation.Text = "Occupation"
            Me.LayoutControlItem_Occupation.TextSize = New System.Drawing.Size(96, 13)
            '
            'LayoutControlItem_EmailID
            '
            Me.LayoutControlItem_EmailID.Control = Me.EmailRecordControl_EmailID
            Me.LayoutControlItem_EmailID.CustomizationFormText = "E-Mail Address"
            Me.LayoutControlItem_EmailID.Location = New System.Drawing.Point(0, 49)
            Me.LayoutControlItem_EmailID.Name = "LayoutControlItem_EmailID"
            Me.LayoutControlItem_EmailID.Size = New System.Drawing.Size(242, 24)
            Me.LayoutControlItem_EmailID.Text = "E-Mail"
            Me.LayoutControlItem_EmailID.TextSize = New System.Drawing.Size(96, 13)
            '
            'LayoutControlItem_MaritalStatusCd
            '
            Me.LayoutControlItem_MaritalStatusCd.Control = Me.LookUpEdit_MaritalStatusCd
            Me.LayoutControlItem_MaritalStatusCd.CustomizationFormText = "Marital Status"
            Me.LayoutControlItem_MaritalStatusCd.Location = New System.Drawing.Point(242, 97)
            Me.LayoutControlItem_MaritalStatusCd.Name = "LayoutControlItem_MaritalStatusCd"
            Me.LayoutControlItem_MaritalStatusCd.Size = New System.Drawing.Size(224, 24)
            Me.LayoutControlItem_MaritalStatusCd.Text = "Marital Status"
            Me.LayoutControlItem_MaritalStatusCd.TextSize = New System.Drawing.Size(96, 13)
            '
            'LayoutControlItem_RaceCD
            '
            Me.LayoutControlItem_RaceCD.Control = Me.LookUpEdit_RaceCD
            Me.LayoutControlItem_RaceCD.CustomizationFormText = "Race"
            Me.LayoutControlItem_RaceCD.Location = New System.Drawing.Point(0, 97)
            Me.LayoutControlItem_RaceCD.Name = "LayoutControlItem_RaceCD"
            Me.LayoutControlItem_RaceCD.Size = New System.Drawing.Size(242, 24)
            Me.LayoutControlItem_RaceCD.Text = "Race"
            Me.LayoutControlItem_RaceCD.TextSize = New System.Drawing.Size(96, 13)
            '
            'LayoutControlItem_GenderCD
            '
            Me.LayoutControlItem_GenderCD.Control = Me.LookUpEdit_GenderCD
            Me.LayoutControlItem_GenderCD.CustomizationFormText = "Gender"
            Me.LayoutControlItem_GenderCD.Location = New System.Drawing.Point(0, 121)
            Me.LayoutControlItem_GenderCD.Name = "LayoutControlItem_GenderCD"
            Me.LayoutControlItem_GenderCD.Size = New System.Drawing.Size(242, 24)
            Me.LayoutControlItem_GenderCD.Text = "Gender"
            Me.LayoutControlItem_GenderCD.TextSize = New System.Drawing.Size(96, 13)
            '
            'LayoutControlItem_EducLevelCompletedCD
            '
            Me.LayoutControlItem_EducLevelCompletedCD.Control = Me.LookUpEdit_EducLevelCompletedCD
            Me.LayoutControlItem_EducLevelCompletedCD.CustomizationFormText = "Education"
            Me.LayoutControlItem_EducLevelCompletedCD.Location = New System.Drawing.Point(242, 121)
            Me.LayoutControlItem_EducLevelCompletedCD.Name = "LayoutControlItem_EducLevelCompletedCD"
            Me.LayoutControlItem_EducLevelCompletedCD.Size = New System.Drawing.Size(224, 24)
            Me.LayoutControlItem_EducLevelCompletedCD.Text = "Education"
            Me.LayoutControlItem_EducLevelCompletedCD.TextSize = New System.Drawing.Size(96, 13)
            '
            'LayoutControlItem_PreferredLanguageCD
            '
            Me.LayoutControlItem_PreferredLanguageCD.Control = Me.LookUpEdit_PreferredLanguageCD
            Me.LayoutControlItem_PreferredLanguageCD.CustomizationFormText = "Preferred Language"
            Me.LayoutControlItem_PreferredLanguageCD.FillControlToClientArea = False
            Me.LayoutControlItem_PreferredLanguageCD.Location = New System.Drawing.Point(0, 145)
            Me.LayoutControlItem_PreferredLanguageCD.Name = "LayoutControlItem_PreferredLanguageCD"
            Me.LayoutControlItem_PreferredLanguageCD.Size = New System.Drawing.Size(242, 24)
            Me.LayoutControlItem_PreferredLanguageCD.Text = "Preferred Language"
            Me.LayoutControlItem_PreferredLanguageCD.TextSize = New System.Drawing.Size(96, 13)
            '
            'LayoutControlItem_IntakeCreditScore
            '
            Me.LayoutControlItem_IntakeCreditScore.Control = Me.SpinEdit_CreditScore
            Me.LayoutControlItem_IntakeCreditScore.CustomizationFormText = "Credit Score"
            Me.LayoutControlItem_IntakeCreditScore.Location = New System.Drawing.Point(0, 193)
            Me.LayoutControlItem_IntakeCreditScore.Name = "LayoutControlItem_IntakeCreditScore"
            Me.LayoutControlItem_IntakeCreditScore.Size = New System.Drawing.Size(242, 24)
            Me.LayoutControlItem_IntakeCreditScore.Text = "Credit Score"
            Me.LayoutControlItem_IntakeCreditScore.TextSize = New System.Drawing.Size(96, 13)
            '
            'LayoutControlItem_SSN
            '
            Me.LayoutControlItem_SSN.Control = Me.Ssn1
            Me.LayoutControlItem_SSN.CustomizationFormText = "SSN"
            Me.LayoutControlItem_SSN.Location = New System.Drawing.Point(0, 169)
            Me.LayoutControlItem_SSN.Name = "LayoutControlItem_SSN"
            Me.LayoutControlItem_SSN.Size = New System.Drawing.Size(242, 24)
            Me.LayoutControlItem_SSN.Text = "SSN"
            Me.LayoutControlItem_SSN.TextSize = New System.Drawing.Size(96, 13)
            '
            'LayoutControlItem_MilitaryServiceCD
            '
            Me.LayoutControlItem_MilitaryServiceCD.Control = Me.PopupContainerEdit_Military
            Me.LayoutControlItem_MilitaryServiceCD.CustomizationFormText = "Military Service"
            Me.LayoutControlItem_MilitaryServiceCD.Location = New System.Drawing.Point(242, 145)
            Me.LayoutControlItem_MilitaryServiceCD.Name = "LayoutControlItem_MilitaryServiceCD"
            Me.LayoutControlItem_MilitaryServiceCD.Size = New System.Drawing.Size(224, 24)
            Me.LayoutControlItem_MilitaryServiceCD.Text = "Military Service"
            Me.LayoutControlItem_MilitaryServiceCD.TextSize = New System.Drawing.Size(96, 13)
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.DateEdit_emp_start_date
            Me.LayoutControlItem1.CustomizationFormText = "Hire Date"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(242, 193)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(224, 24)
            Me.LayoutControlItem1.Text = "Hire Date"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(96, 13)
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.DateEdit_emp_end_date
            Me.LayoutControlItem2.CustomizationFormText = "Term Date"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(242, 217)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(224, 24)
            Me.LayoutControlItem2.Text = "Term Date"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(96, 13)
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.ComboBoxEdit_Person
            Me.LayoutControlItem3.CustomizationFormText = "Same As"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(466, 24)
            Me.LayoutControlItem3.Text = "Same As"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(96, 13)
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.PopupContainerEdit_EverFiledBK
            Me.LayoutControlItem4.CustomizationFormText = "Ever Filed BK"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(242, 49)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(224, 24)
            Me.LayoutControlItem4.Text = "Ever Filed BK"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(96, 13)
            '
            'LayoutControlItem_EthnicityIND
            '
            Me.LayoutControlItem_EthnicityIND.Control = Me.LookUpEdit_Ethnicity
            Me.LayoutControlItem_EthnicityIND.CustomizationFormText = "Ethnicity"
            Me.LayoutControlItem_EthnicityIND.Location = New System.Drawing.Point(0, 241)
            Me.LayoutControlItem_EthnicityIND.Name = "LayoutControlItem_EthnicityIND"
            Me.LayoutControlItem_EthnicityIND.Size = New System.Drawing.Size(242, 54)
            Me.LayoutControlItem_EthnicityIND.Text = "Ethnicity"
            Me.LayoutControlItem_EthnicityIND.TextSize = New System.Drawing.Size(96, 13)
            '
            'LabelControl9
            '
            Me.LabelControl9.Location = New System.Drawing.Point(5, 111)
            Me.LabelControl9.Name = "LabelControl9"
            Me.LabelControl9.Size = New System.Drawing.Size(82, 13)
            Me.LabelControl9.TabIndex = 18
            Me.LabelControl9.Text = "Auth Letter Rcvd"
            '
            'LabelControl8
            '
            Me.LabelControl8.Location = New System.Drawing.Point(5, 59)
            Me.LabelControl8.Name = "LabelControl8"
            Me.LabelControl8.Size = New System.Drawing.Size(60, 13)
            Me.LabelControl8.TabIndex = 14
            Me.LabelControl8.Text = "PMT Current"
            '
            'dtAuthLetter
            '
            Me.dtAuthLetter.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.dtAuthLetter.EditValue = Nothing
            Me.dtAuthLetter.Location = New System.Drawing.Point(93, 108)
            Me.dtAuthLetter.Name = "dtAuthLetter"
            Me.dtAuthLetter.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.dtAuthLetter.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.dtAuthLetter.Size = New System.Drawing.Size(102, 20)
            Me.dtAuthLetter.TabIndex = 19
            '
            'luPmtCurrent
            '
            Me.luPmtCurrent.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.luPmtCurrent.Location = New System.Drawing.Point(93, 56)
            Me.luPmtCurrent.Name = "luPmtCurrent"
            Me.luPmtCurrent.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.luPmtCurrent.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.luPmtCurrent.Properties.DisplayMember = "description"
            Me.luPmtCurrent.Properties.NullText = ""
            Me.luPmtCurrent.Properties.ShowFooter = False
            Me.luPmtCurrent.Properties.ShowHeader = False
            Me.luPmtCurrent.Properties.SortColumnIndex = 1
            Me.luPmtCurrent.Properties.ValueMember = "Id"
            Me.luPmtCurrent.Size = New System.Drawing.Size(103, 20)
            Me.luPmtCurrent.TabIndex = 15
            '
            'dtBkDischargedDate
            '
            Me.dtBkDischargedDate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.dtBkDischargedDate.EditValue = Nothing
            Me.dtBkDischargedDate.Location = New System.Drawing.Point(93, 82)
            Me.dtBkDischargedDate.Name = "dtBkDischargedDate"
            Me.dtBkDischargedDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.dtBkDischargedDate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.dtBkDischargedDate.Properties.VistaTimeProperties.EditFormat.FormatString = "d"
            Me.dtBkDischargedDate.Properties.VistaTimeProperties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.dtBkDischargedDate.Properties.VistaTimeProperties.Mask.EditMask = "d"
            Me.dtBkDischargedDate.Size = New System.Drawing.Size(102, 20)
            Me.dtBkDischargedDate.TabIndex = 17
            '
            'luEverFiledBKChapter
            '
            Me.luEverFiledBKChapter.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.luEverFiledBKChapter.EditValue = ""
            Me.luEverFiledBKChapter.Location = New System.Drawing.Point(93, 30)
            Me.luEverFiledBKChapter.Name = "luEverFiledBKChapter"
            Me.luEverFiledBKChapter.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.luEverFiledBKChapter.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "description"), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default])})
            Me.luEverFiledBKChapter.Properties.DisplayMember = "description"
            Me.luEverFiledBKChapter.Properties.DropDownRows = 5
            Me.luEverFiledBKChapter.Properties.NullText = ""
            Me.luEverFiledBKChapter.Properties.ShowFooter = False
            Me.luEverFiledBKChapter.Properties.ShowHeader = False
            Me.luEverFiledBKChapter.Properties.ValueMember = "Id"
            Me.luEverFiledBKChapter.Size = New System.Drawing.Size(103, 20)
            Me.luEverFiledBKChapter.StyleController = Me.LayoutControl1
            Me.luEverFiledBKChapter.TabIndex = 13
            '
            'LabelControl7
            '
            Me.LabelControl7.Location = New System.Drawing.Point(5, 34)
            Me.LabelControl7.Name = "LabelControl7"
            Me.LabelControl7.Size = New System.Drawing.Size(39, 13)
            Me.LabelControl7.TabIndex = 12
            Me.LabelControl7.Text = "Chapter"
            '
            'LabelControl6
            '
            Me.LabelControl6.Location = New System.Drawing.Point(5, 85)
            Me.LabelControl6.Name = "LabelControl6"
            Me.LabelControl6.Size = New System.Drawing.Size(53, 13)
            Me.LabelControl6.TabIndex = 16
            Me.LabelControl6.Text = "Discharged"
            '
            'LabelControl5
            '
            Me.LabelControl5.Location = New System.Drawing.Point(5, 8)
            Me.LabelControl5.Name = "LabelControl5"
            Me.LabelControl5.Size = New System.Drawing.Size(22, 13)
            Me.LabelControl5.TabIndex = 10
            Me.LabelControl5.Text = "Filed"
            '
            'dtEverFiledBKFiledDate
            '
            Me.dtEverFiledBKFiledDate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.dtEverFiledBKFiledDate.EditValue = Nothing
            Me.dtEverFiledBKFiledDate.Location = New System.Drawing.Point(93, 4)
            Me.dtEverFiledBKFiledDate.Name = "dtEverFiledBKFiledDate"
            Me.dtEverFiledBKFiledDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.dtEverFiledBKFiledDate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.dtEverFiledBKFiledDate.Properties.VistaTimeProperties.Mask.EditMask = "d"
            Me.dtEverFiledBKFiledDate.Size = New System.Drawing.Size(102, 20)
            Me.dtEverFiledBKFiledDate.TabIndex = 11
            '
            'BorrowerControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.LayoutControl1)
            Me.Name = "BorrowerControl"
            Me.Size = New System.Drawing.Size(472, 301)
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.PopupContainerControl_EverFiledBK, System.ComponentModel.ISupportInitialize).EndInit()
            Me.PopupContainerControl_EverFiledBK.ResumeLayout(False)
            Me.PopupContainerControl_EverFiledBK.PerformLayout()
            CType(Me.PopupContainerEdit_EverFiledBK.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PopupContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.PopupContainerControl1.ResumeLayout(False)
            Me.PopupContainerControl1.PerformLayout()
            CType(Me.milDependent.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.milGrade.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.milService.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.milStatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Person.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit_emp_end_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit_emp_end_date.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit_emp_start_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit_emp_start_date.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_Occupation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_PreferredLanguageCD.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_MaritalStatusCd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_Sync.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmailRecordControl_EmailID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_IntakeCreditBureauCD.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_GenderCD.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_EducLevelCompletedCD.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit_DOB.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit_DOB.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Ssn1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_RaceCD.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_MotherMaidenLName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_DisabledIND.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.SpinEdit_CreditScore.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PopupContainerEdit_Military.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_Ethnicity.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_Sync, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_MotherMaidenLName, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_DisabledIND, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_NameID, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_DOB, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_IntakeCreditBureauCD, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_Occupation, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_EmailID, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_MaritalStatusCd, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_RaceCD, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_GenderCD, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_EducLevelCompletedCD, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_PreferredLanguageCD, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_IntakeCreditScore, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_SSN, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_MilitaryServiceCD, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_EthnicityIND, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dtAuthLetter.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dtAuthLetter.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.luPmtCurrent.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dtBkDischargedDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dtBkDischargedDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.luEverFiledBKChapter.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dtEverFiledBKFiledDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dtEverFiledBKFiledDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Protected Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Protected Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Protected Friend WithEvents TextEdit_MotherMaidenLName As DevExpress.XtraEditors.TextEdit
        Protected Friend WithEvents LayoutControlItem_MotherMaidenLName As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents LookUpEdit_IntakeCreditBureauCD As DevExpress.XtraEditors.LookUpEdit
        Protected Friend WithEvents LookUpEdit_GenderCD As DevExpress.XtraEditors.LookUpEdit
        Protected Friend WithEvents LookUpEdit_RaceCD As DevExpress.XtraEditors.LookUpEdit
        Protected Friend WithEvents LookUpEdit_EducLevelCompletedCD As DevExpress.XtraEditors.LookUpEdit
        Protected Friend WithEvents DateEdit_DOB As DevExpress.XtraEditors.DateEdit
        Protected Friend WithEvents Ssn1 As DebtPlus.Data.Controls.ssn
        Protected Friend WithEvents CheckEdit_DisabledIND As DevExpress.XtraEditors.CheckEdit
        Protected Friend WithEvents LayoutControlItem_SSN As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents LayoutControlItem_DOB As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents LayoutControlItem_DisabledIND As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents LayoutControlItem_EducLevelCompletedCD As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents LayoutControlItem_EthnicityIND As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents LayoutControlItem_MilitaryServiceCD As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents LayoutControlItem_RaceCD As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents LayoutControlItem_GenderCD As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents LayoutControlItem_IntakeCreditBureauCD As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents NameRecordControl_NameID As DebtPlus.Data.Controls.NameRecordControl
        Protected Friend WithEvents LayoutControlItem_NameID As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents EmailRecordControl_EmailID As DebtPlus.Data.Controls.EmailRecordControl
        Protected Friend WithEvents LayoutControlItem_EmailID As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents LookUpEdit_Sync As DevExpress.XtraEditors.LookUpEdit
        Protected Friend WithEvents LayoutControlItem_Sync As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents LookUpEdit_MaritalStatusCd As DevExpress.XtraEditors.LookUpEdit
        Protected Friend WithEvents LayoutControlItem_MaritalStatusCd As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents LookUpEdit_PreferredLanguageCD As DevExpress.XtraEditors.LookUpEdit
        Protected Friend WithEvents LayoutControlItem_PreferredLanguageCD As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents TextEdit_Occupation As DevExpress.XtraEditors.TextEdit
        Protected Friend WithEvents LayoutControlItem_Occupation As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents LayoutControlItem_IntakeCreditScore As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents DateEdit_emp_start_date As DevExpress.XtraEditors.DateEdit
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents DateEdit_emp_end_date As DevExpress.XtraEditors.DateEdit
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents ComboBoxEdit_Person As DevExpress.XtraEditors.ComboBoxEdit
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents SpinEdit_CreditScore As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents PopupContainerControl1 As DevExpress.XtraEditors.PopupContainerControl
        Friend WithEvents PopupContainerEdit_Military As DevExpress.XtraEditors.PopupContainerEdit
        Friend WithEvents milDependent As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents milGrade As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents milService As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents milStatus As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents PopupContainerEdit_EverFiledBK As DevExpress.XtraEditors.PopupContainerEdit
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents PopupContainerControl_EverFiledBK As DevExpress.XtraEditors.PopupContainerControl
        Friend WithEvents LookUpEdit_Ethnicity As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents dtAuthLetter As DevExpress.XtraEditors.DateEdit
        Friend WithEvents luPmtCurrent As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents dtBkDischargedDate As DevExpress.XtraEditors.DateEdit
        Friend WithEvents luEverFiledBKChapter As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents dtEverFiledBKFiledDate As DevExpress.XtraEditors.DateEdit
    End Class
End Namespace