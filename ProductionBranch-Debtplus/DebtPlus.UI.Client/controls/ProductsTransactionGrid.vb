﻿Imports System.Linq
Imports DebtPlus.LINQ
Imports System.Windows.Forms
Imports System

Namespace controls
    Friend Class ProductsTransactionGrid
        Inherits MyGridControlClient

        Private colRecords As System.Collections.Generic.List(Of product_transaction) = Nothing
        Private record As DebtPlus.LINQ.client_product = Nothing
        Private bc As BusinessContext = Nothing

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            EnableMenus = False
        End Sub

        Private Sub RegisterHandlers()
            AddHandler GridView1.CustomColumnDisplayText, AddressOf GridView1_CustomColumnDisplayText
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler GridView1.CustomColumnDisplayText, AddressOf GridView1_CustomColumnDisplayText
        End Sub

        Friend Shadows Sub SaveForm()
        End Sub

        Friend Shadows Sub ReadForm(bc As BusinessContext, record As client_product, startingDate As DateTime?, endingDate As DateTime?)

            MyClass.bc = bc
            MyClass.record = record

            GridControl1.BeginUpdate()
            UnRegisterHandlers()
            Try
                ' Generate the query expression
                Dim q As IQueryable(Of product_transaction) = bc.product_transactions.Where(Function(s) s.client_product.HasValue AndAlso s.client_product.Value = record.Id)
                If startingDate.HasValue Then
                    q = q.Where(Function(s) s.date_created >= startingDate.Value.Date)
                End If

                If endingDate.HasValue Then
                    q = q.Where(Function(s) s.date_created < endingDate.Value.AddDays(1).Date)
                End If

                ' Filter the grid control appropriately
                colRecords = q.ToList()
                GridControl1.DataSource = colRecords

            Finally
                RegisterHandlers()
                GridControl1.EndUpdate()
            End Try
        End Sub

        Private Sub GridView1_CustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs)

            ' Find the datarow that is being displayed
            Dim displayRecord As DebtPlus.LINQ.product_transaction = TryCast(GridView1.GetRow(e.RowHandle), DebtPlus.LINQ.product_transaction)
            If displayRecord Is Nothing Then
                Return
            End If

            ' Determine the column that is being shown
            If e.Column Is GridColumn_charged Then
                e.DisplayText = String.Empty
                Select Case displayRecord.transaction_type
                    Case "BB", "CD"
                        e.DisplayText = System.String.Format("{0:c}", displayRecord.cost - displayRecord.discount)
                        Exit Select
                End Select
                Return
            End If

            If e.Column Is GridColumn_paid Then
                e.DisplayText = String.Empty
                Select Case displayRecord.transaction_type
                    Case "DS"
                        e.DisplayText = System.String.Format("{0:c}", displayRecord.disputed)
                        Exit Select
                    Case "RC"
                        e.DisplayText = System.String.Format("{0:c}", displayRecord.payment)
                        Exit Select
                End Select
                Return
            End If
        End Sub

        Protected Overrides Sub OnCreateItem()
        End Sub

        Protected Overrides Sub OnDeleteItem(obj As Object)
        End Sub

        Protected Overrides Sub OnEditItem(obj As Object)
        End Sub

        ''' <summary>
        ''' Determine if the current user is allowed to delete the current note.
        ''' </summary>
        Protected Overrides Function OkToDelete(obj As Object) As Boolean
            Return False
        End Function

        ''' <summary>
        ''' Determine if the current user is allowed to edit the current note.
        ''' </summary>
        ''' <param name="obj">Pointer to the Note structure for the current note</param>
        Protected Function OkToEditNote(obj As Object) As Boolean
            Return False
        End Function
    End Class
End Namespace
