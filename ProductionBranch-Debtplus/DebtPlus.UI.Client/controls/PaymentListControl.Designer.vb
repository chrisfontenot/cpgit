Namespace controls

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class PaymentListControl
        Inherits MyGridControlClient

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container
            Me.GridColumn_date_created = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_tran_type = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_tran_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_credit = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_credit.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_debit_amt = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_debit_amt.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_message = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_message.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.SuspendLayout()
            '
            'GridColumn_date_created
            '
            Me.GridColumn_date_created.Caption = "Date"
            Me.GridColumn_date_created.CustomizationCaption = "Transaction Date"
            Me.GridColumn_date_created.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_date_created.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date_created.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_date_created.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date_created.DisplayFormat.FormatString = "d"
            Me.GridColumn_date_created.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_created.GroupFormat.FormatString = "d"
            Me.GridColumn_date_created.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_created.FieldName = "item_date"
            Me.GridColumn_date_created.Name = "GridColumn_date_created"
            Me.GridColumn_date_created.Visible = True
            Me.GridColumn_date_created.VisibleIndex = 0
            Me.GridColumn_date_created.Width = 72
            '
            'GridColumn_tran_type
            '
            Me.GridColumn_tran_type.Caption = "Tran"
            Me.GridColumn_tran_type.CustomizationCaption = "Transaction Type"
            Me.GridColumn_tran_type.FieldName = "tran_type"
            Me.GridColumn_tran_type.Name = "GridColumn_tran_type"
            Me.GridColumn_tran_type.Visible = True
            Me.GridColumn_tran_type.VisibleIndex = 1
            Me.GridColumn_tran_type.Width = 51
            '
            'GridColumn_credit
            '
            Me.GridColumn_credit.Name = "GridColumn_credit"
            Me.GridColumn_credit.FieldName = "credit_amt"
            Me.GridColumn_credit.CustomizationCaption = "Amount Credited to account"
            Me.GridColumn_credit.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_credit.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_credit.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_credit.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_credit.DisplayFormat.FormatString = "{0:$0.00;($0.00); }"
            Me.GridColumn_credit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_credit.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_credit.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_credit.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_credit.SummaryItem.FieldName = "formatted_credit_amt"
            Me.GridColumn_credit.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_credit.Caption = "Credit"
            Me.GridColumn_credit.Visible = True
            Me.GridColumn_credit.VisibleIndex = 2
            Me.GridColumn_credit.Width = 82
            '
            'GridColumn_debit_amt
            '
            Me.GridColumn_debit_amt.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_debit_amt.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_debit_amt.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_debit_amt.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_debit_amt.Caption = "Debit"
            Me.GridColumn_debit_amt.CustomizationCaption = "Amount withdrawn from account"
            Me.GridColumn_debit_amt.DisplayFormat.FormatString = "{0:$0.00;($0.00); }"
            Me.GridColumn_debit_amt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_debit_amt.FieldName = "debit_amt"
            Me.GridColumn_debit_amt.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_debit_amt.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_debit_amt.Name = "GridColumn_debit_amt"
            Me.GridColumn_debit_amt.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_debit_amt.SummaryItem.FieldName = "formatted_debit_amt"
            Me.GridColumn_debit_amt.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_debit_amt.Visible = True
            Me.GridColumn_debit_amt.VisibleIndex = 3
            Me.GridColumn_debit_amt.Width = 89
            '
            'GridColumn_message
            '
            Me.GridColumn_message.Caption = "Message"
            Me.GridColumn_message.CustomizationCaption = "Transaction Message"
            Me.GridColumn_message.FieldName = "message"
            Me.GridColumn_message.Name = "GridColumn_message"
            Me.GridColumn_message.Visible = True
            Me.GridColumn_message.VisibleIndex = 4
            Me.GridColumn_message.Width = 295
            '
            'Fix Devexpress bug
            '
            Me.GridColumn_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_tran_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_credit.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_debit_amt.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_message.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            '
            'GridView1
            '
            Me.GridView1.Columns.Clear()
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_date_created, Me.GridColumn_tran_type, Me.GridColumn_credit, Me.GridColumn_debit_amt, Me.GridColumn_message})
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsView.ShowFooter = True
            Me.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedRow
            '
            'PaymentListControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Name = "PaymentListControl"
            Me.Size = New System.Drawing.Size(550, 378)
            Me.ResumeLayout(False)
        End Sub
        Friend WithEvents GridColumn_date_created As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_tran_type As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_credit As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_debit_amt As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_message As DevExpress.XtraGrid.Columns.GridColumn
    End Class
End Namespace