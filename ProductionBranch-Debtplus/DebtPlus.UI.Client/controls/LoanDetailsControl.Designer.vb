﻿Namespace controls

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class LoanDetailsControl
        Inherits ControlBaseClient

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.CalcEdit_DelinquencyAmount = New DevExpress.XtraEditors.CalcEdit()
            Me.DateEdit_OriginationDate = New DevExpress.XtraEditors.DateEdit()
            Me.CheckEdit_ARM_Reset = New DevExpress.XtraEditors.CheckEdit()
            Me.CalcEdit_Payment = New DevExpress.XtraEditors.CalcEdit()
            Me.CalcEdit_CurrentLoanBalanceAmt = New DevExpress.XtraEditors.CalcEdit()
            Me.CheckEdit_Privately_Held_Loan = New DevExpress.XtraEditors.CheckEdit()
            Me.CheckEdit_Interest_Only_Loan = New DevExpress.XtraEditors.CheckEdit()
            Me.CalcEdit_OrigLoanAmt = New DevExpress.XtraEditors.CalcEdit()
            Me.CheckEdit_FHA_VA_Insured_Loan = New DevExpress.XtraEditors.CheckEdit()
            Me.CheckEdit_Option_ARM_Loan = New DevExpress.XtraEditors.CheckEdit()
            Me.CalcEdit_ClosingCosts = New DevExpress.XtraEditors.CalcEdit()
            Me.CheckEdit_Hybrid_ARM_Loan = New DevExpress.XtraEditors.CheckEdit()
            Me.PercentEdit_InterestRate = New DebtPlus.Data.Controls.PercentEdit()
            Me.LookUpEdit_MortgageTypeCD = New DevExpress.XtraEditors.LookUpEdit()
            Me.LookUpEdit_FinanceTypeCD = New DevExpress.XtraEditors.LookUpEdit()
            Me.LookUpEdit_LoanTypeCD = New DevExpress.XtraEditors.LookUpEdit()
            Me.comboBox_DelinquencyMonths = New DevExpress.XtraEditors.ComboBoxEdit()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_OrigLoanAmt = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_ClosingCosts = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_MonthsDelinquent = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_InterestRate = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_CurrentLoanBalanceAmt = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_Payment = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_LoanOrigDate = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem_LoanOrigDate = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem4 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.LayoutControlItem_DelinquencyAmount = New DevExpress.XtraLayout.LayoutControlItem()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.CalcEdit_DelinquencyAmount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit_OriginationDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit_OriginationDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_ARM_Reset.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_Payment.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_CurrentLoanBalanceAmt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_Privately_Held_Loan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_Interest_Only_Loan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_OrigLoanAmt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_FHA_VA_Insured_Loan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_Option_ARM_Loan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_ClosingCosts.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_Hybrid_ARM_Loan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PercentEdit_InterestRate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_MortgageTypeCD.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_FinanceTypeCD.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_LoanTypeCD.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.comboBox_DelinquencyMonths.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_OrigLoanAmt, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_ClosingCosts, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_MonthsDelinquent, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_InterestRate, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_CurrentLoanBalanceAmt, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_Payment, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_LoanOrigDate, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem_LoanOrigDate, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_DelinquencyAmount, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.CalcEdit_DelinquencyAmount)
            Me.LayoutControl1.Controls.Add(Me.DateEdit_OriginationDate)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_ARM_Reset)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit_Payment)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit_CurrentLoanBalanceAmt)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_Privately_Held_Loan)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_Interest_Only_Loan)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit_OrigLoanAmt)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_FHA_VA_Insured_Loan)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_Option_ARM_Loan)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit_ClosingCosts)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_Hybrid_ARM_Loan)
            Me.LayoutControl1.Controls.Add(Me.PercentEdit_InterestRate)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_MortgageTypeCD)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_FinanceTypeCD)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_LoanTypeCD)
            Me.LayoutControl1.Controls.Add(Me.comboBox_DelinquencyMonths)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(488, 301, 250, 350)
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(536, 308)
            Me.LayoutControl1.TabIndex = 12
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'CalcEdit_DelinquencyAmount
            '
            Me.CalcEdit_DelinquencyAmount.Location = New System.Drawing.Point(383, 173)
            Me.CalcEdit_DelinquencyAmount.Name = "CalcEdit_DelinquencyAmount"
            Me.CalcEdit_DelinquencyAmount.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.CalcEdit_DelinquencyAmount.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit_DelinquencyAmount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_DelinquencyAmount.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_DelinquencyAmount.Properties.DisplayFormat.FormatString = "c"
            Me.CalcEdit_DelinquencyAmount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_DelinquencyAmount.Properties.EditFormat.FormatString = "c"
            Me.CalcEdit_DelinquencyAmount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_DelinquencyAmount.Properties.Mask.BeepOnError = True
            Me.CalcEdit_DelinquencyAmount.Properties.Mask.EditMask = "c"
            Me.CalcEdit_DelinquencyAmount.Properties.Precision = 2
            Me.CalcEdit_DelinquencyAmount.Size = New System.Drawing.Size(148, 20)
            Me.CalcEdit_DelinquencyAmount.StyleController = Me.LayoutControl1
            Me.CalcEdit_DelinquencyAmount.TabIndex = 22
            '
            'DateEdit_OriginationDate
            '
            Me.DateEdit_OriginationDate.EditValue = Nothing
            Me.DateEdit_OriginationDate.Location = New System.Drawing.Point(106, 5)
            Me.DateEdit_OriginationDate.Name = "DateEdit_OriginationDate"
            Me.DateEdit_OriginationDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.DateEdit_OriginationDate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.DateEdit_OriginationDate.Size = New System.Drawing.Size(172, 20)
            Me.DateEdit_OriginationDate.StyleController = Me.LayoutControl1
            Me.DateEdit_OriginationDate.TabIndex = 21
            '
            'CheckEdit_ARM_Reset
            '
            Me.CheckEdit_ARM_Reset.Location = New System.Drawing.Point(282, 243)
            Me.CheckEdit_ARM_Reset.Name = "CheckEdit_ARM_Reset"
            Me.CheckEdit_ARM_Reset.Properties.Caption = "ARM Reset"
            Me.CheckEdit_ARM_Reset.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
            Me.CheckEdit_ARM_Reset.Size = New System.Drawing.Size(249, 19)
            Me.CheckEdit_ARM_Reset.StyleController = Me.LayoutControl1
            Me.CheckEdit_ARM_Reset.TabIndex = 20
            Me.CheckEdit_ARM_Reset.ToolTip = "Has the rate been rest on an ARM loan?"
            '
            'CalcEdit_Payment
            '
            Me.CalcEdit_Payment.Location = New System.Drawing.Point(106, 149)
            Me.CalcEdit_Payment.Name = "CalcEdit_Payment"
            Me.CalcEdit_Payment.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.CalcEdit_Payment.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit_Payment.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_Payment.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_Payment.Properties.DisplayFormat.FormatString = "c"
            Me.CalcEdit_Payment.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_Payment.Properties.EditFormat.FormatString = "c"
            Me.CalcEdit_Payment.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_Payment.Properties.Mask.BeepOnError = True
            Me.CalcEdit_Payment.Properties.Mask.EditMask = "c"
            Me.CalcEdit_Payment.Properties.Precision = 2
            Me.CalcEdit_Payment.Size = New System.Drawing.Size(172, 20)
            Me.CalcEdit_Payment.StyleController = Me.LayoutControl1
            Me.CalcEdit_Payment.TabIndex = 19
            Me.CalcEdit_Payment.ToolTip = "Current monthly payment on this loan"
            '
            'CalcEdit_CurrentLoanBalanceAmt
            '
            Me.CalcEdit_CurrentLoanBalanceAmt.Location = New System.Drawing.Point(383, 125)
            Me.CalcEdit_CurrentLoanBalanceAmt.Name = "CalcEdit_CurrentLoanBalanceAmt"
            Me.CalcEdit_CurrentLoanBalanceAmt.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_CurrentLoanBalanceAmt.Properties.DisplayFormat.FormatString = "c"
            Me.CalcEdit_CurrentLoanBalanceAmt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_CurrentLoanBalanceAmt.Properties.EditFormat.FormatString = "c"
            Me.CalcEdit_CurrentLoanBalanceAmt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_CurrentLoanBalanceAmt.Properties.Mask.BeepOnError = True
            Me.CalcEdit_CurrentLoanBalanceAmt.Properties.Mask.EditMask = "c"
            Me.CalcEdit_CurrentLoanBalanceAmt.Properties.Precision = 2
            Me.CalcEdit_CurrentLoanBalanceAmt.Size = New System.Drawing.Size(148, 20)
            Me.CalcEdit_CurrentLoanBalanceAmt.StyleController = Me.LayoutControl1
            Me.CalcEdit_CurrentLoanBalanceAmt.TabIndex = 16
            '
            'CheckEdit_Privately_Held_Loan
            '
            Me.CheckEdit_Privately_Held_Loan.Location = New System.Drawing.Point(282, 220)
            Me.CheckEdit_Privately_Held_Loan.Name = "CheckEdit_Privately_Held_Loan"
            Me.CheckEdit_Privately_Held_Loan.Properties.Caption = "Privately Held"
            Me.CheckEdit_Privately_Held_Loan.Size = New System.Drawing.Size(249, 19)
            Me.CheckEdit_Privately_Held_Loan.StyleController = Me.LayoutControl1
            Me.CheckEdit_Privately_Held_Loan.TabIndex = 14
            '
            'CheckEdit_Interest_Only_Loan
            '
            Me.CheckEdit_Interest_Only_Loan.Location = New System.Drawing.Point(282, 197)
            Me.CheckEdit_Interest_Only_Loan.Name = "CheckEdit_Interest_Only_Loan"
            Me.CheckEdit_Interest_Only_Loan.Properties.Caption = "Interest Only"
            Me.CheckEdit_Interest_Only_Loan.Size = New System.Drawing.Size(249, 19)
            Me.CheckEdit_Interest_Only_Loan.StyleController = Me.LayoutControl1
            Me.CheckEdit_Interest_Only_Loan.TabIndex = 13
            '
            'CalcEdit_OrigLoanAmt
            '
            Me.CalcEdit_OrigLoanAmt.Location = New System.Drawing.Point(383, 149)
            Me.CalcEdit_OrigLoanAmt.Name = "CalcEdit_OrigLoanAmt"
            Me.CalcEdit_OrigLoanAmt.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_OrigLoanAmt.Properties.DisplayFormat.FormatString = "c"
            Me.CalcEdit_OrigLoanAmt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_OrigLoanAmt.Properties.EditFormat.FormatString = "c"
            Me.CalcEdit_OrigLoanAmt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_OrigLoanAmt.Properties.Mask.BeepOnError = True
            Me.CalcEdit_OrigLoanAmt.Properties.Mask.EditMask = "c"
            Me.CalcEdit_OrigLoanAmt.Properties.Precision = 2
            Me.CalcEdit_OrigLoanAmt.Size = New System.Drawing.Size(148, 20)
            Me.CalcEdit_OrigLoanAmt.StyleController = Me.LayoutControl1
            Me.CalcEdit_OrigLoanAmt.TabIndex = 17
            '
            'CheckEdit_FHA_VA_Insured_Loan
            '
            Me.CheckEdit_FHA_VA_Insured_Loan.Location = New System.Drawing.Point(5, 243)
            Me.CheckEdit_FHA_VA_Insured_Loan.Name = "CheckEdit_FHA_VA_Insured_Loan"
            Me.CheckEdit_FHA_VA_Insured_Loan.Properties.Caption = "FHA/VA Insured"
            Me.CheckEdit_FHA_VA_Insured_Loan.Size = New System.Drawing.Size(273, 19)
            Me.CheckEdit_FHA_VA_Insured_Loan.StyleController = Me.LayoutControl1
            Me.CheckEdit_FHA_VA_Insured_Loan.TabIndex = 11
            Me.CheckEdit_FHA_VA_Insured_Loan.TabStop = False
            '
            'CheckEdit_Option_ARM_Loan
            '
            Me.CheckEdit_Option_ARM_Loan.Location = New System.Drawing.Point(5, 220)
            Me.CheckEdit_Option_ARM_Loan.Name = "CheckEdit_Option_ARM_Loan"
            Me.CheckEdit_Option_ARM_Loan.Properties.Caption = "Option ARM"
            Me.CheckEdit_Option_ARM_Loan.Size = New System.Drawing.Size(273, 19)
            Me.CheckEdit_Option_ARM_Loan.StyleController = Me.LayoutControl1
            Me.CheckEdit_Option_ARM_Loan.TabIndex = 10
            '
            'CalcEdit_ClosingCosts
            '
            Me.CalcEdit_ClosingCosts.Location = New System.Drawing.Point(106, 101)
            Me.CalcEdit_ClosingCosts.Name = "CalcEdit_ClosingCosts"
            Me.CalcEdit_ClosingCosts.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.CalcEdit_ClosingCosts.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit_ClosingCosts.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_ClosingCosts.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_ClosingCosts.Properties.DisplayFormat.FormatString = "c"
            Me.CalcEdit_ClosingCosts.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_ClosingCosts.Properties.EditFormat.FormatString = "c"
            Me.CalcEdit_ClosingCosts.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_ClosingCosts.Properties.Mask.BeepOnError = True
            Me.CalcEdit_ClosingCosts.Properties.Mask.EditMask = "c"
            Me.CalcEdit_ClosingCosts.Properties.Precision = 2
            Me.CalcEdit_ClosingCosts.Size = New System.Drawing.Size(172, 20)
            Me.CalcEdit_ClosingCosts.StyleController = Me.LayoutControl1
            Me.CalcEdit_ClosingCosts.TabIndex = 18
            '
            'CheckEdit_Hybrid_ARM_Loan
            '
            Me.CheckEdit_Hybrid_ARM_Loan.Location = New System.Drawing.Point(5, 197)
            Me.CheckEdit_Hybrid_ARM_Loan.Name = "CheckEdit_Hybrid_ARM_Loan"
            Me.CheckEdit_Hybrid_ARM_Loan.Properties.Caption = "Hybrid ARM"
            Me.CheckEdit_Hybrid_ARM_Loan.Size = New System.Drawing.Size(273, 19)
            Me.CheckEdit_Hybrid_ARM_Loan.StyleController = Me.LayoutControl1
            Me.CheckEdit_Hybrid_ARM_Loan.TabIndex = 9
            '
            'PercentEdit_InterestRate
            '
            Me.PercentEdit_InterestRate.Location = New System.Drawing.Point(106, 125)
            Me.PercentEdit_InterestRate.Name = "PercentEdit_InterestRate"
            Me.PercentEdit_InterestRate.Properties.Allow100Percent = False
            Me.PercentEdit_InterestRate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.PercentEdit_InterestRate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.PercentEdit_InterestRate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.PercentEdit_InterestRate.Properties.Precision = 3
            Me.PercentEdit_InterestRate.Size = New System.Drawing.Size(172, 20)
            Me.PercentEdit_InterestRate.StyleController = Me.LayoutControl1
            Me.PercentEdit_InterestRate.TabIndex = 7
            '
            'LookUpEdit_MortgageTypeCD
            '
            Me.LookUpEdit_MortgageTypeCD.Location = New System.Drawing.Point(106, 77)
            Me.LookUpEdit_MortgageTypeCD.Name = "LookUpEdit_MortgageTypeCD"
            Me.LookUpEdit_MortgageTypeCD.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.LookUpEdit_MortgageTypeCD.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_MortgageTypeCD.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_MortgageTypeCD.Properties.DisplayMember = "description"
            Me.LookUpEdit_MortgageTypeCD.Properties.NullText = ""
            Me.LookUpEdit_MortgageTypeCD.Properties.ShowFooter = False
            Me.LookUpEdit_MortgageTypeCD.Properties.ShowHeader = False
            Me.LookUpEdit_MortgageTypeCD.Properties.SortColumnIndex = 1
            Me.LookUpEdit_MortgageTypeCD.Properties.ValueMember = "Id"
            Me.LookUpEdit_MortgageTypeCD.Size = New System.Drawing.Size(425, 20)
            Me.LookUpEdit_MortgageTypeCD.StyleController = Me.LayoutControl1
            Me.LookUpEdit_MortgageTypeCD.TabIndex = 6
            '
            'LookUpEdit_FinanceTypeCD
            '
            Me.LookUpEdit_FinanceTypeCD.Location = New System.Drawing.Point(106, 53)
            Me.LookUpEdit_FinanceTypeCD.Name = "LookUpEdit_FinanceTypeCD"
            Me.LookUpEdit_FinanceTypeCD.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.LookUpEdit_FinanceTypeCD.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_FinanceTypeCD.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_FinanceTypeCD.Properties.DisplayMember = "description"
            Me.LookUpEdit_FinanceTypeCD.Properties.NullText = ""
            Me.LookUpEdit_FinanceTypeCD.Properties.ShowFooter = False
            Me.LookUpEdit_FinanceTypeCD.Properties.ShowHeader = False
            Me.LookUpEdit_FinanceTypeCD.Properties.SortColumnIndex = 1
            Me.LookUpEdit_FinanceTypeCD.Properties.ValueMember = "Id"
            Me.LookUpEdit_FinanceTypeCD.Size = New System.Drawing.Size(425, 20)
            Me.LookUpEdit_FinanceTypeCD.StyleController = Me.LayoutControl1
            Me.LookUpEdit_FinanceTypeCD.TabIndex = 5
            '
            'LookUpEdit_LoanTypeCD
            '
            Me.LookUpEdit_LoanTypeCD.Location = New System.Drawing.Point(106, 29)
            Me.LookUpEdit_LoanTypeCD.Name = "LookUpEdit_LoanTypeCD"
            Me.LookUpEdit_LoanTypeCD.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.LookUpEdit_LoanTypeCD.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_LoanTypeCD.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_LoanTypeCD.Properties.DisplayMember = "description"
            Me.LookUpEdit_LoanTypeCD.Properties.NullText = ""
            Me.LookUpEdit_LoanTypeCD.Properties.ShowFooter = False
            Me.LookUpEdit_LoanTypeCD.Properties.ShowHeader = False
            Me.LookUpEdit_LoanTypeCD.Properties.SortColumnIndex = 1
            Me.LookUpEdit_LoanTypeCD.Properties.ValueMember = "Id"
            Me.LookUpEdit_LoanTypeCD.Size = New System.Drawing.Size(425, 20)
            Me.LookUpEdit_LoanTypeCD.StyleController = Me.LayoutControl1
            Me.LookUpEdit_LoanTypeCD.TabIndex = 4
            '
            'comboBox_DelinquencyMonths
            '
            Me.comboBox_DelinquencyMonths.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
            Me.comboBox_DelinquencyMonths.Location = New System.Drawing.Point(106, 173)
            Me.comboBox_DelinquencyMonths.Name = "comboBox_DelinquencyMonths"
            Me.comboBox_DelinquencyMonths.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.comboBox_DelinquencyMonths.Properties.Appearance.Options.UseTextOptions = True
            Me.comboBox_DelinquencyMonths.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.comboBox_DelinquencyMonths.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.comboBox_DelinquencyMonths.Size = New System.Drawing.Size(172, 20)
            Me.comboBox_DelinquencyMonths.StyleController = Me.LayoutControl1
            Me.comboBox_DelinquencyMonths.TabIndex = 15
            Me.comboBox_DelinquencyMonths.ToolTip = "Enter the number of months that the client is CURRENTLY deinquent."
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup1.GroupBordersVisible = False
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem6, Me.LayoutControlItem11, Me.LayoutControlItem_OrigLoanAmt, Me.LayoutControlItem_ClosingCosts, Me.LayoutControlItem_MonthsDelinquent, Me.LayoutControlItem_InterestRate, Me.LayoutControlItem_CurrentLoanBalanceAmt, Me.LayoutControlItem_Payment, Me.LayoutControlItem7, Me.LayoutControlItem8, Me.LayoutControlItem10, Me.LayoutControlItem4, Me.LayoutControlItem_LoanOrigDate, Me.EmptySpaceItem_LoanOrigDate, Me.EmptySpaceItem4, Me.EmptySpaceItem3, Me.LayoutControlItem_DelinquencyAmount})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "Root"
            Me.LayoutControlGroup1.Padding = New DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3)
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(536, 308)
            Me.LayoutControlGroup1.Text = "Root"
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.LookUpEdit_LoanTypeCD
            Me.LayoutControlItem1.CustomizationFormText = "LayoutControlItem1"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(530, 24)
            Me.LayoutControlItem1.Text = "Loan Type"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(98, 13)
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.LookUpEdit_FinanceTypeCD
            Me.LayoutControlItem2.CustomizationFormText = "LayoutControlItem2"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 48)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(530, 24)
            Me.LayoutControlItem2.Text = "Finance Type"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(98, 13)
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.LookUpEdit_MortgageTypeCD
            Me.LayoutControlItem3.CustomizationFormText = "LayoutControlItem3"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 72)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(530, 24)
            Me.LayoutControlItem3.Text = "Mortgage Type"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(98, 13)
            '
            'LayoutControlItem6
            '
            Me.LayoutControlItem6.Control = Me.CheckEdit_Hybrid_ARM_Loan
            Me.LayoutControlItem6.CustomizationFormText = "LayoutControlItem6"
            Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 192)
            Me.LayoutControlItem6.Name = "LayoutControlItem6"
            Me.LayoutControlItem6.Size = New System.Drawing.Size(277, 23)
            Me.LayoutControlItem6.Text = "Hybrid ARM"
            Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem6.TextToControlDistance = 0
            Me.LayoutControlItem6.TextVisible = False
            '
            'LayoutControlItem11
            '
            Me.LayoutControlItem11.Control = Me.CheckEdit_Privately_Held_Loan
            Me.LayoutControlItem11.CustomizationFormText = "LayoutControlItem11"
            Me.LayoutControlItem11.Location = New System.Drawing.Point(277, 215)
            Me.LayoutControlItem11.Name = "LayoutControlItem11"
            Me.LayoutControlItem11.Size = New System.Drawing.Size(253, 23)
            Me.LayoutControlItem11.Text = "Privately Held"
            Me.LayoutControlItem11.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem11.TextToControlDistance = 0
            Me.LayoutControlItem11.TextVisible = False
            '
            'LayoutControlItem_OrigLoanAmt
            '
            Me.LayoutControlItem_OrigLoanAmt.Control = Me.CalcEdit_OrigLoanAmt
            Me.LayoutControlItem_OrigLoanAmt.CustomizationFormText = "Orig Amount"
            Me.LayoutControlItem_OrigLoanAmt.Location = New System.Drawing.Point(277, 144)
            Me.LayoutControlItem_OrigLoanAmt.Name = "LayoutControlItem_OrigLoanAmt"
            Me.LayoutControlItem_OrigLoanAmt.Size = New System.Drawing.Size(253, 24)
            Me.LayoutControlItem_OrigLoanAmt.Text = "Orig Amount"
            Me.LayoutControlItem_OrigLoanAmt.TextSize = New System.Drawing.Size(98, 13)
            '
            'LayoutControlItem_ClosingCosts
            '
            Me.LayoutControlItem_ClosingCosts.Control = Me.CalcEdit_ClosingCosts
            Me.LayoutControlItem_ClosingCosts.CustomizationFormText = "Closing Costs"
            Me.LayoutControlItem_ClosingCosts.Location = New System.Drawing.Point(0, 96)
            Me.LayoutControlItem_ClosingCosts.Name = "LayoutControlItem_ClosingCosts"
            Me.LayoutControlItem_ClosingCosts.Size = New System.Drawing.Size(277, 24)
            Me.LayoutControlItem_ClosingCosts.Text = "Closing Costs"
            Me.LayoutControlItem_ClosingCosts.TextSize = New System.Drawing.Size(98, 13)
            '
            'LayoutControlItem_MonthsDelinquent
            '
            Me.LayoutControlItem_MonthsDelinquent.Control = Me.comboBox_DelinquencyMonths
            Me.LayoutControlItem_MonthsDelinquent.CustomizationFormText = "Loan Delinq State"
            Me.LayoutControlItem_MonthsDelinquent.Location = New System.Drawing.Point(0, 168)
            Me.LayoutControlItem_MonthsDelinquent.Name = "LayoutControlItem_MonthsDelinquent"
            Me.LayoutControlItem_MonthsDelinquent.Size = New System.Drawing.Size(277, 24)
            Me.LayoutControlItem_MonthsDelinquent.Text = "Mos Delinquent"
            Me.LayoutControlItem_MonthsDelinquent.TextSize = New System.Drawing.Size(98, 13)
            '
            'LayoutControlItem_InterestRate
            '
            Me.LayoutControlItem_InterestRate.Control = Me.PercentEdit_InterestRate
            Me.LayoutControlItem_InterestRate.CustomizationFormText = "LayoutControlItem4"
            Me.LayoutControlItem_InterestRate.Location = New System.Drawing.Point(0, 120)
            Me.LayoutControlItem_InterestRate.Name = "LayoutControlItem_InterestRate"
            Me.LayoutControlItem_InterestRate.Size = New System.Drawing.Size(277, 24)
            Me.LayoutControlItem_InterestRate.Text = "Interest Rate"
            Me.LayoutControlItem_InterestRate.TextSize = New System.Drawing.Size(98, 13)
            '
            'LayoutControlItem_CurrentLoanBalanceAmt
            '
            Me.LayoutControlItem_CurrentLoanBalanceAmt.Control = Me.CalcEdit_CurrentLoanBalanceAmt
            Me.LayoutControlItem_CurrentLoanBalanceAmt.CustomizationFormText = "Current Balance"
            Me.LayoutControlItem_CurrentLoanBalanceAmt.Location = New System.Drawing.Point(277, 120)
            Me.LayoutControlItem_CurrentLoanBalanceAmt.Name = "LayoutControlItem_CurrentLoanBalanceAmt"
            Me.LayoutControlItem_CurrentLoanBalanceAmt.Size = New System.Drawing.Size(253, 24)
            Me.LayoutControlItem_CurrentLoanBalanceAmt.Text = "Current Balance"
            Me.LayoutControlItem_CurrentLoanBalanceAmt.TextSize = New System.Drawing.Size(98, 13)
            '
            'LayoutControlItem_Payment
            '
            Me.LayoutControlItem_Payment.Control = Me.CalcEdit_Payment
            Me.LayoutControlItem_Payment.CustomizationFormText = "Monthly Payment"
            Me.LayoutControlItem_Payment.Location = New System.Drawing.Point(0, 144)
            Me.LayoutControlItem_Payment.Name = "LayoutControlItem_Payment"
            Me.LayoutControlItem_Payment.Size = New System.Drawing.Size(277, 24)
            Me.LayoutControlItem_Payment.Text = "Monthly Payment"
            Me.LayoutControlItem_Payment.TextSize = New System.Drawing.Size(98, 13)
            '
            'LayoutControlItem7
            '
            Me.LayoutControlItem7.Control = Me.CheckEdit_Option_ARM_Loan
            Me.LayoutControlItem7.CustomizationFormText = "LayoutControlItem7"
            Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 215)
            Me.LayoutControlItem7.Name = "LayoutControlItem7"
            Me.LayoutControlItem7.Size = New System.Drawing.Size(277, 23)
            Me.LayoutControlItem7.Text = "Option ARM"
            Me.LayoutControlItem7.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem7.TextToControlDistance = 0
            Me.LayoutControlItem7.TextVisible = False
            '
            'LayoutControlItem8
            '
            Me.LayoutControlItem8.Control = Me.CheckEdit_FHA_VA_Insured_Loan
            Me.LayoutControlItem8.CustomizationFormText = "LayoutControlItem8"
            Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 238)
            Me.LayoutControlItem8.Name = "LayoutControlItem8"
            Me.LayoutControlItem8.Size = New System.Drawing.Size(277, 23)
            Me.LayoutControlItem8.Text = "FHA/VA Insured"
            Me.LayoutControlItem8.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem8.TextToControlDistance = 0
            Me.LayoutControlItem8.TextVisible = False
            '
            'LayoutControlItem10
            '
            Me.LayoutControlItem10.Control = Me.CheckEdit_Interest_Only_Loan
            Me.LayoutControlItem10.CustomizationFormText = "LayoutControlItem10"
            Me.LayoutControlItem10.Location = New System.Drawing.Point(277, 192)
            Me.LayoutControlItem10.Name = "LayoutControlItem10"
            Me.LayoutControlItem10.Size = New System.Drawing.Size(253, 23)
            Me.LayoutControlItem10.Text = "Interest Only"
            Me.LayoutControlItem10.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem10.TextToControlDistance = 0
            Me.LayoutControlItem10.TextVisible = False
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.CheckEdit_ARM_Reset
            Me.LayoutControlItem4.CustomizationFormText = "LayoutControlItem4"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(277, 238)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(253, 23)
            Me.LayoutControlItem4.Text = "LayoutControlItem4"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem4.TextToControlDistance = 0
            Me.LayoutControlItem4.TextVisible = False
            '
            'LayoutControlItem_LoanOrigDate
            '
            Me.LayoutControlItem_LoanOrigDate.Control = Me.DateEdit_OriginationDate
            Me.LayoutControlItem_LoanOrigDate.CustomizationFormText = "Loan Orig Date"
            Me.LayoutControlItem_LoanOrigDate.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem_LoanOrigDate.Name = "LayoutControlItem_LoanOrigDate"
            Me.LayoutControlItem_LoanOrigDate.Size = New System.Drawing.Size(277, 24)
            Me.LayoutControlItem_LoanOrigDate.Text = "Loan Orig Date"
            Me.LayoutControlItem_LoanOrigDate.TextSize = New System.Drawing.Size(98, 13)
            '
            'EmptySpaceItem_LoanOrigDate
            '
            Me.EmptySpaceItem_LoanOrigDate.AllowHotTrack = False
            Me.EmptySpaceItem_LoanOrigDate.CustomizationFormText = "EmptySpaceItem1"
            Me.EmptySpaceItem_LoanOrigDate.Location = New System.Drawing.Point(277, 0)
            Me.EmptySpaceItem_LoanOrigDate.Name = "EmptySpaceItem_LoanOrigDate"
            Me.EmptySpaceItem_LoanOrigDate.Size = New System.Drawing.Size(253, 24)
            Me.EmptySpaceItem_LoanOrigDate.Text = "EmptySpaceItem_LoanOrigDate"
            Me.EmptySpaceItem_LoanOrigDate.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem4
            '
            Me.EmptySpaceItem4.AllowHotTrack = False
            Me.EmptySpaceItem4.CustomizationFormText = "EmptySpaceItem4"
            Me.EmptySpaceItem4.Location = New System.Drawing.Point(0, 261)
            Me.EmptySpaceItem4.Name = "EmptySpaceItem4"
            Me.EmptySpaceItem4.Size = New System.Drawing.Size(530, 41)
            Me.EmptySpaceItem4.Text = "EmptySpaceItem4"
            Me.EmptySpaceItem4.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem3
            '
            Me.EmptySpaceItem3.AllowHotTrack = False
            Me.EmptySpaceItem3.CustomizationFormText = "EmptySpaceItem3"
            Me.EmptySpaceItem3.Location = New System.Drawing.Point(277, 96)
            Me.EmptySpaceItem3.Name = "EmptySpaceItem3"
            Me.EmptySpaceItem3.Size = New System.Drawing.Size(253, 24)
            Me.EmptySpaceItem3.Text = "EmptySpaceItem3"
            Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
            '
            'LayoutControlItem_DelinquencyAmount
            '
            Me.LayoutControlItem_DelinquencyAmount.Control = Me.CalcEdit_DelinquencyAmount
            Me.LayoutControlItem_DelinquencyAmount.CustomizationFormText = "Delinquency Amount"
            Me.LayoutControlItem_DelinquencyAmount.Location = New System.Drawing.Point(277, 168)
            Me.LayoutControlItem_DelinquencyAmount.Name = "LayoutControlItem_DelinquencyAmount"
            Me.LayoutControlItem_DelinquencyAmount.Size = New System.Drawing.Size(253, 24)
            Me.LayoutControlItem_DelinquencyAmount.Text = "Delinquency Amount"
            Me.LayoutControlItem_DelinquencyAmount.TextSize = New System.Drawing.Size(98, 13)
            '
            'LoanDetailsControl
            '
            Me.Controls.Add(Me.LayoutControl1)
            Me.Name = "LoanDetailsControl"
            Me.Size = New System.Drawing.Size(536, 308)
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.CalcEdit_DelinquencyAmount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit_OriginationDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit_OriginationDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_ARM_Reset.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_Payment.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_CurrentLoanBalanceAmt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_Privately_Held_Loan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_Interest_Only_Loan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_OrigLoanAmt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_FHA_VA_Insured_Loan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_Option_ARM_Loan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_ClosingCosts.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_Hybrid_ARM_Loan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PercentEdit_InterestRate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_MortgageTypeCD.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_FinanceTypeCD.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_LoanTypeCD.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.comboBox_DelinquencyMonths.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_OrigLoanAmt, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_ClosingCosts, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_MonthsDelinquent, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_InterestRate, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_CurrentLoanBalanceAmt, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_Payment, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_LoanOrigDate, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem_LoanOrigDate, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_DelinquencyAmount, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

        Public WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Public WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Public WithEvents CheckEdit_Privately_Held_Loan As DevExpress.XtraEditors.CheckEdit
        Public WithEvents CheckEdit_Interest_Only_Loan As DevExpress.XtraEditors.CheckEdit
        Public WithEvents CheckEdit_FHA_VA_Insured_Loan As DevExpress.XtraEditors.CheckEdit
        Public WithEvents CheckEdit_Option_ARM_Loan As DevExpress.XtraEditors.CheckEdit
        Public WithEvents CheckEdit_Hybrid_ARM_Loan As DevExpress.XtraEditors.CheckEdit
        Public WithEvents PercentEdit_InterestRate As DebtPlus.Data.Controls.PercentEdit
        Public WithEvents LookUpEdit_MortgageTypeCD As DevExpress.XtraEditors.LookUpEdit
        Public WithEvents LookUpEdit_FinanceTypeCD As DevExpress.XtraEditors.LookUpEdit
        Public WithEvents LookUpEdit_LoanTypeCD As DevExpress.XtraEditors.LookUpEdit
        Public WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Public WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Public WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Public WithEvents LayoutControlItem_InterestRate As DevExpress.XtraLayout.LayoutControlItem
        Public WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
        Public WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
        Public WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
        Public WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
        Public WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
        Public WithEvents CalcEdit_CurrentLoanBalanceAmt As DevExpress.XtraEditors.CalcEdit
        Public WithEvents LayoutControlItem_CurrentLoanBalanceAmt As DevExpress.XtraLayout.LayoutControlItem
        Public WithEvents CalcEdit_ClosingCosts As DevExpress.XtraEditors.CalcEdit
        Public WithEvents CalcEdit_OrigLoanAmt As DevExpress.XtraEditors.CalcEdit
        Public WithEvents LayoutControlItem_OrigLoanAmt As DevExpress.XtraLayout.LayoutControlItem
        Public WithEvents LayoutControlItem_ClosingCosts As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents CalcEdit_Payment As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents LayoutControlItem_Payment As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents CheckEdit_ARM_Reset As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents DateEdit_OriginationDate As DevExpress.XtraEditors.DateEdit
        Friend WithEvents LayoutControlItem_LoanOrigDate As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem4 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem3 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents CalcEdit_DelinquencyAmount As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents LayoutControlItem_DelinquencyAmount As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem_MonthsDelinquent As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem_LoanOrigDate As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents comboBox_DelinquencyMonths As DevExpress.XtraEditors.ComboBoxEdit
    End Class
End Namespace