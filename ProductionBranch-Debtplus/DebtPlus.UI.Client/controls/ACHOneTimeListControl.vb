﻿#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.Events
Imports DebtPlus.UI.Client.Service
Imports DebtPlus.UI.Client.forms.Housing
Imports DebtPlus.LINQ
Imports DevExpress.Utils
Imports System.Globalization
Imports System.Linq

Namespace controls
    Friend Class ACHOneTimeListControl
        Private bc As BusinessContext = Nothing

        ''' <summary>
        ''' Initialize the storage for the control class
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            EnableMenus = True
        End Sub

        Private colRecords As System.Collections.Generic.List(Of DebtPlus.LINQ.ach_onetime)

        ''' <summary>
        ''' Handle the initialization of the control events. Read the list of pending items
        ''' </summary>
        ''' <remarks></remarks>
        Public Overrides Sub ReadForm(bc As DebtPlus.LINQ.BusinessContext)
            MyBase.ReadForm(bc)
            Me.bc = bc

            If colRecords Is Nothing Then
                Try
                    ' Take all pending items that have not been submitted
                    Dim OpenCollection As System.Collections.Generic.List(Of DebtPlus.LINQ.ach_onetime) = bc.ach_onetimes.Where(Function(a) a.client = Context.ClientDs.ClientId AndAlso a.deposit_batch_date Is Nothing).ToList()

                    ' Take the last 3 items that have been submitted
                    Dim ClosedCollection As System.Collections.Generic.List(Of DebtPlus.LINQ.ach_onetime) = bc.ach_onetimes.Where(Function(a) a.client = Context.ClientDs.ClientId AndAlso a.deposit_batch_date IsNot Nothing).OrderByDescending(Function(a) a.deposit_batch_date).Take(3).ToList()

                    ' The resulting list is the combination of the two
                    colRecords = OpenCollection.Union(ClosedCollection).ToList()

                Catch ex As System.Data.SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
                    Return
                End Try
            End If

            ' Build the control for the grid
            GridControl1.DataSource = colRecords
        End Sub

        Protected Overrides Sub OnEditItem(obj As Object)

            ' Find the record for the edit operation
            Dim record As DebtPlus.LINQ.ach_onetime = TryCast(obj, DebtPlus.LINQ.ach_onetime)
            If record Is Nothing Then
                Return
            End If

            ' Do the edit operation on the record
            Using frm As New forms.ach_onetime_edit(record)
                If frm.ShowDialog() <> DialogResult.OK Then
                    Return
                End If
            End Using

            ' Submit the changes
            Try
                bc.SubmitChanges()
                GridView1.RefreshData()
            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
            End Try
        End Sub

        Protected Overrides Sub OnCreateItem()

            ' Create and initialize a new record
            Dim record As ach_onetime = DebtPlus.LINQ.Factory.Manufacture_ach_onetime()

            ' Update the record with the current ACH fields
            record.client = Context.ClientDs.ClientId
            If Context.ClientDs.clientACHRecord IsNot Nothing Then
                record.ABA = Context.ClientDs.clientACHRecord.ABA
                record.AccountNumber = Context.ClientDs.clientACHRecord.AccountNumber
                record.CheckingSavings = Context.ClientDs.clientACHRecord.CheckingSavings
            End If

            ' Find the last deposit that was made by the client at any time. Use this as the default values.
            Dim qLast As DebtPlus.LINQ.ach_onetime = bc.ach_onetimes.Where(Function(ach) ach.client = Context.ClientDs.ClientId).OrderByDescending(Function(ach) ach.date_created).FirstOrDefault()
            If qLast IsNot Nothing Then
                record.bank = qLast.bank
                record.CheckingSavings = qLast.CheckingSavings
                record.AccountNumber = qLast.AccountNumber
                record.ABA = qLast.ABA
            End If

            ' Edit the new record
            Using frm As New forms.ach_onetime_edit(record)
                If frm.ShowDialog() <> DialogResult.OK Then
                    Return
                End If
            End Using

            ' Add the new item to the database and the list
            bc.ach_onetimes.InsertOnSubmit(record)
            colRecords.Add(record)

            ' Insert the system note that the deposit was added
            Dim noteRecord As DebtPlus.LINQ.client_note = DebtPlus.LINQ.Factory.Manufacture_client_note()
            noteRecord.subject = "Added One-Time ACH Deposit"
            noteRecord.client = Context.ClientDs.ClientId
            noteRecord.note = String.Format("Added One-Time ACH deposit item {0}", record.ToString())
            bc.client_notes.InsertOnSubmit(noteRecord)

            Try
                ' Submit the changes
                bc.SubmitChanges()
                GridView1.RefreshData()
            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
            End Try
        End Sub

        Protected Overrides Sub OnDeleteItem(obj As Object)

            ' Find the record for the edit operation
            Dim record As DebtPlus.LINQ.ach_onetime = TryCast(obj, DebtPlus.LINQ.ach_onetime)
            If record Is Nothing Then
                Return
            End If

            ' A record that has a submission date may not be deleted
            If record.deposit_batch_date IsNot Nothing Then
                DebtPlus.Data.Forms.MessageBox.Show("The item has been sent to the bank and may not be deleted at this time.", "Can not delete item", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)
                Return
            End If

            ' Ask if the removal is really desired
            If DebtPlus.Data.Prompts.RequestConfirmation_Delete() <> DialogResult.Yes Then
                Return
            End If

            ' Remove the record from the database
            bc.ach_onetimes.DeleteOnSubmit(record)
            colRecords.Remove(record)

            ' Insert the system note that the deposit was removed
            Dim noteRecord As DebtPlus.LINQ.client_note = DebtPlus.LINQ.Factory.Manufacture_client_note()
            noteRecord.subject = "Deleted One-Time ACH Deposit"
            noteRecord.client = Context.ClientDs.ClientId
            noteRecord.note = String.Format("Deleted One-Time ACH deposit item {0}", record.ToString())
            bc.client_notes.InsertOnSubmit(noteRecord)

            Try
                ' Submit the changes
                bc.SubmitChanges()
                GridView1.RefreshData()
            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
            End Try
        End Sub
    End Class
End Namespace
