﻿Namespace controls
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ProductsNotes

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.ProductsNotesGrid1 = New DebtPlus.UI.Client.controls.ProductsNotesGrid()
            Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
            Me.CheckEdit_System = New DevExpress.XtraEditors.CheckEdit()
            Me.CheckEdit_Perm = New DevExpress.XtraEditors.CheckEdit()
            CType(Me.ProductsNotesGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupControl1.SuspendLayout()
            CType(Me.CheckEdit_System.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_Perm.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ProductsNotesGrid1
            '
            Me.ProductsNotesGrid1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ProductsNotesGrid1.Location = New System.Drawing.Point(0, 48)
            Me.ProductsNotesGrid1.Margin = New System.Windows.Forms.Padding(0)
            Me.ProductsNotesGrid1.Name = "ProductsNotesGrid1"
            Me.ProductsNotesGrid1.Size = New System.Drawing.Size(553, 102)
            Me.ProductsNotesGrid1.TabIndex = 0
            '
            'GroupControl1
            '
            Me.GroupControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GroupControl1.Controls.Add(Me.CheckEdit_System)
            Me.GroupControl1.Controls.Add(Me.CheckEdit_Perm)
            Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
            Me.GroupControl1.Name = "GroupControl1"
            Me.GroupControl1.Size = New System.Drawing.Size(553, 48)
            Me.GroupControl1.TabIndex = 2
            Me.GroupControl1.Text = "Show Notes of the following type(s)"
            '
            'CheckEdit_System
            '
            Me.CheckEdit_System.EditValue = True
            Me.CheckEdit_System.Location = New System.Drawing.Point(81, 24)
            Me.CheckEdit_System.Name = "CheckEdit_System"
            Me.CheckEdit_System.Properties.Caption = "System"
            Me.CheckEdit_System.Size = New System.Drawing.Size(80, 19)
            Me.CheckEdit_System.TabIndex = 1
            '
            'CheckEdit_Perm
            '
            Me.CheckEdit_Perm.EditValue = True
            Me.CheckEdit_Perm.Location = New System.Drawing.Point(0, 24)
            Me.CheckEdit_Perm.Name = "CheckEdit_Perm"
            Me.CheckEdit_Perm.Properties.Caption = "Permanent"
            Me.CheckEdit_Perm.Size = New System.Drawing.Size(80, 19)
            Me.CheckEdit_Perm.TabIndex = 0
            '
            'ProductsNotes
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.GroupControl1)
            Me.Controls.Add(Me.ProductsNotesGrid1)
            Me.Name = "ProductsNotes"
            Me.Size = New System.Drawing.Size(556, 150)
            CType(Me.ProductsNotesGrid1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupControl1.ResumeLayout(False)
            CType(Me.CheckEdit_System.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_Perm.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents ProductsNotesGrid1 As DebtPlus.UI.Client.controls.ProductsNotesGrid
        Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
        Friend WithEvents CheckEdit_System As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents CheckEdit_Perm As DevExpress.XtraEditors.CheckEdit
    End Class
End Namespace
