﻿Namespace controls
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ProductsBalance
        Inherits System.Windows.Forms.UserControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then components.Dispose()
                End If
                components = Nothing
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer = Nothing

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.LabelControl_CurrentBalance = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl_Payments = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl_Disputed = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl_OriginalBalance = New DevExpress.XtraEditors.LabelControl()
            Me.TextEdit_Discount = New DevExpress.XtraEditors.CalcEdit()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.TextEdit_Discount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.LabelControl_CurrentBalance)
            Me.LayoutControl1.Controls.Add(Me.LabelControl_Payments)
            Me.LayoutControl1.Controls.Add(Me.LabelControl_Disputed)
            Me.LayoutControl1.Controls.Add(Me.LabelControl_OriginalBalance)
            Me.LayoutControl1.Controls.Add(Me.TextEdit_Discount)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(300, 238)
            Me.LayoutControl1.TabIndex = 0
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'LabelControl_CurrentBalance
            '
            Me.LabelControl_CurrentBalance.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl_CurrentBalance.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_CurrentBalance.Location = New System.Drawing.Point(162, 100)
            Me.LabelControl_CurrentBalance.Name = "LabelControl_CurrentBalance"
            Me.LabelControl_CurrentBalance.Padding = New System.Windows.Forms.Padding(0, 0, 22, 0)
            Me.LabelControl_CurrentBalance.Size = New System.Drawing.Size(133, 13)
            Me.LabelControl_CurrentBalance.StyleController = Me.LayoutControl1
            Me.LabelControl_CurrentBalance.TabIndex = 7
            '
            'LabelControl_Payments
            '
            Me.LabelControl_Payments.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl_Payments.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_Payments.Location = New System.Drawing.Point(162, 83)
            Me.LabelControl_Payments.Name = "LabelControl_Payments"
            Me.LabelControl_Payments.Padding = New System.Windows.Forms.Padding(0, 0, 22, 0)
            Me.LabelControl_Payments.Size = New System.Drawing.Size(133, 13)
            Me.LabelControl_Payments.StyleController = Me.LayoutControl1
            Me.LabelControl_Payments.TabIndex = 6
            '
            'LabelControl_Disputed
            '
            Me.LabelControl_Disputed.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl_Disputed.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_Disputed.Location = New System.Drawing.Point(162, 66)
            Me.LabelControl_Disputed.Name = "LabelControl_Disputed"
            Me.LabelControl_Disputed.Padding = New System.Windows.Forms.Padding(0, 0, 22, 0)
            Me.LabelControl_Disputed.Size = New System.Drawing.Size(133, 13)
            Me.LabelControl_Disputed.StyleController = Me.LayoutControl1
            Me.LabelControl_Disputed.TabIndex = 5
            '
            'LabelControl_OriginalBalance
            '
            Me.LabelControl_OriginalBalance.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl_OriginalBalance.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_OriginalBalance.Location = New System.Drawing.Point(162, 25)
            Me.LabelControl_OriginalBalance.Name = "LabelControl_OriginalBalance"
            Me.LabelControl_OriginalBalance.Padding = New System.Windows.Forms.Padding(0, 0, 22, 0)
            Me.LabelControl_OriginalBalance.Size = New System.Drawing.Size(133, 13)
            Me.LabelControl_OriginalBalance.StyleController = Me.LayoutControl1
            Me.LabelControl_OriginalBalance.TabIndex = 4
            '
            'TextEdit_Discount
            '
            Me.TextEdit_Discount.EditValue = "$0.00"
            Me.TextEdit_Discount.Location = New System.Drawing.Point(162, 42)
            Me.TextEdit_Discount.Name = "TextEdit_Discount"
            Me.TextEdit_Discount.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.TextEdit_Discount.Properties.Appearance.Options.UseTextOptions = True
            Me.TextEdit_Discount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.TextEdit_Discount.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.TextEdit_Discount.Properties.DisplayFormat.FormatString = "c2"
            Me.TextEdit_Discount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.TextEdit_Discount.Properties.EditFormat.FormatString = "f2"
            Me.TextEdit_Discount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.TextEdit_Discount.Properties.Mask.BeepOnError = True
            Me.TextEdit_Discount.Properties.Mask.EditMask = "c"
            Me.TextEdit_Discount.Properties.ValidateOnEnterKey = True
            Me.TextEdit_Discount.Size = New System.Drawing.Size(133, 20)
            Me.TextEdit_Discount.StyleController = Me.LayoutControl1
            Me.TextEdit_Discount.TabIndex = 8
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup1.GroupBordersVisible = False
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup2})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(300, 238)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlGroup2
            '
            Me.LayoutControlGroup2.CustomizationFormText = "Balance"
            Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem4, Me.LayoutControlItem3, Me.LayoutControlItem2, Me.LayoutControlItem5, Me.LayoutControlItem1})
            Me.LayoutControlGroup2.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
            Me.LayoutControlGroup2.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
            Me.LayoutControlGroup2.Size = New System.Drawing.Size(300, 238)
            Me.LayoutControlGroup2.Text = "Balance"
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.LabelControl_CurrentBalance
            Me.LayoutControlItem4.CustomizationFormText = "Current Balance"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 75)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(294, 137)
            Me.LayoutControlItem4.Text = "Current Balance"
            Me.LayoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(77, 13)
            Me.LayoutControlItem4.TextToControlDistance = 80
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.LabelControl_Payments
            Me.LayoutControlItem3.CustomizationFormText = "Total Payments"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 58)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(294, 17)
            Me.LayoutControlItem3.Text = "Total Payments"
            Me.LayoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(77, 13)
            Me.LayoutControlItem3.TextToControlDistance = 80
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.LabelControl_Disputed
            Me.LayoutControlItem2.CustomizationFormText = "Disputed Amt"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 41)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(294, 17)
            Me.LayoutControlItem2.Text = "Disputed Amt"
            Me.LayoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(77, 13)
            Me.LayoutControlItem2.TextToControlDistance = 80
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Control = Me.TextEdit_Discount
            Me.LayoutControlItem5.CustomizationFormText = "Discount"
            Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 17)
            Me.LayoutControlItem5.Name = "LayoutControlItem5"
            Me.LayoutControlItem5.Size = New System.Drawing.Size(294, 24)
            Me.LayoutControlItem5.Text = "Discount"
            Me.LayoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
            Me.LayoutControlItem5.TextSize = New System.Drawing.Size(77, 13)
            Me.LayoutControlItem5.TextToControlDistance = 80
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.LabelControl_OriginalBalance
            Me.LayoutControlItem1.CustomizationFormText = "Original Balance"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(294, 17)
            Me.LayoutControlItem1.Text = "Original Balance"
            Me.LayoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(77, 13)
            Me.LayoutControlItem1.TextToControlDistance = 80
            '
            'ProductsBalance
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.LayoutControl1)
            Me.Name = "ProductsBalance"
            Me.Size = New System.Drawing.Size(300, 238)
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.TextEdit_Discount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents LabelControl_CurrentBalance As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl_Payments As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl_Disputed As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl_OriginalBalance As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents TextEdit_Discount As DevExpress.XtraEditors.CalcEdit

    End Class
End Namespace
