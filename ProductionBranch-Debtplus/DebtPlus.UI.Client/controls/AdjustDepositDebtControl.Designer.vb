﻿Namespace controls
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class AdjustDepositDebtControl
        Inherits ControlBaseClient

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
            Me.GridColumn_client_creditor = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_creditor = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_creditor_name = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_account_number = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_balance = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_disbursement_factor = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.RepositoryItemCalcEdit_disbursement_factor = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit()
            Me.GridColumn_active_debt = New DevExpress.XtraGrid.Columns.GridColumn()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RepositoryItemCalcEdit_disbursement_factor, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridControl1
            '
            Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.GridControl1.Location = New System.Drawing.Point(0, 0)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCalcEdit_disbursement_factor})
            Me.GridControl1.Size = New System.Drawing.Size(508, 267)
            Me.GridControl1.TabIndex = 0
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(195, Byte), Integer))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(189, Byte), Integer))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(206, Byte), Integer))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(225, Byte), Integer))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(215, Byte), Integer))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(215, Byte), Integer))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(215, Byte), Integer))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_client_creditor, Me.GridColumn_creditor, Me.GridColumn_creditor_name, Me.GridColumn_account_number, Me.GridColumn_balance, Me.GridColumn_disbursement_factor, Me.GridColumn_active_debt})
            Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsBehavior.AutoPopulateColumns = False
            Me.GridView1.OptionsLayout.Columns.StoreAllOptions = True
            Me.GridView1.OptionsLayout.LayoutVersion = "2"
            Me.GridView1.OptionsLayout.StoreAllOptions = True
            Me.GridView1.OptionsLayout.StoreDataSettings = False
            Me.GridView1.OptionsView.EnableAppearanceEvenRow = True
            Me.GridView1.OptionsView.EnableAppearanceOddRow = True
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridView1.OptionsView.ShowIndicator = False
            Me.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedRow
            Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn_creditor, DevExpress.Data.ColumnSortOrder.Ascending)})
            '
            'GridColumn_client_creditor
            '
            Me.GridColumn_client_creditor.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_client_creditor.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_client_creditor.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_client_creditor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_client_creditor.Caption = "ID"
            Me.GridColumn_client_creditor.CustomizationCaption = "Record ID"
            Me.GridColumn_client_creditor.DisplayFormat.FormatString = "{0:f0}"
            Me.GridColumn_client_creditor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_client_creditor.FieldName = "client_creditor"
            Me.GridColumn_client_creditor.GroupFormat.FormatString = "{0:f0}"
            Me.GridColumn_client_creditor.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_client_creditor.Name = "GridColumn_client_creditor"
            Me.GridColumn_client_creditor.OptionsColumn.AllowEdit = False
            Me.GridColumn_client_creditor.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridColumn_client_creditor.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridColumn_client_creditor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_client_creditor.OptionsColumn.ReadOnly = True
            '
            'GridColumn_creditor
            '
            Me.GridColumn_creditor.Caption = "Creditor"
            Me.GridColumn_creditor.CustomizationCaption = "Creditor ID"
            Me.GridColumn_creditor.FieldName = "display_debt_id"
            Me.GridColumn_creditor.Name = "GridColumn_creditor"
            Me.GridColumn_creditor.OptionsColumn.AllowEdit = False
            Me.GridColumn_creditor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_creditor.Visible = True
            Me.GridColumn_creditor.VisibleIndex = 0
            Me.GridColumn_creditor.Width = 59
            '
            'GridColumn_creditor_name
            '
            Me.GridColumn_creditor_name.Caption = "Name"
            Me.GridColumn_creditor_name.CustomizationCaption = "Creditor Name"
            Me.GridColumn_creditor_name.FieldName = "display_creditor_name"
            Me.GridColumn_creditor_name.Name = "GridColumn_creditor_name"
            Me.GridColumn_creditor_name.OptionsColumn.AllowEdit = False
            Me.GridColumn_creditor_name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_creditor_name.Visible = True
            Me.GridColumn_creditor_name.VisibleIndex = 1
            Me.GridColumn_creditor_name.Width = 143
            '
            'GridColumn_account_number
            '
            Me.GridColumn_account_number.Caption = "Account Number"
            Me.GridColumn_account_number.CustomizationCaption = "Account Number"
            Me.GridColumn_account_number.FieldName = "account_number"
            Me.GridColumn_account_number.Name = "GridColumn_account_number"
            Me.GridColumn_account_number.OptionsColumn.AllowEdit = False
            Me.GridColumn_account_number.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_account_number.Visible = True
            Me.GridColumn_account_number.VisibleIndex = 2
            Me.GridColumn_account_number.Width = 124
            '
            'GridColumn_balance
            '
            Me.GridColumn_balance.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_balance.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_balance.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_balance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_balance.Caption = "Balance"
            Me.GridColumn_balance.CustomizationCaption = "Current Debt Balance"
            Me.GridColumn_balance.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_balance.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_balance.FieldName = "current_balance"
            Me.GridColumn_balance.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_balance.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_balance.Name = "GridColumn_balance"
            Me.GridColumn_balance.OptionsColumn.AllowEdit = False
            Me.GridColumn_balance.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_balance.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "current_balance", "{0:c}")})
            Me.GridColumn_balance.Visible = True
            Me.GridColumn_balance.VisibleIndex = 4
            Me.GridColumn_balance.Width = 91
            '
            'GridColumn_disbursement_factor
            '
            Me.GridColumn_disbursement_factor.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_disbursement_factor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_disbursement_factor.Caption = "Disbursement"
            Me.GridColumn_disbursement_factor.ColumnEdit = Me.RepositoryItemCalcEdit_disbursement_factor
            Me.GridColumn_disbursement_factor.CustomizationCaption = "Disbursement Factor"
            Me.GridColumn_disbursement_factor.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_disbursement_factor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_disbursement_factor.FieldName = "debit_amt"
            Me.GridColumn_disbursement_factor.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_disbursement_factor.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_disbursement_factor.Name = "GridColumn_disbursement_factor"
            Me.GridColumn_disbursement_factor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_disbursement_factor.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "debit_amt", "{0:c}")})
            Me.GridColumn_disbursement_factor.Visible = True
            Me.GridColumn_disbursement_factor.VisibleIndex = 3
            Me.GridColumn_disbursement_factor.Width = 87
            '
            'RepositoryItemCalcEdit_disbursement_factor
            '
            Me.RepositoryItemCalcEdit_disbursement_factor.Appearance.Options.UseTextOptions = True
            Me.RepositoryItemCalcEdit_disbursement_factor.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.RepositoryItemCalcEdit_disbursement_factor.AppearanceDisabled.Options.UseTextOptions = True
            Me.RepositoryItemCalcEdit_disbursement_factor.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.RepositoryItemCalcEdit_disbursement_factor.AppearanceDropDown.Options.UseTextOptions = True
            Me.RepositoryItemCalcEdit_disbursement_factor.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.RepositoryItemCalcEdit_disbursement_factor.AppearanceFocused.Options.UseTextOptions = True
            Me.RepositoryItemCalcEdit_disbursement_factor.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.RepositoryItemCalcEdit_disbursement_factor.AppearanceReadOnly.Options.UseTextOptions = True
            Me.RepositoryItemCalcEdit_disbursement_factor.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.RepositoryItemCalcEdit_disbursement_factor.AutoHeight = False
            Me.RepositoryItemCalcEdit_disbursement_factor.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.RepositoryItemCalcEdit_disbursement_factor.DisplayFormat.FormatString = "{0:c}"
            Me.RepositoryItemCalcEdit_disbursement_factor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.RepositoryItemCalcEdit_disbursement_factor.EditFormat.FormatString = "{0:f2}"
            Me.RepositoryItemCalcEdit_disbursement_factor.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.RepositoryItemCalcEdit_disbursement_factor.EditValueChangedDelay = 1
            Me.RepositoryItemCalcEdit_disbursement_factor.Mask.BeepOnError = True
            Me.RepositoryItemCalcEdit_disbursement_factor.Mask.EditMask = "c"
            Me.RepositoryItemCalcEdit_disbursement_factor.Name = "RepositoryItemCalcEdit_disbursement_factor"
            Me.RepositoryItemCalcEdit_disbursement_factor.Precision = 2
            '
            'GridColumn_active_debt
            '
            Me.GridColumn_active_debt.Caption = "GridColumn1"
            Me.GridColumn_active_debt.FieldName = "IsActive"
            Me.GridColumn_active_debt.Name = "GridColumn_active_debt"
            Me.GridColumn_active_debt.OptionsColumn.AllowEdit = False
            Me.GridColumn_active_debt.OptionsColumn.AllowFocus = False
            Me.GridColumn_active_debt.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridColumn_active_debt.OptionsColumn.AllowIncrementalSearch = False
            Me.GridColumn_active_debt.OptionsColumn.AllowMove = False
            Me.GridColumn_active_debt.OptionsColumn.AllowSize = False
            Me.GridColumn_active_debt.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridColumn_active_debt.OptionsColumn.ReadOnly = True
            Me.GridColumn_active_debt.OptionsColumn.ShowCaption = False
            Me.GridColumn_active_debt.OptionsColumn.ShowInCustomizationForm = False
            '
            'AdjustDepositDebtControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.GridControl1)
            Me.Name = "AdjustDepositDebtControl"
            Me.Size = New System.Drawing.Size(508, 267)
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RepositoryItemCalcEdit_disbursement_factor, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents GridColumn_client_creditor As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_creditor As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_creditor_name As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_disbursement_factor As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents RepositoryItemCalcEdit_disbursement_factor As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Friend WithEvents GridColumn_account_number As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_active_debt As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_balance As DevExpress.XtraGrid.Columns.GridColumn
    End Class
End Namespace