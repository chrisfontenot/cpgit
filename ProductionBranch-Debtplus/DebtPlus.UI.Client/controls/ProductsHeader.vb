﻿Imports DebtPlus.LINQ
Imports System.Linq

Namespace controls
    Public Class ProductsHeader
        Inherits System.Windows.Forms.UserControl

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler LabelControl_vendor_label.Click, AddressOf LabelControl_vendor_label_Click
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler LabelControl_vendor_label.Click, AddressOf LabelControl_vendor_label_Click
        End Sub

        Friend Sub ReadForm(bc As BusinessContext, record As client_product)
            UnRegisterHandlers()
            Try
                ' Find the product reference for this vendor. We want the ESCROW flag
                Dim productRecord As vendor_product = bc.vendor_products.Where(Function(s) s.product = record.product AndAlso s.vendor = record.vendor).FirstOrDefault()
                If productRecord IsNot Nothing Then
                    LabelControl_escrow.Text = Helpers.GetEscrowProBonoText(productRecord.escrowProBono)
                End If

                ' Set the vendor name accordingly
                LabelControl_vendor_label.Text = record.vendor1.Label

                If record.vendor1.vendor_type IsNot Nothing Then
                    LabelControl_vendor_type.Text = record.vendor1.vendor_type.description
                End If

                ' Find the general address record for this vendor
                Dim sb As New System.Text.StringBuilder()
                If Not String.IsNullOrWhiteSpace(record.vendor1.Name) Then
                    sb.Append(Environment.NewLine)
                    sb.Append(record.vendor1.Name)
                End If

                Dim ie_adr As System.Collections.Generic.IEnumerator(Of DebtPlus.LINQ.vendor_address) = record.vendor1.vendor_addresses.GetEnumerator()
                Do While ie_adr.MoveNext()
                    Dim adr As DebtPlus.LINQ.vendor_address = ie_adr.Current()
                    If adr.address_type = DebtPlus.LINQ.vendor_address.AddressType_General AndAlso adr.addressID.HasValue Then
                        Dim addrRecord As DebtPlus.LINQ.address = bc.addresses.Where(Function(s) s.Id = adr.addressID.Value).FirstOrDefault()
                        If addrRecord IsNot Nothing Then
                            sb.Append(Environment.NewLine)
                            sb.Append(addrRecord.ToString())
                            Exit Do
                        End If
                    End If
                Loop

                ' Set the name/address information
                If sb.Length > 0 Then
                    sb.Remove(0, 2)
                End If
                LabelControl_NameAndAddress.Text = sb.ToString()
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Friend Shadows Sub SaveForm()
        End Sub

        Private Sub LabelControl_vendor_label_Click(sender As Object, e As System.EventArgs)
            Dim thrd As New System.Threading.Thread(New System.Threading.ThreadStart(AddressOf VendorEditThread))
            thrd.IsBackground = True
            thrd.Name = "VendorEdit"
            thrd.SetApartmentState(Threading.ApartmentState.STA)
            thrd.Start()
        End Sub

        Private Sub VendorEditThread()
            Using bcThread As New DebtPlus.LINQ.BusinessContext()
                Try
                    Dim vendorRecord As DebtPlus.LINQ.vendor = bcThread.vendors.Where(Function(s) s.Label = LabelControl_vendor_label.Text).FirstOrDefault()
                    If vendorRecord IsNot Nothing Then
                        AddHandler bcThread.BeforeSubmitChanges, AddressOf bcThread.RecordSystemNoteHandler

                        Using frm As New DebtPlus.UI.Vendor.Update.Forms.Form_VendorUpdate(bcThread, vendorRecord, False)
                            frm.ShowDialog()
                        End Using

                        RemoveHandler bcThread.BeforeSubmitChanges, AddressOf bcThread.RecordSystemNoteHandler
                        bcThread.SubmitChanges()
                    End If

                Catch ex As System.Data.SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating vendor information")
                End Try
            End Using
        End Sub
    End Class
End Namespace
