﻿Imports DebtPlus.LINQ
Imports System.Linq

Namespace controls
    Public Class ProductsAccount
        Inherits DevExpress.XtraEditors.XtraUserControl

        Private bc As BusinessContext = Nothing
        Private record As client_product = Nothing

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Private Sub RegisterHandlers()
        End Sub

        Private Sub UnRegisterHandlers()
        End Sub

        Friend Shadows Sub SaveForm()
            ProductsBalance1.SaveForm()
            ProductsDispute1.SaveForm()

            record.message = DebtPlus.Utils.Nulls.v_String(TextEdit_Message.EditValue)
            record.reference = DebtPlus.Utils.Nulls.v_String(TextEdit_Reference.EditValue)
        End Sub

        Friend Sub ReadForm(bc As BusinessContext, record As client_product)
            Me.bc = bc
            Me.record = record

            UnRegisterHandlers()
            Try
                ProductsBalance1.ReadForm(bc, record)
                ProductsDispute1.ReadForm(bc, record)

                ' Message and reference fields
                TextEdit_Message.EditValue = record.message
                TextEdit_Reference.EditValue = record.reference

                ' Last Invoice date
                LabelControl_InvoiceDate.Text = If(record.invoice_date.HasValue, record.invoice_date.Value.ToShortDateString(), String.Empty)

                ' Last payment date and amount
                LabelControl_LastPaymentAmt.Text = String.Empty
                LabelControl_LastPaymentDate.Text = String.Empty
                Dim qInvTrans As product_transaction = bc.product_transactions.Where(Function(s) s.client_product.HasValue AndAlso s.client_product.Value = record.Id AndAlso s.transaction_type = "PC").OrderByDescending(Function(s) s.date_created).FirstOrDefault()
                If qInvTrans IsNot Nothing Then
                    LabelControl_LastPaymentDate.Text = qInvTrans.date_created.ToShortDateString()
                    LabelControl_LastPaymentAmt.Text = qInvTrans.payment.ToString("c2")
                End If

            Finally
                RegisterHandlers()
            End Try
        End Sub

    End Class
End Namespace
