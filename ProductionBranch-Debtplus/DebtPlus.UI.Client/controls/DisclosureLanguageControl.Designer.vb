﻿Namespace controls
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class DisclosureLanguageControl
        Inherits DebtPlus.Data.Controls.UserControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.LookUpEdit_Language = New DevExpress.XtraEditors.LookUpEdit()
            Me.LookUpEdit_DisclosureType = New DevExpress.XtraEditors.LookUpEdit()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.LookUpEdit_Language.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_DisclosureType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_Language)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_DisclosureType)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(385, 52)
            Me.LayoutControl1.TabIndex = 0
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'LookUpEdit_Language
            '
            Me.LookUpEdit_Language.Location = New System.Drawing.Point(50, 29)
            Me.LookUpEdit_Language.Name = "LookUpEdit_Language"
            Me.LookUpEdit_Language.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_Language.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("display_language", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.LookUpEdit_Language.Properties.DisplayMember = "display_language"
            Me.LookUpEdit_Language.Properties.NullText = ""
            Me.LookUpEdit_Language.Properties.ShowFooter = False
            Me.LookUpEdit_Language.Properties.ShowHeader = False
            Me.LookUpEdit_Language.Properties.ValueMember = "Id"
            Me.LookUpEdit_Language.Size = New System.Drawing.Size(335, 20)
            Me.LookUpEdit_Language.StyleController = Me.LayoutControl1
            Me.LookUpEdit_Language.TabIndex = 5
            '
            'LookUpEdit_DisclosureType
            '
            Me.LookUpEdit_DisclosureType.Location = New System.Drawing.Point(50, 3)
            Me.LookUpEdit_DisclosureType.Name = "LookUpEdit_DisclosureType"
            Me.LookUpEdit_DisclosureType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_DisclosureType.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.LookUpEdit_DisclosureType.Properties.DisplayMember = "description"
            Me.LookUpEdit_DisclosureType.Properties.NullText = ""
            Me.LookUpEdit_DisclosureType.Properties.ShowFooter = False
            Me.LookUpEdit_DisclosureType.Properties.ShowHeader = False
            Me.LookUpEdit_DisclosureType.Properties.ValueMember = "Id"
            Me.LookUpEdit_DisclosureType.Size = New System.Drawing.Size(335, 20)
            Me.LookUpEdit_DisclosureType.StyleController = Me.LayoutControl1
            Me.LookUpEdit_DisclosureType.TabIndex = 4
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup1.GroupBordersVisible = False
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(385, 52)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.LookUpEdit_DisclosureType
            Me.LayoutControlItem1.CustomizationFormText = "Type"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 3, 3)
            Me.LayoutControlItem1.Size = New System.Drawing.Size(385, 26)
            Me.LayoutControlItem1.Text = "Type"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(47, 13)
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.LookUpEdit_Language
            Me.LayoutControlItem2.CustomizationFormText = "Language"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 26)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 3, 3)
            Me.LayoutControlItem2.Size = New System.Drawing.Size(385, 26)
            Me.LayoutControlItem2.Text = "Language"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(47, 13)
            '
            'DisclosureLanguageControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.LayoutControl1)
            Me.MinimumSize = New System.Drawing.Size(0, 52)
            Me.Name = "DisclosureLanguageControl"
            Me.Size = New System.Drawing.Size(385, 52)
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.LookUpEdit_Language.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_DisclosureType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Protected Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Protected Friend WithEvents LookUpEdit_Language As DevExpress.XtraEditors.LookUpEdit
        Protected Friend WithEvents LookUpEdit_DisclosureType As DevExpress.XtraEditors.LookUpEdit
        Protected Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Protected Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem

    End Class
End Namespace
