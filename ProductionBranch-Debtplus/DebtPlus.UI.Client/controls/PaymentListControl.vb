#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports System.ComponentModel
Imports DebtPlus.Utils
Imports System.Data.SqlClient

Namespace controls

    Friend Class PaymentListControl

        Private WithEvents bt As BackgroundWorker
        Private FromDate As DateTime
        Private ToDate As DateTime
        Private ds As DataSet

        Public Sub New()
            MyBase.New()

            ' Do the one-time initialization next
            bt = New BackgroundWorker()
            ds = New DataSet("ds")

            ' The do the component initialization
            InitializeComponent()

            ' Add the "Handles" clauses
            AddHandler bt.DoWork, AddressOf bt_DoWork
            AddHandler bt.RunWorkerCompleted, AddressOf bt_RunWorkerCompleted

            ' Then finally, do the remainder of the initialization
            EnableMenus = False
        End Sub

        ''' <summary>
        ''' Read the loans table
        ''' </summary>
        Public Shadows Sub ReadForm(ByVal FromDate As DateTime, ByVal ToDate As DateTime)

            ' Save the parameters for the background thread
            Me.FromDate = FromDate
            Me.ToDate = ToDate

            If Not bt.IsBusy Then
                ' Blank the list as an indication that we are busy
                GridControl1.DataSource = Nothing
                GridControl1.RefreshDataSource()
                GridControl1.UseWaitCursor = True

                ' Start a background thread to read the information
                bt.WorkerReportsProgress = False
                bt.WorkerSupportsCancellation = False
                bt.RunWorkerAsync()
            End If
        End Sub

        Private Sub bt_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs)
            Try
                Using cmd As SqlCommand = New SqlCommand
                    cmd.Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    cmd.CommandText = "rpt_transactions_cl"
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add("@client", SqlDbType.Int).Value = Context.ClientId
                    cmd.Parameters.Add("@fromdate", SqlDbType.DateTime).Value = FromDate
                    cmd.Parameters.Add("@todate", SqlDbType.DateTime).Value = ToDate

                    Using da As New SqlDataAdapter(cmd)
                        ds.Reset()
                        da.Fill(ds, "client_transactions")
                    End Using
                End Using

                ' Add two columns to not total the BB items.
                Dim tbl As System.Data.DataTable = ds.Tables(0)

                If Not tbl.Columns.Contains("formatted_credit_amt") Then
                    tbl.Columns.Add("formatted_credit_amt", GetType(Decimal), "IIF([tran_type]='BB', 0, [credit_amt])")
                End If

                If Not tbl.Columns.Contains("formatted_debit_amt") Then
                    tbl.Columns.Add("formatted_debit_amt", GetType(Decimal), "IIF([tran_type]='BB', 0, [debit_amt])")
                End If

            Catch ex As SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading client transactions")
            End Try
        End Sub

        Private Sub bt_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs)
            If ds.Tables.Count > 0 Then
                Dim tbl As DataTable = ds.Tables(0)
                GridControl1.UseWaitCursor = False
                GridControl1.DataSource = tbl.DefaultView
                GridControl1.RefreshDataSource()
                GridView1.BestFitColumns()
            End If
        End Sub
    End Class
End Namespace
