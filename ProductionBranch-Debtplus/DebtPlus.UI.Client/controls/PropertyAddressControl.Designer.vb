﻿Namespace controls
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class PropertyAddressControl
        Inherits ControlBaseClient

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.CheckEdit_UseHomeAddress = New DevExpress.XtraEditors.CheckEdit()
            Me.CheckEdit_ViewCurrent = New DevExpress.XtraEditors.CheckEdit()
            Me.CheckEdit_ViewPrevious = New DevExpress.XtraEditors.CheckEdit()
            Me.AddressRecordControl_AddressID = New DebtPlus.Data.Controls.AddressRecordControl()
            Me.AddressRecordControl_PreviousAddressID = New DebtPlus.Data.Controls.AddressRecordControl()
            CType(Me.CheckEdit_UseHomeAddress.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_ViewCurrent.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_ViewPrevious.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.AddressRecordControl_AddressID, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.AddressRecordControl_PreviousAddressID, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'CheckEdit_UseHomeAddress
            '
            Me.CheckEdit_UseHomeAddress.Location = New System.Drawing.Point(0, 0)
            Me.CheckEdit_UseHomeAddress.Name = "CheckEdit_UseHomeAddress"
            Me.CheckEdit_UseHomeAddress.Properties.Caption = "Use Home Address"
            Me.CheckEdit_UseHomeAddress.Size = New System.Drawing.Size(153, 19)
            Me.CheckEdit_UseHomeAddress.TabIndex = 0
            '
            'CheckEdit_ViewCurrent
            '
            Me.CheckEdit_ViewCurrent.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.CheckEdit_ViewCurrent.EditValue = True
            Me.CheckEdit_ViewCurrent.Location = New System.Drawing.Point(292, 0)
            Me.CheckEdit_ViewCurrent.Name = "CheckEdit_ViewCurrent"
            Me.CheckEdit_ViewCurrent.Properties.Caption = "Intake"
            Me.CheckEdit_ViewCurrent.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
            Me.CheckEdit_ViewCurrent.Properties.RadioGroupIndex = 0
            Me.CheckEdit_ViewCurrent.Size = New System.Drawing.Size(66, 19)
            Me.CheckEdit_ViewCurrent.TabIndex = 1
            '
            'CheckEdit_ViewPrevious
            '
            Me.CheckEdit_ViewPrevious.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.CheckEdit_ViewPrevious.Location = New System.Drawing.Point(364, 0)
            Me.CheckEdit_ViewPrevious.Name = "CheckEdit_ViewPrevious"
            Me.CheckEdit_ViewPrevious.Properties.Caption = "Post"
            Me.CheckEdit_ViewPrevious.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
            Me.CheckEdit_ViewPrevious.Properties.RadioGroupIndex = 0
            Me.CheckEdit_ViewPrevious.Size = New System.Drawing.Size(74, 19)
            Me.CheckEdit_ViewPrevious.TabIndex = 2
            Me.CheckEdit_ViewPrevious.TabStop = False
            '
            'AddressRecordControl_AddressID
            '
            Me.AddressRecordControl_AddressID.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                                                               Or System.Windows.Forms.AnchorStyles.Left) _
                                                              Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.AddressRecordControl_AddressID.Location = New System.Drawing.Point(3, 25)
            Me.AddressRecordControl_AddressID.Name = "AddressRecordControl_AddressID"
            Me.AddressRecordControl_AddressID.Size = New System.Drawing.Size(435, 75)
            Me.AddressRecordControl_AddressID.TabIndex = 3
            '
            'AddressRecordControl_PreviousAddressID
            '
            Me.AddressRecordControl_PreviousAddressID.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                                                                       Or System.Windows.Forms.AnchorStyles.Left) _
                                                                      Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.AddressRecordControl_PreviousAddressID.Location = New System.Drawing.Point(3, 25)
            Me.AddressRecordControl_PreviousAddressID.Name = "AddressRecordControl_PreviousAddressID"
            Me.AddressRecordControl_PreviousAddressID.Size = New System.Drawing.Size(435, 75)
            Me.AddressRecordControl_PreviousAddressID.TabIndex = 4
            '
            'PropertyAddressControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.CheckEdit_ViewPrevious)
            Me.Controls.Add(Me.CheckEdit_ViewCurrent)
            Me.Controls.Add(Me.CheckEdit_UseHomeAddress)
            Me.Controls.Add(Me.AddressRecordControl_AddressID)
            Me.Controls.Add(Me.AddressRecordControl_PreviousAddressID)
            Me.Name = "PropertyAddressControl"
            Me.Size = New System.Drawing.Size(441, 99)
            CType(Me.CheckEdit_UseHomeAddress.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_ViewCurrent.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_ViewPrevious.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.AddressRecordControl_AddressID, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.AddressRecordControl_PreviousAddressID, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
        End Sub

        Public WithEvents CheckEdit_UseHomeAddress As DevExpress.XtraEditors.CheckEdit
        Public WithEvents CheckEdit_ViewCurrent As DevExpress.XtraEditors.CheckEdit
        Public WithEvents CheckEdit_ViewPrevious As DevExpress.XtraEditors.CheckEdit
        Public WithEvents AddressRecordControl_AddressID As DebtPlus.Data.Controls.AddressRecordControl
        Public WithEvents AddressRecordControl_PreviousAddressID As DebtPlus.Data.Controls.AddressRecordControl
    End Class
End Namespace