#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.UI.Client.forms
Imports DebtPlus.LINQ
Imports System.Linq

Namespace controls
    Friend Class ClientDepositsControl
        Private bc As DebtPlus.LINQ.BusinessContext = Nothing

        ''' <summary>
        ''' Event to indicate the deposit amount was changed
        ''' </summary>
        ''' <remarks></remarks>
        Public Event ClientDepositAmountChanged As ClientDepositAmountChangedEventHandler

        ''' <summary>
        ''' Raise the ClientDepositAmountChanged event
        ''' </summary>
        Protected Sub RaiseClientDepositAmountChanged(ByVal e As ClientDepositAmountChangedEventArgs)
            RaiseEvent ClientDepositAmountChanged(Me, e)
        End Sub

        ''' <summary>
        ''' Raise the ClientDepositAmountChanged event
        ''' </summary>
        Protected Overridable Sub OnClientDepositAmountChanged(ByVal e As ClientDepositAmountChangedEventArgs)
            RaiseClientDepositAmountChanged(e)
        End Sub

        ''' <summary>
        ''' Initialize the control
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            EnableMenus = True
        End Sub

        ''' <summary>
        ''' Read the list of client deposit items
        ''' </summary>
        ''' <remarks></remarks>
        Public Overrides Sub ReadForm(bc As DebtPlus.LINQ.BusinessContext)
            Me.bc = bc
            Try
                GridControl1.DataSource = Context.ClientDs.colClientDeposits
                GridView1.BestFitColumns()
                GridControl1.RefreshDataSource()
            Catch ex As Exception
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
            End Try
        End Sub

        ''' <summary>
        ''' Edit the current record information
        ''' </summary>
        Protected Overrides Sub OnEditItem(obj As Object)

            Dim record As DebtPlus.LINQ.client_deposit = TryCast(obj, DebtPlus.LINQ.client_deposit)
            If record Is Nothing Then
                Return
            End If

            ' Find the old dollar amount
            Dim oldValue As Decimal = Context.ClientDs.colClientDeposits.Sum(Function(s) s.deposit_amount)

            ' Edit the new item. If accepted then insert it into the database
            Using frm As New Form_ClientDeposit(record)
                If frm.ShowDialog() <> DialogResult.OK Then
                    Return
                End If
            End Using

            Try
                ' Generate the event that the amount figures were changed
                Dim newValue As Decimal = Context.ClientDs.colClientDeposits.Sum(Function(s) s.deposit_amount)
                If newValue <> oldValue Then
                    GenerateSystemNote(oldValue, newValue)
                    OnClientDepositAmountChanged(New ClientDepositAmountChangedEventArgs(oldValue, newValue))
                End If

                bc.SubmitChanges()
                GridControl1.RefreshDataSource()

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
            End Try
        End Sub

        ''' <summary>
        ''' Create a new deposit record item
        ''' </summary>
        ''' <remarks></remarks>
        Protected Overrides Sub OnCreateItem()

            ' Allocate a new deposit record
            Dim record As client_deposit = DebtPlus.LINQ.Factory.Manufacture_client_deposit(Context.ClientDs.ClientId)

            ' Find the old dollar amount
            Dim oldValue As Decimal = Context.ClientDs.colClientDeposits.Sum(Function(s) s.deposit_amount)

            ' Edit the new item. If accepted then insert it into the database
            Using frm As New Form_ClientDeposit(record)
                If frm.ShowDialog() <> DialogResult.OK Then
                    Return
                End If
            End Using

            Try
                bc.client_deposits.InsertOnSubmit(record)

                ' Refresh the grid with the new values
                Context.ClientDs.colClientDeposits.Add(record)

                ' Generate the event that the amount figures were changed
                Dim newValue As Decimal = Context.ClientDs.colClientDeposits.Sum(Function(s) s.deposit_amount)
                If newValue <> oldValue Then
                    GenerateSystemNote(oldValue, newValue)
                    OnClientDepositAmountChanged(New ClientDepositAmountChangedEventArgs(oldValue, newValue))
                End If

                bc.SubmitChanges()
                GridView1.RefreshData()

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
            End Try
        End Sub

        ''' <summary>
        ''' Delete the client deposit item from the database
        ''' </summary>
        Protected Overrides Sub OnDeleteItem(obj As Object)
            Dim record As DebtPlus.LINQ.client_deposit = TryCast(obj, DebtPlus.LINQ.client_deposit)
            If record Is Nothing Then
                Return
            End If

            ' If OK to delete then attempt to delete the item
            If DebtPlus.Data.Prompts.RequestConfirmation_Delete() <> DialogResult.Yes Then
                Return
            End If

            Try
                ' Find the old dollar amount
                Dim oldValue As Decimal = Context.ClientDs.colClientDeposits.Sum(Function(s) s.deposit_amount)

                bc.client_deposits.DeleteOnSubmit(record)
                Context.ClientDs.colClientDeposits.Remove(record)

                ' Generate the event that the amount figures were changed
                Dim newValue As Decimal = Context.ClientDs.colClientDeposits.Sum(Function(s) s.deposit_amount)
                If newValue <> oldValue Then
                    GenerateSystemNote(oldValue, newValue)
                    OnClientDepositAmountChanged(New ClientDepositAmountChangedEventArgs(oldValue, newValue))
                End If

                bc.SubmitChanges()
                GridView1.RefreshData()

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
            End Try
        End Sub

        Private Sub GenerateSystemNote(ByVal oldValue As Decimal, ByVal newValue As Decimal)

            Dim note As client_note = DebtPlus.LINQ.Factory.Manufacture_client_note(Context.ClientDs.ClientId, 3)
            note.subject = "Changed expected deposit amount"
            note.note = String.Format("Changed the expected client deposit amount from {0:c} to {1:c}", oldValue, newValue)

            bc.client_notes.InsertOnSubmit(note)
        End Sub
    End Class

    ''' <summary>
    ''' Client Deposit Value Changed Event Arguments
    ''' </summary>
    ''' <remarks></remarks>
    Public Class ClientDepositAmountChangedEventArgs
        Inherits System.EventArgs

        Private privateOldValue As Decimal
        Private privateNewValue As Decimal

        Public ReadOnly Property OldValue As Decimal
            Get
                Return privateOldValue
            End Get
        End Property

        Public ReadOnly Property NewValue As Decimal
            Get
                Return privateNewValue
            End Get
        End Property

        Public Sub New(ByVal OldValue As Decimal, ByVal NewValue As Decimal)
            MyBase.New()
            privateOldValue = OldValue
            privateNewValue = NewValue
        End Sub
    End Class

    ''' <summary>
    ''' Handler for the client deposit amount changed event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Delegate Sub ClientDepositAmountChangedEventHandler(ByVal sender As Object, ByVal e As ClientDepositAmountChangedEventArgs)
End Namespace