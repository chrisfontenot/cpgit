﻿Namespace controls

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class LenderControl
        Inherits ControlBaseClient

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.dt_LastLenderUpdated = New DevExpress.XtraEditors.DateEdit()
            Me.LookUpEdit_HPF_CnslrContactLenderOutcome = New DevExpress.XtraEditors.LookUpEdit()
            Me.SpinEdit_Repay_Plan = New DevExpress.XtraEditors.SpinEdit()
            Me.CalEdit_CorpAdvance = New DevExpress.XtraEditors.CalcEdit()
            Me.CalEdit_Monthly_Desired_Repay = New DevExpress.XtraEditors.CalcEdit()
            Me.LookUpEdit_HPF_CnslrContactLenderReason = New DevExpress.XtraEditors.LookUpEdit()
            Me.txtInvestorAccountNumber = New DevExpress.XtraEditors.TextEdit()
            Me.chkCounselorContactSuccess = New DevExpress.XtraEditors.CheckEdit()
            Me.dtCurrentWorkoutPlanDate = New DevExpress.XtraEditors.DateEdit()
            Me.LookUpEdit_HPF_ClientConcatLenderOutcome = New DevExpress.XtraEditors.LookUpEdit()
            Me.luInvestor = New DevExpress.XtraEditors.LookUpEdit()
            Me.TextEdit_FdicNcusNum = New DevExpress.XtraEditors.TextEdit()
            Me.TextEdit_AcctNum = New DevExpress.XtraEditors.TextEdit()
            Me.TextEdit_CaseNumber = New DevExpress.XtraEditors.TextEdit()
            Me.comboBox_ServicerID = New DevExpress.XtraEditors.ComboBoxEdit()
            Me.dtCounselorContactDate = New DevExpress.XtraEditors.DateEdit()
            Me.dtClientContactDate = New DevExpress.XtraEditors.DateEdit()
            Me.LayoutControlGroup_All = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlGroup_Main = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlGroup_HECM_Default = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem17 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem_BottomFiller = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.LayoutControlGroup_CounselorContact = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlGroup_ClientContact = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.dt_LastLenderUpdated.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_LastLenderUpdated.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_HPF_CnslrContactLenderOutcome.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.SpinEdit_Repay_Plan.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalEdit_CorpAdvance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalEdit_Monthly_Desired_Repay.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_HPF_CnslrContactLenderReason.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtInvestorAccountNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.chkCounselorContactSuccess.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dtCurrentWorkoutPlanDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dtCurrentWorkoutPlanDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_HPF_ClientConcatLenderOutcome.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.luInvestor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_FdicNcusNum.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_AcctNum.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_CaseNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.comboBox_ServicerID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dtCounselorContactDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dtCounselorContactDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dtClientContactDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dtClientContactDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup_All, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup_Main, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup_HECM_Default, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem_BottomFiller, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup_CounselorContact, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup_ClientContact, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LayoutControlItem6
            '
            Me.LayoutControlItem6.CustomizationFormText = "LayoutControlItem6"
            Me.LayoutControlItem6.Location = New System.Drawing.Point(515, 0)
            Me.LayoutControlItem6.Name = "LayoutControlItem6"
            Me.LayoutControlItem6.Size = New System.Drawing.Size(1, 292)
            Me.LayoutControlItem6.Text = "LayoutControlItem6"
            Me.LayoutControlItem6.TextSize = New System.Drawing.Size(50, 20)
            Me.LayoutControlItem6.TextToControlDistance = 5
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.dt_LastLenderUpdated)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_HPF_CnslrContactLenderOutcome)
            Me.LayoutControl1.Controls.Add(Me.SpinEdit_Repay_Plan)
            Me.LayoutControl1.Controls.Add(Me.CalEdit_CorpAdvance)
            Me.LayoutControl1.Controls.Add(Me.CalEdit_Monthly_Desired_Repay)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_HPF_CnslrContactLenderReason)
            Me.LayoutControl1.Controls.Add(Me.txtInvestorAccountNumber)
            Me.LayoutControl1.Controls.Add(Me.chkCounselorContactSuccess)
            Me.LayoutControl1.Controls.Add(Me.dtCurrentWorkoutPlanDate)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_HPF_ClientConcatLenderOutcome)
            Me.LayoutControl1.Controls.Add(Me.luInvestor)
            Me.LayoutControl1.Controls.Add(Me.TextEdit_FdicNcusNum)
            Me.LayoutControl1.Controls.Add(Me.TextEdit_AcctNum)
            Me.LayoutControl1.Controls.Add(Me.TextEdit_CaseNumber)
            Me.LayoutControl1.Controls.Add(Me.comboBox_ServicerID)
            Me.LayoutControl1.Controls.Add(Me.dtCounselorContactDate)
            Me.LayoutControl1.Controls.Add(Me.dtClientContactDate)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(488, 301, 250, 350)
            Me.LayoutControl1.OptionsFocus.EnableAutoTabOrder = False
            Me.LayoutControl1.Root = Me.LayoutControlGroup_All
            Me.LayoutControl1.Size = New System.Drawing.Size(536, 312)
            Me.LayoutControl1.TabIndex = 0
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'dt_LastLenderUpdated
            '
            Me.dt_LastLenderUpdated.EditValue = Nothing
            Me.dt_LastLenderUpdated.Location = New System.Drawing.Point(384, 216)
            Me.dt_LastLenderUpdated.Name = "dt_LastLenderUpdated"
            Me.dt_LastLenderUpdated.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.dt_LastLenderUpdated.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.dt_LastLenderUpdated.Size = New System.Drawing.Size(147, 20)
            Me.dt_LastLenderUpdated.StyleController = Me.LayoutControl1
            Me.dt_LastLenderUpdated.TabIndex = 16
            '
            'LookUpEdit_HPF_CnslrContactLenderOutcome
            '
            Me.LookUpEdit_HPF_CnslrContactLenderOutcome.Location = New System.Drawing.Point(384, 72)
            Me.LookUpEdit_HPF_CnslrContactLenderOutcome.Name = "LookUpEdit_HPF_CnslrContactLenderOutcome"
            Me.LookUpEdit_HPF_CnslrContactLenderOutcome.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_HPF_CnslrContactLenderOutcome.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.LookUpEdit_HPF_CnslrContactLenderOutcome.Properties.DisplayMember = "description"
            Me.LookUpEdit_HPF_CnslrContactLenderOutcome.Properties.NullText = ""
            Me.LookUpEdit_HPF_CnslrContactLenderOutcome.Properties.PopupWidth = 350
            Me.LookUpEdit_HPF_CnslrContactLenderOutcome.Properties.ShowFooter = False
            Me.LookUpEdit_HPF_CnslrContactLenderOutcome.Properties.ShowHeader = False
            Me.LookUpEdit_HPF_CnslrContactLenderOutcome.Properties.SortColumnIndex = 1
            Me.LookUpEdit_HPF_CnslrContactLenderOutcome.Properties.ValueMember = "Id"
            Me.LookUpEdit_HPF_CnslrContactLenderOutcome.Size = New System.Drawing.Size(147, 20)
            Me.LookUpEdit_HPF_CnslrContactLenderOutcome.StyleController = Me.LayoutControl1
            Me.LookUpEdit_HPF_CnslrContactLenderOutcome.TabIndex = 11
            '
            'SpinEdit_Repay_Plan
            '
            Me.SpinEdit_Repay_Plan.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
            Me.SpinEdit_Repay_Plan.Location = New System.Drawing.Point(384, 192)
            Me.SpinEdit_Repay_Plan.Name = "SpinEdit_Repay_Plan"
            Me.SpinEdit_Repay_Plan.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.SpinEdit_Repay_Plan.Properties.IsFloatValue = False
            Me.SpinEdit_Repay_Plan.Properties.Mask.EditMask = "N00"
            Me.SpinEdit_Repay_Plan.Properties.MaxValue = New Decimal(New Integer() {99, 0, 0, 0})
            Me.SpinEdit_Repay_Plan.Properties.NullText = "0"
            Me.SpinEdit_Repay_Plan.Size = New System.Drawing.Size(147, 20)
            Me.SpinEdit_Repay_Plan.StyleController = Me.LayoutControl1
            Me.SpinEdit_Repay_Plan.TabIndex = 15
            '
            'CalEdit_CorpAdvance
            '
            Me.CalEdit_CorpAdvance.Location = New System.Drawing.Point(384, 144)
            Me.CalEdit_CorpAdvance.Name = "CalEdit_CorpAdvance"
            Me.CalEdit_CorpAdvance.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalEdit_CorpAdvance.Properties.DisplayFormat.FormatString = "c"
            Me.CalEdit_CorpAdvance.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalEdit_CorpAdvance.Properties.Mask.BeepOnError = True
            Me.CalEdit_CorpAdvance.Properties.Mask.EditMask = "c"
            Me.CalEdit_CorpAdvance.Properties.Mask.IgnoreMaskBlank = False
            Me.CalEdit_CorpAdvance.Properties.Mask.SaveLiteral = False
            Me.CalEdit_CorpAdvance.Properties.Mask.ShowPlaceHolders = False
            Me.CalEdit_CorpAdvance.Properties.Mask.UseMaskAsDisplayFormat = True
            Me.CalEdit_CorpAdvance.Properties.NullText = "$0.00"
            Me.CalEdit_CorpAdvance.Properties.Precision = 2
            Me.CalEdit_CorpAdvance.Size = New System.Drawing.Size(147, 20)
            Me.CalEdit_CorpAdvance.StyleController = Me.LayoutControl1
            Me.CalEdit_CorpAdvance.TabIndex = 13
            '
            'CalEdit_Monthly_Desired_Repay
            '
            Me.CalEdit_Monthly_Desired_Repay.Location = New System.Drawing.Point(384, 168)
            Me.CalEdit_Monthly_Desired_Repay.Name = "CalEdit_Monthly_Desired_Repay"
            Me.CalEdit_Monthly_Desired_Repay.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalEdit_Monthly_Desired_Repay.Properties.DisplayFormat.FormatString = "c"
            Me.CalEdit_Monthly_Desired_Repay.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalEdit_Monthly_Desired_Repay.Properties.Mask.BeepOnError = True
            Me.CalEdit_Monthly_Desired_Repay.Properties.Mask.EditMask = "c"
            Me.CalEdit_Monthly_Desired_Repay.Properties.Mask.IgnoreMaskBlank = False
            Me.CalEdit_Monthly_Desired_Repay.Properties.Mask.SaveLiteral = False
            Me.CalEdit_Monthly_Desired_Repay.Properties.Mask.ShowPlaceHolders = False
            Me.CalEdit_Monthly_Desired_Repay.Properties.Mask.UseMaskAsDisplayFormat = True
            Me.CalEdit_Monthly_Desired_Repay.Properties.NullText = "$0.00"
            Me.CalEdit_Monthly_Desired_Repay.Properties.Precision = 2
            Me.CalEdit_Monthly_Desired_Repay.Size = New System.Drawing.Size(147, 20)
            Me.CalEdit_Monthly_Desired_Repay.StyleController = Me.LayoutControl1
            Me.CalEdit_Monthly_Desired_Repay.TabIndex = 14
            '
            'LookUpEdit_HPF_CnslrContactLenderReason
            '
            Me.LookUpEdit_HPF_CnslrContactLenderReason.Location = New System.Drawing.Point(384, 48)
            Me.LookUpEdit_HPF_CnslrContactLenderReason.Name = "LookUpEdit_HPF_CnslrContactLenderReason"
            Me.LookUpEdit_HPF_CnslrContactLenderReason.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_HPF_CnslrContactLenderReason.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.LookUpEdit_HPF_CnslrContactLenderReason.Properties.DisplayMember = "description"
            Me.LookUpEdit_HPF_CnslrContactLenderReason.Properties.NullText = ""
            Me.LookUpEdit_HPF_CnslrContactLenderReason.Properties.PopupWidth = 350
            Me.LookUpEdit_HPF_CnslrContactLenderReason.Properties.ShowFooter = False
            Me.LookUpEdit_HPF_CnslrContactLenderReason.Properties.ShowHeader = False
            Me.LookUpEdit_HPF_CnslrContactLenderReason.Properties.SortColumnIndex = 1
            Me.LookUpEdit_HPF_CnslrContactLenderReason.Properties.ValueMember = "Id"
            Me.LookUpEdit_HPF_CnslrContactLenderReason.Size = New System.Drawing.Size(147, 20)
            Me.LookUpEdit_HPF_CnslrContactLenderReason.StyleController = Me.LayoutControl1
            Me.LookUpEdit_HPF_CnslrContactLenderReason.TabIndex = 10
            '
            'txtInvestorAccountNumber
            '
            Me.txtInvestorAccountNumber.Location = New System.Drawing.Point(110, 125)
            Me.txtInvestorAccountNumber.Name = "txtInvestorAccountNumber"
            Me.txtInvestorAccountNumber.Properties.Mask.EditMask = "[a-zA-Z0-9]{0,50}"
            Me.txtInvestorAccountNumber.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.txtInvestorAccountNumber.Size = New System.Drawing.Size(159, 20)
            Me.txtInvestorAccountNumber.StyleController = Me.LayoutControl1
            Me.txtInvestorAccountNumber.TabIndex = 5
            '
            'chkCounselorContactSuccess
            '
            Me.chkCounselorContactSuccess.Location = New System.Drawing.Point(279, 96)
            Me.chkCounselorContactSuccess.Margin = New System.Windows.Forms.Padding(0)
            Me.chkCounselorContactSuccess.Name = "chkCounselorContactSuccess"
            Me.chkCounselorContactSuccess.Properties.Caption = "Successful"
            Me.chkCounselorContactSuccess.Size = New System.Drawing.Size(252, 19)
            Me.chkCounselorContactSuccess.StyleController = Me.LayoutControl1
            Me.chkCounselorContactSuccess.TabIndex = 12
            '
            'dtCurrentWorkoutPlanDate
            '
            Me.dtCurrentWorkoutPlanDate.EditValue = Nothing
            Me.dtCurrentWorkoutPlanDate.Location = New System.Drawing.Point(110, 149)
            Me.dtCurrentWorkoutPlanDate.Name = "dtCurrentWorkoutPlanDate"
            Me.dtCurrentWorkoutPlanDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.dtCurrentWorkoutPlanDate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.dtCurrentWorkoutPlanDate.Size = New System.Drawing.Size(159, 20)
            Me.dtCurrentWorkoutPlanDate.StyleController = Me.LayoutControl1
            Me.dtCurrentWorkoutPlanDate.TabIndex = 6
            '
            'LookUpEdit_HPF_ClientConcatLenderOutcome
            '
            Me.LookUpEdit_HPF_ClientConcatLenderOutcome.Location = New System.Drawing.Point(110, 222)
            Me.LookUpEdit_HPF_ClientConcatLenderOutcome.Name = "LookUpEdit_HPF_ClientConcatLenderOutcome"
            Me.LookUpEdit_HPF_ClientConcatLenderOutcome.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_HPF_ClientConcatLenderOutcome.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.LookUpEdit_HPF_ClientConcatLenderOutcome.Properties.DisplayMember = "description"
            Me.LookUpEdit_HPF_ClientConcatLenderOutcome.Properties.NullText = ""
            Me.LookUpEdit_HPF_ClientConcatLenderOutcome.Properties.PopupWidth = 350
            Me.LookUpEdit_HPF_ClientConcatLenderOutcome.Properties.ShowFooter = False
            Me.LookUpEdit_HPF_ClientConcatLenderOutcome.Properties.ShowHeader = False
            Me.LookUpEdit_HPF_ClientConcatLenderOutcome.Properties.SortColumnIndex = 1
            Me.LookUpEdit_HPF_ClientConcatLenderOutcome.Properties.ValueMember = "Id"
            Me.LookUpEdit_HPF_ClientConcatLenderOutcome.Size = New System.Drawing.Size(159, 20)
            Me.LookUpEdit_HPF_ClientConcatLenderOutcome.StyleController = Me.LayoutControl1
            Me.LookUpEdit_HPF_ClientConcatLenderOutcome.TabIndex = 8
            '
            'luInvestor
            '
            Me.luInvestor.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.luInvestor.Location = New System.Drawing.Point(110, 101)
            Me.luInvestor.Name = "luInvestor"
            Me.luInvestor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.luInvestor.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("InvestorName", "InvestorName")})
            Me.luInvestor.Properties.DisplayMember = "InvestorName"
            Me.luInvestor.Properties.DropDownRows = 6
            Me.luInvestor.Properties.MaxLength = 50
            Me.luInvestor.Properties.NullText = ""
            Me.luInvestor.Properties.PopupWidth = 350
            Me.luInvestor.Properties.ShowFooter = False
            Me.luInvestor.Properties.ShowHeader = False
            Me.luInvestor.Properties.SortColumnIndex = 1
            Me.luInvestor.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.luInvestor.Properties.ValidateOnEnterKey = True
            Me.luInvestor.Properties.ValueMember = "Id"
            Me.luInvestor.Size = New System.Drawing.Size(159, 20)
            Me.luInvestor.StyleController = Me.LayoutControl1
            Me.luInvestor.TabIndex = 4
            '
            'TextEdit_FdicNcusNum
            '
            Me.TextEdit_FdicNcusNum.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.TextEdit_FdicNcusNum.Location = New System.Drawing.Point(110, 29)
            Me.TextEdit_FdicNcusNum.Name = "TextEdit_FdicNcusNum"
            Me.TextEdit_FdicNcusNum.Properties.Mask.EditMask = "\d+"
            Me.TextEdit_FdicNcusNum.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.TextEdit_FdicNcusNum.Properties.MaxLength = 50
            Me.TextEdit_FdicNcusNum.Size = New System.Drawing.Size(159, 20)
            Me.TextEdit_FdicNcusNum.StyleController = Me.LayoutControl1
            Me.TextEdit_FdicNcusNum.TabIndex = 1
            Me.TextEdit_FdicNcusNum.ToolTip = "Optional - The loan servicer's F.D.I.C. Number"
            '
            'TextEdit_AcctNum
            '
            Me.TextEdit_AcctNum.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.TextEdit_AcctNum.Location = New System.Drawing.Point(110, 53)
            Me.TextEdit_AcctNum.Name = "TextEdit_AcctNum"
            Me.TextEdit_AcctNum.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.TextEdit_AcctNum.Properties.MaxLength = 50
            Me.TextEdit_AcctNum.Size = New System.Drawing.Size(159, 20)
            Me.TextEdit_AcctNum.StyleController = Me.LayoutControl1
            Me.TextEdit_AcctNum.TabIndex = 2
            Me.TextEdit_AcctNum.ToolTip = "The loan account number"
            '
            'TextEdit_CaseNumber
            '
            Me.TextEdit_CaseNumber.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.TextEdit_CaseNumber.Location = New System.Drawing.Point(110, 77)
            Me.TextEdit_CaseNumber.Name = "TextEdit_CaseNumber"
            Me.TextEdit_CaseNumber.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.TextEdit_CaseNumber.Properties.MaxLength = 50
            Me.TextEdit_CaseNumber.Size = New System.Drawing.Size(159, 20)
            Me.TextEdit_CaseNumber.StyleController = Me.LayoutControl1
            Me.TextEdit_CaseNumber.TabIndex = 3
            Me.TextEdit_CaseNumber.ToolTip = "The mortgage case number"
            '
            'comboBox_ServicerID
            '
            Me.comboBox_ServicerID.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.comboBox_ServicerID.Location = New System.Drawing.Point(110, 5)
            Me.comboBox_ServicerID.Name = "comboBox_ServicerID"
            Me.comboBox_ServicerID.Properties.AutoHeight = False
            Me.comboBox_ServicerID.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.comboBox_ServicerID.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.comboBox_ServicerID.Properties.MaxLength = 50
            Me.comboBox_ServicerID.Properties.PopupFormSize = New System.Drawing.Size(350, 0)
            Me.comboBox_ServicerID.Properties.PopupSizeable = True
            Me.comboBox_ServicerID.Properties.Sorted = True
            Me.comboBox_ServicerID.Size = New System.Drawing.Size(159, 20)
            Me.comboBox_ServicerID.StyleController = Me.LayoutControl1
            Me.comboBox_ServicerID.TabIndex = 0
            Me.comboBox_ServicerID.ToolTip = "Name of the loan servicer (i.e. bank)"
            '
            'dtCounselorContactDate
            '
            Me.dtCounselorContactDate.EditValue = Nothing
            Me.dtCounselorContactDate.Location = New System.Drawing.Point(384, 24)
            Me.dtCounselorContactDate.Name = "dtCounselorContactDate"
            Me.dtCounselorContactDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.dtCounselorContactDate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.dtCounselorContactDate.Size = New System.Drawing.Size(147, 20)
            Me.dtCounselorContactDate.StyleController = Me.LayoutControl1
            Me.dtCounselorContactDate.TabIndex = 9
            '
            'dtClientContactDate
            '
            Me.dtClientContactDate.EditValue = Nothing
            Me.dtClientContactDate.Location = New System.Drawing.Point(110, 198)
            Me.dtClientContactDate.Name = "dtClientContactDate"
            Me.dtClientContactDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.dtClientContactDate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.dtClientContactDate.Size = New System.Drawing.Size(159, 20)
            Me.dtClientContactDate.StyleController = Me.LayoutControl1
            Me.dtClientContactDate.TabIndex = 7
            '
            'LayoutControlGroup_All
            '
            Me.LayoutControlGroup_All.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup_All.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup_All.GroupBordersVisible = False
            Me.LayoutControlGroup_All.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup_Main, Me.LayoutControlGroup_HECM_Default, Me.EmptySpaceItem_BottomFiller, Me.LayoutControlGroup_CounselorContact, Me.LayoutControlGroup_ClientContact})
            Me.LayoutControlGroup_All.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup_All.Name = "LayoutControlGroup_All"
            Me.LayoutControlGroup_All.OptionsItemText.TextToControlDistance = 1
            Me.LayoutControlGroup_All.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
            Me.LayoutControlGroup_All.Size = New System.Drawing.Size(536, 312)
            Me.LayoutControlGroup_All.Text = "LayoutControlGroup_All"
            Me.LayoutControlGroup_All.TextVisible = False
            '
            'LayoutControlGroup_Main
            '
            Me.LayoutControlGroup_Main.CustomizationFormText = "LayoutControlGroup5"
            Me.LayoutControlGroup_Main.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem7, Me.LayoutControlItem8, Me.LayoutControlItem14, Me.LayoutControlItem1})
            Me.LayoutControlGroup_Main.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup_Main.Name = "LayoutControlGroup_Main"
            Me.LayoutControlGroup_Main.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
            Me.LayoutControlGroup_Main.Size = New System.Drawing.Size(274, 174)
            Me.LayoutControlGroup_Main.Text = "LayoutControlGroup_Main"
            Me.LayoutControlGroup_Main.TextVisible = False
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.TextEdit_FdicNcusNum
            Me.LayoutControlItem2.CustomizationFormText = "F.D.I.C. Number"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(268, 24)
            Me.LayoutControlItem2.Text = "F.D.I.C. Number"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(104, 13)
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.TextEdit_AcctNum
            Me.LayoutControlItem3.CustomizationFormText = "Account Number"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 48)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(268, 24)
            Me.LayoutControlItem3.Text = "Account Number"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(104, 13)
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.TextEdit_CaseNumber
            Me.LayoutControlItem4.CustomizationFormText = "Case Number"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 72)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(268, 24)
            Me.LayoutControlItem4.Text = "Case Number"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(104, 13)
            '
            'LayoutControlItem7
            '
            Me.LayoutControlItem7.Control = Me.luInvestor
            Me.LayoutControlItem7.CustomizationFormText = "Investor"
            Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 96)
            Me.LayoutControlItem7.Name = "LayoutControlItem7"
            Me.LayoutControlItem7.Size = New System.Drawing.Size(268, 24)
            Me.LayoutControlItem7.Text = "Investor"
            Me.LayoutControlItem7.TextSize = New System.Drawing.Size(104, 13)
            '
            'LayoutControlItem8
            '
            Me.LayoutControlItem8.Control = Me.txtInvestorAccountNumber
            Me.LayoutControlItem8.CustomizationFormText = "Investor Acct #"
            Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 120)
            Me.LayoutControlItem8.Name = "LayoutControlItem8"
            Me.LayoutControlItem8.Size = New System.Drawing.Size(268, 24)
            Me.LayoutControlItem8.Text = "Investor Acct #"
            Me.LayoutControlItem8.TextSize = New System.Drawing.Size(104, 13)
            '
            'LayoutControlItem14
            '
            Me.LayoutControlItem14.Control = Me.dtCurrentWorkoutPlanDate
            Me.LayoutControlItem14.CustomizationFormText = "Current Workout Plan"
            Me.LayoutControlItem14.Location = New System.Drawing.Point(0, 144)
            Me.LayoutControlItem14.Name = "LayoutControlItem14"
            Me.LayoutControlItem14.Size = New System.Drawing.Size(268, 24)
            Me.LayoutControlItem14.Text = "Current Workout Plan"
            Me.LayoutControlItem14.TextSize = New System.Drawing.Size(104, 13)
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.comboBox_ServicerID
            Me.LayoutControlItem1.CustomizationFormText = "Lender Name"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem1.MinSize = New System.Drawing.Size(159, 24)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(268, 24)
            Me.LayoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
            Me.LayoutControlItem1.Text = "Lender Name"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(104, 13)
            '
            'LayoutControlGroup_HECM_Default
            '
            Me.LayoutControlGroup_HECM_Default.CustomizationFormText = "H.E.C.M. Default"
            Me.LayoutControlGroup_HECM_Default.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem16, Me.LayoutControlItem17, Me.LayoutControlItem18, Me.LayoutControlItem19})
            Me.LayoutControlGroup_HECM_Default.Location = New System.Drawing.Point(274, 120)
            Me.LayoutControlGroup_HECM_Default.Name = "LayoutControlGroup_HECM_Default"
            Me.LayoutControlGroup_HECM_Default.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
            Me.LayoutControlGroup_HECM_Default.Size = New System.Drawing.Size(262, 127)
            Me.LayoutControlGroup_HECM_Default.Text = "H.E.C.M. Default"
            '
            'LayoutControlItem16
            '
            Me.LayoutControlItem16.Control = Me.CalEdit_CorpAdvance
            Me.LayoutControlItem16.CustomizationFormText = "Corp Advance"
            Me.LayoutControlItem16.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem16.Name = "LayoutControlItem16"
            Me.LayoutControlItem16.Size = New System.Drawing.Size(256, 24)
            Me.LayoutControlItem16.Text = "Corp Advance"
            Me.LayoutControlItem16.TextSize = New System.Drawing.Size(104, 13)
            '
            'LayoutControlItem17
            '
            Me.LayoutControlItem17.Control = Me.CalEdit_Monthly_Desired_Repay
            Me.LayoutControlItem17.CustomizationFormText = "Desired Mo. Repay"
            Me.LayoutControlItem17.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem17.Name = "LayoutControlItem17"
            Me.LayoutControlItem17.Size = New System.Drawing.Size(256, 24)
            Me.LayoutControlItem17.Text = "Desired Mo. Repay"
            Me.LayoutControlItem17.TextSize = New System.Drawing.Size(104, 13)
            '
            'LayoutControlItem18
            '
            Me.LayoutControlItem18.Control = Me.SpinEdit_Repay_Plan
            Me.LayoutControlItem18.CustomizationFormText = "Mos. of Repay Plan"
            Me.LayoutControlItem18.Location = New System.Drawing.Point(0, 48)
            Me.LayoutControlItem18.Name = "LayoutControlItem18"
            Me.LayoutControlItem18.Size = New System.Drawing.Size(256, 24)
            Me.LayoutControlItem18.Text = "Mos. of Repay Plan"
            Me.LayoutControlItem18.TextSize = New System.Drawing.Size(104, 13)
            '
            'LayoutControlItem19
            '
            Me.LayoutControlItem19.Control = Me.dt_LastLenderUpdated
            Me.LayoutControlItem19.CustomizationFormText = "Last Lender Updated"
            Me.LayoutControlItem19.Location = New System.Drawing.Point(0, 72)
            Me.LayoutControlItem19.Name = "LayoutControlItem19"
            Me.LayoutControlItem19.Size = New System.Drawing.Size(256, 30)
            Me.LayoutControlItem19.Text = "Last Lender Updated"
            Me.LayoutControlItem19.TextSize = New System.Drawing.Size(104, 13)
            '
            'EmptySpaceItem_BottomFiller
            '
            Me.EmptySpaceItem_BottomFiller.AllowHotTrack = False
            Me.EmptySpaceItem_BottomFiller.CustomizationFormText = "EmptySpaceItem_BottomFiller"
            Me.EmptySpaceItem_BottomFiller.ImageToTextDistance = 0
            Me.EmptySpaceItem_BottomFiller.Location = New System.Drawing.Point(0, 247)
            Me.EmptySpaceItem_BottomFiller.Name = "EmptySpaceItem1"
            Me.EmptySpaceItem_BottomFiller.ShowInCustomizationForm = False
            Me.EmptySpaceItem_BottomFiller.Size = New System.Drawing.Size(536, 65)
            Me.EmptySpaceItem_BottomFiller.Text = "EmptySpaceItem_BottomFiller"
            Me.EmptySpaceItem_BottomFiller.TextSize = New System.Drawing.Size(0, 0)
            '
            'LayoutControlGroup_CounselorContact
            '
            Me.LayoutControlGroup_CounselorContact.CustomizationFormText = "Counselor Lender Contact"
            Me.LayoutControlGroup_CounselorContact.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem15, Me.LayoutControlItem10, Me.LayoutControlItem9, Me.LayoutControlItem13})
            Me.LayoutControlGroup_CounselorContact.Location = New System.Drawing.Point(274, 0)
            Me.LayoutControlGroup_CounselorContact.Name = "LayoutControlGroup_CounselorContact"
            Me.LayoutControlGroup_CounselorContact.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
            Me.LayoutControlGroup_CounselorContact.Size = New System.Drawing.Size(262, 120)
            Me.LayoutControlGroup_CounselorContact.Text = "Counselor Lender Contact"
            '
            'LayoutControlItem15
            '
            Me.LayoutControlItem15.Control = Me.chkCounselorContactSuccess
            Me.LayoutControlItem15.CustomizationFormText = "Counselor Contact Successful"
            Me.LayoutControlItem15.Location = New System.Drawing.Point(0, 72)
            Me.LayoutControlItem15.Name = "LayoutControlItem15"
            Me.LayoutControlItem15.Size = New System.Drawing.Size(256, 23)
            Me.LayoutControlItem15.Text = "Counselor Contact Successful"
            Me.LayoutControlItem15.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem15.TextToControlDistance = 0
            Me.LayoutControlItem15.TextVisible = False
            '
            'LayoutControlItem10
            '
            Me.LayoutControlItem10.Control = Me.LookUpEdit_HPF_CnslrContactLenderOutcome
            Me.LayoutControlItem10.CustomizationFormText = "Counselor Contact Outcome"
            Me.LayoutControlItem10.Location = New System.Drawing.Point(0, 48)
            Me.LayoutControlItem10.Name = "LayoutControlItem10"
            Me.LayoutControlItem10.Size = New System.Drawing.Size(256, 24)
            Me.LayoutControlItem10.Text = "Outcome"
            Me.LayoutControlItem10.TextSize = New System.Drawing.Size(104, 13)
            '
            'LayoutControlItem9
            '
            Me.LayoutControlItem9.Control = Me.LookUpEdit_HPF_CnslrContactLenderReason
            Me.LayoutControlItem9.CustomizationFormText = "Counselor Contact Reason"
            Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem9.Name = "LayoutControlItem9"
            Me.LayoutControlItem9.Size = New System.Drawing.Size(256, 24)
            Me.LayoutControlItem9.Text = "Reason"
            Me.LayoutControlItem9.TextSize = New System.Drawing.Size(104, 13)
            '
            'LayoutControlItem13
            '
            Me.LayoutControlItem13.Control = Me.dtCounselorContactDate
            Me.LayoutControlItem13.CustomizationFormText = "Counselor Contact Date"
            Me.LayoutControlItem13.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem13.Name = "LayoutControlItem13"
            Me.LayoutControlItem13.Size = New System.Drawing.Size(256, 24)
            Me.LayoutControlItem13.Text = "Date"
            Me.LayoutControlItem13.TextSize = New System.Drawing.Size(104, 13)
            '
            'LayoutControlGroup_ClientContact
            '
            Me.LayoutControlGroup_ClientContact.CustomizationFormText = "Client Lender Contact"
            Me.LayoutControlGroup_ClientContact.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem12, Me.LayoutControlItem5})
            Me.LayoutControlGroup_ClientContact.Location = New System.Drawing.Point(0, 174)
            Me.LayoutControlGroup_ClientContact.Name = "LayoutControlGroup_ClientContact"
            Me.LayoutControlGroup_ClientContact.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
            Me.LayoutControlGroup_ClientContact.Size = New System.Drawing.Size(274, 73)
            Me.LayoutControlGroup_ClientContact.Text = "Client Lender Contact"
            '
            'LayoutControlItem12
            '
            Me.LayoutControlItem12.Control = Me.dtClientContactDate
            Me.LayoutControlItem12.CustomizationFormText = "Client Contact Date"
            Me.LayoutControlItem12.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem12.Name = "LayoutControlItem12"
            Me.LayoutControlItem12.Size = New System.Drawing.Size(268, 24)
            Me.LayoutControlItem12.Text = "Date"
            Me.LayoutControlItem12.TextSize = New System.Drawing.Size(104, 13)
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Control = Me.LookUpEdit_HPF_ClientConcatLenderOutcome
            Me.LayoutControlItem5.CustomizationFormText = "Client Contact Outcome"
            Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem5.Name = "LayoutControlItem5"
            Me.LayoutControlItem5.Size = New System.Drawing.Size(268, 24)
            Me.LayoutControlItem5.Text = "Outcome"
            Me.LayoutControlItem5.TextSize = New System.Drawing.Size(104, 13)
            '
            'LenderControl
            '
            Me.Controls.Add(Me.LayoutControl1)
            Me.Name = "LenderControl"
            Me.Size = New System.Drawing.Size(536, 312)
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.dt_LastLenderUpdated.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_LastLenderUpdated.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_HPF_CnslrContactLenderOutcome.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.SpinEdit_Repay_Plan.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalEdit_CorpAdvance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalEdit_Monthly_Desired_Repay.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_HPF_CnslrContactLenderReason.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtInvestorAccountNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.chkCounselorContactSuccess.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dtCurrentWorkoutPlanDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dtCurrentWorkoutPlanDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_HPF_ClientConcatLenderOutcome.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.luInvestor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_FdicNcusNum.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_AcctNum.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_CaseNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.comboBox_ServicerID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dtCounselorContactDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dtCounselorContactDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dtClientContactDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dtClientContactDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup_All, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup_Main, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup_HECM_Default, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem_BottomFiller, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup_CounselorContact, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup_ClientContact, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

        Protected Friend TextEdit_CaseNumber As DevExpress.XtraEditors.TextEdit
        Protected Friend LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Protected Friend TextEdit_FdicNcusNum As DevExpress.XtraEditors.TextEdit
        Protected Friend TextEdit_AcctNum As DevExpress.XtraEditors.TextEdit
        Protected Friend LayoutControlGroup_All As DevExpress.XtraLayout.LayoutControlGroup
        Protected Friend LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend txtInvestorAccountNumber As DevExpress.XtraEditors.TextEdit
        Protected Friend luInvestor As DevExpress.XtraEditors.LookUpEdit
        Protected Friend LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend dtCurrentWorkoutPlanDate As DevExpress.XtraEditors.DateEdit
        Protected Friend chkCounselorContactSuccess As DevExpress.XtraEditors.CheckEdit
        Protected Friend dtCounselorContactDate As DevExpress.XtraEditors.DateEdit
        Protected Friend dtClientContactDate As DevExpress.XtraEditors.DateEdit
        Protected Friend dt_LastLenderUpdated As DevExpress.XtraEditors.DateEdit
        Protected Friend CalEdit_CorpAdvance As DevExpress.XtraEditors.CalcEdit
        Protected Friend WithEvents SpinEdit_Repay_Plan As DevExpress.XtraEditors.SpinEdit
        Protected Friend CalEdit_Monthly_Desired_Repay As DevExpress.XtraEditors.CalcEdit
        Protected Friend comboBox_ServicerID As DevExpress.XtraEditors.ComboBoxEdit
        Protected Friend LookUpEdit_HPF_CnslrContactLenderOutcome As DevExpress.XtraEditors.LookUpEdit
        Protected Friend LookUpEdit_HPF_CnslrContactLenderReason As DevExpress.XtraEditors.LookUpEdit
        Protected Friend LookUpEdit_HPF_ClientConcatLenderOutcome As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LayoutControlGroup_ClientContact As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlGroup_Main As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlGroup_CounselorContact As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlGroup_HECM_Default As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem17 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem_BottomFiller As DevExpress.XtraLayout.EmptySpaceItem
    End Class
End Namespace