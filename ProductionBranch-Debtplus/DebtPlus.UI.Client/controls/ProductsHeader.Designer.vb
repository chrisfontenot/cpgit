﻿Namespace controls
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ProductsHeader

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then components.Dispose()
                End If
                components = Nothing
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer = Nothing

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl_vendor_label = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl_vendor_type = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl_escrow = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl_NameAndAddress = New DevExpress.XtraEditors.LabelControl()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(14, 16)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(34, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Vendor"
            '
            'LabelControl_vendor_label
            '
            Me.LabelControl_vendor_label.Appearance.Font = New System.Drawing.Font("Tahoma", 14.0!, System.Drawing.FontStyle.Underline)
            Me.LabelControl_vendor_label.Appearance.ForeColor = System.Drawing.Color.RoyalBlue
            Me.LabelControl_vendor_label.Location = New System.Drawing.Point(54, 8)
            Me.LabelControl_vendor_label.Name = "LabelControl_vendor_label"
            Me.LabelControl_vendor_label.Size = New System.Drawing.Size(0, 23)
            Me.LabelControl_vendor_label.TabIndex = 1
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(14, 45)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(24, 13)
            Me.LabelControl3.TabIndex = 2
            Me.LabelControl3.Text = "Type"
            '
            'LabelControl_vendor_type
            '
            Me.LabelControl_vendor_type.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_vendor_type.Location = New System.Drawing.Point(54, 45)
            Me.LabelControl_vendor_type.Name = "LabelControl_vendor_type"
            Me.LabelControl_vendor_type.Size = New System.Drawing.Size(155, 13)
            Me.LabelControl_vendor_type.TabIndex = 3
            '
            'LabelControl_escrow
            '
            Me.LabelControl_escrow.Location = New System.Drawing.Point(54, 64)
            Me.LabelControl_escrow.Name = "LabelControl_escrow"
            Me.LabelControl_escrow.Size = New System.Drawing.Size(15, 13)
            Me.LabelControl_escrow.TabIndex = 5
            Me.LabelControl_escrow.Text = "NO"
            '
            'LabelControl6
            '
            Me.LabelControl6.Location = New System.Drawing.Point(14, 64)
            Me.LabelControl6.Name = "LabelControl6"
            Me.LabelControl6.Size = New System.Drawing.Size(34, 13)
            Me.LabelControl6.TabIndex = 4
            Me.LabelControl6.Text = "Escrow"
            '
            'LabelControl_NameAndAddress
            '
            Me.LabelControl_NameAndAddress.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl_NameAndAddress.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LabelControl_NameAndAddress.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LabelControl_NameAndAddress.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_NameAndAddress.Location = New System.Drawing.Point(215, 14)
            Me.LabelControl_NameAndAddress.Name = "LabelControl_NameAndAddress"
            Me.LabelControl_NameAndAddress.Size = New System.Drawing.Size(226, 96)
            Me.LabelControl_NameAndAddress.TabIndex = 6
            '
            'ProductsHeader
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.LabelControl_NameAndAddress)
            Me.Controls.Add(Me.LabelControl_escrow)
            Me.Controls.Add(Me.LabelControl6)
            Me.Controls.Add(Me.LabelControl_vendor_type)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.LabelControl_vendor_label)
            Me.Controls.Add(Me.LabelControl1)
            Me.Name = "ProductsHeader"
            Me.Size = New System.Drawing.Size(457, 126)
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl_vendor_label As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl_vendor_type As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl_escrow As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl_NameAndAddress As DevExpress.XtraEditors.LabelControl

    End Class
End Namespace
