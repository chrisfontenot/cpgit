﻿Namespace controls
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class LoanDSADetailsControl
        Inherits DebtPlus.Data.Controls.UserControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then
                        components.Dispose()
                    End If

                    yesNoOptionsList.Clear()
                    noDsaProgramList.Clear()
                    DsaProgramList.Clear()
                    clientsituationlist.Clear()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
            Me.txtDsaCaseID = New DevExpress.XtraEditors.TextEdit()
            Me.luDsaSpecialist = New DevExpress.XtraEditors.LookUpEdit()
            Me.luClientBelieveSituation = New DevExpress.XtraEditors.LookUpEdit()
            Me.luDsaNoProgramReason = New DevExpress.XtraEditors.LookUpEdit()
            Me.luDsaProgram = New DevExpress.XtraEditors.LookUpEdit()
            Me.luDsaHousingProgramBenefit = New DevExpress.XtraEditors.LookUpEdit()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.txtDsaCaseID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.luDsaSpecialist.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.luClientBelieveSituation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.luDsaNoProgramReason.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.luDsaProgram.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.luDsaHousingProgramBenefit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.LabelControl1)
            Me.LayoutControl1.Controls.Add(Me.txtDsaCaseID)
            Me.LayoutControl1.Controls.Add(Me.luDsaSpecialist)
            Me.LayoutControl1.Controls.Add(Me.luClientBelieveSituation)
            Me.LayoutControl1.Controls.Add(Me.luDsaNoProgramReason)
            Me.LayoutControl1.Controls.Add(Me.luDsaProgram)
            Me.LayoutControl1.Controls.Add(Me.luDsaHousingProgramBenefit)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(386, 240)
            Me.LayoutControl1.TabIndex = 0
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'LabelControl1
            '
            Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl1.Location = New System.Drawing.Point(12, 12)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(225, 13)
            Me.LabelControl1.StyleController = Me.LayoutControl1
            Me.LabelControl1.TabIndex = 33
            Me.LabelControl1.Text = "Document Submission Assistance (DSA)"
            '
            'txtDsaCaseID
            '
            Me.txtDsaCaseID.Location = New System.Drawing.Point(183, 149)
            Me.txtDsaCaseID.Name = "txtDsaCaseID"
            Me.txtDsaCaseID.Properties.ReadOnly = True
            Me.txtDsaCaseID.Size = New System.Drawing.Size(191, 20)
            Me.txtDsaCaseID.StyleController = Me.LayoutControl1
            Me.txtDsaCaseID.TabIndex = 45
            '
            'luDsaSpecialist
            '
            Me.luDsaSpecialist.Location = New System.Drawing.Point(183, 125)
            Me.luDsaSpecialist.Name = "luDsaSpecialist"
            Me.luDsaSpecialist.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.luDsaSpecialist.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("CounselorID", "CounselorID", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Counselor", "Counselor")})
            Me.luDsaSpecialist.Properties.DisplayMember = "Counselor"
            Me.luDsaSpecialist.Properties.NullText = ""
            Me.luDsaSpecialist.Properties.ReadOnly = True
            Me.luDsaSpecialist.Properties.ShowFooter = False
            Me.luDsaSpecialist.Properties.ShowHeader = False
            Me.luDsaSpecialist.Properties.ValueMember = "CounselorID"
            Me.luDsaSpecialist.Size = New System.Drawing.Size(191, 20)
            Me.luDsaSpecialist.StyleController = Me.LayoutControl1
            Me.luDsaSpecialist.TabIndex = 46
            '
            'luClientBelieveSituation
            '
            Me.luClientBelieveSituation.Location = New System.Drawing.Point(183, 101)
            Me.luClientBelieveSituation.Name = "luClientBelieveSituation"
            Me.luClientBelieveSituation.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.luClientBelieveSituation.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "description"), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("sortorder", "sortorder", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default])})
            Me.luClientBelieveSituation.Properties.DisplayMember = "description"
            Me.luClientBelieveSituation.Properties.DropDownRows = 4
            Me.luClientBelieveSituation.Properties.NullText = ""
            Me.luClientBelieveSituation.Properties.ShowFooter = False
            Me.luClientBelieveSituation.Properties.ShowHeader = False
            Me.luClientBelieveSituation.Properties.ValueMember = "oID"
            Me.luClientBelieveSituation.Size = New System.Drawing.Size(191, 20)
            Me.luClientBelieveSituation.StyleController = Me.LayoutControl1
            Me.luClientBelieveSituation.TabIndex = 47
            '
            'luDsaNoProgramReason
            '
            Me.luDsaNoProgramReason.Location = New System.Drawing.Point(183, 53)
            Me.luDsaNoProgramReason.Name = "luDsaNoProgramReason"
            Me.luDsaNoProgramReason.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.luDsaNoProgramReason.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "description")})
            Me.luDsaNoProgramReason.Properties.DisplayMember = "description"
            Me.luDsaNoProgramReason.Properties.NullText = ""
            Me.luDsaNoProgramReason.Properties.ShowFooter = False
            Me.luDsaNoProgramReason.Properties.ShowHeader = False
            Me.luDsaNoProgramReason.Properties.ValueMember = "Id"
            Me.luDsaNoProgramReason.Size = New System.Drawing.Size(191, 20)
            Me.luDsaNoProgramReason.StyleController = Me.LayoutControl1
            Me.luDsaNoProgramReason.TabIndex = 44
            '
            'luDsaProgram
            '
            Me.luDsaProgram.Location = New System.Drawing.Point(183, 77)
            Me.luDsaProgram.Name = "luDsaProgram"
            Me.luDsaProgram.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.luDsaProgram.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Attribute", "Attribute")})
            Me.luDsaProgram.Properties.DisplayMember = "Attribute"
            Me.luDsaProgram.Properties.NullText = ""
            Me.luDsaProgram.Properties.ShowFooter = False
            Me.luDsaProgram.Properties.ShowHeader = False
            Me.luDsaProgram.Properties.ValueMember = "Id"
            Me.luDsaProgram.Size = New System.Drawing.Size(191, 20)
            Me.luDsaProgram.StyleController = Me.LayoutControl1
            Me.luDsaProgram.TabIndex = 43
            '
            'luDsaHousingProgramBenefit
            '
            Me.luDsaHousingProgramBenefit.Location = New System.Drawing.Point(183, 29)
            Me.luDsaHousingProgramBenefit.Name = "luDsaHousingProgramBenefit"
            Me.luDsaHousingProgramBenefit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.luDsaHousingProgramBenefit.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "description"), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default])})
            Me.luDsaHousingProgramBenefit.Properties.DisplayMember = "description"
            Me.luDsaHousingProgramBenefit.Properties.DropDownRows = 3
            Me.luDsaHousingProgramBenefit.Properties.NullText = ""
            Me.luDsaHousingProgramBenefit.Properties.ShowFooter = False
            Me.luDsaHousingProgramBenefit.Properties.ShowHeader = False
            Me.luDsaHousingProgramBenefit.Properties.ValueMember = "Id"
            Me.luDsaHousingProgramBenefit.Size = New System.Drawing.Size(191, 20)
            Me.luDsaHousingProgramBenefit.StyleController = Me.LayoutControl1
            Me.luDsaHousingProgramBenefit.TabIndex = 42
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup1.GroupBordersVisible = False
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.LayoutControlItem6, Me.LayoutControlItem7})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(386, 240)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.luDsaHousingProgramBenefit
            Me.LayoutControlItem1.CustomizationFormText = "DSA Program Benefit"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 17)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(366, 24)
            Me.LayoutControlItem1.Text = "DSA Program Benefit"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(168, 13)
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.luDsaNoProgramReason
            Me.LayoutControlItem2.CustomizationFormText = "Reason for No DSA Benefit"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 41)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(366, 24)
            Me.LayoutControlItem2.Text = "Reason for No DSA Benefit"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(168, 13)
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.luDsaProgram
            Me.LayoutControlItem3.CustomizationFormText = "DSA Program"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 65)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(366, 24)
            Me.LayoutControlItem3.Text = "DSA Program"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(168, 13)
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.luClientBelieveSituation
            Me.LayoutControlItem4.CustomizationFormText = "Situation is"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 89)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(366, 24)
            Me.LayoutControlItem4.Text = "Situation is"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(168, 13)
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Control = Me.luDsaSpecialist
            Me.LayoutControlItem5.CustomizationFormText = "DSA Specialist Name and Extension"
            Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 113)
            Me.LayoutControlItem5.Name = "LayoutControlItem5"
            Me.LayoutControlItem5.Size = New System.Drawing.Size(366, 24)
            Me.LayoutControlItem5.Text = "DSA Specialist Name and Extension"
            Me.LayoutControlItem5.TextSize = New System.Drawing.Size(168, 13)
            '
            'LayoutControlItem6
            '
            Me.LayoutControlItem6.Control = Me.txtDsaCaseID
            Me.LayoutControlItem6.CustomizationFormText = "DSA Case ID"
            Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 137)
            Me.LayoutControlItem6.Name = "LayoutControlItem6"
            Me.LayoutControlItem6.Size = New System.Drawing.Size(366, 83)
            Me.LayoutControlItem6.Text = "DSA Case ID"
            Me.LayoutControlItem6.TextSize = New System.Drawing.Size(168, 13)
            '
            'LayoutControlItem7
            '
            Me.LayoutControlItem7.Control = Me.LabelControl1
            Me.LayoutControlItem7.CustomizationFormText = "LayoutControlItem7"
            Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem7.Name = "LayoutControlItem7"
            Me.LayoutControlItem7.Size = New System.Drawing.Size(366, 17)
            Me.LayoutControlItem7.Text = "LayoutControlItem7"
            Me.LayoutControlItem7.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem7.TextToControlDistance = 0
            Me.LayoutControlItem7.TextVisible = False
            '
            'LoanDSADetailsControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.LayoutControl1)
            Me.Name = "LoanDSADetailsControl"
            Me.Size = New System.Drawing.Size(386, 240)
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.txtDsaCaseID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.luDsaSpecialist.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.luClientBelieveSituation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.luDsaNoProgramReason.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.luDsaProgram.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.luDsaHousingProgramBenefit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents txtDsaCaseID As DevExpress.XtraEditors.TextEdit
        Friend WithEvents luDsaSpecialist As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents luClientBelieveSituation As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents luDsaNoProgramReason As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents luDsaProgram As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents luDsaHousingProgramBenefit As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem

    End Class
End Namespace