﻿Namespace controls
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ProductsTransacitons

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then components.Dispose()
                End If
                components = Nothing
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer = Nothing

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.ProductsTransactionGrid1 = New DebtPlus.UI.Client.controls.ProductsTransactionGrid()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
            Me.LookUpEdit_DateRange = New DevExpress.XtraEditors.LookUpEdit()
            CType(Me.ProductsTransactionGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_DateRange.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ProductsTransactionGrid1
            '
            Me.ProductsTransactionGrid1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ProductsTransactionGrid1.Location = New System.Drawing.Point(0, 0)
            Me.ProductsTransactionGrid1.Margin = New System.Windows.Forms.Padding(0)
            Me.ProductsTransactionGrid1.Name = "ProductsTransactionGrid1"
            Me.ProductsTransactionGrid1.Size = New System.Drawing.Size(553, 127)
            Me.ProductsTransactionGrid1.TabIndex = 0
            '
            'LabelControl1
            '
            Me.LabelControl1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl1.Location = New System.Drawing.Point(311, 133)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(57, 13)
            Me.LabelControl1.TabIndex = 1
            Me.LabelControl1.Text = "Date Range"
            '
            'LookUpEdit_DateRange
            '
            Me.LookUpEdit_DateRange.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LookUpEdit_DateRange.Location = New System.Drawing.Point(374, 130)
            Me.LookUpEdit_DateRange.Name = "LookUpEdit_DateRange"
            Me.LookUpEdit_DateRange.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_DateRange.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.LookUpEdit_DateRange.Properties.DisplayMember = "description"
            Me.LookUpEdit_DateRange.Properties.NullText = ""
            Me.LookUpEdit_DateRange.Properties.ShowFooter = False
            Me.LookUpEdit_DateRange.Properties.ShowHeader = False
            Me.LookUpEdit_DateRange.Properties.ValueMember = "Id"
            Me.LookUpEdit_DateRange.Size = New System.Drawing.Size(179, 20)
            Me.LookUpEdit_DateRange.TabIndex = 2
            '
            'ProductsTransacitons
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.LookUpEdit_DateRange)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.ProductsTransactionGrid1)
            Me.Name = "ProductsTransacitons"
            Me.Size = New System.Drawing.Size(556, 150)
            CType(Me.ProductsTransactionGrid1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_DateRange.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

        Private WithEvents ProductsTransactionGrid1 As DebtPlus.UI.Client.controls.ProductsTransactionGrid
        Private LabelControl1 As DevExpress.XtraEditors.LabelControl
        Private WithEvents LookUpEdit_DateRange As DevExpress.XtraEditors.LookUpEdit
    End Class
End Namespace
