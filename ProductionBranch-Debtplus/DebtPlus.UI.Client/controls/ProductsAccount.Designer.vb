﻿Namespace controls
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ProductsAccount

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then components.Dispose()
                End If
                components = Nothing
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer = Nothing

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.ProductsBalance1 = New DebtPlus.UI.Client.controls.ProductsBalance()
            Me.ProductsDispute1 = New DebtPlus.UI.Client.controls.ProductsDispute()
            Me.TextEdit_Message = New DevExpress.XtraEditors.TextEdit()
            Me.TextEdit_Reference = New DevExpress.XtraEditors.TextEdit()
            Me.LabelControl_LastPaymentAmt = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl_LastPaymentDate = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl_InvoiceDate = New DevExpress.XtraEditors.LabelControl()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.TextEdit_Message.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_Reference.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.ProductsBalance1)
            Me.LayoutControl1.Controls.Add(Me.ProductsDispute1)
            Me.LayoutControl1.Controls.Add(Me.TextEdit_Message)
            Me.LayoutControl1.Controls.Add(Me.TextEdit_Reference)
            Me.LayoutControl1.Controls.Add(Me.LabelControl_LastPaymentAmt)
            Me.LayoutControl1.Controls.Add(Me.LabelControl_LastPaymentDate)
            Me.LayoutControl1.Controls.Add(Me.LabelControl_InvoiceDate)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Margin = New System.Windows.Forms.Padding(0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(538, 238)
            Me.LayoutControl1.TabIndex = 0
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'ProductsBalance1
            '
            Me.ProductsBalance1.Location = New System.Drawing.Point(4, 103)
            Me.ProductsBalance1.Name = "ProductsBalance1"
            Me.ProductsBalance1.Size = New System.Drawing.Size(288, 131)
            Me.ProductsBalance1.TabIndex = 15
            '
            'ProductsDispute1
            '
            Me.ProductsDispute1.Location = New System.Drawing.Point(296, 4)
            Me.ProductsDispute1.Name = "ProductsDispute1"
            Me.ProductsDispute1.Size = New System.Drawing.Size(238, 230)
            Me.ProductsDispute1.TabIndex = 14
            '
            'TextEdit_Message
            '
            Me.TextEdit_Message.EditValue = ""
            Me.TextEdit_Message.Location = New System.Drawing.Point(112, 79)
            Me.TextEdit_Message.MinimumSize = New System.Drawing.Size(180, 20)
            Me.TextEdit_Message.Name = "TextEdit_Message"
            Me.TextEdit_Message.Size = New System.Drawing.Size(180, 20)
            Me.TextEdit_Message.StyleController = Me.LayoutControl1
            Me.TextEdit_Message.TabIndex = 11
            '
            'TextEdit_Reference
            '
            Me.TextEdit_Reference.EditValue = ""
            Me.TextEdit_Reference.Location = New System.Drawing.Point(112, 55)
            Me.TextEdit_Reference.MinimumSize = New System.Drawing.Size(180, 20)
            Me.TextEdit_Reference.Name = "TextEdit_Reference"
            Me.TextEdit_Reference.Size = New System.Drawing.Size(180, 20)
            Me.TextEdit_Reference.StyleController = Me.LayoutControl1
            Me.TextEdit_Reference.TabIndex = 10
            '
            'LabelControl_LastPaymentAmt
            '
            Me.LabelControl_LastPaymentAmt.Location = New System.Drawing.Point(112, 38)
            Me.LabelControl_LastPaymentAmt.Name = "LabelControl_LastPaymentAmt"
            Me.LabelControl_LastPaymentAmt.Size = New System.Drawing.Size(180, 13)
            Me.LabelControl_LastPaymentAmt.StyleController = Me.LayoutControl1
            Me.LabelControl_LastPaymentAmt.TabIndex = 9
            '
            'LabelControl_LastPaymentDate
            '
            Me.LabelControl_LastPaymentDate.Location = New System.Drawing.Point(112, 21)
            Me.LabelControl_LastPaymentDate.Name = "LabelControl_LastPaymentDate"
            Me.LabelControl_LastPaymentDate.Size = New System.Drawing.Size(180, 13)
            Me.LabelControl_LastPaymentDate.StyleController = Me.LayoutControl1
            Me.LabelControl_LastPaymentDate.TabIndex = 7
            '
            'LabelControl_InvoiceDate
            '
            Me.LabelControl_InvoiceDate.Location = New System.Drawing.Point(112, 4)
            Me.LabelControl_InvoiceDate.Name = "LabelControl_InvoiceDate"
            Me.LabelControl_InvoiceDate.Size = New System.Drawing.Size(180, 13)
            Me.LabelControl_InvoiceDate.StyleController = Me.LayoutControl1
            Me.LabelControl_InvoiceDate.TabIndex = 5
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup1.GroupBordersVisible = False
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem2, Me.LayoutControlItem4, Me.LayoutControlItem6, Me.LayoutControlItem7, Me.LayoutControlItem8, Me.LayoutControlItem1, Me.LayoutControlItem3})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Padding = New DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2)
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(538, 238)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.LabelControl_InvoiceDate
            Me.LayoutControlItem2.CustomizationFormText = "Last Invoice Date"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(292, 17)
            Me.LayoutControlItem2.Text = "Last Invoice Date"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(105, 13)
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.LabelControl_LastPaymentDate
            Me.LayoutControlItem4.CustomizationFormText = "Last Payment Date"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 17)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(292, 17)
            Me.LayoutControlItem4.Text = "Last Payment Date"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(105, 13)
            '
            'LayoutControlItem6
            '
            Me.LayoutControlItem6.Control = Me.LabelControl_LastPaymentAmt
            Me.LayoutControlItem6.CustomizationFormText = "Last Payment Amount"
            Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 34)
            Me.LayoutControlItem6.Name = "LayoutControlItem6"
            Me.LayoutControlItem6.Size = New System.Drawing.Size(292, 17)
            Me.LayoutControlItem6.Text = "Last Payment Amount"
            Me.LayoutControlItem6.TextSize = New System.Drawing.Size(105, 13)
            '
            'LayoutControlItem7
            '
            Me.LayoutControlItem7.Control = Me.TextEdit_Reference
            Me.LayoutControlItem7.CustomizationFormText = "Reference Number"
            Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 51)
            Me.LayoutControlItem7.Name = "LayoutControlItem7"
            Me.LayoutControlItem7.Size = New System.Drawing.Size(292, 24)
            Me.LayoutControlItem7.Text = "Reference Number"
            Me.LayoutControlItem7.TextSize = New System.Drawing.Size(105, 13)
            '
            'LayoutControlItem8
            '
            Me.LayoutControlItem8.Control = Me.TextEdit_Message
            Me.LayoutControlItem8.CustomizationFormText = "Message"
            Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 75)
            Me.LayoutControlItem8.Name = "LayoutControlItem8"
            Me.LayoutControlItem8.Size = New System.Drawing.Size(292, 24)
            Me.LayoutControlItem8.Text = "Message"
            Me.LayoutControlItem8.TextSize = New System.Drawing.Size(105, 13)
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.ProductsDispute1
            Me.LayoutControlItem1.CustomizationFormText = "LayoutControlItem1"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(292, 0)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(242, 234)
            Me.LayoutControlItem1.Text = "LayoutControlItem1"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem1.TextToControlDistance = 0
            Me.LayoutControlItem1.TextVisible = False
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.ProductsBalance1
            Me.LayoutControlItem3.CustomizationFormText = "LayoutControlItem3"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 99)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(292, 135)
            Me.LayoutControlItem3.Text = "LayoutControlItem3"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem3.TextToControlDistance = 0
            Me.LayoutControlItem3.TextVisible = False
            '
            'ProductsAccount
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.LayoutControl1)
            Me.Name = "ProductsAccount"
            Me.Size = New System.Drawing.Size(538, 238)
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.TextEdit_Message.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_Reference.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents TextEdit_Message As DevExpress.XtraEditors.TextEdit
        Friend WithEvents TextEdit_Reference As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LabelControl_LastPaymentAmt As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl_LastPaymentDate As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl_InvoiceDate As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents ProductsDispute1 As DebtPlus.UI.Client.controls.ProductsDispute
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents ProductsBalance1 As DebtPlus.UI.Client.controls.ProductsBalance
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem

    End Class
End Namespace
