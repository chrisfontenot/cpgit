﻿Namespace controls
    Friend Class DisclosureLanguageControl
        Implements DevExpress.Utils.Controls.IXtraResizableControl

        Public Event EditValueChanging As DevExpress.XtraEditors.Controls.ChangingEventHandler
        Public Event EditValueChanged As System.EventHandler

        ''' <summary>
        ''' Default language ID for the client.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property DefaultLanguageID As System.Nullable(Of Int32) = Nothing

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            Dim isInDesignMode As Boolean = System.ComponentModel.LicenseManager.UsageMode = System.ComponentModel.LicenseUsageMode.Designtime ' OrElse Debugger.IsAttached = True
            If Not isInDesignMode Then
                RegisterHandlers()
            End If
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf DisclosureLanguageControl_Load
            AddHandler LookUpEdit_DisclosureType.EditValueChanged, AddressOf LookUpEdit_DisclosureType_EditValueChanged
            AddHandler LookUpEdit_Language.EditValueChanging, AddressOf LookUpEdit_Language_EditValueChanging
            AddHandler LookUpEdit_Language.EditValueChanged, AddressOf LookUpEdit_Language_EditValueChanged
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler Load, AddressOf DisclosureLanguageControl_Load
            RemoveHandler LookUpEdit_DisclosureType.EditValueChanged, AddressOf LookUpEdit_DisclosureType_EditValueChanged
            RemoveHandler LookUpEdit_Language.EditValueChanging, AddressOf LookUpEdit_Language_EditValueChanging
            RemoveHandler LookUpEdit_Language.EditValueChanged, AddressOf LookUpEdit_Language_EditValueChanged
        End Sub

        Protected Sub RaiseEditValueChanging(ByVal e As DevExpress.XtraEditors.Controls.ChangingEventArgs)
            RaiseEvent EditValueChanging(Me, e)
        End Sub

        Protected Overridable Sub OnEditValueChanging(ByVal e As DevExpress.XtraEditors.Controls.ChangingEventArgs)
            RaiseEditValueChanging(e)
        End Sub

        Protected Sub RaiseEditValueChanged(ByVal e As System.EventArgs)
            RaiseEvent EditValueChanged(Me, e)
        End Sub

        Protected Overridable Sub OnEditValueChanged(ByVal e As System.EventArgs)
            RaiseEditValueChanged(e)
        End Sub

        Public Property EditValue As System.Nullable(Of Int32)
            Get
                Return DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_Language.EditValue)
            End Get
            Set(value As System.Nullable(Of Int32))
                LookUpEdit_Language.EditValue = value
            End Set
        End Property

        Public Property [ReadOnly] As Boolean
            Get
                Return LookUpEdit_DisclosureType.Properties.[ReadOnly]
            End Get
            Set(value As Boolean)
                If value <> LookUpEdit_DisclosureType.Properties.[ReadOnly] Then
                    LookUpEdit_DisclosureType.Properties.[ReadOnly] = value
                    LookUpEdit_Language.Properties.[ReadOnly] = value
                End If
            End Set
        End Property

        Private Sub DisclosureLanguageControl_Load(sender As Object, e As System.EventArgs)
            UnRegisterHandlers()
            Try
                LookUpEdit_DisclosureType.Properties.DataSource = DebtPlus.LINQ.Cache.client_DisclosureType.getList()
                LoadLanguages()

                ' Try to set the disclosure entry to English if possible.
                Dim colItems As System.Collections.Generic.List(Of DebtPlus.LINQ.client_DisclosureLanguageType) = TryCast(LookUpEdit_Language.Properties.DataSource, System.Collections.Generic.List(Of DebtPlus.LINQ.client_DisclosureLanguageType))
                If colItems IsNot Nothing AndAlso colItems.Count > 0 Then
                    For Each lngItem As DebtPlus.LINQ.client_DisclosureLanguageType In colItems
                        If lngItem.languageID = 1 Then
                            LookUpEdit_Language.EditValue = lngItem.Id
                            Exit For
                        End If
                    Next
                End If

            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Sub LookUpEdit_DisclosureType_EditValueChanged(sender As Object, e As System.EventArgs)
            LoadLanguages()

            ' Try to set the disclosure entry to English if possible.
            Dim colItems As System.Collections.Generic.List(Of DebtPlus.LINQ.client_DisclosureLanguageType) = TryCast(LookUpEdit_Language.Properties.DataSource, System.Collections.Generic.List(Of DebtPlus.LINQ.client_DisclosureLanguageType))
            If colItems IsNot Nothing AndAlso colItems.Count > 0 Then
                For Each lngItem As DebtPlus.LINQ.client_DisclosureLanguageType In colItems
                    If lngItem.languageID = 1 Then
                        LookUpEdit_Language.EditValue = lngItem.Id
                        Exit For
                    End If
                Next
            End If
        End Sub

        Private Sub LookUpEdit_Language_EditValueChanged(sender As Object, e As System.EventArgs)
            OnEditValueChanged(e)
        End Sub

        Private Sub LookUpEdit_Language_EditValueChanging(sender As Object, e As DevExpress.XtraEditors.Controls.ChangingEventArgs)
            Dim newEvt As New DevExpress.XtraEditors.Controls.ChangingEventArgs(e.OldValue, e.NewValue)
            OnEditValueChanging(newEvt)
            e.Cancel = newEvt.Cancel
        End Sub

        ''' <summary>
        ''' Return the selected record for the corresponding language entry
        ''' </summary>
        Public ReadOnly Property DisclosureLanguageRecord As DebtPlus.LINQ.client_DisclosureLanguageType
            Get
                Return TryCast(LookUpEdit_Language.GetSelectedDataRow(), DebtPlus.LINQ.client_DisclosureLanguageType)
            End Get
        End Property

        Private Sub LoadLanguages()

            ' Populate the language list with the correct values
            If LookUpEdit_DisclosureType.EditValue IsNot Nothing Then
                Dim disclosureRecord As DebtPlus.LINQ.client_DisclosureType = TryCast(LookUpEdit_DisclosureType.GetSelectedDataRow(), DebtPlus.LINQ.client_DisclosureType)
                If disclosureRecord IsNot Nothing Then

                    ' If we are allowed to choose any item then just load the entire list.
                    If [ReadOnly] Then
                        LookUpEdit_Language.Properties.DataSource = disclosureRecord.client_DisclosureLanguageTypes.GetNewBindingList()
                        Return
                    End If

                    ' Otherwise, we need to filter the list to show only the items that are suitable.
                    Dim colItems As New System.Collections.Generic.List(Of DebtPlus.LINQ.client_DisclosureLanguageType)()
                    Dim today As DateTime = System.DateTime.Now.Date
                    Dim tomorrow As DateTime = today.AddDays(1)

                    For Each languageItem As DebtPlus.LINQ.client_DisclosureLanguageType In disclosureRecord.client_DisclosureLanguageTypes

                        ' If the item is not effective now then honor the future date
                        If languageItem.effective_date.Date >= tomorrow Then
                            Continue For
                        End If

                        ' If the item has expired then honor the expiration date for the disclosure
                        If languageItem.expiration_date.HasValue AndAlso languageItem.expiration_date.Value <= today Then
                            Continue For
                        End If

                        colItems.Add(languageItem)
                    Next

                    ' Set the source for the items to point to the appropriate language records.
                    LookUpEdit_Language.Properties.DataSource = colItems
                    Return
                End If
            End If

            ' Clear the language list
            LookUpEdit_Language.Properties.DataSource = Nothing
        End Sub

        Public Event Changed(sender As Object, e As System.EventArgs) Implements DevExpress.Utils.Controls.IXtraResizableControl.Changed

        Public ReadOnly Property IsCaptionVisible As Boolean Implements DevExpress.Utils.Controls.IXtraResizableControl.IsCaptionVisible
            Get
                Return False
            End Get
        End Property

        Public ReadOnly Property MaxSize As System.Drawing.Size Implements DevExpress.Utils.Controls.IXtraResizableControl.MaxSize
            Get
                Return New System.Drawing.Size(1000, 52)
            End Get
        End Property

        Public ReadOnly Property MinSize As System.Drawing.Size Implements DevExpress.Utils.Controls.IXtraResizableControl.MinSize
            Get
                Return New System.Drawing.Size(10, 52)
            End Get
        End Property
    End Class
End Namespace
