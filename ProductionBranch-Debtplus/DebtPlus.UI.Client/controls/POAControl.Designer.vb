﻿Namespace controls
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class POAControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer = Nothing

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.PopupContainerControl_POA_Approval = New DevExpress.XtraEditors.PopupContainerControl()
            Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
            Me.LookUpEdit_POA_Stage = New DevExpress.XtraEditors.LookUpEdit()
            Me.LookUpEdit_POA_Other = New DevExpress.XtraEditors.LookUpEdit()
            Me.LookUpEdit_POA_Fin = New DevExpress.XtraEditors.LookUpEdit()
            Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
            Me.LookUpEdit_RealEstate = New DevExpress.XtraEditors.LookUpEdit()
            Me.PopupContainerControl_POA_Phone = New DevExpress.XtraEditors.PopupContainerControl()
            Me.LookUpEdit_POA_PhoneType = New DevExpress.XtraEditors.LookUpEdit()
            Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
            Me.POA_Telephone_RecordControl = New DebtPlus.Data.Controls.TelephoneNumberRecordControl()
            Me.PopupContainerControl_POA_ID = New DevExpress.XtraEditors.PopupContainerControl()
            Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
            Me.dt_POA_ID_Expires = New DevExpress.XtraEditors.DateEdit()
            Me.dt_POA_ID_Issued = New DevExpress.XtraEditors.DateEdit()
            Me.textEdit_POA_ID = New DevExpress.XtraEditors.TextEdit()
            Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
            Me.LookUpEdit_POA_Type = New DevExpress.XtraEditors.LookUpEdit()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.PopupContainerEdit_POA_Approval = New DevExpress.XtraEditors.PopupContainerEdit()
            Me.PopupContainerEdit_POA_Phone = New DevExpress.XtraEditors.PopupContainerEdit()
            Me.PopupContainerEdit_POA_ID = New DevExpress.XtraEditors.PopupContainerEdit()
            Me.dt_POA_Expires = New DevExpress.XtraEditors.DateEdit()
            Me.NameRecordControl_POA = New DebtPlus.Data.Controls.NameRecordControl()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.ApprovalBy = New DevExpress.XtraEditors.LabelControl()
            CType(Me.PopupContainerControl_POA_Approval, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.PopupContainerControl_POA_Approval.SuspendLayout()
            CType(Me.LookUpEdit_POA_Stage.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_POA_Other.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_POA_Fin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_RealEstate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PopupContainerControl_POA_Phone, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.PopupContainerControl_POA_Phone.SuspendLayout()
            CType(Me.LookUpEdit_POA_PhoneType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.POA_Telephone_RecordControl, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PopupContainerControl_POA_ID, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.PopupContainerControl_POA_ID.SuspendLayout()
            CType(Me.dt_POA_ID_Expires.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_POA_ID_Expires.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_POA_ID_Issued.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_POA_ID_Issued.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.textEdit_POA_ID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_POA_Type.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.PopupContainerEdit_POA_Approval.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PopupContainerEdit_POA_Phone.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PopupContainerEdit_POA_ID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_POA_Expires.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_POA_Expires.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'PopupContainerControl_POA_Approval
            '
            Me.PopupContainerControl_POA_Approval.Controls.Add(Me.ApprovalBy)
            Me.PopupContainerControl_POA_Approval.Controls.Add(Me.LabelControl18)
            Me.PopupContainerControl_POA_Approval.Controls.Add(Me.LabelControl17)
            Me.PopupContainerControl_POA_Approval.Controls.Add(Me.LabelControl16)
            Me.PopupContainerControl_POA_Approval.Controls.Add(Me.LookUpEdit_POA_Stage)
            Me.PopupContainerControl_POA_Approval.Controls.Add(Me.LookUpEdit_POA_Other)
            Me.PopupContainerControl_POA_Approval.Controls.Add(Me.LookUpEdit_POA_Fin)
            Me.PopupContainerControl_POA_Approval.Controls.Add(Me.LabelControl15)
            Me.PopupContainerControl_POA_Approval.Controls.Add(Me.LabelControl14)
            Me.PopupContainerControl_POA_Approval.Controls.Add(Me.LookUpEdit_RealEstate)
            Me.PopupContainerControl_POA_Approval.Location = New System.Drawing.Point(6, 158)
            Me.PopupContainerControl_POA_Approval.Name = "PopupContainerControl_POA_Approval"
            Me.PopupContainerControl_POA_Approval.Size = New System.Drawing.Size(243, 158)
            Me.PopupContainerControl_POA_Approval.TabIndex = 0
            '
            'LabelControl18
            '
            Me.LabelControl18.Location = New System.Drawing.Point(11, 124)
            Me.LabelControl18.Name = "LabelControl18"
            Me.LabelControl18.Size = New System.Drawing.Size(12, 13)
            Me.LabelControl18.TabIndex = 8
            Me.LabelControl18.Text = "By"
            '
            'LabelControl17
            '
            Me.LabelControl17.Location = New System.Drawing.Point(11, 97)
            Me.LabelControl17.Name = "LabelControl17"
            Me.LabelControl17.Size = New System.Drawing.Size(28, 13)
            Me.LabelControl17.TabIndex = 6
            Me.LabelControl17.Text = "Stage"
            '
            'LabelControl16
            '
            Me.LabelControl16.Location = New System.Drawing.Point(11, 69)
            Me.LabelControl16.Name = "LabelControl16"
            Me.LabelControl16.Size = New System.Drawing.Size(37, 13)
            Me.LabelControl16.TabIndex = 4
            Me.LabelControl16.Text = "Other *"
            '
            'LookUpEdit_POA_Stage
            '
            Me.LookUpEdit_POA_Stage.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LookUpEdit_POA_Stage.Location = New System.Drawing.Point(92, 91)
            Me.LookUpEdit_POA_Stage.Name = "LookUpEdit_POA_Stage"
            Me.LookUpEdit_POA_Stage.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_POA_Stage.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description")})
            Me.LookUpEdit_POA_Stage.Properties.DisplayMember = "description"
            Me.LookUpEdit_POA_Stage.Properties.NullText = ""
            Me.LookUpEdit_POA_Stage.Properties.ShowFooter = False
            Me.LookUpEdit_POA_Stage.Properties.ShowHeader = False
            Me.LookUpEdit_POA_Stage.Properties.ValueMember = "Id"
            Me.LookUpEdit_POA_Stage.Size = New System.Drawing.Size(142, 20)
            Me.LookUpEdit_POA_Stage.TabIndex = 7
            '
            'LookUpEdit_POA_Other
            '
            Me.LookUpEdit_POA_Other.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LookUpEdit_POA_Other.Location = New System.Drawing.Point(92, 64)
            Me.LookUpEdit_POA_Other.Name = "LookUpEdit_POA_Other"
            Me.LookUpEdit_POA_Other.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_POA_Other.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description")})
            Me.LookUpEdit_POA_Other.Properties.DisplayMember = "description"
            Me.LookUpEdit_POA_Other.Properties.NullText = ""
            Me.LookUpEdit_POA_Other.Properties.ShowFooter = False
            Me.LookUpEdit_POA_Other.Properties.ShowHeader = False
            Me.LookUpEdit_POA_Other.Properties.ValueMember = "Id"
            Me.LookUpEdit_POA_Other.Size = New System.Drawing.Size(142, 20)
            Me.LookUpEdit_POA_Other.TabIndex = 5
            '
            'LookUpEdit_POA_Fin
            '
            Me.LookUpEdit_POA_Fin.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LookUpEdit_POA_Fin.Location = New System.Drawing.Point(92, 37)
            Me.LookUpEdit_POA_Fin.Name = "LookUpEdit_POA_Fin"
            Me.LookUpEdit_POA_Fin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_POA_Fin.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description")})
            Me.LookUpEdit_POA_Fin.Properties.DisplayMember = "description"
            Me.LookUpEdit_POA_Fin.Properties.NullText = ""
            Me.LookUpEdit_POA_Fin.Properties.ShowFooter = False
            Me.LookUpEdit_POA_Fin.Properties.ShowHeader = False
            Me.LookUpEdit_POA_Fin.Properties.ValueMember = "Id"
            Me.LookUpEdit_POA_Fin.Size = New System.Drawing.Size(142, 20)
            Me.LookUpEdit_POA_Fin.TabIndex = 3
            '
            'LabelControl15
            '
            Me.LabelControl15.Location = New System.Drawing.Point(11, 41)
            Me.LabelControl15.Name = "LabelControl15"
            Me.LabelControl15.Size = New System.Drawing.Size(41, 13)
            Me.LabelControl15.TabIndex = 2
            Me.LabelControl15.Text = "Financial"
            '
            'LabelControl14
            '
            Me.LabelControl14.Location = New System.Drawing.Point(11, 13)
            Me.LabelControl14.Name = "LabelControl14"
            Me.LabelControl14.Size = New System.Drawing.Size(55, 13)
            Me.LabelControl14.TabIndex = 0
            Me.LabelControl14.Text = "Real Estate"
            '
            'LookUpEdit_RealEstate
            '
            Me.LookUpEdit_RealEstate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LookUpEdit_RealEstate.Location = New System.Drawing.Point(92, 10)
            Me.LookUpEdit_RealEstate.Name = "LookUpEdit_RealEstate"
            Me.LookUpEdit_RealEstate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_RealEstate.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description"), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "", False, DevExpress.Utils.HorzAlignment.[Default])})
            Me.LookUpEdit_RealEstate.Properties.DisplayMember = "description"
            Me.LookUpEdit_RealEstate.Properties.NullText = ""
            Me.LookUpEdit_RealEstate.Properties.ShowFooter = False
            Me.LookUpEdit_RealEstate.Properties.ShowHeader = False
            Me.LookUpEdit_RealEstate.Properties.ValueMember = "Id"
            Me.LookUpEdit_RealEstate.Size = New System.Drawing.Size(142, 20)
            Me.LookUpEdit_RealEstate.TabIndex = 1
            '
            'PopupContainerControl_POA_Phone
            '
            Me.PopupContainerControl_POA_Phone.Appearance.BackColor = System.Drawing.Color.Transparent
            Me.PopupContainerControl_POA_Phone.Appearance.Options.UseBackColor = True
            Me.PopupContainerControl_POA_Phone.Controls.Add(Me.LookUpEdit_POA_PhoneType)
            Me.PopupContainerControl_POA_Phone.Controls.Add(Me.LabelControl13)
            Me.PopupContainerControl_POA_Phone.Controls.Add(Me.LabelControl12)
            Me.PopupContainerControl_POA_Phone.Controls.Add(Me.POA_Telephone_RecordControl)
            Me.PopupContainerControl_POA_Phone.Location = New System.Drawing.Point(246, 86)
            Me.PopupContainerControl_POA_Phone.Name = "PopupContainerControl_POA_Phone"
            Me.PopupContainerControl_POA_Phone.Size = New System.Drawing.Size(286, 81)
            Me.PopupContainerControl_POA_Phone.TabIndex = 0
            '
            'LookUpEdit_POA_PhoneType
            '
            Me.LookUpEdit_POA_PhoneType.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LookUpEdit_POA_PhoneType.Location = New System.Drawing.Point(99, 45)
            Me.LookUpEdit_POA_PhoneType.Name = "LookUpEdit_POA_PhoneType"
            Me.LookUpEdit_POA_PhoneType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_POA_PhoneType.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description")})
            Me.LookUpEdit_POA_PhoneType.Properties.DisplayMember = "description"
            Me.LookUpEdit_POA_PhoneType.Properties.NullText = ""
            Me.LookUpEdit_POA_PhoneType.Properties.ShowFooter = False
            Me.LookUpEdit_POA_PhoneType.Properties.ShowHeader = False
            Me.LookUpEdit_POA_PhoneType.Properties.ValueMember = "Id"
            Me.LookUpEdit_POA_PhoneType.Size = New System.Drawing.Size(175, 20)
            Me.LookUpEdit_POA_PhoneType.TabIndex = 3
            '
            'LabelControl13
            '
            Me.LabelControl13.Location = New System.Drawing.Point(4, 49)
            Me.LabelControl13.Name = "LabelControl13"
            Me.LabelControl13.Size = New System.Drawing.Size(57, 13)
            Me.LabelControl13.TabIndex = 2
            Me.LabelControl13.Text = "Phone Type"
            '
            'LabelControl12
            '
            Me.LabelControl12.Location = New System.Drawing.Point(4, 18)
            Me.LabelControl12.Name = "LabelControl12"
            Me.LabelControl12.Size = New System.Drawing.Size(58, 13)
            Me.LabelControl12.TabIndex = 0
            Me.LabelControl12.Text = "Tel. Number"
            '
            'POA_Telephone_RecordControl
            '
            Me.POA_Telephone_RecordControl.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.POA_Telephone_RecordControl.ErrorIcon = Nothing
            Me.POA_Telephone_RecordControl.ErrorText = ""
            Me.POA_Telephone_RecordControl.Location = New System.Drawing.Point(99, 18)
            Me.POA_Telephone_RecordControl.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
            Me.POA_Telephone_RecordControl.Name = "POA_Telephone_RecordControl"
            Me.POA_Telephone_RecordControl.Size = New System.Drawing.Size(175, 20)
            Me.POA_Telephone_RecordControl.TabIndex = 1
            '
            'PopupContainerControl_POA_ID
            '
            Me.PopupContainerControl_POA_ID.Controls.Add(Me.LabelControl11)
            Me.PopupContainerControl_POA_ID.Controls.Add(Me.LabelControl10)
            Me.PopupContainerControl_POA_ID.Controls.Add(Me.LabelControl9)
            Me.PopupContainerControl_POA_ID.Controls.Add(Me.dt_POA_ID_Expires)
            Me.PopupContainerControl_POA_ID.Controls.Add(Me.dt_POA_ID_Issued)
            Me.PopupContainerControl_POA_ID.Controls.Add(Me.textEdit_POA_ID)
            Me.PopupContainerControl_POA_ID.Controls.Add(Me.LabelControl8)
            Me.PopupContainerControl_POA_ID.Controls.Add(Me.LookUpEdit_POA_Type)
            Me.PopupContainerControl_POA_ID.Location = New System.Drawing.Point(299, 176)
            Me.PopupContainerControl_POA_ID.Name = "PopupContainerControl_POA_ID"
            Me.PopupContainerControl_POA_ID.Size = New System.Drawing.Size(200, 140)
            Me.PopupContainerControl_POA_ID.TabIndex = 0
            '
            'LabelControl11
            '
            Me.LabelControl11.Location = New System.Drawing.Point(3, 98)
            Me.LabelControl11.Name = "LabelControl11"
            Me.LabelControl11.Size = New System.Drawing.Size(49, 13)
            Me.LabelControl11.TabIndex = 6
            Me.LabelControl11.Text = "ID Expires"
            '
            'LabelControl10
            '
            Me.LabelControl10.Location = New System.Drawing.Point(3, 74)
            Me.LabelControl10.Name = "LabelControl10"
            Me.LabelControl10.Size = New System.Drawing.Size(46, 13)
            Me.LabelControl10.TabIndex = 4
            Me.LabelControl10.Text = "ID Issued"
            '
            'LabelControl9
            '
            Me.LabelControl9.Location = New System.Drawing.Point(3, 50)
            Me.LabelControl9.Name = "LabelControl9"
            Me.LabelControl9.Size = New System.Drawing.Size(46, 13)
            Me.LabelControl9.TabIndex = 2
            Me.LabelControl9.Text = "POA ID #"
            '
            'dt_POA_ID_Expires
            '
            Me.dt_POA_ID_Expires.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.dt_POA_ID_Expires.EditValue = Nothing
            Me.dt_POA_ID_Expires.Location = New System.Drawing.Point(97, 98)
            Me.dt_POA_ID_Expires.Name = "dt_POA_ID_Expires"
            Me.dt_POA_ID_Expires.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.dt_POA_ID_Expires.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.dt_POA_ID_Expires.Size = New System.Drawing.Size(100, 20)
            Me.dt_POA_ID_Expires.TabIndex = 7
            '
            'dt_POA_ID_Issued
            '
            Me.dt_POA_ID_Issued.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.dt_POA_ID_Issued.EditValue = Nothing
            Me.dt_POA_ID_Issued.Location = New System.Drawing.Point(97, 72)
            Me.dt_POA_ID_Issued.Name = "dt_POA_ID_Issued"
            Me.dt_POA_ID_Issued.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.dt_POA_ID_Issued.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.dt_POA_ID_Issued.Size = New System.Drawing.Size(100, 20)
            Me.dt_POA_ID_Issued.TabIndex = 5
            '
            'textEdit_POA_ID
            '
            Me.textEdit_POA_ID.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.textEdit_POA_ID.Location = New System.Drawing.Point(97, 46)
            Me.textEdit_POA_ID.Name = "textEdit_POA_ID"
            Me.textEdit_POA_ID.Size = New System.Drawing.Size(100, 20)
            Me.textEdit_POA_ID.TabIndex = 3
            '
            'LabelControl8
            '
            Me.LabelControl8.Location = New System.Drawing.Point(3, 26)
            Me.LabelControl8.Name = "LabelControl8"
            Me.LabelControl8.Size = New System.Drawing.Size(62, 13)
            Me.LabelControl8.TabIndex = 0
            Me.LabelControl8.Text = "POA ID Type"
            '
            'LookUpEdit_POA_Type
            '
            Me.LookUpEdit_POA_Type.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LookUpEdit_POA_Type.Location = New System.Drawing.Point(97, 20)
            Me.LookUpEdit_POA_Type.Name = "LookUpEdit_POA_Type"
            Me.LookUpEdit_POA_Type.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_POA_Type.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description")})
            Me.LookUpEdit_POA_Type.Properties.DisplayMember = "description"
            Me.LookUpEdit_POA_Type.Properties.NullText = ""
            Me.LookUpEdit_POA_Type.Properties.ShowFooter = False
            Me.LookUpEdit_POA_Type.Properties.ShowHeader = False
            Me.LookUpEdit_POA_Type.Properties.ValueMember = "Id"
            Me.LookUpEdit_POA_Type.Size = New System.Drawing.Size(100, 20)
            Me.LookUpEdit_POA_Type.TabIndex = 1
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.PopupContainerEdit_POA_Approval)
            Me.LayoutControl1.Controls.Add(Me.PopupContainerEdit_POA_Phone)
            Me.LayoutControl1.Controls.Add(Me.PopupContainerEdit_POA_ID)
            Me.LayoutControl1.Controls.Add(Me.dt_POA_Expires)
            Me.LayoutControl1.Controls.Add(Me.NameRecordControl_POA)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(545, 78)
            Me.LayoutControl1.TabIndex = 50
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'PopupContainerEdit_POA_Approval
            '
            Me.PopupContainerEdit_POA_Approval.Location = New System.Drawing.Point(336, 50)
            Me.PopupContainerEdit_POA_Approval.Name = "PopupContainerEdit_POA_Approval"
            Me.PopupContainerEdit_POA_Approval.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.PopupContainerEdit_POA_Approval.Properties.PopupControl = Me.PopupContainerControl_POA_Approval
            Me.PopupContainerEdit_POA_Approval.Properties.ShowPopupCloseButton = False
            Me.PopupContainerEdit_POA_Approval.Size = New System.Drawing.Size(207, 20)
            Me.PopupContainerEdit_POA_Approval.StyleController = Me.LayoutControl1
            Me.PopupContainerEdit_POA_Approval.TabIndex = 43
            '
            'PopupContainerEdit_POA_Phone
            '
            Me.PopupContainerEdit_POA_Phone.Location = New System.Drawing.Point(336, 26)
            Me.PopupContainerEdit_POA_Phone.Name = "PopupContainerEdit_POA_Phone"
            Me.PopupContainerEdit_POA_Phone.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.PopupContainerEdit_POA_Phone.Properties.PopupControl = Me.PopupContainerControl_POA_Phone
            Me.PopupContainerEdit_POA_Phone.Properties.ShowPopupCloseButton = False
            Me.PopupContainerEdit_POA_Phone.Size = New System.Drawing.Size(207, 20)
            Me.PopupContainerEdit_POA_Phone.StyleController = Me.LayoutControl1
            Me.PopupContainerEdit_POA_Phone.TabIndex = 45
            '
            'PopupContainerEdit_POA_ID
            '
            Me.PopupContainerEdit_POA_ID.Location = New System.Drawing.Point(64, 26)
            Me.PopupContainerEdit_POA_ID.Name = "PopupContainerEdit_POA_ID"
            Me.PopupContainerEdit_POA_ID.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.PopupContainerEdit_POA_ID.Properties.PopupControl = Me.PopupContainerControl_POA_ID
            Me.PopupContainerEdit_POA_ID.Properties.ShowPopupCloseButton = False
            Me.PopupContainerEdit_POA_ID.Size = New System.Drawing.Size(206, 20)
            Me.PopupContainerEdit_POA_ID.StyleController = Me.LayoutControl1
            Me.PopupContainerEdit_POA_ID.TabIndex = 44
            '
            'dt_POA_Expires
            '
            Me.dt_POA_Expires.EditValue = Nothing
            Me.dt_POA_Expires.Location = New System.Drawing.Point(64, 50)
            Me.dt_POA_Expires.Name = "dt_POA_Expires"
            Me.dt_POA_Expires.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.dt_POA_Expires.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.dt_POA_Expires.Size = New System.Drawing.Size(206, 20)
            Me.dt_POA_Expires.StyleController = Me.LayoutControl1
            Me.dt_POA_Expires.TabIndex = 42
            '
            'NameRecordControl_POA
            '
            Me.NameRecordControl_POA.Location = New System.Drawing.Point(64, 2)
            Me.NameRecordControl_POA.Margin = New System.Windows.Forms.Padding(0)
            Me.NameRecordControl_POA.Name = "NameRecordControl_POA"
            Me.NameRecordControl_POA.Size = New System.Drawing.Size(479, 20)
            Me.NameRecordControl_POA.TabIndex = 41
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup1.GroupBordersVisible = False
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.LayoutControlItem3})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(545, 78)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.NameRecordControl_POA
            Me.LayoutControlItem1.CustomizationFormText = "POA Name"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(545, 24)
            Me.LayoutControlItem1.Text = "POA Name"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(59, 13)
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.PopupContainerEdit_POA_ID
            Me.LayoutControlItem2.CustomizationFormText = "POA ID"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(272, 24)
            Me.LayoutControlItem2.Text = "POA ID"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(59, 13)
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.PopupContainerEdit_POA_Phone
            Me.LayoutControlItem4.CustomizationFormText = "POA Phone"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(272, 24)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(273, 24)
            Me.LayoutControlItem4.Text = "POA Phone"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(59, 13)
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Control = Me.PopupContainerEdit_POA_Approval
            Me.LayoutControlItem5.CustomizationFormText = "Approval"
            Me.LayoutControlItem5.Location = New System.Drawing.Point(272, 48)
            Me.LayoutControlItem5.Name = "LayoutControlItem5"
            Me.LayoutControlItem5.Size = New System.Drawing.Size(273, 30)
            Me.LayoutControlItem5.Text = "Approval"
            Me.LayoutControlItem5.TextSize = New System.Drawing.Size(59, 13)
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.dt_POA_Expires
            Me.LayoutControlItem3.CustomizationFormText = "POA Expires"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 48)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(272, 30)
            Me.LayoutControlItem3.Text = "POA Expires"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(59, 13)
            '
            'ApprovedBy
            '
            Me.ApprovalBy.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ApprovalBy.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.ApprovalBy.UseMnemonic = False
            Me.ApprovalBy.Location = New System.Drawing.Point(94, 124)
            Me.ApprovalBy.Name = "ApprovedBy"
            Me.ApprovalBy.Size = New System.Drawing.Size(140, 13)
            Me.ApprovalBy.TabIndex = 9
            '
            'POAControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.LayoutControl1)
            Me.Controls.Add(Me.PopupContainerControl_POA_ID)
            Me.Controls.Add(Me.PopupContainerControl_POA_Phone)
            Me.Controls.Add(Me.PopupContainerControl_POA_Approval)
            Me.Name = "POAControl"
            Me.Size = New System.Drawing.Size(545, 78)
            CType(Me.PopupContainerControl_POA_Approval, System.ComponentModel.ISupportInitialize).EndInit()
            Me.PopupContainerControl_POA_Approval.ResumeLayout(False)
            Me.PopupContainerControl_POA_Approval.PerformLayout()
            CType(Me.LookUpEdit_POA_Stage.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_POA_Other.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_POA_Fin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_RealEstate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PopupContainerControl_POA_Phone, System.ComponentModel.ISupportInitialize).EndInit()
            Me.PopupContainerControl_POA_Phone.ResumeLayout(False)
            Me.PopupContainerControl_POA_Phone.PerformLayout()
            CType(Me.LookUpEdit_POA_PhoneType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.POA_Telephone_RecordControl, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PopupContainerControl_POA_ID, System.ComponentModel.ISupportInitialize).EndInit()
            Me.PopupContainerControl_POA_ID.ResumeLayout(False)
            Me.PopupContainerControl_POA_ID.PerformLayout()
            CType(Me.dt_POA_ID_Expires.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_POA_ID_Expires.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_POA_ID_Issued.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_POA_ID_Issued.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.textEdit_POA_ID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_POA_Type.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.PopupContainerEdit_POA_Approval.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PopupContainerEdit_POA_Phone.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PopupContainerEdit_POA_ID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_POA_Expires.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_POA_Expires.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

        Friend WithEvents PopupContainerControl_POA_Approval As DevExpress.XtraEditors.PopupContainerControl
        Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LookUpEdit_POA_Stage As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LookUpEdit_POA_Other As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LookUpEdit_POA_Fin As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LookUpEdit_RealEstate As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents PopupContainerControl_POA_Phone As DevExpress.XtraEditors.PopupContainerControl
        Friend WithEvents LookUpEdit_POA_PhoneType As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents POA_Telephone_RecordControl As DebtPlus.Data.Controls.TelephoneNumberRecordControl
        Friend WithEvents PopupContainerControl_POA_ID As DevExpress.XtraEditors.PopupContainerControl
        Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents dt_POA_ID_Expires As DevExpress.XtraEditors.DateEdit
        Friend WithEvents dt_POA_ID_Issued As DevExpress.XtraEditors.DateEdit
        Friend WithEvents textEdit_POA_ID As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LookUpEdit_POA_Type As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents PopupContainerEdit_POA_Approval As DevExpress.XtraEditors.PopupContainerEdit
        Friend WithEvents PopupContainerEdit_POA_Phone As DevExpress.XtraEditors.PopupContainerEdit
        Friend WithEvents PopupContainerEdit_POA_ID As DevExpress.XtraEditors.PopupContainerEdit
        Friend WithEvents dt_POA_Expires As DevExpress.XtraEditors.DateEdit
        Friend WithEvents NameRecordControl_POA As DebtPlus.Data.Controls.NameRecordControl
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents ApprovalBy As DevExpress.XtraEditors.LabelControl
    End Class
End Namespace
