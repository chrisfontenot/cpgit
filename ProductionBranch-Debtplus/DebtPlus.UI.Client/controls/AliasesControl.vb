#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.Data.Forms
Imports System.Linq
Imports DebtPlus.LINQ

Namespace controls
    Friend Class AliasesControl
        Private colRecords As System.Collections.Generic.List(Of client_addkey) = Nothing
        Private bc As BusinessContext = Nothing

        ''' <summary>
        ''' Initialize the storage for the control class
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            EnableMenus = True
        End Sub

        ''' <summary>
        ''' Edit the item as desired
        ''' </summary>
        Protected Overrides Sub OnEditItem(obj As Object)

            ' Find the record in the collection
            Dim record As client_addkey = TryCast(obj, client_addkey)
            If record Is Nothing Then
                Return
            End If

            ' Do the edit operation. There is no special form, just the input box for the single field.
            If EditForm(record) <> DialogResult.OK Then
                Return
            End If

            Try
                ' Update the record with the new value
                bc.SubmitChanges()

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
            End Try

            ' Refresh the list
            GridView1.RefreshData()
        End Sub

        ''' <summary>
        ''' Create the new item
        ''' </summary>
        Protected Overrides Sub OnCreateItem()

            ' Create a new blank record
            Dim record As client_addkey
            record = DebtPlus.LINQ.Factory.Manufacture_client_addkey()
            record.client = Context.ClientDs.ClientId
            record.additional = String.Empty

            ' Do the edit operation. There is no special form, just the input box for the single field.
            If EditForm(record) <> DialogResult.OK Then
                Return
            End If

            ' Update the record with the new value
            Try
                bc.client_addkeys.InsertOnSubmit(record)
                bc.SubmitChanges()

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
            End Try

            ' Add the new record to the collection
            colRecords.Add(record)
            GridView1.RefreshData()
        End Sub

        ''' <summary>
        ''' Delete the item as desired
        ''' </summary>
        Protected Overrides Sub OnDeleteItem(obj As Object)

            ' Find the record in the collection
            Dim record As client_addkey = TryCast(obj, client_addkey)
            If record Is Nothing Then
                Return
            End If

            ' Do the edit operation. There is no special form, just the input box for the single field.
            If DebtPlus.Data.Prompts.RequestConfirmation_Delete() <> DialogResult.Yes Then
                Return
            End If

            ' Update the record with the new value
            Try
                bc.client_addkeys.DeleteOnSubmit(record)
                bc.SubmitChanges()

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
            End Try

            ' Remove the record from the collection
            colRecords.Remove(record)
            GridView1.RefreshData()
        End Sub

        ''' <summary>
        ''' Read the alias table
        ''' </summary>
        Public Shadows Sub ReadForm(bc As DebtPlus.LINQ.BusinessContext)
            Me.bc = bc

            Try
                If colRecords Is Nothing Then

                    ' Retrieve the collection of records
                    colRecords = bc.client_addkeys.Where(Function(s) s.client = Context.ClientDs.ClientId).ToList()
                    GridControl1.DataSource = colRecords

                    ' Update the grid with the record collection
                    'GridView1.BestFitColumns()
                    'GridView1.RefreshData()
                End If

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
            End Try
        End Sub

        ''' <summary>
        ''' Perform the edit operation on the alias record
        ''' </summary>
        Private Function EditForm(record As client_addkey) As System.Windows.Forms.DialogResult
            Dim answer As System.Windows.Forms.DialogResult = InputBox.Show("What is the value for the additional alias sort key", record.additional, "Client Alias Update", record.additional, 50)
            If answer = DialogResult.OK Then
                record.additional = record.additional.Trim()
            End If
            Return answer
        End Function
    End Class
End Namespace
