﻿Namespace controls
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ProductsDispute
        Inherits System.Windows.Forms.UserControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then components.Dispose()
                End If
                components = Nothing
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer = Nothing

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.MemoExEdit_resolution_note = New DevExpress.XtraEditors.MemoExEdit()
            Me.LookUpEdit_resolution_status = New DevExpress.XtraEditors.LookUpEdit()
            Me.LabelControl_resolution_date = New DevExpress.XtraEditors.LabelControl()
            Me.LookUpEdit_disputed_status = New DevExpress.XtraEditors.LookUpEdit()
            Me.LabelControl_disputed_date = New DevExpress.XtraEditors.LabelControl()
            Me.MemoEdit_disputed_note = New DevExpress.XtraEditors.MemoExEdit()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlGroup3 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.MemoExEdit_resolution_note.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_resolution_status.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_disputed_status.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.MemoEdit_disputed_note.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.MemoExEdit_resolution_note)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_resolution_status)
            Me.LayoutControl1.Controls.Add(Me.LabelControl_resolution_date)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_disputed_status)
            Me.LayoutControl1.Controls.Add(Me.LabelControl_disputed_date)
            Me.LayoutControl1.Controls.Add(Me.MemoEdit_disputed_note)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(344, 222)
            Me.LayoutControl1.TabIndex = 0
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'MemoExEdit_resolution_note
            '
            Me.MemoExEdit_resolution_note.Location = New System.Drawing.Point(64, 184)
            Me.MemoExEdit_resolution_note.Name = "MemoExEdit_resolution_note"
            Me.MemoExEdit_resolution_note.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.MemoExEdit_resolution_note.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.MemoExEdit_resolution_note.Properties.ShowIcon = False
            Me.MemoExEdit_resolution_note.Size = New System.Drawing.Size(266, 20)
            Me.MemoExEdit_resolution_note.StyleController = Me.LayoutControl1
            Me.MemoExEdit_resolution_note.TabIndex = 9
            '
            'LookUpEdit_resolution_status
            '
            Me.LookUpEdit_resolution_status.Location = New System.Drawing.Point(64, 160)
            Me.LookUpEdit_resolution_status.Name = "LookUpEdit_resolution_status"
            Me.LookUpEdit_resolution_status.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.LookUpEdit_resolution_status.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_resolution_status.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.LookUpEdit_resolution_status.Properties.DisplayMember = "Description"
            Me.LookUpEdit_resolution_status.Properties.NullText = ""
            Me.LookUpEdit_resolution_status.Properties.ShowFooter = False
            Me.LookUpEdit_resolution_status.Properties.ShowHeader = False
            Me.LookUpEdit_resolution_status.Properties.ValueMember = "Id"
            Me.LookUpEdit_resolution_status.Size = New System.Drawing.Size(266, 20)
            Me.LookUpEdit_resolution_status.StyleController = Me.LayoutControl1
            Me.LookUpEdit_resolution_status.TabIndex = 8
            '
            'LabelControl_resolution_date
            '
            Me.LabelControl_resolution_date.Location = New System.Drawing.Point(64, 143)
            Me.LabelControl_resolution_date.Name = "LabelControl_resolution_date"
            Me.LabelControl_resolution_date.Size = New System.Drawing.Size(66, 13)
            Me.LabelControl_resolution_date.StyleController = Me.LayoutControl1
            Me.LabelControl_resolution_date.TabIndex = 7
            Me.LabelControl_resolution_date.Text = "LabelControl2"
            '
            'LookUpEdit_disputed_status
            '
            Me.LookUpEdit_disputed_status.Location = New System.Drawing.Point(64, 51)
            Me.LookUpEdit_disputed_status.Name = "LookUpEdit_disputed_status"
            Me.LookUpEdit_disputed_status.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.LookUpEdit_disputed_status.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_disputed_status.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.LookUpEdit_disputed_status.Properties.DisplayMember = "Description"
            Me.LookUpEdit_disputed_status.Properties.NullText = ""
            Me.LookUpEdit_disputed_status.Properties.ShowFooter = False
            Me.LookUpEdit_disputed_status.Properties.ShowHeader = False
            Me.LookUpEdit_disputed_status.Properties.ValueMember = "Id"
            Me.LookUpEdit_disputed_status.Size = New System.Drawing.Size(266, 20)
            Me.LookUpEdit_disputed_status.StyleController = Me.LayoutControl1
            Me.LookUpEdit_disputed_status.TabIndex = 5
            '
            'LabelControl_disputed_date
            '
            Me.LabelControl_disputed_date.Location = New System.Drawing.Point(64, 34)
            Me.LabelControl_disputed_date.Name = "LabelControl_disputed_date"
            Me.LabelControl_disputed_date.Size = New System.Drawing.Size(66, 13)
            Me.LabelControl_disputed_date.StyleController = Me.LayoutControl1
            Me.LabelControl_disputed_date.TabIndex = 4
            Me.LabelControl_disputed_date.Text = "LabelControl1"
            '
            'MemoEdit_disputed_note
            '
            Me.MemoEdit_disputed_note.Location = New System.Drawing.Point(64, 75)
            Me.MemoEdit_disputed_note.Name = "MemoEdit_disputed_note"
            Me.MemoEdit_disputed_note.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.MemoEdit_disputed_note.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.MemoEdit_disputed_note.Properties.ShowIcon = False
            Me.MemoEdit_disputed_note.Size = New System.Drawing.Size(266, 20)
            Me.MemoEdit_disputed_note.StyleController = Me.LayoutControl1
            Me.MemoEdit_disputed_note.TabIndex = 6
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup1.GroupBordersVisible = False
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup2, Me.LayoutControlGroup3})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(344, 222)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlGroup2
            '
            Me.LayoutControlGroup2.CustomizationFormText = "Dispute Resolution"
            Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem6, Me.LayoutControlItem5, Me.LayoutControlItem4})
            Me.LayoutControlGroup2.Location = New System.Drawing.Point(0, 109)
            Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
            Me.LayoutControlGroup2.Size = New System.Drawing.Size(344, 113)
            Me.LayoutControlGroup2.Text = "Dispute Resolution"
            '
            'LayoutControlItem6
            '
            Me.LayoutControlItem6.Control = Me.MemoExEdit_resolution_note
            Me.LayoutControlItem6.CustomizationFormText = "Note"
            Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 41)
            Me.LayoutControlItem6.Name = "LayoutControlItem6"
            Me.LayoutControlItem6.Size = New System.Drawing.Size(320, 28)
            Me.LayoutControlItem6.Text = "Note"
            Me.LayoutControlItem6.TextSize = New System.Drawing.Size(47, 13)
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Control = Me.LookUpEdit_resolution_status
            Me.LayoutControlItem5.CustomizationFormText = "Response"
            Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 17)
            Me.LayoutControlItem5.Name = "LayoutControlItem5"
            Me.LayoutControlItem5.Size = New System.Drawing.Size(320, 24)
            Me.LayoutControlItem5.Text = "Response"
            Me.LayoutControlItem5.TextSize = New System.Drawing.Size(47, 13)
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.LabelControl_resolution_date
            Me.LayoutControlItem4.CustomizationFormText = "Date"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(320, 17)
            Me.LayoutControlItem4.Text = "Date"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(47, 13)
            '
            'LayoutControlGroup3
            '
            Me.LayoutControlGroup3.CustomizationFormText = "Dispute Status"
            Me.LayoutControlGroup3.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem3, Me.LayoutControlItem2, Me.LayoutControlItem1})
            Me.LayoutControlGroup3.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup3.Name = "LayoutControlGroup3"
            Me.LayoutControlGroup3.Size = New System.Drawing.Size(344, 109)
            Me.LayoutControlGroup3.Text = "Dispute Status"
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.MemoEdit_disputed_note
            Me.LayoutControlItem3.CustomizationFormText = "Note"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 41)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(320, 24)
            Me.LayoutControlItem3.Text = "Note"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(47, 13)
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.LookUpEdit_disputed_status
            Me.LayoutControlItem2.CustomizationFormText = "Reason"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 17)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(320, 24)
            Me.LayoutControlItem2.Text = "Reason"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(47, 13)
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.LabelControl_disputed_date
            Me.LayoutControlItem1.CustomizationFormText = "Date"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(320, 17)
            Me.LayoutControlItem1.Text = "Date"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(47, 13)
            '
            'ProductsDispute
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.LayoutControl1)
            Me.Name = "ProductsDispute"
            Me.Size = New System.Drawing.Size(344, 222)
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.MemoExEdit_resolution_note.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_resolution_status.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_disputed_status.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.MemoEdit_disputed_note.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents MemoExEdit_resolution_note As DevExpress.XtraEditors.MemoExEdit
        Friend WithEvents LookUpEdit_resolution_status As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LabelControl_resolution_date As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LookUpEdit_disputed_status As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LabelControl_disputed_date As DevExpress.XtraEditors.LabelControl
        Friend WithEvents MemoEdit_disputed_note As DevExpress.XtraEditors.MemoExEdit
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlGroup3 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem

    End Class
End Namespace
