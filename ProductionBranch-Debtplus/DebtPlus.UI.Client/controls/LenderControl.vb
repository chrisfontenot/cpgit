#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DevExpress.XtraEditors.Controls
Imports DevExpress.XtraEditors
Imports System.ComponentModel
Imports System.Linq
Imports DebtPlus.LINQ
Imports System.Collections.Generic

Namespace controls
    Friend Class LenderControl

        <Description("Shows or Hides the HECM controls"), Category("Misc"), DefaultValueAttribute(False), Browsable(True)> _
        Public Property IsHECMVisible() As Boolean
            Get
                Return LayoutControlGroup_HECM_Default.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
            End Get
            Set(value As Boolean)
                LayoutControlGroup_HECM_Default.Visibility = If(value, DevExpress.XtraLayout.Utils.LayoutVisibility.Always, DevExpress.XtraLayout.Utils.LayoutVisibility.OnlyInCustomization)
            End Set
        End Property

        <Description("Shows or Hides the HPF controls"), Category("Misc"), DefaultValueAttribute(False), Browsable(True)> _
        Public Property IsHPFVisible() As Boolean
            Get
                Return LayoutControlGroup_CounselorContact.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
            End Get
            Set(value As Boolean)
                LayoutControlGroup_CounselorContact.Visibility = If(value, DevExpress.XtraLayout.Utils.LayoutVisibility.Always, DevExpress.XtraLayout.Utils.LayoutVisibility.OnlyInCustomization)
                LayoutControlGroup_ClientContact.Visibility = LayoutControlGroup_CounselorContact.Visibility
                EmptySpaceItem_BottomFiller.Visibility = If(value, DevExpress.XtraLayout.Utils.LayoutVisibility.OnlyInCustomization, DevExpress.XtraLayout.Utils.LayoutVisibility.Always)
            End Set
        End Property

        <Description("Current Servicer ID"), Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
        Public ReadOnly Property ServicerID As System.Nullable(Of Int32)
            Get
                Dim item As DebtPlus.Data.Controls.ComboboxItem = TryCast(comboBox_ServicerID.SelectedItem, DebtPlus.Data.Controls.ComboboxItem)
                If item IsNot Nothing Then
                    Return DirectCast(item.value, Int32)
                End If
                Return Nothing
            End Get
        End Property

        <Description("Current Investor ID"), Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
        Public ReadOnly Property InvestorID As System.Nullable(Of Int32)
            Get
                Return DebtPlus.Utils.Nulls.v_Int32(luInvestor.EditValue)
            End Get
        End Property

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler comboBox_ServicerID.EditValueChanging, AddressOf DebtPlus.Data.Validation.Combobox_ActiveTest
            AddHandler luInvestor.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler SpinEdit_Repay_Plan.EditValueChanging, AddressOf EditValueChanging
            AddHandler LookUpEdit_HPF_ClientConcatLenderOutcome.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler LookUpEdit_HPF_CnslrContactLenderOutcome.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler LookUpEdit_HPF_CnslrContactLenderReason.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler comboBox_ServicerID.EditValueChanging, AddressOf DebtPlus.Data.Validation.Combobox_ActiveTest
            RemoveHandler luInvestor.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler SpinEdit_Repay_Plan.EditValueChanging, AddressOf EditValueChanging
            RemoveHandler LookUpEdit_HPF_ClientConcatLenderOutcome.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler LookUpEdit_HPF_CnslrContactLenderOutcome.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler LookUpEdit_HPF_CnslrContactLenderReason.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
        End Sub

        Private OriginalData As DebtPlus.LINQ.Housing_lender
        Private colServicers As New System.Collections.Generic.List(Of DebtPlus.LINQ.Housing_lender_servicer)()

        ''' <summary>
        ''' This is the ID of the record in the database
        ''' </summary>
        <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), DefaultValue(DirectCast(Nothing, System.Object))> _
        Public Property EditValue As System.Nullable(Of Int32)
            Get
                Return SaveRecord()
            End Get
            Set(ByVal value As System.Nullable(Of Int32))
                UnRegisterHandlers()

                Try
                    ' Clear the current list and obtain the collection of servicers
                    Dim colItemList As System.Collections.Generic.List(Of Housing_lender_servicer) = DebtPlus.LINQ.Cache.Housing_lender_servicer.getList()
                    comboBox_ServicerID.Properties.Items.Clear()

                    ' Build an array of servicers before we add them. The sort takes some time and we want to do that only once, not 1500 times!
                    Dim colItemArray As New System.Collections.ArrayList(colItemList.Count)
                    colItemList.ForEach(Sub(s) colItemArray.Add(New DebtPlus.Data.Controls.ComboboxItem(s.description, s.Id, s.ActiveFlag)))

                    ' Finally, add the collection to the combobox. This will sort it but we add the items en-mass.
                    comboBox_ServicerID.Properties.Items.AddRange(colItemArray)

                    ' Load the lookup controls
                    luInvestor.Properties.DataSource = DebtPlus.LINQ.Cache.Investor.getList()
                    LookUpEdit_HPF_ClientConcatLenderOutcome.Properties.DataSource = DebtPlus.LINQ.Cache.Housing_ClientContactOutcomeType.getList()
                    LookUpEdit_HPF_CnslrContactLenderOutcome.Properties.DataSource = DebtPlus.LINQ.Cache.Housing_CounselorContactOutcomeType.getList()
                    LookUpEdit_HPF_CnslrContactLenderReason.Properties.DataSource = DebtPlus.LINQ.Cache.Housing_CounselorContactReasonType.getList()

                    OriginalData = ReadRecord(value)
                    SetCurrentValues(OriginalData)

                Finally
                    RegisterHandlers()
                End Try
            End Set
        End Property

        ''' <summary>
        ''' Load the editing controls from the current record
        ''' </summary>
        Private Sub SetCurrentValues(ByVal values As DebtPlus.LINQ.Housing_lender)

            ' Find the lender in the list.
            comboBox_ServicerID.SelectedIndex = -1
            If Not System.Nullable.Equals(values.ServicerID, DebtPlus.LINQ.Cache.Housing_lender_servicer.getOther()) Then
                For Each item As DebtPlus.Data.Controls.ComboboxItem In comboBox_ServicerID.Properties.Items
                    If DirectCast(item.value, Int32) = values.ServicerID Then
                        comboBox_ServicerID.SelectedItem = item
                        Exit For
                    End If
                Next
            End If

            ' Do not set the servicer id in the list. Use the default value for a general servicer
            If comboBox_ServicerID.SelectedIndex < 0 Then
                Dim strName As String = DebtPlus.Utils.Nulls.DStr(values.ServicerName)
                comboBox_ServicerID.Text = strName
            End If

            TextEdit_AcctNum.EditValue = values.AcctNum
            TextEdit_CaseNumber.EditValue = values.CaseNumber
            TextEdit_FdicNcusNum.EditValue = values.FdicNcusNum
            txtInvestorAccountNumber.EditValue = values.InvestorAccountNumber
            luInvestor.EditValue = values.InvestorID
            dtClientContactDate.EditValue = values.ContactLenderDate
            dtCounselorContactDate.EditValue = values.AttemptContactDate
            chkCounselorContactSuccess.Checked = values.ContactLenderSuccess.GetValueOrDefault(False)
            dtCurrentWorkoutPlanDate.EditValue = values.WorkoutPlanDate
            CalEdit_CorpAdvance.EditValue = values.corporate_advance
            CalEdit_Monthly_Desired_Repay.EditValue = values.monthly_desired_repay_amt
            SpinEdit_Repay_Plan.EditValue = values.repay_month
            dt_LastLenderUpdated.EditValue = values.last_updated
            LookUpEdit_HPF_ClientConcatLenderOutcome.EditValue = values.ClientConcatLenderOutcome
            LookUpEdit_HPF_CnslrContactLenderReason.EditValue = values.CnslrContactLenderReason
            LookUpEdit_HPF_CnslrContactLenderOutcome.EditValue = values.CnslrContactLenderOutcome
        End Sub

        ''' <summary>
        ''' Retrieve the desired records.
        ''' </summary>
        Private Function ReadRecord(ByVal RecordID As System.Nullable(Of Int32)) As DebtPlus.LINQ.Housing_lender
            Dim NewValues As DebtPlus.LINQ.Housing_lender = Nothing
            If RecordID.GetValueOrDefault(0) > 0 Then
                Using bc As New DebtPlus.LINQ.BusinessContext()
                    Dim id As Int32 = RecordID.Value
                    NewValues = bc.Housing_lenders.Where(Function(s) s.Id = id).FirstOrDefault()
                End Using
            End If

            If NewValues Is Nothing Then
                NewValues = DebtPlus.LINQ.Factory.Manufacture_housing_lender()
                ' Do not set the ID value. Leave it as zero to indicate that the record is "new".
            End If

            Return NewValues
        End Function

        ''' <summary>
        ''' Save the current information to the database
        ''' </summary>
        Private Function SaveRecord() As System.Nullable(Of Int32)
            UnRegisterHandlers()
            Try
                Dim values As DebtPlus.LINQ.Housing_lender = GetCurrentValues()
                Using bc As New DebtPlus.LINQ.BusinessContext()

                    ' If the record can be located then update the record.
                    If values.Id > 0 Then
                        Dim q As DebtPlus.LINQ.Housing_lender = bc.Housing_lenders.Where(Function(s) s.Id = values.Id).FirstOrDefault()
                        If q IsNot Nothing Then
                            q.AcctNum = values.AcctNum
                            q.AttemptContactDate = values.AttemptContactDate
                            q.CaseNumber = values.CaseNumber
                            q.ContactLenderDate = values.ContactLenderDate
                            q.ContactLenderSuccess = values.ContactLenderSuccess
                            q.FdicNcusNum = values.FdicNcusNum
                            q.InvestorID = values.InvestorID
                            q.InvestorAccountNumber = values.InvestorAccountNumber
                            q.ServicerID = values.ServicerID
                            q.ServicerName = values.ServicerName
                            q.WorkoutPlanDate = values.WorkoutPlanDate
                            q.corporate_advance = values.corporate_advance
                            q.monthly_desired_repay_amt = values.monthly_desired_repay_amt
                            q.repay_month = values.repay_month
                            q.last_updated = values.last_updated
                            q.ClientConcatLenderOutcome = values.ClientConcatLenderOutcome
                            q.CnslrContactLenderReason = values.CnslrContactLenderReason
                            q.CnslrContactLenderOutcome = values.CnslrContactLenderOutcome

                            ' Submit the changed dataset to the database
                            bc.SubmitChanges()

                            ' Update the current copy with the database settings
                            OriginalData = values
                            Return OriginalData.Id
                        End If
                    End If

                    ' Add the record to the database if it is not the same as an empty record
                    ' THis record is never NULL. Our pointer must exist if we are needed. The only
                    ' time that something is null is when we are not used and the parent takes care
                    ' of that condition.
                    bc.Housing_lenders.InsertOnSubmit(values)
                    bc.SubmitChanges()
                    OriginalData = values
                    Return OriginalData.Id
                End Using

            Finally
                RegisterHandlers()
            End Try

            ' Return the value as "null".
            Return New Int32()
        End Function

        Private Function GetCurrentValues() As DebtPlus.LINQ.Housing_lender

            Dim record As Housing_lender = DebtPlus.LINQ.Factory.Manufacture_housing_lender()
            record.Id = OriginalData.Id
            record.AcctNum = DebtPlus.Utils.Nulls.v_String(TextEdit_AcctNum.EditValue)
            record.CaseNumber = DebtPlus.Utils.Nulls.v_String(TextEdit_CaseNumber.EditValue)
            record.FdicNcusNum = DebtPlus.Utils.Nulls.v_String(TextEdit_FdicNcusNum.EditValue)
            record.InvestorID = DebtPlus.Utils.Nulls.v_Int32(luInvestor.EditValue)
            record.InvestorAccountNumber = DebtPlus.Utils.Nulls.v_String(txtInvestorAccountNumber.EditValue)
            record.ContactLenderDate = DebtPlus.Utils.Nulls.v_DateTime(dtClientContactDate.EditValue)
            record.AttemptContactDate = DebtPlus.Utils.Nulls.v_DateTime(dtCounselorContactDate.EditValue)
            record.ContactLenderSuccess = chkCounselorContactSuccess.Checked
            record.WorkoutPlanDate = DebtPlus.Utils.Nulls.v_DateTime(dtCurrentWorkoutPlanDate.EditValue)
            record.corporate_advance = DebtPlus.Utils.Nulls.v_Decimal(CalEdit_CorpAdvance.EditValue).GetValueOrDefault(0D)
            record.monthly_desired_repay_amt = DebtPlus.Utils.Nulls.v_Decimal(CalEdit_Monthly_Desired_Repay.EditValue).GetValueOrDefault(0D)
            record.repay_month = DebtPlus.Utils.Nulls.v_Int32(SpinEdit_Repay_Plan.EditValue).GetValueOrDefault(0)
            record.last_updated = DebtPlus.Utils.Nulls.v_DateTime(dt_LastLenderUpdated.EditValue)
            record.ClientConcatLenderOutcome = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_HPF_ClientConcatLenderOutcome.EditValue)
            record.CnslrContactLenderReason = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_HPF_CnslrContactLenderReason.EditValue)
            record.CnslrContactLenderOutcome = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_HPF_CnslrContactLenderOutcome.EditValue)

            ' If the servicer is missing then attempt to find the text name in the list of known servicers
            Dim selectedServicer As DebtPlus.Data.Controls.ComboboxItem = TryCast(comboBox_ServicerID.SelectedItem, DebtPlus.Data.Controls.ComboboxItem)
            If selectedServicer IsNot Nothing Then
                record.ServicerName = Nothing
                record.ServicerID = DirectCast(selectedServicer.value, Int32)
            Else
                Dim servicerName As String = comboBox_ServicerID.Text.Trim()
                Dim q As DebtPlus.LINQ.Housing_lender_servicer = DebtPlus.LINQ.Cache.Housing_lender_servicer.getList().Find(Function(s) String.Compare(s.description, servicerName, True) = 0)
                If q IsNot Nothing Then
                    record.ServicerID = q.Id
                    record.ServicerName = Nothing
                Else
                    record.ServicerID = DebtPlus.LINQ.Cache.Housing_lender_servicer.getOther()
                    record.ServicerName = servicerName
                End If
            End If

            Return record
        End Function

        ''' <summary>
        ''' Handle the change in the numerical value. Do not allow it to go negative.
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)
            Dim NewValue As Double = 0.0#
            If e.NewValue IsNot Nothing AndAlso e.NewValue IsNot DBNull.Value Then
                If Not Double.TryParse(Convert.ToString(e.NewValue), NewValue) Then
                    e.Cancel = True
                    Return
                End If
            End If

            e.Cancel = (NewValue < 0.0#)
        End Sub
    End Class
End Namespace