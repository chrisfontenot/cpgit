﻿Imports System.Linq
Imports DebtPlus.LINQ
Imports System.Windows.Forms
Imports System

Namespace controls
    Friend Class ProductsNotesGrid
        Inherits MyGridControlClient

        Private Delegate Sub RefreshListDelegate()
        Protected colRecords As System.Collections.Generic.List(Of client_product_note) = Nothing
        Private record As DebtPlus.LINQ.client_product = Nothing
        Private bc As BusinessContext = Nothing

        Private currentUser As String = String.Empty
        Private isAdmin As Boolean = False

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            EnableMenus = True
        End Sub

        Private Sub RegisterHandlers()
            AddHandler GridView1.CustomColumnDisplayText, AddressOf GridView1_CustomColumnDisplayText
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler GridView1.CustomColumnDisplayText, AddressOf GridView1_CustomColumnDisplayText
        End Sub

        Friend Shadows Sub SaveForm()
        End Sub

        ''' <summary>
        ''' Do any custom formatting of fields for the current grid. We format the type to a string.
        ''' </summary>
        Private Sub GridView1_CustomColumnDisplayText(sender As Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs)

            ' Find the record being displayed
            Dim record As client_product_note = TryCast(GridView1.GetRow(e.RowHandle), client_product_note)
            If record Is Nothing Then
                Return
            End If

            ' If the column is the note type then format it properly
            If e.Column Is GridColumn_type Then
                Try
                    Dim noteType As DebtPlus.LINQ.InMemory.Notes.NoteTypes = CType(record.type, DebtPlus.LINQ.InMemory.Notes.NoteTypes)
                    e.DisplayText = noteType.ToString()
                Catch
                    e.DisplayText = "Unknown"
                End Try
            End If
        End Sub

        Friend Shadows Sub ReadForm(bc As BusinessContext, record As client_product, IncludePerm As Boolean, IncludeSystem As Boolean)

            UnRegisterHandlers()
            Try
                MyClass.bc = bc
                MyClass.record = record

                If colRecords Is Nothing Then
                    currentUser = BusinessContext.suser_sname()
                    isAdmin = String.Compare(currentUser, "sa", False) = 0
                    colRecords = bc.client_product_notes.Where(Function(s) s.client_product = record.Id).ToList()
                End If

                ' Filter the grid control appropriately
                GridControl1.BeginUpdate()
                GridControl1.DataSource = colRecords

                If IncludePerm AndAlso Not IncludeSystem Then
                    GridView1.ActiveFilterString = "[type]=1"
                ElseIf IncludeSystem AndAlso Not IncludePerm Then
                    GridView1.ActiveFilterString = "[type]=3"
                Else
                    GridView1.ActiveFilterString = String.Empty
                End If

                GridControl1.EndUpdate()

            Finally
                RegisterHandlers()
            End Try
        End Sub

        Protected Overrides Sub OnCreateItem()
            Dim ithrd As System.Threading.Thread = New System.Threading.Thread(New System.Threading.ParameterizedThreadStart(AddressOf CreateNote)) With _
                                                   {
                                                        .Name = "CreateNote",
                                                        .IsBackground = True
                                                   }

            ithrd.SetApartmentState(System.Threading.ApartmentState.STA)
            ithrd.Start(Nothing)
        End Sub

        Protected Overrides Sub OnDeleteItem(obj As Object)
            Dim ithrd As System.Threading.Thread = New System.Threading.Thread(New System.Threading.ParameterizedThreadStart(AddressOf DeleteNote)) With _
                                  {
                                     .Name = "DeleteNote",
                                     .IsBackground = True
                                  }
            ithrd.SetApartmentState(System.Threading.ApartmentState.STA)
            ithrd.Start(obj)
        End Sub

        Protected Overrides Sub OnEditItem(obj As Object)
            Dim ithrd As System.Threading.Thread = New System.Threading.Thread(New System.Threading.ParameterizedThreadStart(AddressOf EditNote)) With _
                                                {
                                                  .Name = "EditNote",
                                                  .IsBackground = True
                                                 }
            ithrd.SetApartmentState(System.Threading.ApartmentState.STA)
            ithrd.Start(obj)
        End Sub

        Public Sub CreateNote(obj As Object)

            ' Create the note.
            Using NoteClass As New DebtPlus.Notes.NoteClass.ClientProductNote()
                If NoteClass.Create(record.Id) Then
                    RefreshList()
                End If
            End Using
        End Sub

        Public Sub DeleteNote(obj As Object)
            Dim currentNote As client_product_note = TryCast(obj, client_product_note)
            If currentNote Is Nothing Then
                Return
            End If

            Using n As New DebtPlus.Notes.NoteClass.ClientProductNote()
                If n.Delete(currentNote.Id) Then
                    RefreshList()
                End If
            End Using
        End Sub

        Public Sub EditNote(obj As Object)
            Dim currentNote As client_product_note = TryCast(obj, client_product_note)
            If currentNote Is Nothing Then
                Return
            End If

            Using n As New DebtPlus.Notes.NoteClass.ClientProductNote()
                If OkToEditNote(obj) Then
                    n.Edit(currentNote.Id)
                    RefreshList()
                Else
                    n.Show(currentNote.Id)
                End If
            End Using
        End Sub

        Private Sub RefreshList()
            If GridControl1.InvokeRequired Then
                Dim ia As System.IAsyncResult = GridControl1.BeginInvoke(New RefreshListDelegate(AddressOf RefreshList))
                GridControl1.EndInvoke(ia)
                Return
            End If

            colRecords = bc.client_product_notes.Where(Function(s) s.client_product = record.Id).ToList()
            GridControl1.DataSource = colRecords
            GridView1.RefreshData()
        End Sub

        ''' <summary>
        ''' Determine if the current user is allowed to delete the current note.
        ''' </summary>
        Protected Overrides Function OkToDelete(obj As Object) As Boolean
            If Not MyBase.OkToDelete(obj) Then
                Return False
            End If

            ' Find the current note. If none then don't allow the edit operation
            Dim currentNote As client_product_note = TryCast(obj, client_product_note)
            If currentNote Is Nothing Then
                Return False
            End If

            ' Admin users are always allowed to delete the note
            If isAdmin Then
                Return True
            End If

            ' If the user is not allowed to delete a note then the user is not allowed
            If Not DebtPlus.Configuration.Config.DeleteNotes Then
                Return False
            End If

            ' Look at the status if the user is different
            If String.Compare(currentNote.created_by, currentUser, True) <> 0 Then
                Return Not currentNote.dont_delete
            End If

            ' Finally, the user is the same. Allow the delete operation.
            Return True
        End Function

        ''' <summary>
        ''' Determine if the current user is allowed to edit the current note.
        ''' </summary>
        ''' <param name="obj">Pointer to the Note structure for the current note</param>
        Protected Function OkToEditNote(obj As Object) As Boolean

            ' THe user must be allowed to edit the note.
            If Not MyBase.OkToEdit(obj) Then
                Return False
            End If

            ' Find the current note. If none then don't allow the edit operation
            Dim currentNote As client_product_note = TryCast(obj, client_product_note)
            If currentNote Is Nothing Then
                Return False
            End If

            ' System notes are never allowed to be edited, even by admin users
            If currentNote.type = 3 Then
                Return False
            End If

            ' Admin users are always allowed to edit the note
            If isAdmin Then
                Return True
            End If

            ' If the user is the same then the edit operation is allowed if the registry says so
            If String.Compare(currentNote.created_by, currentUser, True) = 0 Then
                Return DebtPlus.Configuration.Config.EditExistingNoteSameUser
            End If

            ' If the user is not the same then we look at the indicator flags
            Return DebtPlus.Configuration.Config.EditExistingNoteOtherUser AndAlso Not currentNote.dont_edit
        End Function

        Protected Overrides Sub PopupMenu_Items_Popup(sender As Object, e As EventArgs)

            ' Change the text to show the action allowed for the edit operation.
            If ControlRow >= 0 Then
                Dim obj As Object = GridView1.GetRow(ControlRow)
                If obj IsNot Nothing Then
                    BarButtonItem_Change.Caption = If(OkToEditNote(obj), "&Edit...", "&Show...")
                End If
            End If

            ' Finally, do the standard logic for the event.
            MyBase.PopupMenu_Items_Popup(sender, e)
        End Sub
    End Class
End Namespace
