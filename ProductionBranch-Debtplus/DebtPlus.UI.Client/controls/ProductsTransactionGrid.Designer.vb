﻿Imports DebtPlus.LINQ

Namespace controls
    Partial Class ProductsTransactionGrid

        'NOTE: The following procedure is required by the Component Designer
        'It can be modified using the Component Designer.
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.GridColumn_date_created = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_created_by = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_tran_type = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_cost = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_discount = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_tendered = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_disputed = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_note = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_charged = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_paid = New DevExpress.XtraGrid.Columns.GridColumn()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PopupMenu_Items, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(195, Byte), Integer))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(189, Byte), Integer))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(206, Byte), Integer))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(225, Byte), Integer))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(215, Byte), Integer))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(215, Byte), Integer))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(215, Byte), Integer))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_tran_type, Me.GridColumn_charged, Me.GridColumn_paid, Me.GridColumn_note, Me.GridColumn_date_created, Me.GridColumn_created_by, Me.GridColumn_cost, Me.GridColumn_discount, Me.GridColumn_tendered, Me.GridColumn_disputed})
            Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridView1.OptionsBehavior.AutoPopulateColumns = False
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsDetail.AllowZoomDetail = False
            Me.GridView1.OptionsDetail.EnableMasterViewMode = False
            Me.GridView1.OptionsDetail.ShowDetailTabs = False
            Me.GridView1.OptionsDetail.SmartDetailExpand = False
            Me.GridView1.OptionsFilter.AllowColumnMRUFilterList = False
            Me.GridView1.OptionsFilter.AllowFilterEditor = False
            Me.GridView1.OptionsFilter.AllowFilterIncrementalSearch = False
            Me.GridView1.OptionsFilter.AllowMRUFilterList = False
            Me.GridView1.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = False
            Me.GridView1.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = False
            Me.GridView1.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = False
            Me.GridView1.OptionsLayout.Columns.StoreAllOptions = True
            Me.GridView1.OptionsLayout.LayoutVersion = "1"
            Me.GridView1.OptionsLayout.StoreAllOptions = True
            Me.GridView1.OptionsLayout.StoreDataSettings = False
            Me.GridView1.OptionsMenu.EnableFooterMenu = False
            Me.GridView1.OptionsMenu.ShowAutoFilterRowItem = False
            Me.GridView1.OptionsView.EnableAppearanceEvenRow = True
            Me.GridView1.OptionsView.EnableAppearanceOddRow = True
            Me.GridView1.OptionsView.ShowDetailButtons = False
            Me.GridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridView1.OptionsView.ShowIndicator = False
            Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn_date_created, DevExpress.Data.ColumnSortOrder.Ascending)})
            '
            'GridColumn_date_created
            '
            Me.GridColumn_date_created.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_date_created.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date_created.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_date_created.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date_created.Caption = "Date"
            Me.GridColumn_date_created.DisplayFormat.FormatString = "M/dd/yyyy hh:mm tt"
            Me.GridColumn_date_created.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_created.FieldName = "date_created"
            Me.GridColumn_date_created.GroupFormat.FormatString = "M/dd/yyyy hh:mm tt"
            Me.GridColumn_date_created.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_created.Name = "GridColumn_date_created"
            Me.GridColumn_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_date_created.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value
            Me.GridColumn_date_created.Visible = True
            Me.GridColumn_date_created.VisibleIndex = 4
            Me.GridColumn_date_created.Width = 88
            '
            'GridColumn_created_by
            '
            Me.GridColumn_created_by.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_created_by.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_created_by.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_created_by.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_created_by.Caption = "Creator"
            Me.GridColumn_created_by.FieldName = "created_by"
            Me.GridColumn_created_by.Name = "GridColumn_created_by"
            Me.GridColumn_created_by.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_created_by.Visible = True
            Me.GridColumn_created_by.VisibleIndex = 5
            Me.GridColumn_created_by.Width = 161
            '
            'GridColumn_tran_type
            '
            Me.GridColumn_tran_type.Caption = "Type"
            Me.GridColumn_tran_type.FieldName = "transaction_type"
            Me.GridColumn_tran_type.Name = "GridColumn_tran_type"
            Me.GridColumn_tran_type.Visible = True
            Me.GridColumn_tran_type.VisibleIndex = 0
            Me.GridColumn_tran_type.Width = 62
            '
            'GridColumn_cost
            '
            Me.GridColumn_cost.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_cost.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_cost.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_cost.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_cost.Caption = "Cost"
            Me.GridColumn_cost.DisplayFormat.FormatString = "c2"
            Me.GridColumn_cost.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_cost.FieldName = "cost"
            Me.GridColumn_cost.GroupFormat.FormatString = "c2"
            Me.GridColumn_cost.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_cost.Name = "GridColumn_cost"
            '
            'GridColumn_discount
            '
            Me.GridColumn_discount.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_discount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_discount.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_discount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_discount.Caption = "Discount"
            Me.GridColumn_discount.DisplayFormat.FormatString = "c2"
            Me.GridColumn_discount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_discount.FieldName = "discount"
            Me.GridColumn_discount.GroupFormat.FormatString = "c2"
            Me.GridColumn_discount.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_discount.Name = "GridColumn_discount"
            '
            'GridColumn_tendered
            '
            Me.GridColumn_tendered.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_tendered.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_tendered.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_tendered.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_tendered.Caption = "Tendered"
            Me.GridColumn_tendered.DisplayFormat.FormatString = "c2"
            Me.GridColumn_tendered.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_tendered.FieldName = "payment"
            Me.GridColumn_tendered.GroupFormat.FormatString = "c2"
            Me.GridColumn_tendered.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_tendered.Name = "GridColumn_tendered"
            '
            'GridColumn_disputed
            '
            Me.GridColumn_disputed.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_disputed.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_disputed.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_disputed.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_disputed.Caption = "Disputed"
            Me.GridColumn_disputed.DisplayFormat.FormatString = "c2"
            Me.GridColumn_disputed.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_disputed.FieldName = "disputed"
            Me.GridColumn_disputed.GroupFormat.FormatString = "c2"
            Me.GridColumn_disputed.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_disputed.Name = "GridColumn_disputed"
            '
            'GridColumn_note
            '
            Me.GridColumn_note.Caption = "Note"
            Me.GridColumn_note.CustomizationCaption = "Note"
            Me.GridColumn_note.FieldName = "note"
            Me.GridColumn_note.Name = "GridColumn_note"
            Me.GridColumn_note.OptionsColumn.AllowEdit = False
            Me.GridColumn_note.Visible = True
            Me.GridColumn_note.VisibleIndex = 3
            Me.GridColumn_note.Width = 151
            '
            'GridColumn_charged
            '
            Me.GridColumn_charged.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_charged.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_charged.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_charged.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_charged.Caption = "Charged"
            Me.GridColumn_charged.CustomizationCaption = "Charged"
            Me.GridColumn_charged.DisplayFormat.FormatString = "c"
            Me.GridColumn_charged.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_charged.GroupFormat.FormatString = "c"
            Me.GridColumn_charged.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_charged.Name = "GridColumn_charged"
            Me.GridColumn_charged.Visible = True
            Me.GridColumn_charged.VisibleIndex = 1
            Me.GridColumn_charged.Width = 81
            '
            'GridColumn_paid
            '
            Me.GridColumn_paid.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_paid.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_paid.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_paid.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_paid.Caption = "Paid"
            Me.GridColumn_paid.CustomizationCaption = "Paid"
            Me.GridColumn_paid.DisplayFormat.FormatString = "c"
            Me.GridColumn_paid.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_paid.GroupFormat.FormatString = "c"
            Me.GridColumn_paid.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_paid.Name = "GridColumn_paid"
            Me.GridColumn_paid.Visible = True
            Me.GridColumn_paid.VisibleIndex = 2
            Me.GridColumn_paid.Width = 74
            '
            'ProductsTransactionGrid
            '
            Me.Name = "ProductsTransactionGrid"
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PopupMenu_Items, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

        Private WithEvents GridColumn_date_created As DevExpress.XtraGrid.Columns.GridColumn
        Private WithEvents GridColumn_created_by As DevExpress.XtraGrid.Columns.GridColumn
        Private WithEvents GridColumn_tran_type As DevExpress.XtraGrid.Columns.GridColumn
        Private WithEvents GridColumn_cost As DevExpress.XtraGrid.Columns.GridColumn
        Private WithEvents GridColumn_discount As DevExpress.XtraGrid.Columns.GridColumn
        Private WithEvents GridColumn_tendered As DevExpress.XtraGrid.Columns.GridColumn
        Private WithEvents GridColumn_disputed As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_note As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_charged As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_paid As DevExpress.XtraGrid.Columns.GridColumn
    End Class
End Namespace
