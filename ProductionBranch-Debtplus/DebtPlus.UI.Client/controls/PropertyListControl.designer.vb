﻿Namespace controls

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class PropertyListControl
        Inherits MyGridControlClient

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.GridColumn_oID = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_PrimaryResidenceIND = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_Type = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_Address1 = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_ImprovementsValue = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_LandValue = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_City = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_State = New DevExpress.XtraGrid.Columns.GridColumn()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PopupMenu_Items, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridControl1
            '
            Me.GridControl1.Size = New System.Drawing.Size(335, 182)
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(195, Byte), Integer))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(189, Byte), Integer))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(206, Byte), Integer))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(225, Byte), Integer))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(215, Byte), Integer))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(215, Byte), Integer))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(215, Byte), Integer))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_oID, Me.GridColumn_Address1, Me.GridColumn_City, Me.GridColumn_PrimaryResidenceIND, Me.GridColumn_State, Me.GridColumn_Type, Me.GridColumn_ImprovementsValue, Me.GridColumn_LandValue})
            Me.GridView1.OptionsBehavior.AutoPopulateColumns = False
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsLayout.Columns.StoreAllOptions = True
            Me.GridView1.OptionsLayout.LayoutVersion = "1"
            Me.GridView1.OptionsLayout.StoreAllOptions = True
            Me.GridView1.OptionsLayout.StoreDataSettings = False
            Me.GridView1.OptionsView.EnableAppearanceEvenRow = True
            Me.GridView1.OptionsView.EnableAppearanceOddRow = True
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridView1.OptionsView.ShowIndicator = False
            '
            'barDockControlTop
            '
            Me.barDockControlTop.Size = New System.Drawing.Size(335, 0)
            '
            'barDockControlBottom
            '
            Me.barDockControlBottom.Location = New System.Drawing.Point(0, 182)
            Me.barDockControlBottom.Size = New System.Drawing.Size(335, 0)
            '
            'barDockControlLeft
            '
            Me.barDockControlLeft.Size = New System.Drawing.Size(0, 182)
            '
            'barDockControlRight
            '
            Me.barDockControlRight.Location = New System.Drawing.Point(335, 0)
            Me.barDockControlRight.Size = New System.Drawing.Size(0, 182)
            '
            'GridColumn_oID
            '
            Me.GridColumn_oID.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_oID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_oID.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_oID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_oID.Caption = "ID"
            Me.GridColumn_oID.CustomizationCaption = "Record ID"
            Me.GridColumn_oID.DisplayFormat.FormatString = "{0:f0}"
            Me.GridColumn_oID.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_oID.FieldName = "Id"
            Me.GridColumn_oID.GroupFormat.FormatString = "{0:f0}"
            Me.GridColumn_oID.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_oID.Name = "GridColumn_oID"
            Me.GridColumn_oID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_PrimaryResidenceIND
            '
            Me.GridColumn_PrimaryResidenceIND.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_PrimaryResidenceIND.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_PrimaryResidenceIND.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_PrimaryResidenceIND.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_PrimaryResidenceIND.Caption = "Residency"
            Me.GridColumn_PrimaryResidenceIND.CustomizationCaption = "Residency"
            Me.GridColumn_PrimaryResidenceIND.FieldName = "Residency"
            Me.GridColumn_PrimaryResidenceIND.Name = "GridColumn_PrimaryResidenceIND"
            Me.GridColumn_PrimaryResidenceIND.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_PrimaryResidenceIND.Visible = True
            Me.GridColumn_PrimaryResidenceIND.VisibleIndex = 3
            Me.GridColumn_PrimaryResidenceIND.Width = 81
            '
            'GridColumn_Type
            '
            Me.GridColumn_Type.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Type.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_Type.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Type.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_Type.Caption = "Type"
            Me.GridColumn_Type.CustomizationCaption = "Type"
            Me.GridColumn_Type.FieldName = "PropertyType"
            Me.GridColumn_Type.Name = "GridColumn_Type"
            Me.GridColumn_Type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_Type.Visible = True
            Me.GridColumn_Type.VisibleIndex = 4
            Me.GridColumn_Type.Width = 81
            '
            'GridColumn_Address1
            '
            Me.GridColumn_Address1.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Address1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_Address1.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Address1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_Address1.Caption = "Address 1"
            Me.GridColumn_Address1.CustomizationCaption = "1st line of address"
            Me.GridColumn_Address1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_Address1.FieldName = "Id"
            Me.GridColumn_Address1.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_Address1.Name = "GridColumn_Address1"
            Me.GridColumn_Address1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_Address1.Visible = True
            Me.GridColumn_Address1.VisibleIndex = 0
            Me.GridColumn_Address1.Width = 266
            '
            'GridColumn_ImprovementsValue
            '
            Me.GridColumn_ImprovementsValue.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_ImprovementsValue.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_ImprovementsValue.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_ImprovementsValue.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_ImprovementsValue.Caption = "Improvements"
            Me.GridColumn_ImprovementsValue.CustomizationCaption = "Improvements"
            Me.GridColumn_ImprovementsValue.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_ImprovementsValue.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_ImprovementsValue.FieldName = "ImprovementsValue"
            Me.GridColumn_ImprovementsValue.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_ImprovementsValue.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_ImprovementsValue.Name = "GridColumn_ImprovementsValue"
            Me.GridColumn_ImprovementsValue.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_ImprovementsValue.Visible = True
            Me.GridColumn_ImprovementsValue.VisibleIndex = 5
            Me.GridColumn_ImprovementsValue.Width = 73
            '
            'GridColumn_LandValue
            '
            Me.GridColumn_LandValue.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_LandValue.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_LandValue.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_LandValue.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_LandValue.Caption = "Land"
            Me.GridColumn_LandValue.CustomizationCaption = "Land"
            Me.GridColumn_LandValue.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_LandValue.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_LandValue.FieldName = "LandValue"
            Me.GridColumn_LandValue.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_LandValue.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_LandValue.Name = "GridColumn_LandValue"
            Me.GridColumn_LandValue.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_LandValue.Visible = True
            Me.GridColumn_LandValue.VisibleIndex = 6
            Me.GridColumn_LandValue.Width = 73
            '
            'GridColumn_City
            '
            Me.GridColumn_City.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_City.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_City.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_City.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_City.Caption = "City"
            Me.GridColumn_City.CustomizationCaption = "City Name"
            Me.GridColumn_City.FieldName = "Id"
            Me.GridColumn_City.Name = "GridColumn_City"
            Me.GridColumn_City.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_City.Visible = True
            Me.GridColumn_City.VisibleIndex = 1
            Me.GridColumn_City.Width = 194
            '
            'GridColumn_State
            '
            Me.GridColumn_State.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_State.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_State.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_State.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_State.Caption = "State"
            Me.GridColumn_State.CustomizationCaption = "State Name"
            Me.GridColumn_State.FieldName = "Id"
            Me.GridColumn_State.Name = "GridColumn_State"
            Me.GridColumn_State.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_State.Visible = True
            Me.GridColumn_State.VisibleIndex = 2
            Me.GridColumn_State.Width = 73
            '
            Me.GridColumn_Address1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_Address1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_Address1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_Address1.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_City.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_City.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_City.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_City.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_State.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_State.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_State.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_State.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_PrimaryResidenceIND.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_PrimaryResidenceIND.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_PrimaryResidenceIND.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_PrimaryResidenceIND.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_Type.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_Type.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_Type.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_Type.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom
            '
            'PropertyListControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Name = "PropertyListControl"
            Me.Size = New System.Drawing.Size(335, 182)
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PopupMenu_Items, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

        Protected Friend WithEvents GridColumn_oID As DevExpress.XtraGrid.Columns.GridColumn
        Protected Friend WithEvents GridColumn_PrimaryResidenceIND As DevExpress.XtraGrid.Columns.GridColumn
        Protected Friend WithEvents GridColumn_Type As DevExpress.XtraGrid.Columns.GridColumn
        Protected Friend WithEvents GridColumn_Address1 As DevExpress.XtraGrid.Columns.GridColumn
        Protected Friend WithEvents GridColumn_City As DevExpress.XtraGrid.Columns.GridColumn
        Protected Friend WithEvents GridColumn_State As DevExpress.XtraGrid.Columns.GridColumn
        Protected Friend WithEvents GridColumn_ImprovementsValue As DevExpress.XtraGrid.Columns.GridColumn
        Protected Friend WithEvents GridColumn_LandValue As DevExpress.XtraGrid.Columns.GridColumn
    End Class
End Namespace