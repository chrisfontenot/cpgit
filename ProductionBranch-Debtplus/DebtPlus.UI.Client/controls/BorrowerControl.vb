#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.ComponentModel
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.XtraEditors
Imports DebtPlus.UI.Client.forms
Imports System.Linq
Imports DebtPlus.LINQ

Namespace controls

    Friend Class BorrowerControl
        Private ControlsLoaded As Boolean = False

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler luEverFiledBKChapter.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler LookUpEdit_GenderCD.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler LookUpEdit_RaceCD.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler LookUpEdit_PreferredLanguageCD.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler LookUpEdit_IntakeCreditBureauCD.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler LookUpEdit_MaritalStatusCd.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler LookUpEdit_EducLevelCompletedCD.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler LookUpEdit_Ethnicity.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler milDependent.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler milGrade.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler milService.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler milStatus.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler PopupContainerEdit_Military.QueryDisplayText, AddressOf PopupContainerEdit_Military_QueryDisplayText
            AddHandler PopupContainerEdit_Military.QueryResultValue, AddressOf PopupContainerEdit_Military_QueryResultValue
            AddHandler ComboBoxEdit_Person.SelectedIndexChanged, AddressOf ComboBoxEdit_Person_SelectedIndexChanged
            AddHandler SpinEdit_CreditScore.Spin, AddressOf SpinEdit_CreditScore_Spin
            AddHandler SpinEdit_CreditScore.Validating, AddressOf SpinEdit_CreditScore_Validating
            AddHandler DateEdit_emp_start_date.ButtonClick, AddressOf DateEdit_emp_start_date_ButtonClick
            AddHandler DateEdit_DOB.Validating, AddressOf DateEdit_DOB_Validating
            AddHandler PopupContainerEdit_EverFiledBK.QueryResultValue, AddressOf PopupContainerEdit_EverFiledBK_QueryResultValue
            AddHandler PopupContainerEdit_EverFiledBK.QueryDisplayText, AddressOf PopupContainerEdit_EverFiledBK_QueryDisplayText
            AddHandler dtEverFiledBKFiledDate.EditValueChanged, AddressOf dtEverFiledBKFiledDate_EditValueChanged
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler luEverFiledBKChapter.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler LookUpEdit_GenderCD.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler LookUpEdit_RaceCD.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler LookUpEdit_PreferredLanguageCD.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler LookUpEdit_IntakeCreditBureauCD.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler LookUpEdit_MaritalStatusCd.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler LookUpEdit_EducLevelCompletedCD.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler LookUpEdit_Ethnicity.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler milDependent.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler milGrade.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler milService.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler milStatus.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler PopupContainerEdit_Military.QueryDisplayText, AddressOf PopupContainerEdit_Military_QueryDisplayText
            RemoveHandler PopupContainerEdit_Military.QueryResultValue, AddressOf PopupContainerEdit_Military_QueryResultValue
            RemoveHandler ComboBoxEdit_Person.SelectedIndexChanged, AddressOf ComboBoxEdit_Person_SelectedIndexChanged
            RemoveHandler SpinEdit_CreditScore.Spin, AddressOf SpinEdit_CreditScore_Spin
            RemoveHandler SpinEdit_CreditScore.Validating, AddressOf SpinEdit_CreditScore_Validating
            RemoveHandler DateEdit_emp_start_date.ButtonClick, AddressOf DateEdit_emp_start_date_ButtonClick
            RemoveHandler DateEdit_DOB.Validating, AddressOf DateEdit_DOB_Validating
            RemoveHandler PopupContainerEdit_EverFiledBK.QueryResultValue, AddressOf PopupContainerEdit_EverFiledBK_QueryResultValue
            RemoveHandler PopupContainerEdit_EverFiledBK.QueryDisplayText, AddressOf PopupContainerEdit_EverFiledBK_QueryDisplayText
            RemoveHandler dtEverFiledBKFiledDate.EditValueChanged, AddressOf dtEverFiledBKFiledDate_EditValueChanged
        End Sub

        Private privatePersonID As Int32 = 0
        Private OriginalData As DebtPlus.LINQ.Housing_borrower

        ''' <summary>
        ''' For new records, use this as the seed to the PersonID field
        ''' </summary>
        Public Property PersonID As Int32
            Get
                Return privatePersonID
            End Get
            Set(value As Int32)
                privatePersonID = value
            End Set
        End Property

        ''' <summary>
        ''' This is the ID of the record in the database
        ''' </summary>
        <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), DefaultValue(DirectCast(Nothing, System.Object))> _
        Public Property EditValue As System.Nullable(Of Int32)
            Get
                LoadControls()
                Return SaveRecord()
            End Get
            Set(ByVal value As System.Nullable(Of Int32))
                LoadControls()
                OriginalData = ReadRecord(value)
                SetCurrentValues(OriginalData)
            End Set
        End Property

        ''' <summary>
        ''' Process the LOAD sequence for the control
        ''' </summary>
        Private Sub LoadControls()
            If ControlsLoaded Then
                Return
            End If
            ControlsLoaded = True

            UnRegisterHandlers()
            Try
                LookUpEdit_RaceCD.Properties.DataSource = DebtPlus.LINQ.Cache.RaceType.getList()
                LookUpEdit_GenderCD.Properties.DataSource = DebtPlus.LINQ.Cache.GenderType.getList()
                LookUpEdit_PreferredLanguageCD.Properties.DataSource = DebtPlus.LINQ.Cache.AttributeType.getList.FindAll(Function(l) l.Grouping = "LANGUAGE").ToList()
                LookUpEdit_IntakeCreditBureauCD.Properties.DataSource = DebtPlus.LINQ.InMemory.CreditAgencyTypes.getList()
                LookUpEdit_MaritalStatusCd.Properties.DataSource = DebtPlus.LINQ.Cache.MaritalType.getList()
                LookUpEdit_EducLevelCompletedCD.Properties.DataSource = DebtPlus.LINQ.Cache.EducationType.getList()
                milDependent.Properties.DataSource = DebtPlus.LINQ.Cache.MilitaryDependentType.getList()
                milGrade.Properties.DataSource = DebtPlus.LINQ.Cache.MilitaryGradeType.getList()
                milService.Properties.DataSource = DebtPlus.LINQ.Cache.MilitaryServiceType.getList()
                milStatus.Properties.DataSource = DebtPlus.LINQ.Cache.MilitaryStatusType.getList()
                luEverFiledBKChapter.Properties.DataSource = DebtPlus.LINQ.Cache.BankruptcyClassType.getList() ' was redundant item "DebtPlus.LINQ.Cache.BankruptcyChapter.getList()"
                LookUpEdit_Ethnicity.Properties.DataSource = DebtPlus.LINQ.Cache.EthnicityType.getList()
                luPmtCurrent.Properties.DataSource = DebtPlus.LINQ.InMemory.YesNos.getList()
            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Load the editing controls from the current record
        ''' </summary>
        Private Sub SetCurrentValues(ByVal values As DebtPlus.LINQ.Housing_borrower)

            UnRegisterHandlers()
            Try
                EmailRecordControl_EmailID.EditValue = values.EmailID
                NameRecordControl_NameID.EditValue = values.NameID
                TextEdit_Occupation.EditValue = values.other_job
                LookUpEdit_RaceCD.EditValue = values.Race
                LookUpEdit_GenderCD.EditValue = values.Gender
                LookUpEdit_PreferredLanguageCD.EditValue = values.Language
                Ssn1.EditValue = values.SSN
                SpinEdit_CreditScore.EditValue = values.FICO_Score
                LookUpEdit_IntakeCreditBureauCD.EditValue = values.CreditAgency
                CheckEdit_DisabledIND.EditValue = values.Disabled
                TextEdit_MotherMaidenLName.EditValue = values.Former
                LookUpEdit_MaritalStatusCd.EditValue = values.marital_status
                LookUpEdit_EducLevelCompletedCD.EditValue = values.Education
                DateEdit_DOB.EditValue = values.Birthdate
                DateEdit_emp_start_date.EditValue = values.emp_start_date
                DateEdit_emp_end_date.EditValue = values.emp_end_date
                LookUpEdit_Ethnicity.EditValue = values.ethnicity

                milDependent.EditValue = values.MilitaryDependentID
                milGrade.EditValue = values.MilitaryGradeID
                milService.EditValue = values.MilitaryServiceID
                milStatus.EditValue = values.MilitaryStatusID
                PopupContainerEdit_Military.EditValue = values.MilitaryStatusID

                ' Set the person information
                ComboBoxEdit_Person.SelectedIndex = values.PersonID

                If values.bkfileddate.HasValue Then
                    PopupContainerEdit_EverFiledBK.EditValue = values.bkfileddate
                    dtEverFiledBKFiledDate.EditValue = values.bkfileddate
                    dtBkDischargedDate.EditValue = values.bkdischarge
                    luEverFiledBKChapter.EditValue = values.bkchapter
                    luPmtCurrent.EditValue = values.bkPaymentCurrent
                    dtAuthLetter.EditValue = values.bkAuthLetterDate
                Else
                    PopupContainerEdit_EverFiledBK.EditValue = Nothing
                    dtEverFiledBKFiledDate.EditValue = Nothing
                    dtBkDischargedDate.EditValue = Nothing
                    luEverFiledBKChapter.EditValue = Nothing
                    luPmtCurrent.EditValue = Nothing
                    dtAuthLetter.EditValue = Nothing
                End If

                ' Handle the events that were deferred because we were loading the controls now that they are defined
                PopupContainerEdit_EverFiledBK.Text = EverFiledBK_DisplayText(PopupContainerEdit_EverFiledBK.EditValue)
                PopupContainerEdit_Military.Text = Military_DisplayText(values.MilitaryStatusID)
                PopupContainerEdit_Military.EditValue = values.MilitaryStatusID

                ' Enable the controls as needed
                Enable_BankruptcyControls()
                Enable_Controls()

            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Return a formatted "new" record
        ''' </summary>
        Private Function ManufactureNewRecord() As DebtPlus.LINQ.Housing_borrower
            Dim answer As DebtPlus.LINQ.Housing_borrower = DebtPlus.LINQ.Factory.Manufacture_housing_borrower()
            answer.PersonID = privatePersonID
            Return answer
        End Function

        ''' <summary>
        ''' Retrieve the desired records.
        ''' </summary>
        Private Function ReadRecord(ByVal RecordID As System.Nullable(Of Int32)) As DebtPlus.LINQ.Housing_borrower
            Dim NewValues As DebtPlus.LINQ.Housing_borrower = Nothing
            If RecordID.GetValueOrDefault(0) > 0 Then
                Using bc As New DebtPlus.LINQ.BusinessContext()
                    Dim id As Int32 = RecordID.Value
                    NewValues = bc.Housing_borrowers.Where(Function(b) b.Id = id).FirstOrDefault()
                End Using
            End If

            If NewValues Is Nothing Then
                NewValues = ManufactureNewRecord()
                ' Do not set the ID value. Leave it as zero to indicate that the record is "new".
            End If

            Return NewValues
        End Function

        ''' <summary>
        ''' Save the current information to the database
        ''' </summary>
        Private Function SaveRecord() As System.Nullable(Of Int32)
            UnRegisterHandlers()
            Try
                Dim values As DebtPlus.LINQ.Housing_borrower = GetCurrentValues()
                Using bc As New DebtPlus.LINQ.BusinessContext()

                    ' If the record can be located then update the record.
                    If values.Id > 0 Then
                        Dim q As DebtPlus.LINQ.Housing_borrower = bc.Housing_borrowers.Where(Function(s) s.Id = values.Id).FirstOrDefault()
                        If q IsNot Nothing Then
                            q.PersonID = values.PersonID
                            q.NameID = values.NameID
                            q.EmailID = values.EmailID
                            q.Former = values.Former
                            q.SSN = values.SSN
                            q.Gender = values.Gender
                            q.Race = values.Race
                            q.ethnicity = values.ethnicity
                            q.Language = values.Language
                            q.Disabled = values.Disabled
                            q.MilitaryServiceID = values.MilitaryServiceID
                            q.MilitaryDependentID = values.MilitaryDependentID
                            q.MilitaryGradeID = values.MilitaryGradeID
                            q.MilitaryStatusID = values.MilitaryStatusID
                            q.marital_status = values.marital_status
                            q.Education = values.Education
                            q.Birthdate = values.Birthdate
                            q.FICO_Score = values.FICO_Score
                            q.CreditAgency = values.CreditAgency
                            q.emp_start_date = values.emp_start_date
                            q.emp_end_date = values.emp_end_date
                            q.other_job = values.other_job
                            q.bkfileddate = values.bkfileddate
                            q.bkdischarge = values.bkdischarge
                            q.bkchapter = values.bkchapter
                            q.bkPaymentCurrent = values.bkPaymentCurrent
                            q.bkAuthLetterDate = values.bkAuthLetterDate

                            ' Submit the changed dataset to the database
                            bc.SubmitChanges()

                            ' Update the current copy with the database settings
                            OriginalData = values
                            Return OriginalData.Id
                        End If
                    End If

                    ' Add the record to the database. We always have a borrower record when asked
                    ' so if we don't have one now then we need to make one.
                    bc.Housing_borrowers.InsertOnSubmit(values)
                    bc.SubmitChanges()
                End Using

                OriginalData = values
                Return OriginalData.Id

            Finally
                RegisterHandlers()
            End Try
        End Function

        Private Function GetCurrentValues() As DebtPlus.LINQ.Housing_borrower

            ' Get a new (blank) record and initialize it with the current default values.
            Dim record As DebtPlus.LINQ.Housing_borrower = ManufactureNewRecord()
            record.Id = OriginalData.Id ' Here we do want the ID to be the original value.

            ' Take the combo to mean the Person setting. If it is applicant/co-applicant, don't change the record.
            record.PersonID = ComboBoxEdit_Person.SelectedIndex
            If record.PersonID > 0 Then
                Return record
            End If

            ' Retrieve the values from the controls for the new record. These are common with the people table
            DirectCast(record, DebtPlus.Interfaces.Client.IPeople).EmailID = EmailRecordControl_EmailID.EditValue
            DirectCast(record, DebtPlus.Interfaces.Client.IPeople).NameID = NameRecordControl_NameID.EditValue
            DirectCast(record, DebtPlus.Interfaces.Client.IPeople).other_job = DebtPlus.Utils.Nulls.v_String(TextEdit_Occupation.EditValue)
            DirectCast(record, DebtPlus.Interfaces.Client.IPeople).Race = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_RaceCD.EditValue).GetValueOrDefault()
            DirectCast(record, DebtPlus.Interfaces.Client.IPeople).Gender = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_GenderCD.EditValue).GetValueOrDefault()
            DirectCast(record, DebtPlus.Interfaces.Client.IPeople).SSN = DebtPlus.Utils.Nulls.v_String(Ssn1.EditValue)
            DirectCast(record, DebtPlus.Interfaces.Client.IPeople).FICO_Score = DebtPlus.Utils.Nulls.v_Int32(SpinEdit_CreditScore.EditValue)
            DirectCast(record, DebtPlus.Interfaces.Client.IPeople).CreditAgency = DebtPlus.Utils.Nulls.v_String(LookUpEdit_IntakeCreditBureauCD.EditValue)
            DirectCast(record, DebtPlus.Interfaces.Client.IPeople).Disabled = CheckEdit_DisabledIND.Checked
            DirectCast(record, DebtPlus.Interfaces.Client.IPeople).Former = DebtPlus.Utils.Nulls.v_String(TextEdit_MotherMaidenLName.EditValue)
            DirectCast(record, DebtPlus.Interfaces.Client.IPeople).Education = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_EducLevelCompletedCD.EditValue).GetValueOrDefault()
            DirectCast(record, DebtPlus.Interfaces.Client.IPeople).Birthdate = DebtPlus.Utils.Nulls.v_DateTime(DateEdit_DOB.EditValue)
            DirectCast(record, DebtPlus.Interfaces.Client.IPeople).emp_start_date = DebtPlus.Utils.Nulls.v_DateTime(DateEdit_emp_start_date.EditValue)
            DirectCast(record, DebtPlus.Interfaces.Client.IPeople).emp_end_date = DebtPlus.Utils.Nulls.v_DateTime(DateEdit_emp_end_date.EditValue)
            DirectCast(record, DebtPlus.Interfaces.Client.IPeople).Ethnicity = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_Ethnicity.EditValue).GetValueOrDefault()
            DirectCast(record, DebtPlus.Interfaces.Client.IPeople).MilitaryDependentID = DebtPlus.Utils.Nulls.v_Int32(milDependent.EditValue).GetValueOrDefault()
            DirectCast(record, DebtPlus.Interfaces.Client.IPeople).MilitaryGradeID = DebtPlus.Utils.Nulls.v_Int32(milGrade.EditValue).GetValueOrDefault()
            DirectCast(record, DebtPlus.Interfaces.Client.IPeople).MilitaryServiceID = DebtPlus.Utils.Nulls.v_Int32(milService.EditValue).GetValueOrDefault()
            DirectCast(record, DebtPlus.Interfaces.Client.IPeople).MilitaryStatusID = DebtPlus.Utils.Nulls.v_Int32(milStatus.EditValue).GetValueOrDefault(0)
            DirectCast(record, DebtPlus.Interfaces.Client.IPeople).bkfileddate = DebtPlus.Utils.Nulls.v_DateTime(dtEverFiledBKFiledDate.EditValue)
            DirectCast(record, DebtPlus.Interfaces.Client.IPeople).bkdischarge = DebtPlus.Utils.Nulls.v_DateTime(dtBkDischargedDate.EditValue)
            DirectCast(record, DebtPlus.Interfaces.Client.IPeople).bkchapter = DebtPlus.Utils.Nulls.v_Int32(luEverFiledBKChapter.EditValue)

            ' These fields are not in the people table but are in the borrowers table
            record.Language = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_PreferredLanguageCD.EditValue)
            record.marital_status = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_MaritalStatusCd.EditValue)
            record.bkPaymentCurrent = DebtPlus.Utils.Nulls.v_Int32(luPmtCurrent.EditValue)
            record.bkAuthLetterDate = DebtPlus.Utils.Nulls.v_DateTime(dtAuthLetter.EditValue)

            Return record
        End Function

        Private Sub ComboBoxEdit_Person_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
            Enable_Controls()
        End Sub

        Private Sub Enable_Controls()
            Dim Enabled As Boolean = (ComboBoxEdit_Person.SelectedIndex = 0)

            ' First, disable all controls in the form control.
            For Each ctl As Control In LayoutControl1.Controls
                ctl.Enabled = Enabled
            Next

            ' Then enable the combo again.
            ComboBoxEdit_Person.Enabled = True
        End Sub

        ''' <summary>
        ''' Process the spin button for the FICO score to skip the invalids
        ''' </summary>
        Private Sub SpinEdit_CreditScore_Spin(ByVal sender As Object, ByVal e As SpinEventArgs)
            Dim spn As SpinEdit = CType(sender, SpinEdit)

            ' Find the current (old) value so that it may be properly changed.
            Dim old_score As Int32 = DebtPlus.Utils.Nulls.DInt(spn.EditValue)

            ' Scores less than 0 are not allowed
            If old_score <= 0 Then
                spn.EditValue = 0
                e.Handled = True

                ' Scores between 1 and 299 are not allowed. Go the nearest score in the direction traveled.
            ElseIf old_score < 300 OrElse (old_score = 300 And Not e.IsSpinUp) Then
                If e.IsSpinUp Then
                    spn.EditValue = 300
                    e.Handled = True
                Else
                    spn.EditValue = 0
                    e.Handled = True
                End If

                ' Scores greater than 850 are not allowed
            ElseIf old_score >= 850 AndAlso e.IsSpinUp Then
                spn.EditValue = 850
                e.Handled = True
            End If
        End Sub

        ''' <summary>
        ''' Validate a change in the FICO score
        ''' </summary>
        Private Sub SpinEdit_CreditScore_Validating(ByVal sender As Object, ByVal e As CancelEventArgs)
            Dim spn As SpinEdit = CType(sender, SpinEdit)

            Const STR_RangeError As String = "Valid entries range from 300 to 850"
            Dim ErrorMessage As String = String.Empty

            ' Find the old and new values for the fields
            Dim NewValue As Int32 = DebtPlus.Utils.Nulls.DInt(spn.EditValue)

            ' Validate the range for the entry
            If NewValue < 0 Then
                ErrorMessage = STR_RangeError
                e.Cancel = True

            Else

                If NewValue > 0 Then
                    If NewValue < 300 OrElse NewValue > 850 Then
                        ErrorMessage = STR_RangeError
                        e.Cancel = True
                    End If
                End If
            End If

            spn.ErrorText = ErrorMessage
        End Sub

        ''' <summary>
        ''' Validate the date fields
        ''' </summary>
        Private Sub DateEdit_DOB_Validating(ByVal sender As Object, ByVal e As CancelEventArgs)
            Const STR_MustBe1800 As String = "Date must be greater than 1800"
            Dim dt As DateEdit = CType(sender, DateEdit)

            Dim ErrorMessage As String = String.Empty

            ' Generate an error if the person is too young. The birthdate may be missing. That's ok. But if it is given, it must be valid.
            If dt.EditValue IsNot Nothing Then

                ' Validate the date to a minimum level
                Dim NewDate As DateTime = dt.DateTime
                If NewDate.Year < 1800 Then
                    ErrorMessage = STR_MustBe1800
                ElseIf NewDate > DateTime.Now.Date Then
                    ErrorMessage = "Date can not be in the future"

                Else

                    ' If this is a birthdate then check the age to ensure that the person is at least 16 years old.
                    Const INT_MinimumAge As Int32 = 16
                    Dim CompareDate As DateTime = NewDate.AddYears(INT_MinimumAge).Date
                    If CompareDate.CompareTo(Now.Date) > 0 Then
                        ErrorMessage = String.Format("Must be at least {0:f0} years old", INT_MinimumAge)
                    End If
                End If
            End If

            ' Set the error condition for the control
            dt.ErrorText = ErrorMessage
            e.Cancel = (ErrorMessage <> String.Empty)
        End Sub

        ''' <summary>
        ''' Ask the user for the number of years/months
        ''' </summary>
        Private Sub DateEdit_emp_start_date_ButtonClick(ByVal sender As Object, ByVal e As ButtonPressedEventArgs)
            Dim dt As DateEdit = CType(sender, DateEdit)

            If Convert.ToString(e.Button.Tag) = "Edit" Then
                Using frm As New EmploymentDurationForm
                    Dim p As Point = PointToScreen(dt.Location)
                    p.Offset(0, dt.Size.Height)
                    frm.Location = p

                    frm.StartDate = DebtPlus.Utils.Nulls.v_DateTime(dt.EditValue)
                    If frm.ShowDialog() = DialogResult.OK Then
                        dt.EditValue = frm.StartDate
                    End If
                End Using
            End If

        End Sub

        Private Sub PopupContainerEdit_Military_QueryDisplayText(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.QueryDisplayTextEventArgs)
            e.DisplayText = Military_DisplayText(e.EditValue)
        End Sub

        Private Function Military_DisplayText(ByVal value As Object) As String
            If value IsNot Nothing Then
                Dim arg As Int32 = Convert.ToInt32(value)
                Dim q As DebtPlus.LINQ.militaryStatusType = DebtPlus.LINQ.Cache.MilitaryStatusType.getList().Find(Function(s) s.Id = arg)
                If q IsNot Nothing Then
                    Return q.description
                End If
            End If

            Return String.Empty
        End Function

        Private Sub PopupContainerEdit_Military_QueryResultValue(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs)
            PopupContainerEdit_Military.EditValue = milStatus.EditValue
            e.Value = milStatus.EditValue
        End Sub

        Private Sub PopupContainerEdit_EverFiledBK_QueryDisplayText(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.QueryDisplayTextEventArgs)
            e.DisplayText = EverFiledBK_DisplayText(e.EditValue)
        End Sub

        Private Function EverFiledBK_DisplayText(ByVal value As Object) As String
            If dtEverFiledBKFiledDate.EditValue IsNot Nothing Then
                Dim arg As System.DateTime = dtEverFiledBKFiledDate.DateTime
                Return String.Format("{0:d}", arg)
            End If
            Return String.Empty
        End Function

        Private Sub PopupContainerEdit_EverFiledBK_QueryResultValue(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs)
            PopupContainerEdit_EverFiledBK.EditValue = dtEverFiledBKFiledDate.EditValue
            e.Value = dtEverFiledBKFiledDate.EditValue
        End Sub

        Private Sub dtEverFiledBKFiledDate_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Enable_BankruptcyControls()
        End Sub

        Private Sub Enable_BankruptcyControls()
            If dtEverFiledBKFiledDate.EditValue Is Nothing Then
                luEverFiledBKChapter.Enabled = False
                dtBkDischargedDate.Enabled = False
                luPmtCurrent.Enabled = False
                dtAuthLetter.Enabled = False

                luEverFiledBKChapter.EditValue = Nothing
                dtBkDischargedDate.EditValue = Nothing
                luPmtCurrent.EditValue = Nothing
                dtAuthLetter.EditValue = Nothing
            Else
                luEverFiledBKChapter.Enabled = True
                dtBkDischargedDate.Enabled = True
                luPmtCurrent.Enabled = True
                dtAuthLetter.Enabled = True
            End If
        End Sub

    End Class
End Namespace
