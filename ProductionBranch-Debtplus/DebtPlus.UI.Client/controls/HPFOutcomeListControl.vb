﻿#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DebtPlus.Events
Imports DebtPlus.UI.Client.Service
Imports DebtPlus.UI.Client.forms.Housing
Imports DebtPlus.LINQ
Imports DevExpress.Utils
Imports System.Globalization
Imports System.Linq

Namespace controls

    Friend Class HPFOutcomeListControl
        Private colEvents As System.Collections.Generic.List(Of DebtPlus.LINQ.HPFOutcome)
        Private PropertyID As Int32

        Private Class OutcomeFormatter
            Implements System.ICustomFormatter
            Implements System.IFormatProvider

            Public Function Format(format1 As String, arg As Object, formatProvider As System.IFormatProvider) As String Implements System.ICustomFormatter.Format
                If arg IsNot Nothing AndAlso arg IsNot System.DBNull.Value Then
                    Dim intArg As Int32 = Convert.ToInt32(arg)
                    Dim q As DebtPlus.LINQ.HPFOutcomeType = DebtPlus.LINQ.Cache.HPFOutcomeType.getList().Find(Function(s) s.Id = intArg)
                    If q IsNot Nothing Then
                        Return q.description
                    End If
                    Return String.Format("#{0:f0}", intArg)
                End If
                Return String.Empty
            End Function

            Public Function GetFormat(formatType As System.Type) As Object Implements System.IFormatProvider.GetFormat
                Return Me
            End Function
        End Class

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            ' Activate the right click menu
            EnableMenus = True

            ' Set the processing class for the grid display
            GridColumn_OutcomeTypeID.DisplayFormat.Format = New OutcomeFormatter()
            GridColumn_OutcomeTypeID.GroupFormat.Format = New OutcomeFormatter()
        End Sub

        Public Shadows Sub ReadForm(ByVal propertyID As Int32)
            Me.PropertyID = propertyID

            Using bc As New DebtPlus.LINQ.BusinessContext()
                colEvents = bc.HPFOutcomes.Where(Function(s) s.propertyID = propertyID).ToList()
                GridControl1.DataSource = colEvents
            End Using

        End Sub

        Public Overrides Sub SaveForm(bc As DebtPlus.LINQ.BusinessContext)
            ' Don't do anything here. It is not needed.
        End Sub

        ''' <summary>
        ''' Create a new record for the table
        ''' </summary>
        ''' <remarks></remarks>
        Protected Overrides Sub OnCreateItem()

            ' Create the new record
            Dim q As DebtPlus.LINQ.HPFOutcome = DebtPlus.LINQ.Factory.Manufacture_HPFOutcome(PropertyID)

            ' Allow the user to change the settings
            Using frm As New forms.Housing.HPFOutcome(q)
                If frm.ShowDialog() <> DialogResult.OK Then
                    Return
                End If
            End Using

            ' Attempt to insert the new row into the table
            Try
                Using bc As New BusinessContext()
                    bc.HPFOutcomes.InsertOnSubmit(q)
                    bc.SubmitChanges()
                    colEvents.Add(q)
                    GridControl1.RefreshDataSource()
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error inserting new Event")
            End Try

        End Sub

        ''' <summary>
        ''' Edit the current record in the table
        ''' </summary>
        ''' <param name="obj"></param>
        ''' <remarks></remarks>
        Protected Overrides Sub OnEditItem(obj As Object)

            ' Find the record in the tables for the current item
            Dim record As DebtPlus.LINQ.HPFOutcome = TryCast(obj, DebtPlus.LINQ.HPFOutcome)
            If record Is Nothing Then
                Return
            End If

            ' Edit the record to show the new values
            Using frm As New forms.Housing.HPFOutcome(record)
                If frm.ShowDialog() <> DialogResult.OK Then
                    Return
                End If
            End Using

            ' Locate the record in the tables. We need to use the tables in the new business
            ' context since they are tied to the context from which they were accessed.
            Try
                Using bc As New BusinessContext()
                    Dim q As DebtPlus.LINQ.HPFOutcome = bc.HPFOutcomes.Where(Function(s) s.Id = record.Id).FirstOrDefault()
                    If q Is Nothing Then
                        Return
                    End If

                    ' Update the record in the tables
                    q.NonProfitReferralName = record.NonProfitReferralName
                    q.outcomeTypeID = record.outcomeTypeID
                    q.extRefOtherName = record.extRefOtherName

                    bc.SubmitChanges()

                    ' Refresh the grid contents
                    GridControl1.RefreshDataSource()
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating existing Event")
            End Try
        End Sub

        ''' <summary>
        ''' Delete the current record in the table
        ''' </summary>
        ''' <param name="obj"></param>
        ''' <remarks></remarks>
        Protected Overrides Sub OnDeleteItem(obj As Object)

            ' Find the record in the tables for the current item
            Dim record As DebtPlus.LINQ.HPFOutcome = TryCast(obj, DebtPlus.LINQ.HPFOutcome)
            If record Is Nothing Then
                Return
            End If

            If DebtPlus.Data.Prompts.RequestConfirmation_Delete() <> DialogResult.Yes Then
                Return
            End If

            Try
                Using bc As New BusinessContext()
                    Dim q As DebtPlus.LINQ.HPFOutcome = bc.HPFOutcomes.Where(Function(s) s.Id = record.Id).FirstOrDefault()
                    If q Is Nothing Then
                        Return
                    End If

                    ' Delete the row
                    bc.HPFOutcomes.DeleteOnSubmit(q)
                    bc.SubmitChanges()

                    ' Refresh the grid contents
                    colEvents.Remove(record)
                    GridControl1.RefreshDataSource()
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error deleting existing Event")
            End Try

        End Sub
    End Class
End Namespace
