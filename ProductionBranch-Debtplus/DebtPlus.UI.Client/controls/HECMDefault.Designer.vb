Imports DebtPlus.UI.Client.controls

Namespace controls
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class HECMDefault

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.GridColumn_Budget = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Budget.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_date_created = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_created_by = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_created_by.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridView1
            '
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_Budget, Me.GridColumn_date_created, Me.GridColumn_created_by})
            '
            'GridColumn_Budget
            '
            Me.GridColumn_Budget.Caption = "Referral ID"
            Me.GridColumn_Budget.CustomizationCaption = "Referral ID"
            Me.GridColumn_Budget.FieldName = "Id"
            Me.GridColumn_Budget.Name = "GridColumn_ReferralID"
            Me.GridColumn_Budget.Visible = True
            Me.GridColumn_Budget.VisibleIndex = 0
            '
            'GridColumn_date_created
            '
            Me.GridColumn_date_created.Caption = "Date Created"
            Me.GridColumn_date_created.CustomizationCaption = "Date Created"
            Me.GridColumn_date_created.FieldName = "date_created"
            Me.GridColumn_date_created.Name = "GridColumn_date_created"
            Me.GridColumn_date_created.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_created.DisplayFormat.FormatString = "{0:d} {0:t}"
            Me.GridColumn_date_created.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_created.GroupFormat.FormatString = "{0:d}"
            Me.GridColumn_date_created.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value
            Me.GridColumn_date_created.Visible = True
            Me.GridColumn_date_created.VisibleIndex = 1
            '
            'GridColumn_created_by
            '
            Me.GridColumn_created_by.Caption = "Created By"
            Me.GridColumn_created_by.CustomizationCaption = "Created By"
            Me.GridColumn_created_by.FieldName = "created_by"
            Me.GridColumn_created_by.Name = "GridColumn_created_by"
            Me.GridColumn_created_by.Visible = True
            Me.GridColumn_created_by.VisibleIndex = 2
            '
            'Page_Budgets
            '
            Me.Name = "Page_HECMDefault"
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
        End Sub

        Friend WithEvents GridColumn_Budget As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_date_created As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_created_by As DevExpress.XtraGrid.Columns.GridColumn
    End Class
End Namespace
