#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict Off

Imports System.ComponentModel
Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.Data.Controls
Imports DevExpress.XtraEditors
Imports DebtPlus.UI.Client.forms.Employer
Imports DebtPlus.UI.Client.forms
Imports DebtPlus.LINQ
Imports System.Linq

Namespace controls

    Friend Class JobControl
        Inherits ControlBaseClient

        Public Class RecordSystemNoteArgs
            Inherits EventArgs
            Public Subject As String
            Public Note As String

            Public Sub New(ByVal Subject As String, ByVal Note As String)
                MyBase.New()
                Me.Subject = Subject
                Me.Note = Note
            End Sub
        End Class

        Public Event RecordSystemNote(ByVal Sender As Object, ByVal e As RecordSystemNoteArgs)

        Private Sub RegisterHandlers()
            AddHandler DateEdit_emp_start_date.ButtonPressed, AddressOf ShowPopupForm
            AddHandler employer_name.ButtonClick, AddressOf employer_name_ButtonClick
            AddHandler TelephoneNumberRecordControl1.TelephoneNumberChanged, AddressOf TelephoneNumberRecordControl_Changed
            AddHandler TelephoneNumberRecordControl2.TelephoneNumberChanged, AddressOf TelephoneNumberRecordControl_Changed
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler DateEdit_emp_start_date.ButtonPressed, AddressOf ShowPopupForm
            RemoveHandler employer_name.ButtonClick, AddressOf employer_name_ButtonClick
            RemoveHandler TelephoneNumberRecordControl1.TelephoneNumberChanged, AddressOf TelephoneNumberRecordControl_Changed
            RemoveHandler TelephoneNumberRecordControl2.TelephoneNumberChanged, AddressOf TelephoneNumberRecordControl_Changed
        End Sub

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        ' Employer for the current item
        Private Property currentEmployer As System.Nullable(Of Int32)

#Region " Windows Form Designer generated code "

        'UserControl overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents employer_name As DevExpress.XtraEditors.ButtonEdit
        Friend WithEvents person_job As DevExpress.XtraEditors.ComboBoxEdit
        Friend WithEvents gross_income As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents net_income As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LookUpEdit_PayFreq As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents TelephoneNumberRecordControl2 As DebtPlus.Data.Controls.TelephoneNumberRecordControl
        Friend WithEvents TelephoneNumberRecordControl1 As DebtPlus.Data.Controls.TelephoneNumberRecordControl
        Friend WithEvents DateEdit_emp_start_date As DevExpress.XtraEditors.DateEdit
        Friend WithEvents LayoutControlItem_Employer As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem_Job As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem_PayFreq As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem_GrossIncome As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem_NetIncome As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents item0 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents item1 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents DateEdit_emp_end_date As DevExpress.XtraEditors.DateEdit

        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
            Dim SerializableAppearanceObject2 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
            Me.employer_name = New DevExpress.XtraEditors.ButtonEdit()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.DateEdit_emp_start_date = New DevExpress.XtraEditors.DateEdit()
            Me.DateEdit_emp_end_date = New DevExpress.XtraEditors.DateEdit()
            Me.TelephoneNumberRecordControl1 = New DebtPlus.Data.Controls.TelephoneNumberRecordControl()
            Me.LookUpEdit_PayFreq = New DevExpress.XtraEditors.LookUpEdit()
            Me.person_job = New DevExpress.XtraEditors.ComboBoxEdit()
            Me.TelephoneNumberRecordControl2 = New DebtPlus.Data.Controls.TelephoneNumberRecordControl()
            Me.net_income = New DevExpress.XtraEditors.CalcEdit()
            Me.gross_income = New DevExpress.XtraEditors.CalcEdit()
            Me.LayoutControlItem_PayFreq = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem_Employer = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_Job = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_GrossIncome = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_NetIncome = New DevExpress.XtraLayout.LayoutControlItem()
            Me.item0 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.item1 = New DevExpress.XtraLayout.EmptySpaceItem()
            CType(Me.employer_name.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.DateEdit_emp_start_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit_emp_start_date.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit_emp_end_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit_emp_end_date.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TelephoneNumberRecordControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_PayFreq.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.person_job.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TelephoneNumberRecordControl2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.net_income.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.gross_income.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_PayFreq, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_Employer, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_Job, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_GrossIncome, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_NetIncome, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.item0, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.item1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'employer_name
            '
            Me.employer_name.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.employer_name.Location = New System.Drawing.Point(87, 6)
            Me.employer_name.Name = "employer_name"
            Me.employer_name.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.SpinUp)})
            Me.employer_name.Properties.ReadOnly = True
            Me.employer_name.Size = New System.Drawing.Size(206, 20)
            Me.employer_name.StyleController = Me.LayoutControl1
            Me.employer_name.TabIndex = 1
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.DateEdit_emp_start_date)
            Me.LayoutControl1.Controls.Add(Me.DateEdit_emp_end_date)
            Me.LayoutControl1.Controls.Add(Me.TelephoneNumberRecordControl1)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_PayFreq)
            Me.LayoutControl1.Controls.Add(Me.employer_name)
            Me.LayoutControl1.Controls.Add(Me.person_job)
            Me.LayoutControl1.Controls.Add(Me.TelephoneNumberRecordControl2)
            Me.LayoutControl1.Controls.Add(Me.net_income)
            Me.LayoutControl1.Controls.Add(Me.gross_income)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.HiddenItems.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem_PayFreq})
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Margin = New System.Windows.Forms.Padding(0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(388, 275, 387, 350)
            Me.LayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = True
            Me.LayoutControl1.OptionsFocus.MoveFocusDirection = DevExpress.XtraLayout.MoveFocusDirection.DownThenAcross
            Me.LayoutControl1.OptionsSerialization.RestoreAppearanceItemCaption = True
            Me.LayoutControl1.OptionsSerialization.RestoreAppearanceTabPage = True
            Me.LayoutControl1.OptionsSerialization.RestoreGroupPadding = True
            Me.LayoutControl1.OptionsSerialization.RestoreGroupSpacing = True
            Me.LayoutControl1.OptionsSerialization.RestoreLayoutGroupAppearanceGroup = True
            Me.LayoutControl1.OptionsSerialization.RestoreLayoutItemPadding = True
            Me.LayoutControl1.OptionsSerialization.RestoreLayoutItemSpacing = True
            Me.LayoutControl1.OptionsSerialization.RestoreRootGroupPadding = True
            Me.LayoutControl1.OptionsSerialization.RestoreRootGroupSpacing = True
            Me.LayoutControl1.OptionsSerialization.RestoreTabbedGroupPadding = True
            Me.LayoutControl1.OptionsSerialization.RestoreTabbedGroupSpacing = True
            Me.LayoutControl1.OptionsSerialization.RestoreTextToControlDistance = True
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(499, 104)
            Me.LayoutControl1.TabIndex = 12
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'DateEdit_emp_start_date
            '
            Me.DateEdit_emp_start_date.EditValue = Nothing
            Me.DateEdit_emp_start_date.Location = New System.Drawing.Point(378, 6)
            Me.DateEdit_emp_start_date.Name = "DateEdit_emp_start_date"
            Me.DateEdit_emp_start_date.Properties.ActionButtonIndex = 1
            Me.DateEdit_emp_start_date.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.DateEdit_emp_start_date.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Up, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1, "Click here to enter number of years, months of employment to date", "Edit", Nothing, True), New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject2, "", "DropDown", Nothing, True)})
            Me.DateEdit_emp_start_date.Properties.DisplayFormat.FormatString = "MMM yyyy"
            Me.DateEdit_emp_start_date.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.DateEdit_emp_start_date.Properties.NullValuePrompt = "Should be given"
            Me.DateEdit_emp_start_date.Properties.NullValuePromptShowForEmptyValue = True
            Me.DateEdit_emp_start_date.Properties.ValidateOnEnterKey = True
            Me.DateEdit_emp_start_date.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.[False]
            Me.DateEdit_emp_start_date.Properties.VistaEditTime = DevExpress.Utils.DefaultBoolean.[False]
            Me.DateEdit_emp_start_date.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.DateEdit_emp_start_date.Size = New System.Drawing.Size(115, 20)
            Me.DateEdit_emp_start_date.StyleController = Me.LayoutControl1
            Me.DateEdit_emp_start_date.TabIndex = 16
            Me.DateEdit_emp_start_date.ToolTip = "Starting date for the current employer. We need to determine how long the person " & _
        "has been ""on the job"" for HUD reports and others."
            '
            'DateEdit_emp_end_date
            '
            Me.DateEdit_emp_end_date.EditValue = Nothing
            Me.DateEdit_emp_end_date.Location = New System.Drawing.Point(378, 30)
            Me.DateEdit_emp_end_date.Name = "DateEdit_emp_end_date"
            Me.DateEdit_emp_end_date.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.DateEdit_emp_end_date.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.DateEdit_emp_end_date.Properties.DisplayFormat.FormatString = "MMM yyyy"
            Me.DateEdit_emp_end_date.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.DateEdit_emp_end_date.Properties.NullValuePrompt = "Still Employed"
            Me.DateEdit_emp_end_date.Properties.NullValuePromptShowForEmptyValue = True
            Me.DateEdit_emp_end_date.Properties.ValidateOnEnterKey = True
            Me.DateEdit_emp_end_date.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.[False]
            Me.DateEdit_emp_end_date.Properties.VistaEditTime = DevExpress.Utils.DefaultBoolean.[False]
            Me.DateEdit_emp_end_date.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.DateEdit_emp_end_date.Size = New System.Drawing.Size(115, 20)
            Me.DateEdit_emp_end_date.StyleController = Me.LayoutControl1
            Me.DateEdit_emp_end_date.TabIndex = 15
            Me.DateEdit_emp_end_date.ToolTip = "If the person no longer works at this job, what was the date that the employment " & _
        "was terminated?"
            '
            'TelephoneNumberRecordControl1
            '
            Me.TelephoneNumberRecordControl1.ErrorIcon = Nothing
            Me.TelephoneNumberRecordControl1.ErrorText = ""
            Me.TelephoneNumberRecordControl1.Location = New System.Drawing.Point(378, 54)
            Me.TelephoneNumberRecordControl1.Margin = New System.Windows.Forms.Padding(0)
            Me.TelephoneNumberRecordControl1.Name = "TelephoneNumberRecordControl1"
            Me.TelephoneNumberRecordControl1.Size = New System.Drawing.Size(115, 20)
            Me.TelephoneNumberRecordControl1.TabIndex = 13
            '
            'LookUpEdit_PayFreq
            '
            Me.LookUpEdit_PayFreq.Location = New System.Drawing.Point(88, 102)
            Me.LookUpEdit_PayFreq.Name = "LookUpEdit_PayFreq"
            Me.LookUpEdit_PayFreq.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_PayFreq.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 5, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_PayFreq.Properties.DisplayMember = "description"
            Me.LookUpEdit_PayFreq.Properties.NullText = ""
            Me.LookUpEdit_PayFreq.Properties.ShowFooter = False
            Me.LookUpEdit_PayFreq.Properties.ShowHeader = False
            Me.LookUpEdit_PayFreq.Properties.SortColumnIndex = 1
            Me.LookUpEdit_PayFreq.Properties.ValueMember = "Id"
            Me.LookUpEdit_PayFreq.Size = New System.Drawing.Size(85, 20)
            Me.LookUpEdit_PayFreq.StyleController = Me.LayoutControl1
            Me.LookUpEdit_PayFreq.TabIndex = 12
            '
            'person_job
            '
            Me.person_job.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.person_job.Location = New System.Drawing.Point(87, 30)
            Me.person_job.Name = "person_job"
            Me.person_job.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.person_job.Size = New System.Drawing.Size(206, 20)
            Me.person_job.StyleController = Me.LayoutControl1
            Me.person_job.TabIndex = 3
            '
            'TelephoneNumberRecordControl2
            '
            Me.TelephoneNumberRecordControl2.ErrorIcon = Nothing
            Me.TelephoneNumberRecordControl2.ErrorText = ""
            Me.TelephoneNumberRecordControl2.Location = New System.Drawing.Point(378, 78)
            Me.TelephoneNumberRecordControl2.Margin = New System.Windows.Forms.Padding(0)
            Me.TelephoneNumberRecordControl2.Name = "TelephoneNumberRecordControl2"
            Me.TelephoneNumberRecordControl2.Size = New System.Drawing.Size(115, 20)
            Me.TelephoneNumberRecordControl2.TabIndex = 14
            '
            'net_income
            '
            Me.net_income.Location = New System.Drawing.Point(87, 78)
            Me.net_income.Name = "net_income"
            Me.net_income.Properties.Appearance.Options.UseTextOptions = True
            Me.net_income.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.net_income.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.net_income.Properties.DisplayFormat.FormatString = "c"
            Me.net_income.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.net_income.Properties.EditFormat.FormatString = "F2"
            Me.net_income.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.net_income.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong
            Me.net_income.Properties.Mask.BeepOnError = True
            Me.net_income.Properties.Mask.EditMask = "c2"
            Me.net_income.Properties.Mask.SaveLiteral = False
            Me.net_income.Properties.Mask.ShowPlaceHolders = False
            Me.net_income.Size = New System.Drawing.Size(86, 20)
            Me.net_income.StyleController = Me.LayoutControl1
            Me.net_income.TabIndex = 9
            '
            'gross_income
            '
            Me.gross_income.Location = New System.Drawing.Point(87, 54)
            Me.gross_income.Name = "gross_income"
            Me.gross_income.Properties.Appearance.Options.UseTextOptions = True
            Me.gross_income.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.gross_income.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.gross_income.Properties.DisplayFormat.FormatString = "c"
            Me.gross_income.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.gross_income.Properties.EditFormat.FormatString = "F2"
            Me.gross_income.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.gross_income.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong
            Me.gross_income.Properties.Mask.BeepOnError = True
            Me.gross_income.Properties.Mask.EditMask = "c2"
            Me.gross_income.Properties.Mask.SaveLiteral = False
            Me.gross_income.Properties.Mask.ShowPlaceHolders = False
            Me.gross_income.Size = New System.Drawing.Size(86, 20)
            Me.gross_income.StyleController = Me.LayoutControl1
            Me.gross_income.TabIndex = 5
            '
            'LayoutControlItem_PayFreq
            '
            Me.LayoutControlItem_PayFreq.Control = Me.LookUpEdit_PayFreq
            Me.LayoutControlItem_PayFreq.CustomizationFormText = "Frequency"
            Me.LayoutControlItem_PayFreq.Location = New System.Drawing.Point(0, 96)
            Me.LayoutControlItem_PayFreq.Name = "LayoutControlItem_PayFreq"
            Me.LayoutControlItem_PayFreq.Size = New System.Drawing.Size(171, 26)
            Me.LayoutControlItem_PayFreq.Text = "Frequency"
            Me.LayoutControlItem_PayFreq.TextSize = New System.Drawing.Size(78, 13)
            Me.LayoutControlItem_PayFreq.TextToControlDistance = 5
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem_Employer, Me.LayoutControlItem_Job, Me.LayoutControlItem4, Me.LayoutControlItem3, Me.LayoutControlItem2, Me.LayoutControlItem1, Me.LayoutControlItem_GrossIncome, Me.LayoutControlItem_NetIncome, Me.item0, Me.item1})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "Root"
            Me.LayoutControlGroup1.Padding = New DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3)
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(499, 104)
            Me.LayoutControlGroup1.Text = "Root"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem_Employer
            '
            Me.LayoutControlItem_Employer.Control = Me.employer_name
            Me.LayoutControlItem_Employer.CustomizationFormText = "Employer"
            Me.LayoutControlItem_Employer.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem_Employer.Name = "LayoutControlItem_Employer"
            Me.LayoutControlItem_Employer.Size = New System.Drawing.Size(291, 24)
            Me.LayoutControlItem_Employer.Text = "Employer"
            Me.LayoutControlItem_Employer.TextSize = New System.Drawing.Size(78, 13)
            '
            'LayoutControlItem_Job
            '
            Me.LayoutControlItem_Job.Control = Me.person_job
            Me.LayoutControlItem_Job.CustomizationFormText = "Job"
            Me.LayoutControlItem_Job.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem_Job.Name = "LayoutControlItem_Job"
            Me.LayoutControlItem_Job.Size = New System.Drawing.Size(291, 24)
            Me.LayoutControlItem_Job.Text = "Job"
            Me.LayoutControlItem_Job.TextSize = New System.Drawing.Size(78, 13)
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.DateEdit_emp_start_date
            Me.LayoutControlItem4.CustomizationFormText = "Start Date"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(291, 0)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(200, 24)
            Me.LayoutControlItem4.Text = "Hire Date"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(78, 13)
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.DateEdit_emp_end_date
            Me.LayoutControlItem3.CustomizationFormText = "End Date"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(291, 24)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(200, 24)
            Me.LayoutControlItem3.Text = "Term Date"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(78, 13)
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.TelephoneNumberRecordControl2
            Me.LayoutControlItem2.CustomizationFormText = "Cell Telephone"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(291, 72)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(200, 24)
            Me.LayoutControlItem2.Text = "Cell Telephone"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(78, 13)
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.TelephoneNumberRecordControl1
            Me.LayoutControlItem1.CustomizationFormText = "Work Telephone"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(291, 48)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(200, 24)
            Me.LayoutControlItem1.Text = "Work Telephone"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(78, 13)
            '
            'LayoutControlItem_GrossIncome
            '
            Me.LayoutControlItem_GrossIncome.Control = Me.gross_income
            Me.LayoutControlItem_GrossIncome.CustomizationFormText = "Gross Income"
            Me.LayoutControlItem_GrossIncome.Location = New System.Drawing.Point(0, 48)
            Me.LayoutControlItem_GrossIncome.Name = "LayoutControlItem_GrossIncome"
            Me.LayoutControlItem_GrossIncome.Size = New System.Drawing.Size(171, 24)
            Me.LayoutControlItem_GrossIncome.Text = "Gross Income"
            Me.LayoutControlItem_GrossIncome.TextSize = New System.Drawing.Size(78, 13)
            '
            'LayoutControlItem_NetIncome
            '
            Me.LayoutControlItem_NetIncome.Control = Me.net_income
            Me.LayoutControlItem_NetIncome.CustomizationFormText = "Net Income"
            Me.LayoutControlItem_NetIncome.Location = New System.Drawing.Point(0, 72)
            Me.LayoutControlItem_NetIncome.Name = "LayoutControlItem_NetIncome"
            Me.LayoutControlItem_NetIncome.Size = New System.Drawing.Size(171, 24)
            Me.LayoutControlItem_NetIncome.Text = "Net Income"
            Me.LayoutControlItem_NetIncome.TextSize = New System.Drawing.Size(78, 13)
            '
            'item0
            '
            Me.item0.AllowHotTrack = False
            Me.item0.CustomizationFormText = "item0"
            Me.item0.Location = New System.Drawing.Point(171, 48)
            Me.item0.Name = "item0"
            Me.item0.Size = New System.Drawing.Size(120, 24)
            Me.item0.Text = "item0"
            Me.item0.TextSize = New System.Drawing.Size(0, 0)
            '
            'item1
            '
            Me.item1.AllowHotTrack = False
            Me.item1.CustomizationFormText = "item1"
            Me.item1.Location = New System.Drawing.Point(171, 72)
            Me.item1.Name = "item1"
            Me.item1.Size = New System.Drawing.Size(120, 24)
            Me.item1.Text = "item1"
            Me.item1.TextSize = New System.Drawing.Size(0, 0)
            '
            'JobControl
            '
            Me.Controls.Add(Me.LayoutControl1)
            Me.Margin = New System.Windows.Forms.Padding(0)
            Me.Name = "JobControl"
            Me.Size = New System.Drawing.Size(499, 104)
            CType(Me.employer_name.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.DateEdit_emp_start_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit_emp_start_date.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit_emp_end_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit_emp_end_date.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TelephoneNumberRecordControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_PayFreq.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.person_job.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TelephoneNumberRecordControl2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.net_income.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.gross_income.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_PayFreq, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_Employer, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_Job, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_GrossIncome, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_NetIncome, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.item0, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.item1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        ''' <summary>
        ''' Handle the condition where the telephone number is changed
        ''' </summary>
        Private Sub TelephoneNumberRecordControl_Changed(ByVal Sender As Object, ByVal e As DebtPlus.Events.TelephoneNumberChangedEventArgs)
            Dim Item As String = If(Sender Is TelephoneNumberRecordControl1, "Work", "Cell")
            RaiseEvent RecordSystemNote(Me, New RecordSystemNoteArgs("Changed {0} " + Item + " telephone number", String.Format("Changed {0} telephone number from ""{1}"" to ""{2}""", Item, e.OldTelephoneNumber.ToString(), e.NewTelephoneNumber.ToString())))
        End Sub

        ''' <summary>
        ''' Read and display the information on the form
        ''' </summary>
        Friend Overloads Sub ReadForm(bc As BusinessContext, peopleRecord As DebtPlus.LINQ.people)

            UnRegisterHandlers()
            Try
                ' Set the lookup controls' data-source
                LookUpEdit_PayFreq.Properties.DataSource = DebtPlus.LINQ.Cache.PayFrequencyType.getList()

                ' Define the list of standard jobs as needed
                person_job.Properties.Sorted = True
                person_job.Properties.Items.Clear()
                For Each item As DebtPlus.LINQ.job_description In DebtPlus.LINQ.Cache.job_description.getList()
                    person_job.Properties.Items.Add(New DebtPlus.Data.Controls.ComboboxItem(item.description, item.Id, True))
                Next

                ' Set the job for the person based upon the values in "job" and "other_job"
                Dim jobId As System.Nullable(Of Int32) = peopleRecord.job
                person_job.SelectedIndex = -1
                If jobId.HasValue Then
                    Dim selectedItem As DebtPlus.Data.Controls.ComboboxItem = Nothing
                    For Each itemInList As DebtPlus.Data.Controls.ComboboxItem In person_job.Properties.Items
                        If jobId.Value.Equals(DirectCast(itemInList.value, Int32)) Then
                            person_job.SelectedItem = itemInList
                            Exit For
                        End If
                    Next
                End If

                If person_job.SelectedIndex < 0 Then
                    person_job.Text = peopleRecord.other_job
                End If

                ' Find the name of the employer for this record
                If System.Nullable.Compare(Of System.Int32)(peopleRecord.employer, currentEmployer) <> 0 Then
                    currentEmployer = peopleRecord.employer
                    DisplayEmployer(currentEmployer)
                End If

                ' Load the others as simple bound items.
                gross_income.EditValue = peopleRecord.gross_income
                net_income.EditValue = peopleRecord.net_income
                DateEdit_emp_start_date.EditValue = peopleRecord.emp_start_date
                DateEdit_emp_end_date.EditValue = peopleRecord.emp_end_date
                LookUpEdit_PayFreq.EditValue = peopleRecord.Frequency

                TelephoneNumberRecordControl1.EditValue = peopleRecord.WorkTelephoneID
                TelephoneNumberRecordControl2.EditValue = peopleRecord.CellTelephoneID

                Dim doNotDisturb As Boolean

                Try
                    doNotDisturb = bc.clients.Where(Function(c) c.Id = peopleRecord.Client).First().doNotDisturb

                    MaskPhoneNumbers(doNotDisturb)

                Catch ex As Exception

                End Try

            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Save any changes to the control
        ''' </summary>
        Friend Overloads Sub SaveForm(bc As BusinessContext, peopleRecord As DebtPlus.LINQ.people)
            MyBase.SaveForm(bc)

            ' We need working copies that we can twiddle with which don't trip the "changed" status in the record.
            Dim newJob As System.Nullable(Of System.Int32) = Nothing
            Dim newOtherJob As String = Nothing

            ' The job is either in the "job" field or the "other_job" field. It should never be in both.
            Dim selectedJob As DebtPlus.Data.Controls.ComboboxItem = TryCast(person_job.SelectedItem, DebtPlus.Data.Controls.ComboboxItem)
            If selectedJob IsNot Nothing Then
                newJob = DirectCast(selectedJob.value, Int32)
                newOtherJob = Nothing
            Else
                newJob = Nothing
                newOtherJob = person_job.Text.Trim()

                ' Record a totally blank job field as being null for job and null for other_job
                If String.IsNullOrEmpty(newOtherJob) Then
                    newOtherJob = Nothing
                End If
            End If

            peopleRecord.job = newJob
            peopleRecord.other_job = newOtherJob

            ' Everything else is straight forward.
            peopleRecord.gross_income = DebtPlus.Utils.Nulls.v_Decimal(gross_income.EditValue).GetValueOrDefault()
            peopleRecord.net_income = DebtPlus.Utils.Nulls.v_Decimal(net_income.EditValue).GetValueOrDefault()
            peopleRecord.emp_start_date = DebtPlus.Utils.Nulls.v_DateTime(DateEdit_emp_start_date.EditValue)
            peopleRecord.emp_end_date = DebtPlus.Utils.Nulls.v_DateTime(DateEdit_emp_end_date.EditValue)
            peopleRecord.Frequency = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_PayFreq.EditValue).GetValueOrDefault(4)
            peopleRecord.WorkTelephoneID = TelephoneNumberRecordControl1.EditValue
            peopleRecord.CellTelephoneID = TelephoneNumberRecordControl2.EditValue
            peopleRecord.employer = currentEmployer
        End Sub

        ''' <summary>
        ''' Find the new employer
        ''' </summary>
        Private Sub employer_name_ButtonClick(ByVal sender As Object, ByVal e As ButtonPressedEventArgs)

            Using frm As New ListForm()
                frm.Employer = currentEmployer
                If frm.ShowDialog() = DialogResult.OK Then
                    currentEmployer = frm.Employer
                    DisplayEmployer(currentEmployer)
                End If
            End Using
        End Sub

        ''' <summary>
        ''' Update the control with the name of the employer
        ''' </summary>
        Private Sub DisplayEmployer(ByVal currentEmployer As System.Nullable(Of Int32))

            If currentEmployer.HasValue Then
                Using bc As New BusinessContext()
                    Dim q As DebtPlus.LINQ.employer = bc.employers.Where(Function(s) s.Id = currentEmployer.Value).FirstOrDefault()
                    If q IsNot Nothing Then
                        employer_name.Text = q.name
                        Return
                    End If
                End Using
            End If

            employer_name.Text = String.Empty
        End Sub

        Private Function getEmployerName(ByVal employerID As Int32) As String

            Try
                Using bc As New BusinessContext()
                    Dim q As DebtPlus.LINQ.employer = bc.employers.Where(Function(s) s.Id = employerID).FirstOrDefault()
                    If q IsNot Nothing Then
                        Return q.name
                    End If
                End Using

            Catch ex As SqlClient.SqlException
                Return ex.Message
            End Try

            Return String.Empty
        End Function

        ''' <summary>
        ''' Ask the user for the number of years/months
        ''' </summary>
        Private Sub ShowPopupForm(ByVal sender As Object, ByVal e As ButtonPressedEventArgs)

            If Convert.ToString(e.Button.Tag) = "Edit" Then
                Using frm As New EmploymentDurationForm()

                    ' Find the point just below the control. It is the location for the form.
                    Dim p As Point = PointToScreen(DateEdit_emp_start_date.Location)
                    p.Offset(0, DateEdit_emp_start_date.Size.Height)
                    frm.Location = p

                    frm.StartDate = DebtPlus.Utils.Nulls.v_DateTime(DateEdit_emp_start_date.EditValue)
                    If frm.ShowDialog() = DialogResult.OK Then
                        DateEdit_emp_start_date.DateTime = frm.StartDate
                    End If
                End Using
            End If
        End Sub

        Private Sub MaskPhoneNumbers(doNotDisturb As Boolean)

            If doNotDisturb = True Then
                TelephoneNumberRecordControl1.MaskPhone()
                TelephoneNumberRecordControl2.MaskPhone()

                ''TelephoneNumberRecordControl_HomeTelephoneID.Enabled = False
                ''TelephoneNumberRecordControl_MsgTelephoneID.Enabled = False
            End If
        End Sub
    End Class
End Namespace
