﻿Imports DebtPlus.LINQ
Imports System.Linq
Imports System
Imports System.Windows.Forms

Namespace controls
    Friend Class POAControl
        Inherits ControlBaseClient
        Implements DevExpress.Utils.Controls.IXtraResizableControl

        ''' <summary>
        ''' Current values that are being edited in this control.
        ''' </summary>
        ''' <remarks></remarks>
        Dim currentValues As DebtPlus.LINQ.client_poa = Nothing

        ''' <summary>
        ''' Event to record a system note for the applicant or co-applicant
        ''' </summary>
        ''' <param name="subject">Text for the subject line</param>
        ''' <param name="note">Text for the note</param>
        ''' <remarks></remarks>
        Public Event RecordNote(subject As String, note As String)

        ''' <summary>
        ''' Raise the RecordNote event
        ''' </summary>
        ''' <param name="subject">Text for the subject line</param>
        ''' <param name="note">Text for the note</param>
        ''' <remarks></remarks>
        Protected Sub RaiseRecordNote(subject As String, note As String)
            RaiseEvent RecordNote(subject, note)
        End Sub

        ''' <summary>
        ''' Raise the RecordNote event
        ''' </summary>
        ''' <param name="subject">Text for the subject line</param>
        ''' <param name="note">Text for the note</param>
        ''' <remarks></remarks>
        Protected Overridable Sub OnRecordNote(subject As String, note As String)
            RaiseRecordNote(subject, note)
        End Sub

        ''' <summary>
        ''' Initialize a new instance of the class
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            ' do not try to register the handlers here because the designer will trap due to
            ' the fact that the popup containers will want the text and that takes a database access
            ' which won't be setup until later. Just let the readform do the registration. It will be called in due time.
        End Sub

        ''' <summary>
        ''' Register the event handlers
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub RegisterHandlers()
            AddHandler LookUpEdit_POA_Stage.EditValueChanged, AddressOf LookUpEdit_POA_Stage_EditValueChanged
            AddHandler LookUpEdit_POA_Stage.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler POA_Telephone_RecordControl.TelephoneNumberChanged, AddressOf POA_Telephone_RecordControl_Changed
            AddHandler PopupContainerEdit_POA_Approval.QueryDisplayText, AddressOf PopupContainerEdit_POA_Approval_QueryDisplayText
            AddHandler PopupContainerEdit_POA_ID.QueryDisplayText, AddressOf PopupContainerEdit_POA_ID_QueryDisplayText
            AddHandler PopupContainerEdit_POA_Phone.QueryDisplayText, AddressOf PopupContainerEdit_POA_Phone_QueryDisplayText
            AddHandler NameRecordControl_POA.NameChanged, AddressOf NameRecordControl_POA_NameChanged
        End Sub

        ''' <summary>
        ''' Remove the event handler registrations
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub UnRegisterHandlers()
            RemoveHandler LookUpEdit_POA_Stage.EditValueChanged, AddressOf LookUpEdit_POA_Stage_EditValueChanged
            RemoveHandler LookUpEdit_POA_Stage.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler POA_Telephone_RecordControl.TelephoneNumberChanged, AddressOf POA_Telephone_RecordControl_Changed
            RemoveHandler PopupContainerEdit_POA_Approval.QueryDisplayText, AddressOf PopupContainerEdit_POA_Approval_QueryDisplayText
            RemoveHandler PopupContainerEdit_POA_ID.QueryDisplayText, AddressOf PopupContainerEdit_POA_ID_QueryDisplayText
            RemoveHandler PopupContainerEdit_POA_Phone.QueryDisplayText, AddressOf PopupContainerEdit_POA_Phone_QueryDisplayText
            RemoveHandler NameRecordControl_POA.NameChanged, AddressOf NameRecordControl_POA_NameChanged
        End Sub

        ''' <summary>
        ''' Read the POA record from the database. We are given the current person record to be used.
        ''' </summary>
        ''' <param name="personRecord"></param>
        ''' <remarks></remarks>
        Public Shadows Sub ReadForm(bc As BusinessContext, personRecord As DebtPlus.LINQ.people)
            MyBase.ReadForm(bc)

            UnRegisterHandlers()
            Try
                ' Load the lookup controls with the corresponding datasets
                LookUpEdit_POA_Other.Properties.DataSource = DebtPlus.LINQ.InMemory.validInvalidTypes.getList()
                LookUpEdit_RealEstate.Properties.DataSource = DebtPlus.LINQ.InMemory.validInvalidTypes.getList().FindAll(Function(s) Not s.IsOther)
                LookUpEdit_POA_Fin.Properties.DataSource = DebtPlus.LINQ.InMemory.validInvalidTypes.getList().FindAll(Function(s) Not s.IsOther)
                LookUpEdit_POA_Stage.Properties.DataSource = DebtPlus.LINQ.Cache.poa_lookup.getList()
                LookUpEdit_POA_Type.Properties.DataSource = DebtPlus.LINQ.Cache.poa_type.getList()
                LookUpEdit_POA_PhoneType.Properties.DataSource = DebtPlus.LINQ.Cache.TelephoneType.getList()

                ' Retrieve the poa record from the database
                If currentValues Is Nothing Then
                    currentValues = bc.client_poas.Where(Function(s) s.Client = personRecord.Client AndAlso s.relation.Value = personRecord.Relation).FirstOrDefault()
                    If currentValues Is Nothing Then
                        currentValues = DebtPlus.LINQ.Factory.Manufacture_client_poa()
                        currentValues.Client = personRecord.Client
                        currentValues.relation = personRecord.Relation
                    End If
                End If

                ' Load the controls with the current record contents
                NameRecordControl_POA.EditValue = currentValues.NameID
                POA_Telephone_RecordControl.EditValue = currentValues.TelephoneID
                LookUpEdit_POA_Type.EditValue = currentValues.POAType
                textEdit_POA_ID.EditValue = currentValues.POA_ID
                dt_POA_ID_Issued.EditValue = currentValues.POA_ID_Issued
                dt_POA_ID_Expires.EditValue = currentValues.POA_ID_Expires
                LookUpEdit_RealEstate.EditValue = currentValues.RealEstate
                LookUpEdit_POA_Fin.EditValue = currentValues.Financial
                LookUpEdit_POA_Other.EditValue = currentValues.Other
                LookUpEdit_POA_Stage.EditValue = currentValues.Stage
                ApprovalBy.Text = If(currentValues.ApprovalBy, String.Empty)
                LookUpEdit_POA_PhoneType.EditValue = currentValues.TelephoneType
                dt_POA_Expires.EditValue = currentValues.POA_Expires

                ' After the controls have been loaded then we need to force the update of the
                ' popup control text fields. They are normally tripped by events, but since we
                ' disabled the events, we need to do it here.
                PopupContainerEdit_POA_Phone.Text = getPhoneText()
                PopupContainerEdit_POA_ID.Text = getIDText()

                ' The controls are enabled only if the approval status is *not* "Approved"
                Dim ApprovalString As String = Helpers.GetPOA_StageText(currentValues.Stage)
                PopupContainerEdit_POA_Approval.Text = ApprovalString
                EnableControls(String.Compare(ApprovalString, "Approved", True) = 0)

            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' We are deleting the person record from the people table. We need to delete the POA reference as well.
        ''' </summary>
        ''' <param name="personRecord">Record that we will be deleting</param>
        ''' <remarks></remarks>
        Friend Sub DeleteRecord(personRecord As DebtPlus.LINQ.people)

            ' Find the record to be deleted if there is one
            If currentValues.Id > 0 Then
                Using bc As New BusinessContext()
                    Dim q As client_poa = bc.client_poas.Where(Function(s) s.Id = currentValues.Id).FirstOrDefault()
                    If q IsNot Nothing Then
                        bc.client_poas.DeleteOnSubmit(q)
                        bc.SubmitChanges()
                    End If
                End Using
            End If

            ' Forget the current record. It will be re-created on the next time.
            currentValues = Nothing
        End Sub

        ''' <summary>
        ''' Save the changes to the POA record. We are given the person record to which it is to be associated with.
        ''' </summary>
        ''' <param name="personRecord"></param>
        ''' <remarks></remarks>
        Public Shadows Sub SaveForm(bc As BusinessContext, personRecord As DebtPlus.LINQ.people)
            currentValues.ClearChangedFieldItems()

            ' This is crazy. You never link tables in this fashion!!!! But, someone kludged it so I will preserve it FOR NOW!!
            currentValues.Client = personRecord.Client
            currentValues.relation = personRecord.Relation

            ' Retrieve the current values from the input fields
            currentValues.NameID = DebtPlus.Utils.Nulls.v_Int32(NameRecordControl_POA.EditValue)
            currentValues.TelephoneID = DebtPlus.Utils.Nulls.v_Int32(POA_Telephone_RecordControl.EditValue)
            currentValues.POAType = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_POA_Type.EditValue)
            currentValues.POA_ID = DebtPlus.Utils.Nulls.v_String(textEdit_POA_ID.EditValue)
            currentValues.POA_ID_Issued = DebtPlus.Utils.Nulls.v_DateTime(dt_POA_ID_Issued.EditValue)
            currentValues.POA_ID_Expires = DebtPlus.Utils.Nulls.v_DateTime(dt_POA_ID_Expires.EditValue)
            currentValues.RealEstate = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_RealEstate.EditValue)
            currentValues.Financial = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_POA_Fin.EditValue)
            currentValues.Other = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_POA_Other.EditValue)
            currentValues.Stage = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_POA_Stage.EditValue)
            currentValues.ApprovalBy = ApprovalBy.Text
            currentValues.TelephoneType = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_POA_PhoneType.EditValue)
            currentValues.POA_Expires = DebtPlus.Utils.Nulls.v_DateTime(dt_POA_Expires.EditValue)

            ' If there is a record to be updated then we can simply update it
            If currentValues.Id > 0 Then
                Dim q As client_poa = bc.client_poas.Where(Function(s) s.Id = currentValues.Id).FirstOrDefault()
                If q IsNot Nothing Then
                    q.ClearChangedFieldItems()
                    q.Client = currentValues.Client
                    q.relation = currentValues.relation
                    q.NameID = currentValues.NameID
                    q.TelephoneID = currentValues.TelephoneID
                    q.POAType = currentValues.POAType
                    q.POA_ID = currentValues.POA_ID
                    q.POA_ID_Issued = currentValues.POA_ID_Issued
                    q.POA_ID_Expires = currentValues.POA_ID_Expires
                    q.RealEstate = currentValues.RealEstate
                    q.Financial = currentValues.Financial
                    q.Other = currentValues.Other
                    q.Stage = currentValues.Stage
                    q.ApprovalBy = currentValues.ApprovalBy
                    q.TelephoneType = currentValues.TelephoneType
                    q.POA_Expires = currentValues.POA_Expires

                    bc.SubmitChanges()
                    GenerateSystemNote(q.getChangedFieldItems())
                    Return
                End If
            End If

            ' Insert the new record since it does not exist.
            bc.client_poas.InsertOnSubmit(currentValues)
            bc.SubmitChanges()
            GenerateSystemNote(currentValues.getChangedFieldItems())
        End Sub

        ''' <summary>
        ''' Generate the system note reflecting the changes in the table
        ''' </summary>
        ''' <param name="colChanges">A collection of changed fields. It may be an empty list.</param>
        ''' <remarks></remarks>
        Private Sub GenerateSystemNote(colChanges As System.Collections.Generic.List(Of DebtPlus.LINQ.ChangedFieldItem))

            If colChanges.Count > 0 Then
                Dim text As New System.Text.StringBuilder()
                colChanges.ForEach(Sub(s) text.Append(formatSystemNote(s)))

                Dim subject As String = "Changed {0} POA " + System.String.Join(",", colChanges.Select(Function(s) s.FieldName))
                OnRecordNote(subject, Text.ToString())
            End If
        End Sub

        ''' <summary>
        ''' Routine to translate the code values used in the row to a suitable string for the system note.
        ''' </summary>
        ''' <param name="changedItem">Pointer to the changed field event structure</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function formatSystemNote(changedItem As DebtPlus.LINQ.ChangedFieldItem) As String

            ' Translate code values to the appropriate description strings
            Select Case changedItem.FieldName
                Case "POAType"
                    Return Helpers.FormatString(changedItem.FieldName, Helpers.GetPOATypeText(DirectCast(changedItem.OldValue, System.Nullable(Of System.Int32))), Helpers.GetPOATypeText(DirectCast(changedItem.NewValue, System.Nullable(Of System.Int32))))
                Case "Stage"
                    Return Helpers.FormatString(changedItem.FieldName, Helpers.GetPOA_lookupText(DirectCast(changedItem.OldValue, System.Nullable(Of System.Int32))), Helpers.GetPOA_StageText(DirectCast(changedItem.NewValue, System.Nullable(Of System.Int32))))
                Case "RealEstate", "Financial", "Other"
                    Return Helpers.FormatString(changedItem.FieldName, Helpers.GetValidInvalidText(DirectCast(changedItem.OldValue, System.Nullable(Of System.Int32))), Helpers.GetValidInvalidText(DirectCast(changedItem.NewValue, System.Nullable(Of System.Int32))))
                Case "TelephoneType"
                    Return Helpers.FormatString(changedItem.FieldName, Helpers.GetTelephoneTypeText(DirectCast(changedItem.OldValue, System.Nullable(Of System.Int32))), Helpers.GetTelephoneTypeText(DirectCast(changedItem.NewValue, System.Nullable(Of System.Int32))))
                Case Else
            End Select

            ' Assume that we can just format the field as string with ToString()
            Return Helpers.FormatString(changedItem.FieldName, If(changedItem.OldValue Is Nothing, String.Empty, changedItem.OldValue.ToString()), If(changedItem.NewValue Is Nothing, String.Empty, changedItem.NewValue.ToString()))
        End Function

        ''' <summary>
        ''' Enable or disable the controls based upon the approval status
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub EnableControls(IsApproved As Boolean)

            ' Enable or disable the controls based upon the approval status. If the status is not Approved
            ' then the controls are enabled. When it goes to Approved, we do not allow the edit operation.
            LookUpEdit_RealEstate.Enabled = Not IsApproved
            LookUpEdit_POA_Fin.Enabled = Not IsApproved
            LookUpEdit_POA_Other.Enabled = Not IsApproved
        End Sub

        ''' <summary>
        ''' Enable or disable the controls when the approval stage changes
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub LookUpEdit_POA_Stage_EditValueChanged(sender As Object, e As System.EventArgs)

            ' Set the text for the approval message
            EnableControls(String.Compare(Helpers.GetPOA_StageText(DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_POA_Stage.EditValue)), "Approved", True) = 0)

            ' Update the "ApprovedBy" field to show the current counselor. We just use the last component of the counselor ID string.
            Dim currentCounselorID As String = DebtPlus.LINQ.BusinessContext.suser_sname()
            If Not String.IsNullOrEmpty(currentCounselorID) Then
                ApprovalBy.Text = DebtPlus.Utils.Format.Counselor.FormatCounselor(currentCounselorID)
                Return
            End If

            ' We don't have a valid ID at this time. We can't track who did it.
            ApprovalBy.Text = String.Empty
            Return

        End Sub

        ''' <summary>
        ''' Routine to obtain the text for the approval popup control
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub PopupContainerEdit_POA_Approval_QueryDisplayText(sender As Object, e As DevExpress.XtraEditors.Controls.QueryDisplayTextEventArgs)
            e.DisplayText = Helpers.GetPOA_StageText(DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_POA_Stage.EditValue))
        End Sub

        ''' <summary>
        ''' Routine to obtain the text for the telephone number popup control
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub PopupContainerEdit_POA_Phone_QueryDisplayText(sender As Object, e As DevExpress.XtraEditors.Controls.QueryDisplayTextEventArgs)
            e.DisplayText = getPhoneText()
        End Sub

        ''' <summary>
        ''' Return the proper text for the telephone number popup control
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function getPhoneText() As String
            Dim telephoneValues As DebtPlus.LINQ.TelephoneNumber = POA_Telephone_RecordControl.GetCurrentValues()
            If telephoneValues IsNot Nothing Then
                Return telephoneValues.ToString()
            End If
            Return String.Empty
        End Function

        ''' <summary>
        ''' Routine to obtain the text for the ID number popup control
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub PopupContainerEdit_POA_ID_QueryDisplayText(sender As Object, e As DevExpress.XtraEditors.Controls.QueryDisplayTextEventArgs)
            e.DisplayText = getIDText()
        End Sub

        ''' <summary>
        ''' Routine to obtain the text for the ID number popup control
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function getIDText() As String
            Return DebtPlus.Utils.Nulls.v_String(textEdit_POA_ID.EditValue)
        End Function

        ''' <summary>
        ''' Handle a change in the telephone number control. This is tripped when we get the EditValue from the control.
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub POA_Telephone_RecordControl_Changed(sender As Object, e As DebtPlus.Events.TelephoneNumberChangedEventArgs)
            Dim note As String = String.Format("Changed POA Telephone number from ""{0}"" to ""{1}""", e.OldTelephoneNumber.ToString(), e.NewTelephoneNumber.ToString())
            OnRecordNote("Changed {0} POA Telephone Number", note)
        End Sub

        ''' <summary>
        ''' Handle a change in the name control. This is tripped when we get the EditValue from the control.
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub NameRecordControl_POA_NameChanged(ByVal Sender As Object, ByVal e As DebtPlus.Events.NameChangeEventArgs)
            Dim NoteText As String = String.Format("POA Name Changed from ""{0}"" to ""{1}""", e.OldName.ToString(), e.NewName.ToString())
            OnRecordNote("Changed {0} POA Name", NoteText)
        End Sub

        ''' <summary>
        ''' Event raised when the sizing information is changed. We don't change it so it is not tripped.
        ''' </summary>
        ''' <remarks></remarks>
        Public Event Changed(sender As Object, e As System.EventArgs) Implements DevExpress.Utils.Controls.IXtraResizableControl.Changed

        ''' <summary>
        ''' Return the status for the "display text" information
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property IsCaptionVisible As Boolean Implements DevExpress.Utils.Controls.IXtraResizableControl.IsCaptionVisible
            Get
                Return False    ' We don't want the caption string.
            End Get
        End Property

        ''' <summary>
        ''' Return the maximum size of the control. We don't have a maximum size so return the empty object
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property MaxSize As System.Drawing.Size Implements DevExpress.Utils.Controls.IXtraResizableControl.MaxSize
            Get
                Return System.Drawing.Size.Empty
            End Get
        End Property

        ''' <summary>
        ''' Return the minimum size for the control. We have only a minimum for the height.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property MinSize As System.Drawing.Size Implements DevExpress.Utils.Controls.IXtraResizableControl.MinSize
            Get
                Return New System.Drawing.Size(0, 74)
            End Get
        End Property
    End Class
End Namespace
