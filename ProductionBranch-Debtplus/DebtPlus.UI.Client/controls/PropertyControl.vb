#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Linq
Imports DebtPlus.LINQ

Namespace controls
    Friend Class PropertyControl

        ' Current property record being changed
        Private propertyRecord As Housing_property = Nothing
        Private bc As DebtPlus.LINQ.BusinessContext = Nothing

        ''' <summary>
        ''' Event raised when the address is changed
        ''' </summary>
        Public Event AddressChanged As DebtPlus.Events.AddressChangedEventHandler

        ''' <summary>
        ''' Raise the AddressChanged event
        ''' </summary>
        Protected Sub RaiseAddressChanged(ByVal e As DebtPlus.Events.AddressChangedEventArgs)
            RaiseEvent AddressChanged(Me, e)
        End Sub

        ''' <summary>
        ''' Raise the AddressChanged event
        ''' </summary>
        Protected Overridable Sub OnAddressChanged(ByVal e As DebtPlus.Events.AddressChangedEventArgs)
            RaiseAddressChanged(e)
        End Sub

        ''' <summary>
        ''' Handle the change in the address from the editing control
        ''' </summary>
        Private Sub PropertyAddressControl1_AddressChanged(ByVal sender As Object, ByVal e As DebtPlus.Events.AddressChangedEventArgs)
            OnAddressChanged(e)
        End Sub

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            ' Try to force the tab page back to the first item. If there is an error then just ignore it.
            Try
                TabbedControlGroup1.SelectedTabPageIndex = 0
            Catch ex As Exception
            End Try
        End Sub

        Private Sub RegisterHandlers()
            AddHandler SimpleButton_SendEmail.Click, AddressOf SimpleButton_SendEmail_Click
            AddHandler LookUpEdit_CallReason.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler PropertyAddressControl1.AddressChanged, AddressOf PropertyAddressControl1_AddressChanged
            AddHandler PropertyAddressControl1.CheckEdit_UseHomeAddress.CheckStateChanged, AddressOf CheckEdit_UseHomeAddress_CHeckStateChanged
            AddHandler PropertyAddressControl1.CheckEdit_ViewPrevious.CheckStateChanged, AddressOf CheckEdit_UseHomeAddress_CHeckStateChanged
            AddHandler lu_HPF_Program.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler lu_HPF_SubProgram.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler SimpleButton_SendEmail.Click, AddressOf SimpleButton_SendEmail_Click
            RemoveHandler LookUpEdit_CallReason.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler PropertyAddressControl1.AddressChanged, AddressOf PropertyAddressControl1_AddressChanged
            RemoveHandler PropertyAddressControl1.CheckEdit_UseHomeAddress.CheckStateChanged, AddressOf CheckEdit_UseHomeAddress_CHeckStateChanged
            RemoveHandler PropertyAddressControl1.CheckEdit_ViewPrevious.CheckStateChanged, AddressOf CheckEdit_UseHomeAddress_CHeckStateChanged
            RemoveHandler lu_HPF_Program.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler lu_HPF_SubProgram.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
        End Sub

        Public Shadows Sub ReadForm(bc As BusinessContext, ByVal propertyRecord As Housing_property)
            MyBase.ReadForm(bc)
            Me.propertyRecord = propertyRecord
            Me.bc = bc

            ' The property record must exist in the database. We can't handle a "new" object because too many things hang off the property record.
            System.Diagnostics.Debug.Assert(propertyRecord.Id > 0)

            UnRegisterHandlers()

            ' Set the lookup data source items
            ' We can't do this in the "New" function because it is created before
            ' we have a working context and the cache items may need to be read.

            lu_HPF_Program.Properties.DataSource = DebtPlus.LINQ.Cache.HPFProgram.getList()
            lu_HPF_SubProgram.Properties.DataSource = DebtPlus.LINQ.Cache.HPFSubProgram.getList()
            lu_HPFCounselor.Properties.DataSource = DebtPlus.LINQ.Cache.counselor.CounselorsList()
            lu_PlanToKeepHome.Properties.DataSource = DebtPlus.LINQ.Cache.Housing_RetentionType.getList()
            lu_PropertyType.Properties.DataSource = DebtPlus.LINQ.Cache.HousingType.getList()
            lu_Residency.Properties.DataSource = DebtPlus.LINQ.Cache.Housing_ResidencyType.getList()
            lu_AppraisalType.Properties.DataSource = DebtPlus.LINQ.Cache.AppraisalType.getList()
            lu_HOADelinqState.Properties.DataSource = DebtPlus.LINQ.Cache.DelinqState.getHOA()
            lu_HomeOwners.Properties.DataSource = DebtPlus.LINQ.Cache.TaxInsuranceType.getList()
            lu_InsuranceDelinqState.Properties.DataSource = DebtPlus.LINQ.Cache.DelinqState.getList()
            lu_PmiInsurance.Properties.DataSource = DebtPlus.LINQ.Cache.TaxInsuranceType.getList()
            lu_PropertyTaxes.Properties.DataSource = DebtPlus.LINQ.Cache.TaxInsuranceType.getList()
            lu_TaxDelinqState.Properties.DataSource = DebtPlus.LINQ.Cache.DelinqState.getList()
            LookUpEdit_CallReason.Properties.DataSource = DebtPlus.LINQ.Cache.Housing_ReasonForCallType.getList()

            Try
                ' Set the foreclosure case ID into the label field
                If propertyRecord.HPF_foreclosureCaseID IsNot Nothing Then
                    LabelControl_FCID.Text = String.Format("{0:f0}", propertyRecord.HPF_foreclosureCaseID)
                Else
                    LabelControl_FCID.Text = "N/A"
                End If

                ' Load the record values into the editing controls
                CalcEdit_AnnualTax.EditValue = propertyRecord.annual_property_tax
                CalcEdit_HOADelinqAmount.EditValue = propertyRecord.HOA_delinq_amount
                CalcEdit_TaxDelinqAmount.EditValue = propertyRecord.tax_due_amount
                CalcEdit_txtAnnualInsuranceAmt.EditValue = propertyRecord.annual_ins_amount
                CalcEdit_ImprovementsValue.EditValue = propertyRecord.ImprovementsValue
                CalcEdit_InsDeliqAmount.EditValue = propertyRecord.ins_due_amount
                CalcEdit_LandValue.EditValue = propertyRecord.LandValue
                CalcEdit_PurchasePrice.EditValue = propertyRecord.PurchasePrice
                CalcEdit_SalePrice.EditValue = propertyRecord.SalePrice
                CheckEdit_FcNoticeReceivedIND.Checked = propertyRecord.FcNoticeReceivedIND
                CheckEdit_ForSaleIND.Checked = propertyRecord.ForSaleIND
                CheckEdit_HPF_worked_with_other_agency.Checked = propertyRecord.HPF_worked_with_other_agency
                CheckEdit_UseInReports.Checked = propertyRecord.UseInReports
                dt_AppraisalDate.EditValue = propertyRecord.Appraisal_Date
                dt_FcSaleDT.EditValue = propertyRecord.FcSaleDT
                dt_InsuranceDueDate.EditValue = propertyRecord.ins_due_date
                dt_sales_contract_date.EditValue = propertyRecord.sales_contract_date
                dt_Tax_Duedate.EditValue = propertyRecord.tax_due_date
                lu_AppraisalType.EditValue = propertyRecord.Appraisal_Type
                lu_HOADelinqState.EditValue = propertyRecord.HOA_delinq_state
                lu_HomeOwners.EditValue = propertyRecord.homeowner_ins
                lu_HPF_Program.EditValue = propertyRecord.HPF_Program
                lu_HPF_SubProgram.EditValue = propertyRecord.HPF_SubProgram
                lu_HPFCounselor.EditValue = propertyRecord.HPF_counselor
                lu_InsuranceDelinqState.EditValue = propertyRecord.ins_delinq_state
                lu_PlanToKeepHome.EditValue = propertyRecord.PlanToKeepHomeCD
                lu_PmiInsurance.EditValue = propertyRecord.pmi_insurance
                lu_PropertyTaxes.EditValue = propertyRecord.property_tax
                lu_PropertyType.EditValue = propertyRecord.PropertyType
                lu_Residency.EditValue = propertyRecord.Residency
                lu_TaxDelinqState.EditValue = propertyRecord.tax_delinq_state
                LookUpEdit_CallReason.EditValue = propertyRecord.HPF_ReasonForCallCD
                PropertyAddressControl1.CheckEdit_UseHomeAddress.EditValue = propertyRecord.UseHomeAddress
                SpinEdit_Occupancy.EditValue = propertyRecord.Occupancy
                TextEdit_TrustName.EditValue = propertyRecord.TrustName
                TextEdit_RealityCompany.EditValue = propertyRecord.RealityCompany

                ' Leave the purchase year BLANK so that it is converted back to zero when the record is retrieved
                TextEdit_PurchaseYear.Text = If(propertyRecord.PurchaseYear = 0, String.Empty, propertyRecord.PurchaseYear.ToString("0000"))

                ' Set the property address record
                Dim currentAddress As System.Nullable(Of Int32) = propertyRecord.PropertyAddress
                PropertyAddressControl1.AddressRecordControl_AddressID.EditValue = currentAddress

                ' If there is no address then show the current client's address as the edit values but leave the ID as "undefined" so that it is created if changed.
                If currentAddress Is Nothing AndAlso Context.ClientDs.clientRecord.AddressID.HasValue Then
                    Dim CurrentValues As DebtPlus.LINQ.address = PropertyAddressControl1.AddressRecordControl_AddressID.ReadRecord(Context.ClientDs.clientRecord.AddressID.Value)
                    PropertyAddressControl1.AddressRecordControl_AddressID.SetCurrentValues(CurrentValues)
                End If

                ' Set the previous address information
                PropertyAddressControl1.AddressRecordControl_PreviousAddressID.EditValue = propertyRecord.PreviousAddress

                ' Set the borrower information as needed
                BorrowerControl_OwnerID.EditValue = propertyRecord.OwnerID
                BorrowerControl_CoOwnerID.EditValue = propertyRecord.CoOwnerID

                ' Read the sub-form fields
                LoanListControl1.ReadForm(bc, propertyRecord)
                PropertyNoteListControl1.ReadForm(propertyRecord.Id)
                HpfOutcomeListControl1.ReadForm(propertyRecord.Id)

                EnableResidency(propertyRecord.UseHomeAddress)

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading property information")
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Public Overrides Sub SaveForm(bc As DebtPlus.LINQ.BusinessContext)
            MyBase.SaveForm(bc)
            Me.bc = bc

            ' Save the controls into the current record
            propertyRecord.annual_ins_amount = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_txtAnnualInsuranceAmt.EditValue).GetValueOrDefault(0D)
            propertyRecord.annual_property_tax = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_AnnualTax.EditValue).GetValueOrDefault(0D)
            propertyRecord.Appraisal_Date = DebtPlus.Utils.Nulls.v_DateTime(dt_AppraisalDate.EditValue)
            propertyRecord.Appraisal_Type = DebtPlus.Utils.Nulls.v_Int32(lu_AppraisalType.EditValue)
            propertyRecord.FcNoticeReceivedIND = CheckEdit_FcNoticeReceivedIND.Checked
            propertyRecord.FcSaleDT = DebtPlus.Utils.Nulls.v_DateTime(dt_FcSaleDT.EditValue)
            propertyRecord.ForSaleIND = CheckEdit_ForSaleIND.Checked
            propertyRecord.HOA_delinq_amount = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_HOADelinqAmount.EditValue).GetValueOrDefault(0D)
            propertyRecord.HOA_delinq_state = DebtPlus.Utils.Nulls.v_Int32(lu_HOADelinqState.EditValue)
            propertyRecord.homeowner_ins = DebtPlus.Utils.Nulls.v_Int32(lu_HomeOwners.EditValue)
            propertyRecord.HPF_counselor = DebtPlus.Utils.Nulls.v_Int32(lu_HPFCounselor.EditValue)
            propertyRecord.HPF_Program = DebtPlus.Utils.Nulls.v_Int32(lu_HPF_Program.EditValue)
            propertyRecord.HPF_SubProgram = DebtPlus.Utils.Nulls.v_Int32(lu_HPF_SubProgram.EditValue)
            propertyRecord.HPF_worked_with_other_agency = CheckEdit_HPF_worked_with_other_agency.Checked
            propertyRecord.HPF_ReasonForCallCD = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_CallReason.EditValue)
            propertyRecord.ImprovementsValue = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_ImprovementsValue.EditValue).GetValueOrDefault(0D)
            propertyRecord.ins_delinq_state = DebtPlus.Utils.Nulls.v_Int32(lu_InsuranceDelinqState.EditValue)
            propertyRecord.ins_due_amount = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_InsDeliqAmount.EditValue).GetValueOrDefault(0D)
            propertyRecord.ins_due_date = DebtPlus.Utils.Nulls.v_DateTime(dt_InsuranceDueDate.EditValue)
            propertyRecord.LandValue = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_LandValue.EditValue).GetValueOrDefault(0D)
            propertyRecord.Occupancy = DebtPlus.Utils.Nulls.v_Int32(SpinEdit_Occupancy.EditValue).GetValueOrDefault(0)
            propertyRecord.PlanToKeepHomeCD = DebtPlus.Utils.Nulls.v_Int32(lu_PlanToKeepHome.EditValue)
            propertyRecord.pmi_insurance = DebtPlus.Utils.Nulls.v_Int32(lu_PmiInsurance.EditValue)
            propertyRecord.property_tax = DebtPlus.Utils.Nulls.v_Int32(lu_PropertyTaxes.EditValue)
            propertyRecord.PropertyType = DebtPlus.Utils.Nulls.v_Int32(lu_PropertyType.EditValue).GetValueOrDefault(0)
            propertyRecord.PurchasePrice = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_PurchasePrice.EditValue).GetValueOrDefault(0D)
            propertyRecord.RealityCompany = DebtPlus.Utils.Nulls.v_String(TextEdit_RealityCompany.EditValue)
            propertyRecord.Residency = DebtPlus.Utils.Nulls.v_Int32(lu_Residency.EditValue).GetValueOrDefault(0)
            propertyRecord.SalePrice = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_SalePrice.EditValue).GetValueOrDefault(0D)
            propertyRecord.sales_contract_date = DebtPlus.Utils.Nulls.v_DateTime(dt_sales_contract_date.EditValue)
            propertyRecord.tax_delinq_state = DebtPlus.Utils.Nulls.v_Int32(lu_TaxDelinqState.EditValue)
            propertyRecord.tax_due_amount = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_TaxDelinqAmount.EditValue).GetValueOrDefault(0D)
            propertyRecord.tax_due_date = DebtPlus.Utils.Nulls.v_DateTime(dt_Tax_Duedate.EditValue)
            propertyRecord.TrustName = DebtPlus.Utils.Nulls.v_String(TextEdit_TrustName.EditValue)
            propertyRecord.UseHomeAddress = PropertyAddressControl1.CheckEdit_UseHomeAddress.Checked
            propertyRecord.UseInReports = CheckEdit_UseInReports.Checked

            ' Take the year from the text string. The text input field allows only 4 decimal digits.
            Dim yearValue As Int32
            Dim strValue As String = TextEdit_PurchaseYear.Text.Trim()
            If String.IsNullOrEmpty(strValue) OrElse Not Int32.TryParse(strValue, yearValue) Then
                yearValue = 0

            Else

                ' Handle a two digit year value by trying to guess at the centaury
                If yearValue < 100 Then
                    yearValue += 2000
                    If yearValue > System.DateTime.Now.Year Then
                        yearValue -= 100
                    End If
                End If
            End If
            propertyRecord.PurchaseYear = yearValue

            ' Set the home address and checked status if so indicated
            If Not propertyRecord.UseHomeAddress Then
                propertyRecord.PropertyAddress = PropertyAddressControl1.AddressRecordControl_AddressID.EditValue

            Else

                ' If there is a previous address and we don't want it then now is the time to delete it.
                If propertyRecord.PropertyAddress.HasValue Then
                    Try
                        Dim q As DebtPlus.LINQ.address = bc.addresses.Where(Function(s) s.Id = propertyRecord.PropertyAddress.Value).FirstOrDefault()
                        If q IsNot Nothing Then
                            bc.addresses.DeleteOnSubmit(q)
                            bc.SubmitChanges()
                        End If
                        propertyRecord.PropertyAddress = Nothing

                    Catch ex As SqlException
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error removing extraneous address")
                    End Try
                End If
            End If

            ' Retrieve he borrower and co-borrower information
            propertyRecord.OwnerID = BorrowerControl_OwnerID.EditValue
            propertyRecord.CoOwnerID = BorrowerControl_CoOwnerID.EditValue

            ' Do the sub-item controls as well
            LoanListControl1.SaveForm(bc)
            PropertyNoteListControl1.SaveForm(bc)
        End Sub

        Private Sub CheckEdit_UseHomeAddress_CHeckStateChanged(ByVal Sender As Object, ByVal e As EventArgs)
            EnableResidency(PropertyAddressControl1.CheckEdit_UseHomeAddress.Checked)
        End Sub

        Private Sub EnableResidency(useHomeAddress As Boolean)

            ' Change the residency to "primary residence" if the home address is being used.
            If useHomeAddress Then
                lu_Residency.EditValue = 1
                propertyRecord.Residency = 1
            End If

            ' Enable or disable the type based upon the home address flag
            lu_Residency.Enabled = Not useHomeAddress
        End Sub

        Private Sub SimpleButton_SendEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs)

            Try
                Dim counselor_name As String = String.Empty
                Dim record As DebtPlus.LINQ.email_queue = DebtPlus.LINQ.Factory.Manufacture_email_queue()

                ' Find the template ID
                Dim qTemplate As DebtPlus.LINQ.email_template = DebtPlus.LINQ.Cache.email_template.getList().Find(Function(s) String.Compare(s.description, "HPF BluePrint Notice", True) = 0)
                If qTemplate Is Nothing Then
                    Return
                End If
                record.template = qTemplate.Id

                ' Find the client name
                Dim qPerson As DebtPlus.LINQ.people = Context.ClientDs.colPeople.Find(Function(s) s.Relation = 1)
                If qPerson IsNot Nothing AndAlso qPerson.NameID.HasValue Then
                    Dim qName As DebtPlus.LINQ.Name = bc.Names.Where(Function(s) s.Id = qPerson.NameID.Value).FirstOrDefault()
                    If qName IsNot Nothing Then
                        record.email_name = qName.ToString()
                    End If

                    ' Find the person's email address
                    If qPerson.EmailID.HasValue Then
                        Dim qEmail As DebtPlus.LINQ.EmailAddress = bc.EmailAddresses.Where(Function(s) s.Id = qPerson.EmailID.Value).FirstOrDefault()
                        If qEmail.ValidationCode = EmailAddress.EmailValidationEnum.Valid Then
                            record.email_address = qEmail.Address
                        End If
                    End If
                End If

                ' Find the counselor name
                Dim counselorID As String = DebtPlus.LINQ.BusinessContext.suser_sname()
                If Not String.IsNullOrWhiteSpace(counselorID) Then
                    Dim coCounselor As DebtPlus.LINQ.counselor = DebtPlus.LINQ.Cache.counselor.getList().Find(Function(s) String.Compare(s.Person, counselorID, True) = 0)
                    If coCounselor IsNot Nothing Then
                        counselor_name = coCounselor.Name.ToString()
                    End If
                End If

                ' Build the substitution data for the email message.
                Dim sbSubstitution As New System.Text.StringBuilder()
                sbSubstitution.Append("<substitutions>")
                sbSubstitution.AppendFormat("<substitution><field>client_name</field><value>{0}</value></substitution>", record.email_name)
                sbSubstitution.AppendFormat("<substitution><field>counselor_name</field><value>{0}</value></substitution>", counselor_name)
                sbSubstitution.Append("</substitutions>")

                record.substitutions = System.Xml.Linq.XElement.Parse(sbSubstitution.ToString())

                ' Give the user the option to change the email address.
                Using frm As New DebtPlus.UI.Client.forms.Housing.HPFBluePrint(record)
                    If frm.ShowDialog() <> DialogResult.OK Then
                        Return
                    End If
                End Using

                ' Add the record to the queue
                bc.email_queues.InsertOnSubmit(record)

                ' Generate the system note that we have sent one
                Dim n As client_note = DebtPlus.LINQ.Factory.Manufacture_client_note(Context.ClientDs.ClientId, 3)
                n.subject = "Sent HPF BluePrint Brochure Email"
                n.note = String.Format("We have sent the HPF BluePrint Brochure Email to the following data:{0}{0}Client Name: {1}{0}Counselor Name: {2}{0}Email Address: {3}", Environment.NewLine, record.email_name, counselor_name, record.email_address)
                bc.client_notes.InsertOnSubmit(n)

                ' Make the database changes for the email events.
                bc.SubmitChanges()

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error generating email queue entry")
            End Try
        End Sub
    End Class
End Namespace