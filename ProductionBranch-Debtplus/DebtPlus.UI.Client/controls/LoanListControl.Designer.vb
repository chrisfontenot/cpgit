Namespace controls
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class LoanListControl
        Inherits MyGridControlClient

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container
            Me.GridColumn_oID = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Loan1st2nd = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_MortgageTypeCD = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_FinanceTypeCD = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_LoanDelinquencyMonths = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_CurentLoanBalanceAmt = New DevExpress.XtraGrid.Columns.GridColumn
            Me.SuspendLayout()
            '
            'GridView1
            '
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_oID, Me.GridColumn_Loan1st2nd, Me.GridColumn_MortgageTypeCD, Me.GridColumn_FinanceTypeCD, Me.GridColumn_LoanDelinquencyMonths, Me.GridColumn_CurentLoanBalanceAmt})
            Me.GridView1.OptionsView.ShowFooter = True
            Me.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedRow
            '
            'GridColumn_oID
            '
            Me.GridColumn_oID.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_oID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_oID.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_oID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_oID.Caption = "ID"
            Me.GridColumn_oID.CustomizationCaption = "Record ID"
            Me.GridColumn_oID.DisplayFormat.FormatString = "{0:f0}"
            Me.GridColumn_oID.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_oID.FieldName = "Id"
            Me.GridColumn_oID.GroupFormat.FormatString = "{0:f0}"
            Me.GridColumn_oID.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_oID.Name = "GridColumn_oID"
            Me.GridColumn_oID.OptionsColumn.AllowEdit = False
            '
            'GridColumn_Loan1st2nd
            '
            Me.GridColumn_Loan1st2nd.Caption = "Position"
            Me.GridColumn_Loan1st2nd.CustomizationCaption = "Loan Position"
            Me.GridColumn_Loan1st2nd.FieldName = "Loan1st2nd"
            Me.GridColumn_Loan1st2nd.Name = "GridColumn_Loan1st2nd"
            Me.GridColumn_Loan1st2nd.Visible = True
            Me.GridColumn_Loan1st2nd.VisibleIndex = 0
            '
            'GridColumn_MortgageTypeCD
            '
            Me.GridColumn_MortgageTypeCD.Caption = "Mortgage"
            Me.GridColumn_MortgageTypeCD.FieldName = "Id"
            Me.GridColumn_MortgageTypeCD.Name = "GridColumn_MortgageTypeCD"
            Me.GridColumn_MortgageTypeCD.Visible = True
            Me.GridColumn_MortgageTypeCD.VisibleIndex = 1
            '
            'GridColumn_TermLengthCD
            '
            Me.GridColumn_FinanceTypeCD.Caption = "Financing"
            Me.GridColumn_FinanceTypeCD.FieldName = "Id"
            Me.GridColumn_FinanceTypeCD.Name = "GridColumn_TermLengthCD"
            Me.GridColumn_FinanceTypeCD.Visible = True
            Me.GridColumn_FinanceTypeCD.VisibleIndex = 2
            '
            'GridColumn_LoanDelinquencyMonths
            '
            Me.GridColumn_LoanDelinquencyMonths.Caption = "Delinquent"
            Me.GridColumn_LoanDelinquencyMonths.CustomizationCaption = "Delinquency Status"
            Me.GridColumn_LoanDelinquencyMonths.FieldName = "LoanDelinquencyMonths"
            Me.GridColumn_LoanDelinquencyMonths.Name = "GridColumn_LoanDelinquencyMonths"
            Me.GridColumn_LoanDelinquencyMonths.Visible = True
            Me.GridColumn_LoanDelinquencyMonths.VisibleIndex = 3
            '
            'GridColumn_CurentLoanBalanceAmt
            '
            Me.GridColumn_CurentLoanBalanceAmt.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_CurentLoanBalanceAmt.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_CurentLoanBalanceAmt.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_CurentLoanBalanceAmt.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_CurentLoanBalanceAmt.Caption = "Current Balance"
            Me.GridColumn_CurentLoanBalanceAmt.CustomizationCaption = "Current Loan Balance"
            Me.GridColumn_CurentLoanBalanceAmt.FieldName = "CurrentLoanBalanceAmt"
            Me.GridColumn_CurentLoanBalanceAmt.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_CurentLoanBalanceAmt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_CurentLoanBalanceAmt.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_CurentLoanBalanceAmt.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_CurentLoanBalanceAmt.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_CurentLoanBalanceAmt.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_CurentLoanBalanceAmt.Visible = True
            Me.GridColumn_CurentLoanBalanceAmt.VisibleIndex = 4
            Me.GridColumn_CurentLoanBalanceAmt.Name = "GridColumn_CurentLoanBalanceAmt"
            '
            Me.GridColumn_oID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_Loan1st2nd.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_MortgageTypeCD.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_FinanceTypeCD.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_LoanDelinquencyMonths.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_CurentLoanBalanceAmt.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            '
            Me.GridColumn_Loan1st2nd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_Loan1st2nd.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Loan1st2nd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_Loan1st2nd.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Loan1st2nd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_Loan1st2nd.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_LoanDelinquencyMonths.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_LoanDelinquencyMonths.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_LoanDelinquencyMonths.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_LoanDelinquencyMonths.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_LoanDelinquencyMonths.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_LoanDelinquencyMonths.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_MortgageTypeCD.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_MortgageTypeCD.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_MortgageTypeCD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_MortgageTypeCD.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_MortgageTypeCD.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_MortgageTypeCD.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_FinanceTypeCD.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_FinanceTypeCD.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_FinanceTypeCD.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_FinanceTypeCD.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_FinanceTypeCD.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_FinanceTypeCD.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom
            '
            'LoanListControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Name = "LoanListControl"
            Me.Size = New System.Drawing.Size(550, 378)
            Me.ResumeLayout(False)
        End Sub
        Protected Friend WithEvents GridColumn_oID As DevExpress.XtraGrid.Columns.GridColumn
        Protected Friend WithEvents GridColumn_Loan1st2nd As DevExpress.XtraGrid.Columns.GridColumn
        Protected Friend WithEvents GridColumn_MortgageTypeCD As DevExpress.XtraGrid.Columns.GridColumn
        Protected Friend WithEvents GridColumn_FinanceTypeCD As DevExpress.XtraGrid.Columns.GridColumn
        Protected Friend WithEvents GridColumn_LoanDelinquencyMonths As DevExpress.XtraGrid.Columns.GridColumn
        Protected Friend WithEvents GridColumn_CurentLoanBalanceAmt As DevExpress.XtraGrid.Columns.GridColumn
    End Class
End Namespace