#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.Events
Imports DebtPlus.UI.Client.Service
Imports DebtPlus.UI.Client.forms.Housing
Imports DebtPlus.LINQ
Imports DevExpress.Utils
Imports System.Globalization
Imports System.Linq

Namespace controls

    Friend Class PropertyNoteListControl

        ''' <summary>
        ''' Initialize the storage for the control class
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            EnableMenus = True
        End Sub

        ' Collection of notes
        Private colNotes As System.Collections.Generic.List(Of DebtPlus.LINQ.Housing_PropertyNote)
        Private propertyID As Int32

        ''' <summary>
        ''' Edit the note. We have removed the ability to edit the note so this won't do anything.
        ''' </summary>
        ''' <param name="obj"></param>
        ''' <remarks></remarks>
        Protected Overrides Sub OnEditItem(obj As Object)

            ' Find the note to be displayed.
            Dim item As DebtPlus.LINQ.Housing_PropertyNote = TryCast(obj, DebtPlus.LINQ.Housing_PropertyNote)
            If item Is Nothing Then
                Return
            End If

            ' Show the note. Do not allow the edit operation to occur.
            Using frm As New PropertyNoteForm(item, False)
                frm.ShowDialog()
            End Using

        End Sub

        Protected Overrides Sub OnCreateItem()

            Dim item As Housing_PropertyNote = DebtPlus.LINQ.Factory.Manufacture_Housing_PropertyNote(propertyID)

            ' Edit the note. If OK then update the note text.
            Using frm As New PropertyNoteForm(item, True)
                If frm.ShowDialog() <> DialogResult.OK Then
                    Return
                End If
            End Using

            Using bc As New BusinessContext()
                bc.Housing_PropertyNotes.InsertOnSubmit(item)
                bc.SubmitChanges()
            End Using

            colNotes.Add(item)
            GridControl1.RefreshDataSource()
        End Sub

        Protected Overrides Sub OnDeleteItem(obj As Object)

            Dim item As DebtPlus.LINQ.Housing_PropertyNote = TryCast(obj, DebtPlus.LINQ.Housing_PropertyNote)
            If item Is Nothing Then
                Return
            End If

            If DebtPlus.Data.Prompts.RequestConfirmation_Delete() <> DialogResult.Yes Then
                Return
            End If

            Using bc As New BusinessContext()
                Dim q As DebtPlus.LINQ.Housing_PropertyNote = bc.Housing_PropertyNotes.Where(Function(pn) pn.Id = item.Id).FirstOrDefault()
                If q IsNot Nothing Then
                    bc.Housing_PropertyNotes.DeleteOnSubmit(q)
                    bc.SubmitChanges()
                    colNotes.Remove(item)
                    GridControl1.RefreshDataSource()
                End If
            End Using
        End Sub

        ''' <summary>
        ''' Read the list of notes associated with the indicated property record
        ''' </summary>
        Public Shadows Sub ReadForm(ByVal propertyID As Int32)
            Me.propertyID = propertyID

            ' Read the collection of notes from the system.
            Using bc As New BusinessContext()
                colNotes = bc.Housing_PropertyNotes.Where(Function(pn) pn.PropertyID = propertyID).ToList()
            End Using

            ' Display the notes in the grid
            GridControl1.DataSource = colNotes
        End Sub
    End Class
End Namespace
