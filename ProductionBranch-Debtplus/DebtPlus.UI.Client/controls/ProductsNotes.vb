﻿Imports System.Linq
Imports DebtPlus.LINQ
Imports System.Windows.Forms
Imports System

Namespace controls
    Public Class ProductsNotes
        Inherits DevExpress.XtraEditors.XtraUserControl

        Private record As DebtPlus.LINQ.client_product = Nothing
        Private bc As BusinessContext = Nothing

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Friend Shadows Sub ReadForm(bc As BusinessContext, record As client_product)
            UnRegisterHandlers()
            Try
                MyClass.bc = bc
                MyClass.record = record
                ProductsNotesGrid1.ReadForm(bc, record, CheckEdit_Perm.Checked, CheckEdit_System.Checked)
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Friend Shadows Sub SaveForm()
            ProductsNotesGrid1.SaveForm()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler CheckEdit_System.CheckedChanged, AddressOf CheckChanged
            AddHandler CheckEdit_Perm.CheckedChanged, AddressOf CheckChanged
            AddHandler CheckEdit_Perm.EditValueChanging, AddressOf CheckEditValueChanging
            AddHandler CheckEdit_System.EditValueChanging, AddressOf CheckEditValueChanging
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler CheckEdit_System.CheckedChanged, AddressOf CheckChanged
            RemoveHandler CheckEdit_Perm.CheckedChanged, AddressOf CheckChanged
            RemoveHandler CheckEdit_Perm.EditValueChanging, AddressOf CheckEditValueChanging
            RemoveHandler CheckEdit_System.EditValueChanging, AddressOf CheckEditValueChanging
        End Sub

        Private Sub CheckEditValueChanging(sender As Object, e As DevExpress.XtraEditors.Controls.ChangingEventArgs)
            UnRegisterHandlers()
            Try
                ' If checking the flag then allow it to occur
                Dim boolValue As Boolean = Convert.ToBoolean(e.NewValue)
                If boolValue Then
                    Return
                End If

                ' If removing then ensure that there is something checked
                If CheckEdit_System.Checked AndAlso CheckEdit_Perm.Checked Then
                    Return
                End If

                ' Toggle the other item if we are un-checking a item
                If System.Object.Equals(sender, CheckEdit_Perm) Then
                    CheckEdit_System.Checked = True
                    Return
                End If

                CheckEdit_Perm.Checked = True

            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Sub CheckChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            ProductsNotesGrid1.ReadForm(bc, record, CheckEdit_Perm.Checked, CheckEdit_System.Checked)
        End Sub
    End Class
End Namespace
