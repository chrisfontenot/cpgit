﻿Imports DebtPlus.LINQ

Namespace controls
    Public Class ProductsDispute
        Inherits UserControl

        Private bc As BusinessContext = Nothing
        Private record As client_product = Nothing

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler LookUpEdit_disputed_status.EditValueChanged, AddressOf LookUpEdit_disputed_status_EditValueChanged
            AddHandler LookUpEdit_resolution_status.EditValueChanged, AddressOf LookUpEdit_disputed_status_EditValueChanged
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler LookUpEdit_disputed_status.EditValueChanged, AddressOf LookUpEdit_disputed_status_EditValueChanged
            RemoveHandler LookUpEdit_resolution_status.EditValueChanged, AddressOf LookUpEdit_disputed_status_EditValueChanged
        End Sub

        Friend Sub ReadForm(bc As BusinessContext, record As client_product)
            Me.bc = bc
            Me.record = record

            UnRegisterHandlers()
            Try
                LookUpEdit_disputed_status.Properties.DataSource = Cache.product_dispute_type.getList()
                LookUpEdit_disputed_status.EditValue = record.disputed_status
                LabelControl_disputed_date.Text = If(record.disputed_date.HasValue, record.disputed_date.Value.ToShortDateString(), String.Empty)
                MemoEdit_disputed_note.EditValue = record.disputed_note

                LookUpEdit_resolution_status.Properties.DataSource = Cache.product_resolution_type.getList()
                LookUpEdit_resolution_status.EditValue = record.resolution_status
                LabelControl_resolution_date.Text = If(record.resolution_date.HasValue, record.resolution_date.Value.ToShortDateString(), String.Empty)
                MemoExEdit_resolution_note.EditValue = record.resolution_note

                EnableControls()
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Friend Sub SaveForm()

            ' things are a bit more complex. We need to find the proper values for the record before we
            ' attempt to update the record. So, first, get the current settings.
            Dim newDisputedStatus As Nullable(Of Int32) = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_disputed_status.EditValue)
            Dim newDisputeNote As String = DebtPlus.Utils.Nulls.v_String(MemoEdit_disputed_note.EditValue)
            Dim newDisputedDate As Nullable(Of DateTime) = record.disputed_date
            Dim newResolutionStatus As Nullable(Of Int32) = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_resolution_status.EditValue)
            Dim newResolutionNote As String = DebtPlus.Utils.Nulls.v_String(MemoExEdit_resolution_note.EditValue)

            ' Now, if the dispute statis is set and there is no previous dispute status then we need to update the record
            ' with the new disputed status.
            If newDisputedStatus.HasValue AndAlso Nullable.Compare(newDisputedStatus, record.disputed_status) <> 0 Then
                If Not record.disputed_date.HasValue Then
                    If Not newDisputedDate.HasValue Then
                        newDisputedDate = BusinessContext.getdate()
                    End If
                End If

                ' If there is no reason then use the dispute type description
                If newDisputeNote Is Nothing Then
                    Dim qSel As product_dispute_type = TryCast(LookUpEdit_disputed_status.GetSelectedDataRow, product_dispute_type)
                    If qSel IsNot Nothing Then
                        newDisputeNote = qSel.Description
                    End If
                End If

                ' Update the disputed values in the record
                record.disputed_status = newDisputedStatus
                record.disputed_note = newDisputeNote
                record.disputed_date = newDisputedDate

                ' Mark the dollar amount for the disputed fiture appropriately
                record.disputed_amount = record.cost - record.discount - record.tendered

                ' Generate the transaction to show the disputed status values
                Dim log As product_transaction = Factory.Manufacture_product_transaction()
                log.client_product = record.Id
                log.cost = record.cost
                log.discount = record.discount
                log.disputed = record.disputed_amount
                log.note = record.disputed_note
                log.payment = record.tendered
                log.product = record.product
                log.transaction_type = "DS"
                log.vendor = record.vendor

                bc.product_transactions.InsertOnSubmit(log)
            End If

            ' Look for a change in the resolution status
            If newResolutionStatus.HasValue AndAlso Nullable.Compare(newResolutionStatus, record.resolution_status) <> 0 Then
                Dim newResolutionDate As Nullable(Of DateTime) = BusinessContext.getdate()

                ' If there is no reason then use the dispute type description
                If newResolutionNote Is Nothing Then
                    Dim qSel As product_resolution_type = TryCast(LookUpEdit_resolution_status.GetSelectedDataRow, product_resolution_type)
                    If qSel IsNot Nothing Then
                        newResolutionNote = qSel.Description
                    End If
                End If

                ' Update the disputed values in the record
                record.resolution_status = newResolutionStatus
                record.resolution_note = newResolutionNote
                record.resolution_date = newResolutionDate

                ' If the resolution status says to clear the dispute status then do so now.
                Dim q As product_resolution_type = TryCast(LookUpEdit_resolution_status.GetSelectedDataRow, product_resolution_type)
                If q IsNot Nothing AndAlso q.clear_dispute Then
                    record.disputed_status = Nothing
                    record.disputed_note = Nothing
                    record.disputed_date = Nothing

                    record.resolution_status = Nothing
                    record.resolution_date = Nothing
                    record.resolution_note = Nothing

                    ' Clear the disputed figure
                    record.disputed_amount = 0D
                End If

                ' Record the resolution status
                Dim log As product_transaction = Factory.Manufacture_product_transaction()
                log.transaction_type = "CD"
                log.vendor = record.vendor
                log.client_product = record.Id

                log.cost = record.cost
                log.discount = record.discount
                log.note = record.resolution_note
                log.payment = record.tendered
                log.product = record.product
                log.disputed = record.disputed_amount

                bc.product_transactions.InsertOnSubmit(log)
            End If
        End Sub

        Private Sub LookUpEdit_disputed_status_EditValueChanged(sender As Object, e As EventArgs)
            EnableControls()
        End Sub

        Private Sub EnableControls()

            ' If there is no disputed status then there is nothing here
            If LookUpEdit_disputed_status.EditValue Is Nothing Then
                MemoEdit_disputed_note.Enabled = False
                LookUpEdit_resolution_status.Enabled = False
                MemoExEdit_resolution_note.Enabled = False
                Return
            End If

            ' Disputed has been set. Enable the resolution.
            MemoEdit_disputed_note.Enabled = True
            LookUpEdit_resolution_status.Enabled = True

            ' If there is no resolution status then there is no note
            If LookUpEdit_resolution_status.EditValue Is Nothing Then
                MemoExEdit_resolution_note.Enabled = False
                Return
            End If

            MemoExEdit_resolution_note.Enabled = True
        End Sub
    End Class
End Namespace
