﻿Imports System.ComponentModel
Imports DebtPlus.LINQ
Imports System.Linq
Imports DebtPlus.Utils

Namespace controls
    Public Class ProductsBalance
        Inherits UserControl

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Private bc As BusinessContext = Nothing
        Private record As client_product = Nothing

        Private Sub RegisterHandlers()
            AddHandler TextEdit_Discount.Validating, AddressOf TextEdit_Discount_Validating
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler TextEdit_Discount.Validating, AddressOf TextEdit_Discount_Validating
        End Sub

        Friend Shadows Sub SaveForm()
            Dim discount As Decimal = Nulls.v_Decimal(TextEdit_Discount.EditValue).GetValueOrDefault(0D)

            ' If the discount is different then update the discount figure.
            If discount <> record.discount Then
                record.discount = discount

                ' Generate the log event for changing the discount aount
                Dim log As product_transaction = Factory.Manufacture_product_transaction()
                log.client_product = record.Id
                log.cost = record.cost
                log.discount = record.discount
                log.disputed = record.disputed_amount
                log.note = String.Format("Change discount to {0:c}", discount)
                log.payment = record.tendered
                log.product = record.product
                log.transaction_type = "DC"
                log.vendor = record.vendor

                bc.product_transactions.InsertOnSubmit(log)
            End If
        End Sub

        Friend Sub ReadForm(bc As BusinessContext, record As client_product)
            Me.bc = bc
            Me.record = record

            UnRegisterHandlers()
            Try
                LabelControl_OriginalBalance.Text = record.cost.ToString("c2")
                LabelControl_Disputed.Text = record.disputed_amount.ToString("c2")
                LabelControl_Payments.Text = record.tendered.ToString("c2")
                TextEdit_Discount.EditValue = record.discount

                Dim newBalance As Decimal = (record.cost - record.disputed_amount - record.discount - record.tendered)
                LabelControl_CurrentBalance.Text = newBalance.ToString("c2")

            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Sub TextEdit_Discount_Validating(sender As Object, e As CancelEventArgs)

            Dim discount As Decimal = Nulls.v_Decimal(TextEdit_Discount.EditValue).GetValueOrDefault(0D)
            Dim newBalance As Decimal = (record.cost - record.disputed_amount - discount - record.tendered)

            ' Do not allow the balance to go negative
            If newBalance < 0D Then
                e.Cancel = True
                Return
            End If

            LabelControl_CurrentBalance.Text = newBalance.ToString("c2")
        End Sub
    End Class
End Namespace
