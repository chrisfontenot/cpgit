﻿Namespace controls
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ClientDepositsControl
        Inherits MyGridControlClient

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            components = New System.ComponentModel.Container()
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.GridColumn_ClientDeposit = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_ClientDeposit.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_deposit_amount = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_deposit_amount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_deposit_date = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_deposit_date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridColumn_ClientDeposit
            '
            Me.GridColumn_ClientDeposit.Caption = "ID"
            Me.GridColumn_ClientDeposit.CustomizationCaption = "Record ID"
            Me.GridColumn_ClientDeposit.FieldName = "client_deposit"
            Me.GridColumn_ClientDeposit.Name = "GridColumn_ClientDeposit"
            Me.GridColumn_ClientDeposit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_ClientDeposit.DisplayFormat.FormatString = "{0:f0}"
            Me.GridColumn_ClientDeposit.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_ClientDeposit.GroupFormat.FormatString = "{0:f0}"
            Me.GridColumn_ClientDeposit.Visible = False
            Me.GridColumn_ClientDeposit.VisibleIndex = -1
            '
            'GridColumn_deposit_amount
            '
            Me.GridColumn_deposit_amount.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_deposit_amount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_deposit_amount.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_deposit_amount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_deposit_amount.Caption = "Amount"
            Me.GridColumn_deposit_amount.CustomizationCaption = "Amount of the deposit"
            Me.GridColumn_deposit_amount.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_deposit_amount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_deposit_amount.FieldName = "deposit_amount"
            Me.GridColumn_deposit_amount.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_deposit_amount.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_deposit_amount.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_deposit_amount.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_deposit_amount.Visible = True
            Me.GridColumn_deposit_amount.Name = "GridColumn_deposit_amount"
            Me.GridColumn_deposit_amount.ToolTip = "Amount of the deposit"
            Me.GridColumn_deposit_amount.VisibleIndex = 1
            '
            'GridColumn_deposit_date
            '
            Me.GridColumn_deposit_date.Caption = "Date"
            Me.GridColumn_deposit_date.CustomizationCaption = "Date expected for deposit"
            Me.GridColumn_deposit_date.FieldName = "deposit_date"
            Me.GridColumn_deposit_date.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_deposit_date.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_deposit_date.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_deposit_date.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_deposit_date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_deposit_date.DisplayFormat.FormatString = "{0:d}"
            Me.GridColumn_deposit_date.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_deposit_date.GroupFormat.FormatString = "{0:d}"
            Me.GridColumn_deposit_date.Name = "GridColumn_deposit_date"
            Me.GridColumn_deposit_date.Visible = True
            Me.GridColumn_deposit_date.VisibleIndex = 0
            '
            'GridView1
            '
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_ClientDeposit, Me.GridColumn_deposit_amount, Me.GridColumn_deposit_date})
            Me.GridView1.OptionsView.ShowFooter = True
            Me.GridView1.OptionsView.ShowIndicator = False
            '
            'Page_Budgets
            '
            Me.Name = "ClientDepositsControl"
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
        End Sub
        Friend WithEvents GridColumn_ClientDeposit As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_deposit_amount As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_deposit_date As DevExpress.XtraGrid.Columns.GridColumn
    End Class
End Namespace