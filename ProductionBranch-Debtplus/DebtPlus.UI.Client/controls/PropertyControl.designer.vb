Namespace controls

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class PropertyControl
        Inherits ControlBaseClient

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.SimpleButton_SendEmail = New DevExpress.XtraEditors.SimpleButton()
            Me.LookUpEdit_CallReason = New DevExpress.XtraEditors.LookUpEdit()
            Me.LookUpEdit_Sync = New DevExpress.XtraEditors.LookUpEdit()
            Me.CalcEdit_HOADelinqAmount = New DevExpress.XtraEditors.CalcEdit()
            Me.lu_HOADelinqState = New DevExpress.XtraEditors.LookUpEdit()
            Me.CalcEdit_InsDeliqAmount = New DevExpress.XtraEditors.CalcEdit()
            Me.CalcEdit_TaxDelinqAmount = New DevExpress.XtraEditors.CalcEdit()
            Me.dt_AppraisalDate = New DevExpress.XtraEditors.DateEdit()
            Me.lu_AppraisalType = New DevExpress.XtraEditors.LookUpEdit()
            Me.lu_HPFCounselor = New DevExpress.XtraEditors.LookUpEdit()
            Me.lu_HPF_SubProgram = New DevExpress.XtraEditors.LookUpEdit()
            Me.lu_HPF_Program = New DevExpress.XtraEditors.LookUpEdit()
            Me.CheckEdit_HPF_worked_with_other_agency = New DevExpress.XtraEditors.CheckEdit()
            Me.LabelControl_FCID = New DevExpress.XtraEditors.LabelControl()
            Me.HpfOutcomeListControl1 = New DebtPlus.UI.Client.controls.HPFOutcomeListControl()
            Me.lu_HomeOwners = New DevExpress.XtraEditors.LookUpEdit()
            Me.PropertyNoteListControl1 = New DebtPlus.UI.Client.controls.PropertyNoteListControl()
            Me.dt_InsuranceDueDate = New DevExpress.XtraEditors.DateEdit()
            Me.dt_Tax_Duedate = New DevExpress.XtraEditors.DateEdit()
            Me.lu_InsuranceDelinqState = New DevExpress.XtraEditors.LookUpEdit()
            Me.lu_PmiInsurance = New DevExpress.XtraEditors.LookUpEdit()
            Me.lu_TaxDelinqState = New DevExpress.XtraEditors.LookUpEdit()
            Me.lu_PropertyTaxes = New DevExpress.XtraEditors.LookUpEdit()
            Me.dt_sales_contract_date = New DevExpress.XtraEditors.DateEdit()
            Me.PropertyAddressControl1 = New DebtPlus.UI.Client.controls.PropertyAddressControl()
            Me.dt_FcSaleDT = New DevExpress.XtraEditors.DateEdit()
            Me.TextEdit_TrustName = New DevExpress.XtraEditors.TextEdit()
            Me.lu_Residency = New DevExpress.XtraEditors.LookUpEdit()
            Me.CalcEdit_LandValue = New DevExpress.XtraEditors.CalcEdit()
            Me.CheckEdit_UseInReports = New DevExpress.XtraEditors.CheckEdit()
            Me.LoanListControl1 = New DebtPlus.UI.Client.controls.LoanListControl()
            Me.CheckEdit_FcNoticeReceivedIND = New DevExpress.XtraEditors.CheckEdit()
            Me.BorrowerControl_CoOwnerID = New DebtPlus.UI.Client.controls.BorrowerControl()
            Me.BorrowerControl_OwnerID = New DebtPlus.UI.Client.controls.BorrowerControl()
            Me.SpinEdit_Occupancy = New DevExpress.XtraEditors.SpinEdit()
            Me.TextEdit_RealityCompany = New DevExpress.XtraEditors.TextEdit()
            Me.CalcEdit_ImprovementsValue = New DevExpress.XtraEditors.CalcEdit()
            Me.CalcEdit_PurchasePrice = New DevExpress.XtraEditors.CalcEdit()
            Me.TextEdit_PurchaseYear = New DevExpress.XtraEditors.TextEdit()
            Me.CalcEdit_SalePrice = New DevExpress.XtraEditors.CalcEdit()
            Me.lu_PropertyType = New DevExpress.XtraEditors.LookUpEdit()
            Me.CheckEdit_ForSaleIND = New DevExpress.XtraEditors.CheckEdit()
            Me.CalcEdit_txtAnnualInsuranceAmt = New DevExpress.XtraEditors.CalcEdit()
            Me.CalcEdit_AnnualTax = New DevExpress.XtraEditors.CalcEdit()
            Me.lu_PlanToKeepHome = New DevExpress.XtraEditors.LookUpEdit()
            Me.LayoutControlItem_Sync = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.TabbedControlGroup1 = New DevExpress.XtraLayout.TabbedControlGroup()
            Me.LayoutControlGroup9 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.EmptySpaceItem6 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlGroup10 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem23 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem24 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlGroup_General = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem_OccupancyNum = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_PurchasePrice = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_PropertyCD = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_CurrentMarketValue = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_PurchaseYear = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlGroup4 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem17 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlGroup_TaxesIns = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlGroup6 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem_PropTaxes = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_TaxDelinqState = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem10_APT = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem6_TDD = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem7 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.LayoutControlGroup7 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem7_AIA = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_InsDelinqState = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem8 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.LayoutControlItem11_IDD = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlGroup8 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem_PmiFhaInsurance = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem7_TI = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.LayoutControlGroup11 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem22 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem21 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem9 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem_LoanListControl1 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlGroup_Borrower = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem_BorrowerID = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlGroup_CoBorrower = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem_CoBorrower = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlGroup3 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem_RealityCompany = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_SalePrice = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.LayoutControlItem_ForSaleIND = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_FcNoticeReceivedIND = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem4 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem5 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.LayoutControlGroup5 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem10 = New DevExpress.XtraLayout.EmptySpaceItem()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.LookUpEdit_CallReason.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_Sync.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_HOADelinqAmount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lu_HOADelinqState.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_InsDeliqAmount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_TaxDelinqAmount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_AppraisalDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_AppraisalDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lu_AppraisalType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lu_HPFCounselor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lu_HPF_SubProgram.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lu_HPF_Program.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_HPF_worked_with_other_agency.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.HpfOutcomeListControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lu_HomeOwners.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PropertyNoteListControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_InsuranceDueDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_InsuranceDueDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_Tax_Duedate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_Tax_Duedate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lu_InsuranceDelinqState.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lu_PmiInsurance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lu_TaxDelinqState.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lu_PropertyTaxes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_sales_contract_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_sales_contract_date.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PropertyAddressControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_FcSaleDT.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_FcSaleDT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_TrustName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lu_Residency.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_LandValue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_UseInReports.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LoanListControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_FcNoticeReceivedIND.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.BorrowerControl_CoOwnerID, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.BorrowerControl_OwnerID, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.SpinEdit_Occupancy.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_RealityCompany.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_ImprovementsValue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_PurchasePrice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_PurchaseYear.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_SalePrice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lu_PropertyType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_ForSaleIND.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_txtAnnualInsuranceAmt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_AnnualTax.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lu_PlanToKeepHome.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_Sync, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TabbedControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup9, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup10, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup_General, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_OccupancyNum, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_PurchasePrice, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_PropertyCD, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_CurrentMarketValue, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_PurchaseYear, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup_TaxesIns, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_PropTaxes, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_TaxDelinqState, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem10_APT, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem6_TDD, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem7, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup7, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem7_AIA, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_InsDelinqState, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem8, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem11_IDD, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup8, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_PmiFhaInsurance, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem7_TI, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup11, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem9, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_LoanListControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup_Borrower, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_BorrowerID, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup_CoBorrower, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_CoBorrower, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_RealityCompany, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_SalePrice, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_ForSaleIND, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_FcNoticeReceivedIND, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem10, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.SimpleButton_SendEmail)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_CallReason)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_Sync)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit_HOADelinqAmount)
            Me.LayoutControl1.Controls.Add(Me.lu_HOADelinqState)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit_InsDeliqAmount)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit_TaxDelinqAmount)
            Me.LayoutControl1.Controls.Add(Me.dt_AppraisalDate)
            Me.LayoutControl1.Controls.Add(Me.lu_AppraisalType)
            Me.LayoutControl1.Controls.Add(Me.lu_HPFCounselor)
            Me.LayoutControl1.Controls.Add(Me.lu_HPF_SubProgram)
            Me.LayoutControl1.Controls.Add(Me.lu_HPF_Program)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_HPF_worked_with_other_agency)
            Me.LayoutControl1.Controls.Add(Me.LabelControl_FCID)
            Me.LayoutControl1.Controls.Add(Me.HpfOutcomeListControl1)
            Me.LayoutControl1.Controls.Add(Me.lu_HomeOwners)
            Me.LayoutControl1.Controls.Add(Me.PropertyNoteListControl1)
            Me.LayoutControl1.Controls.Add(Me.dt_InsuranceDueDate)
            Me.LayoutControl1.Controls.Add(Me.dt_Tax_Duedate)
            Me.LayoutControl1.Controls.Add(Me.lu_InsuranceDelinqState)
            Me.LayoutControl1.Controls.Add(Me.lu_PmiInsurance)
            Me.LayoutControl1.Controls.Add(Me.lu_TaxDelinqState)
            Me.LayoutControl1.Controls.Add(Me.lu_PropertyTaxes)
            Me.LayoutControl1.Controls.Add(Me.dt_sales_contract_date)
            Me.LayoutControl1.Controls.Add(Me.PropertyAddressControl1)
            Me.LayoutControl1.Controls.Add(Me.dt_FcSaleDT)
            Me.LayoutControl1.Controls.Add(Me.TextEdit_TrustName)
            Me.LayoutControl1.Controls.Add(Me.lu_Residency)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit_LandValue)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_UseInReports)
            Me.LayoutControl1.Controls.Add(Me.LoanListControl1)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_FcNoticeReceivedIND)
            Me.LayoutControl1.Controls.Add(Me.BorrowerControl_CoOwnerID)
            Me.LayoutControl1.Controls.Add(Me.BorrowerControl_OwnerID)
            Me.LayoutControl1.Controls.Add(Me.SpinEdit_Occupancy)
            Me.LayoutControl1.Controls.Add(Me.TextEdit_RealityCompany)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit_ImprovementsValue)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit_PurchasePrice)
            Me.LayoutControl1.Controls.Add(Me.TextEdit_PurchaseYear)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit_SalePrice)
            Me.LayoutControl1.Controls.Add(Me.lu_PropertyType)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_ForSaleIND)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit_txtAnnualInsuranceAmt)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit_AnnualTax)
            Me.LayoutControl1.Controls.Add(Me.lu_PlanToKeepHome)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.HiddenItems.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem_Sync})
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(548, 433)
            Me.LayoutControl1.TabIndex = 0
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'SimpleButton_SendEmail
            '
            Me.SimpleButton_SendEmail.Location = New System.Drawing.Point(447, 169)
            Me.SimpleButton_SendEmail.MaximumSize = New System.Drawing.Size(84, 26)
            Me.SimpleButton_SendEmail.MinimumSize = New System.Drawing.Size(0, 26)
            Me.SimpleButton_SendEmail.Name = "SimpleButton_SendEmail"
            Me.SimpleButton_SendEmail.Size = New System.Drawing.Size(84, 26)
            Me.SimpleButton_SendEmail.StyleController = Me.LayoutControl1
            Me.SimpleButton_SendEmail.TabIndex = 65
            Me.SimpleButton_SendEmail.Text = "Send BluePrint"
            Me.SimpleButton_SendEmail.ToolTip = "Send client the HPF BluePrint Brochure Email"
            '
            'LookUpEdit_CallReason
            '
            Me.LookUpEdit_CallReason.Location = New System.Drawing.Point(128, 128)
            Me.LookUpEdit_CallReason.Name = "LookUpEdit_CallReason"
            Me.LookUpEdit_CallReason.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_CallReason.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.LookUpEdit_CallReason.Properties.DisplayMember = "description"
            Me.LookUpEdit_CallReason.Properties.NullText = ""
            Me.LookUpEdit_CallReason.Properties.ShowFooter = False
            Me.LookUpEdit_CallReason.Properties.ShowHeader = False
            Me.LookUpEdit_CallReason.Properties.SortColumnIndex = 1
            Me.LookUpEdit_CallReason.Properties.ValueMember = "Id"
            Me.LookUpEdit_CallReason.Size = New System.Drawing.Size(403, 20)
            Me.LookUpEdit_CallReason.StyleController = Me.LayoutControl1
            Me.LookUpEdit_CallReason.TabIndex = 64
            Me.LookUpEdit_CallReason.ToolTip = "Reason for the call to the hotline"
            '
            'LookUpEdit_Sync
            '
            Me.LookUpEdit_Sync.Location = New System.Drawing.Point(125, 213)
            Me.LookUpEdit_Sync.Name = "LookUpEdit_Sync"
            Me.LookUpEdit_Sync.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_Sync.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("oID", "ID", 5, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_Sync.Properties.DisplayMember = "Description"
            Me.LookUpEdit_Sync.Properties.NullText = ""
            Me.LookUpEdit_Sync.Properties.ValueMember = "oID"
            Me.LookUpEdit_Sync.Size = New System.Drawing.Size(354, 20)
            Me.LookUpEdit_Sync.StyleController = Me.LayoutControl1
            Me.LookUpEdit_Sync.TabIndex = 20
            '
            'CalcEdit_HOADelinqAmount
            '
            Me.CalcEdit_HOADelinqAmount.Location = New System.Drawing.Point(398, 371)
            Me.CalcEdit_HOADelinqAmount.Name = "CalcEdit_HOADelinqAmount"
            Me.CalcEdit_HOADelinqAmount.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit_HOADelinqAmount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_HOADelinqAmount.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_HOADelinqAmount.Properties.DisplayFormat.FormatString = "c"
            Me.CalcEdit_HOADelinqAmount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_HOADelinqAmount.Properties.EditFormat.FormatString = "c"
            Me.CalcEdit_HOADelinqAmount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_HOADelinqAmount.Properties.Mask.BeepOnError = True
            Me.CalcEdit_HOADelinqAmount.Properties.Mask.EditMask = "c"
            Me.CalcEdit_HOADelinqAmount.Properties.NullText = "$0.00"
            Me.CalcEdit_HOADelinqAmount.Properties.Precision = 2
            Me.CalcEdit_HOADelinqAmount.Size = New System.Drawing.Size(121, 20)
            Me.CalcEdit_HOADelinqAmount.StyleController = Me.LayoutControl1
            Me.CalcEdit_HOADelinqAmount.TabIndex = 63
            '
            'lu_HOADelinqState
            '
            Me.lu_HOADelinqState.Location = New System.Drawing.Point(140, 371)
            Me.lu_HOADelinqState.Name = "lu_HOADelinqState"
            Me.lu_HOADelinqState.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lu_HOADelinqState.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 5, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default])})
            Me.lu_HOADelinqState.Properties.DisplayMember = "description"
            Me.lu_HOADelinqState.Properties.DropDownRows = 3
            Me.lu_HOADelinqState.Properties.NullText = ""
            Me.lu_HOADelinqState.Properties.ShowFooter = False
            Me.lu_HOADelinqState.Properties.ShowHeader = False
            Me.lu_HOADelinqState.Properties.ValueMember = "Id"
            Me.lu_HOADelinqState.Size = New System.Drawing.Size(131, 20)
            Me.lu_HOADelinqState.StyleController = Me.LayoutControl1
            Me.lu_HOADelinqState.TabIndex = 62
            '
            'CalcEdit_InsDeliqAmount
            '
            Me.CalcEdit_InsDeliqAmount.Location = New System.Drawing.Point(397, 211)
            Me.CalcEdit_InsDeliqAmount.Name = "CalcEdit_InsDeliqAmount"
            Me.CalcEdit_InsDeliqAmount.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit_InsDeliqAmount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_InsDeliqAmount.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_InsDeliqAmount.Properties.DisplayFormat.FormatString = "c"
            Me.CalcEdit_InsDeliqAmount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_InsDeliqAmount.Properties.EditFormat.FormatString = "c"
            Me.CalcEdit_InsDeliqAmount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_InsDeliqAmount.Properties.Mask.BeepOnError = True
            Me.CalcEdit_InsDeliqAmount.Properties.Mask.EditMask = "c"
            Me.CalcEdit_InsDeliqAmount.Properties.NullText = "$0.00"
            Me.CalcEdit_InsDeliqAmount.Properties.Precision = 2
            Me.CalcEdit_InsDeliqAmount.Size = New System.Drawing.Size(122, 20)
            Me.CalcEdit_InsDeliqAmount.StyleController = Me.LayoutControl1
            Me.CalcEdit_InsDeliqAmount.TabIndex = 60
            '
            'CalcEdit_TaxDelinqAmount
            '
            Me.CalcEdit_TaxDelinqAmount.Location = New System.Drawing.Point(398, 95)
            Me.CalcEdit_TaxDelinqAmount.Name = "CalcEdit_TaxDelinqAmount"
            Me.CalcEdit_TaxDelinqAmount.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit_TaxDelinqAmount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_TaxDelinqAmount.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_TaxDelinqAmount.Properties.DisplayFormat.FormatString = "c"
            Me.CalcEdit_TaxDelinqAmount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_TaxDelinqAmount.Properties.EditFormat.FormatString = "c"
            Me.CalcEdit_TaxDelinqAmount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_TaxDelinqAmount.Properties.Mask.BeepOnError = True
            Me.CalcEdit_TaxDelinqAmount.Properties.Mask.EditMask = "c"
            Me.CalcEdit_TaxDelinqAmount.Properties.NullText = "$0.00"
            Me.CalcEdit_TaxDelinqAmount.Properties.Precision = 2
            Me.CalcEdit_TaxDelinqAmount.Size = New System.Drawing.Size(121, 20)
            Me.CalcEdit_TaxDelinqAmount.StyleController = Me.LayoutControl1
            Me.CalcEdit_TaxDelinqAmount.TabIndex = 58
            '
            'dt_AppraisalDate
            '
            Me.dt_AppraisalDate.EditValue = Nothing
            Me.dt_AppraisalDate.Location = New System.Drawing.Point(356, 159)
            Me.dt_AppraisalDate.Name = "dt_AppraisalDate"
            Me.dt_AppraisalDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.dt_AppraisalDate.Properties.Mask.UseMaskAsDisplayFormat = True
            Me.dt_AppraisalDate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.dt_AppraisalDate.Size = New System.Drawing.Size(175, 20)
            Me.dt_AppraisalDate.StyleController = Me.LayoutControl1
            Me.dt_AppraisalDate.TabIndex = 44
            '
            'lu_AppraisalType
            '
            Me.lu_AppraisalType.Location = New System.Drawing.Point(128, 159)
            Me.lu_AppraisalType.Name = "lu_AppraisalType"
            Me.lu_AppraisalType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lu_AppraisalType.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 5, DevExpress.Utils.FormatType.Numeric, "", False, DevExpress.Utils.HorzAlignment.[Default])})
            Me.lu_AppraisalType.Properties.DisplayMember = "Description"
            Me.lu_AppraisalType.Properties.DropDownRows = 3
            Me.lu_AppraisalType.Properties.NullText = ""
            Me.lu_AppraisalType.Properties.ShowFooter = False
            Me.lu_AppraisalType.Properties.ShowHeader = False
            Me.lu_AppraisalType.Properties.ValueMember = "ID"
            Me.lu_AppraisalType.Size = New System.Drawing.Size(113, 20)
            Me.lu_AppraisalType.StyleController = Me.LayoutControl1
            Me.lu_AppraisalType.TabIndex = 43
            '
            'lu_HPFCounselor
            '
            Me.lu_HPFCounselor.Location = New System.Drawing.Point(128, 56)
            Me.lu_HPFCounselor.Name = "lu_HPFCounselor"
            Me.lu_HPFCounselor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lu_HPFCounselor.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 5, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Note", 5, "Note")})
            Me.lu_HPFCounselor.Properties.DisplayMember = "Name"
            Me.lu_HPFCounselor.Properties.NullText = ""
            Me.lu_HPFCounselor.Properties.ShowFooter = False
            Me.lu_HPFCounselor.Properties.ShowLines = False
            Me.lu_HPFCounselor.Properties.SortColumnIndex = 1
            Me.lu_HPFCounselor.Properties.ValueMember = "Id"
            Me.lu_HPFCounselor.Size = New System.Drawing.Size(403, 20)
            Me.lu_HPFCounselor.StyleController = Me.LayoutControl1
            Me.lu_HPFCounselor.TabIndex = 48
            Me.lu_HPFCounselor.ToolTip = "The counselor assigned to this client."
            '
            'lu_HPF_SubProgram
            '
            Me.lu_HPF_SubProgram.Location = New System.Drawing.Point(128, 104)
            Me.lu_HPF_SubProgram.Name = "lu_HPF_SubProgram"
            Me.lu_HPF_SubProgram.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lu_HPF_SubProgram.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.lu_HPF_SubProgram.Properties.DisplayMember = "description"
            Me.lu_HPF_SubProgram.Properties.NullText = ""
            Me.lu_HPF_SubProgram.Properties.ShowFooter = False
            Me.lu_HPF_SubProgram.Properties.ShowHeader = False
            Me.lu_HPF_SubProgram.Properties.SortColumnIndex = 1
            Me.lu_HPF_SubProgram.Properties.ValueMember = "Id"
            Me.lu_HPF_SubProgram.Size = New System.Drawing.Size(403, 20)
            Me.lu_HPF_SubProgram.StyleController = Me.LayoutControl1
            Me.lu_HPF_SubProgram.TabIndex = 47
            '
            'lu_HPF_Program
            '
            Me.lu_HPF_Program.Location = New System.Drawing.Point(128, 80)
            Me.lu_HPF_Program.Name = "lu_HPF_Program"
            Me.lu_HPF_Program.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lu_HPF_Program.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.lu_HPF_Program.Properties.DisplayMember = "description"
            Me.lu_HPF_Program.Properties.NullText = ""
            Me.lu_HPF_Program.Properties.ShowFooter = False
            Me.lu_HPF_Program.Properties.ShowHeader = False
            Me.lu_HPF_Program.Properties.SortColumnIndex = 1
            Me.lu_HPF_Program.Properties.ValueMember = "Id"
            Me.lu_HPF_Program.Size = New System.Drawing.Size(403, 20)
            Me.lu_HPF_Program.StyleController = Me.LayoutControl1
            Me.lu_HPF_Program.TabIndex = 46
            '
            'CheckEdit_HPF_worked_with_other_agency
            '
            Me.CheckEdit_HPF_worked_with_other_agency.Location = New System.Drawing.Point(17, 169)
            Me.CheckEdit_HPF_worked_with_other_agency.Name = "CheckEdit_HPF_worked_with_other_agency"
            Me.CheckEdit_HPF_worked_with_other_agency.Properties.Caption = "Worked with other agency"
            Me.CheckEdit_HPF_worked_with_other_agency.Size = New System.Drawing.Size(426, 20)
            Me.CheckEdit_HPF_worked_with_other_agency.StyleController = Me.LayoutControl1
            Me.CheckEdit_HPF_worked_with_other_agency.TabIndex = 45
            '
            'LabelControl_FCID
            '
            Me.LabelControl_FCID.Location = New System.Drawing.Point(128, 39)
            Me.LabelControl_FCID.Name = "LabelControl_FCID"
            Me.LabelControl_FCID.Size = New System.Drawing.Size(18, 13)
            Me.LabelControl_FCID.StyleController = Me.LayoutControl1
            Me.LabelControl_FCID.TabIndex = 44
            Me.LabelControl_FCID.Text = "N/A"
            '
            'HpfOutcomeListControl1
            '
            Me.HpfOutcomeListControl1.Location = New System.Drawing.Point(29, 255)
            Me.HpfOutcomeListControl1.Name = "HpfOutcomeListControl1"
            Me.HpfOutcomeListControl1.Size = New System.Drawing.Size(490, 149)
            Me.HpfOutcomeListControl1.TabIndex = 43
            '
            'lu_HomeOwners
            '
            Me.lu_HomeOwners.Location = New System.Drawing.Point(140, 187)
            Me.lu_HomeOwners.Name = "lu_HomeOwners"
            Me.lu_HomeOwners.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lu_HomeOwners.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 5, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default])})
            Me.lu_HomeOwners.Properties.DisplayMember = "description"
            Me.lu_HomeOwners.Properties.DropDownRows = 3
            Me.lu_HomeOwners.Properties.NullText = ""
            Me.lu_HomeOwners.Properties.ShowFooter = False
            Me.lu_HomeOwners.Properties.ShowHeader = False
            Me.lu_HomeOwners.Properties.ValueMember = "Id"
            Me.lu_HomeOwners.Size = New System.Drawing.Size(130, 20)
            Me.lu_HomeOwners.StyleController = Me.LayoutControl1
            Me.lu_HomeOwners.TabIndex = 42
            '
            'PropertyNoteListControl1
            '
            Me.PropertyNoteListControl1.Location = New System.Drawing.Point(17, 39)
            Me.PropertyNoteListControl1.Name = "PropertyNoteListControl1"
            Me.PropertyNoteListControl1.Size = New System.Drawing.Size(514, 377)
            Me.PropertyNoteListControl1.TabIndex = 41
            '
            'dt_InsuranceDueDate
            '
            Me.dt_InsuranceDueDate.EditValue = Nothing
            Me.dt_InsuranceDueDate.Location = New System.Drawing.Point(140, 235)
            Me.dt_InsuranceDueDate.Name = "dt_InsuranceDueDate"
            Me.dt_InsuranceDueDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.dt_InsuranceDueDate.Properties.Mask.UseMaskAsDisplayFormat = True
            Me.dt_InsuranceDueDate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.dt_InsuranceDueDate.Properties.VistaTimeProperties.DisplayFormat.FormatString = "d"
            Me.dt_InsuranceDueDate.Properties.VistaTimeProperties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.dt_InsuranceDueDate.Properties.VistaTimeProperties.EditFormat.FormatString = "d"
            Me.dt_InsuranceDueDate.Properties.VistaTimeProperties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.dt_InsuranceDueDate.Properties.VistaTimeProperties.Mask.EditMask = "d"
            Me.dt_InsuranceDueDate.Size = New System.Drawing.Size(130, 20)
            Me.dt_InsuranceDueDate.StyleController = Me.LayoutControl1
            Me.dt_InsuranceDueDate.TabIndex = 40
            '
            'dt_Tax_Duedate
            '
            Me.dt_Tax_Duedate.EditValue = Nothing
            Me.dt_Tax_Duedate.Location = New System.Drawing.Point(140, 119)
            Me.dt_Tax_Duedate.Name = "dt_Tax_Duedate"
            Me.dt_Tax_Duedate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.dt_Tax_Duedate.Properties.Mask.UseMaskAsDisplayFormat = True
            Me.dt_Tax_Duedate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.dt_Tax_Duedate.Properties.VistaTimeProperties.DisplayFormat.FormatString = "d"
            Me.dt_Tax_Duedate.Properties.VistaTimeProperties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.dt_Tax_Duedate.Properties.VistaTimeProperties.EditFormat.FormatString = "d"
            Me.dt_Tax_Duedate.Properties.VistaTimeProperties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.dt_Tax_Duedate.Properties.VistaTimeProperties.Mask.EditMask = "d"
            Me.dt_Tax_Duedate.Size = New System.Drawing.Size(131, 20)
            Me.dt_Tax_Duedate.StyleController = Me.LayoutControl1
            Me.dt_Tax_Duedate.TabIndex = 37
            '
            'lu_InsuranceDelinqState
            '
            Me.lu_InsuranceDelinqState.Location = New System.Drawing.Point(140, 211)
            Me.lu_InsuranceDelinqState.Name = "lu_InsuranceDelinqState"
            Me.lu_InsuranceDelinqState.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lu_InsuranceDelinqState.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 5, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default])})
            Me.lu_InsuranceDelinqState.Properties.DisplayMember = "description"
            Me.lu_InsuranceDelinqState.Properties.DropDownRows = 3
            Me.lu_InsuranceDelinqState.Properties.NullText = ""
            Me.lu_InsuranceDelinqState.Properties.ShowFooter = False
            Me.lu_InsuranceDelinqState.Properties.ShowHeader = False
            Me.lu_InsuranceDelinqState.Properties.ValueMember = "Id"
            Me.lu_InsuranceDelinqState.Size = New System.Drawing.Size(130, 20)
            Me.lu_InsuranceDelinqState.StyleController = Me.LayoutControl1
            Me.lu_InsuranceDelinqState.TabIndex = 35
            '
            'lu_PmiInsurance
            '
            Me.lu_PmiInsurance.Location = New System.Drawing.Point(140, 303)
            Me.lu_PmiInsurance.Name = "lu_PmiInsurance"
            Me.lu_PmiInsurance.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lu_PmiInsurance.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "description", 5, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 5, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default])})
            Me.lu_PmiInsurance.Properties.DisplayMember = "description"
            Me.lu_PmiInsurance.Properties.DropDownRows = 3
            Me.lu_PmiInsurance.Properties.NullText = ""
            Me.lu_PmiInsurance.Properties.ShowFooter = False
            Me.lu_PmiInsurance.Properties.ShowHeader = False
            Me.lu_PmiInsurance.Properties.ValueMember = "Id"
            Me.lu_PmiInsurance.Size = New System.Drawing.Size(131, 20)
            Me.lu_PmiInsurance.StyleController = Me.LayoutControl1
            Me.lu_PmiInsurance.TabIndex = 34
            '
            'lu_TaxDelinqState
            '
            Me.lu_TaxDelinqState.Location = New System.Drawing.Point(140, 95)
            Me.lu_TaxDelinqState.Name = "lu_TaxDelinqState"
            Me.lu_TaxDelinqState.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lu_TaxDelinqState.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 5, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default])})
            Me.lu_TaxDelinqState.Properties.DisplayMember = "description"
            Me.lu_TaxDelinqState.Properties.DropDownRows = 3
            Me.lu_TaxDelinqState.Properties.NullText = ""
            Me.lu_TaxDelinqState.Properties.ShowFooter = False
            Me.lu_TaxDelinqState.Properties.ShowHeader = False
            Me.lu_TaxDelinqState.Properties.ValueMember = "Id"
            Me.lu_TaxDelinqState.Size = New System.Drawing.Size(131, 20)
            Me.lu_TaxDelinqState.StyleController = Me.LayoutControl1
            Me.lu_TaxDelinqState.TabIndex = 33
            '
            'lu_PropertyTaxes
            '
            Me.lu_PropertyTaxes.Location = New System.Drawing.Point(140, 71)
            Me.lu_PropertyTaxes.Name = "lu_PropertyTaxes"
            Me.lu_PropertyTaxes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lu_PropertyTaxes.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 5, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default])})
            Me.lu_PropertyTaxes.Properties.DisplayMember = "description"
            Me.lu_PropertyTaxes.Properties.DropDownRows = 3
            Me.lu_PropertyTaxes.Properties.NullText = ""
            Me.lu_PropertyTaxes.Properties.ShowFooter = False
            Me.lu_PropertyTaxes.Properties.ShowHeader = False
            Me.lu_PropertyTaxes.Properties.ValueMember = "Id"
            Me.lu_PropertyTaxes.Size = New System.Drawing.Size(131, 20)
            Me.lu_PropertyTaxes.StyleController = Me.LayoutControl1
            Me.lu_PropertyTaxes.TabIndex = 32
            '
            'dt_sales_contract_date
            '
            Me.dt_sales_contract_date.EditValue = Nothing
            Me.dt_sales_contract_date.Location = New System.Drawing.Point(356, 87)
            Me.dt_sales_contract_date.Name = "dt_sales_contract_date"
            Me.dt_sales_contract_date.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.dt_sales_contract_date.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.dt_sales_contract_date.Size = New System.Drawing.Size(175, 20)
            Me.dt_sales_contract_date.StyleController = Me.LayoutControl1
            Me.dt_sales_contract_date.TabIndex = 31
            '
            'PropertyAddressControl1
            '
            Me.PropertyAddressControl1.Location = New System.Drawing.Point(29, 225)
            Me.PropertyAddressControl1.Name = "PropertyAddressControl1"
            Me.PropertyAddressControl1.Size = New System.Drawing.Size(490, 179)
            Me.PropertyAddressControl1.TabIndex = 30
            '
            'dt_FcSaleDT
            '
            Me.dt_FcSaleDT.EditValue = Nothing
            Me.dt_FcSaleDT.Location = New System.Drawing.Point(356, 138)
            Me.dt_FcSaleDT.Name = "dt_FcSaleDT"
            Me.dt_FcSaleDT.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.dt_FcSaleDT.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.dt_FcSaleDT.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.dt_FcSaleDT.Size = New System.Drawing.Size(175, 20)
            Me.dt_FcSaleDT.StyleController = Me.LayoutControl1
            Me.dt_FcSaleDT.TabIndex = 28
            '
            'TextEdit_TrustName
            '
            Me.TextEdit_TrustName.Location = New System.Drawing.Point(128, 39)
            Me.TextEdit_TrustName.Name = "TextEdit_TrustName"
            Me.TextEdit_TrustName.Properties.MaxLength = 50
            Me.TextEdit_TrustName.Properties.NullValuePrompt = "If property in a trust, what is the name of the trust?"
            Me.TextEdit_TrustName.Properties.NullValuePromptShowForEmptyValue = True
            Me.TextEdit_TrustName.Size = New System.Drawing.Size(113, 20)
            Me.TextEdit_TrustName.StyleController = Me.LayoutControl1
            Me.TextEdit_TrustName.TabIndex = 24
            Me.TextEdit_TrustName.ToolTip = "If property in a trust, what is the name of the trust?"
            '
            'lu_Residency
            '
            Me.lu_Residency.Enabled = False
            Me.lu_Residency.Location = New System.Drawing.Point(128, 87)
            Me.lu_Residency.Name = "lu_Residency"
            Me.lu_Residency.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.lu_Residency.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lu_Residency.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 5, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.lu_Residency.Properties.DisplayMember = "description"
            Me.lu_Residency.Properties.NullText = ""
            Me.lu_Residency.Properties.ShowFooter = False
            Me.lu_Residency.Properties.ShowHeader = False
            Me.lu_Residency.Properties.SortColumnIndex = 1
            Me.lu_Residency.Properties.ValueMember = "Id"
            Me.lu_Residency.Size = New System.Drawing.Size(113, 20)
            Me.lu_Residency.StyleController = Me.LayoutControl1
            Me.lu_Residency.TabIndex = 23
            Me.lu_Residency.ToolTip = "Is the property occupied by the owner or is it an income property"
            '
            'CalcEdit_LandValue
            '
            Me.CalcEdit_LandValue.Location = New System.Drawing.Point(128, 135)
            Me.CalcEdit_LandValue.Name = "CalcEdit_LandValue"
            Me.CalcEdit_LandValue.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit_LandValue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_LandValue.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_LandValue.Properties.DisplayFormat.FormatString = "{0:c}"
            Me.CalcEdit_LandValue.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_LandValue.Properties.EditFormat.FormatString = "f2"
            Me.CalcEdit_LandValue.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_LandValue.Properties.Mask.BeepOnError = True
            Me.CalcEdit_LandValue.Properties.Mask.EditMask = "c"
            Me.CalcEdit_LandValue.Properties.Mask.SaveLiteral = False
            Me.CalcEdit_LandValue.Properties.Precision = 2
            Me.CalcEdit_LandValue.Size = New System.Drawing.Size(113, 20)
            Me.CalcEdit_LandValue.StyleController = Me.LayoutControl1
            Me.CalcEdit_LandValue.TabIndex = 22
            Me.CalcEdit_LandValue.ToolTip = "What is the value of the land alone for the property?"
            '
            'CheckEdit_UseInReports
            '
            Me.CheckEdit_UseInReports.Location = New System.Drawing.Point(245, 39)
            Me.CheckEdit_UseInReports.Name = "CheckEdit_UseInReports"
            Me.CheckEdit_UseInReports.Properties.Caption = "Report on this property for housing"
            Me.CheckEdit_UseInReports.Size = New System.Drawing.Size(286, 20)
            Me.CheckEdit_UseInReports.StyleController = Me.LayoutControl1
            Me.CheckEdit_UseInReports.TabIndex = 29
            '
            'LoanListControl1
            '
            Me.LoanListControl1.Location = New System.Drawing.Point(17, 39)
            Me.LoanListControl1.Name = "LoanListControl1"
            Me.LoanListControl1.Size = New System.Drawing.Size(514, 377)
            Me.LoanListControl1.TabIndex = 21
            '
            'CheckEdit_FcNoticeReceivedIND
            '
            Me.CheckEdit_FcNoticeReceivedIND.Location = New System.Drawing.Point(17, 138)
            Me.CheckEdit_FcNoticeReceivedIND.Name = "CheckEdit_FcNoticeReceivedIND"
            Me.CheckEdit_FcNoticeReceivedIND.Properties.Caption = "Foreclosure Notice Received"
            Me.CheckEdit_FcNoticeReceivedIND.Size = New System.Drawing.Size(224, 20)
            Me.CheckEdit_FcNoticeReceivedIND.StyleController = Me.LayoutControl1
            Me.CheckEdit_FcNoticeReceivedIND.TabIndex = 19
            Me.CheckEdit_FcNoticeReceivedIND.ToolTip = "Was a foreclosure notice received on this property?"
            '
            'BorrowerControl_CoOwnerID
            '
            Me.BorrowerControl_CoOwnerID.Appearance.BackColor = System.Drawing.Color.Yellow
            Me.BorrowerControl_CoOwnerID.Appearance.ForeColor = System.Drawing.SystemColors.Desktop
            Me.BorrowerControl_CoOwnerID.Appearance.Options.UseBackColor = True
            Me.BorrowerControl_CoOwnerID.Appearance.Options.UseForeColor = True
            Me.BorrowerControl_CoOwnerID.Location = New System.Drawing.Point(17, 39)
            Me.BorrowerControl_CoOwnerID.Name = "BorrowerControl_CoOwnerID"
            Me.BorrowerControl_CoOwnerID.PersonID = 2
            Me.BorrowerControl_CoOwnerID.Size = New System.Drawing.Size(514, 377)
            Me.BorrowerControl_CoOwnerID.TabIndex = 16
            '
            'BorrowerControl_OwnerID
            '
            Me.BorrowerControl_OwnerID.Location = New System.Drawing.Point(17, 39)
            Me.BorrowerControl_OwnerID.Name = "BorrowerControl_OwnerID"
            Me.BorrowerControl_OwnerID.PersonID = 1
            Me.BorrowerControl_OwnerID.Size = New System.Drawing.Size(514, 377)
            Me.BorrowerControl_OwnerID.TabIndex = 15
            '
            'SpinEdit_Occupancy
            '
            Me.SpinEdit_Occupancy.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
            Me.SpinEdit_Occupancy.Location = New System.Drawing.Point(356, 87)
            Me.SpinEdit_Occupancy.Name = "SpinEdit_Occupancy"
            Me.SpinEdit_Occupancy.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.SpinEdit_Occupancy.Properties.DisplayFormat.FormatString = "{0:f0}"
            Me.SpinEdit_Occupancy.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.SpinEdit_Occupancy.Properties.EditFormat.FormatString = "{0:f0}"
            Me.SpinEdit_Occupancy.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.SpinEdit_Occupancy.Properties.IsFloatValue = False
            Me.SpinEdit_Occupancy.Properties.Mask.EditMask = "N00"
            Me.SpinEdit_Occupancy.Properties.MaxValue = New Decimal(New Integer() {31, 0, 0, 0})
            Me.SpinEdit_Occupancy.Size = New System.Drawing.Size(175, 20)
            Me.SpinEdit_Occupancy.StyleController = Me.LayoutControl1
            Me.SpinEdit_Occupancy.TabIndex = 14
            Me.SpinEdit_Occupancy.ToolTip = "How many people are living there?"
            '
            'TextEdit_RealityCompany
            '
            Me.TextEdit_RealityCompany.Location = New System.Drawing.Point(128, 39)
            Me.TextEdit_RealityCompany.Name = "TextEdit_RealityCompany"
            Me.TextEdit_RealityCompany.Size = New System.Drawing.Size(403, 20)
            Me.TextEdit_RealityCompany.StyleController = Me.LayoutControl1
            Me.TextEdit_RealityCompany.TabIndex = 13
            Me.TextEdit_RealityCompany.ToolTip = "Name of the reality company that is selling the property"
            '
            'CalcEdit_ImprovementsValue
            '
            Me.CalcEdit_ImprovementsValue.Location = New System.Drawing.Point(356, 135)
            Me.CalcEdit_ImprovementsValue.Name = "CalcEdit_ImprovementsValue"
            Me.CalcEdit_ImprovementsValue.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit_ImprovementsValue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_ImprovementsValue.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_ImprovementsValue.Properties.DisplayFormat.FormatString = "{0:c}"
            Me.CalcEdit_ImprovementsValue.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_ImprovementsValue.Properties.EditFormat.FormatString = "f2"
            Me.CalcEdit_ImprovementsValue.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_ImprovementsValue.Properties.Mask.BeepOnError = True
            Me.CalcEdit_ImprovementsValue.Properties.Mask.EditMask = "c"
            Me.CalcEdit_ImprovementsValue.Properties.Mask.SaveLiteral = False
            Me.CalcEdit_ImprovementsValue.Properties.Precision = 2
            Me.CalcEdit_ImprovementsValue.Size = New System.Drawing.Size(175, 20)
            Me.CalcEdit_ImprovementsValue.StyleController = Me.LayoutControl1
            Me.CalcEdit_ImprovementsValue.TabIndex = 8
            Me.CalcEdit_ImprovementsValue.ToolTip = "What is the value of the improvements (house, pool, etc.) on the property?"
            '
            'CalcEdit_PurchasePrice
            '
            Me.CalcEdit_PurchasePrice.Location = New System.Drawing.Point(356, 111)
            Me.CalcEdit_PurchasePrice.Name = "CalcEdit_PurchasePrice"
            Me.CalcEdit_PurchasePrice.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit_PurchasePrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_PurchasePrice.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_PurchasePrice.Properties.DisplayFormat.FormatString = "{0:c}"
            Me.CalcEdit_PurchasePrice.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_PurchasePrice.Properties.EditFormat.FormatString = "f2"
            Me.CalcEdit_PurchasePrice.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_PurchasePrice.Properties.Mask.BeepOnError = True
            Me.CalcEdit_PurchasePrice.Properties.Mask.EditMask = "c"
            Me.CalcEdit_PurchasePrice.Properties.Mask.SaveLiteral = False
            Me.CalcEdit_PurchasePrice.Properties.Precision = 2
            Me.CalcEdit_PurchasePrice.Size = New System.Drawing.Size(175, 20)
            Me.CalcEdit_PurchasePrice.StyleController = Me.LayoutControl1
            Me.CalcEdit_PurchasePrice.TabIndex = 7
            Me.CalcEdit_PurchasePrice.ToolTip = "ORIGINALLY, how much was the paid for the property in total"
            '
            'TextEdit_PurchaseYear
            '
            Me.TextEdit_PurchaseYear.Location = New System.Drawing.Point(128, 111)
            Me.TextEdit_PurchaseYear.Name = "TextEdit_PurchaseYear"
            Me.TextEdit_PurchaseYear.Properties.Appearance.Options.UseTextOptions = True
            Me.TextEdit_PurchaseYear.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.TextEdit_PurchaseYear.Properties.DisplayFormat.FormatString = "0000"
            Me.TextEdit_PurchaseYear.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.TextEdit_PurchaseYear.Properties.EditFormat.FormatString = "0000"
            Me.TextEdit_PurchaseYear.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.TextEdit_PurchaseYear.Properties.Mask.BeepOnError = True
            Me.TextEdit_PurchaseYear.Properties.Mask.EditMask = "0000"
            Me.TextEdit_PurchaseYear.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
            Me.TextEdit_PurchaseYear.Properties.Mask.SaveLiteral = False
            Me.TextEdit_PurchaseYear.Properties.MaxLength = 4
            Me.TextEdit_PurchaseYear.Size = New System.Drawing.Size(113, 20)
            Me.TextEdit_PurchaseYear.StyleController = Me.LayoutControl1
            Me.TextEdit_PurchaseYear.TabIndex = 6
            Me.TextEdit_PurchaseYear.ToolTip = "When was the property purchased?"
            '
            'CalcEdit_SalePrice
            '
            Me.CalcEdit_SalePrice.Location = New System.Drawing.Point(356, 63)
            Me.CalcEdit_SalePrice.Name = "CalcEdit_SalePrice"
            Me.CalcEdit_SalePrice.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit_SalePrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_SalePrice.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_SalePrice.Properties.DisplayFormat.FormatString = "{0:c}"
            Me.CalcEdit_SalePrice.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_SalePrice.Properties.EditFormat.FormatString = "{0:c}"
            Me.CalcEdit_SalePrice.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_SalePrice.Properties.Mask.EditMask = "c"
            Me.CalcEdit_SalePrice.Properties.Mask.SaveLiteral = False
            Me.CalcEdit_SalePrice.Properties.Precision = 2
            Me.CalcEdit_SalePrice.Size = New System.Drawing.Size(175, 20)
            Me.CalcEdit_SalePrice.StyleController = Me.LayoutControl1
            Me.CalcEdit_SalePrice.TabIndex = 5
            Me.CalcEdit_SalePrice.ToolTip = "What is the asking price if the property is for sale"
            '
            'lu_PropertyType
            '
            Me.lu_PropertyType.Location = New System.Drawing.Point(128, 63)
            Me.lu_PropertyType.Name = "lu_PropertyType"
            Me.lu_PropertyType.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.lu_PropertyType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lu_PropertyType.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 5, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.lu_PropertyType.Properties.DisplayMember = "description"
            Me.lu_PropertyType.Properties.NullText = ""
            Me.lu_PropertyType.Properties.ShowFooter = False
            Me.lu_PropertyType.Properties.ShowHeader = False
            Me.lu_PropertyType.Properties.SortColumnIndex = 1
            Me.lu_PropertyType.Properties.ValueMember = "Id"
            Me.lu_PropertyType.Size = New System.Drawing.Size(113, 20)
            Me.lu_PropertyType.StyleController = Me.LayoutControl1
            Me.lu_PropertyType.TabIndex = 4
            Me.lu_PropertyType.ToolTip = "What is the type of the property, i.e. Condo, Family Home, Land, etc."
            '
            'CheckEdit_ForSaleIND
            '
            Me.CheckEdit_ForSaleIND.Location = New System.Drawing.Point(17, 63)
            Me.CheckEdit_ForSaleIND.Name = "CheckEdit_ForSaleIND"
            Me.CheckEdit_ForSaleIND.Properties.Caption = "For Sale"
            Me.CheckEdit_ForSaleIND.Size = New System.Drawing.Size(224, 20)
            Me.CheckEdit_ForSaleIND.StyleController = Me.LayoutControl1
            Me.CheckEdit_ForSaleIND.TabIndex = 12
            Me.CheckEdit_ForSaleIND.ToolTip = "Is the property currently for sale?"
            '
            'CalcEdit_txtAnnualInsuranceAmt
            '
            Me.CalcEdit_txtAnnualInsuranceAmt.Location = New System.Drawing.Point(397, 187)
            Me.CalcEdit_txtAnnualInsuranceAmt.Name = "CalcEdit_txtAnnualInsuranceAmt"
            Me.CalcEdit_txtAnnualInsuranceAmt.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit_txtAnnualInsuranceAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_txtAnnualInsuranceAmt.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_txtAnnualInsuranceAmt.Properties.DisplayFormat.FormatString = "c"
            Me.CalcEdit_txtAnnualInsuranceAmt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_txtAnnualInsuranceAmt.Properties.EditFormat.FormatString = "c"
            Me.CalcEdit_txtAnnualInsuranceAmt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_txtAnnualInsuranceAmt.Properties.Mask.BeepOnError = True
            Me.CalcEdit_txtAnnualInsuranceAmt.Properties.Mask.EditMask = "c"
            Me.CalcEdit_txtAnnualInsuranceAmt.Properties.Precision = 2
            Me.CalcEdit_txtAnnualInsuranceAmt.Size = New System.Drawing.Size(122, 20)
            Me.CalcEdit_txtAnnualInsuranceAmt.StyleController = Me.LayoutControl1
            Me.CalcEdit_txtAnnualInsuranceAmt.TabIndex = 38
            '
            'CalcEdit_AnnualTax
            '
            Me.CalcEdit_AnnualTax.Location = New System.Drawing.Point(398, 71)
            Me.CalcEdit_AnnualTax.Name = "CalcEdit_AnnualTax"
            Me.CalcEdit_AnnualTax.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit_AnnualTax.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_AnnualTax.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_AnnualTax.Properties.DisplayFormat.FormatString = "c"
            Me.CalcEdit_AnnualTax.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_AnnualTax.Properties.EditFormat.FormatString = "c"
            Me.CalcEdit_AnnualTax.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_AnnualTax.Properties.Mask.BeepOnError = True
            Me.CalcEdit_AnnualTax.Properties.Mask.EditMask = "c"
            Me.CalcEdit_AnnualTax.Properties.Precision = 2
            Me.CalcEdit_AnnualTax.Size = New System.Drawing.Size(121, 20)
            Me.CalcEdit_AnnualTax.StyleController = Me.LayoutControl1
            Me.CalcEdit_AnnualTax.TabIndex = 39
            '
            'lu_PlanToKeepHome
            '
            Me.lu_PlanToKeepHome.Location = New System.Drawing.Point(356, 63)
            Me.lu_PlanToKeepHome.Name = "lu_PlanToKeepHome"
            Me.lu_PlanToKeepHome.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.lu_PlanToKeepHome.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lu_PlanToKeepHome.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 5, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.lu_PlanToKeepHome.Properties.DisplayMember = "Description"
            Me.lu_PlanToKeepHome.Properties.NullText = "...Please Choose..."
            Me.lu_PlanToKeepHome.Properties.ShowFooter = False
            Me.lu_PlanToKeepHome.Properties.ShowHeader = False
            Me.lu_PlanToKeepHome.Properties.SortColumnIndex = 1
            Me.lu_PlanToKeepHome.Properties.ValueMember = "Id"
            Me.lu_PlanToKeepHome.Size = New System.Drawing.Size(175, 20)
            Me.lu_PlanToKeepHome.StyleController = Me.LayoutControl1
            Me.lu_PlanToKeepHome.TabIndex = 32
            Me.lu_PlanToKeepHome.ToolTip = "Does the client plan to keep the home? If not, how does the client plan to dispos" & _
        "e of the home? Is it a conventional sale/short sale/deed-in-lieu/abandonment, et" & _
        "c."
            '
            'LayoutControlItem_Sync
            '
            Me.LayoutControlItem_Sync.Control = Me.LookUpEdit_Sync
            Me.LayoutControlItem_Sync.CustomizationFormText = "Synchronize"
            Me.LayoutControlItem_Sync.Location = New System.Drawing.Point(0, 173)
            Me.LayoutControlItem_Sync.Name = "LayoutControlItem_Sync"
            Me.LayoutControlItem_Sync.Size = New System.Drawing.Size(472, 48)
            Me.LayoutControlItem_Sync.Text = "Synchronize"
            Me.LayoutControlItem_Sync.TextSize = New System.Drawing.Size(50, 20)
            Me.LayoutControlItem_Sync.TextToControlDistance = 5
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.TabbedControlGroup1})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Padding = New DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2)
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(548, 433)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'TabbedControlGroup1
            '
            Me.TabbedControlGroup1.CustomizationFormText = "TabbedControlGroup1"
            Me.TabbedControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.TabbedControlGroup1.Name = "TabbedControlGroup1"
            Me.TabbedControlGroup1.SelectedTabPage = Me.LayoutControlGroup9
            Me.TabbedControlGroup1.SelectedTabPageIndex = 6
            Me.TabbedControlGroup1.Size = New System.Drawing.Size(542, 427)
            Me.TabbedControlGroup1.TabPages.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup_General, Me.LayoutControlGroup_TaxesIns, Me.LayoutControlGroup2, Me.LayoutControlGroup_Borrower, Me.LayoutControlGroup_CoBorrower, Me.LayoutControlGroup3, Me.LayoutControlGroup9, Me.LayoutControlGroup5})
            Me.TabbedControlGroup1.Text = "TabbedControlGroup1"
            '
            'LayoutControlGroup9
            '
            Me.LayoutControlGroup9.CustomizationFormText = "HPF"
            Me.LayoutControlGroup9.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.EmptySpaceItem6, Me.LayoutControlItem12, Me.LayoutControlItem13, Me.LayoutControlGroup10, Me.LayoutControlItem14, Me.LayoutControlItem15, Me.LayoutControlItem16, Me.LayoutControlItem23, Me.LayoutControlItem24, Me.EmptySpaceItem10})
            Me.LayoutControlGroup9.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup9.Name = "LayoutControlGroup9"
            Me.LayoutControlGroup9.Size = New System.Drawing.Size(518, 381)
            Me.LayoutControlGroup9.Text = "HPF"
            '
            'EmptySpaceItem6
            '
            Me.EmptySpaceItem6.AllowHotTrack = False
            Me.EmptySpaceItem6.CustomizationFormText = "EmptySpaceItem6"
            Me.EmptySpaceItem6.Location = New System.Drawing.Point(0, 160)
            Me.EmptySpaceItem6.MaxSize = New System.Drawing.Size(0, 24)
            Me.EmptySpaceItem6.MinSize = New System.Drawing.Size(104, 24)
            Me.EmptySpaceItem6.Name = "EmptySpaceItem6"
            Me.EmptySpaceItem6.Size = New System.Drawing.Size(518, 24)
            Me.EmptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
            Me.EmptySpaceItem6.Text = "EmptySpaceItem6"
            Me.EmptySpaceItem6.TextSize = New System.Drawing.Size(0, 0)
            '
            'LayoutControlItem12
            '
            Me.LayoutControlItem12.Control = Me.LabelControl_FCID
            Me.LayoutControlItem12.CustomizationFormText = "Foreclosure Case ID"
            Me.LayoutControlItem12.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem12.Name = "LayoutControlItem12"
            Me.LayoutControlItem12.Size = New System.Drawing.Size(518, 17)
            Me.LayoutControlItem12.Text = "Foreclosure Case ID"
            Me.LayoutControlItem12.TextSize = New System.Drawing.Size(108, 13)
            '
            'LayoutControlItem13
            '
            Me.LayoutControlItem13.Control = Me.CheckEdit_HPF_worked_with_other_agency
            Me.LayoutControlItem13.CustomizationFormText = "worked with other agency"
            Me.LayoutControlItem13.Location = New System.Drawing.Point(0, 130)
            Me.LayoutControlItem13.Name = "LayoutControlItem13"
            Me.LayoutControlItem13.Size = New System.Drawing.Size(430, 30)
            Me.LayoutControlItem13.Text = "worked with other agency"
            Me.LayoutControlItem13.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem13.TextToControlDistance = 0
            Me.LayoutControlItem13.TextVisible = False
            '
            'LayoutControlGroup10
            '
            Me.LayoutControlGroup10.CustomizationFormText = "Outcomes"
            Me.LayoutControlGroup10.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem11})
            Me.LayoutControlGroup10.Location = New System.Drawing.Point(0, 184)
            Me.LayoutControlGroup10.Name = "LayoutControlGroup10"
            Me.LayoutControlGroup10.Size = New System.Drawing.Size(518, 197)
            Me.LayoutControlGroup10.Text = "Outcomes"
            '
            'LayoutControlItem11
            '
            Me.LayoutControlItem11.Control = Me.HpfOutcomeListControl1
            Me.LayoutControlItem11.CustomizationFormText = "Outcomes"
            Me.LayoutControlItem11.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem11.Name = "LayoutControlItem11"
            Me.LayoutControlItem11.Size = New System.Drawing.Size(494, 153)
            Me.LayoutControlItem11.Text = "Outcomes"
            Me.LayoutControlItem11.TextLocation = DevExpress.Utils.Locations.Top
            Me.LayoutControlItem11.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem11.TextToControlDistance = 0
            Me.LayoutControlItem11.TextVisible = False
            '
            'LayoutControlItem14
            '
            Me.LayoutControlItem14.Control = Me.lu_HPF_Program
            Me.LayoutControlItem14.CustomizationFormText = "Program"
            Me.LayoutControlItem14.Location = New System.Drawing.Point(0, 41)
            Me.LayoutControlItem14.Name = "LayoutControlItem14"
            Me.LayoutControlItem14.Size = New System.Drawing.Size(518, 24)
            Me.LayoutControlItem14.Text = "Program"
            Me.LayoutControlItem14.TextSize = New System.Drawing.Size(108, 13)
            '
            'LayoutControlItem15
            '
            Me.LayoutControlItem15.Control = Me.lu_HPF_SubProgram
            Me.LayoutControlItem15.CustomizationFormText = "Sub-Program"
            Me.LayoutControlItem15.Location = New System.Drawing.Point(0, 65)
            Me.LayoutControlItem15.Name = "LayoutControlItem15"
            Me.LayoutControlItem15.Size = New System.Drawing.Size(518, 24)
            Me.LayoutControlItem15.Text = "Sub-Program"
            Me.LayoutControlItem15.TextSize = New System.Drawing.Size(108, 13)
            '
            'LayoutControlItem16
            '
            Me.LayoutControlItem16.Control = Me.lu_HPFCounselor
            Me.LayoutControlItem16.CustomizationFormText = "Counselor Name"
            Me.LayoutControlItem16.Location = New System.Drawing.Point(0, 17)
            Me.LayoutControlItem16.Name = "LayoutControlItem16"
            Me.LayoutControlItem16.Size = New System.Drawing.Size(518, 24)
            Me.LayoutControlItem16.Text = "Counselor"
            Me.LayoutControlItem16.TextSize = New System.Drawing.Size(108, 13)
            '
            'LayoutControlItem23
            '
            Me.LayoutControlItem23.Control = Me.LookUpEdit_CallReason
            Me.LayoutControlItem23.CustomizationFormText = "Call Reason"
            Me.LayoutControlItem23.Location = New System.Drawing.Point(0, 89)
            Me.LayoutControlItem23.Name = "LayoutControlItem23"
            Me.LayoutControlItem23.Size = New System.Drawing.Size(518, 24)
            Me.LayoutControlItem23.Text = "Call Reason"
            Me.LayoutControlItem23.TextSize = New System.Drawing.Size(108, 13)
            '
            'LayoutControlItem24
            '
            Me.LayoutControlItem24.Control = Me.SimpleButton_SendEmail
            Me.LayoutControlItem24.CustomizationFormText = "LayoutControlItem24"
            Me.LayoutControlItem24.Location = New System.Drawing.Point(430, 130)
            Me.LayoutControlItem24.Name = "LayoutControlItem24"
            Me.LayoutControlItem24.Size = New System.Drawing.Size(88, 30)
            Me.LayoutControlItem24.Text = "LayoutControlItem24"
            Me.LayoutControlItem24.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem24.TextToControlDistance = 0
            Me.LayoutControlItem24.TextVisible = False
            '
            'LayoutControlGroup_General
            '
            Me.LayoutControlGroup_General.CustomizationFormText = "General"
            Me.LayoutControlGroup_General.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem_OccupancyNum, Me.LayoutControlItem_PurchasePrice, Me.LayoutControlItem_PropertyCD, Me.EmptySpaceItem3, Me.LayoutControlItem2, Me.LayoutControlItem_CurrentMarketValue, Me.LayoutControlItem_PurchaseYear, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlGroup4, Me.LayoutControlItem9, Me.LayoutControlItem6, Me.LayoutControlItem17, Me.LayoutControlItem18})
            Me.LayoutControlGroup_General.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup_General.Name = "LayoutControlGroup_General"
            Me.LayoutControlGroup_General.Padding = New DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2)
            Me.LayoutControlGroup_General.Size = New System.Drawing.Size(518, 381)
            Me.LayoutControlGroup_General.Text = "General"
            '
            'LayoutControlItem_OccupancyNum
            '
            Me.LayoutControlItem_OccupancyNum.Control = Me.SpinEdit_Occupancy
            Me.LayoutControlItem_OccupancyNum.CustomizationFormText = "Occupancy"
            Me.LayoutControlItem_OccupancyNum.Location = New System.Drawing.Point(228, 48)
            Me.LayoutControlItem_OccupancyNum.Name = "LayoutControlItem_OccupancyNum"
            Me.LayoutControlItem_OccupancyNum.Size = New System.Drawing.Size(290, 24)
            Me.LayoutControlItem_OccupancyNum.Text = "Occupancy"
            Me.LayoutControlItem_OccupancyNum.TextSize = New System.Drawing.Size(108, 13)
            '
            'LayoutControlItem_PurchasePrice
            '
            Me.LayoutControlItem_PurchasePrice.Control = Me.CalcEdit_PurchasePrice
            Me.LayoutControlItem_PurchasePrice.CustomizationFormText = "Purchase Price"
            Me.LayoutControlItem_PurchasePrice.Location = New System.Drawing.Point(228, 72)
            Me.LayoutControlItem_PurchasePrice.Name = "LayoutControlItem_PurchasePrice"
            Me.LayoutControlItem_PurchasePrice.Size = New System.Drawing.Size(290, 24)
            Me.LayoutControlItem_PurchasePrice.Text = "Purchase Price"
            Me.LayoutControlItem_PurchasePrice.TextSize = New System.Drawing.Size(108, 13)
            '
            'LayoutControlItem_PropertyCD
            '
            Me.LayoutControlItem_PropertyCD.Control = Me.lu_PropertyType
            Me.LayoutControlItem_PropertyCD.CustomizationFormText = "Property Information"
            Me.LayoutControlItem_PropertyCD.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem_PropertyCD.Name = "LayoutControlItem_PropertyCD"
            Me.LayoutControlItem_PropertyCD.Size = New System.Drawing.Size(228, 24)
            Me.LayoutControlItem_PropertyCD.Text = "Property Type"
            Me.LayoutControlItem_PropertyCD.TextSize = New System.Drawing.Size(108, 13)
            '
            'EmptySpaceItem3
            '
            Me.EmptySpaceItem3.AllowHotTrack = False
            Me.EmptySpaceItem3.CustomizationFormText = "EmptySpaceItem3"
            Me.EmptySpaceItem3.Location = New System.Drawing.Point(0, 144)
            Me.EmptySpaceItem3.MaxSize = New System.Drawing.Size(0, 10)
            Me.EmptySpaceItem3.MinSize = New System.Drawing.Size(10, 10)
            Me.EmptySpaceItem3.Name = "EmptySpaceItem3"
            Me.EmptySpaceItem3.Size = New System.Drawing.Size(518, 10)
            Me.EmptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
            Me.EmptySpaceItem3.Text = "EmptySpaceItem3"
            Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.CalcEdit_LandValue
            Me.LayoutControlItem2.CustomizationFormText = "Land Value"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 96)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(228, 24)
            Me.LayoutControlItem2.Text = "Land Value"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(108, 13)
            '
            'LayoutControlItem_CurrentMarketValue
            '
            Me.LayoutControlItem_CurrentMarketValue.Control = Me.CalcEdit_ImprovementsValue
            Me.LayoutControlItem_CurrentMarketValue.CustomizationFormText = "Current Market Value for Improvements"
            Me.LayoutControlItem_CurrentMarketValue.Location = New System.Drawing.Point(228, 96)
            Me.LayoutControlItem_CurrentMarketValue.Name = "LayoutControlItem_CurrentMarketValue"
            Me.LayoutControlItem_CurrentMarketValue.Size = New System.Drawing.Size(290, 24)
            Me.LayoutControlItem_CurrentMarketValue.Text = "Improvements Value"
            Me.LayoutControlItem_CurrentMarketValue.TextSize = New System.Drawing.Size(108, 13)
            '
            'LayoutControlItem_PurchaseYear
            '
            Me.LayoutControlItem_PurchaseYear.Control = Me.TextEdit_PurchaseYear
            Me.LayoutControlItem_PurchaseYear.CustomizationFormText = "Purchase Year"
            Me.LayoutControlItem_PurchaseYear.Location = New System.Drawing.Point(0, 72)
            Me.LayoutControlItem_PurchaseYear.Name = "LayoutControlItem_PurchaseYear"
            Me.LayoutControlItem_PurchaseYear.Size = New System.Drawing.Size(228, 24)
            Me.LayoutControlItem_PurchaseYear.Text = "Purchase Year"
            Me.LayoutControlItem_PurchaseYear.TextSize = New System.Drawing.Size(108, 13)
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.lu_Residency
            Me.LayoutControlItem3.CustomizationFormText = "LayoutControlItem3"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 48)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(228, 24)
            Me.LayoutControlItem3.Text = "Residency"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(108, 13)
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.TextEdit_TrustName
            Me.LayoutControlItem4.CustomizationFormText = "Trust Name"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(228, 24)
            Me.LayoutControlItem4.Text = "Trust Name"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(108, 13)
            '
            'LayoutControlGroup4
            '
            Me.LayoutControlGroup4.CustomizationFormText = "Property Address"
            Me.LayoutControlGroup4.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1})
            Me.LayoutControlGroup4.Location = New System.Drawing.Point(0, 154)
            Me.LayoutControlGroup4.Name = "LayoutControlGroup4"
            Me.LayoutControlGroup4.Size = New System.Drawing.Size(518, 227)
            Me.LayoutControlGroup4.Text = "Property Address"
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.PropertyAddressControl1
            Me.LayoutControlItem1.CustomizationFormText = "LayoutControlItem1"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(494, 183)
            Me.LayoutControlItem1.Text = "LayoutControlItem1"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem1.TextToControlDistance = 0
            Me.LayoutControlItem1.TextVisible = False
            '
            'LayoutControlItem9
            '
            Me.LayoutControlItem9.Control = Me.CheckEdit_UseInReports
            Me.LayoutControlItem9.CustomizationFormText = "LayoutControlItem9"
            Me.LayoutControlItem9.Location = New System.Drawing.Point(228, 0)
            Me.LayoutControlItem9.Name = "LayoutControlItem9"
            Me.LayoutControlItem9.Size = New System.Drawing.Size(290, 24)
            Me.LayoutControlItem9.Text = "LayoutControlItem9"
            Me.LayoutControlItem9.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem9.TextToControlDistance = 0
            Me.LayoutControlItem9.TextVisible = False
            '
            'LayoutControlItem6
            '
            Me.LayoutControlItem6.Control = Me.lu_PlanToKeepHome
            Me.LayoutControlItem6.CustomizationFormText = "Keep/Sell the property"
            Me.LayoutControlItem6.Location = New System.Drawing.Point(228, 24)
            Me.LayoutControlItem6.Name = "LayoutControlItem6"
            Me.LayoutControlItem6.OptionsToolTip.ToolTip = "Choose ""keep"" or ""sell"" as appropriate. The others ""abandon"" means to walk-away f" & _
        "rom the house."
            Me.LayoutControlItem6.OptionsToolTip.ToolTipTitle = "Does the client plan to keep or sell the home?"
            Me.LayoutControlItem6.Size = New System.Drawing.Size(290, 24)
            Me.LayoutControlItem6.Text = "Keep/Sell the property"
            Me.LayoutControlItem6.TextSize = New System.Drawing.Size(108, 13)
            '
            'LayoutControlItem17
            '
            Me.LayoutControlItem17.Control = Me.lu_AppraisalType
            Me.LayoutControlItem17.CustomizationFormText = "Appraisal Type"
            Me.LayoutControlItem17.Location = New System.Drawing.Point(0, 120)
            Me.LayoutControlItem17.Name = "LayoutControlItem17"
            Me.LayoutControlItem17.Size = New System.Drawing.Size(228, 24)
            Me.LayoutControlItem17.Text = "Appraisal Type"
            Me.LayoutControlItem17.TextSize = New System.Drawing.Size(108, 13)
            '
            'LayoutControlItem18
            '
            Me.LayoutControlItem18.Control = Me.dt_AppraisalDate
            Me.LayoutControlItem18.CustomizationFormText = "Appraisal Date"
            Me.LayoutControlItem18.Location = New System.Drawing.Point(228, 120)
            Me.LayoutControlItem18.Name = "LayoutControlItem18"
            Me.LayoutControlItem18.Size = New System.Drawing.Size(290, 24)
            Me.LayoutControlItem18.Text = "Appraisal Date"
            Me.LayoutControlItem18.TextSize = New System.Drawing.Size(108, 13)
            '
            'LayoutControlGroup_TaxesIns
            '
            Me.LayoutControlGroup_TaxesIns.CustomizationFormText = "LayoutControlGroup_TaxesIns"
            Me.LayoutControlGroup_TaxesIns.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup6, Me.LayoutControlGroup7, Me.LayoutControlGroup8, Me.EmptySpaceItem1, Me.LayoutControlGroup11})
            Me.LayoutControlGroup_TaxesIns.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup_TaxesIns.Name = "LayoutControlGroup_TaxesIns"
            Me.LayoutControlGroup_TaxesIns.Size = New System.Drawing.Size(518, 381)
            Me.LayoutControlGroup_TaxesIns.Text = "Taxes && Ins"
            '
            'LayoutControlGroup6
            '
            Me.LayoutControlGroup6.CustomizationFormText = "Taxes"
            Me.LayoutControlGroup6.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem_PropTaxes, Me.LayoutControlItem_TaxDelinqState, Me.LayoutControlItem10_APT, Me.LayoutControlItem19, Me.LayoutControlItem6_TDD, Me.EmptySpaceItem7})
            Me.LayoutControlGroup6.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup6.Name = "LayoutControlGroup6"
            Me.LayoutControlGroup6.Size = New System.Drawing.Size(518, 116)
            Me.LayoutControlGroup6.Text = "Taxes"
            '
            'LayoutControlItem_PropTaxes
            '
            Me.LayoutControlItem_PropTaxes.Control = Me.lu_PropertyTaxes
            Me.LayoutControlItem_PropTaxes.CustomizationFormText = "Prop. Taxes"
            Me.LayoutControlItem_PropTaxes.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem_PropTaxes.Name = "LayoutControlItem_PropTaxes"
            Me.LayoutControlItem_PropTaxes.Size = New System.Drawing.Size(246, 24)
            Me.LayoutControlItem_PropTaxes.Text = "Property"
            Me.LayoutControlItem_PropTaxes.TextSize = New System.Drawing.Size(108, 13)
            '
            'LayoutControlItem_TaxDelinqState
            '
            Me.LayoutControlItem_TaxDelinqState.Control = Me.lu_TaxDelinqState
            Me.LayoutControlItem_TaxDelinqState.CustomizationFormText = "Tax Delinquent State"
            Me.LayoutControlItem_TaxDelinqState.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem_TaxDelinqState.Name = "LayoutControlItem_TaxDelinqState"
            Me.LayoutControlItem_TaxDelinqState.Size = New System.Drawing.Size(246, 24)
            Me.LayoutControlItem_TaxDelinqState.Text = "Delinq State"
            Me.LayoutControlItem_TaxDelinqState.TextSize = New System.Drawing.Size(108, 13)
            '
            'LayoutControlItem10_APT
            '
            Me.LayoutControlItem10_APT.Control = Me.CalcEdit_AnnualTax
            Me.LayoutControlItem10_APT.CustomizationFormText = "Annual Prop. Tax "
            Me.LayoutControlItem10_APT.Location = New System.Drawing.Point(258, 0)
            Me.LayoutControlItem10_APT.Name = "LayoutControlItem10_APT"
            Me.LayoutControlItem10_APT.Size = New System.Drawing.Size(236, 24)
            Me.LayoutControlItem10_APT.Text = "Annual Property"
            Me.LayoutControlItem10_APT.TextSize = New System.Drawing.Size(108, 13)
            '
            'LayoutControlItem19
            '
            Me.LayoutControlItem19.Control = Me.CalcEdit_TaxDelinqAmount
            Me.LayoutControlItem19.CustomizationFormText = "Delinq Amount"
            Me.LayoutControlItem19.Location = New System.Drawing.Point(258, 24)
            Me.LayoutControlItem19.Name = "LayoutControlItem19"
            Me.LayoutControlItem19.Size = New System.Drawing.Size(236, 48)
            Me.LayoutControlItem19.Text = "Delinq Amount"
            Me.LayoutControlItem19.TextSize = New System.Drawing.Size(108, 13)
            '
            'LayoutControlItem6_TDD
            '
            Me.LayoutControlItem6_TDD.Control = Me.dt_Tax_Duedate
            Me.LayoutControlItem6_TDD.CustomizationFormText = "Tax Due Date"
            Me.LayoutControlItem6_TDD.Location = New System.Drawing.Point(0, 48)
            Me.LayoutControlItem6_TDD.Name = "LayoutControlItem6_TDD"
            Me.LayoutControlItem6_TDD.Size = New System.Drawing.Size(246, 24)
            Me.LayoutControlItem6_TDD.Text = "Due Date"
            Me.LayoutControlItem6_TDD.TextSize = New System.Drawing.Size(108, 13)
            '
            'EmptySpaceItem7
            '
            Me.EmptySpaceItem7.AllowHotTrack = False
            Me.EmptySpaceItem7.CustomizationFormText = "EmptySpaceItem7"
            Me.EmptySpaceItem7.Location = New System.Drawing.Point(246, 0)
            Me.EmptySpaceItem7.Name = "EmptySpaceItem7"
            Me.EmptySpaceItem7.Size = New System.Drawing.Size(12, 72)
            Me.EmptySpaceItem7.Text = "EmptySpaceItem7"
            Me.EmptySpaceItem7.TextSize = New System.Drawing.Size(0, 0)
            '
            'LayoutControlGroup7
            '
            Me.LayoutControlGroup7.CustomizationFormText = "Insurance"
            Me.LayoutControlGroup7.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem10, Me.LayoutControlItem7_AIA, Me.LayoutControlItem_InsDelinqState, Me.EmptySpaceItem8, Me.LayoutControlItem11_IDD, Me.LayoutControlItem20})
            Me.LayoutControlGroup7.Location = New System.Drawing.Point(0, 116)
            Me.LayoutControlGroup7.Name = "LayoutControlGroup7"
            Me.LayoutControlGroup7.Size = New System.Drawing.Size(518, 116)
            Me.LayoutControlGroup7.Text = "Insurance"
            '
            'LayoutControlItem10
            '
            Me.LayoutControlItem10.Control = Me.lu_HomeOwners
            Me.LayoutControlItem10.CustomizationFormText = "Homeowners Insurance"
            Me.LayoutControlItem10.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem10.Name = "LayoutControlItem10"
            Me.LayoutControlItem10.Size = New System.Drawing.Size(245, 24)
            Me.LayoutControlItem10.Text = "Homeowners"
            Me.LayoutControlItem10.TextSize = New System.Drawing.Size(108, 13)
            '
            'LayoutControlItem7_AIA
            '
            Me.LayoutControlItem7_AIA.Control = Me.CalcEdit_txtAnnualInsuranceAmt
            Me.LayoutControlItem7_AIA.CustomizationFormText = "Insurance Annual Amt. "
            Me.LayoutControlItem7_AIA.Location = New System.Drawing.Point(257, 0)
            Me.LayoutControlItem7_AIA.Name = "LayoutControlItem7_AIA"
            Me.LayoutControlItem7_AIA.Size = New System.Drawing.Size(237, 24)
            Me.LayoutControlItem7_AIA.Text = "Annual Amount"
            Me.LayoutControlItem7_AIA.TextSize = New System.Drawing.Size(108, 13)
            '
            'LayoutControlItem_InsDelinqState
            '
            Me.LayoutControlItem_InsDelinqState.Control = Me.lu_InsuranceDelinqState
            Me.LayoutControlItem_InsDelinqState.CustomizationFormText = "Insurance Delinquent State"
            Me.LayoutControlItem_InsDelinqState.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem_InsDelinqState.Name = "LayoutControlItem_InsDelinqState"
            Me.LayoutControlItem_InsDelinqState.Size = New System.Drawing.Size(245, 24)
            Me.LayoutControlItem_InsDelinqState.Text = "Delinq. State"
            Me.LayoutControlItem_InsDelinqState.TextSize = New System.Drawing.Size(108, 13)
            '
            'EmptySpaceItem8
            '
            Me.EmptySpaceItem8.AllowHotTrack = False
            Me.EmptySpaceItem8.CustomizationFormText = "EmptySpaceItem8"
            Me.EmptySpaceItem8.Location = New System.Drawing.Point(245, 0)
            Me.EmptySpaceItem8.Name = "EmptySpaceItem8"
            Me.EmptySpaceItem8.Size = New System.Drawing.Size(12, 72)
            Me.EmptySpaceItem8.Text = "EmptySpaceItem8"
            Me.EmptySpaceItem8.TextSize = New System.Drawing.Size(0, 0)
            '
            'LayoutControlItem11_IDD
            '
            Me.LayoutControlItem11_IDD.Control = Me.dt_InsuranceDueDate
            Me.LayoutControlItem11_IDD.CustomizationFormText = "Insurance Due Date "
            Me.LayoutControlItem11_IDD.Location = New System.Drawing.Point(0, 48)
            Me.LayoutControlItem11_IDD.Name = "LayoutControlItem11_IDD"
            Me.LayoutControlItem11_IDD.Size = New System.Drawing.Size(245, 24)
            Me.LayoutControlItem11_IDD.Text = "Due Date "
            Me.LayoutControlItem11_IDD.TextSize = New System.Drawing.Size(108, 13)
            '
            'LayoutControlItem20
            '
            Me.LayoutControlItem20.Control = Me.CalcEdit_InsDeliqAmount
            Me.LayoutControlItem20.CustomizationFormText = "Delinq Amount"
            Me.LayoutControlItem20.Location = New System.Drawing.Point(257, 24)
            Me.LayoutControlItem20.Name = "LayoutControlItem20"
            Me.LayoutControlItem20.Size = New System.Drawing.Size(237, 48)
            Me.LayoutControlItem20.Text = "Delinq Amount"
            Me.LayoutControlItem20.TextSize = New System.Drawing.Size(108, 13)
            '
            'LayoutControlGroup8
            '
            Me.LayoutControlGroup8.CustomizationFormText = "Mortgage Expenses"
            Me.LayoutControlGroup8.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem_PmiFhaInsurance, Me.EmptySpaceItem7_TI})
            Me.LayoutControlGroup8.Location = New System.Drawing.Point(0, 232)
            Me.LayoutControlGroup8.Name = "LayoutControlGroup8"
            Me.LayoutControlGroup8.Size = New System.Drawing.Size(518, 68)
            Me.LayoutControlGroup8.Text = "Mortgage Expenses"
            '
            'LayoutControlItem_PmiFhaInsurance
            '
            Me.LayoutControlItem_PmiFhaInsurance.Control = Me.lu_PmiInsurance
            Me.LayoutControlItem_PmiFhaInsurance.CustomizationFormText = "LayoutControlItem6"
            Me.LayoutControlItem_PmiFhaInsurance.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem_PmiFhaInsurance.Name = "LayoutControlItem_PmiFhaInsurance"
            Me.LayoutControlItem_PmiFhaInsurance.Size = New System.Drawing.Size(246, 24)
            Me.LayoutControlItem_PmiFhaInsurance.Text = "PMI/FHA Insurance"
            Me.LayoutControlItem_PmiFhaInsurance.TextSize = New System.Drawing.Size(108, 13)
            '
            'EmptySpaceItem7_TI
            '
            Me.EmptySpaceItem7_TI.AllowHotTrack = False
            Me.EmptySpaceItem7_TI.CustomizationFormText = "EmptySpaceItem7"
            Me.EmptySpaceItem7_TI.Location = New System.Drawing.Point(246, 0)
            Me.EmptySpaceItem7_TI.Name = "EmptySpaceItem7_TI"
            Me.EmptySpaceItem7_TI.Size = New System.Drawing.Size(248, 24)
            Me.EmptySpaceItem7_TI.Text = "EmptySpaceItem7_TI"
            Me.EmptySpaceItem7_TI.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem1
            '
            Me.EmptySpaceItem1.AllowHotTrack = False
            Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 368)
            Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Size = New System.Drawing.Size(518, 13)
            Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
            Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
            '
            'LayoutControlGroup11
            '
            Me.LayoutControlGroup11.CustomizationFormText = "HomeOwner's Association (HOA)"
            Me.LayoutControlGroup11.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem22, Me.LayoutControlItem21, Me.EmptySpaceItem9})
            Me.LayoutControlGroup11.Location = New System.Drawing.Point(0, 300)
            Me.LayoutControlGroup11.Name = "LayoutControlGroup11"
            Me.LayoutControlGroup11.Size = New System.Drawing.Size(518, 68)
            Me.LayoutControlGroup11.Text = "HomeOwner's Association (HOA)"
            '
            'LayoutControlItem22
            '
            Me.LayoutControlItem22.Control = Me.CalcEdit_HOADelinqAmount
            Me.LayoutControlItem22.CustomizationFormText = "Delinq Amount"
            Me.LayoutControlItem22.Location = New System.Drawing.Point(258, 0)
            Me.LayoutControlItem22.Name = "LayoutControlItem22"
            Me.LayoutControlItem22.Size = New System.Drawing.Size(236, 24)
            Me.LayoutControlItem22.Text = "Delinq Amount"
            Me.LayoutControlItem22.TextSize = New System.Drawing.Size(108, 13)
            '
            'LayoutControlItem21
            '
            Me.LayoutControlItem21.Control = Me.lu_HOADelinqState
            Me.LayoutControlItem21.CustomizationFormText = "Delinq State"
            Me.LayoutControlItem21.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem21.Name = "LayoutControlItem21"
            Me.LayoutControlItem21.Size = New System.Drawing.Size(246, 24)
            Me.LayoutControlItem21.Text = "Delinq State"
            Me.LayoutControlItem21.TextSize = New System.Drawing.Size(108, 13)
            '
            'EmptySpaceItem9
            '
            Me.EmptySpaceItem9.AllowHotTrack = False
            Me.EmptySpaceItem9.CustomizationFormText = "EmptySpaceItem9"
            Me.EmptySpaceItem9.Location = New System.Drawing.Point(246, 0)
            Me.EmptySpaceItem9.Name = "EmptySpaceItem9"
            Me.EmptySpaceItem9.Size = New System.Drawing.Size(12, 24)
            Me.EmptySpaceItem9.Text = "EmptySpaceItem9"
            Me.EmptySpaceItem9.TextSize = New System.Drawing.Size(0, 0)
            '
            'LayoutControlGroup2
            '
            Me.LayoutControlGroup2.CustomizationFormText = "Loan Information Group"
            Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem_LoanListControl1})
            Me.LayoutControlGroup2.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
            Me.LayoutControlGroup2.Size = New System.Drawing.Size(518, 381)
            Me.LayoutControlGroup2.Text = "Loans"
            '
            'LayoutControlItem_LoanListControl1
            '
            Me.LayoutControlItem_LoanListControl1.Control = Me.LoanListControl1
            Me.LayoutControlItem_LoanListControl1.CustomizationFormText = "Loan Information"
            Me.LayoutControlItem_LoanListControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem_LoanListControl1.Name = "LayoutControlItem_LoanListControl1"
            Me.LayoutControlItem_LoanListControl1.Size = New System.Drawing.Size(518, 381)
            Me.LayoutControlItem_LoanListControl1.Text = "Loan Information"
            Me.LayoutControlItem_LoanListControl1.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem_LoanListControl1.TextToControlDistance = 0
            Me.LayoutControlItem_LoanListControl1.TextVisible = False
            '
            'LayoutControlGroup_Borrower
            '
            Me.LayoutControlGroup_Borrower.CustomizationFormText = "Borrower"
            Me.LayoutControlGroup_Borrower.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem_BorrowerID})
            Me.LayoutControlGroup_Borrower.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup_Borrower.Name = "LayoutControlGroup_Borrower"
            Me.LayoutControlGroup_Borrower.Size = New System.Drawing.Size(518, 381)
            Me.LayoutControlGroup_Borrower.Text = "Borrower"
            '
            'LayoutControlItem_BorrowerID
            '
            Me.LayoutControlItem_BorrowerID.Control = Me.BorrowerControl_OwnerID
            Me.LayoutControlItem_BorrowerID.CustomizationFormText = "Borrower"
            Me.LayoutControlItem_BorrowerID.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem_BorrowerID.Name = "LayoutControlItem_BorrowerID"
            Me.LayoutControlItem_BorrowerID.Size = New System.Drawing.Size(518, 381)
            Me.LayoutControlItem_BorrowerID.Text = "Borrower"
            Me.LayoutControlItem_BorrowerID.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem_BorrowerID.TextToControlDistance = 0
            Me.LayoutControlItem_BorrowerID.TextVisible = False
            '
            'LayoutControlGroup_CoBorrower
            '
            Me.LayoutControlGroup_CoBorrower.CustomizationFormText = "Co Borrower"
            Me.LayoutControlGroup_CoBorrower.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem_CoBorrower})
            Me.LayoutControlGroup_CoBorrower.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup_CoBorrower.Name = "LayoutControlGroup_CoBorrower"
            Me.LayoutControlGroup_CoBorrower.Padding = New DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2)
            Me.LayoutControlGroup_CoBorrower.Size = New System.Drawing.Size(518, 381)
            Me.LayoutControlGroup_CoBorrower.Text = "Co Borrower"
            '
            'LayoutControlItem_CoBorrower
            '
            Me.LayoutControlItem_CoBorrower.Control = Me.BorrowerControl_CoOwnerID
            Me.LayoutControlItem_CoBorrower.CustomizationFormText = "Co Borrower"
            Me.LayoutControlItem_CoBorrower.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem_CoBorrower.Name = "LayoutControlItem_CoBorrower"
            Me.LayoutControlItem_CoBorrower.Size = New System.Drawing.Size(518, 381)
            Me.LayoutControlItem_CoBorrower.Text = "Co Borrower"
            Me.LayoutControlItem_CoBorrower.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem_CoBorrower.TextToControlDistance = 0
            Me.LayoutControlItem_CoBorrower.TextVisible = False
            '
            'LayoutControlGroup3
            '
            Me.LayoutControlGroup3.CustomizationFormText = "For Sale Info"
            Me.LayoutControlGroup3.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem_RealityCompany, Me.LayoutControlItem_SalePrice, Me.EmptySpaceItem2, Me.LayoutControlItem_ForSaleIND, Me.LayoutControlItem_FcNoticeReceivedIND, Me.LayoutControlItem8, Me.LayoutControlItem5, Me.EmptySpaceItem4, Me.EmptySpaceItem5})
            Me.LayoutControlGroup3.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup3.Name = "LayoutControlGroup3"
            Me.LayoutControlGroup3.Size = New System.Drawing.Size(518, 381)
            Me.LayoutControlGroup3.Text = "For Sale Info"
            '
            'LayoutControlItem_RealityCompany
            '
            Me.LayoutControlItem_RealityCompany.Control = Me.TextEdit_RealityCompany
            Me.LayoutControlItem_RealityCompany.CustomizationFormText = "Realty Company"
            Me.LayoutControlItem_RealityCompany.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem_RealityCompany.Name = "LayoutControlItem_RealityCompany"
            Me.LayoutControlItem_RealityCompany.Size = New System.Drawing.Size(518, 24)
            Me.LayoutControlItem_RealityCompany.Text = "Realty Company"
            Me.LayoutControlItem_RealityCompany.TextSize = New System.Drawing.Size(108, 13)
            '
            'LayoutControlItem_SalePrice
            '
            Me.LayoutControlItem_SalePrice.Control = Me.CalcEdit_SalePrice
            Me.LayoutControlItem_SalePrice.CustomizationFormText = "Sale Price"
            Me.LayoutControlItem_SalePrice.Location = New System.Drawing.Point(228, 24)
            Me.LayoutControlItem_SalePrice.Name = "LayoutControlItem_SalePrice"
            Me.LayoutControlItem_SalePrice.Size = New System.Drawing.Size(290, 24)
            Me.LayoutControlItem_SalePrice.Text = "Sale Price"
            Me.LayoutControlItem_SalePrice.TextSize = New System.Drawing.Size(108, 13)
            '
            'EmptySpaceItem2
            '
            Me.EmptySpaceItem2.AllowHotTrack = False
            Me.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Location = New System.Drawing.Point(0, 123)
            Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Size = New System.Drawing.Size(518, 258)
            Me.EmptySpaceItem2.Text = "EmptySpaceItem2"
            Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
            '
            'LayoutControlItem_ForSaleIND
            '
            Me.LayoutControlItem_ForSaleIND.Control = Me.CheckEdit_ForSaleIND
            Me.LayoutControlItem_ForSaleIND.CustomizationFormText = "For Sale"
            Me.LayoutControlItem_ForSaleIND.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem_ForSaleIND.Name = "LayoutControlItem_ForSaleIND"
            Me.LayoutControlItem_ForSaleIND.Size = New System.Drawing.Size(228, 24)
            Me.LayoutControlItem_ForSaleIND.Text = "For Sale"
            Me.LayoutControlItem_ForSaleIND.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem_ForSaleIND.TextToControlDistance = 0
            Me.LayoutControlItem_ForSaleIND.TextVisible = False
            '
            'LayoutControlItem_FcNoticeReceivedIND
            '
            Me.LayoutControlItem_FcNoticeReceivedIND.Control = Me.CheckEdit_FcNoticeReceivedIND
            Me.LayoutControlItem_FcNoticeReceivedIND.CustomizationFormText = "Foreclosure Notice Received"
            Me.LayoutControlItem_FcNoticeReceivedIND.Location = New System.Drawing.Point(0, 99)
            Me.LayoutControlItem_FcNoticeReceivedIND.Name = "LayoutControlItem_FcNoticeReceivedIND"
            Me.LayoutControlItem_FcNoticeReceivedIND.Size = New System.Drawing.Size(228, 24)
            Me.LayoutControlItem_FcNoticeReceivedIND.Text = "Foreclosure Notice Received"
            Me.LayoutControlItem_FcNoticeReceivedIND.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem_FcNoticeReceivedIND.TextToControlDistance = 0
            Me.LayoutControlItem_FcNoticeReceivedIND.TextVisible = False
            '
            'LayoutControlItem8
            '
            Me.LayoutControlItem8.Control = Me.dt_FcSaleDT
            Me.LayoutControlItem8.CustomizationFormText = "Foreclosure Sale Date"
            Me.LayoutControlItem8.Location = New System.Drawing.Point(228, 99)
            Me.LayoutControlItem8.Name = "LayoutControlItem8"
            Me.LayoutControlItem8.Size = New System.Drawing.Size(290, 24)
            Me.LayoutControlItem8.Text = "Foreclosure  Sale Date"
            Me.LayoutControlItem8.TextSize = New System.Drawing.Size(108, 13)
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Control = Me.dt_sales_contract_date
            Me.LayoutControlItem5.CustomizationFormText = "Sale Contract"
            Me.LayoutControlItem5.Location = New System.Drawing.Point(228, 48)
            Me.LayoutControlItem5.Name = "LayoutControlItem5"
            Me.LayoutControlItem5.Size = New System.Drawing.Size(290, 24)
            Me.LayoutControlItem5.Text = "Sale Contract"
            Me.LayoutControlItem5.TextSize = New System.Drawing.Size(108, 13)
            '
            'EmptySpaceItem4
            '
            Me.EmptySpaceItem4.AllowHotTrack = False
            Me.EmptySpaceItem4.CustomizationFormText = "EmptySpaceItem4"
            Me.EmptySpaceItem4.Location = New System.Drawing.Point(0, 48)
            Me.EmptySpaceItem4.Name = "EmptySpaceItem4"
            Me.EmptySpaceItem4.Size = New System.Drawing.Size(228, 24)
            Me.EmptySpaceItem4.Text = "EmptySpaceItem4"
            Me.EmptySpaceItem4.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem5
            '
            Me.EmptySpaceItem5.AllowHotTrack = False
            Me.EmptySpaceItem5.CustomizationFormText = "EmptySpaceItem5"
            Me.EmptySpaceItem5.Location = New System.Drawing.Point(0, 72)
            Me.EmptySpaceItem5.MaxSize = New System.Drawing.Size(0, 27)
            Me.EmptySpaceItem5.MinSize = New System.Drawing.Size(10, 27)
            Me.EmptySpaceItem5.Name = "EmptySpaceItem5"
            Me.EmptySpaceItem5.Size = New System.Drawing.Size(518, 27)
            Me.EmptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
            Me.EmptySpaceItem5.Text = "EmptySpaceItem5"
            Me.EmptySpaceItem5.TextSize = New System.Drawing.Size(0, 0)
            '
            'LayoutControlGroup5
            '
            Me.LayoutControlGroup5.CustomizationFormText = "Notes"
            Me.LayoutControlGroup5.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem7})
            Me.LayoutControlGroup5.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup5.Name = "LayoutControlGroup5"
            Me.LayoutControlGroup5.Size = New System.Drawing.Size(518, 381)
            Me.LayoutControlGroup5.Text = "Notes"
            '
            'LayoutControlItem7
            '
            Me.LayoutControlItem7.Control = Me.PropertyNoteListControl1
            Me.LayoutControlItem7.CustomizationFormText = "LayoutControlItem7"
            Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem7.Name = "LayoutControlItem7"
            Me.LayoutControlItem7.Size = New System.Drawing.Size(518, 381)
            Me.LayoutControlItem7.Text = "LayoutControlItem7"
            Me.LayoutControlItem7.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem7.TextToControlDistance = 0
            Me.LayoutControlItem7.TextVisible = False
            '
            'EmptySpaceItem10
            '
            Me.EmptySpaceItem10.AllowHotTrack = False
            Me.EmptySpaceItem10.CustomizationFormText = "EmptySpaceItem10"
            Me.EmptySpaceItem10.Location = New System.Drawing.Point(0, 113)
            Me.EmptySpaceItem10.MaxSize = New System.Drawing.Size(0, 17)
            Me.EmptySpaceItem10.MinSize = New System.Drawing.Size(10, 17)
            Me.EmptySpaceItem10.Name = "EmptySpaceItem10"
            Me.EmptySpaceItem10.Size = New System.Drawing.Size(518, 17)
            Me.EmptySpaceItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
            Me.EmptySpaceItem10.Text = "EmptySpaceItem10"
            Me.EmptySpaceItem10.TextSize = New System.Drawing.Size(0, 0)
            '
            'PropertyControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.LayoutControl1)
            Me.Name = "PropertyControl"
            Me.Size = New System.Drawing.Size(548, 433)
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.LookUpEdit_CallReason.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_Sync.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_HOADelinqAmount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lu_HOADelinqState.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_InsDeliqAmount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_TaxDelinqAmount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_AppraisalDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_AppraisalDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lu_AppraisalType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lu_HPFCounselor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lu_HPF_SubProgram.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lu_HPF_Program.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_HPF_worked_with_other_agency.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.HpfOutcomeListControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lu_HomeOwners.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PropertyNoteListControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_InsuranceDueDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_InsuranceDueDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_Tax_Duedate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_Tax_Duedate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lu_InsuranceDelinqState.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lu_PmiInsurance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lu_TaxDelinqState.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lu_PropertyTaxes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_sales_contract_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_sales_contract_date.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PropertyAddressControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_FcSaleDT.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_FcSaleDT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_TrustName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lu_Residency.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_LandValue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_UseInReports.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LoanListControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_FcNoticeReceivedIND.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.BorrowerControl_CoOwnerID, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.BorrowerControl_OwnerID, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.SpinEdit_Occupancy.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_RealityCompany.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_ImprovementsValue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_PurchasePrice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_PurchaseYear.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_SalePrice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lu_PropertyType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_ForSaleIND.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_txtAnnualInsuranceAmt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_AnnualTax.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lu_PlanToKeepHome.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_Sync, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TabbedControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup9, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup10, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup_General, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_OccupancyNum, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_PurchasePrice, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_PropertyCD, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_CurrentMarketValue, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_PurchaseYear, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup_TaxesIns, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_PropTaxes, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_TaxDelinqState, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem10_APT, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem6_TDD, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem7, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup7, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem7_AIA, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_InsDelinqState, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem8, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem11_IDD, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup8, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_PmiFhaInsurance, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem7_TI, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup11, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem9, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_LoanListControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup_Borrower, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_BorrowerID, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup_CoBorrower, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_CoBorrower, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_RealityCompany, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_SalePrice, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_ForSaleIND, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_FcNoticeReceivedIND, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem10, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Protected Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Protected Friend WithEvents CheckEdit_ForSaleIND As DevExpress.XtraEditors.CheckEdit
        Protected Friend WithEvents CalcEdit_ImprovementsValue As DevExpress.XtraEditors.CalcEdit
        Protected Friend WithEvents CalcEdit_PurchasePrice As DevExpress.XtraEditors.CalcEdit
        Protected Friend WithEvents TextEdit_PurchaseYear As DevExpress.XtraEditors.TextEdit
        Protected Friend WithEvents CalcEdit_SalePrice As DevExpress.XtraEditors.CalcEdit
        Protected Friend WithEvents lu_PropertyType As DevExpress.XtraEditors.LookUpEdit
        Protected Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Protected Friend WithEvents SpinEdit_Occupancy As DevExpress.XtraEditors.SpinEdit
        Protected Friend WithEvents TextEdit_RealityCompany As DevExpress.XtraEditors.TextEdit
        Protected Friend WithEvents TabbedControlGroup1 As DevExpress.XtraLayout.TabbedControlGroup
        Protected Friend WithEvents BorrowerControl_OwnerID As BorrowerControl
        Protected Friend WithEvents LayoutControlItem_BorrowerID As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents LayoutControlGroup_Borrower As DevExpress.XtraLayout.LayoutControlGroup
        Protected Friend WithEvents BorrowerControl_CoOwnerID As BorrowerControl
        Protected Friend WithEvents LayoutControlGroup_CoBorrower As DevExpress.XtraLayout.LayoutControlGroup
        Protected Friend WithEvents LayoutControlItem_CoBorrower As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents CheckEdit_FcNoticeReceivedIND As DevExpress.XtraEditors.CheckEdit
        Protected Friend WithEvents LookUpEdit_Sync As DevExpress.XtraEditors.LookUpEdit
        Protected Friend WithEvents LayoutControlGroup_General As DevExpress.XtraLayout.LayoutControlGroup
        Protected Friend WithEvents LayoutControlItem_ForSaleIND As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents LayoutControlItem_SalePrice As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents LayoutControlItem_OccupancyNum As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents LayoutControlItem_PurchaseYear As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents LayoutControlItem_PurchasePrice As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents LayoutControlItem_CurrentMarketValue As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents LayoutControlItem_PropertyCD As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents LayoutControlItem_RealityCompany As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents LayoutControlItem_FcNoticeReceivedIND As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents LayoutControlItem_Sync As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LoanListControl1 As LoanListControl
        Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem_LoanListControl1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlGroup3 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem3 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents CalcEdit_LandValue As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents lu_Residency As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents TextEdit_TrustName As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlGroup4 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents dt_FcSaleDT As DevExpress.XtraEditors.DateEdit
        Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents CheckEdit_UseInReports As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents PropertyAddressControl1 As PropertyAddressControl
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents dt_sales_contract_date As DevExpress.XtraEditors.DateEdit
        Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem4 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem5 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents dt_InsuranceDueDate As DevExpress.XtraEditors.DateEdit
        Friend WithEvents dt_Tax_Duedate As DevExpress.XtraEditors.DateEdit
        Friend WithEvents lu_InsuranceDelinqState As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents lu_PmiInsurance As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents lu_TaxDelinqState As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents lu_PropertyTaxes As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LayoutControlGroup_TaxesIns As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem_PropTaxes As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem_TaxDelinqState As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem_PmiFhaInsurance As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem_InsDelinqState As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem6_TDD As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem7_AIA As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem10_APT As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem11_IDD As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem7_TI As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents CalcEdit_txtAnnualInsuranceAmt As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents CalcEdit_AnnualTax As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents lu_PlanToKeepHome As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LayoutControlGroup5 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents PropertyNoteListControl1 As DebtPlus.UI.Client.controls.PropertyNoteListControl
        Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents lu_HomeOwners As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LayoutControlGroup6 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlGroup7 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlGroup8 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents HpfOutcomeListControl1 As DebtPlus.UI.Client.controls.HPFOutcomeListControl
        Friend WithEvents LayoutControlGroup9 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem6 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents CheckEdit_HPF_worked_with_other_agency As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents LabelControl_FCID As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlGroup10 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents lu_HPF_Program As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents lu_HPF_SubProgram As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents lu_HPFCounselor As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents dt_AppraisalDate As DevExpress.XtraEditors.DateEdit
        Friend WithEvents lu_AppraisalType As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LayoutControlItem17 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents CalcEdit_TaxDelinqAmount As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem7 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem8 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents CalcEdit_InsDeliqAmount As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents CalcEdit_HOADelinqAmount As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents lu_HOADelinqState As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LayoutControlItem21 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem22 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlGroup11 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents EmptySpaceItem9 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents LookUpEdit_CallReason As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LayoutControlItem23 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents SimpleButton_SendEmail As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents LayoutControlItem24 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem10 As DevExpress.XtraLayout.EmptySpaceItem
    End Class
End Namespace