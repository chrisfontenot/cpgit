#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.UI.Client.Service
Imports DebtPlus.UI.Client.forms.Housing
Imports DevExpress.Utils
Imports System.Globalization
Imports System.Text
Imports DebtPlus.LINQ
Imports System.Linq

Namespace controls

    Friend Class PropertyListControl

        ' ID associated with the client housing information
        Private clientHousingRecord As DebtPlus.LINQ.client_housing
        Private colProperties As System.Collections.Generic.List(Of Housing_property)

        ''' <summary>
        ''' Initialize the storage for the control class
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            EnableMenus = True
        End Sub

        ''' <summary>
        ''' Edit the indicated housing_property record
        ''' </summary>
        ''' <param name="obj"></param>
        ''' <remarks></remarks>
        Protected Overrides Sub OnEditItem(obj As Object)

            ' Find the property to be edited.
            Dim propertyRecord As Housing_property = TryCast(obj, Housing_property)
            If propertyRecord Is Nothing Then
                Return
            End If

            ' Do the edit operation. This will update the database if the edit is valid.
            If EditProperty(propertyRecord) Then
                GridControl1.RefreshDataSource()
            End If
        End Sub

        Protected Overrides Sub OnCreateItem()
            ' Since there is just too much to hold on a partial record, create a "real" record
            ' in the database first. Then edit it. If needed, we can do the delete operation
            ' on the record should the user want to cancel. But there are too many things "hung"
            ' off the property record.
            Dim propertyRecord As Housing_property = DebtPlus.LINQ.Factory.Manufacture_Housing_property(clientHousingRecord.Id)

            Using bc As New BusinessContext()
                bc.Housing_properties.InsertOnSubmit(propertyRecord)
                bc.SubmitChanges()
            End Using

            ' Edit the property record now that we have a real item
            If Not EditProperty(propertyRecord) Then
                DeleteProperty(propertyRecord)
                Return
            End If

            ' Ok, it is now time to add the property to the grid control.
            colProperties.Add(propertyRecord)
            GridControl1.RefreshDataSource()
        End Sub

        ''' <summary>
        ''' Delete the indicated property from the list
        ''' </summary>
        ''' <param name="obj"></param>
        ''' <remarks></remarks>
        Protected Overrides Sub OnDeleteItem(obj As Object)

            ' Find the property to be edited.
            Dim propertyRecord As Housing_property = TryCast(obj, Housing_property)
            If propertyRecord Is Nothing Then
                Return
            End If

            If DebtPlus.Data.Prompts.RequestConfirmation_Delete() <> DialogResult.Yes Then
                Return
            End If

            ' Delete the property
            If DeleteProperty(propertyRecord) Then
                colProperties.Remove(propertyRecord)
                GridControl1.RefreshDataSource()
            End If
        End Sub

        ''' <summary>
        ''' Delete the indicated property from the system
        ''' </summary>
        ''' <param name="propertyRecord"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function DeleteProperty(ByVal propertyRecord As Housing_property) As Boolean
            Using bc As New BusinessContext()
                Dim q As Housing_property = bc.Housing_properties.Where(Function(s) s.Id = propertyRecord.Id).FirstOrDefault()
                If q IsNot Nothing Then
                    bc.Housing_properties.DeleteOnSubmit(q)
                    bc.SubmitChanges()
                    Return True
                End If
            End Using
            Return False
        End Function

        ''' <summary>
        ''' Do the edit operation on the property record.
        ''' </summary>
        ''' <param name="propertyRecord"></param>
        ''' <returns></returns>
        ''' <remarks>We did, at one point, care about changing the client's home address. We don't now because
        ''' we don't change the home address record. If we create a new address record it is not the home address
        ''' record. It is a new address record. So, therefore, we can't update the home address for the client
        ''' from this form. We can only change the home address from the general tab where we edit the home address
        ''' record itself.</remarks>
        Private Function EditProperty(ByVal propertyRecord As Housing_property) As Boolean

            Using frm As New HousingPropertyForm(Context, propertyRecord)
                Dim answer As DialogResult = frm.ShowDialog()
                If answer <> DialogResult.OK Then
                    Return False
                End If

                ' Do not update the HPF_foreclosureCaseID field. This is display only in this control and if the property record is
                ' being shown and the person does an update, you don't want the ID field being overwritten here. It is set in the
                ' upload sequence to the proper value. Don't touch it here.

                Using bc As New BusinessContext()
                    Dim q As Housing_property = bc.Housing_properties.Where(Function(s) s.Id = propertyRecord.Id).FirstOrDefault()
                    If q IsNot Nothing Then
                        q.annual_ins_amount = propertyRecord.annual_ins_amount
                        q.annual_property_tax = propertyRecord.annual_property_tax
                        q.Appraisal_Date = propertyRecord.Appraisal_Date
                        q.Appraisal_Type = propertyRecord.Appraisal_Type
                        q.CoOwnerID = propertyRecord.CoOwnerID
                        q.FcNoticeReceivedIND = propertyRecord.FcNoticeReceivedIND
                        q.FcSaleDT = propertyRecord.FcSaleDT
                        q.ForSaleIND = propertyRecord.ForSaleIND
                        q.HOA_delinq_amount = propertyRecord.HOA_delinq_amount
                        q.HOA_delinq_state = propertyRecord.HOA_delinq_state
                        q.homeowner_ins = propertyRecord.homeowner_ins
                        q.HousingID = propertyRecord.HousingID
                        q.HPF_counselor = propertyRecord.HPF_counselor
                        q.HPF_interviewID = propertyRecord.HPF_interviewID
                        q.HPF_Program = propertyRecord.HPF_Program
                        q.HPF_SubProgram = propertyRecord.HPF_SubProgram
                        q.HPF_worked_with_other_agency = propertyRecord.HPF_worked_with_other_agency
                        q.HPF_ReasonForCallCD = propertyRecord.HPF_ReasonForCallCD
                        q.ImprovementsValue = propertyRecord.ImprovementsValue
                        q.ins_delinq_state = propertyRecord.ins_delinq_state
                        q.ins_due_amount = propertyRecord.ins_due_amount
                        q.ins_due_date = propertyRecord.ins_due_date
                        q.LandValue = propertyRecord.LandValue
                        q.last_change_by = propertyRecord.last_change_by
                        q.last_change_date = propertyRecord.last_change_date
                        q.Occupancy = propertyRecord.Occupancy
                        q.OwnerID = propertyRecord.OwnerID
                        q.PlanToKeepHomeCD = propertyRecord.PlanToKeepHomeCD
                        q.pmi_insurance = propertyRecord.pmi_insurance
                        q.PreviousAddress = propertyRecord.PreviousAddress
                        q.property_tax = propertyRecord.property_tax
                        q.PropertyAddress = propertyRecord.PropertyAddress
                        q.PropertyType = propertyRecord.PropertyType
                        q.PurchasePrice = propertyRecord.PurchasePrice
                        q.PurchaseYear = propertyRecord.PurchaseYear
                        q.RealityCompany = propertyRecord.RealityCompany
                        q.Residency = propertyRecord.Residency
                        q.SalePrice = propertyRecord.SalePrice
                        q.sales_contract_date = propertyRecord.sales_contract_date
                        q.tax_delinq_state = propertyRecord.tax_delinq_state
                        q.tax_due_amount = propertyRecord.tax_due_amount
                        q.tax_due_date = propertyRecord.tax_due_date
                        q.TrustName = propertyRecord.TrustName
                        q.UseHomeAddress = propertyRecord.UseHomeAddress
                        q.UseInReports = propertyRecord.UseInReports

                        bc.SubmitChanges()
                    End If
                End Using

                Return True
            End Using
        End Function

        ''' <summary>
        ''' Read the properties table
        ''' </summary>
        Public Shadows Sub ReadForm(ByVal clientHousingRecord As client_housing)

            ' Define the formatting fields for the grid control
            GridColumn_Address1.DisplayFormat.Format = New Address1_Formatter(Me)
            GridColumn_Address1.GroupFormat.Format = New Address1_Formatter(Me)
            GridColumn_City.DisplayFormat.Format = New City_Formatter(Me)
            GridColumn_City.GroupFormat.Format = New City_Formatter(Me)
            GridColumn_State.DisplayFormat.Format = New State_Formatter(Me)
            GridColumn_State.GroupFormat.Format = New State_Formatter(Me)
            GridColumn_PrimaryResidenceIND.DisplayFormat.Format = New Residency_Formatter(Me)
            GridColumn_PrimaryResidenceIND.GroupFormat.Format = New Residency_Formatter(Me)
            GridColumn_Type.DisplayFormat.Format = New Type_Formatter(Me)
            GridColumn_Type.GroupFormat.Format = New Type_Formatter(Me)

            ' Save the housing record pointer for later references as needed.

            ' NOTE: this should be the same value as stored in the context.ClientDS structure, but
            ' for the sake of orthogonality, we don't use the single item in that class.
            Me.clientHousingRecord = clientHousingRecord

            ' Update the grid with the items for this housing record.
            Using bc As New BusinessContext()
                colProperties = bc.Housing_properties.Where(Function(s) s.HousingID = clientHousingRecord.Id).ToList()
                GridControl1.DataSource = colProperties
                GridView1.BestFitColumns()
                GridControl1.RefreshDataSource()
            End Using
        End Sub

        Private MustInherit Class BaseFormatter
            Implements ICustomFormatter, IFormatProvider

            Protected parentControl As PropertyListControl
            Public Sub New(ByVal parentControl As PropertyListControl)
                Me.parentControl = parentControl
            End Sub

            Public MustOverride Function Format(ByVal format1 As String, ByVal arg As Object,
                                                ByVal formatProvider As IFormatProvider) As String _
                Implements ICustomFormatter.Format

            Public Function GetFormat(ByVal formatType As Type) As Object Implements IFormatProvider.GetFormat
                Return Me
            End Function

            ''' <summary>
            ''' Return the property address record.
            ''' </summary>
            ''' <param name="key">ID of the housing_property record in the parent's list.</param>
            ''' <returns></returns>
            ''' <remarks></remarks>
            Protected Function getAddressRecord(ByVal key As System.Nullable(Of System.Int32)) As DebtPlus.Interfaces.Addresses.IAddress

                If key.HasValue Then
                    Dim prop As Housing_property = parentControl.colProperties.Where(Function(s) s.Id = key.Value).FirstOrDefault()

                    If prop IsNot Nothing Then

                        ' If we are using the client's home address then take it from the ClientDS structure.
                        If prop.UseHomeAddress OrElse Not prop.PropertyAddress.HasValue Then
                            Return parentControl.Context.ClientDs.clientAddress
                        End If

                        ' Take the address from the database.
                        Using bc As New BusinessContext()
                            Return bc.addresses.Where(Function(s) s.Id = prop.PropertyAddress.Value).FirstOrDefault()
                        End Using
                    End If
                End If

                ' Return the empty structure since the item is not in the tables
                Return Nothing
            End Function
        End Class

        Private Class Address1_Formatter
            Inherits BaseFormatter

            Public Sub New(ByVal parentControl As PropertyListControl)
                MyBase.New(parentControl)
            End Sub

            Public Overrides Function Format(ByVal format1 As String, ByVal arg As Object,
                                             ByVal formatProvider As IFormatProvider) As String

                Dim key As System.Nullable(Of System.Int32) = DebtPlus.Utils.Nulls.v_Int32(arg)
                Dim adr As DebtPlus.Interfaces.Addresses.IAddress = getAddressRecord(key)
                If adr IsNot Nothing Then
                    Return DebtPlus.LINQ.address.getAddressLine1(adr.house, adr.direction, adr.street, adr.suffix, adr.modifier, adr.modifier_value)
                End If

                Return String.Empty
            End Function
        End Class

        Private Class City_Formatter
            Inherits BaseFormatter

            Public Sub New(ByVal parentControl As PropertyListControl)
                MyBase.New(parentControl)
            End Sub

            Public Overrides Function Format(ByVal format1 As String, ByVal arg As Object,
                                             ByVal formatProvider As IFormatProvider) As String

                Dim key As System.Nullable(Of System.Int32) = DebtPlus.Utils.Nulls.v_Int32(arg)
                Dim adr As DebtPlus.Interfaces.Addresses.IAddress = getAddressRecord(key)
                If adr IsNot Nothing Then
                    Return adr.city
                End If

                Return String.Empty
            End Function
        End Class

        Private Class State_Formatter
            Inherits BaseFormatter

            Public Sub New(ByVal parentControl As PropertyListControl)
                MyBase.New(parentControl)
            End Sub

            Public Overrides Function Format(ByVal format1 As String, ByVal arg As Object, ByVal formatProvider As IFormatProvider) As String

                Dim key As System.Nullable(Of System.Int32) = DebtPlus.Utils.Nulls.v_Int32(arg)
                Dim adr As DebtPlus.Interfaces.Addresses.IAddress = getAddressRecord(key)
                If adr IsNot Nothing Then
                    Dim stateRecord As DebtPlus.LINQ.state = DebtPlus.LINQ.Cache.state.getList().Find(Function(s) s.Id = adr.state)
                    If stateRecord IsNot Nothing Then
                        Return stateRecord.Name
                    End If
                End If

                Return String.Empty
            End Function
        End Class

        Private Class Residency_Formatter
            Inherits BaseFormatter

            Public Sub New(ByVal parentControl As PropertyListControl)
                MyBase.New(parentControl)
            End Sub

            Public Overrides Function Format(ByVal format1 As String, ByVal arg As Object, ByVal formatProvider As IFormatProvider) As String

                Dim key As System.Nullable(Of System.Int32) = DebtPlus.Utils.Nulls.v_Int32(arg)
                Return Helpers.GetHousingResidencyText(key)
            End Function
        End Class

        Private Class Type_Formatter
            Inherits BaseFormatter

            Public Sub New(ByVal parentControl As PropertyListControl)
                MyBase.New(parentControl)
            End Sub

            Public Overrides Function Format(ByVal format1 As String, ByVal arg As Object,
                                             ByVal formatProvider As IFormatProvider) As String

                Dim key As System.Nullable(Of System.Int32) = DebtPlus.Utils.Nulls.v_Int32(arg)
                Return Helpers.GetHousingTypeText(key)
            End Function
        End Class
    End Class
End Namespace
