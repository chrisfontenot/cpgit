#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DevExpress.XtraLayout.Utils
Imports System.Collections.Generic
Imports System.Linq
Imports System.Data.SqlClient
Imports DebtPlus.LINQ

Namespace controls
    Friend Class LoanControl
        Private loanRecord As Housing_loan

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            ' Hide the controls that are not used in the Current Loan Detail
            LoanDetailsControl_CurrentDetailID.LayoutControlItem_CurrentLoanBalanceAmt.Visibility = LayoutVisibility.Never
            LoanDetailsControl_CurrentDetailID.LayoutControlItem_InterestRate.Visibility = LayoutVisibility.Never
            LoanDetailsControl_CurrentDetailID.LayoutControlItem_MonthsDelinquent.Visibility = LayoutVisibility.Never
            LoanDetailsControl_CurrentDetailID.LayoutControlItem_OrigLoanAmt.Visibility = LayoutVisibility.Never
            LoanDetailsControl_CurrentDetailID.LayoutControlItem_Payment.Visibility = LayoutVisibility.Never
            LoanDetailsControl_CurrentDetailID.LayoutControlItem_MonthsDelinquent.Visibility = LayoutVisibility.Never
            LoanDetailsControl_CurrentDetailID.LayoutControlItem_LoanOrigDate.Visibility = LayoutVisibility.Never
            LoanDetailsControl_CurrentDetailID.LayoutControlItem_DelinquencyAmount.Visibility = LayoutVisibility.Never
            LoanDetailsControl_CurrentDetailID.EmptySpaceItem_LoanOrigDate.Visibility = LayoutVisibility.Never

            LoanDetailsControl_IntakeID.LayoutControlItem_ClosingCosts.Visibility = LayoutVisibility.Never

            ' Hide the controls that are not used in the Intake loan information
            LenderControl_Origional.IsHECMVisible = False
            LenderControl_Origional.IsHPFVisible = False

            ' Ensure that they are shown on the intake lender control
            LenderControl_Intake.IsHECMVisible = True
            LenderControl_Intake.IsHPFVisible = True

            Try
                ' Always select the left most tab page (but don't die if there is no tab group implemented.)
                TabbedControlGroup1.SelectedTabPageIndex = 0
            Catch ex As Exception
            End Try
        End Sub

        Public Shadows Sub ReadForm(bc As BusinessContext, ByVal loanRecord As Housing_loan)
            MyBase.ReadForm(bc)
            Me.loanRecord = loanRecord

            ' Load the lookup controls' data sources
            LookUpEdit_Position.Properties.DataSource = DebtPlus.LINQ.Cache.Housing_LoanPositionType.getList()

            UnRegisterHandlers()
            Try

                ' Set the sub-record items
                LoanDSADetailsControl1.EditValue = loanRecord.DsaDetailID
                LenderControl_Intake.EditValue = loanRecord.CurrentLenderID
                LenderControl_Origional.EditValue = loanRecord.OrigLenderID

                LoanDSADetailsControl1.Language = Context.ClientDs.clientRecord.language
                LoanDetailsControl_CurrentDetailID.EditValue = loanRecord.CurrentDetailID
                LoanDetailsControl_CurrentDetailID.CalcEdit_ClosingCosts.EditValue = loanRecord.MortgageClosingCosts

                ' Some of the items shown on the details page are actually in the housing_loans table. Load those.
                LoanDetailsControl_IntakeID.EditValue = loanRecord.IntakeDetailID
                LoanDetailsControl_IntakeID.CalcEdit_DelinquencyAmount.EditValue = loanRecord.LoanDelinquencyAmount
                LoanDetailsControl_IntakeID.DateEdit_OriginationDate.EditValue = loanRecord.LoanOriginationDate
                LoanDetailsControl_IntakeID.DelinquencyMonths = loanRecord.LoanDelinquencyMonths
                LoanDetailsControl_IntakeID.CalcEdit_CurrentLoanBalanceAmt.EditValue = loanRecord.CurrentLoanBalanceAmt
                LoanDetailsControl_IntakeID.CalcEdit_OrigLoanAmt.EditValue = loanRecord.OrigLoanAmt

                CheckEdit_UseOrigLenderID.Checked = loanRecord.UseOrigLenderID
                CheckEdit_IntakeSameAsCurrent.Checked = loanRecord.IntakeSameAsCurrent
                CheckEdit_UseInReports.Checked = loanRecord.UseInReports.GetValueOrDefault(False)
                LookUpEdit_Position.EditValue = loanRecord.Loan1st2nd

                ' Enable the controls based upon the checked items
                LenderControl_Origional.Enabled = Not CheckEdit_UseOrigLenderID.Checked
                LoanDetailsControl_CurrentDetailID.Enabled = Not CheckEdit_IntakeSameAsCurrent.Checked
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Sub RegisterHandlers()
            AddHandler CheckEdit_UseOrigLenderID.CheckedChanged, AddressOf CheckEdit_UseOrigLenderID_CheckedChanged
            AddHandler CheckEdit_IntakeSameAsCurrent.CheckedChanged, AddressOf CheckEdit_IntakeSameAsCurrent_CheckedChanged
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler CheckEdit_UseOrigLenderID.CheckedChanged, AddressOf CheckEdit_UseOrigLenderID_CheckedChanged
            RemoveHandler CheckEdit_IntakeSameAsCurrent.CheckedChanged, AddressOf CheckEdit_IntakeSameAsCurrent_CheckedChanged
        End Sub

        Friend Shadows Function SaveForm(bc As BusinessContext) As Boolean

            ' Validate the DSA information before the form is saved normally
            Dim strError As String = LoanDSADetailsControl1.ValidateItems(LenderControl_Intake.ServicerID, LenderControl_Intake.InvestorID)
            If Not String.IsNullOrEmpty(strError) Then
                Dim processedMessage As String = strError.Replace(";", Environment.NewLine)
                DebtPlus.Data.Forms.MessageBox.Show(processedMessage, "Error processing DSA validation", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return False
            End If

            ' Save the information once it has been validated
            loanRecord.DsaDetailID = LoanDSADetailsControl1.EditValue
            loanRecord.IntakeDetailID = LoanDetailsControl_IntakeID.EditValue
            loanRecord.CurrentLenderID = LenderControl_Intake.EditValue

            ' Update the remainder of the loan record that is not sub-records
            loanRecord.LoanDelinquencyAmount = DebtPlus.Utils.Nulls.v_Decimal(LoanDetailsControl_IntakeID.CalcEdit_DelinquencyAmount.EditValue).GetValueOrDefault(0D)
            loanRecord.LoanOriginationDate = DebtPlus.Utils.Nulls.v_DateTime(LoanDetailsControl_IntakeID.DateEdit_OriginationDate.EditValue)
            loanRecord.LoanDelinquencyMonths = DebtPlus.Utils.Nulls.v_Int32(LoanDetailsControl_IntakeID.DelinquencyMonths).GetValueOrDefault(0)
            loanRecord.CurrentLoanBalanceAmt = DebtPlus.Utils.Nulls.v_Decimal(LoanDetailsControl_IntakeID.CalcEdit_CurrentLoanBalanceAmt.EditValue).GetValueOrDefault(0D)
            loanRecord.OrigLoanAmt = DebtPlus.Utils.Nulls.v_Decimal(LoanDetailsControl_IntakeID.CalcEdit_OrigLoanAmt.EditValue).GetValueOrDefault(0D)
            loanRecord.MortgageClosingCosts = DebtPlus.Utils.Nulls.v_Decimal(LoanDetailsControl_CurrentDetailID.CalcEdit_ClosingCosts.EditValue).GetValueOrDefault(0D)
            loanRecord.Loan1st2nd = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_Position.EditValue)
            loanRecord.UseOrigLenderID = CheckEdit_UseOrigLenderID.Checked
            loanRecord.IntakeSameAsCurrent = CheckEdit_IntakeSameAsCurrent.Checked
            loanRecord.UseInReports = CheckEdit_UseInReports.Checked

            ' If the current is the same as intake then discard the current.
            If CheckEdit_IntakeSameAsCurrent.Checked Then
                If loanRecord.CurrentDetailID.HasValue Then
                    Dim q As DebtPlus.LINQ.Housing_loan_detail = bc.Housing_loan_details.Where(Function(s) s.Id = loanRecord.CurrentDetailID.Value).FirstOrDefault()
                    If q IsNot Nothing Then
                        bc.Housing_loan_details.DeleteOnSubmit(q)
                        bc.SubmitChanges()
                    End If
                End If
                loanRecord.CurrentDetailID = Nothing
            Else
                loanRecord.CurrentDetailID = LoanDetailsControl_CurrentDetailID.EditValue
            End If

            ' Clear the original lender information if it is to be the same as the current values.
            If CheckEdit_UseOrigLenderID.Checked Then
                If loanRecord.OrigLenderID.HasValue Then
                    Dim q As Housing_lender = bc.Housing_lenders.Where(Function(s) s.Id = loanRecord.OrigLenderID.Value).FirstOrDefault()
                    If q IsNot Nothing Then
                        bc.Housing_lenders.DeleteOnSubmit(q)
                        bc.SubmitChanges()
                    End If
                End If
                loanRecord.OrigLenderID = Nothing
            Else
                loanRecord.OrigLenderID = LenderControl_Origional.EditValue
            End If

            MyBase.SaveForm(bc)
            Return True
        End Function

        Private Sub CheckEdit_UseOrigLenderID_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
            LenderControl_Origional.Enabled = Not CheckEdit_UseOrigLenderID.Checked
        End Sub

        Private Sub CheckEdit_IntakeSameAsCurrent_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
            LoanDetailsControl_CurrentDetailID.Enabled = Not CheckEdit_IntakeSameAsCurrent.Checked
        End Sub
    End Class
End Namespace