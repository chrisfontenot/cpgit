#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.Data.Controls
Imports DebtPlus.Utils
Imports DebtPlus.UI.Client.Service
Imports System.Data.SqlClient
Imports System.Linq
Imports DebtPlus.LINQ

Namespace controls

    Friend Class IndicatorsControl
        Inherits ControlBaseClient

        ' List of the client's checked indicators
        Dim colClientIndicators As System.Collections.Generic.List(Of client_indicator) = Nothing

        ''Dim bc As New BusinessContext()
        '' Dim bc As BusinessContext = Nothing

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then components.Dispose()
                    ''If bc IsNot Nothing Then bc.Dispose()
                End If
                components = Nothing
                ''bc = Nothing
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer = Nothing

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents IndicatorList As DevExpress.XtraEditors.CheckedListBoxControl

        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.IndicatorList = New DevExpress.XtraEditors.CheckedListBoxControl
            CType(Me.IndicatorList, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'IndicatorList
            '
            Me.IndicatorList.CheckOnClick = True
            Me.IndicatorList.Dock = System.Windows.Forms.DockStyle.Fill
            Me.IndicatorList.Location = New System.Drawing.Point(0, 0)
            Me.IndicatorList.Name = "IndicatorList"
            Me.IndicatorList.Size = New System.Drawing.Size(576, 296)
            Me.IndicatorList.TabIndex = 0
            Me.IndicatorList.SortOrder = System.Windows.Forms.SortOrder.Ascending
            '
            'IndicatorsControl
            '
            Me.Controls.Add(Me.IndicatorList)
            Me.Name = "IndicatorsControl"
            CType(Me.IndicatorList, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
        End Sub

#End Region

        ''' <summary>
        ''' Read and display the information on the control
        ''' </summary>
        Public Overrides Sub ReadForm(bc As DebtPlus.LINQ.BusinessContext)

            Try
                ' If there is a collection then we don't need to read it again.
                If colClientIndicators IsNot Nothing Then
                    Return
                End If

                ' Populate the list on the initial time with the current descriptions from the cache.
                ' Only load the items that are not marked "hidden".
                Dim aryItems As New System.Collections.ArrayList()
                For Each item As DebtPlus.LINQ.indicator In DebtPlus.LINQ.Cache.indicator.getList().FindAll(Function(s) Not s.Hidden)
                    Dim newItem As DebtPlus.Data.Controls.SortedCheckedListboxControlItem = New DebtPlus.Data.Controls.SortedCheckedListboxControlItem(item.Id, item.description, CheckState.Unchecked)
                    aryItems.Add(newItem)
                Next

                ' Sort the indicators by name. Load all of the items at once so there is only one sort for the entire list.
                IndicatorList.Items.Clear()
                IndicatorList.SortOrder = Windows.Forms.SortOrder.Ascending
                IndicatorList.Items.AddRange(aryItems.ToArray)

                ' Read the list of indicators that have been set for this client
                colClientIndicators = bc.client_indicators.Where(Function(s) s.client = Context.ClientDs.ClientId).ToList()

                ' Check all of the items that need to be checked because they are in the list.
                For Each item As client_indicator In colClientIndicators
                    Dim indicatorID As Int32 = item.indicator
                    Dim q As DebtPlus.Data.Controls.SortedCheckedListboxControlItem = IndicatorList.Items.OfType(Of DebtPlus.Data.Controls.SortedCheckedListboxControlItem).Where(Function(s) Convert.ToInt32(s.Value) = indicatorID).FirstOrDefault()
                    If q IsNot Nothing Then
                        q.CheckState = CheckState.Checked
                    End If
                Next

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading indicator data")
            End Try
        End Sub

        ''' <summary>
        ''' Save the control contents to the database
        ''' </summary>
        Public Overrides Sub SaveForm(bc As DebtPlus.LINQ.BusinessContext)
            MyBase.SaveForm(bc)

            ' Process the indicator list
            For Each indicatorItem As DebtPlus.Data.Controls.SortedCheckedListboxControlItem In IndicatorList.Items
                Dim indicatorID As Int32 = Convert.ToInt32(indicatorItem.Value)
                Dim q As DebtPlus.LINQ.client_indicator = colClientIndicators.Where(Function(s) s.indicator = indicatorID).FirstOrDefault()

                ' If the item is checked then we need to add it if it is not found
                If indicatorItem.CheckState = CheckState.Checked Then
                    If q Is Nothing Then

                        ' Manufacture a new checked item for this client's list
                        q = DebtPlus.LINQ.Factory.Manufacture_client_indicator()
                        q.client = Context.ClientDs.ClientId
                        q.indicator = indicatorID

                        bc.client_indicators.InsertOnSubmit(q)
                        colClientIndicators.Add(q)
                    End If
                Else
                    If q IsNot Nothing Then
                        bc.client_indicators.DeleteOnSubmit(q)
                        colClientIndicators.Remove(q)
                    End If
                End If
            Next

            Try
                ' Do the updates that are pending
                bc.SubmitChanges()
            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating client_indicators table")
            End Try
        End Sub
    End Class
End Namespace
