﻿Imports System.Linq
Imports DebtPlus.LINQ
Imports System.Windows.Forms
Imports System

Namespace controls
    Public Class ProductsTransacitons
        Inherits DevExpress.XtraEditors.XtraUserControl

        Private record As DebtPlus.LINQ.client_product = Nothing
        Private bc As BusinessContext = Nothing

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Friend Shadows Sub ReadForm(bc As BusinessContext, record As client_product)
            UnRegisterHandlers()
            Try
                MyClass.bc = bc
                MyClass.record = record

                ' Load the list and set the default value
                If LookUpEdit_DateRange.Properties.DataSource Is Nothing Then
                    LookUpEdit_DateRange.Properties.DataSource = DebtPlus.LINQ.InMemory.dateRangeTypes.getList()
                    LookUpEdit_DateRange.EditValue = DebtPlus.LINQ.InMemory.dateRangeTypes.getDefault().GetValueOrDefault(System.Convert.ToInt32(DebtPlus.LINQ.InMemory.dateRangeType.DateItems.DateItem_Last3Months))
                    LookUpEdit_DateRange.Properties.ForceInitialize()
                End If

                ReadFormItems(TryCast(LookUpEdit_DateRange.GetSelectedDataRow, DebtPlus.LINQ.InMemory.dateRangeType))
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Friend Shadows Sub SaveForm()
        End Sub

        Private Sub RegisterHandlers()
        End Sub

        Private Sub UnRegisterHandlers()
        End Sub

        Private Sub LookUpEdit_DateRange_EditValueChanging(sender As Object, e As DevExpress.XtraEditors.Controls.ChangingEventArgs)
            UnRegisterHandlers()
            Try
                If Not ReadFormItems(TryCast(e.NewValue, DebtPlus.LINQ.InMemory.dateRangeType)) Then
                    e.Cancel = True
                End If
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Function ReadFormItems(selectedItem As DebtPlus.LINQ.InMemory.dateRangeType) As Boolean

            If selectedItem IsNot Nothing Then
                If Not selectedItem.isVariable Then
                    ProductsTransactionGrid1.ReadForm(bc, record, selectedItem.startingDate, selectedItem.endingDate)
                    Return True
                End If

                Using frm As New DebtPlus.Reports.Template.Forms.DateReportParametersForm(DebtPlus.Utils.DateRange.Specific_Date_Range)
                    If frm.ShowDialog() = DialogResult.OK Then
                        ProductsTransactionGrid1.ReadForm(bc, record, frm.Parameter_FromDate, frm.Parameter_ToDate)
                        Return True
                    End If
                End Using
            End If

            Return False
        End Function
    End Class
End Namespace
