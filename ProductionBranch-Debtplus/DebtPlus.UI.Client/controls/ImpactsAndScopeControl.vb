#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.Data.Controls
Imports DebtPlus.Utils
Imports DebtPlus.UI.Client.Service
Imports System.Data.SqlClient
Imports System.Linq
Imports DebtPlus.LINQ

Namespace controls

    Friend Class ImpactsAndScopeControl
        Inherits ControlBaseClient

        ' Current POV being edited
        Dim pov As DebtPlus.LINQ.hud_interview = Nothing
        Dim bc As BusinessContext = Nothing

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            RegisterHandlers()

            ' We need this when the item is loaded, not when the items are read. So, we can't add this to the
            ' RegisterHandlers. Besides, it does not do anything but look at the text of the item anyway....
            AddHandler CheckList_ImpactsAndScope.MeasureItem, AddressOf CheckList_ImpactsAndScope_MeasureItem
        End Sub

        Private Sub RegisterHandlers()
            AddHandler CheckList_ImpactsAndScope.ItemChecking, AddressOf CheckList_ImpactsAndScope_ItemChecking
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler CheckList_ImpactsAndScope.ItemChecking, AddressOf CheckList_ImpactsAndScope_ItemChecking
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents CheckList_ImpactsAndScope As DevExpress.XtraEditors.CheckedListBoxControl

        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.CheckList_ImpactsAndScope = New DevExpress.XtraEditors.CheckedListBoxControl()
            CType(Me.CheckList_ImpactsAndScope, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'CheckList_ImpactsAndScope
            '
            Me.CheckList_ImpactsAndScope.Appearance.Options.UseTextOptions = True
            Me.CheckList_ImpactsAndScope.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.CheckList_ImpactsAndScope.CheckOnClick = True
            Me.CheckList_ImpactsAndScope.Dock = System.Windows.Forms.DockStyle.Fill
            Me.CheckList_ImpactsAndScope.Location = New System.Drawing.Point(0, 0)
            Me.CheckList_ImpactsAndScope.Name = "CheckList_ImpactsAndScope"
            Me.CheckList_ImpactsAndScope.Size = New System.Drawing.Size(576, 296)
            Me.CheckList_ImpactsAndScope.SortOrder = System.Windows.Forms.SortOrder.Ascending
            Me.CheckList_ImpactsAndScope.TabIndex = 0
            '
            'ImpactsAndScopeControl
            '
            Me.Controls.Add(Me.CheckList_ImpactsAndScope)
            Me.Name = "ImpactsAndScopeControl"
            CType(Me.CheckList_ImpactsAndScope, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        ''' <summary>
        ''' Process the change in the enabled status
        ''' </summary>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Protected Overrides Sub OnEnabledChanged(e As System.EventArgs)
            MyBase.OnEnabledChanged(e)
            CheckList_ImpactsAndScope.Enabled = Enabled
        End Sub

        ''' <summary>
        ''' Read and display the information on the control
        ''' </summary>
        Friend Overloads Sub ReadForm(bc As BusinessContext, record As DebtPlus.LINQ.hud_interview)
            Me.bc = bc
            MyBase.ReadForm(bc)

            ' If the collection is defined then we have been here before and the control
            ' properly set (it was left alone from the last time that we were here.)
            If pov IsNot Nothing Then
                Return
            End If

            UnRegisterHandlers()
            Try
                Me.pov = record

                ' Populate the list on the initial time with the current descriptions
                CheckList_ImpactsAndScope.Items.Clear()
                For Each chkItem As DebtPlus.LINQ.Housing_HUD_ImpactType In bc.Housing_HUD_ImpactTypes
                    CheckList_ImpactsAndScope.Items.Add(chkItem, chkItem.description, CheckState.Unchecked, chkItem.ActiveFlag)
                Next

                ' Check all of the items that need to be checked because they are in the list.
                Dim ie As System.Collections.IEnumerator = pov.Housing_Client_Impacts.GetEnumerator()
                Do While ie.MoveNext()
                    Dim item As DebtPlus.LINQ.Housing_Client_Impact = DirectCast(ie.Current, DebtPlus.LINQ.Housing_Client_Impact)
                    Dim qRecord As DevExpress.XtraEditors.Controls.CheckedListBoxItem = CheckList_ImpactsAndScope.Items.OfType(Of DevExpress.XtraEditors.Controls.CheckedListBoxItem)().Where(Function(s) DirectCast(s.Value, Housing_HUD_ImpactType).Id = item.ImpactId).FirstOrDefault()
                    If qRecord IsNot Nothing Then
                        qRecord.CheckState = CheckState.Checked
                    End If
                Loop

            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Set the indicators based upon the result setting.
        ''' </summary>
        ''' <param name="resultID"></param>
        ''' <remarks></remarks>
        Public Sub SetIndicatorsForResult(ByVal povID As System.Nullable(Of Int32), ByVal resultID As System.Nullable(Of Int32))
            UnRegisterHandlers()
            Try
                ' Turn off all indicators that are associated with any result
                Dim ie As System.Collections.IEnumerator = bc.Housing_ImpactResultTypes.GetEnumerator()
                Do While ie.MoveNext()
                    Dim link As DebtPlus.LINQ.Housing_ImpactResultType = DirectCast(ie.Current, DebtPlus.LINQ.Housing_ImpactResultType)
                    Dim qRecord As DevExpress.XtraEditors.Controls.CheckedListBoxItem = CheckList_ImpactsAndScope.Items.OfType(Of DevExpress.XtraEditors.Controls.CheckedListBoxItem)().Where(Function(s) DirectCast(s.Value, Housing_HUD_ImpactType).Id = link.ImpactType).FirstOrDefault()
                    If qRecord IsNot Nothing Then
                        qRecord.CheckState = CheckState.Unchecked
                    End If
                Loop

                If povID.HasValue AndAlso resultID.HasValue Then
                    ' Set the indicator for the indicated result
                    For Each link As DebtPlus.LINQ.Housing_ImpactResultType In bc.Housing_ImpactResultTypes.Where(Function(s) s.ResultType = resultID.Value AndAlso s.POVType = povID.Value)
                        Dim Id As Int32 = link.ImpactType

                        Dim qRecord As DevExpress.XtraEditors.Controls.CheckedListBoxItem = CheckList_ImpactsAndScope.Items.OfType(Of DevExpress.XtraEditors.Controls.CheckedListBoxItem)().Where(Function(s) DirectCast(s.Value, Housing_HUD_ImpactType).Id = Id).FirstOrDefault()
                        If qRecord IsNot Nothing Then
                            qRecord.CheckState = CheckState.Checked
                        End If
                    Next
                End If

            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Save the control contents to the database
        ''' </summary>
        Public Overrides Sub SaveForm(bc As DebtPlus.LINQ.BusinessContext)

            UnRegisterHandlers()
            Try
                ' Correct the list of items for this P.O.V.
                Dim ieList As IEnumerator = CheckList_ImpactsAndScope.Items.GetEnumerator
                Do While ieList.MoveNext
                    Dim listitem As DevExpress.XtraEditors.Controls.CheckedListBoxItem = CType(ieList.Current, DevExpress.XtraEditors.Controls.CheckedListBoxItem)
                    Dim itemType As Housing_HUD_ImpactType = DirectCast(listitem.Value, Housing_HUD_ImpactType)
                    Dim q As Housing_Client_Impact = pov.Housing_Client_Impacts.Where(Function(s) s.ImpactId = itemType.Id).FirstOrDefault()

                    ' Look for the indicator in the list
                    If listitem.CheckState = CheckState.Unchecked Then

                        ' If the item is not checked then delete an item when it is found
                        If q IsNot Nothing Then
                            pov.Housing_Client_Impacts.Remove(q)
                            q.hud_interview = Nothing
                        End If
                    Else
                        ' If the items is checked then add an item that is missing
                        If q Is Nothing Then
                            q = DebtPlus.LINQ.Factory.Manufacture_Housing_Client_Impact()
                            q.Housing_HUD_ImpactType = itemType
                            pov.Housing_Client_Impacts.Add(q)
                        End If
                    End If
                Loop

            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Sub CheckList_ImpactsAndScope_MeasureItem(ByVal sender As Object, ByVal e As MeasureItemEventArgs)
            Dim viewInfo As DevExpress.XtraEditors.ViewInfo.CheckedListBoxViewInfo = TryCast(CheckList_ImpactsAndScope.GetViewInfo(), DevExpress.XtraEditors.ViewInfo.CheckedListBoxViewInfo)

            ' Find the width of the wrapped line
            Dim width As Integer = viewInfo.PaintAppearance.CalcTextSize(e.Graphics, CheckList_ImpactsAndScope.GetItemText(e.Index), 0).ToSize().Width
            width += viewInfo.FullMarkWidth + 3 ' Accommodate the checkbox and it's spacing from the box to the text area.

            ' Find the number of lines of wrapped text
            Dim lines As Integer = 0
            Try
                If viewInfo.ContentRect.Width > 0 Then
                    lines = Convert.ToInt32(Convert.ToDouble(width) / viewInfo.ContentRect.Width)
                End If
            Catch ex As System.Exception
            End Try

            ' Allow for a blank line after the item.
            e.ItemHeight = e.ItemHeight * (lines + 1)
        End Sub

        Private Sub CheckList_ImpactsAndScope_ItemChecking(sender As Object, e As DevExpress.XtraEditors.Controls.ItemCheckingEventArgs)

            ' Find the item that is being changed.
            Dim itemTOChange As DevExpress.XtraEditors.Controls.CheckedListBoxItem = CheckList_ImpactsAndScope.Items(e.Index)

            ' Determine if that item is referenced in the control tables
            Dim itemId As Int32 = DirectCast(itemTOChange.Value, DebtPlus.LINQ.Housing_HUD_ImpactType).Id
            Dim q As DebtPlus.LINQ.Housing_ImpactResultType = DebtPlus.LINQ.Cache.Housing_ImpactResultType.getList().Find(Function(s) s.ImpactType = itemId)

            ' If the item is found then don't allow its status to be changed.
            If q IsNot Nothing Then
                e.Cancel = True
            End If
        End Sub
    End Class
End Namespace
