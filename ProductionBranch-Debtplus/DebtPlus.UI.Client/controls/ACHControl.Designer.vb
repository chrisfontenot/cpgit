﻿Namespace controls
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ACHControl
        Inherits ControlBaseClient

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.txt_AccountNumber = New DevExpress.XtraEditors.TextEdit
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl
            Me.dt_EnrollDate = New DevExpress.XtraEditors.DateEdit
            Me.dt_EndDate = New DevExpress.XtraEditors.DateEdit
            Me.dt_ContractDate = New DevExpress.XtraEditors.DateEdit
            Me.lbl_PrenoteDate = New DevExpress.XtraEditors.LabelControl
            Me.dt_ErrorDate = New DevExpress.XtraEditors.DateEdit
            Me.txt_ABA = New DevExpress.XtraEditors.TextEdit
            Me.dt_StartDate = New DevExpress.XtraEditors.DateEdit
            Me.chk_isActive = New DevExpress.XtraEditors.CheckEdit
            Me.lk_CheckingSavings = New DevExpress.XtraEditors.LookUpEdit
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup
            Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup
            Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem
            Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem
            CType(Me.txt_AccountNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.dt_EnrollDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_EnrollDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_EndDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_EndDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_ContractDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_ContractDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_ErrorDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_ErrorDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txt_ABA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_StartDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_StartDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.chk_isActive.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lk_CheckingSavings.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'txt_AccountNumber
            '
            Me.txt_AccountNumber.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                                 Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.txt_AccountNumber.Location = New System.Drawing.Point(333, 96)
            Me.txt_AccountNumber.Name = "txt_AccountNumber"
            Me.txt_AccountNumber.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.txt_AccountNumber.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.txt_AccountNumber.Properties.MaxLength = 17
            Me.txt_AccountNumber.Size = New System.Drawing.Size(157, 20)
            Me.txt_AccountNumber.StyleController = Me.LayoutControl1
            Me.txt_AccountNumber.TabIndex = 11
            Me.txt_AccountNumber.ToolTip = "This is the bank's account number for the client. It may be up to 17 digits long." & _
                                           ""
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.dt_EnrollDate)
            Me.LayoutControl1.Controls.Add(Me.dt_EndDate)
            Me.LayoutControl1.Controls.Add(Me.dt_ContractDate)
            Me.LayoutControl1.Controls.Add(Me.txt_AccountNumber)
            Me.LayoutControl1.Controls.Add(Me.lbl_PrenoteDate)
            Me.LayoutControl1.Controls.Add(Me.dt_ErrorDate)
            Me.LayoutControl1.Controls.Add(Me.txt_ABA)
            Me.LayoutControl1.Controls.Add(Me.dt_StartDate)
            Me.LayoutControl1.Controls.Add(Me.chk_isActive)
            Me.LayoutControl1.Controls.Add(Me.lk_CheckingSavings)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Margin = New System.Windows.Forms.Padding(0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(495, 179)
            Me.LayoutControl1.TabIndex = 9
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'dt_EnrollDate
            '
            Me.dt_EnrollDate.EditValue = Nothing
            Me.dt_EnrollDate.Location = New System.Drawing.Point(88, 120)
            Me.dt_EnrollDate.Name = "dt_EnrollDate"
            Me.dt_EnrollDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.dt_EnrollDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.dt_EnrollDate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
            Me.dt_EnrollDate.Size = New System.Drawing.Size(158, 20)
            Me.dt_EnrollDate.StyleController = Me.LayoutControl1
            Me.dt_EnrollDate.TabIndex = 14
            Me.dt_EnrollDate.ToolTip = "This is the date that the client enrolled in the program. It is not necessarily t" & _
                                       "he same as the contract date, but usually is."
            '
            'dt_EndDate
            '
            Me.dt_EndDate.EditValue = Nothing
            Me.dt_EndDate.Location = New System.Drawing.Point(333, 144)
            Me.dt_EndDate.Name = "dt_EndDate"
            Me.dt_EndDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.dt_EndDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.dt_EndDate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
            Me.dt_EndDate.Size = New System.Drawing.Size(157, 20)
            Me.dt_EndDate.StyleController = Me.LayoutControl1
            Me.dt_EndDate.TabIndex = 13
            Me.dt_EndDate.ToolTip = "ACH transfers will not be made after this date."
            '
            'dt_ContractDate
            '
            Me.dt_ContractDate.EditValue = Nothing
            Me.dt_ContractDate.Location = New System.Drawing.Point(88, 96)
            Me.dt_ContractDate.Name = "dt_ContractDate"
            Me.dt_ContractDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.dt_ContractDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.dt_ContractDate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
            Me.dt_ContractDate.Size = New System.Drawing.Size(158, 20)
            Me.dt_ContractDate.StyleController = Me.LayoutControl1
            Me.dt_ContractDate.TabIndex = 12
            Me.dt_ContractDate.ToolTip = "This is the date that the user signed the ACH agreement."
            '
            'lbl_PrenoteDate
            '
            Me.lbl_PrenoteDate.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
            Me.lbl_PrenoteDate.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_PrenoteDate.Location = New System.Drawing.Point(88, 48)
            Me.lbl_PrenoteDate.Name = "lbl_PrenoteDate"
            Me.lbl_PrenoteDate.Size = New System.Drawing.Size(158, 13)
            Me.lbl_PrenoteDate.StyleController = Me.LayoutControl1
            Me.lbl_PrenoteDate.TabIndex = 1
            Me.lbl_PrenoteDate.Text = "NOT PRENOTED"
            Me.lbl_PrenoteDate.ToolTip = "This is the date that the account was verified with the bank."
            Me.lbl_PrenoteDate.UseMnemonic = False
            '
            'dt_ErrorDate
            '
            Me.dt_ErrorDate.EditValue = Nothing
            Me.dt_ErrorDate.Location = New System.Drawing.Point(88, 72)
            Me.dt_ErrorDate.Name = "dt_ErrorDate"
            Me.dt_ErrorDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.dt_ErrorDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.dt_ErrorDate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
            Me.dt_ErrorDate.Size = New System.Drawing.Size(158, 20)
            Me.dt_ErrorDate.StyleController = Me.LayoutControl1
            Me.dt_ErrorDate.TabIndex = 5
            Me.dt_ErrorDate.ToolTip = "If ACH receives an error, this is the date that it was posted. Further ACH pulls " & _
                                      "may not be done until this is cleared. To clear, press Control-0 (zero)."
            '
            'txt_ABA
            '
            Me.txt_ABA.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                       Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.txt_ABA.Location = New System.Drawing.Point(333, 72)
            Me.txt_ABA.Name = "txt_ABA"
            Me.txt_ABA.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.txt_ABA.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.txt_ABA.Properties.Mask.BeepOnError = True
            Me.txt_ABA.Properties.Mask.EditMask = "\d{9}"
            Me.txt_ABA.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.txt_ABA.Properties.MaxLength = 9
            Me.txt_ABA.Size = New System.Drawing.Size(157, 20)
            Me.txt_ABA.StyleController = Me.LayoutControl1
            Me.txt_ABA.TabIndex = 7
            Me.txt_ABA.ToolTip = "This is the 9 digit ABA number. It is the number assigned to the bank."
            '
            'dt_StartDate
            '
            Me.dt_StartDate.EditValue = Nothing
            Me.dt_StartDate.Location = New System.Drawing.Point(333, 120)
            Me.dt_StartDate.Name = "dt_StartDate"
            Me.dt_StartDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.dt_StartDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.dt_StartDate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
            Me.dt_StartDate.Size = New System.Drawing.Size(157, 20)
            Me.dt_StartDate.StyleController = Me.LayoutControl1
            Me.dt_StartDate.TabIndex = 9
            Me.dt_StartDate.ToolTip = "ACH transfers will not be made before this date"
            '
            'chk_isActive
            '
            Me.chk_isActive.Location = New System.Drawing.Point(2, 2)
            Me.chk_isActive.Margin = New System.Windows.Forms.Padding(0)
            Me.chk_isActive.Name = "chk_isActive"
            Me.chk_isActive.Properties.Caption = "Enable A.C.H. for this client"
            Me.chk_isActive.Size = New System.Drawing.Size(491, 19)
            Me.chk_isActive.StyleController = Me.LayoutControl1
            Me.chk_isActive.TabIndex = 8
            Me.chk_isActive.ToolTip = "If the client is to be on ACH, please check this box. It will enable the ACH information below."
            '
            'lk_CheckingSavings
            '
            Me.lk_CheckingSavings.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lk_CheckingSavings.Location = New System.Drawing.Point(333, 48)
            Me.lk_CheckingSavings.Name = "lk_CheckingSavings"
            Me.lk_CheckingSavings.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.lk_CheckingSavings.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lk_CheckingSavings.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 25, DevExpress.Utils.FormatType.None, String.Empty, False, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 75, DevExpress.Utils.FormatType.None, String.Empty, True, DevExpress.Utils.HorzAlignment.Near)})
            Me.lk_CheckingSavings.Properties.DisplayMember = "description"
            Me.lk_CheckingSavings.Properties.NullText = ""
            Me.lk_CheckingSavings.Properties.ShowHeader = False
            Me.lk_CheckingSavings.Properties.ValueMember = "Id"
            Me.lk_CheckingSavings.Size = New System.Drawing.Size(157, 20)
            Me.lk_CheckingSavings.StyleController = Me.LayoutControl1
            Me.lk_CheckingSavings.TabIndex = 3
            Me.lk_CheckingSavings.ToolTip = "Select either a checking or savings account as appropriate"
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup1.GroupBordersVisible = False
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup2, Me.LayoutControlItem7})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(495, 179)
            Me.LayoutControlGroup1.Spacing = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlGroup2
            '
            Me.LayoutControlGroup2.CustomizationFormText = "Automated Clearing House Information for Funds Transfer"
            Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem6, Me.LayoutControlItem5, Me.LayoutControlItem4, Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem9, Me.LayoutControlItem8, Me.LayoutControlItem10, Me.LayoutControlItem3})
            Me.LayoutControlGroup2.Location = New System.Drawing.Point(0, 23)
            Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
            Me.LayoutControlGroup2.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
            Me.LayoutControlGroup2.Size = New System.Drawing.Size(495, 156)
            Me.LayoutControlGroup2.Text = "Automated Clearing House Information for Funds Transfer"
            '
            'LayoutControlItem6
            '
            Me.LayoutControlItem6.Control = Me.txt_AccountNumber
            Me.LayoutControlItem6.CustomizationFormText = "Account Number"
            Me.LayoutControlItem6.Location = New System.Drawing.Point(245, 48)
            Me.LayoutControlItem6.Name = "LayoutControlItem6"
            Me.LayoutControlItem6.Size = New System.Drawing.Size(244, 24)
            Me.LayoutControlItem6.Text = "Account Number"
            Me.LayoutControlItem6.TextSize = New System.Drawing.Size(79, 13)
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Control = Me.txt_ABA
            Me.LayoutControlItem5.CustomizationFormText = "Routing Number"
            Me.LayoutControlItem5.Location = New System.Drawing.Point(245, 24)
            Me.LayoutControlItem5.Name = "LayoutControlItem5"
            Me.LayoutControlItem5.Size = New System.Drawing.Size(244, 24)
            Me.LayoutControlItem5.Text = "Routing Number"
            Me.LayoutControlItem5.TextSize = New System.Drawing.Size(79, 13)
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.lk_CheckingSavings
            Me.LayoutControlItem4.CustomizationFormText = "Account Type"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(245, 0)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(244, 24)
            Me.LayoutControlItem4.Text = "Account Type"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(79, 13)
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.lbl_PrenoteDate
            Me.LayoutControlItem1.CustomizationFormText = "Prenote Date"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(245, 24)
            Me.LayoutControlItem1.Text = "Prenote Date"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(79, 13)
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.dt_ErrorDate
            Me.LayoutControlItem2.CustomizationFormText = "Error Date"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(245, 24)
            Me.LayoutControlItem2.Text = "Error Date"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(79, 13)
            '
            'LayoutControlItem9
            '
            Me.LayoutControlItem9.Control = Me.dt_EndDate
            Me.LayoutControlItem9.CustomizationFormText = "End Date"
            Me.LayoutControlItem9.Location = New System.Drawing.Point(245, 96)
            Me.LayoutControlItem9.Name = "LayoutControlItem9"
            Me.LayoutControlItem9.Size = New System.Drawing.Size(244, 34)
            Me.LayoutControlItem9.Text = "End Date"
            Me.LayoutControlItem9.TextSize = New System.Drawing.Size(79, 13)
            '
            'LayoutControlItem8
            '
            Me.LayoutControlItem8.Control = Me.dt_ContractDate
            Me.LayoutControlItem8.CustomizationFormText = "Contract Date"
            Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 48)
            Me.LayoutControlItem8.Name = "LayoutControlItem8"
            Me.LayoutControlItem8.Size = New System.Drawing.Size(245, 24)
            Me.LayoutControlItem8.Text = "Contract Date"
            Me.LayoutControlItem8.TextSize = New System.Drawing.Size(79, 13)
            '
            'LayoutControlItem10
            '
            Me.LayoutControlItem10.Control = Me.dt_EnrollDate
            Me.LayoutControlItem10.CustomizationFormText = "Enroll Date"
            Me.LayoutControlItem10.Location = New System.Drawing.Point(0, 72)
            Me.LayoutControlItem10.Name = "LayoutControlItem10"
            Me.LayoutControlItem10.Size = New System.Drawing.Size(245, 58)
            Me.LayoutControlItem10.Text = "Enroll Date"
            Me.LayoutControlItem10.TextSize = New System.Drawing.Size(79, 13)
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.dt_StartDate
            Me.LayoutControlItem3.CustomizationFormText = "Start Date"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(245, 72)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(244, 24)
            Me.LayoutControlItem3.Text = "Start Date"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(79, 13)
            '
            'LayoutControlItem7
            '
            Me.LayoutControlItem7.Control = Me.chk_isActive
            Me.LayoutControlItem7.CustomizationFormText = "LayoutControlItem7"
            Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem7.Name = "LayoutControlItem7"
            Me.LayoutControlItem7.Size = New System.Drawing.Size(495, 23)
            Me.LayoutControlItem7.Text = "LayoutControlItem7"
            Me.LayoutControlItem7.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem7.TextToControlDistance = 0
            Me.LayoutControlItem7.TextVisible = False
            '
            'ACHControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.LayoutControl1)
            Me.Margin = New System.Windows.Forms.Padding(0)
            Me.Name = "ACHControl"
            Me.Size = New System.Drawing.Size(495, 179)
            CType(Me.txt_AccountNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.dt_EnrollDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_EnrollDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_EndDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_EndDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_ContractDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_ContractDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_ErrorDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_ErrorDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txt_ABA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_StartDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_StartDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.chk_isActive.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lk_CheckingSavings.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
        End Sub

        Friend WithEvents txt_AccountNumber As DevExpress.XtraEditors.TextEdit
        Friend WithEvents txt_ABA As DevExpress.XtraEditors.TextEdit
        Friend WithEvents lk_CheckingSavings As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents dt_StartDate As DevExpress.XtraEditors.DateEdit
        Friend WithEvents dt_ErrorDate As DevExpress.XtraEditors.DateEdit
        Friend WithEvents lbl_PrenoteDate As DevExpress.XtraEditors.LabelControl
        Friend WithEvents chk_isActive As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents dt_ContractDate As DevExpress.XtraEditors.DateEdit
        Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents dt_EndDate As DevExpress.XtraEditors.DateEdit
        Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents dt_EnrollDate As DevExpress.XtraEditors.DateEdit
        Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    End Class
End Namespace