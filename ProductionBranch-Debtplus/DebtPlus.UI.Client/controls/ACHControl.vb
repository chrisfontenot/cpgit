#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DevExpress.XtraEditors.Controls
Imports DevExpress.Utils
Imports System.Text.RegularExpressions
Imports System.Linq
Imports DebtPlus.LINQ

Namespace controls
    Friend Class ACHControl
        Implements System.ComponentModel.INotifyPropertyChanged

        ''' <summary>
        ''' Implement the property changed to reflect that the ACH data record was modified
        ''' </summary>
        Public Event PropertyChanged As System.ComponentModel.PropertyChangedEventHandler Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged
        Protected Sub RaisePropertyChanged(e As System.ComponentModel.PropertyChangedEventArgs)
            RaiseEvent PropertyChanged(Me, e)
        End Sub

        Protected Overridable Sub OnPropertyChanged(e As System.ComponentModel.PropertyChangedEventArgs)
            RaisePropertyChanged(e)
        End Sub

        ' Should we clear the prenote status?
        Private MustPrenote As Boolean

        ''' <summary>
        ''' Initialize the storage for the control class
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler txt_ABA.Validating, AddressOf txt_ach_routing_number_Validating
            AddHandler chk_isActive.CheckStateChanged, AddressOf chk_ach_active_CheckStateChanged
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler txt_ABA.Validating, AddressOf txt_ach_routing_number_Validating
            RemoveHandler chk_isActive.CheckStateChanged, AddressOf chk_ach_active_CheckStateChanged
        End Sub

        ''' <summary>
        ''' Process the initial loading of the control. We do this only once, when the page is created.
        ''' </summary>
        ''' <param name="bc"></param>
        ''' <remarks></remarks>
        Public Overloads Sub LoadForm(bc As DebtPlus.LINQ.BusinessContext)

            ' Initialize for the edit operation
            LayoutControlGroup2.Enabled = False
            lk_CheckingSavings.Properties.DataSource = DebtPlus.LINQ.InMemory.ACHAccountTypes.getList()
        End Sub

        ''' <summary>
        ''' Read the ACH table
        ''' </summary>
        Public Overrides Sub ReadForm(bc As DebtPlus.LINQ.BusinessContext)

            UnRegisterHandlers()
            Try

                ' If there is a record then use it.
                If Context.ClientDs.clientACHRecord IsNot Nothing Then
                    LoadControls(Context.ClientDs.clientACHRecord)
                Else
                    ' There is no record. Create a new item and use the defaults from it.
                    Dim record As client_ach = DebtPlus.LINQ.Factory.Manufacture_client_ach()
                    LoadControls(record)
                End If

            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Load the edit controls from the current record.
        ''' </summary>
        Private Sub LoadControls(record As client_ach)

            txt_AccountNumber.EditValue = record.AccountNumber
            txt_ABA.EditValue = record.ABA
            dt_ErrorDate.EditValue = record.ErrorDate
            dt_StartDate.EditValue = record.StartDate
            dt_EndDate.EditValue = record.EndDate
            dt_ContractDate.EditValue = record.ContractDate
            dt_EnrollDate.EditValue = record.EnrollDate
            lk_CheckingSavings.EditValue = record.CheckingSavings
            lbl_PrenoteDate.Text = If(record.PrenoteDate.HasValue, record.PrenoteDate.Value.ToShortDateString(), "NOT PRENOTED")

            ' Enable the other controls if the checkbox is checked
            chk_isActive.EditValue = record.isActive
            LayoutControlGroup2.Enabled = record.isActive
        End Sub

        ''' <summary>
        ''' Save the ACH table
        ''' </summary>
        Public Overrides Sub SaveForm(bc As DebtPlus.LINQ.BusinessContext)

            ' If there is a record then try to update it.
            If Context.ClientDs.clientACHRecord IsNot Nothing Then
                AddHandler Context.ClientDs.clientACHRecord.PropertyChanged, AddressOf LookForPrenote
                MustPrenote = False
                SaveItems(Context.ClientDs.clientACHRecord)
                RemoveHandler Context.ClientDs.clientACHRecord.PropertyChanged, AddressOf LookForPrenote

                ' If we need to do the prenote operation again because a critical field was changed then
                ' remove the date that the prenote was performed. This will force the prenote out again.
                If MustPrenote Then
                    Context.ClientDs.clientACHRecord.PrenoteDate = Nothing
                End If

                ' Generate the update note if required
                If Context.ClientDs.clientACHRecord.getChangedFieldItems() IsNot Nothing Then
                    GenerateSystemNote(bc, Context.ClientDs.clientACHRecord.getChangedFieldItems())

                    Try
                        bc.SubmitChanges()
                        RaisePropertyChanged(New System.ComponentModel.PropertyChangedEventArgs("client_ach"))

                    Catch ex As System.Data.SqlClient.SqlException
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating client_ach table")
                    End Try
                End If

            Else

                ' There is no ACH record. If the ACH is not active then leave it as such without a record.
                If Not chk_isActive.Checked Then
                    Return
                End If

                ' Manufacture the new ach record
                Dim record As client_ach = DebtPlus.LINQ.Factory.Manufacture_client_ach()
                record.Id = Context.ClientDs.ClientId
                record.PrenoteDate = Nothing
                SaveItems(record)

                ' Generate the update note if required
                GenerateSystemNote(bc, record.getChangedFieldItems())

                Try
                    bc.client_aches.InsertOnSubmit(record)
                    bc.SubmitChanges()
                    Context.ClientDs.clientACHRecord = record
                    RaisePropertyChanged(New System.ComponentModel.PropertyChangedEventArgs("client_ach"))

                Catch ex As System.Data.SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error inserting into client_ach table")
                End Try
            End If
        End Sub

        ''' <summary>
        ''' Save the edit controls into the passed record.
        ''' </summary>
        Private Sub SaveItems(ByRef record As client_ach)
            record.AccountNumber = DebtPlus.Utils.Nulls.v_String(txt_AccountNumber.EditValue)
            record.ABA = DebtPlus.Utils.Nulls.v_String(txt_ABA.EditValue)
            record.StartDate = Convert.ToDateTime(dt_StartDate.EditValue)
            record.EnrollDate = Convert.ToDateTime(dt_EnrollDate.EditValue)
            record.ErrorDate = DebtPlus.Utils.Nulls.v_DateTime(dt_ErrorDate.EditValue)
            record.EndDate = DebtPlus.Utils.Nulls.v_DateTime(dt_EndDate.EditValue)
            record.ContractDate = DebtPlus.Utils.Nulls.v_DateTime(dt_ContractDate.EditValue)
            record.CheckingSavings = Convert.ToChar(lk_CheckingSavings.EditValue)
            record.isActive = chk_isActive.Checked
        End Sub

        ''' <summary>
        ''' Determine if we need to force the prenote operation out again. It must be prenoted
        ''' if we change the keys to the account number, such as the ABA or the account number or
        ''' the checking/savings flag.
        ''' </summary>
        Private Sub LookForPrenote(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)

            ' If the key to the account changes then we need to prenote the account again.
            If (New String() {"ABA", "AccountNumber", "CheckingSavings"}).Contains(e.PropertyName, StringComparer.InvariantCultureIgnoreCase) Then
                MustPrenote = True
            End If
        End Sub

        ''' <summary>
        ''' Enable or disable the ACH information based upon the ACH flag
        ''' </summary>
        Private Sub chk_ach_active_CheckStateChanged(ByVal sender As Object, ByVal e As EventArgs)
            LayoutControlGroup2.Enabled = chk_isActive.Checked
        End Sub

        ''' <summary>
        ''' Validate the ABA routing number
        ''' </summary>
        Private Sub txt_ach_routing_number_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
            Dim ErrorString As String = String.Empty
            Dim StringValue As String = DebtPlus.Utils.Nulls.DStr(txt_ABA.EditValue)

            If StringValue <> String.Empty Then
                If Not Regex.IsMatch(StringValue, "^[0-9]{9}$") Then
                    ErrorString = "value must be 9 digits"
                Else
                    Dim TotalValue As Int32 = 0
                    Dim Factor() As Int32 = New Int32() {3, 7, 1, 3, 7, 1, 3, 7, 1}
                    For Idx As Int32 = 0 To 8
                        Dim DigitValue As Int32 = Convert.ToInt32(StringValue.Chars(Idx)) - 48
                        TotalValue += (DigitValue * Factor(Idx))
                    Next

                    ' The sum should be an even multiple of 10
                    If (TotalValue Mod 10) <> 0 Then
                        ErrorString = "invalid checksum"
                    End If
                End If
            End If

            txt_ABA.ErrorText = ErrorString
            e.Cancel = ErrorString <> String.Empty
        End Sub

        ''' <summary>
        ''' Generate the system note with the changed field
        ''' </summary>
        ''' <param name="colChanges">Collection of changed fields in the people table</param>
        ''' <remarks></remarks>
        Private Sub GenerateSystemNote(bc As DebtPlus.LINQ.BusinessContext, colChanges As System.Collections.Generic.List(Of DebtPlus.LINQ.ChangedFieldItem))

            ' No changed fields are easy to handle here.
            If colChanges.Count < 1 Then
                Return
            End If

            Dim noteRecord As client_note = DebtPlus.LINQ.Factory.Manufacture_client_note()
            noteRecord.client = Context.ClientDs.ClientId
            noteRecord.type = 3

            ' Get a list of the fields that we are changing
            Dim sbNote As New System.Text.StringBuilder()
            colChanges.ForEach(Sub(s) sbNote.Append(FormatNoteField(s)))
            noteRecord.note = sbNote.ToString()

            Dim subjectText As String = String.Format("Changed ACH {0}", String.Join(",", colChanges.Select(Function(s) s.FieldName)))
            If subjectText.Length > 77 Then
                subjectText = subjectText.Substring(0, 77) + "..."
            End If
            noteRecord.subject = subjectText

            bc.client_notes.InsertOnSubmit(noteRecord)
            Context.ClientDs.clientACHRecord.ClearChangedFieldItems()
        End Sub

        ''' <summary>
        ''' Routine to translate the code values in the people record to suitable text strings.
        ''' This is conversion routine to convert the changes in the person record to a system note.
        ''' </summary>
        ''' <param name="item">Pointer to the field changed event arguments</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function FormatNoteField(item As DebtPlus.LINQ.ChangedFieldItem) As String

            ' Translate the fields which have code values to the suitable string. All others use the standard format routine.
            Select Case item.FieldName
                Case "CheckingSavings"
                    Return Helpers.FormatString(item.FieldName, Helpers.GetACHAccountTypeText(DirectCast(item.OldValue, Char)), Helpers.GetACHAccountTypeText(DirectCast(item.NewValue, Char)))
                Case Else
                    ' Fall out of the select statement to return the standard formatting routine.
            End Select

            ' Simply punt and record the item as a string. This works for strings, boolean and simple numbers
            Return Helpers.FormatString(item.FieldName, item.OldValue, item.NewValue)
        End Function
    End Class
End Namespace
