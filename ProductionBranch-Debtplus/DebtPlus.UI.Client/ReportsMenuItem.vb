#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Interfaces.Client
Imports DevExpress.XtraBars
Imports DebtPlus.UI.Client.Service
Imports DebtPlus.Reports

''' <summary>
''' Menu items for the REPORTS menu
''' </summary>
Friend Class ReportsMenuItem
    Inherits BarButtonItem
    Implements IContext

    Protected ClassName As String = String.Empty
    Protected Argument As String = String.Empty

    ''' <summary>
    ''' Indicate that the report is completed
    ''' </summary>
    Public Event Complete As EventHandler

    Protected Sub RaiseComplete()
        RaiseEvent Complete(Me, EventArgs.Empty)
    End Sub

    ''' <summary>
    ''' Context information for the volatile local storage
    ''' </summary>
    Private privateContext As ClientUpdateClass
    Public ReadOnly Property Context() As ClientUpdateClass Implements IContext.Context
        Get
            Return privateContext
        End Get
    End Property

    Public Sub SetContext(ByVal Value As ClientUpdateClass)
        privateContext = Value
    End Sub

    Public Sub New()
        MyBase.New()
    End Sub

    Sub New(ByVal text As String)
        MyClass.New()
        Caption = text
    End Sub

    Sub New(ByVal text As String, ByVal OnClickHandler As ItemClickEventHandler)
        MyClass.New(text)
        AddHandler ItemClick, OnClickHandler
    End Sub

    Sub New(ByVal text As String, ByVal OnClickHandler As ItemClickEventHandler, ByVal Shortcut As Shortcut)
        MyClass.New(text, OnClickHandler)
        ItemShortcut = New BarShortcut(Shortcut)
    End Sub

    Public Sub New(ByVal ContextInfo As ClientUpdateClass, ByVal Name As String, ByVal rpt As DebtPlus.LINQ.report)
        MyClass.New(Name)
        SetContext(ContextInfo)

        ClassName = rpt.ClassName
        Argument = rpt.Argument
    End Sub

    ''' <summary>
    ''' Handle the CLICK event
    ''' </summary>
    Protected Overrides Sub OnClick(ByVal link As BarItemLink)
        MyBase.OnClick(link)

        ' Run the report at this time.
        Show()
    End Sub

    ''' <summary>
    ''' Start the report running
    ''' </summary>
    Protected Overridable Sub Show()
        ' If there is a class then start the thread to process the report
        Dim thrd As New System.Threading.Thread(AddressOf ReportThread)
        thrd.SetApartmentState(Threading.ApartmentState.STA)
        thrd.IsBackground = True
        thrd.Start()
    End Sub

    ''' <summary>
    ''' Thread processing for the report
    ''' </summary>
    Protected Overridable Sub ReportThread()
        If ClassName <> String.Empty Then
            Dim rpt As DebtPlus.Interfaces.Reports.IReports = DebtPlus.Reports.ReportLoader.LoadReport(ClassName, Argument)
            If rpt IsNot Nothing Then

                ' Set the client information into the report
                Dim paramType As IClient = TryCast(rpt, IClient)
                If paramType IsNot Nothing Then
                    paramType.ClientId = Context.ClientId
                End If

                ' Run the report at this point.
                If rpt.RequestReportParameters() = DialogResult.OK Then
                    rpt.DisplayPreviewDialog()
                    RaiseComplete()
                End If
            End If
        End If
    End Sub
End Class
