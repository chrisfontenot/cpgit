﻿Imports System
Imports System.Windows.Forms
Imports DebtPlus.LINQ
Imports System.Linq

Friend Module Helpers

    ''' <summary>
    ''' Routine to format a string to the proper text for a system note.
    ''' </summary>
    ''' <param name="fieldname">Name of the field</param>
    ''' <param name="oldValue">Value for the old item</param>
    ''' <param name="newvalue">Value for the new item</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Friend Function FormatString(fieldname As String, oldValue As Object, newvalue As Object) As String
        Return String.Format("Changed {0} from ""{1}"" to ""{2}""{3}", fieldname, getStringFormat(oldValue), getStringFormat(newvalue), System.Environment.NewLine)
    End Function

    ''' <summary>
    ''' Helping function to convert the object to a suitable string. This is a private function.
    ''' Please do not change it to be externally accessible. It is only called from the FormatString
    ''' function.
    ''' </summary>
    ''' <param name="value">Value to be converted</param>
    ''' <returns>The suitable string representation of the value</returns>
    ''' <remarks></remarks>
    Private Function getStringFormat(value As Object) As String

        ' A null value is empty
        If value Is Nothing Then
            Return String.Empty
        End If

        ' A decimal value is currency
        If TypeOf value Is Decimal Then
            Return String.Format("{0:c}", value)
        End If

        ' A date value does not have the time.
        If TypeOf value Is DateTime Then
            Return String.Format("{0:d}", value)
        End If

        ' All others are simply the suitable string value
        Return value.ToString()
    End Function

    ''' <summary>
    ''' Get the retention event description
    ''' </summary>
    ''' <param name="Key">ID to the retention event table</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Friend Function getRetentionEventText(Key As System.Nullable(Of System.Int32)) As String
        If Key.HasValue Then
            Dim q As DebtPlus.LINQ.retention_event = DebtPlus.LINQ.Cache.retention_event.getList().Find(Function(s) s.Id = Key.Value)
            If q IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(q.description) Then
                Return q.description
            End If
        End If

        Return String.Empty
    End Function

    ''' <summary>
    ''' Get the retention action description
    ''' </summary>
    ''' <param name="Key">ID to the retention action table</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Friend Function getRetentionActionText(Key As System.Nullable(Of System.Int32)) As String
        If Key.HasValue Then
            Dim q As DebtPlus.LINQ.retention_action = DebtPlus.LINQ.Cache.retention_action.getList().Find(Function(s) s.Id = Key.Value)
            If q IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(q.description) Then
                Return q.description
            End If
        End If

        Return String.Empty
    End Function

    ''' <summary>
    ''' Get the appropriate display text for the ACHAccountType. The is "C" for Checking and "S" for Savings.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Friend Function GetACHAccountTypeText(Key As System.Nullable(Of Char)) As String
        If Key.HasValue Then
            Dim q As DebtPlus.LINQ.InMemory.ACHAccountType = DebtPlus.LINQ.InMemory.ACHAccountTypes.getList().Find(Function(s) s.Id = Key.Value)
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    ''' <summary>
    ''' Get the appropriate display text for the military status indication
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Friend Function GetMilitaryStatusText(Key As System.Nullable(Of Int32)) As String
        If Key.HasValue Then
            Dim q As DebtPlus.LINQ.militaryStatusType = DebtPlus.LINQ.Cache.MilitaryStatusType.getList().Find(Function(s) s.Id = Key.Value)
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    ''' <summary>
    ''' Get the appropriate display text for the military grade indication
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Friend Function GetMilitaryGradeText(Key As System.Nullable(Of Int32)) As String
        If Key.HasValue Then
            Dim q As DebtPlus.LINQ.militaryGradeType = DebtPlus.LINQ.Cache.MilitaryGradeType.getList().Find(Function(s) s.Id = Key.Value)
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    ''' <summary>
    ''' Get the appropriate display text for the military service indication
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Friend Function GetMilitaryServiceText(Key As System.Nullable(Of Int32)) As String
        If Key.HasValue Then
            Dim q As DebtPlus.LINQ.militaryServiceType = DebtPlus.LINQ.Cache.MilitaryServiceType.getList().Find(Function(s) s.Id = Key.Value)
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    ''' <summary>
    ''' Get the appropriate display text for the military service indication
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Friend Function GetMilitaryDependentText(Key As System.Nullable(Of Int32)) As String
        If Key.HasValue Then
            Dim q As DebtPlus.LINQ.militaryDependentType = DebtPlus.LINQ.Cache.MilitaryDependentType.getList().Find(Function(s) s.Id = Key.Value)
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    ''' <summary>
    ''' Get the appropriate display text for the No FICO Score Reason indication
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Friend Function GetNoFicoScoreReasonText(Key As System.Nullable(Of Int32)) As String
        If Key.HasValue Then
            Dim q As DebtPlus.LINQ.Housing_FICONotIncludedReason = DebtPlus.LINQ.Cache.Housing_FICONotIncludedReason.getList().Find(Function(s) s.Id = Key.Value)
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    ''' <summary>
    ''' Get the appropriate display text for the BankruptcyChapter indication
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Friend Function GetBankruptcyClassTypeText(Key As System.Nullable(Of Int32)) As String
        If Key.HasValue Then
            Dim q As DebtPlus.LINQ.BankruptcyClassType = DebtPlus.LINQ.Cache.BankruptcyClassType.getList().Find(Function(s) s.Id = Key.Value)
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    ''' <summary>
    ''' Get the appropriate display text for the BankruptcyDistrict indication
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Friend Function GetBankruptcyDistrictText(Key As System.Nullable(Of Int32)) As String
        If Key.HasValue Then
            Dim q As DebtPlus.LINQ.bankruptcy_district = DebtPlus.LINQ.Cache.bankruptcy_district.getList().Find(Function(s) s.Id = Key.Value)
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Friend Function GetPOA_ApprovalText(key As System.Nullable(Of System.Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.poa_lookup = DebtPlus.LINQ.Cache.poa_lookup.getList().Find(Function(s) s.Id = key.Value)
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Friend Function GetPOATypeText(key As System.Nullable(Of System.Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.poa_type = DebtPlus.LINQ.Cache.poa_type.getList().Find(Function(s) s.Id = key.Value)
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return Nothing
    End Function

    Friend Function GetPOA_lookupText(key As System.Nullable(Of System.Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.poa_lookup = DebtPlus.LINQ.Cache.poa_lookup.getList().Find(Function(s) s.Id = key.Value)
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return Nothing
    End Function

    Friend Function GetValidInvalidText(key As System.Nullable(Of System.Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.InMemory.validInvalidType = DebtPlus.LINQ.InMemory.validInvalidTypes.getList().Find(Function(s) s.Id = key.Value)
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return Nothing
    End Function

    Friend Function GetTelephoneTypeText(key As System.Nullable(Of System.Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.TelephoneType = DebtPlus.LINQ.Cache.TelephoneType.getList().Find(Function(s) s.Id = key.Value)
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return Nothing
    End Function

    Friend Function GetPOA_StageText(key As System.Nullable(Of System.Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.poa_lookup = DebtPlus.LINQ.Cache.poa_lookup.getList().Find(Function(s) s.Id = key.Value)
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return Nothing
    End Function

    Friend Function GetSecuredTypeText(key As System.Nullable(Of System.Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.secured_type = DebtPlus.LINQ.Cache.secured_type.getList().Find(Function(s) s.Id = key.Value)
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return Nothing
    End Function

    Friend Function GetTicklerTypeText(key As System.Nullable(Of System.Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.TicklerType = DebtPlus.LINQ.Cache.TicklerType.getList().Find(Function(s) s.Id = key.Value)
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return Nothing
    End Function

    Friend Function GetCounselorNameText(key As System.Nullable(Of System.Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.counselor = DebtPlus.LINQ.Cache.counselor.getList().Find(Function(s) s.Id = key.Value)
            If q IsNot Nothing AndAlso q.Name IsNot Nothing Then
                Return q.Name.ToString()
            End If
        End If
        Return Nothing
    End Function

    Friend Function GetConfigFeeText(key As System.Nullable(Of System.Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.config_fee = DebtPlus.LINQ.Cache.config_fee.getList().Find(Function(s) s.Id = key.Value)
            If q IsNot Nothing AndAlso q.description IsNot Nothing Then
                Return q.description
            End If
        End If
        Return Nothing
    End Function

    Friend Function GetContactMethodText(key As System.Nullable(Of System.Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.ContactMethodType = DebtPlus.LINQ.Cache.ContactMethodType.getList().Find(Function(s) s.Id = key.Value)
            If q IsNot Nothing AndAlso q.description IsNot Nothing Then
                Return q.description
            End If
        End If
        Return Nothing
    End Function

    Friend Function GetProgramParticipationText(key As System.Nullable(Of System.Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.ProgramParticipationType = DebtPlus.LINQ.Cache.ProgramParticipationType.getList().Find(Function(s) s.Id = key.Value)
            If q IsNot Nothing AndAlso q.Description IsNot Nothing Then
                Return q.Description
            End If
        End If
        Return Nothing
    End Function

    Friend Function GetReferredByText(key As System.Nullable(Of System.Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.referred_by = DebtPlus.LINQ.Cache.referred_by.getList().Find(Function(s) s.Id = key.Value)
            If q IsNot Nothing AndAlso q.description IsNot Nothing Then
                Return q.description
            End If
        End If
        Return Nothing
    End Function









    Public Function GetDisabledText(ByVal key As System.Nullable(Of Int32)) As String
        If key.HasValue Then
            If key.Value = 0 Then
                Return System.Boolean.FalseString
            End If
            Return System.Boolean.TrueString
        End If
        Return String.Empty
    End Function

    Public Function GetCounselorText(ByVal key As System.Nullable(Of Int32)) As String

        If key.HasValue Then
            Dim q As DebtPlus.LINQ.counselor = DebtPlus.LINQ.Cache.counselor.getList().Find(Function(s) s.Id = key.Value)
            If q IsNot Nothing Then
                Return q.Name.ToString()
            End If
        End If

        Return String.Empty
    End Function

    Public Function GetCountyText(ByVal key As System.Nullable(Of Int32)) As String

        If key.HasValue Then
            Dim q As DebtPlus.LINQ.county = DebtPlus.LINQ.Cache.county.getList().Find(Function(s) s.Id = key.Value)
            If q IsNot Nothing Then
                Return q.name
            End If
        End If

        Return String.Empty
    End Function

    Public Function GetCreditAgencyText(ByVal key As String) As String

        If Not String.IsNullOrWhiteSpace(key) Then
            Dim q As DebtPlus.LINQ.InMemory.CreditAgencyType = DebtPlus.LINQ.InMemory.CreditAgencyTypes.getList().Find(Function(s) String.Compare(key, s.Id, True) = 0)
            If q IsNot Nothing Then
                Return q.description
            End If
        End If

        Return String.Empty
    End Function

    Public Function GetLoanDelinquencyMonthsText(ByVal key As System.Nullable(Of Int32)) As String

        If key.HasValue Then
            If key.Value = 0 Then
                Return "Current"
            End If

            If key.Value = 1 Then
                Return "Struggling"
            End If

            If key.Value > 0 Then
                Return String.Format("{0:n0}", key.Value)
            End If
        End If

        Return String.Empty
    End Function

    Public Function GetHousingFinancingText(ByVal key As System.Nullable(Of Int32)) As String

        If key.HasValue Then
            Dim q As DebtPlus.LINQ.Housing_FinancingType = DebtPlus.LINQ.Cache.Housing_FinancingType.getList().Find(Function(s) s.Id = key.Value)
            If q IsNot Nothing Then
                Return q.description
            End If
        End If

        Return String.Empty
    End Function

    Public Function GetHousingLoanPositionText(ByVal key As System.Nullable(Of Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.Housing_LoanPositionType = DebtPlus.LINQ.Cache.Housing_LoanPositionType.getList().Find(Function(s) s.Id = key.Value)
            If q IsNot Nothing Then
                Return q.description
            End If
        End If

        Return String.Empty
    End Function

    Public Function GetHousingResidencyText(ByVal key As System.Nullable(Of Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.Housing_ResidencyType = DebtPlus.LINQ.Cache.Housing_ResidencyType.getList().Find(Function(s) s.Id = key.Value)
            If q IsNot Nothing Then
                Return q.description
            End If
        End If

        Return String.Empty
    End Function

    Public Function GetStateMessageTypeText(ByVal key As System.Nullable(Of Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.StateMessageType = DebtPlus.LINQ.Cache.StateMesageType.getList().Find(Function(s) s.Id = key.Value)
            If q IsNot Nothing Then
                Return q.Description
            End If
        End If

        Return String.Empty
    End Function

    Public Function GetOfficeText(ByVal key As System.Nullable(Of Int32)) As String

        If key.HasValue Then
            Dim q As DebtPlus.LINQ.office = DebtPlus.LINQ.Cache.office.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.name
            End If
        End If

        Return String.Empty
    End Function

    Public Function GetDropReasonText(ByVal key As System.Nullable(Of Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.drop_reason = DebtPlus.LINQ.Cache.drop_reason.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GetEducationText(ByVal key As System.Nullable(Of Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.EducationType = DebtPlus.LINQ.Cache.EducationType.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GetemployerText(ByVal key As System.Nullable(Of Int32)) As String

        If key.HasValue AndAlso key.Value > 0 Then
            Using bc As New BusinessContext()
                Dim q As DebtPlus.LINQ.employer = bc.employers.Where(Function(s) s.Id = key.Value).FirstOrDefault()
                If q IsNot Nothing Then
                    Return q.name
                End If
            End Using
        End If

        Return String.Empty
    End Function

    Public Function GetEthnicityText(ByVal key As System.Nullable(Of Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.EthnicityType = DebtPlus.LINQ.Cache.EthnicityType.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GetGenderText(ByVal key As System.Nullable(Of Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.GenderType = DebtPlus.LINQ.Cache.GenderType.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GetFinancialProblemText(ByVal key As System.Nullable(Of Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.financial_problem = DebtPlus.LINQ.Cache.financial_problemType.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GetHouseholdText(ByVal key As System.Nullable(Of Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.HouseholdHeadType = DebtPlus.LINQ.Cache.HouseholdHeadType.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GetHousingGrantText(ByVal key As System.Nullable(Of Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.Housing_GrantType = DebtPlus.LINQ.Cache.Housing_GrantType.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GetHUDassistance_Text(ByVal key As System.Nullable(Of Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.Housing_HUDAssistanceType = DebtPlus.LINQ.Cache.Housing_HUDAssistanceType.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function Getno_fico_score_reasonText(ByVal key As System.Nullable(Of Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.Housing_FICONotIncludedReason = DebtPlus.LINQ.Cache.Housing_FICONotIncludedReason.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GetHousingLoanText(ByVal key As System.Nullable(Of Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.Housing_LoanType = DebtPlus.LINQ.Cache.Housing_LoanType.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GetHousingMortgageText(ByVal key As System.Nullable(Of Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.Housing_MortgageType = DebtPlus.LINQ.Cache.Housing_MortgageType.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GetHousingTypeText(ByVal key As System.Nullable(Of Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.HousingType = DebtPlus.LINQ.Cache.HousingType.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GetJobText(ByVal key As System.Nullable(Of Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.job_description = DebtPlus.LINQ.Cache.job_description.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GetLanguageText(ByVal key As System.Nullable(Of Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.AttributeType = DebtPlus.LINQ.Cache.AttributeType.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GetMarital_statusText(ByVal key As System.Nullable(Of Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.MaritalType = DebtPlus.LINQ.Cache.MaritalType.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GetFrequencyText(ByVal key As System.Nullable(Of Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.PayFrequencyType = DebtPlus.LINQ.Cache.PayFrequencyType.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GetRaceText(ByVal key As System.Nullable(Of Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.RaceType = DebtPlus.LINQ.Cache.RaceType.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function Getreferred_byText(ByVal key As System.Nullable(Of Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.referred_by = DebtPlus.LINQ.Cache.referred_by.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GetRegionText(ByVal key As System.Nullable(Of Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.region = DebtPlus.LINQ.Cache.region.getList().Find(Function(s) s.Id = Convert.ToInt32(key))
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GetRelationText(ByVal key As System.Nullable(Of Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.RelationType = DebtPlus.LINQ.Cache.RelationType.getList().Find(Function(s) s.Id = key.Value)
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GetTriStateText(ByVal key As System.Nullable(Of Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.InMemory.TristateType = DebtPlus.LINQ.InMemory.TristateTypes.getList().Find(Function(s) s.Id = key.Value)
            If q IsNot Nothing Then
                Return q.description
            End If
        End If
        Return String.Empty
    End Function

    Public Function GetSSNText(ByVal key As String) As String
        Return DebtPlus.Utils.Format.SSN.FormatSSN(key)
    End Function

    Public Function GetMilitaryStatusIDText(ByVal key As System.Nullable(Of Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.militaryStatusType = DebtPlus.LINQ.Cache.MilitaryStatusType.getList().Find(Function(s) s.Id = key.Value)
            If q IsNot Nothing Then
                Return q.description
            End If
        End If

        Return String.Empty
    End Function

    Public Function GetMilitaryServiceIDText(ByVal key As System.Nullable(Of Int32)) As String

        If key.HasValue Then
            Dim q As DebtPlus.LINQ.militaryServiceType = DebtPlus.LINQ.Cache.MilitaryServiceType.getList().Find(Function(s) s.Id = key.Value)
            If q IsNot Nothing Then
                Return q.description
            End If
        End If

        Return String.Empty
    End Function

    Public Function GetMilitaryDependentIDText(ByVal key As System.Nullable(Of Int32)) As String

        If key.HasValue Then
            Dim q As DebtPlus.LINQ.militaryDependentType = DebtPlus.LINQ.Cache.MilitaryDependentType.getList().Find(Function(s) s.Id = key.Value)
            If q IsNot Nothing Then
                Return q.description
            End If
        End If

        Return String.Empty
    End Function

    Public Function GetMilitaryGradeIDText(ByVal key As System.Nullable(Of Int32)) As String

        If key.HasValue Then
            Dim q As DebtPlus.LINQ.militaryGradeType = DebtPlus.LINQ.Cache.MilitaryGradeType.getList().Find(Function(s) s.Id = key.Value)
            If q IsNot Nothing Then
                Return q.description
            End If
        End If

        Return String.Empty
    End Function

    Public Function GetSatisfaction_scoreText(ByVal key As System.Nullable(Of Int32)) As String

        If key.HasValue Then
            Dim q As DebtPlus.LINQ.InMemory.SatisfactionScoreType = DebtPlus.LINQ.InMemory.SatisfactionScoreTypes.getList().Find(Function(s) s.Id = key.Value)
            If q IsNot Nothing Then
                Return q.description.Trim() ' We purposely put leading blanks in the description for sorting reasons.
            End If

            Return key.ToString()
        End If

        Return String.Empty
    End Function

    Public Function GetEscrowProBonoText(ByVal key As System.Nullable(Of System.Int32)) As String
        If key.HasValue Then
            Dim q As DebtPlus.LINQ.InMemory.EscrowProBonoType = DebtPlus.LINQ.InMemory.EscrowProBonoTypes.getList().Find(Function(s) s.Id = key.Value)
            If q IsNot Nothing Then
                Return q.description
            End If
        End If

        Return String.Empty
    End Function
End Module