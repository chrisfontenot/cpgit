#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DevExpress.XtraBars
Imports DebtPlus.UI.Client.Service
Imports System.Threading
Imports DebtPlus.Interfaces

''' <summary>
''' Menu items for the LETTERS menu
''' </summary>
Friend Class LettersMenuItem
    Inherits BarButtonItem
    Implements IContext

    ''' <summary>
    ''' Context information for the volatile local storage
    ''' </summary>
    Private privateContext As ClientUpdateClass
    Private letter_code As String
    Private Parent As IForm

    Public ReadOnly Property Context() As ClientUpdateClass Implements IContext.Context
        Get
            Return privateContext
        End Get
    End Property

    Public Sub SetContext(ByVal Value As ClientUpdateClass)
        privateContext = Value
    End Sub

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByVal text As String)
        MyClass.New()
        Caption = text
    End Sub

    Public Sub New(ByVal text As String, ByVal OnClickHandler As ItemClickEventHandler)
        MyClass.New(text)
        AddHandler ItemClick, OnClickHandler
    End Sub

    Public Sub New(ByVal text As String, ByVal OnClickHandler As ItemClickEventHandler, ByVal Shortcut As Shortcut)
        MyClass.New(text, OnClickHandler)
        Me.ItemShortcut = New BarShortcut(Shortcut)
    End Sub

    Public Sub New(ByRef Parent As IForm, ByRef ContextInfo As ClientUpdateClass, ByVal Name As String, ByVal letter As DebtPlus.LINQ.letter_type)
        MyClass.New(Name)
        SetContext(ContextInfo)
        Me.Parent = Parent
        Me.letter_code = letter.letter_code
    End Sub

    ''' <summary>
    ''' Handle the CLICK event
    ''' </summary>
    Protected Overrides Sub OnClick(ByVal link As BarItemLink)
        MyBase.OnClick(link)

        ' If there is a letter_code then display the letter.
        If Not String.IsNullOrEmpty(letter_code) Then

            ' Start a thread to process the letter and the class. Just let it run.
            Dim letter_class As New ClientLetterThread(Context, letter_code, Nothing)
            Dim thrd As New Thread(AddressOf letter_class.LetterThread)
            With thrd
                .Name = "ClientLetterThread"
                .SetApartmentState(ApartmentState.STA)
                .IsBackground = True
                .Start()
            End With
        End If
    End Sub
End Class
