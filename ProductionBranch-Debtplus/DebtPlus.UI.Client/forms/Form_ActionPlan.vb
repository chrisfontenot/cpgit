#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DebtPlus.UI.Common
Imports DevExpress.XtraRichEdit.Commands
Imports DebtPlus.Data.Controls
Imports DebtPlus.UI.Client.Service
Imports DebtPlus.Utils
Imports DebtPlus.LINQ
Imports System.Linq
Imports System.Data.SqlClient
Imports DevExpress.XtraRichEdit
Imports DevExpress.XtraEditors
Imports DevExpress.XtraBars
Imports DevExpress.XtraLayout
Imports DevExpress.XtraRichEdit.SpellChecker
Imports DevExpress.XtraSpellChecker.Native
Imports DebtPlus.Data.Forms
Imports System.IO
Imports DevExpress.Utils
Imports DevExpress.Utils.Menu
Imports Strings = DebtPlus.Utils.Format.Strings

Namespace forms

    Friend Class Form_ActionPlan

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        ' Item collections for the current plan information
        Dim actionPlanRecord As action_plan = Nothing
        Dim bc As BusinessContext = Nothing

        ' Pieces for the various types of processing
        Dim GoalsClass As Goals = Nothing
        Dim ToDoClass As ItemsBase = Nothing
        Dim IncomeClass As ItemsBase = Nothing
        Dim ExpensesClass As ItemsBase = Nothing
        Dim RequiredClass As ItemsBase = Nothing

        ''' <summary>
        ''' Initialize the form
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler SimpleButton_OK.Click, AddressOf Button_OK_Click
            AddHandler BarButtonItem_File_QuickPrint.ItemClick, AddressOf BarButtonItem_File_Print_ItemClick
            AddHandler BarButtonItem_File_Print.ItemClick, AddressOf BarButtonItem_File_PrintDialog_ItemClick
            AddHandler BarButtonItem_File_PrintPreview.ItemClick, AddressOf BarButtonItem_File_PrintPreview_ItemClick
            AddHandler BarButtonItem_Edit_Undo.ItemClick, AddressOf BarButtonItem_Edit_Undo_ItemClick
            AddHandler BarButtonItem_Edit_Redo.ItemClick, AddressOf BarButtonItem_Edit_Redo_ItemClick
            AddHandler BarButtonItem_Edit_Cut.ItemClick, AddressOf BarButtonItem_Edit_Cut_ItemClick
            AddHandler BarButtonItem_Edit_Copy.ItemClick, AddressOf BarButtonItem_Edit_Copy_ItemClick
            AddHandler BarButtonItem_Edit_Paste.ItemClick, AddressOf BarButtonItem_Edit_Paste_ItemClick
            AddHandler BarButtonItem_Edit_Delete.ItemClick, AddressOf BarButtonItem_Edit_Delete_ItemClick
            AddHandler BarButtonItem_Edit_SelectAll.ItemClick, AddressOf BarButtonItem_Edit_SelectAll_ItemClick
            AddHandler BarButtonItem_Edit_Find.ItemClick, AddressOf BarButtonItem_Edit_Search_ItemClick
            AddHandler BarButtonItem_Edit_Replace.ItemClick, AddressOf BarButtonItem_Edit_Replace_ItemClick
            AddHandler BarCheckItem_View_StatusBar.CheckedChanged, AddressOf BarCheckItem_View_Status_CheckedChanged
            AddHandler BarCheckItem_View_Normal.ItemClick, AddressOf BarCheckItem_View_DraftMode_CheckedChanged
            AddHandler BarButtonItem_View_PageLayout.ItemClick, AddressOf BarCheckItem_View_PageMode_CheckedChanged
            AddHandler BarCheckItem_View_Zoom_025.ItemClick, AddressOf View_Zoom_025_CheckedChanged
            AddHandler BarCheckItem_View_Zoom_050.ItemClick, AddressOf View_Zoom_050_CheckedChanged
            AddHandler BarCheckItem_View_Zoom_075.ItemClick, AddressOf View_Zoom_075_CheckedChanged
            AddHandler BarCheckItem_View_Zoom_100.ItemClick, AddressOf View_Zoom_100_CheckedChanged
            AddHandler BarCheckItem_View_Zoom_150.ItemClick, AddressOf View_Zoom_150_CheckedChanged
            AddHandler BarCheckItem_View_Zoom_200.ItemClick, AddressOf View_Zoom_200_CheckedChanged
            AddHandler BarCheckItem_View_Zoom_300.ItemClick, AddressOf View_Zoom_300_CheckedChanged
            AddHandler BarButtonItem_Insert_PageBreak.ItemClick, AddressOf BarButtonItem_Insert_PageBreak_ItemClick
            AddHandler BarButtonItem_Insert_Image.ItemClick, AddressOf BarButtonItem_Insert_Image_ItemClick
            AddHandler BarButtonItem_Insert_File.ItemClick, AddressOf BarButtonItem_Insert_File_ItemClick
            AddHandler BarButtonItem_Format_Paragraph.ItemClick, AddressOf BarButtonItem_Format_Paragraph_ItemClick
            AddHandler BarButtonItem_Format_Character.ItemClick, AddressOf BarButtonItem_Format_Font_ItemClick
            AddHandler BarCheckItem_Format_Bullets_Bullets.ItemClick, AddressOf BarButtonItem_Bullets_Bullets_ItemClick
            AddHandler BarCheckItem_Format_Bullets_Multilevel.ItemClick, AddressOf BarButtonItem_Bullets_Multilevel_ItemClick
            AddHandler BarCheckItem_Format_Bullets_Numbers.ItemClick, AddressOf BarButtonItem_Bullets_Numbers_ItemClick
            AddHandler RepositoryItemColorEdit_DocumentBackgroundColor.ColorChanged, AddressOf RepositoryItemColorEdit_DocumentBackgroundColor_ColorChanged
            AddHandler RepositoryItemColorEdit_TextColor.ColorChanged, AddressOf RepositoryItemColorEdit_TextColor_ColorChanged
            AddHandler RepositoryItemColorEdit_TextBackgoundColor.ColorChanged, AddressOf RepositoryItemColorEdit_TextBackgoundColor_ColorChanged
            AddHandler BarButtonItem_Tools_Spell.ItemClick, AddressOf BarButtonItem_Tools_Spell_ItemClick
            AddHandler BarSubItem_Edit.Popup, AddressOf BarSubItem_Edit_Popup
            AddHandler BarSubItem_View_Zoom.Popup, AddressOf BarSubItem_view_zoom_Popup
            AddHandler BarSubItem_View.Popup, AddressOf BarSubItem_View_Popup
            AddHandler SpellChecker1.CheckCompleteFormShowing, AddressOf SpellChecker1_CheckCompleteFormShowing
            AddHandler TabbedControlGroup1.SelectedPageChanged, AddressOf TabbedControlGroup1_SelectedPageChanged
            AddHandler RichEditControl1.PopupMenuShowing, AddressOf RichEditControl1_PopupMenuShowing
        End Sub

        Public Sub New(ByVal bc As BusinessContext, ByVal ActionPlanRecord As action_plan)
            MyClass.New()
            Me.bc = bc
            Me.actionPlanRecord = ActionPlanRecord
            RegisterEventHandlers()

            ' Initialize the spelling checker to spell the controls
            SpellCheckTextControllersManager.Default.RegisterClass(GetType(RichEditControl), GetType(RichEditSpellCheckController))
        End Sub

        Private Sub RegisterEventHandlers()
            AddHandler Load, AddressOf ActionPlanForm_Load
        End Sub

        Private Sub UnRegisterEventHandlers()
            RemoveHandler Load, AddressOf ActionPlanForm_Load
        End Sub

#Region " Form "

        ''' <summary>
        ''' Process the FORM : LOAD event
        ''' </summary>
        Private Sub ActionPlanForm_Load(ByVal sender As Object, ByVal e As EventArgs)

            UnRegisterEventHandlers()
            Try
                ' Define the processing classes
                ToDoClass = New ItemsBase() With {.ItemGroup = 1, .Ctl = CheckedListBoxControl_ToDo, .BarManagerItem = barManager1, .actionPlanRecord = actionPlanRecord}
                IncomeClass = New ItemsBase() With {.ItemGroup = 2, .Ctl = CheckedListBoxControl_Income, .BarManagerItem = barManager1, .actionPlanRecord = actionPlanRecord}
                ExpensesClass = New ItemsBase() With {.ItemGroup = 3, .Ctl = CheckedListBoxControl_Expenses, .BarManagerItem = barManager1, .actionPlanRecord = actionPlanRecord}
                RequiredClass = New ItemsBase() With {.ItemGroup = 4, .Ctl = CheckedListBoxControl_Required, .BarManagerItem = barManager1, .actionPlanRecord = actionPlanRecord}
                GoalsClass = New Goals() With {.Ctl = RichEditControl1, .actionPlanRecord = actionPlanRecord}

                ' Read the information into the control classes
                ToDoClass.ReadForm(bc)
                IncomeClass.ReadForm(bc)
                ExpensesClass.ReadForm(bc)
                RequiredClass.ReadForm(bc)
                GoalsClass.ReadForm(bc)

            Finally
                RegisterEventHandlers()
            End Try
        End Sub

        Private Sub MyDispose()
            ToDoClass.Dispose()
            IncomeClass.Dispose()
            ExpensesClass.Dispose()
            RequiredClass.Dispose()
        End Sub

        ''' <summary>
        ''' Process the OK button to save the changes
        ''' </summary>
        Private Sub Button_OK_Click(ByVal sender As Object, ByVal e As EventArgs)

            ' We need to first submit the changes for the "other" items to ensure that they are recorded
            ' in the database. We need to have their ID fields should they be checked.
            Try
                bc.SubmitChanges()

                ' Save all of the sub-forms. They will do the work before we return.
                ToDoClass.SaveForm(bc)
                IncomeClass.SaveForm(bc)
                ExpensesClass.SaveForm(bc)
                RequiredClass.SaveForm(bc)
                GoalsClass.SaveForm(bc)

            Catch ex As DebtPlus.LINQ.DataValidationException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "error submitting changes to action_plan data")

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "error submitting changes to action_plan data")
            End Try

            ' Complete the dialog successfully.
            DialogResult = Windows.Forms.DialogResult.OK
        End Sub

#End Region

#Region "Goals"

        Private Class Goals

            Private goalsRecord As DebtPlus.LINQ.action_items_goal = Nothing
            Friend Property actionPlanRecord As DebtPlus.LINQ.action_plan
            Friend Property Ctl As RichEditControl

            Public Sub New()
            End Sub

            Public Sub ReadForm(ByVal bc As BusinessContext)

                ' If there is no goals record then we need to create one.
                goalsRecord = actionPlanRecord.action_items_goals
                If goalsRecord Is Nothing Then
                    goalsRecord = DebtPlus.LINQ.Factory.Manufacture_action_items_goal()
                End If

                ' First, try RTF as the note format
                If Strings.IsRTF(goalsRecord.text) Then
                    Try
                        Ctl.RtfText = goalsRecord.text
                        Ctl.Select()
                        Return
                    Catch ex As Exception
                    End Try
                End If

                ' Try to use HTML as the note format if possible
                If Strings.IsHTML(goalsRecord.text) Then
                    Try
                        Ctl.HtmlText = goalsRecord.text
                        Ctl.Select()
                        Return
                    Catch ex As Exception
                    End Try
                End If

                ' Load the note as ASCII text if the note was not loaded as RTF
                Try
                    Ctl.Text = goalsRecord.text
                    Ctl.Select()
                Catch ex As Exception
                End Try
            End Sub

            Public Sub SaveForm(bc As DebtPlus.LINQ.BusinessContext)
                goalsRecord.text = Ctl.RtfText
                actionPlanRecord.action_items_goals = goalsRecord
            End Sub
        End Class

#End Region

#Region "Items"
        Private Class ItemsBase
            Implements IDisposable

            Friend Property actionPlanRecord As action_plan
            Friend Property Ctl As CheckedListBoxControl
            Friend Property ItemGroup As Int32
            Friend Property BarManagerItem As DevExpress.XtraBars.BarManager

            Private barButton_popup_create As BarButtonItem
            Private barButton_popup_delete As BarButtonItem
            Private barButton_popup_edit As BarButtonItem
            Private pm2 As PopupMenu

            Private ControlItem As Int32 = -1

            Public Sub New()
            End Sub

            Private Sub RegisterEventHandlers()
                If Ctl IsNot Nothing Then
                    AddHandler Ctl.MouseDown, AddressOf MouseDownEvent
                End If
            End Sub

            Private Sub UnRegisterEventHandlers()
                If Ctl IsNot Nothing Then
                    RemoveHandler Ctl.MouseDown, AddressOf MouseDownEvent
                End If
            End Sub

            Private Sub MouseDownEvent(ByVal Sener As Object, ByVal e As MouseEventArgs)
                Dim pt As System.Drawing.Point = Windows.Forms.Control.MousePosition
                Dim localPt As System.Drawing.Point = Ctl.PointToClient(pt)
                Dim EditDeleteEnabled As Boolean = False

                If e.Button <> MouseButtons.Right Then
                    Return
                End If

                If pm2 Is Nothing Then
                    pm2 = New DevExpress.XtraBars.PopupMenu(BarManagerItem)

                    ' Create menu
                    If barButton_popup_create Is Nothing Then
                        barButton_popup_create = New DevExpress.XtraBars.BarButtonItem(BarManagerItem, "&Add...")
                        AddHandler barButton_popup_create.ItemClick, AddressOf CreateClick
                    End If
                    pm2.AddItem(barButton_popup_create)

                    ' Build the edit menu item
                    If barButton_popup_edit Is Nothing Then
                        barButton_popup_edit = New DevExpress.XtraBars.BarButtonItem(BarManagerItem, "&Change...")
                        AddHandler barButton_popup_edit.ItemClick, AddressOf EditClick
                    End If
                    pm2.AddItem(barButton_popup_edit)

                    ' build the delete menu item
                    If barButton_popup_delete Is Nothing Then
                        barButton_popup_delete = New DevExpress.XtraBars.BarButtonItem(BarManagerItem, "&Delete...")
                        AddHandler barButton_popup_delete.ItemClick, AddressOf DeleteClick
                    End If
                    pm2.AddItem(barButton_popup_delete)
                End If

                ControlItem = Ctl.IndexFromPoint(localPt)
                If ControlItem >= 0 Then
                    Dim slb As SortedCheckedListboxControlItem = CType(Ctl.Items(ControlItem), SortedCheckedListboxControlItem)
                    If slb.Value.GetType() Is GetType(DebtPlus.LINQ.action_items_other) Then
                        EditDeleteEnabled = True
                    End If
                End If

                ' Enable or disable the buttons based upon the location
                barButton_popup_create.Enabled = True
                barButton_popup_edit.Enabled = EditDeleteEnabled
                barButton_popup_delete.Enabled = EditDeleteEnabled

                ' Display the popup menu at the current mouse location
                pm2.ShowPopup(BarManagerItem, pt)
            End Sub

            Private Sub CreateClick(ByVal Sender As Object, ByVal e As EventArgs)
                CreateItem()
            End Sub

            Private Sub DeleteClick(ByVal sender As Object, ByVal e As EventArgs)

                ' Find the item to be deleted
                Dim ctlRecord As SortedCheckedListboxControlItem = DirectCast(Ctl.Items(ControlItem), SortedCheckedListboxControlItem)
                If ctlRecord Is Nothing Then
                    Return
                End If

                DeleteItem(ctlRecord)
            End Sub

            Private Sub EditClick(ByVal sender As Object, ByVal e As EventArgs)

                ' Find the item to be edited
                Dim ctlRecord As SortedCheckedListboxControlItem = DirectCast(Ctl.Items(ControlItem), SortedCheckedListboxControlItem)
                If ctlRecord Is Nothing Then
                    Return
                End If

                EditItem(ctlRecord)
            End Sub

            Private Sub CreateItem()

                Dim NewDescription As String = String.Empty
                If DebtPlus.Data.Forms.InputBox.Show("What is the name of the item that you wish to create?", NewDescription, "Create new Action Item", String.Empty, 50) <> DialogResult.OK OrElse String.IsNullOrWhiteSpace(NewDescription) Then
                    Return
                End If

                Dim record As DebtPlus.LINQ.action_items_other = DebtPlus.LINQ.Factory.Manufacture_action_items_other()
                record.description = NewDescription
                record.item_group = ItemGroup

                ' Add the record to the action plan. It will be updated later.
                actionPlanRecord.action_items_others.Add(record)

                ' Create the entry for the list of items and check it.
                Dim ctlItem As New SortedCheckedListboxControlItem()
                ctlItem.Value = record
                ctlItem.Description = NewDescription
                ctlItem.CheckState = CheckState.Checked

                Ctl.Items.Add(ctlItem)
                Ctl.Refresh()
            End Sub

            Private Sub DeleteItem(ctlItem As SortedCheckedListboxControlItem)

                ' There must be a pointer or why are we here?
                If ctlItem Is Nothing Then
                    Return
                End If

                ' Get the "other" record to delete. If this is not an "other" record then this will fail and we will ignore the delete.
                Dim record As DebtPlus.LINQ.action_items_other = TryCast(ctlItem.Value, DebtPlus.LINQ.action_items_other)
                If record Is Nothing Then
                    Return
                End If

                ' Delete the record from the plan and remove it from the list
                actionPlanRecord.action_items_others.Remove(record)
                Ctl.Items.Remove(ctlItem)
                Ctl.Refresh()
            End Sub

            Private Sub EditItem(ctlItem As SortedCheckedListboxControlItem)

                ' There must be a pointer or why are we here?
                If ctlItem Is Nothing Then
                    Return
                End If

                ' Get the "other" record to edit. If this is not an "other" record then this will fail and we will ignore the edit.
                Dim record As DebtPlus.LINQ.action_items_other = TryCast(ctlItem.Value, DebtPlus.LINQ.action_items_other)
                If record Is Nothing Then
                    Return
                End If

                ' Ask for a new description for this record
                Dim NewDescription As String = String.Empty
                If InputBox.Show("What is the new name?", NewDescription, "Edit Action Item", record.description, 50) <> DialogResult.OK OrElse String.IsNullOrWhiteSpace(NewDescription) Then
                    Return
                End If

                ' Update the items with the new description
                record.description = NewDescription
                ctlItem.Description = NewDescription
                Ctl.Refresh()
            End Sub

            Public Sub ReadForm(bc As BusinessContext)

                UnRegisterEventHandlers()
                Try
                    ' Set the sort order for the form the first time
                    Ctl.SortOrder = System.Windows.Forms.SortOrder.Ascending
                    Ctl.Tag = Me

                    ' Load the standard list of item descriptions
                    Dim colStandard As System.Collections.Generic.List(Of action_item) = DebtPlus.LINQ.Cache.action_item.getList()
                    Ctl.Items.Clear()

                    For Each newItem As action_item In colStandard.FindAll(Function(s) s.item_group = ItemGroup).ToList()
                        Dim ctlItem As New SortedCheckedListboxControlItem()
                        ctlItem.Description = newItem.description
                        ctlItem.Value = newItem
                        Ctl.Items.Add(ctlItem)
                    Next

                    ' Add the descriptions for the "other" items.
                    For Each newItem As action_items_other In actionPlanRecord.action_items_others.Where(Function(s) s.item_group = ItemGroup).ToList()
                        Dim ctlItem As New SortedCheckedListboxControlItem()
                        ctlItem.Description = newItem.description
                        ctlItem.Value = newItem
                        Ctl.Items.Add(ctlItem)
                    Next

                    ' Check the items that are needed to be checked
                    For Each checkedItem As client_action_item In actionPlanRecord.client_action_items.ToList()
                        Dim checkedEntry As Int32 = checkedItem.action_item
                        Dim fnd As SortedCheckedListboxControlItem = FindItem(checkedEntry)
                        If fnd IsNot Nothing Then
                            fnd.CheckState = CheckState.Checked
                        End If
                    Next

                Finally
                    RegisterEventHandlers()
                End Try
            End Sub

            Protected Function FindItem(ByVal ActionItem As Int32) As SortedCheckedListboxControlItem
                For Each itm As SortedCheckedListboxControlItem In Ctl.Items
                    If itm.Value.GetType() Is GetType(DebtPlus.LINQ.action_items_other) Then
                        If DirectCast(itm.Value, action_items_other).Id = ActionItem Then
                            Return itm
                        End If
                    End If

                    If itm.Value.GetType() Is GetType(DebtPlus.LINQ.action_item) Then
                        If DirectCast(itm.Value, action_item).Id = ActionItem Then
                            Return itm
                        End If
                    End If
                Next

                Return Nothing
            End Function

            Public Overridable Sub SaveForm(bc As BusinessContext)

                For Each itm As SortedCheckedListboxControlItem In Ctl.Items

                    ' Find the action item type from the list
                    Dim id As Int32 = -1
                    If itm.Value.GetType() Is GetType(DebtPlus.LINQ.action_items_other) Then
                        id = DirectCast(itm.Value, action_items_other).Id
                    ElseIf itm.Value.GetType() Is GetType(DebtPlus.LINQ.action_item) Then
                        id = DirectCast(itm.Value, action_item).Id
                    End If

                    ' If the item could not be determined (why?) then ignore the entry.
                    If id < 0 Then
                        Continue For
                    End If

                    ' Locate the selected record in the list of client_action_items
                    Dim qSelected As client_action_item = actionPlanRecord.client_action_items.Where(Function(s) s.action_item = id).FirstOrDefault()

                    ' If the item is not checked then delete the existing item if we find it.
                    If itm.CheckState = CheckState.Unchecked Then
                        If qSelected IsNot Nothing Then
                            actionPlanRecord.client_action_items.Remove(qSelected)
                        End If

                    Else

                        ' It is checked. If the item is not present then create a new item.
                        If qSelected Is Nothing Then
                            qSelected = DebtPlus.LINQ.Factory.Manufacture_client_action_item()
                            qSelected.action_item = id
                            actionPlanRecord.client_action_items.Add(qSelected)
                        End If
                    End If
                Next
            End Sub

            Private disposedValue As Boolean
            ' To detect redundant calls

            ' IDisposable
            Protected Overridable Sub Dispose(ByVal disposing As Boolean)
                If Not disposedValue Then
                    If disposing Then
                        If pm2 IsNot Nothing Then pm2.Dispose()
                        If barButton_popup_create IsNot Nothing Then barButton_popup_create.Dispose()
                        If barButton_popup_delete IsNot Nothing Then barButton_popup_delete.Dispose()
                        If barButton_popup_edit IsNot Nothing Then barButton_popup_edit.Dispose()
                    End If
                End If
                disposedValue = True
            End Sub

#Region " IDisposable Support "

            ' This code added by Visual Basic to correctly implement the disposable pattern.
            Public Sub Dispose() Implements IDisposable.Dispose
                ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
                Dispose(True)
                GC.SuppressFinalize(Me)
            End Sub

#End Region
        End Class

#End Region

#Region "File menu"

        Private Sub BarButtonItem_File_Print_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Dim cmd As New QuickPrintCommand(RichEditControl1)
            cmd.Execute()
        End Sub

        Private Sub BarButtonItem_File_PrintDialog_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Dim cmd As New PrintCommand(RichEditControl1)
            cmd.Execute()
        End Sub

        Private Sub BarButtonItem_File_PrintPreview_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Dim cmd As New PrintPreviewCommand(RichEditControl1)
            cmd.Execute()
        End Sub

#End Region

#Region "Edit menu"

        Private Sub BarButtonItem_Edit_Undo_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Dim cmd As New UndoCommand(RichEditControl1)
            cmd.Execute()
        End Sub

        Private Sub BarButtonItem_Edit_Redo_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Dim cmd As New RedoCommand(RichEditControl1)
            cmd.Execute()
        End Sub

        Private Sub BarButtonItem_Edit_Cut_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Dim cmd As New CutSelectionCommand(RichEditControl1)
            cmd.Execute()
        End Sub

        Private Sub BarButtonItem_Edit_Copy_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Dim cmd As New CopySelectionCommand(RichEditControl1)
            cmd.Execute()
        End Sub

        Private Sub BarButtonItem_Edit_Paste_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Dim cmd As New PasteSelectionCommand(RichEditControl1)
            cmd.Execute()
        End Sub

        Private Sub BarButtonItem_Edit_Delete_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Dim cmd As New DeleteCommand(RichEditControl1)
            cmd.Execute()
        End Sub

        Private Sub BarButtonItem_Edit_SelectAll_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Dim cmd As New SelectAllCommand(RichEditControl1)
            cmd.Execute()
        End Sub

        Private Sub BarButtonItem_Edit_Search_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Dim cmd As New FindCommand(RichEditControl1)
            cmd.Execute()
        End Sub

        Private Sub BarButtonItem_Edit_Replace_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Dim cmd As New ReplaceCommand(RichEditControl1)
            cmd.Execute()
        End Sub

#End Region

#Region "View menu"

        Private Sub BarCheckItem_View_Status_CheckedChanged(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Bar_StatusBar.Visible = BarCheckItem_View_StatusBar.Checked
        End Sub

        Private Sub BarCheckItem_View_DraftMode_CheckedChanged(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            RichEditControl1.ActiveViewType = RichEditViewType.Draft
        End Sub

        Private Sub BarCheckItem_View_PageMode_CheckedChanged(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            RichEditControl1.ActiveViewType = RichEditViewType.PrintLayout
        End Sub

        Private Sub View_Zoom_025_CheckedChanged(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            RichEditControl1.ActiveView.ZoomFactor = 0.25
        End Sub

        Private Sub View_Zoom_050_CheckedChanged(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            RichEditControl1.ActiveView.ZoomFactor = 0.5
        End Sub

        Private Sub View_Zoom_075_CheckedChanged(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            RichEditControl1.ActiveView.ZoomFactor = 0.75
        End Sub

        Private Sub View_Zoom_100_CheckedChanged(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            RichEditControl1.ActiveView.ZoomFactor = 1.0
        End Sub

        Private Sub View_Zoom_150_CheckedChanged(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            RichEditControl1.ActiveView.ZoomFactor = 1.5
        End Sub

        Private Sub View_Zoom_200_CheckedChanged(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            RichEditControl1.ActiveView.ZoomFactor = 2.0
        End Sub

        Private Sub View_Zoom_300_CheckedChanged(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            RichEditControl1.ActiveView.ZoomFactor = 3.0
        End Sub

#End Region

#Region "Insert menu"

        Private Sub BarButtonItem_Insert_PageBreak_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Dim cmd As New InsertPageBreakCommand(RichEditControl1)
            cmd.Execute()
        End Sub

        Private Sub BarButtonItem_Insert_Image_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Dim cmd As New InsertPictureCommand(RichEditControl1)
            cmd.Execute()
        End Sub

        Private Sub BarButtonItem_Insert_File_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Dim FileName As String = String.Empty
            Dim answer As DialogResult
            Using dlg As New OpenFileDialog
                With dlg
                    .AddExtension = True
                    .AutoUpgradeEnabled = True
                    .CheckFileExists = True
                    .CheckPathExists = True
                    .DefaultExt = ".txt"
                    .FileName = "*.txt"
                    .Filter = "Text Files (*.txt)|*.txt|RTF Files (*.rtf)|*.rtf|HTML Files (*.htm; *.html)|*.htm;*.html|All Files (*.*)|*.*"
                    .FilterIndex = 0
                    .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
                    .Multiselect = False
                    .ReadOnlyChecked = True
                    .RestoreDirectory = True
                    .Title = "Insert file"
                    answer = .ShowDialog()
                    FileName = .FileName
                End With
            End Using

            ' If there is a file then attempt to open it
            If answer = DialogResult.OK AndAlso FileName <> String.Empty Then
                Dim strContents As String = String.Empty
                Dim txFS As StreamReader = Nothing
                Try
                    txFS = New StreamReader(FileName)
                    strContents = txFS.ReadToEnd.Trim()

                Catch ex As FileNotFoundException
                    MessageBox.Show(ex.ToString(), "Error reading include file", MessageBoxButtons.OK, MessageBoxIcon.Error)

                Catch ex As DirectoryNotFoundException
                    MessageBox.Show(ex.ToString(), "Error reading include file", MessageBoxButtons.OK, MessageBoxIcon.Error)

                Catch ex As PathTooLongException
                    MessageBox.Show(ex.ToString(), "Error reading include file", MessageBoxButtons.OK, MessageBoxIcon.Error)

                Finally
                    If txFS IsNot Nothing Then
                        txFS.Close()
                        txFS.Dispose()
                    End If
                End Try

                ' Attempt to determine the file format
                If strContents <> String.Empty Then
                    Try
                        With RichEditControl1.Document
                            Select Case Path.GetExtension(FileName).ToLower()
                                Case ".rtf"
                                    .InsertRtfText(.CaretPosition, strContents)
                                Case ".htm"
                                    .InsertHtmlText(.CaretPosition, strContents)
                                Case ".html"
                                    .InsertHtmlText(.CaretPosition, strContents)
                                Case Else
                                    .InsertText(.CaretPosition, strContents)
                            End Select
                        End With
                    Catch ex As Exception
                        MessageBox.Show(ex.ToString(), "Error inserting file", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End Try
                End If
            End If
        End Sub

#End Region

#Region "Format menu"

        Private Sub BarButtonItem_Format_Paragraph_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Dim cmd As New ShowParagraphFormCommand(RichEditControl1)
            cmd.Execute()
        End Sub

        Private Sub BarButtonItem_Format_Font_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Dim cmd As New ShowFontFormCommand(RichEditControl1)
            cmd.Execute()
        End Sub

        Private Sub BarButtonItem_Bullets_Bullets_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Dim cmd As New ToggleBulletedListCommand(RichEditControl1)
            cmd.Execute()
        End Sub

        Private Sub BarButtonItem_Bullets_Multilevel_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Dim cmd As New ToggleMultiLevelListCommand(RichEditControl1)
            cmd.Execute()
        End Sub

        Private Sub BarButtonItem_Bullets_Numbers_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Dim cmd As New ToggleSimpleNumberingListCommand(RichEditControl1)
            cmd.Execute()
        End Sub

        Private Sub RepositoryItemColorEdit_DocumentBackgroundColor_ColorChanged(ByVal sender As Object, ByVal e As EventArgs)
            Dim NewColor As Color = CType(CType(sender, ColorEdit).EditValue, Color)
            RichEditControl1.ActiveView.BackColor = NewColor
        End Sub

        Private Sub RepositoryItemColorEdit_TextColor_ColorChanged(ByVal sender As Object, ByVal e As EventArgs)
            Dim NewColor As Color = CType(CType(sender, ColorEdit).EditValue, Color)
            Dim cmd As New ChangeFontColorCommand(RichEditControl1)
            ' !!!! FIX THIS !!!!
            cmd.Execute()
        End Sub

        Private Sub RepositoryItemColorEdit_TextBackgoundColor_ColorChanged(ByVal sender As Object, ByVal e As EventArgs)
            Dim NewColor As Color = CType(CType(sender, ColorEdit).EditValue, Color)
            Dim cmd As New ChangeFontBackColorCommand(RichEditControl1)
            ' !!!! FIX THIS !!!!
            cmd.Execute()
        End Sub

#End Region

#Region "Tools menu"

        Private Sub BarButtonItem_Tools_Spell_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            SpellDocument()
        End Sub

#End Region

#Region "Menu popup functions"

        Private Sub BarSubItem_Edit_Popup(ByVal sender As Object, ByVal e As EventArgs)
            BarButtonItem_Edit_Redo.Enabled = CanRedo()
            BarButtonItem_Edit_Undo.Enabled = CanUndo()

            BarButtonItem_Edit_Copy.Enabled = CanCut()
            BarButtonItem_Edit_Cut.Enabled = BarButtonItem_Edit_Copy.Enabled
            BarButtonItem_Edit_Delete.Enabled = BarButtonItem_Edit_Copy.Enabled

            BarButtonItem_Edit_Paste.Enabled = CanPaste()
        End Sub

        Private Shared Function CanRedo() As Boolean
            Return True
        End Function

        Private Shared Function CanUndo() As Boolean
            Return True
        End Function

        Private Shared Function CanCut() As Boolean
            Return True
        End Function

        Private Shared Function CanPaste() As Boolean
            Return Clipboard.ContainsText
        End Function

        Private Sub BarSubItem_view_zoom_Popup(ByVal sender As Object, ByVal e As EventArgs)

            ' Convert the percentage to one of the known categories. Choose the closest one.
            Dim Pct As Int32 = Convert.ToInt32(RichEditControl1.ActiveView.ZoomFactor * 100.0)
            If Pct >= 300 Then
                Pct = 300
            ElseIf Pct >= 200 Then
                Pct = 200
            ElseIf Pct >= 150 Then
                Pct = 150
            ElseIf Pct >= 100 Then
                Pct = 100
            ElseIf Pct >= 75 Then
                Pct = 75
            ElseIf Pct >= 50 Then
                Pct = 50
            Else
                Pct = 25
            End If

            ' Check the appropriate box on the zoom factors.
            BarCheckItem_View_Zoom_300.Checked = Pct = 300
            BarCheckItem_View_Zoom_200.Checked = Pct = 200
            BarCheckItem_View_Zoom_150.Checked = Pct = 150
            BarCheckItem_View_Zoom_100.Checked = Pct = 100
            BarCheckItem_View_Zoom_075.Checked = Pct = 75
            BarCheckItem_View_Zoom_050.Checked = Pct = 50
            BarCheckItem_View_Zoom_025.Checked = Pct = 25
        End Sub

        Private Sub BarSubItem_View_Popup(ByVal sender As Object, ByVal e As EventArgs)
            BarCheckItem_View_Normal.Checked = RichEditControl1.ActiveView.Type = RichEditViewType.Draft
            BarCheckItem_View_PageLayout.Checked = RichEditControl1.ActiveView.Type = RichEditViewType.PrintLayout
            BarCheckItem_View_StatusBar.Checked = Bar_StatusBar.Visible
        End Sub

#End Region

        ''' <summary>
        ''' Spell the document
        ''' </summary>
        Private Sub SpellDocument()

            ' Load the dictionaries on the first time
            If SharedDictionaryStorage1.Dictionaries.Count = 0 Then
                SpellCheckerDictionaries.SetSpellCheckerDictionaries(SharedDictionaryStorage1)
            End If

            ' Restore the option settings for the speller
            ReadSpellingOptions()

            ' Set our spelling options for the new processing
            With SpellChecker1.OptionsSpelling
                .CheckFromCursorPos = DefaultBoolean.False
                .CheckSelectedTextFirst = DefaultBoolean.False
            End With

            ' Do the actual spelling of the buffer
            SpellChecker1.Check(RichEditControl1)
            ' check the note text

            ' Save the spell checker options should they have been shown
            SaveSpellCheckerOptions()

            ' Display the completion event.
            MessageBox.Show("The spelling check is complete.", Application.ProductName, MessageBoxButtons.OK)
        End Sub

        ''' <summary>
        ''' Allow the spelling options to be changed
        ''' </summary>
        Private Sub SetSpellingOptions(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            ReadSpellingOptions()
            If SpellChecker1.FormsManager.ShowOptionsForm = DialogResult.OK Then

                With SpellChecker1.OptionsSpelling
                    .IgnoreEmails = DevExpress.SpellChecker.Native.BoolDefaultBooleanConverter.ConvertBoolToDefaultBoolean(SpellChecker1.FormsManager.OptionsForm.IgnoreEmails)
                    .IgnoreMixedCaseWords = DevExpress.SpellChecker.Native.BoolDefaultBooleanConverter.ConvertBoolToDefaultBoolean(SpellChecker1.FormsManager.OptionsForm.IgnoreMixedCaseWords)
                    .IgnoreRepeatedWords = DevExpress.SpellChecker.Native.BoolDefaultBooleanConverter.ConvertBoolToDefaultBoolean(SpellChecker1.FormsManager.OptionsForm.IgnoreRepeatedWords)
                    .IgnoreUpperCaseWords = DevExpress.SpellChecker.Native.BoolDefaultBooleanConverter.ConvertBoolToDefaultBoolean(SpellChecker1.FormsManager.OptionsForm.IgnoreUpperCase)
                    .IgnoreUrls = DevExpress.SpellChecker.Native.BoolDefaultBooleanConverter.ConvertBoolToDefaultBoolean(SpellChecker1.FormsManager.OptionsForm.IgnoreUrls)
                    .IgnoreWordsWithNumbers = DevExpress.SpellChecker.Native.BoolDefaultBooleanConverter.ConvertBoolToDefaultBoolean(SpellChecker1.FormsManager.OptionsForm.IgnoreWordsWithDigits)
                End With

                SaveSpellCheckerOptions()
            End If
        End Sub

        ''' <summary>
        ''' Set the spelling options into the spelling control
        ''' </summary>
        Private FirstSpelling As Boolean = True

        Private Sub ReadSpellingOptions()

            ' If this is the first time then load the options from the saved storage
            If FirstSpelling Then
                FirstSpelling = False
                Try
                    Dim PathName As String = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "DebtPlus", "Spelling")
                    Dim FileName As String = Path.Combine(PathName, "Options.xml")

                    If File.Exists(FileName) Then
                        SpellChecker1.RestoreFromXML(FileName)
                    End If

                Catch ex As Exception ' Don't trap if we have an error. Just ignore it.
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
                End Try
            End If
        End Sub

        ''' <summary>
        ''' Save the spell checking options to the disk system for next time
        ''' </summary>
        Private Sub SaveSpellCheckerOptions()
            Try
                Dim PathName As String = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "DebtPlus", "Spelling")
                Dim FileName As String = Path.Combine(PathName, "Options.xml")

                If Not Directory.Exists(PathName) Then
                    Directory.CreateDirectory(PathName)
                End If
                SpellChecker1.SaveToXML(FileName)

            Catch ex As Exception ' Don't trap if we have an error. Just ignore it.
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
            End Try
        End Sub

        ''' <summary>
        ''' Callback from the speller to indicate that the operation is complete
        ''' </summary>
        Private Sub SpellChecker1_CheckCompleteFormShowing(ByVal sender As Object, ByVal e As DevExpress.XtraSpellChecker.FormShowingEventArgs)
            e.Handled = True
        End Sub

#Region "Layout"

        Protected Const STR_LayoutXML As String = "Layout.xml"
        Protected Const STR_BarsXML As String = "Bars.xml"

        ''' <summary>
        ''' Filename to the disk storage for this program
        ''' </summary>
        Protected Friend Shared Function XMLDirectoryName() As String
            Dim answer As String =
                    Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                                 String.Format("DebtPlus{0}Notes", Path.DirectorySeparatorChar)) +
                    Path.DirectorySeparatorChar
            Return answer
        End Function

        ''' <summary>
        ''' Handle the layout change event
        ''' </summary>
        Protected Sub LayoutControl01_Layout(ByVal sender As Object, ByVal e As EventArgs)
            Dim PathName As String = XMLDirectoryName()
            Dim Filename As String = Path.Combine(PathName, STR_LayoutXML)
            Dim Success As Boolean = False

            ' Try to save the file directly. This is the normal case once there is a file to be saved.
            Try
                If Directory.Exists(PathName) Then
                    LayoutControl1.SaveLayoutToXml(Filename)
                    Success = True
                End If

            Catch ex As FileLoadException
            Catch ex As DirectoryNotFoundException
            End Try

            ' Try to create the base directory if the item does not exist
            If Not Success Then
                Try
                    Directory.CreateDirectory(PathName)
                    If Directory.Exists(PathName) Then
                        LayoutControl1.SaveLayoutToXml(Filename)
                        Success = True
                    End If

                Catch ex As FileLoadException
                Catch ex As DirectoryNotFoundException
                End Try
            End If
        End Sub

        Private Sub BarManager1_EndCustomization(ByVal sender As Object, ByVal e As EventArgs)
            Dim PathName As String = XMLDirectoryName()
            Dim Filename As String = Path.Combine(PathName, STR_BarsXML)
            Dim Success As Boolean = False

            ' Try to save the file directly. This is the normal case once there is a file to be saved.
            Try
                If Directory.Exists(PathName) Then
                    barManager1.SaveLayoutToXml(Filename)
                    Success = True
                End If

            Catch ex As FileLoadException
            Catch ex As DirectoryNotFoundException
            End Try

            ' Try to create the base directory if the item does not exist
            If Not Success Then
                Try
                    Directory.CreateDirectory(PathName)
                    If Directory.Exists(PathName) Then
                        barManager1.SaveLayoutToXml(Filename)
                        Success = True
                    End If

                Catch ex As FileLoadException
                Catch ex As DirectoryNotFoundException
                End Try
            End If
        End Sub

#End Region

#Region "Tab"

        Private Sub TabbedControlGroup1_SelectedPageChanged(ByVal sender As Object, ByVal e As LayoutTabPageChangedEventArgs)
            Dim IsEditor As Boolean = TabbedControlGroup1.SelectedTabPage Is LayoutControlGroup2

            ' Enable/Disable the menu bar items based upon the editor being the item selected
            BarSubItem_File.Enabled = IsEditor
            BarSubItem_Edit.Enabled = IsEditor
            BarSubItem_Format.Enabled = IsEditor
            BarSubItem_Insert.Enabled = IsEditor
            BarSubItem_Tools.Enabled = IsEditor

            ' The view menu is a bit more complex
            BarSubItem_View_Zoom.Enabled = IsEditor
            BarCheckItem_View_Normal.Enabled = IsEditor
            BarCheckItem_View_PageLayout.Enabled = IsEditor
        End Sub

#End Region

#Region "RichEditControl1"

        Private Sub RichEditControl1_PopupMenuShowing(ByVal sender As Object, ByVal e As DevExpress.XtraRichEdit.PopupMenuShowingEventArgs)
            Dim item As New DXMenuItem("Spell Document...", AddressOf SpellPopup)
            item.BeginGroup = True
            e.Menu.Items.Add(item)
        End Sub

        Private Sub SpellPopup(ByVal Sender As Object, ByVal e As EventArgs)
            SpellDocument()
        End Sub

#End Region
    End Class
End Namespace
