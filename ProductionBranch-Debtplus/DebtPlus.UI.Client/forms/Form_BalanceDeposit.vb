#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.Svc.DataSets
Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.UI.Client.Service
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports DebtPlus.Events
Imports DebtPlus.UI.Client.controls
Imports DebtPlus.LINQ
Imports System.Linq
Imports System.Collections.Generic

Namespace forms
    Friend Class Form_BalanceDeposit
        Implements IContext

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        ' Previous deposit amount from the client
        Private OldDepositAmount As Decimal = 0D

        ''' <summary>
        ''' Event to indicate the deposit amount was changed
        ''' </summary>
        ''' <remarks></remarks>
        Public Event ClientDepositAmountChanged As ClientDepositAmountChangedEventHandler

        ''' <summary>
        ''' Raise the ClientDepositAmountChanged event
        ''' </summary>
        Protected Sub RaiseClientDepositAmountChanged(ByVal e As ClientDepositAmountChangedEventArgs)
            RaiseEvent ClientDepositAmountChanged(Me, e)
        End Sub

        ''' <summary>
        ''' Raise the ClientDepositAmountChanged event
        ''' </summary>
        Protected Overridable Sub OnClientDepositAmountChanged(ByVal e As ClientDepositAmountChangedEventArgs)
            RaiseClientDepositAmountChanged(e)
        End Sub

        Private bc As BusinessContext = Nothing

        ''' <summary>
        ''' Initialize the form
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Load, AddressOf Form_BalanceDeposit_Load
            AddHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
            AddHandler SimpleButton_Prorate.Click, AddressOf SimpleButton_Prorate_Click
            AddHandler CalcEdit1.EditValueChanging, AddressOf CalcEdit1_EditValueChanging
        End Sub

        Public Sub New(ByVal Context As ClientUpdateClass)
            MyClass.New()
            privateContext = Context
            bc = Context.ClientDs.bc
        End Sub

        ''' <summary>
        ''' Pointer to the client update information for the current client
        ''' </summary>
        Private privateContext As ClientUpdateClass = Nothing

        Public ReadOnly Property Context() As ClientUpdateClass Implements IContext.Context
            Get
                Return privateContext
            End Get
        End Property

        ''' <summary>
        ''' Handle the LOAD event for the form
        ''' </summary>
        Private Sub Form_BalanceDeposit_Load(ByVal sender As Object, ByVal e As EventArgs)

            Try
                ' Determine the deposit figures for the current amount
                OldDepositAmount = Context.ClientDs.colClientDeposits.Where(Function(s) Not s.one_time).Sum(Function(s) s.deposit_amount)
                CalcEdit1.EditValue = OldDepositAmount

                ' Disable the buttons until the figures change
                SimpleButton_OK.Enabled = (OldDepositAmount >= 0D)

                ' Read the debt list and load it into the display grid for updates.
                AdjustDepositDebtControl1.ReadForm(bc)

            Catch ex As SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
                SimpleButton_OK.Enabled = False
                CalcEdit1.Enabled = False
            End Try
        End Sub

        ''' <summary>
        ''' Process the CLICK event on the OK button
        ''' </summary>
        Private Sub SimpleButton_OK_Click(ByVal sender As Object, ByVal e As EventArgs)

            ' If the old figure did not change then we need only look for a negative amount.
            Dim newDepositAmount As Decimal = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit1.EditValue).GetValueOrDefault(0D)

            ' Verify that the deposit amount agrees with the disbursement figures
            CheckNegativeDeposit(bc, newDepositAmount)

            Try
                ' Correct the debt disbursement factor table list
                AdjustDepositDebtControl1.SaveForm(bc)

                ' If there are no deposit records and the amount is still zero then we are complete.
                If Context.ClientDs.colClientDeposits.Where(Function(s) Not s.one_time).Count = 0 AndAlso newDepositAmount <= 0D Then
                    DialogResult = Windows.Forms.DialogResult.OK
                    Return
                End If

                ' If there are no deposit records then create a new one
                If Context.ClientDs.colClientDeposits.Where(Function(s) Not s.one_time).Count = 0 Then

                    Dim record As DebtPlus.LINQ.client_deposit = DebtPlus.LINQ.Factory.Manufacture_client_deposit()
                    record.one_time = False
                    record.client = Context.ClientDs.ClientId
                    record.deposit_amount = newDepositAmount
                    record.deposit_date = New DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1)

                    bc.client_deposits.InsertOnSubmit(record)
                    Context.ClientDs.colClientDeposits.Add(record)
                    GenerateSystemNote(bc, 0D, newDepositAmount)
                    bc.SubmitChanges()

                    ' Tell the user that the figures changed and complete the dialog normally
                    OnClientDepositAmountChanged(New ClientDepositAmountChangedEventArgs(0D, newDepositAmount))
                    DialogResult = Windows.Forms.DialogResult.OK
                    Return
                End If

                ' If there is only one record then just update the amount
                OldDepositAmount = Context.ClientDs.colClientDeposits.Where(Function(s) Not s.one_time).Sum(Function(s) s.deposit_amount)
                If Context.ClientDs.colClientDeposits.Where(Function(s) Not s.one_time).Count = 1 Then
                    Dim record As DebtPlus.LINQ.client_deposit = Context.ClientDs.colClientDeposits.Find(Function(s) Not s.one_time)
                    record.deposit_amount = newDepositAmount
                    GenerateSystemNote(bc, OldDepositAmount, newDepositAmount)
                    bc.SubmitChanges()

                    OnClientDepositAmountChanged(New ClientDepositAmountChangedEventArgs(OldDepositAmount, newDepositAmount))
                    DialogResult = Windows.Forms.DialogResult.OK
                    Return
                End If

                ' There are more than one. Do a proportional adjustment of the amount across all of the records
                Context.ClientDs.colClientDeposits.Where(Function(s) Not s.one_time).ToList().ForEach(Sub(s) s.deposit_amount = Convert.ToDecimal(Convert.ToInt64((Convert.ToDouble(newDepositAmount) * Convert.ToDouble(s.deposit_amount)) / Convert.ToDouble(OldDepositAmount) * 100.0)) / 100D)

                ' Ensure that the amount comes out to the penny
                Dim currentFigure As Decimal = Context.ClientDs.colClientDeposits.Where(Function(s) Not s.one_time).Sum(Function(s) s.deposit_amount)
                Dim difference As Decimal = newDepositAmount - currentFigure
                If difference < 0D Then
                    Dim record As DebtPlus.LINQ.client_deposit = Context.ClientDs.colClientDeposits.Where(Function(s) Not s.one_time).OrderByDescending(Function(s) s.deposit_amount).First()
                    record.deposit_amount = record.deposit_amount + difference
                ElseIf difference > 0D Then
                    Dim record As DebtPlus.LINQ.client_deposit = Context.ClientDs.colClientDeposits.Where(Function(s) Not s.one_time).OrderBy(Function(s) s.deposit_amount).First()
                    record.deposit_amount = record.deposit_amount + difference
                End If

                GenerateSystemNote(bc, OldDepositAmount, newDepositAmount)
                bc.SubmitChanges()

                OnClientDepositAmountChanged(New ClientDepositAmountChangedEventArgs(OldDepositAmount, newDepositAmount))
                DialogResult = Windows.Forms.DialogResult.OK

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
            End Try
        End Sub

        ''' <summary>
        ''' Generate a system note for the client when the deposit amount changes
        ''' </summary>
        Private Sub GenerateSystemNote(bc As BusinessContext, ByVal oldValue As Decimal, ByVal newValue As Decimal)

            Dim note As client_note = DebtPlus.LINQ.Factory.Manufacture_client_note(Context.ClientDs.ClientId, 3)
            note.subject = "Changed expected deposit amount"
            note.note = String.Format("Changed the expected client deposit amount from {0:c} to {1:c}", oldValue, newValue)
            bc.client_notes.InsertOnSubmit(note)
        End Sub

        ''' <summary>
        ''' Look for the negative payout information for this client.
        ''' </summary>
        Private Function CheckNegativeDeposit(bc As BusinessContext, currentClientDepositAmount As Decimal) As Boolean

            ' Find the current state for the client.
            Dim clientState As Int32 = Context.ClientDs.clientAddress.state
            Dim stateRecord As state = DebtPlus.LINQ.Cache.state.getList().Find(Function(s) s.Id = clientState)
            If stateRecord Is Nothing OrElse Not stateRecord.NegativeDebtWarning Then
                Return True
            End If

            Try
                Dim budgetExpenses As Decimal = 0D

                ' Find the latest budget for the client. We simply take the last one created.
                Dim q As DebtPlus.LINQ.budget = bc.budgets.Where(Function(s) s.client = Context.ClientDs.ClientId).OrderByDescending(Function(s) s.date_created).FirstOrDefault()
                If q IsNot Nothing Then
                    Dim budgetId As Int32 = q.Id
                    budgetExpenses = bc.budget_details.Where(Function(s) s.budget = budgetId).Sum(Function(s) s.suggested_amount)
                End If

                ' Find the other debt payments
                Dim otherExpenses As Decimal = Context.ClientDs.colOtherDebts.Where(Function(s) s.client = Context.ClientDs.ClientId AndAlso s.payment IsNot Nothing).Sum(Function(s) s.payment).GetValueOrDefault()

                ' Find the client payroll income
                Dim payrollIncome As Decimal = Context.ClientDs.colPeople.Sum(Function(s) s.net_income)

                ' Find the other monthly income
                Dim colAssets As System.Collections.Generic.List(Of DebtPlus.LINQ.asset) = bc.assets.Where(Function(s) s.client = Context.ClientDs.ClientId).ToList()
                Dim otherIncome As Decimal = colAssets.Sum(Function(s) s.asset_amount)

                ' Determine the total client income
                Dim totalIncome As Decimal = payrollIncome + otherIncome

                ' Determine the total client expenses
                Dim totalExpenses As Decimal = budgetExpenses + otherExpenses + currentClientDepositAmount

                ' If there is sufficient income to meet the expenses then we do not want a warning.
                Dim deficit As Decimal = totalExpenses - totalIncome
                If deficit <= 0D Then
                    Return True
                End If

                ' Generate some detail showing the dollar figures for the calculations.
                Dim sb As New System.Text.StringBuilder()
                sb.AppendLine()

                ' Income
                sb.AppendFormat("Payroll income = {0:c}{1}", payrollIncome, Environment.NewLine)
                sb.AppendFormat("Additional income = {0:c}{1}", otherIncome, Environment.NewLine)
                sb.AppendFormat("Total Monthly Income = {0:c}{1}", totalIncome, Environment.NewLine)
                sb.AppendLine()

                ' Expenses
                sb.AppendFormat("Budgeted Expenses = {0:c}{1}", budgetExpenses, Environment.NewLine)
                sb.AppendFormat("Plan payments = {0:c}{1}", currentClientDepositAmount, Environment.NewLine)
                sb.AppendFormat("Other expenses = {0:c}{1}", otherExpenses, Environment.NewLine)
                sb.AppendFormat("Total Monthly Expenses = {0:c}{1}", totalExpenses, Environment.NewLine)
                sb.AppendLine()

                ' Summary
                sb.AppendFormat("Deficit = {0:c}{1}", deficit, Environment.NewLine)

                Dim note As client_note = DebtPlus.LINQ.Factory.Manufacture_client_note(Context.ClientDs.ClientId, 3)
                note.subject = "Budget is negative for this client"
                note.note = "Budget is negative for this client" + sb.ToString()
                bc.client_notes.InsertOnSubmit(note)

                sb.Insert(0, Environment.NewLine)
                sb.Insert(0, "Per state regulation, this client is not eligible for DMP")
                sb.AppendLine()
                sb.Append("Refer to State Info tab.")

                DebtPlus.Data.Forms.MessageBox.Show(sb.ToString(), "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return False

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
            End Try
        End Function

        ''' <summary>
        ''' Handle the prorate operation when it is desired to prorate the money to the debts.
        ''' </summary>
        Private Sub SimpleButton_Prorate_Click(ByVal sender As Object, ByVal e As EventArgs)
            AdjustDepositDebtControl1.ProrateDebts(Convert.ToDecimal(CalcEdit1.EditValue))
        End Sub

        ''' <summary>
        ''' Ensure that the user does not enter an invalid amount into the deposit field.
        ''' </summary>
        Private Sub CalcEdit1_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)
            Dim NewAmount As Decimal = 0D
            If e.NewValue IsNot Nothing AndAlso e.NewValue IsNot DBNull.Value Then NewAmount = Convert.ToDecimal(e.NewValue)
            SimpleButton_OK.Enabled = (NewAmount > 0D)
        End Sub
    End Class
End Namespace
