﻿Namespace forms
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form_ReservedInTrust
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
            Me.SimpleButton_OK = New DevExpress.XtraEditors.SimpleButton()
            Me.SimpleButton_Cancel = New DevExpress.XtraEditors.SimpleButton()
            Me.CalcEdit1 = New DevExpress.XtraEditors.CalcEdit()
            Me.DateEdit1 = New DevExpress.XtraEditors.DateEdit()
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(13, 75)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(67, 13)
            Me.LabelControl1.TabIndex = 1
            Me.LabelControl1.Text = "Dollar Amount"
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(12, 101)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(64, 13)
            Me.LabelControl2.TabIndex = 3
            Me.LabelControl2.Text = "Release Date"
            '
            'SimpleButton_OK
            '
            Me.SimpleButton_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.SimpleButton_OK.Location = New System.Drawing.Point(58, 137)
            Me.SimpleButton_OK.Name = "SimpleButton_OK"
            Me.SimpleButton_OK.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.TabIndex = 5
            Me.SimpleButton_OK.Text = "&OK"
            '
            'SimpleButton_Cancel
            '
            Me.SimpleButton_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.SimpleButton_Cancel.Location = New System.Drawing.Point(159, 136)
            Me.SimpleButton_Cancel.Name = "SimpleButton_Cancel"
            Me.SimpleButton_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.TabIndex = 6
            Me.SimpleButton_Cancel.Text = "&Cancel"
            '
            'CalcEdit1
            '
            Me.CalcEdit1.Location = New System.Drawing.Point(116, 72)
            Me.CalcEdit1.Name = "CalcEdit1"
            Me.CalcEdit1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.CalcEdit1.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit1.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.CalcEdit1.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit1.Properties.AppearanceDropDown.Options.UseTextOptions = True
            Me.CalcEdit1.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit1.Properties.AppearanceFocused.Options.UseTextOptions = True
            Me.CalcEdit1.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit1.Properties.AppearanceReadOnly.Options.UseTextOptions = True
            Me.CalcEdit1.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit1.Properties.DisplayFormat.FormatString = "{0:c}"
            Me.CalcEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit1.Properties.EditFormat.FormatString = "{0:c}"
            Me.CalcEdit1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit1.Properties.Mask.BeepOnError = True
            Me.CalcEdit1.Properties.Mask.EditMask = "c"
            Me.CalcEdit1.Properties.Precision = 2
            Me.CalcEdit1.Size = New System.Drawing.Size(100, 20)
            Me.CalcEdit1.TabIndex = 2
            '
            'DateEdit1
            '
            Me.DateEdit1.EditValue = Nothing
            Me.DateEdit1.Location = New System.Drawing.Point(116, 98)
            Me.DateEdit1.Name = "DateEdit1"
            Me.DateEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.DateEdit1.Properties.ShowToday = False
            Me.DateEdit1.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.DateEdit1.Size = New System.Drawing.Size(100, 20)
            Me.DateEdit1.TabIndex = 4
            '
            'LabelControl3
            '
            Me.LabelControl3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                             Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LabelControl3.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LabelControl3.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl3.Location = New System.Drawing.Point(13, 13)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(267, 44)
            Me.LabelControl3.TabIndex = 0
            Me.LabelControl3.Text = "Enter the new cutoff date and new amount to be reserved in the client's trust fro" & _
                                    "m auto disbursement operations."
            Me.LabelControl3.UseMnemonic = False
            '
            'Form_ReservedInTrust
            '
            Me.AcceptButton = Me.SimpleButton_OK
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.SimpleButton_Cancel
            Me.ClientSize = New System.Drawing.Size(292, 175)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.DateEdit1)
            Me.Controls.Add(Me.CalcEdit1)
            Me.Controls.Add(Me.SimpleButton_Cancel)
            Me.Controls.Add(Me.SimpleButton_OK)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LabelControl1)
            Me.LookAndFeel.UseDefaultLookAndFeel = True
            Me.Name = "Form_ReservedInTrust"
            Me.Text = "Reserved Amount In Trust"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents SimpleButton_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents CalcEdit1 As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents DateEdit1 As DevExpress.XtraEditors.DateEdit
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    End Class
End Namespace