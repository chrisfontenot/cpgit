﻿Namespace forms
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form_ActionPlan
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                    MyDispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Dim OptionsSpelling1 As DevExpress.XtraSpellChecker.OptionsSpelling = New DevExpress.XtraSpellChecker.OptionsSpelling()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_ActionPlan))
            Me.barManager1 = New DevExpress.XtraBars.BarManager(Me.components)
            Me.Bar_MainMenu = New DevExpress.XtraBars.Bar()
            Me.BarSubItem_File = New DevExpress.XtraBars.BarSubItem()
            Me.BarButtonItem_File_QuickPrint = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_File_Print = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_File_PrintPreview = New DevExpress.XtraBars.BarButtonItem()
            Me.BarSubItem_Edit = New DevExpress.XtraBars.BarSubItem()
            Me.BarButtonItem_Edit_Undo = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Edit_Redo = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Edit_Cut = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Edit_Copy = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Edit_Paste = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Edit_Delete = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Edit_SelectAll = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Edit_Find = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Edit_Replace = New DevExpress.XtraBars.BarButtonItem()
            Me.BarSubItem_View = New DevExpress.XtraBars.BarSubItem()
            Me.BarCheckItem_View_Normal = New DevExpress.XtraBars.BarCheckItem()
            Me.BarCheckItem_View_PageLayout = New DevExpress.XtraBars.BarCheckItem()
            Me.BarToolbarsListItem_View_Toolbars = New DevExpress.XtraBars.BarToolbarsListItem()
            Me.BarCheckItem_View_StatusBar = New DevExpress.XtraBars.BarCheckItem()
            Me.BarSubItem_View_Zoom = New DevExpress.XtraBars.BarSubItem()
            Me.BarCheckItem_View_Zoom_025 = New DevExpress.XtraBars.BarCheckItem()
            Me.BarCheckItem_View_Zoom_050 = New DevExpress.XtraBars.BarCheckItem()
            Me.BarCheckItem_View_Zoom_075 = New DevExpress.XtraBars.BarCheckItem()
            Me.BarCheckItem_View_Zoom_100 = New DevExpress.XtraBars.BarCheckItem()
            Me.BarCheckItem_View_Zoom_150 = New DevExpress.XtraBars.BarCheckItem()
            Me.BarCheckItem_View_Zoom_200 = New DevExpress.XtraBars.BarCheckItem()
            Me.BarCheckItem_View_Zoom_300 = New DevExpress.XtraBars.BarCheckItem()
            Me.BarSubItem_Insert = New DevExpress.XtraBars.BarSubItem()
            Me.BarButtonItem_Insert_File = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Insert_Image = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Insert_PageBreak = New DevExpress.XtraBars.BarButtonItem()
            Me.BarSubItem_Format = New DevExpress.XtraBars.BarSubItem()
            Me.BarButtonItem_Format_Character = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Format_Paragraph = New DevExpress.XtraBars.BarButtonItem()
            Me.BarSubItem_Format_Bullets = New DevExpress.XtraBars.BarSubItem()
            Me.BarCheckItem_Format_Bullets_Bullets = New DevExpress.XtraBars.BarCheckItem()
            Me.BarCheckItem_Format_Bullets_Numbers = New DevExpress.XtraBars.BarCheckItem()
            Me.BarCheckItem_Format_Bullets_Multilevel = New DevExpress.XtraBars.BarCheckItem()
            Me.BarEditItem_Format_TextColor = New DevExpress.XtraBars.BarEditItem()
            Me.RepositoryItemColorEdit_TextColor = New DevExpress.XtraEditors.Repository.RepositoryItemColorEdit()
            Me.BarEditItem_Format_TextBackColor = New DevExpress.XtraBars.BarEditItem()
            Me.RepositoryItemColorEdit_TextBackgoundColor = New DevExpress.XtraEditors.Repository.RepositoryItemColorEdit()
            Me.BarEditItem_Format_DocumentColor = New DevExpress.XtraBars.BarEditItem()
            Me.RepositoryItemColorEdit_DocumentBackgroundColor = New DevExpress.XtraEditors.Repository.RepositoryItemColorEdit()
            Me.BarSubItem_Tools = New DevExpress.XtraBars.BarSubItem()
            Me.BarButtonItem_Tools_Spell = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Tools_SpellerOptions = New DevExpress.XtraBars.BarButtonItem()
            Me.Bar_StatusBar = New DevExpress.XtraBars.Bar()
            Me.FontBar1 = New DevExpress.XtraRichEdit.UI.FontBar()
            Me.ChangeFontNameItem1 = New DevExpress.XtraRichEdit.UI.ChangeFontNameItem()
            Me.RepositoryItemFontEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemFontEdit()
            Me.ChangeFontSizeItem1 = New DevExpress.XtraRichEdit.UI.ChangeFontSizeItem()
            Me.RepositoryItemRichEditFontSizeEdit1 = New DevExpress.XtraRichEdit.Design.RepositoryItemRichEditFontSizeEdit()
            Me.RichEditControl1 = New DevExpress.XtraRichEdit.RichEditControl()
            Me.SpellChecker1 = New DevExpress.XtraSpellChecker.SpellChecker()
            Me.ChangeFontColorItem1 = New DevExpress.XtraRichEdit.UI.ChangeFontColorItem()
            Me.ChangeFontBackColorItem1 = New DevExpress.XtraRichEdit.UI.ChangeFontBackColorItem()
            Me.ToggleFontBoldItem1 = New DevExpress.XtraRichEdit.UI.ToggleFontBoldItem()
            Me.ToggleFontItalicItem1 = New DevExpress.XtraRichEdit.UI.ToggleFontItalicItem()
            Me.ToggleFontUnderlineItem1 = New DevExpress.XtraRichEdit.UI.ToggleFontUnderlineItem()
            Me.ToggleFontDoubleUnderlineItem1 = New DevExpress.XtraRichEdit.UI.ToggleFontDoubleUnderlineItem()
            Me.ToggleFontStrikeoutItem1 = New DevExpress.XtraRichEdit.UI.ToggleFontStrikeoutItem()
            Me.ToggleFontDoubleStrikeoutItem1 = New DevExpress.XtraRichEdit.UI.ToggleFontDoubleStrikeoutItem()
            Me.ToggleFontSuperscriptItem1 = New DevExpress.XtraRichEdit.UI.ToggleFontSuperscriptItem()
            Me.ToggleFontSubscriptItem1 = New DevExpress.XtraRichEdit.UI.ToggleFontSubscriptItem()
            Me.FontSizeIncreaseItem1 = New DevExpress.XtraRichEdit.UI.FontSizeIncreaseItem()
            Me.FontSizeDecreaseItem1 = New DevExpress.XtraRichEdit.UI.FontSizeDecreaseItem()
            Me.ShowFontFormItem1 = New DevExpress.XtraRichEdit.UI.ShowFontFormItem()
            Me.ParagraphBar1 = New DevExpress.XtraRichEdit.UI.ParagraphBar()
            Me.ToggleParagraphAlignmentLeftItem1 = New DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentLeftItem()
            Me.ToggleParagraphAlignmentCenterItem1 = New DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentCenterItem()
            Me.ToggleParagraphAlignmentRightItem1 = New DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentRightItem()
            Me.ToggleParagraphAlignmentJustifyItem1 = New DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentJustifyItem()
            Me.ToggleNumberingListItem1 = New DevExpress.XtraRichEdit.UI.ToggleNumberingListItem()
            Me.ToggleBulletedListItem1 = New DevExpress.XtraRichEdit.UI.ToggleBulletedListItem()
            Me.ToggleMultiLevelListItem1 = New DevExpress.XtraRichEdit.UI.ToggleMultiLevelListItem()
            Me.DecreaseIndentItem1 = New DevExpress.XtraRichEdit.UI.DecreaseIndentItem()
            Me.IncreaseIndentItem1 = New DevExpress.XtraRichEdit.UI.IncreaseIndentItem()
            Me.ToggleShowWhitespaceItem1 = New DevExpress.XtraRichEdit.UI.ToggleShowWhitespaceItem()
            Me.ShowParagraphFormItem1 = New DevExpress.XtraRichEdit.UI.ShowParagraphFormItem()
            Me.ClipboardBar1 = New DevExpress.XtraRichEdit.UI.ClipboardBar()
            Me.CutItem1 = New DevExpress.XtraRichEdit.UI.CutItem()
            Me.CopyItem1 = New DevExpress.XtraRichEdit.UI.CopyItem()
            Me.PasteItem1 = New DevExpress.XtraRichEdit.UI.PasteItem()
            Me.CommonBar1 = New DevExpress.XtraRichEdit.UI.CommonBar()
            Me.FileNewItem1 = New DevExpress.XtraRichEdit.UI.FileNewItem()
            Me.FileOpenItem1 = New DevExpress.XtraRichEdit.UI.FileOpenItem()
            Me.FileSaveItem1 = New DevExpress.XtraRichEdit.UI.FileSaveItem()
            Me.FileSaveAsItem1 = New DevExpress.XtraRichEdit.UI.FileSaveAsItem()
            Me.QuickPrintItem1 = New DevExpress.XtraRichEdit.UI.QuickPrintItem()
            Me.PrintItem1 = New DevExpress.XtraRichEdit.UI.PrintItem()
            Me.PrintPreviewItem1 = New DevExpress.XtraRichEdit.UI.PrintPreviewItem()
            Me.UndoItem1 = New DevExpress.XtraRichEdit.UI.UndoItem()
            Me.RedoItem1 = New DevExpress.XtraRichEdit.UI.RedoItem()
            Me.EditingBar1 = New DevExpress.XtraRichEdit.UI.EditingBar()
            Me.FindItem1 = New DevExpress.XtraRichEdit.UI.FindItem()
            Me.ReplaceItem1 = New DevExpress.XtraRichEdit.UI.ReplaceItem()
            Me.StylesBar1 = New DevExpress.XtraRichEdit.UI.StylesBar()
            Me.ChangeStyleItem1 = New DevExpress.XtraRichEdit.UI.ChangeStyleItem()
            Me.RepositoryItemRichEditStyleEdit1 = New DevExpress.XtraRichEdit.Design.RepositoryItemRichEditStyleEdit()
            Me.ZoomBar1 = New DevExpress.XtraRichEdit.UI.ZoomBar()
            Me.ZoomOutItem1 = New DevExpress.XtraRichEdit.UI.ZoomOutItem()
            Me.ZoomInItem1 = New DevExpress.XtraRichEdit.UI.ZoomInItem()
            Me.IllustrationsBar1 = New DevExpress.XtraRichEdit.UI.IllustrationsBar()
            Me.InsertPictureItem1 = New DevExpress.XtraRichEdit.UI.InsertPictureItem()
            Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
            Me.BarButtonItem_View_PageLayout = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
            Me.CheckedListBoxControl_Required = New DevExpress.XtraEditors.CheckedListBoxControl()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.CheckedListBoxControl_Expenses = New DevExpress.XtraEditors.CheckedListBoxControl()
            Me.CheckedListBoxControl_ToDo = New DevExpress.XtraEditors.CheckedListBoxControl()
            Me.SimpleButton_Cancel = New DevExpress.XtraEditors.SimpleButton()
            Me.SimpleButton_OK = New DevExpress.XtraEditors.SimpleButton()
            Me.CheckedListBoxControl_Income = New DevExpress.XtraEditors.CheckedListBoxControl()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.TabbedControlGroup1 = New DevExpress.XtraLayout.TabbedControlGroup()
            Me.LayoutControlGroup3 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlGroup4 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlGroup5 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlGroup6 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.RichEditBarController1 = New DevExpress.XtraRichEdit.UI.RichEditBarController()
            Me.SharedDictionaryStorage1 = New DevExpress.XtraSpellChecker.SharedDictionaryStorage(Me.components)
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RepositoryItemColorEdit_TextColor, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RepositoryItemColorEdit_TextBackgoundColor, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RepositoryItemColorEdit_DocumentBackgroundColor, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RepositoryItemFontEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RepositoryItemRichEditFontSizeEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RepositoryItemRichEditStyleEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckedListBoxControl_Required, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.CheckedListBoxControl_Expenses, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckedListBoxControl_ToDo, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckedListBoxControl_Income, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TabbedControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RichEditBarController1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'DefaultLookAndFeel1
            '
            Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin"
            '
            'barManager1
            '
            Me.barManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar_MainMenu, Me.Bar_StatusBar, Me.FontBar1, Me.ParagraphBar1, Me.ClipboardBar1, Me.CommonBar1, Me.EditingBar1, Me.StylesBar1, Me.ZoomBar1, Me.IllustrationsBar1})
            Me.barManager1.DockControls.Add(Me.barDockControlTop)
            Me.barManager1.DockControls.Add(Me.barDockControlBottom)
            Me.barManager1.DockControls.Add(Me.barDockControlLeft)
            Me.barManager1.DockControls.Add(Me.barDockControlRight)
            Me.barManager1.Form = Me
            Me.barManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarSubItem_File, Me.BarSubItem_Edit, Me.BarSubItem_View, Me.BarSubItem_Insert, Me.BarSubItem_Format, Me.BarSubItem_Tools, Me.BarButtonItem_File_QuickPrint, Me.BarButtonItem_File_Print, Me.BarButtonItem_File_PrintPreview, Me.BarButtonItem_Edit_Cut, Me.BarButtonItem_Edit_Copy, Me.BarButtonItem_Edit_Paste, Me.BarButtonItem_Edit_SelectAll, Me.BarButtonItem_Edit_Delete, Me.BarButtonItem_Edit_Find, Me.BarButtonItem_Edit_Replace, Me.BarCheckItem_View_Normal, Me.BarButtonItem_View_PageLayout, Me.BarToolbarsListItem_View_Toolbars, Me.BarCheckItem_View_StatusBar, Me.BarSubItem_View_Zoom, Me.BarButtonItem1, Me.BarCheckItem_View_Zoom_025, Me.BarCheckItem_View_Zoom_050, Me.BarCheckItem_View_Zoom_075, Me.BarCheckItem_View_Zoom_100, Me.BarCheckItem_View_Zoom_150, Me.BarCheckItem_View_Zoom_200, Me.BarCheckItem_View_Zoom_300, Me.BarButtonItem_Insert_File, Me.BarButtonItem_Insert_Image, Me.BarButtonItem_Insert_PageBreak, Me.BarButtonItem_Format_Character, Me.BarSubItem_Format_Bullets, Me.BarEditItem_Format_TextColor, Me.BarEditItem_Format_TextBackColor, Me.BarEditItem_Format_DocumentColor, Me.BarButtonItem_Format_Paragraph, Me.BarCheckItem_Format_Bullets_Bullets, Me.BarCheckItem_Format_Bullets_Numbers, Me.BarCheckItem_Format_Bullets_Multilevel, Me.BarButtonItem_Tools_Spell, Me.ChangeFontNameItem1, Me.ChangeFontSizeItem1, Me.ChangeFontColorItem1, Me.ChangeFontBackColorItem1, Me.ToggleFontBoldItem1, Me.ToggleFontItalicItem1, Me.ToggleFontUnderlineItem1, Me.ToggleFontDoubleUnderlineItem1, Me.ToggleFontStrikeoutItem1, Me.ToggleFontDoubleStrikeoutItem1, Me.ToggleFontSuperscriptItem1, Me.ToggleFontSubscriptItem1, Me.FontSizeIncreaseItem1, Me.FontSizeDecreaseItem1, Me.ShowFontFormItem1, Me.ToggleParagraphAlignmentLeftItem1, Me.ToggleParagraphAlignmentCenterItem1, Me.ToggleParagraphAlignmentRightItem1, Me.ToggleParagraphAlignmentJustifyItem1, Me.ToggleNumberingListItem1, Me.ToggleBulletedListItem1, Me.ToggleMultiLevelListItem1, Me.DecreaseIndentItem1, Me.IncreaseIndentItem1, Me.ToggleShowWhitespaceItem1, Me.ShowParagraphFormItem1, Me.CutItem1, Me.CopyItem1, Me.PasteItem1, Me.FileNewItem1, Me.FileOpenItem1, Me.FileSaveItem1, Me.FileSaveAsItem1, Me.QuickPrintItem1, Me.PrintItem1, Me.PrintPreviewItem1, Me.UndoItem1, Me.RedoItem1, Me.FindItem1, Me.ReplaceItem1, Me.ChangeStyleItem1, Me.ZoomOutItem1, Me.ZoomInItem1, Me.InsertPictureItem1, Me.BarCheckItem_View_PageLayout, Me.BarButtonItem_Edit_Undo, Me.BarButtonItem_Edit_Redo, Me.BarButtonItem_Tools_SpellerOptions})
            Me.barManager1.MainMenu = Me.Bar_MainMenu
            Me.barManager1.MaxItemId = 98
            Me.barManager1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemColorEdit_TextColor, Me.RepositoryItemColorEdit_TextBackgoundColor, Me.RepositoryItemColorEdit_DocumentBackgroundColor, Me.RepositoryItemFontEdit1, Me.RepositoryItemRichEditFontSizeEdit1, Me.RepositoryItemRichEditStyleEdit1})
            Me.barManager1.StatusBar = Me.Bar_StatusBar
            '
            'Bar_MainMenu
            '
            Me.Bar_MainMenu.BarName = "Main menu"
            Me.Bar_MainMenu.DockCol = 0
            Me.Bar_MainMenu.DockRow = 0
            Me.Bar_MainMenu.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
            Me.Bar_MainMenu.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem_File), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem_Edit), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem_View), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem_Insert), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem_Format), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem_Tools)})
            Me.Bar_MainMenu.OptionsBar.MultiLine = True
            Me.Bar_MainMenu.OptionsBar.UseWholeRow = True
            Me.Bar_MainMenu.Text = "Main menu"
            '
            'BarSubItem_File
            '
            Me.BarSubItem_File.Caption = "File"
            Me.BarSubItem_File.Id = 0
            Me.BarSubItem_File.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_File_QuickPrint), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_File_Print), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_File_PrintPreview)})
            Me.BarSubItem_File.Name = "BarSubItem_File"
            Me.BarSubItem_File.Tag = "file"
            '
            'BarButtonItem_File_QuickPrint
            '
            Me.BarButtonItem_File_QuickPrint.Caption = "Print"
            Me.BarButtonItem_File_QuickPrint.Id = 7
            Me.BarButtonItem_File_QuickPrint.Name = "BarButtonItem_File_QuickPrint"
            Me.BarButtonItem_File_QuickPrint.Tag = "file.print.quick"
            '
            'BarButtonItem_File_Print
            '
            Me.BarButtonItem_File_Print.Caption = "Print..."
            Me.BarButtonItem_File_Print.Id = 8
            Me.BarButtonItem_File_Print.Name = "BarButtonItem_File_Print"
            Me.BarButtonItem_File_Print.Tag = "file.print"
            '
            'BarButtonItem_File_PrintPreview
            '
            Me.BarButtonItem_File_PrintPreview.Caption = "Print Preview..."
            Me.BarButtonItem_File_PrintPreview.Id = 9
            Me.BarButtonItem_File_PrintPreview.Name = "BarButtonItem_File_PrintPreview"
            Me.BarButtonItem_File_PrintPreview.Tag = "file.print.preview"
            '
            'BarSubItem_Edit
            '
            Me.BarSubItem_Edit.Caption = "Edit"
            Me.BarSubItem_Edit.Id = 1
            Me.BarSubItem_Edit.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Edit_Undo), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Edit_Redo), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Edit_Cut, True), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Edit_Copy), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Edit_Paste), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Edit_Delete, True), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Edit_SelectAll), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Edit_Find, True), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Edit_Replace)})
            Me.BarSubItem_Edit.Name = "BarSubItem_Edit"
            Me.BarSubItem_Edit.Tag = "edit"
            '
            'BarButtonItem_Edit_Undo
            '
            Me.BarButtonItem_Edit_Undo.Caption = "Undo"
            Me.BarButtonItem_Edit_Undo.Id = 92
            Me.BarButtonItem_Edit_Undo.Name = "BarButtonItem_Edit_Undo"
            Me.BarButtonItem_Edit_Undo.Tag = "edit.undo"
            '
            'BarButtonItem_Edit_Redo
            '
            Me.BarButtonItem_Edit_Redo.Caption = "Redo"
            Me.BarButtonItem_Edit_Redo.Id = 93
            Me.BarButtonItem_Edit_Redo.Name = "BarButtonItem_Edit_Redo"
            Me.BarButtonItem_Edit_Redo.Tag = "edit.redo"
            '
            'BarButtonItem_Edit_Cut
            '
            Me.BarButtonItem_Edit_Cut.Caption = "Cut"
            Me.BarButtonItem_Edit_Cut.Id = 10
            Me.BarButtonItem_Edit_Cut.Name = "BarButtonItem_Edit_Cut"
            Me.BarButtonItem_Edit_Cut.Tag = "edit.cut"
            '
            'BarButtonItem_Edit_Copy
            '
            Me.BarButtonItem_Edit_Copy.Caption = "Copy"
            Me.BarButtonItem_Edit_Copy.Id = 11
            Me.BarButtonItem_Edit_Copy.Name = "BarButtonItem_Edit_Copy"
            Me.BarButtonItem_Edit_Copy.Tag = "edit.copy"
            '
            'BarButtonItem_Edit_Paste
            '
            Me.BarButtonItem_Edit_Paste.Caption = "Paste"
            Me.BarButtonItem_Edit_Paste.Id = 12
            Me.BarButtonItem_Edit_Paste.Name = "BarButtonItem_Edit_Paste"
            Me.BarButtonItem_Edit_Paste.Tag = "edit.paste"
            '
            'BarButtonItem_Edit_Delete
            '
            Me.BarButtonItem_Edit_Delete.Caption = "Delete"
            Me.BarButtonItem_Edit_Delete.Id = 14
            Me.BarButtonItem_Edit_Delete.Name = "BarButtonItem_Edit_Delete"
            Me.BarButtonItem_Edit_Delete.Tag = "edit.delete"
            '
            'BarButtonItem_Edit_SelectAll
            '
            Me.BarButtonItem_Edit_SelectAll.Caption = "Select All"
            Me.BarButtonItem_Edit_SelectAll.Id = 13
            Me.BarButtonItem_Edit_SelectAll.Name = "BarButtonItem_Edit_SelectAll"
            Me.BarButtonItem_Edit_SelectAll.Tag = "edit.selectall"
            '
            'BarButtonItem_Edit_Find
            '
            Me.BarButtonItem_Edit_Find.Caption = "Find..."
            Me.BarButtonItem_Edit_Find.Id = 15
            Me.BarButtonItem_Edit_Find.Name = "BarButtonItem_Edit_Find"
            Me.BarButtonItem_Edit_Find.Tag = "edit.find"
            '
            'BarButtonItem_Edit_Replace
            '
            Me.BarButtonItem_Edit_Replace.Caption = "Replace..."
            Me.BarButtonItem_Edit_Replace.Id = 16
            Me.BarButtonItem_Edit_Replace.Name = "BarButtonItem_Edit_Replace"
            Me.BarButtonItem_Edit_Replace.Tag = "edit.replace"
            '
            'BarSubItem_View
            '
            Me.BarSubItem_View.Caption = "View"
            Me.BarSubItem_View.Id = 2
            Me.BarSubItem_View.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarCheckItem_View_Normal), New DevExpress.XtraBars.LinkPersistInfo(Me.BarCheckItem_View_PageLayout), New DevExpress.XtraBars.LinkPersistInfo(Me.BarToolbarsListItem_View_Toolbars, True), New DevExpress.XtraBars.LinkPersistInfo(Me.BarCheckItem_View_StatusBar), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem_View_Zoom, True)})
            Me.BarSubItem_View.Name = "BarSubItem_View"
            Me.BarSubItem_View.Tag = "view"
            '
            'BarCheckItem_View_Normal
            '
            Me.BarCheckItem_View_Normal.Caption = "Normal"
            Me.BarCheckItem_View_Normal.Id = 17
            Me.BarCheckItem_View_Normal.Name = "BarCheckItem_View_Normal"
            Me.BarCheckItem_View_Normal.Tag = "view.normal"
            '
            'BarCheckItem_View_PageLayout
            '
            Me.BarCheckItem_View_PageLayout.Caption = "Page Layout"
            Me.BarCheckItem_View_PageLayout.Id = 91
            Me.BarCheckItem_View_PageLayout.Name = "BarCheckItem_View_PageLayout"
            Me.BarCheckItem_View_PageLayout.Tag = "view.pagelayout"
            '
            'BarToolbarsListItem_View_Toolbars
            '
            Me.BarToolbarsListItem_View_Toolbars.Caption = "Tool Bars"
            Me.BarToolbarsListItem_View_Toolbars.Id = 19
            Me.BarToolbarsListItem_View_Toolbars.Name = "BarToolbarsListItem_View_Toolbars"
            Me.BarToolbarsListItem_View_Toolbars.Tag = "view.toolbars"
            '
            'BarCheckItem_View_StatusBar
            '
            Me.BarCheckItem_View_StatusBar.Caption = "Status Bar"
            Me.BarCheckItem_View_StatusBar.Id = 20
            Me.BarCheckItem_View_StatusBar.Name = "BarCheckItem_View_StatusBar"
            Me.BarCheckItem_View_StatusBar.Tag = "view.statusbar"
            '
            'BarSubItem_View_Zoom
            '
            Me.BarSubItem_View_Zoom.Caption = "Zoom"
            Me.BarSubItem_View_Zoom.Id = 21
            Me.BarSubItem_View_Zoom.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarCheckItem_View_Zoom_025), New DevExpress.XtraBars.LinkPersistInfo(Me.BarCheckItem_View_Zoom_050), New DevExpress.XtraBars.LinkPersistInfo(Me.BarCheckItem_View_Zoom_075), New DevExpress.XtraBars.LinkPersistInfo(Me.BarCheckItem_View_Zoom_100), New DevExpress.XtraBars.LinkPersistInfo(Me.BarCheckItem_View_Zoom_150), New DevExpress.XtraBars.LinkPersistInfo(Me.BarCheckItem_View_Zoom_200), New DevExpress.XtraBars.LinkPersistInfo(Me.BarCheckItem_View_Zoom_300)})
            Me.BarSubItem_View_Zoom.Name = "BarSubItem_View_Zoom"
            Me.BarSubItem_View_Zoom.Tag = "view.zoom"
            '
            'BarCheckItem_View_Zoom_025
            '
            Me.BarCheckItem_View_Zoom_025.Caption = "25%"
            Me.BarCheckItem_View_Zoom_025.Id = 23
            Me.BarCheckItem_View_Zoom_025.Name = "BarCheckItem_View_Zoom_025"
            Me.BarCheckItem_View_Zoom_025.Tag = "view.zoom.025"
            '
            'BarCheckItem_View_Zoom_050
            '
            Me.BarCheckItem_View_Zoom_050.Caption = "50%"
            Me.BarCheckItem_View_Zoom_050.Id = 24
            Me.BarCheckItem_View_Zoom_050.Name = "BarCheckItem_View_Zoom_050"
            Me.BarCheckItem_View_Zoom_050.Tag = "view.zoom.050"
            '
            'BarCheckItem_View_Zoom_075
            '
            Me.BarCheckItem_View_Zoom_075.Caption = "75%"
            Me.BarCheckItem_View_Zoom_075.Id = 25
            Me.BarCheckItem_View_Zoom_075.Name = "BarCheckItem_View_Zoom_075"
            Me.BarCheckItem_View_Zoom_075.Tag = "view.zoom.075"
            '
            'BarCheckItem_View_Zoom_100
            '
            Me.BarCheckItem_View_Zoom_100.Caption = "100%"
            Me.BarCheckItem_View_Zoom_100.Id = 26
            Me.BarCheckItem_View_Zoom_100.Name = "BarCheckItem_View_Zoom_100"
            Me.BarCheckItem_View_Zoom_100.Tag = "view.zoom.100"
            '
            'BarCheckItem_View_Zoom_150
            '
            Me.BarCheckItem_View_Zoom_150.Caption = "150%"
            Me.BarCheckItem_View_Zoom_150.Id = 27
            Me.BarCheckItem_View_Zoom_150.Name = "BarCheckItem_View_Zoom_150"
            Me.BarCheckItem_View_Zoom_150.Tag = "view.zoom.150"
            '
            'BarCheckItem_View_Zoom_200
            '
            Me.BarCheckItem_View_Zoom_200.Caption = "200%"
            Me.BarCheckItem_View_Zoom_200.Id = 28
            Me.BarCheckItem_View_Zoom_200.Name = "BarCheckItem_View_Zoom_200"
            Me.BarCheckItem_View_Zoom_200.Tag = "view.zoom.200"
            '
            'BarCheckItem_View_Zoom_300
            '
            Me.BarCheckItem_View_Zoom_300.Caption = "300%"
            Me.BarCheckItem_View_Zoom_300.Id = 29
            Me.BarCheckItem_View_Zoom_300.Name = "BarCheckItem_View_Zoom_300"
            Me.BarCheckItem_View_Zoom_300.Tag = "view.zoom.300"
            '
            'BarSubItem_Insert
            '
            Me.BarSubItem_Insert.Caption = "Insert"
            Me.BarSubItem_Insert.Id = 4
            Me.BarSubItem_Insert.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Insert_File), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Insert_Image), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Insert_PageBreak)})
            Me.BarSubItem_Insert.Name = "BarSubItem_Insert"
            Me.BarSubItem_Insert.Tag = "insert"
            '
            'BarButtonItem_Insert_File
            '
            Me.BarButtonItem_Insert_File.Caption = "File..."
            Me.BarButtonItem_Insert_File.Id = 33
            Me.BarButtonItem_Insert_File.Name = "BarButtonItem_Insert_File"
            Me.BarButtonItem_Insert_File.Tag = "insert.file"
            '
            'BarButtonItem_Insert_Image
            '
            Me.BarButtonItem_Insert_Image.Caption = "Image..."
            Me.BarButtonItem_Insert_Image.Id = 34
            Me.BarButtonItem_Insert_Image.Name = "BarButtonItem_Insert_Image"
            Me.BarButtonItem_Insert_Image.Tag = "insert.image"
            '
            'BarButtonItem_Insert_PageBreak
            '
            Me.BarButtonItem_Insert_PageBreak.Caption = "Page Break"
            Me.BarButtonItem_Insert_PageBreak.Id = 35
            Me.BarButtonItem_Insert_PageBreak.Name = "BarButtonItem_Insert_PageBreak"
            Me.BarButtonItem_Insert_PageBreak.Tag = "insert.pagebreak"
            '
            'BarSubItem_Format
            '
            Me.BarSubItem_Format.Caption = "Format"
            Me.BarSubItem_Format.Id = 5
            Me.BarSubItem_Format.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Format_Character), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Format_Paragraph), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem_Format_Bullets, True), New DevExpress.XtraBars.LinkPersistInfo(Me.BarEditItem_Format_TextColor, True), New DevExpress.XtraBars.LinkPersistInfo(Me.BarEditItem_Format_TextBackColor), New DevExpress.XtraBars.LinkPersistInfo(Me.BarEditItem_Format_DocumentColor)})
            Me.BarSubItem_Format.Name = "BarSubItem_Format"
            Me.BarSubItem_Format.Tag = "format"
            '
            'BarButtonItem_Format_Character
            '
            Me.BarButtonItem_Format_Character.Caption = "Character..."
            Me.BarButtonItem_Format_Character.Id = 36
            Me.BarButtonItem_Format_Character.Name = "BarButtonItem_Format_Character"
            Me.BarButtonItem_Format_Character.Tag = "format.character"
            '
            'BarButtonItem_Format_Paragraph
            '
            Me.BarButtonItem_Format_Paragraph.Caption = "Paragraph..."
            Me.BarButtonItem_Format_Paragraph.Id = 42
            Me.BarButtonItem_Format_Paragraph.Name = "BarButtonItem_Format_Paragraph"
            Me.BarButtonItem_Format_Paragraph.Tag = "format.paragrah"
            '
            'BarSubItem_Format_Bullets
            '
            Me.BarSubItem_Format_Bullets.Caption = "Bullets and Numbering"
            Me.BarSubItem_Format_Bullets.Id = 38
            Me.BarSubItem_Format_Bullets.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarCheckItem_Format_Bullets_Bullets), New DevExpress.XtraBars.LinkPersistInfo(Me.BarCheckItem_Format_Bullets_Numbers), New DevExpress.XtraBars.LinkPersistInfo(Me.BarCheckItem_Format_Bullets_Multilevel)})
            Me.BarSubItem_Format_Bullets.Name = "BarSubItem_Format_Bullets"
            Me.BarSubItem_Format_Bullets.Tag = "format.bullets"
            '
            'BarCheckItem_Format_Bullets_Bullets
            '
            Me.BarCheckItem_Format_Bullets_Bullets.Caption = "Bullets"
            Me.BarCheckItem_Format_Bullets_Bullets.Id = 43
            Me.BarCheckItem_Format_Bullets_Bullets.Name = "BarCheckItem_Format_Bullets_Bullets"
            Me.BarCheckItem_Format_Bullets_Bullets.Tag = "format.bullets.bullets"
            '
            'BarCheckItem_Format_Bullets_Numbers
            '
            Me.BarCheckItem_Format_Bullets_Numbers.Caption = "Numbers"
            Me.BarCheckItem_Format_Bullets_Numbers.Id = 44
            Me.BarCheckItem_Format_Bullets_Numbers.Name = "BarCheckItem_Format_Bullets_Numbers"
            Me.BarCheckItem_Format_Bullets_Numbers.Tag = "format.bullets.numbers"
            '
            'BarCheckItem_Format_Bullets_Multilevel
            '
            Me.BarCheckItem_Format_Bullets_Multilevel.Caption = "Multi-Level"
            Me.BarCheckItem_Format_Bullets_Multilevel.Id = 45
            Me.BarCheckItem_Format_Bullets_Multilevel.Name = "BarCheckItem_Format_Bullets_Multilevel"
            Me.BarCheckItem_Format_Bullets_Multilevel.Tag = "format.bullets.multilevel"
            '
            'BarEditItem_Format_TextColor
            '
            Me.BarEditItem_Format_TextColor.Caption = "Text Color"
            Me.BarEditItem_Format_TextColor.Edit = Me.RepositoryItemColorEdit_TextColor
            Me.BarEditItem_Format_TextColor.Id = 39
            Me.BarEditItem_Format_TextColor.Name = "BarEditItem_Format_TextColor"
            Me.BarEditItem_Format_TextColor.Tag = "format.textcolor"
            '
            'RepositoryItemColorEdit_TextColor
            '
            Me.RepositoryItemColorEdit_TextColor.AutoHeight = False
            Me.RepositoryItemColorEdit_TextColor.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.RepositoryItemColorEdit_TextColor.Name = "RepositoryItemColorEdit_TextColor"
            '
            'BarEditItem_Format_TextBackColor
            '
            Me.BarEditItem_Format_TextBackColor.Caption = "Text Background Color"
            Me.BarEditItem_Format_TextBackColor.Edit = Me.RepositoryItemColorEdit_TextBackgoundColor
            Me.BarEditItem_Format_TextBackColor.Id = 40
            Me.BarEditItem_Format_TextBackColor.Name = "BarEditItem_Format_TextBackColor"
            Me.BarEditItem_Format_TextBackColor.Tag = "format.textbackcolor"
            '
            'RepositoryItemColorEdit_TextBackgoundColor
            '
            Me.RepositoryItemColorEdit_TextBackgoundColor.AutoHeight = False
            Me.RepositoryItemColorEdit_TextBackgoundColor.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.RepositoryItemColorEdit_TextBackgoundColor.Name = "RepositoryItemColorEdit_TextBackgoundColor"
            '
            'BarEditItem_Format_DocumentColor
            '
            Me.BarEditItem_Format_DocumentColor.Caption = "Document Background Color"
            Me.BarEditItem_Format_DocumentColor.Edit = Me.RepositoryItemColorEdit_DocumentBackgroundColor
            Me.BarEditItem_Format_DocumentColor.Id = 41
            Me.BarEditItem_Format_DocumentColor.Name = "BarEditItem_Format_DocumentColor"
            Me.BarEditItem_Format_DocumentColor.Tag = "format.documentcolor"
            '
            'RepositoryItemColorEdit_DocumentBackgroundColor
            '
            Me.RepositoryItemColorEdit_DocumentBackgroundColor.AutoHeight = False
            Me.RepositoryItemColorEdit_DocumentBackgroundColor.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.RepositoryItemColorEdit_DocumentBackgroundColor.Name = "RepositoryItemColorEdit_DocumentBackgroundColor"
            '
            'BarSubItem_Tools
            '
            Me.BarSubItem_Tools.Caption = "Tools"
            Me.BarSubItem_Tools.Id = 6
            Me.BarSubItem_Tools.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Tools_Spell), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Tools_SpellerOptions)})
            Me.BarSubItem_Tools.Name = "BarSubItem_Tools"
            Me.BarSubItem_Tools.Tag = "tools"
            '
            'BarButtonItem_Tools_Spell
            '
            Me.BarButtonItem_Tools_Spell.Caption = "Spell..."
            Me.BarButtonItem_Tools_Spell.Id = 46
            Me.BarButtonItem_Tools_Spell.Name = "BarButtonItem_Tools_Spell"
            Me.BarButtonItem_Tools_Spell.Tag = "tools.spell"
            '
            'BarButtonItem_Tools_SpellerOptions
            '
            Me.BarButtonItem_Tools_SpellerOptions.Caption = "Speller Options..."
            Me.BarButtonItem_Tools_SpellerOptions.Id = 97
            Me.BarButtonItem_Tools_SpellerOptions.Name = "BarButtonItem_Tools_SpellerOptions"
            '
            'Bar_StatusBar
            '
            Me.Bar_StatusBar.BarName = "Status bar"
            Me.Bar_StatusBar.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom
            Me.Bar_StatusBar.DockCol = 0
            Me.Bar_StatusBar.DockRow = 0
            Me.Bar_StatusBar.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
            Me.Bar_StatusBar.OptionsBar.AllowQuickCustomization = False
            Me.Bar_StatusBar.OptionsBar.DrawDragBorder = False
            Me.Bar_StatusBar.OptionsBar.UseWholeRow = True
            Me.Bar_StatusBar.Text = "Status bar"
            Me.Bar_StatusBar.Visible = False
            '
            'FontBar1
            '
            Me.FontBar1.DockCol = 0
            Me.FontBar1.DockRow = 1
            Me.FontBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
            Me.FontBar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.ChangeFontNameItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.ChangeFontSizeItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.ChangeFontColorItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.ChangeFontBackColorItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.ToggleFontBoldItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.ToggleFontItalicItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.ToggleFontUnderlineItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.ToggleFontDoubleUnderlineItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.ToggleFontStrikeoutItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.ToggleFontDoubleStrikeoutItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.ToggleFontSuperscriptItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.ToggleFontSubscriptItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.FontSizeIncreaseItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.FontSizeDecreaseItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.ShowFontFormItem1)})
            Me.FontBar1.Visible = False
            '
            'ChangeFontNameItem1
            '
            Me.ChangeFontNameItem1.Edit = Me.RepositoryItemFontEdit1
            Me.ChangeFontNameItem1.Id = 47
            Me.ChangeFontNameItem1.Name = "ChangeFontNameItem1"
            '
            'RepositoryItemFontEdit1
            '
            Me.RepositoryItemFontEdit1.AutoHeight = False
            Me.RepositoryItemFontEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.RepositoryItemFontEdit1.Name = "RepositoryItemFontEdit1"
            '
            'ChangeFontSizeItem1
            '
            Me.ChangeFontSizeItem1.Edit = Me.RepositoryItemRichEditFontSizeEdit1
            Me.ChangeFontSizeItem1.Id = 48
            Me.ChangeFontSizeItem1.Name = "ChangeFontSizeItem1"
            '
            'RepositoryItemRichEditFontSizeEdit1
            '
            Me.RepositoryItemRichEditFontSizeEdit1.AutoHeight = False
            Me.RepositoryItemRichEditFontSizeEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.RepositoryItemRichEditFontSizeEdit1.Control = Me.RichEditControl1
            Me.RepositoryItemRichEditFontSizeEdit1.Name = "RepositoryItemRichEditFontSizeEdit1"
            '
            'RichEditControl1
            '
            Me.RichEditControl1.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple
            Me.RichEditControl1.Appearance.Text.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.RichEditControl1.Appearance.Text.Options.UseFont = True
            Me.RichEditControl1.Location = New System.Drawing.Point(24, 46)
            Me.RichEditControl1.MenuManager = Me.barManager1
            Me.RichEditControl1.Name = "RichEditControl1"
            Me.RichEditControl1.Options.DocumentSaveOptions.CurrentFormat = DevExpress.XtraRichEdit.DocumentFormat.Rtf
            Me.SpellChecker1.SetShowSpellCheckMenu(Me.RichEditControl1, False)
            Me.RichEditControl1.Size = New System.Drawing.Size(464, 159)
            Me.RichEditControl1.SpellChecker = Me.SpellChecker1
            Me.SpellChecker1.SetSpellCheckerOptions(Me.RichEditControl1, OptionsSpelling1)
            Me.RichEditControl1.TabIndex = 11
            '
            'SpellChecker1
            '
            Me.SpellChecker1.Culture = New System.Globalization.CultureInfo("en-US")
            Me.SpellChecker1.ParentContainer = Nothing
            '
            'ChangeFontColorItem1
            '
            Me.ChangeFontColorItem1.Glyph = CType(resources.GetObject("ChangeFontColorItem1.Glyph"), System.Drawing.Image)
            Me.ChangeFontColorItem1.Id = 49
            Me.ChangeFontColorItem1.LargeGlyph = CType(resources.GetObject("ChangeFontColorItem1.LargeGlyph"), System.Drawing.Image)
            Me.ChangeFontColorItem1.Name = "ChangeFontColorItem1"
            '
            'ChangeFontBackColorItem1
            '
            Me.ChangeFontBackColorItem1.Glyph = CType(resources.GetObject("ChangeFontBackColorItem1.Glyph"), System.Drawing.Image)
            Me.ChangeFontBackColorItem1.Id = 50
            Me.ChangeFontBackColorItem1.LargeGlyph = CType(resources.GetObject("ChangeFontBackColorItem1.LargeGlyph"), System.Drawing.Image)
            Me.ChangeFontBackColorItem1.Name = "ChangeFontBackColorItem1"
            '
            'ToggleFontBoldItem1
            '
            Me.ToggleFontBoldItem1.Glyph = CType(resources.GetObject("ToggleFontBoldItem1.Glyph"), System.Drawing.Image)
            Me.ToggleFontBoldItem1.Id = 51
            Me.ToggleFontBoldItem1.LargeGlyph = CType(resources.GetObject("ToggleFontBoldItem1.LargeGlyph"), System.Drawing.Image)
            Me.ToggleFontBoldItem1.Name = "ToggleFontBoldItem1"
            '
            'ToggleFontItalicItem1
            '
            Me.ToggleFontItalicItem1.Glyph = CType(resources.GetObject("ToggleFontItalicItem1.Glyph"), System.Drawing.Image)
            Me.ToggleFontItalicItem1.Id = 52
            Me.ToggleFontItalicItem1.LargeGlyph = CType(resources.GetObject("ToggleFontItalicItem1.LargeGlyph"), System.Drawing.Image)
            Me.ToggleFontItalicItem1.Name = "ToggleFontItalicItem1"
            '
            'ToggleFontUnderlineItem1
            '
            Me.ToggleFontUnderlineItem1.Glyph = CType(resources.GetObject("ToggleFontUnderlineItem1.Glyph"), System.Drawing.Image)
            Me.ToggleFontUnderlineItem1.Id = 53
            Me.ToggleFontUnderlineItem1.LargeGlyph = CType(resources.GetObject("ToggleFontUnderlineItem1.LargeGlyph"), System.Drawing.Image)
            Me.ToggleFontUnderlineItem1.Name = "ToggleFontUnderlineItem1"
            '
            'ToggleFontDoubleUnderlineItem1
            '
            Me.ToggleFontDoubleUnderlineItem1.Glyph = CType(resources.GetObject("ToggleFontDoubleUnderlineItem1.Glyph"), System.Drawing.Image)
            Me.ToggleFontDoubleUnderlineItem1.Id = 54
            Me.ToggleFontDoubleUnderlineItem1.LargeGlyph = CType(resources.GetObject("ToggleFontDoubleUnderlineItem1.LargeGlyph"), System.Drawing.Image)
            Me.ToggleFontDoubleUnderlineItem1.Name = "ToggleFontDoubleUnderlineItem1"
            '
            'ToggleFontStrikeoutItem1
            '
            Me.ToggleFontStrikeoutItem1.Glyph = CType(resources.GetObject("ToggleFontStrikeoutItem1.Glyph"), System.Drawing.Image)
            Me.ToggleFontStrikeoutItem1.Id = 55
            Me.ToggleFontStrikeoutItem1.LargeGlyph = CType(resources.GetObject("ToggleFontStrikeoutItem1.LargeGlyph"), System.Drawing.Image)
            Me.ToggleFontStrikeoutItem1.Name = "ToggleFontStrikeoutItem1"
            '
            'ToggleFontDoubleStrikeoutItem1
            '
            Me.ToggleFontDoubleStrikeoutItem1.Glyph = CType(resources.GetObject("ToggleFontDoubleStrikeoutItem1.Glyph"), System.Drawing.Image)
            Me.ToggleFontDoubleStrikeoutItem1.Id = 56
            Me.ToggleFontDoubleStrikeoutItem1.LargeGlyph = CType(resources.GetObject("ToggleFontDoubleStrikeoutItem1.LargeGlyph"), System.Drawing.Image)
            Me.ToggleFontDoubleStrikeoutItem1.Name = "ToggleFontDoubleStrikeoutItem1"
            '
            'ToggleFontSuperscriptItem1
            '
            Me.ToggleFontSuperscriptItem1.Glyph = CType(resources.GetObject("ToggleFontSuperscriptItem1.Glyph"), System.Drawing.Image)
            Me.ToggleFontSuperscriptItem1.Id = 57
            Me.ToggleFontSuperscriptItem1.LargeGlyph = CType(resources.GetObject("ToggleFontSuperscriptItem1.LargeGlyph"), System.Drawing.Image)
            Me.ToggleFontSuperscriptItem1.Name = "ToggleFontSuperscriptItem1"
            '
            'ToggleFontSubscriptItem1
            '
            Me.ToggleFontSubscriptItem1.Glyph = CType(resources.GetObject("ToggleFontSubscriptItem1.Glyph"), System.Drawing.Image)
            Me.ToggleFontSubscriptItem1.Id = 58
            Me.ToggleFontSubscriptItem1.LargeGlyph = CType(resources.GetObject("ToggleFontSubscriptItem1.LargeGlyph"), System.Drawing.Image)
            Me.ToggleFontSubscriptItem1.Name = "ToggleFontSubscriptItem1"
            '
            'FontSizeIncreaseItem1
            '
            Me.FontSizeIncreaseItem1.Glyph = CType(resources.GetObject("FontSizeIncreaseItem1.Glyph"), System.Drawing.Image)
            Me.FontSizeIncreaseItem1.Id = 59
            Me.FontSizeIncreaseItem1.LargeGlyph = CType(resources.GetObject("FontSizeIncreaseItem1.LargeGlyph"), System.Drawing.Image)
            Me.FontSizeIncreaseItem1.Name = "FontSizeIncreaseItem1"
            '
            'FontSizeDecreaseItem1
            '
            Me.FontSizeDecreaseItem1.Glyph = CType(resources.GetObject("FontSizeDecreaseItem1.Glyph"), System.Drawing.Image)
            Me.FontSizeDecreaseItem1.Id = 60
            Me.FontSizeDecreaseItem1.LargeGlyph = CType(resources.GetObject("FontSizeDecreaseItem1.LargeGlyph"), System.Drawing.Image)
            Me.FontSizeDecreaseItem1.Name = "FontSizeDecreaseItem1"
            '
            'ShowFontFormItem1
            '
            Me.ShowFontFormItem1.Glyph = CType(resources.GetObject("ShowFontFormItem1.Glyph"), System.Drawing.Image)
            Me.ShowFontFormItem1.Id = 61
            Me.ShowFontFormItem1.LargeGlyph = CType(resources.GetObject("ShowFontFormItem1.LargeGlyph"), System.Drawing.Image)
            Me.ShowFontFormItem1.Name = "ShowFontFormItem1"
            '
            'ParagraphBar1
            '
            Me.ParagraphBar1.DockCol = 1
            Me.ParagraphBar1.DockRow = 1
            Me.ParagraphBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
            Me.ParagraphBar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.ToggleParagraphAlignmentLeftItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.ToggleParagraphAlignmentCenterItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.ToggleParagraphAlignmentRightItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.ToggleParagraphAlignmentJustifyItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.ToggleNumberingListItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.ToggleBulletedListItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.ToggleMultiLevelListItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.DecreaseIndentItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.IncreaseIndentItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.ToggleShowWhitespaceItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.ShowParagraphFormItem1)})
            Me.ParagraphBar1.Visible = False
            '
            'ToggleParagraphAlignmentLeftItem1
            '
            Me.ToggleParagraphAlignmentLeftItem1.Glyph = CType(resources.GetObject("ToggleParagraphAlignmentLeftItem1.Glyph"), System.Drawing.Image)
            Me.ToggleParagraphAlignmentLeftItem1.Id = 62
            Me.ToggleParagraphAlignmentLeftItem1.LargeGlyph = CType(resources.GetObject("ToggleParagraphAlignmentLeftItem1.LargeGlyph"), System.Drawing.Image)
            Me.ToggleParagraphAlignmentLeftItem1.Name = "ToggleParagraphAlignmentLeftItem1"
            '
            'ToggleParagraphAlignmentCenterItem1
            '
            Me.ToggleParagraphAlignmentCenterItem1.Glyph = CType(resources.GetObject("ToggleParagraphAlignmentCenterItem1.Glyph"), System.Drawing.Image)
            Me.ToggleParagraphAlignmentCenterItem1.Id = 63
            Me.ToggleParagraphAlignmentCenterItem1.LargeGlyph = CType(resources.GetObject("ToggleParagraphAlignmentCenterItem1.LargeGlyph"), System.Drawing.Image)
            Me.ToggleParagraphAlignmentCenterItem1.Name = "ToggleParagraphAlignmentCenterItem1"
            '
            'ToggleParagraphAlignmentRightItem1
            '
            Me.ToggleParagraphAlignmentRightItem1.Glyph = CType(resources.GetObject("ToggleParagraphAlignmentRightItem1.Glyph"), System.Drawing.Image)
            Me.ToggleParagraphAlignmentRightItem1.Id = 64
            Me.ToggleParagraphAlignmentRightItem1.LargeGlyph = CType(resources.GetObject("ToggleParagraphAlignmentRightItem1.LargeGlyph"), System.Drawing.Image)
            Me.ToggleParagraphAlignmentRightItem1.Name = "ToggleParagraphAlignmentRightItem1"
            '
            'ToggleParagraphAlignmentJustifyItem1
            '
            Me.ToggleParagraphAlignmentJustifyItem1.Glyph = CType(resources.GetObject("ToggleParagraphAlignmentJustifyItem1.Glyph"), System.Drawing.Image)
            Me.ToggleParagraphAlignmentJustifyItem1.Id = 65
            Me.ToggleParagraphAlignmentJustifyItem1.LargeGlyph = CType(resources.GetObject("ToggleParagraphAlignmentJustifyItem1.LargeGlyph"), System.Drawing.Image)
            Me.ToggleParagraphAlignmentJustifyItem1.Name = "ToggleParagraphAlignmentJustifyItem1"
            '
            'ToggleNumberingListItem1
            '
            Me.ToggleNumberingListItem1.Glyph = CType(resources.GetObject("ToggleNumberingListItem1.Glyph"), System.Drawing.Image)
            Me.ToggleNumberingListItem1.Id = 66
            Me.ToggleNumberingListItem1.LargeGlyph = CType(resources.GetObject("ToggleNumberingListItem1.LargeGlyph"), System.Drawing.Image)
            Me.ToggleNumberingListItem1.Name = "ToggleNumberingListItem1"
            '
            'ToggleBulletedListItem1
            '
            Me.ToggleBulletedListItem1.Glyph = CType(resources.GetObject("ToggleBulletedListItem1.Glyph"), System.Drawing.Image)
            Me.ToggleBulletedListItem1.Id = 67
            Me.ToggleBulletedListItem1.LargeGlyph = CType(resources.GetObject("ToggleBulletedListItem1.LargeGlyph"), System.Drawing.Image)
            Me.ToggleBulletedListItem1.Name = "ToggleBulletedListItem1"
            '
            'ToggleMultiLevelListItem1
            '
            Me.ToggleMultiLevelListItem1.Glyph = CType(resources.GetObject("ToggleMultiLevelListItem1.Glyph"), System.Drawing.Image)
            Me.ToggleMultiLevelListItem1.Id = 68
            Me.ToggleMultiLevelListItem1.LargeGlyph = CType(resources.GetObject("ToggleMultiLevelListItem1.LargeGlyph"), System.Drawing.Image)
            Me.ToggleMultiLevelListItem1.Name = "ToggleMultiLevelListItem1"
            '
            'DecreaseIndentItem1
            '
            Me.DecreaseIndentItem1.Glyph = CType(resources.GetObject("DecreaseIndentItem1.Glyph"), System.Drawing.Image)
            Me.DecreaseIndentItem1.Id = 69
            Me.DecreaseIndentItem1.LargeGlyph = CType(resources.GetObject("DecreaseIndentItem1.LargeGlyph"), System.Drawing.Image)
            Me.DecreaseIndentItem1.Name = "DecreaseIndentItem1"
            '
            'IncreaseIndentItem1
            '
            Me.IncreaseIndentItem1.Glyph = CType(resources.GetObject("IncreaseIndentItem1.Glyph"), System.Drawing.Image)
            Me.IncreaseIndentItem1.Id = 70
            Me.IncreaseIndentItem1.LargeGlyph = CType(resources.GetObject("IncreaseIndentItem1.LargeGlyph"), System.Drawing.Image)
            Me.IncreaseIndentItem1.Name = "IncreaseIndentItem1"
            '
            'ToggleShowWhitespaceItem1
            '
            Me.ToggleShowWhitespaceItem1.Glyph = CType(resources.GetObject("ToggleShowWhitespaceItem1.Glyph"), System.Drawing.Image)
            Me.ToggleShowWhitespaceItem1.Id = 71
            Me.ToggleShowWhitespaceItem1.LargeGlyph = CType(resources.GetObject("ToggleShowWhitespaceItem1.LargeGlyph"), System.Drawing.Image)
            Me.ToggleShowWhitespaceItem1.Name = "ToggleShowWhitespaceItem1"
            '
            'ShowParagraphFormItem1
            '
            Me.ShowParagraphFormItem1.Glyph = CType(resources.GetObject("ShowParagraphFormItem1.Glyph"), System.Drawing.Image)
            Me.ShowParagraphFormItem1.Id = 72
            Me.ShowParagraphFormItem1.LargeGlyph = CType(resources.GetObject("ShowParagraphFormItem1.LargeGlyph"), System.Drawing.Image)
            Me.ShowParagraphFormItem1.Name = "ShowParagraphFormItem1"
            '
            'ClipboardBar1
            '
            Me.ClipboardBar1.DockCol = 2
            Me.ClipboardBar1.DockRow = 1
            Me.ClipboardBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
            Me.ClipboardBar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.CutItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.CopyItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.PasteItem1)})
            Me.ClipboardBar1.Visible = False
            '
            'CutItem1
            '
            Me.CutItem1.Glyph = CType(resources.GetObject("CutItem1.Glyph"), System.Drawing.Image)
            Me.CutItem1.Id = 73
            Me.CutItem1.LargeGlyph = CType(resources.GetObject("CutItem1.LargeGlyph"), System.Drawing.Image)
            Me.CutItem1.Name = "CutItem1"
            '
            'CopyItem1
            '
            Me.CopyItem1.Glyph = CType(resources.GetObject("CopyItem1.Glyph"), System.Drawing.Image)
            Me.CopyItem1.Id = 74
            Me.CopyItem1.LargeGlyph = CType(resources.GetObject("CopyItem1.LargeGlyph"), System.Drawing.Image)
            Me.CopyItem1.Name = "CopyItem1"
            '
            'PasteItem1
            '
            Me.PasteItem1.Glyph = CType(resources.GetObject("PasteItem1.Glyph"), System.Drawing.Image)
            Me.PasteItem1.Id = 75
            Me.PasteItem1.LargeGlyph = CType(resources.GetObject("PasteItem1.LargeGlyph"), System.Drawing.Image)
            Me.PasteItem1.Name = "PasteItem1"
            '
            'CommonBar1
            '
            Me.CommonBar1.DockCol = 3
            Me.CommonBar1.DockRow = 1
            Me.CommonBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
            Me.CommonBar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.FileNewItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.FileOpenItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.FileSaveItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.FileSaveAsItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.QuickPrintItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.PrintPreviewItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.UndoItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.RedoItem1)})
            Me.CommonBar1.Visible = False
            '
            'FileNewItem1
            '
            Me.FileNewItem1.Glyph = CType(resources.GetObject("FileNewItem1.Glyph"), System.Drawing.Image)
            Me.FileNewItem1.Id = 76
            Me.FileNewItem1.LargeGlyph = CType(resources.GetObject("FileNewItem1.LargeGlyph"), System.Drawing.Image)
            Me.FileNewItem1.Name = "FileNewItem1"
            '
            'FileOpenItem1
            '
            Me.FileOpenItem1.Glyph = CType(resources.GetObject("FileOpenItem1.Glyph"), System.Drawing.Image)
            Me.FileOpenItem1.Id = 77
            Me.FileOpenItem1.LargeGlyph = CType(resources.GetObject("FileOpenItem1.LargeGlyph"), System.Drawing.Image)
            Me.FileOpenItem1.Name = "FileOpenItem1"
            '
            'FileSaveItem1
            '
            Me.FileSaveItem1.Glyph = CType(resources.GetObject("FileSaveItem1.Glyph"), System.Drawing.Image)
            Me.FileSaveItem1.Id = 78
            Me.FileSaveItem1.LargeGlyph = CType(resources.GetObject("FileSaveItem1.LargeGlyph"), System.Drawing.Image)
            Me.FileSaveItem1.Name = "FileSaveItem1"
            '
            'FileSaveAsItem1
            '
            Me.FileSaveAsItem1.Glyph = CType(resources.GetObject("FileSaveAsItem1.Glyph"), System.Drawing.Image)
            Me.FileSaveAsItem1.Id = 79
            Me.FileSaveAsItem1.LargeGlyph = CType(resources.GetObject("FileSaveAsItem1.LargeGlyph"), System.Drawing.Image)
            Me.FileSaveAsItem1.Name = "FileSaveAsItem1"
            '
            'QuickPrintItem1
            '
            Me.QuickPrintItem1.Glyph = CType(resources.GetObject("QuickPrintItem1.Glyph"), System.Drawing.Image)
            Me.QuickPrintItem1.Id = 80
            Me.QuickPrintItem1.LargeGlyph = CType(resources.GetObject("QuickPrintItem1.LargeGlyph"), System.Drawing.Image)
            Me.QuickPrintItem1.Name = "QuickPrintItem1"
            '
            'PrintItem1
            '
            Me.PrintItem1.Glyph = CType(resources.GetObject("PrintItem1.Glyph"), System.Drawing.Image)
            Me.PrintItem1.Id = 81
            Me.PrintItem1.LargeGlyph = CType(resources.GetObject("PrintItem1.LargeGlyph"), System.Drawing.Image)
            Me.PrintItem1.Name = "PrintItem1"
            '
            'PrintPreviewItem1
            '
            Me.PrintPreviewItem1.Glyph = CType(resources.GetObject("PrintPreviewItem1.Glyph"), System.Drawing.Image)
            Me.PrintPreviewItem1.Id = 82
            Me.PrintPreviewItem1.LargeGlyph = CType(resources.GetObject("PrintPreviewItem1.LargeGlyph"), System.Drawing.Image)
            Me.PrintPreviewItem1.Name = "PrintPreviewItem1"
            '
            'UndoItem1
            '
            Me.UndoItem1.Glyph = CType(resources.GetObject("UndoItem1.Glyph"), System.Drawing.Image)
            Me.UndoItem1.Id = 83
            Me.UndoItem1.LargeGlyph = CType(resources.GetObject("UndoItem1.LargeGlyph"), System.Drawing.Image)
            Me.UndoItem1.Name = "UndoItem1"
            '
            'RedoItem1
            '
            Me.RedoItem1.Glyph = CType(resources.GetObject("RedoItem1.Glyph"), System.Drawing.Image)
            Me.RedoItem1.Id = 84
            Me.RedoItem1.LargeGlyph = CType(resources.GetObject("RedoItem1.LargeGlyph"), System.Drawing.Image)
            Me.RedoItem1.Name = "RedoItem1"
            '
            'EditingBar1
            '
            Me.EditingBar1.DockCol = 4
            Me.EditingBar1.DockRow = 1
            Me.EditingBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
            Me.EditingBar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.FindItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.ReplaceItem1)})
            Me.EditingBar1.Visible = False
            '
            'FindItem1
            '
            Me.FindItem1.Glyph = CType(resources.GetObject("FindItem1.Glyph"), System.Drawing.Image)
            Me.FindItem1.Id = 85
            Me.FindItem1.LargeGlyph = CType(resources.GetObject("FindItem1.LargeGlyph"), System.Drawing.Image)
            Me.FindItem1.Name = "FindItem1"
            '
            'ReplaceItem1
            '
            Me.ReplaceItem1.Glyph = CType(resources.GetObject("ReplaceItem1.Glyph"), System.Drawing.Image)
            Me.ReplaceItem1.Id = 86
            Me.ReplaceItem1.LargeGlyph = CType(resources.GetObject("ReplaceItem1.LargeGlyph"), System.Drawing.Image)
            Me.ReplaceItem1.Name = "ReplaceItem1"
            '
            'StylesBar1
            '
            Me.StylesBar1.DockCol = 5
            Me.StylesBar1.DockRow = 1
            Me.StylesBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
            Me.StylesBar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.ChangeStyleItem1)})
            Me.StylesBar1.Visible = False
            '
            'ChangeStyleItem1
            '
            Me.ChangeStyleItem1.Edit = Me.RepositoryItemRichEditStyleEdit1
            Me.ChangeStyleItem1.Id = 87
            Me.ChangeStyleItem1.Name = "ChangeStyleItem1"
            '
            'RepositoryItemRichEditStyleEdit1
            '
            Me.RepositoryItemRichEditStyleEdit1.AutoHeight = False
            Me.RepositoryItemRichEditStyleEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.RepositoryItemRichEditStyleEdit1.Control = Me.RichEditControl1
            Me.RepositoryItemRichEditStyleEdit1.Name = "RepositoryItemRichEditStyleEdit1"
            '
            'ZoomBar1
            '
            Me.ZoomBar1.DockCol = 6
            Me.ZoomBar1.DockRow = 1
            Me.ZoomBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
            Me.ZoomBar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.ZoomOutItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.ZoomInItem1)})
            Me.ZoomBar1.Visible = False
            '
            'ZoomOutItem1
            '
            Me.ZoomOutItem1.Glyph = CType(resources.GetObject("ZoomOutItem1.Glyph"), System.Drawing.Image)
            Me.ZoomOutItem1.Id = 88
            Me.ZoomOutItem1.LargeGlyph = CType(resources.GetObject("ZoomOutItem1.LargeGlyph"), System.Drawing.Image)
            Me.ZoomOutItem1.Name = "ZoomOutItem1"
            '
            'ZoomInItem1
            '
            Me.ZoomInItem1.Glyph = CType(resources.GetObject("ZoomInItem1.Glyph"), System.Drawing.Image)
            Me.ZoomInItem1.Id = 89
            Me.ZoomInItem1.LargeGlyph = CType(resources.GetObject("ZoomInItem1.LargeGlyph"), System.Drawing.Image)
            Me.ZoomInItem1.Name = "ZoomInItem1"
            '
            'IllustrationsBar1
            '
            Me.IllustrationsBar1.DockCol = 7
            Me.IllustrationsBar1.DockRow = 1
            Me.IllustrationsBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
            Me.IllustrationsBar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.InsertPictureItem1)})
            Me.IllustrationsBar1.Visible = False
            '
            'InsertPictureItem1
            '
            Me.InsertPictureItem1.Glyph = CType(resources.GetObject("InsertPictureItem1.Glyph"), System.Drawing.Image)
            Me.InsertPictureItem1.Id = 90
            Me.InsertPictureItem1.LargeGlyph = CType(resources.GetObject("InsertPictureItem1.LargeGlyph"), System.Drawing.Image)
            Me.InsertPictureItem1.Name = "InsertPictureItem1"
            '
            'barDockControlTop
            '
            Me.barDockControlTop.CausesValidation = False
            Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
            Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
            Me.barDockControlTop.Size = New System.Drawing.Size(512, 51)
            '
            'barDockControlBottom
            '
            Me.barDockControlBottom.CausesValidation = False
            Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
            Me.barDockControlBottom.Location = New System.Drawing.Point(0, 307)
            Me.barDockControlBottom.Size = New System.Drawing.Size(512, 22)
            '
            'barDockControlLeft
            '
            Me.barDockControlLeft.CausesValidation = False
            Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
            Me.barDockControlLeft.Location = New System.Drawing.Point(0, 51)
            Me.barDockControlLeft.Size = New System.Drawing.Size(0, 256)
            '
            'barDockControlRight
            '
            Me.barDockControlRight.CausesValidation = False
            Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
            Me.barDockControlRight.Location = New System.Drawing.Point(512, 51)
            Me.barDockControlRight.Size = New System.Drawing.Size(0, 256)
            '
            'BarButtonItem_View_PageLayout
            '
            Me.BarButtonItem_View_PageLayout.Caption = "Page Layout"
            Me.BarButtonItem_View_PageLayout.Id = 18
            Me.BarButtonItem_View_PageLayout.Name = "BarButtonItem_View_PageLayout"
            Me.BarButtonItem_View_PageLayout.Tag = "view.pagelayout"
            '
            'BarButtonItem1
            '
            Me.BarButtonItem1.Caption = "BarButtonItem1"
            Me.BarButtonItem1.Id = 22
            Me.BarButtonItem1.Name = "BarButtonItem1"
            '
            'CheckedListBoxControl_Required
            '
            Me.CheckedListBoxControl_Required.CheckOnClick = True
            Me.CheckedListBoxControl_Required.Location = New System.Drawing.Point(24, 46)
            Me.CheckedListBoxControl_Required.Name = "CheckedListBoxControl_Required"
            Me.CheckedListBoxControl_Required.Size = New System.Drawing.Size(464, 159)
            Me.CheckedListBoxControl_Required.StyleController = Me.LayoutControl1
            Me.CheckedListBoxControl_Required.TabIndex = 15
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.CheckedListBoxControl_Required)
            Me.LayoutControl1.Controls.Add(Me.CheckedListBoxControl_Expenses)
            Me.LayoutControl1.Controls.Add(Me.CheckedListBoxControl_ToDo)
            Me.LayoutControl1.Controls.Add(Me.RichEditControl1)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton_Cancel)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton_OK)
            Me.LayoutControl1.Controls.Add(Me.CheckedListBoxControl_Income)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 51)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(512, 256)
            Me.LayoutControl1.TabIndex = 4
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'CheckedListBoxControl_Expenses
            '
            Me.CheckedListBoxControl_Expenses.CheckOnClick = True
            Me.CheckedListBoxControl_Expenses.Location = New System.Drawing.Point(24, 46)
            Me.CheckedListBoxControl_Expenses.Name = "CheckedListBoxControl_Expenses"
            Me.CheckedListBoxControl_Expenses.Size = New System.Drawing.Size(464, 159)
            Me.CheckedListBoxControl_Expenses.StyleController = Me.LayoutControl1
            Me.CheckedListBoxControl_Expenses.TabIndex = 14
            '
            'CheckedListBoxControl_ToDo
            '
            Me.CheckedListBoxControl_ToDo.CheckOnClick = True
            Me.CheckedListBoxControl_ToDo.Location = New System.Drawing.Point(24, 46)
            Me.CheckedListBoxControl_ToDo.Name = "CheckedListBoxControl_ToDo"
            Me.CheckedListBoxControl_ToDo.Size = New System.Drawing.Size(464, 159)
            Me.CheckedListBoxControl_ToDo.StyleController = Me.LayoutControl1
            Me.CheckedListBoxControl_ToDo.TabIndex = 12
            '
            'SimpleButton_Cancel
            '
            Me.SimpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.SimpleButton_Cancel.Location = New System.Drawing.Point(249, 221)
            Me.SimpleButton_Cancel.MaximumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.MinimumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.Name = "SimpleButton_Cancel"
            Me.SimpleButton_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.StyleController = Me.LayoutControl1
            Me.SimpleButton_Cancel.TabIndex = 10
            Me.SimpleButton_Cancel.Text = "&Cancel"
            '
            'SimpleButton_OK
            '
            Me.SimpleButton_OK.Location = New System.Drawing.Point(170, 221)
            Me.SimpleButton_OK.MaximumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.MinimumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.Name = "SimpleButton_OK"
            Me.SimpleButton_OK.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.StyleController = Me.LayoutControl1
            Me.SimpleButton_OK.TabIndex = 9
            Me.SimpleButton_OK.Text = "&OK"
            '
            'CheckedListBoxControl_Income
            '
            Me.CheckedListBoxControl_Income.CheckOnClick = True
            Me.CheckedListBoxControl_Income.Location = New System.Drawing.Point(24, 46)
            Me.CheckedListBoxControl_Income.Name = "CheckedListBoxControl_Income"
            Me.CheckedListBoxControl_Income.Size = New System.Drawing.Size(464, 159)
            Me.CheckedListBoxControl_Income.StyleController = Me.LayoutControl1
            Me.CheckedListBoxControl_Income.TabIndex = 13
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.TabbedControlGroup1, Me.LayoutControlItem6, Me.LayoutControlItem7, Me.EmptySpaceItem1, Me.EmptySpaceItem2})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(512, 256)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'TabbedControlGroup1
            '
            Me.TabbedControlGroup1.CustomizationFormText = "TabbedControlGroup1"
            Me.TabbedControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.TabbedControlGroup1.MultiLine = DevExpress.Utils.DefaultBoolean.[False]
            Me.TabbedControlGroup1.Name = "TabbedControlGroup1"
            Me.TabbedControlGroup1.SelectedTabPage = Me.LayoutControlGroup2
            Me.TabbedControlGroup1.SelectedTabPageIndex = 0
            Me.TabbedControlGroup1.Size = New System.Drawing.Size(492, 209)
            Me.TabbedControlGroup1.TabPages.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup2, Me.LayoutControlGroup3, Me.LayoutControlGroup4, Me.LayoutControlGroup5, Me.LayoutControlGroup6})
            Me.TabbedControlGroup1.Text = "TabbedControlGroup1"
            '
            'LayoutControlGroup3
            '
            Me.LayoutControlGroup3.CustomizationFormText = "Things To Do"
            Me.LayoutControlGroup3.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem2})
            Me.LayoutControlGroup3.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup3.Name = "LayoutControlGroup3"
            Me.LayoutControlGroup3.Size = New System.Drawing.Size(468, 163)
            Me.LayoutControlGroup3.Text = "Things To Do"
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.CheckedListBoxControl_ToDo
            Me.LayoutControlItem2.CustomizationFormText = "Things To Do"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(468, 163)
            Me.LayoutControlItem2.Text = "Things To Do"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem2.TextToControlDistance = 0
            Me.LayoutControlItem2.TextVisible = False
            '
            'LayoutControlGroup2
            '
            Me.LayoutControlGroup2.CustomizationFormText = "Goals And Objectives"
            Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1})
            Me.LayoutControlGroup2.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
            Me.LayoutControlGroup2.Size = New System.Drawing.Size(468, 163)
            Me.LayoutControlGroup2.Text = "Goals And Objectives"
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.RichEditControl1
            Me.LayoutControlItem1.CustomizationFormText = "Goals And Objectives"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(468, 163)
            Me.LayoutControlItem1.Text = "Goals And Objectives"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem1.TextToControlDistance = 0
            Me.LayoutControlItem1.TextVisible = False
            '
            'LayoutControlGroup4
            '
            Me.LayoutControlGroup4.CustomizationFormText = "Increase Income"
            Me.LayoutControlGroup4.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem3})
            Me.LayoutControlGroup4.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup4.Name = "LayoutControlGroup4"
            Me.LayoutControlGroup4.Size = New System.Drawing.Size(468, 163)
            Me.LayoutControlGroup4.Text = "Increase Income"
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.CheckedListBoxControl_Income
            Me.LayoutControlItem3.CustomizationFormText = "LayoutControlItem3"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(468, 163)
            Me.LayoutControlItem3.Text = "LayoutControlItem3"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem3.TextToControlDistance = 0
            Me.LayoutControlItem3.TextVisible = False
            '
            'LayoutControlGroup5
            '
            Me.LayoutControlGroup5.CustomizationFormText = "Reduce Expenses"
            Me.LayoutControlGroup5.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem4})
            Me.LayoutControlGroup5.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup5.Name = "LayoutControlGroup5"
            Me.LayoutControlGroup5.Size = New System.Drawing.Size(468, 163)
            Me.LayoutControlGroup5.Text = "Reduce Expenses"
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.CheckedListBoxControl_Expenses
            Me.LayoutControlItem4.CustomizationFormText = "LayoutControlItem4"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(468, 163)
            Me.LayoutControlItem4.Text = "LayoutControlItem4"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem4.TextToControlDistance = 0
            Me.LayoutControlItem4.TextVisible = False
            '
            'LayoutControlGroup6
            '
            Me.LayoutControlGroup6.CustomizationFormText = "Items Required"
            Me.LayoutControlGroup6.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem5})
            Me.LayoutControlGroup6.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup6.Name = "LayoutControlGroup6"
            Me.LayoutControlGroup6.Size = New System.Drawing.Size(468, 163)
            Me.LayoutControlGroup6.Text = "Items Required"
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Control = Me.CheckedListBoxControl_Required
            Me.LayoutControlItem5.CustomizationFormText = "LayoutControlItem5"
            Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem5.Name = "LayoutControlItem5"
            Me.LayoutControlItem5.Size = New System.Drawing.Size(468, 163)
            Me.LayoutControlItem5.Text = "LayoutControlItem5"
            Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem5.TextToControlDistance = 0
            Me.LayoutControlItem5.TextVisible = False
            '
            'LayoutControlItem6
            '
            Me.LayoutControlItem6.Control = Me.SimpleButton_OK
            Me.LayoutControlItem6.CustomizationFormText = "LayoutControlItem6"
            Me.LayoutControlItem6.Location = New System.Drawing.Point(158, 209)
            Me.LayoutControlItem6.Name = "LayoutControlItem6"
            Me.LayoutControlItem6.Size = New System.Drawing.Size(79, 27)
            Me.LayoutControlItem6.Text = "LayoutControlItem6"
            Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem6.TextToControlDistance = 0
            Me.LayoutControlItem6.TextVisible = False
            '
            'LayoutControlItem7
            '
            Me.LayoutControlItem7.Control = Me.SimpleButton_Cancel
            Me.LayoutControlItem7.CustomizationFormText = "LayoutControlItem7"
            Me.LayoutControlItem7.Location = New System.Drawing.Point(237, 209)
            Me.LayoutControlItem7.Name = "LayoutControlItem7"
            Me.LayoutControlItem7.Size = New System.Drawing.Size(79, 27)
            Me.LayoutControlItem7.Text = "LayoutControlItem7"
            Me.LayoutControlItem7.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem7.TextToControlDistance = 0
            Me.LayoutControlItem7.TextVisible = False
            '
            'EmptySpaceItem1
            '
            Me.EmptySpaceItem1.AllowHotTrack = False
            Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 209)
            Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Size = New System.Drawing.Size(158, 27)
            Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
            Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem2
            '
            Me.EmptySpaceItem2.AllowHotTrack = False
            Me.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Location = New System.Drawing.Point(316, 209)
            Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Size = New System.Drawing.Size(176, 27)
            Me.EmptySpaceItem2.Text = "EmptySpaceItem2"
            Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
            '
            'RichEditBarController1
            '
            Me.RichEditBarController1.BarItems.Add(Me.ChangeFontNameItem1)
            Me.RichEditBarController1.BarItems.Add(Me.ChangeFontSizeItem1)
            Me.RichEditBarController1.BarItems.Add(Me.ChangeFontColorItem1)
            Me.RichEditBarController1.BarItems.Add(Me.ChangeFontBackColorItem1)
            Me.RichEditBarController1.BarItems.Add(Me.ToggleFontBoldItem1)
            Me.RichEditBarController1.BarItems.Add(Me.ToggleFontItalicItem1)
            Me.RichEditBarController1.BarItems.Add(Me.ToggleFontUnderlineItem1)
            Me.RichEditBarController1.BarItems.Add(Me.ToggleFontDoubleUnderlineItem1)
            Me.RichEditBarController1.BarItems.Add(Me.ToggleFontStrikeoutItem1)
            Me.RichEditBarController1.BarItems.Add(Me.ToggleFontDoubleStrikeoutItem1)
            Me.RichEditBarController1.BarItems.Add(Me.ToggleFontSuperscriptItem1)
            Me.RichEditBarController1.BarItems.Add(Me.ToggleFontSubscriptItem1)
            Me.RichEditBarController1.BarItems.Add(Me.FontSizeIncreaseItem1)
            Me.RichEditBarController1.BarItems.Add(Me.FontSizeDecreaseItem1)
            Me.RichEditBarController1.BarItems.Add(Me.ShowFontFormItem1)
            Me.RichEditBarController1.BarItems.Add(Me.ToggleParagraphAlignmentLeftItem1)
            Me.RichEditBarController1.BarItems.Add(Me.ToggleParagraphAlignmentCenterItem1)
            Me.RichEditBarController1.BarItems.Add(Me.ToggleParagraphAlignmentRightItem1)
            Me.RichEditBarController1.BarItems.Add(Me.ToggleParagraphAlignmentJustifyItem1)
            Me.RichEditBarController1.BarItems.Add(Me.ToggleNumberingListItem1)
            Me.RichEditBarController1.BarItems.Add(Me.ToggleBulletedListItem1)
            Me.RichEditBarController1.BarItems.Add(Me.ToggleMultiLevelListItem1)
            Me.RichEditBarController1.BarItems.Add(Me.DecreaseIndentItem1)
            Me.RichEditBarController1.BarItems.Add(Me.IncreaseIndentItem1)
            Me.RichEditBarController1.BarItems.Add(Me.ToggleShowWhitespaceItem1)
            Me.RichEditBarController1.BarItems.Add(Me.ShowParagraphFormItem1)
            Me.RichEditBarController1.BarItems.Add(Me.CutItem1)
            Me.RichEditBarController1.BarItems.Add(Me.CopyItem1)
            Me.RichEditBarController1.BarItems.Add(Me.PasteItem1)
            Me.RichEditBarController1.BarItems.Add(Me.FileNewItem1)
            Me.RichEditBarController1.BarItems.Add(Me.FileOpenItem1)
            Me.RichEditBarController1.BarItems.Add(Me.FileSaveItem1)
            Me.RichEditBarController1.BarItems.Add(Me.FileSaveAsItem1)
            Me.RichEditBarController1.BarItems.Add(Me.QuickPrintItem1)
            Me.RichEditBarController1.BarItems.Add(Me.PrintItem1)
            Me.RichEditBarController1.BarItems.Add(Me.PrintPreviewItem1)
            Me.RichEditBarController1.BarItems.Add(Me.UndoItem1)
            Me.RichEditBarController1.BarItems.Add(Me.RedoItem1)
            Me.RichEditBarController1.BarItems.Add(Me.FindItem1)
            Me.RichEditBarController1.BarItems.Add(Me.ReplaceItem1)
            Me.RichEditBarController1.BarItems.Add(Me.ChangeStyleItem1)
            Me.RichEditBarController1.BarItems.Add(Me.ZoomOutItem1)
            Me.RichEditBarController1.BarItems.Add(Me.ZoomInItem1)
            Me.RichEditBarController1.BarItems.Add(Me.InsertPictureItem1)
            Me.RichEditBarController1.RichEditControl = Me.RichEditControl1
            '
            'Form_ActionPlan
            '
            Me.AcceptButton = Me.SimpleButton_OK
            Me.ClientSize = New System.Drawing.Size(512, 329)
            Me.Controls.Add(Me.LayoutControl1)
            Me.Controls.Add(Me.barDockControlLeft)
            Me.Controls.Add(Me.barDockControlRight)
            Me.Controls.Add(Me.barDockControlBottom)
            Me.Controls.Add(Me.barDockControlTop)
            Me.Name = "Form_ActionPlan"
            Me.Text = "Client ActionPlan Information"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RepositoryItemColorEdit_TextColor, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RepositoryItemColorEdit_TextBackgoundColor, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RepositoryItemColorEdit_DocumentBackgroundColor, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RepositoryItemFontEdit1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RepositoryItemRichEditFontSizeEdit1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RepositoryItemRichEditStyleEdit1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckedListBoxControl_Required, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.CheckedListBoxControl_Expenses, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckedListBoxControl_ToDo, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckedListBoxControl_Income, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TabbedControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RichEditBarController1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents barManager1 As DevExpress.XtraBars.BarManager
        Friend WithEvents Bar_MainMenu As DevExpress.XtraBars.Bar
        Friend WithEvents Bar_StatusBar As DevExpress.XtraBars.Bar
        Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
        Friend WithEvents BarSubItem_File As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarSubItem_Edit As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarSubItem_View As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarSubItem_Insert As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarSubItem_Format As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarSubItem_Tools As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarButtonItem_File_QuickPrint As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_File_Print As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_File_PrintPreview As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Edit_Cut As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Edit_Copy As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Edit_Paste As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Edit_Delete As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Edit_SelectAll As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Edit_Find As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Edit_Replace As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarCheckItem_View_Normal As DevExpress.XtraBars.BarCheckItem
        Friend WithEvents BarButtonItem_View_PageLayout As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarToolbarsListItem_View_Toolbars As DevExpress.XtraBars.BarToolbarsListItem
        Friend WithEvents BarCheckItem_View_StatusBar As DevExpress.XtraBars.BarCheckItem
        Friend WithEvents BarSubItem_View_Zoom As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarCheckItem_View_Zoom_025 As DevExpress.XtraBars.BarCheckItem
        Friend WithEvents BarCheckItem_View_Zoom_050 As DevExpress.XtraBars.BarCheckItem
        Friend WithEvents BarCheckItem_View_Zoom_075 As DevExpress.XtraBars.BarCheckItem
        Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarCheckItem_View_Zoom_100 As DevExpress.XtraBars.BarCheckItem
        Friend WithEvents BarCheckItem_View_Zoom_150 As DevExpress.XtraBars.BarCheckItem
        Friend WithEvents BarCheckItem_View_Zoom_200 As DevExpress.XtraBars.BarCheckItem
        Friend WithEvents BarCheckItem_View_Zoom_300 As DevExpress.XtraBars.BarCheckItem
        Friend WithEvents BarButtonItem_Insert_File As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Insert_Image As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Insert_PageBreak As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Format_Character As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarSubItem_Format_Bullets As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarEditItem_Format_TextColor As DevExpress.XtraBars.BarEditItem
        Friend WithEvents RepositoryItemColorEdit_TextColor As DevExpress.XtraEditors.Repository.RepositoryItemColorEdit
        Friend WithEvents BarEditItem_Format_TextBackColor As DevExpress.XtraBars.BarEditItem
        Friend WithEvents RepositoryItemColorEdit_TextBackgoundColor As DevExpress.XtraEditors.Repository.RepositoryItemColorEdit
        Friend WithEvents BarEditItem_Format_DocumentColor As DevExpress.XtraBars.BarEditItem
        Friend WithEvents RepositoryItemColorEdit_DocumentBackgroundColor As DevExpress.XtraEditors.Repository.RepositoryItemColorEdit
        Friend WithEvents BarButtonItem_Format_Paragraph As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarCheckItem_Format_Bullets_Bullets As DevExpress.XtraBars.BarCheckItem
        Friend WithEvents BarCheckItem_Format_Bullets_Numbers As DevExpress.XtraBars.BarCheckItem
        Friend WithEvents BarCheckItem_Format_Bullets_Multilevel As DevExpress.XtraBars.BarCheckItem
        Friend WithEvents BarButtonItem_Tools_Spell As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents ChangeFontNameItem1 As DevExpress.XtraRichEdit.UI.ChangeFontNameItem
        Friend WithEvents RepositoryItemFontEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemFontEdit
        Friend WithEvents ChangeFontSizeItem1 As DevExpress.XtraRichEdit.UI.ChangeFontSizeItem
        Friend WithEvents RepositoryItemRichEditFontSizeEdit1 As DevExpress.XtraRichEdit.Design.RepositoryItemRichEditFontSizeEdit
        Friend WithEvents ChangeFontColorItem1 As DevExpress.XtraRichEdit.UI.ChangeFontColorItem
        Friend WithEvents ChangeFontBackColorItem1 As DevExpress.XtraRichEdit.UI.ChangeFontBackColorItem
        Friend WithEvents ToggleFontBoldItem1 As DevExpress.XtraRichEdit.UI.ToggleFontBoldItem
        Friend WithEvents ToggleFontItalicItem1 As DevExpress.XtraRichEdit.UI.ToggleFontItalicItem
        Friend WithEvents ToggleFontUnderlineItem1 As DevExpress.XtraRichEdit.UI.ToggleFontUnderlineItem
        Friend WithEvents ToggleFontDoubleUnderlineItem1 As DevExpress.XtraRichEdit.UI.ToggleFontDoubleUnderlineItem
        Friend WithEvents ToggleFontStrikeoutItem1 As DevExpress.XtraRichEdit.UI.ToggleFontStrikeoutItem
        Friend WithEvents ToggleFontDoubleStrikeoutItem1 As DevExpress.XtraRichEdit.UI.ToggleFontDoubleStrikeoutItem
        Friend WithEvents ToggleFontSuperscriptItem1 As DevExpress.XtraRichEdit.UI.ToggleFontSuperscriptItem
        Friend WithEvents ToggleFontSubscriptItem1 As DevExpress.XtraRichEdit.UI.ToggleFontSubscriptItem
        Friend WithEvents FontSizeIncreaseItem1 As DevExpress.XtraRichEdit.UI.FontSizeIncreaseItem
        Friend WithEvents FontSizeDecreaseItem1 As DevExpress.XtraRichEdit.UI.FontSizeDecreaseItem
        Friend WithEvents ShowFontFormItem1 As DevExpress.XtraRichEdit.UI.ShowFontFormItem
        Friend WithEvents ToggleParagraphAlignmentLeftItem1 As DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentLeftItem
        Friend WithEvents ToggleParagraphAlignmentCenterItem1 As DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentCenterItem
        Friend WithEvents ToggleParagraphAlignmentRightItem1 As DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentRightItem
        Friend WithEvents ToggleParagraphAlignmentJustifyItem1 As DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentJustifyItem
        Friend WithEvents ToggleNumberingListItem1 As DevExpress.XtraRichEdit.UI.ToggleNumberingListItem
        Friend WithEvents ToggleBulletedListItem1 As DevExpress.XtraRichEdit.UI.ToggleBulletedListItem
        Friend WithEvents ToggleMultiLevelListItem1 As DevExpress.XtraRichEdit.UI.ToggleMultiLevelListItem
        Friend WithEvents DecreaseIndentItem1 As DevExpress.XtraRichEdit.UI.DecreaseIndentItem
        Friend WithEvents IncreaseIndentItem1 As DevExpress.XtraRichEdit.UI.IncreaseIndentItem
        Friend WithEvents ToggleShowWhitespaceItem1 As DevExpress.XtraRichEdit.UI.ToggleShowWhitespaceItem
        Friend WithEvents ShowParagraphFormItem1 As DevExpress.XtraRichEdit.UI.ShowParagraphFormItem
        Friend WithEvents CutItem1 As DevExpress.XtraRichEdit.UI.CutItem
        Friend WithEvents CopyItem1 As DevExpress.XtraRichEdit.UI.CopyItem
        Friend WithEvents PasteItem1 As DevExpress.XtraRichEdit.UI.PasteItem
        Friend WithEvents FileNewItem1 As DevExpress.XtraRichEdit.UI.FileNewItem
        Friend WithEvents FileOpenItem1 As DevExpress.XtraRichEdit.UI.FileOpenItem
        Friend WithEvents FileSaveItem1 As DevExpress.XtraRichEdit.UI.FileSaveItem
        Friend WithEvents FileSaveAsItem1 As DevExpress.XtraRichEdit.UI.FileSaveAsItem
        Friend WithEvents QuickPrintItem1 As DevExpress.XtraRichEdit.UI.QuickPrintItem
        Friend WithEvents PrintItem1 As DevExpress.XtraRichEdit.UI.PrintItem
        Friend WithEvents PrintPreviewItem1 As DevExpress.XtraRichEdit.UI.PrintPreviewItem
        Friend WithEvents UndoItem1 As DevExpress.XtraRichEdit.UI.UndoItem
        Friend WithEvents RedoItem1 As DevExpress.XtraRichEdit.UI.RedoItem
        Friend WithEvents FindItem1 As DevExpress.XtraRichEdit.UI.FindItem
        Friend WithEvents ReplaceItem1 As DevExpress.XtraRichEdit.UI.ReplaceItem
        Friend WithEvents ChangeStyleItem1 As DevExpress.XtraRichEdit.UI.ChangeStyleItem
        Friend WithEvents RepositoryItemRichEditStyleEdit1 As DevExpress.XtraRichEdit.Design.RepositoryItemRichEditStyleEdit
        Friend WithEvents ZoomOutItem1 As DevExpress.XtraRichEdit.UI.ZoomOutItem
        Friend WithEvents ZoomInItem1 As DevExpress.XtraRichEdit.UI.ZoomInItem
        Friend WithEvents InsertPictureItem1 As DevExpress.XtraRichEdit.UI.InsertPictureItem
        Friend WithEvents RichEditBarController1 As DevExpress.XtraRichEdit.UI.RichEditBarController
        Protected Friend WithEvents FontBar1 As DevExpress.XtraRichEdit.UI.FontBar
        Protected Friend WithEvents ParagraphBar1 As DevExpress.XtraRichEdit.UI.ParagraphBar
        Protected Friend WithEvents ClipboardBar1 As DevExpress.XtraRichEdit.UI.ClipboardBar
        Protected Friend WithEvents CommonBar1 As DevExpress.XtraRichEdit.UI.CommonBar
        Protected Friend WithEvents EditingBar1 As DevExpress.XtraRichEdit.UI.EditingBar
        Protected Friend WithEvents StylesBar1 As DevExpress.XtraRichEdit.UI.StylesBar
        Protected Friend WithEvents ZoomBar1 As DevExpress.XtraRichEdit.UI.ZoomBar
        Protected Friend WithEvents IllustrationsBar1 As DevExpress.XtraRichEdit.UI.IllustrationsBar
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents TabbedControlGroup1 As DevExpress.XtraLayout.TabbedControlGroup
        Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlGroup3 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlGroup4 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlGroup5 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlGroup6 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents SimpleButton_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents RichEditControl1 As DevExpress.XtraRichEdit.RichEditControl
        Friend WithEvents CheckedListBoxControl_ToDo As DevExpress.XtraEditors.CheckedListBoxControl
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents CheckedListBoxControl_Required As DevExpress.XtraEditors.CheckedListBoxControl
        Friend WithEvents CheckedListBoxControl_Expenses As DevExpress.XtraEditors.CheckedListBoxControl
        Friend WithEvents CheckedListBoxControl_Income As DevExpress.XtraEditors.CheckedListBoxControl
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents SpellChecker1 As DevExpress.XtraSpellChecker.SpellChecker
        Friend WithEvents BarButtonItem_Edit_Undo As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Edit_Redo As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarCheckItem_View_PageLayout As DevExpress.XtraBars.BarCheckItem
        Friend WithEvents SharedDictionaryStorage1 As DevExpress.XtraSpellChecker.SharedDictionaryStorage
        Friend WithEvents BarButtonItem_Tools_SpellerOptions As DevExpress.XtraBars.BarButtonItem
    End Class
End Namespace