﻿Imports System.Linq
Imports DebtPlus.LINQ

Namespace forms.Disclosures
    Friend Class Disclosure_RMC_Form

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Private record As DebtPlus.LINQ.client_disclosures_RMC

        Public Overrides Function EditDialog(bc As DebtPlus.LINQ.BusinessContext, ByVal Context As DebtPlus.UI.Client.Service.ClientUpdateClass, ByVal disclosureRecord As DebtPlus.LINQ.client_disclosure) As System.Windows.Forms.DialogResult
            log.Debug("ENTRY EditDialog")

            ' Find the type of the record and load the template
            Dim q As DebtPlus.LINQ.client_DisclosureLanguageType = disclosureRecord.client_DisclosureLanguageType
            If q Is Nothing Then
                log.Debug("EXIT EditDialog. result = cancel")
                Return Windows.Forms.DialogResult.Cancel
            End If

            ' Find the record to be processed. If there is not one, create one.
            record = disclosureRecord.client_disclosures_RMCs
            If record Is Nothing Then
                log.Debug("Reading RMC record")
                disclosureRecord.client_disclosures_RMCs = DebtPlus.LINQ.Factory.Manufacture_client_disclosures_RMC()
                record = disclosureRecord.client_disclosures_RMCs
                log.Debug("Reading RMC record -- complete")
            End If

            ' Put the new URL into the browser once we have the records to process
            documentURI = New System.Uri(q.template_url)
            WebBrowser1.Url = documentURI

            ' Do the standard editing operation
            Dim answer As System.Windows.Forms.DialogResult = MyBase.EditDialog(bc, Context, disclosureRecord)
            log.DebugFormat("EXIT EditDialog. result = {0}", answer.ToString())
            Return answer
        End Function

        Protected Overrides Sub WriteDocumentFields()
            log.Debug("ENTRY WriteDocumentFields")
            MyBase.WriteDocumentFields()

            ' Store the answers to the questions about the response
            ' Do not set the default on the radio buttons. Let the user choose the proper answer.
            If record.Id > 0 Then
                SetRadio("q1_answer", record.q1_answer.ToString())
                SetRadio("q2_answer", record.q2_answer.ToString())
                SetRadio("q3_answer", record.q3_answer.ToString())
                SetRadio("q4_answer", record.q4_answer.ToString())
                SetRadio("q5_answer", record.q5_answer.ToString())
                SetRadio("q6_answer", record.q6_answer.ToString())
                SetRadio("q7_answer", record.q7_answer.ToString())
                SetRadio("q8_answer", record.q8_answer.ToString())
                SetRadio("q9_answer", record.q9_answer.ToString())
                SetRadio("q10_answer", record.q10_answer.ToString())
                SetRadio("q11_answer", record.q11_answer.ToString())
                SetRadio("q12_answer", record.q12_answer.ToString())
                SetRadio("q13_answer", record.q13_answer.ToString())
                SetRadio("q14_answer", record.q14_answer.ToString())
                SetRadio("q15_answer", record.q15_answer.ToString())
                SetRadio("q16_answer", record.q16_answer.ToString())
                SetRadio("q17_answer", record.q17_answer.ToString())
                SetRadio("q18_answer", record.q18_answer.ToString())
                SetRadio("q19_answer", record.q19_answer.ToString())
            End If

            ' The text fields are set here
            SetText("q1_name", record.q1_name)
            SetText("q1_relation", record.q1_relation)
            SetText("q15_comments", record.q15_comments)
            SetText("q20_missing_info", record.q20_missing_info)
            SetText("q10_contact", record.q10_contact)
            SetText("q10_telephone", record.q10_telephone)
            SetText("q10_fax", record.q10_fax)
            SetText("q10_name", record.q10_name)

            log.Debug("EXIT WriteDocumentFields")
        End Sub

        Protected Overrides Sub ReadDocumentFields(ByVal dictionary As MyDictionary)
            log.Debug("ENTER ReadDocumentFields")
            MyBase.ReadDocumentFields(dictionary)

            ' Store the answers to the questions about the response.
            record.q1_answer = DebtPlus.Utils.Nulls.v_Int32(dictionary.GetValue("q1_answer")).GetValueOrDefault()
            record.q2_answer = DebtPlus.Utils.Nulls.v_Int32(dictionary.GetValue("q2_answer")).GetValueOrDefault()
            record.q3_answer = DebtPlus.Utils.Nulls.v_Int32(dictionary.GetValue("q3_answer")).GetValueOrDefault()
            record.q4_answer = DebtPlus.Utils.Nulls.v_Int32(dictionary.GetValue("q4_answer")).GetValueOrDefault()
            record.q5_answer = DebtPlus.Utils.Nulls.v_Int32(dictionary.GetValue("q5_answer")).GetValueOrDefault()
            record.q6_answer = DebtPlus.Utils.Nulls.v_Int32(dictionary.GetValue("q6_answer")).GetValueOrDefault()
            record.q7_answer = DebtPlus.Utils.Nulls.v_Int32(dictionary.GetValue("q7_answer")).GetValueOrDefault()
            record.q8_answer = DebtPlus.Utils.Nulls.v_Int32(dictionary.GetValue("q8_answer")).GetValueOrDefault()
            record.q9_answer = DebtPlus.Utils.Nulls.v_Int32(dictionary.GetValue("q9_answer")).GetValueOrDefault()
            record.q10_answer = DebtPlus.Utils.Nulls.v_Int32(dictionary.GetValue("q10_answer")).GetValueOrDefault()
            record.q11_answer = DebtPlus.Utils.Nulls.v_Int32(dictionary.GetValue("q11_answer")).GetValueOrDefault()
            record.q12_answer = DebtPlus.Utils.Nulls.v_Int32(dictionary.GetValue("q12_answer")).GetValueOrDefault()
            record.q13_answer = DebtPlus.Utils.Nulls.v_Int32(dictionary.GetValue("q13_answer")).GetValueOrDefault()
            record.q14_answer = DebtPlus.Utils.Nulls.v_Int32(dictionary.GetValue("q14_answer")).GetValueOrDefault()
            record.q15_answer = DebtPlus.Utils.Nulls.v_Int32(dictionary.GetValue("q15_answer")).GetValueOrDefault()
            record.q16_answer = DebtPlus.Utils.Nulls.v_Int32(dictionary.GetValue("q16_answer")).GetValueOrDefault()
            record.q17_answer = DebtPlus.Utils.Nulls.v_Int32(dictionary.GetValue("q17_answer")).GetValueOrDefault()
            record.q18_answer = DebtPlus.Utils.Nulls.v_Int32(dictionary.GetValue("q18_answer")).GetValueOrDefault()
            record.q19_answer = DebtPlus.Utils.Nulls.v_Int32(dictionary.GetValue("q19_answer")).GetValueOrDefault()

            record.q1_name = DebtPlus.Utils.Nulls.v_String(dictionary.GetValue("q1_name"))
            record.q1_relation = DebtPlus.Utils.Nulls.v_String(dictionary.GetValue("q1_relation"))
            record.q10_contact = DebtPlus.Utils.Nulls.v_String(dictionary.GetValue("q10_contact"))
            record.q10_fax = DebtPlus.Utils.Nulls.v_String(dictionary.GetValue("q10_fax"))
            record.q10_name = DebtPlus.Utils.Nulls.v_String(dictionary.GetValue("q10_name"))
            record.q10_telephone = DebtPlus.Utils.Nulls.v_String(dictionary.GetValue("q10_telephone"))
            record.q15_comments = DebtPlus.Utils.Nulls.v_String(dictionary.GetValue("q15_comments"))
            record.q20_missing_info = DebtPlus.Utils.Nulls.v_String(dictionary.GetValue("q20_missing_info"))

            ' Clean the fields
            CleanRecord(record)

            log.Debug("EXIT ReadDocumentFields")
        End Sub

        ''' <summary>
        ''' Prevent the input record from exceeding the space allocated for the fields in the database.
        ''' Since this record is free formed from an HTML document, the document can not limit the length
        ''' of the fields. We do it here before the record is updated or inserted into the database.
        ''' </summary>
        ''' <param name="instance">Pointer to the instance of the record before the insert/update operation</param>
        Private Sub CleanRecord(instance As client_disclosures_RMC)
            log.Debug("ENTER CleanRecord")

            ' Prevent the stupidity from occurring.
            If instance Is Nothing Then
                log.Debug("EXIT CleanRecord -- instance IS NULL")
                Return
            End If

            ' The name length is 50 characters
            If instance.q1_name IsNot Nothing AndAlso instance.q1_name.Length > 50 Then
                log.Debug("Truncating q1_name")
                instance.q1_name = instance.q1_name.Substring(0, 50)
            End If

            ' The relation length is 50 characters
            If instance.q1_relation IsNot Nothing AndAlso instance.q1_relation.Length > 50 Then
                log.Debug("Truncating q1_relation")
                instance.q1_relation = instance.q1_relation.Substring(0, 50)
            End If

            ' The contact length is 50 characters
            If instance.q10_contact IsNot Nothing AndAlso instance.q10_contact.Length > 50 Then
                log.Debug("Truncating q10_contact")
                instance.q10_contact = instance.q10_contact.Substring(0, 50)
            End If

            ' The name length is 50 characters
            If instance.q10_name IsNot Nothing AndAlso instance.q10_name.Length > 50 Then
                log.Debug("Truncating q10_name")
                instance.q10_name = instance.q10_name.Substring(0, 50)
            End If

            ' The telephone length is 50 characters
            If instance.q10_telephone IsNot Nothing AndAlso instance.q10_telephone.Length > 50 Then
                log.Debug("Truncating q10_telephone")
                instance.q10_telephone = instance.q10_telephone.Substring(0, 50)
            End If

            ' The fax length is 50 characters
            If instance.q10_fax IsNot Nothing AndAlso instance.q10_fax.Length > 50 Then
                log.Debug("Truncating q10_fax")
                instance.q10_fax = instance.q10_fax.Substring(0, 50)
            End If

            ' The comments length is 512 characters
            If instance.q15_comments IsNot Nothing AndAlso instance.q15_comments.Length > 512 Then
                log.Debug("Truncating q15_comments")
                instance.q15_comments = instance.q15_comments.Substring(0, 512)
            End If

            ' The missing_info length is 4096 characters
            If instance.q20_missing_info IsNot Nothing AndAlso instance.q20_missing_info.Length > 4096 Then
                log.Debug("Truncating q20_missing_info")
                instance.q20_missing_info = instance.q20_missing_info.Substring(0, 4096)
            End If

            log.Debug("EXIT CleanRecord")
        End Sub
    End Class
End Namespace