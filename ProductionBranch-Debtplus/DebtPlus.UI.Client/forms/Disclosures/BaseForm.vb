﻿Namespace forms.Disclosures
    Friend Class BaseForm

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        ' Current client_disclosure record 
        Protected disclosureRecord As DebtPlus.LINQ.client_disclosure

        ' Current HTML document
        Protected doc As HtmlDocument

        ' URL to the document from the disclosureRecord
        Protected documentURI As System.Uri

        ' Context for the editing functions.
        Protected Context As DebtPlus.UI.Client.Service.ClientUpdateClass = Nothing
        Protected bc As DebtPlus.LINQ.BusinessContext = Nothing

        ''' <summary>
        ''' Initialize the new form with no parameters
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler WebBrowser1.DocumentCompleted, AddressOf WebBrowser1_DocumentCompleted
            AddHandler WebBrowser1.Navigating, AddressOf WebBrowser1_Navigating
            AddHandler BarButtonItem_File_Print.ItemClick, AddressOf BarButtonItem_File_Print_ItemClick
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler WebBrowser1.DocumentCompleted, AddressOf WebBrowser1_DocumentCompleted
            RemoveHandler WebBrowser1.Navigating, AddressOf WebBrowser1_Navigating
            RemoveHandler BarButtonItem_File_Print.ItemClick, AddressOf BarButtonItem_File_Print_ItemClick
        End Sub

        ''' <summary>
        ''' Overridable function to the ShowDialog function in the form.
        ''' </summary>
        Public Overridable Function EditDialog() As System.Windows.Forms.DialogResult
            Return ShowDialog()
        End Function

        ''' <summary>
        ''' Perform the EditDialog function with the proper parameters
        ''' </summary>
        Public Overridable Function EditDialog(bc As DebtPlus.LINQ.BusinessContext, ByVal Context As DebtPlus.UI.Client.Service.ClientUpdateClass, ByVal disclosureRecord As DebtPlus.LINQ.client_disclosure) As System.Windows.Forms.DialogResult
            Me.disclosureRecord = disclosureRecord
            Me.Context = Context
            Me.bc = bc
            Return EditDialog()
        End Function

        ''' <summary>
        ''' Process a RADIO control for the HTML text
        ''' </summary>
        Protected Sub SetRadio(ByVal keyField As String, ByVal value As String)

            log.DebugFormat("ENTRY SetRadio({0},{1})", keyField, value)

            ' Get a collection of the input fields for the form
            If doc.Forms.Count < 1 Then
                log.Info("EXIT SetRadio : No form elements on page")
                Return
            End If

            Dim hCollection As HtmlElementCollection = doc.Forms(0).Document.GetElementsByTagName("input")

            ' Find the radio buttons in the input collection
            For Each elem As HtmlElement In hCollection
                Dim elemName As String = elem.GetAttribute("name")
                Dim elemValue As String = elem.GetAttribute("value")

                ' If the radio button is checked then return its value
                If String.Compare(elemName, keyField, True) = 0 AndAlso String.Compare(elemValue, value, True) = 0 Then
                    log.Debug("Element located. Setting TRUE")
                    elem.SetAttribute("checked", "True")
                    Exit For
                End If
            Next

            log.Debug("EXIT SetRadio")
        End Sub

        ''' <summary>
        ''' Process a SPAN control for the HTML text
        ''' </summary>
        Protected Sub SetSpan(ByVal keyField As String, ByVal value As String)
            SetSpan(keyField, value, "innertext")
        End Sub

        ''' <summary>
        ''' Process a SPAN control for the HTML text
        ''' </summary>
        Protected Sub SetSpan(ByVal keyField As String, ByVal value As String, ByVal type As String)
            log.DebugFormat("ENTRY SetSpan({0},{1},{2})", If(keyField, "NULL"), If(value, "NULL"), If(type, "NULL"))

            Dim elem As System.Windows.Forms.HtmlElement = doc.GetElementById(keyField)

            If elem Is Nothing Then
                log.Debug("EXIT SetSpan : No Element")
                Return
            End If

            Dim obj As System.Object = elem.DomElement
            Dim typ As System.Type = obj.GetType()
            typ.InvokeMember(type, System.Reflection.BindingFlags.SetProperty Or System.Reflection.BindingFlags.IgnoreCase Or System.Reflection.BindingFlags.Public, Nothing, obj, New Object() {If(value Is Nothing, String.Empty, value)})
            log.Debug("EXIT SetSpan")
        End Sub

        ''' <summary>
        ''' Set the HTML TEXT control with the proper text.
        ''' </summary>
        Protected Sub SetText(ByVal keyField As String, ByVal value As String)
            log.DebugFormat("ENTRY SetText({0},{1})", If(keyField, "NULL"), If(value, "NULL"))

            Dim elem As System.Windows.Forms.HtmlElement = doc.GetElementById(keyField)

            If elem Is Nothing Then
                log.Debug("EXIT SetText : No Element")
                Return
            End If

            Dim obj As System.Object = elem.DomElement
            Dim typ As System.Type = obj.GetType()
            typ.InvokeMember("value", System.Reflection.BindingFlags.SetProperty Or System.Reflection.BindingFlags.IgnoreCase Or System.Reflection.BindingFlags.Public, Nothing, obj, New Object() {If(value Is Nothing, String.Empty, value)})
            log.Debug("EXIT SetText")
        End Sub

        ''' <summary>
        ''' Update the CHECKBOX HTML control
        ''' </summary>
        Protected Sub SetCheckbox(ByVal keyField As String, ByVal value As Boolean)
            log.DebugFormat("ENTRY SetCheckbox({0},{1})", If(keyField, "NULL"), value.ToString())

            Dim elem As System.Windows.Forms.HtmlElement = doc.GetElementById(keyField)

            If elem Is Nothing Then
                log.Debug("EXIT SetCheckbox : No Element")
                Return
            End If

            Dim obj As System.Object = elem.DomElement
            Dim typ As System.Type = obj.GetType()
            typ.InvokeMember("checked", System.Reflection.BindingFlags.SetProperty Or System.Reflection.BindingFlags.IgnoreCase Or System.Reflection.BindingFlags.Public, Nothing, obj, New Object() {value})
            log.Debug("EXIT SetCheckbox")
        End Sub

        ''' <summary>
        ''' Update a CHECKBOX HTML control. Copy the value to the alternate location. This is used for the MHA form.
        ''' </summary>
        Protected Sub SetCheckbox(ByVal keyField As String, ByVal keyField2 As String, ByVal value As Boolean)
            ' Do the standard logic
            SetCheckbox(keyField, value)

            ' Copy the checked status to the display form.
            If value Then
                SetSpan(keyField2, "&nbsp;X&nbsp;", "innerhtml")
            End If
        End Sub

        ''' <summary>
        ''' Update a SELECT HTML control
        ''' </summary>
        Protected Sub SetSelect(ByVal keyField As String, ByVal value As String)
            log.DebugFormat("ENTRY SetSelect({0},{1})", If(keyField, "NULL"), value.ToString())

            Dim elem As System.Windows.Forms.HtmlElement = doc.GetElementById(keyField)

            If elem Is Nothing Then
                log.Debug("EXIT SetSelect : No Element")
                Return
            End If

            Dim obj As System.Object = elem.DomElement
            Dim typ As System.Type = obj.GetType()
            typ.InvokeMember("SetAttribute", System.Reflection.BindingFlags.InvokeMethod Or System.Reflection.BindingFlags.IgnoreCase Or System.Reflection.BindingFlags.Public, Nothing, obj, New Object() {"value", value})
            log.Debug("EXIT SetSelect")
        End Sub

        ''' <summary>
        ''' The loading of the document is complete. Do the initialization of the data fields.
        ''' </summary>
        Protected Overridable Sub WebBrowser1_DocumentCompleted(sender As Object, e As System.Windows.Forms.WebBrowserDocumentCompletedEventArgs)
            log.Debug("ENTRY WebBrowser1_DocumentCompleted")
            doc = WebBrowser1.Document

            Try
                If documentURI.Equals(e.Url) Then
                    If Not String.IsNullOrWhiteSpace(doc.Title) Then
                        Text = doc.Title
                    End If
                    WriteDocumentFields()
                End If

            Catch ex As Exception
                log.Fatal("Exception in WebBrowser1_DocumentCompleted", ex)
            End Try

            log.Debug("EXIT WebBrowser1_DocumentCompleted")
        End Sub

        ''' <summary>
        ''' Intercept the navigation for the web browser. Do not allow it to navigate outside the form.
        ''' </summary>
        Protected Overridable Sub WebBrowser1_Navigating(sender As Object, e As System.Windows.Forms.WebBrowserNavigatingEventArgs)
            log.DebugFormat("ENTRY WebBrowser1_Navigating. URL = ""{0}""", e.Url.ToString())
            doc = WebBrowser1.Document

            ' If the protocol scheme is "debt" (i.e. "DebtPlus") then accept it as the completion event.
            If e.Url.Scheme = "debt" Then

                ' Update the record contents only on the creation of a new record. We ignore the changes if this
                ' is not a create operation. You can't change the document once it has been created. You can only "show" it.
                If disclosureRecord.Id < 1 Then
                    Dim paramCollection As MyDictionary = QueryParse(e.Url.GetComponents(UriComponents.Query, UriFormat.UriEscaped))
                    ReadDocumentFields(paramCollection)
                    DialogResult = Windows.Forms.DialogResult.OK
                    log.Debug("EXIT WebBrowser1_Navigating. Result = OK")
                    Return
                End If

                DialogResult = Windows.Forms.DialogResult.Cancel
                log.Debug("EXIT WebBrowser1_Navigating. Result = Cancel")
                Return
            End If

            ' Do not attempt to load our own document in a different browser. Use the local browser for it.
            If Not documentURI.Equals(e.Url) Then
                log.DebugFormat("Navigating to external page at ""{0}""", documentURI.ToString())
                e.Cancel = True
                Dim si As New System.Diagnostics.ProcessStartInfo(e.Url.LocalPath)
                System.Diagnostics.Process.Start(si)
            End If

            log.Debug("EXIT WebBrowser1_Navigating")
        End Sub

        ''' <summary>
        ''' Parse the query section of the string
        ''' </summary>
        ''' <param name="url"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Shared Function QueryParse(ByVal url As String) As MyDictionary
            Dim qDict As New MyDictionary()
            For Each qPair As String In url.Split("&"c)
                Dim qVal As String() = qPair.Split("="c)
                Dim keyField As String = qVal(0)
                Dim valueField As String = System.Uri.UnescapeDataString(qVal(1).Replace("+", " "))
                qDict.SetValue(keyField, valueField)
            Next
            Return qDict
        End Function

        Protected Shared Function QueryGet(ByVal url As String, ByVal param As String) As String
            Dim qDict As MyDictionary = QueryParse(url)
            Return qDict(param)
        End Function

        ''' <summary>
        ''' Retrieve the standard set of controls from all of the disclosure records. They should all be present.
        ''' </summary>
        Protected Overridable Sub ReadDocumentFields(ByVal dictionary As MyDictionary)
            log.Debug("ENTRY ReadDocumentFields")

            ' Retrieve the values from the control
            disclosureRecord.applicant = IntParse(dictionary.GetValue("applicant"))
            disclosureRecord.coapplicant = IntParse(dictionary.GetValue("coapplicant"))
            disclosureRecord.applicant_value = dictionary.GetValue("applicant_value")
            disclosureRecord.coapplicant_value = dictionary.GetValue("coapplicant_value")

            ' Create a system note showing the information if we are creating a new disclosure record.
            If disclosureRecord.Id < 1 Then

                Using bc As New DebtPlus.LINQ.BusinessContext()
                    log.Debug("Creating system note")
                    Dim n As DebtPlus.LINQ.client_note = DebtPlus.LINQ.Factory.Manufacture_client_note(Context.ClientDs.ClientId, 3)
                    n.subject = String.Format("{0} completed", Text)
                    n.note = CreateClientNote(disclosureRecord)
                    bc.client_notes.InsertOnSubmit(n)
                    bc.SubmitChanges()
                End Using
                log.Debug("Created system note")
            End If

            log.Debug("EXIT ReadDocumentFields")
        End Sub

        Protected Function CreateClientNote(ByVal record As DebtPlus.LINQ.client_disclosure) As String
            Dim sb As New System.Text.StringBuilder()
            sb.Append("<html><body><table><tr><th>&nbsp;</th><th align=""left"">Status</th><th align=""left"">Name</th></tr>")
            sb.AppendFormat("<tr><th align=""left"">Applicant</th><td>{0}</td><td>{1}</td></tr>", ApplicantStatus(record.applicant), System.Net.WebUtility.HtmlEncode(record.applicant_value))
            sb.AppendFormat("<tr><th align=""left"">Co-Applicant</th><td>{0}</td><td>{1}</td></tr>", ApplicantStatus(record.coapplicant), System.Net.WebUtility.HtmlEncode(record.coapplicant_value))
            sb.Append("</table></body></html>")
            Return sb.ToString()
        End Function

        Private Function ApplicantStatus(ByVal code As Int32) As String
            Select Case code
                Case 1 : Return "No"
                Case 2 : Return "Yes"
                Case 3 : Return "Not Applicable"
                Case 4 : Return "Unavailable"
                Case Else : Return "Unknown"
            End Select
        End Function

        ''' <summary>
        ''' Set the standard set of controls on all of the disclosure records. They all should have these fields.
        ''' </summary>
        Protected Overridable Sub WriteDocumentFields()
            log.Debug("ENTRY WriteDocumentFields")

            ' Fill in the information about the standard set of controls
            SetSelect("applicant", disclosureRecord.applicant.ToString())
            SetSelect("coapplicant", disclosureRecord.coapplicant.ToString())
            SetText("applicant_value", If(disclosureRecord.applicant_value Is Nothing, String.Empty, disclosureRecord.applicant_value))
            SetText("coapplicant_value", If(disclosureRecord.coapplicant_value Is Nothing, String.Empty, disclosureRecord.coapplicant_value))

            log.Debug("EXIT WriteDocumentFields")
        End Sub

        Protected Function IntParse(ByVal InputString As String) As Int32
            If Not String.IsNullOrWhiteSpace(InputString) Then
                Dim intValue As Int32
                If Int32.TryParse(InputString, intValue) Then
                    Return intValue
                End If
            End If
            Return 0
        End Function

        Protected Class MyDictionary
            Inherits System.Collections.Generic.Dictionary(Of String, String)
            Public Function GetValue(ByVal keyField As String) As String
                If Not String.IsNullOrEmpty(keyField) Then
                    If Me.ContainsKey(keyField) Then
                        Return Me(keyField)
                    End If
                End If
                Return String.Empty
            End Function

            Public Sub SetValue(ByVal keyField As String, ByVal valueField As String)
                If Me.ContainsKey(keyField) Then
                    Me(keyField) = valueField
                Else
                    Me.Add(keyField, valueField)
                End If
            End Sub
        End Class

        Private Sub BarButtonItem_File_Print_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs)
            If WebBrowser1 IsNot Nothing Then
                WebBrowser1.Print()
            End If
        End Sub
    End Class
End Namespace