﻿Imports System.Linq
Imports DebtPlus.LINQ

Namespace forms.Disclosures
    Friend Class Disclosure_STD_Form

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Overrides Function EditDialog(bc As DebtPlus.LINQ.BusinessContext, ByVal Context As DebtPlus.UI.Client.Service.ClientUpdateClass, ByVal disclosureRecord As DebtPlus.LINQ.client_disclosure) As System.Windows.Forms.DialogResult
            log.Debug("ENTRY EditDialog")

            ' Find the type of the record and load the template
            Dim q As DebtPlus.LINQ.client_DisclosureLanguageType = disclosureRecord.client_DisclosureLanguageType
            If q Is Nothing Then
                log.Debug("EXIT EditDialog. result = cancel")
                Return Windows.Forms.DialogResult.Cancel
            End If

            documentURI = New System.Uri(q.template_url)
            WebBrowser1.Url = documentURI

            Dim answer As System.Windows.Forms.DialogResult = MyBase.EditDialog(bc, Context, disclosureRecord)
            log.DebugFormat("EXIT EditDialog. result = {0}", answer.ToString())
            Return answer
        End Function
    End Class
End Namespace