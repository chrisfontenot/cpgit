﻿Imports System.Linq
Imports DebtPlus.LINQ

Namespace forms.Disclosures
    Friend Class Disclosure_MHA_Form

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Private record As DebtPlus.LINQ.client_disclosures_MHA

        Public Overrides Function EditDialog(bc As DebtPlus.LINQ.BusinessContext, ByVal Context As DebtPlus.UI.Client.Service.ClientUpdateClass, ByVal disclosureRecord As DebtPlus.LINQ.client_disclosure) As System.Windows.Forms.DialogResult
            log.Debug("ENTRY EditDialog")

            ' Find the type of the record and load the template
            Dim q As DebtPlus.LINQ.client_DisclosureLanguageType = disclosureRecord.client_DisclosureLanguageType
            If q Is Nothing Then
                log.Debug("EXIT EditDialog. result = cancel")
                Return Windows.Forms.DialogResult.Cancel
            End If

            ' Find the record to be processed. If there is not one, create one.
            record = disclosureRecord.client_disclosures_MHAs
            If record Is Nothing Then
                log.Debug("Reading MHA indicator list")
                disclosureRecord.client_disclosures_MHAs = New DebtPlus.LINQ.client_disclosures_MHA()
                record = disclosureRecord.client_disclosures_MHAs
                Dim typ As System.Type = record.GetType()

                Dim colIndicators As System.Collections.Generic.List(Of DebtPlus.LINQ.xpr_disclosure_mhaResult) = bc.xpr_disclosure_mha(disclosureRecord.propertyID.GetValueOrDefault(0))
                If colIndicators IsNot Nothing Then
                    For Each indr As DebtPlus.LINQ.xpr_disclosure_mhaResult In colIndicators
                        Dim propInfo As System.Reflection.PropertyInfo = typ.GetProperty(indr.indicator)
                        If propInfo IsNot Nothing Then
                            Dim objValue As Object = Convert.ChangeType(indr.Value, propInfo.PropertyType)
                            propInfo.SetValue(record, objValue, Nothing)
                        End If
                    Next
                End If
            End If

            ' Put the new URL into the browser once we have the records to process
            documentURI = New System.Uri(q.template_url)
            WebBrowser1.Url = documentURI

            ' Do the standard editing operation
            Dim answer As System.Windows.Forms.DialogResult = MyBase.EditDialog(bc, Context, disclosureRecord)
            log.DebugFormat("EXIT EditDialog. result = {0}", answer.ToString())
            Return answer
        End Function

        Protected Overrides Sub WriteDocumentFields()
            log.Debug("ENTRY WriteDocumentFields")
            MyBase.WriteDocumentFields()

            ' Set the date of the disclosure. The date is read-only so we can't set it during the create operation.
            If disclosureRecord.date_created.Year >= 1980 Then
                SetSpan("date", disclosureRecord.date_created.Date.ToShortDateString())
            Else
                SetSpan("date", System.DateTime.Now.Date.ToShortDateString())
            End If

            ' Read the name of the client from the database
            Dim namePTR As Int32? = bc.peoples.Where(Function(p) p.Client = Context.ClientDs.ClientId AndAlso p.Relation = 1).Select(Function(p) p.NameID).FirstOrDefault()
            If namePTR.HasValue Then
                Dim nameRecord As DebtPlus.LINQ.Name = bc.Names.Where(Function(n) n.Id = namePTR.Value).FirstOrDefault()
                If nameRecord IsNot Nothing Then
                    SetSpan("client_name_and_number_id", String.Format("{0:0000000} {1}", Context.ClientDs.ClientId, nameRecord.ToString()))
                End If
            End If

            ' Find the counselor name
            Dim counselorKey As String = disclosureRecord.created_by
            If String.IsNullOrEmpty(counselorKey) Then
                counselorKey = DebtPlus.LINQ.BusinessContext.suser_sname()
            End If

            Dim co As DebtPlus.LINQ.counselor = DebtPlus.LINQ.Cache.counselor.getList().Find(Function(s) String.Compare(s.Person, counselorKey, True) = 0)
            If co IsNot Nothing AndAlso co.Name IsNot Nothing Then
                SetSpan("counselor_name", co.Name.ToString())
            End If

            ' Store the answers to the questions about the response
            SetCheckbox("answer_01_01", record.answer_01_01)
            SetCheckbox("answer_01_02", record.answer_01_02)
            SetCheckbox("answer_01_03", record.answer_01_03)
            SetCheckbox("answer_01_04", record.answer_01_04)
            SetCheckbox("answer_01_05", record.answer_01_05)
            SetCheckbox("answer_01_06", record.answer_01_06)
            SetCheckbox("answer_01_07", record.answer_01_07)
            SetCheckbox("answer_01_08", record.answer_01_08)
            SetCheckbox("answer_01_09", record.answer_01_09)
            SetCheckbox("answer_01_09", record.answer_01_09)
            SetCheckbox("answer_01_10", record.answer_01_10)
            SetCheckbox("answer_01_11", record.answer_01_11)

            SetCheckbox("answer_02_01", record.answer_02_01)
            SetCheckbox("answer_02_02", record.answer_02_02)
            SetCheckbox("answer_02_03", record.answer_02_03)

            SetCheckbox("answer_03_01", record.answer_03_01)
            SetCheckbox("answer_03_02", record.answer_03_02)
            SetCheckbox("answer_03_03", record.answer_03_03)
            SetCheckbox("answer_03_04", record.answer_03_04)
            SetCheckbox("answer_03_05", record.answer_03_05)

            SetCheckbox("answer_04_01", record.answer_04_01)
            SetCheckbox("answer_04_02", record.answer_04_02)
            SetCheckbox("answer_04_03", record.answer_04_03)
            SetCheckbox("answer_04_04", record.answer_04_04)
            SetCheckbox("answer_04_05", record.answer_04_05)

            SetCheckbox("answer_05_01", record.answer_05_01)
            SetCheckbox("answer_05_02", record.answer_05_02)
            SetCheckbox("answer_05_03", record.answer_05_03)
            SetCheckbox("answer_05_04", record.answer_05_04)
            SetCheckbox("answer_05_05", record.answer_05_05)
            SetCheckbox("answer_05_06", record.answer_05_06)
            SetCheckbox("answer_05_07", record.answer_05_07)

            SetCheckbox("eligible_FHA_HAMP", "eligible_FHA_HAMP_checked", record.eligible_FHA_HAMP)
            SetCheckbox("eligible_HAFA", "eligible_HAFA_checked", record.eligible_HAFA)
            SetCheckbox("eligible_HAMP_tier_1", "eligible_HAMP_tier_1_checked", record.eligible_HAMP_tier_1)
            SetCheckbox("eligible_HAMP_tier_2", "eligible_HAMP_tier_2_checked", record.eligible_HAMP_tier_2)
            SetCheckbox("eligible_HARP", "eligible_HARP_checked", record.eligible_HARP)
            SetCheckbox("eligible_NONE", "eligible_NONE_checked", record.eligible_NONE)

            If disclosureRecord.Id >= 1 Then
                SetRadio("answer_06_01", Convert.ToInt32(record.answer_06_01).ToString())
                SetRadio("answer_06_02", Convert.ToInt32(record.answer_06_02).ToString())
                SetRadio("answer_06_03", Convert.ToInt32(record.answer_06_03).ToString())
                SetRadio("answer_06_04", Convert.ToInt32(record.answer_06_04).ToString())
            End If

            log.Debug("EXIT WriteDocumentFields")
        End Sub

        Private Sub setClientInformation(ByVal bc As DebtPlus.LINQ.BusinessContext, ByVal clientRecord As DebtPlus.LINQ.client)
            log.Debug("ENTER setClientInformation")

            ' Financial problems
            setFinancialProblem(clientRecord.cause_fin_problem1)
            setFinancialProblem(clientRecord.cause_fin_problem2)
            setFinancialProblem(clientRecord.cause_fin_problem3)
            setFinancialProblem(clientRecord.cause_fin_problem4)

            ' Set the information for the client_housing
            Dim clientHousingRecord As DebtPlus.LINQ.client_housing = bc.client_housings.Where(Function(h) h.Id = clientRecord.Id).FirstOrDefault()
            If clientHousingRecord IsNot Nothing Then
                setClientHousingInformation(bc, clientRecord, clientHousingRecord)
            End If

            log.Debug("EXIT setClientInformation")
        End Sub

        Private Sub setPeopleInformation(ByVal person As DebtPlus.LINQ.people)
            log.Debug("ENTER setPeopleInformation")

            record.answer_04_03 = person.gross_income > 0D OrElse person.net_income > 0D

            log.Debug("ENTER setPeopleInformation")
        End Sub

        Private Sub setFinancialProblem(ByVal problemID As Int32?)
            log.Debug("ENTER setFinancialProblem")

            If problemID.HasValue Then
                record.answer_05_02 = True
                record.answer_01_06 = True
                record.answer_04_02 = (New Int32() {37, 38, 40, 45, 53, 55, 63}).Contains(problemID.Value)
            End If

            log.Debug("EXIT setFinancialProblem")
        End Sub

        Private Sub setClientHousingInformation(ByVal bc As DebtPlus.LINQ.BusinessContext, ByVal clientRecord As DebtPlus.LINQ.client, ByVal clientHousingRecord As DebtPlus.LINQ.client_housing)
            log.Debug("ENTER setClientHousingInformation")
            record.answer_05_03 = clientHousingRecord.BackEnd_DTI >= 55.0#

            Dim propertyRecord As Housing_property = Nothing
            If disclosureRecord.propertyID > 0 Then
                propertyRecord = bc.Housing_properties.Where(Function(pr) pr.Id = disclosureRecord.propertyID.GetValueOrDefault()).FirstOrDefault()
            End If

            If propertyRecord IsNot Nothing Then
                setHousingPropertyInformation(bc, clientRecord, clientHousingRecord, propertyRecord)
            End If

            log.Debug("EXIT setClientHousingInformation")
        End Sub

        Private Sub setHousingPropertyInformation(ByVal bc As DebtPlus.LINQ.BusinessContext, ByVal clientRecord As DebtPlus.LINQ.client, ByVal clientHousingRecord As DebtPlus.LINQ.client_housing, ByVal propertyRecord As DebtPlus.LINQ.Housing_property)
            log.Debug("ENTER setHousingPropertyInformation")

            record.answer_01_05 = propertyRecord.Residency = 1 OrElse propertyRecord.Residency = 2 OrElse propertyRecord.Residency = 3
            record.answer_02_02 = record.answer_01_05
            record.answer_05_04 = record.answer_01_05
            record.answer_01_09 = propertyRecord.Residency = 1

            ' If we have a property record then obtain the loans for this property
            Dim colLoans As System.Collections.Generic.List(Of DebtPlus.LINQ.Housing_loan) = bc.Housing_loans.Where(Function(hl) hl.PropertyID = propertyRecord.Id).ToList()
            setHousingLoanInformation(bc, clientRecord, clientHousingRecord, propertyRecord, colLoans)
            log.Debug("EXIT setHousingPropertyInformation")
        End Sub

        Private Sub setHousingLoanInformation(ByVal bc As DebtPlus.LINQ.BusinessContext, ByVal clientRecord As DebtPlus.LINQ.client, ByVal clientHousingRecord As DebtPlus.LINQ.client_housing, ByVal propertyRecord As DebtPlus.LINQ.Housing_property, ByVal colLoans As System.Collections.Generic.List(Of DebtPlus.LINQ.Housing_loan))
            log.Debug("ENTER setHousingLoanInformation")

            ' Find the LTV ratio
            Dim value As Decimal = propertyRecord.ImprovementsValue + propertyRecord.LandValue
            Dim loanAmount As Decimal = colLoans.Sum(Function(s) s.CurrentLoanBalanceAmt).GetValueOrDefault(0D)

            Try
                If value > 0D AndAlso loanAmount > 0D Then
                    record.answer_03_03 = (loanAmount / value) > 0.8
                End If

            Catch ex As Exception
                log.Fatal("Exception in setHousingLoanInformation", ex)
            End Try

            Dim firstLoan As DebtPlus.LINQ.Housing_loan = colLoans.Find(Function(s) s.Loan1st2nd.GetValueOrDefault(0) = 1)
            If firstLoan IsNot Nothing Then
                setFirstLoanInformation(bc, clientRecord, clientHousingRecord, propertyRecord, colLoans, firstLoan)
            End If

            log.Debug("EXIT setHousingLoanInformation")
        End Sub

        Private Sub setFirstLoanInformation(ByVal bc As DebtPlus.LINQ.BusinessContext, ByVal clientRecord As DebtPlus.LINQ.client, ByVal clientHousingRecord As DebtPlus.LINQ.client_housing, ByVal propertyRecord As DebtPlus.LINQ.Housing_property, ByVal colLoans As System.Collections.Generic.List(Of DebtPlus.LINQ.Housing_loan), ByVal FirstLoan As DebtPlus.LINQ.Housing_loan)
            log.Debug("ENTER setFirstLoanInformation")

            record.answer_01_01 = True
            record.answer_01_02 = FirstLoan.LoanOriginationDate.HasValue AndAlso FirstLoan.LoanOriginationDate.Value.Year <= 2009
            record.answer_01_03 = FirstLoan.LoanDelinquencyMonths.GetValueOrDefault(0) <> 0

            ' Retrieve the detail information about the loan
            Dim detailRecord As DebtPlus.LINQ.Housing_loan_detail = Nothing
            If FirstLoan.IntakeDetailID.HasValue Then
                detailRecord = bc.Housing_loan_details.Where(Function(hd) hd.Id = FirstLoan.IntakeDetailID.GetValueOrDefault(0)).FirstOrDefault()
            End If

            ' Retrieve he lender information
            Dim lenderRecord As DebtPlus.LINQ.Housing_lender = Nothing
            If FirstLoan.CurrentLenderID.HasValue Then
                lenderRecord = bc.Housing_lenders.Where(Function(hl) hl.Id = FirstLoan.CurrentLenderID.Value).FirstOrDefault()
            End If

            ' Retrieve the lender servicer value
            Dim lenderServicer As DebtPlus.LINQ.Housing_lender_servicer = Nothing
            If lenderRecord.ServicerID.HasValue Then
                lenderServicer = bc.Housing_lender_servicers.Where(Function(hls) hls.Id = lenderRecord.ServicerID.GetValueOrDefault()).FirstOrDefault()
            End If

            ' If there is a servicer then is it FANNIE MAE or FREDDY MAC?
            If lenderRecord.InvestorID.HasValue AndAlso (lenderRecord.InvestorID.Value = 1 OrElse lenderRecord.InvestorID.Value = 2) Then
                record.answer_03_01 = True
            End If

            ' Handle the detail information
            If detailRecord IsNot Nothing Then
                record.answer_04_01 = detailRecord.FinanceTypeCD.GetValueOrDefault(0) = 2
                If detailRecord.FinanceTypeCD.GetValueOrDefault(0) = 1 Then
                    If lenderRecord.InvestorID.HasValue AndAlso (lenderRecord.InvestorID.Value = 1 OrElse lenderRecord.InvestorID.Value = 2) Then
                        record.answer_05_01 = True
                    End If
                End If
            End If

            log.Debug("EXIT setFirstLoanInformation")
        End Sub

        Protected Overrides Sub ReadDocumentFields(ByVal dictionary As MyDictionary)
            log.Debug("ENTER ReadDocumentFields")
            MyBase.ReadDocumentFields(dictionary)

            ' Store the answers to the questions about the response.
            ' A checkbox has a value if checked or the value is empty if not.
            record.answer_01_01 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_01_01"))
            record.answer_01_02 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_01_02"))
            record.answer_01_03 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_01_03"))
            record.answer_01_04 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_01_04"))
            record.answer_01_05 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_01_05"))
            record.answer_01_06 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_01_06"))
            record.answer_01_07 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_01_07"))
            record.answer_01_08 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_01_08"))
            record.answer_01_09 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_01_09"))
            record.answer_01_09 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_01_09"))
            record.answer_01_10 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_01_10"))
            record.answer_01_11 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_01_11"))

            record.answer_02_01 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_02_01"))
            record.answer_02_02 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_02_02"))
            record.answer_02_03 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_02_03"))

            record.answer_03_01 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_03_01"))
            record.answer_03_02 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_03_02"))
            record.answer_03_03 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_03_03"))
            record.answer_03_04 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_03_04"))
            record.answer_03_05 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_03_05"))

            record.answer_04_01 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_04_01"))
            record.answer_04_02 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_04_02"))
            record.answer_04_03 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_04_03"))
            record.answer_04_04 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_04_04"))
            record.answer_04_05 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_04_05"))

            record.answer_05_01 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_05_01"))
            record.answer_05_02 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_05_02"))
            record.answer_05_03 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_05_03"))
            record.answer_05_04 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_05_04"))
            record.answer_05_05 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_05_05"))
            record.answer_05_06 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_05_06"))
            record.answer_05_07 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_05_07"))

            ' Radio values always have a value. So, we need to look at their value instead.
            record.answer_06_01 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_06_01")) AndAlso dictionary.GetValue("answer_06_01") = "1"
            record.answer_06_02 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_06_02")) AndAlso dictionary.GetValue("answer_06_02") = "1"
            record.answer_06_03 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_06_03")) AndAlso dictionary.GetValue("answer_06_03") = "1"
            record.answer_06_04 = Not String.IsNullOrEmpty(dictionary.GetValue("answer_06_04")) AndAlso dictionary.GetValue("answer_06_04") = "1"

            record.eligible_FHA_HAMP = Not String.IsNullOrEmpty(dictionary.GetValue("eligible_FHA_HAMP"))
            record.eligible_HAFA = Not String.IsNullOrEmpty(dictionary.GetValue("eligible_HAFA"))
            record.eligible_HAMP_tier_1 = Not String.IsNullOrEmpty(dictionary.GetValue("eligible_HAMP_tier_1"))
            record.eligible_HAMP_tier_2 = Not String.IsNullOrEmpty(dictionary.GetValue("eligible_HAMP_tier_2"))
            record.eligible_HARP = Not String.IsNullOrEmpty(dictionary.GetValue("eligible_HARP"))
            record.eligible_NONE = Not String.IsNullOrEmpty(dictionary.GetValue("eligible_NONE"))

            log.Debug("EXIT ReadDocumentFields")
        End Sub
    End Class
End Namespace