﻿Namespace forms.Disclosures
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class BaseForm
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then components.Dispose()
                End If
                components = Nothing
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
            Me.Bar_MainMenu = New DevExpress.XtraBars.Bar()
            Me.BarSubItem_FileMenu = New DevExpress.XtraBars.BarSubItem()
            Me.BarSubItem_ViewMenu = New DevExpress.XtraBars.BarSubItem()
            Me.BarSubItem_EditMenu = New DevExpress.XtraBars.BarSubItem()
            Me.Bar_StatusBar = New DevExpress.XtraBars.Bar()
            Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
            Me.WebBrowser1 = New System.Windows.Forms.WebBrowser()
            Me.BarButtonItem_File_Print = New DevExpress.XtraBars.BarButtonItem()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'DefaultLookAndFeel1
            '
            Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin"
            '
            'BarManager1
            '
            Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar_MainMenu, Me.Bar_StatusBar})
            Me.BarManager1.DockControls.Add(Me.barDockControlTop)
            Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
            Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
            Me.BarManager1.DockControls.Add(Me.barDockControlRight)
            Me.BarManager1.Form = Me
            Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarSubItem_FileMenu, Me.BarSubItem_ViewMenu, Me.BarSubItem_EditMenu, Me.BarButtonItem_File_Print})
            Me.BarManager1.MainMenu = Me.Bar_MainMenu
            Me.BarManager1.MaxItemId = 4
            Me.BarManager1.StatusBar = Me.Bar_StatusBar
            '
            'Bar_MainMenu
            '
            Me.Bar_MainMenu.BarName = "Main menu"
            Me.Bar_MainMenu.DockCol = 0
            Me.Bar_MainMenu.DockRow = 0
            Me.Bar_MainMenu.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
            Me.Bar_MainMenu.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem_FileMenu), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem_ViewMenu), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem_EditMenu)})
            Me.Bar_MainMenu.OptionsBar.MultiLine = True
            Me.Bar_MainMenu.OptionsBar.UseWholeRow = True
            Me.Bar_MainMenu.Text = "Main menu"
            '
            'BarSubItem_FileMenu
            '
            Me.BarSubItem_FileMenu.Caption = "&File"
            Me.BarSubItem_FileMenu.Id = 0
            Me.BarSubItem_FileMenu.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_File_Print)})
            Me.BarSubItem_FileMenu.Name = "BarSubItem_FileMenu"
            '
            'BarSubItem_ViewMenu
            '
            Me.BarSubItem_ViewMenu.Caption = "&View"
            Me.BarSubItem_ViewMenu.Id = 1
            Me.BarSubItem_ViewMenu.Name = "BarSubItem_ViewMenu"
            '
            'BarSubItem_EditMenu
            '
            Me.BarSubItem_EditMenu.Caption = "&Edit"
            Me.BarSubItem_EditMenu.Id = 2
            Me.BarSubItem_EditMenu.Name = "BarSubItem_EditMenu"
            '
            'Bar_StatusBar
            '
            Me.Bar_StatusBar.BarName = "Status bar"
            Me.Bar_StatusBar.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom
            Me.Bar_StatusBar.DockCol = 0
            Me.Bar_StatusBar.DockRow = 0
            Me.Bar_StatusBar.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
            Me.Bar_StatusBar.OptionsBar.AllowQuickCustomization = False
            Me.Bar_StatusBar.OptionsBar.DrawDragBorder = False
            Me.Bar_StatusBar.OptionsBar.UseWholeRow = True
            Me.Bar_StatusBar.Text = "Status bar"
            '
            'barDockControlTop
            '
            Me.barDockControlTop.CausesValidation = False
            Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
            Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
            Me.barDockControlTop.Size = New System.Drawing.Size(559, 25)
            '
            'barDockControlBottom
            '
            Me.barDockControlBottom.CausesValidation = False
            Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
            Me.barDockControlBottom.Location = New System.Drawing.Point(0, 434)
            Me.barDockControlBottom.Size = New System.Drawing.Size(559, 22)
            '
            'barDockControlLeft
            '
            Me.barDockControlLeft.CausesValidation = False
            Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
            Me.barDockControlLeft.Location = New System.Drawing.Point(0, 25)
            Me.barDockControlLeft.Size = New System.Drawing.Size(0, 409)
            '
            'barDockControlRight
            '
            Me.barDockControlRight.CausesValidation = False
            Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
            Me.barDockControlRight.Location = New System.Drawing.Point(559, 25)
            Me.barDockControlRight.Size = New System.Drawing.Size(0, 409)
            '
            'WebBrowser1
            '
            Me.WebBrowser1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.WebBrowser1.Location = New System.Drawing.Point(0, 25)
            Me.WebBrowser1.MinimumSize = New System.Drawing.Size(20, 20)
            Me.WebBrowser1.Name = "WebBrowser1"
            Me.WebBrowser1.Size = New System.Drawing.Size(559, 409)
            Me.WebBrowser1.TabIndex = 4
            '
            'BarButtonItem_File_Print
            '
            Me.BarButtonItem_File_Print.Caption = "Print"
            Me.BarButtonItem_File_Print.Id = 3
            Me.BarButtonItem_File_Print.Name = "BarButtonItem_File_Print"
            '
            'BaseForm
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(559, 456)
            Me.Controls.Add(Me.WebBrowser1)
            Me.Controls.Add(Me.barDockControlLeft)
            Me.Controls.Add(Me.barDockControlRight)
            Me.Controls.Add(Me.barDockControlBottom)
            Me.Controls.Add(Me.barDockControlTop)
            Me.Name = "BaseForm"
            Me.Text = "BaseForm"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Protected WithEvents BarManager1 As DevExpress.XtraBars.BarManager
        Protected WithEvents Bar_MainMenu As DevExpress.XtraBars.Bar
        Protected WithEvents BarSubItem_FileMenu As DevExpress.XtraBars.BarSubItem
        Protected WithEvents BarSubItem_ViewMenu As DevExpress.XtraBars.BarSubItem
        Protected WithEvents BarSubItem_EditMenu As DevExpress.XtraBars.BarSubItem
        Protected WithEvents Bar_StatusBar As DevExpress.XtraBars.Bar
        Protected WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
        Protected WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
        Protected WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
        Protected WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
        Protected WithEvents WebBrowser1 As System.Windows.Forms.WebBrowser
        Friend WithEvents BarButtonItem_File_Print As DevExpress.XtraBars.BarButtonItem
    End Class
End Namespace