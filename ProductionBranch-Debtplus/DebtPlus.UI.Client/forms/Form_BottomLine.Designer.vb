﻿Namespace forms
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form_BottomLine
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Dim StyleFormatCondition1 As DevExpress.XtraGrid.StyleFormatCondition = New DevExpress.XtraGrid.StyleFormatCondition()
            Dim StyleFormatCondition2 As DevExpress.XtraGrid.StyleFormatCondition = New DevExpress.XtraGrid.StyleFormatCondition()
            Dim StyleFormatCondition3 As DevExpress.XtraGrid.StyleFormatCondition = New DevExpress.XtraGrid.StyleFormatCondition()
            Me.GridColumn_rendering = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.barManager1 = New DevExpress.XtraBars.BarManager(Me.components)
            Me.Bar2 = New DevExpress.XtraBars.Bar()
            Me.BarSubItem1 = New DevExpress.XtraBars.BarSubItem()
            Me.BarButtonItem_PrintPreview = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Print = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Exit = New DevExpress.XtraBars.BarButtonItem()
            Me.BarSubItem2 = New DevExpress.XtraBars.BarSubItem()
            Me.BarCheckItem_StayOnTop = New DevExpress.XtraBars.BarCheckItem()
            Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
            Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
            Me.GridColumn_description = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_current = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_suggested = New DevExpress.XtraGrid.Columns.GridColumn()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridColumn_rendering
            '
            Me.GridColumn_rendering.Caption = "GridColumn1"
            Me.GridColumn_rendering.FieldName = "rendering"
            Me.GridColumn_rendering.Name = "GridColumn_rendering"
            '
            'barManager1
            '
            Me.barManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar2})
            Me.barManager1.DockControls.Add(Me.barDockControlTop)
            Me.barManager1.DockControls.Add(Me.barDockControlBottom)
            Me.barManager1.DockControls.Add(Me.barDockControlLeft)
            Me.barManager1.DockControls.Add(Me.barDockControlRight)
            Me.barManager1.Form = Me
            Me.barManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarSubItem1, Me.BarButtonItem_PrintPreview, Me.BarButtonItem_Exit, Me.BarButtonItem_Print, Me.BarSubItem2, Me.BarButtonItem1, Me.BarCheckItem_StayOnTop})
            Me.barManager1.MainMenu = Me.Bar2
            Me.barManager1.MaxItemId = 7
            '
            'Bar2
            '
            Me.Bar2.BarName = "Main menu"
            Me.Bar2.DockCol = 0
            Me.Bar2.DockRow = 0
            Me.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
            Me.Bar2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem2)})
            Me.Bar2.OptionsBar.MultiLine = True
            Me.Bar2.OptionsBar.UseWholeRow = True
            Me.Bar2.Text = "Main menu"
            '
            'BarSubItem1
            '
            Me.BarSubItem1.Caption = "&File"
            Me.BarSubItem1.Id = 0
            Me.BarSubItem1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_PrintPreview), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Print), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Exit, True)})
            Me.BarSubItem1.Name = "BarSubItem1"
            '
            'BarButtonItem_PrintPreview
            '
            Me.BarButtonItem_PrintPreview.Caption = "Print P&review..."
            Me.BarButtonItem_PrintPreview.Description = "Show printing preview dialog"
            Me.BarButtonItem_PrintPreview.Id = 1
            Me.BarButtonItem_PrintPreview.Name = "BarButtonItem_PrintPreview"
            '
            'BarButtonItem_Print
            '
            Me.BarButtonItem_Print.Caption = "&Print"
            Me.BarButtonItem_Print.Id = 3
            Me.BarButtonItem_Print.Name = "BarButtonItem_Print"
            '
            'BarButtonItem_Exit
            '
            Me.BarButtonItem_Exit.Caption = "&Exit"
            Me.BarButtonItem_Exit.Id = 2
            Me.BarButtonItem_Exit.Name = "BarButtonItem_Exit"
            '
            'BarSubItem2
            '
            Me.BarSubItem2.Caption = "&View"
            Me.BarSubItem2.Id = 4
            Me.BarSubItem2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarCheckItem_StayOnTop)})
            Me.BarSubItem2.Name = "BarSubItem2"
            '
            'BarCheckItem_StayOnTop
            '
            Me.BarCheckItem_StayOnTop.Caption = "Stay On Top"
            Me.BarCheckItem_StayOnTop.Id = 6
            Me.BarCheckItem_StayOnTop.Name = "BarCheckItem_StayOnTop"
            '
            'barDockControlTop
            '
            Me.barDockControlTop.CausesValidation = False
            Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
            Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
            Me.barDockControlTop.Size = New System.Drawing.Size(383, 22)
            '
            'barDockControlBottom
            '
            Me.barDockControlBottom.CausesValidation = False
            Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
            Me.barDockControlBottom.Location = New System.Drawing.Point(0, 314)
            Me.barDockControlBottom.Size = New System.Drawing.Size(383, 0)
            '
            'barDockControlLeft
            '
            Me.barDockControlLeft.CausesValidation = False
            Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
            Me.barDockControlLeft.Location = New System.Drawing.Point(0, 22)
            Me.barDockControlLeft.Size = New System.Drawing.Size(0, 292)
            '
            'barDockControlRight
            '
            Me.barDockControlRight.CausesValidation = False
            Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
            Me.barDockControlRight.Location = New System.Drawing.Point(383, 22)
            Me.barDockControlRight.Size = New System.Drawing.Size(0, 292)
            '
            'BarButtonItem1
            '
            Me.BarButtonItem1.Caption = "BarButtonItem1"
            Me.BarButtonItem1.Id = 5
            Me.BarButtonItem1.Name = "BarButtonItem1"
            '
            'GridControl1
            '
            Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.GridControl1.Location = New System.Drawing.Point(0, 22)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.MenuManager = Me.barManager1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(383, 292)
            Me.GridControl1.TabIndex = 9
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_description, Me.GridColumn_current, Me.GridColumn_suggested, Me.GridColumn_rendering})
            StyleFormatCondition1.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            StyleFormatCondition1.Appearance.Options.UseFont = True
            StyleFormatCondition1.ApplyToRow = True
            StyleFormatCondition1.Column = Me.GridColumn_rendering
            StyleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Expression
            StyleFormatCondition1.Expression = "[rendering] = 1 or [rendering] = 3 or [rendering] = 5"
            StyleFormatCondition2.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
            StyleFormatCondition2.Appearance.Options.UseFont = True
            StyleFormatCondition2.ApplyToRow = True
            StyleFormatCondition2.Expression = "[rendering] = 1"
            StyleFormatCondition3.Appearance.BackColor = System.Drawing.Color.Aqua
            StyleFormatCondition3.Appearance.BackColor2 = System.Drawing.Color.Aqua
            StyleFormatCondition3.Appearance.BorderColor = System.Drawing.Color.Aqua
            StyleFormatCondition3.Appearance.Options.UseBackColor = True
            StyleFormatCondition3.Appearance.Options.UseBorderColor = True
            StyleFormatCondition3.ApplyToRow = True
            StyleFormatCondition3.Expression = "[rendering] = 2 or [rendering] = 3"
            Me.GridView1.FormatConditions.AddRange(New DevExpress.XtraGrid.StyleFormatCondition() {StyleFormatCondition1, StyleFormatCondition2, StyleFormatCondition3})
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridView1.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridView1.OptionsBehavior.AutoPopulateColumns = False
            Me.GridView1.OptionsBehavior.AutoSelectAllInEditor = False
            Me.GridView1.OptionsBehavior.CacheValuesOnRowUpdating = DevExpress.Data.CacheRowValuesMode.Disabled
            Me.GridView1.OptionsBehavior.CopyToClipboardWithColumnHeaders = False
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsBehavior.FocusLeaveOnTab = True
            Me.GridView1.OptionsBehavior.KeepFocusedRowOnUpdate = False
            Me.GridView1.OptionsBehavior.ReadOnly = True
            Me.GridView1.OptionsBehavior.SmartVertScrollBar = False
            Me.GridView1.OptionsCustomization.AllowFilter = False
            Me.GridView1.OptionsCustomization.AllowGroup = False
            Me.GridView1.OptionsCustomization.AllowQuickHideColumns = False
            Me.GridView1.OptionsCustomization.AllowSort = False
            Me.GridView1.OptionsDetail.AllowZoomDetail = False
            Me.GridView1.OptionsDetail.EnableMasterViewMode = False
            Me.GridView1.OptionsDetail.ShowDetailTabs = False
            Me.GridView1.OptionsDetail.SmartDetailExpand = False
            Me.GridView1.OptionsFilter.AllowColumnMRUFilterList = False
            Me.GridView1.OptionsFilter.AllowFilterEditor = False
            Me.GridView1.OptionsFilter.AllowFilterIncrementalSearch = False
            Me.GridView1.OptionsFilter.AllowMRUFilterList = False
            Me.GridView1.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = False
            Me.GridView1.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = False
            Me.GridView1.OptionsFind.AllowFindPanel = False
            Me.GridView1.OptionsFind.ClearFindOnClose = False
            Me.GridView1.OptionsFind.HighlightFindResults = False
            Me.GridView1.OptionsFind.ShowCloseButton = False
            Me.GridView1.OptionsHint.ShowCellHints = False
            Me.GridView1.OptionsHint.ShowColumnHeaderHints = False
            Me.GridView1.OptionsHint.ShowFooterHints = False
            Me.GridView1.OptionsMenu.EnableColumnMenu = False
            Me.GridView1.OptionsMenu.EnableFooterMenu = False
            Me.GridView1.OptionsMenu.EnableGroupPanelMenu = False
            Me.GridView1.OptionsMenu.ShowAutoFilterRowItem = False
            Me.GridView1.OptionsMenu.ShowDateTimeGroupIntervalItems = False
            Me.GridView1.OptionsMenu.ShowGroupSortSummaryItems = False
            Me.GridView1.OptionsMenu.ShowSplitItem = False
            Me.GridView1.OptionsNavigation.AutoMoveRowFocus = False
            Me.GridView1.OptionsNavigation.UseOfficePageNavigation = False
            Me.GridView1.OptionsNavigation.UseTabKey = False
            Me.GridView1.OptionsPrint.AllowCancelPrintExport = False
            Me.GridView1.OptionsPrint.AutoResetPrintDocument = False
            Me.GridView1.OptionsPrint.AutoWidth = False
            Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
            Me.GridView1.OptionsSelection.EnableAppearanceFocusedRow = False
            Me.GridView1.OptionsSelection.EnableAppearanceHideSelection = False
            Me.GridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect
            Me.GridView1.OptionsSelection.UseIndicatorForSelection = False
            Me.GridView1.OptionsView.ShowGroupExpandCollapseButtons = False
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            '
            'GridColumn_description
            '
            Me.GridColumn_description.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_description.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_description.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None
            Me.GridColumn_description.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
            Me.GridColumn_description.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.GridColumn_description.CustomizationCaption = "description"
            Me.GridColumn_description.DisplayFormat.FormatString = "d"
            Me.GridColumn_description.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_description.FieldName = "description"
            Me.GridColumn_description.GroupFormat.FormatString = "d"
            Me.GridColumn_description.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_description.Name = "GridColumn_description"
            Me.GridColumn_description.OptionsColumn.AllowEdit = False
            Me.GridColumn_description.OptionsColumn.AllowFocus = False
            Me.GridColumn_description.OptionsColumn.AllowIncrementalSearch = False
            Me.GridColumn_description.OptionsColumn.AllowMove = False
            Me.GridColumn_description.OptionsColumn.AllowShowHide = False
            Me.GridColumn_description.OptionsColumn.ShowCaption = False
            Me.GridColumn_description.OptionsColumn.ShowInCustomizationForm = False
            Me.GridColumn_description.OptionsColumn.ShowInExpressionEditor = False
            Me.GridColumn_description.OptionsColumn.TabStop = False
            Me.GridColumn_description.OptionsFilter.AllowAutoFilter = False
            Me.GridColumn_description.OptionsFilter.AllowFilter = False
            Me.GridColumn_description.OptionsFilter.ImmediateUpdateAutoFilter = False
            Me.GridColumn_description.Visible = True
            Me.GridColumn_description.VisibleIndex = 0
            Me.GridColumn_description.Width = 181
            '
            'GridColumn_current
            '
            Me.GridColumn_current.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_current.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_current.AppearanceCell.TextOptions.Trimming = DevExpress.Utils.Trimming.None
            Me.GridColumn_current.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
            Me.GridColumn_current.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.GridColumn_current.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_current.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_current.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None
            Me.GridColumn_current.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
            Me.GridColumn_current.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.GridColumn_current.Caption = "Current"
            Me.GridColumn_current.CustomizationCaption = "current"
            Me.GridColumn_current.DisplayFormat.FormatString = "c"
            Me.GridColumn_current.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_current.FieldName = "current"
            Me.GridColumn_current.GroupFormat.FormatString = "c"
            Me.GridColumn_current.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_current.Name = "GridColumn_current"
            Me.GridColumn_current.Visible = True
            Me.GridColumn_current.VisibleIndex = 1
            Me.GridColumn_current.Width = 105
            '
            'GridColumn_suggested
            '
            Me.GridColumn_suggested.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_suggested.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_suggested.AppearanceCell.TextOptions.Trimming = DevExpress.Utils.Trimming.None
            Me.GridColumn_suggested.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
            Me.GridColumn_suggested.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.GridColumn_suggested.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_suggested.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_suggested.AppearanceHeader.TextOptions.Trimming = DevExpress.Utils.Trimming.None
            Me.GridColumn_suggested.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
            Me.GridColumn_suggested.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.GridColumn_suggested.Caption = "Suggested"
            Me.GridColumn_suggested.CustomizationCaption = "suggested"
            Me.GridColumn_suggested.DisplayFormat.FormatString = "c"
            Me.GridColumn_suggested.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_suggested.FieldName = "suggested"
            Me.GridColumn_suggested.GroupFormat.FormatString = "c"
            Me.GridColumn_suggested.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_suggested.Name = "GridColumn_suggested"
            Me.GridColumn_suggested.Visible = True
            Me.GridColumn_suggested.VisibleIndex = 2
            Me.GridColumn_suggested.Width = 95
            '
            'Form_BottomLine
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(383, 314)
            Me.Controls.Add(Me.GridControl1)
            Me.Controls.Add(Me.barDockControlLeft)
            Me.Controls.Add(Me.barDockControlRight)
            Me.Controls.Add(Me.barDockControlBottom)
            Me.Controls.Add(Me.barDockControlTop)
            Me.MaximizeBox = False
            Me.Name = "Form_BottomLine"
            Me.Text = "Bottom Line"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents barManager1 As DevExpress.XtraBars.BarManager
        Friend WithEvents Bar2 As DevExpress.XtraBars.Bar
        Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
        Friend WithEvents BarSubItem1 As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarButtonItem_PrintPreview As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Exit As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Print As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarSubItem2 As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarCheckItem_StayOnTop As DevExpress.XtraBars.BarCheckItem
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents GridColumn_description As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_current As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_suggested As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_rendering As DevExpress.XtraGrid.Columns.GridColumn
    End Class
End Namespace