﻿#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Imports System.Linq
Imports DebtPlus.LINQ
Imports DebtPlus.Data.Controls
Imports DebtPlus.Data.Forms
Imports DebtPlus.UI.Client.Service
Imports DebtPlus.UI.Common
Imports DebtPlus.Utils

Namespace forms
    Friend Class Form_ClientDisclosure
        Implements IContext

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Private Class display_property
            Public Property client_property As Housing_property
            Private targetAddressId As System.Nullable(Of Int32) = Nothing
            Private targetAddress As address = Nothing

            Public Sub New()
            End Sub

            Public Sub New(ByVal prop As Housing_property, targetAddressID As System.Nullable(Of Int32))
                MyClass.New()
                Me.client_property = prop
                Me.targetAddressId = targetAddressID
            End Sub

            Public ReadOnly Property Id As Int32
                Get
                    Return client_property.Id
                End Get
            End Property

            Private Function getTargetAddress() As address
                If targetAddress Is Nothing AndAlso targetAddressId.HasValue Then
                    Using bc As New BusinessContext()
                        targetAddress = bc.addresses.Where(Function(s) s.Id = targetAddressId.Value).FirstOrDefault()
                    End Using
                End If
                Return targetAddress
            End Function

            Public ReadOnly Property description As String
                Get
                    If getTargetAddress() Is Nothing Then
                        Return String.Empty
                    End If
                    Return getTargetAddress().AddressLine1()
                End Get
            End Property

            Public Overrides Function ToString() As String
                Return description
            End Function
        End Class

        Private colProperties As System.Collections.Generic.List(Of display_property) = Nothing
        Private record As client_disclosure = Nothing
        Private bc As DebtPlus.LINQ.BusinessContext = Nothing

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            DisclosureLanguageControl1.DefaultLanguageID = Nothing
        End Sub

        Public Sub New(ByVal Context As ClientUpdateClass)
            MyClass.New()
            privateContext = Context

            ' Set the language value for the lookup based upon the client information
            DisclosureLanguageControl1.DefaultLanguageID = Context.ClientDs.clientRecord.language
        End Sub

        Public Sub New(ByVal bc As DebtPlus.LINQ.BusinessContext, ByVal Context As ClientUpdateClass, ByVal record As client_disclosure)
            MyClass.New(Context)
            Me.bc = bc
            Me.record = record
            RegisterHandlers()
        End Sub

        Public Shadows Function ShowDialog(ByVal Owner As System.Windows.Forms.Form) As System.Windows.Forms.DialogResult

            If record IsNot Nothing AndAlso record.Id > 0 Then
                log.DebugFormat("ENTRY ShowDialog entry for record # {0:f0}", record.Id)
                Dim result As System.Windows.Forms.DialogResult = ShowWebPage(record)
                log.DebugFormat("EXIT ShowDialog record # {0:f0}, result {0}", record.Id, result.ToString())
                If result <> Windows.Forms.DialogResult.None Then
                    Return result
                End If
            End If

            log.Debug("ShowDialog calling default")
            Return MyBase.ShowDialog(Owner)
        End Function

        Public Shadows Function ShowDialog() As System.Windows.Forms.DialogResult

            If record IsNot Nothing AndAlso record.Id > 0 Then
                log.DebugFormat("ENTRY ShowDialog entry for record # {0:f0}", record.Id)
                Dim result As System.Windows.Forms.DialogResult = ShowWebPage(record)
                log.DebugFormat("EXIT ShowDialog record # {0:f0}, result {0}", record.Id, result.ToString())
                If result <> Windows.Forms.DialogResult.None Then
                    Return result
                End If
            End If

            log.Debug("ShowDialog calling default")
            Return MyBase.ShowDialog()
        End Function

        ''' <summary>
        ''' Pointer to the client update information for the current client
        ''' </summary>
        Private ReadOnly privateContext As ClientUpdateClass
        Public ReadOnly Property Context() As ClientUpdateClass Implements IContext.Context
            Get
                Return privateContext
            End Get
        End Property

        Private Sub RegisterHandlers()
            AddHandler LookUpEdit_propertyID.EditValueChanged, AddressOf Form_Changed
            AddHandler Load, AddressOf Form_ClientDisclosure_Load
            AddHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
            AddHandler LayoutControl1.Resize, AddressOf LayoutControl1_Resize
            AddHandler DisclosureLanguageControl1.EditValueChanged, AddressOf Form_Changed
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler LookUpEdit_propertyID.EditValueChanged, AddressOf Form_Changed
            RemoveHandler Load, AddressOf Form_ClientDisclosure_Load
            RemoveHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
            RemoveHandler LayoutControl1.Resize, AddressOf LayoutControl1_Resize
            RemoveHandler DisclosureLanguageControl1.EditValueChanged, AddressOf Form_Changed
        End Sub

        Private Sub Form_ClientDisclosure_Load(sender As Object, e As System.EventArgs)
            UnRegisterHandlers()
            Try
                log.Debug("ENTRY Form_ClientDisclosure_Load")

                ' Get the client address from the system if there is not one already defined.
                Dim clientAddress As System.Nullable(Of Int32) = Context.ClientDs.clientRecord.AddressID
                log.DebugFormat("client address id = {0}", If(clientAddress Is Nothing, "NULL", clientAddress.Value.ToString()))

                log.Debug("Reading property collection")
                colProperties = bc.Housing_properties.Where(Function(pr) pr.HousingID = Context.ClientDs.ClientId).Select(Function(pr) New display_property(pr, If(pr.UseHomeAddress, clientAddress, pr.PropertyAddress))).ToList()
                log.DebugFormat("property collection read, count = {0:f0}", colProperties.Count)

                ' Load the values into the controller
                LookUpEdit_propertyID.Properties.DataSource = colProperties
                log.DebugFormat("record.propertyID = {0}, record.disclosureID = {1}", If(record.propertyID.HasValue, record.propertyID.ToString(), "NULL"), record.disclosureID)
                LookUpEdit_propertyID.EditValue = record.propertyID
                DisclosureLanguageControl1.EditValue = record.disclosureID

                ' If it not allowed to change the type/language once they are defined.
                ' If we did then you would have to remove the old extension record and create a new one. This is just too difficult to do so we don't allow it.
                ' If you make a mistake then you can always delete the entire disclosure and create a new one with the proper type.
                DisclosureLanguageControl1.ReadOnly = record.Id > 0

                SimpleButton_OK.Enabled = Not HasErrors()
                EmptySpaceItem_Left.Width = (EmptySpaceItem_Left.Width + EmptySpaceItem_Right.Width) \ 2

            Finally
                RegisterHandlers()
            End Try

            log.Debug("EXIT Form_ClientDisclosure_Load")
        End Sub

        Private Sub Form_Changed(sender As Object, e As System.EventArgs)
            SimpleButton_OK.Enabled = Not HasErrors()
        End Sub

        Private Sub SimpleButton_OK_Click(sender As Object, e As System.EventArgs)
            log.Debug("ENTRY SimpleButton_OK_Click")

            ' Save the parameters for the record
            record.disclosureID = DisclosureLanguageControl1.EditValue.Value
            log.DebugFormat("disclosureID = {0:f0}", record.disclosureID)
            record.client_DisclosureLanguageType = bc.client_DisclosureLanguageTypes.Where(Function(s) s.Id = record.disclosureID).FirstOrDefault()
            log.DebugFormat("client_DisclosureLanguageType = {0:f0}", record.client_DisclosureLanguageType.Id)
            record.propertyID = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_propertyID.EditValue)
            log.DebugFormat("propertyID = {0}", If(record.propertyID.HasValue, record.propertyID.Value.ToString(), "NULL"))

            ' Conduct the web page now.
            DialogResult = ShowWebPage(record)
            log.DebugFormat("DialogResult = {0}", DialogResult.ToString())
            log.Debug("EXIT SimpleButton_OK_Click")
        End Sub

        Private Function ShowWebPage(clientDisclosureRecord As client_disclosure) As System.Windows.Forms.DialogResult
            Dim answer As System.Windows.Forms.DialogResult = Windows.Forms.DialogResult.None
            log.Debug("ENTRY ShowWebPage")

            ' Find the disclosure language record type
            Dim disclosureLanguageRecord As DebtPlus.LINQ.client_DisclosureLanguageType = clientDisclosureRecord.client_DisclosureLanguageType
            If disclosureLanguageRecord Is Nothing Then
                log.DebugFormat("EXIT ShowWebPage. Result = {0}", answer.ToString())
                Return answer
            End If

            ' From the disclosure language record, get the disclosure record and the URL
            Dim url As String = disclosureLanguageRecord.template_url
            log.DebugFormat("URL = {0}", url.ToString())

            ' Find the type of the disclosure from the language reference
            Dim disclosureRecord As DebtPlus.LINQ.client_DisclosureType = clientDisclosureRecord.client_DisclosureLanguageType.client_DisclosureType
            If disclosureLanguageRecord Is Nothing Then
                log.DebugFormat("EXIT ShowWebPage. Result = {0}", answer.ToString())
                Return answer
            End If

            Dim assemblyName As String = disclosureRecord.Assembly
            log.DebugFormat("AssemblyName = {0}", If(assemblyName, "NULL"))

            ' If there is no disclosure type then just return "success"
            If String.IsNullOrWhiteSpace(assemblyName) Then
                answer = System.Windows.Forms.DialogResult.OK
                log.DebugFormat("EXIT ShowWebPage. Result = {0}", answer.ToString())
                Return answer
            End If

            ' Find the type of the form from the name of the assembly
            Dim ProgramType As System.Type = DebtPlus.Utils.Modules.FindTypeFromName(assemblyName)
            If ProgramType Is Nothing Then
                DebtPlus.Data.Forms.MessageBox.Show(String.Format("Unable to find the class '{0}'", assemblyName), "Error running menu item", MessageBoxButtons.OK, MessageBoxIcon.Error)
                log.DebugFormat("EXIT ShowWebPage. Result = {0}", answer.ToString())
                Return answer
            End If

            Dim mmiInstance As Object = Nothing
            Dim constructorInfoObj As System.Reflection.ConstructorInfo = ProgramType.GetConstructor(New Type() {})
            mmiInstance = constructorInfoObj.Invoke(Nothing)
            If mmiInstance Is Nothing Then
                log.Error("Unable to create form instance")
                DebtPlus.Data.Forms.MessageBox.Show(String.Format("Unable to create form class '{0}'", assemblyName), "Error running menu item", MessageBoxButtons.OK, MessageBoxIcon.Error)
                log.DebugFormat("EXIT ShowWebPage. Result = {0}", answer.ToString())
                Return answer
            End If

            ' Find the function that takes a single argument of our disclosure record
            Using editingForm As forms.Disclosures.BaseForm = TryCast(mmiInstance, forms.Disclosures.BaseForm)
                If editingForm Is Nothing Then
                    log.ErrorFormat("Unable to translate form for type '{0}'", assemblyName)
                    DebtPlus.Data.Forms.MessageBox.Show(String.Format("Unable to translate form for type '{0}'", assemblyName), "Error running menu item", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    log.DebugFormat("EXIT ShowWebPage. Result = {0}", answer.ToString())
                    Return answer
                End If

                ' Ok. call the function do the operation
                log.Debug("Invoking form edit function")
                answer = editingForm.EditDialog(bc, Context, clientDisclosureRecord)
                log.DebugFormat("EXIT ShowWebPage. Result = {0}", answer.ToString())
                Return answer
            End Using
        End Function

        Private Function HasErrors() As Boolean

            ' We need a type and a language. (There is no language without a type so we need only test the language.)
            Dim languageRecord As DebtPlus.LINQ.client_DisclosureLanguageType = TryCast(DisclosureLanguageControl1.LookUpEdit_Language.GetSelectedDataRow(), DebtPlus.LINQ.client_DisclosureLanguageType)
            If languageRecord Is Nothing Then
                Return True
            End If

            ' Determine that there really is a disclosure record
            Dim disclosureRecord As DebtPlus.LINQ.client_DisclosureType = languageRecord.client_DisclosureType
            If disclosureRecord Is Nothing Then
                Return True
            End If

            ' If the disclosure record requires a property and there is no property then we can not continue
            If (LookUpEdit_propertyID.EditValue Is Nothing) AndAlso disclosureRecord.required_propertyID Then
                Return True
            End If

            ' The information is complete
            Return False
        End Function

        Private Sub LayoutControl1_Resize(sender As Object, e As System.EventArgs)
            EmptySpaceItem_Left.Width = (EmptySpaceItem_Left.Width + EmptySpaceItem_Right.Width) \ 2
        End Sub
    End Class
End Namespace