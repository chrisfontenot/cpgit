#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.LINQ
Imports DebtPlus.UI.Client.Service
Imports DevExpress.XtraEditors
Imports DevExpress.XtraEditors.Controls
Imports System.Linq

Namespace forms
    Friend Class Form_OtherDebt

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        ' Current record being edited
        Private record As client_other_debt = Nothing

        ''' <summary>
        ''' Initialize the form
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(record As client_other_debt)
            MyClass.New()
            Me.record = record
            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf Form_OtherDebt_Load
            AddHandler CalcEdit_Payment.Validating, AddressOf DebtPlus.Data.Validation.NonNegative
            AddHandler CalcEdit_APR.Validating, AddressOf DebtPlus.Data.Validation.NonNegative
            AddHandler CalcEdit_Balance.Validating, AddressOf DebtPlus.Data.Validation.NonNegative
            AddHandler CalcEdit_APR.Validated, AddressOf CalcEdit_APR_Validated
            AddHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler Load, AddressOf Form_OtherDebt_Load
            RemoveHandler CalcEdit_Payment.Validating, AddressOf DebtPlus.Data.Validation.NonNegative
            RemoveHandler CalcEdit_APR.Validating, AddressOf DebtPlus.Data.Validation.NonNegative
            RemoveHandler CalcEdit_Balance.Validating, AddressOf DebtPlus.Data.Validation.NonNegative
            RemoveHandler CalcEdit_APR.Validated, AddressOf CalcEdit_APR_Validated
            RemoveHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
        End Sub

        ''' <summary>
        ''' Process the FORM LOAD event
        ''' </summary>
        Private Sub Form_OtherDebt_Load(ByVal sender As Object, ByVal e As EventArgs)
            TextEdit_Creditor.EditValue = record.creditor_name
            TextEdit_AccountNumber.EditValue = record.account_number
            CalcEdit_Balance.EditValue = record.balance
            CalcEdit_Payment.EditValue = record.payment
            CalcEdit_APR.EditValue = record.interest_rate
        End Sub

        ''' <summary>
        ''' Correct the interest rate so that it is proper for the database
        ''' </summary>
        Private Sub CalcEdit_APR_Validated(ByVal sender As Object, ByVal e As EventArgs)
            Dim Rate As Double = Convert.ToDecimal(CalcEdit_APR.EditValue)
            If Rate >= 1.0# Then
                Do While Rate >= 1.0#
                    Rate /= 100.0#
                Loop
                CalcEdit_APR.EditValue = Rate
            End If
        End Sub

        Private Sub SimpleButton_OK_Click(sender As Object, e As System.EventArgs)
            record.creditor_name = DebtPlus.Utils.Nulls.v_String(TextEdit_Creditor.EditValue)
            record.account_number = DebtPlus.Utils.Nulls.v_String(TextEdit_AccountNumber.EditValue)
            record.balance = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_Balance.EditValue).GetValueOrDefault(0D)
            record.payment = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_Payment.EditValue).GetValueOrDefault(0D)
            record.interest_rate = DebtPlus.Utils.Nulls.v_Double(CalcEdit_APR.EditValue).GetValueOrDefault(0.0#)
        End Sub
    End Class
End Namespace
