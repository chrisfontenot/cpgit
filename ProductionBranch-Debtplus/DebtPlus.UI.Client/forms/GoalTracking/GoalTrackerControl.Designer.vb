﻿Imports DebtPlus.UI.Client.controls

Namespace forms.Goal

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class GoalTrackerControl
        Inherits ControlBaseClient

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then
                        components.Dispose()
                    End If
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem_Left = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem_Right = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem8 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem9 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem28 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem29 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem30 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem31 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem32 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem33 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem34 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem35 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem36 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem37 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem38 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem39 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem40 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem41 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem42 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem43 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
            Me.txtProgress3Goal1EnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.txtProgress2Goal1EnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.txtProgress1Goal1EnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.txtGoal1EnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
            Me.txtProgress3Goal2EnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.txtProgress2Goal2EnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.txtProgress1Goal2EnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.txtGoal2EnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
            Me.txtProgress3Goal3EnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.txtProgress2Goal3EnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.txtProgress1Goal3EnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.txtGoal3EnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
            Me.txtProgress3Goal4EnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.txtProgress2Goal4EnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.txtProgress1Goal4EnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.txtGoal4EnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.LabelControl25 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
            Me.txtProgress3BankedEnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.txtProgress2BankedEnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.txtProgress1BankedEnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.txtBankedEnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
            Me.txtProgress3UsFinanceEnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.txtProgress2UsFinanceEnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.txtProgress1UsFinanceEnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.txtUsFinanceEnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
            Me.txtProgress3CreditReportEnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.txtProgress2CreditReportEnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.txtProgress1CreditReportEnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.txtCreditReportEnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
            Me.txtProgress3ProgramParticipationEnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.txtProgress2ProgramParticipationEnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.txtProgress1ProgramParticipationEnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.txtProgramParticipationEnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
            Me.txtProgress3EduParticipationEnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.txtProgress2EduParticipationEnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.txtProgress1EduParticipationEnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.txtEduParticipationEnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.ComboBoxEdit_Goal2 = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_Goal3 = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_Goal4 = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_Progress1_Goal4 = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_Progress1_Goal3 = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_Progress1_Goal2 = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_Progress1_Goal1 = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_Progress2_Goal1 = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_Progress2_Goal2 = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_Progress2_Goal3 = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_Progress2_Goal4 = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_Progress3_Goal1 = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_Progress3_Goal2 = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_Progress3_Goal3 = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_Progress3_Goal4 = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_Banked = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_USFinance = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_CreditReport = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_ProgramParticipation = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_EduParticipation = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_Progress1_Banked = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_Progress1_USFinance = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_Progress1_CreditReport = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_Progress1_ProgramParticipation = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_Progress1_EduParticipation = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_Progress2_Banked = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_Progress2_USFinance = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_Progress2_CreditReport = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_Progress2_ProgramParticipation = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_Progress2_EduParticipation = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_Progress3_Banked = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_Progress3_USFinance = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_Progress3_CreditReport = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_Progress3_ProgramParticipation = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_Progress3_EduParticipation = New DevExpress.XtraEditors.LookUpEdit()
            Me.ComboBoxEdit_Goal1 = New DevExpress.XtraEditors.LookUpEdit()
            Me.txtFicoProgress3EnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.txtFicoProgress2EnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.txtFicoProgress1EnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.txtFicoEnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl28 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl29 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl30 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl31 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl32 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl33 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl34 = New DevExpress.XtraEditors.LabelControl()
            Me.txtFicoInitial = New DevExpress.XtraEditors.SpinEdit()
            Me.txtFicoProgress1 = New DevExpress.XtraEditors.SpinEdit()
            Me.txtFicoProgress2 = New DevExpress.XtraEditors.SpinEdit()
            Me.txtFicoProgress3 = New DevExpress.XtraEditors.SpinEdit()
            Me.txtDebtProgress3EnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.txtDebtProgress2EnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.txtDebtProgress1EnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.txtDebtEnteredDate = New DevExpress.XtraEditors.TextEdit()
            Me.LabelControl35 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl36 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl37 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl38 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl39 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl40 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl41 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl42 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl43 = New DevExpress.XtraEditors.LabelControl()
            Me.CalcEditDebtInitial = New DevExpress.XtraEditors.CalcEdit()
            Me.CalcEditDebtProgress1 = New DevExpress.XtraEditors.CalcEdit()
            Me.CalcEditDebtProgress2 = New DevExpress.XtraEditors.CalcEdit()
            Me.CalcEditDebtProgress3 = New DevExpress.XtraEditors.CalcEdit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem_Left, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem_Right, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem8, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem9, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem28, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem29, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem30, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem31, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem32, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem33, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem34, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem35, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem36, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem37, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem38, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem39, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem40, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem41, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem42, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem43, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtProgress3Goal1EnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtProgress2Goal1EnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtProgress1Goal1EnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtGoal1EnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtProgress3Goal2EnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtProgress2Goal2EnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtProgress1Goal2EnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtGoal2EnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtProgress3Goal3EnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtProgress2Goal3EnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtProgress1Goal3EnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtGoal3EnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtProgress3Goal4EnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtProgress2Goal4EnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtProgress1Goal4EnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtGoal4EnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtProgress3BankedEnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtProgress2BankedEnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtProgress1BankedEnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtBankedEnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtProgress3UsFinanceEnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtProgress2UsFinanceEnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtProgress1UsFinanceEnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtUsFinanceEnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtProgress3CreditReportEnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtProgress2CreditReportEnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtProgress1CreditReportEnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtCreditReportEnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtProgress3ProgramParticipationEnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtProgress2ProgramParticipationEnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtProgress1ProgramParticipationEnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtProgramParticipationEnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtProgress3EduParticipationEnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtProgress2EduParticipationEnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtProgress1EduParticipationEnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtEduParticipationEnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Goal2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Goal3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Goal4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Progress1_Goal4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Progress1_Goal3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Progress1_Goal2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Progress1_Goal1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Progress2_Goal1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Progress2_Goal2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Progress2_Goal3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Progress2_Goal4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Progress3_Goal1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Progress3_Goal2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Progress3_Goal3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Progress3_Goal4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Banked.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_USFinance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_CreditReport.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_ProgramParticipation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_EduParticipation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Progress1_Banked.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Progress1_USFinance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Progress1_CreditReport.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Progress1_ProgramParticipation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Progress1_EduParticipation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Progress2_Banked.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Progress2_USFinance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Progress2_CreditReport.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Progress2_ProgramParticipation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Progress2_EduParticipation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Progress3_Banked.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Progress3_USFinance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Progress3_CreditReport.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Progress3_ProgramParticipation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Progress3_EduParticipation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_Goal1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtFicoProgress3EnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtFicoProgress2EnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtFicoProgress1EnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtFicoEnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtFicoInitial.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtFicoProgress1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtFicoProgress2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtFicoProgress3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtDebtProgress3EnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtDebtProgress2EnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtDebtProgress1EnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txtDebtEnteredDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEditDebtInitial.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEditDebtProgress1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEditDebtProgress2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEditDebtProgress3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'EmptySpaceItem1
            '
            Me.EmptySpaceItem1.AllowHotTrack = False
            Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 57)
            Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Size = New System.Drawing.Size(343, 33)
            Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
            Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem2
            '
            Me.EmptySpaceItem2.AllowHotTrack = False
            Me.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Location = New System.Drawing.Point(0, 0)
            Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Size = New System.Drawing.Size(343, 33)
            Me.EmptySpaceItem2.Text = "EmptySpaceItem2"
            Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem3
            '
            Me.EmptySpaceItem3.AllowHotTrack = False
            Me.EmptySpaceItem3.CustomizationFormText = "EmptySpaceItem1"
            Me.EmptySpaceItem3.Location = New System.Drawing.Point(0, 57)
            Me.EmptySpaceItem3.Name = "EmptySpaceItem1"
            Me.EmptySpaceItem3.Size = New System.Drawing.Size(343, 33)
            Me.EmptySpaceItem3.Text = "EmptySpaceItem1"
            Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem_Left
            '
            Me.EmptySpaceItem_Left.AllowHotTrack = False
            Me.EmptySpaceItem_Left.CustomizationFormText = "EmptySpaceItem_Left"
            Me.EmptySpaceItem_Left.Location = New System.Drawing.Point(0, 90)
            Me.EmptySpaceItem_Left.Name = "EmptySpaceItem_Left"
            Me.EmptySpaceItem_Left.Size = New System.Drawing.Size(94, 27)
            Me.EmptySpaceItem_Left.Text = "EmptySpaceItem_Left"
            Me.EmptySpaceItem_Left.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem_Right
            '
            Me.EmptySpaceItem_Right.AllowHotTrack = False
            Me.EmptySpaceItem_Right.CustomizationFormText = "EmptySpaceItem_Right"
            Me.EmptySpaceItem_Right.Location = New System.Drawing.Point(252, 90)
            Me.EmptySpaceItem_Right.Name = "EmptySpaceItem_Right"
            Me.EmptySpaceItem_Right.Size = New System.Drawing.Size(91, 27)
            Me.EmptySpaceItem_Right.Text = "EmptySpaceItem_Right"
            Me.EmptySpaceItem_Right.TextSize = New System.Drawing.Size(0, 0)
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.CustomizationFormText = "Property Address"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 33)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(343, 24)
            Me.LayoutControlItem1.Text = "Line of Service"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(70, 13)
            Me.LayoutControlItem1.TextToControlDistance = 5
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.CustomizationFormText = "Property Address"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 33)
            Me.LayoutControlItem2.Name = "LayoutControlItem1"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(343, 24)
            Me.LayoutControlItem2.Text = "Line of Service"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(70, 13)
            Me.LayoutControlItem2.TextToControlDistance = 5
            '
            'EmptySpaceItem8
            '
            Me.EmptySpaceItem8.AllowHotTrack = False
            Me.EmptySpaceItem8.CustomizationFormText = "EmptySpaceItem4"
            Me.EmptySpaceItem8.Location = New System.Drawing.Point(5, 0)
            Me.EmptySpaceItem8.Name = "EmptySpaceItem4"
            Me.EmptySpaceItem8.Size = New System.Drawing.Size(89, 17)
            Me.EmptySpaceItem8.Text = "EmptySpaceItem4"
            Me.EmptySpaceItem8.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem9
            '
            Me.EmptySpaceItem9.AllowHotTrack = False
            Me.EmptySpaceItem9.CustomizationFormText = "EmptySpaceItem5"
            Me.EmptySpaceItem9.Location = New System.Drawing.Point(724, 0)
            Me.EmptySpaceItem9.Name = "EmptySpaceItem5"
            Me.EmptySpaceItem9.Size = New System.Drawing.Size(429, 17)
            Me.EmptySpaceItem9.Text = "EmptySpaceItem5"
            Me.EmptySpaceItem9.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem28
            '
            Me.EmptySpaceItem28.AllowHotTrack = False
            Me.EmptySpaceItem28.CustomizationFormText = "EmptySpaceItem28"
            Me.EmptySpaceItem28.Location = New System.Drawing.Point(1143, 41)
            Me.EmptySpaceItem28.Name = "EmptySpaceItem28"
            Me.EmptySpaceItem28.Size = New System.Drawing.Size(10, 24)
            Me.EmptySpaceItem28.Text = "EmptySpaceItem28"
            Me.EmptySpaceItem28.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem29
            '
            Me.EmptySpaceItem29.AllowHotTrack = False
            Me.EmptySpaceItem29.CustomizationFormText = "EmptySpaceItem29"
            Me.EmptySpaceItem29.Location = New System.Drawing.Point(1143, 65)
            Me.EmptySpaceItem29.Name = "EmptySpaceItem29"
            Me.EmptySpaceItem29.Size = New System.Drawing.Size(10, 24)
            Me.EmptySpaceItem29.Text = "EmptySpaceItem29"
            Me.EmptySpaceItem29.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem30
            '
            Me.EmptySpaceItem30.AllowHotTrack = False
            Me.EmptySpaceItem30.CustomizationFormText = "EmptySpaceItem30"
            Me.EmptySpaceItem30.Location = New System.Drawing.Point(1143, 89)
            Me.EmptySpaceItem30.Name = "EmptySpaceItem30"
            Me.EmptySpaceItem30.Size = New System.Drawing.Size(10, 24)
            Me.EmptySpaceItem30.Text = "EmptySpaceItem30"
            Me.EmptySpaceItem30.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem31
            '
            Me.EmptySpaceItem31.AllowHotTrack = False
            Me.EmptySpaceItem31.CustomizationFormText = "EmptySpaceItem31"
            Me.EmptySpaceItem31.Location = New System.Drawing.Point(1143, 140)
            Me.EmptySpaceItem31.Name = "EmptySpaceItem31"
            Me.EmptySpaceItem31.Size = New System.Drawing.Size(10, 24)
            Me.EmptySpaceItem31.Text = "EmptySpaceItem31"
            Me.EmptySpaceItem31.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem32
            '
            Me.EmptySpaceItem32.AllowHotTrack = False
            Me.EmptySpaceItem32.CustomizationFormText = "EmptySpaceItem32"
            Me.EmptySpaceItem32.Location = New System.Drawing.Point(1143, 164)
            Me.EmptySpaceItem32.Name = "EmptySpaceItem32"
            Me.EmptySpaceItem32.Size = New System.Drawing.Size(10, 24)
            Me.EmptySpaceItem32.Text = "EmptySpaceItem32"
            Me.EmptySpaceItem32.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem33
            '
            Me.EmptySpaceItem33.AllowHotTrack = False
            Me.EmptySpaceItem33.CustomizationFormText = "EmptySpaceItem33"
            Me.EmptySpaceItem33.Location = New System.Drawing.Point(1143, 188)
            Me.EmptySpaceItem33.Name = "EmptySpaceItem33"
            Me.EmptySpaceItem33.Size = New System.Drawing.Size(10, 24)
            Me.EmptySpaceItem33.Text = "EmptySpaceItem33"
            Me.EmptySpaceItem33.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem34
            '
            Me.EmptySpaceItem34.AllowHotTrack = False
            Me.EmptySpaceItem34.CustomizationFormText = "EmptySpaceItem34"
            Me.EmptySpaceItem34.Location = New System.Drawing.Point(1143, 212)
            Me.EmptySpaceItem34.Name = "EmptySpaceItem34"
            Me.EmptySpaceItem34.Size = New System.Drawing.Size(10, 24)
            Me.EmptySpaceItem34.Text = "EmptySpaceItem34"
            Me.EmptySpaceItem34.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem35
            '
            Me.EmptySpaceItem35.AllowHotTrack = False
            Me.EmptySpaceItem35.CustomizationFormText = "EmptySpaceItem35"
            Me.EmptySpaceItem35.Location = New System.Drawing.Point(1130, 236)
            Me.EmptySpaceItem35.Name = "EmptySpaceItem35"
            Me.EmptySpaceItem35.Size = New System.Drawing.Size(23, 24)
            Me.EmptySpaceItem35.Text = "EmptySpaceItem35"
            Me.EmptySpaceItem35.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem36
            '
            Me.EmptySpaceItem36.AllowHotTrack = False
            Me.EmptySpaceItem36.CustomizationFormText = "EmptySpaceItem36"
            Me.EmptySpaceItem36.Location = New System.Drawing.Point(249, 0)
            Me.EmptySpaceItem36.Name = "EmptySpaceItem36"
            Me.EmptySpaceItem36.Size = New System.Drawing.Size(83, 113)
            Me.EmptySpaceItem36.Text = "EmptySpaceItem36"
            Me.EmptySpaceItem36.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem37
            '
            Me.EmptySpaceItem37.AllowHotTrack = False
            Me.EmptySpaceItem37.CustomizationFormText = "EmptySpaceItem37"
            Me.EmptySpaceItem37.Location = New System.Drawing.Point(753, 17)
            Me.EmptySpaceItem37.Name = "EmptySpaceItem37"
            Me.EmptySpaceItem37.Size = New System.Drawing.Size(10, 96)
            Me.EmptySpaceItem37.Text = "EmptySpaceItem37"
            Me.EmptySpaceItem37.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem38
            '
            Me.EmptySpaceItem38.AllowHotTrack = False
            Me.EmptySpaceItem38.CustomizationFormText = "EmptySpaceItem38"
            Me.EmptySpaceItem38.Location = New System.Drawing.Point(818, 17)
            Me.EmptySpaceItem38.Name = "EmptySpaceItem38"
            Me.EmptySpaceItem38.Size = New System.Drawing.Size(10, 96)
            Me.EmptySpaceItem38.Text = "EmptySpaceItem38"
            Me.EmptySpaceItem38.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem39
            '
            Me.EmptySpaceItem39.AllowHotTrack = False
            Me.EmptySpaceItem39.CustomizationFormText = "EmptySpaceItem39"
            Me.EmptySpaceItem39.Location = New System.Drawing.Point(883, 17)
            Me.EmptySpaceItem39.Name = "EmptySpaceItem39"
            Me.EmptySpaceItem39.Size = New System.Drawing.Size(10, 96)
            Me.EmptySpaceItem39.Text = "EmptySpaceItem39"
            Me.EmptySpaceItem39.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem40
            '
            Me.EmptySpaceItem40.AllowHotTrack = False
            Me.EmptySpaceItem40.CustomizationFormText = "EmptySpaceItem40"
            Me.EmptySpaceItem40.Location = New System.Drawing.Point(948, 17)
            Me.EmptySpaceItem40.Name = "EmptySpaceItem40"
            Me.EmptySpaceItem40.Size = New System.Drawing.Size(10, 96)
            Me.EmptySpaceItem40.Text = "EmptySpaceItem40"
            Me.EmptySpaceItem40.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem41
            '
            Me.EmptySpaceItem41.AllowHotTrack = False
            Me.EmptySpaceItem41.CustomizationFormText = "EmptySpaceItem41"
            Me.EmptySpaceItem41.Location = New System.Drawing.Point(1013, 17)
            Me.EmptySpaceItem41.Name = "EmptySpaceItem41"
            Me.EmptySpaceItem41.Size = New System.Drawing.Size(10, 96)
            Me.EmptySpaceItem41.Text = "EmptySpaceItem41"
            Me.EmptySpaceItem41.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem42
            '
            Me.EmptySpaceItem42.AllowHotTrack = False
            Me.EmptySpaceItem42.CustomizationFormText = "EmptySpaceItem42"
            Me.EmptySpaceItem42.Location = New System.Drawing.Point(1078, 17)
            Me.EmptySpaceItem42.Name = "EmptySpaceItem42"
            Me.EmptySpaceItem42.Size = New System.Drawing.Size(10, 96)
            Me.EmptySpaceItem42.Text = "EmptySpaceItem42"
            Me.EmptySpaceItem42.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem43
            '
            Me.EmptySpaceItem43.AllowHotTrack = False
            Me.EmptySpaceItem43.CustomizationFormText = "EmptySpaceItem43"
            Me.EmptySpaceItem43.Location = New System.Drawing.Point(0, 270)
            Me.EmptySpaceItem43.Name = "EmptySpaceItem43"
            Me.EmptySpaceItem43.Size = New System.Drawing.Size(1153, 82)
            Me.EmptySpaceItem43.Text = "EmptySpaceItem43"
            Me.EmptySpaceItem43.TextSize = New System.Drawing.Size(0, 0)
            '
            'LabelControl17
            '
            Me.LabelControl17.Location = New System.Drawing.Point(1055, 18)
            Me.LabelControl17.Name = "LabelControl17"
            Me.LabelControl17.Size = New System.Drawing.Size(108, 13)
            Me.LabelControl17.TabIndex = 30
            Me.LabelControl17.Text = "Progress Final Entered"
            '
            'LabelControl16
            '
            Me.LabelControl16.Location = New System.Drawing.Point(924, 18)
            Me.LabelControl16.Name = "LabelControl16"
            Me.LabelControl16.Size = New System.Drawing.Size(67, 13)
            Me.LabelControl16.TabIndex = 29
            Me.LabelControl16.Text = "Progress Final"
            '
            'LabelControl15
            '
            Me.LabelControl15.Location = New System.Drawing.Point(818, 18)
            Me.LabelControl15.Name = "LabelControl15"
            Me.LabelControl15.Size = New System.Drawing.Size(92, 13)
            Me.LabelControl15.TabIndex = 28
            Me.LabelControl15.Text = "Progress 2 Entered"
            '
            'LabelControl14
            '
            Me.LabelControl14.Location = New System.Drawing.Point(688, 18)
            Me.LabelControl14.Name = "LabelControl14"
            Me.LabelControl14.Size = New System.Drawing.Size(51, 13)
            Me.LabelControl14.TabIndex = 27
            Me.LabelControl14.Text = "Progress 2"
            '
            'LabelControl13
            '
            Me.LabelControl13.Location = New System.Drawing.Point(581, 18)
            Me.LabelControl13.Name = "LabelControl13"
            Me.LabelControl13.Size = New System.Drawing.Size(92, 13)
            Me.LabelControl13.TabIndex = 26
            Me.LabelControl13.Text = "Progress 1 Entered"
            '
            'LabelControl12
            '
            Me.LabelControl12.Location = New System.Drawing.Point(450, 18)
            Me.LabelControl12.Name = "LabelControl12"
            Me.LabelControl12.Size = New System.Drawing.Size(51, 13)
            Me.LabelControl12.TabIndex = 25
            Me.LabelControl12.Text = "Progress 1"
            '
            'LabelControl11
            '
            Me.LabelControl11.Location = New System.Drawing.Point(342, 18)
            Me.LabelControl11.Name = "LabelControl11"
            Me.LabelControl11.Size = New System.Drawing.Size(62, 13)
            Me.LabelControl11.TabIndex = 24
            Me.LabelControl11.Text = "Goal Entered"
            '
            'LabelControl5
            '
            Me.LabelControl5.Location = New System.Drawing.Point(175, 18)
            Me.LabelControl5.Name = "LabelControl5"
            Me.LabelControl5.Size = New System.Drawing.Size(70, 13)
            Me.LabelControl5.TabIndex = 23
            Me.LabelControl5.Text = "Financial Goals"
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(18, 43)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(30, 13)
            Me.LabelControl1.TabIndex = 31
            Me.LabelControl1.Text = "Goal 1"
            '
            'txtProgress3Goal1EnteredDate
            '
            Me.txtProgress3Goal1EnteredDate.Location = New System.Drawing.Point(1055, 39)
            Me.txtProgress3Goal1EnteredDate.Name = "txtProgress3Goal1EnteredDate"
            Me.txtProgress3Goal1EnteredDate.Properties.ReadOnly = True
            Me.txtProgress3Goal1EnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtProgress3Goal1EnteredDate.TabIndex = 46
            Me.txtProgress3Goal1EnteredDate.TabStop = False
            '
            'txtProgress2Goal1EnteredDate
            '
            Me.txtProgress2Goal1EnteredDate.Location = New System.Drawing.Point(818, 39)
            Me.txtProgress2Goal1EnteredDate.Name = "txtProgress2Goal1EnteredDate"
            Me.txtProgress2Goal1EnteredDate.Properties.ReadOnly = True
            Me.txtProgress2Goal1EnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtProgress2Goal1EnteredDate.TabIndex = 44
            Me.txtProgress2Goal1EnteredDate.TabStop = False
            '
            'txtProgress1Goal1EnteredDate
            '
            Me.txtProgress1Goal1EnteredDate.Location = New System.Drawing.Point(580, 39)
            Me.txtProgress1Goal1EnteredDate.Name = "txtProgress1Goal1EnteredDate"
            Me.txtProgress1Goal1EnteredDate.Properties.ReadOnly = True
            Me.txtProgress1Goal1EnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtProgress1Goal1EnteredDate.TabIndex = 42
            Me.txtProgress1Goal1EnteredDate.TabStop = False
            '
            'txtGoal1EnteredDate
            '
            Me.txtGoal1EnteredDate.Location = New System.Drawing.Point(341, 39)
            Me.txtGoal1EnteredDate.Name = "txtGoal1EnteredDate"
            Me.txtGoal1EnteredDate.Properties.ReadOnly = True
            Me.txtGoal1EnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtGoal1EnteredDate.TabIndex = 40
            Me.txtGoal1EnteredDate.TabStop = False
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(18, 69)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(30, 13)
            Me.LabelControl2.TabIndex = 47
            Me.LabelControl2.Text = "Goal 2"
            '
            'txtProgress3Goal2EnteredDate
            '
            Me.txtProgress3Goal2EnteredDate.Location = New System.Drawing.Point(1055, 65)
            Me.txtProgress3Goal2EnteredDate.Name = "txtProgress3Goal2EnteredDate"
            Me.txtProgress3Goal2EnteredDate.Properties.ReadOnly = True
            Me.txtProgress3Goal2EnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtProgress3Goal2EnteredDate.TabIndex = 55
            Me.txtProgress3Goal2EnteredDate.TabStop = False
            '
            'txtProgress2Goal2EnteredDate
            '
            Me.txtProgress2Goal2EnteredDate.Location = New System.Drawing.Point(818, 65)
            Me.txtProgress2Goal2EnteredDate.Name = "txtProgress2Goal2EnteredDate"
            Me.txtProgress2Goal2EnteredDate.Properties.ReadOnly = True
            Me.txtProgress2Goal2EnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtProgress2Goal2EnteredDate.TabIndex = 53
            Me.txtProgress2Goal2EnteredDate.TabStop = False
            '
            'txtProgress1Goal2EnteredDate
            '
            Me.txtProgress1Goal2EnteredDate.Location = New System.Drawing.Point(580, 65)
            Me.txtProgress1Goal2EnteredDate.Name = "txtProgress1Goal2EnteredDate"
            Me.txtProgress1Goal2EnteredDate.Properties.ReadOnly = True
            Me.txtProgress1Goal2EnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtProgress1Goal2EnteredDate.TabIndex = 51
            Me.txtProgress1Goal2EnteredDate.TabStop = False
            '
            'txtGoal2EnteredDate
            '
            Me.txtGoal2EnteredDate.Location = New System.Drawing.Point(341, 65)
            Me.txtGoal2EnteredDate.Name = "txtGoal2EnteredDate"
            Me.txtGoal2EnteredDate.Properties.ReadOnly = True
            Me.txtGoal2EnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtGoal2EnteredDate.TabIndex = 49
            Me.txtGoal2EnteredDate.TabStop = False
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(18, 95)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(30, 13)
            Me.LabelControl3.TabIndex = 56
            Me.LabelControl3.Text = "Goal 3"
            '
            'txtProgress3Goal3EnteredDate
            '
            Me.txtProgress3Goal3EnteredDate.Location = New System.Drawing.Point(1055, 91)
            Me.txtProgress3Goal3EnteredDate.Name = "txtProgress3Goal3EnteredDate"
            Me.txtProgress3Goal3EnteredDate.Properties.ReadOnly = True
            Me.txtProgress3Goal3EnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtProgress3Goal3EnteredDate.TabIndex = 64
            Me.txtProgress3Goal3EnteredDate.TabStop = False
            '
            'txtProgress2Goal3EnteredDate
            '
            Me.txtProgress2Goal3EnteredDate.Location = New System.Drawing.Point(818, 91)
            Me.txtProgress2Goal3EnteredDate.Name = "txtProgress2Goal3EnteredDate"
            Me.txtProgress2Goal3EnteredDate.Properties.ReadOnly = True
            Me.txtProgress2Goal3EnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtProgress2Goal3EnteredDate.TabIndex = 62
            Me.txtProgress2Goal3EnteredDate.TabStop = False
            '
            'txtProgress1Goal3EnteredDate
            '
            Me.txtProgress1Goal3EnteredDate.Location = New System.Drawing.Point(580, 91)
            Me.txtProgress1Goal3EnteredDate.Name = "txtProgress1Goal3EnteredDate"
            Me.txtProgress1Goal3EnteredDate.Properties.ReadOnly = True
            Me.txtProgress1Goal3EnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtProgress1Goal3EnteredDate.TabIndex = 60
            Me.txtProgress1Goal3EnteredDate.TabStop = False
            '
            'txtGoal3EnteredDate
            '
            Me.txtGoal3EnteredDate.Location = New System.Drawing.Point(341, 91)
            Me.txtGoal3EnteredDate.Name = "txtGoal3EnteredDate"
            Me.txtGoal3EnteredDate.Properties.ReadOnly = True
            Me.txtGoal3EnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtGoal3EnteredDate.TabIndex = 58
            Me.txtGoal3EnteredDate.TabStop = False
            '
            'LabelControl4
            '
            Me.LabelControl4.Location = New System.Drawing.Point(18, 120)
            Me.LabelControl4.Name = "LabelControl4"
            Me.LabelControl4.Size = New System.Drawing.Size(30, 13)
            Me.LabelControl4.TabIndex = 65
            Me.LabelControl4.Text = "Goal 4"
            '
            'txtProgress3Goal4EnteredDate
            '
            Me.txtProgress3Goal4EnteredDate.Location = New System.Drawing.Point(1056, 116)
            Me.txtProgress3Goal4EnteredDate.Name = "txtProgress3Goal4EnteredDate"
            Me.txtProgress3Goal4EnteredDate.Properties.ReadOnly = True
            Me.txtProgress3Goal4EnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtProgress3Goal4EnteredDate.TabIndex = 73
            Me.txtProgress3Goal4EnteredDate.TabStop = False
            '
            'txtProgress2Goal4EnteredDate
            '
            Me.txtProgress2Goal4EnteredDate.Location = New System.Drawing.Point(819, 116)
            Me.txtProgress2Goal4EnteredDate.Name = "txtProgress2Goal4EnteredDate"
            Me.txtProgress2Goal4EnteredDate.Properties.ReadOnly = True
            Me.txtProgress2Goal4EnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtProgress2Goal4EnteredDate.TabIndex = 71
            Me.txtProgress2Goal4EnteredDate.TabStop = False
            '
            'txtProgress1Goal4EnteredDate
            '
            Me.txtProgress1Goal4EnteredDate.Location = New System.Drawing.Point(581, 116)
            Me.txtProgress1Goal4EnteredDate.Name = "txtProgress1Goal4EnteredDate"
            Me.txtProgress1Goal4EnteredDate.Properties.ReadOnly = True
            Me.txtProgress1Goal4EnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtProgress1Goal4EnteredDate.TabIndex = 69
            Me.txtProgress1Goal4EnteredDate.TabStop = False
            '
            'txtGoal4EnteredDate
            '
            Me.txtGoal4EnteredDate.Location = New System.Drawing.Point(341, 116)
            Me.txtGoal4EnteredDate.Name = "txtGoal4EnteredDate"
            Me.txtGoal4EnteredDate.Properties.ReadOnly = True
            Me.txtGoal4EnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtGoal4EnteredDate.TabIndex = 67
            Me.txtGoal4EnteredDate.TabStop = False
            '
            'LabelControl25
            '
            Me.LabelControl25.Location = New System.Drawing.Point(1055, 253)
            Me.LabelControl25.Name = "LabelControl25"
            Me.LabelControl25.Size = New System.Drawing.Size(108, 13)
            Me.LabelControl25.TabIndex = 81
            Me.LabelControl25.Text = "Progress Final Entered"
            '
            'LabelControl24
            '
            Me.LabelControl24.Location = New System.Drawing.Point(924, 253)
            Me.LabelControl24.Name = "LabelControl24"
            Me.LabelControl24.Size = New System.Drawing.Size(67, 13)
            Me.LabelControl24.TabIndex = 80
            Me.LabelControl24.Text = "Progress Final"
            '
            'LabelControl23
            '
            Me.LabelControl23.Location = New System.Drawing.Point(818, 253)
            Me.LabelControl23.Name = "LabelControl23"
            Me.LabelControl23.Size = New System.Drawing.Size(92, 13)
            Me.LabelControl23.TabIndex = 79
            Me.LabelControl23.Text = "Progress 2 Entered"
            '
            'LabelControl22
            '
            Me.LabelControl22.Location = New System.Drawing.Point(687, 253)
            Me.LabelControl22.Name = "LabelControl22"
            Me.LabelControl22.Size = New System.Drawing.Size(51, 13)
            Me.LabelControl22.TabIndex = 78
            Me.LabelControl22.Text = "Progress 2"
            '
            'LabelControl21
            '
            Me.LabelControl21.Location = New System.Drawing.Point(580, 253)
            Me.LabelControl21.Name = "LabelControl21"
            Me.LabelControl21.Size = New System.Drawing.Size(92, 13)
            Me.LabelControl21.TabIndex = 77
            Me.LabelControl21.Text = "Progress 1 Entered"
            '
            'LabelControl20
            '
            Me.LabelControl20.Location = New System.Drawing.Point(449, 253)
            Me.LabelControl20.Name = "LabelControl20"
            Me.LabelControl20.Size = New System.Drawing.Size(51, 13)
            Me.LabelControl20.TabIndex = 76
            Me.LabelControl20.Text = "Progress 1"
            '
            'LabelControl19
            '
            Me.LabelControl19.Location = New System.Drawing.Point(341, 253)
            Me.LabelControl19.Name = "LabelControl19"
            Me.LabelControl19.Size = New System.Drawing.Size(72, 13)
            Me.LabelControl19.TabIndex = 75
            Me.LabelControl19.Text = "Status Entered"
            '
            'LabelControl18
            '
            Me.LabelControl18.Location = New System.Drawing.Point(174, 253)
            Me.LabelControl18.Name = "LabelControl18"
            Me.LabelControl18.Size = New System.Drawing.Size(115, 13)
            Me.LabelControl18.TabIndex = 74
            Me.LabelControl18.Text = "Current Financial Status"
            '
            'LabelControl6
            '
            Me.LabelControl6.Location = New System.Drawing.Point(18, 277)
            Me.LabelControl6.Name = "LabelControl6"
            Me.LabelControl6.Size = New System.Drawing.Size(40, 13)
            Me.LabelControl6.TabIndex = 82
            Me.LabelControl6.Text = "Banked?"
            '
            'txtProgress3BankedEnteredDate
            '
            Me.txtProgress3BankedEnteredDate.Location = New System.Drawing.Point(1055, 273)
            Me.txtProgress3BankedEnteredDate.Name = "txtProgress3BankedEnteredDate"
            Me.txtProgress3BankedEnteredDate.Properties.ReadOnly = True
            Me.txtProgress3BankedEnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtProgress3BankedEnteredDate.TabIndex = 90
            Me.txtProgress3BankedEnteredDate.TabStop = False
            '
            'txtProgress2BankedEnteredDate
            '
            Me.txtProgress2BankedEnteredDate.Location = New System.Drawing.Point(818, 273)
            Me.txtProgress2BankedEnteredDate.Name = "txtProgress2BankedEnteredDate"
            Me.txtProgress2BankedEnteredDate.Properties.ReadOnly = True
            Me.txtProgress2BankedEnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtProgress2BankedEnteredDate.TabIndex = 88
            Me.txtProgress2BankedEnteredDate.TabStop = False
            '
            'txtProgress1BankedEnteredDate
            '
            Me.txtProgress1BankedEnteredDate.Location = New System.Drawing.Point(580, 273)
            Me.txtProgress1BankedEnteredDate.Name = "txtProgress1BankedEnteredDate"
            Me.txtProgress1BankedEnteredDate.Properties.ReadOnly = True
            Me.txtProgress1BankedEnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtProgress1BankedEnteredDate.TabIndex = 86
            Me.txtProgress1BankedEnteredDate.TabStop = False
            '
            'txtBankedEnteredDate
            '
            Me.txtBankedEnteredDate.Location = New System.Drawing.Point(341, 273)
            Me.txtBankedEnteredDate.Name = "txtBankedEnteredDate"
            Me.txtBankedEnteredDate.Properties.ReadOnly = True
            Me.txtBankedEnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtBankedEnteredDate.TabIndex = 84
            Me.txtBankedEnteredDate.TabStop = False
            '
            'LabelControl7
            '
            Me.LabelControl7.Location = New System.Drawing.Point(18, 303)
            Me.LabelControl7.Name = "LabelControl7"
            Me.LabelControl7.Size = New System.Drawing.Size(150, 13)
            Me.LabelControl7.TabIndex = 91
            Me.LabelControl7.Text = "Understand US Finance System"
            '
            'txtProgress3UsFinanceEnteredDate
            '
            Me.txtProgress3UsFinanceEnteredDate.Location = New System.Drawing.Point(1055, 299)
            Me.txtProgress3UsFinanceEnteredDate.Name = "txtProgress3UsFinanceEnteredDate"
            Me.txtProgress3UsFinanceEnteredDate.Properties.ReadOnly = True
            Me.txtProgress3UsFinanceEnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtProgress3UsFinanceEnteredDate.TabIndex = 99
            Me.txtProgress3UsFinanceEnteredDate.TabStop = False
            '
            'txtProgress2UsFinanceEnteredDate
            '
            Me.txtProgress2UsFinanceEnteredDate.Location = New System.Drawing.Point(818, 299)
            Me.txtProgress2UsFinanceEnteredDate.Name = "txtProgress2UsFinanceEnteredDate"
            Me.txtProgress2UsFinanceEnteredDate.Properties.ReadOnly = True
            Me.txtProgress2UsFinanceEnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtProgress2UsFinanceEnteredDate.TabIndex = 97
            Me.txtProgress2UsFinanceEnteredDate.TabStop = False
            '
            'txtProgress1UsFinanceEnteredDate
            '
            Me.txtProgress1UsFinanceEnteredDate.Location = New System.Drawing.Point(580, 299)
            Me.txtProgress1UsFinanceEnteredDate.Name = "txtProgress1UsFinanceEnteredDate"
            Me.txtProgress1UsFinanceEnteredDate.Properties.ReadOnly = True
            Me.txtProgress1UsFinanceEnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtProgress1UsFinanceEnteredDate.TabIndex = 95
            Me.txtProgress1UsFinanceEnteredDate.TabStop = False
            '
            'txtUsFinanceEnteredDate
            '
            Me.txtUsFinanceEnteredDate.Location = New System.Drawing.Point(341, 299)
            Me.txtUsFinanceEnteredDate.Name = "txtUsFinanceEnteredDate"
            Me.txtUsFinanceEnteredDate.Properties.ReadOnly = True
            Me.txtUsFinanceEnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtUsFinanceEnteredDate.TabIndex = 93
            Me.txtUsFinanceEnteredDate.TabStop = False
            '
            'LabelControl8
            '
            Me.LabelControl8.Location = New System.Drawing.Point(18, 328)
            Me.LabelControl8.Name = "LabelControl8"
            Me.LabelControl8.Size = New System.Drawing.Size(110, 13)
            Me.LabelControl8.TabIndex = 100
            Me.LabelControl8.Text = "Errors In Credit Report"
            '
            'txtProgress3CreditReportEnteredDate
            '
            Me.txtProgress3CreditReportEnteredDate.Location = New System.Drawing.Point(1055, 324)
            Me.txtProgress3CreditReportEnteredDate.Name = "txtProgress3CreditReportEnteredDate"
            Me.txtProgress3CreditReportEnteredDate.Properties.ReadOnly = True
            Me.txtProgress3CreditReportEnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtProgress3CreditReportEnteredDate.TabIndex = 108
            Me.txtProgress3CreditReportEnteredDate.TabStop = False
            '
            'txtProgress2CreditReportEnteredDate
            '
            Me.txtProgress2CreditReportEnteredDate.Location = New System.Drawing.Point(818, 324)
            Me.txtProgress2CreditReportEnteredDate.Name = "txtProgress2CreditReportEnteredDate"
            Me.txtProgress2CreditReportEnteredDate.Properties.ReadOnly = True
            Me.txtProgress2CreditReportEnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtProgress2CreditReportEnteredDate.TabIndex = 106
            Me.txtProgress2CreditReportEnteredDate.TabStop = False
            '
            'txtProgress1CreditReportEnteredDate
            '
            Me.txtProgress1CreditReportEnteredDate.Location = New System.Drawing.Point(580, 324)
            Me.txtProgress1CreditReportEnteredDate.Name = "txtProgress1CreditReportEnteredDate"
            Me.txtProgress1CreditReportEnteredDate.Properties.ReadOnly = True
            Me.txtProgress1CreditReportEnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtProgress1CreditReportEnteredDate.TabIndex = 104
            Me.txtProgress1CreditReportEnteredDate.TabStop = False
            '
            'txtCreditReportEnteredDate
            '
            Me.txtCreditReportEnteredDate.Location = New System.Drawing.Point(341, 324)
            Me.txtCreditReportEnteredDate.Name = "txtCreditReportEnteredDate"
            Me.txtCreditReportEnteredDate.Properties.ReadOnly = True
            Me.txtCreditReportEnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtCreditReportEnteredDate.TabIndex = 102
            Me.txtCreditReportEnteredDate.TabStop = False
            '
            'LabelControl9
            '
            Me.LabelControl9.Location = New System.Drawing.Point(18, 355)
            Me.LabelControl9.Name = "LabelControl9"
            Me.LabelControl9.Size = New System.Drawing.Size(102, 13)
            Me.LabelControl9.TabIndex = 109
            Me.LabelControl9.Text = "Program Participation"
            '
            'txtProgress3ProgramParticipationEnteredDate
            '
            Me.txtProgress3ProgramParticipationEnteredDate.Location = New System.Drawing.Point(1055, 351)
            Me.txtProgress3ProgramParticipationEnteredDate.Name = "txtProgress3ProgramParticipationEnteredDate"
            Me.txtProgress3ProgramParticipationEnteredDate.Properties.ReadOnly = True
            Me.txtProgress3ProgramParticipationEnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtProgress3ProgramParticipationEnteredDate.TabIndex = 117
            Me.txtProgress3ProgramParticipationEnteredDate.TabStop = False
            '
            'txtProgress2ProgramParticipationEnteredDate
            '
            Me.txtProgress2ProgramParticipationEnteredDate.Location = New System.Drawing.Point(818, 351)
            Me.txtProgress2ProgramParticipationEnteredDate.Name = "txtProgress2ProgramParticipationEnteredDate"
            Me.txtProgress2ProgramParticipationEnteredDate.Properties.ReadOnly = True
            Me.txtProgress2ProgramParticipationEnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtProgress2ProgramParticipationEnteredDate.TabIndex = 115
            Me.txtProgress2ProgramParticipationEnteredDate.TabStop = False
            '
            'txtProgress1ProgramParticipationEnteredDate
            '
            Me.txtProgress1ProgramParticipationEnteredDate.Location = New System.Drawing.Point(580, 351)
            Me.txtProgress1ProgramParticipationEnteredDate.Name = "txtProgress1ProgramParticipationEnteredDate"
            Me.txtProgress1ProgramParticipationEnteredDate.Properties.ReadOnly = True
            Me.txtProgress1ProgramParticipationEnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtProgress1ProgramParticipationEnteredDate.TabIndex = 113
            Me.txtProgress1ProgramParticipationEnteredDate.TabStop = False
            '
            'txtProgramParticipationEnteredDate
            '
            Me.txtProgramParticipationEnteredDate.Location = New System.Drawing.Point(341, 351)
            Me.txtProgramParticipationEnteredDate.Name = "txtProgramParticipationEnteredDate"
            Me.txtProgramParticipationEnteredDate.Properties.ReadOnly = True
            Me.txtProgramParticipationEnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtProgramParticipationEnteredDate.TabIndex = 111
            Me.txtProgramParticipationEnteredDate.TabStop = False
            '
            'LabelControl10
            '
            Me.LabelControl10.Location = New System.Drawing.Point(18, 381)
            Me.LabelControl10.Name = "LabelControl10"
            Me.LabelControl10.Size = New System.Drawing.Size(78, 13)
            Me.LabelControl10.TabIndex = 118
            Me.LabelControl10.Text = "Ed. Participation"
            '
            'txtProgress3EduParticipationEnteredDate
            '
            Me.txtProgress3EduParticipationEnteredDate.Location = New System.Drawing.Point(1055, 377)
            Me.txtProgress3EduParticipationEnteredDate.Name = "txtProgress3EduParticipationEnteredDate"
            Me.txtProgress3EduParticipationEnteredDate.Properties.ReadOnly = True
            Me.txtProgress3EduParticipationEnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtProgress3EduParticipationEnteredDate.TabIndex = 126
            Me.txtProgress3EduParticipationEnteredDate.TabStop = False
            '
            'txtProgress2EduParticipationEnteredDate
            '
            Me.txtProgress2EduParticipationEnteredDate.Location = New System.Drawing.Point(818, 377)
            Me.txtProgress2EduParticipationEnteredDate.Name = "txtProgress2EduParticipationEnteredDate"
            Me.txtProgress2EduParticipationEnteredDate.Properties.ReadOnly = True
            Me.txtProgress2EduParticipationEnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtProgress2EduParticipationEnteredDate.TabIndex = 124
            Me.txtProgress2EduParticipationEnteredDate.TabStop = False
            '
            'txtProgress1EduParticipationEnteredDate
            '
            Me.txtProgress1EduParticipationEnteredDate.Location = New System.Drawing.Point(580, 377)
            Me.txtProgress1EduParticipationEnteredDate.Name = "txtProgress1EduParticipationEnteredDate"
            Me.txtProgress1EduParticipationEnteredDate.Properties.ReadOnly = True
            Me.txtProgress1EduParticipationEnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtProgress1EduParticipationEnteredDate.TabIndex = 122
            Me.txtProgress1EduParticipationEnteredDate.TabStop = False
            '
            'txtEduParticipationEnteredDate
            '
            Me.txtEduParticipationEnteredDate.Location = New System.Drawing.Point(341, 377)
            Me.txtEduParticipationEnteredDate.Name = "txtEduParticipationEnteredDate"
            Me.txtEduParticipationEnteredDate.Properties.ReadOnly = True
            Me.txtEduParticipationEnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtEduParticipationEnteredDate.TabIndex = 120
            Me.txtEduParticipationEnteredDate.TabStop = False
            '
            'ComboBoxEdit_Goal2
            '
            Me.ComboBoxEdit_Goal2.Location = New System.Drawing.Point(174, 65)
            Me.ComboBoxEdit_Goal2.Name = "ComboBoxEdit_Goal2"
            Me.ComboBoxEdit_Goal2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Goal2.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_Goal2.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_Goal2.Properties.DropDownRows = 10
            Me.ComboBoxEdit_Goal2.Properties.NullText = ""
            Me.ComboBoxEdit_Goal2.Properties.PopupSizeable = False
            Me.ComboBoxEdit_Goal2.Properties.ShowFooter = False
            Me.ComboBoxEdit_Goal2.Properties.ShowHeader = False
            Me.ComboBoxEdit_Goal2.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_Goal2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_Goal2.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_Goal2.Size = New System.Drawing.Size(164, 20)
            Me.ComboBoxEdit_Goal2.TabIndex = 5
            '
            'ComboBoxEdit_Goal3
            '
            Me.ComboBoxEdit_Goal3.Location = New System.Drawing.Point(174, 91)
            Me.ComboBoxEdit_Goal3.Name = "ComboBoxEdit_Goal3"
            Me.ComboBoxEdit_Goal3.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.ComboBoxEdit_Goal3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Goal3.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_Goal3.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_Goal3.Properties.DropDownRows = 10
            Me.ComboBoxEdit_Goal3.Properties.NullText = ""
            Me.ComboBoxEdit_Goal3.Properties.PopupSizeable = False
            Me.ComboBoxEdit_Goal3.Properties.ShowFooter = False
            Me.ComboBoxEdit_Goal3.Properties.ShowHeader = False
            Me.ComboBoxEdit_Goal3.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_Goal3.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_Goal3.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_Goal3.Size = New System.Drawing.Size(164, 20)
            Me.ComboBoxEdit_Goal3.TabIndex = 9
            '
            'ComboBoxEdit_Goal4
            '
            Me.ComboBoxEdit_Goal4.Location = New System.Drawing.Point(174, 116)
            Me.ComboBoxEdit_Goal4.Name = "ComboBoxEdit_Goal4"
            Me.ComboBoxEdit_Goal4.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Goal4.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_Goal4.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_Goal4.Properties.NullText = ""
            Me.ComboBoxEdit_Goal4.Properties.PopupSizeable = False
            Me.ComboBoxEdit_Goal4.Properties.ShowFooter = False
            Me.ComboBoxEdit_Goal4.Properties.ShowHeader = False
            Me.ComboBoxEdit_Goal4.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_Goal4.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_Goal4.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_Goal4.Size = New System.Drawing.Size(164, 20)
            Me.ComboBoxEdit_Goal4.TabIndex = 13
            '
            'ComboBoxEdit_Progress1_Goal4
            '
            Me.ComboBoxEdit_Progress1_Goal4.Location = New System.Drawing.Point(449, 116)
            Me.ComboBoxEdit_Progress1_Goal4.Name = "ComboBoxEdit_Progress1_Goal4"
            Me.ComboBoxEdit_Progress1_Goal4.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Progress1_Goal4.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_Progress1_Goal4.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_Progress1_Goal4.Properties.DropDownRows = 4
            Me.ComboBoxEdit_Progress1_Goal4.Properties.NullText = ""
            Me.ComboBoxEdit_Progress1_Goal4.Properties.PopupSizeable = False
            Me.ComboBoxEdit_Progress1_Goal4.Properties.ShowFooter = False
            Me.ComboBoxEdit_Progress1_Goal4.Properties.ShowHeader = False
            Me.ComboBoxEdit_Progress1_Goal4.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_Progress1_Goal4.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_Progress1_Goal4.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_Progress1_Goal4.Size = New System.Drawing.Size(128, 20)
            Me.ComboBoxEdit_Progress1_Goal4.TabIndex = 14
            '
            'ComboBoxEdit_Progress1_Goal3
            '
            Me.ComboBoxEdit_Progress1_Goal3.Location = New System.Drawing.Point(449, 91)
            Me.ComboBoxEdit_Progress1_Goal3.Name = "ComboBoxEdit_Progress1_Goal3"
            Me.ComboBoxEdit_Progress1_Goal3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Progress1_Goal3.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_Progress1_Goal3.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_Progress1_Goal3.Properties.DropDownRows = 4
            Me.ComboBoxEdit_Progress1_Goal3.Properties.NullText = ""
            Me.ComboBoxEdit_Progress1_Goal3.Properties.PopupSizeable = False
            Me.ComboBoxEdit_Progress1_Goal3.Properties.ShowFooter = False
            Me.ComboBoxEdit_Progress1_Goal3.Properties.ShowHeader = False
            Me.ComboBoxEdit_Progress1_Goal3.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_Progress1_Goal3.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_Progress1_Goal3.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_Progress1_Goal3.Size = New System.Drawing.Size(128, 20)
            Me.ComboBoxEdit_Progress1_Goal3.TabIndex = 10
            '
            'ComboBoxEdit_Progress1_Goal2
            '
            Me.ComboBoxEdit_Progress1_Goal2.Location = New System.Drawing.Point(449, 65)
            Me.ComboBoxEdit_Progress1_Goal2.Name = "ComboBoxEdit_Progress1_Goal2"
            Me.ComboBoxEdit_Progress1_Goal2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Progress1_Goal2.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_Progress1_Goal2.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_Progress1_Goal2.Properties.DropDownRows = 4
            Me.ComboBoxEdit_Progress1_Goal2.Properties.NullText = ""
            Me.ComboBoxEdit_Progress1_Goal2.Properties.PopupSizeable = False
            Me.ComboBoxEdit_Progress1_Goal2.Properties.ShowFooter = False
            Me.ComboBoxEdit_Progress1_Goal2.Properties.ShowHeader = False
            Me.ComboBoxEdit_Progress1_Goal2.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_Progress1_Goal2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_Progress1_Goal2.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_Progress1_Goal2.Size = New System.Drawing.Size(128, 20)
            Me.ComboBoxEdit_Progress1_Goal2.TabIndex = 6
            '
            'ComboBoxEdit_Progress1_Goal1
            '
            Me.ComboBoxEdit_Progress1_Goal1.Location = New System.Drawing.Point(449, 39)
            Me.ComboBoxEdit_Progress1_Goal1.Name = "ComboBoxEdit_Progress1_Goal1"
            Me.ComboBoxEdit_Progress1_Goal1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Progress1_Goal1.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_Progress1_Goal1.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_Progress1_Goal1.Properties.DropDownRows = 4
            Me.ComboBoxEdit_Progress1_Goal1.Properties.NullText = ""
            Me.ComboBoxEdit_Progress1_Goal1.Properties.PopupSizeable = False
            Me.ComboBoxEdit_Progress1_Goal1.Properties.ShowFooter = False
            Me.ComboBoxEdit_Progress1_Goal1.Properties.ShowHeader = False
            Me.ComboBoxEdit_Progress1_Goal1.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_Progress1_Goal1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_Progress1_Goal1.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_Progress1_Goal1.Size = New System.Drawing.Size(128, 20)
            Me.ComboBoxEdit_Progress1_Goal1.TabIndex = 2
            '
            'ComboBoxEdit_Progress2_Goal1
            '
            Me.ComboBoxEdit_Progress2_Goal1.Location = New System.Drawing.Point(687, 39)
            Me.ComboBoxEdit_Progress2_Goal1.Name = "ComboBoxEdit_Progress2_Goal1"
            Me.ComboBoxEdit_Progress2_Goal1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Progress2_Goal1.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_Progress2_Goal1.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_Progress2_Goal1.Properties.DropDownRows = 4
            Me.ComboBoxEdit_Progress2_Goal1.Properties.NullText = ""
            Me.ComboBoxEdit_Progress2_Goal1.Properties.PopupSizeable = False
            Me.ComboBoxEdit_Progress2_Goal1.Properties.ShowFooter = False
            Me.ComboBoxEdit_Progress2_Goal1.Properties.ShowHeader = False
            Me.ComboBoxEdit_Progress2_Goal1.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_Progress2_Goal1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_Progress2_Goal1.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_Progress2_Goal1.Size = New System.Drawing.Size(128, 20)
            Me.ComboBoxEdit_Progress2_Goal1.TabIndex = 3
            '
            'ComboBoxEdit_Progress2_Goal2
            '
            Me.ComboBoxEdit_Progress2_Goal2.Location = New System.Drawing.Point(687, 65)
            Me.ComboBoxEdit_Progress2_Goal2.Name = "ComboBoxEdit_Progress2_Goal2"
            Me.ComboBoxEdit_Progress2_Goal2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Progress2_Goal2.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_Progress2_Goal2.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_Progress2_Goal2.Properties.DropDownRows = 4
            Me.ComboBoxEdit_Progress2_Goal2.Properties.NullText = ""
            Me.ComboBoxEdit_Progress2_Goal2.Properties.PopupSizeable = False
            Me.ComboBoxEdit_Progress2_Goal2.Properties.ShowFooter = False
            Me.ComboBoxEdit_Progress2_Goal2.Properties.ShowHeader = False
            Me.ComboBoxEdit_Progress2_Goal2.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_Progress2_Goal2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_Progress2_Goal2.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_Progress2_Goal2.Size = New System.Drawing.Size(128, 20)
            Me.ComboBoxEdit_Progress2_Goal2.TabIndex = 7
            '
            'ComboBoxEdit_Progress2_Goal3
            '
            Me.ComboBoxEdit_Progress2_Goal3.Location = New System.Drawing.Point(687, 91)
            Me.ComboBoxEdit_Progress2_Goal3.Name = "ComboBoxEdit_Progress2_Goal3"
            Me.ComboBoxEdit_Progress2_Goal3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Progress2_Goal3.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_Progress2_Goal3.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_Progress2_Goal3.Properties.DropDownRows = 4
            Me.ComboBoxEdit_Progress2_Goal3.Properties.NullText = ""
            Me.ComboBoxEdit_Progress2_Goal3.Properties.PopupSizeable = False
            Me.ComboBoxEdit_Progress2_Goal3.Properties.ShowFooter = False
            Me.ComboBoxEdit_Progress2_Goal3.Properties.ShowHeader = False
            Me.ComboBoxEdit_Progress2_Goal3.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_Progress2_Goal3.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_Progress2_Goal3.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_Progress2_Goal3.Size = New System.Drawing.Size(128, 20)
            Me.ComboBoxEdit_Progress2_Goal3.TabIndex = 11
            '
            'ComboBoxEdit_Progress2_Goal4
            '
            Me.ComboBoxEdit_Progress2_Goal4.Location = New System.Drawing.Point(687, 116)
            Me.ComboBoxEdit_Progress2_Goal4.Name = "ComboBoxEdit_Progress2_Goal4"
            Me.ComboBoxEdit_Progress2_Goal4.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Progress2_Goal4.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_Progress2_Goal4.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_Progress2_Goal4.Properties.DropDownRows = 4
            Me.ComboBoxEdit_Progress2_Goal4.Properties.NullText = ""
            Me.ComboBoxEdit_Progress2_Goal4.Properties.PopupSizeable = False
            Me.ComboBoxEdit_Progress2_Goal4.Properties.ShowFooter = False
            Me.ComboBoxEdit_Progress2_Goal4.Properties.ShowHeader = False
            Me.ComboBoxEdit_Progress2_Goal4.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_Progress2_Goal4.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_Progress2_Goal4.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_Progress2_Goal4.Size = New System.Drawing.Size(128, 20)
            Me.ComboBoxEdit_Progress2_Goal4.TabIndex = 15
            '
            'ComboBoxEdit_Progress3_Goal1
            '
            Me.ComboBoxEdit_Progress3_Goal1.Location = New System.Drawing.Point(924, 39)
            Me.ComboBoxEdit_Progress3_Goal1.Name = "ComboBoxEdit_Progress3_Goal1"
            Me.ComboBoxEdit_Progress3_Goal1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Progress3_Goal1.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_Progress3_Goal1.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_Progress3_Goal1.Properties.DropDownRows = 4
            Me.ComboBoxEdit_Progress3_Goal1.Properties.NullText = ""
            Me.ComboBoxEdit_Progress3_Goal1.Properties.PopupSizeable = False
            Me.ComboBoxEdit_Progress3_Goal1.Properties.ShowFooter = False
            Me.ComboBoxEdit_Progress3_Goal1.Properties.ShowHeader = False
            Me.ComboBoxEdit_Progress3_Goal1.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_Progress3_Goal1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_Progress3_Goal1.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_Progress3_Goal1.Size = New System.Drawing.Size(128, 20)
            Me.ComboBoxEdit_Progress3_Goal1.TabIndex = 4
            '
            'ComboBoxEdit_Progress3_Goal2
            '
            Me.ComboBoxEdit_Progress3_Goal2.Location = New System.Drawing.Point(924, 65)
            Me.ComboBoxEdit_Progress3_Goal2.Name = "ComboBoxEdit_Progress3_Goal2"
            Me.ComboBoxEdit_Progress3_Goal2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Progress3_Goal2.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_Progress3_Goal2.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_Progress3_Goal2.Properties.DropDownRows = 4
            Me.ComboBoxEdit_Progress3_Goal2.Properties.NullText = ""
            Me.ComboBoxEdit_Progress3_Goal2.Properties.PopupSizeable = False
            Me.ComboBoxEdit_Progress3_Goal2.Properties.ShowFooter = False
            Me.ComboBoxEdit_Progress3_Goal2.Properties.ShowHeader = False
            Me.ComboBoxEdit_Progress3_Goal2.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_Progress3_Goal2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_Progress3_Goal2.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_Progress3_Goal2.Size = New System.Drawing.Size(128, 20)
            Me.ComboBoxEdit_Progress3_Goal2.TabIndex = 8
            '
            'ComboBoxEdit_Progress3_Goal3
            '
            Me.ComboBoxEdit_Progress3_Goal3.Location = New System.Drawing.Point(924, 91)
            Me.ComboBoxEdit_Progress3_Goal3.Name = "ComboBoxEdit_Progress3_Goal3"
            Me.ComboBoxEdit_Progress3_Goal3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Progress3_Goal3.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_Progress3_Goal3.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_Progress3_Goal3.Properties.DropDownRows = 4
            Me.ComboBoxEdit_Progress3_Goal3.Properties.NullText = ""
            Me.ComboBoxEdit_Progress3_Goal3.Properties.PopupSizeable = False
            Me.ComboBoxEdit_Progress3_Goal3.Properties.ShowFooter = False
            Me.ComboBoxEdit_Progress3_Goal3.Properties.ShowHeader = False
            Me.ComboBoxEdit_Progress3_Goal3.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_Progress3_Goal3.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_Progress3_Goal3.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_Progress3_Goal3.Size = New System.Drawing.Size(128, 20)
            Me.ComboBoxEdit_Progress3_Goal3.TabIndex = 12
            '
            'ComboBoxEdit_Progress3_Goal4
            '
            Me.ComboBoxEdit_Progress3_Goal4.Location = New System.Drawing.Point(924, 116)
            Me.ComboBoxEdit_Progress3_Goal4.Name = "ComboBoxEdit_Progress3_Goal4"
            Me.ComboBoxEdit_Progress3_Goal4.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Progress3_Goal4.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_Progress3_Goal4.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_Progress3_Goal4.Properties.DropDownRows = 4
            Me.ComboBoxEdit_Progress3_Goal4.Properties.NullText = ""
            Me.ComboBoxEdit_Progress3_Goal4.Properties.PopupSizeable = False
            Me.ComboBoxEdit_Progress3_Goal4.Properties.ShowFooter = False
            Me.ComboBoxEdit_Progress3_Goal4.Properties.ShowHeader = False
            Me.ComboBoxEdit_Progress3_Goal4.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_Progress3_Goal4.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_Progress3_Goal4.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_Progress3_Goal4.Size = New System.Drawing.Size(128, 20)
            Me.ComboBoxEdit_Progress3_Goal4.TabIndex = 16
            '
            'ComboBoxEdit_Banked
            '
            Me.ComboBoxEdit_Banked.Location = New System.Drawing.Point(174, 273)
            Me.ComboBoxEdit_Banked.Name = "ComboBoxEdit_Banked"
            Me.ComboBoxEdit_Banked.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Banked.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_Banked.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_Banked.Properties.DropDownRows = 4
            Me.ComboBoxEdit_Banked.Properties.NullText = ""
            Me.ComboBoxEdit_Banked.Properties.PopupSizeable = False
            Me.ComboBoxEdit_Banked.Properties.ShowFooter = False
            Me.ComboBoxEdit_Banked.Properties.ShowHeader = False
            Me.ComboBoxEdit_Banked.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_Banked.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_Banked.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_Banked.Size = New System.Drawing.Size(164, 20)
            Me.ComboBoxEdit_Banked.TabIndex = 25
            '
            'ComboBoxEdit_USFinance
            '
            Me.ComboBoxEdit_USFinance.Location = New System.Drawing.Point(174, 299)
            Me.ComboBoxEdit_USFinance.Name = "ComboBoxEdit_USFinance"
            Me.ComboBoxEdit_USFinance.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_USFinance.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_USFinance.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_USFinance.Properties.DropDownRows = 6
            Me.ComboBoxEdit_USFinance.Properties.NullText = ""
            Me.ComboBoxEdit_USFinance.Properties.PopupSizeable = False
            Me.ComboBoxEdit_USFinance.Properties.ShowFooter = False
            Me.ComboBoxEdit_USFinance.Properties.ShowHeader = False
            Me.ComboBoxEdit_USFinance.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_USFinance.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_USFinance.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_USFinance.Size = New System.Drawing.Size(164, 20)
            Me.ComboBoxEdit_USFinance.TabIndex = 29
            '
            'ComboBoxEdit_CreditReport
            '
            Me.ComboBoxEdit_CreditReport.Location = New System.Drawing.Point(174, 324)
            Me.ComboBoxEdit_CreditReport.Name = "ComboBoxEdit_CreditReport"
            Me.ComboBoxEdit_CreditReport.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_CreditReport.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_CreditReport.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_CreditReport.Properties.DropDownRows = 5
            Me.ComboBoxEdit_CreditReport.Properties.NullText = ""
            Me.ComboBoxEdit_CreditReport.Properties.PopupSizeable = False
            Me.ComboBoxEdit_CreditReport.Properties.ShowFooter = False
            Me.ComboBoxEdit_CreditReport.Properties.ShowHeader = False
            Me.ComboBoxEdit_CreditReport.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_CreditReport.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_CreditReport.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_CreditReport.Size = New System.Drawing.Size(164, 20)
            Me.ComboBoxEdit_CreditReport.TabIndex = 33
            '
            'ComboBoxEdit_ProgramParticipation
            '
            Me.ComboBoxEdit_ProgramParticipation.Location = New System.Drawing.Point(174, 351)
            Me.ComboBoxEdit_ProgramParticipation.Name = "ComboBoxEdit_ProgramParticipation"
            Me.ComboBoxEdit_ProgramParticipation.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_ProgramParticipation.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_ProgramParticipation.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_ProgramParticipation.Properties.DropDownRows = 4
            Me.ComboBoxEdit_ProgramParticipation.Properties.NullText = ""
            Me.ComboBoxEdit_ProgramParticipation.Properties.PopupSizeable = False
            Me.ComboBoxEdit_ProgramParticipation.Properties.ShowFooter = False
            Me.ComboBoxEdit_ProgramParticipation.Properties.ShowHeader = False
            Me.ComboBoxEdit_ProgramParticipation.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_ProgramParticipation.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_ProgramParticipation.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_ProgramParticipation.Size = New System.Drawing.Size(164, 20)
            Me.ComboBoxEdit_ProgramParticipation.TabIndex = 37
            '
            'ComboBoxEdit_EduParticipation
            '
            Me.ComboBoxEdit_EduParticipation.Location = New System.Drawing.Point(174, 377)
            Me.ComboBoxEdit_EduParticipation.Name = "ComboBoxEdit_EduParticipation"
            Me.ComboBoxEdit_EduParticipation.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_EduParticipation.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_EduParticipation.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_EduParticipation.Properties.DropDownRows = 4
            Me.ComboBoxEdit_EduParticipation.Properties.NullText = ""
            Me.ComboBoxEdit_EduParticipation.Properties.PopupSizeable = False
            Me.ComboBoxEdit_EduParticipation.Properties.ShowFooter = False
            Me.ComboBoxEdit_EduParticipation.Properties.ShowHeader = False
            Me.ComboBoxEdit_EduParticipation.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_EduParticipation.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_EduParticipation.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_EduParticipation.Size = New System.Drawing.Size(164, 20)
            Me.ComboBoxEdit_EduParticipation.TabIndex = 41
            '
            'ComboBoxEdit_Progress1_Banked
            '
            Me.ComboBoxEdit_Progress1_Banked.Location = New System.Drawing.Point(449, 273)
            Me.ComboBoxEdit_Progress1_Banked.Name = "ComboBoxEdit_Progress1_Banked"
            Me.ComboBoxEdit_Progress1_Banked.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Progress1_Banked.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_Progress1_Banked.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_Progress1_Banked.Properties.DropDownRows = 4
            Me.ComboBoxEdit_Progress1_Banked.Properties.NullText = ""
            Me.ComboBoxEdit_Progress1_Banked.Properties.PopupSizeable = False
            Me.ComboBoxEdit_Progress1_Banked.Properties.ShowFooter = False
            Me.ComboBoxEdit_Progress1_Banked.Properties.ShowHeader = False
            Me.ComboBoxEdit_Progress1_Banked.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_Progress1_Banked.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_Progress1_Banked.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_Progress1_Banked.Size = New System.Drawing.Size(128, 20)
            Me.ComboBoxEdit_Progress1_Banked.TabIndex = 26
            '
            'ComboBoxEdit_Progress1_USFinance
            '
            Me.ComboBoxEdit_Progress1_USFinance.Location = New System.Drawing.Point(449, 299)
            Me.ComboBoxEdit_Progress1_USFinance.Name = "ComboBoxEdit_Progress1_USFinance"
            Me.ComboBoxEdit_Progress1_USFinance.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Progress1_USFinance.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_Progress1_USFinance.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_Progress1_USFinance.Properties.DropDownRows = 6
            Me.ComboBoxEdit_Progress1_USFinance.Properties.NullText = ""
            Me.ComboBoxEdit_Progress1_USFinance.Properties.PopupSizeable = False
            Me.ComboBoxEdit_Progress1_USFinance.Properties.ShowFooter = False
            Me.ComboBoxEdit_Progress1_USFinance.Properties.ShowHeader = False
            Me.ComboBoxEdit_Progress1_USFinance.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_Progress1_USFinance.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_Progress1_USFinance.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_Progress1_USFinance.Size = New System.Drawing.Size(128, 20)
            Me.ComboBoxEdit_Progress1_USFinance.TabIndex = 30
            '
            'ComboBoxEdit_Progress1_CreditReport
            '
            Me.ComboBoxEdit_Progress1_CreditReport.Location = New System.Drawing.Point(449, 324)
            Me.ComboBoxEdit_Progress1_CreditReport.Name = "ComboBoxEdit_Progress1_CreditReport"
            Me.ComboBoxEdit_Progress1_CreditReport.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Progress1_CreditReport.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_Progress1_CreditReport.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_Progress1_CreditReport.Properties.DropDownRows = 5
            Me.ComboBoxEdit_Progress1_CreditReport.Properties.NullText = ""
            Me.ComboBoxEdit_Progress1_CreditReport.Properties.PopupSizeable = False
            Me.ComboBoxEdit_Progress1_CreditReport.Properties.ShowFooter = False
            Me.ComboBoxEdit_Progress1_CreditReport.Properties.ShowHeader = False
            Me.ComboBoxEdit_Progress1_CreditReport.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_Progress1_CreditReport.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_Progress1_CreditReport.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_Progress1_CreditReport.Size = New System.Drawing.Size(128, 20)
            Me.ComboBoxEdit_Progress1_CreditReport.TabIndex = 34
            '
            'ComboBoxEdit_Progress1_ProgramParticipation
            '
            Me.ComboBoxEdit_Progress1_ProgramParticipation.Location = New System.Drawing.Point(449, 351)
            Me.ComboBoxEdit_Progress1_ProgramParticipation.Name = "ComboBoxEdit_Progress1_ProgramParticipation"
            Me.ComboBoxEdit_Progress1_ProgramParticipation.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Progress1_ProgramParticipation.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_Progress1_ProgramParticipation.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_Progress1_ProgramParticipation.Properties.DropDownRows = 4
            Me.ComboBoxEdit_Progress1_ProgramParticipation.Properties.NullText = ""
            Me.ComboBoxEdit_Progress1_ProgramParticipation.Properties.PopupSizeable = False
            Me.ComboBoxEdit_Progress1_ProgramParticipation.Properties.ShowFooter = False
            Me.ComboBoxEdit_Progress1_ProgramParticipation.Properties.ShowHeader = False
            Me.ComboBoxEdit_Progress1_ProgramParticipation.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_Progress1_ProgramParticipation.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_Progress1_ProgramParticipation.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_Progress1_ProgramParticipation.Size = New System.Drawing.Size(128, 20)
            Me.ComboBoxEdit_Progress1_ProgramParticipation.TabIndex = 38
            '
            'ComboBoxEdit_Progress1_EduParticipation
            '
            Me.ComboBoxEdit_Progress1_EduParticipation.Location = New System.Drawing.Point(449, 377)
            Me.ComboBoxEdit_Progress1_EduParticipation.Name = "ComboBoxEdit_Progress1_EduParticipation"
            Me.ComboBoxEdit_Progress1_EduParticipation.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Progress1_EduParticipation.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_Progress1_EduParticipation.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_Progress1_EduParticipation.Properties.DropDownRows = 4
            Me.ComboBoxEdit_Progress1_EduParticipation.Properties.NullText = ""
            Me.ComboBoxEdit_Progress1_EduParticipation.Properties.PopupSizeable = False
            Me.ComboBoxEdit_Progress1_EduParticipation.Properties.ShowFooter = False
            Me.ComboBoxEdit_Progress1_EduParticipation.Properties.ShowHeader = False
            Me.ComboBoxEdit_Progress1_EduParticipation.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_Progress1_EduParticipation.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_Progress1_EduParticipation.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_Progress1_EduParticipation.Size = New System.Drawing.Size(128, 20)
            Me.ComboBoxEdit_Progress1_EduParticipation.TabIndex = 42
            '
            'ComboBoxEdit_Progress2_Banked
            '
            Me.ComboBoxEdit_Progress2_Banked.Location = New System.Drawing.Point(687, 273)
            Me.ComboBoxEdit_Progress2_Banked.Name = "ComboBoxEdit_Progress2_Banked"
            Me.ComboBoxEdit_Progress2_Banked.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Progress2_Banked.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_Progress2_Banked.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_Progress2_Banked.Properties.DropDownRows = 4
            Me.ComboBoxEdit_Progress2_Banked.Properties.NullText = ""
            Me.ComboBoxEdit_Progress2_Banked.Properties.PopupSizeable = False
            Me.ComboBoxEdit_Progress2_Banked.Properties.ShowFooter = False
            Me.ComboBoxEdit_Progress2_Banked.Properties.ShowHeader = False
            Me.ComboBoxEdit_Progress2_Banked.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_Progress2_Banked.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_Progress2_Banked.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_Progress2_Banked.Size = New System.Drawing.Size(128, 20)
            Me.ComboBoxEdit_Progress2_Banked.TabIndex = 27
            '
            'ComboBoxEdit_Progress2_USFinance
            '
            Me.ComboBoxEdit_Progress2_USFinance.Location = New System.Drawing.Point(687, 299)
            Me.ComboBoxEdit_Progress2_USFinance.Name = "ComboBoxEdit_Progress2_USFinance"
            Me.ComboBoxEdit_Progress2_USFinance.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Progress2_USFinance.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_Progress2_USFinance.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_Progress2_USFinance.Properties.DropDownRows = 6
            Me.ComboBoxEdit_Progress2_USFinance.Properties.NullText = ""
            Me.ComboBoxEdit_Progress2_USFinance.Properties.PopupSizeable = False
            Me.ComboBoxEdit_Progress2_USFinance.Properties.ShowFooter = False
            Me.ComboBoxEdit_Progress2_USFinance.Properties.ShowHeader = False
            Me.ComboBoxEdit_Progress2_USFinance.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_Progress2_USFinance.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_Progress2_USFinance.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_Progress2_USFinance.Size = New System.Drawing.Size(128, 20)
            Me.ComboBoxEdit_Progress2_USFinance.TabIndex = 31
            '
            'ComboBoxEdit_Progress2_CreditReport
            '
            Me.ComboBoxEdit_Progress2_CreditReport.Location = New System.Drawing.Point(687, 324)
            Me.ComboBoxEdit_Progress2_CreditReport.Name = "ComboBoxEdit_Progress2_CreditReport"
            Me.ComboBoxEdit_Progress2_CreditReport.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Progress2_CreditReport.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_Progress2_CreditReport.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_Progress2_CreditReport.Properties.DropDownRows = 5
            Me.ComboBoxEdit_Progress2_CreditReport.Properties.NullText = ""
            Me.ComboBoxEdit_Progress2_CreditReport.Properties.PopupSizeable = False
            Me.ComboBoxEdit_Progress2_CreditReport.Properties.ShowFooter = False
            Me.ComboBoxEdit_Progress2_CreditReport.Properties.ShowHeader = False
            Me.ComboBoxEdit_Progress2_CreditReport.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_Progress2_CreditReport.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_Progress2_CreditReport.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_Progress2_CreditReport.Size = New System.Drawing.Size(128, 20)
            Me.ComboBoxEdit_Progress2_CreditReport.TabIndex = 35
            '
            'ComboBoxEdit_Progress2_ProgramParticipation
            '
            Me.ComboBoxEdit_Progress2_ProgramParticipation.Location = New System.Drawing.Point(687, 351)
            Me.ComboBoxEdit_Progress2_ProgramParticipation.Name = "ComboBoxEdit_Progress2_ProgramParticipation"
            Me.ComboBoxEdit_Progress2_ProgramParticipation.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Progress2_ProgramParticipation.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_Progress2_ProgramParticipation.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_Progress2_ProgramParticipation.Properties.DropDownRows = 4
            Me.ComboBoxEdit_Progress2_ProgramParticipation.Properties.NullText = ""
            Me.ComboBoxEdit_Progress2_ProgramParticipation.Properties.PopupSizeable = False
            Me.ComboBoxEdit_Progress2_ProgramParticipation.Properties.ShowFooter = False
            Me.ComboBoxEdit_Progress2_ProgramParticipation.Properties.ShowHeader = False
            Me.ComboBoxEdit_Progress2_ProgramParticipation.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_Progress2_ProgramParticipation.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_Progress2_ProgramParticipation.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_Progress2_ProgramParticipation.Size = New System.Drawing.Size(128, 20)
            Me.ComboBoxEdit_Progress2_ProgramParticipation.TabIndex = 39
            '
            'ComboBoxEdit_Progress2_EduParticipation
            '
            Me.ComboBoxEdit_Progress2_EduParticipation.Location = New System.Drawing.Point(687, 377)
            Me.ComboBoxEdit_Progress2_EduParticipation.Name = "ComboBoxEdit_Progress2_EduParticipation"
            Me.ComboBoxEdit_Progress2_EduParticipation.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Progress2_EduParticipation.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_Progress2_EduParticipation.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_Progress2_EduParticipation.Properties.DropDownRows = 4
            Me.ComboBoxEdit_Progress2_EduParticipation.Properties.NullText = ""
            Me.ComboBoxEdit_Progress2_EduParticipation.Properties.PopupSizeable = False
            Me.ComboBoxEdit_Progress2_EduParticipation.Properties.ShowFooter = False
            Me.ComboBoxEdit_Progress2_EduParticipation.Properties.ShowHeader = False
            Me.ComboBoxEdit_Progress2_EduParticipation.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_Progress2_EduParticipation.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_Progress2_EduParticipation.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_Progress2_EduParticipation.Size = New System.Drawing.Size(128, 20)
            Me.ComboBoxEdit_Progress2_EduParticipation.TabIndex = 43
            '
            'ComboBoxEdit_Progress3_Banked
            '
            Me.ComboBoxEdit_Progress3_Banked.Location = New System.Drawing.Point(924, 273)
            Me.ComboBoxEdit_Progress3_Banked.Name = "ComboBoxEdit_Progress3_Banked"
            Me.ComboBoxEdit_Progress3_Banked.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Progress3_Banked.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_Progress3_Banked.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_Progress3_Banked.Properties.DropDownRows = 4
            Me.ComboBoxEdit_Progress3_Banked.Properties.NullText = ""
            Me.ComboBoxEdit_Progress3_Banked.Properties.PopupSizeable = False
            Me.ComboBoxEdit_Progress3_Banked.Properties.ShowFooter = False
            Me.ComboBoxEdit_Progress3_Banked.Properties.ShowHeader = False
            Me.ComboBoxEdit_Progress3_Banked.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_Progress3_Banked.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_Progress3_Banked.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_Progress3_Banked.Size = New System.Drawing.Size(128, 20)
            Me.ComboBoxEdit_Progress3_Banked.TabIndex = 28
            '
            'ComboBoxEdit_Progress3_USFinance
            '
            Me.ComboBoxEdit_Progress3_USFinance.Location = New System.Drawing.Point(924, 299)
            Me.ComboBoxEdit_Progress3_USFinance.Name = "ComboBoxEdit_Progress3_USFinance"
            Me.ComboBoxEdit_Progress3_USFinance.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Progress3_USFinance.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_Progress3_USFinance.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_Progress3_USFinance.Properties.DropDownRows = 6
            Me.ComboBoxEdit_Progress3_USFinance.Properties.NullText = ""
            Me.ComboBoxEdit_Progress3_USFinance.Properties.PopupSizeable = False
            Me.ComboBoxEdit_Progress3_USFinance.Properties.ShowFooter = False
            Me.ComboBoxEdit_Progress3_USFinance.Properties.ShowHeader = False
            Me.ComboBoxEdit_Progress3_USFinance.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_Progress3_USFinance.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_Progress3_USFinance.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_Progress3_USFinance.Size = New System.Drawing.Size(128, 20)
            Me.ComboBoxEdit_Progress3_USFinance.TabIndex = 32
            '
            'ComboBoxEdit_Progress3_CreditReport
            '
            Me.ComboBoxEdit_Progress3_CreditReport.Location = New System.Drawing.Point(924, 324)
            Me.ComboBoxEdit_Progress3_CreditReport.Name = "ComboBoxEdit_Progress3_CreditReport"
            Me.ComboBoxEdit_Progress3_CreditReport.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Progress3_CreditReport.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_Progress3_CreditReport.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_Progress3_CreditReport.Properties.DropDownRows = 5
            Me.ComboBoxEdit_Progress3_CreditReport.Properties.NullText = ""
            Me.ComboBoxEdit_Progress3_CreditReport.Properties.PopupSizeable = False
            Me.ComboBoxEdit_Progress3_CreditReport.Properties.ShowFooter = False
            Me.ComboBoxEdit_Progress3_CreditReport.Properties.ShowHeader = False
            Me.ComboBoxEdit_Progress3_CreditReport.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_Progress3_CreditReport.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_Progress3_CreditReport.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_Progress3_CreditReport.Size = New System.Drawing.Size(128, 20)
            Me.ComboBoxEdit_Progress3_CreditReport.TabIndex = 36
            '
            'ComboBoxEdit_Progress3_ProgramParticipation
            '
            Me.ComboBoxEdit_Progress3_ProgramParticipation.Location = New System.Drawing.Point(924, 351)
            Me.ComboBoxEdit_Progress3_ProgramParticipation.Name = "ComboBoxEdit_Progress3_ProgramParticipation"
            Me.ComboBoxEdit_Progress3_ProgramParticipation.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Progress3_ProgramParticipation.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_Progress3_ProgramParticipation.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_Progress3_ProgramParticipation.Properties.DropDownRows = 4
            Me.ComboBoxEdit_Progress3_ProgramParticipation.Properties.NullText = ""
            Me.ComboBoxEdit_Progress3_ProgramParticipation.Properties.PopupSizeable = False
            Me.ComboBoxEdit_Progress3_ProgramParticipation.Properties.ShowFooter = False
            Me.ComboBoxEdit_Progress3_ProgramParticipation.Properties.ShowHeader = False
            Me.ComboBoxEdit_Progress3_ProgramParticipation.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_Progress3_ProgramParticipation.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_Progress3_ProgramParticipation.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_Progress3_ProgramParticipation.Size = New System.Drawing.Size(128, 20)
            Me.ComboBoxEdit_Progress3_ProgramParticipation.TabIndex = 40
            '
            'ComboBoxEdit_Progress3_EduParticipation
            '
            Me.ComboBoxEdit_Progress3_EduParticipation.Location = New System.Drawing.Point(924, 377)
            Me.ComboBoxEdit_Progress3_EduParticipation.Name = "ComboBoxEdit_Progress3_EduParticipation"
            Me.ComboBoxEdit_Progress3_EduParticipation.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Progress3_EduParticipation.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_Progress3_EduParticipation.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_Progress3_EduParticipation.Properties.DropDownRows = 4
            Me.ComboBoxEdit_Progress3_EduParticipation.Properties.NullText = ""
            Me.ComboBoxEdit_Progress3_EduParticipation.Properties.PopupSizeable = False
            Me.ComboBoxEdit_Progress3_EduParticipation.Properties.ShowFooter = False
            Me.ComboBoxEdit_Progress3_EduParticipation.Properties.ShowHeader = False
            Me.ComboBoxEdit_Progress3_EduParticipation.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_Progress3_EduParticipation.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
            Me.ComboBoxEdit_Progress3_EduParticipation.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_Progress3_EduParticipation.Size = New System.Drawing.Size(128, 20)
            Me.ComboBoxEdit_Progress3_EduParticipation.TabIndex = 44
            '
            'ComboBoxEdit_Goal1
            '
            Me.ComboBoxEdit_Goal1.Location = New System.Drawing.Point(174, 39)
            Me.ComboBoxEdit_Goal1.Name = "ComboBoxEdit_Goal1"
            Me.ComboBoxEdit_Goal1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_Goal1.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description")})
            Me.ComboBoxEdit_Goal1.Properties.DisplayMember = "Description"
            Me.ComboBoxEdit_Goal1.Properties.DropDownRows = 10
            Me.ComboBoxEdit_Goal1.Properties.NullText = ""
            Me.ComboBoxEdit_Goal1.Properties.PopupSizeable = False
            Me.ComboBoxEdit_Goal1.Properties.ShowFooter = False
            Me.ComboBoxEdit_Goal1.Properties.ShowHeader = False
            Me.ComboBoxEdit_Goal1.Properties.SortColumnIndex = 1
            Me.ComboBoxEdit_Goal1.Properties.ValueMember = "Id"
            Me.ComboBoxEdit_Goal1.Size = New System.Drawing.Size(164, 20)
            Me.ComboBoxEdit_Goal1.TabIndex = 1
            '
            'txtFicoProgress3EnteredDate
            '
            Me.txtFicoProgress3EnteredDate.Location = New System.Drawing.Point(1056, 168)
            Me.txtFicoProgress3EnteredDate.Name = "txtFicoProgress3EnteredDate"
            Me.txtFicoProgress3EnteredDate.Properties.ReadOnly = True
            Me.txtFicoProgress3EnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtFicoProgress3EnteredDate.TabIndex = 143
            Me.txtFicoProgress3EnteredDate.TabStop = False
            '
            'txtFicoProgress2EnteredDate
            '
            Me.txtFicoProgress2EnteredDate.Location = New System.Drawing.Point(819, 168)
            Me.txtFicoProgress2EnteredDate.Name = "txtFicoProgress2EnteredDate"
            Me.txtFicoProgress2EnteredDate.Properties.ReadOnly = True
            Me.txtFicoProgress2EnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtFicoProgress2EnteredDate.TabIndex = 142
            Me.txtFicoProgress2EnteredDate.TabStop = False
            '
            'txtFicoProgress1EnteredDate
            '
            Me.txtFicoProgress1EnteredDate.Location = New System.Drawing.Point(581, 168)
            Me.txtFicoProgress1EnteredDate.Name = "txtFicoProgress1EnteredDate"
            Me.txtFicoProgress1EnteredDate.Properties.ReadOnly = True
            Me.txtFicoProgress1EnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtFicoProgress1EnteredDate.TabIndex = 141
            Me.txtFicoProgress1EnteredDate.TabStop = False
            '
            'txtFicoEnteredDate
            '
            Me.txtFicoEnteredDate.Location = New System.Drawing.Point(341, 168)
            Me.txtFicoEnteredDate.Name = "txtFicoEnteredDate"
            Me.txtFicoEnteredDate.Properties.ReadOnly = True
            Me.txtFicoEnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtFicoEnteredDate.TabIndex = 140
            Me.txtFicoEnteredDate.TabStop = False
            '
            'LabelControl26
            '
            Me.LabelControl26.Location = New System.Drawing.Point(19, 172)
            Me.LabelControl26.Name = "LabelControl26"
            Me.LabelControl26.Size = New System.Drawing.Size(55, 13)
            Me.LabelControl26.TabIndex = 139
            Me.LabelControl26.Text = "FICO Score"
            '
            'LabelControl27
            '
            Me.LabelControl27.Location = New System.Drawing.Point(1056, 147)
            Me.LabelControl27.Name = "LabelControl27"
            Me.LabelControl27.Size = New System.Drawing.Size(66, 13)
            Me.LabelControl27.TabIndex = 138
            Me.LabelControl27.Text = "FICO Entered"
            '
            'LabelControl28
            '
            Me.LabelControl28.Location = New System.Drawing.Point(924, 147)
            Me.LabelControl28.Name = "LabelControl28"
            Me.LabelControl28.Size = New System.Drawing.Size(50, 13)
            Me.LabelControl28.TabIndex = 137
            Me.LabelControl28.Text = "FICO Final"
            '
            'LabelControl29
            '
            Me.LabelControl29.Location = New System.Drawing.Point(819, 147)
            Me.LabelControl29.Name = "LabelControl29"
            Me.LabelControl29.Size = New System.Drawing.Size(66, 13)
            Me.LabelControl29.TabIndex = 136
            Me.LabelControl29.Text = "FICO Entered"
            '
            'LabelControl30
            '
            Me.LabelControl30.Location = New System.Drawing.Point(687, 147)
            Me.LabelControl30.Name = "LabelControl30"
            Me.LabelControl30.Size = New System.Drawing.Size(79, 13)
            Me.LabelControl30.TabIndex = 135
            Me.LabelControl30.Text = "FICO Progress 2"
            '
            'LabelControl31
            '
            Me.LabelControl31.Location = New System.Drawing.Point(582, 147)
            Me.LabelControl31.Name = "LabelControl31"
            Me.LabelControl31.Size = New System.Drawing.Size(66, 13)
            Me.LabelControl31.TabIndex = 134
            Me.LabelControl31.Text = "FICO Entered"
            '
            'LabelControl32
            '
            Me.LabelControl32.Location = New System.Drawing.Point(450, 147)
            Me.LabelControl32.Name = "LabelControl32"
            Me.LabelControl32.Size = New System.Drawing.Size(79, 13)
            Me.LabelControl32.TabIndex = 133
            Me.LabelControl32.Text = "FICO Progress 1"
            '
            'LabelControl33
            '
            Me.LabelControl33.Location = New System.Drawing.Point(342, 147)
            Me.LabelControl33.Name = "LabelControl33"
            Me.LabelControl33.Size = New System.Drawing.Size(66, 13)
            Me.LabelControl33.TabIndex = 132
            Me.LabelControl33.Text = "FICO Entered"
            '
            'LabelControl34
            '
            Me.LabelControl34.Location = New System.Drawing.Point(175, 147)
            Me.LabelControl34.Name = "LabelControl34"
            Me.LabelControl34.Size = New System.Drawing.Size(54, 13)
            Me.LabelControl34.TabIndex = 131
            Me.LabelControl34.Text = "FICO Initial"
            '
            'txtFicoInitial
            '
            Me.txtFicoInitial.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
            Me.txtFicoInitial.Location = New System.Drawing.Point(174, 168)
            Me.txtFicoInitial.Name = "txtFicoInitial"
            Me.txtFicoInitial.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.txtFicoInitial.Properties.IsFloatValue = False
            Me.txtFicoInitial.Properties.Mask.EditMask = "N00"
            Me.txtFicoInitial.Properties.MaxValue = New Decimal(New Integer() {880, 0, 0, 0})
            Me.txtFicoInitial.Size = New System.Drawing.Size(164, 20)
            Me.txtFicoInitial.TabIndex = 17
            '
            'txtFicoProgress1
            '
            Me.txtFicoProgress1.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
            Me.txtFicoProgress1.Location = New System.Drawing.Point(449, 168)
            Me.txtFicoProgress1.Name = "txtFicoProgress1"
            Me.txtFicoProgress1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.txtFicoProgress1.Properties.IsFloatValue = False
            Me.txtFicoProgress1.Properties.Mask.EditMask = "N00"
            Me.txtFicoProgress1.Properties.MaxValue = New Decimal(New Integer() {880, 0, 0, 0})
            Me.txtFicoProgress1.Size = New System.Drawing.Size(128, 20)
            Me.txtFicoProgress1.TabIndex = 18
            '
            'txtFicoProgress2
            '
            Me.txtFicoProgress2.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
            Me.txtFicoProgress2.Location = New System.Drawing.Point(687, 168)
            Me.txtFicoProgress2.Name = "txtFicoProgress2"
            Me.txtFicoProgress2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.txtFicoProgress2.Properties.IsFloatValue = False
            Me.txtFicoProgress2.Properties.Mask.EditMask = "N00"
            Me.txtFicoProgress2.Properties.MaxValue = New Decimal(New Integer() {880, 0, 0, 0})
            Me.txtFicoProgress2.Size = New System.Drawing.Size(128, 20)
            Me.txtFicoProgress2.TabIndex = 19
            '
            'txtFicoProgress3
            '
            Me.txtFicoProgress3.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
            Me.txtFicoProgress3.Location = New System.Drawing.Point(924, 168)
            Me.txtFicoProgress3.Name = "txtFicoProgress3"
            Me.txtFicoProgress3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.txtFicoProgress3.Properties.IsFloatValue = False
            Me.txtFicoProgress3.Properties.Mask.EditMask = "N00"
            Me.txtFicoProgress3.Properties.MaxValue = New Decimal(New Integer() {880, 0, 0, 0})
            Me.txtFicoProgress3.Properties.NullText = "NULL"
            Me.txtFicoProgress3.Size = New System.Drawing.Size(128, 20)
            Me.txtFicoProgress3.TabIndex = 20
            '
            'txtDebtProgress3EnteredDate
            '
            Me.txtDebtProgress3EnteredDate.Location = New System.Drawing.Point(1056, 220)
            Me.txtDebtProgress3EnteredDate.Name = "txtDebtProgress3EnteredDate"
            Me.txtDebtProgress3EnteredDate.Properties.ReadOnly = True
            Me.txtDebtProgress3EnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtDebtProgress3EnteredDate.TabIndex = 160
            Me.txtDebtProgress3EnteredDate.TabStop = False
            '
            'txtDebtProgress2EnteredDate
            '
            Me.txtDebtProgress2EnteredDate.Location = New System.Drawing.Point(819, 220)
            Me.txtDebtProgress2EnteredDate.Name = "txtDebtProgress2EnteredDate"
            Me.txtDebtProgress2EnteredDate.Properties.ReadOnly = True
            Me.txtDebtProgress2EnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtDebtProgress2EnteredDate.TabIndex = 159
            Me.txtDebtProgress2EnteredDate.TabStop = False
            '
            'txtDebtProgress1EnteredDate
            '
            Me.txtDebtProgress1EnteredDate.Location = New System.Drawing.Point(581, 220)
            Me.txtDebtProgress1EnteredDate.Name = "txtDebtProgress1EnteredDate"
            Me.txtDebtProgress1EnteredDate.Properties.ReadOnly = True
            Me.txtDebtProgress1EnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtDebtProgress1EnteredDate.TabIndex = 158
            Me.txtDebtProgress1EnteredDate.TabStop = False
            '
            'txtDebtEnteredDate
            '
            Me.txtDebtEnteredDate.Location = New System.Drawing.Point(341, 220)
            Me.txtDebtEnteredDate.Name = "txtDebtEnteredDate"
            Me.txtDebtEnteredDate.Properties.ReadOnly = True
            Me.txtDebtEnteredDate.Size = New System.Drawing.Size(92, 20)
            Me.txtDebtEnteredDate.TabIndex = 157
            Me.txtDebtEnteredDate.TabStop = False
            '
            'LabelControl35
            '
            Me.LabelControl35.Location = New System.Drawing.Point(19, 224)
            Me.LabelControl35.Name = "LabelControl35"
            Me.LabelControl35.Size = New System.Drawing.Size(104, 13)
            Me.LabelControl35.TabIndex = 156
            Me.LabelControl35.Text = "Unsecured Debt Total"
            '
            'LabelControl36
            '
            Me.LabelControl36.Location = New System.Drawing.Point(1058, 199)
            Me.LabelControl36.Name = "LabelControl36"
            Me.LabelControl36.Size = New System.Drawing.Size(64, 13)
            Me.LabelControl36.TabIndex = 155
            Me.LabelControl36.Text = "Debt Entered"
            '
            'LabelControl37
            '
            Me.LabelControl37.Location = New System.Drawing.Point(926, 199)
            Me.LabelControl37.Name = "LabelControl37"
            Me.LabelControl37.Size = New System.Drawing.Size(48, 13)
            Me.LabelControl37.TabIndex = 154
            Me.LabelControl37.Text = "Debt Final"
            '
            'LabelControl38
            '
            Me.LabelControl38.Location = New System.Drawing.Point(821, 199)
            Me.LabelControl38.Name = "LabelControl38"
            Me.LabelControl38.Size = New System.Drawing.Size(64, 13)
            Me.LabelControl38.TabIndex = 153
            Me.LabelControl38.Text = "Debt Entered"
            '
            'LabelControl39
            '
            Me.LabelControl39.Location = New System.Drawing.Point(689, 199)
            Me.LabelControl39.Name = "LabelControl39"
            Me.LabelControl39.Size = New System.Drawing.Size(77, 13)
            Me.LabelControl39.TabIndex = 152
            Me.LabelControl39.Text = "Debt Progress 2"
            '
            'LabelControl40
            '
            Me.LabelControl40.Location = New System.Drawing.Point(582, 199)
            Me.LabelControl40.Name = "LabelControl40"
            Me.LabelControl40.Size = New System.Drawing.Size(64, 13)
            Me.LabelControl40.TabIndex = 151
            Me.LabelControl40.Text = "Debt Entered"
            '
            'LabelControl41
            '
            Me.LabelControl41.Location = New System.Drawing.Point(450, 199)
            Me.LabelControl41.Name = "LabelControl41"
            Me.LabelControl41.Size = New System.Drawing.Size(77, 13)
            Me.LabelControl41.TabIndex = 150
            Me.LabelControl41.Text = "Debt Progress 1"
            '
            'LabelControl42
            '
            Me.LabelControl42.Location = New System.Drawing.Point(342, 199)
            Me.LabelControl42.Name = "LabelControl42"
            Me.LabelControl42.Size = New System.Drawing.Size(64, 13)
            Me.LabelControl42.TabIndex = 149
            Me.LabelControl42.Text = "Debt Entered"
            '
            'LabelControl43
            '
            Me.LabelControl43.Location = New System.Drawing.Point(175, 199)
            Me.LabelControl43.Name = "LabelControl43"
            Me.LabelControl43.Size = New System.Drawing.Size(52, 13)
            Me.LabelControl43.TabIndex = 148
            Me.LabelControl43.Text = "Debt Initial"
            '
            'CalcEditDebtInitial
            '
            Me.CalcEditDebtInitial.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
            Me.CalcEditDebtInitial.Location = New System.Drawing.Point(174, 220)
            Me.CalcEditDebtInitial.Name = "CalcEditDebtInitial"
            Me.CalcEditDebtInitial.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEditDebtInitial.Properties.DisplayFormat.FormatString = "c"
            Me.CalcEditDebtInitial.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEditDebtInitial.Properties.EditFormat.FormatString = "F2"
            Me.CalcEditDebtInitial.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEditDebtInitial.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered
            Me.CalcEditDebtInitial.Properties.Mask.EditMask = "N00"
            Me.CalcEditDebtInitial.Size = New System.Drawing.Size(164, 20)
            Me.CalcEditDebtInitial.TabIndex = 21
            '
            'CalcEditDebtProgress1
            '
            Me.CalcEditDebtProgress1.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
            Me.CalcEditDebtProgress1.Location = New System.Drawing.Point(449, 220)
            Me.CalcEditDebtProgress1.Name = "CalcEditDebtProgress1"
            Me.CalcEditDebtProgress1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEditDebtProgress1.Properties.DisplayFormat.FormatString = "c"
            Me.CalcEditDebtProgress1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEditDebtProgress1.Properties.EditFormat.FormatString = "F2"
            Me.CalcEditDebtProgress1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEditDebtProgress1.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered
            Me.CalcEditDebtProgress1.Properties.Mask.EditMask = "N00"
            Me.CalcEditDebtProgress1.Size = New System.Drawing.Size(128, 20)
            Me.CalcEditDebtProgress1.TabIndex = 22
            '
            'CalcEditDebtProgress2
            '
            Me.CalcEditDebtProgress2.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
            Me.CalcEditDebtProgress2.Location = New System.Drawing.Point(687, 220)
            Me.CalcEditDebtProgress2.Name = "CalcEditDebtProgress2"
            Me.CalcEditDebtProgress2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEditDebtProgress2.Properties.DisplayFormat.FormatString = "c"
            Me.CalcEditDebtProgress2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEditDebtProgress2.Properties.EditFormat.FormatString = "F2"
            Me.CalcEditDebtProgress2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEditDebtProgress2.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered
            Me.CalcEditDebtProgress2.Properties.Mask.EditMask = "N00"
            Me.CalcEditDebtProgress2.Size = New System.Drawing.Size(128, 20)
            Me.CalcEditDebtProgress2.TabIndex = 23
            '
            'CalcEditDebtProgress3
            '
            Me.CalcEditDebtProgress3.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
            Me.CalcEditDebtProgress3.Location = New System.Drawing.Point(924, 220)
            Me.CalcEditDebtProgress3.Name = "CalcEditDebtProgress3"
            Me.CalcEditDebtProgress3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEditDebtProgress3.Properties.DisplayFormat.FormatString = "c"
            Me.CalcEditDebtProgress3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEditDebtProgress3.Properties.EditFormat.FormatString = "F2"
            Me.CalcEditDebtProgress3.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEditDebtProgress3.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered
            Me.CalcEditDebtProgress3.Properties.Mask.EditMask = "N00"
            Me.CalcEditDebtProgress3.Size = New System.Drawing.Size(128, 20)
            Me.CalcEditDebtProgress3.TabIndex = 24
            '
            'GoalTrackerControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.txtDebtProgress3EnteredDate)
            Me.Controls.Add(Me.txtDebtProgress2EnteredDate)
            Me.Controls.Add(Me.txtDebtProgress1EnteredDate)
            Me.Controls.Add(Me.txtDebtEnteredDate)
            Me.Controls.Add(Me.LabelControl35)
            Me.Controls.Add(Me.LabelControl36)
            Me.Controls.Add(Me.LabelControl37)
            Me.Controls.Add(Me.LabelControl38)
            Me.Controls.Add(Me.LabelControl39)
            Me.Controls.Add(Me.LabelControl40)
            Me.Controls.Add(Me.LabelControl41)
            Me.Controls.Add(Me.LabelControl42)
            Me.Controls.Add(Me.LabelControl43)
            Me.Controls.Add(Me.txtFicoProgress3)
            Me.Controls.Add(Me.txtFicoProgress2)
            Me.Controls.Add(Me.txtFicoProgress1)
            Me.Controls.Add(Me.txtFicoInitial)
            Me.Controls.Add(Me.txtFicoProgress3EnteredDate)
            Me.Controls.Add(Me.txtFicoProgress2EnteredDate)
            Me.Controls.Add(Me.txtFicoProgress1EnteredDate)
            Me.Controls.Add(Me.txtFicoEnteredDate)
            Me.Controls.Add(Me.LabelControl26)
            Me.Controls.Add(Me.LabelControl27)
            Me.Controls.Add(Me.LabelControl28)
            Me.Controls.Add(Me.LabelControl29)
            Me.Controls.Add(Me.LabelControl30)
            Me.Controls.Add(Me.LabelControl31)
            Me.Controls.Add(Me.LabelControl32)
            Me.Controls.Add(Me.LabelControl33)
            Me.Controls.Add(Me.LabelControl34)
            Me.Controls.Add(Me.txtProgress3EduParticipationEnteredDate)
            Me.Controls.Add(Me.txtProgress2EduParticipationEnteredDate)
            Me.Controls.Add(Me.txtProgress1EduParticipationEnteredDate)
            Me.Controls.Add(Me.txtEduParticipationEnteredDate)
            Me.Controls.Add(Me.LabelControl10)
            Me.Controls.Add(Me.txtProgress3ProgramParticipationEnteredDate)
            Me.Controls.Add(Me.txtProgress2ProgramParticipationEnteredDate)
            Me.Controls.Add(Me.txtProgress1ProgramParticipationEnteredDate)
            Me.Controls.Add(Me.txtProgramParticipationEnteredDate)
            Me.Controls.Add(Me.LabelControl9)
            Me.Controls.Add(Me.txtProgress3CreditReportEnteredDate)
            Me.Controls.Add(Me.txtProgress2CreditReportEnteredDate)
            Me.Controls.Add(Me.txtProgress1CreditReportEnteredDate)
            Me.Controls.Add(Me.txtCreditReportEnteredDate)
            Me.Controls.Add(Me.LabelControl8)
            Me.Controls.Add(Me.txtProgress3UsFinanceEnteredDate)
            Me.Controls.Add(Me.txtProgress2UsFinanceEnteredDate)
            Me.Controls.Add(Me.txtProgress1UsFinanceEnteredDate)
            Me.Controls.Add(Me.txtUsFinanceEnteredDate)
            Me.Controls.Add(Me.LabelControl7)
            Me.Controls.Add(Me.txtProgress3BankedEnteredDate)
            Me.Controls.Add(Me.txtProgress2BankedEnteredDate)
            Me.Controls.Add(Me.txtProgress1BankedEnteredDate)
            Me.Controls.Add(Me.txtBankedEnteredDate)
            Me.Controls.Add(Me.LabelControl6)
            Me.Controls.Add(Me.LabelControl25)
            Me.Controls.Add(Me.LabelControl24)
            Me.Controls.Add(Me.LabelControl23)
            Me.Controls.Add(Me.LabelControl22)
            Me.Controls.Add(Me.LabelControl21)
            Me.Controls.Add(Me.LabelControl20)
            Me.Controls.Add(Me.LabelControl19)
            Me.Controls.Add(Me.LabelControl18)
            Me.Controls.Add(Me.txtProgress3Goal4EnteredDate)
            Me.Controls.Add(Me.txtProgress2Goal4EnteredDate)
            Me.Controls.Add(Me.txtProgress1Goal4EnteredDate)
            Me.Controls.Add(Me.txtGoal4EnteredDate)
            Me.Controls.Add(Me.LabelControl4)
            Me.Controls.Add(Me.txtProgress3Goal3EnteredDate)
            Me.Controls.Add(Me.txtProgress2Goal3EnteredDate)
            Me.Controls.Add(Me.txtProgress1Goal3EnteredDate)
            Me.Controls.Add(Me.txtGoal3EnteredDate)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.txtProgress3Goal2EnteredDate)
            Me.Controls.Add(Me.txtProgress2Goal2EnteredDate)
            Me.Controls.Add(Me.txtProgress1Goal2EnteredDate)
            Me.Controls.Add(Me.txtGoal2EnteredDate)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.txtProgress3Goal1EnteredDate)
            Me.Controls.Add(Me.txtProgress2Goal1EnteredDate)
            Me.Controls.Add(Me.txtProgress1Goal1EnteredDate)
            Me.Controls.Add(Me.txtGoal1EnteredDate)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.LabelControl17)
            Me.Controls.Add(Me.LabelControl16)
            Me.Controls.Add(Me.LabelControl15)
            Me.Controls.Add(Me.LabelControl14)
            Me.Controls.Add(Me.LabelControl13)
            Me.Controls.Add(Me.LabelControl12)
            Me.Controls.Add(Me.LabelControl11)
            Me.Controls.Add(Me.LabelControl5)
            Me.Controls.Add(Me.ComboBoxEdit_Goal2)
            Me.Controls.Add(Me.ComboBoxEdit_Goal3)
            Me.Controls.Add(Me.ComboBoxEdit_Goal4)
            Me.Controls.Add(Me.ComboBoxEdit_Progress1_Goal4)
            Me.Controls.Add(Me.ComboBoxEdit_Progress1_Goal3)
            Me.Controls.Add(Me.ComboBoxEdit_Progress1_Goal2)
            Me.Controls.Add(Me.ComboBoxEdit_Progress1_Goal1)
            Me.Controls.Add(Me.ComboBoxEdit_Progress2_Goal1)
            Me.Controls.Add(Me.ComboBoxEdit_Progress2_Goal2)
            Me.Controls.Add(Me.ComboBoxEdit_Progress2_Goal3)
            Me.Controls.Add(Me.ComboBoxEdit_Progress2_Goal4)
            Me.Controls.Add(Me.ComboBoxEdit_Progress3_Goal1)
            Me.Controls.Add(Me.ComboBoxEdit_Progress3_Goal2)
            Me.Controls.Add(Me.ComboBoxEdit_Progress3_Goal3)
            Me.Controls.Add(Me.ComboBoxEdit_Progress3_Goal4)
            Me.Controls.Add(Me.ComboBoxEdit_Banked)
            Me.Controls.Add(Me.ComboBoxEdit_USFinance)
            Me.Controls.Add(Me.ComboBoxEdit_CreditReport)
            Me.Controls.Add(Me.ComboBoxEdit_ProgramParticipation)
            Me.Controls.Add(Me.ComboBoxEdit_EduParticipation)
            Me.Controls.Add(Me.ComboBoxEdit_Progress1_Banked)
            Me.Controls.Add(Me.ComboBoxEdit_Progress1_USFinance)
            Me.Controls.Add(Me.ComboBoxEdit_Progress1_CreditReport)
            Me.Controls.Add(Me.ComboBoxEdit_Progress1_ProgramParticipation)
            Me.Controls.Add(Me.ComboBoxEdit_Progress1_EduParticipation)
            Me.Controls.Add(Me.ComboBoxEdit_Progress2_Banked)
            Me.Controls.Add(Me.ComboBoxEdit_Progress2_USFinance)
            Me.Controls.Add(Me.ComboBoxEdit_Progress2_CreditReport)
            Me.Controls.Add(Me.ComboBoxEdit_Progress2_ProgramParticipation)
            Me.Controls.Add(Me.ComboBoxEdit_Progress2_EduParticipation)
            Me.Controls.Add(Me.ComboBoxEdit_Progress3_Banked)
            Me.Controls.Add(Me.ComboBoxEdit_Progress3_USFinance)
            Me.Controls.Add(Me.ComboBoxEdit_Progress3_CreditReport)
            Me.Controls.Add(Me.ComboBoxEdit_Progress3_ProgramParticipation)
            Me.Controls.Add(Me.ComboBoxEdit_Progress3_EduParticipation)
            Me.Controls.Add(Me.ComboBoxEdit_Goal1)
            Me.Controls.Add(Me.CalcEditDebtInitial)
            Me.Controls.Add(Me.CalcEditDebtProgress1)
            Me.Controls.Add(Me.CalcEditDebtProgress2)
            Me.Controls.Add(Me.CalcEditDebtProgress3)
            Me.Name = "GoalTrackerControl"
            Me.Size = New System.Drawing.Size(1174, 412)
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem_Left, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem_Right, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem8, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem9, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem28, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem29, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem30, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem31, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem32, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem33, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem34, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem35, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem36, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem37, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem38, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem39, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem40, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem41, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem42, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem43, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtProgress3Goal1EnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtProgress2Goal1EnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtProgress1Goal1EnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtGoal1EnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtProgress3Goal2EnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtProgress2Goal2EnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtProgress1Goal2EnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtGoal2EnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtProgress3Goal3EnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtProgress2Goal3EnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtProgress1Goal3EnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtGoal3EnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtProgress3Goal4EnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtProgress2Goal4EnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtProgress1Goal4EnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtGoal4EnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtProgress3BankedEnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtProgress2BankedEnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtProgress1BankedEnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtBankedEnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtProgress3UsFinanceEnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtProgress2UsFinanceEnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtProgress1UsFinanceEnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtUsFinanceEnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtProgress3CreditReportEnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtProgress2CreditReportEnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtProgress1CreditReportEnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtCreditReportEnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtProgress3ProgramParticipationEnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtProgress2ProgramParticipationEnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtProgress1ProgramParticipationEnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtProgramParticipationEnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtProgress3EduParticipationEnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtProgress2EduParticipationEnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtProgress1EduParticipationEnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtEduParticipationEnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Goal2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Goal3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Goal4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Progress1_Goal4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Progress1_Goal3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Progress1_Goal2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Progress1_Goal1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Progress2_Goal1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Progress2_Goal2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Progress2_Goal3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Progress2_Goal4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Progress3_Goal1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Progress3_Goal2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Progress3_Goal3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Progress3_Goal4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Banked.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_USFinance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_CreditReport.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_ProgramParticipation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_EduParticipation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Progress1_Banked.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Progress1_USFinance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Progress1_CreditReport.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Progress1_ProgramParticipation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Progress1_EduParticipation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Progress2_Banked.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Progress2_USFinance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Progress2_CreditReport.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Progress2_ProgramParticipation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Progress2_EduParticipation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Progress3_Banked.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Progress3_USFinance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Progress3_CreditReport.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Progress3_ProgramParticipation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Progress3_EduParticipation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_Goal1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtFicoProgress3EnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtFicoProgress2EnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtFicoProgress1EnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtFicoEnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtFicoInitial.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtFicoProgress1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtFicoProgress2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtFicoProgress3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtDebtProgress3EnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtDebtProgress2EnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtDebtProgress1EnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txtDebtEnteredDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEditDebtInitial.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEditDebtProgress1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEditDebtProgress2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEditDebtProgress3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem3 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem_Left As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem_Right As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem8 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem9 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem28 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem29 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem30 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem31 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem32 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem33 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem34 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem35 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem37 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem38 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem39 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem40 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem41 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem43 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem42 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem36 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents txtProgress3Goal1EnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents txtProgress2Goal1EnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents txtProgress1Goal1EnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents txtGoal1EnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents txtProgress3Goal2EnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents txtProgress2Goal2EnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents txtProgress1Goal2EnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents txtGoal2EnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents txtProgress3Goal3EnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents txtProgress2Goal3EnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents txtProgress1Goal3EnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents txtGoal3EnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents txtProgress3Goal4EnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents txtProgress2Goal4EnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents txtProgress1Goal4EnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents txtGoal4EnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LabelControl25 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents txtProgress3BankedEnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents txtProgress2BankedEnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents txtProgress1BankedEnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents txtBankedEnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents txtProgress3UsFinanceEnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents txtProgress2UsFinanceEnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents txtProgress1UsFinanceEnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents txtUsFinanceEnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents txtProgress3CreditReportEnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents txtProgress2CreditReportEnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents txtProgress1CreditReportEnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents txtCreditReportEnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents txtProgress3ProgramParticipationEnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents txtProgress2ProgramParticipationEnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents txtProgress1ProgramParticipationEnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents txtProgramParticipationEnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents txtProgress3EduParticipationEnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents txtProgress2EduParticipationEnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents txtProgress1EduParticipationEnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents txtEduParticipationEnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents ComboBoxEdit_Goal2 As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_Goal3 As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_Goal4 As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_Progress1_Goal4 As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_Progress1_Goal3 As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_Progress1_Goal2 As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_Progress1_Goal1 As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_Progress2_Goal1 As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_Progress2_Goal2 As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_Progress2_Goal3 As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_Progress2_Goal4 As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_Progress3_Goal1 As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_Progress3_Goal2 As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_Progress3_Goal3 As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_Progress3_Goal4 As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_Banked As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_USFinance As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_CreditReport As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_ProgramParticipation As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_EduParticipation As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_Progress1_Banked As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_Progress1_USFinance As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_Progress1_CreditReport As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_Progress1_ProgramParticipation As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_Progress1_EduParticipation As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_Progress2_Banked As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_Progress2_USFinance As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_Progress2_CreditReport As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_Progress2_ProgramParticipation As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_Progress2_EduParticipation As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_Progress3_Banked As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_Progress3_USFinance As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_Progress3_CreditReport As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_Progress3_ProgramParticipation As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_Progress3_EduParticipation As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_Goal1 As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents txtFicoProgress3EnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents txtFicoProgress2EnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents txtFicoProgress1EnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents txtFicoEnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl28 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl29 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl30 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl31 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl32 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl33 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl34 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents txtFicoInitial As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents txtFicoProgress1 As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents txtFicoProgress2 As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents txtFicoProgress3 As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents txtDebtProgress3EnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents txtDebtProgress2EnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents txtDebtProgress1EnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents txtDebtEnteredDate As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LabelControl35 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl36 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl37 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl38 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl39 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl40 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl41 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl42 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl43 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents CalcEditDebtInitial As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents CalcEditDebtProgress1 As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents CalcEditDebtProgress2 As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents CalcEditDebtProgress3 As DevExpress.XtraEditors.CalcEdit
    End Class
End Namespace