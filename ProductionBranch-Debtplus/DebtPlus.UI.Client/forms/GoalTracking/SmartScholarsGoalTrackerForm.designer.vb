﻿Namespace forms.Goal

    Partial Class SmartScholarsGoalTrackerForm
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.barManager1 = New DevExpress.XtraBars.BarManager()
            Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
            Me.BarSubItem1 = New DevExpress.XtraBars.BarSubItem()
            Me.BarButtonItem_File_Print = New DevExpress.XtraBars.BarButtonItem()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
            Me.ApplicantTab = New DevExpress.XtraTab.XtraTabControl()
            Me.applicantTabPage = New DevExpress.XtraTab.XtraTabPage()
            Me.applicantGoalControl = New DebtPlus.UI.Client.forms.Goal.GoalTrackerControl()
            Me.coapplicantTabPage = New DevExpress.XtraTab.XtraTabPage()
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
            Me.chkSameAsApplicant = New DevExpress.XtraEditors.CheckEdit()
            Me.coapplicantGoalControl = New DebtPlus.UI.Client.forms.Goal.GoalTrackerControl()
            Me.SimpleButton_OK = New DevExpress.XtraEditors.SimpleButton()
            Me.SimpleButton_Cancel = New DevExpress.XtraEditors.SimpleButton()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ApplicantTab, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.ApplicantTab.SuspendLayout()
            Me.applicantTabPage.SuspendLayout()
            CType(Me.applicantGoalControl, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.coapplicantTabPage.SuspendLayout()
            CType(Me.chkSameAsApplicant.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.coapplicantGoalControl, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'barManager1
            '
            Me.barManager1.DockControls.Add(Me.barDockControlTop)
            Me.barManager1.DockControls.Add(Me.barDockControlBottom)
            Me.barManager1.DockControls.Add(Me.barDockControlLeft)
            Me.barManager1.DockControls.Add(Me.barDockControlRight)
            Me.barManager1.Form = Me
            Me.barManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarSubItem1, Me.BarButtonItem_File_Print})
            Me.barManager1.MaxItemId = 2
            '
            'barDockControlTop
            '
            Me.barDockControlTop.CausesValidation = False
            Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
            Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
            Me.barDockControlTop.Size = New System.Drawing.Size(1213, 0)
            '
            'barDockControlBottom
            '
            Me.barDockControlBottom.CausesValidation = False
            Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
            Me.barDockControlBottom.Location = New System.Drawing.Point(0, 537)
            Me.barDockControlBottom.Size = New System.Drawing.Size(1213, 0)
            '
            'barDockControlLeft
            '
            Me.barDockControlLeft.CausesValidation = False
            Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
            Me.barDockControlLeft.Location = New System.Drawing.Point(0, 0)
            Me.barDockControlLeft.Size = New System.Drawing.Size(0, 537)
            '
            'barDockControlRight
            '
            Me.barDockControlRight.CausesValidation = False
            Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
            Me.barDockControlRight.Location = New System.Drawing.Point(1213, 0)
            Me.barDockControlRight.Size = New System.Drawing.Size(0, 537)
            '
            'BarSubItem1
            '
            Me.BarSubItem1.Caption = "&File"
            Me.BarSubItem1.Id = 0
            Me.BarSubItem1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_File_Print)})
            Me.BarSubItem1.Name = "BarSubItem1"
            '
            'BarButtonItem_File_Print
            '
            Me.BarButtonItem_File_Print.Caption = "&Print..."
            Me.BarButtonItem_File_Print.Id = 1
            Me.BarButtonItem_File_Print.Name = "BarButtonItem_File_Print"
            '
            'LabelControl1
            '
            Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 15.0!, System.Drawing.FontStyle.Bold)
            Me.LabelControl1.Location = New System.Drawing.Point(506, 13)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(153, 24)
            Me.LabelControl1.TabIndex = 5
            Me.LabelControl1.Text = "Smart Scholars"
            '
            'ApplicantTab
            '
            Me.ApplicantTab.Location = New System.Drawing.Point(13, 44)
            Me.ApplicantTab.Name = "ApplicantTab"
            Me.ApplicantTab.SelectedTabPage = Me.applicantTabPage
            Me.ApplicantTab.Size = New System.Drawing.Size(1189, 452)
            Me.ApplicantTab.TabIndex = 6
            Me.ApplicantTab.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.applicantTabPage, Me.coapplicantTabPage})
            '
            'applicantTabPage
            '
            Me.applicantTabPage.Controls.Add(Me.applicantGoalControl)
            Me.applicantTabPage.Name = "applicantTabPage"
            Me.applicantTabPage.Size = New System.Drawing.Size(1183, 424)
            Me.applicantTabPage.Text = "Applicant"
            '
            'applicantGoalControl
            '
            Me.applicantGoalControl.GoalID = 0
            Me.applicantGoalControl.IsModified = False
            Me.applicantGoalControl.Location = New System.Drawing.Point(4, 19)
            Me.applicantGoalControl.Margin = New System.Windows.Forms.Padding(0)
            Me.applicantGoalControl.Name = "applicantGoalControl"
            Me.applicantGoalControl.Size = New System.Drawing.Size(1167, 406)
            Me.applicantGoalControl.TabIndex = 0
            Me.applicantGoalControl.UserType = 0
            '
            'coapplicantTabPage
            '
            Me.coapplicantTabPage.Controls.Add(Me.LabelControl2)
            Me.coapplicantTabPage.Controls.Add(Me.chkSameAsApplicant)
            Me.coapplicantTabPage.Controls.Add(Me.coapplicantGoalControl)
            Me.coapplicantTabPage.Name = "coapplicantTabPage"
            Me.coapplicantTabPage.Size = New System.Drawing.Size(1183, 424)
            Me.coapplicantTabPage.Text = "CoApplicant"
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(23, 16)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(88, 13)
            Me.LabelControl2.TabIndex = 2
            Me.LabelControl2.Text = "Same As Applicant"
            '
            'chkSameAsApplicant
            '
            Me.chkSameAsApplicant.Location = New System.Drawing.Point(124, 13)
            Me.chkSameAsApplicant.MenuManager = Me.barManager1
            Me.chkSameAsApplicant.Name = "chkSameAsApplicant"
            Me.chkSameAsApplicant.Properties.Caption = ""
            Me.chkSameAsApplicant.Size = New System.Drawing.Size(75, 19)
            Me.chkSameAsApplicant.TabIndex = 1
            '
            'coapplicantGoalControl
            '
            Me.coapplicantGoalControl.GoalID = 0
            Me.coapplicantGoalControl.IsModified = False
            Me.coapplicantGoalControl.Location = New System.Drawing.Point(4, 19)
            Me.coapplicantGoalControl.Margin = New System.Windows.Forms.Padding(0)
            Me.coapplicantGoalControl.Name = "coapplicantGoalControl"
            Me.coapplicantGoalControl.Size = New System.Drawing.Size(1178, 397)
            Me.coapplicantGoalControl.TabIndex = 0
            Me.coapplicantGoalControl.UserType = 0
            '
            'SimpleButton_OK
            '
            Me.SimpleButton_OK.Location = New System.Drawing.Point(521, 504)
            Me.SimpleButton_OK.Name = "SimpleButton_OK"
            Me.SimpleButton_OK.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.TabIndex = 7
            Me.SimpleButton_OK.Text = "&OK"
            '
            'SimpleButton_Cancel
            '
            Me.SimpleButton_Cancel.Location = New System.Drawing.Point(616, 504)
            Me.SimpleButton_Cancel.Name = "SimpleButton_Cancel"
            Me.SimpleButton_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.TabIndex = 8
            Me.SimpleButton_Cancel.Text = "&Cancel"
            '
            'SmartScholarsGoalTrackerForm
            '
            Me.ClientSize = New System.Drawing.Size(1213, 537)
            Me.Controls.Add(Me.SimpleButton_Cancel)
            Me.Controls.Add(Me.SimpleButton_OK)
            Me.Controls.Add(Me.ApplicantTab)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.barDockControlLeft)
            Me.Controls.Add(Me.barDockControlRight)
            Me.Controls.Add(Me.barDockControlBottom)
            Me.Controls.Add(Me.barDockControlTop)
            Me.Name = "SmartScholarsGoalTrackerForm"
            Me.Text = "Smart Scholars Goal Tracking"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ApplicantTab, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ApplicantTab.ResumeLayout(False)
            Me.applicantTabPage.ResumeLayout(False)
            CType(Me.applicantGoalControl, System.ComponentModel.ISupportInitialize).EndInit()
            Me.coapplicantTabPage.ResumeLayout(False)
            Me.coapplicantTabPage.PerformLayout()
            CType(Me.chkSameAsApplicant.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.coapplicantGoalControl, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents barManager1 As DevExpress.XtraBars.BarManager
        Friend WithEvents BarSubItem1 As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarButtonItem_File_Print As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
        Friend WithEvents SimpleButton_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents ApplicantTab As DevExpress.XtraTab.XtraTabControl
        Friend WithEvents applicantTabPage As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents coapplicantTabPage As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents applicantGoalControl As GoalTrackerControl
        Friend WithEvents coapplicantGoalControl As GoalTrackerControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents chkSameAsApplicant As DevExpress.XtraEditors.CheckEdit
    End Class
End Namespace