#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.UI.Client.Service
Imports DebtPlus.Utils
Imports DevExpress.XtraBars
Imports System.Linq
Imports System.Data.SqlClient
Imports System.Collections.Generic

Namespace forms.Goal

    Friend Class HCFEGoalTrackerForm

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Dim clientID As Integer
        Dim lineOfService As Integer
        Dim goalID As Integer
        Dim type As Integer

        Private Const AddGoal As Integer = 1
        Private Const EditGoal As Integer = 2

        Dim currentGoal As DebtPlus.LINQ.ClientGoal = Nothing

        ''' <summary>
        ''' Initialize the form
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Load, AddressOf HCFEGoalTrackerForm_Load
        End Sub

        Public Sub New(_goalID As Integer, _type As Integer)
            MyClass.New()
            ''privateContext = Context
            goalID = _goalID
            type = _type
        End Sub

        Public Sub New(_clientID As Integer, _lineOfService As Integer, _type As Integer)
            MyClass.New()
            ''privateContext = Context
            clientID = _clientID
            lineOfService = _lineOfService
            type = _type
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf HCFEGoalTrackerForm_Load
            AddHandler chkSameAsApplicant.CheckedChanged, AddressOf chkSameAsApplicant_CheckedChanged
            AddHandler SimpleButton_Cancel.Click, AddressOf SimpleButton_Cancel_Click
            AddHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
            AddHandler ApplicantTab.SelectedPageChanged, AddressOf ApplicantTab_SelectedPageChanged
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler Load, AddressOf HCFEGoalTrackerForm_Load
            RemoveHandler chkSameAsApplicant.CheckedChanged, AddressOf chkSameAsApplicant_CheckedChanged
            RemoveHandler SimpleButton_Cancel.Click, AddressOf SimpleButton_Cancel_Click
            RemoveHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
            AddHandler ApplicantTab.SelectedPageChanged, AddressOf ApplicantTab_SelectedPageChanged
        End Sub

        ''' <summary>
        ''' Load the form
        ''' </summary>
        Private Sub HCFEGoalTrackerForm_Load(ByVal sender As Object, ByVal e As EventArgs)

            UnRegisterHandlers()

            applicantGoalControl.UserType = 1
            coapplicantGoalControl.UserType = 2

            If type = AddGoal Then
                '' set the Same As Applicant checkbox
                chkSameAsApplicant.Checked = False
            End If

            If type = EditGoal Then

                LoadControls()

            End If

            RegisterHandlers()

        End Sub

        Private Sub LoadControls()
            Using bc As New DebtPlus.LINQ.BusinessContext()
                currentGoal = bc.ClientGoals.Where(Function(cg) cg.Id = goalID AndAlso cg.ActiveFlag = True).FirstOrDefault()
            End Using

            applicantGoalControl.ReadForm(goalID)

            chkSameAsApplicant.Checked = GetCopyApplicantSettingValue(currentGoal.CopyApplicantSetting)

            '' Load values from coapplicant tab 
            If chkSameAsApplicant.Checked = False Then
                coapplicantGoalControl.ReadForm(goalID)
            Else

            End If
        End Sub

        Private Function GetCopyApplicantSettingValue(val As Boolean?) As Boolean
            Dim returnValue As Boolean = False
            If val.HasValue Then
                If val = True Then
                    returnValue = True
                Else
                    returnValue = False
                End If
            Else
                returnValue = False
            End If
            Return returnValue
        End Function


        Private Sub SimpleButton_OK_Click(sender As System.Object, e As System.EventArgs)

            If type = AddGoal Then

                Try

                    Using bc As New DebtPlus.LINQ.BusinessContext()

                        currentGoal = New DebtPlus.LINQ.ClientGoal()
                        currentGoal.ActiveFlag = True
                        currentGoal.ClientID = clientID
                        currentGoal.LineOfServiceID = lineOfService
                        currentGoal.CreatedBy = DebtPlus.LINQ.BusinessContext.suser_sname()
                        currentGoal.CreatedTimestamp = DateTime.Now
                        currentGoal.CopyApplicantSetting = chkSameAsApplicant.Checked

                        bc.ClientGoals.InsertOnSubmit(currentGoal)
                        bc.SubmitChanges()

                    End Using

                Catch ex As Exception

                End Try

            ElseIf type = EditGoal Then

                Try
                    Using bc As New DebtPlus.LINQ.BusinessContext()

                        currentGoal = bc.ClientGoals.Where(Function(cg) cg.Id = goalID AndAlso cg.ActiveFlag = True).FirstOrDefault()

                        If Not currentGoal Is Nothing Then
                            currentGoal.ModifiedBy = DebtPlus.LINQ.BusinessContext.suser_sname()
                            currentGoal.ModifiedTimestamp = DateTime.Now
                            currentGoal.CopyApplicantSetting = chkSameAsApplicant.Checked
                            bc.SubmitChanges()
                        End If
                    End Using

                Catch ex As Exception

                End Try

            End If

            If Not currentGoal Is Nothing Then

                If applicantGoalControl.IsModified = True Then
                    applicantGoalControl.SaveForm(currentGoal.Id)
                End If

                If coapplicantGoalControl.IsModified = True Then
                    coapplicantGoalControl.SaveForm(currentGoal.Id)
                End If

            End If

            DialogResult = Windows.Forms.DialogResult.OK
        End Sub

        Private Sub SimpleButton_Cancel_Click(sender As System.Object, e As System.EventArgs)
            DialogResult = Windows.Forms.DialogResult.Cancel
        End Sub

        Private Sub chkSameAsApplicant_CheckedChanged(sender As System.Object, e As System.EventArgs)

            ProcessCoapplicantTabDisplay()

        End Sub

        Private Sub CopyApplicantDataInCoapplicantTab()

            Dim clientGoalItems As List(Of DebtPlus.LINQ.ClientGoalItem) = applicantGoalControl.GetControlValues()

            coapplicantGoalControl.PopulateClientGoals(clientGoalItems)

            coapplicantGoalControl.MarkControlsReadOnly()

        End Sub

        Private Sub ApplicantTab_SelectedPageChanged(sender As System.Object, e As DevExpress.XtraTab.TabPageChangedEventArgs)
            If e.Page Is coapplicantTabPage Then
                ProcessCoapplicantTabDisplay()
            End If
        End Sub

        Private Sub ProcessCoapplicantTabDisplay()
            If chkSameAsApplicant.Checked = True Then
                CopyApplicantDataInCoapplicantTab()
            Else
                coapplicantGoalControl.MarkControlsAsEditable()
            End If
        End Sub

    End Class
End Namespace
