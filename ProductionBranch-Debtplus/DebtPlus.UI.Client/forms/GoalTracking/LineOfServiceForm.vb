﻿Imports System
Imports System.Linq
Imports DebtPlus.LINQ

Namespace forms.Goal
    Public Class LineOfServiceForm

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Dim clientID As Integer

        Public Sub New(_clientID As Integer)
            MyBase.New()
            InitializeComponent()
            clientID = _clientID
            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler LayoutControl1.Resize, AddressOf LayoutControl1_Resize
            AddHandler Load, AddressOf LineOfServiceForm_Load
            AddHandler ComboBoxEdit_LineOfService.EditValueChanged, AddressOf FormChanged
            AddHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler LayoutControl1.Resize, AddressOf LayoutControl1_Resize
            RemoveHandler Load, AddressOf LineOfServiceForm_Load
            RemoveHandler ComboBoxEdit_LineOfService.EditValueChanged, AddressOf FormChanged
            RemoveHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
        End Sub

        Private Sub LayoutControl1_Resize(sender As Object, e As System.EventArgs)
            UnRegisterHandlers()
            Try
                EmptySpaceItem_Left.Width = (EmptySpaceItem_Left.Width + EmptySpaceItem_Right.Width) \ 2
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Sub LineOfServiceForm_Load(sender As Object, e As System.EventArgs)
            UnRegisterHandlers()

            ComboBoxEdit_LineOfService.Properties.DataSource = DebtPlus.LINQ.Cache.LineofServiceType.getList()

            SimpleButton_OK.Enabled = Not HasErrors()
            RegisterHandlers()
        End Sub

        Private Sub FormChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            SimpleButton_OK.Enabled = Not HasErrors()
        End Sub

        Private Function HasErrors() As Boolean
            Return ComboBoxEdit_LineOfService.EditValue Is Nothing
        End Function

        Private Sub SimpleButton_OK_Click(sender As Object, e As System.EventArgs)

            Dim AddGoal As Integer = 1

            Dim answer As System.Windows.Forms.DialogResult = DialogResult.OK

            Dim lineOfService As Integer = Convert.ToInt32(ComboBoxEdit_LineOfService.EditValue)

            Select Case lineOfService
                Case 1
                    '' Display the Goal Tracker Form to capture Goal Settings
                    Using frm As New HCFEGoalTrackerForm(clientID, lineOfService, AddGoal)
                        answer = frm.ShowDialog()
                    End Using
                Case 2
                    '' Display the Goal Tracker Form to capture Goal Settings
                    Using frm As New SmartScholarsGoalTrackerForm(clientID, lineOfService, AddGoal)
                        answer = frm.ShowDialog()
                    End Using
                Case 3
                    '' Display the Goal Tracker Form to capture Goal Settings
                    Using frm As New PostModPilotGoalTrackerForm(clientID, lineOfService, AddGoal)
                        answer = frm.ShowDialog()
                    End Using
                Case Else

            End Select

            DialogResult = answer

        End Sub
    End Class
End Namespace