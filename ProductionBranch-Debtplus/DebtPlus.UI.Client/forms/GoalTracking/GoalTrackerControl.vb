#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.Data.Forms
Imports DebtPlus.Events
Imports DebtPlus.LINQ
Imports DebtPlus.Svc.DataSets
Imports DebtPlus.Utils
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo
Imports System.ComponentModel
Imports System.Data.SqlClient
Imports System.IO
Imports System.Linq
Imports System.Collections.Generic

Namespace forms.Goal
    Friend Class GoalTrackerControl

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Private Const Applicant As Integer = 1
        Private Const Coapplicant As Integer = 2

        Private Const Goal1 As Integer = 1
        Private Const Goal2 As Integer = 2
        Private Const Goal3 As Integer = 3
        Private Const Goal4 As Integer = 4
        Private Const Banked As Integer = 5
        Private Const USFinancial As Integer = 6
        Private Const CreditReport As Integer = 7
        Private Const ProgramParticipation As Integer = 8
        Private Const EducationParticipation As Integer = 9
        Private Const FicoScore As Integer = 10
        Private Const UnsecuredDebtTotal As Integer = 11

        Private _userType As Integer
        Public Property UserType() As Integer
            Get
                Return _userType
            End Get
            Set(ByVal value As Integer)
                _userType = value
            End Set
        End Property

        Private _goalID As Integer
        Public Property GoalID() As Integer
            Get
                Return _goalID
            End Get
            Set(ByVal value As Integer)
                _goalID = value
            End Set
        End Property

        Private _isModified As Boolean
        Public Property IsModified() As Boolean
            Get
                Return _isModified
            End Get
            Set(ByVal value As Boolean)
                _isModified = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf GoalTrackerControl_Load

            AddHandler ComboBoxEdit_Goal1.EditValueChanged, AddressOf ComboBoxEdit_Goal1_EditValueChanged
            AddHandler ComboBoxEdit_Goal2.EditValueChanged, AddressOf ComboBoxEdit_Goal2_EditValueChanged
            AddHandler ComboBoxEdit_Goal3.EditValueChanged, AddressOf ComboBoxEdit_Goal3_EditValueChanged
            AddHandler ComboBoxEdit_Goal4.EditValueChanged, AddressOf ComboBoxEdit_Goal4_EditValueChanged

            AddHandler ComboBoxEdit_Progress1_Goal1.EditValueChanged, AddressOf ComboBoxEdit_Progress1_Goal1_EditValueChanged
            AddHandler ComboBoxEdit_Progress1_Goal2.EditValueChanged, AddressOf ComboBoxEdit_Progress1_Goal2_EditValueChanged
            AddHandler ComboBoxEdit_Progress1_Goal3.EditValueChanged, AddressOf ComboBoxEdit_Progress1_Goal3_EditValueChanged
            AddHandler ComboBoxEdit_Progress1_Goal4.EditValueChanged, AddressOf ComboBoxEdit_Progress1_Goal4_EditValueChanged

            AddHandler ComboBoxEdit_Progress2_Goal1.EditValueChanged, AddressOf ComboBoxEdit_Progress2_Goal1_EditValueChanged
            AddHandler ComboBoxEdit_Progress2_Goal2.EditValueChanged, AddressOf ComboBoxEdit_Progress2_Goal2_EditValueChanged
            AddHandler ComboBoxEdit_Progress2_Goal3.EditValueChanged, AddressOf ComboBoxEdit_Progress2_Goal3_EditValueChanged
            AddHandler ComboBoxEdit_Progress2_Goal4.EditValueChanged, AddressOf ComboBoxEdit_Progress2_Goal4_EditValueChanged

            AddHandler ComboBoxEdit_Progress3_Goal1.EditValueChanged, AddressOf ComboBoxEdit_Progress3_Goal1_EditValueChanged
            AddHandler ComboBoxEdit_Progress3_Goal2.EditValueChanged, AddressOf ComboBoxEdit_Progress3_Goal2_EditValueChanged
            AddHandler ComboBoxEdit_Progress3_Goal3.EditValueChanged, AddressOf ComboBoxEdit_Progress3_Goal3_EditValueChanged
            AddHandler ComboBoxEdit_Progress3_Goal4.EditValueChanged, AddressOf ComboBoxEdit_Progress3_Goal4_EditValueChanged

            AddHandler ComboBoxEdit_Banked.EditValueChanged, AddressOf ComboBoxEdit_Banked_EditValueChanged
            AddHandler ComboBoxEdit_USFinance.EditValueChanged, AddressOf ComboBoxEdit_USFinance_EditValueChanged
            AddHandler ComboBoxEdit_CreditReport.EditValueChanged, AddressOf ComboBoxEdit_CreditReport_EditValueChanged
            AddHandler ComboBoxEdit_ProgramParticipation.EditValueChanged, AddressOf ComboBoxEdit_ProgramParticipation_EditValueChanged
            AddHandler ComboBoxEdit_EduParticipation.EditValueChanged, AddressOf ComboBoxEdit_EduParticipation_EditValueChanged

            AddHandler ComboBoxEdit_Progress1_Banked.EditValueChanged, AddressOf ComboBoxEdit_Progress1_Banked_EditValueChanged
            AddHandler ComboBoxEdit_Progress1_USFinance.EditValueChanged, AddressOf ComboBoxEdit_Progress1_USFinance_EditValueChanged
            AddHandler ComboBoxEdit_Progress1_CreditReport.EditValueChanged, AddressOf ComboBoxEdit_Progress1_CreditReport_EditValueChanged
            AddHandler ComboBoxEdit_Progress1_ProgramParticipation.EditValueChanged, AddressOf ComboBoxEdit_Progress1_ProgramParticipation_EditValueChanged
            AddHandler ComboBoxEdit_Progress1_EduParticipation.EditValueChanged, AddressOf ComboBoxEdit_Progress1_EduParticipation_EditValueChanged

            AddHandler ComboBoxEdit_Progress2_Banked.EditValueChanged, AddressOf ComboBoxEdit_Progress2_Banked_EditValueChanged
            AddHandler ComboBoxEdit_Progress2_USFinance.EditValueChanged, AddressOf ComboBoxEdit_Progress2_USFinance_EditValueChanged
            AddHandler ComboBoxEdit_Progress2_CreditReport.EditValueChanged, AddressOf ComboBoxEdit_Progress2_CreditReport_EditValueChanged
            AddHandler ComboBoxEdit_Progress2_ProgramParticipation.EditValueChanged, AddressOf ComboBoxEdit_Progress2_ProgramParticipation_EditValueChanged
            AddHandler ComboBoxEdit_Progress2_EduParticipation.EditValueChanged, AddressOf ComboBoxEdit_Progress2_EduParticipation_EditValueChanged

            AddHandler ComboBoxEdit_Progress3_Banked.EditValueChanged, AddressOf ComboBoxEdit_Progress3_Banked_EditValueChanged
            AddHandler ComboBoxEdit_Progress3_USFinance.EditValueChanged, AddressOf ComboBoxEdit_Progress3_USFinance_EditValueChanged
            AddHandler ComboBoxEdit_Progress3_CreditReport.EditValueChanged, AddressOf ComboBoxEdit_Progress3_CreditReport_EditValueChanged
            AddHandler ComboBoxEdit_Progress3_ProgramParticipation.EditValueChanged, AddressOf ComboBoxEdit_Progress3_ProgramParticipation_EditValueChanged
            AddHandler ComboBoxEdit_Progress3_EduParticipation.EditValueChanged, AddressOf ComboBoxEdit_Progress3_EduParticipation_EditValueChanged

            AddHandler txtFicoInitial.EditValueChanged, AddressOf txtFicoInitial_EditValueChanged
            AddHandler txtFicoProgress1.EditValueChanged, AddressOf txtFicoProgress1_EditValueChanged
            AddHandler txtFicoProgress2.EditValueChanged, AddressOf txtFicoProgress2_EditValueChanged
            AddHandler txtFicoProgress3.EditValueChanged, AddressOf txtFicoProgress3_EditValueChanged

            AddHandler CalcEditDebtInitial.TextChanged, AddressOf CalcEditDebtInitial_EditValueChanged
            AddHandler CalcEditDebtProgress1.TextChanged, AddressOf CalcEditDebtProgress1_EditValueChanged
            AddHandler CalcEditDebtProgress2.TextChanged, AddressOf CalcEditDebtProgress2_EditValueChanged
            AddHandler CalcEditDebtProgress3.TextChanged, AddressOf CalcEditDebtProgress3_EditValueChanged
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler Load, AddressOf GoalTrackerControl_Load

            RemoveHandler ComboBoxEdit_Goal1.EditValueChanged, AddressOf ComboBoxEdit_Goal1_EditValueChanged
            RemoveHandler ComboBoxEdit_Goal2.EditValueChanged, AddressOf ComboBoxEdit_Goal2_EditValueChanged
            RemoveHandler ComboBoxEdit_Goal3.EditValueChanged, AddressOf ComboBoxEdit_Goal3_EditValueChanged
            RemoveHandler ComboBoxEdit_Goal4.EditValueChanged, AddressOf ComboBoxEdit_Goal4_EditValueChanged

            RemoveHandler ComboBoxEdit_Progress1_Goal1.EditValueChanged, AddressOf ComboBoxEdit_Progress1_Goal1_EditValueChanged
            RemoveHandler ComboBoxEdit_Progress1_Goal2.EditValueChanged, AddressOf ComboBoxEdit_Progress1_Goal2_EditValueChanged
            RemoveHandler ComboBoxEdit_Progress1_Goal3.EditValueChanged, AddressOf ComboBoxEdit_Progress1_Goal3_EditValueChanged
            RemoveHandler ComboBoxEdit_Progress1_Goal4.EditValueChanged, AddressOf ComboBoxEdit_Progress1_Goal4_EditValueChanged

            RemoveHandler ComboBoxEdit_Progress2_Goal1.EditValueChanged, AddressOf ComboBoxEdit_Progress2_Goal1_EditValueChanged
            RemoveHandler ComboBoxEdit_Progress2_Goal2.EditValueChanged, AddressOf ComboBoxEdit_Progress2_Goal2_EditValueChanged
            RemoveHandler ComboBoxEdit_Progress2_Goal3.EditValueChanged, AddressOf ComboBoxEdit_Progress2_Goal3_EditValueChanged
            RemoveHandler ComboBoxEdit_Progress2_Goal4.EditValueChanged, AddressOf ComboBoxEdit_Progress2_Goal4_EditValueChanged

            RemoveHandler ComboBoxEdit_Progress3_Goal1.EditValueChanged, AddressOf ComboBoxEdit_Progress3_Goal1_EditValueChanged
            RemoveHandler ComboBoxEdit_Progress3_Goal2.EditValueChanged, AddressOf ComboBoxEdit_Progress3_Goal2_EditValueChanged
            RemoveHandler ComboBoxEdit_Progress3_Goal3.EditValueChanged, AddressOf ComboBoxEdit_Progress3_Goal3_EditValueChanged
            RemoveHandler ComboBoxEdit_Progress3_Goal4.EditValueChanged, AddressOf ComboBoxEdit_Progress3_Goal4_EditValueChanged

            RemoveHandler ComboBoxEdit_Banked.EditValueChanged, AddressOf ComboBoxEdit_Banked_EditValueChanged
            RemoveHandler ComboBoxEdit_USFinance.EditValueChanged, AddressOf ComboBoxEdit_USFinance_EditValueChanged
            RemoveHandler ComboBoxEdit_CreditReport.EditValueChanged, AddressOf ComboBoxEdit_CreditReport_EditValueChanged
            RemoveHandler ComboBoxEdit_ProgramParticipation.EditValueChanged, AddressOf ComboBoxEdit_ProgramParticipation_EditValueChanged
            RemoveHandler ComboBoxEdit_EduParticipation.EditValueChanged, AddressOf ComboBoxEdit_EduParticipation_EditValueChanged

            RemoveHandler ComboBoxEdit_Progress1_Banked.EditValueChanged, AddressOf ComboBoxEdit_Progress1_Banked_EditValueChanged
            RemoveHandler ComboBoxEdit_Progress1_USFinance.EditValueChanged, AddressOf ComboBoxEdit_Progress1_USFinance_EditValueChanged
            RemoveHandler ComboBoxEdit_Progress1_CreditReport.EditValueChanged, AddressOf ComboBoxEdit_Progress1_CreditReport_EditValueChanged
            RemoveHandler ComboBoxEdit_Progress1_ProgramParticipation.EditValueChanged, AddressOf ComboBoxEdit_Progress1_ProgramParticipation_EditValueChanged
            RemoveHandler ComboBoxEdit_Progress1_EduParticipation.EditValueChanged, AddressOf ComboBoxEdit_Progress1_EduParticipation_EditValueChanged

            RemoveHandler ComboBoxEdit_Progress2_Banked.EditValueChanged, AddressOf ComboBoxEdit_Progress2_Banked_EditValueChanged
            RemoveHandler ComboBoxEdit_Progress2_USFinance.EditValueChanged, AddressOf ComboBoxEdit_Progress2_USFinance_EditValueChanged
            RemoveHandler ComboBoxEdit_Progress2_CreditReport.EditValueChanged, AddressOf ComboBoxEdit_Progress2_CreditReport_EditValueChanged
            RemoveHandler ComboBoxEdit_Progress2_ProgramParticipation.EditValueChanged, AddressOf ComboBoxEdit_Progress2_ProgramParticipation_EditValueChanged
            RemoveHandler ComboBoxEdit_Progress2_EduParticipation.EditValueChanged, AddressOf ComboBoxEdit_Progress2_EduParticipation_EditValueChanged

            RemoveHandler ComboBoxEdit_Progress3_Banked.EditValueChanged, AddressOf ComboBoxEdit_Progress3_Banked_EditValueChanged
            RemoveHandler ComboBoxEdit_Progress3_USFinance.EditValueChanged, AddressOf ComboBoxEdit_Progress3_USFinance_EditValueChanged
            RemoveHandler ComboBoxEdit_Progress3_CreditReport.EditValueChanged, AddressOf ComboBoxEdit_Progress3_CreditReport_EditValueChanged
            RemoveHandler ComboBoxEdit_Progress3_ProgramParticipation.EditValueChanged, AddressOf ComboBoxEdit_Progress3_ProgramParticipation_EditValueChanged
            RemoveHandler ComboBoxEdit_Progress3_EduParticipation.EditValueChanged, AddressOf ComboBoxEdit_Progress3_EduParticipation_EditValueChanged

            RemoveHandler txtFicoInitial.EditValueChanged, AddressOf txtFicoInitial_EditValueChanged
            RemoveHandler txtFicoProgress1.EditValueChanged, AddressOf txtFicoProgress1_EditValueChanged
            RemoveHandler txtFicoProgress2.EditValueChanged, AddressOf txtFicoProgress2_EditValueChanged
            RemoveHandler txtFicoProgress3.EditValueChanged, AddressOf txtFicoProgress3_EditValueChanged

            RemoveHandler CalcEditDebtInitial.TextChanged, AddressOf CalcEditDebtInitial_EditValueChanged
            RemoveHandler CalcEditDebtProgress1.TextChanged, AddressOf CalcEditDebtProgress1_EditValueChanged
            RemoveHandler CalcEditDebtProgress2.TextChanged, AddressOf CalcEditDebtProgress2_EditValueChanged
            RemoveHandler CalcEditDebtProgress3.TextChanged, AddressOf CalcEditDebtProgress3_EditValueChanged
        End Sub

        Public Sub GoalTrackerControl_Load(sender As Object, e As System.EventArgs)

            If Not DesignMode Then
                '' ComboBoxEdit_Goal1.Properties.Items
                Dim financialStatusList As List(Of FinancialGoalType) = DebtPlus.LINQ.Cache.FinancialGoalType.getList()
                Dim progressList As List(Of GoalProgressType) = DebtPlus.LINQ.Cache.GoalProgressType.getList()
                Dim bankedFinancialStatusList As List(Of BankedFinancialStatusType) = DebtPlus.LINQ.Cache.BankedFinancialStatusType.getList()
                Dim usFinancialSystemList As List(Of UsFinancialSystemType) = DebtPlus.LINQ.Cache.UsFinancialSystemType.getList()
                Dim creditReportErrorList As List(Of CreditReportErrorType) = DebtPlus.LINQ.Cache.CreditReportErrorType.getList()
                Dim programParticipationList As List(Of ProgramParticipationType) = DebtPlus.LINQ.Cache.ProgramParticipationType.getList()
                Dim eduParticipationList As List(Of EducationParticipationType) = DebtPlus.LINQ.Cache.EducationParticipationType.getList()

                If financialStatusList.Count > 0 AndAlso financialStatusList.ElementAt(0).Id > 0 Then
                    financialStatusList.Insert(0, New FinancialGoalType() With {.Id = 0, .Description = ""})
                End If
                If progressList.Count > 0 AndAlso progressList.ElementAt(0).Id > 0 Then
                    progressList.Insert(0, New GoalProgressType() With {.Id = 0, .Description = ""})
                End If
                If bankedFinancialStatusList.Count > 0 AndAlso bankedFinancialStatusList.ElementAt(0).Id > 0 Then
                    bankedFinancialStatusList.Insert(0, New BankedFinancialStatusType() With {.Id = 0, .Description = ""})
                End If
                If usFinancialSystemList.Count > 0 AndAlso usFinancialSystemList.ElementAt(0).Id > 0 Then
                    usFinancialSystemList.Insert(0, New UsFinancialSystemType() With {.Id = 0, .Description = ""})
                End If
                If creditReportErrorList.Count > 0 AndAlso creditReportErrorList.ElementAt(0).Id > 0 Then
                    creditReportErrorList.Insert(0, New CreditReportErrorType() With {.Id = 0, .Description = ""})
                End If
                If programParticipationList.Count > 0 AndAlso programParticipationList.ElementAt(0).Id > 0 Then
                    programParticipationList.Insert(0, New ProgramParticipationType() With {.Id = 0, .Description = ""})
                End If
                If eduParticipationList.Count > 0 AndAlso eduParticipationList.ElementAt(0).Id > 0 Then
                    eduParticipationList.Insert(0, New EducationParticipationType() With {.Id = 0, .Description = ""})
                End If

                ComboBoxEdit_Goal1.Properties.DataSource = financialStatusList
                ComboBoxEdit_Goal2.Properties.DataSource = financialStatusList
                ComboBoxEdit_Goal3.Properties.DataSource = financialStatusList
                ComboBoxEdit_Goal4.Properties.DataSource = financialStatusList

                ComboBoxEdit_Progress1_Goal1.Properties.DataSource = progressList
                ComboBoxEdit_Progress1_Goal2.Properties.DataSource = progressList
                ComboBoxEdit_Progress1_Goal3.Properties.DataSource = progressList
                ComboBoxEdit_Progress1_Goal4.Properties.DataSource = progressList

                ComboBoxEdit_Progress2_Goal1.Properties.DataSource = progressList
                ComboBoxEdit_Progress2_Goal2.Properties.DataSource = progressList
                ComboBoxEdit_Progress2_Goal3.Properties.DataSource = progressList
                ComboBoxEdit_Progress2_Goal4.Properties.DataSource = progressList

                ComboBoxEdit_Progress3_Goal1.Properties.DataSource = progressList
                ComboBoxEdit_Progress3_Goal2.Properties.DataSource = progressList
                ComboBoxEdit_Progress3_Goal3.Properties.DataSource = progressList
                ComboBoxEdit_Progress3_Goal4.Properties.DataSource = progressList

                ComboBoxEdit_Banked.Properties.DataSource = bankedFinancialStatusList
                ComboBoxEdit_USFinance.Properties.DataSource = usFinancialSystemList
                ComboBoxEdit_CreditReport.Properties.DataSource = creditReportErrorList
                ComboBoxEdit_ProgramParticipation.Properties.DataSource = programParticipationList
                ComboBoxEdit_EduParticipation.Properties.DataSource = eduParticipationList

                ComboBoxEdit_Progress1_Banked.Properties.DataSource = bankedFinancialStatusList
                ComboBoxEdit_Progress2_Banked.Properties.DataSource = bankedFinancialStatusList
                ComboBoxEdit_Progress3_Banked.Properties.DataSource = bankedFinancialStatusList

                ComboBoxEdit_Progress1_USFinance.Properties.DataSource = usFinancialSystemList
                ComboBoxEdit_Progress2_USFinance.Properties.DataSource = usFinancialSystemList
                ComboBoxEdit_Progress3_USFinance.Properties.DataSource = usFinancialSystemList

                ComboBoxEdit_Progress1_CreditReport.Properties.DataSource = creditReportErrorList
                ComboBoxEdit_Progress2_CreditReport.Properties.DataSource = creditReportErrorList
                ComboBoxEdit_Progress3_CreditReport.Properties.DataSource = creditReportErrorList

                ComboBoxEdit_Progress1_ProgramParticipation.Properties.DataSource = programParticipationList
                ComboBoxEdit_Progress2_ProgramParticipation.Properties.DataSource = programParticipationList
                ComboBoxEdit_Progress3_ProgramParticipation.Properties.DataSource = programParticipationList

                ComboBoxEdit_Progress1_EduParticipation.Properties.DataSource = eduParticipationList
                ComboBoxEdit_Progress2_EduParticipation.Properties.DataSource = eduParticipationList
                ComboBoxEdit_Progress3_EduParticipation.Properties.DataSource = eduParticipationList
            End If
        End Sub


        ''' <summary>
        ''' Load the form
        ''' </summary>
        Public Shadows Sub ReadForm(ByVal goalID As Integer)

            UnRegisterHandlers()

            Dim goals As List(Of ClientGoalItem) = Nothing

            Using bc As New DebtPlus.LINQ.BusinessContext()

                goals = bc.ClientGoalItems.Where(Function(g) g.ClientGoalID = goalID AndAlso g.UserType = UserType AndAlso g.ActiveFlag = True).ToList()

            End Using

            PopulateClientGoals(goals)

            RegisterHandlers()

        End Sub

        Public Sub PopulateClientGoals(goals As List(Of ClientGoalItem))

            For Each g As ClientGoalItem In goals
                PopulateGoal(g)
            Next

        End Sub

        Private Function SetDate(dt As Date?) As String
            If dt.HasValue Then
                Return Convert.ToDateTime(dt).ToString("MM/dd/yyyy")
            Else
                Return ""
            End If
        End Function


        Private Sub PopulateGoal(goal As ClientGoalItem)

            Select Case goal.GoalType
                Case Goal1
                    ComboBoxEdit_Goal1.EditValue = goal.GoalValue
                    txtGoal1EnteredDate.Text = SetDate(goal.GoalEnteredDate)

                    ComboBoxEdit_Progress1_Goal1.EditValue = goal.Progress1
                    txtProgress1Goal1EnteredDate.Text = SetDate(goal.Progress1Date)

                    ComboBoxEdit_Progress2_Goal1.EditValue = goal.Progress2
                    txtProgress2Goal1EnteredDate.Text = SetDate(goal.Progress2Date)

                    ComboBoxEdit_Progress3_Goal1.EditValue = goal.Progress3
                    txtProgress3Goal1EnteredDate.Text = SetDate(goal.Progress3Date)
                Case Goal2
                    ComboBoxEdit_Goal2.EditValue = goal.GoalValue
                    txtGoal2EnteredDate.Text = SetDate(goal.GoalEnteredDate)

                    ComboBoxEdit_Progress1_Goal2.EditValue = goal.Progress1
                    txtProgress1Goal2EnteredDate.Text = SetDate(goal.Progress1Date)

                    ComboBoxEdit_Progress2_Goal2.EditValue = goal.Progress2
                    txtProgress2Goal2EnteredDate.Text = SetDate(goal.Progress2Date)

                    ComboBoxEdit_Progress3_Goal2.EditValue = goal.Progress3
                    txtProgress3Goal2EnteredDate.Text = SetDate(goal.Progress3Date)
                Case Goal3
                    ComboBoxEdit_Goal3.EditValue = goal.GoalValue
                    txtGoal3EnteredDate.Text = SetDate(goal.GoalEnteredDate)

                    ComboBoxEdit_Progress1_Goal3.EditValue = goal.Progress1
                    txtProgress1Goal3EnteredDate.Text = SetDate(goal.Progress1Date)

                    ComboBoxEdit_Progress2_Goal3.EditValue = goal.Progress2
                    txtProgress2Goal3EnteredDate.Text = SetDate(goal.Progress2Date)

                    ComboBoxEdit_Progress3_Goal3.EditValue = goal.Progress3
                    txtProgress3Goal3EnteredDate.Text = SetDate(goal.Progress3Date)
                Case Goal4
                    ComboBoxEdit_Goal4.EditValue = goal.GoalValue
                    txtGoal4EnteredDate.Text = SetDate(goal.GoalEnteredDate)

                    ComboBoxEdit_Progress1_Goal4.EditValue = goal.Progress1
                    txtProgress1Goal4EnteredDate.Text = SetDate(goal.Progress1Date)

                    ComboBoxEdit_Progress2_Goal4.EditValue = goal.Progress2
                    txtProgress2Goal4EnteredDate.Text = SetDate(goal.Progress2Date)

                    ComboBoxEdit_Progress3_Goal4.EditValue = goal.Progress3
                    txtProgress3Goal4EnteredDate.Text = SetDate(goal.Progress3Date)
                Case Banked
                    ComboBoxEdit_Banked.EditValue = goal.GoalValue
                    txtBankedEnteredDate.Text = SetDate(goal.GoalEnteredDate)

                    ComboBoxEdit_Progress1_Banked.EditValue = goal.Progress1
                    txtProgress1BankedEnteredDate.Text = SetDate(goal.Progress1Date)

                    ComboBoxEdit_Progress2_Banked.EditValue = goal.Progress2
                    txtProgress2BankedEnteredDate.Text = SetDate(goal.Progress2Date)

                    ComboBoxEdit_Progress3_Banked.EditValue = goal.Progress3
                    txtProgress3BankedEnteredDate.Text = SetDate(goal.Progress3Date)
                Case USFinancial
                    ComboBoxEdit_USFinance.EditValue = goal.GoalValue
                    txtUsFinanceEnteredDate.Text = SetDate(goal.GoalEnteredDate)

                    ComboBoxEdit_Progress1_USFinance.EditValue = goal.Progress1
                    txtProgress1UsFinanceEnteredDate.Text = SetDate(goal.Progress1Date)

                    ComboBoxEdit_Progress2_USFinance.EditValue = goal.Progress2
                    txtProgress2UsFinanceEnteredDate.Text = SetDate(goal.Progress2Date)

                    ComboBoxEdit_Progress3_USFinance.EditValue = goal.Progress3
                    txtProgress3UsFinanceEnteredDate.Text = SetDate(goal.Progress3Date)
                Case CreditReport
                    ComboBoxEdit_CreditReport.EditValue = goal.GoalValue
                    txtCreditReportEnteredDate.Text = SetDate(goal.GoalEnteredDate)

                    ComboBoxEdit_Progress1_CreditReport.EditValue = goal.Progress1
                    txtProgress1CreditReportEnteredDate.Text = SetDate(goal.Progress1Date)

                    ComboBoxEdit_Progress2_CreditReport.EditValue = goal.Progress2
                    txtProgress2CreditReportEnteredDate.Text = SetDate(goal.Progress2Date)

                    ComboBoxEdit_Progress3_CreditReport.EditValue = goal.Progress3
                    txtProgress3CreditReportEnteredDate.Text = SetDate(goal.Progress3Date)
                Case ProgramParticipation
                    ComboBoxEdit_ProgramParticipation.EditValue = goal.GoalValue
                    txtProgramParticipationEnteredDate.Text = SetDate(goal.GoalEnteredDate)

                    ComboBoxEdit_Progress1_ProgramParticipation.EditValue = goal.Progress1
                    txtProgress1ProgramParticipationEnteredDate.Text = SetDate(goal.Progress1Date)

                    ComboBoxEdit_Progress2_ProgramParticipation.EditValue = goal.Progress2
                    txtProgress2ProgramParticipationEnteredDate.Text = SetDate(goal.Progress2Date)

                    ComboBoxEdit_Progress3_ProgramParticipation.EditValue = goal.Progress3
                    txtProgress3ProgramParticipationEnteredDate.Text = SetDate(goal.Progress3Date)
                Case EducationParticipation
                    ComboBoxEdit_EduParticipation.EditValue = goal.GoalValue
                    txtEduParticipationEnteredDate.Text = SetDate(goal.GoalEnteredDate)

                    ComboBoxEdit_Progress1_EduParticipation.EditValue = goal.Progress1
                    txtProgress1EduParticipationEnteredDate.Text = SetDate(goal.Progress1Date)

                    ComboBoxEdit_Progress2_EduParticipation.EditValue = goal.Progress2
                    txtProgress2EduParticipationEnteredDate.Text = SetDate(goal.Progress2Date)

                    ComboBoxEdit_Progress3_EduParticipation.EditValue = goal.Progress3
                    txtProgress3EduParticipationEnteredDate.Text = SetDate(goal.Progress3Date)
                Case FicoScore
                    txtFicoInitial.Text = If(goal.GoalValue.HasValue(), goal.GoalValue.ToString(), "")
                    txtFicoEnteredDate.Text = SetDate(goal.GoalEnteredDate)

                    txtFicoProgress1.EditValue = If(goal.Progress1.HasValue(), goal.Progress1.ToString(), "")
                    txtFicoProgress1EnteredDate.Text = SetDate(goal.Progress1Date)

                    txtFicoProgress2.EditValue = If(goal.Progress2.HasValue(), goal.Progress2.ToString(), "")
                    txtFicoProgress2EnteredDate.Text = SetDate(goal.Progress2Date)

                    txtFicoProgress3.EditValue = If(goal.Progress3.HasValue(), goal.Progress3.ToString(), "")
                    txtFicoProgress3EnteredDate.Text = SetDate(goal.Progress3Date)
                Case UnsecuredDebtTotal
                    CalcEditDebtInitial.EditValue = If(goal.GoalValue.HasValue(), Convert.ToDecimal(goal.GoalValue), 0.0)
                    txtDebtEnteredDate.Text = SetDate(goal.GoalEnteredDate)

                    CalcEditDebtProgress1.EditValue = If(goal.Progress1.HasValue(), Convert.ToDecimal(goal.Progress1), 0.0)
                    txtDebtProgress1EnteredDate.Text = SetDate(goal.Progress1Date)

                    CalcEditDebtProgress2.EditValue = If(goal.Progress2.HasValue(), Convert.ToDecimal(goal.Progress2), 0.0)
                    txtDebtProgress2EnteredDate.Text = SetDate(goal.Progress2Date)

                    CalcEditDebtProgress3.EditValue = If(goal.Progress3.HasValue(), Convert.ToDecimal(goal.Progress3), 0.0)
                    txtDebtProgress3EnteredDate.Text = SetDate(goal.Progress3Date)
                Case Else

            End Select

        End Sub

        '' Clear the values of the controls
        Public Sub ClearSelection()

            UnRegisterHandlers()

            ClearControlValues()

            '' Mark All Controls as Editable
            MarkControlsAsEditable()

            RegisterHandlers()

        End Sub

        Private Sub ClearControlValues()
            '' Goal 1
            ComboBoxEdit_Goal1.EditValue = Nothing
            txtGoal1EnteredDate.Text = String.Empty

            ComboBoxEdit_Progress1_Goal1.EditValue = Nothing
            txtProgress1Goal1EnteredDate.Text = String.Empty

            ComboBoxEdit_Progress2_Goal1.EditValue = Nothing
            txtProgress2Goal1EnteredDate.Text = String.Empty

            ComboBoxEdit_Progress3_Goal1.EditValue = Nothing
            txtProgress3Goal1EnteredDate.Text = String.Empty

            '' Goal 2
            ComboBoxEdit_Goal2.EditValue = Nothing
            txtGoal2EnteredDate.Text = String.Empty

            ComboBoxEdit_Progress1_Goal2.EditValue = Nothing
            txtProgress1Goal2EnteredDate.Text = String.Empty

            ComboBoxEdit_Progress2_Goal2.EditValue = Nothing
            txtProgress2Goal2EnteredDate.Text = String.Empty

            ComboBoxEdit_Progress3_Goal2.EditValue = Nothing
            txtProgress3Goal2EnteredDate.Text = String.Empty

            '' Goal 3
            ComboBoxEdit_Goal3.EditValue = Nothing
            txtGoal3EnteredDate.Text = String.Empty

            ComboBoxEdit_Progress1_Goal3.EditValue = Nothing
            txtProgress1Goal3EnteredDate.Text = String.Empty

            ComboBoxEdit_Progress2_Goal3.EditValue = Nothing
            txtProgress2Goal3EnteredDate.Text = String.Empty

            ComboBoxEdit_Progress3_Goal3.EditValue = Nothing
            txtProgress3Goal3EnteredDate.Text = String.Empty

            '' Goal 4
            ComboBoxEdit_Goal4.EditValue = Nothing
            txtGoal4EnteredDate.Text = String.Empty

            ComboBoxEdit_Progress1_Goal4.EditValue = Nothing
            txtProgress1Goal4EnteredDate.Text = String.Empty

            ComboBoxEdit_Progress2_Goal4.EditValue = Nothing
            txtProgress2Goal4EnteredDate.Text = String.Empty

            ComboBoxEdit_Progress3_Goal4.EditValue = Nothing
            txtProgress3Goal4EnteredDate.Text = String.Empty

            '' FICO Score
            txtFicoInitial.Text = String.Empty
            txtFicoEnteredDate.Text = String.Empty

            txtFicoProgress1.EditValue = String.Empty
            txtFicoProgress1EnteredDate.Text = String.Empty

            txtFicoProgress2.EditValue = String.Empty
            txtFicoProgress2EnteredDate.Text = String.Empty

            txtFicoProgress3.EditValue = String.Empty
            txtFicoProgress3EnteredDate.Text = String.Empty

            '' Unsecured Debt Total
            CalcEditDebtInitial.Text = String.Empty
            txtDebtEnteredDate.Text = String.Empty

            CalcEditDebtProgress1.EditValue = String.Empty
            txtDebtProgress1EnteredDate.Text = String.Empty

            CalcEditDebtProgress2.EditValue = String.Empty
            txtDebtProgress2EnteredDate.Text = String.Empty

            CalcEditDebtProgress3.EditValue = String.Empty
            txtDebtProgress3EnteredDate.Text = String.Empty

            '' Banked
            ComboBoxEdit_Banked.EditValue = Nothing
            txtBankedEnteredDate.Text = String.Empty

            ComboBoxEdit_Progress1_Banked.EditValue = Nothing
            txtProgress1BankedEnteredDate.Text = String.Empty

            ComboBoxEdit_Progress2_Banked.EditValue = Nothing
            txtProgress2BankedEnteredDate.Text = String.Empty

            ComboBoxEdit_Progress3_Banked.EditValue = Nothing
            txtProgress3BankedEnteredDate.Text = String.Empty

            '' US Finance
            ComboBoxEdit_USFinance.EditValue = Nothing
            txtUsFinanceEnteredDate.Text = String.Empty

            ComboBoxEdit_Progress1_USFinance.EditValue = Nothing
            txtProgress1UsFinanceEnteredDate.Text = String.Empty

            ComboBoxEdit_Progress2_USFinance.EditValue = Nothing
            txtProgress2UsFinanceEnteredDate.Text = String.Empty

            ComboBoxEdit_Progress3_USFinance.EditValue = Nothing
            txtProgress3UsFinanceEnteredDate.Text = String.Empty

            '' Credit Report
            ComboBoxEdit_CreditReport.EditValue = Nothing
            txtCreditReportEnteredDate.Text = String.Empty

            ComboBoxEdit_Progress1_CreditReport.EditValue = Nothing
            txtProgress1CreditReportEnteredDate.Text = String.Empty

            ComboBoxEdit_Progress2_CreditReport.EditValue = Nothing
            txtProgress2CreditReportEnteredDate.Text = String.Empty

            ComboBoxEdit_Progress3_CreditReport.EditValue = Nothing
            txtProgress3CreditReportEnteredDate.Text = String.Empty

            '' Program Participation
            ComboBoxEdit_ProgramParticipation.EditValue = Nothing
            txtProgramParticipationEnteredDate.Text = String.Empty

            ComboBoxEdit_Progress1_ProgramParticipation.EditValue = Nothing
            txtProgress1ProgramParticipationEnteredDate.Text = String.Empty

            ComboBoxEdit_Progress2_ProgramParticipation.EditValue = Nothing
            txtProgress2ProgramParticipationEnteredDate.Text = String.Empty

            ComboBoxEdit_Progress3_ProgramParticipation.EditValue = Nothing
            txtProgress3ProgramParticipationEnteredDate.Text = String.Empty

            '' Ed Participation
            ComboBoxEdit_EduParticipation.EditValue = Nothing
            txtEduParticipationEnteredDate.Text = String.Empty

            ComboBoxEdit_Progress1_EduParticipation.EditValue = Nothing
            txtProgress1EduParticipationEnteredDate.Text = String.Empty

            ComboBoxEdit_Progress2_EduParticipation.EditValue = Nothing
            txtProgress2EduParticipationEnteredDate.Text = String.Empty

            ComboBoxEdit_Progress3_EduParticipation.EditValue = Nothing
            txtProgress3EduParticipationEnteredDate.Text = String.Empty
        End Sub

        Public Sub MarkControlsReadOnly()
            '' Goal 1
            ComboBoxEdit_Goal1.Properties.ReadOnly = True

            ComboBoxEdit_Progress1_Goal1.Properties.ReadOnly = True
            ComboBoxEdit_Progress2_Goal1.Properties.ReadOnly = True
            ComboBoxEdit_Progress3_Goal1.Properties.ReadOnly = True

            '' Goal 2
            ComboBoxEdit_Goal2.Properties.ReadOnly = True

            ComboBoxEdit_Progress1_Goal2.Properties.ReadOnly = True
            ComboBoxEdit_Progress2_Goal2.Properties.ReadOnly = True
            ComboBoxEdit_Progress3_Goal2.Properties.ReadOnly = True

            '' Goal 3
            ComboBoxEdit_Goal3.Properties.ReadOnly = True

            ComboBoxEdit_Progress1_Goal3.Properties.ReadOnly = True
            ComboBoxEdit_Progress2_Goal3.Properties.ReadOnly = True
            ComboBoxEdit_Progress3_Goal3.Properties.ReadOnly = True

            '' Goal 4
            ComboBoxEdit_Goal4.Properties.ReadOnly = True

            ComboBoxEdit_Progress1_Goal4.Properties.ReadOnly = True
            ComboBoxEdit_Progress2_Goal4.Properties.ReadOnly = True
            ComboBoxEdit_Progress3_Goal4.Properties.ReadOnly = True

            '' FICO Score
            txtFicoInitial.Properties.ReadOnly = True

            txtFicoProgress1.Properties.ReadOnly = True
            txtFicoProgress2.Properties.ReadOnly = True
            txtFicoProgress3.Properties.ReadOnly = True

            '' Unsecured Debt Total
            CalcEditDebtInitial.Properties.ReadOnly = True

            CalcEditDebtProgress1.Properties.ReadOnly = True
            CalcEditDebtProgress2.Properties.ReadOnly = True
            CalcEditDebtProgress3.Properties.ReadOnly = True

            '' Banked
            ComboBoxEdit_Banked.Properties.ReadOnly = True

            ComboBoxEdit_Progress1_Banked.Properties.ReadOnly = True
            ComboBoxEdit_Progress2_Banked.Properties.ReadOnly = True
            ComboBoxEdit_Progress3_Banked.Properties.ReadOnly = True

            '' US Finance
            ComboBoxEdit_USFinance.Properties.ReadOnly = True

            ComboBoxEdit_Progress1_USFinance.Properties.ReadOnly = True
            ComboBoxEdit_Progress2_USFinance.Properties.ReadOnly = True
            ComboBoxEdit_Progress3_USFinance.Properties.ReadOnly = True

            '' Credit Report
            ComboBoxEdit_CreditReport.Properties.ReadOnly = True

            ComboBoxEdit_Progress1_CreditReport.Properties.ReadOnly = True
            ComboBoxEdit_Progress2_CreditReport.Properties.ReadOnly = True
            ComboBoxEdit_Progress3_CreditReport.Properties.ReadOnly = True

            '' Program Participation
            ComboBoxEdit_ProgramParticipation.Properties.ReadOnly = True

            ComboBoxEdit_Progress1_ProgramParticipation.Properties.ReadOnly = True
            ComboBoxEdit_Progress2_ProgramParticipation.Properties.ReadOnly = True
            ComboBoxEdit_Progress3_ProgramParticipation.Properties.ReadOnly = True

            '' Ed Participation
            ComboBoxEdit_EduParticipation.Properties.ReadOnly = True

            ComboBoxEdit_Progress1_EduParticipation.Properties.ReadOnly = True
            ComboBoxEdit_Progress2_EduParticipation.Properties.ReadOnly = True
            ComboBoxEdit_Progress3_EduParticipation.Properties.ReadOnly = True
        End Sub

        Public Sub MarkControlsAsEditable()
            '' Goal 1
            ComboBoxEdit_Goal1.Properties.ReadOnly = False

            ComboBoxEdit_Progress1_Goal1.Properties.ReadOnly = False
            ComboBoxEdit_Progress2_Goal1.Properties.ReadOnly = False
            ComboBoxEdit_Progress3_Goal1.Properties.ReadOnly = False

            '' Goal 2
            ComboBoxEdit_Goal2.Properties.ReadOnly = False

            ComboBoxEdit_Progress1_Goal2.Properties.ReadOnly = False
            ComboBoxEdit_Progress2_Goal2.Properties.ReadOnly = False
            ComboBoxEdit_Progress3_Goal2.Properties.ReadOnly = False

            '' Goal 3
            ComboBoxEdit_Goal3.Properties.ReadOnly = False

            ComboBoxEdit_Progress1_Goal3.Properties.ReadOnly = False
            ComboBoxEdit_Progress2_Goal3.Properties.ReadOnly = False
            ComboBoxEdit_Progress3_Goal3.Properties.ReadOnly = False

            '' Goal 4
            ComboBoxEdit_Goal4.Properties.ReadOnly = False

            ComboBoxEdit_Progress1_Goal4.Properties.ReadOnly = False
            ComboBoxEdit_Progress2_Goal4.Properties.ReadOnly = False
            ComboBoxEdit_Progress3_Goal4.Properties.ReadOnly = False

            '' FICO Score
            txtFicoInitial.Properties.ReadOnly = False

            txtFicoProgress1.Properties.ReadOnly = False
            txtFicoProgress2.Properties.ReadOnly = False
            txtFicoProgress3.Properties.ReadOnly = False

            '' Unsecured Debt Total
            CalcEditDebtInitial.Properties.ReadOnly = False

            CalcEditDebtProgress1.Properties.ReadOnly = False
            CalcEditDebtProgress2.Properties.ReadOnly = False
            CalcEditDebtProgress3.Properties.ReadOnly = False

            '' Banked
            ComboBoxEdit_Banked.Properties.ReadOnly = False

            ComboBoxEdit_Progress1_Banked.Properties.ReadOnly = False
            ComboBoxEdit_Progress2_Banked.Properties.ReadOnly = False
            ComboBoxEdit_Progress3_Banked.Properties.ReadOnly = False

            '' US Financial
            ComboBoxEdit_USFinance.Properties.ReadOnly = False

            ComboBoxEdit_Progress1_USFinance.Properties.ReadOnly = False
            ComboBoxEdit_Progress2_USFinance.Properties.ReadOnly = False
            ComboBoxEdit_Progress3_USFinance.Properties.ReadOnly = False

            '' Credit Report
            ComboBoxEdit_CreditReport.Properties.ReadOnly = False

            ComboBoxEdit_Progress1_CreditReport.Properties.ReadOnly = False
            ComboBoxEdit_Progress2_CreditReport.Properties.ReadOnly = False
            ComboBoxEdit_Progress3_CreditReport.Properties.ReadOnly = False

            '' Program Participation
            ComboBoxEdit_ProgramParticipation.Properties.ReadOnly = False

            ComboBoxEdit_Progress1_ProgramParticipation.Properties.ReadOnly = False
            ComboBoxEdit_Progress2_ProgramParticipation.Properties.ReadOnly = False
            ComboBoxEdit_Progress3_ProgramParticipation.Properties.ReadOnly = False

            '' Ed Participation
            ComboBoxEdit_EduParticipation.Properties.ReadOnly = False

            ComboBoxEdit_Progress1_EduParticipation.Properties.ReadOnly = False
            ComboBoxEdit_Progress2_EduParticipation.Properties.ReadOnly = False
            ComboBoxEdit_Progress3_EduParticipation.Properties.ReadOnly = False
        End Sub

        ''' <summary>
        ''' Save the information once we have a goal ID
        ''' </summary>
        Public Shadows Sub SaveForm(ByVal goalID As Integer)

            Dim currentGoalItems As List(Of ClientGoalItem) = GetControlValues()

            If Not currentGoalItems Is Nothing AndAlso currentGoalItems.Count > 0 Then
                '' set the Client Goal ID
                For Each goalItem As ClientGoalItem In currentGoalItems
                    goalItem.ClientGoalID = goalID
                Next
            End If

            Using bc As New DebtPlus.LINQ.BusinessContext()

                Dim oldGoalItems As List(Of ClientGoalItem) = bc.ClientGoalItems.Where(Function(i) i.ClientGoalID = goalID AndAlso i.ActiveFlag = True AndAlso i.UserType = UserType).ToList()

                If Not oldGoalItems Is Nothing AndAlso oldGoalItems.Count > 0 Then
                    For Each item As ClientGoalItem In oldGoalItems
                        item.ActiveFlag = False
                    Next
                End If

                bc.ClientGoalItems.InsertAllOnSubmit(currentGoalItems)
                bc.SubmitChanges()
            End Using

        End Sub

        Public Function GetControlValues() As List(Of ClientGoalItem)

            Dim currentGoalItems As List(Of ClientGoalItem) = New List(Of ClientGoalItem)()

            Dim goalItem As ClientGoalItem = Nothing
            '' Goal 1
            goalItem = GetGoalData(Goal1)
            If Not goalItem Is Nothing Then
                currentGoalItems.Add(goalItem)
            End If

            '' Goal 2
            goalItem = GetGoalData(Goal2)
            If Not goalItem Is Nothing Then
                currentGoalItems.Add(goalItem)
            End If

            '' Goal 3
            goalItem = GetGoalData(Goal3)
            If Not goalItem Is Nothing Then
                currentGoalItems.Add(goalItem)
            End If

            '' Goal 4
            goalItem = GetGoalData(Goal4)
            If Not goalItem Is Nothing Then
                currentGoalItems.Add(goalItem)
            End If

            '' Fico Score
            goalItem = GetGoalData(FicoScore)
            If Not goalItem Is Nothing Then
                currentGoalItems.Add(goalItem)
            End If

            '' Unsecured Debt Total
            goalItem = GetGoalData(UnsecuredDebtTotal)
            If Not goalItem Is Nothing Then
                currentGoalItems.Add(goalItem)
            End If

            '' Banked
            goalItem = GetGoalData(Banked)
            If Not goalItem Is Nothing Then
                currentGoalItems.Add(goalItem)
            End If

            '' US Financial System
            goalItem = GetGoalData(USFinancial)
            If Not goalItem Is Nothing Then
                currentGoalItems.Add(goalItem)
            End If

            '' Error Credit Report
            goalItem = GetGoalData(CreditReport)
            If Not goalItem Is Nothing Then
                currentGoalItems.Add(goalItem)
            End If

            '' Program Participation
            goalItem = GetGoalData(ProgramParticipation)
            If Not goalItem Is Nothing Then
                currentGoalItems.Add(goalItem)
            End If

            '' Education Participation
            goalItem = GetGoalData(EducationParticipation)
            If Not goalItem Is Nothing Then
                currentGoalItems.Add(goalItem)
            End If

            Return currentGoalItems

        End Function

        Private Function GetGoalData(goalType As Integer) As ClientGoalItem

            Dim goalItem As ClientGoalItem = New ClientGoalItem()
            With goalItem
                .ActiveFlag = True
                .GoalType = goalType
                .UserType = UserType
                .CreatedBy = DebtPlus.LINQ.BusinessContext.suser_sname()
                .CreatedTimestamp = DateTime.Now
            End With

            Select Case goalType
                Case Goal1
                    If ComboBoxEdit_Goal1.EditValue Is Nothing Or GetComboEditValue(ComboBoxEdit_Goal1.EditValue) Is Nothing Then
                        Return Nothing
                    Else
                        goalItem.GoalValue = GetComboEditValue(ComboBoxEdit_Goal1.EditValue)
                        goalItem.GoalEnteredDate = GetDateValue(txtGoal1EnteredDate.Text)

                        goalItem.Progress1 = GetComboEditValue(ComboBoxEdit_Progress1_Goal1.EditValue)
                        goalItem.Progress1Date = GetDateValue(txtProgress1Goal1EnteredDate.Text)

                        goalItem.Progress2 = GetComboEditValue(ComboBoxEdit_Progress2_Goal1.EditValue)
                        goalItem.Progress2Date = GetDateValue(txtProgress2Goal1EnteredDate.Text)

                        goalItem.Progress3 = GetComboEditValue(ComboBoxEdit_Progress3_Goal1.EditValue)
                        goalItem.Progress3Date = GetDateValue(txtProgress3Goal1EnteredDate.Text)
                    End If

                Case Goal2
                    If ComboBoxEdit_Goal2.EditValue Is Nothing Or GetComboEditValue(ComboBoxEdit_Goal2.EditValue) Is Nothing Then
                        Return Nothing
                    Else
                        goalItem.GoalValue = GetComboEditValue(ComboBoxEdit_Goal2.EditValue)
                        goalItem.GoalEnteredDate = GetDateValue(txtGoal2EnteredDate.Text)

                        goalItem.Progress1 = GetComboEditValue(ComboBoxEdit_Progress1_Goal2.EditValue)
                        goalItem.Progress1Date = GetDateValue(txtProgress1Goal2EnteredDate.Text)

                        goalItem.Progress2 = GetComboEditValue(ComboBoxEdit_Progress2_Goal2.EditValue)
                        goalItem.Progress2Date = GetDateValue(txtProgress2Goal2EnteredDate.Text)

                        goalItem.Progress3 = GetComboEditValue(ComboBoxEdit_Progress3_Goal2.EditValue)
                        goalItem.Progress3Date = GetDateValue(txtProgress3Goal2EnteredDate.Text)
                    End If

                Case Goal3
                    If ComboBoxEdit_Goal3.EditValue Is Nothing Or GetComboEditValue(ComboBoxEdit_Goal3.EditValue) Is Nothing Then
                        Return Nothing
                    Else
                        goalItem.GoalValue = GetComboEditValue(ComboBoxEdit_Goal3.EditValue)
                        goalItem.GoalEnteredDate = GetDateValue(txtGoal3EnteredDate.Text)

                        goalItem.Progress1 = GetComboEditValue(ComboBoxEdit_Progress1_Goal3.EditValue)
                        goalItem.Progress1Date = GetDateValue(txtProgress1Goal3EnteredDate.Text)

                        goalItem.Progress2 = GetComboEditValue(ComboBoxEdit_Progress2_Goal3.EditValue)
                        goalItem.Progress2Date = GetDateValue(txtProgress2Goal3EnteredDate.Text)

                        goalItem.Progress3 = GetComboEditValue(ComboBoxEdit_Progress3_Goal3.EditValue)
                        goalItem.Progress3Date = GetDateValue(txtProgress3Goal3EnteredDate.Text)
                    End If

                Case Goal4
                    If ComboBoxEdit_Goal4.EditValue Is Nothing Or GetComboEditValue(ComboBoxEdit_Goal4.EditValue) Is Nothing Then
                        Return Nothing
                    Else
                        goalItem.GoalValue = GetComboEditValue(ComboBoxEdit_Goal4.EditValue)
                        goalItem.GoalEnteredDate = GetDateValue(txtGoal4EnteredDate.Text)

                        goalItem.Progress1 = GetComboEditValue(ComboBoxEdit_Progress1_Goal4.EditValue)
                        goalItem.Progress1Date = GetDateValue(txtProgress1Goal4EnteredDate.Text)

                        goalItem.Progress2 = GetComboEditValue(ComboBoxEdit_Progress2_Goal4.EditValue)
                        goalItem.Progress2Date = GetDateValue(txtProgress2Goal4EnteredDate.Text)

                        goalItem.Progress3 = GetComboEditValue(ComboBoxEdit_Progress3_Goal4.EditValue)
                        goalItem.Progress3Date = GetDateValue(txtProgress3Goal4EnteredDate.Text)
                    End If

                Case FicoScore
                    If String.IsNullOrWhiteSpace(txtFicoInitial.Text.Trim()) Then
                        Return Nothing
                    Else
                        goalItem.GoalValue = Integer.Parse(txtFicoInitial.Text.Trim())
                        goalItem.GoalEnteredDate = GetDateValue(txtFicoEnteredDate.Text)

                        goalItem.Progress1 = Integer.Parse(txtFicoProgress1.Text.Trim())
                        goalItem.Progress1Date = GetDateValue(txtFicoProgress1EnteredDate.Text)

                        goalItem.Progress2 = Integer.Parse(txtFicoProgress2.Text.Trim())
                        goalItem.Progress2Date = GetDateValue(txtFicoProgress2EnteredDate.Text)

                        goalItem.Progress3 = Integer.Parse(txtFicoProgress3.Text.Trim())
                        goalItem.Progress3Date = GetDateValue(txtFicoProgress3EnteredDate.Text)
                    End If

                Case UnsecuredDebtTotal
                    If String.IsNullOrWhiteSpace(CalcEditDebtInitial.Text.Trim()) Then
                        Return Nothing
                    Else
                        goalItem.GoalValue = GetIntegerValue(CalcEditDebtInitial.Text)
                        goalItem.GoalEnteredDate = GetDateValue(txtDebtEnteredDate.Text)

                        goalItem.Progress1 = GetIntegerValue(CalcEditDebtProgress1.Text)
                        goalItem.Progress1Date = GetDateValue(txtDebtProgress1EnteredDate.Text)

                        goalItem.Progress2 = GetIntegerValue(CalcEditDebtProgress2.Text)
                        goalItem.Progress2Date = GetDateValue(txtDebtProgress2EnteredDate.Text)

                        goalItem.Progress3 = GetIntegerValue(CalcEditDebtProgress3.Text)
                        goalItem.Progress3Date = GetDateValue(txtDebtProgress3EnteredDate.Text)
                    End If

                Case Banked
                    If ComboBoxEdit_Banked.EditValue Is Nothing Or GetComboEditValue(ComboBoxEdit_Banked.EditValue) Is Nothing Then
                        Return Nothing
                    Else
                        goalItem.GoalValue = GetComboEditValue(ComboBoxEdit_Banked.EditValue)
                        goalItem.GoalEnteredDate = GetDateValue(txtBankedEnteredDate.Text)

                        goalItem.Progress1 = GetComboEditValue(ComboBoxEdit_Progress1_Banked.EditValue)
                        goalItem.Progress1Date = GetDateValue(txtProgress1BankedEnteredDate.Text)

                        goalItem.Progress2 = GetComboEditValue(ComboBoxEdit_Progress2_Banked.EditValue)
                        goalItem.Progress2Date = GetDateValue(txtProgress2BankedEnteredDate.Text)

                        goalItem.Progress3 = GetComboEditValue(ComboBoxEdit_Progress3_Banked.EditValue)
                        goalItem.Progress3Date = GetDateValue(txtProgress3BankedEnteredDate.Text)
                    End If

                Case USFinancial
                    If ComboBoxEdit_USFinance.EditValue Is Nothing Or GetComboEditValue(ComboBoxEdit_USFinance.EditValue) Is Nothing Then
                        Return Nothing
                    Else
                        goalItem.GoalValue = GetComboEditValue(ComboBoxEdit_USFinance.EditValue)
                        goalItem.GoalEnteredDate = GetDateValue(txtUsFinanceEnteredDate.Text)

                        goalItem.Progress1 = GetComboEditValue(ComboBoxEdit_Progress1_USFinance.EditValue)
                        goalItem.Progress1Date = GetDateValue(txtProgress1UsFinanceEnteredDate.Text)

                        goalItem.Progress2 = GetComboEditValue(ComboBoxEdit_Progress2_USFinance.EditValue)
                        goalItem.Progress2Date = GetDateValue(txtProgress2UsFinanceEnteredDate.Text)

                        goalItem.Progress3 = GetComboEditValue(ComboBoxEdit_Progress3_USFinance.EditValue)
                        goalItem.Progress3Date = GetDateValue(txtProgress3UsFinanceEnteredDate.Text)
                    End If

                Case CreditReport
                    If ComboBoxEdit_CreditReport.EditValue Is Nothing Or GetComboEditValue(ComboBoxEdit_CreditReport.EditValue) Is Nothing Then
                        Return Nothing
                    Else
                        goalItem.GoalValue = GetComboEditValue(ComboBoxEdit_CreditReport.EditValue)
                        goalItem.GoalEnteredDate = GetDateValue(txtCreditReportEnteredDate.Text)

                        goalItem.Progress1 = GetComboEditValue(ComboBoxEdit_Progress1_CreditReport.EditValue)
                        goalItem.Progress1Date = GetDateValue(txtProgress1CreditReportEnteredDate.Text)

                        goalItem.Progress2 = GetComboEditValue(ComboBoxEdit_Progress2_CreditReport.EditValue)
                        goalItem.Progress2Date = GetDateValue(txtProgress2CreditReportEnteredDate.Text)

                        goalItem.Progress3 = GetComboEditValue(ComboBoxEdit_Progress3_CreditReport.EditValue)
                        goalItem.Progress3Date = GetDateValue(txtProgress3CreditReportEnteredDate.Text)
                    End If

                Case ProgramParticipation
                    If ComboBoxEdit_ProgramParticipation.EditValue Is Nothing Or GetComboEditValue(ComboBoxEdit_ProgramParticipation.EditValue) Is Nothing Then
                        Return Nothing
                    Else
                        goalItem.GoalValue = GetComboEditValue(ComboBoxEdit_ProgramParticipation.EditValue)
                        goalItem.GoalEnteredDate = GetDateValue(txtProgramParticipationEnteredDate.Text)

                        goalItem.Progress1 = GetComboEditValue(ComboBoxEdit_Progress1_ProgramParticipation.EditValue)
                        goalItem.Progress1Date = GetDateValue(txtProgress1ProgramParticipationEnteredDate.Text)

                        goalItem.Progress2 = GetComboEditValue(ComboBoxEdit_Progress2_ProgramParticipation.EditValue)
                        goalItem.Progress2Date = GetDateValue(txtProgress2ProgramParticipationEnteredDate.Text)

                        goalItem.Progress3 = GetComboEditValue(ComboBoxEdit_Progress3_ProgramParticipation.EditValue)
                        goalItem.Progress3Date = GetDateValue(txtProgress3ProgramParticipationEnteredDate.Text)
                    End If

                Case EducationParticipation
                    If ComboBoxEdit_EduParticipation.EditValue Is Nothing Or GetComboEditValue(ComboBoxEdit_EduParticipation.EditValue) Is Nothing Then
                        Return Nothing
                    Else
                        goalItem.GoalValue = GetComboEditValue(ComboBoxEdit_EduParticipation.EditValue)
                        goalItem.GoalEnteredDate = GetDateValue(txtEduParticipationEnteredDate.Text)

                        goalItem.Progress1 = GetComboEditValue(ComboBoxEdit_Progress1_EduParticipation.EditValue)
                        goalItem.Progress1Date = GetDateValue(txtProgress1EduParticipationEnteredDate.Text)

                        goalItem.Progress2 = GetComboEditValue(ComboBoxEdit_Progress2_EduParticipation.EditValue)
                        goalItem.Progress2Date = GetDateValue(txtProgress2EduParticipationEnteredDate.Text)

                        goalItem.Progress3 = GetComboEditValue(ComboBoxEdit_Progress3_EduParticipation.EditValue)
                        goalItem.Progress3Date = GetDateValue(txtProgress3EduParticipationEnteredDate.Text)
                    End If

                Case Else

            End Select

            Return goalItem
        End Function

        Private Function GetComboEditValue(val As Object) As Integer?
            If val > 0 Then
                Return Convert.ToInt32(val)
            Else
                Return Nothing
            End If
        End Function

        Private Function GetDateValue(dateValue As String) As Date?
            If Not String.IsNullOrWhiteSpace(dateValue) Then
                Return Convert.ToDateTime(dateValue).Date
            Else
                Return Nothing
            End If
        End Function

        ''' <summary>
        ''' Return the directory to the saved layout information
        ''' </summary>
        Protected Overridable Function XMLBasePath() As String
            Dim BasePath As String = String.Format("{0}{1}DebtPlus", Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), Path.DirectorySeparatorChar)
            Return Path.Combine(BasePath, String.Format("Client.Update{0}{1}", Path.DirectorySeparatorChar, Nothing))
        End Function

        Private Function GetDate() As String
            Return DateTime.Now.ToString("MM/dd/yyyy")
        End Function

        Private Sub SetValue(lookUpEdit As DevExpress.XtraEditors.LookUpEdit, textEdit As DevExpress.XtraEditors.TextEdit)
            If lookUpEdit.EditValue > 0 Then
                textEdit.Text = GetDate()
            Else
                textEdit.Text = String.Empty
            End If

            _isModified = True
        End Sub

        Private Sub SetValue(spinEdit As DevExpress.XtraEditors.SpinEdit, textEdit As DevExpress.XtraEditors.TextEdit)
            Dim controlValue As Integer = 0
            If Not String.IsNullOrWhiteSpace(spinEdit.Text) AndAlso Integer.TryParse(spinEdit.Text.Trim(), controlValue) Then
                If controlValue > 0 Then
                    textEdit.Text = GetDate()
                Else
                    textEdit.Text = String.Empty
                End If
            Else
                textEdit.Text = String.Empty
            End If

            _isModified = True
        End Sub

        Private Sub SetValue(calcEdit As DevExpress.XtraEditors.CalcEdit, textEdit As DevExpress.XtraEditors.TextEdit)
            Dim controlValue As Integer = GetIntegerValue(calcEdit.Text)

            If controlValue > 0 Then
                textEdit.Text = GetDate()
            Else
                textEdit.Text = String.Empty
            End If

            _isModified = True
        End Sub


        '' Financial Status
        Private Sub ComboBoxEdit_Goal1_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_Goal1, txtGoal1EnteredDate)

            '' Reset the progress when the Goal is changed
            ComboBoxEdit_Progress1_Goal1.EditValue = Nothing
            ComboBoxEdit_Progress2_Goal1.EditValue = Nothing
            ComboBoxEdit_Progress3_Goal1.EditValue = Nothing

            txtProgress1Goal1EnteredDate.Text = String.Empty
            txtProgress2Goal1EnteredDate.Text = String.Empty
            txtProgress3Goal1EnteredDate.Text = String.Empty
        End Sub

        Private Sub ComboBoxEdit_Goal2_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_Goal2, txtGoal2EnteredDate)

            '' Reset the progress when the Goal is changed
            ComboBoxEdit_Progress1_Goal2.EditValue = Nothing
            ComboBoxEdit_Progress2_Goal2.EditValue = Nothing
            ComboBoxEdit_Progress3_Goal2.EditValue = Nothing

            txtProgress1Goal2EnteredDate.Text = String.Empty
            txtProgress2Goal2EnteredDate.Text = String.Empty
            txtProgress3Goal2EnteredDate.Text = String.Empty
        End Sub

        Private Sub ComboBoxEdit_Goal3_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_Goal3, txtGoal3EnteredDate)

            '' Reset the progress when the Goal is changed
            ComboBoxEdit_Progress1_Goal3.EditValue = Nothing
            ComboBoxEdit_Progress2_Goal3.EditValue = Nothing
            ComboBoxEdit_Progress3_Goal3.EditValue = Nothing

            txtProgress1Goal3EnteredDate.Text = String.Empty
            txtProgress2Goal3EnteredDate.Text = String.Empty
            txtProgress3Goal3EnteredDate.Text = String.Empty
        End Sub

        Private Sub ComboBoxEdit_Goal4_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_Goal4, txtGoal4EnteredDate)

            '' Reset the progress when the Goal is changed
            ComboBoxEdit_Progress1_Goal4.EditValue = Nothing
            ComboBoxEdit_Progress2_Goal4.EditValue = Nothing
            ComboBoxEdit_Progress3_Goal4.EditValue = Nothing

            txtProgress1Goal4EnteredDate.Text = String.Empty
            txtProgress2Goal4EnteredDate.Text = String.Empty
            txtProgress3Goal4EnteredDate.Text = String.Empty
        End Sub

        '' Banked
        Private Sub ComboBoxEdit_Banked_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_Banked, txtBankedEnteredDate)

            '' Reset the progress when the Banked is changed
            ComboBoxEdit_Progress1_Banked.EditValue = Nothing
            ComboBoxEdit_Progress2_Banked.EditValue = Nothing
            ComboBoxEdit_Progress3_Banked.EditValue = Nothing

            txtProgress1BankedEnteredDate.Text = String.Empty
            txtProgress2BankedEnteredDate.Text = String.Empty
            txtProgress3BankedEnteredDate.Text = String.Empty
        End Sub

        '' US Finance
        Private Sub ComboBoxEdit_USFinance_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_USFinance, txtUsFinanceEnteredDate)

            '' Reset the progress when the Us Finance is changed
            ComboBoxEdit_Progress1_USFinance.EditValue = Nothing
            ComboBoxEdit_Progress2_USFinance.EditValue = Nothing
            ComboBoxEdit_Progress3_USFinance.EditValue = Nothing

            txtProgress1UsFinanceEnteredDate.Text = String.Empty
            txtProgress2UsFinanceEnteredDate.Text = String.Empty
            txtProgress3UsFinanceEnteredDate.Text = String.Empty
        End Sub

        '' Error Credit Report
        Private Sub ComboBoxEdit_CreditReport_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_CreditReport, txtCreditReportEnteredDate)

            '' Reset the progress when the Credit Report is changed
            ComboBoxEdit_Progress1_CreditReport.EditValue = Nothing
            ComboBoxEdit_Progress2_CreditReport.EditValue = Nothing
            ComboBoxEdit_Progress3_CreditReport.EditValue = Nothing

            txtProgress1CreditReportEnteredDate.Text = String.Empty
            txtProgress2CreditReportEnteredDate.Text = String.Empty
            txtProgress3CreditReportEnteredDate.Text = String.Empty
        End Sub

        '' Program Participation
        Private Sub ComboBoxEdit_ProgramParticipation_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_ProgramParticipation, txtProgramParticipationEnteredDate)

            '' Reset the progress when the Program Participation is changed
            ComboBoxEdit_Progress1_ProgramParticipation.EditValue = Nothing
            ComboBoxEdit_Progress2_ProgramParticipation.EditValue = Nothing
            ComboBoxEdit_Progress3_ProgramParticipation.EditValue = Nothing

            txtProgress1ProgramParticipationEnteredDate.Text = String.Empty
            txtProgress2ProgramParticipationEnteredDate.Text = String.Empty
            txtProgress3ProgramParticipationEnteredDate.Text = String.Empty
        End Sub

        '' Education Participation
        Private Sub ComboBoxEdit_EduParticipation_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_EduParticipation, txtEduParticipationEnteredDate)

            '' Reset the progress when the Program Participation is changed
            ComboBoxEdit_Progress1_EduParticipation.EditValue = Nothing
            ComboBoxEdit_Progress2_EduParticipation.EditValue = Nothing
            ComboBoxEdit_Progress3_EduParticipation.EditValue = Nothing

            txtProgress1EduParticipationEnteredDate.Text = String.Empty
            txtProgress2EduParticipationEnteredDate.Text = String.Empty
            txtProgress3EduParticipationEnteredDate.Text = String.Empty
        End Sub

        '' Financial Status - Progress 1
        Private Sub ComboBoxEdit_Progress1_Goal1_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_Progress1_Goal1, txtProgress1Goal1EnteredDate)
        End Sub

        Private Sub ComboBoxEdit_Progress1_Goal2_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_Progress1_Goal2, txtProgress1Goal2EnteredDate)
        End Sub

        Private Sub ComboBoxEdit_Progress1_Goal3_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_Progress1_Goal3, txtProgress1Goal3EnteredDate)
        End Sub

        Private Sub ComboBoxEdit_Progress1_Goal4_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_Progress1_Goal4, txtProgress1Goal4EnteredDate)
        End Sub

        '' Financial Status - Progress 2
        Private Sub ComboBoxEdit_Progress2_Goal1_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_Progress2_Goal1, txtProgress2Goal1EnteredDate)
        End Sub

        Private Sub ComboBoxEdit_Progress2_Goal2_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_Progress2_Goal2, txtProgress2Goal2EnteredDate)
        End Sub

        Private Sub ComboBoxEdit_Progress2_Goal3_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_Progress2_Goal3, txtProgress2Goal3EnteredDate)
        End Sub

        Private Sub ComboBoxEdit_Progress2_Goal4_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_Progress2_Goal4, txtProgress2Goal4EnteredDate)
        End Sub

        '' Financial Status - Progress 3
        Private Sub ComboBoxEdit_Progress3_Goal1_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_Progress3_Goal1, txtProgress3Goal1EnteredDate)
        End Sub

        Private Sub ComboBoxEdit_Progress3_Goal2_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_Progress3_Goal2, txtProgress3Goal2EnteredDate)
        End Sub

        Private Sub ComboBoxEdit_Progress3_Goal3_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_Progress3_Goal3, txtProgress3Goal3EnteredDate)
        End Sub

        Private Sub ComboBoxEdit_Progress3_Goal4_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_Progress3_Goal4, txtProgress3Goal4EnteredDate)
        End Sub

        '' Banked
        Private Sub ComboBoxEdit_Progress1_Banked_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_Progress1_Banked, txtProgress1BankedEnteredDate)
        End Sub

        Private Sub ComboBoxEdit_Progress2_Banked_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_Progress2_Banked, txtProgress2BankedEnteredDate)
        End Sub

        Private Sub ComboBoxEdit_Progress3_Banked_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_Progress3_Banked, txtProgress3BankedEnteredDate)
        End Sub

        '' US Finance
        Private Sub ComboBoxEdit_Progress1_USFinance_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_Progress1_USFinance, txtProgress1UsFinanceEnteredDate)
        End Sub

        Private Sub ComboBoxEdit_Progress2_USFinance_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_Progress2_USFinance, txtProgress2UsFinanceEnteredDate)
        End Sub

        Private Sub ComboBoxEdit_Progress3_USFinance_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_Progress3_USFinance, txtProgress3UsFinanceEnteredDate)
        End Sub

        '' Error Credit Report
        Private Sub ComboBoxEdit_Progress1_CreditReport_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_Progress1_CreditReport, txtProgress1CreditReportEnteredDate)
        End Sub

        Private Sub ComboBoxEdit_Progress2_CreditReport_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_Progress2_CreditReport, txtProgress2CreditReportEnteredDate)
        End Sub

        Private Sub ComboBoxEdit_Progress3_CreditReport_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_Progress3_CreditReport, txtProgress3CreditReportEnteredDate)
        End Sub

        '' Program Participation
        Private Sub ComboBoxEdit_Progress1_ProgramParticipation_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_Progress1_ProgramParticipation, txtProgress1ProgramParticipationEnteredDate)
        End Sub

        Private Sub ComboBoxEdit_Progress2_ProgramParticipation_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_Progress2_ProgramParticipation, txtProgress2ProgramParticipationEnteredDate)
        End Sub

        Private Sub ComboBoxEdit_Progress3_ProgramParticipation_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_Progress3_ProgramParticipation, txtProgress3ProgramParticipationEnteredDate)
        End Sub

        '' Education Participation
        Private Sub ComboBoxEdit_Progress1_EduParticipation_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_Progress1_EduParticipation, txtProgress1EduParticipationEnteredDate)
        End Sub

        Private Sub ComboBoxEdit_Progress2_EduParticipation_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_Progress2_EduParticipation, txtProgress2EduParticipationEnteredDate)
        End Sub

        Private Sub ComboBoxEdit_Progress3_EduParticipation_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(ComboBoxEdit_Progress3_EduParticipation, txtProgress3EduParticipationEnteredDate)
        End Sub

        '' Fico Score
        Private Sub txtFicoInitial_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(txtFicoInitial, txtFicoEnteredDate)

            '' Reset the progress when the Fico Score is changed
            txtFicoProgress1.Text = String.Empty
            txtFicoProgress2.Text = String.Empty
            txtFicoProgress3.Text = String.Empty

            txtFicoProgress1EnteredDate.Text = String.Empty
            txtFicoProgress2EnteredDate.Text = String.Empty
            txtFicoProgress3EnteredDate.Text = String.Empty
        End Sub

        Private Sub txtFicoProgress1_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(txtFicoProgress1, txtFicoProgress1EnteredDate)
        End Sub

        Private Sub txtFicoProgress2_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(txtFicoProgress2, txtFicoProgress2EnteredDate)
        End Sub

        Private Sub txtFicoProgress3_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(txtFicoProgress3, txtFicoProgress3EnteredDate)
        End Sub

        '' Fico Score
        Private Sub CalcEditDebtInitial_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(CalcEditDebtInitial, txtDebtEnteredDate)

            '' Reset the progress when the Unsecured Debt Total is changed
            CalcEditDebtProgress1.Text = Nothing
            CalcEditDebtProgress2.Text = Nothing
            CalcEditDebtProgress3.Text = Nothing

            txtDebtProgress1EnteredDate.Text = String.Empty
            txtDebtProgress2EnteredDate.Text = String.Empty
            txtDebtProgress3EnteredDate.Text = String.Empty
        End Sub

        Private Sub CalcEditDebtProgress1_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(CalcEditDebtProgress1, txtDebtProgress1EnteredDate)
        End Sub

        Private Sub CalcEditDebtProgress2_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(CalcEditDebtProgress2, txtDebtProgress2EnteredDate)
        End Sub

        Private Sub CalcEditDebtProgress3_EditValueChanged(sender As System.Object, e As System.EventArgs)
            SetValue(CalcEditDebtProgress3, txtDebtProgress3EnteredDate)
        End Sub

        Private Function GetIntegerValue(text As String) As Integer
            Dim result As Integer = 0
            If Not String.IsNullOrWhiteSpace(text) AndAlso text <> "NULL" Then
                result = Integer.Parse(text.Trim(), Globalization.NumberStyles.Currency)
            End If
            Return result
        End Function

    End Class
End Namespace
