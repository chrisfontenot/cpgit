﻿Public Class GoalSummary

    ' Use for logging purposes if needed
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

    Private _goalID As Integer
    Public Property GoalID() As Integer
        Get
            Return _goalID
        End Get
        Set(ByVal value As Integer)
            _goalID = value
        End Set
    End Property

    Private _lineOfService As String
    Public Property LineOfService() As String
        Get
            Return _lineOfService
        End Get
        Set(ByVal value As String)
            _lineOfService = value
        End Set
    End Property

    Private _lineOfServiceType As Integer
    Public Property LineOfServiceType() As Integer
        Get
            Return _lineOfServiceType
        End Get
        Set(ByVal value As Integer)
            _lineOfServiceType = value
        End Set
    End Property

    Private _createdBy As String
    Public Property CreatedBy() As String
        Get
            Return _createdBy
        End Get
        Set(ByVal value As String)
            _createdBy = value
        End Set
    End Property

    Private _createdDate As DateTime
    Public Property CreatedTimestamp() As DateTime
        Get
            Return _createdDate
        End Get
        Set(ByVal value As DateTime)
            _createdDate = value
        End Set
    End Property

    Private _modifiedBy As String
    Public Property ModifiedBy() As String
        Get
            Return _modifiedBy
        End Get
        Set(ByVal value As String)
            _modifiedBy = value
        End Set
    End Property

    Private _modifiedDate As DateTime?
    Public Property ModifiedTimestamp() As DateTime?
        Get
            Return _modifiedDate
        End Get
        Set(ByVal value As DateTime?)
            _modifiedDate = value
        End Set
    End Property

    Private _applicant As String
    Public Property Applicant() As String
        Get
            Return _applicant
        End Get
        Set(ByVal value As String)
            _applicant = value
        End Set
    End Property

    Private _coapplicant As String
    Public Property Coapplicant() As String
        Get
            Return _coapplicant
        End Get
        Set(ByVal value As String)
            _coapplicant = value
        End Set
    End Property
End Class
