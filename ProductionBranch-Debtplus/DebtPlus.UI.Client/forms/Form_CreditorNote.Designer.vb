﻿Namespace forms
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form_CreditorNote
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.MemoEdit_Message = New DevExpress.XtraEditors.MemoEdit
            Me.LookUpEdit_MessageList = New DevExpress.XtraEditors.LookUpEdit
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.DateEdit_note_date = New DevExpress.XtraEditors.DateEdit
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
            Me.CalcEdit_note_amount = New DevExpress.XtraEditors.CalcEdit
            Me.SimpleButton_OK = New DevExpress.XtraEditors.SimpleButton
            Me.SimpleButton_Cancel = New DevExpress.XtraEditors.SimpleButton
            Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.MemoEdit_Message.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_MessageList.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit_note_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit_note_date.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_note_amount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(14, 16)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(42, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "&Message"
            '
            'MemoEdit_Message
            '
            Me.MemoEdit_Message.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                                                 Or System.Windows.Forms.AnchorStyles.Left) _
                                                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.MemoEdit_Message.Location = New System.Drawing.Point(86, 13)
            Me.MemoEdit_Message.Name = "MemoEdit_Message"
            Me.MemoEdit_Message.Size = New System.Drawing.Size(281, 69)
            Me.MemoEdit_Message.TabIndex = 1
            '
            'LookUpEdit_MessageList
            '
            Me.LookUpEdit_MessageList.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                                                      Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LookUpEdit_MessageList.Location = New System.Drawing.Point(86, 88)
            Me.LookUpEdit_MessageList.Name = "LookUpEdit_MessageList"
            Me.LookUpEdit_MessageList.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_MessageList.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.None)})
            Me.LookUpEdit_MessageList.Properties.DisplayMember = "description"
            Me.LookUpEdit_MessageList.Properties.NullText = ""
            Me.LookUpEdit_MessageList.Properties.ShowFooter = False
            Me.LookUpEdit_MessageList.Properties.ShowHeader = False
            Me.LookUpEdit_MessageList.Properties.ValueMember = "description"
            Me.LookUpEdit_MessageList.Size = New System.Drawing.Size(281, 20)
            Me.LookUpEdit_MessageList.TabIndex = 2
            Me.LookUpEdit_MessageList.Properties.SortColumnIndex = 1
            '
            'LabelControl2
            '
            Me.LabelControl2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.LabelControl2.Location = New System.Drawing.Point(14, 117)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(23, 13)
            Me.LabelControl2.TabIndex = 3
            Me.LabelControl2.Text = "&Date"
            '
            'DateEdit_note_date
            '
            Me.DateEdit_note_date.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.DateEdit_note_date.EditValue = Nothing
            Me.DateEdit_note_date.Location = New System.Drawing.Point(86, 114)
            Me.DateEdit_note_date.Name = "DateEdit_note_date"
            Me.DateEdit_note_date.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.DateEdit_note_date.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton})
            Me.DateEdit_note_date.Size = New System.Drawing.Size(100, 20)
            Me.DateEdit_note_date.TabIndex = 4
            '
            'LabelControl3
            '
            Me.LabelControl3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl3.Location = New System.Drawing.Point(224, 117)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(37, 13)
            Me.LabelControl3.TabIndex = 5
            Me.LabelControl3.Text = "&Amount"
            '
            'CalcEdit_note_amount
            '
            Me.CalcEdit_note_amount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.CalcEdit_note_amount.Location = New System.Drawing.Point(267, 114)
            Me.CalcEdit_note_amount.Name = "CalcEdit_note_amount"
            Me.CalcEdit_note_amount.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit_note_amount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_note_amount.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.CalcEdit_note_amount.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_note_amount.Properties.AppearanceDropDown.Options.UseTextOptions = True
            Me.CalcEdit_note_amount.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_note_amount.Properties.AppearanceFocused.Options.UseTextOptions = True
            Me.CalcEdit_note_amount.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_note_amount.Properties.AppearanceReadOnly.Options.UseTextOptions = True
            Me.CalcEdit_note_amount.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_note_amount.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_note_amount.Properties.DisplayFormat.FormatString = "{0:c}"
            Me.CalcEdit_note_amount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_note_amount.Properties.EditFormat.FormatString = "{0:c}"
            Me.CalcEdit_note_amount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_note_amount.Properties.Mask.BeepOnError = True
            Me.CalcEdit_note_amount.Properties.Mask.EditMask = "c"
            Me.CalcEdit_note_amount.Properties.Precision = 2
            Me.CalcEdit_note_amount.Size = New System.Drawing.Size(100, 20)
            Me.CalcEdit_note_amount.TabIndex = 6
            '
            'SimpleButton_OK
            '
            Me.SimpleButton_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.SimpleButton_OK.Location = New System.Drawing.Point(98, 170)
            Me.SimpleButton_OK.Name = "SimpleButton_OK"
            Me.SimpleButton_OK.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.TabIndex = 8
            Me.SimpleButton_OK.Text = "&OK"
            '
            'SimpleButton_Cancel
            '
            Me.SimpleButton_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.SimpleButton_Cancel.Location = New System.Drawing.Point(204, 170)
            Me.SimpleButton_Cancel.Name = "SimpleButton_Cancel"
            Me.SimpleButton_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.TabIndex = 9
            Me.SimpleButton_Cancel.Text = "&Cancel"
            '
            'CheckEdit1
            '
            Me.CheckEdit1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                                          Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.CheckEdit1.Location = New System.Drawing.Point(12, 140)
            Me.CheckEdit1.Name = "CheckEdit1"
            Me.CheckEdit1.Properties.Caption = "Send the same note to all active creditors for this client"
            Me.CheckEdit1.Size = New System.Drawing.Size(294, 19)
            Me.CheckEdit1.TabIndex = 7
            '
            'Form_CreditorNote
            '
            Me.AcceptButton = Me.SimpleButton_OK
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.SimpleButton_Cancel
            Me.ClientSize = New System.Drawing.Size(376, 205)
            Me.Controls.Add(Me.CheckEdit1)
            Me.Controls.Add(Me.SimpleButton_Cancel)
            Me.Controls.Add(Me.SimpleButton_OK)
            Me.Controls.Add(Me.CalcEdit_note_amount)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.DateEdit_note_date)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LookUpEdit_MessageList)
            Me.Controls.Add(Me.MemoEdit_Message)
            Me.Controls.Add(Me.LabelControl1)
            Me.Name = "Form_CreditorNote"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "Creditor Note"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.MemoEdit_Message.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_MessageList.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit_note_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit_note_date.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_note_amount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents MemoEdit_Message As DevExpress.XtraEditors.MemoEdit
        Friend WithEvents LookUpEdit_MessageList As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents DateEdit_note_date As DevExpress.XtraEditors.DateEdit
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents CalcEdit_note_amount As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents SimpleButton_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
    End Class
End Namespace