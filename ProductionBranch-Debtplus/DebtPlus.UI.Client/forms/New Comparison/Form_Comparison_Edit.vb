#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.ComponentModel
Imports DebtPlus.Data.Controls
Imports DebtPlus.UI.Client.Service
Imports DevExpress.XtraBars
Imports DebtPlus.UI.Client.forms.Comparison.Report
Imports System.Linq
Imports DebtPlus.LINQ

Namespace forms.Comparison

    Friend Class Form_Comparison_Edit
        Private Const STR_Percent As String = "{0:p3}"
        Private Const STR_Currency As String = "{0:c}"

        ' Handle to the debt being updated
        Private salesDebtRecord As sales_debt
        Private colPayout As System.Collections.Generic.List(Of DebtPlus.LINQ.Events.SalesPayoutEventArgs) = Nothing
        Private privateReadOnly As Boolean = False

        ''' <summary>
        ''' Is the record "read-only"?
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks>It is READONALY if the sales file was exported</remarks>
        Public Property [ReadOnly] As Boolean
            Get
                Return privateReadOnly
            End Get
            Set(value As Boolean)
                UnRegisterHandlers()
                privateReadOnly = value

                ' Enable or disable the read-only status in the controls
                cbo_Name.Properties.ReadOnly = value
                cb_Self_PaymentPercent.Properties.ReadOnly = value
                lk_CreditorType.Properties.ReadOnly = value
                tx_AccountNumber.Properties.ReadOnly = value
                clc_Balance.Properties.ReadOnly = value
                clc_Self_LateFees.Properties.ReadOnly = value
                clc_Self_OverlimitFees.Properties.ReadOnly = value
                clc_DMP_EnteredPayment.Properties.ReadOnly = value
                clc_DMP_InterestRate.Properties.ReadOnly = value
                clc_Self_InterestRate.Properties.ReadOnly = value

                RegisterHandlers()
            End Set
        End Property

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            cb_Self_PaymentPercent.Properties.Items.Clear()
            For Each Str As String In New String() {"1%", "2%", "2.08%", "2.5%", "2.78%", "3%", "3.5%", "4%", "4.5%", "5%"}
                cb_Self_PaymentPercent.Properties.Items.Add(Str)
            Next
        End Sub

        ''' <summary>
        ''' Initialize the form
        ''' </summary>
        Public Sub New(ByVal salesDebtRecord As sales_debt)
            MyClass.New()
            Me.salesDebtRecord = salesDebtRecord

            ' Define the lookup DataSource items
            lk_CreditorType.Properties.DataSource = DebtPlus.LINQ.Cache.sales_creditor_type.getList()

            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf Form_Load
            AddHandler FormClosing, AddressOf Form_Comparison_Edit_FormClosing
            AddHandler SimpleButton1.Click, AddressOf SimpleButton1_Click
            AddHandler cbo_Name.Validated, AddressOf Name_Validated
            AddHandler lk_CreditorType.EditValueChanged, AddressOf CreditorType_Changed
            AddHandler BarButtonItem_ClientPayout.ItemClick, AddressOf BarButtonItem_ClientPayout_ItemClick
            AddHandler BarButtonItem_AgencyPayout.ItemClick, AddressOf BarButtonItem_AgencyPayout_ItemClick
            AddHandler cb_Self_PaymentPercent.Validated, AddressOf cb_Self_PaymentPercent_Validated
            AddHandler cb_Self_PaymentPercent.EditValueChanging, AddressOf cb_Self_PaymentPercent_EditValueChanging
            AddHandler cb_Self_PaymentPercent.SelectedIndexChanged, AddressOf cb_Self_PaymentPercent_SelectedIndexChanged

            If salesDebtRecord IsNot Nothing Then
                AddHandler salesDebtRecord.PropertyChanged, AddressOf salesDebtRecord_PropertyChanged
            End If
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler Load, AddressOf Form_Load
            RemoveHandler FormClosing, AddressOf Form_Comparison_Edit_FormClosing
            RemoveHandler SimpleButton1.Click, AddressOf SimpleButton1_Click
            RemoveHandler cbo_Name.Validated, AddressOf Name_Validated
            RemoveHandler lk_CreditorType.EditValueChanged, AddressOf CreditorType_Changed
            RemoveHandler BarButtonItem_ClientPayout.ItemClick, AddressOf BarButtonItem_ClientPayout_ItemClick
            RemoveHandler BarButtonItem_AgencyPayout.ItemClick, AddressOf BarButtonItem_AgencyPayout_ItemClick
            RemoveHandler cb_Self_PaymentPercent.Validated, AddressOf cb_Self_PaymentPercent_Validated
            RemoveHandler cb_Self_PaymentPercent.EditValueChanging, AddressOf cb_Self_PaymentPercent_EditValueChanging
            RemoveHandler cb_Self_PaymentPercent.SelectedIndexChanged, AddressOf cb_Self_PaymentPercent_SelectedIndexChanged

            If salesDebtRecord IsNot Nothing Then
                RemoveHandler salesDebtRecord.PropertyChanged, AddressOf salesDebtRecord_PropertyChanged
            End If
        End Sub

        Private Sub Form_Comparison_Edit_FormClosing(ByVal sender As Object, ByVal e As FormClosingEventArgs)
            UnRegisterHandlers()
            SavePlacement("ClientUpdate.Comparison.Edit")

            Dim item As DebtPlus.LINQ.xpr_Sales_CreditorsResult = CType(cbo_Name.SelectedItem, DebtPlus.LINQ.xpr_Sales_CreditorsResult)
            If item IsNot Nothing Then
                salesDebtRecord.creditor_name = item.Name
            End If
        End Sub

        Private Sub Form_Load(ByVal sender As Object, ByVal e As EventArgs)
            LoadPlacement("ClientUpdate.Comparison.Edit")

            lk_CreditorType.DataBindings.Clear()
            tx_AccountNumber.DataBindings.Clear()
            clc_Balance.DataBindings.Clear()
            clc_Self_LateFees.DataBindings.Clear()
            clc_Self_OverlimitFees.DataBindings.Clear()
            clc_DMP_EnteredPayment.DataBindings.Clear()
            clc_DMP_InterestRate.DataBindings.Clear()
            clc_Self_InterestRate.DataBindings.Clear()

            UnRegisterHandlers()
            Try

                Dim SelectedValue As Int32 = -1
                Dim ie As IEnumerator = DebtPlus.LINQ.Cache.xpr_sales_creditor.getList().GetEnumerator
                Do While ie.MoveNext
                    Dim NamedCreditor As DebtPlus.LINQ.xpr_Sales_CreditorsResult = CType(ie.Current, DebtPlus.LINQ.xpr_Sales_CreditorsResult)
                    Dim NewIndex As Int32 = cbo_Name.Properties.Items.Add(NamedCreditor)
                    If String.Compare(NamedCreditor.Name, salesDebtRecord.creditor_name, True) = 0 Then
                        SelectedValue = NewIndex
                        Exit Do
                    End If
                Loop

                cbo_Name.SelectedIndex = SelectedValue
                If SelectedValue < 0 Then cbo_Name.Text = salesDebtRecord.creditor_name

                cb_Self_PaymentPercent.Text = FormatDebtPercent(salesDebtRecord.SELF_PaymentPercent)

                lk_CreditorType.DataBindings.Add("EditValue", salesDebtRecord, "creditor_type", False, DataSourceUpdateMode.OnPropertyChanged)
                tx_AccountNumber.DataBindings.Add("EditValue", salesDebtRecord, "account_number", False, DataSourceUpdateMode.OnPropertyChanged)
                clc_Balance.DataBindings.Add("EditValue", salesDebtRecord, "balance", False, DataSourceUpdateMode.OnPropertyChanged)
                clc_Self_LateFees.DataBindings.Add("EditValue", salesDebtRecord, "SELF_LateFees", False, DataSourceUpdateMode.OnPropertyChanged)
                clc_Self_OverlimitFees.DataBindings.Add("EditValue", salesDebtRecord, "SELF_OverLimitFees", False, DataSourceUpdateMode.OnPropertyChanged)
                clc_DMP_EnteredPayment.DataBindings.Add("EditValue", salesDebtRecord, "DMP_EnteredPayment", False, DataSourceUpdateMode.OnPropertyChanged)
                clc_DMP_InterestRate.DataBindings.Add("EditValue", salesDebtRecord, "DMP_InterestRate", False, DataSourceUpdateMode.OnPropertyChanged)
                clc_Self_InterestRate.DataBindings.Add("EditValue", salesDebtRecord, "SELF_InterestRate", False, DataSourceUpdateMode.OnPropertyChanged)

                ' Display the text fields and the policy matrix values
                ShowPolicy()
                DisplayTextFields()

            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Function ParseDebtPercent(ByVal str As String) As Double
            Dim dblPct As Double
            If Double.TryParse(str.Trim().TrimEnd("%"c), dblPct) Then
                While dblPct >= 1.0#
                    dblPct /= 100.0#
                End While
                Return dblPct
            End If
            Return 0.0#
        End Function

        Private Function FormatDebtPercent(ByVal obj As Object) As String
            If obj Is Nothing OrElse System.Object.Equals(obj, System.DBNull.Value) Then
                Return String.Empty
            End If
            Dim dblPct As Double = DirectCast(obj, Double)
            If dblPct = 0.0# Then Return "0%"
            Dim str As String = (dblPct * 100.0#).ToString().Trim("0"c).TrimEnd("."c) + "%"
            Return str
        End Function

        Private Sub Name_Validated(ByVal sender As Object, ByVal e As EventArgs)
            UnRegisterHandlers()
            Try
                salesDebtRecord.creditor_name = cbo_Name.Text.Trim()
                lk_CreditorType.EditValue = salesDebtRecord.creditor_type
                ShowPolicy()
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Sub CreditorType_Changed(ByVal sender As Object, ByVal e As EventArgs)
            UnRegisterHandlers()
            Try
                salesDebtRecord.creditor_type = Convert.ToString(lk_CreditorType.EditValue)
                ShowPolicy()
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Sub ShowPolicy()

            ' If there is a creditor that matches the item then show that policy matrix value
            Dim q As DebtPlus.LINQ.xpr_Sales_CreditorsResult = TryCast(cbo_Name.SelectedItem, DebtPlus.LINQ.xpr_Sales_CreditorsResult)
            If q IsNot Nothing Then
                RichEditControl1.RtfText = q.PolicyMatrix()
                Return
            End If

            RichEditControl1.Text = String.Empty
        End Sub

        Private Sub salesDebtRecord_PropertyChanged(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
            UnRegisterHandlers()
            Try
                DisplayTextFields()
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Sub DisplayTextFields()

            lb_DMP_InterestFees.Text = String.Format(STR_Currency, salesDebtRecord.DMP_InterestFees)
            lb_DMP_MinimumPayment.Text = String.Format(STR_Currency, salesDebtRecord.DMP_MinimumPayment)
            lb_DMP_MinimumProrate.Text = String.Format(STR_Percent, salesDebtRecord.DMP_MinimumProrate)
            lb_DMP_FinanceCharge.Text = String.Format(STR_Currency, salesDebtRecord.DMP_FinanceCharge)

            lb_DMP_GoingToPrincipal.Text = String.Format(STR_Currency, salesDebtRecord.DMP_GoingToPrincipal)

            ' Change the color if the value is unsuitable for paying off the debt
            If salesDebtRecord.DMP_GoingToPrincipal <= 0D AndAlso salesDebtRecord.Balance > 0D Then
                If Not lb_DMP_GoingToPrincipal.Font.Bold Then
                    lb_DMP_GoingToPrincipal.Font = New Font(lb_DMP_GoingToPrincipal.Font, FontStyle.Bold)
                    lb_DMP_GoingToPrincipal.BackColor = Color.Red
                    lb_DMP_GoingToPrincipal.ForeColor = Color.White
                End If
            Else
                If lb_DMP_GoingToPrincipal.Font.Bold Then
                    lb_DMP_GoingToPrincipal.Font = New Font(lb_DMP_GoingToPrincipal.Font, FontStyle.Regular)
                    lb_DMP_GoingToPrincipal.BackColor = lb_DMP_InterestFees.BackColor
                    lb_DMP_GoingToPrincipal.ForeColor = lb_DMP_InterestFees.ForeColor
                End If
            End If

            lb_Self_MonthlyFinanceCharge.Text = String.Format(STR_Currency, salesDebtRecord.Self_MonthlyFinanceCharge)
            lb_Self_InterestAndFees.Text = String.Format(STR_Currency, salesDebtRecord.Self_InterestAndFees)
            lb_Self_MonthlyPayment.Text = String.Format(STR_Currency, salesDebtRecord.Self_MonthlyPayment)

            lb_Self_GoingToPrincipal.Text = String.Format(STR_Currency, salesDebtRecord.Self_GoingToPrincipal)

            ' Change the color if the value is unsuitable for paying off the debt
            If salesDebtRecord.Self_GoingToPrincipal <= 0D AndAlso salesDebtRecord.Balance > 0D Then
                If Not lb_Self_GoingToPrincipal.Font.Bold Then
                    lb_Self_GoingToPrincipal.Font = New Font(lb_Self_GoingToPrincipal.Font, FontStyle.Bold)
                    lb_Self_GoingToPrincipal.BackColor = Color.Red
                    lb_Self_GoingToPrincipal.ForeColor = Color.White
                End If
            Else
                If lb_Self_GoingToPrincipal.Font.Bold Then
                    lb_Self_GoingToPrincipal.Font = New Font(lb_Self_GoingToPrincipal.Font, FontStyle.Regular)
                    lb_Self_GoingToPrincipal.BackColor = lb_DMP_InterestFees.BackColor
                    lb_Self_GoingToPrincipal.ForeColor = lb_DMP_InterestFees.ForeColor
                End If
            End If
        End Sub

        Private Sub SimpleButton1_Click(ByVal Sender As Object, ByVal e As EventArgs)
            DialogResult = DialogResult.OK
        End Sub

        Private Sub BarButtonItem_ClientPayout_ItemClick(sender As Object, e As ItemClickEventArgs)

            UnRegisterHandlers()

            ' Allocate a table to hold the data
            colPayout = New System.Collections.Generic.List(Of DebtPlus.LINQ.Events.SalesPayoutEventArgs)

            Try
                ' Calculate the payout now
                AddHandler salesDebtRecord.SelfPayout, AddressOf PayoutEvent
                Try
                    salesDebtRecord.SELF_CalculatePayout()
                Finally
                    RemoveHandler salesDebtRecord.SelfPayout, AddressOf PayoutEvent
                End Try

                ' Print the information that is now in the table
                Using rpt As New PayoutSchedule()
                    AddHandler rpt.GetTitleString, (Sub(o As Object, t As DebtPlus.Reports.Template.TitleStringEventArgs) t.Title = "Client Payout Schedule")
                    rpt.DataSource = colPayout
                    rpt.DisplayPreviewDialog()
                End Using

            Finally
                colPayout.Clear()
                colPayout = Nothing
                RegisterHandlers()
            End Try
        End Sub

        Private Sub BarButtonItem_AgencyPayout_ItemClick(sender As Object, e As ItemClickEventArgs)
            UnRegisterHandlers()

            ' Allocate a table to hold the data
            colPayout = New System.Collections.Generic.List(Of DebtPlus.LINQ.Events.SalesPayoutEventArgs)

            Try

                ' Calculate the payout now
                AddHandler salesDebtRecord.PlanPayout, AddressOf PayoutEvent
                Try
                    salesDebtRecord.DMP_CalculatePayout()
                Finally
                    RemoveHandler salesDebtRecord.PlanPayout, AddressOf PayoutEvent
                End Try

                ' Print the information that is now in the table
                Using rpt As New PayoutSchedule()
                    AddHandler rpt.GetTitleString, (Sub(o As Object, t As DebtPlus.Reports.Template.TitleStringEventArgs) t.Title = "PLAN Payout Schedule")
                    rpt.DataSource = colPayout
                    rpt.DisplayPreviewDialog()
                End Using

            Finally
                colPayout.Clear()
                colPayout = Nothing
                RegisterHandlers()
            End Try
        End Sub

        Private Sub PayoutEvent(ByVal sender As Object, ByVal e As DebtPlus.LINQ.Events.SalesPayoutEventArgs)
            colPayout.Add(e)
        End Sub

        Private Sub cb_Self_PaymentPercent_EditValueChanging(sender As Object, e As DevExpress.XtraEditors.Controls.ChangingEventArgs)
            If e.NewValue Is Nothing Then Return

            Dim strNewValue As String = Convert.ToString(e.NewValue)
            If String.IsNullOrEmpty(strNewValue) Then Return

            ' Check to ensure that the person put in a valid rate
            If (New System.Text.RegularExpressions.Regex("\d*(\.\d*)?%?")).IsMatch(strNewValue) Then Return

            ' The entry does not match the valid pattern. Reject it.
            e.Cancel = True
        End Sub

        Private Sub cb_Self_PaymentPercent_SelectedIndexChanged(sender As Object, e As System.EventArgs)
            Dim pct As Double = ParseDebtPercent(cb_Self_PaymentPercent.Text)
            salesDebtRecord.Self_PaymentPercent = pct
        End Sub

        Private Sub cb_Self_PaymentPercent_Validated(sender As Object, e As System.EventArgs)
            Dim pct As Double = ParseDebtPercent(cb_Self_PaymentPercent.Text)
            salesDebtRecord.Self_PaymentPercent = pct
            UnRegisterHandlers()
            Try
                cb_Self_PaymentPercent.Text = FormatDebtPercent(pct)
            Finally
                RegisterHandlers()
            End Try
        End Sub
    End Class
End Namespace
