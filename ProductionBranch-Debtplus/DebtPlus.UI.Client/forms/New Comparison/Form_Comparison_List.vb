#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.UI.Client.Service
Imports DevExpress.XtraTab
Imports DevExpress.XtraBars
Imports DevExpress.XtraPrinting.Localization
Imports System.Threading
Imports DebtPlus.UI.Client.forms.Comparison.Report
Imports DebtPlus.Reports.Template
Imports DebtPlus.LINQ
Imports System.Linq

Namespace forms.Comparison

    Friend Class Form_Comparison_List
        Private bc As BusinessContext = Nothing
        Private SalesFileRecord As sales_file = Nothing

        ''' <summary>
        ''' Is the current sales file read-only?
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private ReadOnly Property [ReadOnly] As Boolean
            Get
                If SalesFileRecord Is Nothing OrElse SalesFileRecord.date_exported.HasValue Then
                    Return True
                End If
                Return False
            End Get
        End Property

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            ' Process the initial loading for the "view note" function
            BarCheckItem_ViewNote.Checked = False
            LayoutControlItem_Note.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.OnlyInCustomization

            ' Process the initial loading for the tab page
            XtraTabControl1.SelectedTabPageIndex = 0
        End Sub

        Private Sub RegisterHandlers()
            AddHandler BarCheckItem_ViewNote.CheckedChanged, AddressOf BarCheckItem_ViewNote_CheckedChanged
            AddHandler Load, AddressOf Form_Comparison_List_Load
            AddHandler BarButtonItem_Print.ItemClick, AddressOf BarButtonItem_SaveAndPrint_ItemClick
            AddHandler BarButtonItem_Save.ItemClick, AddressOf BarButtonItem_Save_ItemClick
            AddHandler BarButtonItem_Export.ItemClick, AddressOf BarButtonItem_Export_ItemClick
            AddHandler BarButtonItem_Exit.ItemClick, AddressOf BarButtonItem_Exit_ItemClick
            AddHandler XtraTabControl1.SelectedPageChanging, AddressOf XtraTabControl1_SelectedPageChanging
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler BarCheckItem_ViewNote.CheckedChanged, AddressOf BarCheckItem_ViewNote_CheckedChanged
            RemoveHandler Load, AddressOf Form_Comparison_List_Load
            RemoveHandler BarButtonItem_Print.ItemClick, AddressOf BarButtonItem_SaveAndPrint_ItemClick
            RemoveHandler BarButtonItem_Save.ItemClick, AddressOf BarButtonItem_Save_ItemClick
            RemoveHandler BarButtonItem_Export.ItemClick, AddressOf BarButtonItem_Export_ItemClick
            RemoveHandler BarButtonItem_Exit.ItemClick, AddressOf BarButtonItem_Exit_ItemClick
            RemoveHandler XtraTabControl1.SelectedPageChanging, AddressOf XtraTabControl1_SelectedPageChanging
        End Sub

        Public Sub New(bc As BusinessContext, salesFileRecord As sales_file)
            MyClass.New()
            Me.bc = bc
            Me.SalesFileRecord = salesFileRecord
            RegisterHandlers()
        End Sub

        Private Sub Form_Comparison_List_Load(ByVal sender As Object, ByVal e As EventArgs)
            UnRegisterHandlers()
            Try

                ' Disable the save functions if the record is read-only
                BarButtonItem_Save.Enabled = Not [ReadOnly]
                BarButtonItem_Export.Enabled = Not [ReadOnly]

                ' Enable or disable the note edit accordingly
                TextEdit_Note.Properties.ReadOnly = [ReadOnly]
                TextEdit_Note.EditValue = SalesFileRecord.note

                ' Load the proper page
                LoadPage(XtraTabControl1.SelectedTabPageIndex)
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Sub XtraTabControl1_SelectedPageChanging(ByVal sender As Object, ByVal e As TabPageChangingEventArgs)
            LoadPage(XtraTabControl1.TabPages.IndexOf(e.Page))
        End Sub

        Private Sub LoadPage(ByVal Index As Int32)
            ' Process the page load sequence
            Select Case Index
                Case 0
                    Comparison_ClientControl1.Read(bc, SalesFileRecord)
                Case 1
                    Comparison_PlanControl1.Read(bc, SalesFileRecord)
                Case 2
                    Comparison_CompareControl1.Read(SalesFileRecord)
                Case Else
            End Select
        End Sub

        Private Sub BarButtonItem_SaveAndPrint_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            PrintInformation()
        End Sub

        Private Sub BarButtonItem_Save_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            SalesFileRecord.note = DebtPlus.Utils.Nulls.v_String(TextEdit_Note.EditValue)
            DialogResult = System.Windows.Forms.DialogResult.OK
        End Sub

        Private Sub BarButtonItem_Export_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            SalesFileRecord.note = DebtPlus.Utils.Nulls.v_String(TextEdit_Note.EditValue)
            DialogResult = System.Windows.Forms.DialogResult.Yes
        End Sub

        Private Sub BarButtonItem_Exit_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            SalesFileRecord.note = DebtPlus.Utils.Nulls.v_String(TextEdit_Note.EditValue)
            DialogResult = System.Windows.Forms.DialogResult.No
        End Sub

        Public Sub PrintInformation()
            Dim thrd As New System.Threading.Thread(New System.Threading.ParameterizedThreadStart(AddressOf ReportPrintThread))
            thrd.Name = "ComparisonPrintThread"
            thrd.IsBackground = True
            thrd.SetApartmentState(ApartmentState.STA)
            thrd.Start(Nothing)
        End Sub

        Private Sub ReportPrintThread(ByVal obj As Object)

            PreviewLocalizer.Active = New MyLocalizerClass()
            Using rpt As New Comparison_Report(SalesFileRecord)
                Using frm As New PrintPreviewForm(rpt)
                    frm.ShowDialog()
                End Using
            End Using
        End Sub

        Private Sub BarCheckItem_ViewNote_CheckedChanged(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs)
            LayoutControlItem_Note.Visibility = If(BarCheckItem_ViewNote.Checked, DevExpress.XtraLayout.Utils.LayoutVisibility.Always, DevExpress.XtraLayout.Utils.LayoutVisibility.OnlyInCustomization)
        End Sub

        ''' <summary>
        ''' Localization for the report printing to set the appropriate header text
        ''' </summary>
        Private Class MyLocalizerClass
            Inherits PreviewLocalizer

            Public Overrides Function GetLocalizedString(ByVal id As PreviewStringId) As String
                If id = PreviewStringId.PreviewForm_Caption Then
                    Return MyBase.GetLocalizedString(id) + " - Comparison Information"
                Else
                    Return MyBase.GetLocalizedString(id)
                End If
            End Function
        End Class
    End Class
End Namespace
