﻿Namespace forms.Comparison
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form_Comparison_Edit
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then
                        components.Dispose()
                    End If
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
            Me.cbo_Name = New DevExpress.XtraEditors.ComboBoxEdit()
            Me.lk_CreditorType = New DevExpress.XtraEditors.LookUpEdit()
            Me.tx_AccountNumber = New DevExpress.XtraEditors.TextEdit()
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
            Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
            Me.clc_Balance = New DevExpress.XtraEditors.CalcEdit()
            Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
            Me.clc_Self_InterestRate = New DebtPlus.Data.Controls.PercentEdit()
            Me.lb_Self_MonthlyPayment = New DevExpress.XtraEditors.LabelControl()
            Me.clc_Self_LateFees = New DevExpress.XtraEditors.CalcEdit()
            Me.clc_Self_OverlimitFees = New DevExpress.XtraEditors.CalcEdit()
            Me.lb_Self_MonthlyFinanceCharge = New DevExpress.XtraEditors.LabelControl()
            Me.lb_Self_InterestAndFees = New DevExpress.XtraEditors.LabelControl()
            Me.lb_Self_GoingToPrincipal = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
            Me.clc_DMP_InterestRate = New DebtPlus.Data.Controls.PercentEdit()
            Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
            Me.lb_DMP_MinimumProrate = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
            Me.lb_DMP_MinimumPayment = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
            Me.clc_DMP_EnteredPayment = New DevExpress.XtraEditors.CalcEdit()
            Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
            Me.lb_DMP_GoingToPrincipal = New DevExpress.XtraEditors.LabelControl()
            Me.lb_DMP_InterestFees = New DevExpress.XtraEditors.LabelControl()
            Me.lb_DMP_FinanceCharge = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl28 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl29 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl30 = New DevExpress.XtraEditors.LabelControl()
            Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
            Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
            Me.RichEditControl1 = New DevExpress.XtraRichEdit.RichEditControl()
            Me.barManager1 = New DevExpress.XtraBars.BarManager(Me.components)
            Me.Bar2 = New DevExpress.XtraBars.Bar()
            Me.BarSubItem1 = New DevExpress.XtraBars.BarSubItem()
            Me.BarButtonItem_ClientPayout = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_AgencyPayout = New DevExpress.XtraBars.BarButtonItem()
            Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
            Me.cb_Self_PaymentPercent = New DevExpress.XtraEditors.ComboBoxEdit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupControl1.SuspendLayout()
            CType(Me.cbo_Name.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lk_CreditorType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.tx_AccountNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.PanelControl1.SuspendLayout()
            CType(Me.clc_Balance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.clc_Self_InterestRate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.clc_Self_LateFees.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.clc_Self_OverlimitFees.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.clc_DMP_InterestRate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.clc_DMP_EnteredPayment.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.cb_Self_PaymentPercent.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GroupControl1
            '
            Me.GroupControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GroupControl1.Controls.Add(Me.cbo_Name)
            Me.GroupControl1.Controls.Add(Me.lk_CreditorType)
            Me.GroupControl1.Controls.Add(Me.tx_AccountNumber)
            Me.GroupControl1.Controls.Add(Me.LabelControl3)
            Me.GroupControl1.Controls.Add(Me.LabelControl2)
            Me.GroupControl1.Controls.Add(Me.LabelControl1)
            Me.GroupControl1.Location = New System.Drawing.Point(13, 37)
            Me.GroupControl1.Name = "GroupControl1"
            Me.GroupControl1.Size = New System.Drawing.Size(375, 100)
            Me.GroupControl1.TabIndex = 0
            Me.GroupControl1.Text = "Creditor Information"
            '
            'cbo_Name
            '
            Me.cbo_Name.Location = New System.Drawing.Point(112, 23)
            Me.cbo_Name.Name = "cbo_Name"
            Me.cbo_Name.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.cbo_Name.Properties.MaxLength = 50
            Me.cbo_Name.Properties.Sorted = True
            Me.cbo_Name.Size = New System.Drawing.Size(258, 20)
            Me.cbo_Name.TabIndex = 1
            Me.cbo_Name.ToolTip = "Name of the creditor for the debt"
            '
            'lk_CreditorType
            '
            Me.lk_CreditorType.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lk_CreditorType.Location = New System.Drawing.Point(112, 49)
            Me.lk_CreditorType.Name = "lk_CreditorType"
            Me.lk_CreditorType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lk_CreditorType.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 25, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 75, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near)})
            Me.lk_CreditorType.Properties.DisplayMember = "description"
            Me.lk_CreditorType.Properties.NullText = ""
            Me.lk_CreditorType.Properties.ShowFooter = False
            Me.lk_CreditorType.Properties.ShowHeader = False
            Me.lk_CreditorType.Properties.SortColumnIndex = 1
            Me.lk_CreditorType.Properties.ValueMember = "Id"
            Me.lk_CreditorType.Size = New System.Drawing.Size(258, 20)
            Me.lk_CreditorType.TabIndex = 3
            Me.lk_CreditorType.ToolTip = "What type of creditor is this debt?"
            '
            'tx_AccountNumber
            '
            Me.tx_AccountNumber.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.tx_AccountNumber.Location = New System.Drawing.Point(112, 75)
            Me.tx_AccountNumber.Name = "tx_AccountNumber"
            Me.tx_AccountNumber.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.tx_AccountNumber.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.tx_AccountNumber.Properties.MaxLength = 22
            Me.tx_AccountNumber.Size = New System.Drawing.Size(258, 20)
            Me.tx_AccountNumber.TabIndex = 5
            Me.tx_AccountNumber.ToolTip = "Current account number of the debt"
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(6, 78)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(79, 13)
            Me.LabelControl3.TabIndex = 4
            Me.LabelControl3.Text = "Account Number"
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(5, 52)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(66, 13)
            Me.LabelControl2.TabIndex = 2
            Me.LabelControl2.Text = "Creditor Type"
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(6, 26)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(27, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Name"
            '
            'PanelControl1
            '
            Me.PanelControl1.Controls.Add(Me.clc_Balance)
            Me.PanelControl1.Controls.Add(Me.LabelControl4)
            Me.PanelControl1.Location = New System.Drawing.Point(13, 144)
            Me.PanelControl1.Name = "PanelControl1"
            Me.PanelControl1.Size = New System.Drawing.Size(217, 33)
            Me.PanelControl1.TabIndex = 1
            '
            'clc_Balance
            '
            Me.clc_Balance.Location = New System.Drawing.Point(112, 5)
            Me.clc_Balance.Name = "clc_Balance"
            Me.clc_Balance.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.clc_Balance.Properties.DisplayFormat.FormatString = "{0:c}"
            Me.clc_Balance.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.clc_Balance.Properties.Precision = 2
            Me.clc_Balance.Size = New System.Drawing.Size(100, 20)
            Me.clc_Balance.TabIndex = 1
            Me.clc_Balance.ToolTip = "Current balance of the debt"
            '
            'LabelControl4
            '
            Me.LabelControl4.Location = New System.Drawing.Point(6, 8)
            Me.LabelControl4.Name = "LabelControl4"
            Me.LabelControl4.Size = New System.Drawing.Size(37, 13)
            Me.LabelControl4.TabIndex = 0
            Me.LabelControl4.Text = "Balance"
            '
            'LabelControl5
            '
            Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold)
            Me.LabelControl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.LabelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl5.Location = New System.Drawing.Point(18, 184)
            Me.LabelControl5.Name = "LabelControl5"
            Me.LabelControl5.Size = New System.Drawing.Size(207, 16)
            Me.LabelControl5.TabIndex = 2
            Me.LabelControl5.Text = "Client Is Currently Paying"
            '
            'LabelControl6
            '
            Me.LabelControl6.Location = New System.Drawing.Point(19, 210)
            Me.LabelControl6.Name = "LabelControl6"
            Me.LabelControl6.Size = New System.Drawing.Size(65, 13)
            Me.LabelControl6.TabIndex = 3
            Me.LabelControl6.Text = "Interest Rate"
            '
            'LabelControl7
            '
            Me.LabelControl7.Location = New System.Drawing.Point(19, 236)
            Me.LabelControl7.Name = "LabelControl7"
            Me.LabelControl7.Size = New System.Drawing.Size(82, 13)
            Me.LabelControl7.TabIndex = 5
            Me.LabelControl7.Text = "Payment Percent"
            '
            'LabelControl8
            '
            Me.LabelControl8.Location = New System.Drawing.Point(18, 263)
            Me.LabelControl8.Name = "LabelControl8"
            Me.LabelControl8.Size = New System.Drawing.Size(85, 13)
            Me.LabelControl8.TabIndex = 7
            Me.LabelControl8.Text = "Minimum Payment"
            Me.LabelControl8.UseMnemonic = False
            '
            'LabelControl9
            '
            Me.LabelControl9.Location = New System.Drawing.Point(18, 285)
            Me.LabelControl9.Name = "LabelControl9"
            Me.LabelControl9.Size = New System.Drawing.Size(47, 13)
            Me.LabelControl9.TabIndex = 9
            Me.LabelControl9.Text = "Late Fees"
            '
            'LabelControl10
            '
            Me.LabelControl10.Location = New System.Drawing.Point(18, 310)
            Me.LabelControl10.Name = "LabelControl10"
            Me.LabelControl10.Size = New System.Drawing.Size(68, 13)
            Me.LabelControl10.TabIndex = 11
            Me.LabelControl10.Text = "Overlimit Fees"
            '
            'LabelControl11
            '
            Me.LabelControl11.Location = New System.Drawing.Point(18, 348)
            Me.LabelControl11.Name = "LabelControl11"
            Me.LabelControl11.Size = New System.Drawing.Size(75, 13)
            Me.LabelControl11.TabIndex = 13
            Me.LabelControl11.Text = "Finance Charge"
            Me.LabelControl11.UseMnemonic = False
            '
            'LabelControl12
            '
            Me.LabelControl12.Location = New System.Drawing.Point(18, 367)
            Me.LabelControl12.Name = "LabelControl12"
            Me.LabelControl12.Size = New System.Drawing.Size(93, 13)
            Me.LabelControl12.TabIndex = 15
            Me.LabelControl12.Text = "Total Interest/Fees"
            Me.LabelControl12.UseMnemonic = False
            '
            'LabelControl13
            '
            Me.LabelControl13.Location = New System.Drawing.Point(18, 386)
            Me.LabelControl13.Name = "LabelControl13"
            Me.LabelControl13.Size = New System.Drawing.Size(91, 13)
            Me.LabelControl13.TabIndex = 17
            Me.LabelControl13.Text = "$ Going to Principal"
            Me.LabelControl13.UseMnemonic = False
            '
            'clc_Self_InterestRate
            '
            Me.clc_Self_InterestRate.Location = New System.Drawing.Point(125, 207)
            Me.clc_Self_InterestRate.Name = "clc_Self_InterestRate"
            Me.clc_Self_InterestRate.Properties.Allow100Percent = False
            Me.clc_Self_InterestRate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.clc_Self_InterestRate.Properties.DisplayFormat.FormatString = "{0:p3}"
            Me.clc_Self_InterestRate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.clc_Self_InterestRate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.clc_Self_InterestRate.Properties.Precision = 5
            Me.clc_Self_InterestRate.Size = New System.Drawing.Size(100, 20)
            Me.clc_Self_InterestRate.TabIndex = 4
            Me.clc_Self_InterestRate.ToolTip = "fixed, simple interest rate for the debt"
            '
            'lb_Self_MonthlyPayment
            '
            Me.lb_Self_MonthlyPayment.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lb_Self_MonthlyPayment.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lb_Self_MonthlyPayment.Location = New System.Drawing.Point(125, 263)
            Me.lb_Self_MonthlyPayment.Name = "lb_Self_MonthlyPayment"
            Me.lb_Self_MonthlyPayment.Size = New System.Drawing.Size(100, 13)
            Me.lb_Self_MonthlyPayment.TabIndex = 8
            Me.lb_Self_MonthlyPayment.Text = "$0.00"
            Me.lb_Self_MonthlyPayment.UseMnemonic = False
            '
            'clc_Self_LateFees
            '
            Me.clc_Self_LateFees.Location = New System.Drawing.Point(125, 282)
            Me.clc_Self_LateFees.Name = "clc_Self_LateFees"
            Me.clc_Self_LateFees.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.clc_Self_LateFees.Properties.DisplayFormat.FormatString = "{0:c}"
            Me.clc_Self_LateFees.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.clc_Self_LateFees.Properties.Precision = 2
            Me.clc_Self_LateFees.Size = New System.Drawing.Size(100, 20)
            Me.clc_Self_LateFees.TabIndex = 10
            Me.clc_Self_LateFees.ToolTip = "Dollar amount of any late fees"
            '
            'clc_Self_OverlimitFees
            '
            Me.clc_Self_OverlimitFees.Location = New System.Drawing.Point(125, 307)
            Me.clc_Self_OverlimitFees.Name = "clc_Self_OverlimitFees"
            Me.clc_Self_OverlimitFees.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.clc_Self_OverlimitFees.Properties.DisplayFormat.FormatString = "{0:c}"
            Me.clc_Self_OverlimitFees.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.clc_Self_OverlimitFees.Properties.Precision = 2
            Me.clc_Self_OverlimitFees.Size = New System.Drawing.Size(100, 20)
            Me.clc_Self_OverlimitFees.TabIndex = 12
            Me.clc_Self_OverlimitFees.ToolTip = "Dollar amount of any over-limit fees"
            '
            'lb_Self_MonthlyFinanceCharge
            '
            Me.lb_Self_MonthlyFinanceCharge.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lb_Self_MonthlyFinanceCharge.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lb_Self_MonthlyFinanceCharge.Location = New System.Drawing.Point(125, 348)
            Me.lb_Self_MonthlyFinanceCharge.Name = "lb_Self_MonthlyFinanceCharge"
            Me.lb_Self_MonthlyFinanceCharge.Size = New System.Drawing.Size(100, 13)
            Me.lb_Self_MonthlyFinanceCharge.TabIndex = 14
            Me.lb_Self_MonthlyFinanceCharge.Text = "$0.00"
            Me.lb_Self_MonthlyFinanceCharge.UseMnemonic = False
            '
            'lb_Self_InterestAndFees
            '
            Me.lb_Self_InterestAndFees.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lb_Self_InterestAndFees.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lb_Self_InterestAndFees.Location = New System.Drawing.Point(125, 367)
            Me.lb_Self_InterestAndFees.Name = "lb_Self_InterestAndFees"
            Me.lb_Self_InterestAndFees.Size = New System.Drawing.Size(100, 13)
            Me.lb_Self_InterestAndFees.TabIndex = 16
            Me.lb_Self_InterestAndFees.Text = "$0.00"
            Me.lb_Self_InterestAndFees.UseMnemonic = False
            '
            'lb_Self_GoingToPrincipal
            '
            Me.lb_Self_GoingToPrincipal.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lb_Self_GoingToPrincipal.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lb_Self_GoingToPrincipal.Location = New System.Drawing.Point(125, 386)
            Me.lb_Self_GoingToPrincipal.Name = "lb_Self_GoingToPrincipal"
            Me.lb_Self_GoingToPrincipal.Size = New System.Drawing.Size(100, 13)
            Me.lb_Self_GoingToPrincipal.TabIndex = 18
            Me.lb_Self_GoingToPrincipal.Text = "$0.00"
            Me.lb_Self_GoingToPrincipal.UseMnemonic = False
            '
            'LabelControl18
            '
            Me.LabelControl18.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl18.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold)
            Me.LabelControl18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.LabelControl18.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl18.Location = New System.Drawing.Point(248, 184)
            Me.LabelControl18.Name = "LabelControl18"
            Me.LabelControl18.Size = New System.Drawing.Size(246, 16)
            Me.LabelControl18.TabIndex = 19
            Me.LabelControl18.Text = "What the Agency Plan May Allow"
            '
            'clc_DMP_InterestRate
            '
            Me.clc_DMP_InterestRate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.clc_DMP_InterestRate.Location = New System.Drawing.Point(394, 207)
            Me.clc_DMP_InterestRate.Name = "clc_DMP_InterestRate"
            Me.clc_DMP_InterestRate.Properties.Allow100Percent = False
            Me.clc_DMP_InterestRate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.clc_DMP_InterestRate.Properties.DisplayFormat.FormatString = "{0:p3}"
            Me.clc_DMP_InterestRate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.clc_DMP_InterestRate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.clc_DMP_InterestRate.Properties.Precision = 5
            Me.clc_DMP_InterestRate.Size = New System.Drawing.Size(100, 20)
            Me.clc_DMP_InterestRate.TabIndex = 21
            Me.clc_DMP_InterestRate.ToolTip = "Interest rate when on the DMP Plan"
            '
            'LabelControl19
            '
            Me.LabelControl19.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl19.Location = New System.Drawing.Point(277, 210)
            Me.LabelControl19.Name = "LabelControl19"
            Me.LabelControl19.Size = New System.Drawing.Size(65, 13)
            Me.LabelControl19.TabIndex = 20
            Me.LabelControl19.Text = "Interest Rate"
            '
            'lb_DMP_MinimumProrate
            '
            Me.lb_DMP_MinimumProrate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lb_DMP_MinimumProrate.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lb_DMP_MinimumProrate.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lb_DMP_MinimumProrate.Location = New System.Drawing.Point(394, 236)
            Me.lb_DMP_MinimumProrate.Name = "lb_DMP_MinimumProrate"
            Me.lb_DMP_MinimumProrate.Size = New System.Drawing.Size(100, 13)
            Me.lb_DMP_MinimumProrate.TabIndex = 23
            Me.lb_DMP_MinimumProrate.Text = "00.000 %"
            Me.lb_DMP_MinimumProrate.UseMnemonic = False
            '
            'LabelControl21
            '
            Me.LabelControl21.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl21.Location = New System.Drawing.Point(277, 236)
            Me.LabelControl21.Name = "LabelControl21"
            Me.LabelControl21.Size = New System.Drawing.Size(79, 13)
            Me.LabelControl21.TabIndex = 22
            Me.LabelControl21.Text = "Minimum Prorate"
            Me.LabelControl21.UseMnemonic = False
            '
            'lb_DMP_MinimumPayment
            '
            Me.lb_DMP_MinimumPayment.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lb_DMP_MinimumPayment.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lb_DMP_MinimumPayment.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lb_DMP_MinimumPayment.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lb_DMP_MinimumPayment.Location = New System.Drawing.Point(394, 272)
            Me.lb_DMP_MinimumPayment.Name = "lb_DMP_MinimumPayment"
            Me.lb_DMP_MinimumPayment.Size = New System.Drawing.Size(100, 13)
            Me.lb_DMP_MinimumPayment.TabIndex = 25
            Me.lb_DMP_MinimumPayment.Text = "$0.00"
            Me.lb_DMP_MinimumPayment.UseMnemonic = False
            '
            'LabelControl23
            '
            Me.LabelControl23.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl23.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl23.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LabelControl23.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.LabelControl23.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl23.Location = New System.Drawing.Point(277, 259)
            Me.LabelControl23.Name = "LabelControl23"
            Me.LabelControl23.Size = New System.Drawing.Size(111, 35)
            Me.LabelControl23.TabIndex = 24
            Me.LabelControl23.Text = "Calculated   Minimum Payment"
            Me.LabelControl23.UseMnemonic = False
            '
            'clc_DMP_EnteredPayment
            '
            Me.clc_DMP_EnteredPayment.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.clc_DMP_EnteredPayment.Location = New System.Drawing.Point(394, 307)
            Me.clc_DMP_EnteredPayment.Name = "clc_DMP_EnteredPayment"
            Me.clc_DMP_EnteredPayment.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.clc_DMP_EnteredPayment.Properties.DisplayFormat.FormatString = "{0:c}"
            Me.clc_DMP_EnteredPayment.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.clc_DMP_EnteredPayment.Properties.Precision = 2
            Me.clc_DMP_EnteredPayment.Size = New System.Drawing.Size(100, 20)
            Me.clc_DMP_EnteredPayment.TabIndex = 27
            Me.clc_DMP_EnteredPayment.ToolTip = "Expected deposit amount to pay off this debt"
            '
            'LabelControl24
            '
            Me.LabelControl24.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl24.Location = New System.Drawing.Point(277, 310)
            Me.LabelControl24.Name = "LabelControl24"
            Me.LabelControl24.Size = New System.Drawing.Size(65, 13)
            Me.LabelControl24.TabIndex = 26
            Me.LabelControl24.Text = "Plan Payment"
            '
            'lb_DMP_GoingToPrincipal
            '
            Me.lb_DMP_GoingToPrincipal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lb_DMP_GoingToPrincipal.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lb_DMP_GoingToPrincipal.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lb_DMP_GoingToPrincipal.Location = New System.Drawing.Point(394, 386)
            Me.lb_DMP_GoingToPrincipal.Name = "lb_DMP_GoingToPrincipal"
            Me.lb_DMP_GoingToPrincipal.Size = New System.Drawing.Size(100, 13)
            Me.lb_DMP_GoingToPrincipal.TabIndex = 33
            Me.lb_DMP_GoingToPrincipal.Text = "$0.00"
            Me.lb_DMP_GoingToPrincipal.UseMnemonic = False
            '
            'lb_DMP_InterestFees
            '
            Me.lb_DMP_InterestFees.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lb_DMP_InterestFees.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lb_DMP_InterestFees.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lb_DMP_InterestFees.Location = New System.Drawing.Point(394, 367)
            Me.lb_DMP_InterestFees.Name = "lb_DMP_InterestFees"
            Me.lb_DMP_InterestFees.Size = New System.Drawing.Size(100, 13)
            Me.lb_DMP_InterestFees.TabIndex = 31
            Me.lb_DMP_InterestFees.Text = "$0.00"
            Me.lb_DMP_InterestFees.UseMnemonic = False
            '
            'lb_DMP_FinanceCharge
            '
            Me.lb_DMP_FinanceCharge.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lb_DMP_FinanceCharge.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lb_DMP_FinanceCharge.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lb_DMP_FinanceCharge.Location = New System.Drawing.Point(394, 348)
            Me.lb_DMP_FinanceCharge.Name = "lb_DMP_FinanceCharge"
            Me.lb_DMP_FinanceCharge.Size = New System.Drawing.Size(100, 13)
            Me.lb_DMP_FinanceCharge.TabIndex = 29
            Me.lb_DMP_FinanceCharge.Text = "$0.00"
            Me.lb_DMP_FinanceCharge.UseMnemonic = False
            '
            'LabelControl28
            '
            Me.LabelControl28.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl28.Location = New System.Drawing.Point(277, 386)
            Me.LabelControl28.Name = "LabelControl28"
            Me.LabelControl28.Size = New System.Drawing.Size(91, 13)
            Me.LabelControl28.TabIndex = 32
            Me.LabelControl28.Text = "$ Going to Principal"
            Me.LabelControl28.UseMnemonic = False
            '
            'LabelControl29
            '
            Me.LabelControl29.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl29.Location = New System.Drawing.Point(277, 367)
            Me.LabelControl29.Name = "LabelControl29"
            Me.LabelControl29.Size = New System.Drawing.Size(93, 13)
            Me.LabelControl29.TabIndex = 30
            Me.LabelControl29.Text = "Total Interest/Fees"
            Me.LabelControl29.UseMnemonic = False
            '
            'LabelControl30
            '
            Me.LabelControl30.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl30.Location = New System.Drawing.Point(277, 348)
            Me.LabelControl30.Name = "LabelControl30"
            Me.LabelControl30.Size = New System.Drawing.Size(75, 13)
            Me.LabelControl30.TabIndex = 28
            Me.LabelControl30.Text = "Finance Charge"
            Me.LabelControl30.UseMnemonic = False
            '
            'SimpleButton1
            '
            Me.SimpleButton1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton1.Location = New System.Drawing.Point(419, 37)
            Me.SimpleButton1.Name = "SimpleButton1"
            Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton1.TabIndex = 35
            Me.SimpleButton1.Text = "&OK"
            '
            'SimpleButton2
            '
            Me.SimpleButton2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.SimpleButton2.Location = New System.Drawing.Point(419, 79)
            Me.SimpleButton2.Name = "SimpleButton2"
            Me.SimpleButton2.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton2.TabIndex = 36
            Me.SimpleButton2.Text = "&Cancel"
            '
            'RichEditControl1
            '
            Me.RichEditControl1.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple
            Me.RichEditControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.RichEditControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D
            Me.RichEditControl1.Location = New System.Drawing.Point(12, 416)
            Me.RichEditControl1.MenuManager = Me.barManager1
            Me.RichEditControl1.Name = "RichEditControl1"
            Me.RichEditControl1.Options.VerticalScrollbar.Visibility = DevExpress.XtraRichEdit.RichEditScrollbarVisibility.Visible
            Me.RichEditControl1.Size = New System.Drawing.Size(482, 119)
            Me.RichEditControl1.TabIndex = 37
            Me.RichEditControl1.Views.SimpleView.BackColor = System.Drawing.SystemColors.ControlLight
            '
            'barManager1
            '
            Me.barManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar2})
            Me.barManager1.DockControls.Add(Me.barDockControlTop)
            Me.barManager1.DockControls.Add(Me.barDockControlBottom)
            Me.barManager1.DockControls.Add(Me.barDockControlLeft)
            Me.barManager1.DockControls.Add(Me.barDockControlRight)
            Me.barManager1.Form = Me
            Me.barManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarSubItem1, Me.BarButtonItem_ClientPayout, Me.BarButtonItem_AgencyPayout})
            Me.barManager1.MainMenu = Me.Bar2
            Me.barManager1.MaxItemId = 3
            '
            'Bar2
            '
            Me.Bar2.BarName = "Main menu"
            Me.Bar2.DockCol = 0
            Me.Bar2.DockRow = 0
            Me.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
            Me.Bar2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem1)})
            Me.Bar2.OptionsBar.MultiLine = True
            Me.Bar2.OptionsBar.UseWholeRow = True
            Me.Bar2.Text = "Main menu"
            '
            'BarSubItem1
            '
            Me.BarSubItem1.Caption = "&View"
            Me.BarSubItem1.Id = 0
            Me.BarSubItem1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_ClientPayout), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_AgencyPayout)})
            Me.BarSubItem1.Name = "BarSubItem1"
            '
            'BarButtonItem_ClientPayout
            '
            Me.BarButtonItem_ClientPayout.Caption = "Client Payout Schedule..."
            Me.BarButtonItem_ClientPayout.Id = 1
            Me.BarButtonItem_ClientPayout.Name = "BarButtonItem_ClientPayout"
            '
            'BarButtonItem_AgencyPayout
            '
            Me.BarButtonItem_AgencyPayout.Caption = "Agency Payout Schedule..."
            Me.BarButtonItem_AgencyPayout.Id = 2
            Me.BarButtonItem_AgencyPayout.Name = "BarButtonItem_AgencyPayout"
            '
            'barDockControlTop
            '
            Me.barDockControlTop.CausesValidation = False
            Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
            Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
            Me.barDockControlTop.Size = New System.Drawing.Size(506, 22)
            '
            'barDockControlBottom
            '
            Me.barDockControlBottom.CausesValidation = False
            Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
            Me.barDockControlBottom.Location = New System.Drawing.Point(0, 547)
            Me.barDockControlBottom.Size = New System.Drawing.Size(506, 0)
            '
            'barDockControlLeft
            '
            Me.barDockControlLeft.CausesValidation = False
            Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
            Me.barDockControlLeft.Location = New System.Drawing.Point(0, 22)
            Me.barDockControlLeft.Size = New System.Drawing.Size(0, 525)
            '
            'barDockControlRight
            '
            Me.barDockControlRight.CausesValidation = False
            Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
            Me.barDockControlRight.Location = New System.Drawing.Point(506, 22)
            Me.barDockControlRight.Size = New System.Drawing.Size(0, 525)
            '
            'cb_Self_PaymentPercent
            '
            Me.cb_Self_PaymentPercent.Location = New System.Drawing.Point(125, 233)
            Me.cb_Self_PaymentPercent.Name = "cb_Self_PaymentPercent"
            Me.cb_Self_PaymentPercent.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.cb_Self_PaymentPercent.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.cb_Self_PaymentPercent.Properties.PopupSizeable = True
            Me.cb_Self_PaymentPercent.Size = New System.Drawing.Size(100, 20)
            Me.cb_Self_PaymentPercent.TabIndex = 6
            Me.cb_Self_PaymentPercent.ToolTip = "Rate at which the debt is to be paid"
            '
            'Form_Comparison_Edit
            '
            Me.AcceptButton = Me.SimpleButton1
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.SimpleButton2
            Me.ClientSize = New System.Drawing.Size(506, 547)
            Me.Controls.Add(Me.SimpleButton2)
            Me.Controls.Add(Me.SimpleButton1)
            Me.Controls.Add(Me.lb_DMP_GoingToPrincipal)
            Me.Controls.Add(Me.lb_DMP_InterestFees)
            Me.Controls.Add(Me.lb_DMP_FinanceCharge)
            Me.Controls.Add(Me.LabelControl28)
            Me.Controls.Add(Me.LabelControl29)
            Me.Controls.Add(Me.LabelControl30)
            Me.Controls.Add(Me.clc_DMP_EnteredPayment)
            Me.Controls.Add(Me.LabelControl24)
            Me.Controls.Add(Me.lb_DMP_MinimumPayment)
            Me.Controls.Add(Me.LabelControl23)
            Me.Controls.Add(Me.lb_DMP_MinimumProrate)
            Me.Controls.Add(Me.LabelControl21)
            Me.Controls.Add(Me.clc_DMP_InterestRate)
            Me.Controls.Add(Me.LabelControl19)
            Me.Controls.Add(Me.LabelControl18)
            Me.Controls.Add(Me.lb_Self_GoingToPrincipal)
            Me.Controls.Add(Me.lb_Self_InterestAndFees)
            Me.Controls.Add(Me.lb_Self_MonthlyFinanceCharge)
            Me.Controls.Add(Me.clc_Self_OverlimitFees)
            Me.Controls.Add(Me.clc_Self_LateFees)
            Me.Controls.Add(Me.lb_Self_MonthlyPayment)
            Me.Controls.Add(Me.clc_Self_InterestRate)
            Me.Controls.Add(Me.LabelControl13)
            Me.Controls.Add(Me.LabelControl12)
            Me.Controls.Add(Me.LabelControl11)
            Me.Controls.Add(Me.LabelControl10)
            Me.Controls.Add(Me.LabelControl9)
            Me.Controls.Add(Me.LabelControl8)
            Me.Controls.Add(Me.LabelControl7)
            Me.Controls.Add(Me.LabelControl6)
            Me.Controls.Add(Me.LabelControl5)
            Me.Controls.Add(Me.PanelControl1)
            Me.Controls.Add(Me.GroupControl1)
            Me.Controls.Add(Me.RichEditControl1)
            Me.Controls.Add(Me.cb_Self_PaymentPercent)
            Me.Controls.Add(Me.barDockControlLeft)
            Me.Controls.Add(Me.barDockControlRight)
            Me.Controls.Add(Me.barDockControlBottom)
            Me.Controls.Add(Me.barDockControlTop)
            Me.Name = "Form_Comparison_Edit"
            Me.Text = "Information about the debt"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupControl1.ResumeLayout(False)
            Me.GroupControl1.PerformLayout()
            CType(Me.cbo_Name.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lk_CreditorType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.tx_AccountNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.PanelControl1.ResumeLayout(False)
            Me.PanelControl1.PerformLayout()
            CType(Me.clc_Balance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.clc_Self_InterestRate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.clc_Self_LateFees.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.clc_Self_OverlimitFees.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.clc_DMP_InterestRate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.clc_DMP_EnteredPayment.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.cb_Self_PaymentPercent.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

        Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
        Friend WithEvents lk_CreditorType As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents tx_AccountNumber As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
        Friend WithEvents clc_Balance As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents clc_Self_InterestRate As DebtPlus.Data.Controls.PercentEdit
        Friend WithEvents lb_Self_MonthlyPayment As DevExpress.XtraEditors.LabelControl
        Friend WithEvents clc_Self_LateFees As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents clc_Self_OverlimitFees As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents lb_Self_MonthlyFinanceCharge As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lb_Self_InterestAndFees As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lb_Self_GoingToPrincipal As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents clc_DMP_InterestRate As DebtPlus.Data.Controls.PercentEdit
        Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lb_DMP_MinimumProrate As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lb_DMP_MinimumPayment As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents clc_DMP_EnteredPayment As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lb_DMP_GoingToPrincipal As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lb_DMP_InterestFees As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lb_DMP_FinanceCharge As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl28 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl29 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl30 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents cbo_Name As DevExpress.XtraEditors.ComboBoxEdit
        Friend WithEvents RichEditControl1 As DevExpress.XtraRichEdit.RichEditControl
        Friend WithEvents barManager1 As DevExpress.XtraBars.BarManager
        Friend WithEvents Bar2 As DevExpress.XtraBars.Bar
        Friend WithEvents BarSubItem1 As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarButtonItem_ClientPayout As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_AgencyPayout As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
        Friend WithEvents cb_Self_PaymentPercent As DevExpress.XtraEditors.ComboBoxEdit
    End Class
End Namespace