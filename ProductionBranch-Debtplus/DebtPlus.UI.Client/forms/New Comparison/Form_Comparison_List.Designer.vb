﻿Namespace forms.Comparison

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form_Comparison_List
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then
                        components.Dispose()
                    End If
                End If

            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Me.barManager1 = New DevExpress.XtraBars.BarManager(Me.components)
            Me.Bar2 = New DevExpress.XtraBars.Bar()
            Me.BarSubItem1 = New DevExpress.XtraBars.BarSubItem()
            Me.BarButtonItem_Print = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Export = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Save = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Exit = New DevExpress.XtraBars.BarButtonItem()
            Me.BarSubItem2 = New DevExpress.XtraBars.BarSubItem()
            Me.BarCheckItem_ViewNote = New DevExpress.XtraBars.BarCheckItem()
            Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
            Me.BarButtonItem_SaveAndExit = New DevExpress.XtraBars.BarButtonItem()
            Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
            Me.XtraTabPage_Client = New DevExpress.XtraTab.XtraTabPage()
            Me.Comparison_ClientControl1 = New DebtPlus.UI.Client.forms.Comparison.Comparison_ClientControl()
            Me.XtraTabPage_Agency = New DevExpress.XtraTab.XtraTabPage()
            Me.Comparison_PlanControl1 = New DebtPlus.UI.Client.forms.Comparison.Comparison_PlanControl()
            Me.XtraTabPage_Comparison = New DevExpress.XtraTab.XtraTabPage()
            Me.Comparison_CompareControl1 = New DebtPlus.UI.Client.forms.Comparison.Comparison_CompareControl()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.TextEdit_Note = New DevExpress.XtraEditors.TextEdit()
            Me.LayoutControlItem_Note = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.XtraTabControl1.SuspendLayout()
            Me.XtraTabPage_Client.SuspendLayout()
            Me.XtraTabPage_Agency.SuspendLayout()
            Me.XtraTabPage_Comparison.SuspendLayout()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.TextEdit_Note.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_Note, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'DefaultLookAndFeel1
            '
            Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "Caramel"
            '
            'barManager1
            '
            Me.barManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar2})
            Me.barManager1.DockControls.Add(Me.barDockControlTop)
            Me.barManager1.DockControls.Add(Me.barDockControlBottom)
            Me.barManager1.DockControls.Add(Me.barDockControlLeft)
            Me.barManager1.DockControls.Add(Me.barDockControlRight)
            Me.barManager1.Form = Me
            Me.barManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarSubItem1, Me.BarButtonItem_Print, Me.BarButtonItem_Export, Me.BarButtonItem_Save, Me.BarButtonItem_SaveAndExit, Me.BarButtonItem_Exit, Me.BarSubItem2, Me.BarCheckItem_ViewNote})
            Me.barManager1.MainMenu = Me.Bar2
            Me.barManager1.MaxItemId = 8
            '
            'Bar2
            '
            Me.Bar2.BarName = "Main menu"
            Me.Bar2.DockCol = 0
            Me.Bar2.DockRow = 0
            Me.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
            Me.Bar2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem2)})
            Me.Bar2.OptionsBar.MultiLine = True
            Me.Bar2.OptionsBar.UseWholeRow = True
            Me.Bar2.Text = "Main menu"
            '
            'BarSubItem1
            '
            Me.BarSubItem1.Caption = "&File"
            Me.BarSubItem1.Id = 0
            Me.BarSubItem1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Print), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Export, True), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Save), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Exit)})
            Me.BarSubItem1.Name = "BarSubItem1"
            '
            'BarButtonItem_Print
            '
            Me.BarButtonItem_Print.Caption = "Print..."
            Me.BarButtonItem_Print.Id = 1
            Me.BarButtonItem_Print.Name = "BarButtonItem_Print"
            '
            'BarButtonItem_Export
            '
            Me.BarButtonItem_Export.Caption = "&Export"
            Me.BarButtonItem_Export.Id = 2
            Me.BarButtonItem_Export.Name = "BarButtonItem_Export"
            '
            'BarButtonItem_Save
            '
            Me.BarButtonItem_Save.Caption = "Save"
            Me.BarButtonItem_Save.Id = 3
            Me.BarButtonItem_Save.Name = "BarButtonItem_Save"
            '
            'BarButtonItem_Exit
            '
            Me.BarButtonItem_Exit.Caption = "Exit"
            Me.BarButtonItem_Exit.Id = 5
            Me.BarButtonItem_Exit.Name = "BarButtonItem_Exit"
            '
            'BarSubItem2
            '
            Me.BarSubItem2.Caption = "&View"
            Me.BarSubItem2.Id = 6
            Me.BarSubItem2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Caption, Me.BarCheckItem_ViewNote, "&View Note")})
            Me.BarSubItem2.Name = "BarSubItem2"
            '
            'BarCheckItem_ViewNote
            '
            Me.BarCheckItem_ViewNote.Caption = "&View Note"
            Me.BarCheckItem_ViewNote.Id = 7
            Me.BarCheckItem_ViewNote.Name = "BarCheckItem_ViewNote"
            '
            'barDockControlTop
            '
            Me.barDockControlTop.CausesValidation = False
            Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
            Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
            Me.barDockControlTop.Size = New System.Drawing.Size(627, 24)
            '
            'barDockControlBottom
            '
            Me.barDockControlBottom.CausesValidation = False
            Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
            Me.barDockControlBottom.Location = New System.Drawing.Point(0, 367)
            Me.barDockControlBottom.Size = New System.Drawing.Size(627, 0)
            '
            'barDockControlLeft
            '
            Me.barDockControlLeft.CausesValidation = False
            Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
            Me.barDockControlLeft.Location = New System.Drawing.Point(0, 24)
            Me.barDockControlLeft.Size = New System.Drawing.Size(0, 343)
            '
            'barDockControlRight
            '
            Me.barDockControlRight.CausesValidation = False
            Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
            Me.barDockControlRight.Location = New System.Drawing.Point(627, 24)
            Me.barDockControlRight.Size = New System.Drawing.Size(0, 343)
            '
            'BarButtonItem_SaveAndExit
            '
            Me.BarButtonItem_SaveAndExit.Caption = "Save And Exit"
            Me.BarButtonItem_SaveAndExit.Id = 4
            Me.BarButtonItem_SaveAndExit.Name = "BarButtonItem_SaveAndExit"
            '
            'XtraTabControl1
            '
            Me.XtraTabControl1.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.[True]
            Me.XtraTabControl1.Location = New System.Drawing.Point(2, 2)
            Me.XtraTabControl1.Name = "XtraTabControl1"
            Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage_Client
            Me.XtraTabControl1.Size = New System.Drawing.Size(623, 315)
            Me.XtraTabControl1.TabIndex = 5
            Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage_Client, Me.XtraTabPage_Agency, Me.XtraTabPage_Comparison})
            '
            'XtraTabPage_Client
            '
            Me.XtraTabPage_Client.Appearance.PageClient.BackColor = System.Drawing.Color.Silver
            Me.XtraTabPage_Client.Appearance.PageClient.Options.UseBackColor = True
            Me.XtraTabPage_Client.Controls.Add(Me.Comparison_ClientControl1)
            Me.XtraTabPage_Client.Name = "XtraTabPage_Client"
            Me.XtraTabPage_Client.Size = New System.Drawing.Size(616, 287)
            Me.XtraTabPage_Client.Text = "Client"
            '
            'Comparison_ClientControl1
            '
            Me.Comparison_ClientControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.Comparison_ClientControl1.Location = New System.Drawing.Point(0, 0)
            Me.Comparison_ClientControl1.Name = "Comparison_ClientControl1"
            Me.Comparison_ClientControl1.Size = New System.Drawing.Size(616, 287)
            Me.Comparison_ClientControl1.TabIndex = 0
            '
            'XtraTabPage_Agency
            '
            Me.XtraTabPage_Agency.Controls.Add(Me.Comparison_PlanControl1)
            Me.XtraTabPage_Agency.Name = "XtraTabPage_Agency"
            Me.XtraTabPage_Agency.Size = New System.Drawing.Size(616, 311)
            Me.XtraTabPage_Agency.Text = "Agency"
            '
            'Comparison_PlanControl1
            '
            Me.Comparison_PlanControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.Comparison_PlanControl1.Location = New System.Drawing.Point(0, 0)
            Me.Comparison_PlanControl1.Name = "Comparison_PlanControl1"
            Me.Comparison_PlanControl1.Size = New System.Drawing.Size(616, 311)
            Me.Comparison_PlanControl1.TabIndex = 0
            '
            'XtraTabPage_Comparison
            '
            Me.XtraTabPage_Comparison.Controls.Add(Me.Comparison_CompareControl1)
            Me.XtraTabPage_Comparison.Name = "XtraTabPage_Comparison"
            Me.XtraTabPage_Comparison.Size = New System.Drawing.Size(616, 311)
            Me.XtraTabPage_Comparison.Text = "Comparison"
            '
            'Comparison_CompareControl1
            '
            Me.Comparison_CompareControl1.Appearance.BackColor = System.Drawing.SystemColors.Window
            Me.Comparison_CompareControl1.Appearance.Options.UseBackColor = True
            Me.Comparison_CompareControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.Comparison_CompareControl1.Location = New System.Drawing.Point(0, 0)
            Me.Comparison_CompareControl1.Name = "Comparison_CompareControl1"
            Me.Comparison_CompareControl1.Size = New System.Drawing.Size(616, 311)
            Me.Comparison_CompareControl1.TabIndex = 0
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.TextEdit_Note)
            Me.LayoutControl1.Controls.Add(Me.XtraTabControl1)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(397, 210, 250, 350)
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(627, 343)
            Me.LayoutControl1.TabIndex = 10
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'TextEdit_Note
            '
            Me.TextEdit_Note.Location = New System.Drawing.Point(28, 321)
            Me.TextEdit_Note.MenuManager = Me.barManager1
            Me.TextEdit_Note.Name = "TextEdit_Note"
            Me.TextEdit_Note.Properties.MaxLength = 50
            Me.TextEdit_Note.Size = New System.Drawing.Size(597, 20)
            Me.TextEdit_Note.StyleController = Me.LayoutControl1
            Me.TextEdit_Note.TabIndex = 6
            '
            'LayoutControlItem_Note
            '
            Me.LayoutControlItem_Note.Control = Me.TextEdit_Note
            Me.LayoutControlItem_Note.CustomizationFormText = "Note"
            Me.LayoutControlItem_Note.Location = New System.Drawing.Point(0, 319)
            Me.LayoutControlItem_Note.Name = "LayoutControlItem_Note"
            Me.LayoutControlItem_Note.Size = New System.Drawing.Size(627, 24)
            Me.LayoutControlItem_Note.Text = "Note"
            Me.LayoutControlItem_Note.TextSize = New System.Drawing.Size(23, 13)
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup1.GroupBordersVisible = False
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem_Note})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(627, 343)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.XtraTabControl1
            Me.LayoutControlItem1.CustomizationFormText = "Sales Debts"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(627, 319)
            Me.LayoutControlItem1.Text = "Sales Debts"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem1.TextToControlDistance = 0
            Me.LayoutControlItem1.TextVisible = False
            '
            'Form_Comparison_List
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(627, 367)
            Me.Controls.Add(Me.LayoutControl1)
            Me.Controls.Add(Me.barDockControlLeft)
            Me.Controls.Add(Me.barDockControlRight)
            Me.Controls.Add(Me.barDockControlBottom)
            Me.Controls.Add(Me.barDockControlTop)
            Me.Name = "Form_Comparison_List"
            Me.Text = "Comparison Information"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.XtraTabControl1.ResumeLayout(False)
            Me.XtraTabPage_Client.ResumeLayout(False)
            Me.XtraTabPage_Agency.ResumeLayout(False)
            Me.XtraTabPage_Comparison.ResumeLayout(False)
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.TextEdit_Note.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_Note, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents barManager1 As DevExpress.XtraBars.BarManager
        Friend WithEvents Bar2 As DevExpress.XtraBars.Bar
        Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
        Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
        Friend WithEvents XtraTabPage_Client As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents Comparison_ClientControl1 As Comparison_ClientControl
        Friend WithEvents XtraTabPage_Agency As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents Comparison_PlanControl1 As Comparison_PlanControl
        Friend WithEvents XtraTabPage_Comparison As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents Comparison_CompareControl1 As Comparison_CompareControl
        Friend WithEvents BarSubItem1 As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarButtonItem_Print As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Export As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Save As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_SaveAndExit As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Exit As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents TextEdit_Note As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem_Note As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents BarSubItem2 As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarCheckItem_ViewNote As DevExpress.XtraBars.BarCheckItem
    End Class
End Namespace