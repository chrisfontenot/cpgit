﻿Namespace forms.Comparison
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Comparison_GridControl
        Inherits DevExpress.XtraEditors.XtraUserControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer = Nothing

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.AddMenu = New System.Windows.Forms.MenuItem()
            Me.EditMenu = New System.Windows.Forms.MenuItem()
            Me.DeleteMenu = New System.Windows.Forms.MenuItem()
            Me.LocalMenu = New System.Windows.Forms.ContextMenu()
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'AddMenu
            '
            Me.AddMenu.Index = 0
            Me.AddMenu.Text = "Add..."
            '
            'EditMenu
            '
            Me.EditMenu.Index = 1
            Me.EditMenu.Text = "Edit..."
            '
            'DeleteMenu
            '
            Me.DeleteMenu.Index = 2
            Me.DeleteMenu.Text = "Delete"
            '
            'LocalMenu
            '
            Me.LocalMenu.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.AddMenu, Me.EditMenu, Me.DeleteMenu})
            '
            'GridControl1
            '
            Me.GridControl1.ContextMenu = Me.LocalMenu
            Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.GridControl1.Location = New System.Drawing.Point(0, 0)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(527, 212)
            Me.GridControl1.TabIndex = 0
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsView.ShowFooter = True
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            '
            'Comparison_GridControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.GridControl1)
            Me.Name = "Comparison_GridControl"
            Me.Size = New System.Drawing.Size(527, 212)
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

        Protected WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Protected WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Protected WithEvents AddMenu As System.Windows.Forms.MenuItem
        Protected WithEvents EditMenu As System.Windows.Forms.MenuItem
        Protected WithEvents DeleteMenu As System.Windows.Forms.MenuItem
        Protected WithEvents LocalMenu As System.Windows.Forms.ContextMenu
    End Class
End Namespace
