#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.UI.Client.Service
Imports DevExpress.XtraEditors
Imports DebtPlus.LINQ
Imports System.Linq

Namespace forms.Comparison
    Friend Class Comparison_CompareControl

        Private ReadOnly CurrencyFormat As String = "{0:c}"
        Private ReadOnly PercentFormat As String = "{0:p3}"
        Private ReadOnly IntegerFormat As String = "{0:n0}"
        Private ReadOnly YearsFormat As String = "{0:f1}"

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler LineControl1.Paint, AddressOf LineControl_Paint
            AddHandler LineControl2.Paint, AddressOf LineControl_Paint
            AddHandler LineControl3.Paint, AddressOf LineControl_Paint
            AddHandler LineControl4.Paint, AddressOf LineControl_Paint
            AddHandler LineControl5.Paint, AddressOf LineControl_Paint
            AddHandler clc_ExtraDMPAmount.EditValueChanging, AddressOf clc_ExtraDMPAmount_EditValueChanging
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler LineControl1.Paint, AddressOf LineControl_Paint
            RemoveHandler LineControl2.Paint, AddressOf LineControl_Paint
            RemoveHandler LineControl3.Paint, AddressOf LineControl_Paint
            RemoveHandler LineControl4.Paint, AddressOf LineControl_Paint
            RemoveHandler LineControl5.Paint, AddressOf LineControl_Paint
            RemoveHandler clc_ExtraDMPAmount.EditValueChanging, AddressOf clc_ExtraDMPAmount_EditValueChanging
        End Sub

        ' Current record being edited
        Private record As sales_file

        Private ReadOnly Property [ReadOnly] As Boolean
            Get
                Return record Is Nothing OrElse record.date_exported.HasValue
            End Get
        End Property

        Public Sub Read(record As sales_file)
            Me.record = record

            UnRegisterHandlers()

            ' Set the read-only status on the edit control to prohibit changes if exported
            clc_ExtraDMPAmount.Properties.ReadOnly = [ReadOnly]

            Try
                clc_ExtraDMPAmount.EditValue = record.extra_amount
                UpdateFields()
            Finally
                RegisterHandlers()
            End Try

        End Sub

        Private Sub LineControl_Paint(ByVal sender As Object, ByVal e As PaintEventArgs)
            With CType(sender, PanelControl)
                Dim g As Graphics = e.Graphics

                ' Fill the rectangle with the background color
                Using br As New SolidBrush(.Appearance.BackColor)
                    g.FillRectangle(br, e.ClipRectangle)

                    ' Draw the line
                    Using pn As New Pen(.Appearance.ForeColor, 2)
                        g.DrawLine(pn, New Point(0, 0), New Point(e.ClipRectangle.Width, 0))
                    End Using
                End Using
            End With
        End Sub

        Private Sub clc_ExtraDMPAmount_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            UpdateFields()
        End Sub

        Private Sub UpdateFields()

            ' Find the client's plan length
            Dim PlanLength As Int32 = Convert.ToInt32(System.Math.Round(record.SELF_LengthMonths, 0))
            If PlanLength < 1.0 Then
                lbl_Self_LengthMonths.Text = "---"
                lbl_Self_LengthYears.Text = "---"
            Else
                lbl_Self_LengthMonths.Text = String.Format(IntegerFormat, PlanLength)
                lbl_Self_LengthYears.Text = String.Format(YearsFormat, Convert.ToDouble(PlanLength) / 12.0#)
            End If

            ' Find the plan's plan length
            PlanLength = Convert.ToInt32(System.Math.Round(record.DMP_LengthMonths, 0))
            If PlanLength < 1.0 Then
                lbl_DMP_LengthMonths.Text = "---"
                lbl_DMP_LengthYears.Text = "---"
            Else
                lbl_DMP_LengthMonths.Text = String.Format(IntegerFormat, PlanLength)
                lbl_DMP_LengthYears.Text = String.Format(YearsFormat, Convert.ToDouble(PlanLength) / 12.0#)
            End If

            lbl_Self_TotalPayoutPayment_1.Text = String.Format(CurrencyFormat, record.SELF_TotalPayoutPayment)
            lbl_Self_TotalPayoutPayment_2.Text = String.Format(CurrencyFormat, record.SELF_TotalPayoutPayment)
            lbl_Self_TotalFinanceCharge.Text = String.Format(CurrencyFormat, record.SELF_TotalFinanceCharge)
            lbl_Self_TotalInterestRate_1.Text = String.Format(PercentFormat, System.Math.Round(record.SELF_TotalInterestRate, 3))
            lbl_Self_TotalInterestRate_2.Text = String.Format(PercentFormat, System.Math.Round(record.SELF_TotalInterestRate, 3))
            lbl_Self_TotalInterestAndFees.Text = String.Format(CurrencyFormat, record.SELF_TotalInterestAndFees)
            lbl_Self_TotalGoingToPrincipal.Text = String.Format(CurrencyFormat, record.SELF_TotalGoingToPrincipal)
            lbl_Self_TotalPercentGoingToPrincipal.Text = String.Format(PercentFormat, record.SELF_TotalPercentGoingToPrincipal)
            lbl_Self_TotalBalance.Text = String.Format(CurrencyFormat, record.TotalBalance)
            lbl_Self_TotalInterestPaid.Text = String.Format(CurrencyFormat, record.SELF_TotalInterestPaid)
            lbl_Self_TotalPaid.Text = String.Format(CurrencyFormat, record.SELF_TotalPaid)
            lbl_Self_LateOverlimitFeeSavings.Text = String.Format(CurrencyFormat, record.SELF_LateOverlimitFeeSavings)

            lbl_DMP_TotalEnteredPayment_1.Text = String.Format(CurrencyFormat, record.DMP_TotalEnteredPayment)
            lbl_DMP_TotalFinanceCharge.Text = String.Format(CurrencyFormat, record.DMP_TotalFinanceCharge)
            lbl_DMP_TotalInterestRate_1.Text = String.Format(PercentFormat, System.Math.Round(record.DMP_TotalInterestRate, 3))
            lbl_DMP_TotalInterestFees.Text = String.Format(CurrencyFormat, record.DMP_TotalInterestFees)
            lbl_DMP_TotalGoingToPrincipal.Text = String.Format(CurrencyFormat, record.DMP_TotalGoingToPrincipal)
            lbl_DMP_TotalPercentGoingToprincipal.Text = String.Format(PercentFormat, record.DMP_TotalPercentGoingToprincipal)
            lbl_DMP_TotalBalance.Text = String.Format(CurrencyFormat, record.TotalBalance)
            lbl_DMP_TotalInterestPaid.Text = String.Format(CurrencyFormat, record.DMP_TotalInterestPaid)
            lbl_DMP_TotalAmountPaid.Text = String.Format(CurrencyFormat, record.DMP_TotalAmountPaid)
            lbl_DMP_TotalPayment.Text = String.Format(CurrencyFormat, record.DMP_TotalPayment)
            lbl_DMP_TotalEnteredPayment_2.Text = String.Format(CurrencyFormat, record.DMP_TotalEnteredPayment)
            lbl_DMP_TotalInterestRate_2.Text = String.Format(PercentFormat, System.Math.Round(record.DMP_TotalInterestRate, 3))

            lbl_MonthlySavingsPayments.Text = String.Format(CurrencyFormat, record.MonthlySavings_Payments)
            lbl_MonthlySavingsFinanceCharge.Text = String.Format(CurrencyFormat, record.MonthlySavings_FinanceCharge)
            lbl_MonthlySavingsEstimatedInterestRate.Text = String.Format(PercentFormat, record.MonthlySavings_EstimatedInterestRate)
            lbl_MonthlySavingsTotalInterestAndFees.Text = String.Format(CurrencyFormat, record.MonthlySavings_TotalInterestAndFees)
            lbl_Savings.Text = String.Format(CurrencyFormat, record.Savings)
            lbl_InterestSavings.Text = String.Format(CurrencyFormat, record.SELF_TotalInterestPaid - record.DMP_TotalInterestPaid)
        End Sub

        Private Sub clc_ExtraDMPAmount_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)

            If Not [ReadOnly] Then
                Dim NewValue As System.Nullable(Of Decimal) = DebtPlus.Utils.Nulls.v_Decimal(e.NewValue)
                If NewValue.HasValue AndAlso NewValue.Value >= 0D Then
                    record.extra_amount = NewValue.Value
                    UpdateFields()
                    Return
                End If
            End If

            e.Cancel = True
        End Sub
    End Class
End Namespace