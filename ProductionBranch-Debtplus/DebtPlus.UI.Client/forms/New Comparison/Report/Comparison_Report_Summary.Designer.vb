Namespace forms.Comparison.Report
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Comparison_Report_Summary
        Inherits DevExpress.XtraReports.UI.XtraReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Comparison_Report_Summary))
            Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
            Me.XrLabel50 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_DMP_TotalPayment = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel44 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel41 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Savings = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_DMP_TotalAmountPaid = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Self_TotalPaid = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_DMP_LengthMonths = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Self_LengthMonths = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_DMP_ExtraAmount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_DMP_TotalInterestPaid = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel54 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_DMP_LengthYears = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Self_LengthYears = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_DMP_TotalInterestRate_2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Self_TotalInterestRate_2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_DMP_TotalEnteredPayment_2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Self_TotalPayoutPayment_2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_DMP_TotalBalance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Self_TotalBalance = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel22 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Self_TotalInterestPaid = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel40 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_DMP_TotalPercentGoingToprincipal = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Self_TotalPercentGoingToPrincipal = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_DMP_TotalGoingToPrincipal = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Self_TotalGoingToPrincipal = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_MonthlySavings_TotalInterestAndFees = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_DMP_TotalInterestFees = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Self_TotalInterestAndFees = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_MonthlySavings_EstimatedInterestRate = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_DMP_TotalInterestRate_1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Self_TotalInterestRate_1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_MonthlySavings_FinanceCharge = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_DMP_TotalFinanceCharge = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Self_TotalFinanceCharge = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_MonthlySavings_Payments = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_DMP_TotalEnteredPayment_1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_Self_TotalPayoutPayment_1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPanel2 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel16 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel17 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel18 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel19 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel20 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel21 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
            Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
            Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrPanel3 = New DevExpress.XtraReports.UI.XRPanel()
            Me.XrLabel23 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
            Me.calc_Self_LengthMonths = New DevExpress.XtraReports.UI.CalculatedField()
            Me.calc_DMP_LengthMonths = New DevExpress.XtraReports.UI.CalculatedField()
            Me.calc_Self_LengthYears = New DevExpress.XtraReports.UI.CalculatedField()
            Me.calc_DMP_LengthYears = New DevExpress.XtraReports.UI.CalculatedField()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel50, Me.XrLabel_DMP_TotalPayment, Me.XrLabel44, Me.XrLabel41, Me.XrLabel_Savings, Me.XrLabel_DMP_TotalAmountPaid, Me.XrLabel_Self_TotalPaid, Me.XrLabel_DMP_LengthMonths, Me.XrLabel_Self_LengthMonths, Me.XrLabel_DMP_ExtraAmount, Me.XrLabel_DMP_TotalInterestPaid, Me.XrLabel54, Me.XrLabel_DMP_LengthYears, Me.XrLabel_Self_LengthYears, Me.XrLabel_DMP_TotalInterestRate_2, Me.XrLabel_Self_TotalInterestRate_2, Me.XrLabel_DMP_TotalEnteredPayment_2, Me.XrLabel_Self_TotalPayoutPayment_2, Me.XrLabel_DMP_TotalBalance, Me.XrLabel_Self_TotalBalance, Me.XrLabel22, Me.XrLabel_Self_TotalInterestPaid, Me.XrLine1, Me.XrLabel40, Me.XrLabel_DMP_TotalPercentGoingToprincipal, Me.XrLabel_Self_TotalPercentGoingToPrincipal, Me.XrLabel_DMP_TotalGoingToPrincipal, Me.XrLabel_Self_TotalGoingToPrincipal, Me.XrLabel_MonthlySavings_TotalInterestAndFees, Me.XrLabel_DMP_TotalInterestFees, Me.XrLabel_Self_TotalInterestAndFees, Me.XrLabel_MonthlySavings_EstimatedInterestRate, Me.XrLabel_DMP_TotalInterestRate_1, Me.XrLabel_Self_TotalInterestRate_1, Me.XrLabel_MonthlySavings_FinanceCharge, Me.XrLabel_DMP_TotalFinanceCharge, Me.XrLabel_Self_TotalFinanceCharge, Me.XrLabel_MonthlySavings_Payments, Me.XrLabel_DMP_TotalEnteredPayment_1, Me.XrLabel_Self_TotalPayoutPayment_1, Me.XrLabel5, Me.XrLabel4, Me.XrLabel3, Me.XrPanel2, Me.XrPanel1, Me.XrLabel2})
            Me.Detail.HeightF = 374.0!
            Me.Detail.Name = "Detail"
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel50
            '
            Me.XrLabel50.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel50.LocationFloat = New DevExpress.Utils.PointFloat(633.0!, 354.1667!)
            Me.XrLabel50.Name = "XrLabel50"
            Me.XrLabel50.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel50.SizeF = New System.Drawing.SizeF(217.0!, 17.0!)
            Me.XrLabel50.StylePriority.UseFont = False
            Me.XrLabel50.Text = "AN APPROXIMATE SAVINGS OF"
            '
            'XrLabel_DMP_TotalPayment
            '
            Me.XrLabel_DMP_TotalPayment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "DMP_TotalPayment", "{0:c}")})
            Me.XrLabel_DMP_TotalPayment.LocationFloat = New DevExpress.Utils.PointFloat(254.0!, 354.1667!)
            Me.XrLabel_DMP_TotalPayment.Name = "XrLabel_DMP_TotalPayment"
            Me.XrLabel_DMP_TotalPayment.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_DMP_TotalPayment.SizeF = New System.Drawing.SizeF(120.0!, 17.0!)
            Me.XrLabel_DMP_TotalPayment.StylePriority.UseTextAlignment = False
            Me.XrLabel_DMP_TotalPayment.Text = "$0.00"
            Me.XrLabel_DMP_TotalPayment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel44
            '
            Me.XrLabel44.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 329.1667!)
            Me.XrLabel44.Name = "XrLabel44"
            Me.XrLabel44.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel44.SizeF = New System.Drawing.SizeF(253.0!, 17.0!)
            Me.XrLabel44.StylePriority.UseTextAlignment = False
            Me.XrLabel44.Text = "+ EXTRA PAYMENT $ AMOUNT"
            Me.XrLabel44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            '
            'XrLabel41
            '
            Me.XrLabel41.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel41.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 354.1667!)
            Me.XrLabel41.Name = "XrLabel41"
            Me.XrLabel41.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel41.SizeF = New System.Drawing.SizeF(250.0!, 17.0!)
            Me.XrLabel41.StylePriority.UseFont = False
            Me.XrLabel41.Text = "TOTAL PLAN DEPOSIT AMOUNT"
            '
            'XrLabel_Savings
            '
            Me.XrLabel_Savings.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Savings", "{0:c}")})
            Me.XrLabel_Savings.LocationFloat = New DevExpress.Utils.PointFloat(855.0!, 354.1667!)
            Me.XrLabel_Savings.Name = "XrLabel_Savings"
            Me.XrLabel_Savings.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Savings.SizeF = New System.Drawing.SizeF(170.0!, 17.0!)
            Me.XrLabel_Savings.StylePriority.UseTextAlignment = False
            Me.XrLabel_Savings.Text = "$0.00"
            Me.XrLabel_Savings.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_DMP_TotalAmountPaid
            '
            Me.XrLabel_DMP_TotalAmountPaid.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "DMP_TotalAmountPaid", "{0:c}")})
            Me.XrLabel_DMP_TotalAmountPaid.LocationFloat = New DevExpress.Utils.PointFloat(889.0!, 304.1667!)
            Me.XrLabel_DMP_TotalAmountPaid.Name = "XrLabel_DMP_TotalAmountPaid"
            Me.XrLabel_DMP_TotalAmountPaid.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_DMP_TotalAmountPaid.SizeF = New System.Drawing.SizeF(136.0!, 17.0!)
            Me.XrLabel_DMP_TotalAmountPaid.StylePriority.UseTextAlignment = False
            Me.XrLabel_DMP_TotalAmountPaid.Text = "$0.00"
            Me.XrLabel_DMP_TotalAmountPaid.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Self_TotalPaid
            '
            Me.XrLabel_Self_TotalPaid.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Self_TotalPaid", "{0:c}")})
            Me.XrLabel_Self_TotalPaid.LocationFloat = New DevExpress.Utils.PointFloat(889.0!, 279.1667!)
            Me.XrLabel_Self_TotalPaid.Name = "XrLabel_Self_TotalPaid"
            Me.XrLabel_Self_TotalPaid.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Self_TotalPaid.SizeF = New System.Drawing.SizeF(136.0!, 17.0!)
            Me.XrLabel_Self_TotalPaid.StylePriority.UseTextAlignment = False
            Me.XrLabel_Self_TotalPaid.Text = "$0.00"
            Me.XrLabel_Self_TotalPaid.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_DMP_LengthMonths
            '
            Me.XrLabel_DMP_LengthMonths.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "calc_DMP_LengthMonths")})
            Me.XrLabel_DMP_LengthMonths.LocationFloat = New DevExpress.Utils.PointFloat(541.0!, 304.1667!)
            Me.XrLabel_DMP_LengthMonths.Name = "XrLabel_DMP_LengthMonths"
            Me.XrLabel_DMP_LengthMonths.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_DMP_LengthMonths.SizeF = New System.Drawing.SizeF(83.0!, 17.0!)
            Me.XrLabel_DMP_LengthMonths.StylePriority.UseTextAlignment = False
            Me.XrLabel_DMP_LengthMonths.Text = "0"
            Me.XrLabel_DMP_LengthMonths.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Self_LengthMonths
            '
            Me.XrLabel_Self_LengthMonths.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "calc_Self_LengthMonths")})
            Me.XrLabel_Self_LengthMonths.LocationFloat = New DevExpress.Utils.PointFloat(541.0!, 279.1667!)
            Me.XrLabel_Self_LengthMonths.Name = "XrLabel_Self_LengthMonths"
            Me.XrLabel_Self_LengthMonths.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Self_LengthMonths.SizeF = New System.Drawing.SizeF(83.0!, 17.0!)
            Me.XrLabel_Self_LengthMonths.StylePriority.UseTextAlignment = False
            Me.XrLabel_Self_LengthMonths.Text = "0"
            Me.XrLabel_Self_LengthMonths.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_DMP_ExtraAmount
            '
            Me.XrLabel_DMP_ExtraAmount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "DMP_ExtraAmount", "{0:c}")})
            Me.XrLabel_DMP_ExtraAmount.LocationFloat = New DevExpress.Utils.PointFloat(254.0!, 329.1667!)
            Me.XrLabel_DMP_ExtraAmount.Name = "XrLabel_DMP_ExtraAmount"
            Me.XrLabel_DMP_ExtraAmount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_DMP_ExtraAmount.SizeF = New System.Drawing.SizeF(120.0!, 17.0!)
            Me.XrLabel_DMP_ExtraAmount.StylePriority.UseTextAlignment = False
            Me.XrLabel_DMP_ExtraAmount.Text = "$0.00"
            Me.XrLabel_DMP_ExtraAmount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_DMP_TotalInterestPaid
            '
            Me.XrLabel_DMP_TotalInterestPaid.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "DMP_TotalInterestPaid", "{0:c}")})
            Me.XrLabel_DMP_TotalInterestPaid.LocationFloat = New DevExpress.Utils.PointFloat(732.0!, 304.1667!)
            Me.XrLabel_DMP_TotalInterestPaid.Name = "XrLabel_DMP_TotalInterestPaid"
            Me.XrLabel_DMP_TotalInterestPaid.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_DMP_TotalInterestPaid.SizeF = New System.Drawing.SizeF(153.0!, 17.0!)
            Me.XrLabel_DMP_TotalInterestPaid.StylePriority.UseTextAlignment = False
            Me.XrLabel_DMP_TotalInterestPaid.Text = "$0.00"
            Me.XrLabel_DMP_TotalInterestPaid.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel54
            '
            Me.XrLabel54.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel54.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 279.1667!)
            Me.XrLabel54.Name = "XrLabel54"
            Me.XrLabel54.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel54.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
            Me.XrLabel54.StylePriority.UseFont = False
            Me.XrLabel54.Text = "Client"
            '
            'XrLabel_DMP_LengthYears
            '
            Me.XrLabel_DMP_LengthYears.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "calc_DMP_LengthYears")})
            Me.XrLabel_DMP_LengthYears.LocationFloat = New DevExpress.Utils.PointFloat(641.0!, 304.1667!)
            Me.XrLabel_DMP_LengthYears.Name = "XrLabel_DMP_LengthYears"
            Me.XrLabel_DMP_LengthYears.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_DMP_LengthYears.SizeF = New System.Drawing.SizeF(83.0!, 17.0!)
            Me.XrLabel_DMP_LengthYears.StylePriority.UseTextAlignment = False
            Me.XrLabel_DMP_LengthYears.Text = "0.00"
            Me.XrLabel_DMP_LengthYears.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Self_LengthYears
            '
            Me.XrLabel_Self_LengthYears.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "calc_Self_LengthYears")})
            Me.XrLabel_Self_LengthYears.LocationFloat = New DevExpress.Utils.PointFloat(641.0!, 279.1667!)
            Me.XrLabel_Self_LengthYears.Name = "XrLabel_Self_LengthYears"
            Me.XrLabel_Self_LengthYears.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Self_LengthYears.SizeF = New System.Drawing.SizeF(83.0!, 17.0!)
            Me.XrLabel_Self_LengthYears.StylePriority.UseTextAlignment = False
            Me.XrLabel_Self_LengthYears.Text = "0.00"
            Me.XrLabel_Self_LengthYears.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_DMP_TotalInterestRate_2
            '
            Me.XrLabel_DMP_TotalInterestRate_2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "DMP_TotalInterestRate", "{0:p3}")})
            Me.XrLabel_DMP_TotalInterestRate_2.LocationFloat = New DevExpress.Utils.PointFloat(424.0!, 304.1667!)
            Me.XrLabel_DMP_TotalInterestRate_2.Name = "XrLabel_DMP_TotalInterestRate_2"
            Me.XrLabel_DMP_TotalInterestRate_2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_DMP_TotalInterestRate_2.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
            Me.XrLabel_DMP_TotalInterestRate_2.StylePriority.UseTextAlignment = False
            Me.XrLabel_DMP_TotalInterestRate_2.Text = "0.000 %"
            Me.XrLabel_DMP_TotalInterestRate_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Self_TotalInterestRate_2
            '
            Me.XrLabel_Self_TotalInterestRate_2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Self_TotalInterestRate", "{0:p3}")})
            Me.XrLabel_Self_TotalInterestRate_2.LocationFloat = New DevExpress.Utils.PointFloat(424.0!, 279.1667!)
            Me.XrLabel_Self_TotalInterestRate_2.Name = "XrLabel_Self_TotalInterestRate_2"
            Me.XrLabel_Self_TotalInterestRate_2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Self_TotalInterestRate_2.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
            Me.XrLabel_Self_TotalInterestRate_2.StylePriority.UseTextAlignment = False
            Me.XrLabel_Self_TotalInterestRate_2.Text = "0.000 %"
            Me.XrLabel_Self_TotalInterestRate_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_DMP_TotalEnteredPayment_2
            '
            Me.XrLabel_DMP_TotalEnteredPayment_2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "DMP_TotalEnteredPayment", "{0:c}")})
            Me.XrLabel_DMP_TotalEnteredPayment_2.LocationFloat = New DevExpress.Utils.PointFloat(258.0!, 304.1667!)
            Me.XrLabel_DMP_TotalEnteredPayment_2.Name = "XrLabel_DMP_TotalEnteredPayment_2"
            Me.XrLabel_DMP_TotalEnteredPayment_2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_DMP_TotalEnteredPayment_2.SizeF = New System.Drawing.SizeF(116.0!, 17.0!)
            Me.XrLabel_DMP_TotalEnteredPayment_2.StylePriority.UseTextAlignment = False
            Me.XrLabel_DMP_TotalEnteredPayment_2.Text = "$0.00"
            Me.XrLabel_DMP_TotalEnteredPayment_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Self_TotalPayoutPayment_2
            '
            Me.XrLabel_Self_TotalPayoutPayment_2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Self_TotalPayoutPayment", "{0:c}")})
            Me.XrLabel_Self_TotalPayoutPayment_2.LocationFloat = New DevExpress.Utils.PointFloat(258.0!, 279.1667!)
            Me.XrLabel_Self_TotalPayoutPayment_2.Name = "XrLabel_Self_TotalPayoutPayment_2"
            Me.XrLabel_Self_TotalPayoutPayment_2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Self_TotalPayoutPayment_2.SizeF = New System.Drawing.SizeF(116.0!, 17.0!)
            Me.XrLabel_Self_TotalPayoutPayment_2.StylePriority.UseTextAlignment = False
            Me.XrLabel_Self_TotalPayoutPayment_2.Text = "$0.00"
            Me.XrLabel_Self_TotalPayoutPayment_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_DMP_TotalBalance
            '
            Me.XrLabel_DMP_TotalBalance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "TotalBalance", "{0:c}")})
            Me.XrLabel_DMP_TotalBalance.LocationFloat = New DevExpress.Utils.PointFloat(149.0!, 304.1667!)
            Me.XrLabel_DMP_TotalBalance.Name = "XrLabel_DMP_TotalBalance"
            Me.XrLabel_DMP_TotalBalance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_DMP_TotalBalance.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
            Me.XrLabel_DMP_TotalBalance.StylePriority.UseTextAlignment = False
            Me.XrLabel_DMP_TotalBalance.Text = "$0.00"
            Me.XrLabel_DMP_TotalBalance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Self_TotalBalance
            '
            Me.XrLabel_Self_TotalBalance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "TotalBalance", "{0:c}")})
            Me.XrLabel_Self_TotalBalance.LocationFloat = New DevExpress.Utils.PointFloat(149.0!, 279.1667!)
            Me.XrLabel_Self_TotalBalance.Name = "XrLabel_Self_TotalBalance"
            Me.XrLabel_Self_TotalBalance.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Self_TotalBalance.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
            Me.XrLabel_Self_TotalBalance.StylePriority.UseTextAlignment = False
            Me.XrLabel_Self_TotalBalance.Text = "$0.00"
            Me.XrLabel_Self_TotalBalance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel22
            '
            Me.XrLabel22.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel22.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 304.1667!)
            Me.XrLabel22.Name = "XrLabel22"
            Me.XrLabel22.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel22.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
            Me.XrLabel22.StylePriority.UseFont = False
            Me.XrLabel22.Text = "PLAN"
            '
            'XrLabel_Self_TotalInterestPaid
            '
            Me.XrLabel_Self_TotalInterestPaid.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Self_TotalInterestPaid", "{0:c}")})
            Me.XrLabel_Self_TotalInterestPaid.LocationFloat = New DevExpress.Utils.PointFloat(732.0!, 279.1667!)
            Me.XrLabel_Self_TotalInterestPaid.Name = "XrLabel_Self_TotalInterestPaid"
            Me.XrLabel_Self_TotalInterestPaid.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Self_TotalInterestPaid.SizeF = New System.Drawing.SizeF(153.0!, 17.0!)
            Me.XrLabel_Self_TotalInterestPaid.StylePriority.UseTextAlignment = False
            Me.XrLabel_Self_TotalInterestPaid.Text = "$0.00"
            Me.XrLabel_Self_TotalInterestPaid.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLine1
            '
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 171.0694!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(1050.0!, 9.0!)
            '
            'XrLabel40
            '
            Me.XrLabel40.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel40.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 193.5139!)
            Me.XrLabel40.Name = "XrLabel40"
            Me.XrLabel40.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel40.SizeF = New System.Drawing.SizeF(1050.0!, 25.0!)
            Me.XrLabel40.StylePriority.UseFont = False
            Me.XrLabel40.StylePriority.UseTextAlignment = False
            Me.XrLabel40.Text = "POTENTIAL TOTAL SAVINGS *see note*"
            Me.XrLabel40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel_DMP_TotalPercentGoingToprincipal
            '
            Me.XrLabel_DMP_TotalPercentGoingToprincipal.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "DMP_TotalPercentGoingToprincipal", "{0:p3}")})
            Me.XrLabel_DMP_TotalPercentGoingToprincipal.LocationFloat = New DevExpress.Utils.PointFloat(925.0!, 115.625!)
            Me.XrLabel_DMP_TotalPercentGoingToprincipal.Name = "XrLabel_DMP_TotalPercentGoingToprincipal"
            Me.XrLabel_DMP_TotalPercentGoingToprincipal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_DMP_TotalPercentGoingToprincipal.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
            Me.XrLabel_DMP_TotalPercentGoingToprincipal.StylePriority.UseTextAlignment = False
            Me.XrLabel_DMP_TotalPercentGoingToprincipal.Text = "0.000 %"
            Me.XrLabel_DMP_TotalPercentGoingToprincipal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Self_TotalPercentGoingToPrincipal
            '
            Me.XrLabel_Self_TotalPercentGoingToPrincipal.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Self_TotalPercentGoingToPrincipal", "{0:p3}")})
            Me.XrLabel_Self_TotalPercentGoingToPrincipal.LocationFloat = New DevExpress.Utils.PointFloat(925.0!, 90.625!)
            Me.XrLabel_Self_TotalPercentGoingToPrincipal.Name = "XrLabel_Self_TotalPercentGoingToPrincipal"
            Me.XrLabel_Self_TotalPercentGoingToPrincipal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Self_TotalPercentGoingToPrincipal.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
            Me.XrLabel_Self_TotalPercentGoingToPrincipal.StylePriority.UseTextAlignment = False
            Me.XrLabel_Self_TotalPercentGoingToPrincipal.Text = "0.000 %"
            Me.XrLabel_Self_TotalPercentGoingToPrincipal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_DMP_TotalGoingToPrincipal
            '
            Me.XrLabel_DMP_TotalGoingToPrincipal.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "DMP_TotalGoingToPrincipal", "{0:c}")})
            Me.XrLabel_DMP_TotalGoingToPrincipal.LocationFloat = New DevExpress.Utils.PointFloat(733.0001!, 115.625!)
            Me.XrLabel_DMP_TotalGoingToPrincipal.Name = "XrLabel_DMP_TotalGoingToPrincipal"
            Me.XrLabel_DMP_TotalGoingToPrincipal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_DMP_TotalGoingToPrincipal.SizeF = New System.Drawing.SizeF(153.0!, 17.0!)
            Me.XrLabel_DMP_TotalGoingToPrincipal.StylePriority.UseTextAlignment = False
            Me.XrLabel_DMP_TotalGoingToPrincipal.Text = "$0.00"
            Me.XrLabel_DMP_TotalGoingToPrincipal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Self_TotalGoingToPrincipal
            '
            Me.XrLabel_Self_TotalGoingToPrincipal.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Self_TotalGoingToPrincipal", "{0:c}")})
            Me.XrLabel_Self_TotalGoingToPrincipal.LocationFloat = New DevExpress.Utils.PointFloat(733.0001!, 90.625!)
            Me.XrLabel_Self_TotalGoingToPrincipal.Name = "XrLabel_Self_TotalGoingToPrincipal"
            Me.XrLabel_Self_TotalGoingToPrincipal.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Self_TotalGoingToPrincipal.SizeF = New System.Drawing.SizeF(153.0!, 17.0!)
            Me.XrLabel_Self_TotalGoingToPrincipal.StylePriority.UseTextAlignment = False
            Me.XrLabel_Self_TotalGoingToPrincipal.Text = "$0.00"
            Me.XrLabel_Self_TotalGoingToPrincipal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_MonthlySavings_TotalInterestAndFees
            '
            Me.XrLabel_MonthlySavings_TotalInterestAndFees.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "MonthlySavings_TotalInterestAndFees", "{0:c}")})
            Me.XrLabel_MonthlySavings_TotalInterestAndFees.LocationFloat = New DevExpress.Utils.PointFloat(533.0!, 140.625!)
            Me.XrLabel_MonthlySavings_TotalInterestAndFees.Name = "XrLabel_MonthlySavings_TotalInterestAndFees"
            Me.XrLabel_MonthlySavings_TotalInterestAndFees.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_MonthlySavings_TotalInterestAndFees.SizeF = New System.Drawing.SizeF(192.0!, 17.0!)
            Me.XrLabel_MonthlySavings_TotalInterestAndFees.StylePriority.UseTextAlignment = False
            Me.XrLabel_MonthlySavings_TotalInterestAndFees.Text = "$0.00"
            Me.XrLabel_MonthlySavings_TotalInterestAndFees.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_DMP_TotalInterestFees
            '
            Me.XrLabel_DMP_TotalInterestFees.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "DMP_TotalInterestFees", "{0:c}")})
            Me.XrLabel_DMP_TotalInterestFees.LocationFloat = New DevExpress.Utils.PointFloat(533.0!, 115.625!)
            Me.XrLabel_DMP_TotalInterestFees.Name = "XrLabel_DMP_TotalInterestFees"
            Me.XrLabel_DMP_TotalInterestFees.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_DMP_TotalInterestFees.SizeF = New System.Drawing.SizeF(192.0!, 17.0!)
            Me.XrLabel_DMP_TotalInterestFees.StylePriority.UseTextAlignment = False
            Me.XrLabel_DMP_TotalInterestFees.Text = "$0.00"
            Me.XrLabel_DMP_TotalInterestFees.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Self_TotalInterestAndFees
            '
            Me.XrLabel_Self_TotalInterestAndFees.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Self_TotalInterestAndFees", "{0:c}")})
            Me.XrLabel_Self_TotalInterestAndFees.LocationFloat = New DevExpress.Utils.PointFloat(533.0!, 90.625!)
            Me.XrLabel_Self_TotalInterestAndFees.Name = "XrLabel_Self_TotalInterestAndFees"
            Me.XrLabel_Self_TotalInterestAndFees.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Self_TotalInterestAndFees.SizeF = New System.Drawing.SizeF(192.0!, 17.0!)
            Me.XrLabel_Self_TotalInterestAndFees.StylePriority.UseTextAlignment = False
            Me.XrLabel_Self_TotalInterestAndFees.Text = "$0.00"
            Me.XrLabel_Self_TotalInterestAndFees.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_MonthlySavings_EstimatedInterestRate
            '
            Me.XrLabel_MonthlySavings_EstimatedInterestRate.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "MonthlySavings_EstimatedInterestRate", "{0:p3}")})
            Me.XrLabel_MonthlySavings_EstimatedInterestRate.LocationFloat = New DevExpress.Utils.PointFloat(424.0001!, 140.625!)
            Me.XrLabel_MonthlySavings_EstimatedInterestRate.Name = "XrLabel_MonthlySavings_EstimatedInterestRate"
            Me.XrLabel_MonthlySavings_EstimatedInterestRate.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_MonthlySavings_EstimatedInterestRate.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
            Me.XrLabel_MonthlySavings_EstimatedInterestRate.StylePriority.UseTextAlignment = False
            Me.XrLabel_MonthlySavings_EstimatedInterestRate.Text = "0.000 %"
            Me.XrLabel_MonthlySavings_EstimatedInterestRate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_DMP_TotalInterestRate_1
            '
            Me.XrLabel_DMP_TotalInterestRate_1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "DMP_TotalInterestRate", "{0:p3}")})
            Me.XrLabel_DMP_TotalInterestRate_1.LocationFloat = New DevExpress.Utils.PointFloat(424.0001!, 115.625!)
            Me.XrLabel_DMP_TotalInterestRate_1.Name = "XrLabel_DMP_TotalInterestRate_1"
            Me.XrLabel_DMP_TotalInterestRate_1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_DMP_TotalInterestRate_1.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
            Me.XrLabel_DMP_TotalInterestRate_1.StylePriority.UseTextAlignment = False
            Me.XrLabel_DMP_TotalInterestRate_1.Text = "0.000 %"
            Me.XrLabel_DMP_TotalInterestRate_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Self_TotalInterestRate_1
            '
            Me.XrLabel_Self_TotalInterestRate_1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Self_TotalInterestRate", "{0:p3}")})
            Me.XrLabel_Self_TotalInterestRate_1.LocationFloat = New DevExpress.Utils.PointFloat(424.0001!, 90.625!)
            Me.XrLabel_Self_TotalInterestRate_1.Name = "XrLabel_Self_TotalInterestRate_1"
            Me.XrLabel_Self_TotalInterestRate_1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Self_TotalInterestRate_1.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
            Me.XrLabel_Self_TotalInterestRate_1.StylePriority.UseTextAlignment = False
            Me.XrLabel_Self_TotalInterestRate_1.Text = "0.000 %"
            Me.XrLabel_Self_TotalInterestRate_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_MonthlySavings_FinanceCharge
            '
            Me.XrLabel_MonthlySavings_FinanceCharge.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "MonthlySavings_FinanceCharge", "{0:c}")})
            Me.XrLabel_MonthlySavings_FinanceCharge.LocationFloat = New DevExpress.Utils.PointFloat(258.0!, 140.625!)
            Me.XrLabel_MonthlySavings_FinanceCharge.Name = "XrLabel_MonthlySavings_FinanceCharge"
            Me.XrLabel_MonthlySavings_FinanceCharge.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_MonthlySavings_FinanceCharge.SizeF = New System.Drawing.SizeF(116.0!, 17.0!)
            Me.XrLabel_MonthlySavings_FinanceCharge.StylePriority.UseTextAlignment = False
            Me.XrLabel_MonthlySavings_FinanceCharge.Text = "$0.00"
            Me.XrLabel_MonthlySavings_FinanceCharge.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_DMP_TotalFinanceCharge
            '
            Me.XrLabel_DMP_TotalFinanceCharge.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "DMP_TotalFinanceCharge", "{0:c}")})
            Me.XrLabel_DMP_TotalFinanceCharge.LocationFloat = New DevExpress.Utils.PointFloat(258.0!, 115.625!)
            Me.XrLabel_DMP_TotalFinanceCharge.Name = "XrLabel_DMP_TotalFinanceCharge"
            Me.XrLabel_DMP_TotalFinanceCharge.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_DMP_TotalFinanceCharge.SizeF = New System.Drawing.SizeF(116.0!, 17.0!)
            Me.XrLabel_DMP_TotalFinanceCharge.StylePriority.UseTextAlignment = False
            Me.XrLabel_DMP_TotalFinanceCharge.Text = "$0.00"
            Me.XrLabel_DMP_TotalFinanceCharge.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Self_TotalFinanceCharge
            '
            Me.XrLabel_Self_TotalFinanceCharge.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Self_TotalFinanceCharge", "{0:c}")})
            Me.XrLabel_Self_TotalFinanceCharge.LocationFloat = New DevExpress.Utils.PointFloat(258.0!, 90.625!)
            Me.XrLabel_Self_TotalFinanceCharge.Name = "XrLabel_Self_TotalFinanceCharge"
            Me.XrLabel_Self_TotalFinanceCharge.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Self_TotalFinanceCharge.SizeF = New System.Drawing.SizeF(116.0!, 17.0!)
            Me.XrLabel_Self_TotalFinanceCharge.StylePriority.UseTextAlignment = False
            Me.XrLabel_Self_TotalFinanceCharge.Text = "$0.00"
            Me.XrLabel_Self_TotalFinanceCharge.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_MonthlySavings_Payments
            '
            Me.XrLabel_MonthlySavings_Payments.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "MonthlySavings_Payments", "{0:c}")})
            Me.XrLabel_MonthlySavings_Payments.LocationFloat = New DevExpress.Utils.PointFloat(149.0!, 140.625!)
            Me.XrLabel_MonthlySavings_Payments.Name = "XrLabel_MonthlySavings_Payments"
            Me.XrLabel_MonthlySavings_Payments.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_MonthlySavings_Payments.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
            Me.XrLabel_MonthlySavings_Payments.StylePriority.UseTextAlignment = False
            Me.XrLabel_MonthlySavings_Payments.Text = "$0.00"
            Me.XrLabel_MonthlySavings_Payments.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_DMP_TotalEnteredPayment_1
            '
            Me.XrLabel_DMP_TotalEnteredPayment_1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "DMP_TotalEnteredPayment", "{0:c}")})
            Me.XrLabel_DMP_TotalEnteredPayment_1.LocationFloat = New DevExpress.Utils.PointFloat(149.0!, 115.625!)
            Me.XrLabel_DMP_TotalEnteredPayment_1.Name = "XrLabel_DMP_TotalEnteredPayment_1"
            Me.XrLabel_DMP_TotalEnteredPayment_1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_DMP_TotalEnteredPayment_1.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
            Me.XrLabel_DMP_TotalEnteredPayment_1.StylePriority.UseTextAlignment = False
            Me.XrLabel_DMP_TotalEnteredPayment_1.Text = "$0.00"
            Me.XrLabel_DMP_TotalEnteredPayment_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_Self_TotalPayoutPayment_1
            '
            Me.XrLabel_Self_TotalPayoutPayment_1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Self_TotalPayoutPayment", "{0:c}")})
            Me.XrLabel_Self_TotalPayoutPayment_1.LocationFloat = New DevExpress.Utils.PointFloat(149.0!, 90.625!)
            Me.XrLabel_Self_TotalPayoutPayment_1.Name = "XrLabel_Self_TotalPayoutPayment_1"
            Me.XrLabel_Self_TotalPayoutPayment_1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_Self_TotalPayoutPayment_1.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
            Me.XrLabel_Self_TotalPayoutPayment_1.StylePriority.UseTextAlignment = False
            Me.XrLabel_Self_TotalPayoutPayment_1.Text = "$0.00"
            Me.XrLabel_Self_TotalPayoutPayment_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel5
            '
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(0.00003178914!, 140.625!)
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
            Me.XrLabel5.StylePriority.UseFont = False
            Me.XrLabel5.Text = "TOTAL SAVINGS"
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(0.00003178914!, 115.625!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.Text = "PLAN"
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(0.00003178914!, 90.625!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
            Me.XrLabel3.StylePriority.UseFont = False
            Me.XrLabel3.Text = "Client"
            '
            'XrPanel2
            '
            Me.XrPanel2.BackColor = System.Drawing.Color.Teal
            Me.XrPanel2.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel14, Me.XrLabel15, Me.XrLabel16, Me.XrLabel17, Me.XrLabel18, Me.XrLabel19, Me.XrLabel20, Me.XrLabel21})
            Me.XrPanel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 231.9583!)
            Me.XrPanel2.Name = "XrPanel2"
            Me.XrPanel2.SizeF = New System.Drawing.SizeF(1050.0!, 35.0!)
            Me.XrPanel2.StylePriority.UseBackColor = False
            '
            'XrLabel14
            '
            Me.XrLabel14.CanGrow = False
            Me.XrLabel14.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel14.ForeColor = System.Drawing.Color.White
            Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(900.0!, 1.0!)
            Me.XrLabel14.Multiline = True
            Me.XrLabel14.Name = "XrLabel14"
            Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100.0!)
            Me.XrLabel14.SizeF = New System.Drawing.SizeF(125.0!, 33.0!)
            Me.XrLabel14.StylePriority.UseFont = False
            Me.XrLabel14.StylePriority.UseForeColor = False
            Me.XrLabel14.StylePriority.UsePadding = False
            Me.XrLabel14.StylePriority.UseTextAlignment = False
            Me.XrLabel14.Text = "TOTAL AMOUNT" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "PAID BY END"
            Me.XrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'XrLabel15
            '
            Me.XrLabel15.CanGrow = False
            Me.XrLabel15.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel15.ForeColor = System.Drawing.Color.White
            Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(766.0!, 1.0!)
            Me.XrLabel15.Multiline = True
            Me.XrLabel15.Name = "XrLabel15"
            Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100.0!)
            Me.XrLabel15.SizeF = New System.Drawing.SizeF(119.0!, 33.0!)
            Me.XrLabel15.StylePriority.UseFont = False
            Me.XrLabel15.StylePriority.UseForeColor = False
            Me.XrLabel15.StylePriority.UsePadding = False
            Me.XrLabel15.StylePriority.UseTextAlignment = False
            Me.XrLabel15.Text = "TOTAL" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "INTEREST PAID"
            Me.XrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'XrLabel16
            '
            Me.XrLabel16.CanGrow = False
            Me.XrLabel16.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel16.ForeColor = System.Drawing.Color.White
            Me.XrLabel16.LocationFloat = New DevExpress.Utils.PointFloat(641.0!, 17.0!)
            Me.XrLabel16.Multiline = True
            Me.XrLabel16.Name = "XrLabel16"
            Me.XrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100.0!)
            Me.XrLabel16.SizeF = New System.Drawing.SizeF(83.0!, 16.0!)
            Me.XrLabel16.StylePriority.UseFont = False
            Me.XrLabel16.StylePriority.UseForeColor = False
            Me.XrLabel16.StylePriority.UsePadding = False
            Me.XrLabel16.StylePriority.UseTextAlignment = False
            Me.XrLabel16.Text = "IN YEARS"
            Me.XrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'XrLabel17
            '
            Me.XrLabel17.CanGrow = False
            Me.XrLabel17.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel17.ForeColor = System.Drawing.Color.White
            Me.XrLabel17.LocationFloat = New DevExpress.Utils.PointFloat(541.0!, 17.0!)
            Me.XrLabel17.Multiline = True
            Me.XrLabel17.Name = "XrLabel17"
            Me.XrLabel17.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100.0!)
            Me.XrLabel17.SizeF = New System.Drawing.SizeF(83.0!, 16.0!)
            Me.XrLabel17.StylePriority.UseFont = False
            Me.XrLabel17.StylePriority.UseForeColor = False
            Me.XrLabel17.StylePriority.UsePadding = False
            Me.XrLabel17.StylePriority.UseTextAlignment = False
            Me.XrLabel17.Text = "IN MONTHS"
            Me.XrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'XrLabel18
            '
            Me.XrLabel18.BorderColor = System.Drawing.Color.White
            Me.XrLabel18.Borders = DevExpress.XtraPrinting.BorderSide.Bottom
            Me.XrLabel18.CanGrow = False
            Me.XrLabel18.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel18.ForeColor = System.Drawing.Color.White
            Me.XrLabel18.LocationFloat = New DevExpress.Utils.PointFloat(541.0!, 1.0!)
            Me.XrLabel18.Multiline = True
            Me.XrLabel18.Name = "XrLabel18"
            Me.XrLabel18.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel18.SizeF = New System.Drawing.SizeF(183.0!, 16.0!)
            Me.XrLabel18.StylePriority.UseBorderColor = False
            Me.XrLabel18.StylePriority.UseBorders = False
            Me.XrLabel18.StylePriority.UseFont = False
            Me.XrLabel18.StylePriority.UseForeColor = False
            Me.XrLabel18.StylePriority.UsePadding = False
            Me.XrLabel18.StylePriority.UseTextAlignment = False
            Me.XrLabel18.Text = "LENGTH OF PLAN"
            Me.XrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel19
            '
            Me.XrLabel19.CanGrow = False
            Me.XrLabel19.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel19.ForeColor = System.Drawing.Color.White
            Me.XrLabel19.LocationFloat = New DevExpress.Utils.PointFloat(408.0!, 1.0!)
            Me.XrLabel19.Multiline = True
            Me.XrLabel19.Name = "XrLabel19"
            Me.XrLabel19.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100.0!)
            Me.XrLabel19.SizeF = New System.Drawing.SizeF(116.0!, 33.0!)
            Me.XrLabel19.StylePriority.UseFont = False
            Me.XrLabel19.StylePriority.UseForeColor = False
            Me.XrLabel19.StylePriority.UsePadding = False
            Me.XrLabel19.StylePriority.UseTextAlignment = False
            Me.XrLabel19.Text = "ESTIMATED" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "INTEREST RATE"
            Me.XrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'XrLabel20
            '
            Me.XrLabel20.CanGrow = False
            Me.XrLabel20.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel20.ForeColor = System.Drawing.Color.White
            Me.XrLabel20.LocationFloat = New DevExpress.Utils.PointFloat(291.0!, 1.0!)
            Me.XrLabel20.Multiline = True
            Me.XrLabel20.Name = "XrLabel20"
            Me.XrLabel20.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100.0!)
            Me.XrLabel20.SizeF = New System.Drawing.SizeF(83.0!, 33.0!)
            Me.XrLabel20.StylePriority.UseFont = False
            Me.XrLabel20.StylePriority.UseForeColor = False
            Me.XrLabel20.StylePriority.UsePadding = False
            Me.XrLabel20.StylePriority.UseTextAlignment = False
            Me.XrLabel20.Text = "MONTHLY" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "PAYMENT"
            Me.XrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'XrLabel21
            '
            Me.XrLabel21.CanGrow = False
            Me.XrLabel21.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel21.ForeColor = System.Drawing.Color.White
            Me.XrLabel21.LocationFloat = New DevExpress.Utils.PointFloat(166.0!, 1.0!)
            Me.XrLabel21.Multiline = True
            Me.XrLabel21.Name = "XrLabel21"
            Me.XrLabel21.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100.0!)
            Me.XrLabel21.SizeF = New System.Drawing.SizeF(83.0!, 33.0!)
            Me.XrLabel21.StylePriority.UseFont = False
            Me.XrLabel21.StylePriority.UseForeColor = False
            Me.XrLabel21.StylePriority.UsePadding = False
            Me.XrLabel21.StylePriority.UseTextAlignment = False
            Me.XrLabel21.Text = "PRINCIPAL" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "DEBT"
            Me.XrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel6, Me.XrLabel7, Me.XrLabel8, Me.XrLabel9, Me.XrLabel10, Me.XrLabel11})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 47.5!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(1050.0!, 33.0!)
            Me.XrPanel1.StylePriority.UseBackColor = False
            '
            'XrLabel6
            '
            Me.XrLabel6.CanGrow = False
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(900.0!, 0.0!)
            Me.XrLabel6.Multiline = True
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(125.0!, 33.0!)
            Me.XrLabel6.StylePriority.UseFont = False
            Me.XrLabel6.StylePriority.UseForeColor = False
            Me.XrLabel6.StylePriority.UsePadding = False
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "PERCENT GOING" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "TO PRINCIPAL"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'XrLabel7
            '
            Me.XrLabel7.CanGrow = False
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel7.ForeColor = System.Drawing.Color.White
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(753.0!, 0.0!)
            Me.XrLabel7.Multiline = True
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(133.0!, 33.0!)
            Me.XrLabel7.StylePriority.UseFont = False
            Me.XrLabel7.StylePriority.UseForeColor = False
            Me.XrLabel7.StylePriority.UsePadding = False
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            Me.XrLabel7.Text = "$ AMOUNT GOING" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "TO PRINCIPAL"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'XrLabel8
            '
            Me.XrLabel8.CanGrow = False
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.ForeColor = System.Drawing.Color.White
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(600.0!, 0.0!)
            Me.XrLabel8.Multiline = True
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(125.0!, 33.0!)
            Me.XrLabel8.StylePriority.UseFont = False
            Me.XrLabel8.StylePriority.UseForeColor = False
            Me.XrLabel8.StylePriority.UsePadding = False
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "TOTAL INTEREST" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & " AND FEES"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'XrLabel9
            '
            Me.XrLabel9.CanGrow = False
            Me.XrLabel9.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel9.ForeColor = System.Drawing.Color.White
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(408.0!, 0.0!)
            Me.XrLabel9.Multiline = True
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(116.0!, 33.0!)
            Me.XrLabel9.StylePriority.UseFont = False
            Me.XrLabel9.StylePriority.UseForeColor = False
            Me.XrLabel9.StylePriority.UsePadding = False
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "ANNUAL EST." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "INTEREST RATE"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'XrLabel10
            '
            Me.XrLabel10.CanGrow = False
            Me.XrLabel10.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel10.ForeColor = System.Drawing.Color.White
            Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(291.0!, 0.0!)
            Me.XrLabel10.Multiline = True
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100.0!)
            Me.XrLabel10.SizeF = New System.Drawing.SizeF(83.0!, 33.0!)
            Me.XrLabel10.StylePriority.UseFont = False
            Me.XrLabel10.StylePriority.UseForeColor = False
            Me.XrLabel10.StylePriority.UsePadding = False
            Me.XrLabel10.StylePriority.UseTextAlignment = False
            Me.XrLabel10.Text = "FINANCE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "CHARGE"
            Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'XrLabel11
            '
            Me.XrLabel11.CanGrow = False
            Me.XrLabel11.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel11.ForeColor = System.Drawing.Color.White
            Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(166.0!, 0.0!)
            Me.XrLabel11.Multiline = True
            Me.XrLabel11.Name = "XrLabel11"
            Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 4, 100.0!)
            Me.XrLabel11.SizeF = New System.Drawing.SizeF(83.0!, 33.0!)
            Me.XrLabel11.StylePriority.UseFont = False
            Me.XrLabel11.StylePriority.UseForeColor = False
            Me.XrLabel11.StylePriority.UsePadding = False
            Me.XrLabel11.StylePriority.UseTextAlignment = False
            Me.XrLabel11.Text = "MONTHLY" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "PAYMENTS"
            Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 10.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(1050.0!, 25.0!)
            Me.XrLabel2.StylePriority.UseFont = False
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "POTENTIAL MONTHLY SAVINGS"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 16.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(1050.0!, 33.0!)
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "CLIENT / PLAN COMPARISON INFORMATION"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'TopMargin
            '
            Me.TopMargin.HeightF = 25.0!
            Me.TopMargin.Name = "TopMargin"
            '
            'BottomMargin
            '
            Me.BottomMargin.HeightF = 25.0!
            Me.BottomMargin.Name = "BottomMargin"
            '
            'ReportHeader
            '
            Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1})
            Me.ReportHeader.HeightF = 39.0!
            Me.ReportHeader.Name = "ReportHeader"
            Me.ReportHeader.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel3})
            Me.ReportFooter.HeightF = 175.0!
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrPanel3
            '
            Me.XrPanel3.Borders = CType((((DevExpress.XtraPrinting.BorderSide.Left Or DevExpress.XtraPrinting.BorderSide.Top) _
                                          Or DevExpress.XtraPrinting.BorderSide.Right) _
                                         Or DevExpress.XtraPrinting.BorderSide.Bottom), DevExpress.XtraPrinting.BorderSide)
            Me.XrPanel3.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel23, Me.XrLabel12, Me.XrLabel13})
            Me.XrPanel3.LocationFloat = New DevExpress.Utils.PointFloat(125.0!, 38.9167!)
            Me.XrPanel3.Name = "XrPanel3"
            Me.XrPanel3.SizeF = New System.Drawing.SizeF(793.7499!, 126.0833!)
            Me.XrPanel3.StylePriority.UseBorders = False
            '
            'XrLabel23
            '
            Me.XrLabel23.Borders = DevExpress.XtraPrinting.BorderSide.None
            Me.XrLabel23.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel23.LocationFloat = New DevExpress.Utils.PointFloat(68.58336!, 10.00001!)
            Me.XrLabel23.Name = "XrLabel23"
            Me.XrLabel23.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel23.SizeF = New System.Drawing.SizeF(60.41667!, 23.0!)
            Me.XrLabel23.StylePriority.UseBorders = False
            Me.XrLabel23.StylePriority.UseFont = False
            Me.XrLabel23.StylePriority.UseTextAlignment = False
            Me.XrLabel23.Text = "*NOTE*"
            Me.XrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel12
            '
            Me.XrLabel12.Borders = DevExpress.XtraPrinting.BorderSide.None
            Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(22.20833!, 60.00001!)
            Me.XrLabel12.Name = "XrLabel12"
            Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel12.SizeF = New System.Drawing.SizeF(767.0!, 50.0!)
            Me.XrLabel12.StylePriority.UseBorders = False
            Me.XrLabel12.Text = resources.GetString("XrLabel12.Text")
            '
            'XrLabel13
            '
            Me.XrLabel13.Borders = DevExpress.XtraPrinting.BorderSide.None
            Me.XrLabel13.Font = New System.Drawing.Font("Times New Roman", 10.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(129.0!, 10.00001!)
            Me.XrLabel13.Name = "XrLabel13"
            Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel13.SizeF = New System.Drawing.SizeF(654.7499!, 32.99999!)
            Me.XrLabel13.StylePriority.UseBorders = False
            Me.XrLabel13.StylePriority.UseFont = False
            Me.XrLabel13.StylePriority.UseTextAlignment = False
            Me.XrLabel13.Text = "THIS CLIENT/PLAN COMPARISON IS ONLY AN ESTIMATE. IT DOES NOT TAKE INTO ACCOUNT PO" & _
                                "TENTIAL LATE AND OVER LIMIT FEES NOR THE CCCS MONTHLY MAINTENANCE FEE."
            Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'calc_Self_LengthMonths
            '
            Me.calc_Self_LengthMonths.FieldType = DevExpress.XtraReports.UI.FieldType.[String]
            Me.calc_Self_LengthMonths.Name = "calc_Self_LengthMonths"
            '
            'calc_DMP_LengthMonths
            '
            Me.calc_DMP_LengthMonths.FieldType = DevExpress.XtraReports.UI.FieldType.[String]
            Me.calc_DMP_LengthMonths.Name = "calc_DMP_LengthMonths"
            '
            'calc_Self_LengthYears
            '
            Me.calc_Self_LengthYears.FieldType = DevExpress.XtraReports.UI.FieldType.[String]
            Me.calc_Self_LengthYears.Name = "calc_Self_LengthYears"
            '
            'calc_DMP_LengthYears
            '
            Me.calc_DMP_LengthYears.FieldType = DevExpress.XtraReports.UI.FieldType.[String]
            Me.calc_DMP_LengthYears.Name = "calc_DMP_LengthYears"
            '
            'Comparison_Report_Summary
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.ReportHeader, Me.ReportFooter})
            Me.CalculatedFields.AddRange(New DevExpress.XtraReports.UI.CalculatedField() {Me.calc_Self_LengthMonths, Me.calc_Self_LengthYears, Me.calc_DMP_LengthMonths, Me.calc_DMP_LengthYears})
            Me.Landscape = True
            Me.Margins = New System.Drawing.Printing.Margins(25, 25, 25, 25)
            Me.PageHeight = 850
            Me.PageWidth = 1100
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.Version = "11.2"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel2 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel16 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel17 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel18 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel19 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel20 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel21 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DMP_TotalPercentGoingToprincipal As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Self_TotalPercentGoingToPrincipal As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DMP_TotalGoingToPrincipal As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Self_TotalGoingToPrincipal As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_MonthlySavings_TotalInterestAndFees As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DMP_TotalInterestFees As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Self_TotalInterestAndFees As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_MonthlySavings_EstimatedInterestRate As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DMP_TotalInterestRate_1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Self_TotalInterestRate_1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_MonthlySavings_FinanceCharge As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DMP_TotalFinanceCharge As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Self_TotalFinanceCharge As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_MonthlySavings_Payments As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DMP_TotalEnteredPayment_1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Self_TotalPayoutPayment_1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel40 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel_Savings As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DMP_TotalAmountPaid As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Self_TotalPaid As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DMP_LengthMonths As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Self_LengthMonths As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DMP_ExtraAmount As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DMP_TotalInterestPaid As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel54 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DMP_LengthYears As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Self_LengthYears As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DMP_TotalInterestRate_2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Self_TotalInterestRate_2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DMP_TotalEnteredPayment_2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Self_TotalPayoutPayment_2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DMP_TotalBalance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Self_TotalBalance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel22 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Self_TotalInterestPaid As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel41 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel50 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DMP_TotalPayment As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel44 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
        Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
        Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents XrPanel3 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel23 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents calc_Self_LengthMonths As DevExpress.XtraReports.UI.CalculatedField
        Friend WithEvents calc_DMP_LengthMonths As DevExpress.XtraReports.UI.CalculatedField
        Friend WithEvents calc_Self_LengthYears As DevExpress.XtraReports.UI.CalculatedField
        Friend WithEvents calc_DMP_LengthYears As DevExpress.XtraReports.UI.CalculatedField
    End Class
End Namespace