#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.ComponentModel
Imports DebtPlus.Reports.Template
Imports DebtPlus.UI.Client.Service
Imports System.Drawing.Printing
Imports DebtPlus.Utils.Format
Imports DebtPlus.LINQ
Imports System.Linq

Namespace forms.Comparison.Report

    Friend Class Comparison_Report
        Inherits BaseXtraReportClass

        Private salesFileRecord As sales_file

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(ByVal SalesFileRecord As sales_file)
            MyClass.New()
            Me.salesFileRecord = SalesFileRecord

            ' Pass the sales file to the sub-reports
            Comparison_Report_Client1.salesFileRecord = SalesFileRecord
            Comparison_Report_Plan1.salesFileRecord = SalesFileRecord
            Comparison_Report_Summary1.salesFileRecord = SalesFileRecord

            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler XrLabel_AgencyName.BeforePrint, AddressOf XrLabel_AgencyName_BeforePrint
            AddHandler XrLabel_ClientID.BeforePrint, AddressOf XrLabel_ClientID_BeforePrint
            AddHandler XrLabel_ClientName.BeforePrint, AddressOf XrLabel_ClientName_BeforePrint
            AddHandler XrLabel_DateCreated.BeforePrint, AddressOf XrLabel_DateCreated_BeforePrint
            AddHandler XrSubreport_Client.BeforePrint, AddressOf XrSubreport_Client_BeforePrint
            AddHandler XrSubreport_Plan.BeforePrint, AddressOf XrSubreport_Plan_BeforePrint
            AddHandler XrSubreport_Summary.BeforePrint, AddressOf XrSubreport_Summary_BeforePrint
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler XrLabel_AgencyName.BeforePrint, AddressOf XrLabel_AgencyName_BeforePrint
            RemoveHandler XrLabel_ClientID.BeforePrint, AddressOf XrLabel_ClientID_BeforePrint
            RemoveHandler XrLabel_ClientName.BeforePrint, AddressOf XrLabel_ClientName_BeforePrint
            RemoveHandler XrLabel_DateCreated.BeforePrint, AddressOf XrLabel_DateCreated_BeforePrint
            RemoveHandler XrSubreport_Client.BeforePrint, AddressOf XrSubreport_Client_BeforePrint
            RemoveHandler XrSubreport_Plan.BeforePrint, AddressOf XrSubreport_Plan_BeforePrint
            RemoveHandler XrSubreport_Summary.BeforePrint, AddressOf XrSubreport_Summary_BeforePrint
        End Sub

        ''' <summary>
        ''' Handle the client ID before it is printed
        ''' </summary>
        Private Sub XrLabel_ClientID_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = DirectCast(sender, DevExpress.XtraReports.UI.XRLabel)
            lbl.Text = String.Format("{0:0000000}", salesFileRecord.client)
        End Sub

        ''' <summary>
        ''' Handle the client name before it is printed
        ''' </summary>
        Private Sub XrLabel_ClientName_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = DirectCast(sender, DevExpress.XtraReports.UI.XRLabel)

            ' Find the name of the applicant
            Dim client As Int32 = salesFileRecord.client
            If client >= 0 Then
                Using bc As New BusinessContext()
                    Dim n As DebtPlus.LINQ.Name = bc.peoples.Join(bc.Names, Function(p) p.NameID, Function(nam) nam.Id, Function(p, nam) New With {.client = p.Client, .relation = p.Relation, .name = nam}).Where(Function(x) x.client = salesFileRecord.client AndAlso x.relation = DebtPlus.LINQ.Cache.RelationType.Self).Select(Function(x) x.name).FirstOrDefault()
                    If n IsNot Nothing Then
                        lbl.Text = n.ToString()
                        Return
                    End If
                End Using
            End If

            lbl.Text = String.Empty
        End Sub

        ''' <summary>
        ''' Handle the creation date for the comparison data before it is printed
        ''' </summary>
        Private Sub XrLabel_DateCreated_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = DirectCast(sender, DevExpress.XtraReports.UI.XRLabel)
            lbl.Text = String.Format("{0:MMMM d, yyyy}", salesFileRecord.date_created)
        End Sub

        ''' <summary>
        ''' Handle the agency name before it is printed
        ''' </summary>
        Private Sub XrLabel_AgencyName_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim ctl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)

            Try
                ' Find the name from the cached config entry
                Dim q As DebtPlus.LINQ.config = DebtPlus.LINQ.Cache.config.getList()(0)
                ctl.Text = q.name
            Catch
            End Try
        End Sub

        ''' <summary>
        ''' Handle the client information before it is printed
        ''' </summary>
        Private Sub XrSubreport_Client_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim SubReport As DevExpress.XtraReports.UI.XRSubreport = CType(sender, DevExpress.XtraReports.UI.XRSubreport)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(SubReport.ReportSource, DevExpress.XtraReports.UI.XtraReport)

            rpt.DataSource = salesFileRecord.sales_debts
            For Each calc As DevExpress.XtraReports.UI.CalculatedField In rpt.CalculatedFields
                calc.Assign(rpt.DataSource, rpt.DataMember)
            Next
        End Sub

        ''' <summary>
        ''' Handle the DMP Plan sub-report before it is printed
        ''' </summary>
        Private Sub XrSubreport_Plan_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim SubReport As DevExpress.XtraReports.UI.XRSubreport = CType(sender, DevExpress.XtraReports.UI.XRSubreport)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(SubReport.ReportSource, DevExpress.XtraReports.UI.XtraReport)

            rpt.DataSource = salesFileRecord.sales_debts
            For Each calc As DevExpress.XtraReports.UI.CalculatedField In rpt.CalculatedFields
                calc.Assign(rpt.DataSource, rpt.DataMember)
            Next
        End Sub

        ''' <summary>
        ''' Handle the summary sub-report before it is printed
        ''' </summary>
        Private Sub XrSubreport_Summary_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim SubReport As DevExpress.XtraReports.UI.XRSubreport = CType(sender, DevExpress.XtraReports.UI.XRSubreport)
            Dim rpt As DevExpress.XtraReports.UI.XtraReport = CType(SubReport.ReportSource, DevExpress.XtraReports.UI.XtraReport)

            ' Turn the single structure into an IList object for the datasource
            Dim lst As New ArrayList
            lst.Add(salesFileRecord)
            rpt.DataSource = lst
        End Sub

    End Class
End Namespace
