Namespace forms.Comparison.Report
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Comparison_Report

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Comparison_Report))
            Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
            Me.XrLabel_AgencyName = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_ClientID = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_DateCreated = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_ClientName = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo()
            Me.XrSubreport_Summary = New DevExpress.XtraReports.UI.XRSubreport()
            Me.Comparison_Report_Summary1 = New DebtPlus.UI.Client.forms.Comparison.Report.Comparison_Report_Summary(Me.components)
            Me.XrSubreport_Plan = New DevExpress.XtraReports.UI.XRSubreport()
            Me.Comparison_Report_Plan1 = New DebtPlus.UI.Client.forms.Comparison.Report.Comparison_Report_Plan(Me.components)
            Me.XrSubreport_Client = New DevExpress.XtraReports.UI.XRSubreport()
            Me.Comparison_Report_Client1 = New DebtPlus.UI.Client.forms.Comparison.Report.Comparison_Report_Client(Me.components)
            CType(Me.Comparison_Report_Summary1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Comparison_Report_Plan1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Comparison_Report_Client1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrSubreport_Summary, Me.XrSubreport_Plan, Me.XrSubreport_Client})
            Me.Detail.HeightF = 72.0!
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'PageFooter
            '
            Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_AgencyName, Me.XrLabel_ClientID, Me.XrLabel_DateCreated, Me.XrLabel_ClientName, Me.XrPageInfo1})
            Me.PageFooter.HeightF = 59.0!
            Me.PageFooter.Name = "PageFooter"
            Me.PageFooter.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_AgencyName
            '
            Me.XrLabel_AgencyName.LocationFloat = New DevExpress.Utils.PointFloat(150.0!, 34.0!)
            Me.XrLabel_AgencyName.Name = "XrLabel_AgencyName"
            Me.XrLabel_AgencyName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_AgencyName.SizeF = New System.Drawing.SizeF(900.0!, 16.0!)
            Me.XrLabel_AgencyName.StylePriority.UseTextAlignment = False
            Me.XrLabel_AgencyName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_ClientID
            '
            Me.XrLabel_ClientID.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 34.0!)
            Me.XrLabel_ClientID.Name = "XrLabel_ClientID"
            Me.XrLabel_ClientID.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ClientID.SizeF = New System.Drawing.SizeF(141.0!, 16.0!)
            '
            'XrLabel_DateCreated
            '
            Me.XrLabel_DateCreated.LocationFloat = New DevExpress.Utils.PointFloat(742.0!, 17.0!)
            Me.XrLabel_DateCreated.Name = "XrLabel_DateCreated"
            Me.XrLabel_DateCreated.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_DateCreated.SizeF = New System.Drawing.SizeF(200.0!, 16.0!)
            '
            'XrLabel_ClientName
            '
            Me.XrLabel_ClientName.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 17.0!)
            Me.XrLabel_ClientName.Name = "XrLabel_ClientName"
            Me.XrLabel_ClientName.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_ClientName.SizeF = New System.Drawing.SizeF(733.0!, 16.0!)
            '
            'XrPageInfo1
            '
            Me.XrPageInfo1.Format = "Page {0:f0} of {1:f0}"
            Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(950.0!, 17.0!)
            Me.XrPageInfo1.Name = "XrPageInfo1"
            Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
            Me.XrPageInfo1.StylePriority.UseTextAlignment = False
            Me.XrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrSubreport_Summary
            '
            Me.XrSubreport_Summary.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 48.0!)
            Me.XrSubreport_Summary.Name = "XrSubreport_Summary"
            Me.XrSubreport_Summary.ReportSource = Me.Comparison_Report_Summary1
            Me.XrSubreport_Summary.SizeF = New System.Drawing.SizeF(1050.0!, 24.0!)
            '
            'XrSubreport_Plan
            '
            Me.XrSubreport_Plan.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 24.0!)
            Me.XrSubreport_Plan.Name = "XrSubreport_Plan"
            Me.XrSubreport_Plan.ReportSource = Me.Comparison_Report_Plan1
            Me.XrSubreport_Plan.SizeF = New System.Drawing.SizeF(1050.0!, 24.0!)
            '
            'XrSubreport_Client
            '
            Me.XrSubreport_Client.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrSubreport_Client.Name = "XrSubreport_Client"
            Me.XrSubreport_Client.ReportSource = Me.Comparison_Report_Client1
            Me.XrSubreport_Client.SizeF = New System.Drawing.SizeF(1050.0!, 24.0!)
            '
            'Comparison_Report
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.TopMarginBand1, Me.Detail, Me.BottomMarginBand1, Me.PageFooter})
            Me.Landscape = True
            Me.PageHeight = 850
            Me.PageWidth = 1100
            Me.RequestParameters = False
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            CType(Me.Comparison_Report_Summary1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Comparison_Report_Plan1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Comparison_Report_Client1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        End Sub

        Private WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
        Private WithEvents XrSubreport_Client As DevExpress.XtraReports.UI.XRSubreport
        Private WithEvents Comparison_Report_Client1 As Comparison_Report_Client
        Private WithEvents XrSubreport_Plan As DevExpress.XtraReports.UI.XRSubreport
        Private WithEvents Comparison_Report_Plan1 As Comparison_Report_Plan
        Private WithEvents XrSubreport_Summary As DevExpress.XtraReports.UI.XRSubreport
        Private WithEvents Comparison_Report_Summary1 As Comparison_Report_Summary
        Private WithEvents XrLabel_DateCreated As DevExpress.XtraReports.UI.XRLabel
        Private WithEvents XrLabel_ClientName As DevExpress.XtraReports.UI.XRLabel
        Private WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
        Private WithEvents XrLabel_AgencyName As DevExpress.XtraReports.UI.XRLabel
        Private WithEvents XrLabel_ClientID As DevExpress.XtraReports.UI.XRLabel
    End Class
End Namespace
