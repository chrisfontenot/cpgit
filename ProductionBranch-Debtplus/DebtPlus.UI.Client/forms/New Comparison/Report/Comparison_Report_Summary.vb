#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports System.ComponentModel
Imports DebtPlus.UI.Client.Service

Namespace forms.Comparison.Report

    Friend Class Comparison_Report_Summary

        ''' <summary>
        ''' Current sales file record
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Friend Property salesFileRecord As DebtPlus.LINQ.sales_file

        Public Sub New(ByVal Container As IContainer)
            MyClass.New()
            Container.Add(Me)
        End Sub

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            AddHandler calc_DMP_LengthMonths.GetValue, AddressOf calc_DMP_LengthMonths_GetValue
            AddHandler calc_DMP_LengthYears.GetValue, AddressOf calc_DMP_LengthYears_GetValue
            AddHandler calc_Self_LengthMonths.GetValue, AddressOf calc_Self_LengthMonths_GetValue
            AddHandler calc_Self_LengthYears.GetValue, AddressOf calc_Self_LengthYears_GetValue
        End Sub

        Private Sub calc_DMP_LengthMonths_GetValue(sender As Object, e As DevExpress.XtraReports.UI.GetValueEventArgs)
            Dim v As Double = salesFileRecord.plan_months
            If v < 0.0 Then
                e.Value = "---"
            Else
                e.Value = String.Format("{0:n0}", System.Math.Floor(v))
            End If
        End Sub

        Private Sub calc_DMP_LengthYears_GetValue(sender As Object, e As DevExpress.XtraReports.UI.GetValueEventArgs)
            Dim v As Double = salesFileRecord.plan_months
            If v < 0.0 Then
                e.Value = "---"
            Else
                e.Value = String.Format("{0:###0.0}", System.Math.Round(v / 12.0#, 1))
            End If
        End Sub

        Private Sub calc_Self_LengthMonths_GetValue(sender As Object, e As DevExpress.XtraReports.UI.GetValueEventArgs)
            Dim v As Double = salesFileRecord.SELF_LengthMonths
            If v < 0.0 Then
                e.Value = "---"
            Else
                e.Value = String.Format("{0:n0}", System.Math.Floor(v))
            End If
        End Sub

        Private Sub calc_Self_LengthYears_GetValue(sender As Object, e As DevExpress.XtraReports.UI.GetValueEventArgs)
            Dim v As Double = salesFileRecord.SELF_LengthMonths
            If v < 0.0 Then
                e.Value = "---"
            Else
                e.Value = String.Format("{0:###0.0}", System.Math.Round(v / 12.0#, 1))
            End If
        End Sub
    End Class
End Namespace