#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports System.ComponentModel
Imports DebtPlus.UI.Client.Service
Imports System.Drawing.Printing

Namespace forms.Comparison.Report

    Friend Class Comparison_Report_Plan

        ''' <summary>
        ''' Current sales file record
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Friend Property salesFileRecord As DebtPlus.LINQ.sales_file

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler XrLabel_DMP_TotalMinimumPayment.BeforePrint, AddressOf XrLabel_DMP_TotalMinimumPayment_BeforePrint
            AddHandler XrLabel_DMP_TotalEnteredPayment.BeforePrint, AddressOf XrLabel_DMP_TotalEnteredPayment_BeforePrint
            AddHandler XrLabel_DMP_TotalInterestRate.BeforePrint, AddressOf XrLabel_DMP_TotalInterestRate_BeforePrint
            AddHandler XrLabel_DMP_TotalFees.BeforePrint, AddressOf XrLabel_DMP_TotalFees_BeforePrint
            AddHandler XrLabel_DMP_TotalFinanceCharge.BeforePrint, AddressOf XrLabel_DMP_TotalFinanceCharge_BeforePrint
            AddHandler XrLabel_DMP_TotalInterestFees.BeforePrint, AddressOf XrLabel_DMP_TotalInterestFees_BeforePrint
            AddHandler XrLabel_DMP_TotalGoingToPrincipal.BeforePrint, AddressOf XrLabel_DMP_TotalGoingToPrincipal_BeforePrint
            AddHandler XrLabel_Total_Balance.BeforePrint, AddressOf XrLabel_Total_Balance_BeforePrint
        End Sub

        Public Sub New(ByVal Container As IContainer)
            MyClass.New()
            Container.Add(Me)
        End Sub

        Private Sub XrLabel_DMP_TotalMinimumPayment_BeforePrint(sender As Object, e As PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            lbl.Text = String.Format("{0:c}", salesFileRecord.DMP_TotalMinimumPayment)
        End Sub

        Private Sub XrLabel_DMP_TotalEnteredPayment_BeforePrint(sender As Object, e As PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            lbl.Text = String.Format("{0:c}", salesFileRecord.DMP_TotalEnteredPayment)
        End Sub

        Private Sub XrLabel_DMP_TotalInterestRate_BeforePrint(sender As Object, e As PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            lbl.Text = String.Format("{0:c}", salesFileRecord.DMP_TotalInterestRate)
        End Sub

        Private Sub XrLabel_DMP_TotalFees_BeforePrint(sender As Object, e As PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            lbl.Text = String.Format("{0:c}", salesFileRecord.DMP_TotalFees)
        End Sub

        Private Sub XrLabel_DMP_TotalFinanceCharge_BeforePrint(sender As Object, e As PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            lbl.Text = String.Format("{0:c}", salesFileRecord.DMP_TotalFinanceCharge)
        End Sub

        Private Sub XrLabel_DMP_TotalInterestFees_BeforePrint(sender As Object, e As PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            lbl.Text = String.Format("{0:c}", salesFileRecord.DMP_TotalInterestFees)
        End Sub

        Private Sub XrLabel_DMP_TotalGoingToPrincipal_BeforePrint(sender As Object, e As PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            lbl.Text = String.Format("{0:c}", salesFileRecord.DMP_TotalGoingToPrincipal)
        End Sub

        Private Sub XrLabel_Total_Balance_BeforePrint(sender As Object, e As PrintEventArgs)
            Dim lbl As DevExpress.XtraReports.UI.XRLabel = CType(sender, DevExpress.XtraReports.UI.XRLabel)
            lbl.Text = String.Format("{0:c}", salesFileRecord.TotalBalance)
        End Sub
    End Class
End Namespace