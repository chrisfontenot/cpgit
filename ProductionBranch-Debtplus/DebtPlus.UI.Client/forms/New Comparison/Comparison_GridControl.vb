﻿#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.UI.Client.Service
Imports DebtPlus.LINQ
Imports System.Linq

Namespace forms.Comparison
    Public Class Comparison_GridControl

        ' Sales File to be displayed
        Protected SalesFileRecord As sales_file
        Protected bc As BusinessContext = Nothing

        ' TRUE if the edit operation changed a field in the record.
        Private PropertyChangedEventOccured As Boolean

        ' Generate the menu for the grid control
        Protected ControlRow As Int32 = -1

        ''' <summary>
        ''' Is the current record read-only?
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private ReadOnly Property [ReadOnly] As Boolean
            Get
                Return SalesFileRecord Is Nothing OrElse SalesFileRecord.date_exported.HasValue
            End Get
        End Property

        ''' <summary>
        ''' Process the initial creation of the class
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        ''' <summary>
        ''' Add the event handler registrations
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub RegisterHandlers()
            AddHandler DoubleClick, AddressOf GridView1_DoubleClick
            AddHandler MouseDown, AddressOf GridView1_MouseDown
            AddHandler GridView1.MouseDown, AddressOf GridView1_MouseDown
            AddHandler LocalMenu.Popup, AddressOf LocalMenu_Popup
            AddHandler EditMenu.Click, AddressOf EditMenu_Click
            AddHandler AddMenu.Click, AddressOf AddMenu_Click
            AddHandler DeleteMenu.Click, AddressOf DeleteMenu_Click
            AddHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
        End Sub

        ''' <summary>
        ''' Remove the event handler registrations
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub UnRegisterHandlers()
            RemoveHandler DoubleClick, AddressOf GridView1_DoubleClick
            RemoveHandler MouseDown, AddressOf GridView1_MouseDown
            RemoveHandler GridView1.MouseDown, AddressOf GridView1_MouseDown
            RemoveHandler LocalMenu.Popup, AddressOf LocalMenu_Popup
            RemoveHandler EditMenu.Click, AddressOf EditMenu_Click
            RemoveHandler AddMenu.Click, AddressOf AddMenu_Click
            RemoveHandler DeleteMenu.Click, AddressOf DeleteMenu_Click
            RemoveHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
        End Sub

        ''' <summary>
        ''' Build the grid control record list from the sales file
        ''' </summary>
        ''' <param name="SalesFileRecord"></param>
        ''' <remarks></remarks>
        Public Overridable Sub Read(bc As BusinessContext, SalesFileRecord As sales_file)
            UnRegisterHandlers()
            Me.SalesFileRecord = SalesFileRecord
            Me.bc = bc

            ' Change the text for the edit menu reflect the read-only status for the record
            If [ReadOnly] Then
                EditMenu.Text = "&Show..."
            Else
                EditMenu.Text = "&Change..."
            End If

            Try
                GridControl1.DataSource = SalesFileRecord.sales_debts.GetNewBindingList()
                GridControl1.RefreshDataSource()
                GridView1.BestFitColumns()
            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Handle the before popup event from the menu. Enable the proper items
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Protected Sub LocalMenu_Popup(ByVal sender As Object, ByVal e As EventArgs)
            AddMenu.Enabled = Not [ReadOnly]

            ' Find the row that is currently selected. If available, enable edit and delete.
            If ControlRow >= 0 Then
                Dim debt As sales_debt = TryCast(GridView1.GetRow(ControlRow), sales_debt)
                If debt IsNot Nothing Then
                    DeleteMenu.Enabled = Not [ReadOnly]
                    EditMenu.Enabled = True
                    Return
                End If
            End If

            DeleteMenu.Enabled = False
            EditMenu.Enabled = False
        End Sub

        ''' <summary>
        ''' Process an ADD (create) event from the popup menu
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub AddMenu_Click(ByVal sender As Object, ByVal e As EventArgs)
            If Not [ReadOnly] Then
                CreateNewRecord()
            End If
        End Sub

        ''' <summary>
        ''' Process a DELETE event from the popup menu
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub DeleteMenu_Click(ByVal sender As Object, ByVal e As EventArgs)
            If Not [ReadOnly] Then
                DeleteRecord(GridView1.GetRow(ControlRow))
            End If
        End Sub

        ''' <summary>
        ''' Process an EDIT event from the popup menu
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub EditMenu_Click(ByVal sender As Object, ByVal e As EventArgs)
            If [ReadOnly] Then
                ShowRecord(GridView1.GetRow(ControlRow))
            Else
                EditRecord(GridView1.GetRow(ControlRow))
            End If
        End Sub

        ''' <summary>
        ''' Create a new record for the list
        ''' </summary>
        ''' <remarks></remarks>
        Protected Sub CreateNewRecord()

            ' Create a new record
            Using fac As New DebtPlus.LINQ.Factory()
                Dim salesDebtRecord As sales_debt = fac.Manufacture_sales_debt()

                ' Edit the new record
                Using frm As New Form_Comparison_Edit(salesDebtRecord)
                    If frm.ShowDialog() <> DialogResult.OK Then
                        Return
                    End If
                End Using

                ' Add the record to the collection. It will insert it when we update the record.
                SalesFileRecord.sales_debts.Add(salesDebtRecord)
                GridControl1.DataSource = SalesFileRecord.sales_debts.GetNewBindingList()

                ' Since this is a new debt, recalculate the payout information
                SalesFileRecord.DMP_NeedToRecalculate = True
                SalesFileRecord.SELF_NeedToRecalculate = True
            End Using

            ' Ensure that the grid information is refreshed
            GridControl1.RefreshDataSource()
            GridView1.RefreshData()
        End Sub

        ''' <summary>
        ''' Delete the record from the database collection.
        ''' </summary>
        ''' <param name="obj"></param>
        ''' <remarks></remarks>
        Protected Sub DeleteRecord(ByVal obj As Object)

            Dim record As sales_debt = TryCast(obj, sales_debt)
            If record Is Nothing Then
                Return
            End If

            ' Delete the record from the list. It will be deleted when we update the list.
            SalesFileRecord.sales_debts.Remove(record)

            ' Since this is a removed debt, recalculate the payout information
            SalesFileRecord.DMP_NeedToRecalculate = True
            SalesFileRecord.SELF_NeedToRecalculate = True

            ' Ensure that the grid information is refreshed
            GridControl1.DataSource = SalesFileRecord.sales_debts.GetNewBindingList()
            GridControl1.RefreshDataSource()
            GridView1.RefreshData()
        End Sub

        ''' <summary>
        ''' Show the current plan record
        ''' </summary>
        ''' <param name="obj"></param>
        ''' <remarks></remarks>
        Protected Sub ShowRecord(ByVal obj As Object)

            Dim record As sales_debt = TryCast(obj, sales_debt)
            If record Is Nothing Then
                Return
            End If

            Using frm As New Form_Comparison_Edit(record)
                frm.ReadOnly = True
                frm.ShowDialog()
            End Using
        End Sub

        ''' <summary>
        ''' Edit the current plan record
        ''' </summary>
        ''' <param name="obj"></param>
        ''' <remarks></remarks>
        Protected Sub EditRecord(ByVal obj As Object)

            Dim record As sales_debt = TryCast(obj, sales_debt)
            If record Is Nothing Then
                Return
            End If

            Using frm As New Form_Comparison_Edit(record)

                ' Display the edit dialog.
                If frm.ShowDialog() <> DialogResult.OK Then

                    ' Since we do the updates immediately when the fields change then we need to refresh the record
                    ' from the database should the user wish to cancel the edit. Do this only on existing records
                    ' since newly created records do not exist in the database.
                    If record.Id > 0 Then
                        bc.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, record)
                    End If

                    Return
                End If
            End Using

            ' A change to the record needs to have the payout recalculated.
            SalesFileRecord.DMP_NeedToRecalculate = True
            SalesFileRecord.SELF_NeedToRecalculate = True

            ' Ensure that the grid information is refreshed
            GridControl1.DataSource = SalesFileRecord.sales_debts.GetNewBindingList()
            GridControl1.RefreshDataSource()
            GridView1.RefreshData()
        End Sub

        ''' <summary>
        ''' Process a double-click event on the grid control to edit the record.
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Protected Sub GridView1_DoubleClick(ByVal sender As Object, ByVal e As EventArgs)

            ' Find the row targeted as the double-click item
            Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = GridView1.CalcHitInfo((GridControl1.PointToClient(System.Windows.Forms.Control.MousePosition)))
            ControlRow = hi.RowHandle
            If Not hi.IsValid OrElse ControlRow < 0 Then
                Return
            End If

            ' Do the edit operation
            If [ReadOnly] Then
                ShowRecord(GridView1.GetRow(ControlRow))
            Else
                EditRecord(GridView1.GetRow(ControlRow))
            End If
        End Sub

        ''' <summary>
        ''' Process a mouse-down event on the grid control to control the pop-up events
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Protected Sub GridView1_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs)
            Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = GridView1.CalcHitInfo(New Point(e.X, e.Y))
            ControlRow = If(hi.IsValid, hi.RowHandle, -1)
        End Sub
    End Class
End Namespace