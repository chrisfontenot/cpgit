#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.Svc.Debt
Imports DebtPlus.Svc.DataSets
Imports DebtPlus.UI.Client.Service
Imports DebtPlus.Events
Imports DevExpress.XtraBars
Imports System.Drawing.Printing
Imports DebtPlus.Data.Forms
Imports System.Data.SqlClient
Imports System.ComponentModel
Imports DebtPlus.LINQ
Imports System.Linq

Namespace forms

    Friend Class Form_BottomLine
        Implements IContext

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Private bc As BusinessContext = Nothing
        Private colRecords As System.Collections.Generic.List(Of BottomLineData)
        Private ReadOnly _privateContext As ClientUpdateClass = Nothing
        Private Shared privateInstance As Form_BottomLine = Nothing

        Private Delegate Sub SetData(ByVal lineNo As Int32, ByVal value As System.Nullable(Of Decimal))

        Private Enum RecordOrder As Integer
            ln_INCOME_AMOUNTS = 0
            ln_Applicant_Net_Pay
            ln_CoApplicant_Net_Pay
            ln_Other_Monthly_Income
            ln_Total_Income
            ln_EXPENSE_AMOUNTS
            ln_Housing_Expenses
            ln_Living_Expenses
            ln_Non_Managed_Debt_Payments
            ln_Subtotal_Expenses
            ln_Secured_Loan_Payments
            ln_Plan_Payments
            ln_Total_Expenses
            ln_Surplus_Deficit
        End Enum

        Private Enum RecordRendering As Integer
            normal = 0
            bold = 1
            teal = 2
            teal_bold = 3
            pink = 4
            pink_bold = 5
        End Enum

        ''' <summary>
        ''' Detail information for the bottom line is a collection of records that are of this class
        ''' </summary>
        Public Class BottomLineData
            Implements System.ComponentModel.INotifyPropertyChanged

            Public Event PropertyChanged As System.ComponentModel.PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
            Protected Sub RaisePropertyChanged(ByVal e As System.ComponentModel.PropertyChangedEventArgs)
                RaiseEvent PropertyChanged(Me, e)
            End Sub
            Protected Overridable Sub OnPropertyChanged(ByVal e As System.ComponentModel.PropertyChangedEventArgs)
                RaisePropertyChanged(e)
            End Sub
            Protected Sub RaisePropertyChanged(ByVal PropertyName As String)
                OnPropertyChanged(New System.ComponentModel.PropertyChangedEventArgs(PropertyName))
            End Sub

            Public Sub New()
            End Sub
            Public Sub New(description As String)
                Me.New(description, RecordRendering.normal, Nothing, Nothing)
            End Sub
            Public Sub New(description As String, ByVal rendering As Int32)
                Me.New(description, rendering, Nothing, Nothing)
            End Sub
            Public Sub New(description As String, rendering As Int32, current As System.Nullable(Of Decimal), suggested As System.Nullable(Of Decimal))
                Me.description = description
                Me.rendering = rendering
                Me.current = current
                Me.suggested = suggested
            End Sub

            Private _rendering As Int32 = 0
            Public Property rendering As Int32
                Get
                    Return _rendering
                End Get
                Set(value As Int32)
                    If _rendering <> value Then
                        _rendering = value
                        RaisePropertyChanged("rendering")
                    End If
                End Set
            End Property

            Private _description As String

            Public Property description As String
                Get
                    Return _description
                End Get
                Set(value As String)
                    If (value Is Nothing AndAlso _description Is Nothing) Then
                        Return
                    End If
                    If (value IsNot Nothing AndAlso _description IsNot Nothing) Then
                        If value.Equals(description) Then Return
                    End If
                    _description = value
                    RaisePropertyChanged("description")
                End Set
            End Property

            Private _current As System.Nullable(Of Decimal)
            Public Property current As System.Nullable(Of Decimal)
                Get
                    Return _current
                End Get
                Set(value As System.Nullable(Of Decimal))
                    If System.Nullable.Compare(Of Decimal)(value, _current) <> 0 Then
                        _current = value
                        RaisePropertyChanged("current")
                    End If
                End Set
            End Property

            Private _suggested As System.Nullable(Of Decimal)
            Public Property suggested As System.Nullable(Of Decimal)
                Get
                    Return _suggested
                End Get
                Set(value As System.Nullable(Of Decimal))
                    If System.Nullable.Compare(Of Decimal)(value, _suggested) <> 0 Then
                        _suggested = value
                        RaisePropertyChanged("suggested")
                    End If
                End Set
            End Property
        End Class

        Private Function getDataSource() As System.Collections.Generic.List(Of BottomLineData)
            colRecords = New System.Collections.Generic.List(Of BottomLineData)
            colRecords.AddRange(New BottomLineData() _
                {
                    New BottomLineData("INCOME AMOUNTS", RecordRendering.bold),
                    New BottomLineData("Applicant Net Pay"),
                    New BottomLineData("Co-Applicant Net Pay"),
                    New BottomLineData("Other Monthly Income"),
                    New BottomLineData("Total Income", RecordRendering.teal_bold),
                    New BottomLineData("EXPENSE AMOUNTS", RecordRendering.bold),
                    New BottomLineData("Housing Expenses"),
                    New BottomLineData("Living Expenses"),
                    New BottomLineData("Non-Managed Debt Payments"),
                    New BottomLineData("Subtotal Expenses", RecordRendering.teal_bold),
                    New BottomLineData("Secured Loan Payments"),
                    New BottomLineData("Plan Payments"),
                    New BottomLineData("Total Expenses", RecordRendering.teal_bold),
                    New BottomLineData("Surplus/Deficit", RecordRendering.bold)
                })

            Return colRecords
        End Function

        Private Sub RegisterHandlers()
            AddHandler FormClosed, AddressOf Form_BottomLine_FormClosed
            AddHandler Load, AddressOf Form_BottomLine_Load
            AddHandler BarButtonItem_Print.ItemClick, AddressOf BarButtonItem_Print_ItemClick
            AddHandler BarButtonItem_PrintPreview.ItemClick, AddressOf BarButtonItem_PrintPreview_ItemClick
            AddHandler BarButtonItem_Exit.ItemClick, AddressOf BarButtonItem2_ItemClick
            AddHandler Disposed, AddressOf Form_BottomLine_Disposed
            AddHandler BarCheckItem_StayOnTop.CheckedChanged, AddressOf BarCheckItem_StayOnTop_CheckedChanged
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler FormClosed, AddressOf Form_BottomLine_FormClosed
            RemoveHandler Load, AddressOf Form_BottomLine_Load
            RemoveHandler BarButtonItem_Print.ItemClick, AddressOf BarButtonItem_Print_ItemClick
            RemoveHandler BarButtonItem_PrintPreview.ItemClick, AddressOf BarButtonItem_PrintPreview_ItemClick
            RemoveHandler BarButtonItem_Exit.ItemClick, AddressOf BarButtonItem2_ItemClick
            RemoveHandler Disposed, AddressOf Form_BottomLine_Disposed
            RemoveHandler BarCheckItem_StayOnTop.CheckedChanged, AddressOf BarCheckItem_StayOnTop_CheckedChanged
        End Sub

        Private ReadOnly Property NonManagedExpenses() As Decimal
            Get
                If Context.ClientDs.colOtherDebts IsNot Nothing AndAlso Context.ClientDs.colOtherDebts.Count > 0 Then
                    Dim ans As Decimal = Convert.ToDecimal(Context.ClientDs.colOtherDebts.Sum(Function(s) s.payment))
                    Return ans
                End If
                Return 0D
            End Get
        End Property

        Private ReadOnly Property NetIncome_1() As Decimal
            Get
                Dim personRecord As DebtPlus.LINQ.people = Context.ClientDs.colPeople.Find(Function(s) s.Relation = 1)
                If personRecord IsNot Nothing Then
                    Return DebtPlus.LINQ.BusinessContext.PayAmount(personRecord.net_income, personRecord.Frequency)
                End If

                Return 0D
            End Get
        End Property

        ''' <summary>
        ''' Co-Applicant NET income
        ''' </summary>
        Private ReadOnly Property NetIncome_2() As Decimal
            Get
                Dim personRecord As DebtPlus.LINQ.people = Context.ClientDs.colPeople.Find(Function(s) s.Relation <> 1)
                If personRecord IsNot Nothing Then
                    Return DebtPlus.LINQ.BusinessContext.PayAmount(personRecord.net_income, personRecord.Frequency)
                End If

                Return 0D
            End Get
        End Property

        Private ReadOnly Property OtherIncome() As Decimal
            Get
                Try
                    If Context.ClientDs.colAssets Is Nothing Then
                        Context.ClientDs.colAssets = bc.assets.Where(Function(s) s.client = Context.ClientDs.ClientId).ToList()
                    End If

                    Return Context.ClientDs.colAssets.Sum(Function(s) s.asset_amount)

                Catch ex As System.Data.SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
                End Try

                Return 0D
            End Get
        End Property

        Private ReadOnly Property DisbursementFactor() As Decimal
            Get
                Return Context.DebtRecords.Cast(Of DebtRecord).Where(Function(s) s.DebtType <> Interfaces.Debt.DebtTypeEnum.SetupFee).Sum(Function(s) s.disbursement_factor)
            End Get
        End Property

        Private ReadOnly Property NonDmpPayment() As Decimal
            Get
                Return Context.DebtRecords.Cast(Of DebtRecord).Where(Function(s) s.DebtType <> Interfaces.Debt.DebtTypeEnum.SetupFee).Sum(Function(s) s.non_dmp_payment)
            End Get
        End Property

        Public ReadOnly Property Context() As ClientUpdateClass Implements IContext.Context
            Get
                Return _privateContext
            End Get
        End Property

        ' These two fields are taken from the current budget
        Private _cur_housingExpenses As Decimal = 0D
        Private _cur_livingExpenses As Decimal = 0D
        Private _sug_housingExpenses As Decimal = 0D
        Private _sug_livingExpenses As Decimal = 0D
        Private Const SecuredExpenses As Decimal = 0D

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(bc As BusinessContext)
            MyClass.New()
            Me.bc = bc
            RegisterHandlers()
        End Sub

        Public Sub New(ByVal Context As ClientUpdateClass)
            MyClass.New(Context.ClientDs.bc)
            _privateContext = Context
            Context.ClientDs.RegisterHandler(AddressOf UpdatedFields)
        End Sub

        ''' <summary>
        ''' Ensure that our form is shown only once per application
        ''' </summary>
        Public Shared Function Instance(ByVal context As ClientUpdateClass) As Form_BottomLine
            If privateInstance Is Nothing OrElse privateInstance.IsDisposed Then privateInstance = New Form_BottomLine(context)
            With privateInstance
                .WindowState = FormWindowState.Normal
                .BringToFront()
            End With
            Return privateInstance
        End Function

        ''' <summary>
        ''' When the form terminates, remove our handler
        ''' </summary>
        Private Sub Form_BottomLine_FormClosed(ByVal sender As Object, ByVal e As FormClosedEventArgs)
            Context.ClientDs.DeRegisterHandler(AddressOf UpdatedFields)
        End Sub

        Private Sub Form_BottomLine_Load(ByVal sender As Object, ByVal e As EventArgs)
            UnRegisterHandlers()
            Try
                ' Load the form position
                LoadPlacement("Client.BottomLine")

                ' Determine if the form is to be the topmost form
                TopMost = DebtPlus.Configuration.Config.ClientBottomLineFormAlwaysOnTop
                BarCheckItem_StayOnTop.Checked = TopMost

                ' Display the information for the form and hook into the table changes
                ReloadBudgetInformation()
                RebuildInformation()

            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Class BudgetDetails
            Public Property budget As Int32
            Public Property client_amount As System.Nullable(Of Decimal)
            Public Property suggested_amount As System.Nullable(Of Decimal)
            Public Property housing_expense As System.Nullable(Of Boolean)
        End Class

        ''' <summary>
        ''' Called if the budget update is canceled or we are loading
        ''' </summary>
        Private Sub ReloadBudgetInformation()

            Try
                Dim budgetRecord As DebtPlus.LINQ.budget = bc.budgets.Where(Function(s) s.client = Context.ClientDs.ClientId).OrderByDescending(Function(s) s.date_created).FirstOrDefault()
                If budgetRecord IsNot Nothing Then
                    Dim details As System.Collections.Generic.List(Of BudgetDetails) = (From bd In bc.budget_details Group Join cat In bc.budget_categories On bd.budget_category Equals cat.Id Into detailGroup = Group From detailInfo In detailGroup.DefaultIfEmpty() Select New BudgetDetails() With {.budget = bd.budget, .client_amount = bd.client_amount, .suggested_amount = bd.suggested_amount, .housing_expense = detailInfo.housing_expense}).Where(Function(s) s.budget = budgetRecord.Id).ToList()

                    _sug_livingExpenses = details.Sum(Function(s) s.suggested_amount).GetValueOrDefault(0D)
                    _sug_housingExpenses = details.FindAll(Function(s) s.housing_expense.GetValueOrDefault(False)).Sum(Function(s) s.suggested_amount).GetValueOrDefault(0D)
                    _sug_livingExpenses -= _sug_housingExpenses

                    _cur_livingExpenses = details.Sum(Function(s) s.client_amount).GetValueOrDefault(0D)
                    _cur_housingExpenses = details.FindAll(Function(s) s.housing_expense.GetValueOrDefault(False)).Sum(Function(s) s.client_amount).GetValueOrDefault(0D)
                    _cur_livingExpenses -= _cur_housingExpenses
                    Return
                End If

            Catch ex As SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading budget information")
            End Try

            _sug_livingExpenses = 0D
            _sug_housingExpenses = 0D
            _cur_livingExpenses = 0D
            _cur_housingExpenses = 0D
        End Sub

        Private Sub Form_BottomLine_Disposed(ByVal sender As Object, ByVal e As EventArgs)
            privateInstance = Nothing
        End Sub

        Private Sub RebuildInformation()

            If InvokeRequired Then
                BeginInvoke(New MethodInvoker(AddressOf RebuildInformation))
            Else
                Using cm As New DebtPlus.UI.Common.CursorManager()
                    GridControl1.DataSource = getDataSource()

                    ' Current (client) values
                    Calc_Totals(Sub(lineNo As Int32, value As System.Nullable(Of Decimal)) colRecords.Item(lineNo).current = value,
                                NetIncome_1, NetIncome_2, OtherIncome,
                                _cur_livingExpenses, _cur_housingExpenses, NonManagedExpenses,
                                SecuredExpenses, NonDmpPayment)

                    ' Suggested (counselor) values
                    Calc_Totals(Sub(lineNo As Int32, value As System.Nullable(Of Decimal)) colRecords.Item(lineNo).suggested = value,
                                NetIncome_1, NetIncome_2, OtherIncome,
                                _sug_livingExpenses, _sug_housingExpenses, NonManagedExpenses,
                                SecuredExpenses, DisbursementFactor)

                    ' Refresh the grid data source to force the grid to update
                    GridControl1.RefreshDataSource()
                End Using
            End If
        End Sub

        Private Sub Calc_Totals(ByVal setter As SetData, NetIncome_1 As System.Nullable(Of Decimal), NetIncome_2 As System.Nullable(Of Decimal), OtherIncome As System.Nullable(Of Decimal), livingExpenses As System.Nullable(Of Decimal), housingExpenses As System.Nullable(Of Decimal), NonManagedExpenses As System.Nullable(Of Decimal), SecuredExpenses As System.Nullable(Of Decimal), UnsecuredExpenses As System.Nullable(Of Decimal))
            Dim totalIncome As Decimal = NetIncome_1.GetValueOrDefault(0D) + NetIncome_2.GetValueOrDefault(0D) + OtherIncome.GetValueOrDefault(0D)
            Dim subtotalExpenses As Decimal = housingExpenses.GetValueOrDefault(0D) + livingExpenses.GetValueOrDefault(0D) + NonManagedExpenses.GetValueOrDefault(0D)
            Dim totalExpenses As Decimal = subtotalExpenses + SecuredExpenses.GetValueOrDefault(0D) + UnsecuredExpenses.GetValueOrDefault(0D)
            Dim surplus_deficit As Decimal = totalIncome - totalExpenses

            setter(RecordOrder.ln_Applicant_Net_Pay, NetIncome_1)
            setter(RecordOrder.ln_CoApplicant_Net_Pay, NetIncome_2)
            setter(RecordOrder.ln_Other_Monthly_Income, OtherIncome)
            setter(RecordOrder.ln_Total_Income, totalIncome)

            setter(RecordOrder.ln_Housing_Expenses, housingExpenses)
            setter(RecordOrder.ln_Living_Expenses, livingExpenses)
            setter(RecordOrder.ln_Non_Managed_Debt_Payments, NonManagedExpenses)
            setter(RecordOrder.ln_Subtotal_Expenses, subtotalExpenses)

            setter(RecordOrder.ln_Secured_Loan_Payments, SecuredExpenses)
            setter(RecordOrder.ln_Plan_Payments, UnsecuredExpenses)

            setter(RecordOrder.ln_Total_Expenses, totalExpenses)

            setter(RecordOrder.ln_Surplus_Deficit, surplus_deficit)
        End Sub

        Private Sub UpdatedFields(ByVal Sender As Object, ByVal e As FieldChangedEventArgs)
            Select Case FieldChangedEventArgs.Table(e).ToLower()
                Case "people"
                    UpdatedFields_People(e)
                Case "view_debt_info"
                    UpdatedFields_Debts(e)
                Case "assets"
                    RebuildInformation()
                Case "secured_loans"
                    RebuildInformation()
                Case "client_other_debts"
                    RebuildInformation()
                Case "reload_budgets"
                    ReloadBudgetInformation()
                    RebuildInformation()
            End Select
        End Sub

        Private Sub UpdatedFields_People(ByVal e As FieldChangedEventArgs)
            If e.FieldName = "people:net_income" OrElse e.FieldName = "people:Frequency" Then RebuildInformation()
        End Sub

        Private Sub UpdatedFields_Debts(ByVal e As FieldChangedEventArgs)
            If e.FieldName = "view_debt_info:disbursement_factor" Then RebuildInformation()
        End Sub

        Private Sub BarButtonItem_Print_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Try
                If GridControl1.IsPrintingAvailable() Then
                    GridControl1.Print()
                End If

            Catch ex As InvalidPrinterException
                MessageBox.Show(ex.Message, "Printing Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        End Sub

        Private Sub BarButtonItem_PrintPreview_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Try
                If GridControl1.IsPrintingAvailable() Then
                    GridControl1.ShowPrintPreview()
                End If

            Catch ex As InvalidPrinterException
                MessageBox.Show(ex.Message, "Printing Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        End Sub

        Private Sub BarButtonItem2_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Close()
        End Sub

        Private Sub BarCheckItem_StayOnTop_CheckedChanged(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            TopMost = BarCheckItem_StayOnTop.Checked
            DebtPlus.Configuration.Config.ClientBottomLineFormAlwaysOnTop = TopMost
        End Sub

        ''' <summary>
        ''' Rebuild the bottom line form information based upon the new data in the database
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub RefreshData()
            If privateInstance IsNot Nothing Then
                privateInstance.RebuildInformation()
            End If
        End Sub
    End Class
End Namespace
