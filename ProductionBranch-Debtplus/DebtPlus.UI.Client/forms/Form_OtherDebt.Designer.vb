﻿Namespace forms
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form_OtherDebt
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_OtherDebt))
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
            Me.TextEdit_Creditor = New DevExpress.XtraEditors.TextEdit
            Me.TextEdit_AccountNumber = New DevExpress.XtraEditors.TextEdit
            Me.CalcEdit_Balance = New DevExpress.XtraEditors.CalcEdit
            Me.CalcEdit_Payment = New DevExpress.XtraEditors.CalcEdit
            Me.CalcEdit_APR = New DebtPlus.Data.Controls.PercentEdit
            Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl
            Me.SimpleButton_OK = New DevExpress.XtraEditors.SimpleButton
            Me.SimpleButton_Cancel = New DevExpress.XtraEditors.SimpleButton
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_Creditor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_AccountNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_Balance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_Payment.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_APR.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Appearance.Options.UseTextOptions = True
            Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl1.Location = New System.Drawing.Point(12, 12)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(268, 57)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = resources.GetString("LabelControl1.Text")
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(12, 91)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(39, 13)
            Me.LabelControl2.TabIndex = 1
            Me.LabelControl2.Text = "Creditor"
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(13, 117)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(79, 13)
            Me.LabelControl3.TabIndex = 3
            Me.LabelControl3.Text = "Account Number"
            '
            'LabelControl4
            '
            Me.LabelControl4.Location = New System.Drawing.Point(13, 143)
            Me.LabelControl4.Name = "LabelControl4"
            Me.LabelControl4.Size = New System.Drawing.Size(37, 13)
            Me.LabelControl4.TabIndex = 5
            Me.LabelControl4.Text = "Balance"
            '
            'LabelControl5
            '
            Me.LabelControl5.Location = New System.Drawing.Point(12, 169)
            Me.LabelControl5.Name = "LabelControl5"
            Me.LabelControl5.Size = New System.Drawing.Size(42, 13)
            Me.LabelControl5.TabIndex = 7
            Me.LabelControl5.Text = "Payment"
            '
            'TextEdit_Creditor
            '
            Me.TextEdit_Creditor.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                                 Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.TextEdit_Creditor.Location = New System.Drawing.Point(98, 88)
            Me.TextEdit_Creditor.Name = "TextEdit_Creditor"
            Me.TextEdit_Creditor.Properties.MaxLength = 50
            Me.TextEdit_Creditor.Size = New System.Drawing.Size(182, 20)
            Me.TextEdit_Creditor.TabIndex = 2
            '
            'TextEdit_AccountNumber
            '
            Me.TextEdit_AccountNumber.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                                      Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.TextEdit_AccountNumber.Location = New System.Drawing.Point(98, 114)
            Me.TextEdit_AccountNumber.Name = "TextEdit_AccountNumber"
            Me.TextEdit_AccountNumber.Properties.MaxLength = 50
            Me.TextEdit_AccountNumber.Size = New System.Drawing.Size(182, 20)
            Me.TextEdit_AccountNumber.TabIndex = 4
            '
            'CalcEdit_Balance
            '
            Me.CalcEdit_Balance.Location = New System.Drawing.Point(98, 140)
            Me.CalcEdit_Balance.Name = "CalcEdit_Balance"
            Me.CalcEdit_Balance.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit_Balance.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_Balance.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.CalcEdit_Balance.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_Balance.Properties.AppearanceDropDown.Options.UseTextOptions = True
            Me.CalcEdit_Balance.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_Balance.Properties.AppearanceFocused.Options.UseTextOptions = True
            Me.CalcEdit_Balance.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_Balance.Properties.AppearanceReadOnly.Options.UseTextOptions = True
            Me.CalcEdit_Balance.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_Balance.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_Balance.Properties.DisplayFormat.FormatString = "c"
            Me.CalcEdit_Balance.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_Balance.Properties.EditFormat.FormatString = "c"
            Me.CalcEdit_Balance.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_Balance.Properties.Mask.EditMask = "n2"
            Me.CalcEdit_Balance.Properties.Mask.SaveLiteral = False
            Me.CalcEdit_Balance.Properties.Precision = 2
            Me.CalcEdit_Balance.Size = New System.Drawing.Size(100, 20)
            Me.CalcEdit_Balance.TabIndex = 6
            '
            'CalcEdit_Payment
            '
            Me.CalcEdit_Payment.Location = New System.Drawing.Point(98, 166)
            Me.CalcEdit_Payment.Name = "CalcEdit_Payment"
            Me.CalcEdit_Payment.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit_Payment.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_Payment.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.CalcEdit_Payment.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_Payment.Properties.AppearanceDropDown.Options.UseTextOptions = True
            Me.CalcEdit_Payment.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_Payment.Properties.AppearanceFocused.Options.UseTextOptions = True
            Me.CalcEdit_Payment.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_Payment.Properties.AppearanceReadOnly.Options.UseTextOptions = True
            Me.CalcEdit_Payment.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_Payment.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_Payment.Properties.DisplayFormat.FormatString = "c"
            Me.CalcEdit_Payment.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_Payment.Properties.EditFormat.FormatString = "c"
            Me.CalcEdit_Payment.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_Payment.Properties.Mask.EditMask = "n2"
            Me.CalcEdit_Payment.Properties.Mask.SaveLiteral = False
            Me.CalcEdit_Payment.Properties.Precision = 2
            Me.CalcEdit_Payment.Size = New System.Drawing.Size(100, 20)
            Me.CalcEdit_Payment.TabIndex = 8
            '
            'CalcEdit_APR
            '
            Me.CalcEdit_APR.Location = New System.Drawing.Point(98, 192)
            Me.CalcEdit_APR.Name = "CalcEdit_APR"
            Me.CalcEdit_APR.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit_APR.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_APR.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.CalcEdit_APR.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_APR.Properties.AppearanceDropDown.Options.UseTextOptions = True
            Me.CalcEdit_APR.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_APR.Properties.AppearanceFocused.Options.UseTextOptions = True
            Me.CalcEdit_APR.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_APR.Properties.AppearanceReadOnly.Options.UseTextOptions = True
            Me.CalcEdit_APR.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_APR.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_APR.Properties.DisplayFormat.FormatString = "{0:p}"
            Me.CalcEdit_APR.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_APR.Properties.EditFormat.FormatString = "{0:p}"
            Me.CalcEdit_APR.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_APR.Size = New System.Drawing.Size(100, 20)
            Me.CalcEdit_APR.TabIndex = 10
            '
            'LabelControl6
            '
            Me.LabelControl6.Location = New System.Drawing.Point(12, 195)
            Me.LabelControl6.Name = "LabelControl6"
            Me.LabelControl6.Size = New System.Drawing.Size(32, 13)
            Me.LabelControl6.TabIndex = 9
            Me.LabelControl6.Text = "A.P.R."
            '
            'SimpleButton_OK
            '
            Me.SimpleButton_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.SimpleButton_OK.Location = New System.Drawing.Point(58, 231)
            Me.SimpleButton_OK.Name = "SimpleButton_OK"
            Me.SimpleButton_OK.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.TabIndex = 11
            Me.SimpleButton_OK.Text = "&OK"
            '
            'SimpleButton_Cancel
            '
            Me.SimpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.SimpleButton_Cancel.Location = New System.Drawing.Point(159, 231)
            Me.SimpleButton_Cancel.Name = "SimpleButton_Cancel"
            Me.SimpleButton_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.TabIndex = 12
            Me.SimpleButton_Cancel.Text = "&Cancel"
            '
            'Form_OtherDebt
            '
            Me.AcceptButton = Me.SimpleButton_OK
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.SimpleButton_Cancel
            Me.ClientSize = New System.Drawing.Size(292, 266)
            Me.Controls.Add(Me.SimpleButton_Cancel)
            Me.Controls.Add(Me.SimpleButton_OK)
            Me.Controls.Add(Me.CalcEdit_APR)
            Me.Controls.Add(Me.LabelControl6)
            Me.Controls.Add(Me.CalcEdit_Payment)
            Me.Controls.Add(Me.CalcEdit_Balance)
            Me.Controls.Add(Me.TextEdit_AccountNumber)
            Me.Controls.Add(Me.TextEdit_Creditor)
            Me.Controls.Add(Me.LabelControl5)
            Me.Controls.Add(Me.LabelControl4)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LabelControl1)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
            Me.MaximizeBox = False
            Me.Name = "Form_OtherDebt"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "Other Non-Managed Debts"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_Creditor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_AccountNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_Balance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_Payment.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_APR.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents TextEdit_Creditor As DevExpress.XtraEditors.TextEdit
        Friend WithEvents TextEdit_AccountNumber As DevExpress.XtraEditors.TextEdit
        Friend WithEvents CalcEdit_Balance As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents CalcEdit_Payment As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents CalcEdit_APR As DebtPlus.Data.Controls.PercentEdit
        Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents SimpleButton_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton_Cancel As DevExpress.XtraEditors.SimpleButton
    End Class
End Namespace