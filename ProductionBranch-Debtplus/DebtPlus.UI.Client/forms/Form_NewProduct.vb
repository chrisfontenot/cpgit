﻿Imports DebtPlus.LINQ
Imports System.Linq

Namespace forms
    Friend Class Form_NewProduct
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        Private bc As BusinessContext = Nothing
        Private vendorRecord As DebtPlus.LINQ.vendor = Nothing
        Private clientState As String = String.Empty
        Private record As client_product = Nothing
        Private clientRecord As DebtPlus.LINQ.client = Nothing

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(bc As BusinessContext, clientRecord As DebtPlus.LINQ.client, record As client_product)
            MyClass.New()
            Me.bc = bc
            Me.record = record
            Me.clientRecord = clientRecord
            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Me.Load, AddressOf Form_NewProduct_Load
            AddHandler Me.Resize, AddressOf Form_NewProduct_Resize
            AddHandler VendorID1.Validating, AddressOf VendorID1_Validating
            AddHandler LookUpEdit_ProductType.EditValueChanged, AddressOf Form_Changed
            AddHandler TextEdit_ReferenceNumber.EditValueChanged, AddressOf Form_Changed
            AddHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler Me.Load, AddressOf Form_NewProduct_Load
            RemoveHandler Me.Resize, AddressOf Form_NewProduct_Resize
            RemoveHandler VendorID1.Validating, AddressOf VendorID1_Validating
            RemoveHandler LookUpEdit_ProductType.EditValueChanged, AddressOf Form_Changed
            RemoveHandler TextEdit_ReferenceNumber.EditValueChanged, AddressOf Form_Changed
            RemoveHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
        End Sub

        Private Sub Form_Changed(sender As Object, e As System.EventArgs)
            SimpleButton_OK.Enabled = Not HasErrors()
        End Sub

        Private Sub Form_NewProduct_Load(sender As Object, e As System.EventArgs)
            UnRegisterHandlers()
            Try
                EmptySpaceItem_Left.Width = (EmptySpaceItem_Left.Width + EmptySpaceItem_Right.Width) \ 2

                vendorRecord = record.vendor1
                If vendorRecord IsNot Nothing Then
                    VendorID1.EditValue = vendorRecord.Id
                    LookUpEdit_ProductType.Properties.DataSource = vendorRecord.vendor_products.GetNewBindingList()
                    LookUpEdit_ProductType.EditValue = record.product
                    LabelControl_VendorName.Text = vendorRecord.Name
                Else
                    VendorID1.EditValue = Nothing
                    LookUpEdit_ProductType.Properties.DataSource = Nothing
                    LookUpEdit_ProductType.EditValue = Nothing
                    LabelControl_VendorName.Text = String.Empty
                End If

                TextEdit_ReferenceNumber.EditValue = record.reference
                SimpleButton_OK.Enabled = Not HasErrors()
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Function HasErrors() As Boolean

            If VendorID1.EditValue Is Nothing Then
                Return True
            End If

            If LookUpEdit_ProductType.EditValue Is Nothing Then
                Return True
            End If

            Return False
        End Function

        Private Sub VendorID1_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs)
            UnRegisterHandlers()
            Try
                If Not VendorID1.EditValue.HasValue Then
                    LookUpEdit_ProductType.Properties.DataSource = Nothing
                    LabelControl_VendorName.Text = String.Empty
                    Return
                End If

                ' Ensure that the vendor exists in the system.
                vendorRecord = bc.vendors.Where(Function(s) s.Id = VendorID1.EditValue.Value).FirstOrDefault()
                If vendorRecord Is Nothing Then
                    LookUpEdit_ProductType.Properties.DataSource = Nothing
                    LabelControl_VendorName.Text = String.Empty
                    Return
                End If

                ' bind the lookup control to the product list for this vendor
                LookUpEdit_ProductType.Properties.DataSource = vendorRecord.vendor_products.Select(Function(s) s.product1).ToList()
                LabelControl_VendorName.Text = vendorRecord.Name

            Finally
                SimpleButton_OK.Enabled = Not HasErrors()
                RegisterHandlers()
            End Try
        End Sub

        Private Sub Form_NewProduct_Resize(sender As Object, e As System.EventArgs)
            UnRegisterHandlers()
            Try
                EmptySpaceItem_Left.Width = (EmptySpaceItem_Left.Width + EmptySpaceItem_Right.Width) \ 2
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Sub SimpleButton_OK_Click(sender As Object, e As System.EventArgs)

            ' Define the standard fields for the record
            record.reference = DebtPlus.Utils.Nulls.v_String(TextEdit_ReferenceNumber.EditValue)
            record.vendor1 = vendorRecord
            record.product1 = CType(LookUpEdit_ProductType.GetSelectedDataRow(), DebtPlus.LINQ.product)

            ' Find the proper balance information
            record.cost = record.product1.Amount
            Dim adr As DebtPlus.LINQ.address = bc.addresses.Where(Function(s) s.Id = clientRecord.AddressID.GetValueOrDefault()).FirstOrDefault()
            If adr IsNot Nothing Then
                Dim override As DebtPlus.LINQ.product_state = bc.product_states.Where(Function(s) s.productID = record.product1.Id AndAlso s.stateID = adr.state).OrderByDescending(Function(s) s.date_created).FirstOrDefault()
                If override IsNot Nothing Then
                    record.cost = override.Amount
                End If
            End If

            ' Finally, accept the dialog
            DialogResult = Windows.Forms.DialogResult.OK
        End Sub
    End Class
End Namespace
