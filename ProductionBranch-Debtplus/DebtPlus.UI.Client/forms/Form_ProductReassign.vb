#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.UI.Client.Service
Imports DebtPlus.LINQ
Imports System.Linq

Namespace forms
    Friend Class Form_ProductReassign
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        ' Values passed to the routine
        Private record As DebtPlus.LINQ.client_product = Nothing
        Private bc As DebtPlus.LINQ.BusinessContext = Nothing
        Public Property VendorID As Int32?

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            VendorID = Nothing
        End Sub

        Public Sub New(bc As DebtPlus.LINQ.BusinessContext, record As DebtPlus.LINQ.client_product)
            MyClass.New()
            Me.bc = bc
            Me.record = record
            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf Form_ProductReassign_Load
            AddHandler LookUpEdit_Vendor.EditValueChanged, AddressOf FormChanged
            AddHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler Load, AddressOf Form_ProductReassign_Load
            RemoveHandler LookUpEdit_Vendor.EditValueChanged, AddressOf FormChanged
            RemoveHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
        End Sub

        ''' <summary>
        ''' Handle the FORM LOAD event
        ''' </summary>
        Private Sub Form_ProductReassign_Load(ByVal sender As Object, ByVal e As EventArgs)

            UnRegisterHandlers()
            Try
                Dim colVendors As System.Collections.Generic.List(Of DebtPlus.LINQ.vendor) = bc.vendors.Join(bc.vendor_products, Function(v) v.Id, Function(p) p.vendor, Function(v, p) New With {Key .v = v, Key .p = p}).Where(Function(x) x.p.product = record.product AndAlso x.v.ActiveFlag AndAlso x.v.Id <> record.vendor).Select(Function(x) x.v).ToList()
                LookUpEdit_Vendor.Properties.DataSource = colVendors
                LookUpEdit_Vendor.EditValue = Nothing

                SimpleButton_OK.Enabled = Not HasErrors()
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Sub FormChanged(sender As Object, e As System.EventArgs)
            SimpleButton_OK.Enabled = Not HasErrors()
        End Sub

        Private Function HasErrors() As Boolean
            If LookUpEdit_Vendor.EditValue Is Nothing Then
                Return True
            End If

            Return False
        End Function

        Private Sub SimpleButton_OK_Click(sender As Object, e As System.EventArgs)
            VendorID = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_Vendor.EditValue)
            DialogResult = Windows.Forms.DialogResult.OK
        End Sub
    End Class
End Namespace
