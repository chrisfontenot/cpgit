#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.ComponentModel
Imports DebtPlus.UI.Creditor.Widgets.Controls
Imports DebtPlus.UI.Client.Service
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports DebtPlus.LINQ
Imports System.Linq

Namespace forms.Reassign
    Friend Class Form_ReassignDebt
        Implements IContext

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Private ReadOnly row As DataRow

        ''' <summary>
        ''' Initialize the form
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler CreditorID_New.Validated, AddressOf CreditorID_new_Validated
            AddHandler TextEdit_new_account_number.Validating, AddressOf TextEdit_new_account_number_Validating
        End Sub

        Public Sub New(ByVal Context As ClientUpdateClass)
            MyClass.New()
            privateContext = Context
        End Sub

        Public Sub New(ByVal context As ClientUpdateClass, ByVal row As DataRow)
            MyClass.New(context)
            Me.row = row
        End Sub

        ''' <summary>
        ''' Pointer to the client update information for the current ClientId
        ''' </summary>
        Private ReadOnly privateContext As ClientUpdateClass

        Public ReadOnly Property Context() As ClientUpdateClass Implements IContext.Context
            Get
                Return privateContext
            End Get
        End Property

        ''' <summary>
        ''' Attempt to validate the creditor ID
        ''' </summary>
        Private Sub CreditorID_new_Validated(ByVal sender As Object, ByVal e As EventArgs)
            Dim Creditor As String = CType(sender, CreditorID).EditValue

            If Not String.IsNullOrEmpty(Creditor) Then
                Using bc As New DebtPlus.LINQ.BusinessContext()
                    LabelControl_creditor_name.Text = bc.creditors.Where(Function(s) s.Id = Creditor).Select(Function(s) s.creditor_name).FirstOrDefault()
                    SimpleButton_OK.Enabled = Not HasErrors()
                End Using
                Return
            End If

            LabelControl_creditor_name.Text = String.Empty
            SimpleButton_OK.Enabled = False
        End Sub

        Private Function HasErrors() As Boolean
            Dim answer As Boolean = Not ValidCreditorID()
            If answer Then
                CreditorID_New.ErrorText = "Invalid Creditor"
            Else
                CreditorID_New.ErrorText = String.Empty

                answer = Not ValidAccountNumber(CreditorID_New.EditValue)
                If answer Then
                    TextEdit_new_account_number.ErrorText = "Invalid account number"
                Else
                    TextEdit_new_account_number.ErrorText = String.Empty
                End If
            End If

            Return answer
        End Function

        Private Function ValidCreditorID() As Boolean
            Return (LabelControl_creditor_name.Text <> String.Empty)
        End Function

        ''' <summary>
        ''' Validate the account number for the creditor
        ''' </summary>
        Private Function ValidAccountNumber(ByVal Creditor As String) As Boolean

            ' There must be a creditor to process one
            If Creditor Is Nothing OrElse Creditor = String.Empty Then Return False

            ' An empty account number is valid for this form.
            If TextEdit_new_account_number.EditValue Is Nothing OrElse TextEdit_new_account_number.EditValue Is DBNull.Value _
                Then Return True

            ' Determine the acceptable masks for the creditor. Match it against the account number.
            Dim answer As Boolean = True
            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim rd As SqlDataReader = Nothing
            Try
                cn.Open()
                Using cmd As SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "SELECT isnull(b.type,'C') as type, isnull(cm.rpps_biller_id,'') as rpps_biller_id FROM creditor_methods cm INNER JOIN banks b on cm.bank = b.bank INNER JOIN creditors cr on cm.creditor = cr.creditor_id WHERE cr.creditor = @creditor AND cm.type = 'EFT'"
                        .Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = Creditor
                        rd = .ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
                    End With
                End Using

                Do While rd.Read
                    Dim type As String = rd.GetString(0)
                    Dim rpps_biller_id As String = rd.GetString(1)
                    Select Case type
                        Case "C"
                            answer = ValidAccountCheck()
                        Case "R"
                            answer = ValidAccountRPPS(rpps_biller_id)
                        Case Else
                            answer = ValidAccountCheck()
                    End Select

                    ' Determine if the mask is valid. If so, stop processing.
                    If answer Then Exit Do
                Loop

            Catch ex As SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading creditor_methods")

            Finally
                If rd IsNot Nothing Then rd.Dispose()
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return answer
        End Function

        ''' <summary>
        ''' Validate the account number for RPPS billers
        ''' </summary>
        Private Function ValidAccountRPPS(ByVal RppsBillerID As String) As Boolean
            Dim answer As Boolean = False
            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Try
                cn.Open()
                Using cmd As SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "SELECT ids.rpps_biller_id FROM rpps_biller_ids ids INNER JOIN rpps_masks m ON ids.rpps_biller_id = m.rpps_biller_id WHERE ids.rpps_biller_id = @rpps_biller_id AND @account_number LIKE dbo.map_rpps_masks(m.mask)"
                        .Parameters.Add("@account_number", SqlDbType.VarChar, 80).Value = TextEdit_new_account_number.Text
                        .Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 20).Value = RppsBillerID
                        Dim objAnswer As Object = .ExecuteScalar
                        If objAnswer IsNot Nothing AndAlso objAnswer IsNot DBNull.Value Then answer = True
                    End With
                End Using

            Catch ex As SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading rpps_biller_ids")

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            Return answer
        End Function

        ''' <summary>
        ''' Validate the account number for check receipients
        ''' </summary>
        Private Function ValidAccountCheck() As Boolean
            Return True
        End Function

        ''' <summary>
        ''' Process the account number validation sequence
        ''' </summary>
        Private Sub TextEdit_new_account_number_Validating(ByVal sender As Object, ByVal e As CancelEventArgs)

            ' Enable or disable the OK button depending upon successful search of the creditor
            SimpleButton_OK.Enabled = Not HasErrors()
        End Sub
    End Class
End Namespace
