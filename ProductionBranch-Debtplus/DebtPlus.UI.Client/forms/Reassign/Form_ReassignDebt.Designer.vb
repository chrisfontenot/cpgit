﻿
Imports DebtPlus.UI.Creditor.Widgets.Controls
Imports DebtPlus.UI.Creditor.Widgets

Namespace forms.Reassign

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form_ReassignDebt
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_ReassignDebt))
            Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject
            Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip
            Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem
            Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.TextEdit_new_account_number = New DevExpress.XtraEditors.TextEdit
            Me.LabelControl_creditor_name = New DevExpress.XtraEditors.LabelControl
            Me.SimpleButton_OK = New DevExpress.XtraEditors.SimpleButton
            Me.SimpleButton_Cancel = New DevExpress.XtraEditors.SimpleButton
            Me.CreditorID_New = New CreditorID
            Me.CheckEdit_NewProposals = New DevExpress.XtraEditors.CheckEdit
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_new_account_number.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CreditorID_New.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_NewProposals.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(13, 16)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(77, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "New C&reditor ID"
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(13, 43)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(103, 13)
            Me.LabelControl2.TabIndex = 3
            Me.LabelControl2.Text = "New &Account Number"
            '
            'TextEdit_new_account_number
            '
            Me.TextEdit_new_account_number.Location = New System.Drawing.Point(122, 40)
            Me.TextEdit_new_account_number.Name = "TextEdit_new_account_number"
            Me.TextEdit_new_account_number.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.TextEdit_new_account_number.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.TextEdit_new_account_number.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong
            Me.TextEdit_new_account_number.Properties.Mask.BeepOnError = True
            Me.TextEdit_new_account_number.Properties.Mask.EditMask = "([A-Z0-9]){0,22}"
            Me.TextEdit_new_account_number.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.TextEdit_new_account_number.Properties.MaxLength = 22
            Me.TextEdit_new_account_number.Properties.NullText = "[DO NOT CHANGE ACCOUNT NUMBER]"
            Me.TextEdit_new_account_number.Size = New System.Drawing.Size(257, 20)
            Me.TextEdit_new_account_number.TabIndex = 4
            '
            'LabelControl_creditor_name
            '
            Me.LabelControl_creditor_name.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LabelControl_creditor_name.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter
            Me.LabelControl_creditor_name.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.LabelControl_creditor_name.AutoEllipsis = True
            Me.LabelControl_creditor_name.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_creditor_name.Location = New System.Drawing.Point(228, 16)
            Me.LabelControl_creditor_name.Name = "LabelControl_creditor_name"
            Me.LabelControl_creditor_name.Size = New System.Drawing.Size(151, 13)
            Me.LabelControl_creditor_name.TabIndex = 2
            Me.LabelControl_creditor_name.UseMnemonic = False
            '
            'SimpleButton_OK
            '
            Me.SimpleButton_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.SimpleButton_OK.Enabled = False
            Me.SimpleButton_OK.Location = New System.Drawing.Point(101, 74)
            Me.SimpleButton_OK.Name = "SimpleButton_OK"
            Me.SimpleButton_OK.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.TabIndex = 6
            Me.SimpleButton_OK.Text = "&OK"
            '
            'SimpleButton_Cancel
            '
            Me.SimpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.SimpleButton_Cancel.Location = New System.Drawing.Point(208, 74)
            Me.SimpleButton_Cancel.Name = "SimpleButton_Cancel"
            Me.SimpleButton_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.TabIndex = 7
            Me.SimpleButton_Cancel.Text = "&Cancel"
            '
            'CreditorID_New
            '
            Me.CreditorID_New.EditValue = Nothing
            Me.CreditorID_New.Location = New System.Drawing.Point(122, 13)
            Me.CreditorID_New.Name = "CreditorID_New"
            Me.CreditorID_New.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, CType(resources.GetObject("CreditorID_New.Properties.Buttons"), System.Drawing.Image), New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1, "Click here to search for a creditor ID", "search", Nothing, True)})
            Me.CreditorID_New.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.CreditorID_New.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong
            Me.CreditorID_New.Properties.Mask.BeepOnError = True
            Me.CreditorID_New.Properties.Mask.EditMask = "[A-Z]{1,2}[0-9]{4,}"
            Me.CreditorID_New.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.CreditorID_New.Properties.Mask.UseMaskAsDisplayFormat = True
            Me.CreditorID_New.Properties.MaxLength = 10
            Me.CreditorID_New.Size = New System.Drawing.Size(100, 20)
            Me.CreditorID_New.TabIndex = 1
            Me.CreditorID_New.ValidationLevel = CreditorID.ValidationLevelEnum.NotProhibitUse
            '
            'CheckEdit_NewProposals
            '
            Me.CheckEdit_NewProposals.Location = New System.Drawing.Point(11, 111)
            Me.CheckEdit_NewProposals.Name = "CheckEdit_NewProposals"
            Me.CheckEdit_NewProposals.Properties.Caption = "Create the new proposal with PENDING status"
            Me.CheckEdit_NewProposals.Size = New System.Drawing.Size(254, 19)
            SuperToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.[True]
            ToolTipTitleItem1.Text = "How do you want to generate a new proposal?"
            ToolTipItem1.LeftIndent = 6
            ToolTipItem1.Text = resources.GetString("ToolTipItem1.Text")
            SuperToolTip1.Items.Add(ToolTipTitleItem1)
            SuperToolTip1.Items.Add(ToolTipItem1)
            Me.CheckEdit_NewProposals.SuperTip = SuperToolTip1
            Me.CheckEdit_NewProposals.TabIndex = 5
            '
            'Form_ReassignDebt
            '
            Me.AcceptButton = Me.SimpleButton_OK
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.SimpleButton_Cancel
            Me.ClientSize = New System.Drawing.Size(385, 139)
            Me.Controls.Add(Me.CheckEdit_NewProposals)
            Me.Controls.Add(Me.CreditorID_New)
            Me.Controls.Add(Me.SimpleButton_Cancel)
            Me.Controls.Add(Me.SimpleButton_OK)
            Me.Controls.Add(Me.LabelControl_creditor_name)
            Me.Controls.Add(Me.TextEdit_new_account_number)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LabelControl1)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
            Me.LookAndFeel.UseDefaultLookAndFeel = True
            Me.MaximizeBox = False
            Me.MinimizeBox = False
            Me.Name = "Form_ReassignDebt"
            Me.Text = "Reassign Debt to New Creditor"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_new_account_number.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CreditorID_New.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_NewProposals.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents TextEdit_new_account_number As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LabelControl_creditor_name As DevExpress.XtraEditors.LabelControl
        Friend WithEvents SimpleButton_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents CreditorID_New As CreditorID
        Public WithEvents CheckEdit_NewProposals As DevExpress.XtraEditors.CheckEdit
    End Class
End Namespace