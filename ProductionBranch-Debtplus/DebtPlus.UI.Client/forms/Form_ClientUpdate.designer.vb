﻿Imports DebtPlus.UI.Client.controls

Namespace forms

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form_ClientUpdate
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then components.Dispose()
                End If
                components = Nothing
                privateContext = Nothing
                bc = Nothing
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
            Me.XtraTabPage_Person1 = New DevExpress.XtraTab.XtraTabPage()
            Me.XtraTabPage_Person2 = New DevExpress.XtraTab.XtraTabPage()
            Me.XtraTabPage_General = New DevExpress.XtraTab.XtraTabPage()
            Me.XtraTabPage_Notices = New DevExpress.XtraTab.XtraTabPage()
            Me.XtraTabPage_Disclosures = New DevExpress.XtraTab.XtraTabPage()
            Me.XtraTabPage_Income = New DevExpress.XtraTab.XtraTabPage()
            Me.XtraTabPage_Budgets = New DevExpress.XtraTab.XtraTabPage()
            Me.XtraTabPage_Debts = New DevExpress.XtraTab.XtraTabPage()
            Me.XtraTabPage_Products = New DevExpress.XtraTab.XtraTabPage()
            Me.XtraTabPage_Secured = New DevExpress.XtraTab.XtraTabPage()
            Me.XtraTabPage_OtherDebts = New DevExpress.XtraTab.XtraTabPage()
            Me.XtraTabPage_Retention = New DevExpress.XtraTab.XtraTabPage()
            Me.XtraTabPage_Notes = New DevExpress.XtraTab.XtraTabPage()
            Me.XtraTabPage_ActionPlan = New DevExpress.XtraTab.XtraTabPage()
            Me.XtraTabPage_Taxes = New DevExpress.XtraTab.XtraTabPage()
            Me.XtraTabPage_Aliases = New DevExpress.XtraTab.XtraTabPage()
            Me.XtraTabPage_Housing = New DevExpress.XtraTab.XtraTabPage()
            Me.XtraTabPage_DMP = New DevExpress.XtraTab.XtraTabPage()
            Me.XtraTabPage_Deposits = New DevExpress.XtraTab.XtraTabPage()
            Me.XtraTabPage_Payments = New DevExpress.XtraTab.XtraTabPage()
            Me.XtraTabPage_Comparison = New DevExpress.XtraTab.XtraTabPage()
            Me.MenuItem_File = New System.Windows.Forms.MenuItem()
            Me.MenuItem_File_Exit = New System.Windows.Forms.MenuItem()
            Me.MenuItem_Items = New System.Windows.Forms.MenuItem()
            Me.barManager1 = New DevExpress.XtraBars.BarManager(Me.components)
            Me.MenuBar = New DevExpress.XtraBars.Bar()
            Me.BarSubItem_File = New DevExpress.XtraBars.BarSubItem()
            Me.BarButtonItem_File_ScannedDocuments = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_File_Exit = New DevExpress.XtraBars.BarButtonItem()
            Me.BarSubItem_View = New DevExpress.XtraBars.BarSubItem()
            Me.BarButtonItem_View_BottomLine = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_View_Ticklers = New DevExpress.XtraBars.BarButtonItem()
            Me.BarSubItem_Upload = New DevExpress.XtraBars.BarSubItem()
            Me.BarSubItem_UploadDSA = New DevExpress.XtraBars.BarSubItem()
            Me.BarButtonItem_UploadHPF = New DevExpress.XtraBars.BarButtonItem()
            Me.BarSubItem_Appointments = New DevExpress.XtraBars.BarSubItem()
            Me.BarButtonItem_Appointments_New = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Appointments_Result = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Appointments_List = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Appointments_Cancel = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Appointments_Confirm = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Appointments_Reschedule = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Appointments_Walkin = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Appointments_HUD = New DevExpress.XtraBars.BarButtonItem()
            Me.BarSubItem_Trust = New DevExpress.XtraBars.BarSubItem()
            Me.BarButtonItem_Trust_ReservedInTrust = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Trust_CreditorThreshold = New DevExpress.XtraBars.BarButtonItem()
            Me.BarSubItem_Creditor = New DevExpress.XtraBars.BarSubItem()
            Me.BarButtonItem_Creditor_ChgDepositAmt = New DevExpress.XtraBars.BarButtonItem()
            Me.BarSubItem_Creditor_View = New DevExpress.XtraBars.BarSubItem()
            Me.BarButtonItem_Creditor_SchedPay = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Creditor_Note = New DevExpress.XtraBars.BarButtonItem()
            Me.BarSubItem_Letters = New DevExpress.XtraBars.BarSubItem()
            Me.BarSubItem_Reports = New DevExpress.XtraBars.BarSubItem()
            Me.BarSubItem_WWW = New DevExpress.XtraBars.BarSubItem()
            Me.BarButtonItem_WWW_Account = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_WWW_Messages = New DevExpress.XtraBars.BarButtonItem()
            Me.BarSubItem_Help = New DevExpress.XtraBars.BarSubItem()
            Me.BarButtonItem_Help_Contents = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Help_WhatIS = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Help_About = New DevExpress.XtraBars.BarButtonItem()
            Me.StatusBar = New DevExpress.XtraBars.Bar()
            Me.BarClientID = New DevExpress.XtraBars.BarEditItem()
            Me.RepositoryItemTextEditClientID = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
            Me.BarClientName = New DevExpress.XtraBars.BarStaticItem()
            Me.BarMonthlyFee = New DevExpress.XtraBars.BarStaticItem()
            Me.BarACHStatus = New DevExpress.XtraBars.BarStaticItem()
            Me.BarActiveStatus = New DevExpress.XtraBars.BarStaticItem()
            Me.BarClientStatus = New DevExpress.XtraBars.BarStaticItem()
            Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
            Me.BarButtonItem_Creditor_Payout = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Creditor_View = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_WWW_User_Create = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Reports = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Letters_Temp = New DevExpress.XtraBars.BarButtonItem()
            Me.BarSubItem_UploadHPF = New DevExpress.XtraBars.BarSubItem()
            Me.Bar3 = New DevExpress.XtraBars.Bar()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.XtraTabControl1.SuspendLayout()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RepositoryItemTextEditClientID, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'DefaultLookAndFeel1
            '
            Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin"
            '
            'XtraTabControl1
            '
            Me.XtraTabControl1.AllowDrop = True
            Me.XtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.XtraTabControl1.HeaderButtonsShowMode = DevExpress.XtraTab.TabButtonShowMode.WhenNeeded
            Me.XtraTabControl1.Location = New System.Drawing.Point(0, 25)
            Me.XtraTabControl1.MultiLine = DevExpress.Utils.DefaultBoolean.[True]
            Me.XtraTabControl1.Name = "XtraTabControl1"
            Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage_Person1
            Me.XtraTabControl1.Size = New System.Drawing.Size(728, 379)
            Me.XtraTabControl1.TabIndex = 1
            Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage_Person1, Me.XtraTabPage_Person2, Me.XtraTabPage_General, Me.XtraTabPage_Notices, Me.XtraTabPage_Disclosures, Me.XtraTabPage_Income, Me.XtraTabPage_Budgets, Me.XtraTabPage_Debts, Me.XtraTabPage_Products, Me.XtraTabPage_Secured, Me.XtraTabPage_OtherDebts, Me.XtraTabPage_Retention, Me.XtraTabPage_Notes, Me.XtraTabPage_ActionPlan, Me.XtraTabPage_Taxes, Me.XtraTabPage_Aliases, Me.XtraTabPage_Housing, Me.XtraTabPage_DMP, Me.XtraTabPage_Deposits, Me.XtraTabPage_Payments, Me.XtraTabPage_Comparison})
            '
            'XtraTabPage_Person1
            '
            Me.XtraTabPage_Person1.AllowDrop = True
            Me.XtraTabPage_Person1.AllowTouchScroll = True
            Me.XtraTabPage_Person1.Name = "XtraTabPage_Person1"
            Me.XtraTabPage_Person1.Size = New System.Drawing.Size(720, 327)
            Me.XtraTabPage_Person1.Tag = "Applicant"
            Me.XtraTabPage_Person1.Text = "Applicant"
            '
            'XtraTabPage_Person2
            '
            Me.XtraTabPage_Person2.AllowDrop = True
            Me.XtraTabPage_Person2.AllowTouchScroll = True
            Me.XtraTabPage_Person2.Name = "XtraTabPage_Person2"
            Me.XtraTabPage_Person2.Size = New System.Drawing.Size(720, 327)
            Me.XtraTabPage_Person2.Tag = "CoApplicant"
            Me.XtraTabPage_Person2.Text = "CoApplicant"
            '
            'XtraTabPage_General
            '
            Me.XtraTabPage_General.AllowDrop = True
            Me.XtraTabPage_General.AllowTouchScroll = True
            Me.XtraTabPage_General.Name = "XtraTabPage_General"
            Me.XtraTabPage_General.Size = New System.Drawing.Size(720, 327)
            Me.XtraTabPage_General.Tag = "General"
            Me.XtraTabPage_General.Text = "General"
            '
            'XtraTabPage_Notices
            '
            Me.XtraTabPage_Notices.AllowDrop = True
            Me.XtraTabPage_Notices.AllowTouchScroll = True
            Me.XtraTabPage_Notices.Name = "XtraTabPage_Notices"
            Me.XtraTabPage_Notices.ShowCloseButton = DevExpress.Utils.DefaultBoolean.[False]
            Me.XtraTabPage_Notices.Size = New System.Drawing.Size(720, 327)
            Me.XtraTabPage_Notices.Tag = "Notices"
            Me.XtraTabPage_Notices.Text = "State Info"
            '
            'XtraTabPage_Disclosures
            '
            Me.XtraTabPage_Disclosures.AllowDrop = True
            Me.XtraTabPage_Disclosures.AllowTouchScroll = True
            Me.XtraTabPage_Disclosures.Name = "XtraTabPage_Disclosures"
            Me.XtraTabPage_Disclosures.Size = New System.Drawing.Size(720, 327)
            Me.XtraTabPage_Disclosures.Tag = "Disclosure"
            Me.XtraTabPage_Disclosures.Text = "Disclosures"
            '
            'XtraTabPage_Income
            '
            Me.XtraTabPage_Income.AllowDrop = True
            Me.XtraTabPage_Income.AllowTouchScroll = True
            Me.XtraTabPage_Income.Name = "XtraTabPage_Income"
            Me.XtraTabPage_Income.Size = New System.Drawing.Size(720, 327)
            Me.XtraTabPage_Income.Tag = "Income"
            Me.XtraTabPage_Income.Text = "Income"
            '
            'XtraTabPage_Budgets
            '
            Me.XtraTabPage_Budgets.AllowDrop = True
            Me.XtraTabPage_Budgets.AllowTouchScroll = True
            Me.XtraTabPage_Budgets.Name = "XtraTabPage_Budgets"
            Me.XtraTabPage_Budgets.Size = New System.Drawing.Size(720, 327)
            Me.XtraTabPage_Budgets.Tag = "Budgets"
            Me.XtraTabPage_Budgets.Text = "Budget"
            '
            'XtraTabPage_Debts
            '
            Me.XtraTabPage_Debts.AllowDrop = True
            Me.XtraTabPage_Debts.AllowTouchScroll = True
            Me.XtraTabPage_Debts.Name = "XtraTabPage_Debts"
            Me.XtraTabPage_Debts.Size = New System.Drawing.Size(720, 327)
            Me.XtraTabPage_Debts.Tag = "Debts"
            Me.XtraTabPage_Debts.Text = "Debts"
            '
            'XtraTabPage_Products
            '
            Me.XtraTabPage_Products.Name = "XtraTabPage_Products"
            Me.XtraTabPage_Products.Size = New System.Drawing.Size(720, 327)
            Me.XtraTabPage_Products.Tag = "Products"
            Me.XtraTabPage_Products.Text = "Products"
            '
            'XtraTabPage_Secured
            '
            Me.XtraTabPage_Secured.AllowDrop = True
            Me.XtraTabPage_Secured.AllowTouchScroll = True
            Me.XtraTabPage_Secured.Name = "XtraTabPage_Secured"
            Me.XtraTabPage_Secured.Size = New System.Drawing.Size(720, 327)
            Me.XtraTabPage_Secured.Tag = "Secured"
            Me.XtraTabPage_Secured.Text = "Secured"
            '
            'XtraTabPage_OtherDebts
            '
            Me.XtraTabPage_OtherDebts.AllowDrop = True
            Me.XtraTabPage_OtherDebts.AllowTouchScroll = True
            Me.XtraTabPage_OtherDebts.Name = "XtraTabPage_OtherDebts"
            Me.XtraTabPage_OtherDebts.Size = New System.Drawing.Size(720, 327)
            Me.XtraTabPage_OtherDebts.Tag = "OtherDebts"
            Me.XtraTabPage_OtherDebts.Text = "Other Debt"
            '
            'XtraTabPage_Retention
            '
            Me.XtraTabPage_Retention.AllowDrop = True
            Me.XtraTabPage_Retention.AllowTouchScroll = True
            Me.XtraTabPage_Retention.Name = "XtraTabPage_Retention"
            Me.XtraTabPage_Retention.Size = New System.Drawing.Size(720, 327)
            Me.XtraTabPage_Retention.Tag = "Retention"
            Me.XtraTabPage_Retention.Text = "Retention"
            '
            'XtraTabPage_Notes
            '
            Me.XtraTabPage_Notes.AllowDrop = True
            Me.XtraTabPage_Notes.AllowTouchScroll = True
            Me.XtraTabPage_Notes.Name = "XtraTabPage_Notes"
            Me.XtraTabPage_Notes.Size = New System.Drawing.Size(720, 327)
            Me.XtraTabPage_Notes.Tag = "Notes"
            Me.XtraTabPage_Notes.Text = "Notes"
            '
            'XtraTabPage_ActionPlan
            '
            Me.XtraTabPage_ActionPlan.AllowDrop = True
            Me.XtraTabPage_ActionPlan.AllowTouchScroll = True
            Me.XtraTabPage_ActionPlan.Name = "XtraTabPage_ActionPlan"
            Me.XtraTabPage_ActionPlan.Size = New System.Drawing.Size(720, 327)
            Me.XtraTabPage_ActionPlan.Tag = "ActionPlan"
            Me.XtraTabPage_ActionPlan.Text = "Action Plan"
            '
            'XtraTabPage_Taxes
            '
            Me.XtraTabPage_Taxes.AllowDrop = True
            Me.XtraTabPage_Taxes.AllowTouchScroll = True
            Me.XtraTabPage_Taxes.Name = "XtraTabPage_Taxes"
            Me.XtraTabPage_Taxes.Size = New System.Drawing.Size(720, 327)
            Me.XtraTabPage_Taxes.Tag = "Taxes"
            Me.XtraTabPage_Taxes.Text = "Taxes"
            '
            'XtraTabPage_Aliases
            '
            Me.XtraTabPage_Aliases.AllowDrop = True
            Me.XtraTabPage_Aliases.AllowTouchScroll = True
            Me.XtraTabPage_Aliases.Name = "XtraTabPage_Aliases"
            Me.XtraTabPage_Aliases.Size = New System.Drawing.Size(720, 327)
            Me.XtraTabPage_Aliases.Tag = "Aliases"
            Me.XtraTabPage_Aliases.Text = "Misc"
            '
            'XtraTabPage_Housing
            '
            Me.XtraTabPage_Housing.AllowDrop = True
            Me.XtraTabPage_Housing.AllowTouchScroll = True
            Me.XtraTabPage_Housing.Name = "XtraTabPage_Housing"
            Me.XtraTabPage_Housing.Size = New System.Drawing.Size(720, 327)
            Me.XtraTabPage_Housing.Tag = "Housing"
            Me.XtraTabPage_Housing.Text = "Housing"
            '
            'XtraTabPage_DMP
            '
            Me.XtraTabPage_DMP.AllowDrop = True
            Me.XtraTabPage_DMP.AllowTouchScroll = True
            Me.XtraTabPage_DMP.Name = "XtraTabPage_DMP"
            Me.XtraTabPage_DMP.Size = New System.Drawing.Size(720, 327)
            Me.XtraTabPage_DMP.Tag = "DMP"
            Me.XtraTabPage_DMP.Text = "D.M.P."
            '
            'XtraTabPage_Deposits
            '
            Me.XtraTabPage_Deposits.AllowDrop = True
            Me.XtraTabPage_Deposits.AllowTouchScroll = True
            Me.XtraTabPage_Deposits.Name = "XtraTabPage_Deposits"
            Me.XtraTabPage_Deposits.Size = New System.Drawing.Size(720, 327)
            Me.XtraTabPage_Deposits.Tag = "Deposits"
            Me.XtraTabPage_Deposits.Text = "Deposits"
            '
            'XtraTabPage_Payments
            '
            Me.XtraTabPage_Payments.AllowDrop = True
            Me.XtraTabPage_Payments.AllowTouchScroll = True
            Me.XtraTabPage_Payments.Name = "XtraTabPage_Payments"
            Me.XtraTabPage_Payments.Size = New System.Drawing.Size(720, 327)
            Me.XtraTabPage_Payments.Tag = "Payments"
            Me.XtraTabPage_Payments.Text = "Payments"
            '
            'XtraTabPage_Comparison
            '
            Me.XtraTabPage_Comparison.AllowDrop = True
            Me.XtraTabPage_Comparison.AllowTouchScroll = True
            Me.XtraTabPage_Comparison.Name = "XtraTabPage_Comparison"
            Me.XtraTabPage_Comparison.Size = New System.Drawing.Size(720, 327)
            Me.XtraTabPage_Comparison.Tag = "Comparison"
            Me.XtraTabPage_Comparison.Text = "Comparison"
            '
            'MenuItem_File
            '
            Me.MenuItem_File.Index = -1
            Me.MenuItem_File.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem_File_Exit})
            Me.MenuItem_File.Text = "&File"
            '
            'MenuItem_File_Exit
            '
            Me.MenuItem_File_Exit.Index = 0
            Me.MenuItem_File_Exit.Text = "&Exit"
            '
            'MenuItem_Items
            '
            Me.MenuItem_Items.Index = -1
            Me.MenuItem_Items.Text = "&Items"
            '
            'barManager1
            '
            Me.barManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.MenuBar, Me.StatusBar})
            Me.barManager1.DockControls.Add(Me.barDockControlTop)
            Me.barManager1.DockControls.Add(Me.barDockControlBottom)
            Me.barManager1.DockControls.Add(Me.barDockControlLeft)
            Me.barManager1.DockControls.Add(Me.barDockControlRight)
            Me.barManager1.Form = Me
            Me.barManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarClientID, Me.BarClientName, Me.BarMonthlyFee, Me.BarActiveStatus, Me.BarClientStatus, Me.BarACHStatus, Me.BarSubItem_File, Me.BarButtonItem_File_ScannedDocuments, Me.BarButtonItem_File_Exit, Me.BarSubItem_View, Me.BarButtonItem_View_BottomLine, Me.BarButtonItem_View_Ticklers, Me.BarSubItem_Appointments, Me.BarSubItem_Trust, Me.BarSubItem_Creditor, Me.BarSubItem_Letters, Me.BarSubItem_Reports, Me.BarSubItem_WWW, Me.BarSubItem_Help, Me.BarButtonItem_Appointments_New, Me.BarButtonItem_Appointments_Result, Me.BarButtonItem_Appointments_List, Me.BarButtonItem_Appointments_Cancel, Me.BarButtonItem_Appointments_Confirm, Me.BarButtonItem_Appointments_Reschedule, Me.BarButtonItem_Appointments_Walkin, Me.BarButtonItem_Appointments_HUD, Me.BarButtonItem_Trust_ReservedInTrust, Me.BarButtonItem_Trust_CreditorThreshold, Me.BarButtonItem_Creditor_ChgDepositAmt, Me.BarButtonItem_Creditor_Payout, Me.BarButtonItem_Creditor_View, Me.BarButtonItem_Creditor_SchedPay, Me.BarButtonItem_Creditor_Note, Me.BarButtonItem_WWW_User_Create, Me.BarButtonItem_WWW_Messages, Me.BarButtonItem_WWW_Account, Me.BarButtonItem_Help_Contents, Me.BarButtonItem_Help_WhatIS, Me.BarButtonItem_Help_About, Me.BarSubItem_Creditor_View, Me.BarButtonItem_Reports, Me.BarButtonItem_Letters_Temp, Me.BarSubItem_Upload, Me.BarSubItem_UploadDSA, Me.BarSubItem_UploadHPF, Me.BarButtonItem_UploadHPF})
            Me.barManager1.MainMenu = Me.MenuBar
            Me.barManager1.MaxItemId = 53
            Me.barManager1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTextEditClientID})
            Me.barManager1.StatusBar = Me.StatusBar
            '
            'MenuBar
            '
            Me.MenuBar.BarName = "Main menu"
            Me.MenuBar.DockCol = 0
            Me.MenuBar.DockRow = 0
            Me.MenuBar.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
            Me.MenuBar.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem_File), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem_View), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem_Upload), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem_Appointments), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem_Trust), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem_Creditor), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem_Letters), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem_Reports), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem_WWW), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem_Help)})
            Me.MenuBar.OptionsBar.MultiLine = True
            Me.MenuBar.OptionsBar.UseWholeRow = True
            Me.MenuBar.Text = "Main menu"
            '
            'BarSubItem_File
            '
            Me.BarSubItem_File.Caption = "&File"
            Me.BarSubItem_File.Id = 6
            Me.BarSubItem_File.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_File_ScannedDocuments), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_File_Exit)})
            Me.BarSubItem_File.Name = "BarSubItem_File"
            '
            'BarButtonItem_File_ScannedDocuments
            '
            Me.BarButtonItem_File_ScannedDocuments.Caption = "&Scanned Documents..."
            Me.BarButtonItem_File_ScannedDocuments.Id = 7
            Me.BarButtonItem_File_ScannedDocuments.Name = "BarButtonItem_File_ScannedDocuments"
            Me.BarButtonItem_File_ScannedDocuments.Visibility = DevExpress.XtraBars.BarItemVisibility.OnlyInCustomizing
            '
            'BarButtonItem_File_Exit
            '
            Me.BarButtonItem_File_Exit.Caption = "&Exit"
            Me.BarButtonItem_File_Exit.Id = 8
            Me.BarButtonItem_File_Exit.Name = "BarButtonItem_File_Exit"
            '
            'BarSubItem_View
            '
            Me.BarSubItem_View.Caption = "&View"
            Me.BarSubItem_View.Id = 9
            Me.BarSubItem_View.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_View_BottomLine), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_View_Ticklers)})
            Me.BarSubItem_View.Name = "BarSubItem_View"
            '
            'BarButtonItem_View_BottomLine
            '
            Me.BarButtonItem_View_BottomLine.Caption = "&Bottom Line..."
            Me.BarButtonItem_View_BottomLine.Id = 10
            Me.BarButtonItem_View_BottomLine.Name = "BarButtonItem_View_BottomLine"
            '
            'BarButtonItem_View_Ticklers
            '
            Me.BarButtonItem_View_Ticklers.Caption = "&Ticklers..."
            Me.BarButtonItem_View_Ticklers.Id = 11
            Me.BarButtonItem_View_Ticklers.Name = "BarButtonItem_View_Ticklers"
            '
            'BarSubItem_Upload
            '
            Me.BarSubItem_Upload.Caption = "&Upload"
            Me.BarSubItem_Upload.Id = 49
            Me.BarSubItem_Upload.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem_UploadDSA), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_UploadHPF)})
            Me.BarSubItem_Upload.Name = "BarSubItem_Upload"
            '
            'BarSubItem_UploadDSA
            '
            Me.BarSubItem_UploadDSA.Caption = "&DSA Upload"
            Me.BarSubItem_UploadDSA.Id = 50
            Me.BarSubItem_UploadDSA.Name = "BarSubItem_UploadDSA"
            '
            'BarButtonItem_UploadHPF
            '
            Me.BarButtonItem_UploadHPF.Caption = "&HPF Upload..."
            Me.BarButtonItem_UploadHPF.Id = 52
            Me.BarButtonItem_UploadHPF.Name = "BarButtonItem_UploadHPF"
            '
            'BarSubItem_Appointments
            '
            Me.BarSubItem_Appointments.Caption = "&Appointments"
            Me.BarSubItem_Appointments.Id = 12
            Me.BarSubItem_Appointments.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Appointments_New), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Appointments_Result), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Appointments_List), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Appointments_Cancel), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Appointments_Confirm), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Appointments_Reschedule), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Appointments_Walkin), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Appointments_HUD, True)})
            Me.BarSubItem_Appointments.Name = "BarSubItem_Appointments"
            '
            'BarButtonItem_Appointments_New
            '
            Me.BarButtonItem_Appointments_New.Caption = "&New..."
            Me.BarButtonItem_Appointments_New.Id = 20
            Me.BarButtonItem_Appointments_New.Name = "BarButtonItem_Appointments_New"
            '
            'BarButtonItem_Appointments_Result
            '
            Me.BarButtonItem_Appointments_Result.Caption = "&Result..."
            Me.BarButtonItem_Appointments_Result.Id = 21
            Me.BarButtonItem_Appointments_Result.Name = "BarButtonItem_Appointments_Result"
            '
            'BarButtonItem_Appointments_List
            '
            Me.BarButtonItem_Appointments_List.Caption = "&List..."
            Me.BarButtonItem_Appointments_List.Id = 22
            Me.BarButtonItem_Appointments_List.Name = "BarButtonItem_Appointments_List"
            '
            'BarButtonItem_Appointments_Cancel
            '
            Me.BarButtonItem_Appointments_Cancel.Caption = "&Cancel..."
            Me.BarButtonItem_Appointments_Cancel.Id = 23
            Me.BarButtonItem_Appointments_Cancel.Name = "BarButtonItem_Appointments_Cancel"
            '
            'BarButtonItem_Appointments_Confirm
            '
            Me.BarButtonItem_Appointments_Confirm.Caption = "C&onfirm..."
            Me.BarButtonItem_Appointments_Confirm.Id = 24
            Me.BarButtonItem_Appointments_Confirm.Name = "BarButtonItem_Appointments_Confirm"
            '
            'BarButtonItem_Appointments_Reschedule
            '
            Me.BarButtonItem_Appointments_Reschedule.Caption = "R&eschedule..."
            Me.BarButtonItem_Appointments_Reschedule.Id = 25
            Me.BarButtonItem_Appointments_Reschedule.Name = "BarButtonItem_Appointments_Reschedule"
            '
            'BarButtonItem_Appointments_Walkin
            '
            Me.BarButtonItem_Appointments_Walkin.Caption = "&Walkin..."
            Me.BarButtonItem_Appointments_Walkin.Id = 26
            Me.BarButtonItem_Appointments_Walkin.Name = "BarButtonItem_Appointments_Walkin"
            '
            'BarButtonItem_Appointments_HUD
            '
            Me.BarButtonItem_Appointments_HUD.Caption = "&HUD Result..."
            Me.BarButtonItem_Appointments_HUD.Id = 27
            Me.BarButtonItem_Appointments_HUD.Name = "BarButtonItem_Appointments_HUD"
            '
            'BarSubItem_Trust
            '
            Me.BarSubItem_Trust.Caption = "&Trust"
            Me.BarSubItem_Trust.Id = 13
            Me.BarSubItem_Trust.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Trust_ReservedInTrust), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Trust_CreditorThreshold)})
            Me.BarSubItem_Trust.Name = "BarSubItem_Trust"
            '
            'BarButtonItem_Trust_ReservedInTrust
            '
            Me.BarButtonItem_Trust_ReservedInTrust.Caption = "Reserved In Trust..."
            Me.BarButtonItem_Trust_ReservedInTrust.Id = 28
            Me.BarButtonItem_Trust_ReservedInTrust.Name = "BarButtonItem_Trust_ReservedInTrust"
            '
            'BarButtonItem_Trust_CreditorThreshold
            '
            Me.BarButtonItem_Trust_CreditorThreshold.Caption = "&Creditor Threshold..."
            Me.BarButtonItem_Trust_CreditorThreshold.Id = 29
            Me.BarButtonItem_Trust_CreditorThreshold.Name = "BarButtonItem_Trust_CreditorThreshold"
            '
            'BarSubItem_Creditor
            '
            Me.BarSubItem_Creditor.Caption = "&Creditor"
            Me.BarSubItem_Creditor.Id = 14
            Me.BarSubItem_Creditor.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Creditor_ChgDepositAmt), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem_Creditor_View), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Creditor_SchedPay), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Creditor_Note)})
            Me.BarSubItem_Creditor.Name = "BarSubItem_Creditor"
            '
            'BarButtonItem_Creditor_ChgDepositAmt
            '
            Me.BarButtonItem_Creditor_ChgDepositAmt.Caption = "Change Deposit Amount..."
            Me.BarButtonItem_Creditor_ChgDepositAmt.Id = 30
            Me.BarButtonItem_Creditor_ChgDepositAmt.Name = "BarButtonItem_Creditor_ChgDepositAmt"
            '
            'BarSubItem_Creditor_View
            '
            Me.BarSubItem_Creditor_View.Caption = "&View"
            Me.BarSubItem_Creditor_View.Id = 42
            Me.BarSubItem_Creditor_View.Name = "BarSubItem_Creditor_View"
            '
            'BarButtonItem_Creditor_SchedPay
            '
            Me.BarButtonItem_Creditor_SchedPay.Caption = "&Sched Pay..."
            Me.BarButtonItem_Creditor_SchedPay.Id = 33
            Me.BarButtonItem_Creditor_SchedPay.Name = "BarButtonItem_Creditor_SchedPay"
            '
            'BarButtonItem_Creditor_Note
            '
            Me.BarButtonItem_Creditor_Note.Caption = "&Note..."
            Me.BarButtonItem_Creditor_Note.Id = 34
            Me.BarButtonItem_Creditor_Note.Name = "BarButtonItem_Creditor_Note"
            '
            'BarSubItem_Letters
            '
            Me.BarSubItem_Letters.Caption = "&Letters"
            Me.BarSubItem_Letters.Id = 15
            Me.BarSubItem_Letters.Name = "BarSubItem_Letters"
            '
            'BarSubItem_Reports
            '
            Me.BarSubItem_Reports.Caption = "&Reports"
            Me.BarSubItem_Reports.Id = 16
            Me.BarSubItem_Reports.Name = "BarSubItem_Reports"
            '
            'BarSubItem_WWW
            '
            Me.BarSubItem_WWW.Caption = "&WWW"
            Me.BarSubItem_WWW.Id = 17
            Me.BarSubItem_WWW.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_WWW_Account), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_WWW_Messages)})
            Me.BarSubItem_WWW.Name = "BarSubItem_WWW"
            '
            'BarButtonItem_WWW_Account
            '
            Me.BarButtonItem_WWW_Account.Caption = "&Account..."
            Me.BarButtonItem_WWW_Account.Id = 38
            Me.BarButtonItem_WWW_Account.Name = "BarButtonItem_WWW_Account"
            '
            'BarButtonItem_WWW_Messages
            '
            Me.BarButtonItem_WWW_Messages.Caption = "&Messages..."
            Me.BarButtonItem_WWW_Messages.Id = 37
            Me.BarButtonItem_WWW_Messages.Name = "BarButtonItem_WWW_Messages"
            '
            'BarSubItem_Help
            '
            Me.BarSubItem_Help.Caption = "&Help"
            Me.BarSubItem_Help.Id = 18
            Me.BarSubItem_Help.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Help_Contents), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Help_WhatIS), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Help_About)})
            Me.BarSubItem_Help.Name = "BarSubItem_Help"
            '
            'BarButtonItem_Help_Contents
            '
            Me.BarButtonItem_Help_Contents.Caption = "&Contents..."
            Me.BarButtonItem_Help_Contents.Enabled = False
            Me.BarButtonItem_Help_Contents.Id = 39
            Me.BarButtonItem_Help_Contents.Name = "BarButtonItem_Help_Contents"
            '
            'BarButtonItem_Help_WhatIS
            '
            Me.BarButtonItem_Help_WhatIS.Caption = "&WhatIS"
            Me.BarButtonItem_Help_WhatIS.Enabled = False
            Me.BarButtonItem_Help_WhatIS.Id = 40
            Me.BarButtonItem_Help_WhatIS.Name = "BarButtonItem_Help_WhatIS"
            '
            'BarButtonItem_Help_About
            '
            Me.BarButtonItem_Help_About.Caption = "&About..."
            Me.BarButtonItem_Help_About.Id = 41
            Me.BarButtonItem_Help_About.Name = "BarButtonItem_Help_About"
            '
            'StatusBar
            '
            Me.StatusBar.BarName = "Status bar"
            Me.StatusBar.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom
            Me.StatusBar.DockCol = 0
            Me.StatusBar.DockRow = 0
            Me.StatusBar.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
            Me.StatusBar.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarClientID), New DevExpress.XtraBars.LinkPersistInfo(Me.BarClientName), New DevExpress.XtraBars.LinkPersistInfo(Me.BarMonthlyFee), New DevExpress.XtraBars.LinkPersistInfo(Me.BarACHStatus), New DevExpress.XtraBars.LinkPersistInfo(Me.BarActiveStatus), New DevExpress.XtraBars.LinkPersistInfo(Me.BarClientStatus)})
            Me.StatusBar.OptionsBar.AllowQuickCustomization = False
            Me.StatusBar.OptionsBar.DrawDragBorder = False
            Me.StatusBar.OptionsBar.UseWholeRow = True
            Me.StatusBar.Text = "Status bar"
            '
            'BarClientID
            '
            Me.BarClientID.Edit = Me.RepositoryItemTextEditClientID
            Me.BarClientID.Id = 48
            Me.BarClientID.Name = "BarClientID"
            Me.BarClientID.Width = 85
            '
            'RepositoryItemTextEditClientID
            '
            Me.RepositoryItemTextEditClientID.AutoHeight = False
            Me.RepositoryItemTextEditClientID.Name = "RepositoryItemTextEditClientID"
            Me.RepositoryItemTextEditClientID.ReadOnly = True
            '
            'BarClientName
            '
            Me.BarClientName.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring
            Me.BarClientName.Id = 1
            Me.BarClientName.Name = "BarClientName"
            Me.BarClientName.TextAlignment = System.Drawing.StringAlignment.Near
            Me.BarClientName.Width = 32
            '
            'BarMonthlyFee
            '
            Me.BarMonthlyFee.Id = 2
            Me.BarMonthlyFee.Name = "BarMonthlyFee"
            Me.BarMonthlyFee.TextAlignment = System.Drawing.StringAlignment.Near
            '
            'BarACHStatus
            '
            Me.BarACHStatus.Caption = "ACH"
            Me.BarACHStatus.Id = 5
            Me.BarACHStatus.Name = "BarACHStatus"
            Me.BarACHStatus.TextAlignment = System.Drawing.StringAlignment.Near
            '
            'BarActiveStatus
            '
            Me.BarActiveStatus.Id = 3
            Me.BarActiveStatus.Name = "BarActiveStatus"
            Me.BarActiveStatus.TextAlignment = System.Drawing.StringAlignment.Near
            '
            'BarClientStatus
            '
            Me.BarClientStatus.Id = 4
            Me.BarClientStatus.Name = "BarClientStatus"
            Me.BarClientStatus.TextAlignment = System.Drawing.StringAlignment.Near
            '
            'barDockControlTop
            '
            Me.barDockControlTop.CausesValidation = False
            Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
            Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
            Me.barDockControlTop.Size = New System.Drawing.Size(728, 25)
            '
            'barDockControlBottom
            '
            Me.barDockControlBottom.CausesValidation = False
            Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
            Me.barDockControlBottom.Location = New System.Drawing.Point(0, 404)
            Me.barDockControlBottom.Size = New System.Drawing.Size(728, 28)
            '
            'barDockControlLeft
            '
            Me.barDockControlLeft.CausesValidation = False
            Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
            Me.barDockControlLeft.Location = New System.Drawing.Point(0, 25)
            Me.barDockControlLeft.Size = New System.Drawing.Size(0, 379)
            '
            'barDockControlRight
            '
            Me.barDockControlRight.CausesValidation = False
            Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
            Me.barDockControlRight.Location = New System.Drawing.Point(728, 25)
            Me.barDockControlRight.Size = New System.Drawing.Size(0, 379)
            '
            'BarButtonItem_Creditor_Payout
            '
            Me.BarButtonItem_Creditor_Payout.Caption = "P&ayout..."
            Me.BarButtonItem_Creditor_Payout.Id = 31
            Me.BarButtonItem_Creditor_Payout.Name = "BarButtonItem_Creditor_Payout"
            '
            'BarButtonItem_Creditor_View
            '
            Me.BarButtonItem_Creditor_View.Caption = "&View"
            Me.BarButtonItem_Creditor_View.Id = 32
            Me.BarButtonItem_Creditor_View.Name = "BarButtonItem_Creditor_View"
            '
            'BarButtonItem_WWW_User_Create
            '
            Me.BarButtonItem_WWW_User_Create.Caption = "&Create..."
            Me.BarButtonItem_WWW_User_Create.Id = 36
            Me.BarButtonItem_WWW_User_Create.Name = "BarButtonItem_WWW_User_Create"
            '
            'BarButtonItem_Reports
            '
            Me.BarButtonItem_Reports.Caption = "Temp"
            Me.BarButtonItem_Reports.Id = 46
            Me.BarButtonItem_Reports.Name = "BarButtonItem_Reports"
            '
            'BarButtonItem_Letters_Temp
            '
            Me.BarButtonItem_Letters_Temp.Caption = "Temp"
            Me.BarButtonItem_Letters_Temp.Id = 47
            Me.BarButtonItem_Letters_Temp.Name = "BarButtonItem_Letters_Temp"
            '
            'BarSubItem_UploadHPF
            '
            Me.BarSubItem_UploadHPF.Caption = "&HPF Upload"
            Me.BarSubItem_UploadHPF.Id = 51
            Me.BarSubItem_UploadHPF.Name = "BarSubItem_UploadHPF"
            '
            'Bar3
            '
            Me.Bar3.BarName = "Status bar"
            Me.Bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom
            Me.Bar3.DockCol = 0
            Me.Bar3.DockRow = 0
            Me.Bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
            Me.Bar3.OptionsBar.AllowQuickCustomization = False
            Me.Bar3.OptionsBar.DrawDragBorder = False
            Me.Bar3.OptionsBar.UseWholeRow = True
            Me.Bar3.Text = "Status bar"
            '
            'Form_ClientUpdate
            '
            Me.ClientSize = New System.Drawing.Size(728, 432)
            Me.Controls.Add(Me.XtraTabControl1)
            Me.Controls.Add(Me.barDockControlLeft)
            Me.Controls.Add(Me.barDockControlRight)
            Me.Controls.Add(Me.barDockControlBottom)
            Me.Controls.Add(Me.barDockControlTop)
            Me.Name = "Form_ClientUpdate"
            Me.Text = "Client Update Form"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.XtraTabControl1.ResumeLayout(False)
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RepositoryItemTextEditClientID, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

        Friend WithEvents XtraTabPage_General As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents XtraTabPage_Person1 As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents XtraTabPage_Income As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents XtraTabPage_Taxes As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents XtraTabPage_Aliases As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents XtraTabPage_Budgets As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents XtraTabPage_Housing As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents XtraTabPage_ActionPlan As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents XtraTabPage_Debts As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents XtraTabPage_Deposits As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents XtraTabPage_DMP As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents XtraTabPage_Comparison As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents XtraTabPage_Person2 As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents XtraTabPage_Notes As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents XtraTabPage_OtherDebts As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents XtraTabPage_Payments As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents XtraTabPage_Secured As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents XtraTabPage_Retention As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
        Friend WithEvents MenuItem_File As System.Windows.Forms.MenuItem
        Friend WithEvents MenuItem_File_Exit As System.Windows.Forms.MenuItem
        Friend WithEvents MenuItem_Items As System.Windows.Forms.MenuItem
        Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
        Friend WithEvents barManager1 As DevExpress.XtraBars.BarManager
        Friend WithEvents MenuBar As DevExpress.XtraBars.Bar
        Friend WithEvents StatusBar As DevExpress.XtraBars.Bar
        Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
        Friend WithEvents Bar3 As DevExpress.XtraBars.Bar
        Friend WithEvents BarClientName As DevExpress.XtraBars.BarStaticItem
        Friend WithEvents BarMonthlyFee As DevExpress.XtraBars.BarStaticItem
        Friend WithEvents BarActiveStatus As DevExpress.XtraBars.BarStaticItem
        Friend WithEvents BarClientStatus As DevExpress.XtraBars.BarStaticItem
        Friend WithEvents BarACHStatus As DevExpress.XtraBars.BarStaticItem
        Friend WithEvents BarSubItem_File As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarButtonItem_File_ScannedDocuments As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_File_Exit As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarSubItem_View As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarButtonItem_View_BottomLine As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_View_Ticklers As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarSubItem_Appointments As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarSubItem_Trust As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarSubItem_Creditor As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarSubItem_Letters As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarSubItem_Reports As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarSubItem_WWW As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarSubItem_Help As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarButtonItem_Appointments_New As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Appointments_Result As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Appointments_List As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Appointments_Cancel As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Appointments_Confirm As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Appointments_Reschedule As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Appointments_Walkin As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Appointments_HUD As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Trust_ReservedInTrust As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Trust_CreditorThreshold As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Creditor_ChgDepositAmt As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Creditor_Payout As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Creditor_View As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Creditor_SchedPay As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Creditor_Note As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_WWW_User_Create As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_WWW_Messages As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_WWW_Account As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Help_Contents As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Help_WhatIS As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Help_About As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarSubItem_Creditor_View As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarButtonItem_Reports As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Letters_Temp As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarClientID As DevExpress.XtraBars.BarEditItem
        Friend WithEvents RepositoryItemTextEditClientID As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Friend WithEvents XtraTabPage_Notices As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents BarSubItem_Upload As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarSubItem_UploadDSA As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarSubItem_UploadHPF As DevExpress.XtraBars.BarSubItem
        Friend WithEvents XtraTabPage_Disclosures As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents BarButtonItem_UploadHPF As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents XtraTabPage_Products As DevExpress.XtraTab.XtraTabPage
    End Class
End Namespace