#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.XtraEditors

Namespace forms

    Public Class EmploymentDurationForm

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        ''' <summary>
        ''' Initialize the form
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler TextEdit_Months.EditValueChanging, AddressOf TextEdit_EditValueChanging
            AddHandler TextEdit_Years.EditValueChanging, AddressOf TextEdit_EditValueChanging
        End Sub

        ''' <summary>
        ''' Return the number of months in the input field
        ''' </summary>
        ''' <returns>Int32 value for the number of entered months</returns>
        ''' <remarks></remarks>
        Private Function Months() As Int32
            With TextEdit_Months
                Dim v As String = .Text.Trim()
                If v <> String.Empty Then
                    Dim ItemValue As Int32
                    If Int32.TryParse(v, ItemValue) AndAlso ItemValue >= 0 Then
                        Return ItemValue
                    End If
                End If
                Return 0
            End With
        End Function

        ''' <summary>
        ''' Return the number of years in the input field
        ''' </summary>
        ''' <returns>Int32 value for the number of entered years</returns>
        ''' <remarks></remarks>
        Private Function Years() As Int32
            With TextEdit_Years
                Dim v As String = .Text.Trim()
                If v <> String.Empty Then
                    Dim ItemValue As Int32
                    If Int32.TryParse(v, ItemValue) AndAlso ItemValue >= 0 Then
                        Return ItemValue
                    End If
                End If
                Return 0
            End With
        End Function

        ''' <summary>
        ''' Return the total number of months for both the years and months fields
        ''' </summary>
        ''' <returns>The number of months.</returns>
        ''' <remarks></remarks>
        Public Function GetMonths() As Int32
            Return (Years() * 12) + Months()
        End Function

        ''' <summary>
        ''' This is the entered starting date.
        ''' </summary>
        ''' <value>The date that should represent the hire date for the employee</value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property StartDate() As System.Nullable(Of DateTime)
            Get
                Return DateTime.Now.Date.AddMonths(-GetMonths())
            End Get
            Set(ByVal value As System.Nullable(Of DateTime))
                If value.HasValue Then
                    Dim Months As Int32 = NumberMonths(DateTime.Now, value.Value)
                    Dim Years As Int32 = (Months \ 12)
                    Months -= (Years * 12)

                    TextEdit_Months.EditValue = Months
                    TextEdit_Years.EditValue = Years
                Else
                    TextEdit_Months.EditValue = 0
                    TextEdit_Years.EditValue = 0
                End If
            End Set
        End Property

        ''' <summary>
        ''' Compute the number of months between two dates
        ''' </summary>
        ''' <param name="ToDate">Ending date for the subtraction (minuend)</param>
        ''' <param name="FromDate">Staring date for the subtraction (subtrahend)</param>
        ''' <returns>The number of months between the two dates</returns>
        ''' <remarks></remarks>
        Private Function NumberMonths(ByVal ToDate As DateTime, ByVal FromDate As DateTime) As Int32
            Dim ToDate_Year As Int32, ToDate_Month As Int32, ToDate_Day As Int32
            Dim FromDate_Year As Int32, FromDate_Month As Int32, FromDate_Day As Int32

            ToDate_Year = ToDate.Year
            ToDate_Month = ToDate.Month
            ToDate_Day = ToDate.Day
            FromDate_Year = FromDate.Year
            FromDate_Month = FromDate.Month
            FromDate_Day = FromDate.Day

            Return (ToDate_Year - FromDate_Year) * 12 + (ToDate_Month - FromDate_Month)
        End Function

        ''' <summary>
        ''' Do not allow negative values in the input field.
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub TextEdit_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)
            With CType(sender, TextEdit)
                Dim v As Int32 = DebtPlus.Utils.Nulls.DInt(e.NewValue)
                If v < 0 Then
                    e.Cancel = True
                End If
            End With
        End Sub
    End Class
End Namespace
