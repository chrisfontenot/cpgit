﻿Namespace forms
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form_NewProduct

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then components.Dispose()
                End If
                components = Nothing
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer = Nothing

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.SimpleButton_Cancel = New DevExpress.XtraEditors.SimpleButton()
            Me.SimpleButton_OK = New DevExpress.XtraEditors.SimpleButton()
            Me.TextEdit_ReferenceNumber = New DevExpress.XtraEditors.TextEdit()
            Me.LookUpEdit_ProductType = New DevExpress.XtraEditors.LookUpEdit()
            Me.LabelControl_VendorName = New DevExpress.XtraEditors.LabelControl()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem_Left = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem_Right = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.VendorID1 = New DebtPlus.UI.Vendor.Widgets.Controls.VendorID()
            Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.TextEdit_ReferenceNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_ProductType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem_Left, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem_Right, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.VendorID1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'DefaultLookAndFeel1
            '
            Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin"
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.VendorID1)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton_Cancel)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton_OK)
            Me.LayoutControl1.Controls.Add(Me.TextEdit_ReferenceNumber)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_ProductType)
            Me.LayoutControl1.Controls.Add(Me.LabelControl_VendorName)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(365, 155)
            Me.LayoutControl1.TabIndex = 0
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'SimpleButton_Cancel
            '
            Me.SimpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.SimpleButton_Cancel.Location = New System.Drawing.Point(193, 117)
            Me.SimpleButton_Cancel.MaximumSize = New System.Drawing.Size(75, 26)
            Me.SimpleButton_Cancel.MinimumSize = New System.Drawing.Size(75, 26)
            Me.SimpleButton_Cancel.Name = "SimpleButton_Cancel"
            Me.SimpleButton_Cancel.Size = New System.Drawing.Size(75, 26)
            Me.SimpleButton_Cancel.StyleController = Me.LayoutControl1
            Me.SimpleButton_Cancel.TabIndex = 9
            Me.SimpleButton_Cancel.Text = "&Cancel"
            '
            'SimpleButton_OK
            '
            Me.SimpleButton_OK.Location = New System.Drawing.Point(114, 117)
            Me.SimpleButton_OK.MaximumSize = New System.Drawing.Size(75, 26)
            Me.SimpleButton_OK.MinimumSize = New System.Drawing.Size(75, 26)
            Me.SimpleButton_OK.Name = "SimpleButton_OK"
            Me.SimpleButton_OK.Size = New System.Drawing.Size(75, 26)
            Me.SimpleButton_OK.StyleController = Me.LayoutControl1
            Me.SimpleButton_OK.TabIndex = 8
            Me.SimpleButton_OK.Text = "&OK"
            '
            'TextEdit_ReferenceNumber
            '
            Me.TextEdit_ReferenceNumber.Location = New System.Drawing.Point(105, 77)
            Me.TextEdit_ReferenceNumber.Name = "TextEdit_ReferenceNumber"
            Me.TextEdit_ReferenceNumber.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.TextEdit_ReferenceNumber.Properties.Mask.BeepOnError = True
            Me.TextEdit_ReferenceNumber.Properties.Mask.EditMask = "[^ ].*"
            Me.TextEdit_ReferenceNumber.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.TextEdit_ReferenceNumber.Properties.MaxLength = 50
            Me.TextEdit_ReferenceNumber.Size = New System.Drawing.Size(248, 20)
            Me.TextEdit_ReferenceNumber.StyleController = Me.LayoutControl1
            Me.TextEdit_ReferenceNumber.TabIndex = 7
            '
            'LookUpEdit_ProductType
            '
            Me.LookUpEdit_ProductType.Location = New System.Drawing.Point(105, 53)
            Me.LookUpEdit_ProductType.Name = "LookUpEdit_ProductType"
            Me.LookUpEdit_ProductType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_ProductType.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.LookUpEdit_ProductType.Properties.DisplayMember = "description"
            Me.LookUpEdit_ProductType.Properties.NullText = ""
            Me.LookUpEdit_ProductType.Properties.ShowFooter = False
            Me.LookUpEdit_ProductType.Properties.ShowHeader = False
            Me.LookUpEdit_ProductType.Properties.ValueMember = "Id"
            Me.LookUpEdit_ProductType.Size = New System.Drawing.Size(248, 20)
            Me.LookUpEdit_ProductType.StyleController = Me.LayoutControl1
            Me.LookUpEdit_ProductType.TabIndex = 6
            '
            'LabelControl_VendorName
            '
            Me.LabelControl_VendorName.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_VendorName.Location = New System.Drawing.Point(105, 36)
            Me.LabelControl_VendorName.Name = "LabelControl_VendorName"
            Me.LabelControl_VendorName.Size = New System.Drawing.Size(248, 13)
            Me.LabelControl_VendorName.StyleController = Me.LayoutControl1
            Me.LabelControl_VendorName.TabIndex = 5
            Me.LabelControl_VendorName.UseMnemonic = False
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup1.GroupBordersVisible = False
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.EmptySpaceItem1, Me.LayoutControlItem5, Me.LayoutControlItem6, Me.EmptySpaceItem_Left, Me.EmptySpaceItem_Right, Me.EmptySpaceItem2, Me.LayoutControlItem7})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(365, 155)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.LabelControl_VendorName
            Me.LayoutControlItem2.CustomizationFormText = "Vendor Name"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(345, 17)
            Me.LayoutControlItem2.Text = "Vendor Name"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(90, 13)
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.LookUpEdit_ProductType
            Me.LayoutControlItem3.CustomizationFormText = "Product Type"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 41)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(345, 24)
            Me.LayoutControlItem3.Text = "Product Type"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(90, 13)
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.TextEdit_ReferenceNumber
            Me.LayoutControlItem4.CustomizationFormText = "Reference Number"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 65)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(345, 24)
            Me.LayoutControlItem4.Text = "Reference Number"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(90, 13)
            '
            'EmptySpaceItem1
            '
            Me.EmptySpaceItem1.AllowHotTrack = False
            Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 89)
            Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Size = New System.Drawing.Size(345, 16)
            Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
            Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Control = Me.SimpleButton_OK
            Me.LayoutControlItem5.CustomizationFormText = "LayoutControlItem5"
            Me.LayoutControlItem5.Location = New System.Drawing.Point(102, 105)
            Me.LayoutControlItem5.Name = "LayoutControlItem5"
            Me.LayoutControlItem5.Size = New System.Drawing.Size(79, 30)
            Me.LayoutControlItem5.Text = "LayoutControlItem5"
            Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem5.TextToControlDistance = 0
            Me.LayoutControlItem5.TextVisible = False
            '
            'LayoutControlItem6
            '
            Me.LayoutControlItem6.Control = Me.SimpleButton_Cancel
            Me.LayoutControlItem6.CustomizationFormText = "LayoutControlItem6"
            Me.LayoutControlItem6.Location = New System.Drawing.Point(181, 105)
            Me.LayoutControlItem6.Name = "LayoutControlItem6"
            Me.LayoutControlItem6.Size = New System.Drawing.Size(79, 30)
            Me.LayoutControlItem6.Text = "LayoutControlItem6"
            Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem6.TextToControlDistance = 0
            Me.LayoutControlItem6.TextVisible = False
            '
            'EmptySpaceItem_Left
            '
            Me.EmptySpaceItem_Left.AllowHotTrack = False
            Me.EmptySpaceItem_Left.CustomizationFormText = "EmptySpaceItem2"
            Me.EmptySpaceItem_Left.Location = New System.Drawing.Point(0, 105)
            Me.EmptySpaceItem_Left.Name = "EmptySpaceItem_Left"
            Me.EmptySpaceItem_Left.Size = New System.Drawing.Size(102, 30)
            Me.EmptySpaceItem_Left.Text = "EmptySpaceItem_Left"
            Me.EmptySpaceItem_Left.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem_Right
            '
            Me.EmptySpaceItem_Right.AllowHotTrack = False
            Me.EmptySpaceItem_Right.CustomizationFormText = "EmptySpaceItem3"
            Me.EmptySpaceItem_Right.Location = New System.Drawing.Point(260, 105)
            Me.EmptySpaceItem_Right.Name = "EmptySpaceItem_Right"
            Me.EmptySpaceItem_Right.Size = New System.Drawing.Size(85, 30)
            Me.EmptySpaceItem_Right.Text = "EmptySpaceItem_Right"
            Me.EmptySpaceItem_Right.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem2
            '
            Me.EmptySpaceItem2.AllowHotTrack = False
            Me.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Location = New System.Drawing.Point(196, 0)
            Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Size = New System.Drawing.Size(149, 24)
            Me.EmptySpaceItem2.Text = "EmptySpaceItem2"
            Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
            '
            'VendorID1
            '
            Me.VendorID1.AllowDrop = True
            Me.VendorID1.Location = New System.Drawing.Point(105, 12)
            Me.VendorID1.Name = "VendorID1"
            Me.VendorID1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DebtPlus.UI.Common.Controls.SearchButton()})
            Me.VendorID1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.VendorID1.Size = New System.Drawing.Size(99, 20)
            Me.VendorID1.StyleController = Me.LayoutControl1
            Me.VendorID1.TabIndex = 10
            '
            'LayoutControlItem7
            '
            Me.LayoutControlItem7.Control = Me.VendorID1
            Me.LayoutControlItem7.CustomizationFormText = "Vendor ID"
            Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem7.Name = "LayoutControlItem7"
            Me.LayoutControlItem7.Size = New System.Drawing.Size(196, 24)
            Me.LayoutControlItem7.Text = "Vendor ID"
            Me.LayoutControlItem7.TextSize = New System.Drawing.Size(90, 13)
            '
            'Form_NewProduct
            '
            Me.AcceptButton = Me.SimpleButton_OK
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.SimpleButton_Cancel
            Me.ClientSize = New System.Drawing.Size(365, 155)
            Me.Controls.Add(Me.LayoutControl1)
            Me.Name = "Form_NewProduct"
            Me.Text = "New Product Information"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.TextEdit_ReferenceNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_ProductType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem_Left, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem_Right, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.VendorID1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents SimpleButton_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents TextEdit_ReferenceNumber As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LookUpEdit_ProductType As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LabelControl_VendorName As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem_Left As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem_Right As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents VendorID1 As DebtPlus.UI.Vendor.Widgets.Controls.VendorID
        Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    End Class
End Namespace
