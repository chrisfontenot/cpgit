#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.UI.Client.Service

Namespace forms
    Friend Class Form_ClientDeposit

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        ' Record to be edited
        Private record As DebtPlus.LINQ.client_deposit

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(ByVal record As DebtPlus.LINQ.client_deposit)
            MyClass.New()
            Me.record = record
            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler SimpleButton1.Click, AddressOf SimpleButton1_Click
            AddHandler Load, AddressOf Form_ClientDeposit_Load
            AddHandler CalcEdit1.EditValueChanged, AddressOf CalcEdit1_EditValueChanged
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler Load, AddressOf Form_ClientDeposit_Load
            RemoveHandler CalcEdit1.EditValueChanged, AddressOf CalcEdit1_EditValueChanged
        End Sub

        ''' <summary>
        ''' Handle the FORM LOAD event
        ''' </summary>
        Private Sub Form_ClientDeposit_Load(ByVal sender As Object, ByVal e As EventArgs)

            UnRegisterHandlers()
            Try
                ' Ensure that the date is at least today. It is required for the update.
                Dim LowestDate As DateTime = DateTime.Now.Date
                DateEdit1.Properties.MinValue = LowestDate

                Dim NewDate As DateTime = record.deposit_date.Date
                Do While NewDate.Subtract(LowestDate).TotalDays > 365
                    NewDate = NewDate.AddYears(1)
                Loop

                Do While NewDate < LowestDate
                    NewDate = NewDate.AddMonths(1)
                Loop

                ' Load the controls with the record contents
                DateEdit1.EditValue = NewDate
                CalcEdit1.EditValue = record.deposit_amount
                CheckEdit1.EditValue = record.one_time

                SimpleButton1.Enabled = (Convert.ToDecimal(CalcEdit1.EditValue) > 0D)
            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Process a pending change in the dollar amount
        ''' </summary>
        Private Sub CalcEdit1_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            SimpleButton1.Enabled = (Convert.ToDecimal(CalcEdit1.EditValue) > 0D)
        End Sub

        Private Sub SimpleButton1_Click(sender As Object, e As System.EventArgs)
            record.deposit_date = Convert.ToDateTime(DateEdit1.EditValue)
            record.deposit_amount = Convert.ToDecimal(CalcEdit1.EditValue)
            record.one_time = Convert.ToBoolean(CheckEdit1.EditValue)
        End Sub
    End Class
End Namespace
