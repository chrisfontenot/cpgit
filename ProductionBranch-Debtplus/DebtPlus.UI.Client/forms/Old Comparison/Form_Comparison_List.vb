#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DebtPlus.UI.Client.Service
Imports DevExpress.XtraTab
Imports DevExpress.XtraBars
Imports DevExpress.XtraPrinting.Localization
Imports System.Threading
Imports DebtPlus.UI.Client.forms.Comparison.Report
Imports DebtPlus.Reports.Template

Namespace forms.Comparison

    Friend Class Form_Comparison_List
        Implements IContext

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Friend SalesFile As SalesFileInfoComparisonClass
        Private ReadOnly drv As DataRowView

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(ByVal Context As ClientUpdateClass)
            MyBase.New()
            InitializeComponent()
            privateContext = Context
        End Sub

        Public Sub New(ByVal Context As ClientUpdateClass, ByVal drv As DataRowView)
            MyBase.New()
            InitializeComponent()
            privateContext = Context
            MyClass.drv = drv
        End Sub

        ''' <summary>
        ''' Pointer to the client update information for the current client
        ''' </summary>
        Private privateContext As ClientUpdateClass

        Public ReadOnly Property Context() As ClientUpdateClass Implements IContext.Context
            Get
                Return privateContext
            End Get
        End Property

        Private Sub Form_Comparison_List_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

            ' Build the list of known creditors and types for editing
            SalesFile = New SalesFileInfoComparisonClass(Context, drv)
            AddHandler FormClosing, AddressOf Form_Comparison_List_FormClosing

            ' Set the default page and monitor the changes to the tabs
            LoadPage(XtraTabControl1.SelectedTabPageIndex)
            AddHandler XtraTabControl1.SelectedPageChanging, AddressOf XtraTabControl1_SelectedPageChanging
        End Sub

        Private Sub XtraTabControl1_SelectedPageChanging(ByVal sender As Object, ByVal e As TabPageChangingEventArgs)
            LoadPage(XtraTabControl1.TabPages.IndexOf(e.Page))
        End Sub

        Private Sub LoadPage(ByVal Index As Int32)
            Select Case Index
                Case 0
                    Comparison_ClientControl1.Read()
                Case 1
                    Comparison_PlanControl1.Read()
                Case 2
                    Comparison_CompareControl1.Read()
                Case Else
            End Select
        End Sub

        Private Sub BarButtonItem_SaveAndPrint_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) _
            Handles BarButtonItem_Print.ItemClick
            PrintInformation(SalesFile)
        End Sub

        Private Sub BarButtonItem_Save_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) _
            Handles BarButtonItem_Save.ItemClick
            SalesFile.WriteDebts()
        End Sub

        Private Sub BarButtonItem_Export_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) _
            Handles BarButtonItem_Export.ItemClick
            RemoveHandler FormClosing, AddressOf Form_Comparison_List_FormClosing
            SalesFile.WriteDebts(True)
            DialogResult = DialogResult.Yes
        End Sub

        Private Sub BarButtonItem_Exit_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs) _
            Handles BarButtonItem_Exit.ItemClick
            RemoveHandler FormClosing, AddressOf Form_Comparison_List_FormClosing
            SalesFile.WriteDebts(False)
            DialogResult = DialogResult.OK
        End Sub

        Private Sub Form_Comparison_List_FormClosing(ByVal sender As Object, ByVal e As FormClosingEventArgs)
            SalesFile.WriteDebts(False)
        End Sub

        Public Sub PrintInformation(ByVal salesFileinfo As SalesFileInfoComparisonClass)
            Dim thrd As New Thread(New ParameterizedThreadStart(AddressOf ReportPrintThread))
            thrd.Name = "ComparisonPrintThread"
            thrd.IsBackground = True
            thrd.SetApartmentState(ApartmentState.STA)
            thrd.Start(salesFileinfo)
        End Sub

        Private Sub ReportPrintThread(ByVal obj As Object)

            Dim salesFileInfo As SalesFileInfoComparisonClass = CType(obj, DebtPlus.UI.Client.Service.SalesFileInfoComparisonClass)
            PreviewLocalizer.Active = New MyLocalizerClass
            Using rpt As New Comparison_Report(salesFileInfo)
                Using frm As New PrintPreviewForm(rpt)
                    frm.ShowDialog()
                End Using
            End Using
        End Sub

        ''' <summary>
        ''' Localization for the report printing to set the appropriate header text
        ''' </summary>
        Private Class MyLocalizerClass
            Inherits PreviewLocalizer

            Public Overrides Function GetLocalizedString(ByVal id As PreviewStringId) As String
                If id = PreviewStringId.PreviewForm_Caption Then
                    Return MyBase.GetLocalizedString(id) + " - Comparison Information"
                Else
                    Return MyBase.GetLocalizedString(id)
                End If
            End Function
        End Class
    End Class
End Namespace
