﻿Namespace forms.Comparison

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form_Comparison_List
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then
                        components.Dispose()
                    End If
                    If SalesFile IsNot Nothing Then
                        SalesFile.Dispose()
                        SalesFile = Nothing
                    End If
                End If

            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container
            Me.barManager1 = New DevExpress.XtraBars.BarManager(Me.components)
            Me.Bar2 = New DevExpress.XtraBars.Bar
            Me.BarSubItem1 = New DevExpress.XtraBars.BarSubItem
            Me.BarButtonItem_Print = New DevExpress.XtraBars.BarButtonItem
            Me.BarButtonItem_Export = New DevExpress.XtraBars.BarButtonItem
            Me.BarButtonItem_Save = New DevExpress.XtraBars.BarButtonItem
            Me.BarButtonItem_Exit = New DevExpress.XtraBars.BarButtonItem
            Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl
            Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl
            Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl
            Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl
            Me.BarButtonItem_SaveAndExit = New DevExpress.XtraBars.BarButtonItem
            Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl
            Me.XtraTabPage_Client = New DevExpress.XtraTab.XtraTabPage
            Me.Comparison_ClientControl1 = New Comparison_ClientControl
            Me.XtraTabPage_Agency = New DevExpress.XtraTab.XtraTabPage
            Me.Comparison_PlanControl1 = New Comparison_PlanControl
            Me.XtraTabPage_Comparison = New DevExpress.XtraTab.XtraTabPage
            Me.Comparison_CompareControl1 = New Comparison_CompareControl
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.XtraTabControl1.SuspendLayout()
            Me.XtraTabPage_Client.SuspendLayout()
            Me.XtraTabPage_Agency.SuspendLayout()
            Me.XtraTabPage_Comparison.SuspendLayout()
            Me.SuspendLayout()
            '
            'barManager1
            '
            Me.barManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar2})
            Me.barManager1.DockControls.Add(Me.barDockControlTop)
            Me.barManager1.DockControls.Add(Me.barDockControlBottom)
            Me.barManager1.DockControls.Add(Me.barDockControlLeft)
            Me.barManager1.DockControls.Add(Me.barDockControlRight)
            Me.barManager1.Form = Me
            Me.barManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarSubItem1, Me.BarButtonItem_Print, Me.BarButtonItem_Export, Me.BarButtonItem_Save, Me.BarButtonItem_SaveAndExit, Me.BarButtonItem_Exit})
            Me.barManager1.MainMenu = Me.Bar2
            Me.barManager1.MaxItemId = 6
            '
            'Bar2
            '
            Me.Bar2.BarName = "Main menu"
            Me.Bar2.DockCol = 0
            Me.Bar2.DockRow = 0
            Me.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
            Me.Bar2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem1)})
            Me.Bar2.OptionsBar.MultiLine = True
            Me.Bar2.OptionsBar.UseWholeRow = True
            Me.Bar2.Text = "Main menu"
            '
            'BarSubItem1
            '
            Me.BarSubItem1.Caption = "&File"
            Me.BarSubItem1.Id = 0
            Me.BarSubItem1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Print), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Export, True), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Save), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Exit)})
            Me.BarSubItem1.Name = "BarSubItem1"
            '
            'BarButtonItem_Print
            '
            Me.BarButtonItem_Print.Caption = "Print..."
            Me.BarButtonItem_Print.Id = 1
            Me.BarButtonItem_Print.Name = "BarButtonItem_Print"
            '
            'BarButtonItem_Export
            '
            Me.BarButtonItem_Export.Caption = "&Export"
            Me.BarButtonItem_Export.Id = 2
            Me.BarButtonItem_Export.Name = "BarButtonItem_Export"
            '
            'BarButtonItem_Save
            '
            Me.BarButtonItem_Save.Caption = "Save"
            Me.BarButtonItem_Save.Id = 3
            Me.BarButtonItem_Save.Name = "BarButtonItem_Save"
            '
            'BarButtonItem_Exit
            '
            Me.BarButtonItem_Exit.Caption = "Exit"
            Me.BarButtonItem_Exit.Id = 5
            Me.BarButtonItem_Exit.Name = "BarButtonItem_Exit"
            '
            'barDockControlTop
            '
            Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
            Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
            Me.barDockControlTop.Size = New System.Drawing.Size(627, 24)
            '
            'barDockControlBottom
            '
            Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
            Me.barDockControlBottom.Location = New System.Drawing.Point(0, 367)
            Me.barDockControlBottom.Size = New System.Drawing.Size(627, 0)
            '
            'barDockControlLeft
            '
            Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
            Me.barDockControlLeft.Location = New System.Drawing.Point(0, 24)
            Me.barDockControlLeft.Size = New System.Drawing.Size(0, 343)
            '
            'barDockControlRight
            '
            Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
            Me.barDockControlRight.Location = New System.Drawing.Point(627, 24)
            Me.barDockControlRight.Size = New System.Drawing.Size(0, 343)
            '
            'BarButtonItem_SaveAndExit
            '
            Me.BarButtonItem_SaveAndExit.Caption = "Save And Exit"
            Me.BarButtonItem_SaveAndExit.Id = 4
            Me.BarButtonItem_SaveAndExit.Name = "BarButtonItem_SaveAndExit"
            '
            'XtraTabControl1
            '
            Me.XtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.XtraTabControl1.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.[True]
            Me.XtraTabControl1.Location = New System.Drawing.Point(0, 24)
            Me.XtraTabControl1.Name = "XtraTabControl1"
            Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage_Client
            Me.XtraTabControl1.Size = New System.Drawing.Size(627, 343)
            Me.XtraTabControl1.TabIndex = 5
            Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage_Client, Me.XtraTabPage_Agency, Me.XtraTabPage_Comparison})
            '
            'XtraTabPage_Client
            '
            Me.XtraTabPage_Client.Appearance.PageClient.BackColor = System.Drawing.Color.Silver
            Me.XtraTabPage_Client.Appearance.PageClient.Options.UseBackColor = True
            Me.XtraTabPage_Client.Controls.Add(Me.Comparison_ClientControl1)
            Me.XtraTabPage_Client.Name = "XtraTabPage_Client"
            Me.XtraTabPage_Client.Size = New System.Drawing.Size(620, 314)
            Me.XtraTabPage_Client.Text = "Client"
            '
            'Comparison_ClientControl1
            '
            Me.Comparison_ClientControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.Comparison_ClientControl1.Location = New System.Drawing.Point(0, 0)
            Me.Comparison_ClientControl1.Name = "Comparison_ClientControl1"
            Me.Comparison_ClientControl1.Size = New System.Drawing.Size(620, 314)
            Me.Comparison_ClientControl1.TabIndex = 0
            '
            'XtraTabPage_Agency
            '
            Me.XtraTabPage_Agency.Controls.Add(Me.Comparison_PlanControl1)
            Me.XtraTabPage_Agency.Name = "XtraTabPage_Agency"
            Me.XtraTabPage_Agency.Size = New System.Drawing.Size(620, 314)
            Me.XtraTabPage_Agency.Text = "Agency"
            '
            'Comparison_PlanControl1
            '
            Me.Comparison_PlanControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.Comparison_PlanControl1.Location = New System.Drawing.Point(0, 0)
            Me.Comparison_PlanControl1.Name = "Comparison_PlanControl1"
            Me.Comparison_PlanControl1.Size = New System.Drawing.Size(620, 314)
            Me.Comparison_PlanControl1.TabIndex = 0
            '
            'XtraTabPage_Comparison
            '
            Me.XtraTabPage_Comparison.Controls.Add(Me.Comparison_CompareControl1)
            Me.XtraTabPage_Comparison.Name = "XtraTabPage_Comparison"
            Me.XtraTabPage_Comparison.Size = New System.Drawing.Size(620, 314)
            Me.XtraTabPage_Comparison.Text = "Comparison"
            '
            'Comparison_CompareControl1
            '
            Me.Comparison_CompareControl1.Appearance.BackColor = System.Drawing.SystemColors.Window
            Me.Comparison_CompareControl1.Appearance.Options.UseBackColor = True
            Me.Comparison_CompareControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.Comparison_CompareControl1.Location = New System.Drawing.Point(0, 0)
            Me.Comparison_CompareControl1.Name = "Comparison_CompareControl1"
            Me.Comparison_CompareControl1.Size = New System.Drawing.Size(620, 314)
            Me.Comparison_CompareControl1.TabIndex = 0
            '
            'Form_Comparison_List
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(627, 367)
            Me.Controls.Add(Me.XtraTabControl1)
            Me.Controls.Add(Me.barDockControlLeft)
            Me.Controls.Add(Me.barDockControlRight)
            Me.Controls.Add(Me.barDockControlBottom)
            Me.Controls.Add(Me.barDockControlTop)
            Me.Name = "Form_Comparison_List"
            Me.Text = "Comparison Information"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.XtraTabControl1.ResumeLayout(False)
            Me.XtraTabPage_Client.ResumeLayout(False)
            Me.XtraTabPage_Agency.ResumeLayout(False)
            Me.XtraTabPage_Comparison.ResumeLayout(False)
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents barManager1 As DevExpress.XtraBars.BarManager
        Friend WithEvents Bar2 As DevExpress.XtraBars.Bar
        Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
        Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
        Friend WithEvents XtraTabPage_Client As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents Comparison_ClientControl1 As Comparison_ClientControl
        Friend WithEvents XtraTabPage_Agency As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents Comparison_PlanControl1 As Comparison_PlanControl
        Friend WithEvents XtraTabPage_Comparison As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents Comparison_CompareControl1 As Comparison_CompareControl
        Friend WithEvents BarSubItem1 As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarButtonItem_Print As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Export As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Save As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_SaveAndExit As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Exit As DevExpress.XtraBars.BarButtonItem
    End Class
End Namespace