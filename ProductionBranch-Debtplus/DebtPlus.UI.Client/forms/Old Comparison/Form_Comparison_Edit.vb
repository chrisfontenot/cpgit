#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports System.ComponentModel
Imports DebtPlus.Data.Controls
Imports DebtPlus.UI.Client.Service
Imports DevExpress.XtraBars
Imports DebtPlus.UI.Client.forms.Comparison.Report

Namespace forms.Comparison

    Friend Class Form_Comparison_Edit

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Private Const STR_Percent As String = "{0:p3}"
        Private Const STR_Currency As String = "{0:c}"

        ' Handle to the debt being updated
        Private Debt As DebtComparisonClass = Nothing
        Private dsPayout As DataSet = Nothing

        ''' <summary>
        ''' Initialize the form
        ''' </summary>
        Public Sub New(ByVal debt As DebtComparisonClass)
            MyClass.New()
            MyClass.Debt = debt
        End Sub

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            cb_Self_PaymentPercent.Properties.Items.Clear()
            For Each Str As String In New String() {"1%", "2%", "2.08%", "2.5%", "2.78%", "3%", "3.5%", "4%", "4.5%", "5%"}
                cb_Self_PaymentPercent.Properties.Items.Add(Str)
            Next

            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler SimpleButton1.Click, AddressOf SimpleButton1_Click
            AddHandler FormClosing, AddressOf Form_Comparison_Edit_FormClosing
            AddHandler cbo_Name.Validated, AddressOf Name_Validated
            AddHandler lk_CreditorType.EditValueChanged, AddressOf CreditorType_Changed
            AddHandler BarButtonItem_ClientPayout.ItemClick, AddressOf BarButtonItem_ClientPayout_ItemClick
            AddHandler BarButtonItem_AgencyPayout.ItemClick, AddressOf BarButtonItem_AgencyPayout_ItemClick
            AddHandler cb_Self_PaymentPercent.Validated, AddressOf cb_Self_PaymentPercent_Validated
            AddHandler cb_Self_PaymentPercent.EditValueChanging, AddressOf cb_Self_PaymentPercent_EditValueChanging
            AddHandler cb_Self_PaymentPercent.SelectedIndexChanged, AddressOf cb_Self_PaymentPercent_SelectedIndexChanged

            If Debt IsNot Nothing Then
                AddHandler Debt.PropertyChanged, AddressOf Debt_PropertyChanged
            End If
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler SimpleButton1.Click, AddressOf SimpleButton1_Click
            RemoveHandler FormClosing, AddressOf Form_Comparison_Edit_FormClosing
            RemoveHandler cbo_Name.Validated, AddressOf Name_Validated
            RemoveHandler lk_CreditorType.EditValueChanged, AddressOf CreditorType_Changed
            RemoveHandler BarButtonItem_ClientPayout.ItemClick, AddressOf BarButtonItem_ClientPayout_ItemClick
            RemoveHandler BarButtonItem_AgencyPayout.ItemClick, AddressOf BarButtonItem_AgencyPayout_ItemClick
            RemoveHandler cb_Self_PaymentPercent.Validated, AddressOf cb_Self_PaymentPercent_Validated
            RemoveHandler cb_Self_PaymentPercent.EditValueChanging, AddressOf cb_Self_PaymentPercent_EditValueChanging
            RemoveHandler cb_Self_PaymentPercent.SelectedIndexChanged, AddressOf cb_Self_PaymentPercent_SelectedIndexChanged

            If Debt IsNot Nothing Then
                RemoveHandler Debt.PropertyChanged, AddressOf Debt_PropertyChanged
            End If
        End Sub

        Private Sub Form_Comparison_Edit_FormClosing(ByVal sender As Object, ByVal e As FormClosingEventArgs)
            UnRegisterHandlers()
            SavePlacement("ClientUpdate.Comparison.Edit")

            If cbo_Name.SelectedIndex >= 0 Then
                Debt.Name = CType(cbo_Name.SelectedItem, DebtPlus.Data.Controls.ComboboxItem).description
            End If
        End Sub

        Private Sub Form_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
            LoadPlacement("ClientUpdate.Comparison.Edit")

            lk_CreditorType.DataBindings.Clear()
            tx_AccountNumber.DataBindings.Clear()
            clc_Balance.DataBindings.Clear()
            clc_Self_LateFees.DataBindings.Clear()
            clc_Self_OverlimitFees.DataBindings.Clear()
            clc_DMP_EnteredPayment.DataBindings.Clear()
            clc_DMP_InterestRate.DataBindings.Clear()
            clc_Self_InterestRate.DataBindings.Clear()

            UnRegisterHandlers()
            Try

                lk_CreditorType.Properties.DataSource = Debt.SalesFile.KnownTypeList

                Dim SelectedValue As Int32 = -1
                Dim ie As IEnumerator = Debt.SalesFile.KnownCreditorList.GetEnumerator
                Do While ie.MoveNext
                    Dim NamedCreditor As KnownCreditorComparisonClass = CType(ie.Current, KnownCreditorComparisonClass)
                    Dim NewIndex As Int32 = cbo_Name.Properties.Items.Add(New DebtPlus.Data.Controls.ComboboxItem(NamedCreditor.Name, NamedCreditor.Key))
                    If String.Compare(NamedCreditor.Name, Debt.Name, True) = 0 Then
                        SelectedValue = NewIndex
                        Exit Do
                    End If
                Loop

                cbo_Name.SelectedIndex = SelectedValue
                If SelectedValue < 0 Then cbo_Name.Text = Debt.Name

                cb_Self_PaymentPercent.Text = FormatDebtPercent(Debt.Self_PaymentPercent)

                lk_CreditorType.DataBindings.Add("EditValue", Debt, "CreditorType", False, DataSourceUpdateMode.OnPropertyChanged)
                tx_AccountNumber.DataBindings.Add("EditValue", Debt, "AccountNumber", False, DataSourceUpdateMode.OnPropertyChanged)
                clc_Balance.DataBindings.Add("EditValue", Debt, "Balance", False, DataSourceUpdateMode.OnPropertyChanged)
                clc_Self_LateFees.DataBindings.Add("EditValue", Debt, "Self_LateFees", False, DataSourceUpdateMode.OnPropertyChanged)
                clc_Self_OverlimitFees.DataBindings.Add("EditValue", Debt, "Self_OverLimitFees", False, DataSourceUpdateMode.OnPropertyChanged)
                clc_DMP_EnteredPayment.DataBindings.Add("EditValue", Debt, "DMP_EnteredPayment", False, DataSourceUpdateMode.OnPropertyChanged)
                clc_DMP_InterestRate.DataBindings.Add("EditValue", Debt, "DMP_InterestRate", False, DataSourceUpdateMode.OnPropertyChanged)
                clc_Self_InterestRate.DataBindings.Add("EditValue", Debt, "Self_InterestRate", False, DataSourceUpdateMode.OnPropertyChanged)

                ' Display the text fields and the policy matrix values
                ShowPolicy()
                DisplayTextFields()

            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Function ParseDebtPercent(ByVal str As String) As Double
            Dim dblPct As Double
            If Double.TryParse(str.Trim().TrimEnd("%"c), dblPct) Then
                While dblPct >= 1.0#
                    dblPct /= 100.0#
                End While
                Return dblPct
            End If
            Return 0.0#
        End Function

        Private Function FormatDebtPercent(ByVal obj As Object) As String
            If obj Is Nothing OrElse System.Object.Equals(obj, System.DBNull.Value) Then
                Return String.Empty
            End If
            Dim dblPct As Double = DirectCast(obj, Double)
            If dblPct = 0.0# Then Return "0%"
            Dim str As String = (dblPct * 100.0#).ToString().Trim("0"c).TrimEnd("."c) + "%"
            Return str
        End Function

        Private Sub Name_Validated(ByVal sender As Object, ByVal e As EventArgs)
            UnRegisterHandlers()
            Try
                Debt.Name = cbo_Name.Text.Trim()
                lk_CreditorType.EditValue = Debt.CreditorType
                ShowPolicy()
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Sub CreditorType_Changed(ByVal sender As Object, ByVal e As EventArgs)
            UnRegisterHandlers()
            Try
                Debt.CreditorType = Convert.ToString(lk_CreditorType.EditValue)
                ShowPolicy()
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Sub ShowPolicy()
            RichEditControl1.Text = String.Empty

            ' If there is a creditor that matches the item then show that policy matrix value
            If Debt.Creditor > 0 Then
                Dim cr As KnownCreditorComparisonClass = Debt.SalesFile.KnownCreditorList.Find(Debt.Creditor)
                If cr IsNot Nothing Then
                    RichEditControl1.RtfText = cr.PolicyMatrix()
                End If
            End If
        End Sub

        Private Sub Debt_PropertyChanged(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
            UnRegisterHandlers()
            Try
                DisplayTextFields()
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Sub DisplayTextFields()

            lb_DMP_InterestFees.Text = String.Format(STR_Currency, Debt.DMP_InterestFees)
            lb_DMP_MinimumPayment.Text = String.Format(STR_Currency, Debt.DMP_MinimumPayment)
            lb_DMP_MinimumProrate.Text = String.Format(STR_Percent, Debt.DMP_MinimumProrate)
            lb_DMP_FinanceCharge.Text = String.Format(STR_Currency, Debt.DMP_FinanceCharge)

            lb_DMP_GoingToPrincipal.Text = String.Format(STR_Currency, Debt.DMP_GoingToPrincipal)

            ' Change the color if the value is unsuitable for paying off the debt
            If Debt.DMP_GoingToPrincipal <= 0D AndAlso Debt.Balance > 0D Then
                If Not lb_DMP_GoingToPrincipal.Font.Bold Then
                    lb_DMP_GoingToPrincipal.Font = New Font(lb_DMP_GoingToPrincipal.Font, FontStyle.Bold)
                    lb_DMP_GoingToPrincipal.BackColor = Color.Red
                    lb_DMP_GoingToPrincipal.ForeColor = Color.White
                End If
            Else
                If lb_DMP_GoingToPrincipal.Font.Bold Then
                    lb_DMP_GoingToPrincipal.Font = New Font(lb_DMP_GoingToPrincipal.Font, FontStyle.Regular)
                    lb_DMP_GoingToPrincipal.BackColor = lb_DMP_InterestFees.BackColor
                    lb_DMP_GoingToPrincipal.ForeColor = lb_DMP_InterestFees.ForeColor
                End If
            End If

            lb_Self_MonthlyFinanceCharge.Text = String.Format(STR_Currency, Debt.Self_MonthlyFinanceCharge)
            lb_Self_InterestAndFees.Text = String.Format(STR_Currency, Debt.Self_InterestAndFees)
            lb_Self_MonthlyPayment.Text = String.Format(STR_Currency, Debt.Self_MonthlyPayment)

            lb_Self_GoingToPrincipal.Text = String.Format(STR_Currency, Debt.Self_GoingToPrincipal)

            ' Change the color if the value is unsuitable for paying off the debt
            If Debt.Self_GoingToPrincipal <= 0D AndAlso Debt.Balance > 0D Then
                If Not lb_Self_GoingToPrincipal.Font.Bold Then
                    lb_Self_GoingToPrincipal.Font = New Font(lb_Self_GoingToPrincipal.Font, FontStyle.Bold)
                    lb_Self_GoingToPrincipal.BackColor = Color.Red
                    lb_Self_GoingToPrincipal.ForeColor = Color.White
                End If
            Else
                If lb_Self_GoingToPrincipal.Font.Bold Then
                    lb_Self_GoingToPrincipal.Font = New Font(lb_Self_GoingToPrincipal.Font, FontStyle.Regular)
                    lb_Self_GoingToPrincipal.BackColor = lb_DMP_InterestFees.BackColor
                    lb_Self_GoingToPrincipal.ForeColor = lb_DMP_InterestFees.ForeColor
                End If
            End If
        End Sub

        Private Sub SimpleButton1_Click(ByVal Sender As Object, ByVal e As EventArgs)
#If 0 Then
        ' Try to compute the length of the plan with these numbers
        Dim current_balance As Decimal = DebtPlus.Utils.Nulls.DDec(clc_Balance.EditValue)
        Dim rate As Double = DebtPlus.Utils.Nulls.DDbl(clc_DMP_InterestRate.EditValue)
        Dim payment As Double = DebtPlus.Utils.Nulls.DDbl(clc_DMP_EnteredPayment.EditValue)

        Dim StatusMessage As New System.Text.StringBuilder
        StatusMessage.Append(Environment.NewLine)
        StatusMessage.Append(Environment.NewLine)
        StatusMessage.AppendFormat("Balance = {0:c2}", current_balance)
        StatusMessage.Append(Environment.NewLine)
        StatusMessage.AppendFormat("Interest Rate = {0:p}", rate)
        StatusMessage.Append(Environment.NewLine)
        StatusMessage.AppendFormat("Payment = {0:c}", payment)

        ' The information needs to be acceptable or cancelled.
        If payment <= 0 Then
            DebtPlus.Data.Forms.MessageBox.Show("The items entered below do not allow the debt to be paid." + StatusMessage. _
ToString, "Sorry, please check the data again.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return
        End If

        Dim Periods As Double = 0.0
        Try
            If rate <= 0.0 Then
                Periods = current_balance / payment
            Else
                Periods = Microsoft.VisualBasic.NPer(rate / 12.0#, -payment, current_balance)
            End If
        Catch ex As Exception
        End Try

        ' If there are too many months then ask if the values should be changed.
        If Periods < 0.0 OrElse System.Math.Round(Periods) > 60.0 Then
            StatusMessage.Append(Environment.NewLine)
            StatusMessage.AppendFormat("Months = {0:n0}", Periods)

            Dim PMT As Double = 1D - Microsoft.VisualBasic.Pmt(rate / 12.0, 60.0, current_balance)
            StatusMessage.Append(Environment.NewLine)
            StatusMessage.Append(Environment.NewLine)
            StatusMessage.AppendFormat(
"In order to complete in about 60 months you would need a payment of at least {0:c}", PMT)

            If DebtPlus.Data.Forms.MessageBox.Show(
"The items entered below exceed the 60 month (5 year) plan maximum. Do you wish to adjust them now?" + StatusMessage. _
ToString, "Sorry, please check the data again.", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation,
MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes Then
                Return
            End If
        End If
#End If
            ' All is well. Set the dialog answer
            DialogResult = DialogResult.OK
        End Sub

        Private Sub BarButtonItem_ClientPayout_ItemClick(sender As Object, e As ItemClickEventArgs)

            UnRegisterHandlers()
            dsPayout = New System.Data.DataSet("dsPayout")
            Try
                ' Construct the table to hold the payout information
                Dim tbl As New DataTable("ClientPayout")
                tbl.Columns.Add("month", GetType(Int32))
                tbl.Columns.Add("payment", GetType(Decimal))
                tbl.Columns.Add("interest", GetType(Decimal))
                tbl.Columns.Add("principal", GetType(Decimal))
                tbl.Columns.Add("balance", GetType(Decimal))
                dsPayout.Tables.Add(tbl)

                ' Calculate the payout now
                AddHandler Debt.ClientPayout, AddressOf PayoutEvent
                Try
                    Debt.Self_CalculatePayout()
                Finally
                    RemoveHandler Debt.ClientPayout, AddressOf PayoutEvent
                End Try

                ' Print the information that is now in the table
                Using rpt As New PayoutSchedule()
                    rpt.SetTitle("Client Payout Schedule")
                    rpt.DataSource = tbl
                    rpt.ShowPreviewDialog()
                End Using

            Finally
                dsPayout.Dispose()
                dsPayout = Nothing
                RegisterHandlers()
            End Try
        End Sub

        Private Sub BarButtonItem_AgencyPayout_ItemClick(sender As Object, e As ItemClickEventArgs)
            UnRegisterHandlers()
            dsPayout = New DataSet("dsPayout")

            Try
                ' Construct the table to hold the payout information
                Dim tbl As New DataTable("AgencyPayout")
                tbl.Columns.Add("month", GetType(Int32))
                tbl.Columns.Add("payment", GetType(Decimal))
                tbl.Columns.Add("interest", GetType(Decimal))
                tbl.Columns.Add("principal", GetType(Decimal))
                tbl.Columns.Add("balance", GetType(Decimal))
                dsPayout.Tables.Add(tbl)

                ' Calculate the payout now
                AddHandler Debt.PlanPayout, AddressOf PayoutEvent
                Try
                    Debt.DMP_CalculatePayout()
                Finally
                    RemoveHandler Debt.PlanPayout, AddressOf PayoutEvent
                End Try

                ' Print the information that is now in the table
                Using rpt As New PayoutSchedule()
                    rpt.SetTitle("PLAN Payout Schedule")
                    rpt.DataSource = tbl
                    rpt.ShowPreviewDialog()
                End Using

            Finally
                dsPayout.Dispose()
                dsPayout = Nothing
                RegisterHandlers()
            End Try
        End Sub

        Private Sub PayoutEvent(ByVal sender As Object, ByVal e As DebtComparisonClass.PayoutEventArgs)
            Dim tbl As DataTable = dsPayout.Tables(0)
            Dim row As DataRow = tbl.NewRow()
            row("month") = e.Month
            row("payment") = e.Payment
            row("interest") = e.Interest
            row("principal") = e.Principal
            row("balance") = e.Balance
            tbl.Rows.Add(row)
        End Sub

        Private Sub cb_Self_PaymentPercent_EditValueChanging(sender As Object, e As DevExpress.XtraEditors.Controls.ChangingEventArgs)
            If e.NewValue Is Nothing Then Return

            Dim strNewValue As String = Convert.ToString(e.NewValue)
            If String.IsNullOrEmpty(strNewValue) Then Return

            ' Check to ensure that the person put in a valid rate
            If (New System.Text.RegularExpressions.Regex("\d*(\.\d*)?%?")).IsMatch(strNewValue) Then Return

            ' The entry does not match the valid pattern. Reject it.
            e.Cancel = True
        End Sub

        Private Sub cb_Self_PaymentPercent_SelectedIndexChanged(sender As Object, e As System.EventArgs)
            Dim pct As Double = ParseDebtPercent(cb_Self_PaymentPercent.Text)
            Debt.Self_PaymentPercent = pct
        End Sub

        Private Sub cb_Self_PaymentPercent_Validated(sender As Object, e As System.EventArgs)
            Dim pct As Double = ParseDebtPercent(cb_Self_PaymentPercent.Text)
            Debt.Self_PaymentPercent = pct
            UnRegisterHandlers()
            Try
                cb_Self_PaymentPercent.Text = FormatDebtPercent(pct)
            Finally
                RegisterHandlers()
            End Try
        End Sub
    End Class
End Namespace
