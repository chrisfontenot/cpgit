﻿Namespace forms.Comparison

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Comparison_PlanControl
        Inherits DevExpress.XtraEditors.XtraUserControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl
            Me.GridView1 = New Comparison_GridView
            Me.GridColumn_Type = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_Creditor = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Creditor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_Balance = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Balance.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_MinPayment = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_MinPayment.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_MinProrate = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_MinProrate.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_Payment = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Payment.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_InterestRate = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_InterestRate.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_FinanceAmount = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_FinanceAmount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_TotalFees = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_TotalFees.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_DollarsToPrincipal = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_DollarsToPrincipal.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridControl1
            '
            Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.GridControl1.EmbeddedNavigator.Name = ""
            Me.GridControl1.Location = New System.Drawing.Point(0, 0)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(411, 150)
            Me.GridControl1.TabIndex = 0
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_Type, Me.GridColumn_Creditor, Me.GridColumn_Balance, Me.GridColumn_MinPayment, Me.GridColumn_MinProrate, Me.GridColumn_Payment, Me.GridColumn_InterestRate, Me.GridColumn_FinanceAmount, Me.GridColumn_TotalFees, Me.GridColumn_DollarsToPrincipal})
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsView.ShowFooter = True
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            '
            'GridColumn_Type
            '
            Me.GridColumn_Type.Caption = "T"
            Me.GridColumn_Type.CustomizationCaption = "Creditor Type"
            Me.GridColumn_Type.FieldName = "CreditorType"
            Me.GridColumn_Type.Name = "GridColumn_Type"
            Me.GridColumn_Type.Visible = True
            Me.GridColumn_Type.VisibleIndex = 0
            '
            'GridColumn_Creditor
            '
            Me.GridColumn_Creditor.Caption = "Creditor"
            Me.GridColumn_Creditor.CustomizationCaption = "Creditor Name"
            Me.GridColumn_Creditor.FieldName = "Name"
            Me.GridColumn_Creditor.Name = "GridColumn_Creditor"
            Me.GridColumn_Creditor.Visible = True
            Me.GridColumn_Creditor.VisibleIndex = 1
            '
            'GridColumn_Balance
            '
            Me.GridColumn_Balance.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Balance.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Balance.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Balance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Balance.Caption = "Balance"
            Me.GridColumn_Balance.CustomizationCaption = "Balance"
            Me.GridColumn_Balance.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_Balance.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Balance.FieldName = "Balance"
            Me.GridColumn_Balance.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_Balance.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Balance.Name = "GridColumn_Balance"
            Me.GridColumn_Balance.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_Balance.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_Balance.Visible = True
            Me.GridColumn_Balance.VisibleIndex = 2
            '
            'GridColumn_MinPayment
            '
            Me.GridColumn_MinPayment.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_MinPayment.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_MinPayment.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_MinPayment.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_MinPayment.Caption = "Min Payment"
            Me.GridColumn_MinPayment.CustomizationCaption = "Minimum Payment"
            Me.GridColumn_MinPayment.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_MinPayment.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_MinPayment.FieldName = "DMP_MinimumPayment"
            Me.GridColumn_MinPayment.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_MinPayment.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_MinPayment.Name = "GridColumn_MinPayment"
            Me.GridColumn_MinPayment.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_MinPayment.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_MinPayment.Visible = True
            Me.GridColumn_MinPayment.VisibleIndex = 3
            '
            'GridColumn_MinProrate
            '
            Me.GridColumn_MinProrate.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_MinProrate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_MinProrate.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_MinProrate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_MinProrate.Caption = "Min Prorate"
            Me.GridColumn_MinProrate.CustomizationCaption = "Minimum Prorate"
            Me.GridColumn_MinProrate.DisplayFormat.FormatString = "{0:p}"
            Me.GridColumn_MinProrate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_MinProrate.FieldName = "DMP_MinimumProrate"
            Me.GridColumn_MinProrate.GroupFormat.FormatString = "{0:p}"
            Me.GridColumn_MinProrate.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_MinProrate.Name = "GridColumn_MinProrate"
            Me.GridColumn_MinProrate.Visible = True
            Me.GridColumn_MinProrate.VisibleIndex = 4
            '
            'GridColumn_Payment
            '
            Me.GridColumn_Payment.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Payment.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Payment.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Payment.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Payment.Caption = "Payment"
            Me.GridColumn_Payment.CustomizationCaption = "Payment Amount"
            Me.GridColumn_Payment.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_Payment.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Payment.FieldName = "DMP_EnteredPayment"
            Me.GridColumn_Payment.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_Payment.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Payment.Name = "GridColumn_Payment"
            Me.GridColumn_Payment.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_Payment.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_Payment.Visible = True
            Me.GridColumn_Payment.VisibleIndex = 5
            '
            'GridColumn_InterestRate
            '
            Me.GridColumn_InterestRate.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_InterestRate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_InterestRate.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_InterestRate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_InterestRate.Caption = "A.P.R."
            Me.GridColumn_InterestRate.CustomizationCaption = "Interest Rate"
            Me.GridColumn_InterestRate.DisplayFormat.FormatString = "{0:p}"
            Me.GridColumn_InterestRate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_InterestRate.FieldName = "DMP_InterestRate"
            Me.GridColumn_InterestRate.GroupFormat.FormatString = "{0:p}"
            Me.GridColumn_InterestRate.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_InterestRate.Name = "GridColumn_InterestRate"
            Me.GridColumn_InterestRate.Visible = True
            Me.GridColumn_InterestRate.VisibleIndex = 6
            '
            'GridColumn_FinanceAmount
            '
            Me.GridColumn_FinanceAmount.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_FinanceAmount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_FinanceAmount.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_FinanceAmount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_FinanceAmount.Caption = "Finance"
            Me.GridColumn_FinanceAmount.CustomizationCaption = "Finance Amount"
            Me.GridColumn_FinanceAmount.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_FinanceAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_FinanceAmount.FieldName = "DMP_FinanceCharge"
            Me.GridColumn_FinanceAmount.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_FinanceAmount.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_FinanceAmount.Name = "GridColumn_FinanceAmount"
            Me.GridColumn_FinanceAmount.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_FinanceAmount.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_FinanceAmount.Visible = True
            Me.GridColumn_FinanceAmount.VisibleIndex = 7
            '
            'GridColumn_TotalFees
            '
            Me.GridColumn_TotalFees.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_TotalFees.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_TotalFees.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_TotalFees.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_TotalFees.Caption = "Total Int/Fees"
            Me.GridColumn_TotalFees.CustomizationCaption = "Total Interest and Fees"
            Me.GridColumn_TotalFees.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_TotalFees.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_TotalFees.FieldName = "DMP_Fees"
            Me.GridColumn_TotalFees.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_TotalFees.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_TotalFees.Name = "GridColumn_TotalFees"
            Me.GridColumn_TotalFees.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_TotalFees.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_TotalFees.Visible = True
            Me.GridColumn_TotalFees.VisibleIndex = 8
            '
            'GridColumn_DollarsToPrincipal
            '
            Me.GridColumn_DollarsToPrincipal.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_DollarsToPrincipal.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_DollarsToPrincipal.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_DollarsToPrincipal.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_DollarsToPrincipal.Caption = "$ Principal"
            Me.GridColumn_DollarsToPrincipal.CustomizationCaption = "Amount going to the principal"
            Me.GridColumn_DollarsToPrincipal.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_DollarsToPrincipal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_DollarsToPrincipal.FieldName = "DMP_GoingToPrincipal"
            Me.GridColumn_DollarsToPrincipal.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_DollarsToPrincipal.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_DollarsToPrincipal.Name = "GridColumn_DollarsToPrincipal"
            Me.GridColumn_DollarsToPrincipal.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_DollarsToPrincipal.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_DollarsToPrincipal.Visible = True
            Me.GridColumn_DollarsToPrincipal.VisibleIndex = 9
            '
            'Comparison_PlanControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.GridControl1)
            Me.Name = "Comparison_PlanControl"
            Me.Size = New System.Drawing.Size(411, 150)
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As Comparison_GridView
        Friend WithEvents GridColumn_Type As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Creditor As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Balance As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_MinPayment As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_MinProrate As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Payment As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_InterestRate As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_FinanceAmount As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_TotalFees As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_DollarsToPrincipal As DevExpress.XtraGrid.Columns.GridColumn
    End Class
End Namespace