#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DevExpress.XtraGrid.Views.Grid
Imports DebtPlus.UI.Client.Service
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo

Namespace forms.Comparison

    Friend Class Comparison_GridView
        Inherits GridView

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        ' Generate the menu for the grid control
        Dim WithEvents AddMenu As New MenuItem("Add...")
        Dim WithEvents EditMenu As New MenuItem("Edit...")
        Dim WithEvents DeleteMenu As New MenuItem("Delete")
        Dim WithEvents LocalMenu As New ContextMenu(New MenuItem() {AddMenu, EditMenu, DeleteMenu})
        Dim ControlRow As Int32 = -1

        Private ReadOnly Property SalesFile() As SalesFileInfoComparisonClass
            Get
                Return CType(GridControl.FindForm(), Form_Comparison_List).SalesFile
            End Get
        End Property

        Protected Overrides Sub OnEndInit()
            MyBase.OnEndInit()
            If Not DesignMode Then
                GridControl.ContextMenu = LocalMenu
            End If
        End Sub

        Private Sub LocalMenu_Popup(ByVal sender As Object, ByVal e As EventArgs) Handles LocalMenu.Popup
            If Not DesignMode Then

                ' Find the row that is curently selected
                Dim debt As DebtComparisonClass = Nothing
                If ControlRow >= 0 Then debt = SalesFile.DebtList.Item(ControlRow)

                ' Enable the edit and delete if there is a row to process
                DeleteMenu.Enabled = Not IsNothing(debt)
                EditMenu.Enabled = Not IsNothing(debt)
                AddMenu.Enabled = True
            End If
        End Sub

        Private Sub AddMenu_Click(ByVal sender As Object, ByVal e As EventArgs) Handles AddMenu.Click
            If Not DesignMode Then
                Dim gv As GridView = Me
                Dim gc As GridControl = gv.GridControl

                Dim Debt As New DebtComparisonClass(SalesFile)
                With New Form_Comparison_Edit(Debt)
                    Dim answer As DialogResult = .ShowDialog
                    .Dispose()
                    If answer = DialogResult.OK Then
                        SalesFile.DebtList.Add(Debt)
                        SalesFile.WriteDebts(False)
                        gv.RefreshData()
                    End If
                End With
            End If
        End Sub

        Private Sub DeleteMenu_Click(ByVal sender As Object, ByVal e As EventArgs) Handles DeleteMenu.Click
            If Not DesignMode Then
                Dim gv As GridView = Me
                Dim gc As GridControl = gv.GridControl

                Dim debt As DebtComparisonClass = Nothing
                If ControlRow >= 0 Then debt = SalesFile.DebtList.Item(ControlRow)
                If debt IsNot Nothing Then
                    SalesFile.DebtList.Remove(debt)
                    SalesFile.WriteDebts(False)
                    gv.RefreshData()
                End If
            End If
        End Sub

        Private Sub EditMenu_Click(ByVal sender As Object, ByVal e As EventArgs) Handles EditMenu.Click
            If Not DesignMode Then
                Dim gv As GridView = Me
                Dim gc As GridControl = gv.GridControl

                Dim debt As DebtComparisonClass = Nothing
                If ControlRow >= 0 Then debt = SalesFile.DebtList.Item(ControlRow)
                If debt IsNot Nothing Then
                    Using frm As New Form_Comparison_Edit(debt)
                        Dim answer As DialogResult = frm.ShowDialog
                        If answer = DialogResult.OK Then
                            SalesFile.WriteDebts(False)
                            gv.RefreshData()
                        End If
                    End Using
                End If
            End If
        End Sub

        Private Sub Comparison_GridView_DoubleClick(ByVal sender As Object, ByVal e As EventArgs) Handles Me.DoubleClick
            ' Find the row targetted as the double-click item
            Dim gv As GridView = Me
            Dim ctl As GridControl = gv.GridControl
            Dim hi As GridHitInfo = gv.CalcHitInfo((ctl.PointToClient(Control.MousePosition)))

            ' Find the datarowview from the input tables.
            ControlRow = hi.RowHandle
            Dim debt As DebtComparisonClass = Nothing
            If ControlRow >= 0 Then debt = SalesFile.DebtList.Item(ControlRow)

            If debt IsNot Nothing Then
                Using frm As New Form_Comparison_Edit(debt)
                    Dim answer As DialogResult = frm.ShowDialog
                    If answer = DialogResult.OK Then
                        SalesFile.WriteDebts(False)
                        gv.RefreshData()
                    End If
                End Using
            End If
        End Sub

        Private Sub Comparison_GridView_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs) Handles Me.MouseDown
            If Not DesignMode Then

                Dim gv As GridView = Me
                Dim ctl As GridControl = gv.GridControl
                Dim hi As GridHitInfo = gv.CalcHitInfo(New Point(e.X, e.Y))

                ' Remember the position for the popup menu handler.
                If hi.InRow Then
                    ControlRow = hi.RowHandle
                Else
                    ControlRow = -1
                End If
            End If
        End Sub
    End Class
End Namespace