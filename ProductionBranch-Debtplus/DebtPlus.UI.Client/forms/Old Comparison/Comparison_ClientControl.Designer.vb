﻿Namespace forms.Comparison
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Comparison_ClientControl
        Inherits DevExpress.XtraEditors.XtraUserControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl
            Me.GridView1 = New Comparison_GridView
            Me.GridColumn_Type = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_Name = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_AccountNumber = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_AccountNumber.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_Balance = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Balance.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_Self_InterestRate = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Self_InterestRate.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_Self_MonthlyPayment = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Self_MonthlyPayment.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_Self_LateFees = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Self_LateFees.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_Self_OverLimit = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Self_OverLimit.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_Self_MonthlyFinanceCharge = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Self_MonthlyFinanceCharge.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_Self_InterestAndFees = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Self_InterestAndFees.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_Self_GoingToPrincipal = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_Self_GoingToPrincipal.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridControl1
            '
            Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.GridControl1.EmbeddedNavigator.Name = ""
            Me.GridControl1.Location = New System.Drawing.Point(0, 0)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(411, 150)
            Me.GridControl1.TabIndex = 0
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_Type, Me.GridColumn_Name, Me.GridColumn_AccountNumber, Me.GridColumn_Balance, Me.GridColumn_Self_InterestRate, Me.GridColumn_Self_MonthlyPayment, Me.GridColumn_Self_LateFees, Me.GridColumn_Self_OverLimit, Me.GridColumn_Self_MonthlyFinanceCharge, Me.GridColumn_Self_InterestAndFees, Me.GridColumn_Self_GoingToPrincipal})
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsView.ShowFooter = True
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            '
            'GridColumn_Type
            '
            Me.GridColumn_Type.Caption = "T"
            Me.GridColumn_Type.CustomizationCaption = "Creditor Type"
            Me.GridColumn_Type.FieldName = "CreditorType"
            Me.GridColumn_Type.Name = "GridColumn_Type"
            Me.GridColumn_Type.OptionsColumn.AllowEdit = False
            Me.GridColumn_Type.UnboundType = DevExpress.Data.UnboundColumnType.[String]
            Me.GridColumn_Type.Visible = True
            Me.GridColumn_Type.VisibleIndex = 0
            Me.GridColumn_Type.Width = 20
            '
            'GridColumn_Name
            '
            Me.GridColumn_Name.Caption = "Creditor"
            Me.GridColumn_Name.CustomizationCaption = "Creditor Name"
            Me.GridColumn_Name.FieldName = "Name"
            Me.GridColumn_Name.Name = "GridColumn_Name"
            Me.GridColumn_Name.OptionsColumn.AllowEdit = False
            Me.GridColumn_Name.UnboundType = DevExpress.Data.UnboundColumnType.[String]
            Me.GridColumn_Name.Visible = True
            Me.GridColumn_Name.VisibleIndex = 1
            Me.GridColumn_Name.Width = 206
            '
            'GridColumn_AccountNumber
            '
            Me.GridColumn_AccountNumber.Caption = "Account Number"
            Me.GridColumn_AccountNumber.CustomizationCaption = "Account Number"
            Me.GridColumn_AccountNumber.FieldName = "AccountNumber"
            Me.GridColumn_AccountNumber.Name = "GridColumn_AccountNumber"
            Me.GridColumn_AccountNumber.OptionsColumn.AllowEdit = False
            Me.GridColumn_AccountNumber.UnboundType = DevExpress.Data.UnboundColumnType.[String]
            '
            'GridColumn_Balance
            '
            Me.GridColumn_Balance.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Balance.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Balance.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Balance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Balance.Caption = "Balance"
            Me.GridColumn_Balance.CustomizationCaption = "Balance"
            Me.GridColumn_Balance.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_Balance.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Balance.FieldName = "Balance"
            Me.GridColumn_Balance.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_Balance.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Balance.Name = "GridColumn_Balance"
            Me.GridColumn_Balance.OptionsColumn.AllowEdit = False
            Me.GridColumn_Balance.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_Balance.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_Balance.UnboundType = DevExpress.Data.UnboundColumnType.[Decimal]
            Me.GridColumn_Balance.Visible = True
            Me.GridColumn_Balance.VisibleIndex = 2
            Me.GridColumn_Balance.Width = 50
            '
            'GridColumn_Self_InterestRate
            '
            Me.GridColumn_Self_InterestRate.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Self_InterestRate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Self_InterestRate.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Self_InterestRate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Self_InterestRate.Caption = "A.P.R."
            Me.GridColumn_Self_InterestRate.CustomizationCaption = "Interest Rate (A.P.R.)"
            Me.GridColumn_Self_InterestRate.DisplayFormat.FormatString = "p3"
            Me.GridColumn_Self_InterestRate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Self_InterestRate.FieldName = "Self_InterestRate"
            Me.GridColumn_Self_InterestRate.GroupFormat.FormatString = "p3"
            Me.GridColumn_Self_InterestRate.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Self_InterestRate.Name = "GridColumn_Self_InterestRate"
            Me.GridColumn_Self_InterestRate.OptionsColumn.AllowEdit = False
            Me.GridColumn_Self_InterestRate.UnboundType = DevExpress.Data.UnboundColumnType.[Object]
            Me.GridColumn_Self_InterestRate.Visible = True
            Me.GridColumn_Self_InterestRate.VisibleIndex = 3
            Me.GridColumn_Self_InterestRate.Width = 50
            '
            'GridColumn_Self_MonthlyPayment
            '
            Me.GridColumn_Self_MonthlyPayment.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Self_MonthlyPayment.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Self_MonthlyPayment.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Self_MonthlyPayment.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Self_MonthlyPayment.Caption = "Payment"
            Me.GridColumn_Self_MonthlyPayment.CustomizationCaption = "Monthly Payment"
            Me.GridColumn_Self_MonthlyPayment.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_Self_MonthlyPayment.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Self_MonthlyPayment.FieldName = "Self_MonthlyPayment"
            Me.GridColumn_Self_MonthlyPayment.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_Self_MonthlyPayment.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Self_MonthlyPayment.Name = "GridColumn_Self_MonthlyPayment"
            Me.GridColumn_Self_MonthlyPayment.OptionsColumn.AllowEdit = False
            Me.GridColumn_Self_MonthlyPayment.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_Self_MonthlyPayment.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_Self_MonthlyPayment.UnboundType = DevExpress.Data.UnboundColumnType.[Decimal]
            Me.GridColumn_Self_MonthlyPayment.Visible = True
            Me.GridColumn_Self_MonthlyPayment.VisibleIndex = 4
            Me.GridColumn_Self_MonthlyPayment.Width = 64
            '
            'GridColumn_Self_LateFees
            '
            Me.GridColumn_Self_LateFees.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Self_LateFees.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Self_LateFees.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Self_LateFees.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Self_LateFees.Caption = "Late"
            Me.GridColumn_Self_LateFees.CustomizationCaption = "Late Fees"
            Me.GridColumn_Self_LateFees.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_Self_LateFees.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Self_LateFees.FieldName = "Self_LateFees"
            Me.GridColumn_Self_LateFees.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_Self_LateFees.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Self_LateFees.Name = "GridColumn_Self_LateFees"
            Me.GridColumn_Self_LateFees.OptionsColumn.AllowEdit = False
            Me.GridColumn_Self_LateFees.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_Self_LateFees.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_Self_LateFees.UnboundType = DevExpress.Data.UnboundColumnType.[Decimal]
            Me.GridColumn_Self_LateFees.Visible = True
            Me.GridColumn_Self_LateFees.VisibleIndex = 5
            Me.GridColumn_Self_LateFees.Width = 46
            '
            'GridColumn_Self_OverLimit
            '
            Me.GridColumn_Self_OverLimit.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Self_OverLimit.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Self_OverLimit.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Self_OverLimit.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Self_OverLimit.Caption = "Overlimit"
            Me.GridColumn_Self_OverLimit.CustomizationCaption = "Overlimit Fees"
            Me.GridColumn_Self_OverLimit.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_Self_OverLimit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Self_OverLimit.FieldName = "Self_OverLimitFees"
            Me.GridColumn_Self_OverLimit.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_Self_OverLimit.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Self_OverLimit.Name = "GridColumn_Self_OverLimit"
            Me.GridColumn_Self_OverLimit.OptionsColumn.AllowEdit = False
            Me.GridColumn_Self_OverLimit.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_Self_OverLimit.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_Self_OverLimit.UnboundType = DevExpress.Data.UnboundColumnType.[Decimal]
            Me.GridColumn_Self_OverLimit.Visible = True
            Me.GridColumn_Self_OverLimit.VisibleIndex = 6
            Me.GridColumn_Self_OverLimit.Width = 46
            '
            'GridColumn_FinanceCharge
            '
            Me.GridColumn_Self_MonthlyFinanceCharge.Caption = "Monthly Finance Charge"
            Me.GridColumn_Self_MonthlyFinanceCharge.CustomizationCaption = "Monthly Finance Charge"
            Me.GridColumn_Self_MonthlyFinanceCharge.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_Self_MonthlyFinanceCharge.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Self_MonthlyFinanceCharge.FieldName = "Self_MonthlyFinanceCharge"
            Me.GridColumn_Self_MonthlyFinanceCharge.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_Self_MonthlyFinanceCharge.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Self_MonthlyFinanceCharge.Name = "GridColumn_Self_MonthlyFinanceCharge"
            Me.GridColumn_Self_MonthlyFinanceCharge.OptionsColumn.AllowEdit = False
            Me.GridColumn_Self_MonthlyFinanceCharge.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_Self_MonthlyFinanceCharge.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_Self_MonthlyFinanceCharge.UnboundType = DevExpress.Data.UnboundColumnType.[Decimal]
            '
            'GridColumn_Self_InterestAndFees
            '
            Me.GridColumn_Self_InterestAndFees.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Self_InterestAndFees.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Self_InterestAndFees.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Self_InterestAndFees.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Self_InterestAndFees.Caption = "Finance"
            Me.GridColumn_Self_InterestAndFees.CustomizationCaption = "Total Financing Fees"
            Me.GridColumn_Self_InterestAndFees.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_Self_InterestAndFees.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Self_InterestAndFees.FieldName = "Self_InterestAndFees"
            Me.GridColumn_Self_InterestAndFees.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_Self_InterestAndFees.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Self_InterestAndFees.Name = "GridColumn_Self_InterestAndFees"
            Me.GridColumn_Self_InterestAndFees.OptionsColumn.AllowEdit = False
            Me.GridColumn_Self_InterestAndFees.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_Self_InterestAndFees.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_Self_InterestAndFees.UnboundType = DevExpress.Data.UnboundColumnType.[Decimal]
            Me.GridColumn_Self_InterestAndFees.Visible = True
            Me.GridColumn_Self_InterestAndFees.VisibleIndex = 7
            Me.GridColumn_Self_InterestAndFees.Width = 55
            '
            'GridColumn_Self_GoingToPrincipal
            '
            Me.GridColumn_Self_GoingToPrincipal.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Self_GoingToPrincipal.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Self_GoingToPrincipal.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Self_GoingToPrincipal.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Self_GoingToPrincipal.Caption = "Principal"
            Me.GridColumn_Self_GoingToPrincipal.CustomizationCaption = "Dollar amount going to principal"
            Me.GridColumn_Self_GoingToPrincipal.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_Self_GoingToPrincipal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Self_GoingToPrincipal.FieldName = "Self_GoingToPrincipal"
            Me.GridColumn_Self_GoingToPrincipal.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_Self_GoingToPrincipal.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Self_GoingToPrincipal.Name = "GridColumn_Self_GoingToPrincipal"
            Me.GridColumn_Self_GoingToPrincipal.OptionsColumn.AllowEdit = False
            Me.GridColumn_Self_GoingToPrincipal.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_Self_GoingToPrincipal.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_Self_GoingToPrincipal.UnboundType = DevExpress.Data.UnboundColumnType.[Decimal]
            Me.GridColumn_Self_GoingToPrincipal.Visible = True
            Me.GridColumn_Self_GoingToPrincipal.VisibleIndex = 8
            Me.GridColumn_Self_GoingToPrincipal.Width = 52
            '
            'Comparison_ClientControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.GridControl1)
            Me.Name = "Comparison_ClientControl"
            Me.Size = New System.Drawing.Size(411, 150)
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As Comparison_GridView
        Friend WithEvents GridColumn_Type As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Name As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Balance As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Self_InterestRate As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Self_MonthlyPayment As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Self_LateFees As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Self_OverLimit As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Self_InterestAndFees As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Self_GoingToPrincipal As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_AccountNumber As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Self_MonthlyFinanceCharge As DevExpress.XtraGrid.Columns.GridColumn
    End Class
End Namespace