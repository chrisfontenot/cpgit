﻿Namespace forms.Comparison
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Comparison_CompareControl
        Inherits DevExpress.XtraEditors.XtraUserControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_Self_TotalPayoutPayment_1 = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_DMP_TotalEnteredPayment_1 = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_MonthlySavingsPayments = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_Self_TotalFinanceCharge = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_DMP_TotalFinanceCharge = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_MonthlySavingsFinanceCharge = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_Self_TotalInterestRate_1 = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_DMP_TotalInterestRate_1 = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_MonthlySavingsEstimatedInterestRate = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_Self_TotalInterestAndFees = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_DMP_TotalInterestFees = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_MonthlySavingsTotalInterestAndFees = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_Self_TotalGoingToPrincipal = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_DMP_TotalGoingToPrincipal = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_Self_TotalPercentGoingToPrincipal = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_DMP_TotalPercentGoingToprincipal = New DevExpress.XtraEditors.LabelControl()
            Me.LineControl1 = New DevExpress.XtraEditors.PanelControl()
            Me.LineControl2 = New DevExpress.XtraEditors.PanelControl()
            Me.LineControl3 = New DevExpress.XtraEditors.PanelControl()
            Me.LineControl4 = New DevExpress.XtraEditors.PanelControl()
            Me.lbl_DMP_TotalAmountPaid = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_Self_TotalPaid = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_DMP_TotalInterestPaid = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_Self_TotalInterestPaid = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_Self_LengthYears = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_Self_LengthMonths = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_DMP_TotalInterestRate_2 = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_Self_TotalInterestRate_2 = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_DMP_TotalEnteredPayment_2 = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_Self_TotalPayoutPayment_2 = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_DMP_TotalBalance = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_Self_TotalBalance = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl45 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl46 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl47 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl48 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl49 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl50 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl51 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl52 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl32 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl35 = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_DMP_LengthMonths = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_DMP_LengthYears = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl44 = New DevExpress.XtraEditors.LabelControl()
            Me.clc_ExtraDMPAmount = New DevExpress.XtraEditors.CalcEdit()
            Me.LineControl5 = New DevExpress.XtraEditors.PanelControl()
            Me.LabelControl53 = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_DMP_TotalPayment = New DevExpress.XtraEditors.LabelControl()
            Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
            Me.LabelControl60 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl59 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl58 = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_Savings = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_Self_LateOverlimitFeeSavings = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_InterestSavings = New DevExpress.XtraEditors.LabelControl()
            CType(Me.LineControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LineControl2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LineControl3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LineControl4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.clc_ExtraDMPAmount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LineControl5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.PanelControl2.SuspendLayout()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                             Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl1.Appearance.BackColor = System.Drawing.Color.LimeGreen
            Me.LabelControl1.Appearance.BackColor2 = System.Drawing.Color.LimeGreen
            Me.LabelControl1.Appearance.BorderColor = System.Drawing.Color.LimeGreen
            Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
            Me.LabelControl1.Appearance.ForeColor = System.Drawing.Color.White
            Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl1.Location = New System.Drawing.Point(0, 0)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(601, 19)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "POTENTIAL MONTHLY SAVINGS"
            '
            'LabelControl2
            '
            Me.LabelControl2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                             Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl2.Appearance.BackColor = System.Drawing.Color.LimeGreen
            Me.LabelControl2.Appearance.BackColor2 = System.Drawing.Color.LimeGreen
            Me.LabelControl2.Appearance.BorderColor = System.Drawing.Color.LimeGreen
            Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
            Me.LabelControl2.Appearance.ForeColor = System.Drawing.Color.White
            Me.LabelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl2.Location = New System.Drawing.Point(0, 135)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(601, 19)
            Me.LabelControl2.TabIndex = 1
            Me.LabelControl2.Text = "POTENTIAL TOTAL SAVINGS"
            '
            'LabelControl3
            '
            Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl3.Location = New System.Drawing.Point(90, 25)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(51, 26)
            Me.LabelControl3.TabIndex = 2
            Me.LabelControl3.Text = "Monthly" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Payment" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
            '
            'LabelControl4
            '
            Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl4.Location = New System.Drawing.Point(180, 25)
            Me.LabelControl4.Name = "LabelControl4"
            Me.LabelControl4.Size = New System.Drawing.Size(46, 26)
            Me.LabelControl4.TabIndex = 3
            Me.LabelControl4.Text = "Finance" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Charges" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
            '
            'LabelControl5
            '
            Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl5.Location = New System.Drawing.Point(254, 25)
            Me.LabelControl5.Name = "LabelControl5"
            Me.LabelControl5.Size = New System.Drawing.Size(57, 26)
            Me.LabelControl5.TabIndex = 4
            Me.LabelControl5.Text = "Estimated" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "A.P.R."
            '
            'LabelControl6
            '
            Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl6.Location = New System.Drawing.Point(349, 25)
            Me.LabelControl6.Name = "LabelControl6"
            Me.LabelControl6.Size = New System.Drawing.Size(79, 26)
            Me.LabelControl6.TabIndex = 5
            Me.LabelControl6.Text = "Total Interest" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "And Fees" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
            '
            'LabelControl7
            '
            Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl7.Location = New System.Drawing.Point(448, 25)
            Me.LabelControl7.Name = "LabelControl7"
            Me.LabelControl7.Size = New System.Drawing.Size(65, 26)
            Me.LabelControl7.TabIndex = 6
            Me.LabelControl7.Text = "$ Amount" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "To Principal" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
            '
            'LabelControl8
            '
            Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl8.Location = New System.Drawing.Point(533, 25)
            Me.LabelControl8.Name = "LabelControl8"
            Me.LabelControl8.Size = New System.Drawing.Size(65, 26)
            Me.LabelControl8.TabIndex = 7
            Me.LabelControl8.Text = "Percent" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "To Principal" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
            '
            'LabelControl9
            '
            Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl9.Location = New System.Drawing.Point(3, 55)
            Me.LabelControl9.Name = "LabelControl9"
            Me.LabelControl9.Size = New System.Drawing.Size(32, 13)
            Me.LabelControl9.TabIndex = 8
            Me.LabelControl9.Text = "Client"
            '
            'LabelControl10
            '
            Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl10.Location = New System.Drawing.Point(3, 74)
            Me.LabelControl10.Name = "LabelControl10"
            Me.LabelControl10.Size = New System.Drawing.Size(24, 13)
            Me.LabelControl10.TabIndex = 9
            Me.LabelControl10.Text = "Plan"
            '
            'LabelControl11
            '
            Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl11.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Int32), CType(CType(0, Byte), Int32), CType(CType(192, Byte), Int32))
            Me.LabelControl11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl11.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl11.Location = New System.Drawing.Point(3, 101)
            Me.LabelControl11.Name = "LabelControl11"
            Me.LabelControl11.Size = New System.Drawing.Size(44, 13)
            Me.LabelControl11.TabIndex = 10
            Me.LabelControl11.Text = "Savings"
            '
            'lbl_Self_TotalPayoutPayment_1
            '
            Me.lbl_Self_TotalPayoutPayment_1.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_Self_TotalPayoutPayment_1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_Self_TotalPayoutPayment_1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_Self_TotalPayoutPayment_1.Location = New System.Drawing.Point(62, 55)
            Me.lbl_Self_TotalPayoutPayment_1.Name = "lbl_Self_TotalPayoutPayment_1"
            Me.lbl_Self_TotalPayoutPayment_1.Size = New System.Drawing.Size(79, 13)
            Me.lbl_Self_TotalPayoutPayment_1.TabIndex = 11
            Me.lbl_Self_TotalPayoutPayment_1.Text = "$0.00"
            '
            'lbl_DMP_TotalEnteredPayment_1
            '
            Me.lbl_DMP_TotalEnteredPayment_1.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_DMP_TotalEnteredPayment_1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_DMP_TotalEnteredPayment_1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_DMP_TotalEnteredPayment_1.Location = New System.Drawing.Point(62, 74)
            Me.lbl_DMP_TotalEnteredPayment_1.Name = "lbl_DMP_TotalEnteredPayment_1"
            Me.lbl_DMP_TotalEnteredPayment_1.Size = New System.Drawing.Size(79, 13)
            Me.lbl_DMP_TotalEnteredPayment_1.TabIndex = 12
            Me.lbl_DMP_TotalEnteredPayment_1.Text = "$0.00"
            '
            'lbl_MonthlySavingsPayments
            '
            Me.lbl_MonthlySavingsPayments.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_MonthlySavingsPayments.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_MonthlySavingsPayments.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_MonthlySavingsPayments.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
            Me.lbl_MonthlySavingsPayments.LineColor = System.Drawing.SystemColors.WindowText
            Me.lbl_MonthlySavingsPayments.LineLocation = DevExpress.XtraEditors.LineLocation.Top
            Me.lbl_MonthlySavingsPayments.LineOrientation = DevExpress.XtraEditors.LabelLineOrientation.Horizontal
            Me.lbl_MonthlySavingsPayments.LineVisible = True
            Me.lbl_MonthlySavingsPayments.Location = New System.Drawing.Point(62, 101)
            Me.lbl_MonthlySavingsPayments.Name = "lbl_MonthlySavingsPayments"
            Me.lbl_MonthlySavingsPayments.Size = New System.Drawing.Size(79, 16)
            Me.lbl_MonthlySavingsPayments.TabIndex = 13
            Me.lbl_MonthlySavingsPayments.Text = "$0.00"
            '
            'lbl_Self_TotalFinanceCharge
            '
            Me.lbl_Self_TotalFinanceCharge.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_Self_TotalFinanceCharge.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_Self_TotalFinanceCharge.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_Self_TotalFinanceCharge.Location = New System.Drawing.Point(147, 55)
            Me.lbl_Self_TotalFinanceCharge.Name = "lbl_Self_TotalFinanceCharge"
            Me.lbl_Self_TotalFinanceCharge.Size = New System.Drawing.Size(79, 13)
            Me.lbl_Self_TotalFinanceCharge.TabIndex = 14
            Me.lbl_Self_TotalFinanceCharge.Text = "$0.00"
            '
            'lbl_DMP_TotalFinanceCharge
            '
            Me.lbl_DMP_TotalFinanceCharge.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_DMP_TotalFinanceCharge.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_DMP_TotalFinanceCharge.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_DMP_TotalFinanceCharge.Location = New System.Drawing.Point(147, 74)
            Me.lbl_DMP_TotalFinanceCharge.Name = "lbl_DMP_TotalFinanceCharge"
            Me.lbl_DMP_TotalFinanceCharge.Size = New System.Drawing.Size(79, 13)
            Me.lbl_DMP_TotalFinanceCharge.TabIndex = 15
            Me.lbl_DMP_TotalFinanceCharge.Text = "$0.00"
            '
            'lbl_MonthlySavingsFinanceCharge
            '
            Me.lbl_MonthlySavingsFinanceCharge.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_MonthlySavingsFinanceCharge.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_MonthlySavingsFinanceCharge.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_MonthlySavingsFinanceCharge.Location = New System.Drawing.Point(147, 101)
            Me.lbl_MonthlySavingsFinanceCharge.Name = "lbl_MonthlySavingsFinanceCharge"
            Me.lbl_MonthlySavingsFinanceCharge.Size = New System.Drawing.Size(79, 13)
            Me.lbl_MonthlySavingsFinanceCharge.TabIndex = 16
            Me.lbl_MonthlySavingsFinanceCharge.Text = "$0.00"
            '
            'lbl_Self_TotalInterestRate_1
            '
            Me.lbl_Self_TotalInterestRate_1.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_Self_TotalInterestRate_1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_Self_TotalInterestRate_1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_Self_TotalInterestRate_1.Location = New System.Drawing.Point(232, 55)
            Me.lbl_Self_TotalInterestRate_1.Name = "lbl_Self_TotalInterestRate_1"
            Me.lbl_Self_TotalInterestRate_1.Size = New System.Drawing.Size(79, 13)
            Me.lbl_Self_TotalInterestRate_1.TabIndex = 17
            Me.lbl_Self_TotalInterestRate_1.Text = "0.00 %"
            '
            'lbl_DMP_TotalInterestRate_1
            '
            Me.lbl_DMP_TotalInterestRate_1.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_DMP_TotalInterestRate_1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_DMP_TotalInterestRate_1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_DMP_TotalInterestRate_1.Location = New System.Drawing.Point(232, 74)
            Me.lbl_DMP_TotalInterestRate_1.Name = "lbl_DMP_TotalInterestRate_1"
            Me.lbl_DMP_TotalInterestRate_1.Size = New System.Drawing.Size(79, 13)
            Me.lbl_DMP_TotalInterestRate_1.TabIndex = 18
            Me.lbl_DMP_TotalInterestRate_1.Text = "0.00 %"
            '
            'lbl_MonthlySavingsEstimatedInterestRate
            '
            Me.lbl_MonthlySavingsEstimatedInterestRate.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_MonthlySavingsEstimatedInterestRate.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_MonthlySavingsEstimatedInterestRate.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_MonthlySavingsEstimatedInterestRate.Location = New System.Drawing.Point(232, 101)
            Me.lbl_MonthlySavingsEstimatedInterestRate.Name = "lbl_MonthlySavingsEstimatedInterestRate"
            Me.lbl_MonthlySavingsEstimatedInterestRate.Size = New System.Drawing.Size(79, 13)
            Me.lbl_MonthlySavingsEstimatedInterestRate.TabIndex = 19
            Me.lbl_MonthlySavingsEstimatedInterestRate.Text = "0.00 %"
            '
            'lbl_Self_TotalInterestAndFees
            '
            Me.lbl_Self_TotalInterestAndFees.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_Self_TotalInterestAndFees.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_Self_TotalInterestAndFees.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_Self_TotalInterestAndFees.Location = New System.Drawing.Point(349, 55)
            Me.lbl_Self_TotalInterestAndFees.Name = "lbl_Self_TotalInterestAndFees"
            Me.lbl_Self_TotalInterestAndFees.Size = New System.Drawing.Size(79, 13)
            Me.lbl_Self_TotalInterestAndFees.TabIndex = 20
            Me.lbl_Self_TotalInterestAndFees.Text = "$0.00"
            '
            'lbl_DMP_TotalInterestFees
            '
            Me.lbl_DMP_TotalInterestFees.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_DMP_TotalInterestFees.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_DMP_TotalInterestFees.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_DMP_TotalInterestFees.Location = New System.Drawing.Point(349, 74)
            Me.lbl_DMP_TotalInterestFees.Name = "lbl_DMP_TotalInterestFees"
            Me.lbl_DMP_TotalInterestFees.Size = New System.Drawing.Size(79, 13)
            Me.lbl_DMP_TotalInterestFees.TabIndex = 21
            Me.lbl_DMP_TotalInterestFees.Text = "$0.00"
            '
            'lbl_MonthlySavingsTotalInterestAndFees
            '
            Me.lbl_MonthlySavingsTotalInterestAndFees.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_MonthlySavingsTotalInterestAndFees.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_MonthlySavingsTotalInterestAndFees.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_MonthlySavingsTotalInterestAndFees.Location = New System.Drawing.Point(349, 101)
            Me.lbl_MonthlySavingsTotalInterestAndFees.Name = "lbl_MonthlySavingsTotalInterestAndFees"
            Me.lbl_MonthlySavingsTotalInterestAndFees.Size = New System.Drawing.Size(79, 13)
            Me.lbl_MonthlySavingsTotalInterestAndFees.TabIndex = 22
            Me.lbl_MonthlySavingsTotalInterestAndFees.Text = "$0.00"
            '
            'lbl_Self_TotalGoingToPrincipal
            '
            Me.lbl_Self_TotalGoingToPrincipal.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_Self_TotalGoingToPrincipal.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_Self_TotalGoingToPrincipal.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_Self_TotalGoingToPrincipal.Location = New System.Drawing.Point(434, 55)
            Me.lbl_Self_TotalGoingToPrincipal.Name = "lbl_Self_TotalGoingToPrincipal"
            Me.lbl_Self_TotalGoingToPrincipal.Size = New System.Drawing.Size(79, 13)
            Me.lbl_Self_TotalGoingToPrincipal.TabIndex = 23
            Me.lbl_Self_TotalGoingToPrincipal.Text = "$0.00"
            '
            'lbl_DMP_TotalGoingToPrincipal
            '
            Me.lbl_DMP_TotalGoingToPrincipal.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_DMP_TotalGoingToPrincipal.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_DMP_TotalGoingToPrincipal.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_DMP_TotalGoingToPrincipal.Location = New System.Drawing.Point(434, 74)
            Me.lbl_DMP_TotalGoingToPrincipal.Name = "lbl_DMP_TotalGoingToPrincipal"
            Me.lbl_DMP_TotalGoingToPrincipal.Size = New System.Drawing.Size(79, 13)
            Me.lbl_DMP_TotalGoingToPrincipal.TabIndex = 24
            Me.lbl_DMP_TotalGoingToPrincipal.Text = "$0.00"
            '
            'lbl_Self_TotalPercentGoingToPrincipal
            '
            Me.lbl_Self_TotalPercentGoingToPrincipal.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_Self_TotalPercentGoingToPrincipal.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_Self_TotalPercentGoingToPrincipal.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_Self_TotalPercentGoingToPrincipal.Location = New System.Drawing.Point(519, 57)
            Me.lbl_Self_TotalPercentGoingToPrincipal.Name = "lbl_Self_TotalPercentGoingToPrincipal"
            Me.lbl_Self_TotalPercentGoingToPrincipal.Size = New System.Drawing.Size(79, 13)
            Me.lbl_Self_TotalPercentGoingToPrincipal.TabIndex = 26
            Me.lbl_Self_TotalPercentGoingToPrincipal.Text = "0.00 %"
            '
            'lbl_DMP_TotalPercentGoingToprincipal
            '
            Me.lbl_DMP_TotalPercentGoingToprincipal.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_DMP_TotalPercentGoingToprincipal.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_DMP_TotalPercentGoingToprincipal.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_DMP_TotalPercentGoingToprincipal.Location = New System.Drawing.Point(519, 76)
            Me.lbl_DMP_TotalPercentGoingToprincipal.Name = "lbl_DMP_TotalPercentGoingToprincipal"
            Me.lbl_DMP_TotalPercentGoingToprincipal.Size = New System.Drawing.Size(79, 13)
            Me.lbl_DMP_TotalPercentGoingToprincipal.TabIndex = 27
            Me.lbl_DMP_TotalPercentGoingToprincipal.Text = "0.00 %"
            '
            'LineControl1
            '
            Me.LineControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
            Me.LineControl1.Location = New System.Drawing.Point(62, 92)
            Me.LineControl1.Name = "LineControl1"
            Me.LineControl1.Size = New System.Drawing.Size(79, 5)
            Me.LineControl1.TabIndex = 30
            '
            'LineControl2
            '
            Me.LineControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
            Me.LineControl2.Location = New System.Drawing.Point(147, 92)
            Me.LineControl2.Name = "LineControl2"
            Me.LineControl2.Size = New System.Drawing.Size(79, 5)
            Me.LineControl2.TabIndex = 31
            '
            'LineControl3
            '
            Me.LineControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
            Me.LineControl3.Location = New System.Drawing.Point(232, 92)
            Me.LineControl3.Name = "LineControl3"
            Me.LineControl3.Size = New System.Drawing.Size(79, 5)
            Me.LineControl3.TabIndex = 32
            '
            'LineControl4
            '
            Me.LineControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
            Me.LineControl4.Location = New System.Drawing.Point(349, 92)
            Me.LineControl4.Name = "LineControl4"
            Me.LineControl4.Size = New System.Drawing.Size(79, 5)
            Me.LineControl4.TabIndex = 33
            '
            'lbl_DMP_TotalAmountPaid
            '
            Me.lbl_DMP_TotalAmountPaid.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_DMP_TotalAmountPaid.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_DMP_TotalAmountPaid.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_DMP_TotalAmountPaid.Location = New System.Drawing.Point(519, 211)
            Me.lbl_DMP_TotalAmountPaid.Name = "lbl_DMP_TotalAmountPaid"
            Me.lbl_DMP_TotalAmountPaid.Size = New System.Drawing.Size(79, 13)
            Me.lbl_DMP_TotalAmountPaid.TabIndex = 58
            Me.lbl_DMP_TotalAmountPaid.Text = "$0.00"
            '
            'lbl_Self_TotalPaid
            '
            Me.lbl_Self_TotalPaid.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_Self_TotalPaid.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_Self_TotalPaid.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_Self_TotalPaid.Location = New System.Drawing.Point(519, 192)
            Me.lbl_Self_TotalPaid.Name = "lbl_Self_TotalPaid"
            Me.lbl_Self_TotalPaid.Size = New System.Drawing.Size(79, 13)
            Me.lbl_Self_TotalPaid.TabIndex = 57
            Me.lbl_Self_TotalPaid.Text = "$0.00"
            '
            'lbl_DMP_TotalInterestPaid
            '
            Me.lbl_DMP_TotalInterestPaid.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_DMP_TotalInterestPaid.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_DMP_TotalInterestPaid.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_DMP_TotalInterestPaid.Location = New System.Drawing.Point(434, 209)
            Me.lbl_DMP_TotalInterestPaid.Name = "lbl_DMP_TotalInterestPaid"
            Me.lbl_DMP_TotalInterestPaid.Size = New System.Drawing.Size(79, 13)
            Me.lbl_DMP_TotalInterestPaid.TabIndex = 56
            Me.lbl_DMP_TotalInterestPaid.Text = "$0.00"
            '
            'lbl_Self_TotalInterestPaid
            '
            Me.lbl_Self_TotalInterestPaid.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_Self_TotalInterestPaid.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_Self_TotalInterestPaid.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_Self_TotalInterestPaid.Location = New System.Drawing.Point(434, 190)
            Me.lbl_Self_TotalInterestPaid.Name = "lbl_Self_TotalInterestPaid"
            Me.lbl_Self_TotalInterestPaid.Size = New System.Drawing.Size(79, 13)
            Me.lbl_Self_TotalInterestPaid.TabIndex = 55
            Me.lbl_Self_TotalInterestPaid.Text = "$0.00"
            '
            'lbl_Self_LengthYears
            '
            Me.lbl_Self_LengthYears.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_Self_LengthYears.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_Self_LengthYears.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_Self_LengthYears.Location = New System.Drawing.Point(393, 190)
            Me.lbl_Self_LengthYears.Name = "lbl_Self_LengthYears"
            Me.lbl_Self_LengthYears.Size = New System.Drawing.Size(35, 13)
            Me.lbl_Self_LengthYears.TabIndex = 53
            Me.lbl_Self_LengthYears.Text = "0.00"
            '
            'lbl_Self_LengthMonths
            '
            Me.lbl_Self_LengthMonths.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_Self_LengthMonths.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_Self_LengthMonths.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_Self_LengthMonths.Location = New System.Drawing.Point(328, 190)
            Me.lbl_Self_LengthMonths.Name = "lbl_Self_LengthMonths"
            Me.lbl_Self_LengthMonths.Size = New System.Drawing.Size(46, 13)
            Me.lbl_Self_LengthMonths.TabIndex = 52
            Me.lbl_Self_LengthMonths.Text = "0"
            '
            'lbl_DMP_TotalInterestRate_2
            '
            Me.lbl_DMP_TotalInterestRate_2.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_DMP_TotalInterestRate_2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_DMP_TotalInterestRate_2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_DMP_TotalInterestRate_2.Location = New System.Drawing.Point(232, 209)
            Me.lbl_DMP_TotalInterestRate_2.Name = "lbl_DMP_TotalInterestRate_2"
            Me.lbl_DMP_TotalInterestRate_2.Size = New System.Drawing.Size(79, 13)
            Me.lbl_DMP_TotalInterestRate_2.TabIndex = 50
            Me.lbl_DMP_TotalInterestRate_2.Text = "0.00 %"
            '
            'lbl_Self_TotalInterestRate_2
            '
            Me.lbl_Self_TotalInterestRate_2.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_Self_TotalInterestRate_2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_Self_TotalInterestRate_2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_Self_TotalInterestRate_2.Location = New System.Drawing.Point(232, 190)
            Me.lbl_Self_TotalInterestRate_2.Name = "lbl_Self_TotalInterestRate_2"
            Me.lbl_Self_TotalInterestRate_2.Size = New System.Drawing.Size(79, 13)
            Me.lbl_Self_TotalInterestRate_2.TabIndex = 49
            Me.lbl_Self_TotalInterestRate_2.Text = "0.00 %"
            '
            'lbl_DMP_TotalEnteredPayment_2
            '
            Me.lbl_DMP_TotalEnteredPayment_2.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_DMP_TotalEnteredPayment_2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_DMP_TotalEnteredPayment_2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_DMP_TotalEnteredPayment_2.Location = New System.Drawing.Point(147, 209)
            Me.lbl_DMP_TotalEnteredPayment_2.Name = "lbl_DMP_TotalEnteredPayment_2"
            Me.lbl_DMP_TotalEnteredPayment_2.Size = New System.Drawing.Size(79, 13)
            Me.lbl_DMP_TotalEnteredPayment_2.TabIndex = 47
            Me.lbl_DMP_TotalEnteredPayment_2.Text = "$0.00"
            '
            'lbl_Self_TotalPayoutPayment_2
            '
            Me.lbl_Self_TotalPayoutPayment_2.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_Self_TotalPayoutPayment_2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_Self_TotalPayoutPayment_2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_Self_TotalPayoutPayment_2.Location = New System.Drawing.Point(147, 190)
            Me.lbl_Self_TotalPayoutPayment_2.Name = "lbl_Self_TotalPayoutPayment_2"
            Me.lbl_Self_TotalPayoutPayment_2.Size = New System.Drawing.Size(79, 13)
            Me.lbl_Self_TotalPayoutPayment_2.TabIndex = 46
            Me.lbl_Self_TotalPayoutPayment_2.Text = "$0.00"
            '
            'lbl_DMP_TotalBalance
            '
            Me.lbl_DMP_TotalBalance.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_DMP_TotalBalance.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_DMP_TotalBalance.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_DMP_TotalBalance.Location = New System.Drawing.Point(62, 209)
            Me.lbl_DMP_TotalBalance.Name = "lbl_DMP_TotalBalance"
            Me.lbl_DMP_TotalBalance.Size = New System.Drawing.Size(79, 13)
            Me.lbl_DMP_TotalBalance.TabIndex = 44
            Me.lbl_DMP_TotalBalance.Text = "$0.00"
            '
            'lbl_Self_TotalBalance
            '
            Me.lbl_Self_TotalBalance.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_Self_TotalBalance.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_Self_TotalBalance.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_Self_TotalBalance.Location = New System.Drawing.Point(62, 190)
            Me.lbl_Self_TotalBalance.Name = "lbl_Self_TotalBalance"
            Me.lbl_Self_TotalBalance.Size = New System.Drawing.Size(79, 13)
            Me.lbl_Self_TotalBalance.TabIndex = 43
            Me.lbl_Self_TotalBalance.Text = "$0.00"
            '
            'LabelControl45
            '
            Me.LabelControl45.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl45.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl45.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl45.Location = New System.Drawing.Point(3, 209)
            Me.LabelControl45.Name = "LabelControl45"
            Me.LabelControl45.Size = New System.Drawing.Size(24, 13)
            Me.LabelControl45.TabIndex = 41
            Me.LabelControl45.Text = "Plan"
            '
            'LabelControl46
            '
            Me.LabelControl46.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl46.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl46.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl46.Location = New System.Drawing.Point(3, 190)
            Me.LabelControl46.Name = "LabelControl46"
            Me.LabelControl46.Size = New System.Drawing.Size(32, 13)
            Me.LabelControl46.TabIndex = 40
            Me.LabelControl46.Text = "Client"
            '
            'LabelControl47
            '
            Me.LabelControl47.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl47.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl47.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl47.Location = New System.Drawing.Point(533, 160)
            Me.LabelControl47.Name = "LabelControl47"
            Me.LabelControl47.Size = New System.Drawing.Size(65, 26)
            Me.LabelControl47.TabIndex = 39
            Me.LabelControl47.Text = "Total Amt" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Paid At End" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
            '
            'LabelControl48
            '
            Me.LabelControl48.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl48.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl48.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl48.Location = New System.Drawing.Point(434, 160)
            Me.LabelControl48.Name = "LabelControl48"
            Me.LabelControl48.Size = New System.Drawing.Size(79, 26)
            Me.LabelControl48.TabIndex = 38
            Me.LabelControl48.Text = "Total" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Interest Paid" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
            '
            'LabelControl49
            '
            Me.LabelControl49.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl49.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.LabelControl49.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl49.Location = New System.Drawing.Point(327, 159)
            Me.LabelControl49.Name = "LabelControl49"
            Me.LabelControl49.Size = New System.Drawing.Size(101, 15)
            Me.LabelControl49.TabIndex = 37
            Me.LabelControl49.Text = "Length Of Plan" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
            '
            'LabelControl50
            '
            Me.LabelControl50.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl50.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl50.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl50.Location = New System.Drawing.Point(254, 160)
            Me.LabelControl50.Name = "LabelControl50"
            Me.LabelControl50.Size = New System.Drawing.Size(57, 26)
            Me.LabelControl50.TabIndex = 36
            Me.LabelControl50.Text = "Estimated" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "A.P.R."
            '
            'LabelControl51
            '
            Me.LabelControl51.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl51.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl51.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl51.Location = New System.Drawing.Point(166, 160)
            Me.LabelControl51.Name = "LabelControl51"
            Me.LabelControl51.Size = New System.Drawing.Size(60, 26)
            Me.LabelControl51.TabIndex = 35
            Me.LabelControl51.Text = "Monthly" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Payment" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
            '
            'LabelControl52
            '
            Me.LabelControl52.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl52.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl52.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl52.Location = New System.Drawing.Point(90, 160)
            Me.LabelControl52.Name = "LabelControl52"
            Me.LabelControl52.Size = New System.Drawing.Size(51, 26)
            Me.LabelControl52.TabIndex = 34
            Me.LabelControl52.Text = "Principal" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Debt" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
            '
            'LabelControl32
            '
            Me.LabelControl32.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl32.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl32.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl32.Location = New System.Drawing.Point(317, 171)
            Me.LabelControl32.Name = "LabelControl32"
            Me.LabelControl32.Size = New System.Drawing.Size(57, 15)
            Me.LabelControl32.TabIndex = 59
            Me.LabelControl32.Text = "Months" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
            '
            'LabelControl35
            '
            Me.LabelControl35.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl35.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl35.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl35.Location = New System.Drawing.Point(380, 171)
            Me.LabelControl35.Name = "LabelControl35"
            Me.LabelControl35.Size = New System.Drawing.Size(48, 15)
            Me.LabelControl35.TabIndex = 60
            Me.LabelControl35.Text = "Years" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
            '
            'lbl_DMP_LengthMonths
            '
            Me.lbl_DMP_LengthMonths.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_DMP_LengthMonths.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_DMP_LengthMonths.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_DMP_LengthMonths.Location = New System.Drawing.Point(328, 209)
            Me.lbl_DMP_LengthMonths.Name = "lbl_DMP_LengthMonths"
            Me.lbl_DMP_LengthMonths.Size = New System.Drawing.Size(46, 13)
            Me.lbl_DMP_LengthMonths.TabIndex = 61
            Me.lbl_DMP_LengthMonths.Text = "0"
            '
            'lbl_DMP_LengthYears
            '
            Me.lbl_DMP_LengthYears.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_DMP_LengthYears.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_DMP_LengthYears.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_DMP_LengthYears.Location = New System.Drawing.Point(382, 209)
            Me.lbl_DMP_LengthYears.Name = "lbl_DMP_LengthYears"
            Me.lbl_DMP_LengthYears.Size = New System.Drawing.Size(46, 13)
            Me.lbl_DMP_LengthYears.TabIndex = 62
            Me.lbl_DMP_LengthYears.Text = "0.00"
            '
            'LabelControl44
            '
            Me.LabelControl44.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl44.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl44.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl44.Location = New System.Drawing.Point(3, 232)
            Me.LabelControl44.Name = "LabelControl44"
            Me.LabelControl44.Size = New System.Drawing.Size(149, 13)
            Me.LabelControl44.TabIndex = 63
            Me.LabelControl44.Text = "+ Extra Payment Amount"
            '
            'clc_ExtraDMPAmount
            '
            Me.clc_ExtraDMPAmount.Location = New System.Drawing.Point(158, 228)
            Me.clc_ExtraDMPAmount.Name = "clc_ExtraDMPAmount"
            Me.clc_ExtraDMPAmount.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.clc_ExtraDMPAmount.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.clc_ExtraDMPAmount.Properties.DisplayFormat.FormatString = "c2"
            Me.clc_ExtraDMPAmount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.clc_ExtraDMPAmount.Properties.EditFormat.FormatString = "F2"
            Me.clc_ExtraDMPAmount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.clc_ExtraDMPAmount.Properties.NullText = "$0.00"
            Me.clc_ExtraDMPAmount.Properties.Precision = 2
            Me.clc_ExtraDMPAmount.Size = New System.Drawing.Size(89, 20)
            Me.clc_ExtraDMPAmount.TabIndex = 64
            '
            'LineControl5
            '
            Me.LineControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
            Me.LineControl5.Location = New System.Drawing.Point(147, 254)
            Me.LineControl5.Name = "LineControl5"
            Me.LineControl5.Size = New System.Drawing.Size(88, 5)
            Me.LineControl5.TabIndex = 65
            '
            'LabelControl53
            '
            Me.LabelControl53.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl53.Location = New System.Drawing.Point(3, 265)
            Me.LabelControl53.Name = "LabelControl53"
            Me.LabelControl53.Size = New System.Drawing.Size(136, 13)
            Me.LabelControl53.TabIndex = 66
            Me.LabelControl53.Text = "CREDITOR DEPOSIT AMT"
            '
            'lbl_DMP_TotalPayment
            '
            Me.lbl_DMP_TotalPayment.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_DMP_TotalPayment.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_DMP_TotalPayment.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_DMP_TotalPayment.Location = New System.Drawing.Point(147, 265)
            Me.lbl_DMP_TotalPayment.Name = "lbl_DMP_TotalPayment"
            Me.lbl_DMP_TotalPayment.Size = New System.Drawing.Size(79, 13)
            Me.lbl_DMP_TotalPayment.TabIndex = 67
            Me.lbl_DMP_TotalPayment.Text = "$0.00"
            '
            'PanelControl2
            '
            Me.PanelControl2.Appearance.BackColor = System.Drawing.Color.Gold
            Me.PanelControl2.Appearance.BackColor2 = System.Drawing.Color.Gold
            Me.PanelControl2.Appearance.BorderColor = System.Drawing.Color.Gold
            Me.PanelControl2.Appearance.ForeColor = System.Drawing.SystemColors.WindowText
            Me.PanelControl2.Appearance.Options.UseBackColor = True
            Me.PanelControl2.Appearance.Options.UseBorderColor = True
            Me.PanelControl2.Appearance.Options.UseForeColor = True
            Me.PanelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003
            Me.PanelControl2.Controls.Add(Me.LabelControl60)
            Me.PanelControl2.Controls.Add(Me.LabelControl59)
            Me.PanelControl2.Controls.Add(Me.LabelControl58)
            Me.PanelControl2.Controls.Add(Me.lbl_Savings)
            Me.PanelControl2.Controls.Add(Me.lbl_Self_LateOverlimitFeeSavings)
            Me.PanelControl2.Controls.Add(Me.lbl_InterestSavings)
            Me.PanelControl2.Cursor = System.Windows.Forms.Cursors.Default
            Me.PanelControl2.Location = New System.Drawing.Point(342, 242)
            Me.PanelControl2.Name = "PanelControl2"
            Me.PanelControl2.Size = New System.Drawing.Size(259, 65)
            Me.PanelControl2.TabIndex = 68
            '
            'LabelControl60
            '
            Me.LabelControl60.Appearance.BackColor = System.Drawing.Color.Transparent
            Me.LabelControl60.Appearance.BackColor2 = System.Drawing.Color.Transparent
            Me.LabelControl60.Appearance.BorderColor = System.Drawing.Color.Transparent
            Me.LabelControl60.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl60.Appearance.ForeColor = System.Drawing.Color.Red
            Me.LabelControl60.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl60.Location = New System.Drawing.Point(5, 42)
            Me.LabelControl60.Name = "LabelControl60"
            Me.LabelControl60.Size = New System.Drawing.Size(79, 13)
            Me.LabelControl60.TabIndex = 63
            Me.LabelControl60.Text = "Total Savings:"
            '
            'LabelControl59
            '
            Me.LabelControl59.Appearance.BackColor = System.Drawing.Color.Transparent
            Me.LabelControl59.Appearance.BackColor2 = System.Drawing.Color.Transparent
            Me.LabelControl59.Appearance.BorderColor = System.Drawing.Color.Transparent
            Me.LabelControl59.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl59.Appearance.ForeColor = System.Drawing.Color.Red
            Me.LabelControl59.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl59.Location = New System.Drawing.Point(5, 23)
            Me.LabelControl59.Name = "LabelControl59"
            Me.LabelControl59.Size = New System.Drawing.Size(164, 13)
            Me.LabelControl59.TabIndex = 62
            Me.LabelControl59.Text = "Late Fee/Over-Limit Savings:"
            '
            'LabelControl58
            '
            Me.LabelControl58.Appearance.BackColor = System.Drawing.Color.Transparent
            Me.LabelControl58.Appearance.BackColor2 = System.Drawing.Color.Transparent
            Me.LabelControl58.Appearance.BorderColor = System.Drawing.Color.Transparent
            Me.LabelControl58.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl58.Appearance.ForeColor = System.Drawing.Color.Red
            Me.LabelControl58.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl58.Location = New System.Drawing.Point(5, 4)
            Me.LabelControl58.Name = "LabelControl58"
            Me.LabelControl58.Size = New System.Drawing.Size(97, 13)
            Me.LabelControl58.TabIndex = 61
            Me.LabelControl58.Text = "Interest Savings:"
            '
            'lbl_Savings
            '
            Me.lbl_Savings.Appearance.BackColor = System.Drawing.Color.Transparent
            Me.lbl_Savings.Appearance.BackColor2 = System.Drawing.Color.Transparent
            Me.lbl_Savings.Appearance.BorderColor = System.Drawing.Color.Transparent
            Me.lbl_Savings.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_Savings.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_Savings.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_Savings.Location = New System.Drawing.Point(108, 42)
            Me.lbl_Savings.Name = "lbl_Savings"
            Me.lbl_Savings.Size = New System.Drawing.Size(146, 13)
            Me.lbl_Savings.TabIndex = 60
            Me.lbl_Savings.Text = "$0.00"
            '
            'lbl_Self_LateOverlimitFeeSavings
            '
            Me.lbl_Self_LateOverlimitFeeSavings.Appearance.BackColor = System.Drawing.Color.Transparent
            Me.lbl_Self_LateOverlimitFeeSavings.Appearance.BackColor2 = System.Drawing.Color.Transparent
            Me.lbl_Self_LateOverlimitFeeSavings.Appearance.BorderColor = System.Drawing.Color.Transparent
            Me.lbl_Self_LateOverlimitFeeSavings.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_Self_LateOverlimitFeeSavings.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_Self_LateOverlimitFeeSavings.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_Self_LateOverlimitFeeSavings.Location = New System.Drawing.Point(175, 23)
            Me.lbl_Self_LateOverlimitFeeSavings.Name = "lbl_Self_LateOverlimitFeeSavings"
            Me.lbl_Self_LateOverlimitFeeSavings.Size = New System.Drawing.Size(79, 13)
            Me.lbl_Self_LateOverlimitFeeSavings.TabIndex = 59
            Me.lbl_Self_LateOverlimitFeeSavings.Text = "$0.00"
            '
            'lbl_InterestSavings
            '
            Me.lbl_InterestSavings.Appearance.BackColor = System.Drawing.Color.Transparent
            Me.lbl_InterestSavings.Appearance.BackColor2 = System.Drawing.Color.Transparent
            Me.lbl_InterestSavings.Appearance.BorderColor = System.Drawing.Color.Transparent
            Me.lbl_InterestSavings.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.lbl_InterestSavings.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_InterestSavings.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_InterestSavings.Location = New System.Drawing.Point(108, 5)
            Me.lbl_InterestSavings.Name = "lbl_InterestSavings"
            Me.lbl_InterestSavings.Size = New System.Drawing.Size(146, 13)
            Me.lbl_InterestSavings.TabIndex = 58
            Me.lbl_InterestSavings.Text = "$0.00"
            '
            'Comparison_CompareControl
            '
            Me.Appearance.BackColor = System.Drawing.SystemColors.Window
            Me.Appearance.Options.UseBackColor = True
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.PanelControl2)
            Me.Controls.Add(Me.lbl_DMP_TotalPayment)
            Me.Controls.Add(Me.LabelControl53)
            Me.Controls.Add(Me.LineControl5)
            Me.Controls.Add(Me.clc_ExtraDMPAmount)
            Me.Controls.Add(Me.LabelControl44)
            Me.Controls.Add(Me.lbl_DMP_LengthYears)
            Me.Controls.Add(Me.lbl_DMP_LengthMonths)
            Me.Controls.Add(Me.lbl_DMP_TotalAmountPaid)
            Me.Controls.Add(Me.lbl_Self_TotalPaid)
            Me.Controls.Add(Me.lbl_DMP_TotalInterestPaid)
            Me.Controls.Add(Me.lbl_Self_TotalInterestPaid)
            Me.Controls.Add(Me.lbl_Self_LengthYears)
            Me.Controls.Add(Me.lbl_Self_LengthMonths)
            Me.Controls.Add(Me.lbl_DMP_TotalInterestRate_2)
            Me.Controls.Add(Me.lbl_Self_TotalInterestRate_2)
            Me.Controls.Add(Me.lbl_DMP_TotalEnteredPayment_2)
            Me.Controls.Add(Me.lbl_Self_TotalPayoutPayment_2)
            Me.Controls.Add(Me.lbl_DMP_TotalBalance)
            Me.Controls.Add(Me.lbl_Self_TotalBalance)
            Me.Controls.Add(Me.LabelControl45)
            Me.Controls.Add(Me.LabelControl46)
            Me.Controls.Add(Me.LabelControl47)
            Me.Controls.Add(Me.LabelControl48)
            Me.Controls.Add(Me.LabelControl49)
            Me.Controls.Add(Me.LabelControl50)
            Me.Controls.Add(Me.LabelControl51)
            Me.Controls.Add(Me.LabelControl52)
            Me.Controls.Add(Me.LineControl4)
            Me.Controls.Add(Me.LineControl3)
            Me.Controls.Add(Me.LineControl2)
            Me.Controls.Add(Me.LineControl1)
            Me.Controls.Add(Me.lbl_DMP_TotalPercentGoingToprincipal)
            Me.Controls.Add(Me.lbl_Self_TotalPercentGoingToPrincipal)
            Me.Controls.Add(Me.lbl_DMP_TotalGoingToPrincipal)
            Me.Controls.Add(Me.lbl_Self_TotalGoingToPrincipal)
            Me.Controls.Add(Me.lbl_MonthlySavingsTotalInterestAndFees)
            Me.Controls.Add(Me.lbl_DMP_TotalInterestFees)
            Me.Controls.Add(Me.lbl_Self_TotalInterestAndFees)
            Me.Controls.Add(Me.lbl_MonthlySavingsEstimatedInterestRate)
            Me.Controls.Add(Me.lbl_DMP_TotalInterestRate_1)
            Me.Controls.Add(Me.lbl_Self_TotalInterestRate_1)
            Me.Controls.Add(Me.lbl_MonthlySavingsFinanceCharge)
            Me.Controls.Add(Me.lbl_DMP_TotalFinanceCharge)
            Me.Controls.Add(Me.lbl_Self_TotalFinanceCharge)
            Me.Controls.Add(Me.lbl_MonthlySavingsPayments)
            Me.Controls.Add(Me.lbl_DMP_TotalEnteredPayment_1)
            Me.Controls.Add(Me.lbl_Self_TotalPayoutPayment_1)
            Me.Controls.Add(Me.LabelControl11)
            Me.Controls.Add(Me.LabelControl10)
            Me.Controls.Add(Me.LabelControl9)
            Me.Controls.Add(Me.LabelControl8)
            Me.Controls.Add(Me.LabelControl7)
            Me.Controls.Add(Me.LabelControl6)
            Me.Controls.Add(Me.LabelControl5)
            Me.Controls.Add(Me.LabelControl4)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.LabelControl32)
            Me.Controls.Add(Me.LabelControl35)
            Me.Name = "Comparison_CompareControl"
            Me.Size = New System.Drawing.Size(601, 310)
            CType(Me.LineControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LineControl2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LineControl3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LineControl4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.clc_ExtraDMPAmount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LineControl5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
            Me.PanelControl2.ResumeLayout(False)
            Me.PanelControl2.PerformLayout()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_Self_TotalPayoutPayment_1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_DMP_TotalEnteredPayment_1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_MonthlySavingsPayments As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_Self_TotalFinanceCharge As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_DMP_TotalFinanceCharge As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_MonthlySavingsFinanceCharge As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_Self_TotalInterestRate_1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_DMP_TotalInterestRate_1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_MonthlySavingsEstimatedInterestRate As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_Self_TotalInterestAndFees As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_DMP_TotalInterestFees As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_MonthlySavingsTotalInterestAndFees As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_Self_TotalGoingToPrincipal As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_DMP_TotalGoingToPrincipal As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_Self_TotalPercentGoingToPrincipal As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_DMP_TotalPercentGoingToprincipal As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LineControl1 As DevExpress.XtraEditors.PanelControl
        Friend WithEvents LineControl2 As DevExpress.XtraEditors.PanelControl
        Friend WithEvents LineControl3 As DevExpress.XtraEditors.PanelControl
        Friend WithEvents LineControl4 As DevExpress.XtraEditors.PanelControl
        Friend WithEvents lbl_DMP_TotalAmountPaid As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_Self_TotalPaid As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_DMP_TotalInterestPaid As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_Self_TotalInterestPaid As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_Self_LengthYears As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_Self_LengthMonths As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_DMP_TotalInterestRate_2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_Self_TotalInterestRate_2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_DMP_TotalEnteredPayment_2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_Self_TotalPayoutPayment_2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_DMP_TotalBalance As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_Self_TotalBalance As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl45 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl46 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl47 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl48 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl49 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl50 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl51 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl52 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl32 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl35 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_DMP_LengthMonths As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_DMP_LengthYears As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl44 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents clc_ExtraDMPAmount As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents LineControl5 As DevExpress.XtraEditors.PanelControl
        Friend WithEvents LabelControl53 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_DMP_TotalPayment As DevExpress.XtraEditors.LabelControl
        Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
        Friend WithEvents LabelControl60 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl59 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl58 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_Savings As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_Self_LateOverlimitFeeSavings As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_InterestSavings As DevExpress.XtraEditors.LabelControl

    End Class
End Namespace