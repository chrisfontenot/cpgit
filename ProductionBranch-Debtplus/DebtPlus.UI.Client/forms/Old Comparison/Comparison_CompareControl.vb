#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.UI.Client.Service
Imports DevExpress.XtraEditors

Namespace forms.Comparison

    Friend Class Comparison_CompareControl

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Private ReadOnly Property SalesFile() As SalesFileInfoComparisonClass
            Get
                Return CType(ParentForm, Form_Comparison_List).SalesFile
            End Get
        End Property

        Public Sub Read()

            ' Bind the control to the data source
            With clc_ExtraDMPAmount
                .EditValue = SalesFile.DMP_ExtraAmount
            End With

            UpdateFields()
            AddHandler clc_ExtraDMPAmount.EditValueChanging, AddressOf clc_ExtraDMPAmount_EditValueChanging
        End Sub

        Private Sub LineControl1_Paint(ByVal sender As Object, ByVal e As PaintEventArgs) _
            Handles LineControl1.Paint, LineControl2.Paint, LineControl3.Paint, LineControl4.Paint, LineControl5.Paint
            With CType(sender, PanelControl)
                Dim g As Graphics = e.Graphics
                Dim br As SolidBrush = Nothing
                Dim pn As Pen = Nothing

                Try
                    ' Fill the rectange with the background color
                    br = New SolidBrush(.Appearance.BackColor)
                    g.FillRectangle(br, e.ClipRectangle)

                    ' Draw the line
                    pn = New Pen(.Appearance.ForeColor, 2)
                    g.DrawLine(pn, New Point(0, 0), New Point(e.ClipRectangle.Width, 0))

                Finally
                    If br IsNot Nothing Then br.Dispose()
                    If pn IsNot Nothing Then pn.Dispose()
                End Try
            End With
        End Sub

        Private Sub clc_ExtraDMPAmount_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            UpdateFields()
        End Sub

        Private Sub UpdateFields()
            Const CurrencyFormat As String = "{0:c}"
            Const PercentFormat As String = "{0:p3}"
            Const IntegerFormat As String = "{0:n0}"
            Const YearsFormat As String = "{0:f1}"

            With SalesFile

                ' Find the client's plan length
                Dim PlanLength As Int32 = Convert.ToInt32(System.Math.Round(.Self_LengthMonths, 0))
                If PlanLength < 1.0 Then
                    lbl_Self_LengthMonths.Text = "---"
                    lbl_Self_LengthYears.Text = "---"
                Else
                    lbl_Self_LengthMonths.Text = String.Format(IntegerFormat, PlanLength)
                    lbl_Self_LengthYears.Text = String.Format(YearsFormat, Convert.ToDouble(PlanLength) / 12.0#)
                End If

                ' Find the plan's plan length
                PlanLength = Convert.ToInt32(System.Math.Round(.DMP_LengthMonths, 0))
                If PlanLength < 1.0 Then
                    lbl_DMP_LengthMonths.Text = "---"
                    lbl_DMP_LengthYears.Text = "---"
                Else
                    lbl_DMP_LengthMonths.Text = String.Format(IntegerFormat, PlanLength)
                    lbl_DMP_LengthYears.Text = String.Format(YearsFormat, Convert.ToDouble(PlanLength) / 12.0#)
                End If

                lbl_Self_TotalPayoutPayment_1.Text = String.Format(CurrencyFormat, .Self_TotalPayoutPayment)
                lbl_Self_TotalPayoutPayment_2.Text = String.Format(CurrencyFormat, .Self_TotalPayoutPayment)
                lbl_Self_TotalFinanceCharge.Text = String.Format(CurrencyFormat, .Self_TotalFinanceCharge)
                lbl_Self_TotalInterestRate_1.Text = String.Format(PercentFormat,
                                                                  System.Math.Round(.Self_TotalInterestRate, 3))
                lbl_Self_TotalInterestRate_2.Text = String.Format(PercentFormat,
                                                                  System.Math.Round(.Self_TotalInterestRate, 3))
                lbl_Self_TotalInterestAndFees.Text = String.Format(CurrencyFormat, .Self_TotalInterestAndFees)
                lbl_Self_TotalGoingToPrincipal.Text = String.Format(CurrencyFormat, .Self_TotalGoingToPrincipal)
                lbl_Self_TotalPercentGoingToPrincipal.Text = String.Format(PercentFormat, .Self_TotalPercentGoingToPrincipal)
                lbl_Self_TotalBalance.Text = String.Format(CurrencyFormat, .TotalBalance)
                lbl_Self_TotalInterestPaid.Text = String.Format(CurrencyFormat, .Self_TotalInterestPaid)
                lbl_Self_TotalPaid.Text = String.Format(CurrencyFormat, .Self_TotalPaid)
                lbl_Self_LateOverlimitFeeSavings.Text = String.Format(CurrencyFormat, .Self_LateOverlimitFeeSavings)

                lbl_DMP_TotalEnteredPayment_1.Text = String.Format(CurrencyFormat, .DMP_TotalEnteredPayment)
                lbl_DMP_TotalFinanceCharge.Text = String.Format(CurrencyFormat, .DMP_TotalFinanceCharge)
                lbl_DMP_TotalInterestRate_1.Text = String.Format(PercentFormat, System.Math.Round(.DMP_TotalInterestRate, 3))
                lbl_DMP_TotalInterestFees.Text = String.Format(CurrencyFormat, .DMP_TotalInterestFees)
                lbl_DMP_TotalGoingToPrincipal.Text = String.Format(CurrencyFormat, .DMP_TotalGoingToPrincipal)
                lbl_DMP_TotalPercentGoingToprincipal.Text = String.Format(PercentFormat, .DMP_TotalPercentGoingToprincipal)
                lbl_DMP_TotalBalance.Text = String.Format(CurrencyFormat, .TotalBalance)
                lbl_DMP_TotalInterestPaid.Text = String.Format(CurrencyFormat, .DMP_TotalInterestPaid)
                lbl_DMP_TotalAmountPaid.Text = String.Format(CurrencyFormat, .DMP_TotalAmountPaid)
                lbl_DMP_TotalPayment.Text = String.Format(CurrencyFormat, .DMP_TotalPayment)
                lbl_DMP_TotalEnteredPayment_2.Text = String.Format(CurrencyFormat, .DMP_TotalEnteredPayment)
                lbl_DMP_TotalInterestRate_2.Text = String.Format(PercentFormat, System.Math.Round(.DMP_TotalInterestRate, 3))

                lbl_MonthlySavingsPayments.Text = String.Format(CurrencyFormat, .MonthlySavings_Payments)
                lbl_MonthlySavingsFinanceCharge.Text = String.Format(CurrencyFormat, .MonthlySavings_FinanceCharge)
                lbl_MonthlySavingsEstimatedInterestRate.Text = String.Format(PercentFormat,
                                                                             .MonthlySavings_EstimatedInterestRate)
                lbl_MonthlySavingsTotalInterestAndFees.Text = String.Format(CurrencyFormat,
                                                                            .MonthlySavings_TotalInterestAndFees)
                lbl_Savings.Text = String.Format(CurrencyFormat, .Savings)
                lbl_InterestSavings.Text = String.Format(CurrencyFormat, .Self_TotalInterestPaid - .DMP_TotalInterestPaid)
            End With
        End Sub

        Private Sub clc_ExtraDMPAmount_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)
            Dim NewValue As Decimal = 0D

            If e.NewValue IsNot Nothing AndAlso e.NewValue IsNot DBNull.Value Then
                NewValue = Convert.ToDecimal(e.NewValue)
                If NewValue >= 0D Then
                    SalesFile.DMP_ExtraAmount = NewValue
                    UpdateFields()
                    Return
                End If
            End If

            e.Cancel = True
        End Sub
    End Class
End Namespace