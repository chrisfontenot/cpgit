#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports System.ComponentModel
Imports DebtPlus.UI.Client.Service
Imports System.Drawing.Printing
Imports DevExpress.XtraReports.UI

Namespace forms.Comparison.Report

    Friend Class Comparison_Report_Client
        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(ByVal Container As IContainer)
            MyClass.New()
            Container.Add(Me)
        End Sub

        ''' <summary>
        ''' Locate the original sales information for this report
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function SalesFileInfo() As SalesFileInfoComparisonClass
            Dim MasterRpt As XtraReport = Me.MasterReport
            Return CType(MasterRpt.Tag, SalesFileInfoComparisonClass)
        End Function

        ''' <summary>
        ''' Print the total balance amount
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub XrLabel_Total_Balance_BeforePrint(sender As Object, e As PrintEventArgs) _
            Handles XrLabel_Total_Balance.BeforePrint
            With CType(sender, XRLabel)
                .Text = String.Format("{0:c}", SalesFileInfo.TotalBalance)
            End With
        End Sub

        ''' <summary>
        ''' Print the total payout information
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub XrLabel_Total_PayoutPayment_BeforePrint(sender As Object, e As PrintEventArgs) _
            Handles XrLabel_Total_PayoutPayment.BeforePrint
            With CType(sender, XRLabel)
                .Text = String.Format("{0:c}", SalesFileInfo.Self_TotalPayoutPayment)
            End With
        End Sub

        ''' <summary>
        ''' Print the total late fees information
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub XrLabel_Total_LateFees_BeforePrint(sender As Object, e As PrintEventArgs) _
            Handles XrLabel_Total_LateFees.BeforePrint
            With CType(sender, XRLabel)
                .Text = String.Format("{0:c}", SalesFileInfo.Self_TotalLateFees)
            End With
        End Sub

        ''' <summary>
        ''' Print the total over-limit fees information
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub XrLabel_Total_OverLimitFees_BeforePrint(sender As Object, e As PrintEventArgs) _
            Handles XrLabel_Total_OverLimitFees.BeforePrint
            With CType(sender, XRLabel)
                .Text = String.Format("{0:c}", SalesFileInfo.Self_TotalOverLimitFees)
            End With
        End Sub

        ''' <summary>
        ''' Print the total monthly finance charges information
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub XrLabel_Total_MonthlyFinanceCharge_BeforePrint(sender As Object, e As PrintEventArgs) _
            Handles XrLabel_Total_MonthlyFinanceCharge.BeforePrint
            With CType(sender, XRLabel)
                .Text = String.Format("{0:c}", SalesFileInfo.Self_TotalFinanceCharge)
            End With
        End Sub

        ''' <summary>
        ''' Print the total interest and fees information
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub XrLabel_Total_InterestAndFees_BeforePrint(sender As Object, e As PrintEventArgs) _
            Handles XrLabel_Total_InterestAndFees.BeforePrint
            With CType(sender, XRLabel)
                .Text = String.Format("{0:c}", SalesFileInfo.Self_TotalInterestAndFees)
            End With
        End Sub

        ''' <summary>
        ''' Print the total dollar amount going to the principal information
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub XrLabel_Total_GoingToPrincipal_BeforePrint(sender As Object, e As PrintEventArgs) _
            Handles XrLabel_Total_GoingToPrincipal.BeforePrint
            With CType(sender, XRLabel)
                .Text = String.Format("{0:c}", SalesFileInfo.Self_TotalGoingToPrincipal)
            End With
        End Sub

        ''' <summary>
        ''' Print the total interest rate information
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub XrLabel_Total_InterestRate_BeforePrint(sender As Object, e As PrintEventArgs) _
            Handles XrLabel_Total_InterestRate.BeforePrint
            With CType(sender, XRLabel)
                .Text = String.Format("{0:p}", SalesFileInfo.Self_TotalInterestRate)
            End With
        End Sub
    End Class
End Namespace