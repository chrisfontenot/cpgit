Namespace forms.Comparison.Report
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Comparison_Report_Client
        Inherits DevExpress.XtraReports.UI.XtraReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.Detail = New DevExpress.XtraReports.UI.DetailBand
            Me.XrLabel_Self_GoingToPrincipal = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Self_InterestAndFees = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_MonthlyFinanceCharge = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Self_OverLimitFees = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Self_LateFees = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Self_MonthlyPayment = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Self_InterestRate = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Balance = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Name = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_CreditorType = New DevExpress.XtraReports.UI.XRLabel
            Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine
            Me.XrLabel27 = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Total_InterestAndFees = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Total_MonthlyFinanceCharge = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Total_OverLimitFees = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Total_LateFees = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Total_PayoutPayment = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Total_InterestRate = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Total_Balance = New DevExpress.XtraReports.UI.XRLabel
            Me.XrLabel_Total_GoingToPrincipal = New DevExpress.XtraReports.UI.XRLabel
            Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel
            Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand
            Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_Self_GoingToPrincipal, Me.XrLabel_Self_InterestAndFees, Me.XrLabel_MonthlyFinanceCharge, Me.XrLabel_Self_OverLimitFees, Me.XrLabel_Self_LateFees, Me.XrLabel_Self_MonthlyPayment, Me.XrLabel_Self_InterestRate, Me.XrLabel_Balance, Me.XrLabel_Name, Me.XrLabel_CreditorType})
            Me.Detail.HeightF = 17.0!
            Me.Detail.Name = "Detail"
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_Self_GoingToPrincipal
            '
            Me.XrLabel_Self_GoingToPrincipal.CanGrow = False
            Me.XrLabel_Self_GoingToPrincipal.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Self_GoingToPrincipal", "{0:c}")})
            Me.XrLabel_Self_GoingToPrincipal.LocationFloat = New DevExpress.Utils.PointFloat(891.0!, 0.0!)
            Me.XrLabel_Self_GoingToPrincipal.Name = "XrLabel_Self_GoingToPrincipal"
            Me.XrLabel_Self_GoingToPrincipal.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_Self_GoingToPrincipal.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_Self_GoingToPrincipal.StylePriority.UsePadding = False
            Me.XrLabel_Self_GoingToPrincipal.StylePriority.UseTextAlignment = False
            Me.XrLabel_Self_GoingToPrincipal.Text = "$0.00"
            Me.XrLabel_Self_GoingToPrincipal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_Self_GoingToPrincipal.WordWrap = False
            '
            'XrLabel_Self_InterestAndFees
            '
            Me.XrLabel_Self_InterestAndFees.CanGrow = False
            Me.XrLabel_Self_InterestAndFees.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Self_InterestAndFees", "{0:c}")})
            Me.XrLabel_Self_InterestAndFees.LocationFloat = New DevExpress.Utils.PointFloat(808.0!, 0.0!)
            Me.XrLabel_Self_InterestAndFees.Name = "XrLabel_Self_InterestAndFees"
            Me.XrLabel_Self_InterestAndFees.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_Self_InterestAndFees.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_Self_InterestAndFees.StylePriority.UsePadding = False
            Me.XrLabel_Self_InterestAndFees.StylePriority.UseTextAlignment = False
            Me.XrLabel_Self_InterestAndFees.Text = "$0.00"
            Me.XrLabel_Self_InterestAndFees.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_Self_InterestAndFees.WordWrap = False
            '
            'XrLabel_Self_MonthlyFinanceCharge
            '
            Me.XrLabel_MonthlyFinanceCharge.CanGrow = False
            Me.XrLabel_MonthlyFinanceCharge.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Self_MonthlyFinanceCharge", "{0:c}")})
            Me.XrLabel_MonthlyFinanceCharge.LocationFloat = New DevExpress.Utils.PointFloat(725.0!, 0.0!)
            Me.XrLabel_MonthlyFinanceCharge.Name = "XrLabel_Self_MonthlyFinanceCharge"
            Me.XrLabel_MonthlyFinanceCharge.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_MonthlyFinanceCharge.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_MonthlyFinanceCharge.StylePriority.UsePadding = False
            Me.XrLabel_MonthlyFinanceCharge.StylePriority.UseTextAlignment = False
            Me.XrLabel_MonthlyFinanceCharge.Text = "$0.00"
            Me.XrLabel_MonthlyFinanceCharge.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_MonthlyFinanceCharge.WordWrap = False
            '
            'XrLabel_Self_OverLimitFees
            '
            Me.XrLabel_Self_OverLimitFees.CanGrow = False
            Me.XrLabel_Self_OverLimitFees.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Self_OverLimitFees", "{0:c}")})
            Me.XrLabel_Self_OverLimitFees.LocationFloat = New DevExpress.Utils.PointFloat(641.0!, 0.0!)
            Me.XrLabel_Self_OverLimitFees.Name = "XrLabel_Self_OverLimitFees"
            Me.XrLabel_Self_OverLimitFees.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_Self_OverLimitFees.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_Self_OverLimitFees.StylePriority.UsePadding = False
            Me.XrLabel_Self_OverLimitFees.StylePriority.UseTextAlignment = False
            Me.XrLabel_Self_OverLimitFees.Text = "$0.00"
            Me.XrLabel_Self_OverLimitFees.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_Self_OverLimitFees.WordWrap = False
            '
            'XrLabel_Self_LateFees
            '
            Me.XrLabel_Self_LateFees.CanGrow = False
            Me.XrLabel_Self_LateFees.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Self_LateFees", "{0:c}")})
            Me.XrLabel_Self_LateFees.LocationFloat = New DevExpress.Utils.PointFloat(558.0!, 0.0!)
            Me.XrLabel_Self_LateFees.Name = "XrLabel_Self_LateFees"
            Me.XrLabel_Self_LateFees.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_Self_LateFees.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_Self_LateFees.StylePriority.UsePadding = False
            Me.XrLabel_Self_LateFees.StylePriority.UseTextAlignment = False
            Me.XrLabel_Self_LateFees.Text = "$0.00"
            Me.XrLabel_Self_LateFees.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_Self_LateFees.WordWrap = False
            '
            'XrLabel_Self_MonthlyPayment
            '
            Me.XrLabel_Self_MonthlyPayment.CanGrow = False
            Me.XrLabel_Self_MonthlyPayment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Self_MonthlyPayment", "{0:c}")})
            Me.XrLabel_Self_MonthlyPayment.LocationFloat = New DevExpress.Utils.PointFloat(475.0!, 0.0!)
            Me.XrLabel_Self_MonthlyPayment.Name = "XrLabel_Self_MonthlyPayment"
            Me.XrLabel_Self_MonthlyPayment.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_Self_MonthlyPayment.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_Self_MonthlyPayment.StylePriority.UsePadding = False
            Me.XrLabel_Self_MonthlyPayment.StylePriority.UseTextAlignment = False
            Me.XrLabel_Self_MonthlyPayment.Text = "$0.00"
            Me.XrLabel_Self_MonthlyPayment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_Self_MonthlyPayment.WordWrap = False
            '
            'XrLabel_Self_InterestRate
            '
            Me.XrLabel_Self_InterestRate.CanGrow = False
            Me.XrLabel_Self_InterestRate.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Self_InterestRate", "{0:p3}")})
            Me.XrLabel_Self_InterestRate.LocationFloat = New DevExpress.Utils.PointFloat(400.0!, 0.0!)
            Me.XrLabel_Self_InterestRate.Name = "XrLabel_Self_InterestRate"
            Me.XrLabel_Self_InterestRate.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_Self_InterestRate.SizeF = New System.Drawing.SizeF(66.0!, 17.0!)
            Me.XrLabel_Self_InterestRate.StylePriority.UsePadding = False
            Me.XrLabel_Self_InterestRate.StylePriority.UseTextAlignment = False
            Me.XrLabel_Self_InterestRate.Text = "0.000 %"
            Me.XrLabel_Self_InterestRate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_Self_InterestRate.WordWrap = False
            '
            'XrLabel_Balance
            '
            Me.XrLabel_Balance.CanGrow = False
            Me.XrLabel_Balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Balance", "{0:c}")})
            Me.XrLabel_Balance.LocationFloat = New DevExpress.Utils.PointFloat(308.0!, 0.0!)
            Me.XrLabel_Balance.Name = "XrLabel_Balance"
            Me.XrLabel_Balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_Balance.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_Balance.StylePriority.UsePadding = False
            Me.XrLabel_Balance.StylePriority.UseTextAlignment = False
            Me.XrLabel_Balance.Text = "$0.00"
            Me.XrLabel_Balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_Balance.WordWrap = False
            '
            'XrLabel_Name
            '
            Me.XrLabel_Name.CanGrow = False
            Me.XrLabel_Name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Name")})
            Me.XrLabel_Name.LocationFloat = New DevExpress.Utils.PointFloat(91.0!, 0.0!)
            Me.XrLabel_Name.Name = "XrLabel_Name"
            Me.XrLabel_Name.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_Name.SizeF = New System.Drawing.SizeF(208.0!, 17.0!)
            Me.XrLabel_Name.StylePriority.UsePadding = False
            Me.XrLabel_Name.WordWrap = False
            '
            'XrLabel_CreditorType
            '
            Me.XrLabel_CreditorType.CanGrow = False
            Me.XrLabel_CreditorType.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "CreditorType")})
            Me.XrLabel_CreditorType.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
            Me.XrLabel_CreditorType.Name = "XrLabel_CreditorType"
            Me.XrLabel_CreditorType.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_CreditorType.SizeF = New System.Drawing.SizeF(66.0!, 17.0!)
            Me.XrLabel_CreditorType.StylePriority.UsePadding = False
            Me.XrLabel_CreditorType.WordWrap = False
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 51.0!
            Me.PageHeader.Name = "PageHeader"
            Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel2, Me.XrLabel3, Me.XrLabel4, Me.XrLabel5, Me.XrLabel6, Me.XrLabel7, Me.XrLabel8, Me.XrLabel9, Me.XrLabel10, Me.XrLabel11})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(1050.0!, 44.0!)
            Me.XrPanel1.StylePriority.UseBackColor = False
            '
            'XrLabel2
            '
            Me.XrLabel2.CanGrow = False
            Me.XrLabel2.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel2.ForeColor = System.Drawing.Color.White
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
            Me.XrLabel2.Multiline = True
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(66.0!, 42.0!)
            Me.XrLabel2.StylePriority.UseFont = False
            Me.XrLabel2.StylePriority.UseForeColor = False
            Me.XrLabel2.StylePriority.UsePadding = False
            Me.XrLabel2.StylePriority.UseTextAlignment = False
            Me.XrLabel2.Text = "" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "TYPE OF" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "ACCOUNT"
            Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
            '
            'XrLabel3
            '
            Me.XrLabel3.CanGrow = False
            Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel3.ForeColor = System.Drawing.Color.White
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(891.0!, 0.0!)
            Me.XrLabel3.Multiline = True
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(75.0!, 42.0!)
            Me.XrLabel3.StylePriority.UseFont = False
            Me.XrLabel3.StylePriority.UseForeColor = False
            Me.XrLabel3.StylePriority.UsePadding = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "$ GOING" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "TO" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "PRINCIPAL"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'XrLabel4
            '
            Me.XrLabel4.CanGrow = False
            Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel4.ForeColor = System.Drawing.Color.White
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(808.0!, 0.0!)
            Me.XrLabel4.Multiline = True
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(75.0!, 42.0!)
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.StylePriority.UseForeColor = False
            Me.XrLabel4.StylePriority.UsePadding = False
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "TOTAL" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "INTEREST" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "& FEES"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'XrLabel5
            '
            Me.XrLabel5.CanGrow = False
            Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel5.ForeColor = System.Drawing.Color.White
            Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(725.0!, 0.0!)
            Me.XrLabel5.Multiline = True
            Me.XrLabel5.Name = "XrLabel5"
            Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel5.SizeF = New System.Drawing.SizeF(75.0!, 42.0!)
            Me.XrLabel5.StylePriority.UseFont = False
            Me.XrLabel5.StylePriority.UseForeColor = False
            Me.XrLabel5.StylePriority.UsePadding = False
            Me.XrLabel5.StylePriority.UseTextAlignment = False
            Me.XrLabel5.Text = "" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "FINANCE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "CHARGE"
            Me.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'XrLabel6
            '
            Me.XrLabel6.CanGrow = False
            Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel6.ForeColor = System.Drawing.Color.White
            Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(641.0!, 0.0!)
            Me.XrLabel6.Multiline = True
            Me.XrLabel6.Name = "XrLabel6"
            Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel6.SizeF = New System.Drawing.SizeF(75.0!, 42.0!)
            Me.XrLabel6.StylePriority.UseFont = False
            Me.XrLabel6.StylePriority.UseForeColor = False
            Me.XrLabel6.StylePriority.UsePadding = False
            Me.XrLabel6.StylePriority.UseTextAlignment = False
            Me.XrLabel6.Text = "" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "OVERLIMIT" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "FEES"
            Me.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'XrLabel7
            '
            Me.XrLabel7.CanGrow = False
            Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel7.ForeColor = System.Drawing.Color.White
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(558.0!, 0.0!)
            Me.XrLabel7.Multiline = True
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(75.0!, 42.0!)
            Me.XrLabel7.StylePriority.UseFont = False
            Me.XrLabel7.StylePriority.UseForeColor = False
            Me.XrLabel7.StylePriority.UsePadding = False
            Me.XrLabel7.StylePriority.UseTextAlignment = False
            Me.XrLabel7.Text = "" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "LATE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "FEES"
            Me.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'XrLabel8
            '
            Me.XrLabel8.CanGrow = False
            Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel8.ForeColor = System.Drawing.Color.White
            Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(475.0!, 0.0!)
            Me.XrLabel8.Multiline = True
            Me.XrLabel8.Name = "XrLabel8"
            Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel8.SizeF = New System.Drawing.SizeF(75.0!, 42.0!)
            Me.XrLabel8.StylePriority.UseFont = False
            Me.XrLabel8.StylePriority.UseForeColor = False
            Me.XrLabel8.StylePriority.UsePadding = False
            Me.XrLabel8.StylePriority.UseTextAlignment = False
            Me.XrLabel8.Text = "REQUIRED" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "MINIMUM" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "PAYMENT"
            Me.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'XrLabel9
            '
            Me.XrLabel9.CanGrow = False
            Me.XrLabel9.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel9.ForeColor = System.Drawing.Color.White
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(400.0!, 0.0!)
            Me.XrLabel9.Multiline = True
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(66.0!, 42.0!)
            Me.XrLabel9.StylePriority.UseFont = False
            Me.XrLabel9.StylePriority.UseForeColor = False
            Me.XrLabel9.StylePriority.UsePadding = False
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "INTEREST" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "RATE"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'XrLabel10
            '
            Me.XrLabel10.CanGrow = False
            Me.XrLabel10.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel10.ForeColor = System.Drawing.Color.White
            Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(308.0!, 0.0!)
            Me.XrLabel10.Multiline = True
            Me.XrLabel10.Name = "XrLabel10"
            Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel10.SizeF = New System.Drawing.SizeF(75.0!, 42.0!)
            Me.XrLabel10.StylePriority.UseFont = False
            Me.XrLabel10.StylePriority.UseForeColor = False
            Me.XrLabel10.StylePriority.UsePadding = False
            Me.XrLabel10.StylePriority.UseTextAlignment = False
            Me.XrLabel10.Text = "" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "CURRENT" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "BALANCE"
            Me.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'XrLabel11
            '
            Me.XrLabel11.CanGrow = False
            Me.XrLabel11.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel11.ForeColor = System.Drawing.Color.White
            Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(91.0!, 0.0!)
            Me.XrLabel11.Multiline = True
            Me.XrLabel11.Name = "XrLabel11"
            Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel11.SizeF = New System.Drawing.SizeF(209.0!, 42.0!)
            Me.XrLabel11.StylePriority.UseFont = False
            Me.XrLabel11.StylePriority.UseForeColor = False
            Me.XrLabel11.StylePriority.UsePadding = False
            Me.XrLabel11.StylePriority.UseTextAlignment = False
            Me.XrLabel11.Text = "NAME OF ACCOUNT"
            Me.XrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLine1, Me.XrLabel27, Me.XrLabel_Total_InterestAndFees, Me.XrLabel_Total_MonthlyFinanceCharge, Me.XrLabel_Total_OverLimitFees, Me.XrLabel_Total_LateFees, Me.XrLabel_Total_PayoutPayment, Me.XrLabel_Total_InterestRate, Me.XrLabel_Total_Balance, Me.XrLabel_Total_GoingToPrincipal})
            Me.ReportFooter.Font = New System.Drawing.Font("Times New Roman", 16.0!, System.Drawing.FontStyle.Bold)
            Me.ReportFooter.HeightF = 41.0!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.StylePriority.UseFont = False
            '
            'XrLine1
            '
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 2.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLine1.SizeF = New System.Drawing.SizeF(958.0!, 9.0!)
            Me.XrLine1.StylePriority.UsePadding = False
            '
            'XrLabel27
            '
            Me.XrLabel27.CanGrow = False
            Me.XrLabel27.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel27.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 17.0!)
            Me.XrLabel27.Name = "XrLabel27"
            Me.XrLabel27.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel27.SizeF = New System.Drawing.SizeF(208.0!, 17.0!)
            Me.XrLabel27.StylePriority.UseFont = False
            Me.XrLabel27.StylePriority.UsePadding = False
            Me.XrLabel27.Text = "TOTALS"
            Me.XrLabel27.WordWrap = False
            '
            'XrLabel_Total_InterestAndFees
            '
            Me.XrLabel_Total_InterestAndFees.CanGrow = False
            Me.XrLabel_Total_InterestAndFees.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_InterestAndFees.LocationFloat = New DevExpress.Utils.PointFloat(808.0!, 17.0!)
            Me.XrLabel_Total_InterestAndFees.Name = "XrLabel_Total_InterestAndFees"
            Me.XrLabel_Total_InterestAndFees.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_Total_InterestAndFees.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_Total_InterestAndFees.StylePriority.UseFont = False
            Me.XrLabel_Total_InterestAndFees.StylePriority.UsePadding = False
            Me.XrLabel_Total_InterestAndFees.StylePriority.UseTextAlignment = False
            Me.XrLabel_Total_InterestAndFees.Text = "$0.00"
            Me.XrLabel_Total_InterestAndFees.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_Total_InterestAndFees.WordWrap = False
            '
            'XrLabel_Total_MonthlyFinanceCharge
            '
            Me.XrLabel_Total_MonthlyFinanceCharge.CanGrow = False
            Me.XrLabel_Total_MonthlyFinanceCharge.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_MonthlyFinanceCharge.LocationFloat = New DevExpress.Utils.PointFloat(725.0!, 17.0!)
            Me.XrLabel_Total_MonthlyFinanceCharge.Name = "XrLabel_Total_MonthlyFinanceCharge"
            Me.XrLabel_Total_MonthlyFinanceCharge.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_Total_MonthlyFinanceCharge.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_Total_MonthlyFinanceCharge.StylePriority.UseFont = False
            Me.XrLabel_Total_MonthlyFinanceCharge.StylePriority.UsePadding = False
            Me.XrLabel_Total_MonthlyFinanceCharge.StylePriority.UseTextAlignment = False
            Me.XrLabel_Total_MonthlyFinanceCharge.Text = "$0.00"
            Me.XrLabel_Total_MonthlyFinanceCharge.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_Total_MonthlyFinanceCharge.WordWrap = False
            '
            'XrLabel_Total_OverLimitFees
            '
            Me.XrLabel_Total_OverLimitFees.CanGrow = False
            Me.XrLabel_Total_OverLimitFees.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_OverLimitFees.LocationFloat = New DevExpress.Utils.PointFloat(642.0!, 17.0!)
            Me.XrLabel_Total_OverLimitFees.Name = "XrLabel_Total_OverLimitFees"
            Me.XrLabel_Total_OverLimitFees.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_Total_OverLimitFees.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_Total_OverLimitFees.StylePriority.UseFont = False
            Me.XrLabel_Total_OverLimitFees.StylePriority.UsePadding = False
            Me.XrLabel_Total_OverLimitFees.StylePriority.UseTextAlignment = False
            Me.XrLabel_Total_OverLimitFees.Text = "$0.00"
            Me.XrLabel_Total_OverLimitFees.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_Total_OverLimitFees.WordWrap = False
            '
            'XrLabel_Total_LateFees
            '
            Me.XrLabel_Total_LateFees.CanGrow = False
            Me.XrLabel_Total_LateFees.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_LateFees.LocationFloat = New DevExpress.Utils.PointFloat(558.0!, 17.0!)
            Me.XrLabel_Total_LateFees.Name = "XrLabel_Total_LateFees"
            Me.XrLabel_Total_LateFees.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_Total_LateFees.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_Total_LateFees.StylePriority.UseFont = False
            Me.XrLabel_Total_LateFees.StylePriority.UsePadding = False
            Me.XrLabel_Total_LateFees.StylePriority.UseTextAlignment = False
            Me.XrLabel_Total_LateFees.Text = "$0.00"
            Me.XrLabel_Total_LateFees.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_Total_LateFees.WordWrap = False
            '
            'XrLabel_Total_PayoutPayment
            '
            Me.XrLabel_Total_PayoutPayment.CanGrow = False
            Me.XrLabel_Total_PayoutPayment.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_PayoutPayment.LocationFloat = New DevExpress.Utils.PointFloat(475.0!, 17.0!)
            Me.XrLabel_Total_PayoutPayment.Name = "XrLabel_Total_PayoutPayment"
            Me.XrLabel_Total_PayoutPayment.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_Total_PayoutPayment.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_Total_PayoutPayment.StylePriority.UseFont = False
            Me.XrLabel_Total_PayoutPayment.StylePriority.UsePadding = False
            Me.XrLabel_Total_PayoutPayment.StylePriority.UseTextAlignment = False
            Me.XrLabel_Total_PayoutPayment.Text = "$0.00"
            Me.XrLabel_Total_PayoutPayment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_Total_PayoutPayment.WordWrap = False
            '
            'XrLabel_Total_InterestRate
            '
            Me.XrLabel_Total_InterestRate.CanGrow = False
            Me.XrLabel_Total_InterestRate.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_InterestRate.LocationFloat = New DevExpress.Utils.PointFloat(400.0!, 17.0!)
            Me.XrLabel_Total_InterestRate.Name = "XrLabel_Total_InterestRate"
            Me.XrLabel_Total_InterestRate.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_Total_InterestRate.SizeF = New System.Drawing.SizeF(66.0!, 17.0!)
            Me.XrLabel_Total_InterestRate.StylePriority.UseFont = False
            Me.XrLabel_Total_InterestRate.StylePriority.UsePadding = False
            Me.XrLabel_Total_InterestRate.StylePriority.UseTextAlignment = False
            Me.XrLabel_Total_InterestRate.Text = "0.000 %"
            Me.XrLabel_Total_InterestRate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_Total_InterestRate.WordWrap = False
            '
            'XrLabel_Total_Balance
            '
            Me.XrLabel_Total_Balance.CanGrow = False
            Me.XrLabel_Total_Balance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_Balance.LocationFloat = New DevExpress.Utils.PointFloat(308.0!, 17.0!)
            Me.XrLabel_Total_Balance.Name = "XrLabel_Total_Balance"
            Me.XrLabel_Total_Balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_Total_Balance.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_Total_Balance.StylePriority.UseFont = False
            Me.XrLabel_Total_Balance.StylePriority.UsePadding = False
            Me.XrLabel_Total_Balance.StylePriority.UseTextAlignment = False
            Me.XrLabel_Total_Balance.Text = "$0.00"
            Me.XrLabel_Total_Balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_Total_Balance.WordWrap = False
            '
            'XrLabel_Total_GoingToPrincipal
            '
            Me.XrLabel_Total_GoingToPrincipal.CanGrow = False
            Me.XrLabel_Total_GoingToPrincipal.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_GoingToPrincipal.LocationFloat = New DevExpress.Utils.PointFloat(892.0!, 17.0!)
            Me.XrLabel_Total_GoingToPrincipal.Name = "XrLabel_Total_GoingToPrincipal"
            Me.XrLabel_Total_GoingToPrincipal.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_Total_GoingToPrincipal.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_Total_GoingToPrincipal.StylePriority.UseFont = False
            Me.XrLabel_Total_GoingToPrincipal.StylePriority.UsePadding = False
            Me.XrLabel_Total_GoingToPrincipal.StylePriority.UseTextAlignment = False
            Me.XrLabel_Total_GoingToPrincipal.Text = "$0.00"
            Me.XrLabel_Total_GoingToPrincipal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_Total_GoingToPrincipal.WordWrap = False
            '
            'ReportHeader
            '
            Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1})
            Me.ReportHeader.HeightF = 52.0!
            Me.ReportHeader.Name = "ReportHeader"
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Times New Roman", 16.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(1050.0!, 33.0!)
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "INFORMATION BASED UPON THE CURRENT CLIENT PAYMENTS"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'TopMargin
            '
            Me.TopMargin.HeightF = 25.0!
            Me.TopMargin.Name = "TopMargin"
            '
            'BottomMargin
            '
            Me.BottomMargin.HeightF = 25.0!
            Me.BottomMargin.Name = "BottomMargin"
            '
            'Comparison_Report_Client
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.ReportFooter, Me.ReportHeader, Me.TopMargin, Me.BottomMargin})
            Me.Landscape = True
            Me.Margins = New System.Drawing.Printing.Margins(25, 25, 25, 25)
            Me.PageHeight = 850
            Me.PageWidth = 1100
            Me.Version = "10.1"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
        Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
        Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_CreditorType As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Self_GoingToPrincipal As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Self_InterestAndFees As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_MonthlyFinanceCharge As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Self_OverLimitFees As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Self_LateFees As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Self_MonthlyPayment As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Self_InterestRate As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel27 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_InterestAndFees As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_MonthlyFinanceCharge As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_OverLimitFees As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_LateFees As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_PayoutPayment As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_InterestRate As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_Balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_GoingToPrincipal As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
        Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
    End Class
End Namespace