#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports System.ComponentModel
Imports DebtPlus.UI.Client.Service
Imports System.Drawing.Printing
Imports DevExpress.XtraReports.UI

Namespace forms.Comparison.Report

    Friend Class Comparison_Report_Plan
        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(ByVal Container As IContainer)
            MyClass.New()
            Container.Add(Me)
        End Sub

        Private Function SalesFileInfo() As SalesFileInfoComparisonClass
            Dim MasterRpt As XtraReport = Me.MasterReport
            Return CType(MasterRpt.Tag, SalesFileInfoComparisonClass)
        End Function

        Private Sub XrLabel_DMP_TotalMinimumPayment_BeforePrint(sender As Object, e As PrintEventArgs) _
            Handles XrLabel_DMP_TotalMinimumPayment.BeforePrint
            With CType(sender, XRLabel)
                .Text = String.Format("{0:c}", SalesFileInfo.DMP_TotalMinimumPayment)
            End With
        End Sub

        Private Sub XrLabel_DMP_TotalEnteredPayment_BeforePrint(sender As Object, e As PrintEventArgs) _
            Handles XrLabel_DMP_TotalEnteredPayment.BeforePrint
            With CType(sender, XRLabel)
                .Text = String.Format("{0:c}", SalesFileInfo.DMP_TotalEnteredPayment)
            End With
        End Sub

        Private Sub XrLabel_DMP_TotalInterestRate_BeforePrint(sender As Object, e As PrintEventArgs) _
            Handles XrLabel_DMP_TotalInterestRate.BeforePrint
            With CType(sender, XRLabel)
                .Text = String.Format("{0:p}", SalesFileInfo.DMP_TotalInterestRate)
            End With
        End Sub

        Private Sub XrLabel_DMP_TotalFees_BeforePrint(sender As Object, e As PrintEventArgs) _
            Handles XrLabel_DMP_TotalFees.BeforePrint
            With CType(sender, XRLabel)
                .Text = String.Format("{0:c}", SalesFileInfo.DMP_TotalFees)
            End With
        End Sub

        Private Sub XrLabel_DMP_TotalFinanceCharge_BeforePrint(sender As Object, e As PrintEventArgs) _
            Handles XrLabel_DMP_TotalFinanceCharge.BeforePrint
            With CType(sender, XRLabel)
                .Text = String.Format("{0:c}", SalesFileInfo.DMP_TotalFinanceCharge)
            End With
        End Sub

        Private Sub XrLabel_DMP_TotalInterestFees_BeforePrint(sender As Object, e As PrintEventArgs) _
            Handles XrLabel_DMP_TotalInterestFees.BeforePrint
            With CType(sender, XRLabel)
                .Text = String.Format("{0:c}", SalesFileInfo.DMP_TotalInterestFees)
            End With
        End Sub

        Private Sub XrLabel_DMP_TotalGoingToPrincipal_BeforePrint(sender As Object, e As PrintEventArgs) _
            Handles XrLabel_DMP_TotalGoingToPrincipal.BeforePrint
            With CType(sender, XRLabel)
                .Text = String.Format("{0:c}", SalesFileInfo.DMP_TotalGoingToPrincipal)
            End With
        End Sub

        Private Sub XrLabel_Total_Balance_BeforePrint(sender As Object, e As PrintEventArgs) _
            Handles XrLabel_Total_Balance.BeforePrint
            With CType(sender, XRLabel)
                .Text = String.Format("{0:c}", SalesFileInfo.TotalBalance)
            End With
        End Sub
    End Class
End Namespace