Namespace forms.Comparison.Report
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Comparison_Report_Plan
        Inherits DevExpress.XtraReports.UI.XtraReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.Detail = New DevExpress.XtraReports.UI.DetailBand
            Me.XrLabel_DMP_GoingToPrincipal = New DevExpress.XtraReports.UI.XrLabel
            Me.XrLabel_DMP_InterestFees = New DevExpress.XtraReports.UI.XrLabel
            Me.XrLabel_DMP_FinanceCharge = New DevExpress.XtraReports.UI.XrLabel
            Me.XrLabel_DMP_Fees = New DevExpress.XtraReports.UI.XrLabel
            Me.XrLabel_DMP_InterestRate = New DevExpress.XtraReports.UI.XrLabel
            Me.XrLabel_DMP_EnteredPayment = New DevExpress.XtraReports.UI.XrLabel
            Me.XrLabel_DMP_MinimumPayment = New DevExpress.XtraReports.UI.XrLabel
            Me.XrLabel_DMP_MinimumProrate = New DevExpress.XtraReports.UI.XrLabel
            Me.XrLabel_Balance = New DevExpress.XtraReports.UI.XrLabel
            Me.XrLabel_Name = New DevExpress.XtraReports.UI.XrLabel
            Me.XrLabel_CreditorType = New DevExpress.XtraReports.UI.XrLabel
            Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand
            Me.XrPanel1 = New DevExpress.XtraReports.UI.XRPanel
            Me.XrLabel_11 = New DevExpress.XtraReports.UI.XrLabel
            Me.XrLabel_10 = New DevExpress.XtraReports.UI.XrLabel
            Me.XrLabel_8 = New DevExpress.XtraReports.UI.XrLabel
            Me.XrLabel_7 = New DevExpress.XtraReports.UI.XrLabel
            Me.XrLabel_6 = New DevExpress.XtraReports.UI.XrLabel
            Me.XrLabel_4 = New DevExpress.XtraReports.UI.XrLabel
            Me.XrLabel_2 = New DevExpress.XtraReports.UI.XrLabel
            Me.XrLabel_12 = New DevExpress.XtraReports.UI.XrLabel
            Me.XrLabel_9 = New DevExpress.XtraReports.UI.XrLabel
            Me.XrLabel_3 = New DevExpress.XtraReports.UI.XrLabel
            Me.XrLabel_15 = New DevExpress.XtraReports.UI.XrLabel
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand
            Me.XrLabel_DMP_TotalGoingToPrincipal = New DevExpress.XtraReports.UI.XrLabel
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine
            Me.XrLabel_27 = New DevExpress.XtraReports.UI.XrLabel
            Me.XrLabel_DMP_TotalFinanceCharge = New DevExpress.XtraReports.UI.XrLabel
            Me.XrLabel_DMP_TotalFees = New DevExpress.XtraReports.UI.XrLabel
            Me.XrLabel_DMP_TotalInterestRate = New DevExpress.XtraReports.UI.XrLabel
            Me.XrLabel_DMP_TotalEnteredPayment = New DevExpress.XtraReports.UI.XrLabel
            Me.XrLabel_DMP_TotalMinimumPayment = New DevExpress.XtraReports.UI.XrLabel
            Me.XrLabel_DMP_TotalMinimumProrate = New DevExpress.XtraReports.UI.XrLabel
            Me.XrLabel_Total_Balance = New DevExpress.XtraReports.UI.XrLabel
            Me.XrLabel_DMP_TotalInterestFees = New DevExpress.XtraReports.UI.XrLabel
            Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand
            Me.XrLabel_1 = New DevExpress.XtraReports.UI.XrLabel
            Me.XrLabel_17 = New DevExpress.XtraReports.UI.XrLabel
            Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand
            Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_DMP_GoingToPrincipal, Me.XrLabel_DMP_InterestFees, Me.XrLabel_DMP_FinanceCharge, Me.XrLabel_DMP_Fees, Me.XrLabel_DMP_InterestRate, Me.XrLabel_DMP_EnteredPayment, Me.XrLabel_DMP_MinimumPayment, Me.XrLabel_DMP_MinimumProrate, Me.XrLabel_Balance, Me.XrLabel_Name, Me.XrLabel_CreditorType})
            Me.Detail.HeightF = 17.0!
            Me.Detail.Name = "Detail"
            Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrLabel_DMP_GoingToPrincipal
            '
            Me.XrLabel_DMP_GoingToPrincipal.CanGrow = False
            Me.XrLabel_DMP_GoingToPrincipal.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "DMP_GoingToPrincipal", "{0:c}")})
            Me.XrLabel_DMP_GoingToPrincipal.LocationFloat = New DevExpress.Utils.PointFloat(975.0!, 0.0!)
            Me.XrLabel_DMP_GoingToPrincipal.Name = "XrLabel_DMP_GoingToPrincipal"
            Me.XrLabel_DMP_GoingToPrincipal.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_DMP_GoingToPrincipal.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_DMP_GoingToPrincipal.StylePriority.UsePadding = False
            Me.XrLabel_DMP_GoingToPrincipal.StylePriority.UseTextAlignment = False
            Me.XrLabel_DMP_GoingToPrincipal.Text = "$0.00"
            Me.XrLabel_DMP_GoingToPrincipal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_DMP_GoingToPrincipal.WordWrap = False
            '
            'XrLabel_DMP_InterestFees
            '
            Me.XrLabel_DMP_InterestFees.CanGrow = False
            Me.XrLabel_DMP_InterestFees.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "DMP_InterestFees", "{0:c}")})
            Me.XrLabel_DMP_InterestFees.LocationFloat = New DevExpress.Utils.PointFloat(891.0!, 0.0!)
            Me.XrLabel_DMP_InterestFees.Name = "XrLabel_DMP_InterestFees"
            Me.XrLabel_DMP_InterestFees.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_DMP_InterestFees.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_DMP_InterestFees.StylePriority.UsePadding = False
            Me.XrLabel_DMP_InterestFees.StylePriority.UseTextAlignment = False
            Me.XrLabel_DMP_InterestFees.Text = "$0.00"
            Me.XrLabel_DMP_InterestFees.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_DMP_InterestFees.WordWrap = False
            '
            'XrLabel_DMP_FinanceCharge
            '
            Me.XrLabel_DMP_FinanceCharge.CanGrow = False
            Me.XrLabel_DMP_FinanceCharge.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "DMP_FinanceCharge", "{0:c}")})
            Me.XrLabel_DMP_FinanceCharge.LocationFloat = New DevExpress.Utils.PointFloat(808.0!, 0.0!)
            Me.XrLabel_DMP_FinanceCharge.Name = "XrLabel_DMP_FinanceCharge"
            Me.XrLabel_DMP_FinanceCharge.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_DMP_FinanceCharge.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_DMP_FinanceCharge.StylePriority.UsePadding = False
            Me.XrLabel_DMP_FinanceCharge.StylePriority.UseTextAlignment = False
            Me.XrLabel_DMP_FinanceCharge.Text = "$0.00"
            Me.XrLabel_DMP_FinanceCharge.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_DMP_FinanceCharge.WordWrap = False
            '
            'XrLabel_DMP_Fees
            '
            Me.XrLabel_DMP_Fees.CanGrow = False
            Me.XrLabel_DMP_Fees.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "DMP_Fees", "{0:c}")})
            Me.XrLabel_DMP_Fees.LocationFloat = New DevExpress.Utils.PointFloat(732.0!, 0.0!)
            Me.XrLabel_DMP_Fees.Name = "XrLabel_DMP_Fees"
            Me.XrLabel_DMP_Fees.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_DMP_Fees.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_DMP_Fees.StylePriority.UsePadding = False
            Me.XrLabel_DMP_Fees.StylePriority.UseTextAlignment = False
            Me.XrLabel_DMP_Fees.Text = "$0.00"
            Me.XrLabel_DMP_Fees.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_DMP_Fees.WordWrap = False
            '
            'XrLabel_DMP_InterestRate
            '
            Me.XrLabel_DMP_InterestRate.CanGrow = False
            Me.XrLabel_DMP_InterestRate.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "DMP_InterestRate", "{0:p3}")})
            Me.XrLabel_DMP_InterestRate.LocationFloat = New DevExpress.Utils.PointFloat(649.0!, 0.0!)
            Me.XrLabel_DMP_InterestRate.Name = "XrLabel_DMP_InterestRate"
            Me.XrLabel_DMP_InterestRate.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_DMP_InterestRate.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_DMP_InterestRate.StylePriority.UsePadding = False
            Me.XrLabel_DMP_InterestRate.StylePriority.UseTextAlignment = False
            Me.XrLabel_DMP_InterestRate.Text = "$0.00"
            Me.XrLabel_DMP_InterestRate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_DMP_InterestRate.WordWrap = False
            '
            'XrLabel_DMP_EnteredPayment
            '
            Me.XrLabel_DMP_EnteredPayment.CanGrow = False
            Me.XrLabel_DMP_EnteredPayment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "DMP_EnteredPayment", "{0:c}")})
            Me.XrLabel_DMP_EnteredPayment.LocationFloat = New DevExpress.Utils.PointFloat(566.0!, 0.0!)
            Me.XrLabel_DMP_EnteredPayment.Name = "XrLabel_DMP_EnteredPayment"
            Me.XrLabel_DMP_EnteredPayment.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_DMP_EnteredPayment.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_DMP_EnteredPayment.StylePriority.UsePadding = False
            Me.XrLabel_DMP_EnteredPayment.StylePriority.UseTextAlignment = False
            Me.XrLabel_DMP_EnteredPayment.Text = "$0.00"
            Me.XrLabel_DMP_EnteredPayment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_DMP_EnteredPayment.WordWrap = False
            '
            'XrLabel_DMP_MinimumPayment
            '
            Me.XrLabel_DMP_MinimumPayment.CanGrow = False
            Me.XrLabel_DMP_MinimumPayment.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "DMP_MinimumPayment", "{0:c}")})
            Me.XrLabel_DMP_MinimumPayment.LocationFloat = New DevExpress.Utils.PointFloat(483.0!, 0.0!)
            Me.XrLabel_DMP_MinimumPayment.Name = "XrLabel_DMP_MinimumPayment"
            Me.XrLabel_DMP_MinimumPayment.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_DMP_MinimumPayment.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_DMP_MinimumPayment.StylePriority.UsePadding = False
            Me.XrLabel_DMP_MinimumPayment.StylePriority.UseTextAlignment = False
            Me.XrLabel_DMP_MinimumPayment.Text = "$0.00"
            Me.XrLabel_DMP_MinimumPayment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_DMP_MinimumPayment.WordWrap = False
            '
            'XrLabel_DMP_MinimumProrate
            '
            Me.XrLabel_DMP_MinimumProrate.CanGrow = False
            Me.XrLabel_DMP_MinimumProrate.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "DMP_MinimumProrate", "{0:p3}")})
            Me.XrLabel_DMP_MinimumProrate.LocationFloat = New DevExpress.Utils.PointFloat(409.0!, 0.0!)
            Me.XrLabel_DMP_MinimumProrate.Name = "XrLabel_DMP_MinimumProrate"
            Me.XrLabel_DMP_MinimumProrate.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_DMP_MinimumProrate.SizeF = New System.Drawing.SizeF(66.0!, 17.0!)
            Me.XrLabel_DMP_MinimumProrate.StylePriority.UsePadding = False
            Me.XrLabel_DMP_MinimumProrate.StylePriority.UseTextAlignment = False
            Me.XrLabel_DMP_MinimumProrate.Text = "0.000 %"
            Me.XrLabel_DMP_MinimumProrate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_DMP_MinimumProrate.WordWrap = False
            '
            'XrLabel_Balance
            '
            Me.XrLabel_Balance.CanGrow = False
            Me.XrLabel_Balance.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Balance", "{0:c}")})
            Me.XrLabel_Balance.LocationFloat = New DevExpress.Utils.PointFloat(316.0!, 0.0!)
            Me.XrLabel_Balance.Name = "XrLabel_Balance"
            Me.XrLabel_Balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_Balance.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_Balance.StylePriority.UsePadding = False
            Me.XrLabel_Balance.StylePriority.UseTextAlignment = False
            Me.XrLabel_Balance.Text = "$0.00"
            Me.XrLabel_Balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_Balance.WordWrap = False
            '
            'XrLabel_Name
            '
            Me.XrLabel_Name.CanGrow = False
            Me.XrLabel_Name.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "Name")})
            Me.XrLabel_Name.LocationFloat = New DevExpress.Utils.PointFloat(91.0!, 0.0!)
            Me.XrLabel_Name.Name = "XrLabel_Name"
            Me.XrLabel_Name.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_Name.SizeF = New System.Drawing.SizeF(208.0!, 17.0!)
            Me.XrLabel_Name.StylePriority.UsePadding = False
            Me.XrLabel_Name.WordWrap = False
            '
            'XrLabel_CreditorType
            '
            Me.XrLabel_CreditorType.CanGrow = False
            Me.XrLabel_CreditorType.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "CreditorType")})
            Me.XrLabel_CreditorType.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 0.0!)
            Me.XrLabel_CreditorType.Name = "XrLabel_CreditorType"
            Me.XrLabel_CreditorType.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_CreditorType.SizeF = New System.Drawing.SizeF(66.0!, 17.0!)
            Me.XrLabel_CreditorType.StylePriority.UsePadding = False
            Me.XrLabel_CreditorType.WordWrap = False
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPanel1})
            Me.PageHeader.HeightF = 51.0!
            Me.PageHeader.Name = "PageHeader"
            Me.PageHeader.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
            '
            'XrPanel1
            '
            Me.XrPanel1.BackColor = System.Drawing.Color.Teal
            Me.XrPanel1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_11, Me.XrLabel_10, Me.XrLabel_8, Me.XrLabel_7, Me.XrLabel_6, Me.XrLabel_4, Me.XrLabel_2, Me.XrLabel_12, Me.XrLabel_9, Me.XrLabel_3, Me.XrLabel_15})
            Me.XrPanel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrPanel1.Name = "XrPanel1"
            Me.XrPanel1.SizeF = New System.Drawing.SizeF(1050.0!, 44.0!)
            Me.XrPanel1.StylePriority.UseBackColor = False
            '
            'XrLabel_11
            '
            Me.XrLabel_11.CanGrow = False
            Me.XrLabel_11.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_11.ForeColor = System.Drawing.Color.White
            Me.XrLabel_11.LocationFloat = New DevExpress.Utils.PointFloat(91.0!, 1.0!)
            Me.XrLabel_11.Multiline = True
            Me.XrLabel_11.Name = "XrLabel_11"
            Me.XrLabel_11.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_11.SizeF = New System.Drawing.SizeF(209.0!, 42.0!)
            Me.XrLabel_11.StylePriority.UseFont = False
            Me.XrLabel_11.StylePriority.UseForeColor = False
            Me.XrLabel_11.StylePriority.UsePadding = False
            Me.XrLabel_11.StylePriority.UseTextAlignment = False
            Me.XrLabel_11.Text = "NAME OF ACCOUNT"
            Me.XrLabel_11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
            '
            'XrLabel_10
            '
            Me.XrLabel_10.CanGrow = False
            Me.XrLabel_10.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_10.ForeColor = System.Drawing.Color.White
            Me.XrLabel_10.LocationFloat = New DevExpress.Utils.PointFloat(316.0!, 1.0!)
            Me.XrLabel_10.Multiline = True
            Me.XrLabel_10.Name = "XrLabel_10"
            Me.XrLabel_10.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_10.SizeF = New System.Drawing.SizeF(75.0!, 42.0!)
            Me.XrLabel_10.StylePriority.UseFont = False
            Me.XrLabel_10.StylePriority.UseForeColor = False
            Me.XrLabel_10.StylePriority.UsePadding = False
            Me.XrLabel_10.StylePriority.UseTextAlignment = False
            Me.XrLabel_10.Text = "" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "CURRENT" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "BALANCE"
            Me.XrLabel_10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'XrLabel_8
            '
            Me.XrLabel_8.CanGrow = False
            Me.XrLabel_8.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_8.ForeColor = System.Drawing.Color.White
            Me.XrLabel_8.LocationFloat = New DevExpress.Utils.PointFloat(400.0!, 1.0!)
            Me.XrLabel_8.Multiline = True
            Me.XrLabel_8.Name = "XrLabel_8"
            Me.XrLabel_8.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_8.SizeF = New System.Drawing.SizeF(75.0!, 42.0!)
            Me.XrLabel_8.StylePriority.UseFont = False
            Me.XrLabel_8.StylePriority.UseForeColor = False
            Me.XrLabel_8.StylePriority.UsePadding = False
            Me.XrLabel_8.StylePriority.UseTextAlignment = False
            Me.XrLabel_8.Text = "PLAN" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "MINIMUM" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "PRORATE"
            Me.XrLabel_8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'XrLabel_7
            '
            Me.XrLabel_7.CanGrow = False
            Me.XrLabel_7.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_7.ForeColor = System.Drawing.Color.White
            Me.XrLabel_7.LocationFloat = New DevExpress.Utils.PointFloat(483.0!, 0.0!)
            Me.XrLabel_7.Multiline = True
            Me.XrLabel_7.Name = "XrLabel_7"
            Me.XrLabel_7.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_7.SizeF = New System.Drawing.SizeF(75.0!, 42.0!)
            Me.XrLabel_7.StylePriority.UseFont = False
            Me.XrLabel_7.StylePriority.UseForeColor = False
            Me.XrLabel_7.StylePriority.UsePadding = False
            Me.XrLabel_7.StylePriority.UseTextAlignment = False
            Me.XrLabel_7.Text = "PLAN" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "MINIMUM" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "PAYMENT"
            Me.XrLabel_7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'XrLabel_6
            '
            Me.XrLabel_6.CanGrow = False
            Me.XrLabel_6.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_6.ForeColor = System.Drawing.Color.White
            Me.XrLabel_6.LocationFloat = New DevExpress.Utils.PointFloat(575.0!, 1.0!)
            Me.XrLabel_6.Multiline = True
            Me.XrLabel_6.Name = "XrLabel_6"
            Me.XrLabel_6.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_6.SizeF = New System.Drawing.SizeF(67.0!, 42.0!)
            Me.XrLabel_6.StylePriority.UseFont = False
            Me.XrLabel_6.StylePriority.UseForeColor = False
            Me.XrLabel_6.StylePriority.UsePadding = False
            Me.XrLabel_6.StylePriority.UseTextAlignment = False
            Me.XrLabel_6.Text = "" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "PLAN" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "PAYMENT"
            Me.XrLabel_6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'XrLabel_4
            '
            Me.XrLabel_4.CanGrow = False
            Me.XrLabel_4.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_4.ForeColor = System.Drawing.Color.White
            Me.XrLabel_4.LocationFloat = New DevExpress.Utils.PointFloat(741.0!, 1.0!)
            Me.XrLabel_4.Multiline = True
            Me.XrLabel_4.Name = "XrLabel_4"
            Me.XrLabel_4.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_4.SizeF = New System.Drawing.SizeF(66.0!, 42.0!)
            Me.XrLabel_4.StylePriority.UseFont = False
            Me.XrLabel_4.StylePriority.UseForeColor = False
            Me.XrLabel_4.StylePriority.UsePadding = False
            Me.XrLabel_4.StylePriority.UseTextAlignment = False
            Me.XrLabel_4.Text = "" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "TOTAL" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "FEES"
            Me.XrLabel_4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'XrLabel_2
            '
            Me.XrLabel_2.CanGrow = False
            Me.XrLabel_2.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_2.ForeColor = System.Drawing.Color.White
            Me.XrLabel_2.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 1.0!)
            Me.XrLabel_2.Multiline = True
            Me.XrLabel_2.Name = "XrLabel_2"
            Me.XrLabel_2.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_2.SizeF = New System.Drawing.SizeF(66.0!, 42.0!)
            Me.XrLabel_2.StylePriority.UseFont = False
            Me.XrLabel_2.StylePriority.UseForeColor = False
            Me.XrLabel_2.StylePriority.UsePadding = False
            Me.XrLabel_2.StylePriority.UseTextAlignment = False
            Me.XrLabel_2.Text = "TYPE OF" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "ACCOUNT"
            Me.XrLabel_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft
            '
            'XrLabel_12
            '
            Me.XrLabel_12.CanGrow = False
            Me.XrLabel_12.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_12.ForeColor = System.Drawing.Color.White
            Me.XrLabel_12.LocationFloat = New DevExpress.Utils.PointFloat(817.0!, 1.0!)
            Me.XrLabel_12.Multiline = True
            Me.XrLabel_12.Name = "XrLabel_12"
            Me.XrLabel_12.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_12.SizeF = New System.Drawing.SizeF(66.0!, 42.0!)
            Me.XrLabel_12.StylePriority.UseFont = False
            Me.XrLabel_12.StylePriority.UseForeColor = False
            Me.XrLabel_12.StylePriority.UsePadding = False
            Me.XrLabel_12.StylePriority.UseTextAlignment = False
            Me.XrLabel_12.Text = "" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "FINANCE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "CHARGE"
            Me.XrLabel_12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'XrLabel_9
            '
            Me.XrLabel_9.Borders = DevExpress.XtraPrinting.BorderSide.None
            Me.XrLabel_9.BorderWidth = 1
            Me.XrLabel_9.CanGrow = False
            Me.XrLabel_9.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_9.ForeColor = System.Drawing.Color.White
            Me.XrLabel_9.LocationFloat = New DevExpress.Utils.PointFloat(658.0!, 1.0!)
            Me.XrLabel_9.Multiline = True
            Me.XrLabel_9.Name = "XrLabel_9"
            Me.XrLabel_9.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_9.SizeF = New System.Drawing.SizeF(66.0!, 42.0!)
            Me.XrLabel_9.StylePriority.UseBorders = False
            Me.XrLabel_9.StylePriority.UseBorderWidth = False
            Me.XrLabel_9.StylePriority.UseFont = False
            Me.XrLabel_9.StylePriority.UseForeColor = False
            Me.XrLabel_9.StylePriority.UsePadding = False
            Me.XrLabel_9.StylePriority.UseTextAlignment = False
            Me.XrLabel_9.Text = "" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "INTEREST" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "RATE"
            Me.XrLabel_9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'XrLabel_3
            '
            Me.XrLabel_3.CanGrow = False
            Me.XrLabel_3.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_3.ForeColor = System.Drawing.Color.White
            Me.XrLabel_3.LocationFloat = New DevExpress.Utils.PointFloat(975.0!, 1.0!)
            Me.XrLabel_3.Multiline = True
            Me.XrLabel_3.Name = "XrLabel_3"
            Me.XrLabel_3.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_3.SizeF = New System.Drawing.SizeF(75.0!, 42.0!)
            Me.XrLabel_3.StylePriority.UseFont = False
            Me.XrLabel_3.StylePriority.UseForeColor = False
            Me.XrLabel_3.StylePriority.UsePadding = False
            Me.XrLabel_3.StylePriority.UseTextAlignment = False
            Me.XrLabel_3.Text = "$ GOING" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "TO" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "PRINCIPAL"
            Me.XrLabel_3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'XrLabel_15
            '
            Me.XrLabel_15.CanGrow = False
            Me.XrLabel_15.Font = New System.Drawing.Font("Times New Roman", 8.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_15.ForeColor = System.Drawing.Color.White
            Me.XrLabel_15.LocationFloat = New DevExpress.Utils.PointFloat(900.0!, 1.0!)
            Me.XrLabel_15.Multiline = True
            Me.XrLabel_15.Name = "XrLabel_15"
            Me.XrLabel_15.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_15.SizeF = New System.Drawing.SizeF(66.0!, 42.0!)
            Me.XrLabel_15.StylePriority.UseFont = False
            Me.XrLabel_15.StylePriority.UseForeColor = False
            Me.XrLabel_15.StylePriority.UsePadding = False
            Me.XrLabel_15.StylePriority.UseTextAlignment = False
            Me.XrLabel_15.Text = "TOTAL" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "INTEREST" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "& FEES"
            Me.XrLabel_15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_DMP_TotalGoingToPrincipal, Me.XrLine1, Me.XrLabel_27, Me.XrLabel_DMP_TotalFinanceCharge, Me.XrLabel_DMP_TotalFees, Me.XrLabel_DMP_TotalInterestRate, Me.XrLabel_DMP_TotalEnteredPayment, Me.XrLabel_DMP_TotalMinimumPayment, Me.XrLabel_DMP_TotalMinimumProrate, Me.XrLabel_Total_Balance, Me.XrLabel_DMP_TotalInterestFees})
            Me.ReportFooter.Font = New System.Drawing.Font("Times New Roman", 16.0!, System.Drawing.FontStyle.Bold)
            Me.ReportFooter.HeightF = 41.0!
            Me.ReportFooter.Name = "ReportFooter"
            Me.ReportFooter.StylePriority.UseFont = False
            '
            'XrLabel_DMP_TotalGoingToPrincipal
            '
            Me.XrLabel_DMP_TotalGoingToPrincipal.CanGrow = False
            Me.XrLabel_DMP_TotalGoingToPrincipal.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_DMP_TotalGoingToPrincipal.LocationFloat = New DevExpress.Utils.PointFloat(975.0!, 17.0!)
            Me.XrLabel_DMP_TotalGoingToPrincipal.Name = "XrLabel_DMP_TotalGoingToPrincipal"
            Me.XrLabel_DMP_TotalGoingToPrincipal.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_DMP_TotalGoingToPrincipal.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_DMP_TotalGoingToPrincipal.StylePriority.UseFont = False
            Me.XrLabel_DMP_TotalGoingToPrincipal.StylePriority.UsePadding = False
            Me.XrLabel_DMP_TotalGoingToPrincipal.StylePriority.UseTextAlignment = False
            Me.XrLabel_DMP_TotalGoingToPrincipal.Text = "$0.00"
            Me.XrLabel_DMP_TotalGoingToPrincipal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_DMP_TotalGoingToPrincipal.WordWrap = False
            '
            'XrLine1
            '
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 2.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLine1.SizeF = New System.Drawing.SizeF(1042.0!, 9.0!)
            Me.XrLine1.StylePriority.UsePadding = False
            '
            'XrLabel_27
            '
            Me.XrLabel_27.CanGrow = False
            Me.XrLabel_27.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_27.LocationFloat = New DevExpress.Utils.PointFloat(8.0!, 17.0!)
            Me.XrLabel_27.Name = "XrLabel_27"
            Me.XrLabel_27.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_27.SizeF = New System.Drawing.SizeF(208.0!, 17.0!)
            Me.XrLabel_27.StylePriority.UseFont = False
            Me.XrLabel_27.StylePriority.UsePadding = False
            Me.XrLabel_27.Text = "TOTALS"
            Me.XrLabel_27.WordWrap = False
            '
            'XrLabel_DMP_TotalFinanceCharge
            '
            Me.XrLabel_DMP_TotalFinanceCharge.CanGrow = False
            Me.XrLabel_DMP_TotalFinanceCharge.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_DMP_TotalFinanceCharge.LocationFloat = New DevExpress.Utils.PointFloat(808.0!, 17.0!)
            Me.XrLabel_DMP_TotalFinanceCharge.Name = "XrLabel_DMP_TotalFinanceCharge"
            Me.XrLabel_DMP_TotalFinanceCharge.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_DMP_TotalFinanceCharge.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_DMP_TotalFinanceCharge.StylePriority.UseFont = False
            Me.XrLabel_DMP_TotalFinanceCharge.StylePriority.UsePadding = False
            Me.XrLabel_DMP_TotalFinanceCharge.StylePriority.UseTextAlignment = False
            Me.XrLabel_DMP_TotalFinanceCharge.Text = "$0.00"
            Me.XrLabel_DMP_TotalFinanceCharge.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_DMP_TotalFinanceCharge.WordWrap = False
            '
            'XrLabel_DMP_TotalFees
            '
            Me.XrLabel_DMP_TotalFees.CanGrow = False
            Me.XrLabel_DMP_TotalFees.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_DMP_TotalFees.LocationFloat = New DevExpress.Utils.PointFloat(732.0!, 17.0!)
            Me.XrLabel_DMP_TotalFees.Name = "XrLabel_DMP_TotalFees"
            Me.XrLabel_DMP_TotalFees.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_DMP_TotalFees.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_DMP_TotalFees.StylePriority.UseFont = False
            Me.XrLabel_DMP_TotalFees.StylePriority.UsePadding = False
            Me.XrLabel_DMP_TotalFees.StylePriority.UseTextAlignment = False
            Me.XrLabel_DMP_TotalFees.Text = "$0.00"
            Me.XrLabel_DMP_TotalFees.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_DMP_TotalFees.WordWrap = False
            '
            'XrLabel_DMP_TotalInterestRate
            '
            Me.XrLabel_DMP_TotalInterestRate.CanGrow = False
            Me.XrLabel_DMP_TotalInterestRate.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_DMP_TotalInterestRate.LocationFloat = New DevExpress.Utils.PointFloat(649.0!, 17.0!)
            Me.XrLabel_DMP_TotalInterestRate.Name = "XrLabel_DMP_TotalInterestRate"
            Me.XrLabel_DMP_TotalInterestRate.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_DMP_TotalInterestRate.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_DMP_TotalInterestRate.StylePriority.UseFont = False
            Me.XrLabel_DMP_TotalInterestRate.StylePriority.UsePadding = False
            Me.XrLabel_DMP_TotalInterestRate.StylePriority.UseTextAlignment = False
            Me.XrLabel_DMP_TotalInterestRate.Text = "$0.00"
            Me.XrLabel_DMP_TotalInterestRate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_DMP_TotalInterestRate.WordWrap = False
            '
            'XrLabel_DMP_TotalEnteredPayment
            '
            Me.XrLabel_DMP_TotalEnteredPayment.CanGrow = False
            Me.XrLabel_DMP_TotalEnteredPayment.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_DMP_TotalEnteredPayment.LocationFloat = New DevExpress.Utils.PointFloat(566.0!, 17.0!)
            Me.XrLabel_DMP_TotalEnteredPayment.Name = "XrLabel_DMP_TotalEnteredPayment"
            Me.XrLabel_DMP_TotalEnteredPayment.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_DMP_TotalEnteredPayment.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_DMP_TotalEnteredPayment.StylePriority.UseFont = False
            Me.XrLabel_DMP_TotalEnteredPayment.StylePriority.UsePadding = False
            Me.XrLabel_DMP_TotalEnteredPayment.StylePriority.UseTextAlignment = False
            Me.XrLabel_DMP_TotalEnteredPayment.Text = "$0.00"
            Me.XrLabel_DMP_TotalEnteredPayment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_DMP_TotalEnteredPayment.WordWrap = False
            '
            'XrLabel_DMP_TotalMinimumPayment
            '
            Me.XrLabel_DMP_TotalMinimumPayment.CanGrow = False
            Me.XrLabel_DMP_TotalMinimumPayment.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_DMP_TotalMinimumPayment.LocationFloat = New DevExpress.Utils.PointFloat(483.0!, 17.0!)
            Me.XrLabel_DMP_TotalMinimumPayment.Name = "XrLabel_DMP_TotalMinimumPayment"
            Me.XrLabel_DMP_TotalMinimumPayment.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_DMP_TotalMinimumPayment.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_DMP_TotalMinimumPayment.StylePriority.UseFont = False
            Me.XrLabel_DMP_TotalMinimumPayment.StylePriority.UsePadding = False
            Me.XrLabel_DMP_TotalMinimumPayment.StylePriority.UseTextAlignment = False
            Me.XrLabel_DMP_TotalMinimumPayment.Text = "$0.00"
            Me.XrLabel_DMP_TotalMinimumPayment.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_DMP_TotalMinimumPayment.WordWrap = False
            '
            'XrLabel_DMP_TotalMinimumProrate
            '
            Me.XrLabel_DMP_TotalMinimumProrate.CanGrow = False
            Me.XrLabel_DMP_TotalMinimumProrate.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_DMP_TotalMinimumProrate.LocationFloat = New DevExpress.Utils.PointFloat(409.0!, 17.0!)
            Me.XrLabel_DMP_TotalMinimumProrate.Name = "XrLabel_DMP_TotalMinimumProrate"
            Me.XrLabel_DMP_TotalMinimumProrate.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_DMP_TotalMinimumProrate.SizeF = New System.Drawing.SizeF(66.0!, 17.0!)
            Me.XrLabel_DMP_TotalMinimumProrate.StylePriority.UseFont = False
            Me.XrLabel_DMP_TotalMinimumProrate.StylePriority.UsePadding = False
            Me.XrLabel_DMP_TotalMinimumProrate.StylePriority.UseTextAlignment = False
            Me.XrLabel_DMP_TotalMinimumProrate.Text = "0.000 %"
            Me.XrLabel_DMP_TotalMinimumProrate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_DMP_TotalMinimumProrate.Visible = False
            Me.XrLabel_DMP_TotalMinimumProrate.WordWrap = False
            '
            'XrLabel_Total_Balance
            '
            Me.XrLabel_Total_Balance.CanGrow = False
            Me.XrLabel_Total_Balance.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_Total_Balance.LocationFloat = New DevExpress.Utils.PointFloat(316.0!, 17.0!)
            Me.XrLabel_Total_Balance.Name = "XrLabel_Total_Balance"
            Me.XrLabel_Total_Balance.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_Total_Balance.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_Total_Balance.StylePriority.UseFont = False
            Me.XrLabel_Total_Balance.StylePriority.UsePadding = False
            Me.XrLabel_Total_Balance.StylePriority.UseTextAlignment = False
            Me.XrLabel_Total_Balance.Text = "$0.00"
            Me.XrLabel_Total_Balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_Total_Balance.WordWrap = False
            '
            'XrLabel_DMP_TotalInterestFees
            '
            Me.XrLabel_DMP_TotalInterestFees.CanGrow = False
            Me.XrLabel_DMP_TotalInterestFees.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_DMP_TotalInterestFees.LocationFloat = New DevExpress.Utils.PointFloat(891.0!, 17.0!)
            Me.XrLabel_DMP_TotalInterestFees.Name = "XrLabel_DMP_TotalInterestFees"
            Me.XrLabel_DMP_TotalInterestFees.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_DMP_TotalInterestFees.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_DMP_TotalInterestFees.StylePriority.UseFont = False
            Me.XrLabel_DMP_TotalInterestFees.StylePriority.UsePadding = False
            Me.XrLabel_DMP_TotalInterestFees.StylePriority.UseTextAlignment = False
            Me.XrLabel_DMP_TotalInterestFees.Text = "$0.00"
            Me.XrLabel_DMP_TotalInterestFees.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_DMP_TotalInterestFees.WordWrap = False
            '
            'ReportHeader
            '
            Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_1})
            Me.ReportHeader.HeightF = 52.0!
            Me.ReportHeader.Name = "ReportHeader"
            Me.ReportHeader.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand
            '
            'XrLabel_1
            '
            Me.XrLabel_1.Font = New System.Drawing.Font("Times New Roman", 16.0!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_1.Name = "XrLabel_1"
            Me.XrLabel_1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_1.SizeF = New System.Drawing.SizeF(1050.0!, 33.0!)
            Me.XrLabel_1.StylePriority.UseFont = False
            Me.XrLabel_1.StylePriority.UseTextAlignment = False
            Me.XrLabel_1.Text = "INFORMATION BASED UPON THE PLAN PAYMENTS"
            Me.XrLabel_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'XrLabel_17
            '
            Me.XrLabel_17.CanGrow = False
            Me.XrLabel_17.LocationFloat = New DevExpress.Utils.PointFloat(483.0!, 0.0!)
            Me.XrLabel_17.Name = "XrLabel_17"
            Me.XrLabel_17.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
            Me.XrLabel_17.SizeF = New System.Drawing.SizeF(75.0!, 17.0!)
            Me.XrLabel_17.StylePriority.UsePadding = False
            Me.XrLabel_17.StylePriority.UseTextAlignment = False
            Me.XrLabel_17.Text = "$0.00"
            Me.XrLabel_17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
            Me.XrLabel_17.WordWrap = False
            '
            'TopMargin
            '
            Me.TopMargin.HeightF = 25.0!
            Me.TopMargin.Name = "TopMargin"
            '
            'BottomMargin
            '
            Me.BottomMargin.HeightF = 25.0!
            Me.BottomMargin.Name = "BottomMargin"
            '
            'Comparison_Report_Plan
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.ReportFooter, Me.ReportHeader, Me.TopMargin, Me.BottomMargin})
            Me.Landscape = True
            Me.Margins = New System.Drawing.Printing.Margins(25, 25, 25, 25)
            Me.PageHeight = 850
            Me.PageWidth = 1100
            Me.Version = "10.1"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub
        Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
        Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
        Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
        Friend WithEvents XrLabel_1 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrPanel1 As DevExpress.XtraReports.UI.XRPanel
        Friend WithEvents XrLabel_2 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_3 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_4 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_6 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_7 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_8 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_9 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_10 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_11 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Name As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_CreditorType As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DMP_InterestFees As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DMP_FinanceCharge As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DMP_Fees As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DMP_InterestRate As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DMP_EnteredPayment As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DMP_MinimumPayment As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DMP_MinimumProrate As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Friend WithEvents XrLabel_27 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DMP_TotalFinanceCharge As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DMP_TotalFees As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DMP_TotalInterestRate As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DMP_TotalEnteredPayment As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DMP_TotalMinimumPayment As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DMP_TotalMinimumProrate As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_Total_Balance As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DMP_TotalInterestFees As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_12 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_15 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DMP_GoingToPrincipal As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_DMP_TotalGoingToPrincipal As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents XrLabel_17 As DevExpress.XtraReports.UI.XRLabel
        Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
        Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
    End Class
End Namespace