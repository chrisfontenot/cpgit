#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.ComponentModel
Imports DebtPlus.Reports.Template
Imports DebtPlus.UI.Client.Service
Imports System.Drawing.Printing
Imports DevExpress.XtraReports.UI
Imports DebtPlus.Utils.Format
Imports DebtPlus.LINQ
Imports System.Linq

Namespace forms.Comparison.Report

    Friend Class Comparison_Report
        Inherits BaseXtraReportClass

        Private ReadOnly Property SalesInfo() As SalesFileInfoComparisonClass
            Get
                Return CType(Tag, SalesFileInfoComparisonClass)
            End Get
        End Property

        Public Sub New()
            MyClass.New(CType(Nothing, SalesFileInfoComparisonClass))
        End Sub

        Public Sub New(ByVal SalesInfo As SalesFileInfoComparisonClass)
            MyBase.New()
            InitializeComponent()
            MyClass.Tag = SalesInfo

            AddHandler XrLabel_AgencyName.BeforePrint, AddressOf XrLabel_AgencyName_BeforePrint
            AddHandler XrLabel_ClientID.BeforePrint, AddressOf XrLabel_ClientID_BeforePrint
            AddHandler XrLabel_ClientName.BeforePrint, AddressOf XrLabel_ClientName_BeforePrint
            AddHandler XrLabel_DateCreated.BeforePrint, AddressOf XrLabel_DateCreated_BeforePrint
            AddHandler XrSubreport_Client.BeforePrint, AddressOf XrSubreport_Client_BeforePrint
            AddHandler XrSubreport_Plan.BeforePrint, AddressOf XrSubreport_Plan_BeforePrint
            AddHandler XrSubreport_Summary.BeforePrint, AddressOf XrSubreport_Summary_BeforePrint
        End Sub

        ''' <summary>
        ''' Handle the client ID before it is printed
        ''' </summary>
        Private Sub XrLabel_ClientID_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim lbl As XRLabel = CType(sender, XRLabel)
            Dim rpt As XtraReport = CType(lbl.Report, XtraReport)
            Dim SalesFileInfo As SalesFileInfoComparisonClass = CType(rpt.Tag, SalesFileInfoComparisonClass)
            Dim clientID As Int32 = SalesInfo.Context.ClientId
            lbl.Text = String.Format("{0:0000000}", clientID)
        End Sub

        ''' <summary>
        ''' Handle the client name before it is printed
        ''' </summary>
        Private Sub XrLabel_ClientName_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim lbl As XRLabel = CType(sender, XRLabel)
            Dim rpt As XtraReport = CType(lbl.Report, XtraReport)
            Dim SalesFileInfo As SalesFileInfoComparisonClass = CType(rpt.Tag, SalesFileInfoComparisonClass)
            Dim clientID As Int32 = SalesInfo.Context.ClientId

            If clientID >= 0 Then
                Using bc As New BusinessContext()
                    Dim n As DebtPlus.LINQ.Name = bc.peoples.Join(bc.Names, Function(p) p.NameID, Function(nam) nam.Id, Function(p, nam) New With {.client = p.Client, .relation = p.Relation, .name = nam}).Where(Function(x) x.client = clientID AndAlso x.relation = DebtPlus.LINQ.Cache.RelationType.Self).Select(Function(x) x.name).FirstOrDefault()
                    If n IsNot Nothing Then
                        lbl.Text = n.ToString()
                        Return
                    End If
                End Using
            End If

            lbl.Text = String.Empty
        End Sub

        ''' <summary>
        ''' Handle the creation date for the comparison data before it is printed
        ''' </summary>
        Private Sub XrLabel_DateCreated_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim lbl As XRLabel = CType(sender, XRLabel)
            Dim rpt As XtraReport = CType(lbl.Report, XtraReport)
            Dim SalesFileInfo As SalesFileInfoComparisonClass = CType(rpt.Tag, SalesFileInfoComparisonClass)

            lbl.Text = String.Format("{0:MMMM d, yyyy}", SalesInfo.DateCreated)
        End Sub

        ''' <summary>
        ''' Handle the agency name before it is printed
        ''' </summary>
        Private Sub XrLabel_AgencyName_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim lbl As XRLabel = CType(sender, XRLabel)

            Dim cnf As config = DebtPlus.LINQ.Cache.config.getList().FirstOrDefault()
            If cnf IsNot Nothing Then
                lbl.Text = cnf.name
                Return
            End If

            lbl.Text = String.Empty
        End Sub

        ''' <summary>
        ''' Handle the client information before it is printed
        ''' </summary>
        Private Sub XrSubreport_Client_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim SubReport As XRSubreport = CType(sender, XRSubreport)
            Dim MasterRpt As XtraReport = CType(SubReport.Report, XtraReport)
            Dim SalesFileInfo As SalesFileInfoComparisonClass = CType(MasterRpt.Tag, SalesFileInfoComparisonClass)
            Dim rpt As XtraReport = CType(SubReport.ReportSource, XtraReport)
            With rpt
                .DataSource = SalesFileInfo.DebtList
                For Each calc As CalculatedField In rpt.CalculatedFields
                    calc.Assign(rpt.DataSource, rpt.DataMember)
                Next
            End With
        End Sub

        ''' <summary>
        ''' Handle the DMP Plan sub-report before it is printed
        ''' </summary>
        Private Sub XrSubreport_Plan_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim SubReport As XRSubreport = CType(sender, XRSubreport)
            Dim MasterRpt As XtraReport = CType(SubReport.Report, XtraReport)
            Dim SalesFileInfo As SalesFileInfoComparisonClass = CType(MasterRpt.Tag, SalesFileInfoComparisonClass)
            Dim rpt As XtraReport = CType(SubReport.ReportSource, XtraReport)
            With rpt
                .DataSource = SalesFileInfo.DebtList

                For Each calc As CalculatedField In rpt.CalculatedFields
                    calc.Assign(rpt.DataSource, rpt.DataMember)
                Next
            End With
        End Sub

        ''' <summary>
        ''' Handle the summary sub-report before it is printed
        ''' </summary>
        Private Sub XrSubreport_Summary_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim SubReport As XRSubreport = CType(sender, XRSubreport)
            Dim MasterRpt As XtraReport = CType(SubReport.Report, XtraReport)
            Dim rpt As XtraReport = CType(SubReport.ReportSource, XtraReport)
            Dim SalesFileInfo As SalesFileInfoComparisonClass = CType(MasterRpt.Tag, SalesFileInfoComparisonClass)

            ' Turn the single structure into an IList object for the datasource
            Dim lst As New ArrayList
            lst.Add(SalesFileInfo)
            rpt.DataSource = lst
        End Sub

    End Class
End Namespace
