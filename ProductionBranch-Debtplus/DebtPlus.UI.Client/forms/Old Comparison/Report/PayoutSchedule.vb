Imports DebtPlus.Reports.Template

Namespace forms.Comparison.Report
    Public Class PayoutSchedule
        Inherits TemplateXtraReportClass

        Private privateTitle As String = String.Empty

        Public Sub SetTitle(ByVal Title As String)
            privateTitle = Title
        End Sub

        Public Overrides ReadOnly Property ReportTitle As String
            Get
                Return privateTitle
            End Get
        End Property

    End Class
End Namespace
