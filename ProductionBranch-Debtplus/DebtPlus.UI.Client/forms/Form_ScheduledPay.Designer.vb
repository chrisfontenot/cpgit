﻿Namespace forms
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form_ScheduledPay
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then
                        components.Dispose()
                    End If
                End If

            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.SimpleButton_clear = New DevExpress.XtraEditors.SimpleButton()
            Me.SimpleButton_reset = New DevExpress.XtraEditors.SimpleButton()
            Me.SimpleButton_ok = New DevExpress.XtraEditors.SimpleButton()
            Me.SimpleButton_cancel = New DevExpress.XtraEditors.SimpleButton()
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
            Me.GridColumn_IsActive = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_creditor = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_name = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_account_number = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_orig_payment = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_sched_payment = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.RepositoryItemCalcEdit_SchedPay = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit()
            Me.GridColumn_disbursement_factor = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_balance = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_this_month = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_last_month = New DevExpress.XtraGrid.Columns.GridColumn()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RepositoryItemCalcEdit_SchedPay, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'SimpleButton_clear
            '
            Me.SimpleButton_clear.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton_clear.Location = New System.Drawing.Point(13, 251)
            Me.SimpleButton_clear.Name = "SimpleButton_clear"
            Me.SimpleButton_clear.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_clear.TabIndex = 1
            Me.SimpleButton_clear.Text = "Clear"
            '
            'SimpleButton_reset
            '
            Me.SimpleButton_reset.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton_reset.Location = New System.Drawing.Point(94, 251)
            Me.SimpleButton_reset.Name = "SimpleButton_reset"
            Me.SimpleButton_reset.Size = New System.Drawing.Size(111, 23)
            Me.SimpleButton_reset.TabIndex = 2
            Me.SimpleButton_reset.Text = "Disbursement Factor"
            '
            'SimpleButton_ok
            '
            Me.SimpleButton_ok.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton_ok.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.SimpleButton_ok.Location = New System.Drawing.Point(399, 251)
            Me.SimpleButton_ok.Name = "SimpleButton_ok"
            Me.SimpleButton_ok.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_ok.TabIndex = 3
            Me.SimpleButton_ok.Text = "&OK"
            '
            'SimpleButton_cancel
            '
            Me.SimpleButton_cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.SimpleButton_cancel.Location = New System.Drawing.Point(480, 251)
            Me.SimpleButton_cancel.Name = "SimpleButton_cancel"
            Me.SimpleButton_cancel.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_cancel.TabIndex = 4
            Me.SimpleButton_cancel.Text = "&Cancel"
            '
            'GridControl1
            '
            Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                                             Or System.Windows.Forms.AnchorStyles.Left) _
                                            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GridControl1.Location = New System.Drawing.Point(13, 13)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCalcEdit_SchedPay})
            Me.GridControl1.Size = New System.Drawing.Size(542, 232)
            Me.GridControl1.TabIndex = 0
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.ActiveFilterString = "([current_balance] > 0.0 Or [ccl_zero_balance] <> False) And [IsActive] <> False"
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_IsActive, Me.GridColumn_creditor, Me.GridColumn_name, Me.GridColumn_account_number, Me.GridColumn_orig_payment, Me.GridColumn_sched_payment, Me.GridColumn_disbursement_factor, Me.GridColumn_balance, Me.GridColumn_this_month, Me.GridColumn_last_month})
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsView.ShowFooter = True
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            '
            'GridColumn_IsActive
            '
            Me.GridColumn_IsActive.Caption = "IsActive"
            Me.GridColumn_IsActive.FieldName = "IsActive"
            Me.GridColumn_IsActive.Name = "GridColumn_IsActive"
            Me.GridColumn_IsActive.OptionsColumn.AllowEdit = False
            Me.GridColumn_IsActive.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_IsActive.Width = 20
            '
            'GridColumn_creditor
            '
            Me.GridColumn_creditor.Caption = "Creditor"
            Me.GridColumn_creditor.FieldName = "creditor"
            Me.GridColumn_creditor.Name = "GridColumn_creditor"
            Me.GridColumn_creditor.OptionsColumn.AllowEdit = False
            Me.GridColumn_creditor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_creditor.Visible = True
            Me.GridColumn_creditor.VisibleIndex = 0
            Me.GridColumn_creditor.Width = 68
            '
            'GridColumn_name
            '
            Me.GridColumn_name.Caption = "Name"
            Me.GridColumn_name.FieldName = "display_creditor_name"
            Me.GridColumn_name.Name = "GridColumn_name"
            Me.GridColumn_name.OptionsColumn.AllowEdit = False
            Me.GridColumn_name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_name.Visible = True
            Me.GridColumn_name.VisibleIndex = 1
            Me.GridColumn_name.Width = 68
            '
            'GridColumn_account_number
            '
            Me.GridColumn_account_number.Caption = "Acct #"
            Me.GridColumn_account_number.FieldName = "display_account_number"
            Me.GridColumn_account_number.Name = "GridColumn_account_number"
            Me.GridColumn_account_number.OptionsColumn.AllowEdit = False
            Me.GridColumn_account_number.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_account_number.Visible = True
            Me.GridColumn_account_number.VisibleIndex = 2
            Me.GridColumn_account_number.Width = 65
            '
            'GridColumn_orig_payment
            '
            Me.GridColumn_orig_payment.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_orig_payment.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_orig_payment.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_orig_payment.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_orig_payment.Caption = "Orig Pmt"
            Me.GridColumn_orig_payment.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_orig_payment.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_orig_payment.FieldName = "orig_dmp_payment"
            Me.GridColumn_orig_payment.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_orig_payment.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_orig_payment.Name = "GridColumn_orig_payment"
            Me.GridColumn_orig_payment.OptionsColumn.AllowEdit = False
            Me.GridColumn_orig_payment.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_orig_payment.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "orig_dmp_payment", "{0:c}")})
            Me.GridColumn_orig_payment.Visible = True
            Me.GridColumn_orig_payment.VisibleIndex = 3
            Me.GridColumn_orig_payment.Width = 68
            '
            'GridColumn_sched_payment
            '
            Me.GridColumn_sched_payment.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_sched_payment.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_sched_payment.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_sched_payment.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_sched_payment.Caption = "Sched Pay"
            Me.GridColumn_sched_payment.ColumnEdit = Me.RepositoryItemCalcEdit_SchedPay
            Me.GridColumn_sched_payment.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_sched_payment.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_sched_payment.FieldName = "sched_payment"
            Me.GridColumn_sched_payment.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_sched_payment.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_sched_payment.Name = "GridColumn_sched_payment"
            Me.GridColumn_sched_payment.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_sched_payment.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sched_payment", "{0:c}")})
            Me.GridColumn_sched_payment.Visible = True
            Me.GridColumn_sched_payment.VisibleIndex = 5
            Me.GridColumn_sched_payment.Width = 68
            '
            'RepositoryItemCalcEdit_SchedPay
            '
            Me.RepositoryItemCalcEdit_SchedPay.Appearance.Options.UseTextOptions = True
            Me.RepositoryItemCalcEdit_SchedPay.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.RepositoryItemCalcEdit_SchedPay.AppearanceDisabled.Options.UseTextOptions = True
            Me.RepositoryItemCalcEdit_SchedPay.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.RepositoryItemCalcEdit_SchedPay.AppearanceDropDown.Options.UseTextOptions = True
            Me.RepositoryItemCalcEdit_SchedPay.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.RepositoryItemCalcEdit_SchedPay.AppearanceFocused.Options.UseTextOptions = True
            Me.RepositoryItemCalcEdit_SchedPay.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.RepositoryItemCalcEdit_SchedPay.AppearanceReadOnly.Options.UseTextOptions = True
            Me.RepositoryItemCalcEdit_SchedPay.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.RepositoryItemCalcEdit_SchedPay.AutoHeight = False
            Me.RepositoryItemCalcEdit_SchedPay.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.RepositoryItemCalcEdit_SchedPay.DisplayFormat.FormatString = "{0:c}"
            Me.RepositoryItemCalcEdit_SchedPay.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.RepositoryItemCalcEdit_SchedPay.EditFormat.FormatString = "{0:c}"
            Me.RepositoryItemCalcEdit_SchedPay.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.RepositoryItemCalcEdit_SchedPay.Mask.BeepOnError = True
            Me.RepositoryItemCalcEdit_SchedPay.Mask.EditMask = "c"
            Me.RepositoryItemCalcEdit_SchedPay.Name = "RepositoryItemCalcEdit_SchedPay"
            Me.RepositoryItemCalcEdit_SchedPay.Precision = 2
            '
            'GridColumn_disbursement_factor
            '
            Me.GridColumn_disbursement_factor.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_disbursement_factor.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_disbursement_factor.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_disbursement_factor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_disbursement_factor.Caption = "Disb Factor"
            Me.GridColumn_disbursement_factor.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_disbursement_factor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_disbursement_factor.FieldName = "display_disbursement_factor"
            Me.GridColumn_disbursement_factor.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_disbursement_factor.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_disbursement_factor.Name = "GridColumn_disbursement_factor"
            Me.GridColumn_disbursement_factor.OptionsColumn.AllowEdit = False
            Me.GridColumn_disbursement_factor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_disbursement_factor.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "display_disbursement_factor", "{0:c}")})
            Me.GridColumn_disbursement_factor.Visible = True
            Me.GridColumn_disbursement_factor.VisibleIndex = 4
            Me.GridColumn_disbursement_factor.Width = 68
            '
            'GridColumn_balance
            '
            Me.GridColumn_balance.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_balance.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_balance.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_balance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_balance.Caption = "Balance"
            Me.GridColumn_balance.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_balance.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_balance.FieldName = "display_current_balance"
            Me.GridColumn_balance.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_balance.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_balance.Name = "GridColumn_balance"
            Me.GridColumn_balance.OptionsColumn.AllowEdit = False
            Me.GridColumn_balance.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_balance.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "display_current_balance", "{0:c}")})
            Me.GridColumn_balance.Visible = True
            Me.GridColumn_balance.VisibleIndex = 6
            Me.GridColumn_balance.Width = 68
            '
            'GridColumn_this_month
            '
            Me.GridColumn_this_month.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_this_month.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_this_month.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_this_month.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_this_month.Caption = "Curr Mo Disb"
            Me.GridColumn_this_month.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_this_month.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_this_month.FieldName = "payments_month_0"
            Me.GridColumn_this_month.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_this_month.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_this_month.Name = "GridColumn_this_month"
            Me.GridColumn_this_month.OptionsColumn.AllowEdit = False
            Me.GridColumn_this_month.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_this_month.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "payments_month_0", "{0:c}")})
            Me.GridColumn_this_month.Visible = True
            Me.GridColumn_this_month.VisibleIndex = 7
            Me.GridColumn_this_month.Width = 68
            '
            'GridColumn_last_month
            '
            Me.GridColumn_last_month.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_last_month.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_last_month.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_last_month.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_last_month.Caption = "Last Mo Disb"
            Me.GridColumn_last_month.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_last_month.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_last_month.FieldName = "payments_month_1"
            Me.GridColumn_last_month.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_last_month.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_last_month.Name = "GridColumn_last_month"
            Me.GridColumn_last_month.OptionsColumn.AllowEdit = False
            Me.GridColumn_last_month.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_last_month.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "payments_month_1", "{0:c}")})
            Me.GridColumn_last_month.Visible = True
            Me.GridColumn_last_month.VisibleIndex = 8
            '
            'Form_ScheduledPay
            '
            Me.AcceptButton = Me.SimpleButton_ok
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.SimpleButton_cancel
            Me.ClientSize = New System.Drawing.Size(567, 286)
            Me.Controls.Add(Me.GridControl1)
            Me.Controls.Add(Me.SimpleButton_cancel)
            Me.Controls.Add(Me.SimpleButton_ok)
            Me.Controls.Add(Me.SimpleButton_reset)
            Me.Controls.Add(Me.SimpleButton_clear)
            Me.Name = "Form_ScheduledPay"
            Me.Text = "Update Scheduled Pay"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RepositoryItemCalcEdit_SchedPay, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents SimpleButton_clear As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton_reset As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton_ok As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton_cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents GridColumn_creditor As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_name As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_account_number As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_orig_payment As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_sched_payment As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_IsActive As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_disbursement_factor As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_balance As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_this_month As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_last_month As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents RepositoryItemCalcEdit_SchedPay As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    End Class
End Namespace