﻿Imports System.Linq
Imports DebtPlus.LINQ
Imports DebtPlus.Data.Controls
Imports DebtPlus.Data.Forms
Imports DebtPlus.UI.Client.Service
Imports DebtPlus.UI.Common
Imports DebtPlus.Utils

Namespace forms
    Friend Class Form_Product
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Private record As DebtPlus.LINQ.client_product = Nothing
        Private bc As DebtPlus.LINQ.BusinessContext = Nothing

        Public Sub New(bc As DebtPlus.LINQ.BusinessContext, record As DebtPlus.LINQ.client_product)
            MyClass.New()
            Me.record = record
            Me.bc = bc
            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Me.Load, AddressOf Form_Product_Load
            AddHandler LayoutControl1.Resize, AddressOf LayoutControl1_Resize
            AddHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
            AddHandler BarButtonItem_FileExit.ItemClick, AddressOf BarButtonItem_FileExit_ItemClick
            AddHandler BarButtonItemViewAccount.ItemClick, AddressOf BarButtonItemViewAccount_ItemClick
            AddHandler BarButtonItemViewNotes.ItemClick, AddressOf BarButtonItemViewNotes_ItemClick
            AddHandler BarCheckItemViewTransactions.ItemClick, AddressOf BarCheckItemViewTransactions_ItemClick
            AddHandler BarSubItem_Menu_View.Popup, AddressOf BarSubItem_Menu_View_Popup
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler Me.Load, AddressOf Form_Product_Load
            RemoveHandler LayoutControl1.Resize, AddressOf LayoutControl1_Resize
            RemoveHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
        End Sub

        Private Sub Form_Product_Load(sender As Object, e As System.EventArgs)
            UnRegisterHandlers()
            Try
                ' Load the controls with the current record information
                ProductsHeader1.ReadForm(bc, record)
                ProductsNotes1.ReadForm(bc, record)
                ProductsAccount1.ReadForm(bc, record)
                ProductsTransacitons1.ReadForm(bc, record)

                EmptySpaceItem_Left.Width = (EmptySpaceItem_Left.Width + EmptySpaceItem_Right.Width) \ 2
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Sub LayoutControl1_Resize(sender As Object, e As System.EventArgs)
            UnRegisterHandlers()
            Try
                EmptySpaceItem_Left.Width = (EmptySpaceItem_Left.Width + EmptySpaceItem_Right.Width) \ 2
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Sub SimpleButton_OK_Click(sender As Object, e As System.EventArgs)
            UnRegisterHandlers()
            Try
                ProductsHeader1.SaveForm()
                ProductsNotes1.SaveForm()
                ProductsAccount1.SaveForm()
                ProductsTransacitons1.SaveForm()

                DialogResult = Windows.Forms.DialogResult.OK
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Sub BarButtonItem_FileExit_ItemClick(sender As Object, e As System.EventArgs)
            DialogResult = Windows.Forms.DialogResult.Cancel
            If Not Modal Then
                Close()
            End If
        End Sub

        Private Sub BarCheckItemViewTransactions_ItemClick(sender As Object, e As System.EventArgs)
            XtraTabControl1.SelectedTabPage = XtraTabPage_Transactions
        End Sub

        Private Sub BarButtonItemViewAccount_ItemClick(sender As Object, e As System.EventArgs)
            XtraTabControl1.SelectedTabPage = XtraTabPage_Account
        End Sub

        Private Sub BarButtonItemViewNotes_ItemClick(sender As Object, e As System.EventArgs)
            XtraTabControl1.SelectedTabPage = XtraTabPage_Notes
        End Sub

        Private Sub BarSubItem_Menu_View_Popup(sender As Object, e As System.EventArgs)
            BarButtonItemViewAccount.Checked = (XtraTabControl1.SelectedTabPage Is XtraTabPage_Account)
            BarButtonItemViewNotes.Checked = (XtraTabControl1.SelectedTabPage Is XtraTabPage_Notes)
            BarCheckItemViewTransactions.Checked = (XtraTabControl1.SelectedTabPage Is XtraTabPage_Transactions)
        End Sub
    End Class
End Namespace