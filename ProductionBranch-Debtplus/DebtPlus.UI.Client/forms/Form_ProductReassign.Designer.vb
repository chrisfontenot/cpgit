﻿Namespace forms
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form_ProductReassign

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.SimpleButton_OK = New DevExpress.XtraEditors.SimpleButton()
            Me.SimpleButton_Cancel = New DevExpress.XtraEditors.SimpleButton()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
            Me.LookUpEdit_Vendor = New DevExpress.XtraEditors.LookUpEdit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_Vendor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'DefaultLookAndFeel1
            '
            Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin"
            '
            'SimpleButton_OK
            '
            Me.SimpleButton_OK.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton_OK.Location = New System.Drawing.Point(218, 12)
            Me.SimpleButton_OK.Name = "SimpleButton_OK"
            Me.SimpleButton_OK.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.TabIndex = 5
            Me.SimpleButton_OK.Text = "&OK"
            '
            'SimpleButton_Cancel
            '
            Me.SimpleButton_Cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.SimpleButton_Cancel.Location = New System.Drawing.Point(218, 41)
            Me.SimpleButton_Cancel.Name = "SimpleButton_Cancel"
            Me.SimpleButton_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.TabIndex = 6
            Me.SimpleButton_Cancel.Text = "&Cancel"
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(12, 22)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(58, 13)
            Me.LabelControl1.TabIndex = 7
            Me.LabelControl1.Text = "New Vendor"
            '
            'LookUpEdit_Vendor
            '
            Me.LookUpEdit_Vendor.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LookUpEdit_Vendor.Location = New System.Drawing.Point(78, 20)
            Me.LookUpEdit_Vendor.Name = "LookUpEdit_Vendor"
            Me.LookUpEdit_Vendor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_Vendor.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.LookUpEdit_Vendor.Properties.DisplayMember = "Name"
            Me.LookUpEdit_Vendor.Properties.NullText = ""
            Me.LookUpEdit_Vendor.Properties.ShowFooter = False
            Me.LookUpEdit_Vendor.Properties.ShowHeader = False
            Me.LookUpEdit_Vendor.Properties.SortColumnIndex = 1
            Me.LookUpEdit_Vendor.Properties.ValueMember = "Id"
            Me.LookUpEdit_Vendor.Size = New System.Drawing.Size(134, 20)
            Me.LookUpEdit_Vendor.TabIndex = 8
            '
            'Form_ProductReassign
            '
            Me.AcceptButton = Me.SimpleButton_OK
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.SimpleButton_Cancel
            Me.ClientSize = New System.Drawing.Size(305, 96)
            Me.Controls.Add(Me.LookUpEdit_Vendor)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.SimpleButton_Cancel)
            Me.Controls.Add(Me.SimpleButton_OK)
            Me.MaximizeBox = False
            Me.MinimizeBox = False
            Me.Name = "Form_ProductReassign"
            Me.Text = "Product Reassignment"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_Vendor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents SimpleButton_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LookUpEdit_Vendor As DevExpress.XtraEditors.LookUpEdit
    End Class
End Namespace