﻿Namespace forms
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form_ThresholdAdjust
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.ComboBoxEdit1 = New DevExpress.XtraEditors.ComboBoxEdit
            Me.LabelControl_current = New DevExpress.XtraEditors.LabelControl
            Me.SimpleButton_OK = New DevExpress.XtraEditors.SimpleButton
            Me.SimpleButton_Cancel = New DevExpress.XtraEditors.SimpleButton
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                             Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl1.Appearance.Options.UseTextOptions = True
            Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl1.Location = New System.Drawing.Point(13, 13)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(267, 33)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Enter the new threshold value for future creditor(s) added to this client."
            Me.LabelControl1.UseMnemonic = False
            '
            'ComboBoxEdit1
            '
            Me.ComboBoxEdit1.Anchor = System.Windows.Forms.AnchorStyles.Top
            Me.ComboBoxEdit1.Location = New System.Drawing.Point(96, 53)
            Me.ComboBoxEdit1.Name = "ComboBoxEdit1"
            Me.ComboBoxEdit1.Properties.Appearance.Options.UseTextOptions = True
            Me.ComboBoxEdit1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.ComboBoxEdit1.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.ComboBoxEdit1.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.ComboBoxEdit1.Properties.AppearanceDropDown.Options.UseTextOptions = True
            Me.ComboBoxEdit1.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.ComboBoxEdit1.Properties.AppearanceFocused.Options.UseTextOptions = True
            Me.ComboBoxEdit1.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.ComboBoxEdit1.Properties.AppearanceReadOnly.Options.UseTextOptions = True
            Me.ComboBoxEdit1.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.ComboBoxEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit1.Size = New System.Drawing.Size(100, 20)
            Me.ComboBoxEdit1.TabIndex = 1
            '
            'LabelControl_current
            '
            Me.LabelControl_current.Appearance.Options.UseTextOptions = True
            Me.LabelControl_current.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.LabelControl_current.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.LabelControl_current.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_current.Location = New System.Drawing.Point(13, 79)
            Me.LabelControl_current.Name = "LabelControl_current"
            Me.LabelControl_current.Size = New System.Drawing.Size(267, 13)
            Me.LabelControl_current.TabIndex = 2
            Me.LabelControl_current.Text = "The current value is 2.1%"
            Me.LabelControl_current.UseMnemonic = False
            '
            'SimpleButton_OK
            '
            Me.SimpleButton_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.SimpleButton_OK.Location = New System.Drawing.Point(55, 107)
            Me.SimpleButton_OK.Name = "SimpleButton_OK"
            Me.SimpleButton_OK.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.TabIndex = 3
            Me.SimpleButton_OK.Text = "&OK"
            '
            'SimpleButton_Cancel
            '
            Me.SimpleButton_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.SimpleButton_Cancel.Location = New System.Drawing.Point(163, 107)
            Me.SimpleButton_Cancel.Name = "SimpleButton_Cancel"
            Me.SimpleButton_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.TabIndex = 4
            Me.SimpleButton_Cancel.Text = "&Cancel"
            '
            'Form_ThresholdAdjust
            '
            Me.AcceptButton = Me.SimpleButton_OK
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.SimpleButton_Cancel
            Me.ClientSize = New System.Drawing.Size(292, 145)
            Me.Controls.Add(Me.SimpleButton_Cancel)
            Me.Controls.Add(Me.SimpleButton_OK)
            Me.Controls.Add(Me.LabelControl_current)
            Me.Controls.Add(Me.ComboBoxEdit1)
            Me.Controls.Add(Me.LabelControl1)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
            Me.MaximizeBox = False
            Me.Name = "Form_ThresholdAdjust"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "New Debt Threshold Factor"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents ComboBoxEdit1 As DevExpress.XtraEditors.ComboBoxEdit
        Friend WithEvents LabelControl_current As DevExpress.XtraEditors.LabelControl
        Friend WithEvents SimpleButton_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton_Cancel As DevExpress.XtraEditors.SimpleButton
    End Class
End Namespace