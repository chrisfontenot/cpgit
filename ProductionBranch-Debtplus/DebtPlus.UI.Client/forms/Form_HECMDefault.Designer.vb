﻿Namespace forms
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form_HECMDefault
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.SimpleButton_OK = New DevExpress.XtraEditors.SimpleButton()
            Me.SimpleButton_Cancel = New DevExpress.XtraEditors.SimpleButton()
            Me.lu_Option = New DevExpress.XtraEditors.LookUpEdit()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
            Me.lu_checkup = New DevExpress.XtraEditors.LookUpEdit()
            Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
            Me.lu_Tier = New DevExpress.XtraEditors.LookUpEdit()
            Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
            Me.lu_bdt_bec = New DevExpress.XtraEditors.LookUpEdit()
            Me.dt_Hardest_Hit_Ref_Date = New DevExpress.XtraEditors.DateEdit()
            Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
            Me.se_Ref_Food = New DevExpress.XtraEditors.SpinEdit()
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
            Me.calcEdit_Savings_Food = New DevExpress.XtraEditors.CalcEdit()
            Me.calcEdit_Savings_Med = New DevExpress.XtraEditors.CalcEdit()
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
            Me.se_Ref_Med = New DevExpress.XtraEditors.SpinEdit()
            Me.calcEdit_Savings_Prescription = New DevExpress.XtraEditors.CalcEdit()
            Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
            Me.se_Ref_Presciption = New DevExpress.XtraEditors.SpinEdit()
            Me.calcEdit_Savings_Home_Ins = New DevExpress.XtraEditors.CalcEdit()
            Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
            Me.se_Ref_Car_Ins = New DevExpress.XtraEditors.SpinEdit()
            Me.calcEdit_Savings_Income = New DevExpress.XtraEditors.CalcEdit()
            Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
            Me.se_Ref_Income = New DevExpress.XtraEditors.SpinEdit()
            Me.calcEdit_Savings_Utility = New DevExpress.XtraEditors.CalcEdit()
            Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
            Me.se_Ref_Utility = New DevExpress.XtraEditors.SpinEdit()
            Me.calcEdit_Savings_Home_Repair = New DevExpress.XtraEditors.CalcEdit()
            Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
            Me.se_Ref_Home_Repair = New DevExpress.XtraEditors.SpinEdit()
            Me.calcEdit_Savings_Prop_Taxes = New DevExpress.XtraEditors.CalcEdit()
            Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
            Me.se_Ref_Prop_Taxes = New DevExpress.XtraEditors.SpinEdit()
            Me.calcEdit_Savings_Social = New DevExpress.XtraEditors.CalcEdit()
            Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
            Me.se_Ref_Social = New DevExpress.XtraEditors.SpinEdit()
            Me.calcEdit_Savings_SSI = New DevExpress.XtraEditors.CalcEdit()
            Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
            Me.se_Ref_SSI = New DevExpress.XtraEditors.SpinEdit()
            Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
            Me.lu_File_Status = New DevExpress.XtraEditors.LookUpEdit()
            Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
            Me.dt_File_Status_Date = New DevExpress.XtraEditors.DateEdit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lu_Option.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lu_checkup.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lu_Tier.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lu_bdt_bec.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_Hardest_Hit_Ref_Date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_Hardest_Hit_Ref_Date.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.se_Ref_Food.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.calcEdit_Savings_Food.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.calcEdit_Savings_Med.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.se_Ref_Med.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.calcEdit_Savings_Prescription.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.se_Ref_Presciption.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.calcEdit_Savings_Home_Ins.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.se_Ref_Car_Ins.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.calcEdit_Savings_Income.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.se_Ref_Income.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.calcEdit_Savings_Utility.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.se_Ref_Utility.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.calcEdit_Savings_Home_Repair.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.se_Ref_Home_Repair.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.calcEdit_Savings_Prop_Taxes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.se_Ref_Prop_Taxes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.calcEdit_Savings_Social.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.se_Ref_Social.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.calcEdit_Savings_SSI.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.se_Ref_SSI.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lu_File_Status.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_File_Status_Date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_File_Status_Date.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'SimpleButton_OK
            '
            Me.SimpleButton_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.SimpleButton_OK.Location = New System.Drawing.Point(131, 415)
            Me.SimpleButton_OK.Name = "SimpleButton_OK"
            Me.SimpleButton_OK.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.TabIndex = 47
            Me.SimpleButton_OK.Text = "&OK"
            '
            'SimpleButton_Cancel
            '
            Me.SimpleButton_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.SimpleButton_Cancel.Location = New System.Drawing.Point(231, 415)
            Me.SimpleButton_Cancel.Name = "SimpleButton_Cancel"
            Me.SimpleButton_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.TabIndex = 48
            Me.SimpleButton_Cancel.Text = "&Cancel"
            '
            'lu_Option
            '
            Me.lu_Option.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lu_Option.Location = New System.Drawing.Point(141, 61)
            Me.lu_Option.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.lu_Option.Name = "lu_Option"
            Me.lu_Option.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lu_Option.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.Numeric, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description")})
            Me.lu_Option.Properties.DisplayMember = "description"
            Me.lu_Option.Properties.NullText = ""
            Me.lu_Option.Properties.ShowFooter = False
            Me.lu_Option.Properties.ShowHeader = False
            Me.lu_Option.Properties.ShowLines = False
            Me.lu_Option.Properties.SortColumnIndex = 1
            Me.lu_Option.Properties.ValueMember = "Id"
            Me.lu_Option.Size = New System.Drawing.Size(108, 20)
            Me.lu_Option.TabIndex = 9
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(14, 66)
            Me.LabelControl1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(32, 13)
            Me.LabelControl1.TabIndex = 8
            Me.LabelControl1.Text = "Option"
            '
            'LabelControl7
            '
            Me.LabelControl7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl7.Location = New System.Drawing.Point(14, 22)
            Me.LabelControl7.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.LabelControl7.Name = "LabelControl7"
            Me.LabelControl7.Size = New System.Drawing.Size(78, 13)
            Me.LabelControl7.TabIndex = 0
            Me.LabelControl7.Text = "Benefit Checkup"
            '
            'lu_checkup
            '
            Me.lu_checkup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lu_checkup.Location = New System.Drawing.Point(141, 17)
            Me.lu_checkup.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.lu_checkup.Name = "lu_checkup"
            Me.lu_checkup.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.lu_checkup.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lu_checkup.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.Numeric, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description")})
            Me.lu_checkup.Properties.DisplayMember = "description"
            Me.lu_checkup.Properties.NullText = ""
            Me.lu_checkup.Properties.ShowFooter = False
            Me.lu_checkup.Properties.ShowHeader = False
            Me.lu_checkup.Properties.ValueMember = "Id"
            Me.lu_checkup.Size = New System.Drawing.Size(108, 20)
            Me.lu_checkup.TabIndex = 1
            '
            'LabelControl8
            '
            Me.LabelControl8.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl8.Location = New System.Drawing.Point(254, 66)
            Me.LabelControl8.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.LabelControl8.Name = "LabelControl8"
            Me.LabelControl8.Size = New System.Drawing.Size(18, 13)
            Me.LabelControl8.TabIndex = 10
            Me.LabelControl8.Text = "Tier"
            '
            'lu_Tier
            '
            Me.lu_Tier.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lu_Tier.Location = New System.Drawing.Point(333, 61)
            Me.lu_Tier.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.lu_Tier.Name = "lu_Tier"
            Me.lu_Tier.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lu_Tier.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.Numeric, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description")})
            Me.lu_Tier.Properties.DisplayMember = "description"
            Me.lu_Tier.Properties.NullText = ""
            Me.lu_Tier.Properties.ShowFooter = False
            Me.lu_Tier.Properties.ShowHeader = False
            Me.lu_Tier.Properties.ValueMember = "Id"
            Me.lu_Tier.Size = New System.Drawing.Size(86, 20)
            Me.lu_Tier.TabIndex = 11
            '
            'LabelControl9
            '
            Me.LabelControl9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl9.Location = New System.Drawing.Point(254, 22)
            Me.LabelControl9.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.LabelControl9.Name = "LabelControl9"
            Me.LabelControl9.Size = New System.Drawing.Size(42, 13)
            Me.LabelControl9.TabIndex = 2
            Me.LabelControl9.Text = "BDT/BEC"
            '
            'lu_bdt_bec
            '
            Me.lu_bdt_bec.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lu_bdt_bec.Location = New System.Drawing.Point(333, 17)
            Me.lu_bdt_bec.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.lu_bdt_bec.Name = "lu_bdt_bec"
            Me.lu_bdt_bec.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.lu_bdt_bec.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lu_bdt_bec.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.Numeric, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description")})
            Me.lu_bdt_bec.Properties.DisplayMember = "description"
            Me.lu_bdt_bec.Properties.NullText = ""
            Me.lu_bdt_bec.Properties.ShowFooter = False
            Me.lu_bdt_bec.Properties.ShowHeader = False
            Me.lu_bdt_bec.Properties.ValueMember = "Id"
            Me.lu_bdt_bec.Size = New System.Drawing.Size(86, 20)
            Me.lu_bdt_bec.TabIndex = 3
            '
            'dt_Hardest_Hit_Ref_Date
            '
            Me.dt_Hardest_Hit_Ref_Date.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.dt_Hardest_Hit_Ref_Date.EditValue = Nothing
            Me.dt_Hardest_Hit_Ref_Date.Location = New System.Drawing.Point(141, 83)
            Me.dt_Hardest_Hit_Ref_Date.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.dt_Hardest_Hit_Ref_Date.Name = "dt_Hardest_Hit_Ref_Date"
            Me.dt_Hardest_Hit_Ref_Date.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.dt_Hardest_Hit_Ref_Date.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.dt_Hardest_Hit_Ref_Date.Size = New System.Drawing.Size(108, 20)
            Me.dt_Hardest_Hit_Ref_Date.TabIndex = 13
            '
            'LabelControl10
            '
            Me.LabelControl10.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl10.Location = New System.Drawing.Point(14, 88)
            Me.LabelControl10.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.LabelControl10.Name = "LabelControl10"
            Me.LabelControl10.Size = New System.Drawing.Size(122, 13)
            Me.LabelControl10.TabIndex = 12
            Me.LabelControl10.Text = "Hardest Hit Referral Date"
            '
            'se_Ref_Food
            '
            Me.se_Ref_Food.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
            Me.se_Ref_Food.Location = New System.Drawing.Point(141, 150)
            Me.se_Ref_Food.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.se_Ref_Food.Name = "se_Ref_Food"
            Me.se_Ref_Food.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.se_Ref_Food.Properties.IsFloatValue = False
            Me.se_Ref_Food.Properties.Mask.EditMask = "N00"
            Me.se_Ref_Food.Properties.MaxValue = New Decimal(New Integer() {99, 0, 0, 0})
            Me.se_Ref_Food.Size = New System.Drawing.Size(57, 20)
            Me.se_Ref_Food.TabIndex = 18
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(14, 154)
            Me.LabelControl2.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(24, 13)
            Me.LabelControl2.TabIndex = 17
            Me.LabelControl2.Text = "Food"
            '
            'calcEdit_Savings_Food
            '
            Me.calcEdit_Savings_Food.Location = New System.Drawing.Point(243, 150)
            Me.calcEdit_Savings_Food.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.calcEdit_Savings_Food.Name = "calcEdit_Savings_Food"
            Me.calcEdit_Savings_Food.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.calcEdit_Savings_Food.Properties.DisplayFormat.FormatString = "c"
            Me.calcEdit_Savings_Food.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.calcEdit_Savings_Food.Properties.Mask.BeepOnError = True
            Me.calcEdit_Savings_Food.Properties.Mask.EditMask = "c"
            Me.calcEdit_Savings_Food.Properties.Mask.IgnoreMaskBlank = False
            Me.calcEdit_Savings_Food.Properties.Mask.SaveLiteral = False
            Me.calcEdit_Savings_Food.Properties.Mask.ShowPlaceHolders = False
            Me.calcEdit_Savings_Food.Properties.Mask.UseMaskAsDisplayFormat = True
            Me.calcEdit_Savings_Food.Properties.NullText = "$0.00"
            Me.calcEdit_Savings_Food.Properties.Precision = 2
            Me.calcEdit_Savings_Food.Size = New System.Drawing.Size(86, 20)
            Me.calcEdit_Savings_Food.TabIndex = 19
            '
            'calcEdit_Savings_Med
            '
            Me.calcEdit_Savings_Med.Location = New System.Drawing.Point(243, 175)
            Me.calcEdit_Savings_Med.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.calcEdit_Savings_Med.Name = "calcEdit_Savings_Med"
            Me.calcEdit_Savings_Med.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.calcEdit_Savings_Med.Properties.DisplayFormat.FormatString = "c"
            Me.calcEdit_Savings_Med.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.calcEdit_Savings_Med.Properties.Mask.BeepOnError = True
            Me.calcEdit_Savings_Med.Properties.Mask.EditMask = "c"
            Me.calcEdit_Savings_Med.Properties.Mask.IgnoreMaskBlank = False
            Me.calcEdit_Savings_Med.Properties.Mask.SaveLiteral = False
            Me.calcEdit_Savings_Med.Properties.Mask.ShowPlaceHolders = False
            Me.calcEdit_Savings_Med.Properties.Mask.UseMaskAsDisplayFormat = True
            Me.calcEdit_Savings_Med.Properties.NullText = "$0.00"
            Me.calcEdit_Savings_Med.Properties.Precision = 2
            Me.calcEdit_Savings_Med.Size = New System.Drawing.Size(86, 20)
            Me.calcEdit_Savings_Med.TabIndex = 22
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(14, 180)
            Me.LabelControl3.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(35, 13)
            Me.LabelControl3.TabIndex = 20
            Me.LabelControl3.Text = "Medical"
            '
            'se_Ref_Med
            '
            Me.se_Ref_Med.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
            Me.se_Ref_Med.Location = New System.Drawing.Point(141, 175)
            Me.se_Ref_Med.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.se_Ref_Med.Name = "se_Ref_Med"
            Me.se_Ref_Med.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.se_Ref_Med.Properties.IsFloatValue = False
            Me.se_Ref_Med.Properties.Mask.EditMask = "N00"
            Me.se_Ref_Med.Properties.MaxValue = New Decimal(New Integer() {99, 0, 0, 0})
            Me.se_Ref_Med.Size = New System.Drawing.Size(57, 20)
            Me.se_Ref_Med.TabIndex = 21
            '
            'calcEdit_Savings_Prescription
            '
            Me.calcEdit_Savings_Prescription.Location = New System.Drawing.Point(243, 200)
            Me.calcEdit_Savings_Prescription.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.calcEdit_Savings_Prescription.Name = "calcEdit_Savings_Prescription"
            Me.calcEdit_Savings_Prescription.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.calcEdit_Savings_Prescription.Properties.DisplayFormat.FormatString = "c"
            Me.calcEdit_Savings_Prescription.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.calcEdit_Savings_Prescription.Properties.Mask.BeepOnError = True
            Me.calcEdit_Savings_Prescription.Properties.Mask.EditMask = "c"
            Me.calcEdit_Savings_Prescription.Properties.Mask.IgnoreMaskBlank = False
            Me.calcEdit_Savings_Prescription.Properties.Mask.SaveLiteral = False
            Me.calcEdit_Savings_Prescription.Properties.Mask.ShowPlaceHolders = False
            Me.calcEdit_Savings_Prescription.Properties.Mask.UseMaskAsDisplayFormat = True
            Me.calcEdit_Savings_Prescription.Properties.NullText = "$0.00"
            Me.calcEdit_Savings_Prescription.Properties.Precision = 2
            Me.calcEdit_Savings_Prescription.Size = New System.Drawing.Size(86, 20)
            Me.calcEdit_Savings_Prescription.TabIndex = 25
            '
            'LabelControl4
            '
            Me.LabelControl4.Location = New System.Drawing.Point(14, 205)
            Me.LabelControl4.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.LabelControl4.Name = "LabelControl4"
            Me.LabelControl4.Size = New System.Drawing.Size(56, 13)
            Me.LabelControl4.TabIndex = 23
            Me.LabelControl4.Text = "Prescription"
            '
            'se_Ref_Presciption
            '
            Me.se_Ref_Presciption.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
            Me.se_Ref_Presciption.Location = New System.Drawing.Point(141, 200)
            Me.se_Ref_Presciption.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.se_Ref_Presciption.Name = "se_Ref_Presciption"
            Me.se_Ref_Presciption.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.se_Ref_Presciption.Properties.IsFloatValue = False
            Me.se_Ref_Presciption.Properties.Mask.EditMask = "N00"
            Me.se_Ref_Presciption.Properties.MaxValue = New Decimal(New Integer() {99, 0, 0, 0})
            Me.se_Ref_Presciption.Size = New System.Drawing.Size(57, 20)
            Me.se_Ref_Presciption.TabIndex = 24
            '
            'calcEdit_Savings_Home_Ins
            '
            Me.calcEdit_Savings_Home_Ins.Location = New System.Drawing.Point(243, 250)
            Me.calcEdit_Savings_Home_Ins.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.calcEdit_Savings_Home_Ins.Name = "calcEdit_Savings_Home_Ins"
            Me.calcEdit_Savings_Home_Ins.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.calcEdit_Savings_Home_Ins.Properties.DisplayFormat.FormatString = "c"
            Me.calcEdit_Savings_Home_Ins.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.calcEdit_Savings_Home_Ins.Properties.Mask.BeepOnError = True
            Me.calcEdit_Savings_Home_Ins.Properties.Mask.EditMask = "c"
            Me.calcEdit_Savings_Home_Ins.Properties.Mask.IgnoreMaskBlank = False
            Me.calcEdit_Savings_Home_Ins.Properties.Mask.SaveLiteral = False
            Me.calcEdit_Savings_Home_Ins.Properties.Mask.ShowPlaceHolders = False
            Me.calcEdit_Savings_Home_Ins.Properties.Mask.UseMaskAsDisplayFormat = True
            Me.calcEdit_Savings_Home_Ins.Properties.NullText = "$0.00"
            Me.calcEdit_Savings_Home_Ins.Properties.Precision = 2
            Me.calcEdit_Savings_Home_Ins.Size = New System.Drawing.Size(86, 20)
            Me.calcEdit_Savings_Home_Ins.TabIndex = 31
            '
            'LabelControl5
            '
            Me.LabelControl5.Location = New System.Drawing.Point(14, 255)
            Me.LabelControl5.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.LabelControl5.Name = "LabelControl5"
            Me.LabelControl5.Size = New System.Drawing.Size(91, 13)
            Me.LabelControl5.TabIndex = 29
            Me.LabelControl5.Text = "HOI/Car Insurance"
            '
            'se_Ref_Car_Ins
            '
            Me.se_Ref_Car_Ins.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
            Me.se_Ref_Car_Ins.Location = New System.Drawing.Point(141, 250)
            Me.se_Ref_Car_Ins.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.se_Ref_Car_Ins.Name = "se_Ref_Car_Ins"
            Me.se_Ref_Car_Ins.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.se_Ref_Car_Ins.Properties.IsFloatValue = False
            Me.se_Ref_Car_Ins.Properties.Mask.EditMask = "N00"
            Me.se_Ref_Car_Ins.Properties.MaxValue = New Decimal(New Integer() {99, 0, 0, 0})
            Me.se_Ref_Car_Ins.Size = New System.Drawing.Size(57, 20)
            Me.se_Ref_Car_Ins.TabIndex = 30
            '
            'calcEdit_Savings_Income
            '
            Me.calcEdit_Savings_Income.Location = New System.Drawing.Point(243, 326)
            Me.calcEdit_Savings_Income.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.calcEdit_Savings_Income.Name = "calcEdit_Savings_Income"
            Me.calcEdit_Savings_Income.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.calcEdit_Savings_Income.Properties.DisplayFormat.FormatString = "c"
            Me.calcEdit_Savings_Income.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.calcEdit_Savings_Income.Properties.Mask.BeepOnError = True
            Me.calcEdit_Savings_Income.Properties.Mask.EditMask = "c"
            Me.calcEdit_Savings_Income.Properties.Mask.IgnoreMaskBlank = False
            Me.calcEdit_Savings_Income.Properties.Mask.SaveLiteral = False
            Me.calcEdit_Savings_Income.Properties.Mask.ShowPlaceHolders = False
            Me.calcEdit_Savings_Income.Properties.Mask.UseMaskAsDisplayFormat = True
            Me.calcEdit_Savings_Income.Properties.NullText = "$0.00"
            Me.calcEdit_Savings_Income.Properties.Precision = 2
            Me.calcEdit_Savings_Income.Size = New System.Drawing.Size(86, 20)
            Me.calcEdit_Savings_Income.TabIndex = 40
            '
            'LabelControl6
            '
            Me.LabelControl6.Location = New System.Drawing.Point(14, 330)
            Me.LabelControl6.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.LabelControl6.Name = "LabelControl6"
            Me.LabelControl6.Size = New System.Drawing.Size(35, 13)
            Me.LabelControl6.TabIndex = 38
            Me.LabelControl6.Text = "Income"
            '
            'se_Ref_Income
            '
            Me.se_Ref_Income.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
            Me.se_Ref_Income.Location = New System.Drawing.Point(141, 326)
            Me.se_Ref_Income.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.se_Ref_Income.Name = "se_Ref_Income"
            Me.se_Ref_Income.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.se_Ref_Income.Properties.IsFloatValue = False
            Me.se_Ref_Income.Properties.Mask.EditMask = "N00"
            Me.se_Ref_Income.Properties.MaxValue = New Decimal(New Integer() {99, 0, 0, 0})
            Me.se_Ref_Income.Size = New System.Drawing.Size(57, 20)
            Me.se_Ref_Income.TabIndex = 39
            '
            'calcEdit_Savings_Utility
            '
            Me.calcEdit_Savings_Utility.Location = New System.Drawing.Point(243, 225)
            Me.calcEdit_Savings_Utility.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.calcEdit_Savings_Utility.Name = "calcEdit_Savings_Utility"
            Me.calcEdit_Savings_Utility.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.calcEdit_Savings_Utility.Properties.DisplayFormat.FormatString = "c"
            Me.calcEdit_Savings_Utility.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.calcEdit_Savings_Utility.Properties.Mask.BeepOnError = True
            Me.calcEdit_Savings_Utility.Properties.Mask.EditMask = "c"
            Me.calcEdit_Savings_Utility.Properties.Mask.IgnoreMaskBlank = False
            Me.calcEdit_Savings_Utility.Properties.Mask.SaveLiteral = False
            Me.calcEdit_Savings_Utility.Properties.Mask.ShowPlaceHolders = False
            Me.calcEdit_Savings_Utility.Properties.Mask.UseMaskAsDisplayFormat = True
            Me.calcEdit_Savings_Utility.Properties.NullText = "$0.00"
            Me.calcEdit_Savings_Utility.Properties.Precision = 2
            Me.calcEdit_Savings_Utility.Size = New System.Drawing.Size(86, 20)
            Me.calcEdit_Savings_Utility.TabIndex = 28
            '
            'LabelControl11
            '
            Me.LabelControl11.Location = New System.Drawing.Point(14, 230)
            Me.LabelControl11.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.LabelControl11.Name = "LabelControl11"
            Me.LabelControl11.Size = New System.Drawing.Size(27, 13)
            Me.LabelControl11.TabIndex = 26
            Me.LabelControl11.Text = "Utility"
            '
            'se_Ref_Utility
            '
            Me.se_Ref_Utility.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
            Me.se_Ref_Utility.Location = New System.Drawing.Point(141, 225)
            Me.se_Ref_Utility.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.se_Ref_Utility.Name = "se_Ref_Utility"
            Me.se_Ref_Utility.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.se_Ref_Utility.Properties.IsFloatValue = False
            Me.se_Ref_Utility.Properties.Mask.EditMask = "N00"
            Me.se_Ref_Utility.Properties.MaxValue = New Decimal(New Integer() {99, 0, 0, 0})
            Me.se_Ref_Utility.Size = New System.Drawing.Size(57, 20)
            Me.se_Ref_Utility.TabIndex = 27
            '
            'calcEdit_Savings_Home_Repair
            '
            Me.calcEdit_Savings_Home_Repair.Location = New System.Drawing.Point(243, 301)
            Me.calcEdit_Savings_Home_Repair.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.calcEdit_Savings_Home_Repair.Name = "calcEdit_Savings_Home_Repair"
            Me.calcEdit_Savings_Home_Repair.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.calcEdit_Savings_Home_Repair.Properties.DisplayFormat.FormatString = "c"
            Me.calcEdit_Savings_Home_Repair.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.calcEdit_Savings_Home_Repair.Properties.Mask.BeepOnError = True
            Me.calcEdit_Savings_Home_Repair.Properties.Mask.EditMask = "c"
            Me.calcEdit_Savings_Home_Repair.Properties.Mask.IgnoreMaskBlank = False
            Me.calcEdit_Savings_Home_Repair.Properties.Mask.SaveLiteral = False
            Me.calcEdit_Savings_Home_Repair.Properties.Mask.ShowPlaceHolders = False
            Me.calcEdit_Savings_Home_Repair.Properties.Mask.UseMaskAsDisplayFormat = True
            Me.calcEdit_Savings_Home_Repair.Properties.NullText = "$0.00"
            Me.calcEdit_Savings_Home_Repair.Properties.Precision = 2
            Me.calcEdit_Savings_Home_Repair.Size = New System.Drawing.Size(86, 20)
            Me.calcEdit_Savings_Home_Repair.TabIndex = 37
            '
            'LabelControl12
            '
            Me.LabelControl12.Location = New System.Drawing.Point(14, 305)
            Me.LabelControl12.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.LabelControl12.Name = "LabelControl12"
            Me.LabelControl12.Size = New System.Drawing.Size(61, 13)
            Me.LabelControl12.TabIndex = 35
            Me.LabelControl12.Text = "Home Repair"
            '
            'se_Ref_Home_Repair
            '
            Me.se_Ref_Home_Repair.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
            Me.se_Ref_Home_Repair.Location = New System.Drawing.Point(141, 301)
            Me.se_Ref_Home_Repair.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.se_Ref_Home_Repair.Name = "se_Ref_Home_Repair"
            Me.se_Ref_Home_Repair.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.se_Ref_Home_Repair.Properties.IsFloatValue = False
            Me.se_Ref_Home_Repair.Properties.Mask.EditMask = "N00"
            Me.se_Ref_Home_Repair.Properties.MaxValue = New Decimal(New Integer() {99, 0, 0, 0})
            Me.se_Ref_Home_Repair.Size = New System.Drawing.Size(57, 20)
            Me.se_Ref_Home_Repair.TabIndex = 36
            '
            'calcEdit_Savings_Prop_Taxes
            '
            Me.calcEdit_Savings_Prop_Taxes.Location = New System.Drawing.Point(243, 275)
            Me.calcEdit_Savings_Prop_Taxes.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.calcEdit_Savings_Prop_Taxes.Name = "calcEdit_Savings_Prop_Taxes"
            Me.calcEdit_Savings_Prop_Taxes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.calcEdit_Savings_Prop_Taxes.Properties.DisplayFormat.FormatString = "c"
            Me.calcEdit_Savings_Prop_Taxes.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.calcEdit_Savings_Prop_Taxes.Properties.Mask.BeepOnError = True
            Me.calcEdit_Savings_Prop_Taxes.Properties.Mask.EditMask = "c"
            Me.calcEdit_Savings_Prop_Taxes.Properties.Mask.IgnoreMaskBlank = False
            Me.calcEdit_Savings_Prop_Taxes.Properties.Mask.SaveLiteral = False
            Me.calcEdit_Savings_Prop_Taxes.Properties.Mask.ShowPlaceHolders = False
            Me.calcEdit_Savings_Prop_Taxes.Properties.Mask.UseMaskAsDisplayFormat = True
            Me.calcEdit_Savings_Prop_Taxes.Properties.NullText = "$0.00"
            Me.calcEdit_Savings_Prop_Taxes.Properties.Precision = 2
            Me.calcEdit_Savings_Prop_Taxes.Size = New System.Drawing.Size(86, 20)
            Me.calcEdit_Savings_Prop_Taxes.TabIndex = 34
            '
            'LabelControl13
            '
            Me.LabelControl13.Location = New System.Drawing.Point(14, 280)
            Me.LabelControl13.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.LabelControl13.Name = "LabelControl13"
            Me.LabelControl13.Size = New System.Drawing.Size(74, 13)
            Me.LabelControl13.TabIndex = 32
            Me.LabelControl13.Text = "Property Taxes"
            '
            'se_Ref_Prop_Taxes
            '
            Me.se_Ref_Prop_Taxes.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
            Me.se_Ref_Prop_Taxes.Location = New System.Drawing.Point(141, 275)
            Me.se_Ref_Prop_Taxes.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.se_Ref_Prop_Taxes.Name = "se_Ref_Prop_Taxes"
            Me.se_Ref_Prop_Taxes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.se_Ref_Prop_Taxes.Properties.IsFloatValue = False
            Me.se_Ref_Prop_Taxes.Properties.Mask.EditMask = "N00"
            Me.se_Ref_Prop_Taxes.Properties.MaxValue = New Decimal(New Integer() {99, 0, 0, 0})
            Me.se_Ref_Prop_Taxes.Size = New System.Drawing.Size(57, 20)
            Me.se_Ref_Prop_Taxes.TabIndex = 33
            '
            'calcEdit_Savings_Social
            '
            Me.calcEdit_Savings_Social.Location = New System.Drawing.Point(243, 351)
            Me.calcEdit_Savings_Social.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.calcEdit_Savings_Social.Name = "calcEdit_Savings_Social"
            Me.calcEdit_Savings_Social.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.calcEdit_Savings_Social.Properties.DisplayFormat.FormatString = "c"
            Me.calcEdit_Savings_Social.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.calcEdit_Savings_Social.Properties.Mask.BeepOnError = True
            Me.calcEdit_Savings_Social.Properties.Mask.EditMask = "c"
            Me.calcEdit_Savings_Social.Properties.Mask.IgnoreMaskBlank = False
            Me.calcEdit_Savings_Social.Properties.Mask.SaveLiteral = False
            Me.calcEdit_Savings_Social.Properties.Mask.ShowPlaceHolders = False
            Me.calcEdit_Savings_Social.Properties.Mask.UseMaskAsDisplayFormat = True
            Me.calcEdit_Savings_Social.Properties.NullText = "$0.00"
            Me.calcEdit_Savings_Social.Properties.Precision = 2
            Me.calcEdit_Savings_Social.Size = New System.Drawing.Size(86, 20)
            Me.calcEdit_Savings_Social.TabIndex = 43
            '
            'LabelControl14
            '
            Me.LabelControl14.Location = New System.Drawing.Point(14, 355)
            Me.LabelControl14.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.LabelControl14.Name = "LabelControl14"
            Me.LabelControl14.Size = New System.Drawing.Size(70, 13)
            Me.LabelControl14.TabIndex = 41
            Me.LabelControl14.Text = "Social Services"
            '
            'se_Ref_Social
            '
            Me.se_Ref_Social.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
            Me.se_Ref_Social.Location = New System.Drawing.Point(141, 351)
            Me.se_Ref_Social.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.se_Ref_Social.Name = "se_Ref_Social"
            Me.se_Ref_Social.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.se_Ref_Social.Properties.IsFloatValue = False
            Me.se_Ref_Social.Properties.Mask.EditMask = "N00"
            Me.se_Ref_Social.Properties.MaxValue = New Decimal(New Integer() {99, 0, 0, 0})
            Me.se_Ref_Social.Size = New System.Drawing.Size(57, 20)
            Me.se_Ref_Social.TabIndex = 42
            '
            'calcEdit_Savings_SSI
            '
            Me.calcEdit_Savings_SSI.Location = New System.Drawing.Point(243, 376)
            Me.calcEdit_Savings_SSI.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.calcEdit_Savings_SSI.Name = "calcEdit_Savings_SSI"
            Me.calcEdit_Savings_SSI.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.calcEdit_Savings_SSI.Properties.DisplayFormat.FormatString = "c"
            Me.calcEdit_Savings_SSI.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.calcEdit_Savings_SSI.Properties.Mask.BeepOnError = True
            Me.calcEdit_Savings_SSI.Properties.Mask.EditMask = "c"
            Me.calcEdit_Savings_SSI.Properties.Mask.IgnoreMaskBlank = False
            Me.calcEdit_Savings_SSI.Properties.Mask.SaveLiteral = False
            Me.calcEdit_Savings_SSI.Properties.Mask.ShowPlaceHolders = False
            Me.calcEdit_Savings_SSI.Properties.Mask.UseMaskAsDisplayFormat = True
            Me.calcEdit_Savings_SSI.Properties.NullText = "$0.00"
            Me.calcEdit_Savings_SSI.Properties.Precision = 2
            Me.calcEdit_Savings_SSI.Size = New System.Drawing.Size(86, 20)
            Me.calcEdit_Savings_SSI.TabIndex = 46
            '
            'LabelControl15
            '
            Me.LabelControl15.Location = New System.Drawing.Point(14, 380)
            Me.LabelControl15.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.LabelControl15.Name = "LabelControl15"
            Me.LabelControl15.Size = New System.Drawing.Size(16, 13)
            Me.LabelControl15.TabIndex = 44
            Me.LabelControl15.Text = "SSI"
            '
            'se_Ref_SSI
            '
            Me.se_Ref_SSI.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
            Me.se_Ref_SSI.Location = New System.Drawing.Point(141, 376)
            Me.se_Ref_SSI.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.se_Ref_SSI.Name = "se_Ref_SSI"
            Me.se_Ref_SSI.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.se_Ref_SSI.Properties.IsFloatValue = False
            Me.se_Ref_SSI.Properties.Mask.EditMask = "N00"
            Me.se_Ref_SSI.Properties.MaxValue = New Decimal(New Integer() {99, 0, 0, 0})
            Me.se_Ref_SSI.Size = New System.Drawing.Size(57, 20)
            Me.se_Ref_SSI.TabIndex = 45
            '
            'LabelControl16
            '
            Me.LabelControl16.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
            Me.LabelControl16.Location = New System.Drawing.Point(14, 119)
            Me.LabelControl16.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.LabelControl16.Name = "LabelControl16"
            Me.LabelControl16.Size = New System.Drawing.Size(54, 14)
            Me.LabelControl16.TabIndex = 14
            Me.LabelControl16.Text = "Referrals"
            '
            'LabelControl17
            '
            Me.LabelControl17.Appearance.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
            Me.LabelControl17.Location = New System.Drawing.Point(120, 121)
            Me.LabelControl17.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.LabelControl17.Name = "LabelControl17"
            Me.LabelControl17.Size = New System.Drawing.Size(78, 13)
            Me.LabelControl17.TabIndex = 15
            Me.LabelControl17.Text = "# of Referrals"
            '
            'LabelControl18
            '
            Me.LabelControl18.Appearance.Font = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
            Me.LabelControl18.Location = New System.Drawing.Point(285, 121)
            Me.LabelControl18.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.LabelControl18.Name = "LabelControl18"
            Me.LabelControl18.Size = New System.Drawing.Size(44, 13)
            Me.LabelControl18.TabIndex = 16
            Me.LabelControl18.Text = "Savings"
            '
            'LabelControl19
            '
            Me.LabelControl19.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl19.Location = New System.Drawing.Point(14, 46)
            Me.LabelControl19.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.LabelControl19.Name = "LabelControl19"
            Me.LabelControl19.Size = New System.Drawing.Size(50, 13)
            Me.LabelControl19.TabIndex = 4
            Me.LabelControl19.Text = "File Status"
            '
            'lu_File_Status
            '
            Me.lu_File_Status.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lu_File_Status.Location = New System.Drawing.Point(141, 39)
            Me.lu_File_Status.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.lu_File_Status.Name = "lu_File_Status"
            Me.lu_File_Status.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lu_File_Status.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.Numeric, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description")})
            Me.lu_File_Status.Properties.DisplayMember = "description"
            Me.lu_File_Status.Properties.NullText = ""
            Me.lu_File_Status.Properties.ShowFooter = False
            Me.lu_File_Status.Properties.ShowHeader = False
            Me.lu_File_Status.Properties.ValueMember = "Id"
            Me.lu_File_Status.Size = New System.Drawing.Size(108, 20)
            Me.lu_File_Status.TabIndex = 5
            '
            'LabelControl20
            '
            Me.LabelControl20.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl20.Location = New System.Drawing.Point(254, 46)
            Me.LabelControl20.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.LabelControl20.Name = "LabelControl20"
            Me.LabelControl20.Size = New System.Drawing.Size(76, 13)
            Me.LabelControl20.TabIndex = 6
            Me.LabelControl20.Text = "File Status Date"
            '
            'dt_File_Status_Date
            '
            Me.dt_File_Status_Date.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.dt_File_Status_Date.EditValue = Nothing
            Me.dt_File_Status_Date.Location = New System.Drawing.Point(333, 39)
            Me.dt_File_Status_Date.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.dt_File_Status_Date.Name = "dt_File_Status_Date"
            Me.dt_File_Status_Date.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.dt_File_Status_Date.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.dt_File_Status_Date.Size = New System.Drawing.Size(87, 20)
            Me.dt_File_Status_Date.TabIndex = 7
            '
            'Form_HECMDefault
            '
            Me.AcceptButton = Me.SimpleButton_OK
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.SimpleButton_Cancel
            Me.ClientSize = New System.Drawing.Size(437, 451)
            Me.Controls.Add(Me.LabelControl20)
            Me.Controls.Add(Me.dt_File_Status_Date)
            Me.Controls.Add(Me.lu_File_Status)
            Me.Controls.Add(Me.LabelControl19)
            Me.Controls.Add(Me.LabelControl18)
            Me.Controls.Add(Me.LabelControl17)
            Me.Controls.Add(Me.LabelControl16)
            Me.Controls.Add(Me.calcEdit_Savings_SSI)
            Me.Controls.Add(Me.LabelControl15)
            Me.Controls.Add(Me.se_Ref_SSI)
            Me.Controls.Add(Me.calcEdit_Savings_Social)
            Me.Controls.Add(Me.LabelControl14)
            Me.Controls.Add(Me.se_Ref_Social)
            Me.Controls.Add(Me.calcEdit_Savings_Prop_Taxes)
            Me.Controls.Add(Me.LabelControl13)
            Me.Controls.Add(Me.se_Ref_Prop_Taxes)
            Me.Controls.Add(Me.calcEdit_Savings_Home_Repair)
            Me.Controls.Add(Me.LabelControl12)
            Me.Controls.Add(Me.se_Ref_Home_Repair)
            Me.Controls.Add(Me.calcEdit_Savings_Utility)
            Me.Controls.Add(Me.LabelControl11)
            Me.Controls.Add(Me.se_Ref_Utility)
            Me.Controls.Add(Me.calcEdit_Savings_Income)
            Me.Controls.Add(Me.LabelControl6)
            Me.Controls.Add(Me.se_Ref_Income)
            Me.Controls.Add(Me.calcEdit_Savings_Home_Ins)
            Me.Controls.Add(Me.LabelControl5)
            Me.Controls.Add(Me.se_Ref_Car_Ins)
            Me.Controls.Add(Me.calcEdit_Savings_Prescription)
            Me.Controls.Add(Me.LabelControl4)
            Me.Controls.Add(Me.se_Ref_Presciption)
            Me.Controls.Add(Me.calcEdit_Savings_Med)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.se_Ref_Med)
            Me.Controls.Add(Me.calcEdit_Savings_Food)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.se_Ref_Food)
            Me.Controls.Add(Me.LabelControl10)
            Me.Controls.Add(Me.dt_Hardest_Hit_Ref_Date)
            Me.Controls.Add(Me.LabelControl9)
            Me.Controls.Add(Me.lu_bdt_bec)
            Me.Controls.Add(Me.LabelControl8)
            Me.Controls.Add(Me.lu_Tier)
            Me.Controls.Add(Me.LabelControl7)
            Me.Controls.Add(Me.lu_checkup)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.lu_Option)
            Me.Controls.Add(Me.SimpleButton_Cancel)
            Me.Controls.Add(Me.SimpleButton_OK)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
            Me.MaximizeBox = False
            Me.Name = "Form_HECMDefault"
            Me.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
            Me.Text = "HECM Default"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lu_Option.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lu_checkup.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lu_Tier.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lu_bdt_bec.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_Hardest_Hit_Ref_Date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_Hardest_Hit_Ref_Date.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.se_Ref_Food.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.calcEdit_Savings_Food.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.calcEdit_Savings_Med.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.se_Ref_Med.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.calcEdit_Savings_Prescription.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.se_Ref_Presciption.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.calcEdit_Savings_Home_Ins.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.se_Ref_Car_Ins.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.calcEdit_Savings_Income.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.se_Ref_Income.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.calcEdit_Savings_Utility.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.se_Ref_Utility.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.calcEdit_Savings_Home_Repair.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.se_Ref_Home_Repair.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.calcEdit_Savings_Prop_Taxes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.se_Ref_Prop_Taxes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.calcEdit_Savings_Social.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.se_Ref_Social.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.calcEdit_Savings_SSI.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.se_Ref_SSI.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lu_File_Status.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_File_Status_Date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_File_Status_Date.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents SimpleButton_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents lu_Option As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lu_checkup As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lu_Tier As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lu_bdt_bec As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents dt_Hardest_Hit_Ref_Date As DevExpress.XtraEditors.DateEdit
        Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents se_Ref_Food As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents calcEdit_Savings_Food As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents calcEdit_Savings_Med As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents se_Ref_Med As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents calcEdit_Savings_Prescription As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents se_Ref_Presciption As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents calcEdit_Savings_Home_Ins As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents se_Ref_Car_Ins As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents calcEdit_Savings_Income As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents se_Ref_Income As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents calcEdit_Savings_Utility As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents se_Ref_Utility As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents calcEdit_Savings_Home_Repair As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents se_Ref_Home_Repair As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents calcEdit_Savings_Prop_Taxes As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents se_Ref_Prop_Taxes As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents calcEdit_Savings_Social As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents se_Ref_Social As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents calcEdit_Savings_SSI As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents se_Ref_SSI As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lu_File_Status As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents dt_File_Status_Date As DevExpress.XtraEditors.DateEdit
    End Class
End Namespace