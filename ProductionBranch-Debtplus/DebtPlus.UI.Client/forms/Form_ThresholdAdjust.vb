#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DebtPlus.Data.Controls
Imports DebtPlus.UI.Client.Service
Imports System.Globalization

Namespace forms

    Friend Class Form_ThresholdAdjust
        Implements IContext

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        ''' <summary>
        ''' Initialize the form
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Load, AddressOf Form_ThresholdAdjust_Load
            AddHandler ComboBoxEdit1.SelectedIndexChanged, AddressOf ComboBoxEdit1_SelectedIndexChanged
        End Sub

        Public Sub New(ByVal Context As ClientUpdateClass)
            MyClass.New()
            privateContext = Context
        End Sub

        ''' <summary>
        ''' Pointer to the client update information for the current client
        ''' </summary>
        Private ReadOnly privateContext As ClientUpdateClass

        Public ReadOnly Property Context() As ClientUpdateClass Implements IContext.Context
            Get
                Return privateContext
            End Get
        End Property

        Private Class MyItems
            Inherits ComboboxItem

            Public Sub New(ByVal Value As Double)
                MyBase.New(String.Format("{0:p}", Value), Value)
            End Sub

            Public Sub New()
                MyBase.New()
            End Sub

            Public Sub New(ByVal Description As String, ByVal Value As Object, ByVal Active As Boolean)
                MyBase.New(Description, Value, Active)
            End Sub

            Public Sub New(ByVal Description As String, ByVal Value As Object)
                MyBase.New(Description, Value)
            End Sub

            Public Sub New(ByVal Description As String)
                MyBase.New(Description)
            End Sub
        End Class

        Private privateThreshold As Double = 0.021

        Public Property ParameterThreshold() As Double
            Get
                Return privateThreshold
            End Get
            Set(ByVal value As Double)
                While value >= 1.0#
                    value /= 100.0#
                End While
                privateThreshold = value
            End Set
        End Property

        Private Sub Form_ThresholdAdjust_Load(ByVal sender As Object, ByVal e As EventArgs)

            ' Set the starting value
            LabelControl_current.Text = String.Format("The current value is {0:p}", ParameterThreshold)

            With ComboBoxEdit1
                Dim SelectedItem As Int32 = -1
                With .Properties
                    With .Items
                        .Clear()
                        For Each Pct As Double In New Double() {0.015, 0.02, 0.021, 0.025, 0.028, 0.03, 0.031, 0.035, 0.04, 0.045, 0.05, 0.051}
                            Dim NewItem As Int32 = .Add(New MyItems(Pct))
                            If ParameterThreshold.CompareTo(Pct) = 0 Then SelectedItem = NewItem
                        Next
                    End With
                End With

                .SelectedIndex = SelectedItem
                If SelectedItem < 0 Then .Text = String.Format("{0:p}", ParameterThreshold)
                AddHandler .TextChanged, AddressOf ComboBoxEdit1_TextChanged
            End With
        End Sub

        Private Sub ComboBoxEdit1_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
            Dim ItemValue As Double
            Dim ItemText As String = ComboBoxEdit1.Text.Replace(NumberFormatInfo.CurrentInfo.PercentSymbol, String.Empty).Replace(" ", "")

            ' Try to parse the input. If positive then look for a match in the selected table.
            If Double.TryParse(ItemText, ItemValue) AndAlso ItemValue > 0.0# Then
                ParameterThreshold = ItemValue
                SimpleButton_OK.Enabled = True

                With ComboBoxEdit1
                    For ItemNo As Int32 = 0 To .Properties.Items.Count - 1
                        If DirectCast(CType(.Properties.Items(ItemNo), MyItems).value, Double) = ParameterThreshold Then
                            .SelectedIndex = ItemNo
                            Exit For
                        End If
                    Next
                End With

            Else

                ' The rate is not defined. Reject the entry.
                SimpleButton_OK.Enabled = False
            End If
        End Sub

        Private Sub ComboBoxEdit1_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
            If ComboBoxEdit1.SelectedIndex >= 0 Then
                SimpleButton_OK.Enabled = True
                ParameterThreshold = DirectCast(DirectCast(ComboBoxEdit1.SelectedItem, MyItems).value, Double)
            End If
        End Sub
    End Class
End Namespace
