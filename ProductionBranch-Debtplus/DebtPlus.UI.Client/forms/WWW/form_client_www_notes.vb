#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DebtPlus.Data.Forms
Imports DebtPlus.UI.Client.Service
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo
Imports System.Linq
Imports DebtPlus.LINQ

Namespace forms
    Friend Class Form_client_www_notes
        Inherits DebtPlusForm

        Private client_www_record As client_www
        Protected ControlRow As Int32

        ''' <summary>
        ''' Initialize the form
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(client_www_record As client_www)
            MyClass.New()
            Me.client_www_record = client_www_record
            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf Form_client_www_notes_Load
            AddHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler Load, AddressOf Form_client_www_notes_Load
            RemoveHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents GridColumn_date_created As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_created_by As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_date_shown As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_message As DevExpress.XtraGrid.Columns.GridColumn

        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
            Me.GridColumn_date_created = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_created_by = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_date_shown = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_message = New DevExpress.XtraGrid.Columns.GridColumn()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'DefaultLookAndFeel1
            '
            Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin"
            '
            'GridControl1
            '
            Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.GridControl1.Location = New System.Drawing.Point(0, 0)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(536, 266)
            Me.GridControl1.TabIndex = 0
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_date_created, Me.GridColumn_created_by, Me.GridColumn_date_shown, Me.GridColumn_message})
            Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            '
            'GridColumn_date_created
            '
            Me.GridColumn_date_created.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_date_created.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date_created.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_date_created.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date_created.Caption = "Date"
            Me.GridColumn_date_created.CustomizationCaption = "Created date"
            Me.GridColumn_date_created.DisplayFormat.FormatString = "d"
            Me.GridColumn_date_created.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_created.FieldName = "date_created"
            Me.GridColumn_date_created.GroupFormat.FormatString = "d"
            Me.GridColumn_date_created.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_created.Name = "GridColumn_date_created"
            Me.GridColumn_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_date_created.Visible = True
            Me.GridColumn_date_created.VisibleIndex = 0
            Me.GridColumn_date_created.Width = 66
            '
            'GridColumn_created_by
            '
            Me.GridColumn_created_by.Caption = "Counselor"
            Me.GridColumn_created_by.CustomizationCaption = "Message Creator"
            Me.GridColumn_created_by.FieldName = "created_by"
            Me.GridColumn_created_by.Name = "GridColumn_created_by"
            Me.GridColumn_created_by.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_created_by.Visible = True
            Me.GridColumn_created_by.VisibleIndex = 1
            Me.GridColumn_created_by.Width = 93
            '
            'GridColumn_date_shown
            '
            Me.GridColumn_date_shown.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_date_shown.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date_shown.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_date_shown.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date_shown.Caption = "Read"
            Me.GridColumn_date_shown.CustomizationCaption = "Date delivered to user"
            Me.GridColumn_date_shown.DisplayFormat.FormatString = "d"
            Me.GridColumn_date_shown.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_shown.FieldName = "date_shown"
            Me.GridColumn_date_shown.GroupFormat.FormatString = "d"
            Me.GridColumn_date_shown.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_shown.Name = "GridColumn_date_shown"
            Me.GridColumn_date_shown.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_date_shown.Visible = True
            Me.GridColumn_date_shown.VisibleIndex = 2
            Me.GridColumn_date_shown.Width = 71
            '
            'GridColumn_message
            '
            Me.GridColumn_message.Caption = "Message"
            Me.GridColumn_message.CustomizationCaption = "Message"
            Me.GridColumn_message.FieldName = "message"
            Me.GridColumn_message.Name = "GridColumn_message"
            Me.GridColumn_message.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_message.Visible = True
            Me.GridColumn_message.VisibleIndex = 3
            Me.GridColumn_message.Width = 302
            '
            'Form_Messages
            '
            Me.ClientSize = New System.Drawing.Size(536, 266)
            Me.Controls.Add(Me.GridControl1)
            Me.Name = "Form_Messages"
            Me.Text = "WWW Messages"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        ''' <summary>
        ''' Create the context menu for the control
        ''' </summary>
        Private Function myContextMenu() As ContextMenu
            Dim menu As New ContextMenu

            With menu.MenuItems
                .Add(New MenuItem("Add", AddressOf MenuItemCreate, Shortcut.Ins))
                .Add(New MenuItem("Change", AddressOf MenuItemEdit))
                .Add(New MenuItem("Delete", AddressOf MenuItemDelete, Shortcut.Del))
            End With
            AddHandler menu.Popup, AddressOf ContextMenuPopup

            Return menu
        End Function

        ''' <summary>
        ''' Handle the popup function for the context menu
        ''' </summary>
        Private Sub ContextMenuPopup(ByVal Sender As Object, ByVal e As EventArgs)
            For Each itm As MenuItem In CType(Sender, ContextMenu).MenuItems
                Select Case itm.Text.ToLower()
                    Case "add"
                        itm.Enabled = True
                    Case "delete", "change"
                        itm.Enabled = (ControlRow >= 0)
                End Select
            Next
        End Sub

        ''' <summary>
        ''' Process a CREATE menu item choice
        ''' </summary>
        Protected Sub MenuItemCreate(ByVal sender As Object, ByVal e As EventArgs)
            CreateItem()
        End Sub

        ''' <summary>
        ''' Process an EDIT menu item choice
        ''' </summary>
        Protected Sub MenuItemEdit(ByVal sender As Object, ByVal e As EventArgs)

            ' Edit the indicated item
            If ControlRow >= 0 Then
                Dim www As client_www_note = DirectCast(GridView1.GetRow(ControlRow), client_www_note)
                If www IsNot Nothing Then
                    EditItem(www)
                End If
            End If

        End Sub

        ''' <summary>
        ''' Process a DELETE menu item
        ''' </summary>
        Protected Sub MenuItemDelete(ByVal sender As Object, ByVal e As EventArgs)
            If ControlRow >= 0 Then
                DeleteItem(TryCast(GridView1.GetRow(ControlRow), client_www_note))
            End If
        End Sub

        ''' <summary>
        ''' Function to edit the item in the list
        ''' </summary>
        Protected Sub EditItem(ByVal record As client_www_note)
            Dim ResultString As String = String.Empty

            ' Determine if the user changed the message
            If InputBox.Show("What do you wish the message to say?", ResultString, "Client Message Edit", record.message, 50) = DialogResult.OK Then
                record.message = ResultString
            End If
        End Sub

        ''' <summary>
        ''' Function to delete the item in the list
        ''' </summary>
        Protected Sub DeleteItem(ByVal record As client_www_note)
            If record IsNot Nothing Then

                ' Ask if the user wants to delete the note
                If DebtPlus.Data.Prompts.RequestConfirmation_Delete() = DialogResult.Yes Then
                    client_www_record.client_www_notes.Remove(record)
                    GridControl1.DataSource = client_www_record.client_www_notes.GetNewBindingList()
                    GridView1.RefreshData()
                End If
            End If
        End Sub

        ''' <summary>
        ''' Function to create an item in the list
        ''' </summary>
        Protected Sub CreateItem()
            Dim record As client_www_note = DebtPlus.LINQ.Factory.Manufacture_client_www_note()
            Dim ResultString As String = String.Empty
            If InputBox.Show("What do you wish the message to say?", ResultString, "Client Message Edit", String.Empty, 50) = DialogResult.OK Then
                record.message = ResultString
                client_www_record.client_www_notes.Add(record)

                ' Refresh the grid of messages
                GridControl1.DataSource = client_www_record.client_www_notes.GetNewBindingList()
                GridView1.RefreshData()
            End If
        End Sub

        ''' <summary>
        ''' Double Click on an item -- edit it
        ''' </summary>
        Private Sub GridView1_DoubleClick(ByVal sender As Object, ByVal e As EventArgs)

            ' Find the row targeted as the double-click item
            Dim hi As GridHitInfo = GridView1.CalcHitInfo((GridControl1.PointToClient(MousePosition)))
            Dim ControlRow As Int32 = hi.RowHandle
            If ControlRow >= 0 Then
                EditItem(TryCast(GridView1.GetRow(ControlRow), client_www_note))
            End If
        End Sub

        ''' <summary>
        ''' Process the load event on the form
        ''' </summary>
        Private Sub Form_client_www_notes_Load(ByVal sender As Object, ByVal e As EventArgs)

            ' Load the grid control with the note list
            GridControl1.ContextMenu = myContextMenu()
            GridControl1.DataSource = client_www_record.client_www_notes
            GridControl1.RefreshDataSource()

            GridView1.BestFitColumns()
        End Sub
    End Class
End Namespace
