﻿Imports DebtPlus.LINQ
Imports System.Linq

Public Class Form_client_www
    Inherits DebtPlus.Data.Forms.DebtPlusForm
    Protected record As DebtPlus.LINQ.client_www = Nothing

    Public Sub New()
        MyBase.New()
        InitializeComponent()
    End Sub

    Public Sub New(record As DebtPlus.LINQ.client_www)
        MyClass.New()
        Me.record = record
        RegisterHandlers()
    End Sub

    Private Sub RegisterHandlers()
        AddHandler Me.Load, AddressOf Form_client_www_load
        AddHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
        AddHandler Me.Resize, AddressOf Form_client_www_Resize
        AddHandler ButtonEdit_password.ButtonPressed, AddressOf ButtonEdit_password_ButtonPressed

        AddHandler ComboBoxEdit_question.TextChanged, AddressOf FormChanged
        AddHandler ComboBoxEdit_question.SelectedIndexChanged, AddressOf FormChanged
        AddHandler ButtonEdit_password.TextChanged, AddressOf FormChanged
        AddHandler TextEdit_answer.TextChanged, AddressOf FormChanged
        AddHandler TextEdit_email.TextChanged, AddressOf FormChanged
        AddHandler CheckEdit_approved.CheckedChanged, AddressOf FormChanged
        AddHandler CheckEdit_locked_out.CheckedChanged, AddressOf FormChanged

        If record IsNot Nothing Then
            AddHandler record.PropertyChanged, AddressOf record_PropertyChanged
        End If
    End Sub

    Private Sub UnRegisterHandlers()
        RemoveHandler Me.Load, AddressOf Form_client_www_load
        RemoveHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
        RemoveHandler Me.Resize, AddressOf Form_client_www_Resize
        RemoveHandler ButtonEdit_password.ButtonPressed, AddressOf ButtonEdit_password_ButtonPressed

        RemoveHandler ComboBoxEdit_question.TextChanged, AddressOf FormChanged
        RemoveHandler ComboBoxEdit_question.SelectedIndexChanged, AddressOf FormChanged
        RemoveHandler ButtonEdit_password.TextChanged, AddressOf FormChanged
        RemoveHandler TextEdit_answer.TextChanged, AddressOf FormChanged
        RemoveHandler TextEdit_email.TextChanged, AddressOf FormChanged
        RemoveHandler CheckEdit_approved.CheckedChanged, AddressOf FormChanged
        RemoveHandler CheckEdit_locked_out.CheckedChanged, AddressOf FormChanged

        If record IsNot Nothing Then
            RemoveHandler record.PropertyChanged, AddressOf record_PropertyChanged
        End If
    End Sub

    Private Sub Form_client_www_load(ByVal sender As Object, ByVal e As System.EventArgs)

        UnRegisterHandlers()
        Try
            ComboBoxEdit_question.Properties.Items.Clear()
            ComboBoxEdit_question.Properties.Sorted = False

            ' Load the list of questions
            Dim doc As New System.Xml.XmlDocument()
            Dim asm As Reflection.Assembly = Reflection.Assembly.GetExecutingAssembly()
            Using ios As System.IO.Stream = asm.GetManifestResourceStream("DebtPlus.UI.Client.Security Questions.xml")
                If ios IsNot Nothing Then
                    Using txt As New System.IO.StreamReader(ios)
                        Dim xmlString As String = txt.ReadToEnd()
                        doc.LoadXml(xmlString)
                    End Using
                End If
            End Using

            ' Translate the XML document into our list of questions
            If doc.HasChildNodes Then
                Dim ParentNode As System.Xml.XmlNode = doc.ChildNodes(0)
                For Each node As System.Xml.XmlNode In ParentNode.ChildNodes
                    Dim atr As Xml.XmlAttribute = node.Attributes("text")
                    If atr IsNot Nothing Then
                        Dim questionText As String = atr.Value
                        Dim questionItem As New DebtPlus.Data.Controls.ComboboxItem(questionText, True)
                        ComboBoxEdit_question.Properties.Items.Add(questionItem)
                    End If
                Next
            End If
            doc = Nothing
            ComboBoxEdit_question.Properties.Sorted = True

            ' Set the fields for the client
            LabelControl_last_password_change_date.Text = record.LastPasswordChangeDate.ToShortDateString()
            LabelControl_last_activity.Text = record.LastActivityDate.ToShortDateString()
            LabelControl_last_lockout.Text = If(record.LastLockoutDate.HasValue, record.LastLockoutDate.Value.ToShortDateString(), "NEVER")
            LabelControl_last_login.Text = record.LastLoginDate.ToShortDateString()
            LabelControl_id.Text = record.Id.ToString()
            TextEdit_email.EditValue = record.Email

            ' Find the question in the list of questions.
            Dim q As DebtPlus.Data.Controls.ComboboxItem = ComboBoxEdit_question.Properties.Items.OfType(Of DebtPlus.Data.Controls.ComboboxItem)().Where(Function(s) s.description = If(record.PasswordQuestion, String.Empty)).FirstOrDefault()
            If q IsNot Nothing Then
                ComboBoxEdit_question.SelectedItem = q
            End If

            ' Enable the OK button if there are no errors
            SimpleButton_OK.Enabled = Not HasErrors()
        Finally
            RegisterHandlers()
        End Try
    End Sub

    Protected Sub FormChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        SimpleButton_OK.Enabled = Not HasErrors()
    End Sub

    Protected Overridable Function HasErrors() As Boolean

        ' A security question is required
        If String.IsNullOrWhiteSpace(ComboBoxEdit_question.Text) Then Return True

        ' An email address is required
        If String.IsNullOrWhiteSpace(TextEdit_email.Text) Then Return True

        ' Validate the email address as it was entered
        Dim cls As New DebtPlus.EmailValidation.IsEMail()
        If Not cls.IsEmailValid(TextEdit_email.Text) Then
            Return True
        End If

        Return False
    End Function

    Private Sub Form_client_www_Resize(sender As Object, e As System.EventArgs)
        UnRegisterHandlers()
        Try
            EmptySpaceItem_left.Width = (EmptySpaceItem_left.Width + EmptySpaceItem_right.Width) \ 2
        Finally
            RegisterHandlers()
        End Try
    End Sub

    Private Sub SimpleButton_OK_Click(sender As Object, e As System.EventArgs)

        ' Set the simple fields that can be shown.
        record.IsApproved = CheckEdit_approved.Checked
        record.PasswordQuestion = ComboBoxEdit_question.Text
        record.Email = TextEdit_email.Text

        ' Set the lockout status and the date when the record last locked out.
        If record.IsLockedOut <> CheckEdit_locked_out.Checked Then
            record.IsLockedOut = CheckEdit_locked_out.Checked
            If record.IsLockedOut Then
                record.LastLockoutDate = System.DateTime.UtcNow
            End If
        End If

        ' The password field is more complicated. Update the password with the encrypted entry if needed
        If Not String.IsNullOrWhiteSpace(ButtonEdit_password.Text) Then
            record.Password = Service.www_class.MD5(ButtonEdit_password.Text.Trim())
            record.LastPasswordChangeDate = DateTime.UtcNow
            record.FailedPasswordAttemptCount = 0
            record.FailedPasswordAttemptWindowStart = DateTime.UtcNow
        End If

        ' Do the same to the security question
        If Not String.IsNullOrWhiteSpace(TextEdit_answer.Text) Then
            record.PasswordAnswer = Service.www_class.MD5(TextEdit_answer.Text.Trim())
            record.FailedPasswordAnswerAttemptCount = 0
            record.FailedPasswordAnswerAttemptWindowStart = DateTime.UtcNow
        End If
    End Sub

    ''' <summary>
    ''' Set the lastActivityDate when the data is changed on the current record.
    ''' </summary>
    Private Sub record_PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        UnRegisterHandlers()
        Try
            record.LastActivityDate = DateTime.UtcNow()
        Finally
            RegisterHandlers()
        End Try
    End Sub

    Private Sub ButtonEdit_password_ButtonPressed(sender As Object, e As DevExpress.XtraEditors.Controls.ButtonPressedEventArgs)
        ButtonEdit_password.Text = Service.www_class.CreatePassword()
        SimpleButton_OK.Enabled = Not HasErrors()
    End Sub
End Class