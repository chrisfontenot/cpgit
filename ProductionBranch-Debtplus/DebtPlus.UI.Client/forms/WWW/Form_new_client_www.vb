﻿Imports DebtPlus.LINQ
Imports System.Linq

Public Class Form_new_client_www
    Inherits Form_client_www

    Public Sub New()
        MyBase.New()
        InitializeComponent()
    End Sub

    Public Sub New(ByVal record As client_www)
        MyBase.New(record)
        InitializeComponent()
        AddHandler Me.Load, AddressOf Form_new_client_www_Load
    End Sub

    Protected Overrides Function HasErrors() As Boolean

        ' If we are creating the record then the password is required
        If String.IsNullOrWhiteSpace(ButtonEdit_password.Text) Then
            Return True
        End If

        ' If we are creating the record then a password answer is required
        If String.IsNullOrWhiteSpace(TextEdit_answer.Text) Then
            Return True
        End If

        Return MyBase.HasErrors()
    End Function

    Private Sub Form_new_client_www_Load(sender As Object, e As System.EventArgs)

        ' Create a new password for the client. We just generate one as if the button was pressed.
        ButtonEdit_password.Text = Service.www_class.CreatePassword()

        ' Choose the first security question. One needs to be chosen.
        If ComboBoxEdit_question.Properties.Items.Count > 0 Then
            ComboBoxEdit_question.SelectedIndex = 0
        End If

        SimpleButton_OK.Enabled = Not HasErrors()

    End Sub
End Class