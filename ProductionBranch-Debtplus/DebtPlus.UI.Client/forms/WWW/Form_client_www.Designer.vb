﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_client_www

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If components IsNot Nothing Then components.Dispose()
            End If
            components = Nothing
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim ToolTipSeparatorItem1 As DevExpress.Utils.ToolTipSeparatorItem = New DevExpress.Utils.ToolTipSeparatorItem()
        Dim ToolTipTitleItem2 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim SerializableAppearanceObject1 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Dim SuperToolTip2 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
        Dim ToolTipTitleItem3 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Dim ToolTipItem2 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
        Dim ToolTipSeparatorItem2 As DevExpress.Utils.ToolTipSeparatorItem = New DevExpress.Utils.ToolTipSeparatorItem()
        Dim ToolTipTitleItem4 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.SimpleButton_Cancel = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton_OK = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl_id = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl_last_lockout = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl_last_activity = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl_last_login = New DevExpress.XtraEditors.LabelControl()
        Me.CheckEdit_locked_out = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit_approved = New DevExpress.XtraEditors.CheckEdit()
        Me.TextEdit_answer = New DevExpress.XtraEditors.TextEdit()
        Me.ComboBoxEdit_question = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ButtonEdit_password = New DevExpress.XtraEditors.ButtonEdit()
        Me.TextEdit_email = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem_left = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem_right = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LabelControl_last_password_change_date = New DevExpress.XtraEditors.LabelControl()
        Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem4 = New DevExpress.XtraLayout.EmptySpaceItem()
        CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.CheckEdit_locked_out.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit_approved.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit_answer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit_question.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ButtonEdit_password.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit_email.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem_left, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem_right, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DefaultLookAndFeel1
        '
        Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin"
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.LabelControl_last_password_change_date)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton_Cancel)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton_OK)
        Me.LayoutControl1.Controls.Add(Me.LabelControl_id)
        Me.LayoutControl1.Controls.Add(Me.LabelControl_last_lockout)
        Me.LayoutControl1.Controls.Add(Me.LabelControl_last_activity)
        Me.LayoutControl1.Controls.Add(Me.LabelControl_last_login)
        Me.LayoutControl1.Controls.Add(Me.CheckEdit_locked_out)
        Me.LayoutControl1.Controls.Add(Me.CheckEdit_approved)
        Me.LayoutControl1.Controls.Add(Me.TextEdit_answer)
        Me.LayoutControl1.Controls.Add(Me.ComboBoxEdit_question)
        Me.LayoutControl1.Controls.Add(Me.ButtonEdit_password)
        Me.LayoutControl1.Controls.Add(Me.TextEdit_email)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(388, 289)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'SimpleButton_Cancel
        '
        Me.SimpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.SimpleButton_Cancel.Location = New System.Drawing.Point(203, 251)
        Me.SimpleButton_Cancel.MaximumSize = New System.Drawing.Size(75, 26)
        Me.SimpleButton_Cancel.MinimumSize = New System.Drawing.Size(75, 26)
        Me.SimpleButton_Cancel.Name = "SimpleButton_Cancel"
        Me.SimpleButton_Cancel.Size = New System.Drawing.Size(75, 26)
        Me.SimpleButton_Cancel.StyleController = Me.LayoutControl1
        Me.SimpleButton_Cancel.TabIndex = 15
        Me.SimpleButton_Cancel.Text = "&Cancel"
        '
        'SimpleButton_OK
        '
        Me.SimpleButton_OK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.SimpleButton_OK.Location = New System.Drawing.Point(124, 251)
        Me.SimpleButton_OK.MaximumSize = New System.Drawing.Size(75, 26)
        Me.SimpleButton_OK.MinimumSize = New System.Drawing.Size(75, 26)
        Me.SimpleButton_OK.Name = "SimpleButton_OK"
        Me.SimpleButton_OK.Size = New System.Drawing.Size(75, 26)
        Me.SimpleButton_OK.StyleController = Me.LayoutControl1
        Me.SimpleButton_OK.TabIndex = 14
        Me.SimpleButton_OK.Text = "&OK"
        '
        'LabelControl_id
        '
        Me.LabelControl_id.Location = New System.Drawing.Point(107, 12)
        Me.LabelControl_id.Name = "LabelControl_id"
        Me.LabelControl_id.Size = New System.Drawing.Size(269, 13)
        Me.LabelControl_id.StyleController = Me.LayoutControl1
        Me.LabelControl_id.TabIndex = 13
        '
        'LabelControl_last_lockout
        '
        Me.LabelControl_last_lockout.Location = New System.Drawing.Point(304, 46)
        Me.LabelControl_last_lockout.Name = "LabelControl_last_lockout"
        Me.LabelControl_last_lockout.Size = New System.Drawing.Size(72, 13)
        Me.LabelControl_last_lockout.StyleController = Me.LayoutControl1
        Me.LabelControl_last_lockout.TabIndex = 12
        Me.LabelControl_last_lockout.Text = "MM/MM/MMMM"
        '
        'LabelControl_last_activity
        '
        Me.LabelControl_last_activity.Location = New System.Drawing.Point(107, 29)
        Me.LabelControl_last_activity.Name = "LabelControl_last_activity"
        Me.LabelControl_last_activity.Size = New System.Drawing.Size(72, 13)
        Me.LabelControl_last_activity.StyleController = Me.LayoutControl1
        Me.LabelControl_last_activity.TabIndex = 11
        Me.LabelControl_last_activity.Text = "MM/MM/MMMM"
        '
        'LabelControl_last_login
        '
        Me.LabelControl_last_login.Location = New System.Drawing.Point(107, 46)
        Me.LabelControl_last_login.Name = "LabelControl_last_login"
        Me.LabelControl_last_login.Size = New System.Drawing.Size(72, 13)
        Me.LabelControl_last_login.StyleController = Me.LayoutControl1
        Me.LabelControl_last_login.TabIndex = 10
        Me.LabelControl_last_login.Text = "MM/MM/MMMM"
        '
        'CheckEdit_locked_out
        '
        Me.CheckEdit_locked_out.Location = New System.Drawing.Point(203, 197)
        Me.CheckEdit_locked_out.Name = "CheckEdit_locked_out"
        Me.CheckEdit_locked_out.Properties.Caption = "Locked Out"
        Me.CheckEdit_locked_out.Size = New System.Drawing.Size(173, 20)
        Me.CheckEdit_locked_out.StyleController = Me.LayoutControl1
        Me.CheckEdit_locked_out.TabIndex = 9
        '
        'CheckEdit_approved
        '
        Me.CheckEdit_approved.Location = New System.Drawing.Point(12, 197)
        Me.CheckEdit_approved.Name = "CheckEdit_approved"
        Me.CheckEdit_approved.Properties.Caption = "Approved"
        Me.CheckEdit_approved.Size = New System.Drawing.Size(187, 20)
        Me.CheckEdit_approved.StyleController = Me.LayoutControl1
        Me.CheckEdit_approved.TabIndex = 8
        '
        'TextEdit_answer
        '
        Me.TextEdit_answer.Location = New System.Drawing.Point(107, 154)
        Me.TextEdit_answer.Name = "TextEdit_answer"
        Me.TextEdit_answer.Size = New System.Drawing.Size(269, 20)
        Me.TextEdit_answer.StyleController = Me.LayoutControl1
        ToolTipTitleItem1.Text = "WARNING"
        ToolTipItem1.LeftIndent = 6
        ToolTipItem1.Text = "Do not enter a security answer into this field unless you mean to change the answ" & _
    "er. If you don't want to change the answer then leave this field blank."
        ToolTipTitleItem2.LeftIndent = 6
        ToolTipTitleItem2.Text = "We can not show the client's answer to the security question. We can only change " & _
    "it to a new value."
        SuperToolTip1.Items.Add(ToolTipTitleItem1)
        SuperToolTip1.Items.Add(ToolTipItem1)
        SuperToolTip1.Items.Add(ToolTipSeparatorItem1)
        SuperToolTip1.Items.Add(ToolTipTitleItem2)
        Me.TextEdit_answer.SuperTip = SuperToolTip1
        Me.TextEdit_answer.TabIndex = 7
        '
        'ComboBoxEdit_question
        '
        Me.ComboBoxEdit_question.Location = New System.Drawing.Point(107, 130)
        Me.ComboBoxEdit_question.Name = "ComboBoxEdit_question"
        Me.ComboBoxEdit_question.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit_question.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBoxEdit_question.Size = New System.Drawing.Size(269, 20)
        Me.ComboBoxEdit_question.StyleController = Me.LayoutControl1
        Me.ComboBoxEdit_question.TabIndex = 6
        Me.ComboBoxEdit_question.ToolTip = "Security question used for password recovery"
        '
        'ButtonEdit_password
        '
        Me.ButtonEdit_password.Location = New System.Drawing.Point(107, 82)
        Me.ButtonEdit_password.Name = "ButtonEdit_password"
        Me.ButtonEdit_password.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1, "Generate a new passwod for the client", Nothing, Nothing, True)})
        Me.ButtonEdit_password.Size = New System.Drawing.Size(269, 20)
        Me.ButtonEdit_password.StyleController = Me.LayoutControl1
        ToolTipTitleItem3.Text = "WARNING"
        ToolTipItem2.LeftIndent = 6
        ToolTipItem2.Text = "Do not enter a password into this field unless you mean to change the client's pa" & _
    "ssword. If you don't want to change the password, then leave this field blank."
        ToolTipTitleItem4.LeftIndent = 6
        ToolTipTitleItem4.Text = "We can not show the current password for the client. We can only change it."
        SuperToolTip2.Items.Add(ToolTipTitleItem3)
        SuperToolTip2.Items.Add(ToolTipItem2)
        SuperToolTip2.Items.Add(ToolTipSeparatorItem2)
        SuperToolTip2.Items.Add(ToolTipTitleItem4)
        Me.ButtonEdit_password.SuperTip = SuperToolTip2
        Me.ButtonEdit_password.TabIndex = 4
        '
        'TextEdit_email
        '
        Me.TextEdit_email.EditValue = ""
        Me.TextEdit_email.Location = New System.Drawing.Point(107, 106)
        Me.TextEdit_email.Name = "TextEdit_email"
        Me.TextEdit_email.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Underline)
        Me.TextEdit_email.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.TextEdit_email.Properties.Appearance.Options.UseFont = True
        Me.TextEdit_email.Properties.Appearance.Options.UseForeColor = True
        Me.TextEdit_email.Size = New System.Drawing.Size(269, 20)
        Me.TextEdit_email.StyleController = Me.LayoutControl1
        Me.TextEdit_email.TabIndex = 5
        Me.TextEdit_email.ToolTip = "Client E-mail address used for password recovery."
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.LayoutControlItem6, Me.LayoutControlItem11, Me.LayoutControlItem12, Me.LayoutControlItem7, Me.EmptySpaceItem_left, Me.EmptySpaceItem_right, Me.EmptySpaceItem3, Me.EmptySpaceItem1, Me.LayoutControlItem10, Me.LayoutControlItem8, Me.EmptySpaceItem2, Me.LayoutControlItem13, Me.LayoutControlItem9, Me.EmptySpaceItem4})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(388, 289)
        Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.ButtonEdit_password
        Me.LayoutControlItem1.CustomizationFormText = "Password"
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 70)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(368, 24)
        Me.LayoutControlItem1.Text = "Password"
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(92, 13)
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.TextEdit_email
        Me.LayoutControlItem2.CustomizationFormText = "E-Mail address"
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 94)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(368, 24)
        Me.LayoutControlItem2.Text = "E-Mail address"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(92, 13)
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.ComboBoxEdit_question
        Me.LayoutControlItem3.CustomizationFormText = "Question"
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 118)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(368, 24)
        Me.LayoutControlItem3.Text = "Question"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(92, 13)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.TextEdit_answer
        Me.LayoutControlItem4.CustomizationFormText = "Answer"
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 142)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(368, 24)
        Me.LayoutControlItem4.Text = "Answer"
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(92, 13)
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.CheckEdit_approved
        Me.LayoutControlItem5.CustomizationFormText = "Approved"
        Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 185)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(191, 24)
        Me.LayoutControlItem5.Text = "Approved"
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem5.TextToControlDistance = 0
        Me.LayoutControlItem5.TextVisible = False
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.CheckEdit_locked_out
        Me.LayoutControlItem6.CustomizationFormText = "Locked Out"
        Me.LayoutControlItem6.Location = New System.Drawing.Point(191, 185)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(177, 24)
        Me.LayoutControlItem6.Text = "Locked Out"
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem6.TextToControlDistance = 0
        Me.LayoutControlItem6.TextVisible = False
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.Control = Me.SimpleButton_OK
        Me.LayoutControlItem11.CustomizationFormText = "LayoutControlItem11"
        Me.LayoutControlItem11.Location = New System.Drawing.Point(112, 239)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(79, 30)
        Me.LayoutControlItem11.Text = "LayoutControlItem11"
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem11.TextToControlDistance = 0
        Me.LayoutControlItem11.TextVisible = False
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.Control = Me.SimpleButton_Cancel
        Me.LayoutControlItem12.CustomizationFormText = "LayoutControlItem12"
        Me.LayoutControlItem12.Location = New System.Drawing.Point(191, 239)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(79, 30)
        Me.LayoutControlItem12.Text = "LayoutControlItem12"
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem12.TextToControlDistance = 0
        Me.LayoutControlItem12.TextVisible = False
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.LabelControl_last_login
        Me.LayoutControlItem7.CustomizationFormText = "Last Login"
        Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 34)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(171, 17)
        Me.LayoutControlItem7.Text = "Last Login"
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(92, 13)
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.Control = Me.LabelControl_last_lockout
        Me.LayoutControlItem9.CustomizationFormText = "Last Lockout"
        Me.LayoutControlItem9.Location = New System.Drawing.Point(197, 34)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(171, 17)
        Me.LayoutControlItem9.Text = "Last Lockout"
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(92, 13)
        '
        'EmptySpaceItem_left
        '
        Me.EmptySpaceItem_left.AllowHotTrack = False
        Me.EmptySpaceItem_left.CustomizationFormText = "EmptySpaceItem_left"
        Me.EmptySpaceItem_left.Location = New System.Drawing.Point(0, 239)
        Me.EmptySpaceItem_left.Name = "EmptySpaceItem_left"
        Me.EmptySpaceItem_left.Size = New System.Drawing.Size(112, 30)
        Me.EmptySpaceItem_left.Text = "EmptySpaceItem_left"
        Me.EmptySpaceItem_left.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem_right
        '
        Me.EmptySpaceItem_right.AllowHotTrack = False
        Me.EmptySpaceItem_right.CustomizationFormText = "EmptySpaceItem_right"
        Me.EmptySpaceItem_right.Location = New System.Drawing.Point(270, 239)
        Me.EmptySpaceItem_right.Name = "EmptySpaceItem_right"
        Me.EmptySpaceItem_right.Size = New System.Drawing.Size(98, 30)
        Me.EmptySpaceItem_right.Text = "EmptySpaceItem_right"
        Me.EmptySpaceItem_right.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem3
        '
        Me.EmptySpaceItem3.AllowHotTrack = False
        Me.EmptySpaceItem3.CustomizationFormText = "EmptySpaceItem3"
        Me.EmptySpaceItem3.Location = New System.Drawing.Point(0, 209)
        Me.EmptySpaceItem3.Name = "EmptySpaceItem3"
        Me.EmptySpaceItem3.Size = New System.Drawing.Size(368, 30)
        Me.EmptySpaceItem3.Text = "EmptySpaceItem3"
        Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 166)
        Me.EmptySpaceItem1.MaxSize = New System.Drawing.Size(0, 19)
        Me.EmptySpaceItem1.MinSize = New System.Drawing.Size(10, 19)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(368, 19)
        Me.EmptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.Control = Me.LabelControl_id
        Me.LayoutControlItem10.CustomizationFormText = "ID"
        Me.LayoutControlItem10.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(368, 17)
        Me.LayoutControlItem10.Text = "ID"
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(92, 13)
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.LabelControl_last_activity
        Me.LayoutControlItem8.CustomizationFormText = "Last Activity"
        Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 17)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(171, 17)
        Me.LayoutControlItem8.Text = "Last Activity"
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(92, 13)
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = False
        Me.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(0, 51)
        Me.EmptySpaceItem2.MaxSize = New System.Drawing.Size(0, 19)
        Me.EmptySpaceItem2.MinSize = New System.Drawing.Size(10, 19)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(368, 19)
        Me.EmptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.EmptySpaceItem2.Text = "EmptySpaceItem2"
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'LabelControl_last_password_change_date
        '
        Me.LabelControl_last_password_change_date.Location = New System.Drawing.Point(304, 29)
        Me.LabelControl_last_password_change_date.Name = "LabelControl_last_password_change_date"
        Me.LabelControl_last_password_change_date.Size = New System.Drawing.Size(72, 13)
        Me.LabelControl_last_password_change_date.StyleController = Me.LayoutControl1
        Me.LabelControl_last_password_change_date.TabIndex = 16
        Me.LabelControl_last_password_change_date.Text = "MM/MM/MMMM"
        '
        'LayoutControlItem13
        '
        Me.LayoutControlItem13.Control = Me.LabelControl_last_password_change_date
        Me.LayoutControlItem13.CustomizationFormText = "Password Changed"
        Me.LayoutControlItem13.Location = New System.Drawing.Point(197, 17)
        Me.LayoutControlItem13.Name = "LayoutControlItem13"
        Me.LayoutControlItem13.Size = New System.Drawing.Size(171, 17)
        Me.LayoutControlItem13.Text = "Password Changed"
        Me.LayoutControlItem13.TextSize = New System.Drawing.Size(92, 13)
        '
        'EmptySpaceItem4
        '
        Me.EmptySpaceItem4.AllowHotTrack = False
        Me.EmptySpaceItem4.CustomizationFormText = "EmptySpaceItem4"
        Me.EmptySpaceItem4.Location = New System.Drawing.Point(171, 17)
        Me.EmptySpaceItem4.Name = "EmptySpaceItem4"
        Me.EmptySpaceItem4.Size = New System.Drawing.Size(26, 34)
        Me.EmptySpaceItem4.Text = "EmptySpaceItem4"
        Me.EmptySpaceItem4.TextSize = New System.Drawing.Size(0, 0)
        '
        'Form_client_www
        '
        Me.AcceptButton = Me.SimpleButton_OK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.SimpleButton_Cancel
        Me.ClientSize = New System.Drawing.Size(388, 289)
        Me.Controls.Add(Me.LayoutControl1)
        Me.Name = "Form_client_www"
        Me.Text = "Client Web Access"
        CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.CheckEdit_locked_out.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit_approved.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit_answer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit_question.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ButtonEdit_password.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit_email.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem_left, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem_right, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Protected WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Protected WithEvents SimpleButton_Cancel As DevExpress.XtraEditors.SimpleButton
    Protected WithEvents SimpleButton_OK As DevExpress.XtraEditors.SimpleButton
    Protected WithEvents LabelControl_id As DevExpress.XtraEditors.LabelControl
    Protected WithEvents LabelControl_last_lockout As DevExpress.XtraEditors.LabelControl
    Protected WithEvents LabelControl_last_activity As DevExpress.XtraEditors.LabelControl
    Protected WithEvents LabelControl_last_login As DevExpress.XtraEditors.LabelControl
    Protected WithEvents CheckEdit_locked_out As DevExpress.XtraEditors.CheckEdit
    Protected WithEvents CheckEdit_approved As DevExpress.XtraEditors.CheckEdit
    Protected WithEvents TextEdit_answer As DevExpress.XtraEditors.TextEdit
    Protected WithEvents ComboBoxEdit_question As DevExpress.XtraEditors.ComboBoxEdit
    Protected WithEvents ButtonEdit_password As DevExpress.XtraEditors.ButtonEdit
    Protected WithEvents TextEdit_email As DevExpress.XtraEditors.TextEdit
    Protected WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Protected WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Protected WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Protected WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Protected WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Protected WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Protected WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Protected WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Protected WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Protected WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Protected WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Protected WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Protected WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Protected WithEvents EmptySpaceItem_left As DevExpress.XtraLayout.EmptySpaceItem
    Protected WithEvents EmptySpaceItem_right As DevExpress.XtraLayout.EmptySpaceItem
    Protected WithEvents EmptySpaceItem3 As DevExpress.XtraLayout.EmptySpaceItem
    Protected WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LabelControl_last_password_change_date As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem4 As DevExpress.XtraLayout.EmptySpaceItem
End Class
