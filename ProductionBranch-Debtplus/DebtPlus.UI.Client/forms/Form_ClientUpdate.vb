#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Data.Forms
Imports DebtPlus.Interfaces.Client
Imports DebtPlus.Interfaces.Reports
Imports DebtPlus.LINQ
Imports DebtPlus.Reports
Imports DebtPlus.Svc.Client.Appointments
Imports DebtPlus.Svc.DataSets
Imports DebtPlus.Svc.Debt
Imports DebtPlus.UI.Client.forms.Appointments
Imports DebtPlus.UI.Client.forms.Housing
Imports DebtPlus.UI.Client.pages
Imports DebtPlus.UI.Client.Service
Imports DebtPlus.UI.Client.Ticklers
Imports DebtPlus.Utils
Imports DevExpress.XtraBars
Imports DevExpress.XtraEditors
Imports DevExpress.XtraTab
Imports DevExpress.XtraTab.ViewInfo
Imports System
Imports System.ComponentModel
Imports System.Data.SqlClient
Imports System.Linq
Imports System.Threading

Namespace forms
    Friend Class Form_ClientUpdate
        Implements IContext

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Private privateContext As ClientUpdateClass
        Private bc As BusinessContext = Nothing
        Friend Property isCreated As Boolean = False

        ''' <summary>
        ''' Initialize the form
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        ''' <summary>
        ''' Initialize the form
        ''' </summary>
        Public Sub New(ByVal Context As ClientUpdateClass)
            MyClass.New()
            privateContext = Context
            bc = Context.ClientDs.bc
            RegisterHandlers()
        End Sub

        Public ReadOnly Property Context() As ClientUpdateClass Implements IContext.Context
            Get
                Return privateContext
            End Get
        End Property

        ''' <summary>
        ''' Register the events for this form
        ''' </summary>
        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf ClientUpdateForm_Load
            AddHandler Closing, AddressOf ClientUpdateForm_Closing
            AddHandler Resize, AddressOf ClientUpdateForm_Resize

            AddHandler XtraTabControl1.SelectedPageChanging, AddressOf XtraTabControl1_SelectedPageChanging
            AddHandler XtraTabControl1.SelectedPageChanged, AddressOf XtraTabControl1_SelectedPageChanged
            AddHandler BarButtonItem_File_Exit.ItemClick, AddressOf File_Exit_Click
            AddHandler BarButtonItem_WWW_Messages.ItemClick, AddressOf BarButtonItem_WWW_Messages_Click
            AddHandler BarButtonItem_WWW_Account.ItemClick, AddressOf BarButtonItem_WWW_Account_Click
            AddHandler BarSubItem_View.Popup, AddressOf MenuItem_View_Popup
            AddHandler XtraTabControl1.MouseMove, AddressOf XtraTabControl1_MouseMove
            AddHandler XtraTabControl1.DragEnter, AddressOf XtraTabControl1_DragEnter
            AddHandler XtraTabControl1.DragDrop, AddressOf XtraTabControl1_DragDrop
            AddHandler BarSubItem_Reports.Popup, AddressOf BarSubItem_Reports_Popup
            AddHandler BarSubItem_Letters.Popup, AddressOf BarSubItem_Letters_Popup
            AddHandler BarButtonItem_Appointments_List.ItemClick, AddressOf BarButtonItem_Appointments_List_ItemClick
            AddHandler BarButtonItem_Appointments_New.ItemClick, AddressOf BarButtonItem_Appointments_New_ItemClick
            AddHandler BarButtonItem_Appointments_Result.ItemClick, AddressOf BarButtonItem_Appointments_Result_ItemClick
            AddHandler BarButtonItem_Appointments_Reschedule.ItemClick, AddressOf BarButtonItem_Appointments_Reschedule_ItemClick
            AddHandler BarButtonItem_Appointments_Cancel.ItemClick, AddressOf BarButtonItem_Appointments_Cancel_ItemClick
            AddHandler BarButtonItem_Appointments_Confirm.ItemClick, AddressOf BarButtonItem_Appointments_Confirm_ItemClick
            AddHandler BarButtonItem_Appointments_Walkin.ItemClick, AddressOf BarButtonItem_Appointments_Walkin_ItemClick
            AddHandler BarButtonItem_Appointments_HUD.ItemClick, AddressOf BarButtonItem_Appointments_HUD_ItemClick
            AddHandler BarButtonItem_View_BottomLine.ItemClick, AddressOf BarButtonItem_View_BottomLine_ItemClick
            AddHandler BarButtonItem_View_Ticklers.ItemClick, AddressOf BarButtonItem_View_Ticklers_ItemClick
            AddHandler BarButtonItem_Help_About.ItemClick, AddressOf BarButtonItem_Help_About_ItemClick
            AddHandler BarButtonItem_Trust_CreditorThreshold.ItemClick, AddressOf BarButtonItem_Trust_CreditorThreshold_ItemClick
            AddHandler BarButtonItem_Trust_ReservedInTrust.ItemClick, AddressOf BarButtonItem_Trust_ReservedInTrust_ItemClick
            AddHandler BarButtonItem_Creditor_ChgDepositAmt.ItemClick, AddressOf BarButtonItem_Creditor_ChgDepositAmt_ItemClick
            AddHandler BarButtonItem_Creditor_Note.ItemClick, AddressOf BarButtonItem_Creditor_Note_ItemClick
            AddHandler BarButtonItem_UploadHPF.ItemClick, AddressOf BarButtonItem_UploadHPF_ItemClick
            AddHandler BarSubItem_Creditor_View.Popup, AddressOf BarSubItem_Creditor_View_Popup
            AddHandler BarButtonItem_Creditor_SchedPay.ItemClick, AddressOf BarButtonItem_Creditor_SchedPay_ItemClick
            AddHandler BarSubItem_Upload.Popup, AddressOf BarSubItem_Upload_Popup

            If Page_Person1 IsNot Nothing Then
                AddHandler Page_Person1.Changed, AddressOf Recalculate_BottomLine
            End If

            If Page_Person2 IsNot Nothing Then
                AddHandler Page_Person2.Changed, AddressOf Recalculate_BottomLine
            End If

            If Page_OtherDebt1 IsNot Nothing Then
                AddHandler Page_OtherDebt1.Changed, AddressOf Recalculate_BottomLine
            End If

            If Page_Deposits1 IsNot Nothing Then
                AddHandler Page_Deposits1.PropertyChanged, AddressOf UpdateStatusBar
            End If

            If Page_DMP1 IsNot Nothing Then
                AddHandler Page_DMP1.PropertyChanged, AddressOf UpdateStatusBar
            End If
        End Sub

        ''' <summary>
        ''' Remove the event registration on this form
        ''' </summary>
        Private Sub UnRegisterHandlers()
            RemoveHandler Load, AddressOf ClientUpdateForm_Load
            RemoveHandler Closing, AddressOf ClientUpdateForm_Closing
            RemoveHandler Resize, AddressOf ClientUpdateForm_Resize

            RemoveHandler XtraTabControl1.SelectedPageChanging, AddressOf XtraTabControl1_SelectedPageChanging
            RemoveHandler XtraTabControl1.SelectedPageChanged, AddressOf XtraTabControl1_SelectedPageChanged
            RemoveHandler BarButtonItem_File_Exit.ItemClick, AddressOf File_Exit_Click
            RemoveHandler BarButtonItem_WWW_Messages.ItemClick, AddressOf BarButtonItem_WWW_Messages_Click
            RemoveHandler BarButtonItem_WWW_Account.ItemClick, AddressOf BarButtonItem_WWW_Account_Click
            RemoveHandler BarSubItem_View.Popup, AddressOf MenuItem_View_Popup
            RemoveHandler XtraTabControl1.MouseMove, AddressOf XtraTabControl1_MouseMove
            RemoveHandler XtraTabControl1.DragEnter, AddressOf XtraTabControl1_DragEnter
            RemoveHandler XtraTabControl1.DragDrop, AddressOf XtraTabControl1_DragDrop
            RemoveHandler BarSubItem_Reports.Popup, AddressOf BarSubItem_Reports_Popup
            RemoveHandler BarSubItem_Letters.Popup, AddressOf BarSubItem_Letters_Popup
            RemoveHandler BarButtonItem_Appointments_List.ItemClick, AddressOf BarButtonItem_Appointments_List_ItemClick
            RemoveHandler BarButtonItem_Appointments_New.ItemClick, AddressOf BarButtonItem_Appointments_New_ItemClick
            RemoveHandler BarButtonItem_Appointments_Result.ItemClick, AddressOf BarButtonItem_Appointments_Result_ItemClick
            RemoveHandler BarButtonItem_Appointments_Reschedule.ItemClick, AddressOf BarButtonItem_Appointments_Reschedule_ItemClick
            RemoveHandler BarButtonItem_Appointments_Cancel.ItemClick, AddressOf BarButtonItem_Appointments_Cancel_ItemClick
            RemoveHandler BarButtonItem_Appointments_Confirm.ItemClick, AddressOf BarButtonItem_Appointments_Confirm_ItemClick
            RemoveHandler BarButtonItem_Appointments_Walkin.ItemClick, AddressOf BarButtonItem_Appointments_Walkin_ItemClick
            RemoveHandler BarButtonItem_Appointments_HUD.ItemClick, AddressOf BarButtonItem_Appointments_HUD_ItemClick
            RemoveHandler BarButtonItem_View_BottomLine.ItemClick, AddressOf BarButtonItem_View_BottomLine_ItemClick
            RemoveHandler BarButtonItem_View_Ticklers.ItemClick, AddressOf BarButtonItem_View_Ticklers_ItemClick
            RemoveHandler BarButtonItem_Help_About.ItemClick, AddressOf BarButtonItem_Help_About_ItemClick
            RemoveHandler BarButtonItem_Trust_CreditorThreshold.ItemClick, AddressOf BarButtonItem_Trust_CreditorThreshold_ItemClick
            RemoveHandler BarButtonItem_Trust_ReservedInTrust.ItemClick, AddressOf BarButtonItem_Trust_ReservedInTrust_ItemClick
            RemoveHandler BarButtonItem_Creditor_ChgDepositAmt.ItemClick, AddressOf BarButtonItem_Creditor_ChgDepositAmt_ItemClick
            RemoveHandler BarButtonItem_Creditor_Note.ItemClick, AddressOf BarButtonItem_Creditor_Note_ItemClick
            RemoveHandler BarButtonItem_UploadHPF.ItemClick, AddressOf BarButtonItem_UploadHPF_ItemClick
            RemoveHandler BarSubItem_Creditor_View.Popup, AddressOf BarSubItem_Creditor_View_Popup
            RemoveHandler BarButtonItem_Creditor_SchedPay.ItemClick, AddressOf BarButtonItem_Creditor_SchedPay_ItemClick
            RemoveHandler BarSubItem_Upload.Popup, AddressOf BarSubItem_Upload_Popup

            If Page_Person1 IsNot Nothing Then
                RemoveHandler Page_Person1.Changed, AddressOf Recalculate_BottomLine
            End If

            If Page_Person2 IsNot Nothing Then
                RemoveHandler Page_Person2.Changed, AddressOf Recalculate_BottomLine
            End If

            If Page_OtherDebt1 IsNot Nothing Then
                RemoveHandler Page_OtherDebt1.Changed, AddressOf Recalculate_BottomLine
            End If

            If Page_Deposits1 IsNot Nothing Then
                RemoveHandler Page_Deposits1.PropertyChanged, AddressOf UpdateStatusBar
            End If

            If Page_DMP1 IsNot Nothing Then
                RemoveHandler Page_DMP1.PropertyChanged, AddressOf UpdateStatusBar
            End If
        End Sub

#Region " Form "

#Region "Status Bar"
        ''' <summary>
        ''' This is an obsolete function to filter the status bar update events. It is not called.
        ''' </summary>
        ''' <param name="Sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub StatusBar_HandlerFunction(ByVal Sender As Object, ByVal e As DebtPlus.Events.FieldChangedEventArgs)
            Select Case e.FieldName
                Case "form.Name"
                    BarClientName.Caption = Convert.ToString(e.NewValue)

                Case "active_status", "client_status", "view_debt_info:disbursement_factor"
                    UpdateStatusBar()
            End Select
        End Sub

        ''' <summary>
        ''' Provide a handler for the event processing. It just calls the main function to completely update the status bar.
        ''' </summary>
        Private Sub UpdateStatusBar(sender As Object, e As System.EventArgs)
            UpdateStatusBar()
        End Sub

        ''' <summary>
        ''' Update the status bar to reflect the contents of the client storage
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub UpdateStatusBar()

            ' Set the client ID panel
            BarClientID.EditValue = String.Format("{0:0000000}", Context.ClientDs.ClientId)

            ' Find the client name
            If Context.ClientDs.applicantRecord IsNot Nothing Then
                Dim nameRecord As DebtPlus.Interfaces.Names.IName = Context.ClientDs.GetNameByID(Context.ClientDs.applicantRecord.NameID)
                If nameRecord IsNot Nothing Then
                    BarClientName.Caption = nameRecord.ToString()
                End If
            End If

            BarActiveStatus.Caption = Context.ClientDs.clientRecord.active_status
            BarClientStatus.Caption = Context.ClientDs.clientRecord.client_status

            ' Find the client fields
            Dim ACHstring As String = String.Empty
            If Context.ClientDs.clientACHRecord IsNot Nothing AndAlso Context.ClientDs.clientACHRecord.isActive Then
                ACHstring = "ACH"
            End If

            ' Set the ACH status accordingly. It is only visible if it has a value.
            BarACHStatus.Caption = ACHstring
            If ACHstring <> String.Empty Then
                BarACHStatus.Visibility = BarItemVisibility.Always
            Else
                BarACHStatus.Visibility = BarItemVisibility.OnlyInCustomizing
            End If

            ' Find the debt for the client fee
            Dim FeeDebt As DebtRecord = Context.DebtRecords.Cast(Of DebtRecord).Where(Function(s) s.DebtType = Interfaces.Debt.DebtTypeEnum.MonthlyFee).FirstOrDefault()
            Dim FeeAmount As Decimal = If(FeeDebt Is Nothing, 0D, FeeDebt.disbursement_factor)
            BarMonthlyFee.Caption = String.Format("{0:c}", FeeAmount)
        End Sub
#End Region

#Region " Page References "

        Friend WithEvents Page_General1 As Page_General = Nothing
        Friend WithEvents Page_Person1 As Page_Person = Nothing
        Friend WithEvents Page_Person2 As Page_Person = Nothing
        Friend WithEvents Page_Assets1 As Page_Assets = Nothing
        Friend WithEvents Page_Taxes1 As Page_Taxes = Nothing
        Friend WithEvents Page_Budgets1 As Page_Budgets = Nothing
        Friend WithEvents Page_Aliases1 As Page_Aliases = Nothing
        Friend WithEvents Page_ActionPlan1 As Page_ActionPlan = Nothing
        Friend WithEvents Page_Deposits1 As Page_Deposits = Nothing
        Friend WithEvents Page_SecuredProperty1 As Page_SecuredProperty = Nothing
        Friend WithEvents Page_DMP1 As Page_DMP = Nothing
        Friend WithEvents Page_Notes1 As Page_Notes = Nothing
        Friend WithEvents Page_OtherDebt1 As Page_OtherDebt = Nothing
        Friend WithEvents Page_Disclosure1 As Page_Disclosure = Nothing
        Friend WithEvents Page_Payments1 As Page_Payments = Nothing
        Friend WithEvents Page_Retention1 As Page_Retention = Nothing
        Friend WithEvents Page_Debts1 As Page_Debts = Nothing
        Friend WithEvents Page_Comparison1 As Page_Comparison = Nothing
        Friend WithEvents Page_Housing1 As Page_Housing = Nothing
        Friend WithEvents Page_Notices1 As Page_Notices = Nothing
        Friend WithEvents Page_Products1 As Page_Products = Nothing

#End Region

#Region "drag/drop"
#If False Then
        ''' <summary>
        ''' Handle the mouse movement in the drag/drop condition
        ''' </summary>
        Protected Sub Form_ClientUpdate_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs)

            ' Determine if the mouse is over the field
            Dim pt As System.Drawing.Point = PointToClient(e.Location)
            ' Do nothing if the operation is not valid
            If ((e.Button = 0) OrElse (e.X >= 0 AndAlso e.X < Width AndAlso e.Y >= 0 AndAlso e.Y < Height)) Then
                Return
            End If

            ' Find the client for the item
            Dim txt As String = BarClientID.Caption

            ' Start a drag-drop operation when the mouse moves outside the control
            Dim dobj As New DataObject()
            dobj.SetData(DataFormats.Text, True, txt)

            ' Do the operation. It will return when the operation is complete.
            Dim effect As DragDropEffects = DragDropEffects.Copy
            effect = DoDragDrop(dobj, effect)
        End Sub

        ''' <summary>
        ''' Process the start of the drag/drop
        ''' </summary>
        Protected Sub BarClientID_OnDragEnter(ByVal sender As Object, ByVal e As DragEventArgs)
            e.Effect = DragDropEffects.None
        End Sub

        ''' <summary>
        ''' Determine if the drag event should be canceled.
        ''' </summary>
        Protected Sub BarClientID_OnQueryContinueDrag(ByVal sender As Object, ByVal e As QueryContinueDragEventArgs)

            ' if the escape key is pressed then cancel the operation
            If (e.EscapePressed) Then
                e.Action = DragAction.Cancel
                Return
            End If

            MyBase.OnQueryContinueDrag(e)
        End Sub
#End If
#End Region

        ''' <summary>
        ''' Replace the text name with the string value
        ''' </summary>
        Private Sub ClientUpdateForm_Resize(ByVal sender As Object, ByVal e As EventArgs)
            If Context IsNot Nothing Then
                If WindowState = FormWindowState.Minimized Then
                    Text = String.Format("{0:0000000}", Context.ClientId)
                Else
                    Text = "DebtPlus " + String.Format("{0:0000000}", Context.ClientId)
                End If
            End If
        End Sub

        ''' <summary>
        ''' Process the FORM : LOAD event
        ''' </summary>
        Private Sub ClientUpdateForm_Load(ByVal sender As Object, ByVal e As EventArgs)
            log.Debug("Entry to ClientUpdateForm_Load")

            ' Remove the handlers, do the initialization and then re-register them
            UnRegisterHandlers()
            Try

                ' Restore the form position
                LoadPlacement("Client.Update")
                log.Debug("LoadPlacement Return")

                ' Load the menus
                Load_View_MenuItems()
                log.Debug("Load_View_MenuItems Return")

                ' Load the list of deposit records
                Context.ClientDs.colClientDeposits = bc.client_deposits.Where(Function(s) s.client = Context.ClientDs.ClientId).ToList()
                log.Debug("colClientDeposits Read")

                ' List of other debts for the client that are not managed
                Context.ClientDs.colOtherDebts = Context.ClientDs.bc.client_other_debts.Where(Function(s) s.client = Context.ClientDs.clientRecord.Id).ToList()
                log.Debug("colOtherDebts Read")

                ' Load the client home address
                Context.ClientDs.clientAddress = If(Context.ClientDs.clientRecord.AddressID.HasValue, Context.ClientDs.bc.addresses.Where(Function(s) s.Id = Context.ClientDs.clientRecord.AddressID.Value).FirstOrDefault(), Nothing)
                log.Debug("clientAddress Read")

                ' If there is no address then generate a blank one. We need an address record, even if it is empty.
                If Context.ClientDs.clientAddress Is Nothing Then
                    Context.ClientDs.clientAddress = DebtPlus.LINQ.Factory.Manufacture_address()
                End If

                ' Add the ACH information into the list at this point
                Context.ClientDs.clientACHRecord = Context.ClientDs.bc.client_aches.Where(Function(s) s.Id = Context.ClientDs.clientRecord.Id).FirstOrDefault()
                log.Debug("ACH data Read")

                ' We need to define an item for the reports menu so that the popup logic will work.
                ' You won't get a popup event until there are items to be shown
                Dim newItem As New BarButtonItem(barManager1, "Temporary")
                BarSubItem_Reports.AddItem(newItem)
                log.Debug("BarSubItem_Reports Created")

                ' Do the same with the letters
                newItem = New BarButtonItem(barManager1, "Temporary")
                BarSubItem_Letters.AddItem(newItem)
                log.Debug("BarSubItem_Letters Created")

                ' Update the creditor menu to include the payout information
                Dim rpt As DebtPlus.LINQ.report
                rpt = DebtPlus.LINQ.Factory.Manufacture_report()
                rpt.Argument = String.Empty
                rpt.ClassName = "DebtPlus.Reports.Client.Payout.Detail"
                Dim newBandItem As New PayoutReportsMenuItem(Context, "Payout...", rpt)

                AddHandler newBandItem.ItemClick, AddressOf Reports_ItemClick
                AddHandler newBandItem.Complete, AddressOf PayoutComplete
                newBandItem.Manager = barManager1
                newBandItem.Name = "Reports_" + Guid.NewGuid().ToString()
                BarSubItem_Creditor.AddItem(newBandItem)

                log.Debug("DebtPlus.Reports.Client.Payout.Detail Created")

                ' Read the list of debts for this client. It is needed for the remainder of the form.
                Dim newDebtList As New ClientUpdateDebtRecordList()
                log.Debug("newDebtList Created")
                DirectCast(newDebtList, System.ComponentModel.ISupportInitialize).BeginInit()
                Try
                    newDebtList.ReadDebtsTable(Context.ClientDs.ClientId)
                    log.Debug("newDebtList Read")
                Finally
                    DirectCast(newDebtList, System.ComponentModel.ISupportInitialize).EndInit()
                End Try
                Context.DebtRecords = newDebtList

                ' Ensure that the form has the proper value and not the default setting.
                Text = "DebtPlus " + String.Format("{0:0000000}", Context.ClientId)

                ' Change the page to the "Applicant" information
                XtraTabControl1.SelectedTabPage = XtraTabPage_Person1
                log.Debug("XtraTabPage_Person1 Selected")
                LoadNewTabPage()

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading client information")

            Finally
                RegisterHandlers()
                log.Debug("RegisterHandlers returned")
            End Try

            ' Register the status bar to receive the update events for table changes and update the bar the first time
            Context.ClientDs.RegisterHandler(AddressOf StatusBar_HandlerFunction)
            log.Debug("Context.ClientDs.RegisterHandler returned")

            UpdateStatusBar()
            log.Debug("UpdateStatusBar returned")
        End Sub

        ''' <summary>
        ''' The payout report was printed. Update the debts
        ''' </summary>
        Private Sub PayoutComplete(ByVal Sender As Object, ByVal e As EventArgs)
            If InvokeRequired Then
                EndInvoke(BeginInvoke(New MethodInvoker(AddressOf UpdatePayoutDebts)))
                Return
            End If

            UpdatePayoutDebts()
        End Sub

        ''' <summary>
        ''' The payout report was printed. Update the debts
        ''' </summary>
        Private Sub UpdatePayoutDebts()
            Context.DebtRecords.SaveData(True)
        End Sub

        ''' <summary>
        ''' Process the FORM : CLOSING event
        ''' </summary>
        Private Sub ClientUpdateForm_Closing(ByVal sender As Object, ByVal e As CancelEventArgs)

            If Not DebtPlus.Configuration.DesignMode.IsInDesignMode Then
                UnRegisterHandlers()

                ' Find the active control for the form
                Dim ctl As Control = ActiveControl
                If ctl IsNot Nothing Then
                    SelectNextControl(ctl, True, False, True, True)
                End If

                ' Fire the final save event
                Dim currentpage As DebtPlus.UI.Client.controls.ControlBaseClient = FindCurrentPage()
                If currentpage IsNot Nothing Then currentpage.SaveForm(bc)

                ' Close the background BottomLine form
                Form_BottomLine.Instance(Context).Close()
            End If
        End Sub

        ''' <summary>
        ''' Process the FILE -> EXIT : CLICK event
        ''' </summary>
        Private Sub File_Exit_Click(ByVal sender As Object, ByVal e As EventArgs)
            DialogResult = DialogResult.OK
            If Not Modal Then Close()
        End Sub

#End Region

#Region " TabControl "

        ''' <summary>
        ''' Process the Page change event for the tab control
        ''' </summary>
        Private Sub XtraTabControl1_SelectedPageChanging(ByVal sender As Object, ByVal e As TabPageChangingEventArgs)
            If Not SaveChanges() Then
                e.Cancel = True
            End If
        End Sub

        ''' <summary>
        ''' Force a save event to occur on the page data
        ''' </summary>
        Public Sub ThreadSafeSaveChanges()
            If InvokeRequired Then
                EndInvoke(BeginInvoke(New MethodInvoker(AddressOf ThreadSafeSaveChanges)))
                Return
            End If

            SaveChanges()
        End Sub

        Public Function SaveChanges() As Boolean

            Dim currentPage As DebtPlus.UI.Client.controls.ControlBaseClient = FindCurrentPage()
            If currentPage IsNot Nothing Then
                Try
                    currentPage.SaveForm(bc)
                    Context.SaveData()

                Catch ex As System.Data.SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating database")
                    Return False

                Catch ex As ApplicationException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Form validation error")
                    Return False
                End Try
            End If

            Return True
        End Function

        ''' <summary>
        ''' Process the Page change event for the tab control
        ''' </summary>
        Private Sub XtraTabControl1_SelectedPageChanged(ByVal sender As Object, ByVal e As TabPageChangedEventArgs)
            UnRegisterHandlers()
            Try
                LoadNewTabPage()
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Sub LoadNewTabPage()
            log.Debug("LoadNewTabPage called")
            Dim tabPage As XtraTabPage = XtraTabControl1.SelectedTabPage
            Dim currentPage As DebtPlus.UI.Client.controls.ControlBaseClient = Nothing

            If tabPage.Controls.Count > 0 Then
                currentPage = TryCast(tabPage.Controls(0), DebtPlus.UI.Client.controls.ControlBaseClient)
            End If

            ' Remove the "item" menu items
            MenuItem_Items.MenuItems.Clear()
            log.Debug("MenuItem_Items.MenuItems.Clear")

            ' Disable the creditor menu when we are not on the debts page
            BarSubItem_Creditor.Enabled = (Convert.ToString(tabPage.Tag) = "Debts")

            ' Define the page if there is not one
            If currentPage Is Nothing Then
                log.DebugFormat("Creating tab page '{0}'", tabPage.Tag)

                Dim newControl As DevExpress.XtraEditors.XtraUserControl
                Select Case Convert.ToString(tabPage.Tag)
                    Case "ActionPlan"
                        Page_ActionPlan1 = New Page_ActionPlan()
                        Page_ActionPlan1.Name = "Page_ActionPlan1"
                        newControl = Page_ActionPlan1

                    Case "Aliases"
                        Page_Aliases1 = New Page_Aliases()
                        Page_Aliases1.Name = "Page_Aliases1"
                        newControl = Page_Aliases1

                    Case "Applicant"
                        Page_Person1 = New Page_Person()
                        Page_Person1.Name = "Page_Person1"
                        Page_Person1.IsApplicant = True
                        newControl = Page_Person1

                    Case "Budgets"
                        Page_Budgets1 = New Page_Budgets()
                        Page_Budgets1.Name = "Page_Budgets1"
                        newControl = Page_Budgets1

                    Case "CoApplicant"
                        Page_Person2 = New Page_Person()
                        Page_Person2.Name = "Page_Person2"
                        Page_Person2.IsApplicant = False
                        newControl = Page_Person2

                    Case "Comparison"
                        Page_Comparison1 = New Page_Comparison()
                        Page_Comparison1.Name = "Page_Comparison1"
                        newControl = Page_Comparison1

                    Case "DMP"
                        Page_DMP1 = New Page_DMP()
                        Page_DMP1.Name = "Page_DMP1"
                        newControl = Page_DMP1

                    Case "Debts"
                        Page_Debts1 = New Page_Debts()
                        Page_Debts1.Name = "Page_Debts1"
                        Page_Debts1.LoadDefaultFilters()
                        Page_Debts1.SetDebtMenuFilters(BarSubItem_Creditor_View, barManager1)
                        Page_Debts1.SetDebtMenuFilters(Page_Debts1.BarSubItem_View, barManager1)
                        newControl = Page_Debts1

                    Case "Deposits"
                        Page_Deposits1 = New Page_Deposits()
                        Page_Deposits1.Name = "Page_Deposits1"
                        newControl = Page_Deposits1

                    Case "Disclosure"
                        Page_Disclosure1 = New Page_Disclosure()
                        Page_Disclosure1.Name = "Page_Disclosure1"
                        newControl = Page_Disclosure1

                    Case "General"
                        Page_General1 = New Page_General()
                        Page_General1.Name = "Page_General1"
                        newControl = Page_General1

                    Case "Housing"
                        Page_Housing1 = New Page_Housing()
                        Page_Housing1.Name = "Page_Housing1"
                        newControl = Page_Housing1

                    Case "Income"
                        Page_Assets1 = New Page_Assets()
                        Page_Assets1.Name = "Page_Assets1"
                        newControl = Page_Assets1

                    Case "Notes"
                        Page_Notes1 = New Page_Notes()
                        Page_Notes1.Name = "Page_Notes1"
                        newControl = Page_Notes1

                    Case "Notices"
                        Page_Notices1 = New Page_Notices()
                        Page_Notices1.Name = "Page_Notices1"
                        newControl = Page_Notices1

                    Case "OtherDebts"
                        Page_OtherDebt1 = New Page_OtherDebt()
                        Page_OtherDebt1.Name = "Page_OtherDebt1"
                        newControl = Page_OtherDebt1

                    Case "Payments"
                        Page_Payments1 = New Page_Payments()
                        Page_Payments1.Name = "Page_Payments1"
                        newControl = Page_Payments1

                    Case "Products"
                        Page_Products1 = New Page_Products()
                        Page_Products1.Name = "Page_Products1"
                        newControl = Page_Products1

                    Case "Retention"
                        Page_Retention1 = New Page_Retention()
                        Page_Retention1.Name = "Page_Retention1"
                        newControl = Page_Retention1

                    Case "Secured"
                        Page_SecuredProperty1 = New Page_SecuredProperty()
                        Page_SecuredProperty1.Name = "Page_SecuredProperty1"
                        newControl = Page_SecuredProperty1

                    Case "Taxes"
                        Page_Taxes1 = New Page_Taxes()
                        Page_Taxes1.Name = "Page_Taxes1"
                        newControl = Page_Taxes1

                    Case Else
                        System.Diagnostics.Debug.Assert(False, "The tag is not valid for the control")
                        newControl = Nothing
                        Return
                End Select

                log.Debug("Tab Page Created")

                ' If there is a page defined then make it occupy the whole space available
                currentPage = DirectCast(newControl, DebtPlus.UI.Client.controls.ControlBaseClient)
                newControl.Location = New Point(0, 0)
                newControl.Size = New Size(553, 290)
                newControl.TabIndex = 0
                newControl.Dock = DockStyle.Fill

                tabPage.SuspendLayout()
                tabPage.Controls.Add(newControl)
                tabPage.ResumeLayout(True)

                log.Debug("Tab Page CreatControl call")
                newControl.CreateControl()
                log.Debug("Tab Page CreatControl return")

                ' Raise the load event on the control. All setup has been performed and the
                ' control is properly defined for the first time. So, invoke the LoadForm method here.
                currentPage.LoadForm(bc)
                log.Debug("Tab Page LoadForm return")
            End If

            ' Here is where we go when we want to load the data on the new page
            log.Debug("Tab Page ReadForm call")
            currentPage.ReadForm(bc)
            log.Debug("Tab Page ReadForm return")
        End Sub

        ''' <summary>
        ''' Routine to find the currently selected page's control item
        ''' </summary>
        Friend Function FindCurrentPage() As DebtPlus.UI.Client.controls.ControlBaseClient
            Dim currentPage As DebtPlus.UI.Client.controls.ControlBaseClient = Nothing

            Dim tabPage As XtraTabPage = XtraTabControl1.SelectedTabPage
            If tabPage IsNot Nothing Then
                If tabPage.Controls.Count > 0 Then
                    currentPage = CType(tabPage.Controls(0), DebtPlus.UI.Client.controls.ControlBaseClient)
                End If
            End If

            Return currentPage
        End Function

        ''' <summary>
        ''' Part of Drag/Drop - Handle the mouse move operation
        ''' </summary>
        Private Sub XtraTabControl1_MouseMove(ByVal sender As Object, ByVal e As MouseEventArgs)
            Dim c As XtraTabControl = CType(sender, XtraTabControl)
            If e.Button = MouseButtons.Left Then

                ' Find the location for the click/move event
                Dim pt As New Point(e.X, e.Y)
                Dim hi As XtraTabHitInfo = c.CalcHitInfo(pt)
                Dim dragPage As XtraTabPage = hi.Page

                ' If the item is a page then allow the page to be moved
                If hi.Page IsNot Nothing Then
                    Dim dobj As New DataObject
                    Dim typ As Type = GetType(XtraTabPage)
                    dobj.SetData(typ, dragPage)
                    Dim effect As DragDropEffects = c.DoDragDrop(dobj, DragDropEffects.Move)
                    If effect = DragDropEffects.Move Then
                        ' Do something since the pages were re-ordered. Save the new layout, etc.
                    End If
                End If
            End If
        End Sub

        ''' <summary>
        ''' Part of Drag/Drop - Give visual feedback if a drop is possible
        ''' </summary>
        Private Sub XtraTabControl1_DragEnter(ByVal sender As Object, ByVal e As DragEventArgs)
            Dim typ As Type = GetType(XtraTabPage)
            If e.Data.GetDataPresent(typ) Then
                e.Effect = e.AllowedEffect And DragDropEffects.Move
            Else
                e.Effect = DragDropEffects.None
            End If
        End Sub

        ''' <summary>
        ''' Part of Drag/Drop - Do the drop operation here
        ''' </summary>
        Private Sub XtraTabControl1_DragDrop(ByVal sender As Object, ByVal e As DragEventArgs)
            Dim c As XtraTabControl = CType(sender, XtraTabControl)

            ' Indicate that we did nothing. This is the most common condition.
            e.Effect = DragDropEffects.None

            ' If there is a text field then look to determine the acceptable processing
            Dim typ As Type = GetType(XtraTabPage)
            If e.Data.GetDataPresent(typ) AndAlso (e.AllowedEffect And DragDropEffects.Move) = DragDropEffects.Move Then

                ' Find the page where we dropped the item
                Dim pt As New Point(e.X, e.Y)
                Dim hi As XtraTabHitInfo = c.CalcHitInfo(c.PointToClient(pt))
                Dim dropPage As XtraTabPage = hi.Page
                Dim dragPage As XtraTabPage = TryCast(e.Data.GetData(typ), XtraTabPage)

                ' Our page must be valid at this point or we can't do anything
                If dropPage IsNot Nothing AndAlso dragPage IsNot Nothing Then
                    If dragPage IsNot dropPage Then

                        ' Ensure that the pages passed in the drag/drop are our own pages and not something else
                        Dim dragPageIndex As Int32 = c.TabPages.IndexOf(dragPage)
                        Dim dropPageIndex As Int32 = c.TabPages.IndexOf(dropPage)
                        If dragPageIndex >= 0 AndAlso dropPageIndex >= 0 Then

                            ' Do the proper move based upon the direction of the move.
                            If dropPageIndex < dragPageIndex Then
                                c.TabPages.Move(dropPageIndex, dragPage)
                            Else
                                c.TabPages.Move(dropPageIndex + 1, dragPage)
                            End If

                            ' We only support move. No modifier needs to be tested
                            e.Effect = DragDropEffects.Move
                        End If
                    End If
                End If
            End If
        End Sub

#End Region

#Region " WWW Menu "

        ''' <summary>
        ''' Process the WWW MENU : MESSAGES : CLICK event
        ''' </summary>
        Private Sub BarButtonItem_WWW_Messages_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim userName As String = Context.ClientDs.ClientId.ToString()

            ' Find the user record in the database.
            Dim www_record As client_www = bc.client_wwws.Where(Function(s) s.UserName = userName AndAlso s.ApplicationName = "/").FirstOrDefault()

            ' If the record is not found then we need to create a new one. Ask first.
            If www_record Is Nothing Then
                If DebtPlus.Data.Forms.MessageBox.Show("The client does not have WWW access. Do you want to create it now?", "Can not create message", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
                    www_record = CreateClientWWW()
                End If
            End If

            ' A record is required at this point to continue.
            If www_record Is Nothing Then
                Return
            End If

            ' Create a new message record for this client.
            Using frm As New Form_client_www_notes(www_record)
                If frm.ShowDialog() <> Windows.Forms.DialogResult.OK Then
                    Return
                End If
            End Using

            Try
                ' Submit the changes to the database once we have completed successfully.
                bc.SubmitChanges()
            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating client www notes")
            End Try
        End Sub

        ''' <summary>
        ''' Handle the click event on the www account
        ''' </summary>
        Private Sub BarButtonItem_WWW_Account_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim userName As String = Context.ClientDs.ClientId.ToString()

            ' Find the user record in the database. If not found, create a new item.
            Dim www_record As client_www = bc.client_wwws.Where(Function(s) s.UserName = userName AndAlso s.ApplicationName = "/").FirstOrDefault()
            If www_record Is Nothing Then
                CreateClientWWW()
                Return
            End If

            ' Update the record now that we have it defined.
            Using frm As New Form_client_www(www_record)
                If frm.ShowDialog() <> Windows.Forms.DialogResult.OK Then
                    Return
                End If
            End Using

            Try
                bc.SubmitChanges()
            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating client www access record")
            End Try
        End Sub

        ''' <summary>
        ''' Create a new www access record for this client.
        ''' </summary>
        Private Function CreateClientWWW() As client_www

            Dim record As client_www = DebtPlus.LINQ.Factory.Manufacture_client_www()
            record.UserName = Context.ClientDs.ClientId.ToString()
            record.ApplicationName = "/"

            Using frm As New Form_new_client_www(record)
                If frm.ShowDialog() <> Windows.Forms.DialogResult.OK Then
                    Return Nothing
                End If
            End Using

            ' Add the client access to the system
            bc.client_wwws.InsertOnSubmit(record)
            Try
                bc.SubmitChanges()
                Return record

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error inserting new client web access record")
            End Try

            Return Nothing
        End Function
#End Region

#Region " Reports Menu "

        ''' <summary>
        ''' Load the reports menu on the first selection
        ''' </summary>
        Private ReportsMenuLoaded As Boolean

        Private Sub BarSubItem_Reports_Popup(ByVal sender As Object, ByVal e As EventArgs)
            If Not ReportsMenuLoaded Then
                ReportsMenuLoaded = True
                Load_Reports_MenuItems()
            End If
        End Sub

        ''' <summary>
        ''' Load the reports menu items from the database tables
        ''' </summary>
        Private Sub Load_Reports_MenuItems()

            ' Clear the current reports menu items. We need to remove the "Temp" item that enables the popup
            BarSubItem_Reports.ClearLinks()

            Try
                Using crm As New DebtPlus.UI.Common.CursorManager()

                    ' Read the reports from the database
                    Dim rptList As System.Collections.Generic.List(Of DebtPlus.LINQ.report) = DebtPlus.LINQ.Cache.report.getList().FindAll(Function(rpt) rpt.Type = "CL" AndAlso rpt.ClassName IsNot Nothing AndAlso rpt.ClassName <> String.Empty AndAlso rpt.menu_name IsNot Nothing AndAlso rpt.menu_name <> String.Empty)

                    ' Process the reports in the resulting list
                    For Each rpt As DebtPlus.LINQ.report In rptList.OrderBy(Function(r) r.menu_name)
                        Dim menuName As String = rpt.menu_name.Trim()

                        ' Find the corresponding link to the item in the menu tree
                        Dim band As BarSubItem = BarSubItem_Reports
                        Do
                            ' Find the directory name in the string. If none, terminate to add the item.
                            Dim directoryOffset As Int32 = menuName.IndexOf("\"c)
                            If directoryOffset < 0 Then Exit Do

                            ' Split the name into a group and the "rest of the string"
                            Dim groupName As String = menuName.Substring(0, directoryOffset).Trim()
                            menuName = menuName.Substring(directoryOffset + 1)
                            If groupName <> String.Empty Then

                                ' Find the name of the group from the current base so that we may follow it down the tree.
                                Dim foundItem As BarSubItem = Nothing
                                For Each searchItem As BarItemLink In band.ItemLinks
                                    If TypeOf searchItem Is BarSubItemLink Then
                                        If String.Compare(CType(searchItem, BarSubItemLink).Caption, groupName, True) = 0 Then
                                            foundItem = CType(searchItem.Item, BarSubItem)
                                            Exit For
                                        End If
                                    End If
                                Next

                                ' If the menu item is not found, add it to the list.
                                If foundItem Is Nothing Then
                                    Dim newBandItem As New BarSubItem()
                                    newBandItem.Manager = barManager1
                                    newBandItem.Caption = groupName
                                    newBandItem.Name = "Reports_" + Guid.NewGuid().ToString()

                                    band.AddItem(newBandItem)
                                    foundItem = newBandItem
                                End If

                                ' Start the search from this point
                                band = foundItem
                            End If
                        Loop

                        ' Add the item to the menu
                        Dim newItem As New ReportsMenuItem(Context, menuName, rpt)
                        AddHandler newItem.ItemClick, AddressOf Reports_ItemClick
                        newItem.Manager = barManager1
                        newItem.Name = "Reports_" + Guid.NewGuid().ToString()
                        newItem.Caption = menuName

                        band.AddItem(newItem)
                    Next
                End Using

            Catch ex As SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading reports table")
            End Try
        End Sub

        ''' <summary>
        ''' Flush the current cached writes to the database when a report is chosen. It needs to see the
        ''' correct copy and if the in-memory values differ, update the database copy.
        ''' </summary>
        Private Sub Reports_ItemClick(ByVal Sender As Object, ByVal e As ItemClickEventArgs)
            ThreadSafeSaveChanges()
        End Sub

#End Region

#Region " Letters Menu "

        ''' <summary>
        ''' Load the letters menu on the first selection
        ''' </summary>
        Private LettersMenuLoaded As Boolean

        Private Sub BarSubItem_Letters_Popup(ByVal sender As Object, ByVal e As EventArgs)
            If Not LettersMenuLoaded Then
                LettersMenuLoaded = True
                Load_Letters_MenuItems()
            End If
        End Sub

        ''' <summary>
        ''' Load the Letters menu items from the database tables
        ''' </summary>
        Private Sub Load_Letters_MenuItems()

            ' Clear the current letters menu items. We need to remove the "Temp" item that enables the popup
            BarSubItem_Letters.ClearLinks()

            Dim Regions() As Int32 = New Int32() {0, 1}
            Using cm As New DebtPlus.UI.Common.CursorManager()
                Try

                    ' Read the list of letters from the database
                    Dim letterList As System.Collections.Generic.List(Of DebtPlus.LINQ.letter_type) = DebtPlus.LINQ.Cache.letter_type.getList().FindAll(Function(l) Regions.Contains(l.region) AndAlso l.letter_group = "CL" AndAlso l.language = 1 AndAlso (l.menu_name IsNot Nothing AndAlso l.menu_name <> "")).OrderBy(Function(l) l.menu_name).ToList()

                    For Each letterType As DebtPlus.LINQ.letter_type In letterList
                        Dim menuName As String = letterType.menu_name.Trim()

                        ' Find the corresponding link to the item in the menu tree
                        Dim band As BarSubItem = BarSubItem_Letters
                        Do
                            ' Find the directory name in the string. If none, terminate to add the item.
                            Dim directoryOffset As Int32 = menuName.IndexOf("\"c)
                            If directoryOffset < 0 Then Exit Do

                            ' Split the name into a group and the "rest of the string"
                            Dim groupName As String = menuName.Substring(0, directoryOffset).Trim()
                            menuName = menuName.Substring(directoryOffset + 1)
                            If groupName <> String.Empty Then

                                ' Find the name of the group from the current base so that we may follow it down the tree.
                                Dim foundItem As BarSubItem = Nothing
                                For Each searchItem As BarItemLink In band.ItemLinks
                                    If TypeOf searchItem Is BarSubItemLink Then
                                        If String.Compare(CType(searchItem, BarSubItemLink).Caption, groupName, True) = 0 Then
                                            foundItem = CType(searchItem.Item, BarSubItem)
                                            Exit For
                                        End If
                                    End If
                                Next

                                ' If the menu item is not found, add it to the list.
                                If foundItem Is Nothing Then
                                    Dim newBandItem As New BarSubItem()
                                    newBandItem.Manager = barManager1
                                    newBandItem.Caption = groupName
                                    newBandItem.Name = "Letters_" + Guid.NewGuid().ToString()
                                    band.AddItem(newBandItem)
                                    foundItem = newBandItem
                                End If

                                ' Start the search from this point
                                band = foundItem
                            End If
                        Loop

                        ' Add the item to the menu
                        Dim newItem As New LettersMenuItem(Me, Context, menuName, letterType)

                        AddHandler newItem.ItemClick, AddressOf Letters_ItemClick
                        newItem.Manager = barManager1
                        newItem.Name = "Letters_" + Guid.NewGuid().ToString()
                        newItem.Caption = menuName
                        band.AddItem(newItem)
                    Next

                Catch ex As System.Data.SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading letter_types table")
                End Try
            End Using
        End Sub

        ''' <summary>
        ''' Save changes to the current page when a letter is selected
        ''' </summary>
        Private Sub Letters_ItemClick(ByVal Sender As Object, ByVal e As ItemClickEventArgs)
            ThreadSafeSaveChanges()
        End Sub

#End Region

        Private Class RefreshClientStatusResults
            Public Property active_status As String
            Public Property client_status As String
            Public Property active_status_date As System.Nullable(Of DateTime)
        End Class

        ''' <summary>
        ''' Refresh the client status when the appointment changes the values
        ''' </summary>
        Private Sub RefreshClientStatus()

            Dim NewClientStatus As String = "CRE"
            Dim NewActiveStatus As String = String.Empty
            Dim ActiveStatusDate As Object = DBNull.Value

            Try
                Using dc As New DebtPlus.LINQ.BusinessContext()
                    Dim q As RefreshClientStatusResults = dc.clients.Where(Function(c) c.Id = Context.ClientId).Select(Function(c) New RefreshClientStatusResults With {.client_status = c.client_status, .active_status = c.active_status, .active_status_date = c.active_status_date}).SingleOrDefault()
                    If Not String.IsNullOrEmpty(q.active_status) Then NewActiveStatus = q.active_status
                    If Not String.IsNullOrEmpty(q.client_status) Then NewClientStatus = q.client_status
                    If q.active_status_date IsNot Nothing Then ActiveStatusDate = q.active_status_date.Value

                    ' Correct the client information if the appointment is booked
                    UpdateClientActiveStatus(NewClientStatus, NewActiveStatus, ActiveStatusDate)
                End Using

            Catch ex As SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading client/active status following appointment booking")
            End Try
        End Sub

        ''' <summary>
        ''' Handle cross thread problems and update the client and active statuses
        ''' </summary>
        Private Delegate Sub UpdateClientActiveStatusDelegate(ByVal NewClientStatus As String, ByVal NewActiveStatus As String, ByVal ActiveStatusDate As Object)
        Private Sub UpdateClientActiveStatus(ByVal NewClientStatus As String, ByVal NewActiveStatus As String, ByVal ActiveStatusDate As Object)

            ' Switch to the primary thread if required to perform the update
            If InvokeRequired Then
                Dim ia As IAsyncResult = BeginInvoke(New UpdateClientActiveStatusDelegate(AddressOf UpdateClientActiveStatus), New Object() {NewClientStatus, NewActiveStatus, ActiveStatusDate})
                EndInvoke(ia)
                Return
            End If

            ' Look for a change in the client status information
            Dim oldStatus As String = Context.ClientDs.clientRecord.client_status
            If oldStatus <> NewClientStatus Then
                Context.ClientDs.clientRecord.client_status = NewClientStatus
                Context.ClientDs.clientRecord.client_status_date = DebtPlus.Utils.Nulls.v_DateTime(ActiveStatusDate)
                UpdateStatusBar()
            End If

            ' Look for a change in the active status information
            oldStatus = Context.ClientDs.clientRecord.active_status
            If oldStatus <> NewActiveStatus Then
                Context.ClientDs.clientRecord.active_status = NewActiveStatus
                Context.ClientDs.clientRecord.active_status_date = DebtPlus.Utils.Nulls.v_DateTime(ActiveStatusDate)
                UpdateStatusBar()
            End If
        End Sub

#Region " Appointments "

        Private Sub BarButtonItem_Appointments_List_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            ThreadSafeSaveChanges()

            ' Do the report since we know the name we can just create the report and not go through the loader
            Dim rpt As DebtPlus.Reports.Client.Appointments.ClientAppointmentsReport = New DebtPlus.Reports.Client.Appointments.ClientAppointmentsReport()
            rpt.ClientID = Context.ClientDs.ClientId
            rpt.Parameter_PendingOnly = True
            rpt.AllowParameterChangesByUser = True
            rpt.RunReportInSeparateThread()
        End Sub

        Private Sub BarButtonItem_Appointments_New_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            ThreadSafeSaveChanges()
            AppointmentsNew()
        End Sub

        Private Sub BarButtonItem_Appointments_Result_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            ThreadSafeSaveChanges()
            AppointmentsResult()
        End Sub

        Private Sub BarButtonItem_Appointments_Reschedule_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            ThreadSafeSaveChanges()
            AppointmentsReschedule()
        End Sub

        Private Sub BarButtonItem_Appointments_Cancel_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            ThreadSafeSaveChanges()
            AppointmentsCancel()
        End Sub

        Private Sub BarButtonItem_Appointments_Confirm_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            ThreadSafeSaveChanges()
            AppointmentsConfirm()
        End Sub

        Private Sub BarButtonItem_Appointments_Walkin_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            ThreadSafeSaveChanges()
            AppointmentsWalkin()
        End Sub

        Private Sub BarButtonItem_Appointments_HUD_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            ThreadSafeSaveChanges()
            AppointmentsHUD()
        End Sub

        Private Const BackgroundThread As Boolean = True

        Private Sub AppointmentsNew()
            Dim thrd As New Thread(AddressOf AppointmentsNewThread)
            thrd.Name = "Appointments_New_Thread"
            thrd.SetApartmentState(ApartmentState.STA)
            thrd.IsBackground = BackgroundThread
            thrd.Start(Context)
        End Sub

        Private Sub AppointmentsNewThread(ByVal cuc As Object)
            Dim ctx As ClientUpdateClass = TryCast(cuc, ClientUpdateClass)
            If ctx Is Nothing Then
                Return
            End If

            Using bc As New DebtPlus.LINQ.BusinessContext()
                Using frm As New Appointments_Form_Book(bc, ctx.ClientDs.ClientId)
                    Dim answer As DialogResult = frm.ShowDialog()
                    If answer <> DialogResult.OK Then
                        Return
                    End If
                End Using

                Try
                    ' Ensure that the changes to the database have been completed before we terminate
                    bc.SubmitChanges()
                Catch ex As System.Data.SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
                End Try

                ' Refresh the client status from the appointment data change
                RefreshClientStatus()
            End Using
        End Sub

        Private Sub AppointmentsResult()
            Dim thrd As New Thread(AddressOf AppointmentsResultThread)
            thrd.Name = "Appointments_Result_Thread"
            thrd.SetApartmentState(ApartmentState.STA)
            thrd.IsBackground = BackgroundThread
            thrd.Start(Context)
        End Sub

        Friend Sub AppointmentsResultThread(ByVal cuc As Object)
            Dim ctx As ClientUpdateClass = TryCast(cuc, ClientUpdateClass)
            If ctx Is Nothing Then
                Return
            End If

            Using bc As New DebtPlus.LINQ.BusinessContext()
                Using frm As New Appointments_Form_Result(bc, ctx.ClientDs.ClientId)
                    If frm.ShowDialog() <> DialogResult.OK Then
                        Return
                    End If
                End Using

                Try
                    ' Ensure that the changes to the database have been completed before we terminate
                    bc.SubmitChanges()
                Catch ex As System.Data.SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
                End Try

                ' Refresh the client status from the appointment data change
                RefreshClientStatus()
            End Using
        End Sub

        Public Sub AppointmentsReschedule()
            Dim thrd As New Thread(AddressOf AppointmentsRescheduleThread)
            thrd.Name = "Appointments_Reschedule_Thread"
            thrd.SetApartmentState(ApartmentState.STA)
            thrd.IsBackground = BackgroundThread
            thrd.Start(Context)
        End Sub

        Friend Sub AppointmentsRescheduleThread(ByVal cuc As Object)
            Dim ctx As ClientUpdateClass = TryCast(cuc, ClientUpdateClass)
            If ctx Is Nothing Then
                Return
            End If

            Using bc As New DebtPlus.LINQ.BusinessContext()
                Using frm As New Appointments_Form_Reschedule(bc, ctx.ClientDs.ClientId)
                    frm.ShowDialog()
                End Using
                Try
                    ' Ensure that the changes to the database have been completed before we terminate
                    bc.SubmitChanges()
                Catch ex As System.Data.SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
                End Try
            End Using
        End Sub

        Public Sub AppointmentsCancel()
            Dim thrd As New Thread(AddressOf AppointmentsCancelThread)
            thrd.Name = "Appointments_Cancel_Thread"
            thrd.SetApartmentState(ApartmentState.STA)
            thrd.IsBackground = BackgroundThread
            thrd.Start(Context)
        End Sub

        Friend Sub AppointmentsCancelThread(ByVal cuc As Object)
            Dim ctx As ClientUpdateClass = TryCast(cuc, ClientUpdateClass)
            If ctx Is Nothing Then
                Return
            End If

            Using bc As New DebtPlus.LINQ.BusinessContext()
                Using frm As New Appointments_Form_Cancel(bc, ctx.ClientDs.ClientId)
                    frm.ShowDialog()
                End Using
                Try
                    ' Ensure that the changes to the database have been completed before we terminate
                    bc.SubmitChanges()
                Catch ex As System.Data.SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
                End Try
            End Using
        End Sub

        Public Sub AppointmentsConfirm()
            Dim thrd As New Thread(AddressOf AppointmentsConfirmThread)
            thrd.Name = "Appointments_Confirm_Thread"
            thrd.SetApartmentState(ApartmentState.STA)
            thrd.IsBackground = BackgroundThread
            thrd.Start(Context)
        End Sub

        Friend Sub AppointmentsConfirmThread(ByVal cuc As Object)
            Dim ctx As ClientUpdateClass = TryCast(cuc, ClientUpdateClass)
            If ctx Is Nothing Then
                Return
            End If

            Using bc As New DebtPlus.LINQ.BusinessContext()
                Using frm As New Appointments_Form_Confirm(bc, ctx.ClientDs.ClientId)
                    frm.ShowDialog()
                End Using
                Try
                    ' Ensure that the changes to the database have been completed before we terminate
                    bc.SubmitChanges()
                Catch ex As System.Data.SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
                End Try
            End Using
        End Sub

        Public Sub AppointmentsWalkin()
            Dim thrd As New Thread(AddressOf AppointmentsWalkinThread)
            thrd.Name = "Appointments_Walkin_Thread"
            thrd.SetApartmentState(ApartmentState.STA)
            thrd.IsBackground = BackgroundThread
            thrd.Start(Context)
        End Sub

        Friend Sub AppointmentsWalkinThread(ByVal cuc As Object)

            ' Find the client update context
            Dim ctx As ClientUpdateClass = TryCast(cuc, ClientUpdateClass)
            If ctx Is Nothing Then
                Return
            End If

            Using bc As New BusinessContext()
                Using frm As New Appointments_Form_Walkin(bc, ctx.ClientDs.ClientId)
                    frm.ShowDialog()
                End Using

                Try
                    ' Ensure that the changes to the database have been performed.
                    bc.SubmitChanges()

                Catch ex As System.Data.SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
                End Try
            End Using
        End Sub

        Public Sub AppointmentsHUD()
            Dim thrd As New Thread(AddressOf AppointmentsHUDThread)
            thrd.Name = "Appointments_HUD_Thread"
            thrd.SetApartmentState(ApartmentState.STA)
            thrd.IsBackground = BackgroundThread
            thrd.Start(Context)
        End Sub

        Friend Sub AppointmentsHUDThread(ByVal cuc As Object)

            ' Find the update context
            Dim ctx As ClientUpdateClass = TryCast(cuc, ClientUpdateClass)
            If ctx Is Nothing Then
                Return
            End If

            Using bc As New BusinessContext()
                Using frm As New HUD_Interview_Form(bc, ctx.ClientDs.ClientId)
                    frm.ShowDialog()
                End Using

                Try
                    ' Ensure that the changes to the database are updated
                    bc.SubmitChanges()

                Catch ex As System.Data.SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
                End Try
            End Using
        End Sub
#End Region

#Region " View Menu "

        ''' <summary>
        ''' Load the view menu items from the tab pages
        ''' </summary>
        Private Sub Load_View_MenuItems()

            ' Load the view menu items with the tab pages
            Dim startGroup As Boolean = True

            For Each tabPage As XtraTabPage In XtraTabControl1.TabPages

                Dim newItem As New BarCheckItem()
                newItem.Caption = "&" + Replace(tabPage.Text, "&", "&&")
                newItem.Checked = False
                newItem.Tag = tabPage.Tag
                AddHandler newItem.ItemClick, AddressOf MenuItem_View_Click
                barManager1.Items.Add(newItem)

                ' Add the new item as a start of a new group.
                BarSubItem_View.AddItem(newItem).BeginGroup = startGroup
                startGroup = False
            Next
        End Sub

        ''' <summary>
        ''' Process the VIEW menu items POPUP event
        ''' </summary>
        Private Sub MenuItem_View_Popup(ByVal sender As Object, ByVal e As EventArgs)
            Dim subItem As BarSubItem = CType(sender, BarSubItem)
            Dim showingPage As String = Convert.ToString(XtraTabControl1.SelectedTabPage.Tag)

            For Each item As BarItemLink In subItem.ItemLinks
                If item.Item.Tag IsNot Nothing AndAlso TypeOf item Is BarCheckItemLink Then
                    Dim barChkItem As BarCheckItem = CType(item.Item, BarCheckItem)
                    barChkItem.Checked = String.Compare(Convert.ToString(barChkItem.Tag), showingPage, False) = 0
                End If
            Next
        End Sub

        ''' <summary>
        ''' Process the VIEW menu items CLICK event
        ''' </summary>
        Private Sub MenuItem_View_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim mgr As BarManager = CType(sender, BarManager)
            Dim link As BarItemLink = mgr.PressedLink
            Dim item As BarItem = link.Item
            Dim selectedPage As String = Convert.ToString(item.Tag)

            ' Find the desired page and select it. It will trip the update events automatically.
            For Each tabPage As XtraTabPage In XtraTabControl1.TabPages
                If Convert.ToString(tabPage.Tag) = selectedPage Then
                    XtraTabControl1.SelectedTabPageIndex = XtraTabControl1.TabPages.IndexOf(tabPage)
                    Exit For
                End If
            Next
        End Sub
#End Region

#Region " View BottomLine "

        ''' <summary>
        ''' Display the bottom line form information
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub BarButtonItem_View_BottomLine_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Form_BottomLine.Instance(Context).Show()
        End Sub

        ''' <summary>
        ''' Rebuild the bottom line form data when it has changed
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub Recalculate_BottomLine(ByVal sender As Object, ByVal e As System.EventArgs)
            Form_BottomLine.RefreshData()
        End Sub
#End Region

#Region " View Client Ticklers "

        Private Sub BarButtonItem_View_Ticklers_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Dim thrd As New Thread(AddressOf BarButtonItem_View_Ticklers_Thread)
            thrd.Name = "BarButtonItem_View_Ticklers_Thread"
            thrd.SetApartmentState(ApartmentState.STA)
            thrd.Start(Context)
        End Sub

        Private Sub BarButtonItem_View_Ticklers_Thread(ByVal obj As Object)
            Dim ctx As ClientUpdateClass = CType(obj, ClientUpdateClass)
            Using frm As New Form_Ticklers_ClientList(ctx.ClientDs.ClientId)
                frm.ShowDialog()
            End Using
        End Sub
#End Region

#Region " Help "

#Region " Help About "

        Private Sub BarButtonItem_Help_About_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Using frm As New AboutBoxForm()
                frm.ShowDialog()
            End Using
        End Sub

#End Region

#End Region

#Region " Trust "

#Region " Trust Threshold "

        ''' <summary>
        ''' Adjust the threshold amount for the debts in the client
        ''' </summary>
        Private Sub BarButtonItem_Trust_CreditorThreshold_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Using frm As New Form_ThresholdAdjust()
                frm.ParameterThreshold = Context.DebtRecords.Threshold
                If frm.ShowDialog() = DialogResult.OK Then
                    Context.DebtRecords.Threshold = frm.ParameterThreshold
                End If
            End Using
        End Sub

#End Region

#Region " Trust ReservedInTrust "

        Private Sub BarButtonItem_Trust_ReservedInTrust_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Dim thrd As New Thread(AddressOf BarButtonItem_Trust_ReservedInTrust_ItemClick_Thread)
            thrd.IsBackground = True
            thrd.SetApartmentState(ApartmentState.STA)
            thrd.Start()
        End Sub

        Private Sub BarButtonItem_Trust_ReservedInTrust_ItemClick_Thread()

            Using frm As New Form_ReservedInTrust(Context)
                frm.ShowDialog()
            End Using

        End Sub

#End Region

#End Region

#Region " Creditor Debts "

#Region " CHANGE DEPOSIT AMT "

        ''' <summary>
        ''' Process the CREDITOR -> CHANGE DEPOSIT AMT click event
        ''' </summary>
        Private Sub BarButtonItem_Creditor_ChgDepositAmt_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Dim thrd As New Thread(AddressOf BarButtonItem_Creditor_ChgDepositAmt_ItemClick_Thread)
            thrd.Name = "BarButtonItem_Creditor_ChgDepositAmt_ItemClick_Thread"
            thrd.IsBackground = True
            thrd.SetApartmentState(ApartmentState.STA)
            thrd.Start()
        End Sub

        ''' <summary>
        ''' Process the CREDITOR -> CHANGE DEPOSIT AMT thread
        ''' </summary>
        Private Sub BarButtonItem_Creditor_ChgDepositAmt_ItemClick_Thread()

            Using frm As New Form_BalanceDeposit(Context)
                frm.ShowDialog()
            End Using

        End Sub

#End Region

#Region " Creditor Note "

        ''' <summary>
        ''' Process the CREDITOR -> NOTES click event
        ''' </summary>
        Private Sub BarButtonItem_Creditor_Note_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Dim thrd As New Thread(AddressOf BarButtonItem_Creditor_Note_ItemClick_Thread)
            thrd.Name = "BarButtonItem_Creditor_Note_ItemClick_Thread"
            thrd.IsBackground = True
            thrd.SetApartmentState(ApartmentState.STA)
            thrd.Start()
        End Sub

        ''' <summary>
        ''' Process the CREDITOR -> NOTES thread
        ''' </summary>
        Private Sub BarButtonItem_Creditor_Note_ItemClick_Thread()
            Dim currentDebt As Object = Nothing

            ' Attempt to find the selected debt in the list
            If TypeOf FindCurrentPage() Is Page_Debts Then
                Dim pg As Page_Debts = CType(FindCurrentPage(), Page_Debts)
                If pg.GridView1.FocusedRowHandle >= 0 Then
                    Dim debt As DebtRecord = CType(pg.GridView1.GetRow(pg.GridView1.FocusedRowHandle), DebtRecord)
                    currentDebt = debt.client_creditor
                End If
            End If

            ' Ask for the new note text

            Using frm As New Form_CreditorNote(Context, currentDebt)
                frm.ShowDialog()
            End Using

        End Sub

#End Region

#Region " Creditor View "

        ''' <summary>
        ''' Process the POPUP event for the CREDITOR -> VIEW menu
        ''' </summary>
        Private Sub BarSubItem_Creditor_View_Popup(ByVal sender As Object, ByVal e As EventArgs)
            Page_Debts1.CheckViewSelections(BarSubItem_Creditor_View)
        End Sub

#End Region

#Region " Creditor SchedPay "

        ''' <summary>
        ''' Process the CREDITOR -> SCHEDPAY click event
        ''' </summary>
        Private Sub BarButtonItem_Creditor_SchedPay_ItemClick(ByVal sender As Object, ByVal e As ItemClickEventArgs)
            Dim thrd As New Thread(AddressOf BarButtonItem_Creditor_SchedPay_ItemClick_Thread)
            thrd.Name = "Creditor_SchedPay"
            thrd.SetApartmentState(ApartmentState.STA)
            thrd.IsBackground = True
            thrd.Start()
        End Sub

        ''' <summary>
        ''' Thread procedure for CREDITOR -> SCHEDPAY click event
        ''' </summary>
        Private Sub BarButtonItem_Creditor_SchedPay_ItemClick_Thread()

            Using frm As New Form_ScheduledPay(Context)
                frm.ShowDialog()
            End Using

        End Sub

#End Region

#End Region

        ''' <summary>
        ''' Handle the POPUP event for the UPLOAD menu. We need to populate it with the current list of properties.
        ''' </summary>
        Private Sub BarSubItem_Upload_Popup(sender As Object, e As System.EventArgs)

            ' Save the changes to the tables so that the database is in sync.
            ThreadSafeSaveChanges()

            Dim colProperties As System.Collections.Generic.List(Of Housing_property) = bc.Housing_properties.Where(Function(s) s.HousingID = Context.ClientDs.ClientId).ToList()
            populateHosingPropertyMenu(BarSubItem_UploadDSA, colProperties.GetEnumerator(), Nothing, AddressOf DSAHousingPropertyItemPopup)
        End Sub

        ''' <summary>
        ''' Process a POPUP event for the DSA menu upload. Populate the list with the loan information.
        ''' </summary>
        Private Sub DSAHousingPropertyItemPopup(sender As Object, e As System.EventArgs)

            ' Find the menu that was being shown. If not able to do that then just ignore the event
            Dim menuBar As DevExpress.XtraBars.BarSubItem = TryCast(sender, DevExpress.XtraBars.BarSubItem)
            If menuBar Is Nothing Then
                Return
            End If

            ' Find the property record from the tag.
            Dim propertyRecord As Housing_property = TryCast(menuBar.Tag, Housing_property)
            If propertyRecord Is Nothing Then
                Return
            End If

            ' Find the list of loans on this property. If there are none then do not replace the
            ' one marker. It will just appear to be blank and not have any click event handler for it.
            Dim colLoans As System.Collections.Generic.List(Of Housing_loan) = bc.Housing_loans.Where(Function(s) s.PropertyID = propertyRecord.Id).ToList()
            populateHosingPropertyMenu(menuBar, colLoans.GetEnumerator(), AddressOf DSAHousingLoanItemClick, Nothing)

            ' If there are no loans then we need at least one item to keep the popup status alive. Generate one.
            If colLoans.Count = 0 Then
                housingPropertyMenuCount += 1

                ' Create a new menu item for the property. We don't use it for DSA but we need to have
                ' a sub-item for their menu to make it work.
                Dim newMenuItem As New DevExpress.XtraBars.BarButtonItem() With {.Id = housingPropertyMenuCount, .Caption = "There are no loans to upload", .Visibility = BarItemVisibility.OnlyInRuntime, .Manager = barManager1}
                menuBar.AddItem(newMenuItem)

                ' Do NOT register any click events since we don't process this empty item.

            End If
        End Sub

        ''' <summary>
        ''' Populate the upload menu item with the property information
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub populateHosingPropertyMenu(ByVal menuItem As DevExpress.XtraBars.BarSubItem, ByVal ieList As System.Collections.IEnumerator, ByVal clickEventDelegate As DevExpress.XtraBars.ItemClickEventHandler, ByVal popupEventDelegate As System.EventHandler)

            ' Toss any existing property information
            menuItem.ItemLinks.Clear()

            ' Add the items to the list
            While ieList.MoveNext()
                housingPropertyMenuCount += 1

                ' Create a new menu item for the property. We don't use it for DSA but we need to have
                ' a sub-item for their menu to make it work.
                Dim newMenuItem As New DevExpress.XtraBars.BarButtonItem() With {.Id = housingPropertyMenuCount, .Caption = ieList.Current().ToString().Replace("&", "&&"), .Visibility = BarItemVisibility.OnlyInRuntime, .Manager = barManager1}

                ' If this is a DSA menu then we need to allocate a menu item, not a button item.
                If popupEventDelegate IsNot Nothing Then
                    housingPropertyMenuCount += 1
                    Dim newPopupMenuItem As New DevExpress.XtraBars.BarSubItem() With {.Id = housingPropertyMenuCount, .Caption = ieList.Current().ToString().Replace("&", "&&"), .Visibility = BarItemVisibility.OnlyInRuntime, .Manager = barManager1}

                    AddHandler newPopupMenuItem.Popup, popupEventDelegate
                    newPopupMenuItem.Tag = ieList.Current()
                    menuItem.AddItem(newPopupMenuItem)

                    ' Add the button item to the menu
                    newPopupMenuItem.AddItem(newMenuItem)

                Else

                    ' Hook into the proper event based upon whether this is a DSA or HPF menu
                    If clickEventDelegate IsNot Nothing Then
                        AddHandler newMenuItem.ItemClick, clickEventDelegate
                    End If

                    newMenuItem.Tag = ieList.Current()
                    menuItem.AddItem(newMenuItem)
                End If
            End While
        End Sub

        Private housingPropertyMenuCount As Int32 = 0

        ''' <summary>
        ''' Handle the upload event for the HPF properties
        ''' </summary>
        Private Sub BarButtonItem_UploadHPF_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs)

            ' Start a thread to do the upload
            Dim thrd As New System.Threading.Thread(New System.Threading.ParameterizedThreadStart(AddressOf HPFUploadProcedure))
            thrd.SetApartmentState(ApartmentState.STA)
            thrd.Name = "HPF Upload"
            thrd.Start()
        End Sub

        ''' <summary>
        ''' Do the HPF Upload for the property. We send all loans for the property.
        ''' </summary>
        ''' <param name="obj"></param>
        ''' <remarks></remarks>
        Private Sub HPFUploadProcedure(ByVal obj As Object)
            Using frm As New HPFUploadForm(Context)
                frm.ShowDialog()
            End Using
        End Sub

        ''' <summary>
        ''' Handle the upload event for the DSA loans
        ''' </summary>
        Private Sub DSAHousingLoanItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs)

            ' Find the menu that was being shown. If not able to do that then just ignore the event
            Dim buttonBar As DevExpress.XtraBars.BarItem = TryCast(e.Item, DevExpress.XtraBars.BarItem)
            If buttonBar Is Nothing Then
                Return
            End If

            ' Find the loan record from the tag.
            Dim loanRecord As Housing_loan = TryCast(buttonBar.Tag, Housing_loan)
            If loanRecord Is Nothing Then
                Return
            End If

            ' Start a thread to do the upload
            Dim thrd As New System.Threading.Thread(New System.Threading.ParameterizedThreadStart(AddressOf DSAUploadProcedure))
            thrd.SetApartmentState(ApartmentState.STA)
            thrd.Name = "DSA Upload"
            thrd.Start(loanRecord)
        End Sub

        ''' <summary>
        ''' Do the DSA upload for the given loan. The loan points to the property and the property to the housing record and the client.
        ''' </summary>
        Private Sub DSAUploadProcedure(ByVal obj As Object)

            ' Find the loan record
            Dim loanRecord As Housing_loan = TryCast(obj, Housing_loan)
            If loanRecord Is Nothing Then
                Return
            End If

            Dim message As String = Nothing

            ' Do some validation to ensure that the case is completely defined.
            If Not loanRecord.CurrentLenderID.HasValue Then
                message = "The loan is missing the lender information. Please complete the loan data."
            ElseIf Not loanRecord.IntakeDetailID.HasValue Then
                message = "The loan is missing the intake detail information about the loan. Please complete the loan data."
            Else

                ' Submit the case to RxOffice
                Dim service As DebtPlus.Svc.DsaUpload.UploadService = New DebtPlus.Svc.DsaUpload.UploadService()
                Try
                    message = service.UploadCase(Context.ClientDs.ClientId, loanRecord.PropertyID, loanRecord.Id, loanRecord.CurrentLenderID.Value, loanRecord.IntakeDetailID.Value)
                Catch ex As Exception
                    message = ex.Message
                End Try
                service = Nothing
            End If

            ' If there was an error the complain here.
            If Not String.IsNullOrWhiteSpace(message) Then
                DebtPlus.Data.Forms.MessageBox.Show(message, "DSA Upload")
                Return
            End If

            ' No error condition on the submission.
            DebtPlus.Data.Forms.MessageBox.Show("Case details submitted to RxOffice successfully.", "Submission Successful")
        End Sub
    End Class
End Namespace
