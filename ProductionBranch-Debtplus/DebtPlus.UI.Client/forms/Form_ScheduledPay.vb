#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.ComponentModel
Imports DebtPlus.Interfaces.Debt
Imports DebtPlus.Svc.Common
Imports DevExpress.XtraGrid.Views.Base
Imports DebtPlus.Svc.Debt
Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.UI.Client.Service
Imports DebtPlus.Events
Imports System.IO
Imports DevExpress.Data.Filtering.Exceptions
Imports System.Globalization

Namespace forms

    Friend Class Form_ScheduledPay
        Implements Service.IContext
        Implements ISupportInitialize

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Private InInit As Boolean = True
        Private ReadOnly privateContext As ClientUpdateClass

        ''' <summary>
        ''' Initialize the form
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Load, AddressOf MyGridControl_Load
            AddHandler GridView1.Layout, AddressOf LayoutChanged
            AddHandler SimpleButton_clear.Click, AddressOf SimpleButton_clear_Click
            AddHandler SimpleButton_reset.Click, AddressOf SimpleButton_reset_Click
            AddHandler RepositoryItemCalcEdit_SchedPay.EditValueChanged, AddressOf RepositoryItemCalcEdit_SchedPay_EditValueChanged
            AddHandler RepositoryItemCalcEdit_SchedPay.EditValueChanging, AddressOf RepositoryItemCalcEdit_SchedPay_EditValueChanging
        End Sub

        ''' <summary>
        ''' Initialize the form with context information
        ''' </summary>
        ''' <param name="Context"></param>
        ''' <remarks></remarks>
        Public Sub New(ByVal Context As ClientUpdateClass)
            MyClass.New()
            privateContext = Context
        End Sub

        ''' <summary>
        ''' Pointer to the client update information for the current client
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Context() As ClientUpdateClass Implements Service.IContext.Context
            Get
                Return privateContext
            End Get
        End Property

        ''' <summary>
        ''' Return the directory to the saved layout information
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Overridable Function XMLBasePath() As String
            If DesignMode Then Return String.Empty
            Dim BasePath As String = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "DebtPlus")
            Return Path.Combine(BasePath, String.Format("Client.Update{0}{1}", Path.DirectorySeparatorChar, Name))
        End Function

        ''' <summary>
        ''' When loading the control, restore the layout from the file
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub MyGridControl_Load(ByVal sender As Object, ByVal e As EventArgs)
            BeginInit()

            If Not DesignMode AndAlso Context IsNot Nothing AndAlso Context.DebtRecords.Count > 0 Then
                With GridControl1
                    .BeginUpdate()
                    .DataSource = Context.DebtRecords

                    ' Bind the control to the items which are only active
                    With GridView1
                        .BeginUpdate()
                        GridColumn_balance.BestFit()
                        ReloadGridControlLayout()
                        .OptionsView.ShowFilterPanelMode = ShowFilterPanelMode.Never

                        Try
                            .ActiveFilter.Clear()
                            .ActiveFilterString =
                                "([IsActive] <> False) And (([current_balance] > 0.0) OR ([ccl_zero_balance] <> False))"

                        Catch ex As InvalidPropertyPathException
                            DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

                        Finally
                            .EndUpdate()
                        End Try
                    End With

                    .EndUpdate()
                End With
            End If

            EndInit()
        End Sub

        ''' <summary>
        ''' Reload the layout of the grid control if needed
        ''' </summary>
        ''' <remarks></remarks>
        Protected Sub ReloadGridControlLayout()

            ' Find the base path to the saved file location
            Dim PathName As String = XMLBasePath()
            If Not String.IsNullOrEmpty(PathName) Then

                Try
                    Dim FileName As String = Path.Combine(PathName, GridControl1.Name + ".Grid.xml")
                    If File.Exists(FileName) Then
                        GridView1.RestoreLayoutFromXml(FileName)
                    End If

                Catch ex As DirectoryNotFoundException
                Catch ex As FileNotFoundException
                End Try
            End If
        End Sub

        ''' <summary>
        ''' If the layout is changed then update the file
        ''' </summary>
        ''' <param name="Sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub LayoutChanged(ByVal Sender As Object, ByVal e As EventArgs)
            If Not InInit Then
                Dim PathName As String = XMLBasePath()
                If Not String.IsNullOrEmpty(PathName) Then
                    If Not Directory.Exists(PathName) Then
                        Directory.CreateDirectory(PathName)
                    End If

                    Dim FileName As String = Path.Combine(PathName, GridControl1.Name + ".Grid.xml")
                    GridView1.SaveLayoutToXml(FileName)
                End If
            End If
        End Sub

        ''' <summary>
        ''' Clear the scheduled payment field if desired
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub SimpleButton_clear_Click(ByVal sender As Object, ByVal e As EventArgs)
            GridView1.BeginDataUpdate()
            Try
                For Each Debt As DebtRecord In Context.DebtRecords
                    Debt.sched_payment = 0D
                Next

                CalculateMonthlySchedPaymentFee()

            Finally
                GridView1.EndDataUpdate()
            End Try
        End Sub

        ''' <summary>
        ''' Handle the DISBURSEMENT FACTOR button click
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub SimpleButton_reset_Click(ByVal sender As Object, ByVal e As EventArgs)
            GridView1.BeginDataUpdate()

            Try
                ' Move the disbursement factor to the debts but remove any payments this month.
                For Each Debt As DebtRecord In Context.DebtRecords
                    With Debt
                        .sched_payment = MaximumAmount(Debt, .disbursement_factor)
                    End With
                Next
                CalculateMonthlySchedPaymentFee()

            Finally
                GridView1.EndDataUpdate()
            End Try
        End Sub

        ''' <summary>
        ''' Calculate the fee amount for the debts based upon the new entry
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub RepositoryItemCalcEdit_SchedPay_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            Dim RowHandle As Int32 = GridView1.FocusedRowHandle
            If GridView1.IsDataRow(RowHandle) Then
                Dim DebtRow As ClientUpdateDebtRecord = TryCast(GridView1.GetRow(RowHandle), ClientUpdateDebtRecord)
                If DebtRow IsNot Nothing AndAlso DebtRow.DebtType <> DebtPlus.Interfaces.Debt.DebtTypeEnum.MonthlyFee Then
                    CalculateMonthlySchedPaymentFee()
                End If
            End If
        End Sub

        ''' <summary>
        ''' Update the monthly fee amount record for the current
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub CalculateMonthlySchedPaymentFee()
            Dim FeeRecord As ClientUpdateDebtRecord = CType(Context.DebtRecords.MonthlyFeeDebt, ClientUpdateDebtRecord)
            If FeeRecord IsNot Nothing Then

                ' Generate a fee calculation object
                Dim fee As New MonthlyFeeCalculation()
                Try
                    ' Give the fee routine a dataset for common storage
                    AddHandler fee.QueryValue, AddressOf SchedPayment_QueryFeeValue

                    ' Find the monthly fee for the current list of debts
                    Dim NewAmount As Decimal = fee.FeeAmount()

                    ' If the fee differs from what we calculated then update the fee amount
                    If NewAmount <> FeeRecord.sched_payment Then
                        FeeRecord.sched_payment = NewAmount

                        ' Find the fee row in the list
                        Dim Offset As Int32 = Context.DebtRecords.IndexOf(FeeRecord)
                        If Offset >= 0 Then
                            Dim RowHandle As Int32 = GridView1.GetRowHandle(Offset)
                            If RowHandle >= 0 Then
                                GridView1.RefreshRow(RowHandle)
                                GridView1.UpdateTotalSummary()
                                GridView1.InvalidateFooter()
                            End If
                        End If
                    End If

                Catch ex As System.Data.SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error calculating fee information")

                Finally
                    If fee IsNot Nothing Then
                        RemoveHandler fee.QueryValue, AddressOf SchedPayment_QueryFeeValue
                        fee.Dispose()
                    End If
                End Try
            End If
        End Sub

        ''' <summary>
        ''' Interface to the fee routine for obtaining caller items
        ''' </summary>
        ''' <param name="Sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub SchedPayment_QueryFeeValue(ByVal Sender As Object, ByVal e As DebtPlus.Events.ParameterValueEventArgs)
            With CType(Context.DebtRecords, IFeeable)
                Select Case e.Name

                    Case "disbursed_creditors"
                        ' Number of creditors to disburse
                        Dim answer As Int32 = 0
                        For RowHandle As Int32 = 0 To GridView1.RowCount - 1
                            Dim CurrentDebt As ClientUpdateDebtRecord = TryCast(GridView1.GetRow(RowHandle), ClientUpdateDebtRecord)
                            If Not CurrentDebt.IsHeld AndAlso CurrentDebt.IsProratable AndAlso Not CurrentDebt.IsFee Then
                                If CurrentDebt.sched_payment > 0D Then
                                    answer += 1
                                End If
                            End If
                        Next
                        e.Value = answer

                    Case "dollars_disbursed"
                        ' Total disbursed funds are the totals from the scheduled payments
                        Dim answer As Decimal = 0D
                        For RowHandle As Int32 = 0 To GridView1.RowCount - 1
                            Dim CurrentDebt As ClientUpdateDebtRecord = TryCast(GridView1.GetRow(RowHandle), ClientUpdateDebtRecord)
                            If Not CurrentDebt.IsHeld AndAlso CurrentDebt.IsProratable AndAlso Not CurrentDebt.IsFee Then
                                answer += DebtPlus.Utils.Nulls.DDec(CurrentDebt.sched_payment)
                            End If
                        Next
                        e.Value = answer

                    Case "limit_monthly_max"
                        e.Value = True
                        ' = true for sched_payment, false for disbursement factor

                    Case Else
                        ' All others are simply passed to the debt list for evaluation.
                        e.Value = .QueryFeeValue(e.Name)
                End Select
            End With
        End Sub

        ''' <summary>
        ''' Handle the update event for the scheduled pay
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub RepositoryItemCalcEdit_SchedPay_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)

            ' Find the new value being edited
            Dim NewValue As Decimal = 0D
            If e.NewValue IsNot Nothing AndAlso e.NewValue IsNot DBNull.Value Then
                If TypeOf e.NewValue Is Decimal Then
                    NewValue = Convert.ToDecimal(e.NewValue)
                ElseIf TypeOf e.NewValue Is String Then
                    Decimal.TryParse(Convert.ToString(e.NewValue), NewValue)
                End If
            End If

            ' Do not allow the figure to be outside the legal range
            Dim debt As DebtRecord = CType(GridView1.GetRow(GridView1.FocusedRowHandle), DebtRecord)
            e.Cancel = (NewValue < 0D) OrElse (NewValue > MaximumAmount(debt, NewValue))
        End Sub

        ''' <summary>
        ''' Calculate the maximum amount for the current debt
        ''' </summary>
        ''' <param name="debt">Pointer to the debt record</param>
        ''' <param name="DisbursementFactor">Proposed amount for the disbursement factor for the debt</param>
        ''' <returns>The maximum allowed in the scheduled pay</returns>
        ''' <remarks></remarks>
        Private Function MaximumAmount(ByVal debt As DebtRecord, ByVal DisbursementFactor As Object) As Decimal
            Dim ProposedValue As Decimal = 0D

            With debt
                Dim HasZeroBalance As Boolean = DebtPlus.Utils.Nulls.DBool(.ccl_zero_balance)
                Dim IsActive As Boolean = DebtPlus.Utils.Nulls.DBool(.IsActive)
                Dim CurrentBalance As Decimal = DebtPlus.Utils.Nulls.DDec(.current_balance)

                If IsActive AndAlso (CurrentBalance > 0D OrElse HasZeroBalance) Then
                    If DisbursementFactor IsNot Nothing AndAlso DisbursementFactor IsNot DBNull.Value Then ProposedValue = Convert.ToDecimal(DisbursementFactor, CultureInfo.InvariantCulture)

                    ' Limit the amount to the balance of the debt
                    If Not HasZeroBalance AndAlso ProposedValue > CurrentBalance Then
                        ProposedValue = CurrentBalance
                        'Else -- Per Caroline on 10/25/2010 - Remove limit on current month's disbursement. This may cause some debts to be over-paid!
                        ' Limit the amount to the figure, less any payments this month.
                        'ProposedValue -= .payments_month_0
                    End If

                    ' Do not allow the figure to go negative.
                    If ProposedValue < 0D Then ProposedValue = 0D
                End If
            End With

            Return ProposedValue
        End Function

        ''' <summary>
        ''' Start the initialization sequence
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub BeginInit() Implements ISupportInitialize.BeginInit
            InInit = True
        End Sub

        ''' <summary>
        ''' End the initalization sequence
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub EndInit() Implements ISupportInitialize.EndInit
            InInit = False
        End Sub
    End Class
End Namespace