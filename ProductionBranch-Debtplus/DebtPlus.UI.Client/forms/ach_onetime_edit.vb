﻿Imports DebtPlus.LINQ
Imports System.Linq

Namespace forms
    Public Class ach_onetime_edit

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Private record As DebtPlus.LINQ.ach_onetime

        ''' <summary>
        ''' Initialize the new form class
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        ''' <summary>
        ''' Initialize the new form class
        ''' </summary>
        ''' <param name="record">Address of the record to be edited</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal record As DebtPlus.LINQ.ach_onetime)
            MyClass.New()
            Me.record = record
            RegisterHandlers()
        End Sub

        ''' <summary>
        ''' Register the event handlers
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf ach_onetime_edit_Load
            AddHandler SimpleButton2.Click, AddressOf SimpleButton2_Click
            AddHandler DateEdit_EffectiveDate.Validating, AddressOf DateEdit_EffectiveDate_Validating
            AddHandler LayoutControl1.Resize, AddressOf LayoutControl1_Resize
            AddHandler TextEdit_ABA.Validating, AddressOf TextEdit_ABA_Validating
            AddHandler TextEdit_AccountNumber.KeyPress, AddressOf TextEdit_AccountNumber_KeyPress
            AddHandler TextEdit_AccountNumber.Validated, AddressOf TextEdit_AccountNumber_Validated
            AddHandler LookUpEdit_bank.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest

            AddHandler TextEdit_ABA.EditValueChanged, AddressOf FieldChanged
            AddHandler TextEdit_AccountNumber.EditValueChanged, AddressOf FieldChanged
            AddHandler CalcEdit_Amount.EditValueChanged, AddressOf FieldChanged
            AddHandler DateEdit_EffectiveDate.EditValueChanged, AddressOf FieldChanged
            AddHandler CheckEdit_CheckingSavings.EditValueChanged, AddressOf FieldChanged
            AddHandler LookUpEdit_bank.EditValueChanged, AddressOf FieldChanged

        End Sub

        ''' <summary>
        ''' Remove the event handler registrations
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub UnRegisterHandlers()
            RemoveHandler Load, AddressOf ach_onetime_edit_Load
            RemoveHandler SimpleButton2.Click, AddressOf SimpleButton2_Click
            RemoveHandler DateEdit_EffectiveDate.Validating, AddressOf DateEdit_EffectiveDate_Validating
            RemoveHandler LayoutControl1.Resize, AddressOf LayoutControl1_Resize
            RemoveHandler TextEdit_ABA.Validating, AddressOf TextEdit_ABA_Validating
            RemoveHandler TextEdit_AccountNumber.KeyPress, AddressOf TextEdit_AccountNumber_KeyPress
            RemoveHandler TextEdit_AccountNumber.Validated, AddressOf TextEdit_AccountNumber_Validated
            RemoveHandler LookUpEdit_bank.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest

            RemoveHandler TextEdit_ABA.EditValueChanged, AddressOf FieldChanged
            RemoveHandler TextEdit_AccountNumber.EditValueChanged, AddressOf FieldChanged
            RemoveHandler CalcEdit_Amount.EditValueChanged, AddressOf FieldChanged
            RemoveHandler DateEdit_EffectiveDate.EditValueChanged, AddressOf FieldChanged
            RemoveHandler CheckEdit_CheckingSavings.EditValueChanged, AddressOf FieldChanged
            RemoveHandler LookUpEdit_bank.EditValueChanged, AddressOf FieldChanged

        End Sub

        ''' <summary>
        ''' Process the form's LOAD event
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub ach_onetime_edit_Load(sender As Object, e As System.EventArgs)

            UnRegisterHandlers()

            Try
                ' Set the initial size to center the buttons when the form is loaded
                EmptySpaceItem_Left.Width = (EmptySpaceItem_Left.Width + EmptySpaceItem_Right.Width) \ 2

                LookUpEdit_bank.Properties.DataSource = DebtPlus.LINQ.Cache.bank.getList().FindAll(Function(s) s.type = "A")

                ' Set the control values with the current record information
                TextEdit_ABA.EditValue = record.ABA
                TextEdit_AccountNumber.EditValue = record.AccountNumber
                CheckEdit_CheckingSavings.Checked = If(record.CheckingSavings = "S"c, True, False)
                CalcEdit_Amount.EditValue = record.Amount
                DateEdit_EffectiveDate.EditValue = record.EffectiveDate
                LookUpEdit_bank.EditValue = record.bank

                ' If the record has already been submitted then it is read-only
                If record.deposit_batch_date IsNot Nothing Then
                    TextEdit_ABA.Properties.ReadOnly = True
                    TextEdit_AccountNumber.Properties.ReadOnly = True
                    CalcEdit_Amount.Properties.ReadOnly = True
                    DateEdit_EffectiveDate.Properties.ReadOnly = True
                    CheckEdit_CheckingSavings.Properties.ReadOnly = True
                    CheckEdit1.Properties.ReadOnly = True
                    LookUpEdit_bank.Properties.ReadOnly = True

                    SimpleButton2.Enabled = False
                    Return
                End If

                ' Define the minimum value for the effective date. It needs to be two days from now (1 for the bank, 1 for the file submission)
                Dim MinimumDate As DateTime = DebtPlus.Utils.Calendar.NextWorkingDay(DateTime.Today.Date)

                ' Ensure that the value of the record is at least the minimum value or we will get an error setting it.
                Dim itemDate As System.Nullable(Of DateTime) = record.EffectiveDate
                If itemDate Is Nothing Then
                    DateEdit_EffectiveDate.Properties.MinValue = DateTime.Now.Date.AddDays(-29)
                    DateEdit_EffectiveDate.Properties.MaxValue = DateTime.Now.Date.AddDays(29)
                    itemDate = MinimumDate
                End If

                ' Update the control settings
                DateEdit_EffectiveDate.EditValue = itemDate

                ' Enable the OK button if the record is valid.
                SimpleButton2.Enabled = Not HasErrors()

            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Find the next working day for the bank
        ''' </summary>
        ''' <returns>The next working day for the bank</returns>
        ''' <remarks></remarks>
        Public Shared Function NextWorkingDay(ByVal InputDateTime As DateTime) As DateTime

            ' Ensure that it does not fall on a weekend
            Select Case InputDateTime.DayOfWeek
                Case DayOfWeek.Friday
                    Return InputDateTime.Date.AddDays(3) ' FRI -> MON
                Case DayOfWeek.Saturday
                    Return InputDateTime.Date.AddDays(2) ' SAT -> MON
                Case Else
                    Return InputDateTime.Date.AddDays(1) ' SUN -> MON, MON -> TUE, TUE -> WED, THU -> FRI
            End Select

        End Function

        ''' <summary>
        ''' Process the CLICK event on the OK button to reset the record values
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub SimpleButton2_Click(sender As Object, e As System.EventArgs)

            ' Retrieve the values from the control settings when the record is changed
            record.bank = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_bank.EditValue).GetValueOrDefault()
            record.ABA = DebtPlus.Utils.Nulls.v_String(TextEdit_ABA.EditValue)
            record.AccountNumber = DebtPlus.Utils.Nulls.v_String(TextEdit_AccountNumber.EditValue)
            record.CheckingSavings = If(CheckEdit_CheckingSavings.Checked, "S"c, "C"c)
            record.Amount = Convert.ToDecimal(CalcEdit_Amount.EditValue)
            record.EffectiveDate = Convert.ToDateTime(DateEdit_EffectiveDate.EditValue).Date()

        End Sub

        ''' <summary>
        ''' Do not allow invalid effective dates to be entered
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub DateEdit_EffectiveDate_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs)
            If DateEdit_EffectiveDate.EditValue IsNot Nothing Then

                ' Find the day of the week. Do not allow a value for Saturday or Sunday to be used.
                Dim NewDate As DateTime = Convert.ToDateTime(DateEdit_EffectiveDate.EditValue)
                If NewDate.DayOfWeek = DayOfWeek.Saturday OrElse NewDate.DayOfWeek = DayOfWeek.Sunday Then
                    DateEdit_EffectiveDate.ErrorText = "Must be Monday through Friday"
                    e.Cancel = True
                    Return
                End If

            End If

            DateEdit_EffectiveDate.ErrorText = String.Empty
        End Sub

        ''' <summary>
        ''' Process a change in a data field to enable or disable the OK button
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub FieldChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            SimpleButton2.Enabled = Not HasErrors()
        End Sub

        ''' <summary>
        ''' Determine if there are any errors on the form
        ''' </summary>
        ''' <returns>TRUE if there is an error in the data as entered</returns>
        ''' <remarks></remarks>
        Private Function HasErrors() As Boolean
            If CalcEdit_Amount.EditValue Is Nothing Then Return True
            If DateEdit_EffectiveDate.EditValue Is Nothing Then Return True
            If TextEdit_ABA.EditValue Is Nothing Then Return True
            If TextEdit_AccountNumber.EditValue Is Nothing Then Return True
            If Not DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_bank.EditValue).HasValue Then Return True

            If Convert.ToDecimal(CalcEdit_Amount.EditValue) <= 0D Then Return True
            If Convert.ToString(TextEdit_ABA.EditValue).Length <> 9 Then Return True
            If Convert.ToString(TextEdit_AccountNumber.EditValue).Trim().Length = 0 Then Return True

            Return False
        End Function

        ''' <summary>
        ''' Handle the change in the size of the controls to center the two buttons on the form
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub LayoutControl1_Resize(sender As Object, e As System.EventArgs)
            EmptySpaceItem_Left.Width = (EmptySpaceItem_Left.Width + EmptySpaceItem_Right.Width) \ 2
        End Sub

        Private Sub TextEdit_ABA_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs)
            Dim ErrorString As String = String.Empty
            Dim StringValue As String = DebtPlus.Utils.Nulls.v_String(TextEdit_ABA.EditValue)

            If Not String.IsNullOrEmpty(StringValue) Then
                If Not System.Text.RegularExpressions.Regex.IsMatch(StringValue, "^[0-9]{9}$") Then
                    ErrorString = "value must be 9 digits"
                Else
                    Dim TotalValue As Int32 = 0
                    Dim Factor() As Int32 = New Int32() {3, 7, 1, 3, 7, 1, 3, 7, 1}
                    For Idx As Int32 = 0 To 8
                        Dim DigitValue As Int32 = Convert.ToInt32(StringValue.Chars(Idx)) - 48
                        TotalValue += (DigitValue * Factor(Idx))
                    Next

                    ' The sum should be an even multiple of 10
                    If (TotalValue Mod 10) <> 0 Then
                        ErrorString = "invalid checksum"
                    End If
                End If
            End If

            TextEdit_ABA.ErrorText = ErrorString
            e.Cancel = ErrorString <> String.Empty
        End Sub

        Private Sub TextEdit_AccountNumber_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs)

            ' Do now allow leading spaces in the account number
            If TextEdit_AccountNumber.SelectionStart = 0 AndAlso e.KeyChar = " "c Then
                e.Handled = True
            End If

        End Sub

        Private Sub TextEdit_AccountNumber_Validated(sender As Object, e As System.EventArgs)

            UnRegisterHandlers()
            Try

                ' Remove leading and trailing spaces from the account number
                TextEdit_AccountNumber.Text = TextEdit_AccountNumber.Text.Trim()

            Finally
                RegisterHandlers()
            End Try
        End Sub
    End Class
End Namespace