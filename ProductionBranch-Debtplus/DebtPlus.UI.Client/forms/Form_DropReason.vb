#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.UI.Client.Service

Namespace forms

    Friend Class Form_DropReason
        Implements IContext

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Private privateContext As ClientUpdateClass = Nothing

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Load, AddressOf Form_DropReason_Load
        End Sub

        Public Sub New(ByVal context As ClientUpdateClass)
            MyClass.New()
            privateContext = context
        End Sub

        Public ReadOnly Property Context() As ClientUpdateClass Implements IContext.Context
            Get
                Return privateContext
            End Get
        End Property

        Private Sub Form_DropReason_Load(ByVal sender As Object, ByVal e As EventArgs)
            lk_Drop_Reason.Properties.DataSource = DebtPlus.LINQ.Cache.drop_reason.getList()
            AddHandler lk_Drop_Reason.EditValueChanging, AddressOf lk_Drop_Reason_EditValueChanging
        End Sub

        Private Sub lk_Drop_Reason_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)
            SimpleButton1.Enabled = (Not e.NewValue Is Nothing AndAlso e.NewValue IsNot DBNull.Value)
        End Sub
    End Class
End Namespace