﻿Namespace forms.Housing
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class HPFOutcome
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.SimpleButton_Cancel = New DevExpress.XtraEditors.SimpleButton()
            Me.SimpleButton_OK = New DevExpress.XtraEditors.SimpleButton()
            Me.TextEdit_extRefOtherName = New DevExpress.XtraEditors.TextEdit()
            Me.LookUpEdit_outcomeTypeID = New DevExpress.XtraEditors.LookUpEdit()
            Me.textEdit_nonProfitReferralName = New DevExpress.XtraEditors.TextEdit()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem_left = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem_right = New DevExpress.XtraLayout.EmptySpaceItem()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.TextEdit_extRefOtherName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_outcomeTypeID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.textEdit_nonProfitReferralName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem_left, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem_right, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.SimpleButton_Cancel)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton_OK)
            Me.LayoutControl1.Controls.Add(Me.TextEdit_extRefOtherName)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_outcomeTypeID)
            Me.LayoutControl1.Controls.Add(Me.textEdit_nonProfitReferralName)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(401, 150)
            Me.LayoutControl1.TabIndex = 0
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'SimpleButton_Cancel
            '
            Me.SimpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.SimpleButton_Cancel.Location = New System.Drawing.Point(245, 115)
            Me.SimpleButton_Cancel.MaximumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.MinimumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.Name = "SimpleButton_Cancel"
            Me.SimpleButton_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.StyleController = Me.LayoutControl1
            Me.SimpleButton_Cancel.TabIndex = 8
            Me.SimpleButton_Cancel.Text = "&Cancel"
            '
            'SimpleButton_OK
            '
            Me.SimpleButton_OK.Location = New System.Drawing.Point(166, 115)
            Me.SimpleButton_OK.MaximumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.MinimumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.Name = "SimpleButton_OK"
            Me.SimpleButton_OK.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.StyleController = Me.LayoutControl1
            Me.SimpleButton_OK.TabIndex = 7
            Me.SimpleButton_OK.Text = "&OK"
            '
            'TextEdit_extRefOtherName
            '
            Me.TextEdit_extRefOtherName.Location = New System.Drawing.Point(132, 60)
            Me.TextEdit_extRefOtherName.Name = "TextEdit_extRefOtherName"
            Me.TextEdit_extRefOtherName.Properties.MaxLength = 50
            Me.TextEdit_extRefOtherName.Size = New System.Drawing.Size(257, 20)
            Me.TextEdit_extRefOtherName.StyleController = Me.LayoutControl1
            Me.TextEdit_extRefOtherName.TabIndex = 6
            '
            'LookUpEdit_outcomeTypeID
            '
            Me.LookUpEdit_outcomeTypeID.Location = New System.Drawing.Point(132, 12)
            Me.LookUpEdit_outcomeTypeID.Name = "LookUpEdit_outcomeTypeID"
            Me.LookUpEdit_outcomeTypeID.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_outcomeTypeID.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.LookUpEdit_outcomeTypeID.Properties.DisplayMember = "description"
            Me.LookUpEdit_outcomeTypeID.Properties.NullText = ""
            Me.LookUpEdit_outcomeTypeID.Properties.ShowFooter = False
            Me.LookUpEdit_outcomeTypeID.Properties.ShowHeader = False
            Me.LookUpEdit_outcomeTypeID.Properties.SortColumnIndex = 1
            Me.LookUpEdit_outcomeTypeID.Properties.ValueMember = "Id"
            Me.LookUpEdit_outcomeTypeID.Size = New System.Drawing.Size(257, 20)
            Me.LookUpEdit_outcomeTypeID.StyleController = Me.LayoutControl1
            Me.LookUpEdit_outcomeTypeID.TabIndex = 4
            '
            'textEdit_nonProfitReferralName
            '
            Me.textEdit_nonProfitReferralName.Location = New System.Drawing.Point(132, 36)
            Me.textEdit_nonProfitReferralName.Name = "textEdit_nonProfitReferralName"
            Me.textEdit_nonProfitReferralName.Properties.MaxLength = 50
            Me.textEdit_nonProfitReferralName.Size = New System.Drawing.Size(257, 20)
            Me.textEdit_nonProfitReferralName.StyleController = Me.LayoutControl1
            Me.textEdit_nonProfitReferralName.TabIndex = 5
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup1.GroupBordersVisible = False
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.EmptySpaceItem1, Me.EmptySpaceItem_left, Me.EmptySpaceItem_right})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(401, 150)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.LookUpEdit_outcomeTypeID
            Me.LayoutControlItem1.CustomizationFormText = "Outcome Type"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(381, 24)
            Me.LayoutControlItem1.Text = "Outcome Type"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(117, 13)
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.textEdit_nonProfitReferralName
            Me.LayoutControlItem2.CustomizationFormText = "Non-Profit Referral"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(381, 24)
            Me.LayoutControlItem2.Text = "Non-Profit Referral"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(117, 13)
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.TextEdit_extRefOtherName
            Me.LayoutControlItem3.CustomizationFormText = "Other Referenced Name"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 48)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(381, 24)
            Me.LayoutControlItem3.Text = "Other Referenced Name"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(117, 13)
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.SimpleButton_OK
            Me.LayoutControlItem4.CustomizationFormText = "LayoutControlItem4"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(154, 103)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(79, 27)
            Me.LayoutControlItem4.Text = "LayoutControlItem4"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem4.TextToControlDistance = 0
            Me.LayoutControlItem4.TextVisible = False
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Control = Me.SimpleButton_Cancel
            Me.LayoutControlItem5.CustomizationFormText = "LayoutControlItem5"
            Me.LayoutControlItem5.Location = New System.Drawing.Point(233, 103)
            Me.LayoutControlItem5.Name = "LayoutControlItem5"
            Me.LayoutControlItem5.Size = New System.Drawing.Size(79, 27)
            Me.LayoutControlItem5.Text = "LayoutControlItem5"
            Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem5.TextToControlDistance = 0
            Me.LayoutControlItem5.TextVisible = False
            '
            'EmptySpaceItem1
            '
            Me.EmptySpaceItem1.AllowHotTrack = False
            Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 72)
            Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Size = New System.Drawing.Size(381, 31)
            Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
            Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem_left
            '
            Me.EmptySpaceItem_left.AllowHotTrack = False
            Me.EmptySpaceItem_left.CustomizationFormText = "EmptySpaceItem_left"
            Me.EmptySpaceItem_left.Location = New System.Drawing.Point(0, 103)
            Me.EmptySpaceItem_left.Name = "EmptySpaceItem_left"
            Me.EmptySpaceItem_left.Size = New System.Drawing.Size(154, 27)
            Me.EmptySpaceItem_left.Text = "EmptySpaceItem_left"
            Me.EmptySpaceItem_left.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem_right
            '
            Me.EmptySpaceItem_right.AllowHotTrack = False
            Me.EmptySpaceItem_right.CustomizationFormText = "EmptySpaceItem_right"
            Me.EmptySpaceItem_right.Location = New System.Drawing.Point(312, 103)
            Me.EmptySpaceItem_right.Name = "EmptySpaceItem_right"
            Me.EmptySpaceItem_right.Size = New System.Drawing.Size(69, 27)
            Me.EmptySpaceItem_right.Text = "EmptySpaceItem_right"
            Me.EmptySpaceItem_right.TextSize = New System.Drawing.Size(0, 0)
            '
            'HPFOutcome
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(401, 150)
            Me.Controls.Add(Me.LayoutControl1)
            Me.Name = "HPFOutcome"
            Me.Text = "HPF Outcome Entry"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.TextEdit_extRefOtherName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_outcomeTypeID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.textEdit_nonProfitReferralName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem_left, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem_right, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents SimpleButton_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents TextEdit_extRefOtherName As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LookUpEdit_outcomeTypeID As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents SimpleButton_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem_left As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem_right As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents textEdit_nonProfitReferralName As DevExpress.XtraEditors.TextEdit
    End Class
End Namespace