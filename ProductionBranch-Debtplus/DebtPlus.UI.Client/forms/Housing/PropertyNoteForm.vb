﻿#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DebtPlus.UI.Client.Service

Namespace forms.Housing
    Friend Class PropertyNoteForm

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Private noteRecord As DebtPlus.LINQ.Housing_PropertyNote = Nothing
        Private allowEdit As Boolean = False

        ''' <summary>
        ''' Initialize the new class
        ''' </summary>
        ''' <param name="propertyNote">Pointer to the note record to be edited</param>
        ''' <param name="allowEdit">Do we allow the note to be edited? If false, the note may not be changed. If true, the normal edit operation is allowed.</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal propertyNote As DebtPlus.LINQ.Housing_PropertyNote, allowEdit As Boolean)
            MyBase.New()
            InitializeComponent()
            Me.noteRecord = propertyNote
            Me.allowEdit = allowEdit
            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf PropertyNoteForm_Load
            AddHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler Load, AddressOf PropertyNoteForm_Load
            RemoveHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
        End Sub

        Private Sub PropertyNoteForm_Load(sender As Object, e As System.EventArgs)
            UnRegisterHandlers()
            Try
                MemoEdit1.EditValue = noteRecord.NoteBuffer

                ' Make the input field read-only if we don't allow the edit operation to continue.
                ' DO NOT use the "Enabled" field. That would block the scroll bars from working.
                If Not allowEdit Then
                    MemoEdit1.Properties.ReadOnly = True
                    SimpleButton_OK.Enabled = False
                Else
                    MemoEdit1.Properties.ReadOnly = False
                    SimpleButton_OK.Enabled = True
                End If

            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Sub SimpleButton_OK_Click(sender As Object, e As System.EventArgs)
            noteRecord.NoteBuffer = DebtPlus.Utils.Nulls.v_String(MemoEdit1.EditValue)
        End Sub
    End Class
End Namespace