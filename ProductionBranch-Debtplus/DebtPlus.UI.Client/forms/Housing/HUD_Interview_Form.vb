#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.Utils.Format
Imports System.Linq
Imports DebtPlus.LINQ

Namespace forms.Housing
    Public Class HUD_Interview_Form

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Private minutes As Int32 = 0
        Private povRecord As hud_interview = Nothing
        Private bc As BusinessContext = Nothing
        Private clientID As Int32 = -1

        ''' <summary>
        ''' Resulting transaction record for the event. Only generated if a time or grant money is entered.
        ''' </summary>
        Public Property HudTransaction As hud_transaction

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            HudTransaction = Nothing
        End Sub

        Public Sub New(bc As BusinessContext, ByVal ClientID As Int32)
            MyClass.New(bc, ClientID, 0)
        End Sub

        Public Sub New(bc As BusinessContext, ByVal ClientID As Int32, ByVal minutes As Int32)
            MyClass.New()
            Me.bc = bc
            Me.clientID = ClientID
            Me.minutes = minutes

            ' Load the list of possible values into the lookup controls
            LookUpEdit_HUD_Grant.Properties.DataSource = DebtPlus.LINQ.Cache.Housing_GrantType.getList()
            LookUpEdit_interview.Properties.DataSource = DebtPlus.LINQ.Cache.Housing_PurposeOfVisitType.getList()
            LookUpEdit_termination.Properties.DataSource = DebtPlus.LINQ.Cache.Housing_TerminationReasonType.getList()

            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf Housing_Form_Load
            AddHandler SimpleButton1.Click, AddressOf SimpleButton1_Click

            AddHandler SpinEdit_event.EditValueChanging, AddressOf Edit_EditValueChanging
            AddHandler SpinEdit_event.EditValueChanged, AddressOf SpinEdit_event_EditValueChanged
            AddHandler CalcEdit_GrantAmountUsed.EditValueChanging, AddressOf Edit_EditValueChanging
            AddHandler CalcEdit_HousingFeeAmount.EditValueChanging, AddressOf Edit_EditValueChanging

            AddHandler LookUpEdit_interview.EditValueChanging, AddressOf LookUpEdit_interview_EditValueChanging
            AddHandler LookUpEdit_interview.EditValueChanged, AddressOf LookUpEdit_interview_EditValueChanged
            AddHandler LookUpEdit_result.EditValueChanging, AddressOf LookUpEdit_result_EditValueChanging

            AddHandler LookUpEdit_result.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler LookUpEdit_interview.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler LookUpEdit_termination.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler LookUpEdit_HUD_Grant.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler Load, AddressOf Housing_Form_Load
            RemoveHandler SimpleButton1.Click, AddressOf SimpleButton1_Click

            RemoveHandler SpinEdit_event.EditValueChanging, AddressOf Edit_EditValueChanging
            RemoveHandler SpinEdit_event.EditValueChanged, AddressOf SpinEdit_event_EditValueChanged
            RemoveHandler CalcEdit_GrantAmountUsed.EditValueChanging, AddressOf Edit_EditValueChanging
            RemoveHandler CalcEdit_HousingFeeAmount.EditValueChanging, AddressOf Edit_EditValueChanging

            RemoveHandler LookUpEdit_interview.EditValueChanging, AddressOf LookUpEdit_interview_EditValueChanging
            RemoveHandler LookUpEdit_interview.EditValueChanged, AddressOf LookUpEdit_interview_EditValueChanged
            RemoveHandler LookUpEdit_result.EditValueChanging, AddressOf LookUpEdit_result_EditValueChanging

            RemoveHandler LookUpEdit_result.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler LookUpEdit_interview.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler LookUpEdit_termination.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler LookUpEdit_HUD_Grant.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
        End Sub

        ''' <summary>
        ''' Process the load event for the form. Bind the controls to the record
        ''' </summary>
        Private Sub Housing_Form_Load(ByVal sender As Object, ByVal e As EventArgs)
            UnRegisterHandlers()
            Try

                ' Find the pending HUD purpose of visit to see if we can add to it
                povRecord = bc.hud_interviews.Where(Function(pov) pov.client = clientID).OrderByDescending(Function(pov) pov.interview_date).Take(1).FirstOrDefault()

                ' If there is a pending transaction then determine if the operation is within the last year
                If povRecord IsNot Nothing AndAlso povRecord.termination_date.HasValue AndAlso povRecord.termination_date.Value <= DateTime.Now.Date.AddMonths(-12) Then
                    povRecord = Nothing
                End If

                ' If there is a pending transaction then ask if it is to be amended
                If povRecord IsNot Nothing AndAlso povRecord.termination_date.HasValue Then

                    ' Find the type of the interview from the record
                    Dim interviewDescription As String = "an unknown type"
                    Dim q As Housing_PurposeOfVisitType = DebtPlus.LINQ.Cache.Housing_PurposeOfVisitType.getList().Find(Function(s) s.Id = povRecord.interview_type)
                    If q IsNot Nothing Then
                        interviewDescription = q.description
                    End If

                    ' Ask if the interview is to be continued
                    If DebtPlus.Data.Forms.MessageBox.Show(String.Format("A previously created {1}visit was created{0}on {2:d} and closed on {3:d}.{0}{0}You should not create an new visit for the same reason within{0}one year for the client.{0}{0}Do you want to re-open the previously closed visit and{0}add additional minutes to it?{0}{0}You should answer YES if you want to add additional minutes{0}to the above visit and NO if this visit is a new issue.", Environment.NewLine, interviewDescription, povRecord.interview_date, povRecord.termination_date.Value), "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1) = DialogResult.Yes Then

                        ' Erase the termination information
                        povRecord.termination_date = Nothing
                        povRecord.termination_reason = Nothing
                        povRecord.termination_counselor = Nothing

                        ' Erase the result information
                        povRecord.result_date = Nothing
                        povRecord.hud_result = Nothing
                        povRecord.result_counselor = Nothing

                    Else
                        povRecord = Nothing
                    End If
                End If

                ' If there is no POV record then we need to create a new one
                If povRecord Is Nothing Then
                    povRecord = DebtPlus.LINQ.Factory.Manufacture_hud_interview(clientID)
                End If

                ' Set the static display text
                LabelControl_result_date_created.Text = If(povRecord.result_date.HasValue, String.Format("{0:d}", povRecord.result_date.Value), String.Empty)
                LabelControl_result_created_by.Text = If(String.IsNullOrEmpty(povRecord.result_counselor), String.Empty, povRecord.result_counselor)
                LabelControl_interview_date_created.Text = String.Format("{0:d}", povRecord.interview_date)
                LabelControl_interview_created_by.Text = povRecord.interview_counselor

                ' Load the other values into the controls
                SpinEdit_event.EditValue = minutes
                CalcEdit_GrantAmountUsed.EditValue = 0D
                CalcEdit_HousingFeeAmount.EditValue = povRecord.HousingFeeAmount
                LookUpEdit_interview.EditValue = povRecord.interview_type
                LookUpEdit_termination.EditValue = povRecord.termination_reason
                LookUpEdit_HUD_Grant.EditValue = povRecord.HUD_Grant
                LookUpEdit_result.EditValue = povRecord.hud_result

                ' If there is a result type for an existing record then lock the interview type
                If povRecord.Id > 0 AndAlso povRecord.hud_result.GetValueOrDefault(0) > 0 Then
                    LookUpEdit_interview.Properties.ReadOnly = True
                    CalcEdit_HousingFeeAmount.Properties.ReadOnly = True
                    LookUpEdit_HUD_Grant.Properties.ReadOnly = True
                End If

                ' Refresh the form based upon the current record items
                EnableControls(povRecord.interview_type)
                CheckList_ImpactsAndScope.ReadForm(bc, povRecord)
                UpdateLabel(minutes)

            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Enable the controls when an interview is being chosen
        ''' </summary>
        Private Sub LookUpEdit_interview_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)
            EnableControls(DebtPlus.Utils.Nulls.v_Int32(e.NewValue))
            CheckList_ImpactsAndScope.SetIndicatorsForResult(DebtPlus.Utils.Nulls.v_Int32(e.NewValue), DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_result.EditValue))
        End Sub

        ''' <summary>
        ''' Enable the controls when an result is chosen
        ''' </summary>
        Private Sub LookUpEdit_result_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)
            CheckList_ImpactsAndScope.SetIndicatorsForResult(DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_interview.EditValue), DebtPlus.Utils.Nulls.v_Int32(e.NewValue))
        End Sub

        ''' <summary>
        ''' Enable the controls when an interview is chosen
        ''' </summary>
        Private Sub EnableControls(ByVal pov As System.Nullable(Of Int32))
            Dim isReadOnly As Boolean = pov.GetValueOrDefault(0) < 1

            ' Mark the items as "locked" if there is no interview
            LookUpEdit_termination.Properties.ReadOnly = isReadOnly
            LookUpEdit_result.Properties.ReadOnly = isReadOnly
            SpinEdit_event.Properties.ReadOnly = isReadOnly
            CalcEdit_GrantAmountUsed.Properties.ReadOnly = isReadOnly

            ' Disable the controls that don't support ReadOnly
            SimpleButton1.Enabled = Not isReadOnly
            CheckList_ImpactsAndScope.Enabled = Not isReadOnly

            ' Correct the list of possible entries in the results table
            If isReadOnly Then
                LookUpEdit_result.Properties.DataSource = Nothing
            Else
                LookUpEdit_result.Properties.DataSource = bc.Housing_ResultTypes.Join(bc.Housing_AllowedVisitOutcomeTypes, Function(hr) hr.Id, Function(avot) avot.Outcome, Function(hr, avot) New With {Key .hr = hr, Key .avot = avot}).Where(Function(x) x.avot.PurposeOfVisit = pov.Value).Select(Function(x) x.hr)
                LookUpEdit_result.RefreshEditValue()
            End If

        End Sub

        ''' <summary>
        ''' Change the result set list when interview changes
        ''' </summary>
        Private Sub LookUpEdit_interview_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)

            ' Find the value and enable or disable the controls accordingly
            Dim pov As System.Nullable(Of Int32) = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_interview.EditValue)
            EnableControls(pov)

            ' If there is a housing fee amount then set it into the control
            If pov.HasValue Then
                Dim q As DebtPlus.LINQ.Housing_PurposeOfVisitType = DebtPlus.LINQ.Cache.Housing_PurposeOfVisitType.getList().Find(Function(s) s.Id = pov.Value)
                If q IsNot Nothing AndAlso q.HousingFeeAmount > 0D Then
                    CalcEdit_HousingFeeAmount.EditValue = q.HousingFeeAmount
                End If
            End If
        End Sub

        ''' <summary>
        ''' Process the OK button CLICK event
        ''' </summary>
        Private Sub SimpleButton1_Click(ByVal sender As Object, ByVal e As EventArgs)

            ' Retrieve the values from the input controls
            povRecord.termination_reason = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_termination.EditValue)
            povRecord.hud_result = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_result.EditValue)
            povRecord.HousingFeeAmount = Convert.ToDecimal(CalcEdit_HousingFeeAmount.EditValue)
            povRecord.interview_type = Convert.ToInt32(LookUpEdit_interview.EditValue)
            povRecord.HUD_Grant = Convert.ToInt32(LookUpEdit_HUD_Grant.EditValue)

            ' There must always be a client.
            Debug.Assert(povRecord.client > 0)

            ' If there is a result then set the result date
            If povRecord.hud_result.HasValue AndAlso Not povRecord.result_date.HasValue Then
                povRecord.result_date = DebtPlus.LINQ.BusinessContext.getdate()
                povRecord.result_counselor = DebtPlus.LINQ.BusinessContext.suser_sname()
            End If

            ' If there is a termination reason then set the termination date
            If povRecord.termination_reason.HasValue AndAlso Not povRecord.termination_date.HasValue Then
                povRecord.termination_date = DebtPlus.LINQ.BusinessContext.getdate()
                povRecord.termination_counselor = DebtPlus.LINQ.BusinessContext.suser_sname()
            End If

            ' Set the list of impacts. They are attached to the current record.
            CheckList_ImpactsAndScope.SaveForm(bc)

            ' Generate the transaction record for the time intervals
            Dim minutes As Int32 = Convert.ToInt32(DebtPlus.Utils.Nulls.v_Decimal(SpinEdit_event.EditValue).GetValueOrDefault(0D))
            Dim grantUsed As Decimal = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_GrantAmountUsed.EditValue).GetValueOrDefault(0D)

            If minutes > 0 OrElse grantUsed > 0D Then
                HudTransaction = DebtPlus.LINQ.Factory.Manufacture_hud_transaction()
                HudTransaction.GrantAmountUsed = grantUsed
                HudTransaction.minutes = minutes

                ' Attach the transaction to the interview.
                povRecord.hud_transactions.Add(HudTransaction)
            End If

            Try
                ' If the record is not new then just do the database update operation and leave.
                If povRecord.Id > 0 Then
                    bc.SubmitChanges()
                    Return
                End If

                ' Create the new POV record for this client.
                bc.hud_interviews.InsertOnSubmit(povRecord)
                bc.SubmitChanges()

                ' We need to trip the custom POV logic when the new record is created.
                ' There is no need to refresh the record since the record is not updated by this procedure normally.
                ' (Even if it was, we are on the way out and will dispose of the record here....)
                bc.xpr_housing_custom_events(povRecord)

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
            End Try
        End Sub

        ''' <summary>
        ''' Update the label for minutes as the value changes
        ''' </summary>
        Private Sub SpinEdit_event_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            UpdateLabel(DebtPlus.Utils.Nulls.v_Decimal(SpinEdit_event.EditValue))
        End Sub

        ''' <summary>
        ''' Update the label for minutes as the value changes
        ''' </summary>
        Private Sub UpdateLabel(ByVal Value As Decimal?)

            ' Update the text for the label and force a re-draw
            Dim FormattedString As String = DebtPlus.Utils.Format.Time.HoursAndMinutes(Value.GetValueOrDefault(0D))
            If FormattedString = String.Empty Then FormattedString = "in minutes"
            LabelControl_event_minutes.Text = String.Format("({0})", FormattedString)
            LabelControl_event_minutes.Refresh()
        End Sub

        ''' <summary>
        ''' Do not allow the minutes to go negative
        ''' </summary>
        Private Sub Edit_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)
            Dim dblValue As Double = 0.0#
            If e.NewValue IsNot Nothing AndAlso Double.TryParse(Convert.ToString(e.NewValue), dblValue) Then
                If dblValue < 0.0# Then
                    e.Cancel = True
                End If
            End If
        End Sub

        ''' <summary>
        ''' Center the buttons on the form
        ''' </summary>
        Private Sub LayoutControl1_Resize(sender As Object, e As EventArgs)
            EmptySpaceItem_Left.Width = (EmptySpaceItem_Left.Width + EmptySpaceItem_Right.Width) \ 2
        End Sub
    End Class
End NameSpace
