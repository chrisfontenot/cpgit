﻿#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.Events
Imports DebtPlus.UI.Client.Service
Imports DebtPlus.UI.Client.forms.Housing
Imports DebtPlus.LINQ
Imports DevExpress.Utils
Imports System.Globalization
Imports System.Linq

Namespace forms.Housing
    Friend Class HPFOutcome
        Private record As DebtPlus.LINQ.HPFOutcome

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(ByVal record As DebtPlus.LINQ.HPFOutcome)
            MyBase.New()
            InitializeComponent()

            Me.record = record
            RegisterHandlers()

            ' Set the lookup data tables
            LookUpEdit_outcomeTypeID.Properties.DataSource = DebtPlus.LINQ.Cache.HPFOutcomeType.getList()
        End Sub

        ''' <summary>
        ''' Register the event handlers
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf HPFOutcome_Load
            AddHandler Resize, AddressOf HPFOutcome_Resize
            AddHandler TextEdit_extRefOtherName.EditValueChanged, AddressOf formChanged
            AddHandler LookUpEdit_outcomeTypeID.EditValueChanged, AddressOf formChanged
            AddHandler LookUpEdit_outcomeTypeID.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
            AddHandler TextEdit_extRefOtherName.EditValueChanging, AddressOf TextEdit_extRefOtherName_EditValueChanging
            AddHandler TextEdit_extRefOtherName.Validated, AddressOf TextEdit_extRefOtherName_Validated
        End Sub

        ''' <summary>
        ''' Remove the event handlers
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub UnRegisterHandlers()
            RemoveHandler Load, AddressOf HPFOutcome_Load
            RemoveHandler Resize, AddressOf HPFOutcome_Resize
            RemoveHandler TextEdit_extRefOtherName.EditValueChanged, AddressOf formChanged
            RemoveHandler LookUpEdit_outcomeTypeID.EditValueChanged, AddressOf formChanged
            RemoveHandler LookUpEdit_outcomeTypeID.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
            RemoveHandler TextEdit_extRefOtherName.EditValueChanging, AddressOf TextEdit_extRefOtherName_EditValueChanging
            RemoveHandler TextEdit_extRefOtherName.Validated, AddressOf TextEdit_extRefOtherName_Validated
        End Sub

        ''' <summary>
        ''' If the form is resize,d center the buttons
        ''' </summary>
        Private Sub HPFOutcome_Resize(ByVal sender As Object, ByVal e As System.EventArgs)
            UnRegisterHandlers()
            Try
                EmptySpaceItem_left.Width = (EmptySpaceItem_left.Width + EmptySpaceItem_right.Width) \ 2
            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Process the LOAD event for the form
        ''' </summary>
        Private Sub HPFOutcome_Load(ByVal sender As Object, ByVal e As System.EventArgs)
            UnRegisterHandlers()
            Try

                ' Update the editing controls with the record contents
                TextEdit_extRefOtherName.EditValue = record.extRefOtherName
                textEdit_nonProfitReferralName.EditValue = record.NonProfitReferralName
                LookUpEdit_outcomeTypeID.EditValue = record.outcomeTypeID

                ' Enable/disable the OK button for the first time
                SimpleButton_OK.Enabled = Not HasErrors()

                ' Since the form is not being resized when it is created, do the resize operation now.
                EmptySpaceItem_left.Width = (EmptySpaceItem_left.Width + EmptySpaceItem_right.Width) \ 2
            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Determine if the form's controls have any error conditions that would prevent the OK button from being used.
        ''' </summary>
        Private Function HasErrors() As Boolean

            ' The only required items is the outcome id.
            Return LookUpEdit_outcomeTypeID.EditValue Is Nothing

        End Function

        ''' <summary>
        ''' Process a change in the form.
        ''' </summary>
        Private Sub formChanged(ByVal sender As Object, ByVal e As EventArgs)
            SimpleButton_OK.Enabled = Not HasErrors()
        End Sub

        ''' <summary>
        ''' Handle the click event on the OK button
        ''' </summary>
        Private Sub SimpleButton_OK_Click(ByVal sender As Object, ByVal e As System.EventArgs)

            ' Retrieve the current values for the record
            record.extRefOtherName = DebtPlus.Utils.Nulls.v_String(TextEdit_extRefOtherName.EditValue)
            record.NonProfitReferralName = DebtPlus.Utils.Nulls.v_String(textEdit_nonProfitReferralName.EditValue)
            record.outcomeTypeID = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_outcomeTypeID.EditValue)

            ' Complete the form normally
            DialogResult = Windows.Forms.DialogResult.OK
        End Sub

        ''' <summary>
        ''' Ensure that the name entry is not entered as blank
        ''' </summary>
        Private Sub TextEdit_extRefOtherName_EditValueChanging(sender As Object, e As DevExpress.XtraEditors.Controls.ChangingEventArgs)
            If e.NewValue Is Nothing Then Return
            Dim strValue As String = Convert.ToString(e.NewValue)

            ' Do not allow leading blanks in the name
            If strValue = String.Empty Then Return
            If System.Char.IsWhiteSpace(strValue(0)) Then
                e.Cancel = True
            End If
        End Sub

        ''' <summary>
        ''' Trim the extra spaces from the name entry
        ''' </summary>
        Private Sub TextEdit_extRefOtherName_Validated(sender As Object, e As System.EventArgs)
            If TextEdit_extRefOtherName.EditValue IsNot Nothing Then
                Dim strValue As String = Convert.ToString(TextEdit_extRefOtherName.EditValue)
                Dim newString As String = strValue.Trim()
                If strValue <> newString Then
                    TextEdit_extRefOtherName.EditValue = newString
                End If
            End If
        End Sub
    End Class
End Namespace