﻿Imports System
Imports System.Linq
Imports DebtPlus.LINQ

Public Class HPFUploadForm
    Implements Client.Service.IContext

    ' Use for logging purposes if needed
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

    Private ReadOnly privateContext As Service.ClientUpdateClass
    Public ReadOnly Property Context As Service.ClientUpdateClass Implements Service.IContext.Context
        Get
            Return privateContext
        End Get
    End Property

    Public Sub New()
        MyBase.New()
        InitializeComponent()
    End Sub

    Public Sub New(ByVal context As Service.ClientUpdateClass)
        MyClass.New()
        privateContext = context
        RegisterHandlers()
    End Sub

    Private Sub RegisterHandlers()
        AddHandler LayoutControl1.Resize, AddressOf LayoutControl1_Resize
        AddHandler Load, AddressOf HPFUploadForm_Load
        AddHandler ComboBoxEdit1.SelectedIndexChanged, AddressOf FormChanged
        AddHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
    End Sub

    Private Sub UnRegisterHandlers()
        RemoveHandler LayoutControl1.Resize, AddressOf LayoutControl1_Resize
        RemoveHandler Load, AddressOf HPFUploadForm_Load
        RemoveHandler ComboBoxEdit1.SelectedIndexChanged, AddressOf FormChanged
        RemoveHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
    End Sub

    ''' <summary>
    ''' Perform the upload on the indicated property to HPF
    ''' </summary>
    ''' <param name="propertyRecord"></param>
    ''' <param name="OverrideChecks"></param>
    ''' <remarks></remarks>
    Private Sub PerformUpload(ByVal propertyRecord As DebtPlus.LINQ.Housing_property, ByVal OverrideChecks As Boolean)

        Using cm As New DebtPlus.UI.Common.CursorManager()
            Dim fcID As System.Nullable(Of System.Int32)
            Dim resp As DebtPlus.SOAP.HPF.Agency.ForeclosureCaseSaveResponse = Nothing
            Dim request As DebtPlus.UI.Housing.HPF.Data.ForeclosureCaseSet = Nothing

            Using cls As New DebtPlus.UI.Housing.HPF.SubmissionCase()
                Try
                    ' Generate the HPF submission data from the property.
                    request = cls.GenerateForeclosureCaseSet(propertyRecord)
                    If request Is Nothing Then
                        Return
                    End If

                    ' If desired, do agency validation rules before we submit the case.
                    If Not CheckEdit_Override.Checked Then
                        resp = cls.Validate(request)
                    End If

                    ' If there is no error then submit the case to HPF
                    If resp Is Nothing OrElse resp.Status = DebtPlus.SOAP.HPF.Agency.ResponseStatus.Success Then
                        Using comm As New DebtPlus.SOAP.HPF.Communication()
                            Dim reqSet As DebtPlus.SOAP.HPF.Agency.ForeclosureCaseSetDTO = request.Extract()
                            Dim reqOp As DebtPlus.SOAP.HPF.Agency.ForeclosureCaseSaveRequest = New DebtPlus.SOAP.HPF.Agency.ForeclosureCaseSaveRequest() With {
                                .ForeclosureCaseSet = reqSet
                                }

                            resp = comm.Submit(reqOp)
                        End Using
                    End If

                Catch ex As Exception
                    Dim swConfig As New System.Diagnostics.BooleanSwitch("HousingHPFTrap", "Include message boxes in traps")
                    Dim trConfig As Boolean = swConfig.Enabled()
                    If trConfig Then DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Exception in submission", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error)

                    ' Generate a response message for the failure when an exception occurs
                    Dim colMessages As New System.Collections.Generic.List(Of DebtPlus.SOAP.HPF.Agency.ExceptionMessage)()
                    colMessages.Add(New DebtPlus.SOAP.HPF.Agency.ExceptionMessage() With {.ErrorCode = "SYS0000", .Message = ex.Message})

                    resp = New DebtPlus.SOAP.HPF.Agency.ForeclosureCaseSaveResponse() With _
                            {
                                .Messages = colMessages.ToArray(),
                                .Status = DebtPlus.SOAP.HPF.Agency.ResponseStatus.Fail
                            }
                End Try

                ' Generate the log event for the submission
                Using bc As New BusinessContext()
                    Dim record As DebtPlus.LINQ.HPFSubmission = DebtPlus.LINQ.Factory.Manufacture_HPFSubmission()
                    record.propertyID = propertyRecord.Id
                    record.requestType = DebtPlus.LINQ.HPFSubmission.requestType_SUBMIT

                    ' If there is a request then serialize it
                    If request IsNot Nothing Then
                        Dim strRequest As String = request.Serialize()
                        record.requestXML = System.Xml.Linq.XElement.Parse(strRequest)
                    End If

                    ' If we have a response then we can do something about it.
                    Dim note As DebtPlus.LINQ.client_note = DebtPlus.LINQ.Factory.Manufacture_client_note(Context.ClientDs.ClientId, 3)
                    note.subject = "HPF Submission: No Response"
                    If resp IsNot Nothing Then
                        note.subject = "HPF Submission: " + resp.Status.ToString()
                        note.note = resp.ToString()

                        fcID = resp.FcId
                        record.FcId = resp.FcId
                        record.responseStatus = resp.Status.ToString()

                        Dim strResponse As String = resp.Serialize()
                        record.responseXML = System.Xml.Linq.XElement.Parse(strResponse)

                        ' Update the housing property record with the foreclosure case ID so that re-submissions will be properly performed
                        ' Do this only if we have an FCID from the remote system. If there is an error on the submission, don't reset the
                        ' value back to null. Just leave it alone. It may be null to start or it may not be.
                        Dim q As Housing_property = bc.Housing_properties.Where(Function(s) s.Id = propertyRecord.Id).FirstOrDefault()
                        If q IsNot Nothing AndAlso fcID IsNot Nothing Then
                            q.HPF_foreclosureCaseID = fcID
                        End If
                    End If

                    bc.client_notes.InsertOnSubmit(note)
                    bc.HPFSubmissions.InsertOnSubmit(record)

                    ' Update the database with the changes now that they have been done.
                    bc.SubmitChanges()
                End Using

                ' After logging the entry in the database, we need a response to continue.
                If resp Is Nothing Then
                    DebtPlus.Data.Forms.MessageBox.Show("No response from the HPF submission", "HPF Submission failure")
                    Return
                End If

                ' If the operation is successful and it is FannieMae DQ then we need to add some additional data
                If resp.Status = DebtPlus.SOAP.HPF.Agency.ResponseStatus.Success AndAlso PropertyIsFannieMaeDQ(propertyRecord) Then
                    SendFannieMaeDQ(propertyRecord, request.ForeclosureCase.ActionItemsNotes, request.ForeclosureCase.LoanDfltReasonNotes, fcID.Value)
                End If

                DebtPlus.Data.Forms.MessageBox.Show(resp.ToString(), "HPF Submission complete")
            End Using
        End Using

        DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    ''' <summary>
    ''' Is the property a FannieMae DQ? These take special processing for the time being.
    ''' </summary>
    ''' <returns>TRUE if the property's program is FannieMae DQ</returns>
    Private Function PropertyIsFannieMaeDQ(ByVal propertyRecord As Housing_property) As Boolean

        ' Look at the program associated with the property. If it is the magic value for
        ' FanieMae DQ then return true.
        If propertyRecord IsNot Nothing AndAlso propertyRecord.HPF_Program.HasValue Then
            Dim q As HPFProgram = DebtPlus.LINQ.Cache.HPFProgram.getList().Find(Function(s) s.Id = propertyRecord.HPF_Program.Value)
            If q IsNot Nothing AndAlso q.programID = 2005 Then
                Return True
            End If
        End If

        ' This is not a FannieMae DQ property transaction
        Return False
    End Function

    Private Sub SendFannieMaeDQ(ByVal propertyRecord As DebtPlus.LINQ.Housing_property, ByVal ActionItemNotes As String, ByVal LoanDfltReasonNotes As String, ByVal fcID As Int32)
        Using bc As New BusinessContext()

            ' If there is a request then serialize it
            Dim record As DebtPlus.LINQ.HPFSubmission = DebtPlus.LINQ.Factory.Manufacture_HPFSubmission()
            record.propertyID = propertyRecord.Id
            record.requestType = DebtPlus.LINQ.HPFSubmission.requestType_SESSION

            ' Find the counselor from the property record
            Dim counselorRecord As DebtPlus.LINQ.counselor = Nothing
            If propertyRecord.HPF_counselor.HasValue Then
                counselorRecord = DebtPlus.LINQ.Cache.counselor.getList().Find(Function(s) s.Id = propertyRecord.HPF_counselor.Value)
            End If

            Try
                '
                ' Create a very canned event record for the completion item. This is the low level record structure
                ' and is not stored in the database.
                '
                Using EventFac As New DebtPlus.UI.Housing.HPF.SubmissionCase()
                    Dim evtRecord As DebtPlus.UI.Housing.HPF.Data.Event = EventFac.GenerateEvent(propertyRecord)
                    evtRecord.CounselorIdRef = DebtPlus.UI.Housing.HPF.Mapping.mapCounselor(counselorRecord)
                    evtRecord.ChgLstUserId = evtRecord.CounselorIdRef
                    evtRecord.FcId = fcID
                    evtRecord.EventTypeCd = "OUTCALL"
                    evtRecord.EventOutcomeCd = "SESSIONCOMPLETE"
                    evtRecord.Comments = "FannieMae DQ Completed Event"
                    evtRecord.CompletedInd = "Y"
                    evtRecord.ProgramStageId = 18
                    evtRecord.RpcInd = "Y"

                    '
                    ' Create a very canned session record for the completion item. This is the low level record structure
                    ' and is not stored in the database.
                    '
                    Dim sessRecord As DebtPlus.UI.Housing.HPF.Data.Session = EventFac.GenerateSession(propertyRecord)
                    sessRecord.ActionItemsNotes = ActionItemNotes
                    sessRecord.CompletionEvent = evtRecord
                    sessRecord.LoanDfltReasonNotes = LoanDfltReasonNotes
                    sessRecord.FcId = fcID
                    sessRecord.ChgLstUserId = evtRecord.CounselorIdRef

                    ' Serialize the request block
                    Dim strRequest As String = sessRecord.Serialize()
                    record.requestXML = System.Xml.Linq.XElement.Parse(strRequest)
                    bc.HPFSubmissions.InsertOnSubmit(record)

                    ' Send this session along to HPF as well.
                    '
                    Using cls As New DebtPlus.SOAP.HPF.Communication()
                        Dim sessionReqOp As New DebtPlus.SOAP.HPF.Agency.SessionSaveRequest() With {.Session = sessRecord.Extract()}
                        Dim resp As DebtPlus.SOAP.HPF.Agency.SessionSaveResponse = cls.Submit(sessionReqOp)

                        ' Serialize the response block
                        If resp IsNot Nothing Then
                            Dim strResponse As String = resp.Serialize()
                            record.responseXML = System.Xml.Linq.XElement.Parse(strResponse)
                        End If
                    End Using
                End Using

            Catch ex As System.Exception
                Dim swConfig As New System.Diagnostics.BooleanSwitch("HousingHPFTrap", "Include message boxes in traps")
                Dim trConfig As Boolean = swConfig.Enabled()
                If trConfig Then DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Exception in submission", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error)

                Dim strResponse As String = ex.ToString()
                record.responseXML = System.Xml.Linq.XElement.Parse(strResponse)
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error submitting HPF case")

            Finally
                bc.SubmitChanges()
            End Try
        End Using
    End Sub

    Private Sub LayoutControl1_Resize(sender As Object, e As System.EventArgs)
        UnRegisterHandlers()
        Try
            EmptySpaceItem_Left.Width = (EmptySpaceItem_Left.Width + EmptySpaceItem_Right.Width) \ 2
        Finally
            RegisterHandlers()
        End Try
    End Sub

    Private Sub HPFUploadForm_Load(sender As Object, e As System.EventArgs)
        UnRegisterHandlers()
        Try
            Using bc As New BusinessContext()

                ' Enable or disable the checkbox to allow the override to be set
                CheckEdit_Override.Checked = False
                CheckEdit_Override.Enabled = IsAdmin(bc)

                ' Find the client home address
                Dim clientAdr As System.Nullable(Of Int32) = bc.clients.Where(Function(c) c.Id = Context.ClientDs.ClientId).Select(Function(c) c.AddressID).FirstOrDefault()

                ' Find the list of properties for the client
                Dim colProperties As System.Collections.Generic.List(Of Housing_property) = bc.Housing_properties.Where(Function(s) s.HousingID = Context.ClientDs.ClientId).ToList()

                ' Set the property addresses into the combobox
                ComboBoxEdit1.Properties.Items.Clear()
                For Each prop As Housing_property In colProperties
                    Dim adr As System.Nullable(Of Int32) = prop.PropertyAddress
                    If adr Is Nothing OrElse prop.UseHomeAddress Then
                        adr = clientAdr
                    End If

                    Dim addressRecord As address = Nothing
                    If adr.HasValue Then
                        addressRecord = bc.addresses.Where(Function(s) s.Id = adr.Value).FirstOrDefault()
                    End If

                    Dim strAddress As String = String.Empty
                    If addressRecord IsNot Nothing Then
                        strAddress = addressRecord.ToString().Replace(Environment.NewLine, " ")
                    End If

                    ' Allocate a new property address record
                    Dim ctl As New DebtPlus.Data.Controls.ComboboxItem(strAddress, prop.Id)
                    ComboBoxEdit1.Properties.Items.Add(ctl)
                Next
            End Using

            ' Select the first address
            If ComboBoxEdit1.Properties.Items.Count > 0 Then
                ComboBoxEdit1.SelectedIndex = 0
            End If

            ' Correct the centering of the buttons
            EmptySpaceItem_Left.Width = (EmptySpaceItem_Left.Width + EmptySpaceItem_Right.Width) \ 2
            SimpleButton_OK.Enabled = Not HasErrors()
        Finally
            RegisterHandlers()
        End Try
    End Sub

    Private Sub FormChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        SimpleButton_OK.Enabled = Not HasErrors()
    End Sub

    Private Function HasErrors() As Boolean
        Return ComboBoxEdit1.SelectedItem Is Nothing
    End Function

    Private Function IsAdmin(ByVal bc As BusinessContext) As Boolean

        ' Find the ID of the desired attribute code. We know the name, not the ID.
        Dim adminType As AttributeType = DebtPlus.LINQ.Cache.AttributeType.getList().Find(Function(s) s.Grouping Is Nothing AndAlso String.Compare(s.Attribute, "HPF Admin", True) = 0)
        If adminType Is Nothing Then
            Return False
        End If

        ' Find the counselor id associated with the current person.
        Dim co As counselor = DebtPlus.LINQ.Cache.counselor.getList().Find(Function(s) String.Compare(s.Person, DebtPlus.LINQ.BusinessContext.suser_sname(), True) = 0)
        If co Is Nothing Then
            Return False
        End If

        ' Look for the corresponding counselor attribute in the attributes list for the counselor
        Dim id As counselor_attribute = bc.counselor_attributes.Where(Function(s) s.Attribute = adminType.Id AndAlso s.Counselor = co.Id).FirstOrDefault()
        If id IsNot Nothing Then
            Return True
        End If

        Return False
    End Function

    Private Sub SimpleButton_OK_Click(sender As Object, e As System.EventArgs)
        Dim selectedItem As DebtPlus.Data.Controls.ComboboxItem = TryCast(ComboBoxEdit1.SelectedItem, DebtPlus.Data.Controls.ComboboxItem)
        If selectedItem Is Nothing Then
            Return
        End If

        Dim propertyID As Int32 = Convert.ToInt32(selectedItem.value)
        If propertyID < 1 Then
            Return
        End If

        ' Retrieve the property record from the system
        Using bc As New BusinessContext()
            Dim propertyRecord As Housing_property = bc.Housing_properties.Where(Function(s) s.Id = propertyID).FirstOrDefault()
            If propertyRecord Is Nothing Then
                Return
            End If

            Using cm As New DebtPlus.UI.Common.CursorManager()
                PerformUpload(propertyRecord, CheckEdit_Override.Checked)
            End Using
        End Using
    End Sub
End Class