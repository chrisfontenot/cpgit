﻿Imports DebtPlus.UI.Client.controls

Namespace forms.Housing

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class HousingPropertyForm
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.PropertyControl1 = New DebtPlus.UI.Client.controls.PropertyControl()
            Me.SimpleButton_OK = New DevExpress.XtraEditors.SimpleButton()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PropertyControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'PropertyControl1
            '
            Me.PropertyControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.PropertyControl1.Location = New System.Drawing.Point(13, 13)
            Me.PropertyControl1.Name = "PropertyControl1"
            Me.PropertyControl1.Size = New System.Drawing.Size(499, 445)
            Me.PropertyControl1.TabIndex = 0
            '
            'SimpleButton_OK
            '
            Me.SimpleButton_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.SimpleButton_OK.Location = New System.Drawing.Point(228, 471)
            Me.SimpleButton_OK.Name = "SimpleButton_OK"
            Me.SimpleButton_OK.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.TabIndex = 1
            Me.SimpleButton_OK.Text = "&OK"
            Me.SimpleButton_OK.ToolTip = "Click here to commit the changes to the database"
            '
            'HousingPropertyForm
            '
            Me.AcceptButton = Me.SimpleButton_OK
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(530, 506)
            Me.Controls.Add(Me.SimpleButton_OK)
            Me.Controls.Add(Me.PropertyControl1)
            Me.Name = "HousingPropertyForm"
            Me.Text = "Housing Property Information"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PropertyControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents PropertyControl1 As PropertyControl
        Friend WithEvents SimpleButton_OK As DevExpress.XtraEditors.SimpleButton
    End Class
End Namespace