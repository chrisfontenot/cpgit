#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DebtPlus.UI.Client.Service
Imports DebtPlus.LINQ
Imports System.Linq

Namespace forms.Housing
    Friend Class HousingPropertyForm
        Implements IContext

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        ''' <summary>
        ''' Event raised when the address field is changed
        ''' </summary>
        Public Event AddressChanged As DebtPlus.Events.AddressChangedEventHandler

        ' This is a pointer to the current property row being edited
        Private ReadOnly propertyRecord As Housing_property
        Private ReadOnly privateContext As ClientUpdateClass
        Private bc As BusinessContext = Nothing

        Public ReadOnly Property Context() As ClientUpdateClass Implements IContext.Context
            Get
                Return privateContext
            End Get
        End Property

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(bc As BusinessContext)
            MyClass.New()
            Me.bc = bc
        End Sub

        Public Sub New(ByVal context As ClientUpdateClass, ByVal propertyRecord As Housing_property)
            MyClass.New(context.ClientDs.bc)
            privateContext = context
            Me.propertyRecord = propertyRecord
            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf Form_Load
            AddHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Clicked
            AddHandler PropertyControl1.AddressChanged, AddressOf PropertyControl1_AddressChanged
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler Load, AddressOf Form_Load
            RemoveHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Clicked
            RemoveHandler PropertyControl1.AddressChanged, AddressOf PropertyControl1_AddressChanged
        End Sub

        Private Sub Form_Load(ByVal sender As Object, ByVal e As EventArgs)
            UnRegisterHandlers()
            Try
                PropertyControl1.ReadForm(bc, propertyRecord)
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Sub SimpleButton_OK_Clicked(sender As Object, e As EventArgs)
            PropertyControl1.SaveForm(bc)
        End Sub

        Private Sub PropertyControl1_AddressChanged(sender As Object, e As DebtPlus.Events.AddressChangedEventArgs)
            RaiseEvent AddressChanged(Me, e)
        End Sub
    End Class
End Namespace