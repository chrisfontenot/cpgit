Namespace forms.Housing
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class HUD_Interview_Form
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then components.Dispose()
                End If
                povRecord = Nothing
                bc = Nothing
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer = Nothing

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.LookUpEdit_HUD_Grant = New DevExpress.XtraEditors.LookUpEdit()
            Me.CalcEdit_GrantAmountUsed = New DevExpress.XtraEditors.CalcEdit()
            Me.CalcEdit_HousingFeeAmount = New DevExpress.XtraEditors.CalcEdit()
            Me.LookUpEdit_result = New DevExpress.XtraEditors.LookUpEdit()
            Me.LabelControl_interview_date_created = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl_interview_created_by = New DevExpress.XtraEditors.LabelControl()
            Me.LookUpEdit_termination = New DevExpress.XtraEditors.LookUpEdit()
            Me.LookUpEdit_interview = New DevExpress.XtraEditors.LookUpEdit()
            Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
            Me.LabelControl_event_minutes = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl_result_created_by = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl_result_date_created = New DevExpress.XtraEditors.LabelControl()
            Me.SpinEdit_event = New DevExpress.XtraEditors.SpinEdit()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.item0 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.LayoutControlGroup3 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlGroup4 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlGroup5 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.item1 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem_Left = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem5 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem6 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem4 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem_Right = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.LayoutControlGroup6 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.CheckList_ImpactsAndScope = New DebtPlus.UI.Client.controls.ImpactsAndScopeControl()
            Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.LookUpEdit_HUD_Grant.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_GrantAmountUsed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_HousingFeeAmount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_result.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_termination.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_interview.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.SpinEdit_event.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.item0, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.item1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem_Left, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem_Right, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckList_ImpactsAndScope, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'DefaultLookAndFeel1
            '
            Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin"
            '
            'SimpleButton2
            '
            Me.SimpleButton2.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.SimpleButton2.Location = New System.Drawing.Point(294, 436)
            Me.SimpleButton2.MaximumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton2.MinimumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton2.Name = "SimpleButton2"
            Me.SimpleButton2.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton2.StyleController = Me.LayoutControl1
            Me.SimpleButton2.TabIndex = 5
            Me.SimpleButton2.Text = "&Cancel"
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.CheckList_ImpactsAndScope)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_HUD_Grant)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit_GrantAmountUsed)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit_HousingFeeAmount)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_result)
            Me.LayoutControl1.Controls.Add(Me.LabelControl_interview_date_created)
            Me.LayoutControl1.Controls.Add(Me.LabelControl_interview_created_by)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_termination)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_interview)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton1)
            Me.LayoutControl1.Controls.Add(Me.LabelControl_event_minutes)
            Me.LayoutControl1.Controls.Add(Me.LabelControl_result_created_by)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton2)
            Me.LayoutControl1.Controls.Add(Me.LabelControl_result_date_created)
            Me.LayoutControl1.Controls.Add(Me.SpinEdit_event)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(556, 282, 250, 350)
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(584, 471)
            Me.LayoutControl1.TabIndex = 6
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'LookUpEdit_HUD_Grant
            '
            Me.LookUpEdit_HUD_Grant.Location = New System.Drawing.Point(92, 68)
            Me.LookUpEdit_HUD_Grant.Name = "LookUpEdit_HUD_Grant"
            Me.LookUpEdit_HUD_Grant.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_HUD_Grant.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_HUD_Grant.Properties.DisplayMember = "description"
            Me.LookUpEdit_HUD_Grant.Properties.NullText = ""
            Me.LookUpEdit_HUD_Grant.Properties.ShowFooter = False
            Me.LookUpEdit_HUD_Grant.Properties.ShowHeader = False
            Me.LookUpEdit_HUD_Grant.Properties.SortColumnIndex = 1
            Me.LookUpEdit_HUD_Grant.Properties.ValueMember = "Id"
            Me.LookUpEdit_HUD_Grant.Size = New System.Drawing.Size(240, 20)
            Me.LookUpEdit_HUD_Grant.StyleController = Me.LayoutControl1
            Me.LookUpEdit_HUD_Grant.TabIndex = 11
            '
            'CalcEdit_GrantAmountUsed
            '
            Me.CalcEdit_GrantAmountUsed.Location = New System.Drawing.Point(92, 211)
            Me.CalcEdit_GrantAmountUsed.Name = "CalcEdit_GrantAmountUsed"
            Me.CalcEdit_GrantAmountUsed.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.CalcEdit_GrantAmountUsed.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_GrantAmountUsed.Properties.DisplayFormat.FormatString = "{0:c}"
            Me.CalcEdit_GrantAmountUsed.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_GrantAmountUsed.Properties.EditFormat.FormatString = "{0:c}"
            Me.CalcEdit_GrantAmountUsed.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_GrantAmountUsed.Properties.Mask.BeepOnError = True
            Me.CalcEdit_GrantAmountUsed.Properties.Mask.EditMask = "c"
            Me.CalcEdit_GrantAmountUsed.Properties.Precision = 2
            Me.CalcEdit_GrantAmountUsed.Size = New System.Drawing.Size(87, 20)
            Me.CalcEdit_GrantAmountUsed.StyleController = Me.LayoutControl1
            Me.CalcEdit_GrantAmountUsed.TabIndex = 10
            Me.CalcEdit_GrantAmountUsed.ToolTip = "How much of the Grant was used for this purpose?"
            '
            'CalcEdit_HousingFeeAmount
            '
            Me.CalcEdit_HousingFeeAmount.Location = New System.Drawing.Point(92, 92)
            Me.CalcEdit_HousingFeeAmount.Name = "CalcEdit_HousingFeeAmount"
            Me.CalcEdit_HousingFeeAmount.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.CalcEdit_HousingFeeAmount.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_HousingFeeAmount.Properties.DisplayFormat.FormatString = "{0:c}"
            Me.CalcEdit_HousingFeeAmount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_HousingFeeAmount.Properties.EditFormat.FormatString = "{0:c}"
            Me.CalcEdit_HousingFeeAmount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_HousingFeeAmount.Properties.Mask.BeepOnError = True
            Me.CalcEdit_HousingFeeAmount.Properties.Mask.EditMask = "c"
            Me.CalcEdit_HousingFeeAmount.Properties.Precision = 2
            Me.CalcEdit_HousingFeeAmount.Size = New System.Drawing.Size(87, 20)
            Me.CalcEdit_HousingFeeAmount.StyleController = Me.LayoutControl1
            Me.CalcEdit_HousingFeeAmount.TabIndex = 9
            Me.CalcEdit_HousingFeeAmount.ToolTip = "If you charged a fee for the interview, what did you charge?"
            '
            'LookUpEdit_result
            '
            Me.LookUpEdit_result.Location = New System.Drawing.Point(92, 289)
            Me.LookUpEdit_result.Name = "LookUpEdit_result"
            Me.LookUpEdit_result.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_result.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_result.Properties.DisplayMember = "description"
            Me.LookUpEdit_result.Properties.NullText = ""
            Me.LookUpEdit_result.Properties.ShowFooter = False
            Me.LookUpEdit_result.Properties.ShowHeader = False
            Me.LookUpEdit_result.Properties.SortColumnIndex = 1
            Me.LookUpEdit_result.Properties.ValueMember = "Id"
            Me.LookUpEdit_result.Size = New System.Drawing.Size(240, 20)
            Me.LookUpEdit_result.StyleController = Me.LayoutControl1
            Me.LookUpEdit_result.TabIndex = 8
            '
            'LabelControl_interview_date_created
            '
            Me.LabelControl_interview_date_created.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_interview_date_created.Location = New System.Drawing.Point(92, 116)
            Me.LabelControl_interview_date_created.Name = "LabelControl_interview_date_created"
            Me.LabelControl_interview_date_created.Size = New System.Drawing.Size(112, 13)
            Me.LabelControl_interview_date_created.StyleController = Me.LayoutControl1
            Me.LabelControl_interview_date_created.TabIndex = 7
            Me.LabelControl_interview_date_created.UseMnemonic = False
            '
            'LabelControl_interview_created_by
            '
            Me.LabelControl_interview_created_by.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_interview_created_by.Location = New System.Drawing.Point(276, 116)
            Me.LabelControl_interview_created_by.Name = "LabelControl_interview_created_by"
            Me.LabelControl_interview_created_by.Size = New System.Drawing.Size(56, 13)
            Me.LabelControl_interview_created_by.StyleController = Me.LayoutControl1
            Me.LabelControl_interview_created_by.TabIndex = 6
            Me.LabelControl_interview_created_by.UseMnemonic = False
            '
            'LookUpEdit_termination
            '
            Me.LookUpEdit_termination.Location = New System.Drawing.Point(92, 390)
            Me.LookUpEdit_termination.Name = "LookUpEdit_termination"
            Me.LookUpEdit_termination.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_termination.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_termination.Properties.DisplayMember = "description"
            Me.LookUpEdit_termination.Properties.NullText = ""
            Me.LookUpEdit_termination.Properties.ShowFooter = False
            Me.LookUpEdit_termination.Properties.ShowHeader = False
            Me.LookUpEdit_termination.Properties.ShowLines = False
            Me.LookUpEdit_termination.Properties.SortColumnIndex = 1
            Me.LookUpEdit_termination.Properties.ValueMember = "Id"
            Me.LookUpEdit_termination.Size = New System.Drawing.Size(240, 20)
            Me.LookUpEdit_termination.StyleController = Me.LayoutControl1
            Me.LookUpEdit_termination.TabIndex = 1
            '
            'LookUpEdit_interview
            '
            Me.LookUpEdit_interview.Location = New System.Drawing.Point(92, 44)
            Me.LookUpEdit_interview.Name = "LookUpEdit_interview"
            Me.LookUpEdit_interview.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_interview.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_interview.Properties.DisplayMember = "description"
            Me.LookUpEdit_interview.Properties.NullText = ""
            Me.LookUpEdit_interview.Properties.ShowFooter = False
            Me.LookUpEdit_interview.Properties.ShowHeader = False
            Me.LookUpEdit_interview.Properties.SortColumnIndex = 1
            Me.LookUpEdit_interview.Properties.ValueMember = "Id"
            Me.LookUpEdit_interview.Size = New System.Drawing.Size(240, 20)
            Me.LookUpEdit_interview.StyleController = Me.LayoutControl1
            Me.LookUpEdit_interview.TabIndex = 1
            '
            'SimpleButton1
            '
            Me.SimpleButton1.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton1.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.SimpleButton1.Location = New System.Drawing.Point(215, 436)
            Me.SimpleButton1.MaximumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton1.MinimumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton1.Name = "SimpleButton1"
            Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton1.StyleController = Me.LayoutControl1
            Me.SimpleButton1.TabIndex = 4
            Me.SimpleButton1.Text = "&OK"
            '
            'LabelControl_event_minutes
            '
            Me.LabelControl_event_minutes.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl_event_minutes.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl_event_minutes.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_event_minutes.Location = New System.Drawing.Point(183, 187)
            Me.LabelControl_event_minutes.Name = "LabelControl_event_minutes"
            Me.LabelControl_event_minutes.Padding = New System.Windows.Forms.Padding(0, 3, 0, 0)
            Me.LabelControl_event_minutes.Size = New System.Drawing.Size(149, 16)
            Me.LabelControl_event_minutes.StyleController = Me.LayoutControl1
            Me.LabelControl_event_minutes.TabIndex = 2
            Me.LabelControl_event_minutes.Text = "(in minutes)"
            Me.LabelControl_event_minutes.UseMnemonic = False
            '
            'LabelControl_result_created_by
            '
            Me.LabelControl_result_created_by.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_result_created_by.Location = New System.Drawing.Point(276, 313)
            Me.LabelControl_result_created_by.Name = "LabelControl_result_created_by"
            Me.LabelControl_result_created_by.Size = New System.Drawing.Size(56, 13)
            Me.LabelControl_result_created_by.StyleController = Me.LayoutControl1
            Me.LabelControl_result_created_by.TabIndex = 3
            Me.LabelControl_result_created_by.UseMnemonic = False
            '
            'LabelControl_result_date_created
            '
            Me.LabelControl_result_date_created.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_result_date_created.Location = New System.Drawing.Point(92, 313)
            Me.LabelControl_result_date_created.Name = "LabelControl_result_date_created"
            Me.LabelControl_result_date_created.Size = New System.Drawing.Size(112, 13)
            Me.LabelControl_result_date_created.StyleController = Me.LayoutControl1
            Me.LabelControl_result_date_created.TabIndex = 5
            Me.LabelControl_result_date_created.UseMnemonic = False
            '
            'SpinEdit_event
            '
            Me.SpinEdit_event.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
            Me.SpinEdit_event.Location = New System.Drawing.Point(92, 187)
            Me.SpinEdit_event.Name = "SpinEdit_event"
            Me.SpinEdit_event.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.SpinEdit_event.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.[Default]
            Me.SpinEdit_event.Properties.Increment = New Decimal(New Integer() {15, 0, 0, 0})
            Me.SpinEdit_event.Properties.IsFloatValue = False
            Me.SpinEdit_event.Properties.Mask.BeepOnError = True
            Me.SpinEdit_event.Properties.Mask.EditMask = "f0"
            Me.SpinEdit_event.Size = New System.Drawing.Size(87, 20)
            Me.SpinEdit_event.StyleController = Me.LayoutControl1
            Me.SpinEdit_event.TabIndex = 1
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup2, Me.LayoutControlGroup3, Me.LayoutControlGroup4, Me.LayoutControlGroup5, Me.LayoutControlItem2, Me.LayoutControlItem1, Me.EmptySpaceItem_Left, Me.EmptySpaceItem5, Me.EmptySpaceItem6, Me.EmptySpaceItem4, Me.EmptySpaceItem_Right, Me.LayoutControlGroup6, Me.EmptySpaceItem3})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "Root"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(584, 471)
            Me.LayoutControlGroup1.Text = "Root"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlGroup2
            '
            Me.LayoutControlGroup2.AppearanceGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LayoutControlGroup2.AppearanceGroup.Options.UseFont = True
            Me.LayoutControlGroup2.CustomizationFormText = "Interview"
            Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem8, Me.LayoutControlItem11, Me.LayoutControlItem10, Me.LayoutControlItem14, Me.LayoutControlItem9, Me.item0})
            Me.LayoutControlGroup2.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
            Me.LayoutControlGroup2.Size = New System.Drawing.Size(336, 133)
            Me.LayoutControlGroup2.Text = "Interview"
            '
            'LayoutControlItem8
            '
            Me.LayoutControlItem8.Control = Me.LookUpEdit_interview
            Me.LayoutControlItem8.CustomizationFormText = "Type"
            Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem8.Name = "LayoutControlItem8"
            Me.LayoutControlItem8.Size = New System.Drawing.Size(312, 24)
            Me.LayoutControlItem8.Text = "Type"
            Me.LayoutControlItem8.TextSize = New System.Drawing.Size(65, 13)
            '
            'LayoutControlItem11
            '
            Me.LayoutControlItem11.Control = Me.LabelControl_interview_date_created
            Me.LayoutControlItem11.CustomizationFormText = "Date Created"
            Me.LayoutControlItem11.Location = New System.Drawing.Point(0, 72)
            Me.LayoutControlItem11.Name = "LayoutControlItem11"
            Me.LayoutControlItem11.Size = New System.Drawing.Size(184, 17)
            Me.LayoutControlItem11.Text = "Date Created"
            Me.LayoutControlItem11.TextSize = New System.Drawing.Size(65, 13)
            '
            'LayoutControlItem10
            '
            Me.LayoutControlItem10.Control = Me.LabelControl_interview_created_by
            Me.LayoutControlItem10.CustomizationFormText = "Created By"
            Me.LayoutControlItem10.Location = New System.Drawing.Point(184, 72)
            Me.LayoutControlItem10.Name = "LayoutControlItem10"
            Me.LayoutControlItem10.Size = New System.Drawing.Size(128, 17)
            Me.LayoutControlItem10.Text = "Created By"
            Me.LayoutControlItem10.TextSize = New System.Drawing.Size(65, 13)
            '
            'LayoutControlItem14
            '
            Me.LayoutControlItem14.Control = Me.LookUpEdit_HUD_Grant
            Me.LayoutControlItem14.CustomizationFormText = "Grant"
            Me.LayoutControlItem14.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem14.Name = "LayoutControlItem14"
            Me.LayoutControlItem14.Size = New System.Drawing.Size(312, 24)
            Me.LayoutControlItem14.Text = "Grant"
            Me.LayoutControlItem14.TextSize = New System.Drawing.Size(65, 13)
            '
            'LayoutControlItem9
            '
            Me.LayoutControlItem9.Control = Me.CalcEdit_HousingFeeAmount
            Me.LayoutControlItem9.CustomizationFormText = "Fee Amount"
            Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 48)
            Me.LayoutControlItem9.Name = "LayoutControlItem9"
            Me.LayoutControlItem9.Size = New System.Drawing.Size(159, 24)
            Me.LayoutControlItem9.Text = "Fee Amount"
            Me.LayoutControlItem9.TextSize = New System.Drawing.Size(65, 13)
            '
            'item0
            '
            Me.item0.AllowHotTrack = False
            Me.item0.CustomizationFormText = "item0"
            Me.item0.Location = New System.Drawing.Point(159, 48)
            Me.item0.Name = "item0"
            Me.item0.Size = New System.Drawing.Size(153, 24)
            Me.item0.Text = "item0"
            Me.item0.TextSize = New System.Drawing.Size(0, 0)
            '
            'LayoutControlGroup3
            '
            Me.LayoutControlGroup3.AppearanceGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LayoutControlGroup3.AppearanceGroup.Options.UseFont = True
            Me.LayoutControlGroup3.CustomizationFormText = "Result"
            Me.LayoutControlGroup3.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem4, Me.LayoutControlItem12, Me.LayoutControlItem5})
            Me.LayoutControlGroup3.Location = New System.Drawing.Point(0, 245)
            Me.LayoutControlGroup3.Name = "LayoutControlGroup3"
            Me.LayoutControlGroup3.Size = New System.Drawing.Size(336, 85)
            Me.LayoutControlGroup3.Text = "Result"
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.LabelControl_result_date_created
            Me.LayoutControlItem4.CustomizationFormText = "Date Created"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(184, 17)
            Me.LayoutControlItem4.Text = "Date Created"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(65, 13)
            '
            'LayoutControlItem12
            '
            Me.LayoutControlItem12.Control = Me.LookUpEdit_result
            Me.LayoutControlItem12.CustomizationFormText = "Type"
            Me.LayoutControlItem12.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem12.Name = "LayoutControlItem12"
            Me.LayoutControlItem12.Size = New System.Drawing.Size(312, 24)
            Me.LayoutControlItem12.Text = "Type"
            Me.LayoutControlItem12.TextSize = New System.Drawing.Size(65, 13)
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Control = Me.LabelControl_result_created_by
            Me.LayoutControlItem5.CustomizationFormText = "Created By"
            Me.LayoutControlItem5.Location = New System.Drawing.Point(184, 24)
            Me.LayoutControlItem5.Name = "LayoutControlItem5"
            Me.LayoutControlItem5.Size = New System.Drawing.Size(128, 17)
            Me.LayoutControlItem5.Text = "Created By"
            Me.LayoutControlItem5.TextSize = New System.Drawing.Size(65, 13)
            '
            'LayoutControlGroup4
            '
            Me.LayoutControlGroup4.AppearanceGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LayoutControlGroup4.AppearanceGroup.Options.UseFont = True
            Me.LayoutControlGroup4.CustomizationFormText = "Termination"
            Me.LayoutControlGroup4.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem3})
            Me.LayoutControlGroup4.Location = New System.Drawing.Point(0, 346)
            Me.LayoutControlGroup4.Name = "LayoutControlGroup4"
            Me.LayoutControlGroup4.Size = New System.Drawing.Size(336, 68)
            Me.LayoutControlGroup4.Text = "Termination"
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.LookUpEdit_termination
            Me.LayoutControlItem3.CustomizationFormText = "Reason"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(312, 24)
            Me.LayoutControlItem3.Text = "Reason"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(65, 13)
            '
            'LayoutControlGroup5
            '
            Me.LayoutControlGroup5.AppearanceGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LayoutControlGroup5.AppearanceGroup.Options.UseFont = True
            Me.LayoutControlGroup5.CustomizationFormText = "Duration"
            Me.LayoutControlGroup5.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem6, Me.LayoutControlItem7, Me.LayoutControlItem13, Me.item1})
            Me.LayoutControlGroup5.Location = New System.Drawing.Point(0, 143)
            Me.LayoutControlGroup5.Name = "LayoutControlGroup5"
            Me.LayoutControlGroup5.Size = New System.Drawing.Size(336, 92)
            Me.LayoutControlGroup5.Text = "Duration"
            '
            'LayoutControlItem6
            '
            Me.LayoutControlItem6.Control = Me.SpinEdit_event
            Me.LayoutControlItem6.CustomizationFormText = "Duration"
            Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem6.Name = "LayoutControlItem6"
            Me.LayoutControlItem6.Size = New System.Drawing.Size(159, 24)
            Me.LayoutControlItem6.Text = "Duration"
            Me.LayoutControlItem6.TextSize = New System.Drawing.Size(65, 13)
            '
            'LayoutControlItem7
            '
            Me.LayoutControlItem7.Control = Me.LabelControl_event_minutes
            Me.LayoutControlItem7.CustomizationFormText = "LayoutControlItem7"
            Me.LayoutControlItem7.Location = New System.Drawing.Point(159, 0)
            Me.LayoutControlItem7.Name = "LayoutControlItem7"
            Me.LayoutControlItem7.Size = New System.Drawing.Size(153, 24)
            Me.LayoutControlItem7.Text = "LayoutControlItem7"
            Me.LayoutControlItem7.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem7.TextToControlDistance = 0
            Me.LayoutControlItem7.TextVisible = False
            '
            'LayoutControlItem13
            '
            Me.LayoutControlItem13.Control = Me.CalcEdit_GrantAmountUsed
            Me.LayoutControlItem13.CustomizationFormText = "Grant Amount Used"
            Me.LayoutControlItem13.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem13.Name = "LayoutControlItem13"
            Me.LayoutControlItem13.Size = New System.Drawing.Size(159, 24)
            Me.LayoutControlItem13.Text = "Grant Used"
            Me.LayoutControlItem13.TextSize = New System.Drawing.Size(65, 13)
            '
            'item1
            '
            Me.item1.AllowHotTrack = False
            Me.item1.CustomizationFormText = "item1"
            Me.item1.Location = New System.Drawing.Point(159, 24)
            Me.item1.Name = "item1"
            Me.item1.Size = New System.Drawing.Size(153, 24)
            Me.item1.Text = "item1"
            Me.item1.TextSize = New System.Drawing.Size(0, 0)
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.SimpleButton2
            Me.LayoutControlItem2.CustomizationFormText = "LayoutControlItem2"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(282, 424)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(79, 27)
            Me.LayoutControlItem2.Text = "LayoutControlItem2"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem2.TextToControlDistance = 0
            Me.LayoutControlItem2.TextVisible = False
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.SimpleButton1
            Me.LayoutControlItem1.CustomizationFormText = "LayoutControlItem1"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(203, 424)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(79, 27)
            Me.LayoutControlItem1.Text = "LayoutControlItem1"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem1.TextToControlDistance = 0
            Me.LayoutControlItem1.TextVisible = False
            '
            'EmptySpaceItem_Left
            '
            Me.EmptySpaceItem_Left.AllowHotTrack = False
            Me.EmptySpaceItem_Left.CustomizationFormText = "EmptySpaceItem1"
            Me.EmptySpaceItem_Left.Location = New System.Drawing.Point(0, 424)
            Me.EmptySpaceItem_Left.Name = "EmptySpaceItem_Left"
            Me.EmptySpaceItem_Left.Size = New System.Drawing.Size(203, 27)
            Me.EmptySpaceItem_Left.Text = "EmptySpaceItem_Left"
            Me.EmptySpaceItem_Left.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem5
            '
            Me.EmptySpaceItem5.AllowHotTrack = False
            Me.EmptySpaceItem5.CustomizationFormText = "EmptySpaceItem5"
            Me.EmptySpaceItem5.Location = New System.Drawing.Point(0, 330)
            Me.EmptySpaceItem5.Name = "EmptySpaceItem5"
            Me.EmptySpaceItem5.Size = New System.Drawing.Size(336, 16)
            Me.EmptySpaceItem5.Text = "EmptySpaceItem5"
            Me.EmptySpaceItem5.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem6
            '
            Me.EmptySpaceItem6.AllowHotTrack = False
            Me.EmptySpaceItem6.CustomizationFormText = "EmptySpaceItem6"
            Me.EmptySpaceItem6.Location = New System.Drawing.Point(0, 235)
            Me.EmptySpaceItem6.Name = "EmptySpaceItem6"
            Me.EmptySpaceItem6.Size = New System.Drawing.Size(336, 10)
            Me.EmptySpaceItem6.Text = "EmptySpaceItem6"
            Me.EmptySpaceItem6.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem4
            '
            Me.EmptySpaceItem4.AllowHotTrack = False
            Me.EmptySpaceItem4.CustomizationFormText = "EmptySpaceItem4"
            Me.EmptySpaceItem4.Location = New System.Drawing.Point(0, 133)
            Me.EmptySpaceItem4.Name = "EmptySpaceItem4"
            Me.EmptySpaceItem4.Size = New System.Drawing.Size(336, 10)
            Me.EmptySpaceItem4.Text = "EmptySpaceItem4"
            Me.EmptySpaceItem4.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem_Right
            '
            Me.EmptySpaceItem_Right.AllowHotTrack = False
            Me.EmptySpaceItem_Right.CustomizationFormText = "EmptySpaceItem2"
            Me.EmptySpaceItem_Right.Location = New System.Drawing.Point(361, 424)
            Me.EmptySpaceItem_Right.Name = "EmptySpaceItem_Right"
            Me.EmptySpaceItem_Right.Size = New System.Drawing.Size(203, 27)
            Me.EmptySpaceItem_Right.Text = "EmptySpaceItem_Right"
            Me.EmptySpaceItem_Right.TextSize = New System.Drawing.Size(0, 0)
            '
            'LayoutControlGroup6
            '
            Me.LayoutControlGroup6.AppearanceGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LayoutControlGroup6.AppearanceGroup.Options.UseFont = True
            Me.LayoutControlGroup6.CustomizationFormText = "Impact and Scope"
            Me.LayoutControlGroup6.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem15})
            Me.LayoutControlGroup6.Location = New System.Drawing.Point(336, 0)
            Me.LayoutControlGroup6.Name = "LayoutControlGroup6"
            Me.LayoutControlGroup6.OptionsItemText.TextAlignMode = DevExpress.XtraLayout.TextAlignModeGroup.CustomSize
            Me.LayoutControlGroup6.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 9, 0)
            Me.LayoutControlGroup6.Size = New System.Drawing.Size(228, 414)
            Me.LayoutControlGroup6.Text = "Impact and Scope"
            '
            'EmptySpaceItem3
            '
            Me.EmptySpaceItem3.AllowHotTrack = False
            Me.EmptySpaceItem3.CustomizationFormText = "EmptySpaceItem3"
            Me.EmptySpaceItem3.Location = New System.Drawing.Point(0, 414)
            Me.EmptySpaceItem3.Name = "EmptySpaceItem3"
            Me.EmptySpaceItem3.Size = New System.Drawing.Size(564, 10)
            Me.EmptySpaceItem3.Text = "EmptySpaceItem3"
            Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
            '
            'CheckList_ImpactsAndScope
            '
            Me.CheckList_ImpactsAndScope.Location = New System.Drawing.Point(351, 44)
            Me.CheckList_ImpactsAndScope.Name = "CheckList_ImpactsAndScope"
            Me.CheckList_ImpactsAndScope.Size = New System.Drawing.Size(218, 375)
            Me.CheckList_ImpactsAndScope.TabIndex = 12
            Me.CheckList_ImpactsAndScope.TabStop = False
            '
            'LayoutControlItem15
            '
            Me.LayoutControlItem15.Control = Me.CheckList_ImpactsAndScope
            Me.LayoutControlItem15.CustomizationFormText = "LayoutControlItem15"
            Me.LayoutControlItem15.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem15.Name = "LayoutControlItem15"
            Me.LayoutControlItem15.Size = New System.Drawing.Size(222, 379)
            Me.LayoutControlItem15.Text = "LayoutControlItem15"
            Me.LayoutControlItem15.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem15.TextToControlDistance = 0
            Me.LayoutControlItem15.TextVisible = False
            '
            'HUD_Interview_Form
            '
            Me.AcceptButton = Me.SimpleButton1
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.SimpleButton2
            Me.ClientSize = New System.Drawing.Size(584, 471)
            Me.Controls.Add(Me.LayoutControl1)
            Me.Name = "HUD_Interview_Form"
            Me.Text = "HUD Interviews, Results, and Time"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.LookUpEdit_HUD_Grant.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_GrantAmountUsed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_HousingFeeAmount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_result.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_termination.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_interview.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.SpinEdit_event.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.item0, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.item1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem_Left, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem_Right, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckList_ImpactsAndScope, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents LabelControl_event_minutes As DevExpress.XtraEditors.LabelControl
        Friend WithEvents SpinEdit_event As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents LookUpEdit_termination As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LabelControl_result_date_created As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl_result_created_by As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LookUpEdit_interview As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LabelControl_interview_date_created As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl_interview_created_by As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LookUpEdit_result As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents CalcEdit_HousingFeeAmount As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents CalcEdit_GrantAmountUsed As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents item0 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents LayoutControlGroup3 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlGroup4 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlGroup5 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents item1 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem_Left As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem_Right As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem3 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem5 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem6 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem4 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents LookUpEdit_HUD_Grant As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents CheckList_ImpactsAndScope As controls.ImpactsAndScopeControl
        Friend WithEvents LayoutControlGroup6 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
    End Class
End NameSpace