#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DebtPlus.UI.Client.Service
Imports DebtPlus.LINQ
Imports System.Linq

Namespace forms.Housing

    Public Class LoanInformationForm
        Implements IContext

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Private ReadOnly privateContext As ClientUpdateClass
        Private ReadOnly loanRecord As Housing_loan
        Private bc As BusinessContext = Nothing

        ''' <summary>
        ''' Initialize the form
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        ''' <summary>
        ''' Initialize the form
        ''' </summary>
        Public Sub New(bc As BusinessContext)
            MyClass.New()
            Me.bc = bc
        End Sub

        ''' <summary>
        ''' Initialize the form
        ''' </summary>
        Public Sub New(ByVal Context As ClientUpdateClass, ByVal loanRecord As Housing_loan)
            MyClass.New(Context.ClientDs.bc)
            privateContext = Context
            Me.loanRecord = loanRecord
            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf LoanInformationForm_Load
            AddHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
            AddHandler FormClosing, AddressOf LoanInformationForm_FormClosing
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler Load, AddressOf LoanInformationForm_Load
            RemoveHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
            RemoveHandler FormClosing, AddressOf LoanInformationForm_FormClosing
        End Sub

        Public ReadOnly Property Context() As ClientUpdateClass Implements IContext.Context
            Get
                Return privateContext
            End Get
        End Property

        Private Sub LoanInformationForm_Load(ByVal sender As Object, ByVal e As EventArgs)
            UnRegisterHandlers()
            Try
                LoadPlacement("Housing_LoanInformationForm")
                LoanControl1.ReadForm(bc, loanRecord)
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Sub SimpleButton_OK_Click(ByVal sender As Object, ByVal e As EventArgs)
            If LoanControl1.SaveForm(bc) Then
                DialogResult = Windows.Forms.DialogResult.OK
            End If
        End Sub

        Private Sub LoanInformationForm_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs)
            If e.CloseReason = CloseReason.FormOwnerClosing Then
                SavePlacement("Housing_LoanInformationForm")
            End If
        End Sub
    End Class
End Namespace
