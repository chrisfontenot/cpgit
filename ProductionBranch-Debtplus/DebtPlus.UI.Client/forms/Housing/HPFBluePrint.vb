﻿Namespace forms.Housing
    Public Class HPFBluePrint

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Private record As DebtPlus.LINQ.email_queue = Nothing
        Public Sub New(Record As DebtPlus.LINQ.email_queue)
            MyClass.New()
            Me.record = Record
            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Me.Load, AddressOf HPFBluePrint_Load
            AddHandler Me.Resize, AddressOf HPFBluePrint_Resize
            AddHandler SimpleButton_Yes.Click, AddressOf SimpleButton_Yes_Click
            AddHandler EmailControl1.EditValueChanged, AddressOf EmailControl1_EditValueChanged
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler Me.Load, AddressOf HPFBluePrint_Load
            RemoveHandler Me.Resize, AddressOf HPFBluePrint_Resize
            RemoveHandler SimpleButton_Yes.Click, AddressOf SimpleButton_Yes_Click
            RemoveHandler EmailControl1.EditValueChanged, AddressOf EmailControl1_EditValueChanged
        End Sub

        Private Sub HPFBluePrint_Load(sender As Object, e As System.EventArgs)
            UnRegisterHandlers()
            Try
                ' Center the buttons accordingly
                EmptySpaceItem_Left.Width = (EmptySpaceItem_Left.Width + EmptySpaceItem_Right.Width) \ 2

                ' Load the controls for the email address
                Dim eAddress As New DebtPlus.LINQ.EmailAddress() With _
                    {
                        .Address = record.email_address,
                        .ValidationCode = EmailTest.FindValidationCode(record.email_address)
                    }
                EmailControl1.SetCurrentValues(eAddress)

                SimpleButton_Yes.Enabled = Not HasErrors()

            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Sub EmailControl1_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            SimpleButton_Yes.Enabled = Not HasErrors()
        End Sub

        ''' <summary>
        ''' Look for an error condition on the input form
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function HasErrors() As Boolean

            ' Ensure that the entry is not a defined "NONE" or "MISSING" or some other strange item.
            Dim e As DebtPlus.LINQ.EmailAddress = EmailControl1.GetCurrentValues()
            If e.ValidationCode <> LINQ.EmailAddress.EmailValidationEnum.Valid Then
                Return True
            End If

            ' Look for the valid email address even if there is one.
            If EmailTest.FindValidationCode(e.Address) = LINQ.EmailAddress.EmailValidationEnum.Valid Then
                Return False
            End If

            ' The entered address is invalid.
            Return True
        End Function

        ''' <summary>
        ''' Handle the resize event for the form to center the buttons as needed
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub HPFBluePrint_Resize(sender As Object, e As System.EventArgs) Handles Me.Resize
            EmptySpaceItem_Left.Width = (EmptySpaceItem_Left.Width + EmptySpaceItem_Right.Width) \ 2
        End Sub

        ''' <summary>
        ''' Process the CLICK event on the YES button to update the record structure correctly.
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub SimpleButton_Yes_Click(sender As Object, e As System.EventArgs) Handles SimpleButton_Yes.Click
            Dim eAddress As DebtPlus.LINQ.EmailAddress = EmailControl1.GetCurrentValues()

            ' Do a test to ensure that the entered email address is valid.
            If eAddress.ValidationCode = LINQ.EmailAddress.EmailValidationEnum.Valid Then
                If EmailTest.FindValidationCode(eAddress.Address) = LINQ.EmailAddress.EmailValidationEnum.Valid Then
                    record.email_address = eAddress.Address
                    DialogResult = Windows.Forms.DialogResult.OK
                    Return
                End If
            End If

        End Sub
    End Class

    Friend Module EmailTest
        Friend Function FindValidationCode(address As String) As LINQ.EmailAddress.EmailValidationEnum

            If String.IsNullOrWhiteSpace(address) Then
                Return LINQ.EmailAddress.EmailValidationEnum.Missing
            End If

            Dim Validate As New DebtPlus.EmailValidation.IsEMail()
            If Validate.IsEmailValid(address) Then
                Return LINQ.EmailAddress.EmailValidationEnum.Valid
            End If

            Return LINQ.EmailAddress.EmailValidationEnum.Invalid
        End Function
    End Module
End Namespace