﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class HPFUploadForm
    Inherits DebtPlus.Data.Forms.DebtPlusForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If components IsNot Nothing Then components.Dispose()
            End If
            components = Nothing
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.SimpleButton_Cancel = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton_OK = New DevExpress.XtraEditors.SimpleButton()
        Me.CheckEdit_Override = New DevExpress.XtraEditors.CheckEdit()
        Me.ComboBoxEdit1 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem_Left = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem_Right = New DevExpress.XtraLayout.EmptySpaceItem()
        CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.CheckEdit_Override.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem_Left, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem_Right, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.SimpleButton_Cancel)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton_OK)
        Me.LayoutControl1.Controls.Add(Me.CheckEdit_Override)
        Me.LayoutControl1.Controls.Add(Me.ComboBoxEdit1)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(363, 194)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'SimpleButton_Cancel
        '
        Me.SimpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.SimpleButton_Cancel.Location = New System.Drawing.Point(185, 159)
        Me.SimpleButton_Cancel.MaximumSize = New System.Drawing.Size(75, 23)
        Me.SimpleButton_Cancel.MinimumSize = New System.Drawing.Size(75, 23)
        Me.SimpleButton_Cancel.Name = "SimpleButton_Cancel"
        Me.SimpleButton_Cancel.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton_Cancel.StyleController = Me.LayoutControl1
        Me.SimpleButton_Cancel.TabIndex = 7
        Me.SimpleButton_Cancel.Text = "&Cancel"
        '
        'SimpleButton_OK
        '
        Me.SimpleButton_OK.Location = New System.Drawing.Point(106, 159)
        Me.SimpleButton_OK.MaximumSize = New System.Drawing.Size(75, 23)
        Me.SimpleButton_OK.MinimumSize = New System.Drawing.Size(75, 23)
        Me.SimpleButton_OK.Name = "SimpleButton_OK"
        Me.SimpleButton_OK.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton_OK.StyleController = Me.LayoutControl1
        Me.SimpleButton_OK.TabIndex = 6
        Me.SimpleButton_OK.Text = "&Start"
        '
        'CheckEdit_Override
        '
        Me.CheckEdit_Override.Location = New System.Drawing.Point(12, 36)
        Me.CheckEdit_Override.Name = "CheckEdit_Override"
        Me.CheckEdit_Override.Properties.Caption = "Override Local Validation Checks"
        Me.CheckEdit_Override.Size = New System.Drawing.Size(339, 19)
        Me.CheckEdit_Override.StyleController = Me.LayoutControl1
        Me.CheckEdit_Override.TabIndex = 5
        '
        'ComboBoxEdit1
        '
        Me.ComboBoxEdit1.Location = New System.Drawing.Point(99, 12)
        Me.ComboBoxEdit1.Name = "ComboBoxEdit1"
        Me.ComboBoxEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBoxEdit1.Size = New System.Drawing.Size(252, 20)
        Me.ComboBoxEdit1.StyleController = Me.LayoutControl1
        Me.ComboBoxEdit1.TabIndex = 4
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.EmptySpaceItem1, Me.EmptySpaceItem_Left, Me.EmptySpaceItem_Right})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(363, 194)
        Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.ComboBoxEdit1
        Me.LayoutControlItem1.CustomizationFormText = "Property Address"
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(343, 24)
        Me.LayoutControlItem1.Text = "Property Address"
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(84, 13)
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.CheckEdit_Override
        Me.LayoutControlItem2.CustomizationFormText = "LayoutControlItem2"
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(343, 23)
        Me.LayoutControlItem2.Text = "LayoutControlItem2"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem2.TextToControlDistance = 0
        Me.LayoutControlItem2.TextVisible = False
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.SimpleButton_OK
        Me.LayoutControlItem3.CustomizationFormText = "LayoutControlItem3"
        Me.LayoutControlItem3.Location = New System.Drawing.Point(94, 147)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(79, 27)
        Me.LayoutControlItem3.Text = "LayoutControlItem3"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem3.TextToControlDistance = 0
        Me.LayoutControlItem3.TextVisible = False
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.SimpleButton_Cancel
        Me.LayoutControlItem4.CustomizationFormText = "LayoutControlItem4"
        Me.LayoutControlItem4.Location = New System.Drawing.Point(173, 147)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(79, 27)
        Me.LayoutControlItem4.Text = "LayoutControlItem4"
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem4.TextToControlDistance = 0
        Me.LayoutControlItem4.TextVisible = False
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 47)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(343, 100)
        Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem_Left
        '
        Me.EmptySpaceItem_Left.AllowHotTrack = False
        Me.EmptySpaceItem_Left.CustomizationFormText = "EmptySpaceItem_Left"
        Me.EmptySpaceItem_Left.Location = New System.Drawing.Point(0, 147)
        Me.EmptySpaceItem_Left.Name = "EmptySpaceItem_Left"
        Me.EmptySpaceItem_Left.Size = New System.Drawing.Size(94, 27)
        Me.EmptySpaceItem_Left.Text = "EmptySpaceItem_Left"
        Me.EmptySpaceItem_Left.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem_Right
        '
        Me.EmptySpaceItem_Right.AllowHotTrack = False
        Me.EmptySpaceItem_Right.CustomizationFormText = "EmptySpaceItem_Right"
        Me.EmptySpaceItem_Right.Location = New System.Drawing.Point(252, 147)
        Me.EmptySpaceItem_Right.Name = "EmptySpaceItem_Right"
        Me.EmptySpaceItem_Right.Size = New System.Drawing.Size(91, 27)
        Me.EmptySpaceItem_Right.Text = "EmptySpaceItem_Right"
        Me.EmptySpaceItem_Right.TextSize = New System.Drawing.Size(0, 0)
        '
        'HPFUploadForm
        '
        Me.AcceptButton = Me.SimpleButton_OK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.SimpleButton_Cancel
        Me.ClientSize = New System.Drawing.Size(363, 194)
        Me.Controls.Add(Me.LayoutControl1)
        Me.MaximizeBox = False
        Me.Name = "HPFUploadForm"
        Me.Text = "HPF Upload"
        CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.CheckEdit_Override.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem_Left, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem_Right, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents SimpleButton_Cancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton_OK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents CheckEdit_Override As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ComboBoxEdit1 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem_Left As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem_Right As DevExpress.XtraLayout.EmptySpaceItem
End Class
