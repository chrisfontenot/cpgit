#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.LINQ

Namespace forms
    Friend Class Form_HECMDefault

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        ' Current record being edited
        Dim record As hecm_default

        ''' <summary>
        ''' Initialize the form
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(ByVal record As DebtPlus.LINQ.hecm_default)
            MyClass.New()
            Me.record = record

            lu_Option.Properties.DataSource = DebtPlus.LINQ.Cache.HECM_Lookup.getOptionList()
            lu_Tier.Properties.DataSource = DebtPlus.LINQ.Cache.HECM_Lookup.getTierList()
            lu_bdt_bec.Properties.DataSource = DebtPlus.LINQ.Cache.HECM_Lookup.getYesNoList()
            lu_checkup.Properties.DataSource = DebtPlus.LINQ.Cache.HECM_Lookup.getYesNoList()
            lu_File_Status.Properties.DataSource = DebtPlus.LINQ.Cache.HECM_Lookup.getFileStatus()

            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
            AddHandler Load, AddressOf Form_HECMDefault_Load
            AddHandler lu_bdt_bec.CustomDisplayText, AddressOf lu_bdc_bec_CustomDisplayText
            AddHandler lu_checkup.CustomDisplayText, AddressOf lu_checkup_CustomDisplayText
            AddHandler lu_File_Status.CustomDisplayText, AddressOf lu_File_Status_CustomDisplayText
            AddHandler se_Ref_Food.Properties.EditValueChanged, AddressOf Form_Changed
            AddHandler se_Ref_Car_Ins.Properties.EditValueChanged, AddressOf Form_Changed
            AddHandler se_Ref_Home_Repair.Properties.EditValueChanged, AddressOf Form_Changed
            AddHandler se_Ref_Income.Properties.EditValueChanged, AddressOf Form_Changed
            AddHandler se_Ref_Med.Properties.EditValueChanged, AddressOf Form_Changed
            AddHandler se_Ref_Presciption.Properties.EditValueChanged, AddressOf Form_Changed
            AddHandler se_Ref_Prop_Taxes.Properties.EditValueChanged, AddressOf Form_Changed
            AddHandler se_Ref_Social.Properties.EditValueChanged, AddressOf Form_Changed
            AddHandler se_Ref_SSI.Properties.EditValueChanged, AddressOf Form_Changed
            AddHandler se_Ref_Utility.Properties.EditValueChanged, AddressOf Form_Changed
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
            RemoveHandler Load, AddressOf Form_HECMDefault_Load
            RemoveHandler lu_bdt_bec.CustomDisplayText, AddressOf lu_bdc_bec_CustomDisplayText
            RemoveHandler lu_checkup.CustomDisplayText, AddressOf lu_checkup_CustomDisplayText
            RemoveHandler lu_File_Status.CustomDisplayText, AddressOf lu_File_Status_CustomDisplayText
            RemoveHandler se_Ref_Food.Properties.EditValueChanged, AddressOf Form_Changed
            RemoveHandler se_Ref_Car_Ins.Properties.EditValueChanged, AddressOf Form_Changed
            RemoveHandler se_Ref_Home_Repair.Properties.EditValueChanged, AddressOf Form_Changed
            RemoveHandler se_Ref_Income.Properties.EditValueChanged, AddressOf Form_Changed
            RemoveHandler se_Ref_Med.Properties.EditValueChanged, AddressOf Form_Changed
            RemoveHandler se_Ref_Presciption.Properties.EditValueChanged, AddressOf Form_Changed
            RemoveHandler se_Ref_Prop_Taxes.Properties.EditValueChanged, AddressOf Form_Changed
            RemoveHandler se_Ref_Social.Properties.EditValueChanged, AddressOf Form_Changed
            RemoveHandler se_Ref_SSI.Properties.EditValueChanged, AddressOf Form_Changed
            RemoveHandler se_Ref_Utility.Properties.EditValueChanged, AddressOf Form_Changed
        End Sub

        ''' <summary>
        ''' Process the FORM LOAD event
        ''' </summary>
        Private Sub Form_HECMDefault_Load(ByVal sender As Object, ByVal e As EventArgs)

            UnRegisterHandlers()
            Try
                ' Restore the form position
                LoadPlacement("Client.Update.HECMDefault")

                lu_Option.EditValue = record.referral_option
                lu_Tier.EditValue = record.tier
                lu_bdt_bec.EditValue = record.bdt_bec
                lu_checkup.EditValue = record.checkup
                dt_Hardest_Hit_Ref_Date.EditValue = record.Hardest_Hit_Ref_Date
                lu_File_Status.EditValue = record.file_status
                dt_File_Status_Date.EditValue = record.file_status_date

                calcEdit_Savings_Food.EditValue = record.save_food.GetValueOrDefault()
                calcEdit_Savings_Home_Ins.EditValue = record.save_car_ins.GetValueOrDefault()
                calcEdit_Savings_Home_Repair.EditValue = record.save_home_repair.GetValueOrDefault()
                calcEdit_Savings_Income.EditValue = record.save_income.GetValueOrDefault()
                calcEdit_Savings_Med.EditValue = record.save_medical.GetValueOrDefault()
                calcEdit_Savings_Prescription.EditValue = record.save_prescription.GetValueOrDefault()
                calcEdit_Savings_Prop_Taxes.EditValue = record.save_property_taxes.GetValueOrDefault()
                calcEdit_Savings_Social.EditValue = record.save_social_services.GetValueOrDefault()
                calcEdit_Savings_SSI.EditValue = record.save_ssi.GetValueOrDefault()
                calcEdit_Savings_Utility.EditValue = record.save_utility.GetValueOrDefault()

                se_Ref_Food.EditValue = record.ref_food
                se_Ref_Car_Ins.EditValue = record.ref_car_ins
                se_Ref_Home_Repair.EditValue = record.ref_home_repair
                se_Ref_Income.EditValue = record.ref_income
                se_Ref_Med.EditValue = record.ref_medical
                se_Ref_Presciption.EditValue = record.ref_prescription
                se_Ref_Prop_Taxes.EditValue = record.ref_property_taxes
                se_Ref_Social.EditValue = record.ref_social_services
                se_Ref_SSI.EditValue = record.ref_ssi
                se_Ref_Utility.EditValue = record.ref_utility

            Finally
                RegisterHandlers()
            End Try

            EnableControls()
        End Sub

        Private Sub Form_Changed(sender As Object, e As EventArgs)
            EnableControls()
        End Sub

        Private Sub EnableControls()
            calcEdit_Savings_Food.Enabled = Enabled AndAlso se_Ref_Food.Value > 0D
            calcEdit_Savings_Med.Enabled = Enabled AndAlso se_Ref_Med.Value > 0D
            calcEdit_Savings_Home_Ins.Enabled = Enabled AndAlso se_Ref_Car_Ins.Value > 0D
            calcEdit_Savings_Home_Repair.Enabled = Enabled AndAlso se_Ref_Home_Repair.Value > 0D
            calcEdit_Savings_Income.Enabled = Enabled AndAlso se_Ref_Income.Value > 0D
            calcEdit_Savings_Prescription.Enabled = Enabled AndAlso se_Ref_Presciption.Value > 0D
            calcEdit_Savings_Prop_Taxes.Enabled = Enabled AndAlso se_Ref_Prop_Taxes.Value > 0D
            calcEdit_Savings_Social.Enabled = Enabled AndAlso se_Ref_Social.Value > 0D
            calcEdit_Savings_SSI.Enabled = Enabled AndAlso se_Ref_SSI.Value > 0D
            calcEdit_Savings_Utility.Enabled = Enabled AndAlso se_Ref_Utility.Value > 0D
        End Sub

        Private Sub lu_bdc_bec_CustomDisplayText(sender As Object, e As CustomDisplayTextEventArgs)
            If e.Value IsNot Nothing Then
                Dim arg As Integer = Convert.ToInt32(e.Value)
                Dim q As DebtPlus.LINQ.hecm_default_lookup = DebtPlus.LINQ.Cache.HECM_Lookup.getYesNoList().Find(Function(s) s.Id = arg)
                If q IsNot Nothing Then
                    e.DisplayText = q.description
                    Return
                End If
            End If

            e.DisplayText = String.Empty
        End Sub

        Private Sub lu_checkup_CustomDisplayText(sender As Object, e As CustomDisplayTextEventArgs)
            If e.Value IsNot Nothing Then
                Dim arg As Integer = Convert.ToInt32(e.Value)
                Dim q As DebtPlus.LINQ.hecm_default_lookup = DebtPlus.LINQ.Cache.HECM_Lookup.getYesNoList().Find(Function(s) s.Id = arg)
                If q IsNot Nothing Then
                    e.DisplayText = q.description
                    Return
                End If
            End If

            e.DisplayText = String.Empty
        End Sub

        Private Sub lu_File_Status_CustomDisplayText(sender As Object, e As CustomDisplayTextEventArgs)
            If e.Value IsNot Nothing Then
                Dim arg As Integer = Convert.ToInt32(e.Value)
                Dim q As DebtPlus.LINQ.hecm_default_lookup = DebtPlus.LINQ.Cache.HECM_Lookup.getFileStatus.Find(Function(s) s.Id = arg)
                If q IsNot Nothing Then
                    e.DisplayText = q.description
                    Return
                End If
            End If

            e.DisplayText = String.Empty
        End Sub

        Private Sub SimpleButton_OK_Click(sender As Object, e As EventArgs)

            record.referral_option = DebtPlus.Utils.Nulls.v_Int32(lu_Option.EditValue)
            record.tier = DebtPlus.Utils.Nulls.v_Int32(lu_Tier.EditValue)
            record.bdt_bec = DebtPlus.Utils.Nulls.v_Int32(lu_bdt_bec.EditValue)
            record.checkup = DebtPlus.Utils.Nulls.v_Int32(lu_checkup.EditValue)
            record.Hardest_Hit_Ref_Date = DebtPlus.Utils.Nulls.v_DateTime(dt_Hardest_Hit_Ref_Date.EditValue)
            record.file_status = DebtPlus.Utils.Nulls.v_Int32(lu_File_Status.EditValue)
            record.file_status_date = DebtPlus.Utils.Nulls.v_DateTime(dt_File_Status_Date.EditValue)

            record.save_food = DebtPlus.Utils.Nulls.v_Decimal(calcEdit_Savings_Food.EditValue)
            record.save_car_ins = DebtPlus.Utils.Nulls.v_Decimal(calcEdit_Savings_Home_Ins.EditValue)
            record.save_home_repair = DebtPlus.Utils.Nulls.v_Decimal(calcEdit_Savings_Home_Repair.EditValue)
            record.save_income = DebtPlus.Utils.Nulls.v_Decimal(calcEdit_Savings_Income.EditValue)
            record.save_medical = DebtPlus.Utils.Nulls.v_Decimal(calcEdit_Savings_Med.EditValue)
            record.save_prescription = DebtPlus.Utils.Nulls.v_Decimal(calcEdit_Savings_Prescription.EditValue)
            record.save_property_taxes = DebtPlus.Utils.Nulls.v_Decimal(calcEdit_Savings_Prop_Taxes.EditValue)
            record.save_social_services = DebtPlus.Utils.Nulls.v_Decimal(calcEdit_Savings_Social.EditValue)
            record.save_ssi = DebtPlus.Utils.Nulls.v_Decimal(calcEdit_Savings_SSI.EditValue)
            record.save_utility = DebtPlus.Utils.Nulls.v_Decimal(calcEdit_Savings_Utility.EditValue)

            record.ref_food = DebtPlus.Utils.Nulls.v_Int32(se_Ref_Food.EditValue)
            record.ref_car_ins = DebtPlus.Utils.Nulls.v_Int32(se_Ref_Car_Ins.EditValue)
            record.ref_home_repair = DebtPlus.Utils.Nulls.v_Int32(se_Ref_Home_Repair.EditValue)
            record.ref_income = DebtPlus.Utils.Nulls.v_Int32(se_Ref_Income.EditValue)
            record.ref_medical = DebtPlus.Utils.Nulls.v_Int32(se_Ref_Med.EditValue)
            record.ref_prescription = DebtPlus.Utils.Nulls.v_Int32(se_Ref_Presciption.EditValue)
            record.ref_property_taxes = DebtPlus.Utils.Nulls.v_Int32(se_Ref_Prop_Taxes.EditValue)
            record.ref_social_services = DebtPlus.Utils.Nulls.v_Int32(se_Ref_Social.EditValue)
            record.ref_ssi = DebtPlus.Utils.Nulls.v_Int32(se_Ref_SSI.EditValue)
            record.ref_utility = DebtPlus.Utils.Nulls.v_Int32(se_Ref_Utility.EditValue)

            DialogResult = Windows.Forms.DialogResult.OK
        End Sub
    End Class
End Namespace
