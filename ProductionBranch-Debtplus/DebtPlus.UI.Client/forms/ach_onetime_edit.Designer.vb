﻿Namespace forms
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class ach_onetime_edit
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.DateEdit_EffectiveDate = New DevExpress.XtraEditors.DateEdit()
            Me.CalcEdit_Amount = New DevExpress.XtraEditors.CalcEdit()
            Me.CheckEdit_CheckingSavings = New DevExpress.XtraEditors.CheckEdit()
            Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit()
            Me.TextEdit_AccountNumber = New DevExpress.XtraEditors.TextEdit()
            Me.TextEdit_ABA = New DevExpress.XtraEditors.TextEdit()
            Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
            Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem_Right = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem_Left = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem4 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.LookUpEdit_bank = New DevExpress.XtraEditors.LookUpEdit()
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.DateEdit_EffectiveDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit_EffectiveDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_Amount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_CheckingSavings.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_AccountNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_ABA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem_Right, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem_Left, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_bank.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'DefaultLookAndFeel1
            '
            Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin"
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_bank)
            Me.LayoutControl1.Controls.Add(Me.DateEdit_EffectiveDate)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit_Amount)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_CheckingSavings)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit1)
            Me.LayoutControl1.Controls.Add(Me.TextEdit_AccountNumber)
            Me.LayoutControl1.Controls.Add(Me.TextEdit_ABA)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton1)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton2)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(284, 218)
            Me.LayoutControl1.TabIndex = 0
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'DateEdit_EffectiveDate
            '
            Me.DateEdit_EffectiveDate.EditValue = Nothing
            Me.DateEdit_EffectiveDate.Location = New System.Drawing.Point(84, 143)
            Me.DateEdit_EffectiveDate.Name = "DateEdit_EffectiveDate"
            Me.DateEdit_EffectiveDate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.DateEdit_EffectiveDate.Properties.Appearance.Options.UseTextOptions = True
            Me.DateEdit_EffectiveDate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.DateEdit_EffectiveDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.DateEdit_EffectiveDate.Properties.ShowToday = False
            Me.DateEdit_EffectiveDate.Properties.VistaEditTime = DevExpress.Utils.DefaultBoolean.[False]
            Me.DateEdit_EffectiveDate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.DateEdit_EffectiveDate.Size = New System.Drawing.Size(188, 20)
            Me.DateEdit_EffectiveDate.StyleController = Me.LayoutControl1
            Me.DateEdit_EffectiveDate.TabIndex = 10
            '
            'CalcEdit_Amount
            '
            Me.CalcEdit_Amount.Location = New System.Drawing.Point(84, 119)
            Me.CalcEdit_Amount.Name = "CalcEdit_Amount"
            Me.CalcEdit_Amount.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit_Amount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_Amount.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_Amount.Properties.DisplayFormat.FormatString = "c"
            Me.CalcEdit_Amount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_Amount.Properties.EditFormat.FormatString = "c"
            Me.CalcEdit_Amount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_Amount.Properties.Mask.BeepOnError = True
            Me.CalcEdit_Amount.Properties.Mask.EditMask = "c"
            Me.CalcEdit_Amount.Properties.Precision = 2
            Me.CalcEdit_Amount.Properties.ValidateOnEnterKey = True
            Me.CalcEdit_Amount.Size = New System.Drawing.Size(188, 20)
            Me.CalcEdit_Amount.StyleController = Me.LayoutControl1
            Me.CalcEdit_Amount.TabIndex = 9
            '
            'CheckEdit_CheckingSavings
            '
            Me.CheckEdit_CheckingSavings.EditValue = "C"
            Me.CheckEdit_CheckingSavings.Location = New System.Drawing.Point(154, 84)
            Me.CheckEdit_CheckingSavings.Name = "CheckEdit_CheckingSavings"
            Me.CheckEdit_CheckingSavings.Properties.Caption = "Savings"
            Me.CheckEdit_CheckingSavings.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
            Me.CheckEdit_CheckingSavings.Properties.RadioGroupIndex = 0
            Me.CheckEdit_CheckingSavings.Properties.ValueChecked = "S"
            Me.CheckEdit_CheckingSavings.Properties.ValueUnchecked = "C"
            Me.CheckEdit_CheckingSavings.Size = New System.Drawing.Size(118, 21)
            Me.CheckEdit_CheckingSavings.StyleController = Me.LayoutControl1
            Me.CheckEdit_CheckingSavings.TabIndex = 8
            Me.CheckEdit_CheckingSavings.TabStop = False
            '
            'CheckEdit1
            '
            Me.CheckEdit1.EditValue = True
            Me.CheckEdit1.Location = New System.Drawing.Point(12, 84)
            Me.CheckEdit1.Name = "CheckEdit1"
            Me.CheckEdit1.Properties.Caption = "Checking"
            Me.CheckEdit1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
            Me.CheckEdit1.Properties.RadioGroupIndex = 0
            Me.CheckEdit1.Size = New System.Drawing.Size(138, 21)
            Me.CheckEdit1.StyleController = Me.LayoutControl1
            Me.CheckEdit1.TabIndex = 7
            '
            'TextEdit_AccountNumber
            '
            Me.TextEdit_AccountNumber.Location = New System.Drawing.Point(84, 60)
            Me.TextEdit_AccountNumber.Name = "TextEdit_AccountNumber"
            Me.TextEdit_AccountNumber.Size = New System.Drawing.Size(188, 20)
            Me.TextEdit_AccountNumber.StyleController = Me.LayoutControl1
            Me.TextEdit_AccountNumber.TabIndex = 6
            '
            'TextEdit_ABA
            '
            Me.TextEdit_ABA.Location = New System.Drawing.Point(84, 36)
            Me.TextEdit_ABA.Name = "TextEdit_ABA"
            Me.TextEdit_ABA.Properties.Mask.BeepOnError = True
            Me.TextEdit_ABA.Properties.Mask.EditMask = "000000000"
            Me.TextEdit_ABA.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
            Me.TextEdit_ABA.Properties.Mask.SaveLiteral = False
            Me.TextEdit_ABA.Properties.MaxLength = 9
            Me.TextEdit_ABA.Size = New System.Drawing.Size(188, 20)
            Me.TextEdit_ABA.StyleController = Me.LayoutControl1
            Me.TextEdit_ABA.TabIndex = 5
            '
            'SimpleButton1
            '
            Me.SimpleButton1.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.SimpleButton1.Location = New System.Drawing.Point(146, 183)
            Me.SimpleButton1.MaximumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton1.MinimumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton1.Name = "SimpleButton1"
            Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton1.StyleController = Me.LayoutControl1
            Me.SimpleButton1.TabIndex = 1
            Me.SimpleButton1.Text = "&Cancel"
            '
            'SimpleButton2
            '
            Me.SimpleButton2.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.SimpleButton2.Location = New System.Drawing.Point(67, 183)
            Me.SimpleButton2.MaximumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton2.MinimumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton2.Name = "SimpleButton2"
            Me.SimpleButton2.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton2.StyleController = Me.LayoutControl1
            Me.SimpleButton2.TabIndex = 2
            Me.SimpleButton2.Text = "&OK"
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup1.GroupBordersVisible = False
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.LayoutControlItem6, Me.LayoutControlItem7, Me.LayoutControlItem8, Me.LayoutControlItem9, Me.EmptySpaceItem_Right, Me.EmptySpaceItem_Left, Me.EmptySpaceItem3, Me.EmptySpaceItem4, Me.LayoutControlItem3})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(284, 218)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.SimpleButton2
            Me.LayoutControlItem1.CustomizationFormText = "LayoutControlItem1"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(55, 171)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(79, 27)
            Me.LayoutControlItem1.Text = "LayoutControlItem1"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem1.TextToControlDistance = 0
            Me.LayoutControlItem1.TextVisible = False
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.SimpleButton1
            Me.LayoutControlItem2.CustomizationFormText = "LayoutControlItem2"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(134, 171)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(79, 27)
            Me.LayoutControlItem2.Text = "LayoutControlItem2"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem2.TextToControlDistance = 0
            Me.LayoutControlItem2.TextVisible = False
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.TextEdit_ABA
            Me.LayoutControlItem4.CustomizationFormText = "ABA #"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(264, 24)
            Me.LayoutControlItem4.Text = "ABA #"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(69, 13)
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Control = Me.TextEdit_AccountNumber
            Me.LayoutControlItem5.CustomizationFormText = "Account #"
            Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 48)
            Me.LayoutControlItem5.Name = "LayoutControlItem5"
            Me.LayoutControlItem5.Size = New System.Drawing.Size(264, 24)
            Me.LayoutControlItem5.Text = "Account #"
            Me.LayoutControlItem5.TextSize = New System.Drawing.Size(69, 13)
            '
            'LayoutControlItem6
            '
            Me.LayoutControlItem6.Control = Me.CheckEdit1
            Me.LayoutControlItem6.CustomizationFormText = "LayoutControlItem6"
            Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 72)
            Me.LayoutControlItem6.Name = "LayoutControlItem6"
            Me.LayoutControlItem6.Size = New System.Drawing.Size(142, 25)
            Me.LayoutControlItem6.Text = "LayoutControlItem6"
            Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem6.TextToControlDistance = 0
            Me.LayoutControlItem6.TextVisible = False
            '
            'LayoutControlItem7
            '
            Me.LayoutControlItem7.Control = Me.CheckEdit_CheckingSavings
            Me.LayoutControlItem7.CustomizationFormText = "LayoutControlItem7"
            Me.LayoutControlItem7.Location = New System.Drawing.Point(142, 72)
            Me.LayoutControlItem7.Name = "LayoutControlItem7"
            Me.LayoutControlItem7.Size = New System.Drawing.Size(122, 25)
            Me.LayoutControlItem7.Text = "LayoutControlItem7"
            Me.LayoutControlItem7.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem7.TextToControlDistance = 0
            Me.LayoutControlItem7.TextVisible = False
            '
            'LayoutControlItem8
            '
            Me.LayoutControlItem8.Control = Me.CalcEdit_Amount
            Me.LayoutControlItem8.CustomizationFormText = "Amount"
            Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 107)
            Me.LayoutControlItem8.Name = "LayoutControlItem8"
            Me.LayoutControlItem8.Size = New System.Drawing.Size(264, 24)
            Me.LayoutControlItem8.Text = "Amount"
            Me.LayoutControlItem8.TextSize = New System.Drawing.Size(69, 13)
            '
            'LayoutControlItem9
            '
            Me.LayoutControlItem9.Control = Me.DateEdit_EffectiveDate
            Me.LayoutControlItem9.CustomizationFormText = "Effective Date"
            Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 131)
            Me.LayoutControlItem9.Name = "LayoutControlItem9"
            Me.LayoutControlItem9.Size = New System.Drawing.Size(264, 24)
            Me.LayoutControlItem9.Text = "Effective Date"
            Me.LayoutControlItem9.TextSize = New System.Drawing.Size(69, 13)
            '
            'EmptySpaceItem_Right
            '
            Me.EmptySpaceItem_Right.AllowHotTrack = False
            Me.EmptySpaceItem_Right.CustomizationFormText = "EmptySpaceItem_Right"
            Me.EmptySpaceItem_Right.Location = New System.Drawing.Point(213, 171)
            Me.EmptySpaceItem_Right.Name = "EmptySpaceItem_Right"
            Me.EmptySpaceItem_Right.Size = New System.Drawing.Size(51, 27)
            Me.EmptySpaceItem_Right.Text = "EmptySpaceItem_Right"
            Me.EmptySpaceItem_Right.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem_Left
            '
            Me.EmptySpaceItem_Left.AllowHotTrack = False
            Me.EmptySpaceItem_Left.CustomizationFormText = "EmptySpaceItem_Left"
            Me.EmptySpaceItem_Left.Location = New System.Drawing.Point(0, 171)
            Me.EmptySpaceItem_Left.Name = "EmptySpaceItem_Left"
            Me.EmptySpaceItem_Left.Size = New System.Drawing.Size(55, 27)
            Me.EmptySpaceItem_Left.Text = "EmptySpaceItem_Left"
            Me.EmptySpaceItem_Left.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem3
            '
            Me.EmptySpaceItem3.AllowHotTrack = False
            Me.EmptySpaceItem3.CustomizationFormText = "EmptySpaceItem3"
            Me.EmptySpaceItem3.Location = New System.Drawing.Point(0, 155)
            Me.EmptySpaceItem3.Name = "EmptySpaceItem3"
            Me.EmptySpaceItem3.Size = New System.Drawing.Size(264, 16)
            Me.EmptySpaceItem3.Text = "EmptySpaceItem3"
            Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem4
            '
            Me.EmptySpaceItem4.AllowHotTrack = False
            Me.EmptySpaceItem4.CustomizationFormText = "EmptySpaceItem4"
            Me.EmptySpaceItem4.Location = New System.Drawing.Point(0, 97)
            Me.EmptySpaceItem4.MaxSize = New System.Drawing.Size(0, 10)
            Me.EmptySpaceItem4.MinSize = New System.Drawing.Size(10, 10)
            Me.EmptySpaceItem4.Name = "EmptySpaceItem4"
            Me.EmptySpaceItem4.Size = New System.Drawing.Size(264, 10)
            Me.EmptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
            Me.EmptySpaceItem4.Text = "EmptySpaceItem4"
            Me.EmptySpaceItem4.TextSize = New System.Drawing.Size(0, 0)
            '
            'LookUpEdit_bank
            '
            Me.LookUpEdit_bank.Location = New System.Drawing.Point(84, 12)
            Me.LookUpEdit_bank.Name = "LookUpEdit_bank"
            Me.LookUpEdit_bank.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_bank.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.LookUpEdit_bank.Properties.DisplayMember = "Name"
            Me.LookUpEdit_bank.Properties.NullText = ""
            Me.LookUpEdit_bank.Properties.ShowFooter = False
            Me.LookUpEdit_bank.Properties.ShowHeader = False
            Me.LookUpEdit_bank.Properties.ValueMember = "Id"
            Me.LookUpEdit_bank.Size = New System.Drawing.Size(188, 20)
            Me.LookUpEdit_bank.StyleController = Me.LayoutControl1
            Me.LookUpEdit_bank.TabIndex = 11
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.LookUpEdit_bank
            Me.LayoutControlItem3.CustomizationFormText = "Bank"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(264, 24)
            Me.LayoutControlItem3.Text = "Bank"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(69, 13)
            '
            'ach_onetime_edit
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(284, 218)
            Me.Controls.Add(Me.LayoutControl1)
            Me.Name = "ach_onetime_edit"
            Me.Text = "ACH OneTime Transaction"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.DateEdit_EffectiveDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit_EffectiveDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_Amount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_CheckingSavings.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_AccountNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_ABA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem_Right, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem_Left, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_bank.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents DateEdit_EffectiveDate As DevExpress.XtraEditors.DateEdit
        Friend WithEvents CalcEdit_Amount As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents CheckEdit_CheckingSavings As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents TextEdit_AccountNumber As DevExpress.XtraEditors.TextEdit
        Friend WithEvents TextEdit_ABA As DevExpress.XtraEditors.TextEdit
        Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem_Right As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem_Left As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem3 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem4 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents LookUpEdit_bank As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    End Class
End Namespace
