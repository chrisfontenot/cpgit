#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.Data.Forms
Imports DebtPlus.Events
Imports DebtPlus.LINQ
Imports DebtPlus.Svc.DataSets
Imports DebtPlus.Utils
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo
Imports System.ComponentModel
Imports System.Data.SqlClient
Imports System.IO
Imports System.Linq

Namespace forms.Budgets

    Friend Class BudgetDetailControl

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        ' Budget information
        Private ReadOnly ds As New DataSet("ds")

        ' Information for the "Number of automobiles" question in the suggested budget field
        Private privateAutoCount As System.Nullable(Of System.Int32) = Nothing

        Private WithEvents _BudgetDetailsTable As DataTable = Nothing
        Private Property BudgetDetailsTable As DataTable
            Get
                Return _BudgetDetailsTable
            End Get
            Set(value As DataTable)
                If _BudgetDetailsTable IsNot Nothing Then
                    RemoveHandler _BudgetDetailsTable.ColumnChanged, AddressOf BudgetTable_ColumnChanged
                End If
                _BudgetDetailsTable = value
                If _BudgetDetailsTable IsNot Nothing Then
                    AddHandler _BudgetDetailsTable.ColumnChanged, AddressOf BudgetTable_ColumnChanged
                End If
            End Set
        End Property

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Load, AddressOf MyGridControl_Load
            AddHandler GridControl1.ProcessGridKey, AddressOf GridControl1_ProcessGridKey
            AddHandler Button_OK.Click, AddressOf Button_OK_Click
            AddHandler Button_Cancel.Click, AddressOf Button_Cancel_Click
            AddHandler ContextMenu1.Popup, AddressOf ContextMenu1_Popup
            AddHandler MenuItem_Delete.Click, AddressOf MenuItem_Delete_Click
            AddHandler MenuItem_Add.Click, AddressOf MenuItem_Add_Click
            AddHandler MenuItem_Edit.Click, AddressOf MenuItem_Edit_Click
            AddHandler GridView1.ShowingEditor, AddressOf GridView1_ShowingEditor
        End Sub

        ''' <summary>
        ''' Load the form
        ''' </summary>
        Public Shadows Sub ReadForm(ByVal Budget As Int32)

            ' Indicate that we need to refresh the auto count upon each entry. The database is updated
            ' by the secured_loans tab and we don't get notification that the values have been changed.
            privateAutoCount = Nothing

            Try
                Using cmd As SqlCommand = New SqlCommand
                    cmd.Connection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    cmd.CommandType = CommandType.Text
                    If Budget <= 0 Then
                        ' Do not attempt to pick up data if the budget is invalid. It is just being created.
                        cmd.CommandText = "SELECT bc.budget_category, bc.description, bc.auto_factor, bc.person_factor, bc.maximum_factor, bc.heading, bc.detail, bc.living_expense, bc.housing_expense, bc.rpps_code, convert(money,case when isnull(bc.detail,1) = 0 then null else 0 end) as client_amount, convert(money,case when isnull(bc.detail,1) = 0 then null else 0 end) as suggested_amount, convert(datetime,null) as date_created, convert(varchar(80),null) as created_by from budget_categories bc union all SELECT bco.budget_category, bco.description, null as auto_factor, null as person_factor, null as maximum_factor, 0 as heading, 1 as detail, 1 as living_expense, 0 as housing_expense, 6 as rpps_code, convert(money,0) as client_amount, convert(money,0) as suggested_amount, convert(datetime,null) as date_created, convert(varchar(80),null) as created_by from budget_categories_other bco where bco.client = @client order by 1"
                    Else
                        cmd.CommandText = "SELECT bc.budget_category, bc.description, bc.auto_factor, bc.person_factor, bc.maximum_factor, bc.heading, bc.detail, bc.living_expense, bc.housing_expense, bc.rpps_code, convert(money,case when isnull(bc.detail,1) = 0 then null else isnull(bd.client_amount,0) end) as client_amount, convert(money,case when isnull(bc.detail,1) = 0 then null else isnull(bd.suggested_amount,0) end) as suggested_amount, bd.date_created, bd.created_by from budget_categories bc left outer join budget_detail bd on bc.budget_category = bd.budget_category and bd.budget = @budget union all SELECT bco.budget_category, bco.description, null as auto_factor, null as person_factor, null as maximum_factor, 0 as heading, 1 as detail, 1 as living_expense, 0 as housing_expense, 6 as rpps_code, isnull(bd.client_amount,0) as client_amount, isnull(bd.suggested_amount,0) as suggested_amount, bd.date_created, bd.created_by from budget_categories_other bco left outer join budget_detail bd on bco.budget_category = bd.budget_category and bd.budget = @budget where bco.client = @client order by 1"
                        cmd.Parameters.Add("@budget", SqlDbType.Int).Value = Budget
                    End If
                    cmd.Parameters.Add("@client", SqlDbType.Int).Value = Context.ClientId

                    Using da As New SqlDataAdapter(cmd)
                        da.Fill(ds, "budget_details")
                    End Using
                End Using

                BudgetDetailsTable = ds.Tables(0)

                ' Find the initial category for creating new items. It is one past the upper limit of the current category list.
                Dim StartingCategory As Object = BudgetDetailsTable.Compute("max(budget_category)", String.Empty)
                If StartingCategory Is Nothing OrElse StartingCategory Is DBNull.Value OrElse Convert.ToInt32(StartingCategory) < 10000 Then
                    StartingCategory = 10000
                End If

                BudgetDetailsTable.PrimaryKey = New DataColumn() {BudgetDetailsTable.Columns("budget_category")}
                For Each colDefault As DataColumn In BudgetDetailsTable.Columns
                    Select Case colDefault.ColumnName

                        ' These have specific defaults
                        Case "detail", "living_expense" : colDefault.DefaultValue = True
                        Case "heading", "housing_expense" : colDefault.DefaultValue = False
                        Case "description" : colDefault.DefaultValue = String.Empty
                        Case "rpps_code" : colDefault.DefaultValue = 6

                            ' The budget category should be a primary key. So, make it unique. (It really isn't, but budget_detail can't as it may be null.)
                        Case "budget_category"
                            colDefault.AutoIncrement = True
                            colDefault.AutoIncrementSeed = Convert.ToInt32(StartingCategory) + 1
                            colDefault.AutoIncrementStep = 1

                            ' All others are simply null
                        Case Else : colDefault.DefaultValue = DBNull.Value
                    End Select
                Next

                ' Add a column for the difference information
                Dim col As New DataColumn("difference", GetType(Decimal), "client_amount-suggested_amount")
                col.DefaultValue = 0D
                BudgetDetailsTable.Columns.Add(col)

                Dim vue As DataView = BudgetDetailsTable.DefaultView
                GridControl1.DataSource = vue
                GridControl1.RefreshDataSource()

                ' Try to find the first row with detail
                For RowHandle As Int32 = 0 To GridView1.RowCount - 1
                    If GridView1.IsDataRow(RowHandle) Then
                        If Convert.ToBoolean(GridView1.GetDataRow(RowHandle)("detail")) Then
                            GridView1.FocusedRowHandle = RowHandle
                            GridView1.FocusedColumn = GridColumn_current_amount
                            Exit For
                        End If
                    End If
                Next

            Catch ex As SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading budget details")
            End Try
        End Sub

        ''' <summary>
        ''' Save the information once we have a budget ID
        ''' </summary>
        Public Shadows Sub SaveForm(ByVal Budget As Int32, ByVal NewBudget As Boolean)
            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim txn As SqlTransaction = Nothing
            Try
                cn.Open()
                txn = cn.BeginTransaction

                ' Obtain the list of items deleted from the table. These need to be delete from the other categories table first.
                Using vue As New DataView(BudgetDetailsTable, String.Empty, String.Empty, DataViewRowState.Deleted)
                    For Each drv As DataRowView In vue

                        ' Do not allow the category to be one the standard items
                        If DebtPlus.Utils.Nulls.DInt(drv("budget_category")) < 10000 Then Throw New ApplicationException("Budget category deleted is not in valid range")
                        If Not DebtPlus.Utils.Nulls.DBool(drv("detail")) Then Throw New ApplicationException("Budget category being deleted is not marked DETAIL")

                        ' Delete the additional budget category. The system will handle the deletion of the details.
                        Using cmd As SqlCommand = New SqlCommand
                            cmd.Connection = cn
                            cmd.Transaction = txn
                            cmd.CommandText = "DELETE FROM budget_categories_other WHERE budget_category=@budget_category"
                            cmd.Parameters.Add("@budget_category", SqlDbType.Int).Value = drv("budget_category")
                            cmd.ExecuteNonQuery()
                        End Using
                    Next
                End Using

                ' Obtain the list of items inserted into the table. These need to be added to the other categories table first.
                Using vue As New DataView(BudgetDetailsTable, String.Empty, String.Empty, DataViewRowState.Added)
                    For Each drv As DataRowView In vue
                        Using cmd As SqlCommand = New SqlCommand
                            cmd.Connection = cn
                            cmd.Transaction = txn
                            cmd.CommandText = "xpr_insert_budget_categories_other"
                            cmd.CommandType = CommandType.StoredProcedure

                            cmd.Parameters.Add("RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                            cmd.Parameters.Add("@client", SqlDbType.Int).Value = Context.ClientId
                            cmd.Parameters.Add("@description", SqlDbType.VarChar, 50).Value = drv("description")
                            cmd.ExecuteNonQuery()

                            ' Update the row with the budget category
                            drv.BeginEdit()
                            drv("budget_category") = cmd.Parameters(0).Value
                            drv.EndEdit()
                        End Using

                        ' If this is not a new budget then attempt to write the detail information.
                        ' If this is a new budget, it will be written because all items are written.
                        If Not NewBudget Then InsertRow(cn, txn, Budget, drv)
                    Next
                End Using

                ' If we are creating a new budget then use the whole table. Otherwise, just look for the modified rows.
                If NewBudget Then
                    Using vue As New DataView(BudgetDetailsTable, "([detail]<>0) AND (([client_amount]>0) OR ([suggested_amount]>0))", "budget_category", DataViewRowState.CurrentRows)
                        For Each drv As DataRowView In vue
                            InsertRow(cn, txn, Budget, drv)
                        Next drv
                    End Using

                Else

                    ' Process only the modified rows but accept all "comers"
                    Using vue As New DataView(BudgetDetailsTable, "[detail]<>0", String.Empty, DataViewRowState.ModifiedCurrent)
                        For Each drv As DataRowView In vue
                            If UpdateRow(cn, txn, Budget, drv) = 0 Then
                                InsertRow(cn, txn, Budget, drv)
                            End If
                        Next drv
                    End Using
                End If

                ' Just for consistency, update the checkpoint here too
                BudgetDetailsTable.AcceptChanges()

                ' Commit the transaction at this point and indicate that we need to reload the list.
                txn.Commit()
                txn = Nothing

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating the budget details information")
            End Try
        End Sub

        Private Function UpdateRow(ByVal cn As SqlConnection, ByVal txn As SqlTransaction, ByVal Budget As Int32, ByVal drv As DataRowView) As Int32
            Dim answer As Int32
            Using cmd As SqlCommand = New SqlCommand
                cmd.Connection = cn
                cmd.Transaction = txn
                cmd.CommandText = "UPDATE budget_detail SET client_amount=@client_amount,suggested_amount=@suggested_amount WHERE budget=@budget AND budget_category=@budget_category"
                cmd.Parameters.Add("@budget", SqlDbType.Int).Value = Budget
                cmd.Parameters.Add("@budget_category", SqlDbType.Int).Value = drv("budget_category")
                cmd.Parameters.Add("@client_amount", SqlDbType.Decimal).Value = drv("client_amount")
                cmd.Parameters.Add("@suggested_amount", SqlDbType.Decimal).Value = drv("suggested_amount")
                answer = cmd.ExecuteNonQuery()
            End Using

            Return answer
        End Function

        Private Function InsertRow(ByVal cn As SqlConnection, ByVal txn As SqlTransaction, ByVal Budget As Int32, ByVal drv As DataRowView) As Int32
            Dim answer As Int32
            Using cmd As SqlCommand = New SqlCommand
                cmd.Connection = cn
                cmd.Transaction = txn
                cmd.CommandText = "INSERT INTO budget_detail(budget,budget_category,client_amount,suggested_amount) VALUES (@budget,@budget_category,isnull(@client_amount,0),isnull(@suggested_amount,0));"
                cmd.Parameters.Add("@budget", SqlDbType.Int).Value = Budget
                cmd.Parameters.Add("@budget_category", SqlDbType.Int).Value = drv("budget_category")
                cmd.Parameters.Add("@client_amount", SqlDbType.Decimal).Value = drv("client_amount")
                cmd.Parameters.Add("@suggested_amount", SqlDbType.Decimal).Value = drv("suggested_amount")
                answer = cmd.ExecuteNonQuery()
            End Using

            Return answer
        End Function

        Public Event Completed As EventHandler

        ''' <summary>
        ''' Process the OK button
        ''' </summary>
        Private Sub Button_OK_Click(ByVal sender As Object, ByVal e As EventArgs)
            RaiseEvent Completed(Me, e)
        End Sub

        Public Event Cancelled As EventHandler

        ''' <summary>
        ''' Process the CANCEL button
        ''' </summary>
        Private Sub Button_Cancel_Click(ByVal sender As Object, ByVal e As EventArgs)
            RaiseEvent Cancelled(Me, e)
        End Sub

        ''' <summary>
        ''' Validate the changes to the grid editing controls.
        ''' </summary>
        Private Sub GridView1_ValidatingEditor(ByVal sender As Object, ByVal e As BaseContainerValidateEditorEventArgs)
            e.Valid = True
            ' Assume that all is valid

            ' Process the edit of the current amount
            If GridView1.FocusedColumn Is GridColumn_current_amount OrElse GridView1.FocusedColumn Is GridColumn_suggested _
                Then
                If e.Value IsNot Nothing Then
                    If Convert.ToDecimal(e.Value) < 0D Then
                        e.Valid = False
                        e.ErrorText = "Minimum amount is $0.00"
                    End If
                End If
            End If
        End Sub

        ''' <summary>
        ''' Process a change in the row for the grid
        ''' </summary>
        Private Sub GridView1_FocusedRowChanged(ByVal sender As Object, ByVal e As FocusedRowChangedEventArgs)
            Dim drv As DataRowView = CType(GridView1.GetRow(e.FocusedRowHandle), DataRowView)

            ' If we are at the new item row then enable the editors
            If e.FocusedRowHandle = GridControl.NewItemRowHandle Then
                GridColumn_description.OptionsColumn.AllowEdit = True
                GridColumn_current_amount.OptionsColumn.AllowEdit = True
                GridColumn_suggested.OptionsColumn.AllowEdit = True

            Else
                Debug.Assert(Not IsNothing(drv))

                ' Allow a change to the category name if this was recently added.
                If Convert.ToInt32(drv("budget_category")) < 0 Then
                    GridColumn_description.OptionsColumn.AllowEdit = True
                Else
                    GridColumn_description.OptionsColumn.AllowEdit = False
                End If

                If Convert.ToBoolean(drv("detail")) Then
                    GridColumn_current_amount.OptionsColumn.AllowEdit = True
                    GridColumn_suggested.OptionsColumn.AllowEdit = True
                Else ' The row is not a detail row. Do not allow the edit operation
                    GridColumn_current_amount.OptionsColumn.AllowEdit = False
                    GridColumn_suggested.OptionsColumn.AllowEdit = False
                End If
            End If
        End Sub

        ''' <summary>
        ''' Handle updates on the table. Do any copy operations.
        ''' </summary>
        Private Sub BudgetTable_ColumnChanged(ByVal sender As Object, ByVal e As DataColumnChangeEventArgs)
            Dim row As DataRow = e.Row

            ' Tell the handlers that the budget information was changed
            Context.ClientDs.EnvokeHandlers(sender, New FieldChangedEventArgs(FieldChangedEventArgs.Join("budget_details", e.Column.ColumnName), e.ProposedValue))

            ' If we are setting the client amount then copy it to the suggested amount
            If e.Column.ColumnName = "client_amount" AndAlso e.ProposedValue IsNot DBNull.Value Then
                If IsNothing(row("suggested_amount")) OrElse row("suggested_amount") Is DBNull.Value OrElse
                    Convert.ToDecimal(row("suggested_amount")) = 0D Then

                    ' Find the current value and the limit for the suggested item
                    Dim NewValue As Decimal = Convert.ToDecimal(e.ProposedValue)
                    Dim MaxValue As Decimal = MaximumAmount(row)

                    ' If the new amount is more than suggested, limit its value
                    If NewValue > MaxValue Then NewValue = MaxValue

                    ' Set the suggested amount column so that the counselor need not enter it.
                    GridView1.BeginUpdate()
                    row("suggested_amount") = NewValue
                    GridView1.EndUpdate()
                End If
            End If
        End Sub

        ''' <summary>
        ''' Local function to find the maximum amount for the cell
        ''' </summary>
        Private Function MaximumAmount(ByVal Row As DataRow) As Decimal
            Dim answer As Decimal = Decimal.MaxValue

            If Row IsNot Nothing Then

                Dim person_factor As Decimal
                If Row("person_factor") IsNot Nothing AndAlso Row("person_factor") IsNot DBNull.Value Then
                    person_factor = Convert.ToDecimal(Row("person_factor"))
                    answer = person_factor * Convert.ToDecimal(People)
                End If

                Dim auto_factor As Decimal
                If Row("auto_factor") IsNot Nothing AndAlso Row("auto_factor") IsNot DBNull.Value Then
                    auto_factor = Convert.ToDecimal(Row("auto_factor"))
                    answer = auto_factor * Convert.ToDecimal(AutoCount)
                End If

                Dim maximum_factor As Decimal
                If Not IsNothing(Row("maximum_factor")) AndAlso Row("maximum_factor") IsNot DBNull.Value Then
                    maximum_factor = Convert.ToDecimal(Row("maximum_factor"))
                    If answer > maximum_factor Then answer = maximum_factor
                End If
            End If

            Return answer
        End Function

        ''' <summary>
        ''' Find the number of automobiles for the client.
        ''' </summary>
        Private ReadOnly Property AutoCount As Int32
            Get
                ' If there is no value then determine the value on the first call.
                If Not privateAutoCount.HasValue Then
                    Using bc As New DebtPlus.LINQ.BusinessContext()
                        Dim col As System.Collections.Generic.List(Of secured_property) = _
                            bc.secured_type_groups _
                            .Join(bc.secured_types, Function(stg) stg.Id, Function(st) st.secured_type_group, Function(stg, st) New With {.stg = stg, .st = st}) _
                            .Join(bc.secured_properties, Function(x) x.st.Id, Function(p) p.secured_type, Function(x, p) New With {.stg = x.stg, .st = x.st, .p = p}) _
                            .Where(Function(x) x.p.client = Context.ClientDs.ClientId AndAlso x.stg.KeyType = "AUTO") _
                            .Select(Function(x) x.p).ToList()

                        privateAutoCount = col.Count()
                    End Using
                End If

                Return privateAutoCount.Value
            End Get
        End Property

        ''' <summary>
        ''' Find the number of people in the household
        ''' </summary>
        Private ReadOnly Property People As Int32
            Get
                Return Context.ClientDs.colPeople.Count() + Context.ClientDs.clientRecord.dependents
            End Get
        End Property

        ''' <summary>
        ''' Process a right click on the menu items for the grid
        ''' </summary>
        Private Sub ContextMenu1_Popup(ByVal sender As Object, ByVal e As EventArgs)
            Dim gv As GridView = GridView1
            Dim ctl As GridControl = CType(gv.GridControl, GridControl)
            Dim hi As GridHitInfo = gv.CalcHitInfo((ctl.PointToClient(MousePosition)))
            Dim DeleteBudgetCategory As Int32 = 0

            ' Remember the position for the popup menu handler.
            MenuItem_Delete.Enabled = False
            MenuItem_Edit.Enabled = False
            If hi.InRow Then
                Dim ControlRow As Int32 = hi.RowHandle
                Dim drv As DataRowView = CType(gv.GetRow(ControlRow), DataRowView)
                If drv IsNot Nothing Then
                    If Not IsNothing(drv("budget_category")) AndAlso drv("budget_category") IsNot DBNull.Value Then
                        DeleteBudgetCategory = Convert.ToInt32(drv("budget_category"))

                        MenuItem_Delete.Tag = DeleteBudgetCategory
                        MenuItem_Delete.Enabled = (DeleteBudgetCategory < 0 OrElse DeleteBudgetCategory >= 10000)

                        MenuItem_Edit.Tag = DeleteBudgetCategory
                        MenuItem_Edit.Enabled = (DeleteBudgetCategory < 0 OrElse DeleteBudgetCategory >= 10000)
                    End If
                End If
            End If
        End Sub

        ''' <summary>
        ''' Handle the delete operation for a category
        ''' </summary>
        Private Sub MenuItem_Delete_Click(ByVal sender As Object, ByVal e As EventArgs)
            Using vue As New DataView(BudgetDetailsTable, String.Format("budget_category={0:d}", Convert.ToInt32(MenuItem_Delete.Tag)), String.Empty, DataViewRowState.CurrentRows)
                If vue.Count > 0 Then vue(0).Delete()
            End Using
        End Sub

        ''' <summary>
        ''' Add a new category to the list of categories for this user
        ''' </summary>
        Private Sub MenuItem_Add_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim CategoryName As String = String.Empty

            Do
                Select Case InputBox.Show("Name of the optional category", CategoryName, "New Budget Category", String.Empty, 50)

                    Case DialogResult.OK
                        ' Remove leading/trailing blanks. If blank then do not create the category.
                        CategoryName = CategoryName.Trim()
                        If CategoryName = String.Empty Then Exit Do

                        ' Ensure that the row is not currently defined with this category
                        Dim DuplicatesCount As Object = BudgetDetailsTable.Compute("count(budget_category)", String.Format("description = '{0}'", CategoryName.Replace("'", "''")))
                        If DuplicatesCount Is Nothing OrElse DuplicatesCount Is DBNull.Value Then DuplicatesCount = 0

                        ' If the name is not duplicated then accept the value
                        If Convert.ToInt32(DuplicatesCount) <= 0 Then
                            Dim row As DataRow = BudgetDetailsTable.NewRow
                            row.BeginEdit()
                            row.Item("description") = CategoryName
                            row.EndEdit()

                            ' Add the new row to the table
                            GridView1.BeginUpdate()
                            Try
                                BudgetDetailsTable.Rows.Add(row)
                            Finally
                                GridView1.EndUpdate()
                            End Try
                            Exit Do
                        End If

                        MessageBox.Show("The category is currently defined and may not be duplicated. Please use a different name.", "Duplicate category name", MessageBoxButtons.OK, MessageBoxIcon.Error)

                    Case Else
                        Exit Do
                End Select
            Loop
        End Sub

        ''' <summary>
        ''' Edit the category value when desired
        ''' </summary>
        Private Sub MenuItem_Edit_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim CategoryName As String = String.Empty

            Using vue As New DataView(BudgetDetailsTable, String.Format("budget_category={0:d}", Convert.ToInt32(MenuItem_Delete.Tag)), String.Empty, DataViewRowState.CurrentRows)
                Dim row As DataRow = vue(0).Row
                Dim CurrentDescription As String

                If vue.Count > 0 Then
                    CurrentDescription = Convert.ToString(vue(0)("description")).Trim()
                Else
                    CurrentDescription = String.Empty
                End If

                Do While CurrentDescription <> String.Empty
                    If InputBox.Show(String.Format("You may change the category name to a new value by entering the name here.{0}{0}WARNING: Doing this will change the names on all of the budgets for this client.", Environment.NewLine), CategoryName, "Change Budget Category Name", CurrentDescription, 50) <> DialogResult.OK Then
                        Exit Do
                    End If

                    ' Remove leading/trailing blanks. If blank then do not create the category.
                    CategoryName = CategoryName.Trim()
                    If CategoryName = String.Empty Then Exit Do

                    ' Ensure that the row is not currently defined with this category
                    Dim DuplicatesCount As Object = BudgetDetailsTable.Compute("count(budget_category)", String.Format("description = '{0}' AND description <> '{1}'", CategoryName.Replace("'", "''"), CurrentDescription.Replace("'", "''")))
                    If DuplicatesCount Is Nothing OrElse DuplicatesCount Is DBNull.Value Then DuplicatesCount = 0

                    ' If the name is not duplicated then accept the value
                    If Convert.ToInt32(DuplicatesCount) <= 0 Then

                        ' If the row is on the database then ensure then update the name directly.
                        ' There is no other way to tell that the item was changed.
                        If row.RowState <> DataRowState.Added Then
                            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                            Dim RowsUpdated As Int32
                            Try
                                cn.Open()
                                Using cmd As SqlCommand = New SqlCommand
                                    cmd.Connection = cn
                                    cmd.CommandText = "UPDATE budget_categories_other SET description=@new_description WHERE budget_category = @budget_category AND client = @client"
                                    cmd.CommandType = CommandType.Text
                                    cmd.Parameters.Add("@new_description", SqlDbType.VarChar, 50).Value = CategoryName
                                    cmd.Parameters.Add("@budget_category", SqlDbType.Int).Value = row("budget_category")
                                    cmd.Parameters.Add("@client", SqlDbType.Int).Value = Context.ClientId
                                    RowsUpdated = cmd.ExecuteNonQuery()
                                End Using

                            Catch ex As SqlException
                                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating budget category descriptions")

                            Finally
                                If cn IsNot Nothing Then cn.Dispose()
                            End Try
                        End If

                        ' Update the description with the changes
                        row("description") = CategoryName
                        Exit Do
                    End If
                Loop
            End Using
        End Sub

        ''' <summary>
        ''' Do not show the editor on non-detail fields
        ''' </summary>
        Private Sub GridView1_ShowingEditor(ByVal sender As Object, ByVal e As CancelEventArgs)
            Dim IsDetail As Boolean = GridView1.IsDataRow(GridView1.FocusedRowHandle)
            If IsDetail Then
                Dim CurrentRow As DataRow = CType(GridView1.GetFocusedRow, DataRowView).Row
                If Convert.ToInt32(CurrentRow("budget_category")) < 10000 Then
                    If Convert.ToBoolean(CurrentRow("heading")) Then IsDetail = False
                End If
            End If

            e.Cancel = Not IsDetail
        End Sub

        ''' <summary>
        ''' Return the directory to the saved layout information
        ''' </summary>
        Protected Overridable Function XMLBasePath() As String
            Dim BasePath As String = String.Format("{0}{1}DebtPlus", Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), Path.DirectorySeparatorChar)
            Return Path.Combine(BasePath, String.Format("Client.Update{0}{1}", Path.DirectorySeparatorChar, ParentForm.Name))
        End Function

        ''' <summary>
        ''' When loading the control, restore the layout from the file
        ''' </summary>
        Private Sub MyGridControl_Load(ByVal sender As Object, ByVal e As EventArgs)
            Dim PathName As String = XMLBasePath()
            Dim FileName As String = Path.Combine(PathName, Name + ".Grid.xml")

            ' Start by fitting the columns as best that we can.
            GridView1.BestFitColumns()

            Try
                If File.Exists(FileName) Then
                    GridView1.RestoreLayoutFromXml(FileName)
                End If

            Catch ex As DirectoryNotFoundException
            Catch ex As FileNotFoundException
            Catch ex As FileLoadException
            End Try

            ' Hook into the changes of the layout from this point
            AddHandler GridView1.Layout, AddressOf LayoutChanged
        End Sub

        ''' <summary>
        ''' If the layout is changed then update the file
        ''' </summary>
        Private Sub LayoutChanged(ByVal Sender As Object, ByVal e As EventArgs)

            Try
                Dim PathName As String = XMLBasePath()
                If Not Directory.Exists(PathName) Then
                    Directory.CreateDirectory(PathName)
                End If

                Dim FileName As String = Path.Combine(PathName, Name + ".Grid.xml")
                GridView1.SaveLayoutToXml(FileName)

            Catch ex As IOException
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
            End Try
        End Sub

        ''' <summary>
        ''' Handle the keyboard preview on the gridview
        ''' </summary>
        Private Sub GridControl1_ProcessGridKey(ByVal sender As Object, ByVal e As KeyEventArgs)
            Dim NewRow As Int32 = GridView1.FocusedRowHandle

            ' Do nothing special in the non-grid areas
            If Not GridView1.IsDataRow(NewRow) Then Return

            ' Look at the key. If this key is a back tab then try to skip the non-edit fields in reverse
            If e.KeyCode = Keys.Tab Then
                Dim NewColumn As GridColumn = GridView1.FocusedColumn
                If NewColumn Is Nothing Then Return

                If e.Shift Then

                    '------------------------------------------------------------------------------
                    '         Back Tab                                                         --
                    '------------------------------------------------------------------------------

                    ' Go from the current amout to the previous line's suggested
                    If NewColumn Is GridColumn_current_amount Then
                        Do
                            NewRow -= 1
                            If NewRow < 0 Then Return
                            Dim row As DataRow = GridView1.GetDataRow(NewRow)
                            If row Is Nothing Then Return
                            If Convert.ToBoolean(row("detail")) Then Exit Do
                        Loop
                        NewColumn = GridColumn_suggested
                    Else

                        ' Go from the suggested to the current line's input amount
                        If NewColumn IsNot GridColumn_suggested Then
                            Return
                        End If
                        NewColumn = GridColumn_current_amount
                    End If

                Else

                    '------------------------------------------------------------------------------
                    '       Forward Tab                                                        --
                    '------------------------------------------------------------------------------

                    ' If this is the suggested column then go to the next row's input field
                    If NewColumn Is GridColumn_suggested Then
                        Do
                            NewRow += 1
                            Dim row As DataRow = GridView1.GetDataRow(NewRow)
                            If row Is Nothing Then Return
                            If Convert.ToBoolean(row("detail")) Then Exit Do
                        Loop
                        NewColumn = GridColumn_current_amount

                    Else ' Otherwise, if this is not the input amount then let the system handle it.
                        If NewColumn IsNot GridColumn_current_amount Then
                            Return
                        End If

                        ' Go from the input amount to the suggested column.
                        NewColumn = GridColumn_suggested
                    End If
                End If

                ' Set the new row and column
                GridView1.FocusedColumn = NewColumn
                GridView1.FocusedRowHandle = NewRow

                ' Tell the grid control not to process the tab key
                e.Handled = True
            End If
        End Sub

        Public Sub PrintReport()
            Using rpt As New BudgetReport(Context.ClientId, GridControl1)
                rpt.DisplayPreviewDialog()
            End Using
        End Sub
    End Class
End Namespace
