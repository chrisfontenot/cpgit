﻿Imports DebtPlus.UI.Client.controls

Namespace forms.Budgets

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class BudgetDetailControl
        Inherits ControlBaseClient

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then
                        components.Dispose()
                    End If
                    ds.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim StyleFormatCondition1 As DevExpress.XtraGrid.StyleFormatCondition = New DevExpress.XtraGrid.StyleFormatCondition()
            Dim StyleFormatCondition2 As DevExpress.XtraGrid.StyleFormatCondition = New DevExpress.XtraGrid.StyleFormatCondition()
            Me.GridColumn_difference = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_heading = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton()
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton()
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
            Me.ContextMenu1 = New System.Windows.Forms.ContextMenu()
            Me.MenuItem_Add = New System.Windows.Forms.MenuItem()
            Me.MenuItem_Edit = New System.Windows.Forms.MenuItem()
            Me.MenuItem_Delete = New System.Windows.Forms.MenuItem()
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
            Me.GridColumn_budget_category = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_auto = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_person = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_maximum = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_detail = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_housing = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_living = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_description = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.Description_Edit = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
            Me.GridColumn_current_amount = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.Current_Amount_Edit = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit()
            Me.GridColumn_suggested = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.Suggested_Amount_Edit = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Description_Edit, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Current_Amount_Edit, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Suggested_Amount_Edit, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridColumn_difference
            '
            Me.GridColumn_difference.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_difference.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_difference.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_difference.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_difference.Caption = "Difference"
            Me.GridColumn_difference.CustomizationCaption = "Difference Amount"
            Me.GridColumn_difference.DisplayFormat.FormatString = "c"
            Me.GridColumn_difference.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_difference.FieldName = "difference"
            Me.GridColumn_difference.GroupFormat.FormatString = "c"
            Me.GridColumn_difference.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_difference.Name = "GridColumn_difference"
            Me.GridColumn_difference.OptionsColumn.AllowEdit = False
            Me.GridColumn_difference.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_difference.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "difference", "{0:c}")})
            Me.GridColumn_difference.Visible = True
            Me.GridColumn_difference.VisibleIndex = 3
            Me.GridColumn_difference.Width = 73
            '
            'GridColumn_heading
            '
            Me.GridColumn_heading.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_heading.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_heading.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_heading.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_heading.Caption = "Heading"
            Me.GridColumn_heading.CustomizationCaption = "Heading Category"
            Me.GridColumn_heading.FieldName = "heading"
            Me.GridColumn_heading.Name = "GridColumn_heading"
            Me.GridColumn_heading.OptionsColumn.AllowEdit = False
            Me.GridColumn_heading.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Button_Cancel.Location = New System.Drawing.Point(268, 293)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.TabIndex = 5
            Me.Button_Cancel.Text = "&Cancel"
            '
            'Button_OK
            '
            Me.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.Button_OK.DialogResult = System.Windows.Forms.DialogResult.No
            Me.Button_OK.Location = New System.Drawing.Point(172, 293)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.Size = New System.Drawing.Size(75, 23)
            Me.Button_OK.TabIndex = 4
            Me.Button_OK.Text = "&OK"
            '
            'GridControl1
            '
            Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                                             Or System.Windows.Forms.AnchorStyles.Left) _
                                            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GridControl1.ContextMenu = Me.ContextMenu1
            Me.GridControl1.Location = New System.Drawing.Point(3, 3)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.Current_Amount_Edit, Me.Suggested_Amount_Edit, Me.Description_Edit})
            Me.GridControl1.Size = New System.Drawing.Size(502, 284)
            Me.GridControl1.TabIndex = 3
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'ContextMenu1
            '
            Me.ContextMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem_Add, Me.MenuItem_Edit, Me.MenuItem_Delete})
            '
            'MenuItem_Add
            '
            Me.MenuItem_Add.Index = 0
            Me.MenuItem_Add.Text = "Add..."
            '
            'MenuItem_Edit
            '
            Me.MenuItem_Edit.Index = 1
            Me.MenuItem_Edit.Text = "&Edit..."
            '
            'MenuItem_Delete
            '
            Me.MenuItem_Delete.Index = 2
            Me.MenuItem_Delete.Text = "&Delete"
            '
            'GridView1
            '
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_budget_category, Me.GridColumn_auto, Me.GridColumn_person, Me.GridColumn_maximum, Me.GridColumn_heading, Me.GridColumn_detail, Me.GridColumn_housing, Me.GridColumn_living, Me.GridColumn_description, Me.GridColumn_current_amount, Me.GridColumn_suggested, Me.GridColumn_difference})
            StyleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(255, Byte), Int32), CType(CType(192, Byte), Int32))
            StyleFormatCondition1.Appearance.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(255, Byte), Int32), CType(CType(192, Byte), Int32))
            StyleFormatCondition1.Appearance.Options.UseBackColor = True
            StyleFormatCondition1.Column = Me.GridColumn_difference
            StyleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.NotEqual
            StyleFormatCondition1.Value1 = New Decimal(New Int32() {0, 0, 0, 0})
            StyleFormatCondition2.Appearance.BackColor = System.Drawing.Color.Red
            StyleFormatCondition2.Appearance.BackColor2 = System.Drawing.Color.Red
            StyleFormatCondition2.Appearance.Font = New System.Drawing.Font("Verdana", 7.5!, System.Drawing.FontStyle.Bold)
            StyleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.White
            StyleFormatCondition2.Appearance.Options.UseBackColor = True
            StyleFormatCondition2.Appearance.Options.UseFont = True
            StyleFormatCondition2.Appearance.Options.UseForeColor = True
            StyleFormatCondition2.ApplyToRow = True
            StyleFormatCondition2.Column = Me.GridColumn_heading
            StyleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal
            StyleFormatCondition2.Value1 = True
            Me.GridView1.FormatConditions.AddRange(New DevExpress.XtraGrid.StyleFormatCondition() {StyleFormatCondition1, StyleFormatCondition2})
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.NewItemRowText = "Enter New Item here"
            Me.GridView1.OptionsCustomization.AllowSort = False
            Me.GridView1.OptionsNavigation.AutoFocusNewRow = True
            Me.GridView1.OptionsNavigation.EnterMoveNextColumn = True
            Me.GridView1.OptionsPrint.EnableAppearanceEvenRow = True
            Me.GridView1.OptionsPrint.EnableAppearanceOddRow = True
            Me.GridView1.OptionsView.EnableAppearanceEvenRow = True
            Me.GridView1.OptionsView.EnableAppearanceOddRow = True
            Me.GridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Bottom
            Me.GridView1.OptionsView.RowAutoHeight = True
            Me.GridView1.OptionsView.ShowFooter = True
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn_budget_category, DevExpress.Data.ColumnSortOrder.Ascending)})
            '
            'GridColumn_budget_category
            '
            Me.GridColumn_budget_category.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_budget_category.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_budget_category.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_budget_category.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_budget_category.Caption = "Category ID"
            Me.GridColumn_budget_category.CustomizationCaption = "ID of budget category"
            Me.GridColumn_budget_category.FieldName = "budget_category"
            Me.GridColumn_budget_category.Name = "GridColumn_budget_category"
            Me.GridColumn_budget_category.OptionsColumn.AllowEdit = False
            Me.GridColumn_budget_category.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_auto
            '
            Me.GridColumn_auto.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_auto.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_auto.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_auto.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_auto.Caption = "Auto"
            Me.GridColumn_auto.CustomizationCaption = "Factor for Automobiles"
            Me.GridColumn_auto.DisplayFormat.FormatString = "$#,##0.00;($#,##0.00); "
            Me.GridColumn_auto.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_auto.FieldName = "auto_factor"
            Me.GridColumn_auto.GroupFormat.FormatString = "c"
            Me.GridColumn_auto.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_auto.Name = "GridColumn_auto"
            Me.GridColumn_auto.OptionsColumn.AllowEdit = False
            Me.GridColumn_auto.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_person
            '
            Me.GridColumn_person.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_person.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_person.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_person.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_person.Caption = "Person"
            Me.GridColumn_person.CustomizationCaption = "Factor for per-person"
            Me.GridColumn_person.DisplayFormat.FormatString = "$#,##0.00;($#,##0.00); "
            Me.GridColumn_person.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_person.FieldName = "person_factor"
            Me.GridColumn_person.GroupFormat.FormatString = "c"
            Me.GridColumn_person.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_person.Name = "GridColumn_person"
            Me.GridColumn_person.OptionsColumn.AllowEdit = False
            Me.GridColumn_person.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_maximum
            '
            Me.GridColumn_maximum.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_maximum.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_maximum.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_maximum.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_maximum.Caption = "Maximum"
            Me.GridColumn_maximum.CustomizationCaption = "Maximum dollar amount for the category"
            Me.GridColumn_maximum.DisplayFormat.FormatString = "c"
            Me.GridColumn_maximum.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_maximum.FieldName = "maximum_factor"
            Me.GridColumn_maximum.GroupFormat.FormatString = "c"
            Me.GridColumn_maximum.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_maximum.Name = "GridColumn_maximum"
            Me.GridColumn_maximum.OptionsColumn.AllowEdit = False
            Me.GridColumn_maximum.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_detail
            '
            Me.GridColumn_detail.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_detail.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_detail.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_detail.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_detail.Caption = "Detail"
            Me.GridColumn_detail.CustomizationCaption = "Allow budget detail on this row?"
            Me.GridColumn_detail.FieldName = "detail"
            Me.GridColumn_detail.Name = "GridColumn_detail"
            Me.GridColumn_detail.OptionsColumn.AllowEdit = False
            Me.GridColumn_detail.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_housing
            '
            Me.GridColumn_housing.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_housing.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_housing.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_housing.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_housing.Caption = "Housing"
            Me.GridColumn_housing.CustomizationCaption = "Is it a housing expense?"
            Me.GridColumn_housing.FieldName = "housing_expense"
            Me.GridColumn_housing.Name = "GridColumn_housing"
            Me.GridColumn_housing.OptionsColumn.AllowEdit = False
            Me.GridColumn_housing.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_living
            '
            Me.GridColumn_living.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_living.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_living.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_living.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_living.Caption = "Living"
            Me.GridColumn_living.CustomizationCaption = "Is it a living expense?"
            Me.GridColumn_living.FieldName = "living_expense"
            Me.GridColumn_living.Name = "GridColumn_living"
            Me.GridColumn_living.OptionsColumn.AllowEdit = False
            Me.GridColumn_living.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_description
            '
            Me.GridColumn_description.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_description.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_description.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_description.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_description.Caption = "Budget Category"
            Me.GridColumn_description.ColumnEdit = Me.Description_Edit
            Me.GridColumn_description.CustomizationCaption = "Budget Category Description"
            Me.GridColumn_description.FieldName = "description"
            Me.GridColumn_description.Name = "GridColumn_description"
            Me.GridColumn_description.OptionsColumn.AllowEdit = False
            Me.GridColumn_description.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_description.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "budget_category", "TOTALS")})
            Me.GridColumn_description.Visible = True
            Me.GridColumn_description.VisibleIndex = 0
            Me.GridColumn_description.Width = 192
            '
            'Description_Edit
            '
            Me.Description_Edit.AutoHeight = False
            Me.Description_Edit.MaxLength = 50
            Me.Description_Edit.Name = "Description_Edit"
            Me.Description_Edit.ValidateOnEnterKey = True
            '
            'GridColumn_current_amount
            '
            Me.GridColumn_current_amount.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_current_amount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_current_amount.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_current_amount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_current_amount.Caption = "Current"
            Me.GridColumn_current_amount.ColumnEdit = Me.Current_Amount_Edit
            Me.GridColumn_current_amount.CustomizationCaption = "Current Amount"
            Me.GridColumn_current_amount.DisplayFormat.FormatString = "$#,##0.00;($#,##0.00); "
            Me.GridColumn_current_amount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_current_amount.FieldName = "client_amount"
            Me.GridColumn_current_amount.GroupFormat.FormatString = "$#,##0.00;($#,##0.00); "
            Me.GridColumn_current_amount.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_current_amount.Name = "GridColumn_current_amount"
            Me.GridColumn_current_amount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_current_amount.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowOnlyInEditor
            Me.GridColumn_current_amount.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "client_amount", "{0:c}")})
            Me.GridColumn_current_amount.Visible = True
            Me.GridColumn_current_amount.VisibleIndex = 1
            Me.GridColumn_current_amount.Width = 66
            '
            'Current_Amount_Edit
            '
            Me.Current_Amount_Edit.Appearance.Options.UseTextOptions = True
            Me.Current_Amount_Edit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.Current_Amount_Edit.AutoHeight = False
            Me.Current_Amount_Edit.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.Current_Amount_Edit.DisplayFormat.FormatString = "$#,##0.00;($#,##0.00); "
            Me.Current_Amount_Edit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.Current_Amount_Edit.EditFormat.FormatString = "$#,##0.00;($#,##0.00); "
            Me.Current_Amount_Edit.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.Current_Amount_Edit.Mask.BeepOnError = True
            Me.Current_Amount_Edit.Mask.EditMask = "c"
            Me.Current_Amount_Edit.Mask.SaveLiteral = False
            Me.Current_Amount_Edit.Mask.UseMaskAsDisplayFormat = True
            Me.Current_Amount_Edit.Name = "Current_Amount_Edit"
            Me.Current_Amount_Edit.ValidateOnEnterKey = True
            '
            'GridColumn_suggested
            '
            Me.GridColumn_suggested.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_suggested.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_suggested.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_suggested.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_suggested.Caption = "Suggested"
            Me.GridColumn_suggested.ColumnEdit = Me.Suggested_Amount_Edit
            Me.GridColumn_suggested.CustomizationCaption = "Suggested Amount"
            Me.GridColumn_suggested.DisplayFormat.FormatString = "c"
            Me.GridColumn_suggested.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_suggested.FieldName = "suggested_amount"
            Me.GridColumn_suggested.GroupFormat.FormatString = "c"
            Me.GridColumn_suggested.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_suggested.Name = "GridColumn_suggested"
            Me.GridColumn_suggested.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_suggested.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowOnlyInEditor
            Me.GridColumn_suggested.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "suggested_amount", "{0:c}")})
            Me.GridColumn_suggested.Visible = True
            Me.GridColumn_suggested.VisibleIndex = 2
            Me.GridColumn_suggested.Width = 73
            '
            'Suggested_Amount_Edit
            '
            Me.Suggested_Amount_Edit.Appearance.Options.UseTextOptions = True
            Me.Suggested_Amount_Edit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.Suggested_Amount_Edit.AutoHeight = False
            Me.Suggested_Amount_Edit.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.Suggested_Amount_Edit.DisplayFormat.FormatString = "c"
            Me.Suggested_Amount_Edit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.Suggested_Amount_Edit.EditFormat.FormatString = "c"
            Me.Suggested_Amount_Edit.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.Suggested_Amount_Edit.Mask.BeepOnError = True
            Me.Suggested_Amount_Edit.Mask.EditMask = "c"
            Me.Suggested_Amount_Edit.Name = "Suggested_Amount_Edit"
            Me.Suggested_Amount_Edit.ValidateOnEnterKey = True
            '
            'BudgetDetailControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_OK)
            Me.Controls.Add(Me.GridControl1)
            Me.Name = "BudgetDetailControl"
            Me.Size = New System.Drawing.Size(505, 329)
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Description_Edit, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Current_Amount_Edit, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Suggested_Amount_Edit, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents GridColumn_budget_category As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_auto As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_person As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_maximum As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_heading As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_detail As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_housing As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_living As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_description As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents Description_Edit As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
        Friend WithEvents GridColumn_current_amount As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents Current_Amount_Edit As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Friend WithEvents GridColumn_suggested As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents Suggested_Amount_Edit As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Friend WithEvents GridColumn_difference As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents ContextMenu1 As System.Windows.Forms.ContextMenu
        Friend WithEvents MenuItem_Delete As System.Windows.Forms.MenuItem
        Friend WithEvents MenuItem_Add As System.Windows.Forms.MenuItem
        Friend WithEvents MenuItem_Edit As System.Windows.Forms.MenuItem
    End Class
End Namespace