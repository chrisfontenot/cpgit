Namespace forms.Budgets
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class BudgetReport

        'XtraReport overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Designer
        'It can be modified using the Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim XrSummary1 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary2 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim XrSummary3 As DevExpress.XtraReports.UI.XRSummary = New DevExpress.XtraReports.UI.XRSummary()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(BudgetReport))
            Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
            Me.XrLabel_total_difference = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_suggested_amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
            Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_total_client_amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_suggested_amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_client_amount = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_detail = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_difference = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
            Me.XrLabel_client = New DevExpress.XtraReports.UI.XRLabel()
            Me.ParameterBudget = New DevExpress.XtraReports.Parameters.Parameter()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'PageHeader
            '
            Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel1, Me.XrLabel2, Me.XrLabel3, Me.XrLabel4})
            Me.PageHeader.HeightF = 146.8334!
            Me.PageHeader.Controls.SetChildIndex(Me.XrPanel_AgencyAddress, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLine_Title, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel_Subtitle, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrPageInfo1, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel4, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel3, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel2, 0)
            Me.PageHeader.Controls.SetChildIndex(Me.XrLabel1, 0)
            '
            'PageFooter
            '
            Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel9, Me.XrLabel_client})
            Me.PageFooter.HeightF = 25.0!
            Me.PageFooter.Controls.SetChildIndex(Me.XrLabel_client, 0)
            Me.PageFooter.Controls.SetChildIndex(Me.XrLabel9, 0)
            Me.PageFooter.Controls.SetChildIndex(Me.XrPageInfo_PageNumber, 0)
            '
            'XrLabel_Title
            '
            Me.XrLabel_Title.StylePriority.UseFont = False
            '
            'XrPageInfo_PageNumber
            '
            Me.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = False
            '
            'XRLabel_Agency_Name
            '
            Me.XRLabel_Agency_Name.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address3
            '
            Me.XrLabel_Agency_Address3.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address1
            '
            Me.XrLabel_Agency_Address1.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Phone
            '
            Me.XrLabel_Agency_Phone.StylePriority.UseFont = False
            Me.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = False
            '
            'XrLabel_Agency_Address2
            '
            Me.XrLabel_Agency_Address2.StylePriority.UseFont = False
            Me.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = False
            '
            'Detail
            '
            Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_difference, Me.XrLabel_detail, Me.XrLabel_client_amount, Me.XrLabel_suggested_amount})
            Me.Detail.HeightF = 17.0!
            '
            'BottomMarginBand1
            '
            Me.BottomMarginBand1.HeightF = 26.79164!
            '
            'ReportFooter
            '
            Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel_total_difference, Me.XrLabel_total_suggested_amount, Me.XrLine1, Me.XrLabel7, Me.XrLabel_total_client_amount})
            Me.ReportFooter.HeightF = 27.00001!
            Me.ReportFooter.Name = "ReportFooter"
            '
            'XrLabel_total_difference
            '
            Me.XrLabel_total_difference.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "difference")})
            Me.XrLabel_total_difference.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_difference.LocationFloat = New DevExpress.Utils.PointFloat(534.75!, 10.00001!)
            Me.XrLabel_total_difference.Name = "XrLabel_total_difference"
            Me.XrLabel_total_difference.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_difference.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
            Me.XrLabel_total_difference.StylePriority.UseFont = False
            Me.XrLabel_total_difference.StylePriority.UseTextAlignment = False
            XrSummary1.FormatString = "{0:c}"
            XrSummary1.IgnoreNullValues = True
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_difference.Summary = XrSummary1
            Me.XrLabel_total_difference.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_total_suggested_amount
            '
            Me.XrLabel_total_suggested_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "suggested_amount")})
            Me.XrLabel_total_suggested_amount.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_suggested_amount.LocationFloat = New DevExpress.Utils.PointFloat(397.25!, 10.00001!)
            Me.XrLabel_total_suggested_amount.Name = "XrLabel_total_suggested_amount"
            Me.XrLabel_total_suggested_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_suggested_amount.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
            Me.XrLabel_total_suggested_amount.StylePriority.UseFont = False
            Me.XrLabel_total_suggested_amount.StylePriority.UseTextAlignment = False
            XrSummary2.FormatString = "{0:c}"
            XrSummary2.IgnoreNullValues = True
            XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_suggested_amount.Summary = XrSummary2
            Me.XrLabel_total_suggested_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLine1
            '
            Me.XrLine1.ForeColor = System.Drawing.Color.Teal
            Me.XrLine1.LineWidth = 2
            Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLine1.Name = "XrLine1"
            Me.XrLine1.SizeF = New System.Drawing.SizeF(659.75!, 9.000015!)
            Me.XrLine1.StylePriority.UseForeColor = False
            '
            'XrLabel7
            '
            Me.XrLabel7.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 10.00001!)
            Me.XrLabel7.Name = "XrLabel7"
            Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel7.SizeF = New System.Drawing.SizeF(268.0!, 17.0!)
            Me.XrLabel7.StylePriority.UseFont = False
            Me.XrLabel7.Text = "MONTHLY TOTAL"
            '
            'XrLabel_total_client_amount
            '
            Me.XrLabel_total_client_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client_amount")})
            Me.XrLabel_total_client_amount.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_total_client_amount.LocationFloat = New DevExpress.Utils.PointFloat(293.75!, 10.00001!)
            Me.XrLabel_total_client_amount.Name = "XrLabel_total_client_amount"
            Me.XrLabel_total_client_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_total_client_amount.SizeF = New System.Drawing.SizeF(91.0!, 17.0!)
            Me.XrLabel_total_client_amount.StylePriority.UseFont = False
            Me.XrLabel_total_client_amount.StylePriority.UseTextAlignment = False
            XrSummary3.FormatString = "{0:c}"
            XrSummary3.IgnoreNullValues = True
            XrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report
            Me.XrLabel_total_client_amount.Summary = XrSummary3
            Me.XrLabel_total_client_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel4
            '
            Me.XrLabel4.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
            Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(397.25!, 124.0!)
            Me.XrLabel4.Name = "XrLabel4"
            Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel4.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
            Me.XrLabel4.StylePriority.UseFont = False
            Me.XrLabel4.StylePriority.UseTextAlignment = False
            Me.XrLabel4.Text = "SUGGESTED"
            Me.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel3
            '
            Me.XrLabel3.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
            Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(293.75!, 124.0!)
            Me.XrLabel3.Name = "XrLabel3"
            Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel3.SizeF = New System.Drawing.SizeF(91.0!, 17.0!)
            Me.XrLabel3.StylePriority.UseFont = False
            Me.XrLabel3.StylePriority.UseTextAlignment = False
            Me.XrLabel3.Text = "Client"
            Me.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel2
            '
            Me.XrLabel2.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
            Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 124.0!)
            Me.XrLabel2.Name = "XrLabel2"
            Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel2.SizeF = New System.Drawing.SizeF(100.0!, 17.0!)
            Me.XrLabel2.StylePriority.UseFont = False
            Me.XrLabel2.Text = "CATEGORY"
            '
            'XrLabel_suggested_amount
            '
            Me.XrLabel_suggested_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "suggested_amount", "{0:c}")})
            Me.XrLabel_suggested_amount.LocationFloat = New DevExpress.Utils.PointFloat(397.25!, 0.0!)
            Me.XrLabel_suggested_amount.Name = "XrLabel_suggested_amount"
            Me.XrLabel_suggested_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_suggested_amount.Scripts.OnBeforePrint = "XrLabel_suggested_amount_BeforePrint"
            Me.XrLabel_suggested_amount.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
            Me.XrLabel_suggested_amount.StylePriority.UseTextAlignment = False
            Me.XrLabel_suggested_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_client_amount
            '
            Me.XrLabel_client_amount.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "client_amount", "{0:c}")})
            Me.XrLabel_client_amount.LocationFloat = New DevExpress.Utils.PointFloat(293.75!, 0.0!)
            Me.XrLabel_client_amount.Name = "XrLabel_client_amount"
            Me.XrLabel_client_amount.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client_amount.Scripts.OnBeforePrint = "XrLabel_client_amount_BeforePrint"
            Me.XrLabel_client_amount.SizeF = New System.Drawing.SizeF(91.0!, 17.0!)
            Me.XrLabel_client_amount.StylePriority.UseTextAlignment = False
            Me.XrLabel_client_amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_detail
            '
            Me.XrLabel_detail.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "description")})
            Me.XrLabel_detail.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 0.0!)
            Me.XrLabel_detail.Name = "XrLabel_detail"
            Me.XrLabel_detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_detail.Scripts.OnBeforePrint = "XrLabel_detail_BeforePrint"
            Me.XrLabel_detail.SizeF = New System.Drawing.SizeF(268.0!, 17.0!)
            '
            'XrLabel1
            '
            Me.XrLabel1.Font = New System.Drawing.Font("Arial", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
            Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(534.75!, 124.0!)
            Me.XrLabel1.Name = "XrLabel1"
            Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel1.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
            Me.XrLabel1.StylePriority.UseFont = False
            Me.XrLabel1.StylePriority.UseTextAlignment = False
            Me.XrLabel1.Text = "DIFFERENCE"
            Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel_difference
            '
            Me.XrLabel_difference.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "difference", "{0:c}")})
            Me.XrLabel_difference.LocationFloat = New DevExpress.Utils.PointFloat(534.75!, 0.0!)
            Me.XrLabel_difference.Name = "XrLabel_difference"
            Me.XrLabel_difference.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_difference.Scripts.OnBeforePrint = "XrLabel_difference_BeforePrint"
            Me.XrLabel_difference.SizeF = New System.Drawing.SizeF(125.0!, 17.0!)
            Me.XrLabel_difference.StylePriority.UseTextAlignment = False
            Me.XrLabel_difference.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight
            '
            'XrLabel9
            '
            Me.XrLabel9.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel9.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 6.666628!)
            Me.XrLabel9.Name = "XrLabel9"
            Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel9.SizeF = New System.Drawing.SizeF(100.0!, 18.33337!)
            Me.XrLabel9.StylePriority.UseFont = False
            Me.XrLabel9.StylePriority.UseForeColor = False
            Me.XrLabel9.StylePriority.UseTextAlignment = False
            Me.XrLabel9.Text = "Rev. 02/2011"
            Me.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
            Me.XrLabel9.Visible = False
            '
            'XrLabel_client
            '
            Me.XrLabel_client.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold)
            Me.XrLabel_client.ForeColor = System.Drawing.Color.Teal
            Me.XrLabel_client.LocationFloat = New DevExpress.Utils.PointFloat(100.0!, 6.666628!)
            Me.XrLabel_client.Name = "XrLabel_client"
            Me.XrLabel_client.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
            Me.XrLabel_client.Scripts.OnBeforePrint = "XrLabel_Client_BeforePrint"
            Me.XrLabel_client.SizeF = New System.Drawing.SizeF(508.0001!, 18.33337!)
            Me.XrLabel_client.StylePriority.UseFont = False
            Me.XrLabel_client.StylePriority.UseForeColor = False
            Me.XrLabel_client.StylePriority.UseTextAlignment = False
            Me.XrLabel_client.Text = "Client: 0000000"
            Me.XrLabel_client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
            '
            'ParameterBudget
            '
            Me.ParameterBudget.Description = "Budget ID"
            Me.ParameterBudget.Name = "ParameterBudget"
            Me.ParameterBudget.Type = GetType(Int32)
            Me.ParameterBudget.Visible = False
            '
            'BudgetReport
            '
            Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageHeader, Me.ReportFooter, Me.PageFooter, Me.TopMarginBand1, Me.BottomMarginBand1})
            Me.Margins = New System.Drawing.Printing.Margins(25, 25, 25, 27)
            Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.ParameterBudget})
            Me.RequestParameters = False
            Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
            Me.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString")
            Me.Scripts.OnBeforePrint = "BudgetSubReport_BeforePrint"
            Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle_Header, Me.XrControlStyle_Totals, Me.XrControlStyle_HeaderPannel, Me.XrControlStyle_GroupHeader})
            Me.Version = "11.2"
            Me.Controls.SetChildIndex(Me.BottomMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.TopMarginBand1, 0)
            Me.Controls.SetChildIndex(Me.PageFooter, 0)
            Me.Controls.SetChildIndex(Me.ReportFooter, 0)
            Me.Controls.SetChildIndex(Me.PageHeader, 0)
            Me.Controls.SetChildIndex(Me.Detail, 0)
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Protected Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
        Protected Friend WithEvents XrLabel_total_suggested_amount As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
        Protected Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_total_client_amount As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_detail As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_client_amount As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_suggested_amount As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_difference As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_total_difference As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents XrLabel_client As DevExpress.XtraReports.UI.XRLabel
        Protected Friend WithEvents ParameterBudget As DevExpress.XtraReports.Parameters.Parameter
    End Class
End Namespace
