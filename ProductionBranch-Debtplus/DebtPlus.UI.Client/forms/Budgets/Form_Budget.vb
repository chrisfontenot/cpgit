#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.UI.Client.Service
Imports DebtPlus.Utils
Imports DevExpress.XtraBars
Imports System.Data.SqlClient

Namespace forms.Budgets

    Friend Class Form_Budget
        Implements IContext

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Friend WithEvents BudgetDetailControl1 As BudgetDetailControl
        Private ReadOnly drv As DataRowView

        ''' <summary>
        ''' Initialize the form
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Load, AddressOf Form_Budget_Load
            AddHandler BarButtonItem_File_Print.ItemClick, AddressOf BarButtonItem_File_Print_ItemClick
            AddHandler BudgetDetailControl1.Completed, AddressOf BudgetDetailControl1_Completed
            AddHandler BudgetDetailControl1.Cancelled, AddressOf BudgetDetailControl1_Cancelled
        End Sub

        Public Sub New(ByVal Context As ClientUpdateClass)
            MyClass.New()
            privateContext = Context
        End Sub

        Public Sub New(ByVal Context As ClientUpdateClass, ByVal drv As DataRowView)
            MyClass.New(Context)
            Me.drv = drv
        End Sub

        ''' <summary>
        ''' Pointer to the client update information for the current client
        ''' </summary>
        Private ReadOnly privateContext As ClientUpdateClass

        Public ReadOnly Property Context() As ClientUpdateClass Implements IContext.Context
            Get
                Return privateContext
            End Get
        End Property

        ''' <summary>
        ''' Load the form
        ''' </summary>
        Private Sub Form_Budget_Load(ByVal sender As Object, ByVal e As EventArgs)

            ' Restore the form position
            LoadPlacement("Client.Update.Budget")

            ' If the budget is new, find the last budget for the client
            Dim Budget As Int32

            ' If the budget is not new then try to find the budget id.
            If drv.IsNew Then
                Budget = -1
            Else
                Budget = DebtPlus.Utils.Nulls.DInt(drv("budget"), -1)
            End If

            ' If the budget is not defined then try to find the last budget for the client.
            If Budget <= 0 Then
                Dim tbl As DataTable = Context.ClientDs.BudgetsTable
                Using vue As New DataView(tbl, "[budget]>0", "date_created desc", DataViewRowState.CurrentRows)
                    If vue.Count > 0 Then
                        Budget = DebtPlus.Utils.Nulls.DInt(vue(0)("budget"), -1)
                    End If
                End Using
            End If

            ' Read the detail for the budget
            BudgetDetailControl1.ReadForm(Budget)
        End Sub

        Private Sub BudgetDetailControl1_Cancelled(ByVal sender As Object, ByVal e As EventArgs)
            DialogResult = DialogResult.Cancel
        End Sub

        Private Sub BudgetDetailControl1_Completed(ByVal sender As Object, ByVal e As EventArgs)

            Try
                Dim Budget As Int32
                Dim NewBudget As Boolean = False

                If drv.IsNew Then
                    Budget = -1
                Else
                    Budget = DebtPlus.Utils.Nulls.DInt(drv("budget"), -1)
                End If

                ' Indicate that the budget data needs is not changed and just complete the dialog
                DialogResult = DialogResult.Cancel

                ' Create a new budget for the data
                If Budget <= 0 Then
                    NewBudget = True
                    Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)

                    Try
                        cn.Open()
                        Using cmd As SqlCommand = New SqlCommand
                            With cmd
                                .Connection = cn
                                .CommandText = "xpr_insert_budgets"
                                .CommandType = CommandType.StoredProcedure
                                .Parameters.Add("RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                                .Parameters.Add("@client", SqlDbType.Int).Value = Context.ClientId
                                .Parameters.Add("@type", SqlDbType.VarChar, 10).Value = "F"
                                .ExecuteNonQuery()
                                Budget = DebtPlus.Utils.Nulls.DInt(.Parameters(0).Value, -1)
                            End With
                        End Using

                        ' Insert the budget information into the budget row
                        drv("budget") = Budget
                        drv("date_created") = Now.Date
                        drv("created_by") = "Me"

                        ' Indicate that the budget data needs to be reloaded
                        DialogResult = DialogResult.OK

                    Catch ex As System.Data.SqlClient.SqlException
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error creating the budget")

                    Finally
                        If cn IsNot Nothing Then cn.Dispose()
                    End Try
                End If

                ' Save the budget information to the database. This will record all of the detail information.
                If Budget > 0 Then
                    BudgetDetailControl1.SaveForm(Budget, NewBudget)
                End If

            Catch ex As Exception
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error trying to save budget data")
            End Try
        End Sub

        Private Sub BarButtonItem_File_Print_ItemClick(sender As Object, e As ItemClickEventArgs)
            BudgetDetailControl1.PrintReport()
        End Sub
    End Class
End Namespace
