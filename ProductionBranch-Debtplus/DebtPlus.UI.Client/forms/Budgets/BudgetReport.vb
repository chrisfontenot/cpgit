#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DebtPlus.Reports.Template
Imports DevExpress.XtraGrid
Imports System.Drawing.Printing
Imports DebtPlus.Utils

Namespace forms.Budgets

    Public Class BudgetReport
        Inherits TemplateXtraReportClass

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Public Overrides ReadOnly Property ReportTitle As String
            Get
                Return "Budget Information"
            End Get
        End Property

        Private ReadOnly _grid As GridControl
        Private ReadOnly _clientId As Int32

        Public Sub New(ByVal clientId As Int32, ByVal grid As GridControl)
            MyBase.New()
            InitializeComponent()
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies()

            AddHandler BeforePrint, AddressOf BudgetReport_BeforePrint
            AddHandler XrLabel_client_amount.BeforePrint, AddressOf XrLabel_client_amount_BeforePrint
            AddHandler XrLabel_suggested_amount.BeforePrint, AddressOf XrLabel_suggested_amount_BeforePrint
            AddHandler XrLabel_detail.BeforePrint, AddressOf XrLabel_detail_BeforePrint
            AddHandler XrLabel_difference.BeforePrint, AddressOf XrLabel_difference_BeforePrint
            AddHandler XrLabel_client.BeforePrint, AddressOf XrLabel_client_BeforePrint

            _clientId = clientId
            _grid = grid
        End Sub

        Private Sub BudgetReport_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            Dim vue As DataView = CType(_grid.DataSource, DataView)
            DataSource = New DataView(vue.Table, vue.RowFilter, "budget_category", vue.RowStateFilter)
        End Sub

        Private Sub XrLabel_client_amount_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim Detail As Boolean = Convert.ToInt32(.Report.GetCurrentColumnValue("detail")) <> 0
                e.Cancel = Not Detail
            End With
        End Sub

        Private Sub XrLabel_suggested_amount_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim Detail As Boolean = Convert.ToInt32(.Report.GetCurrentColumnValue("detail")) <> 0
                e.Cancel = Not Detail
            End With
        End Sub

        Private Sub XrLabel_difference_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim Detail As Boolean = Convert.ToInt32(.Report.GetCurrentColumnValue("detail")) <> 0
                e.Cancel = Not Detail
            End With
        End Sub

        Private Sub XrLabel_detail_BeforePrint(ByVal sender As Object, ByVal e As PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                Dim Heading As Boolean = Convert.ToInt32(.Report.GetCurrentColumnValue("heading")) <> 0
                If Heading Then
                    .BackColor = Color.Red
                    .ForeColor = Color.White
                    .Font = New Font(.Font, FontStyle.Bold)
                Else
                    .BackColor = Color.Transparent
                    .ForeColor = Color.FromKnownColor(KnownColor.ControlText)
                    .Font = New Font(.Font, FontStyle.Regular)
                End If
            End With
        End Sub

        Private Sub XrLabel_client_BeforePrint(sender As Object, e As PrintEventArgs)
            With CType(sender, DevExpress.XtraReports.UI.XRLabel)
                .Text = String.Format("Client: {0:0000000}", _clientId)
            End With
        End Sub
    End Class
End Namespace
