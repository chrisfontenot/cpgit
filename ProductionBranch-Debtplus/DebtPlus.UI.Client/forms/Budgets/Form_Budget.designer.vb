﻿Namespace forms.Budgets

    Partial Class Form_Budget
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Me.BudgetDetailControl1 = New BudgetDetailControl()
            Me.barManager1 = New DevExpress.XtraBars.BarManager(Me.components)
            Me.Bar2 = New DevExpress.XtraBars.Bar()
            Me.BarSubItem1 = New DevExpress.XtraBars.BarSubItem()
            Me.BarButtonItem_File_Print = New DevExpress.XtraBars.BarButtonItem()
            Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.BudgetDetailControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'BudgetDetailControl1
            '
            Me.BudgetDetailControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.BudgetDetailControl1.Location = New System.Drawing.Point(0, 24)
            Me.BudgetDetailControl1.Name = "BudgetDetailControl1"
            Me.BudgetDetailControl1.Size = New System.Drawing.Size(512, 286)
            Me.BudgetDetailControl1.TabIndex = 0
            '
            'barManager1
            '
            Me.barManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar2})
            Me.barManager1.DockControls.Add(Me.barDockControlTop)
            Me.barManager1.DockControls.Add(Me.barDockControlBottom)
            Me.barManager1.DockControls.Add(Me.barDockControlLeft)
            Me.barManager1.DockControls.Add(Me.barDockControlRight)
            Me.barManager1.Form = Me
            Me.barManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarSubItem1, Me.BarButtonItem_File_Print})
            Me.barManager1.MainMenu = Me.Bar2
            Me.barManager1.MaxItemId = 2
            '
            'Bar2
            '
            Me.Bar2.BarName = "Main menu"
            Me.Bar2.DockCol = 0
            Me.Bar2.DockRow = 0
            Me.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
            Me.Bar2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem1)})
            Me.Bar2.OptionsBar.MultiLine = True
            Me.Bar2.OptionsBar.UseWholeRow = True
            Me.Bar2.Text = "Main menu"
            '
            'BarSubItem1
            '
            Me.BarSubItem1.Caption = "&File"
            Me.BarSubItem1.Id = 0
            Me.BarSubItem1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_File_Print)})
            Me.BarSubItem1.Name = "BarSubItem1"
            '
            'BarButtonItem_File_Print
            '
            Me.BarButtonItem_File_Print.Caption = "&Print..."
            Me.BarButtonItem_File_Print.Id = 1
            Me.BarButtonItem_File_Print.Name = "BarButtonItem_File_Print"
            '
            'barDockControlTop
            '
            Me.barDockControlTop.CausesValidation = False
            Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
            Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
            Me.barDockControlTop.Size = New System.Drawing.Size(512, 24)
            '
            'barDockControlBottom
            '
            Me.barDockControlBottom.CausesValidation = False
            Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
            Me.barDockControlBottom.Location = New System.Drawing.Point(0, 310)
            Me.barDockControlBottom.Size = New System.Drawing.Size(512, 0)
            '
            'barDockControlLeft
            '
            Me.barDockControlLeft.CausesValidation = False
            Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
            Me.barDockControlLeft.Location = New System.Drawing.Point(0, 24)
            Me.barDockControlLeft.Size = New System.Drawing.Size(0, 286)
            '
            'barDockControlRight
            '
            Me.barDockControlRight.CausesValidation = False
            Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
            Me.barDockControlRight.Location = New System.Drawing.Point(512, 24)
            Me.barDockControlRight.Size = New System.Drawing.Size(0, 286)
            '
            'Form_Budget
            '
            Me.ClientSize = New System.Drawing.Size(512, 310)
            Me.Controls.Add(Me.BudgetDetailControl1)
            Me.Controls.Add(Me.barDockControlLeft)
            Me.Controls.Add(Me.barDockControlRight)
            Me.Controls.Add(Me.barDockControlBottom)
            Me.Controls.Add(Me.barDockControlTop)
            Me.Name = "Form_Budget"
            Me.Text = "Client Budget Information"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.BudgetDetailControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents barManager1 As DevExpress.XtraBars.BarManager
        Friend WithEvents Bar2 As DevExpress.XtraBars.Bar
        Friend WithEvents BarSubItem1 As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarButtonItem_File_Print As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    End Class
End Namespace