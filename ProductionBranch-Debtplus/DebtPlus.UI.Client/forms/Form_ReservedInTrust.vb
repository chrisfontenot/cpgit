#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DebtPlus.Svc.DataSets
Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.UI.Client.Service
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports DebtPlus.Events

Namespace forms

    Friend Class Form_ReservedInTrust
        Implements IContext

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        ''' <summary>
        ''' Initialize the form
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(ByVal Context As ClientUpdateClass)
            MyClass.New()
            privateContext = Context
            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf Form_ReservedInTrust_Load
            AddHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
            AddHandler DateEdit1.EditValueChanging, AddressOf DateEdit1_EditValueChanging
            AddHandler CalcEdit1.EditValueChanging, AddressOf CalcEdit1_EditValueChanging
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler Load, AddressOf Form_ReservedInTrust_Load
            RemoveHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
            RemoveHandler DateEdit1.EditValueChanging, AddressOf DateEdit1_EditValueChanging
            RemoveHandler CalcEdit1.EditValueChanging, AddressOf CalcEdit1_EditValueChanging
        End Sub

        ''' <summary>
        ''' Pointer to the client update information for the current client
        ''' </summary>
        Private ReadOnly privateContext As ClientUpdateClass

        Public ReadOnly Property Context() As ClientUpdateClass Implements IContext.Context
            Get
                Return privateContext
            End Get
        End Property

        ''' <summary>
        ''' Process the One-Time load event
        ''' </summary>
        Private Sub Form_ReservedInTrust_Load(ByVal sender As Object, ByVal e As EventArgs)

            UnRegisterHandlers()
            Try
                ' If there is no cutoff amount or the cutoff date has expired then there is no cutoff figure.
                If Context.ClientDs.clientRecord.reserved_in_trust <= 0D OrElse (Not Context.ClientDs.clientRecord.reserved_in_trust_cutoff.HasValue) OrElse Context.ClientDs.clientRecord.reserved_in_trust_cutoff.Value.Date <= System.DateTime.Now.Date Then
                    DateEdit1.EditValue = Nothing
                    CalcEdit1.EditValue = 0D
                Else
                    DateEdit1.EditValue = Context.ClientDs.clientRecord.reserved_in_trust_cutoff
                    CalcEdit1.EditValue = Context.ClientDs.clientRecord.reserved_in_trust
                End If

            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Do not allow the date to go before today
        ''' </summary>
        Private Sub DateEdit1_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)

            ' The date must be at least tomorrow to be valid. It can not be earlier than that.
            Dim newDate As DateTime? = DebtPlus.Utils.Nulls.v_DateTime(e.NewValue)
            If newDate.HasValue AndAlso newDate.Value.Date < System.DateTime.Now.Date.AddDays(1) Then
                e.Cancel = True
            End If

        End Sub

        ''' <summary>
        ''' Do not allow the amount to go negative or more than the trust
        ''' </summary>
        Private Sub CalcEdit1_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)
            Dim Value As Decimal = DebtPlus.Utils.Nulls.v_Decimal(e.NewValue).GetValueOrDefault(0D)

            ' The value may not be negative
            If Value < 0D Then
                e.Cancel = True
                Return
            End If

            ' You are only allowed to reserve to the current trust balance
            If Context.ClientDs.clientRecord.held_in_trust < 0D OrElse Value > Context.ClientDs.clientRecord.held_in_trust Then
                e.Cancel = True
                Return
            End If
        End Sub

        ''' <summary>
        ''' Update the reserved amount
        ''' </summary>
        Private Sub SimpleButton_OK_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim newReserved As Decimal = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit1.EditValue).GetValueOrDefault(0D)
            Dim newReservedDate As System.Nullable(Of System.DateTime) = DebtPlus.Utils.Nulls.v_DateTime(DateEdit1.EditValue)

            ' If there are no reserved figures then clear the information
            If newReserved <= 0D OrElse Not newReservedDate.HasValue Then
                newReserved = 0D
                newReservedDate = Nothing
            End If

            ' If the values are not changed then simply leave now.
            If newReserved = Context.ClientDs.clientRecord.reserved_in_trust AndAlso System.Nullable.Compare(Of DateTime)(newReservedDate, Context.ClientDs.clientRecord.reserved_in_trust_cutoff) = 0 Then
                Return
            End If

            ' Set the values into the client record
            Context.ClientDs.clientRecord.reserved_in_trust = newReserved
            Context.ClientDs.clientRecord.reserved_in_trust_cutoff = newReservedDate

            Try
                ' Call the stored procedure to update the client information as needed when the values are changed.
                Context.ClientDs.bc.xpr_client_reserved_in_trust_update(Context.ClientDs.ClientId, Context.ClientDs.clientRecord.reserved_in_trust, Context.ClientDs.clientRecord.reserved_in_trust_cutoff)

            Catch ex As Exception
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error setting reserved in trust figures")
            End Try
        End Sub
    End Class
End Namespace
