﻿Namespace forms
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form_Product

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then components.Dispose()
                End If
                components = Nothing
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer = Nothing

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Me.Bar2 = New DevExpress.XtraBars.Bar()
            Me.BarSubItem_Menu_File = New DevExpress.XtraBars.BarSubItem()
            Me.BarButtonItem_FileExit = New DevExpress.XtraBars.BarButtonItem()
            Me.BarSubItem_Menu_View = New DevExpress.XtraBars.BarSubItem()
            Me.BarButtonItemViewAccount = New DevExpress.XtraBars.BarCheckItem()
            Me.BarButtonItemViewNotes = New DevExpress.XtraBars.BarCheckItem()
            Me.BarSubItem_Menu_Reports = New DevExpress.XtraBars.BarSubItem()
            Me.Bar3 = New DevExpress.XtraBars.Bar()
            Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
            Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
            Me.EmptySpaceItem_Left = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem_Right = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.SimpleButton_Cancel = New DevExpress.XtraEditors.SimpleButton()
            Me.ProductsHeader1 = New DebtPlus.UI.Client.controls.ProductsHeader()
            Me.SimpleButton_OK = New DevExpress.XtraEditors.SimpleButton()
            Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
            Me.XtraTabPage_Account = New DevExpress.XtraTab.XtraTabPage()
            Me.ProductsAccount1 = New DebtPlus.UI.Client.controls.ProductsAccount()
            Me.XtraTabPage_Notes = New DevExpress.XtraTab.XtraTabPage()
            Me.ProductsNotes1 = New DebtPlus.UI.Client.controls.ProductsNotes()
            Me.XtraTabPage_Transactions = New DevExpress.XtraTab.XtraTabPage()
            Me.ProductsTransacitons1 = New DebtPlus.UI.Client.controls.ProductsTransacitons()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.BarCheckItemViewTransactions = New DevExpress.XtraBars.BarCheckItem()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem_Left, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem_Right, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.XtraTabControl1.SuspendLayout()
            Me.XtraTabPage_Account.SuspendLayout()
            Me.XtraTabPage_Notes.SuspendLayout()
            Me.XtraTabPage_Transactions.SuspendLayout()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'DefaultLookAndFeel1
            '
            Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin"
            '
            'Bar2
            '
            Me.Bar2.BarName = "Main menu"
            Me.Bar2.DockCol = 0
            Me.Bar2.DockRow = 0
            Me.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
            Me.Bar2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem_Menu_File), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem_Menu_View), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem_Menu_Reports)})
            Me.Bar2.OptionsBar.MultiLine = True
            Me.Bar2.OptionsBar.UseWholeRow = True
            Me.Bar2.Text = "Main menu"
            '
            'BarSubItem_Menu_File
            '
            Me.BarSubItem_Menu_File.Caption = "&File"
            Me.BarSubItem_Menu_File.Id = 0
            Me.BarSubItem_Menu_File.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_FileExit)})
            Me.BarSubItem_Menu_File.Name = "BarSubItem_Menu_File"
            '
            'BarButtonItem_FileExit
            '
            Me.BarButtonItem_FileExit.Caption = "&Exit"
            Me.BarButtonItem_FileExit.Id = 3
            Me.BarButtonItem_FileExit.Name = "BarButtonItem_FileExit"
            '
            'BarSubItem_Menu_View
            '
            Me.BarSubItem_Menu_View.Caption = "&View"
            Me.BarSubItem_Menu_View.Id = 1
            Me.BarSubItem_Menu_View.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemViewAccount), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemViewNotes), New DevExpress.XtraBars.LinkPersistInfo(Me.BarCheckItemViewTransactions)})
            Me.BarSubItem_Menu_View.Name = "BarSubItem_Menu_View"
            '
            'BarButtonItemViewAccount
            '
            Me.BarButtonItemViewAccount.Caption = "&Account"
            Me.BarButtonItemViewAccount.Id = 4
            Me.BarButtonItemViewAccount.Name = "BarButtonItemViewAccount"
            '
            'BarButtonItemViewNotes
            '
            Me.BarButtonItemViewNotes.Caption = "&Notes"
            Me.BarButtonItemViewNotes.Id = 5
            Me.BarButtonItemViewNotes.Name = "BarButtonItemViewNotes"
            '
            'BarSubItem_Menu_Reports
            '
            Me.BarSubItem_Menu_Reports.Caption = "&Reports"
            Me.BarSubItem_Menu_Reports.Id = 2
            Me.BarSubItem_Menu_Reports.Name = "BarSubItem_Menu_Reports"
            '
            'Bar3
            '
            Me.Bar3.BarName = "Status bar"
            Me.Bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom
            Me.Bar3.DockCol = 0
            Me.Bar3.DockRow = 0
            Me.Bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
            Me.Bar3.OptionsBar.AllowQuickCustomization = False
            Me.Bar3.OptionsBar.DrawDragBorder = False
            Me.Bar3.OptionsBar.UseWholeRow = True
            Me.Bar3.Text = "Status bar"
            '
            'barDockControlBottom
            '
            Me.barDockControlBottom.CausesValidation = False
            Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
            Me.barDockControlBottom.Location = New System.Drawing.Point(0, 435)
            Me.barDockControlBottom.Size = New System.Drawing.Size(554, 22)
            '
            'barDockControlLeft
            '
            Me.barDockControlLeft.CausesValidation = False
            Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
            Me.barDockControlLeft.Location = New System.Drawing.Point(0, 25)
            Me.barDockControlLeft.Size = New System.Drawing.Size(0, 410)
            '
            'barDockControlRight
            '
            Me.barDockControlRight.CausesValidation = False
            Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
            Me.barDockControlRight.Location = New System.Drawing.Point(554, 25)
            Me.barDockControlRight.Size = New System.Drawing.Size(0, 410)
            '
            'barDockControlTop
            '
            Me.barDockControlTop.CausesValidation = False
            Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
            Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
            Me.barDockControlTop.Size = New System.Drawing.Size(554, 25)
            '
            'BarManager1
            '
            Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar2, Me.Bar3})
            Me.BarManager1.DockControls.Add(Me.barDockControlTop)
            Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
            Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
            Me.BarManager1.DockControls.Add(Me.barDockControlRight)
            Me.BarManager1.Form = Me
            Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarSubItem_Menu_File, Me.BarSubItem_Menu_View, Me.BarSubItem_Menu_Reports, Me.BarButtonItem_FileExit, Me.BarButtonItemViewAccount, Me.BarButtonItemViewNotes, Me.BarCheckItemViewTransactions})
            Me.BarManager1.MainMenu = Me.Bar2
            Me.BarManager1.MaxItemId = 9
            Me.BarManager1.StatusBar = Me.Bar3
            '
            'EmptySpaceItem_Left
            '
            Me.EmptySpaceItem_Left.AllowHotTrack = False
            Me.EmptySpaceItem_Left.CustomizationFormText = "EmptySpaceItem_Left"
            Me.EmptySpaceItem_Left.Location = New System.Drawing.Point(0, 370)
            Me.EmptySpaceItem_Left.Name = "EmptySpaceItem_Left"
            Me.EmptySpaceItem_Left.Size = New System.Drawing.Size(181, 30)
            Me.EmptySpaceItem_Left.Text = "EmptySpaceItem_Left"
            Me.EmptySpaceItem_Left.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem_Right
            '
            Me.EmptySpaceItem_Right.AllowHotTrack = False
            Me.EmptySpaceItem_Right.CustomizationFormText = "EmptySpaceItem_Right"
            Me.EmptySpaceItem_Right.Location = New System.Drawing.Point(339, 370)
            Me.EmptySpaceItem_Right.Name = "EmptySpaceItem_Right"
            Me.EmptySpaceItem_Right.Size = New System.Drawing.Size(215, 30)
            Me.EmptySpaceItem_Right.Text = "EmptySpaceItem_Right"
            Me.EmptySpaceItem_Right.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem3
            '
            Me.EmptySpaceItem3.AllowHotTrack = False
            Me.EmptySpaceItem3.CustomizationFormText = "EmptySpaceItem3"
            Me.EmptySpaceItem3.Location = New System.Drawing.Point(0, 400)
            Me.EmptySpaceItem3.MaxSize = New System.Drawing.Size(0, 10)
            Me.EmptySpaceItem3.MinSize = New System.Drawing.Size(10, 10)
            Me.EmptySpaceItem3.Name = "EmptySpaceItem3"
            Me.EmptySpaceItem3.Size = New System.Drawing.Size(554, 10)
            Me.EmptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
            Me.EmptySpaceItem3.Text = "EmptySpaceItem3"
            Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.SimpleButton_Cancel)
            Me.LayoutControl1.Controls.Add(Me.ProductsHeader1)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton_OK)
            Me.LayoutControl1.Controls.Add(Me.XtraTabControl1)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 25)
            Me.LayoutControl1.Margin = New System.Windows.Forms.Padding(0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(554, 410)
            Me.LayoutControl1.TabIndex = 8
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'SimpleButton_Cancel
            '
            Me.SimpleButton_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.SimpleButton_Cancel.Location = New System.Drawing.Point(262, 372)
            Me.SimpleButton_Cancel.MaximumSize = New System.Drawing.Size(75, 26)
            Me.SimpleButton_Cancel.MinimumSize = New System.Drawing.Size(75, 26)
            Me.SimpleButton_Cancel.Name = "SimpleButton_Cancel"
            Me.SimpleButton_Cancel.Size = New System.Drawing.Size(75, 26)
            Me.SimpleButton_Cancel.StyleController = Me.LayoutControl1
            Me.SimpleButton_Cancel.TabIndex = 3
            Me.SimpleButton_Cancel.Text = "&Cancel"
            '
            'ProductsHeader1
            '
            Me.ProductsHeader1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ProductsHeader1.Location = New System.Drawing.Point(2, 2)
            Me.ProductsHeader1.Name = "ProductsHeader1"
            Me.ProductsHeader1.Size = New System.Drawing.Size(550, 88)
            Me.ProductsHeader1.TabIndex = 1
            '
            'SimpleButton_OK
            '
            Me.SimpleButton_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_OK.Location = New System.Drawing.Point(183, 372)
            Me.SimpleButton_OK.MaximumSize = New System.Drawing.Size(75, 26)
            Me.SimpleButton_OK.MinimumSize = New System.Drawing.Size(75, 26)
            Me.SimpleButton_OK.Name = "SimpleButton_OK"
            Me.SimpleButton_OK.Size = New System.Drawing.Size(75, 26)
            Me.SimpleButton_OK.StyleController = Me.LayoutControl1
            Me.SimpleButton_OK.TabIndex = 2
            Me.SimpleButton_OK.Text = "&OK"
            '
            'XtraTabControl1
            '
            Me.XtraTabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.XtraTabControl1.Location = New System.Drawing.Point(2, 94)
            Me.XtraTabControl1.Name = "XtraTabControl1"
            Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage_Account
            Me.XtraTabControl1.Size = New System.Drawing.Size(550, 274)
            Me.XtraTabControl1.TabIndex = 0
            Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage_Account, Me.XtraTabPage_Notes, Me.XtraTabPage_Transactions})
            '
            'XtraTabPage_Account
            '
            Me.XtraTabPage_Account.Controls.Add(Me.ProductsAccount1)
            Me.XtraTabPage_Account.Name = "XtraTabPage_Account"
            Me.XtraTabPage_Account.Size = New System.Drawing.Size(542, 244)
            Me.XtraTabPage_Account.Text = "Account"
            '
            'ProductsAccount1
            '
            Me.ProductsAccount1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.ProductsAccount1.Location = New System.Drawing.Point(0, 0)
            Me.ProductsAccount1.Name = "ProductsAccount1"
            Me.ProductsAccount1.Size = New System.Drawing.Size(542, 244)
            Me.ProductsAccount1.TabIndex = 0
            '
            'XtraTabPage_Notes
            '
            Me.XtraTabPage_Notes.Controls.Add(Me.ProductsNotes1)
            Me.XtraTabPage_Notes.Name = "XtraTabPage_Notes"
            Me.XtraTabPage_Notes.Size = New System.Drawing.Size(542, 244)
            Me.XtraTabPage_Notes.Text = "Notes"
            '
            'ProductsNotes1
            '
            Me.ProductsNotes1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.ProductsNotes1.Location = New System.Drawing.Point(0, 0)
            Me.ProductsNotes1.Name = "ProductsNotes1"
            Me.ProductsNotes1.Size = New System.Drawing.Size(542, 244)
            Me.ProductsNotes1.TabIndex = 0
            '
            'XtraTabPage_Transactions
            '
            Me.XtraTabPage_Transactions.Controls.Add(Me.ProductsTransacitons1)
            Me.XtraTabPage_Transactions.Name = "XtraTabPage_Transactions"
            Me.XtraTabPage_Transactions.Size = New System.Drawing.Size(542, 244)
            Me.XtraTabPage_Transactions.Text = "Transactions"
            '
            'ProductsTransacitons1
            '
            Me.ProductsTransacitons1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.ProductsTransacitons1.Location = New System.Drawing.Point(0, 0)
            Me.ProductsTransacitons1.Name = "ProductsTransacitons1"
            Me.ProductsTransacitons1.Size = New System.Drawing.Size(542, 244)
            Me.ProductsTransacitons1.TabIndex = 0
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup1.GroupBordersVisible = False
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.EmptySpaceItem_Left, Me.EmptySpaceItem_Right, Me.EmptySpaceItem3})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(554, 410)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.ProductsHeader1
            Me.LayoutControlItem1.CustomizationFormText = "LayoutControlItem1"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem1.MaxSize = New System.Drawing.Size(0, 92)
            Me.LayoutControlItem1.MinSize = New System.Drawing.Size(104, 92)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(554, 92)
            Me.LayoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
            Me.LayoutControlItem1.Text = "LayoutControlItem1"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem1.TextToControlDistance = 0
            Me.LayoutControlItem1.TextVisible = False
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.XtraTabControl1
            Me.LayoutControlItem2.CustomizationFormText = "LayoutControlItem2"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 92)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(554, 278)
            Me.LayoutControlItem2.Text = "LayoutControlItem2"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem2.TextToControlDistance = 0
            Me.LayoutControlItem2.TextVisible = False
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.SimpleButton_OK
            Me.LayoutControlItem3.CustomizationFormText = "LayoutControlItem3"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(181, 370)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(79, 30)
            Me.LayoutControlItem3.Text = "LayoutControlItem3"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem3.TextToControlDistance = 0
            Me.LayoutControlItem3.TextVisible = False
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.SimpleButton_Cancel
            Me.LayoutControlItem4.CustomizationFormText = "LayoutControlItem4"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(260, 370)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(79, 30)
            Me.LayoutControlItem4.Text = "LayoutControlItem4"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem4.TextToControlDistance = 0
            Me.LayoutControlItem4.TextVisible = False
            '
            'BarCheckItemViewTransactions
            '
            Me.BarCheckItemViewTransactions.Caption = "&Transactions"
            Me.BarCheckItemViewTransactions.Id = 8
            Me.BarCheckItemViewTransactions.Name = "BarCheckItemViewTransactions"
            '
            'Form_Product
            '
            Me.AcceptButton = Me.SimpleButton_OK
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.SimpleButton_Cancel
            Me.ClientSize = New System.Drawing.Size(554, 457)
            Me.Controls.Add(Me.LayoutControl1)
            Me.Controls.Add(Me.barDockControlLeft)
            Me.Controls.Add(Me.barDockControlRight)
            Me.Controls.Add(Me.barDockControlBottom)
            Me.Controls.Add(Me.barDockControlTop)
            Me.Name = "Form_Product"
            Me.Text = "Client Production Reference"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem_Left, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem_Right, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.XtraTabControl1.ResumeLayout(False)
            Me.XtraTabPage_Account.ResumeLayout(False)
            Me.XtraTabPage_Notes.ResumeLayout(False)
            Me.XtraTabPage_Transactions.ResumeLayout(False)
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
        Friend WithEvents XtraTabPage_Account As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents ProductsAccount1 As DebtPlus.UI.Client.controls.ProductsAccount
        Friend WithEvents XtraTabPage_Notes As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents ProductsNotes1 As DebtPlus.UI.Client.controls.ProductsNotes
        Friend WithEvents ProductsHeader1 As DebtPlus.UI.Client.controls.ProductsHeader
        Friend WithEvents SimpleButton_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents SimpleButton_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem_Left As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem_Right As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem3 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
        Friend WithEvents Bar2 As DevExpress.XtraBars.Bar
        Friend WithEvents BarSubItem_Menu_File As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarSubItem_Menu_View As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarSubItem_Menu_Reports As DevExpress.XtraBars.BarSubItem
        Friend WithEvents BarButtonItem_FileExit As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItemViewAccount As DevExpress.XtraBars.BarCheckItem
        Friend WithEvents BarButtonItemViewNotes As DevExpress.XtraBars.BarCheckItem
        Friend WithEvents Bar3 As DevExpress.XtraBars.Bar
        Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
        Friend WithEvents XtraTabPage_Transactions As DevExpress.XtraTab.XtraTabPage
        Friend WithEvents ProductsTransacitons1 As DebtPlus.UI.Client.controls.ProductsTransacitons
        Friend WithEvents BarCheckItemViewTransactions As DevExpress.XtraBars.BarCheckItem
    End Class
End Namespace
