﻿Namespace forms
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class EmploymentDurationForm
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.SimpleButton_OK = New DevExpress.XtraEditors.SimpleButton
            Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton
            Me.TextEdit_Years = New DevExpress.XtraEditors.TextEdit
            Me.TextEdit_Months = New DevExpress.XtraEditors.TextEdit
            CType(Me.TextEdit_Years.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_Months.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(12, 15)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(27, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "&Years"
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(12, 41)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(35, 13)
            Me.LabelControl2.TabIndex = 2
            Me.LabelControl2.Text = "&Months"
            '
            'SimpleButton_OK
            '
            Me.SimpleButton_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.SimpleButton_OK.Location = New System.Drawing.Point(10, 66)
            Me.SimpleButton_OK.Name = "SimpleButton_OK"
            Me.SimpleButton_OK.Size = New System.Drawing.Size(46, 15)
            Me.SimpleButton_OK.TabIndex = 4
            Me.SimpleButton_OK.Text = "&OK"
            '
            'SimpleButton1
            '
            Me.SimpleButton1.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton1.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.SimpleButton1.Location = New System.Drawing.Point(62, 66)
            Me.SimpleButton1.Name = "SimpleButton1"
            Me.SimpleButton1.Size = New System.Drawing.Size(46, 15)
            Me.SimpleButton1.TabIndex = 5
            Me.SimpleButton1.Text = "&Cancel"
            '
            'TextEdit_Years
            '
            Me.TextEdit_Years.Location = New System.Drawing.Point(59, 12)
            Me.TextEdit_Years.Name = "TextEdit_Years"
            Me.TextEdit_Years.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.TextEdit_Years.Properties.Appearance.Options.UseTextOptions = True
            Me.TextEdit_Years.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.TextEdit_Years.Properties.DisplayFormat.FormatString = "f0"
            Me.TextEdit_Years.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.TextEdit_Years.Properties.EditFormat.FormatString = "f0"
            Me.TextEdit_Years.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.TextEdit_Years.Properties.Mask.BeepOnError = True
            Me.TextEdit_Years.Properties.Mask.EditMask = "f0"
            Me.TextEdit_Years.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
            Me.TextEdit_Years.Properties.ValidateOnEnterKey = True
            Me.TextEdit_Years.Size = New System.Drawing.Size(46, 20)
            Me.TextEdit_Years.TabIndex = 1
            '
            'TextEdit_Months
            '
            Me.TextEdit_Months.Location = New System.Drawing.Point(59, 38)
            Me.TextEdit_Months.Name = "TextEdit_Months"
            Me.TextEdit_Months.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.TextEdit_Months.Properties.Appearance.Options.UseTextOptions = True
            Me.TextEdit_Months.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.TextEdit_Months.Properties.DisplayFormat.FormatString = "f0"
            Me.TextEdit_Months.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.TextEdit_Months.Properties.EditFormat.FormatString = "f0"
            Me.TextEdit_Months.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.TextEdit_Months.Properties.Mask.BeepOnError = True
            Me.TextEdit_Months.Properties.Mask.EditMask = "f0"
            Me.TextEdit_Months.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
            Me.TextEdit_Months.Properties.ValidateOnEnterKey = True
            Me.TextEdit_Months.Size = New System.Drawing.Size(46, 20)
            Me.TextEdit_Months.TabIndex = 3
            '
            'EmploymentDurationForm
            '
            Me.Appearance.BackColor = System.Drawing.SystemColors.Window
            Me.Appearance.Options.UseBackColor = True
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(117, 93)
            Me.Controls.Add(Me.TextEdit_Months)
            Me.Controls.Add(Me.TextEdit_Years)
            Me.Controls.Add(Me.SimpleButton1)
            Me.Controls.Add(Me.SimpleButton_OK)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LabelControl1)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
            Me.Name = "EmploymentDurationForm"
            Me.ShowInTaskbar = False
            Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
            Me.Text = "Duration"
            Me.TopMost = True
            CType(Me.TextEdit_Years.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_Months.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents SimpleButton_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents TextEdit_Years As DevExpress.XtraEditors.TextEdit
        Friend WithEvents TextEdit_Months As DevExpress.XtraEditors.TextEdit
    End Class
End Namespace