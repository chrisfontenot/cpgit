﻿Imports DebtPlus.UI.Client.controls

Namespace forms

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form_BalanceDeposit
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.SimpleButton_OK = New DevExpress.XtraEditors.SimpleButton()
            Me.SimpleButton_Cancel = New DevExpress.XtraEditors.SimpleButton()
            Me.SimpleButton_Prorate = New DevExpress.XtraEditors.SimpleButton()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
            Me.CalcEdit1 = New DevExpress.XtraEditors.CalcEdit()
            Me.AdjustDepositDebtControl1 = New DebtPlus.UI.Client.controls.AdjustDepositDebtControl()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.AdjustDepositDebtControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'DefaultLookAndFeel1
            '
            Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin"
            '
            'SimpleButton_OK
            '
            Me.SimpleButton_OK.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton_OK.Enabled = False
            Me.SimpleButton_OK.Location = New System.Drawing.Point(393, 13)
            Me.SimpleButton_OK.Name = "SimpleButton_OK"
            Me.SimpleButton_OK.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.TabIndex = 4
            Me.SimpleButton_OK.Text = "&OK"
            '
            'SimpleButton_Cancel
            '
            Me.SimpleButton_Cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.SimpleButton_Cancel.Location = New System.Drawing.Point(393, 42)
            Me.SimpleButton_Cancel.Name = "SimpleButton_Cancel"
            Me.SimpleButton_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.TabIndex = 5
            Me.SimpleButton_Cancel.Text = "&Cancel"
            '
            'SimpleButton_Prorate
            '
            Me.SimpleButton_Prorate.Location = New System.Drawing.Point(228, 13)
            Me.SimpleButton_Prorate.Name = "SimpleButton_Prorate"
            Me.SimpleButton_Prorate.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Prorate.TabIndex = 2
            Me.SimpleButton_Prorate.Text = "&Prorate"
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(12, 19)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(104, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "New Deposit Amount:"
            '
            'CalcEdit1
            '
            Me.CalcEdit1.Location = New System.Drawing.Point(122, 16)
            Me.CalcEdit1.Name = "CalcEdit1"
            Me.CalcEdit1.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit1.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.CalcEdit1.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit1.Properties.AppearanceDropDown.Options.UseTextOptions = True
            Me.CalcEdit1.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit1.Properties.AppearanceFocused.Options.UseTextOptions = True
            Me.CalcEdit1.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit1.Properties.AppearanceReadOnly.Options.UseTextOptions = True
            Me.CalcEdit1.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit1.Properties.DisplayFormat.FormatString = "{0:c}"
            Me.CalcEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit1.Properties.EditFormat.FormatString = "{0:c}"
            Me.CalcEdit1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit1.Properties.Mask.BeepOnError = True
            Me.CalcEdit1.Properties.Mask.EditMask = "c"
            Me.CalcEdit1.Properties.Precision = 2
            Me.CalcEdit1.Size = New System.Drawing.Size(100, 20)
            Me.CalcEdit1.TabIndex = 1
            '
            'AdjustDepositDebtControl1
            '
            Me.AdjustDepositDebtControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.AdjustDepositDebtControl1.Location = New System.Drawing.Point(12, 71)
            Me.AdjustDepositDebtControl1.Name = "AdjustDepositDebtControl1"
            Me.AdjustDepositDebtControl1.Size = New System.Drawing.Size(457, 225)
            Me.AdjustDepositDebtControl1.TabIndex = 3
            '
            'Form_BalanceDeposit
            '
            Me.AcceptButton = Me.SimpleButton_OK
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.SimpleButton_Cancel
            Me.ClientSize = New System.Drawing.Size(482, 308)
            Me.Controls.Add(Me.CalcEdit1)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.SimpleButton_Prorate)
            Me.Controls.Add(Me.SimpleButton_Cancel)
            Me.Controls.Add(Me.SimpleButton_OK)
            Me.Controls.Add(Me.AdjustDepositDebtControl1)
            Me.Name = "Form_BalanceDeposit"
            Me.Text = "Adjust Deposit Amount"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.AdjustDepositDebtControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents AdjustDepositDebtControl1 As AdjustDepositDebtControl
        Friend WithEvents SimpleButton_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton_Prorate As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents CalcEdit1 As DevExpress.XtraEditors.CalcEdit
    End Class
End Namespace