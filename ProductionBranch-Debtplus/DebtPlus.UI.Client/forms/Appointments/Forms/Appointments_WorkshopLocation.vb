#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DebtPlus.UI.Common
Imports DebtPlus.Svc.Client.Appointments
Imports DebtPlus.LINQ
Imports System.Linq

Namespace forms.Appointments

    Public Class Appointments_WorkshopLocation
        Private wlRecord As workshop_location
        Protected bc As BusinessContext = Nothing

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(bc As BusinessContext, ByVal wlRecord As workshop_location)
            MyClass.New()
            Me.wlRecord = wlRecord
            Me.bc = bc
            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf Form_Load
            AddHandler LayoutControl1.Resize, AddressOf LayoutControl1_Resize
        End Sub

        Private Sub UnregisterHandlers()
            RemoveHandler Load, AddressOf Form_Load
            RemoveHandler LayoutControl1.Resize, AddressOf LayoutControl1_Resize
        End Sub

        Private Sub Form_Load(sender As Object, e As EventArgs)

            ' Change the dialog title as possible to the name
            Dim organization As String = wlRecord.organization
            If Not String.IsNullOrEmpty(organization) Then
                Text = organization + " Location Information"
            Else
                Text = "Workshop Location Information"
            End If

            ' Find the address information
            Dim AddressID As Int32 = wlRecord.AddressID.GetValueOrDefault(0)
            If AddressID > 0 Then
                Dim q As DebtPlus.LINQ.address = bc.addresses.Where(Function(a) a.Id = AddressID).FirstOrDefault()
                If q IsNot Nothing Then
                    LabelControl_Addr.Text = q.ToString()
                End If
            End If

            ' Find the telephone information
            Dim TelephoneID As Int32 = wlRecord.TelephoneID.GetValueOrDefault(0)
            If TelephoneID > 0 Then
                Dim q As DebtPlus.LINQ.TelephoneNumber = bc.TelephoneNumbers.Where(Function(t) t.Id = TelephoneID).FirstOrDefault()
                If q IsNot Nothing Then
                    LabelControl_Phone.Text = q.ToString()
                End If
            End If

            ' Find the contact name
            Dim Name As String = wlRecord.name
            If Not String.IsNullOrEmpty(Name) Then
                LabelControl_Name.Text = Name
            End If
        End Sub

        Private Sub LayoutControl1_Resize(sender As Object, e As System.EventArgs)
            EmptySpaceItem_Left.Width = (EmptySpaceItem_Left.Width + EmptySpaceItem_Right.Width) \ 2
        End Sub
    End Class
End Namespace
