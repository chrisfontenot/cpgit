Namespace forms.Appointments
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Appointments_Form_Book_Select

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Me.SimpleButton_OK = New DevExpress.XtraEditors.SimpleButton()
            Me.SimpleButton_Cancel = New DevExpress.XtraEditors.SimpleButton()
            Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
            Me.SimpleButton_Previous = New DevExpress.XtraEditors.SimpleButton()
            Me.SimpleButton_Next = New DevExpress.XtraEditors.SimpleButton()
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
            Me.GridColumn_start_time = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_Office = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_appt_type = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_avail = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_booked = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_counselor = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.barManager1 = New DevExpress.XtraBars.BarManager(Me.components)
            Me.StatusBar1 = New DevExpress.XtraBars.Bar()
            Me.BarStaticItem_Week = New DevExpress.XtraBars.BarStaticItem()
            Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
            Me.GridColumn_client_time = New DevExpress.XtraGrid.Columns.GridColumn()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupControl1.SuspendLayout()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'SimpleButton_OK
            '
            Me.SimpleButton_OK.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton_OK.Location = New System.Drawing.Point(539, 13)
            Me.SimpleButton_OK.Name = "SimpleButton_OK"
            Me.SimpleButton_OK.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.TabIndex = 0
            Me.SimpleButton_OK.Text = "&OK"
            '
            'SimpleButton_Cancel
            '
            Me.SimpleButton_Cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.SimpleButton_Cancel.Location = New System.Drawing.Point(539, 42)
            Me.SimpleButton_Cancel.Name = "SimpleButton_Cancel"
            Me.SimpleButton_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.TabIndex = 1
            Me.SimpleButton_Cancel.Text = "&Cancel"
            '
            'GroupControl1
            '
            Me.GroupControl1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GroupControl1.Controls.Add(Me.SimpleButton_Previous)
            Me.GroupControl1.Controls.Add(Me.SimpleButton_Next)
            Me.GroupControl1.Location = New System.Drawing.Point(526, 87)
            Me.GroupControl1.Name = "GroupControl1"
            Me.GroupControl1.Size = New System.Drawing.Size(100, 86)
            Me.GroupControl1.TabIndex = 2
            Me.GroupControl1.Text = "Week"
            '
            'SimpleButton_Previous
            '
            Me.SimpleButton_Previous.Location = New System.Drawing.Point(13, 53)
            Me.SimpleButton_Previous.Name = "SimpleButton_Previous"
            Me.SimpleButton_Previous.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Previous.TabIndex = 3
            Me.SimpleButton_Previous.Text = "&Previous"
            '
            'SimpleButton_Next
            '
            Me.SimpleButton_Next.Location = New System.Drawing.Point(13, 24)
            Me.SimpleButton_Next.Name = "SimpleButton_Next"
            Me.SimpleButton_Next.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Next.TabIndex = 2
            Me.SimpleButton_Next.Text = "&Next"
            '
            'GridControl1
            '
            Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GridControl1.Location = New System.Drawing.Point(12, 12)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(508, 225)
            Me.GridControl1.TabIndex = 3
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(195, Byte), Integer))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(189, Byte), Integer))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(206, Byte), Integer))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(225, Byte), Integer))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(215, Byte), Integer))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(215, Byte), Integer))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(215, Byte), Integer))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_start_time, Me.GridColumn_client_time, Me.GridColumn_Office, Me.GridColumn_appt_type, Me.GridColumn_avail, Me.GridColumn_booked, Me.GridColumn_counselor})
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            '
            'GridColumn_start_time
            '
            Me.GridColumn_start_time.Caption = "Counselor Date Time"
            Me.GridColumn_start_time.CustomizationCaption = "Date and Time of the appointment in Counselor Timezone"
            Me.GridColumn_start_time.DisplayFormat.FormatString = "ddd MM/dd hh:mm tt"
            Me.GridColumn_start_time.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_start_time.FieldName = "start_time"
            Me.GridColumn_start_time.GroupFormat.FormatString = "d"
            Me.GridColumn_start_time.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_start_time.Name = "GridColumn_start_time"
            Me.GridColumn_start_time.OptionsColumn.AllowEdit = False
            Me.GridColumn_start_time.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_start_time.Visible = True
            Me.GridColumn_start_time.VisibleIndex = 0
            Me.GridColumn_start_time.Width = 112
            '
            'GridColumn_Office
            '
            Me.GridColumn_Office.Caption = "Office"
            Me.GridColumn_Office.CustomizationCaption = "Office Location"
            Me.GridColumn_Office.FieldName = "office"
            Me.GridColumn_Office.Name = "GridColumn_Office"
            Me.GridColumn_Office.OptionsColumn.AllowEdit = False
            Me.GridColumn_Office.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_Office.Visible = True
            Me.GridColumn_Office.VisibleIndex = 2
            Me.GridColumn_Office.Width = 119
            '
            'GridColumn_appt_type
            '
            Me.GridColumn_appt_type.Caption = "Type"
            Me.GridColumn_appt_type.CustomizationCaption = "Type of the appointment"
            Me.GridColumn_appt_type.FieldName = "appt_type"
            Me.GridColumn_appt_type.Name = "GridColumn_appt_type"
            Me.GridColumn_appt_type.OptionsColumn.AllowEdit = False
            Me.GridColumn_appt_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_appt_type.Visible = True
            Me.GridColumn_appt_type.VisibleIndex = 3
            Me.GridColumn_appt_type.Width = 53
            '
            'GridColumn_avail
            '
            Me.GridColumn_avail.Caption = "Avail"
            Me.GridColumn_avail.CustomizationCaption = "Number Available"
            Me.GridColumn_avail.DisplayFormat.FormatString = "n0"
            Me.GridColumn_avail.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_avail.FieldName = "available_slots"
            Me.GridColumn_avail.GroupFormat.FormatString = "n0"
            Me.GridColumn_avail.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_avail.Name = "GridColumn_avail"
            Me.GridColumn_avail.OptionsColumn.AllowEdit = False
            Me.GridColumn_avail.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_avail.Visible = True
            Me.GridColumn_avail.VisibleIndex = 4
            Me.GridColumn_avail.Width = 33
            '
            'GridColumn_booked
            '
            Me.GridColumn_booked.Caption = "BK"
            Me.GridColumn_booked.CustomizationCaption = "Number booked"
            Me.GridColumn_booked.DisplayFormat.FormatString = "n0"
            Me.GridColumn_booked.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_booked.FieldName = "booked"
            Me.GridColumn_booked.GroupFormat.FormatString = "n0"
            Me.GridColumn_booked.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_booked.Name = "GridColumn_booked"
            Me.GridColumn_booked.OptionsColumn.AllowEdit = False
            Me.GridColumn_booked.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_booked.Visible = True
            Me.GridColumn_booked.VisibleIndex = 5
            Me.GridColumn_booked.Width = 33
            '
            'GridColumn_counselor
            '
            Me.GridColumn_counselor.Caption = "Counselor"
            Me.GridColumn_counselor.CustomizationCaption = "Counselor Name"
            Me.GridColumn_counselor.FieldName = "counselor"
            Me.GridColumn_counselor.Name = "GridColumn_counselor"
            Me.GridColumn_counselor.OptionsColumn.AllowEdit = False
            Me.GridColumn_counselor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'barManager1
            '
            Me.barManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.StatusBar1})
            Me.barManager1.DockControls.Add(Me.barDockControlTop)
            Me.barManager1.DockControls.Add(Me.barDockControlBottom)
            Me.barManager1.DockControls.Add(Me.barDockControlLeft)
            Me.barManager1.DockControls.Add(Me.barDockControlRight)
            Me.barManager1.Form = Me
            Me.barManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarStaticItem_Week})
            Me.barManager1.MaxItemId = 1
            Me.barManager1.StatusBar = Me.StatusBar1
            '
            'StatusBar1
            '
            Me.StatusBar1.BarName = "Status bar"
            Me.StatusBar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom
            Me.StatusBar1.DockCol = 0
            Me.StatusBar1.DockRow = 0
            Me.StatusBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
            Me.StatusBar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarStaticItem_Week)})
            Me.StatusBar1.OptionsBar.AllowQuickCustomization = False
            Me.StatusBar1.OptionsBar.DrawDragBorder = False
            Me.StatusBar1.OptionsBar.DrawSizeGrip = True
            Me.StatusBar1.OptionsBar.UseWholeRow = True
            Me.StatusBar1.Text = "Status bar"
            '
            'BarStaticItem_Week
            '
            Me.BarStaticItem_Week.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring
            Me.BarStaticItem_Week.Id = 0
            Me.BarStaticItem_Week.Name = "BarStaticItem_Week"
            Me.BarStaticItem_Week.TextAlignment = System.Drawing.StringAlignment.Near
            Me.BarStaticItem_Week.Width = 32
            '
            'barDockControlTop
            '
            Me.barDockControlTop.CausesValidation = False
            Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
            Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
            Me.barDockControlTop.Size = New System.Drawing.Size(638, 0)
            '
            'barDockControlBottom
            '
            Me.barDockControlBottom.CausesValidation = False
            Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
            Me.barDockControlBottom.Location = New System.Drawing.Point(0, 243)
            Me.barDockControlBottom.Size = New System.Drawing.Size(638, 25)
            '
            'barDockControlLeft
            '
            Me.barDockControlLeft.CausesValidation = False
            Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
            Me.barDockControlLeft.Location = New System.Drawing.Point(0, 0)
            Me.barDockControlLeft.Size = New System.Drawing.Size(0, 243)
            '
            'barDockControlRight
            '
            Me.barDockControlRight.CausesValidation = False
            Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
            Me.barDockControlRight.Location = New System.Drawing.Point(638, 0)
            Me.barDockControlRight.Size = New System.Drawing.Size(0, 243)
            '
            'GridColumn_client_time
            '
            Me.GridColumn_client_time.Caption = "Client Date Time"
            Me.GridColumn_client_time.CustomizationCaption = "Date and Time of the appointment in Client Timezone"
            Me.GridColumn_client_time.DisplayFormat.FormatString = "ddd MM/dd hh:mm tt"
            Me.GridColumn_client_time.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_client_time.FieldName = "client_time"
            Me.GridColumn_client_time.GroupFormat.FormatString = "d"
            Me.GridColumn_client_time.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_client_time.Name = "GridColumn_client_time"
            Me.GridColumn_client_time.OptionsColumn.AllowEdit = False
            Me.GridColumn_client_time.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_client_time.Visible = True
            Me.GridColumn_client_time.VisibleIndex = 1
            '
            'Appointments_Form_Book_Select
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.SimpleButton_Cancel
            Me.ClientSize = New System.Drawing.Size(638, 268)
            Me.Controls.Add(Me.GridControl1)
            Me.Controls.Add(Me.GroupControl1)
            Me.Controls.Add(Me.SimpleButton_Cancel)
            Me.Controls.Add(Me.SimpleButton_OK)
            Me.Controls.Add(Me.barDockControlLeft)
            Me.Controls.Add(Me.barDockControlRight)
            Me.Controls.Add(Me.barDockControlBottom)
            Me.Controls.Add(Me.barDockControlTop)
            Me.Name = "Appointments_Form_Book_Select"
            Me.Text = "Select Appointment"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupControl1.ResumeLayout(False)
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Public WithEvents SimpleButton_OK As DevExpress.XtraEditors.SimpleButton
        Public WithEvents SimpleButton_Cancel As DevExpress.XtraEditors.SimpleButton
        Public WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
        Public WithEvents SimpleButton_Previous As DevExpress.XtraEditors.SimpleButton
        Public WithEvents SimpleButton_Next As DevExpress.XtraEditors.SimpleButton
        Public WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Public WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Public WithEvents GridColumn_start_time As DevExpress.XtraGrid.Columns.GridColumn
        Public WithEvents GridColumn_Office As DevExpress.XtraGrid.Columns.GridColumn
        Public WithEvents GridColumn_appt_type As DevExpress.XtraGrid.Columns.GridColumn
        Public WithEvents GridColumn_avail As DevExpress.XtraGrid.Columns.GridColumn
        Public WithEvents GridColumn_counselor As DevExpress.XtraGrid.Columns.GridColumn
        Public WithEvents barManager1 As DevExpress.XtraBars.BarManager
        Public WithEvents StatusBar1 As DevExpress.XtraBars.Bar
        Public WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
        Public WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
        Public WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
        Public WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
        Public WithEvents BarStaticItem_Week As DevExpress.XtraBars.BarStaticItem
        Public WithEvents GridColumn_booked As DevExpress.XtraGrid.Columns.GridColumn
        Public WithEvents GridColumn_client_time As DevExpress.XtraGrid.Columns.GridColumn
    End Class
End NameSpace
