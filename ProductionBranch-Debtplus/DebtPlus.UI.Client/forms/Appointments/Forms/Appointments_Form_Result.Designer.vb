Namespace forms.Appointments
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Appointments_Form_Result
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.Appointments_Control_Result1 = New DebtPlus.UI.Client.forms.Appointments.Appointments_Control_Result()
            Me.Appointments_Control_List_NoWorkshop1 = New DebtPlus.UI.Client.forms.Appointments.Appointments_Control_List_NoWorkshop()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'DefaultLookAndFeel1
            '
            Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin"
            '
            'Appointments_Control_Result1
            '
            Me.Appointments_Control_Result1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.Appointments_Control_Result1.Location = New System.Drawing.Point(0, 0)
            Me.Appointments_Control_Result1.Margin = New System.Windows.Forms.Padding(0)
            Me.Appointments_Control_Result1.MinimumSize = New System.Drawing.Size(508, 341)
            Me.Appointments_Control_Result1.Name = "Appointments_Control_Result1"
            Me.Appointments_Control_Result1.Size = New System.Drawing.Size(541, 341)
            Me.Appointments_Control_Result1.TabIndex = 1
            '
            'Appointments_Control_List_NoWorkshop1
            '
            Me.Appointments_Control_List_NoWorkshop1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.Appointments_Control_List_NoWorkshop1.Location = New System.Drawing.Point(0, 0)
            Me.Appointments_Control_List_NoWorkshop1.Name = "Appointments_Control_List_NoWorkshop1"
            Me.Appointments_Control_List_NoWorkshop1.Size = New System.Drawing.Size(541, 306)
            Me.Appointments_Control_List_NoWorkshop1.TabIndex = 0
            '
            'Appointments_Form_Result
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(541, 306)
            Me.Controls.Add(Me.Appointments_Control_Result1)
            Me.Controls.Add(Me.Appointments_Control_List_NoWorkshop1)
            Me.Name = "Appointments_Form_Result"
            Me.Text = "Result Status for the Appointment"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Public WithEvents Appointments_Control_List_NoWorkshop1 As Appointments_Control_List_NoWorkshop
        Public WithEvents Appointments_Control_Result1 As Appointments_Control_Result
    End Class
End NameSpace