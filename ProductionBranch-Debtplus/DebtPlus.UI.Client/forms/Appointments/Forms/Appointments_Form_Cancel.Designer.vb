Namespace forms.Appointments
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Appointments_Form_Cancel
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.Appointments_List_Control1 = New Appointments_Control_List
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'Appointments_List_Control1
            '
            Me.Appointments_List_Control1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.Appointments_List_Control1.Location = New System.Drawing.Point(0, 0)
            Me.Appointments_List_Control1.Name = "Appointments_List_Control1"
            Me.Appointments_List_Control1.Size = New System.Drawing.Size(416, 266)
            Me.ToolTipController1.SetSuperTip(Me.Appointments_List_Control1, Nothing)
            Me.Appointments_List_Control1.TabIndex = 0
            Me.Appointments_List_Control1.Text = "Select the appointment that you wish to cancel. You may cancel only one appointment at a time."
            '
            'Appointments_Form_Cancel
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(416, 266)
            Me.Controls.Add(Me.Appointments_List_Control1)
            Me.Name = "Appointments_Form_Cancel"
            Me.ToolTipController1.SetSuperTip(Me, Nothing)
            Me.Text = "Cancel Counseling Appointment"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Public WithEvents Appointments_List_Control1 As Appointments_Control_List
    End Class
End NameSpace