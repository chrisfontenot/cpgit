#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DebtPlus.Svc.Client.Appointments

Imports DebtPlus.LINQ
Imports System.Linq

Namespace forms.Appointments
    Public Class Appointments_Form_Result
        Private clientID As Int32 = -1
        Private record As DebtPlus.LINQ.client_appointment
        Protected bc As BusinessContext = Nothing

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        ''' <summary>
        ''' Initialize the form
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        ''' <summary>
        ''' Initialize the form with context information
        ''' </summary>
        Public Sub New(bc As BusinessContext, ByVal clientID As Int32)
            MyClass.New()

            Me.bc = bc
            Me.clientID = clientID
            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf Appointments_Form_Result_Load
            AddHandler Appointments_Control_List_NoWorkshop1.EditItem, AddressOf Appointments_Control_List_NoWorkshop1_EditItem
            AddHandler Appointments_Control_Result1.Cancelled, AddressOf Appointments_Control_Result1_Cancelled
            AddHandler Appointments_Control_List_NoWorkshop1.Cancel, AddressOf Appointments_Control_List_NoWorkshop1_Cancel
            AddHandler Appointments_Control_Result1.Completed, AddressOf Appointments_Control_Result1_Completed
            AddHandler Appointments_Control_Result1.Completing, AddressOf Appointments_Control_Result1_Completing
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler Load, AddressOf Appointments_Form_Result_Load
            RemoveHandler Appointments_Control_List_NoWorkshop1.EditItem, AddressOf Appointments_Control_List_NoWorkshop1_EditItem
            RemoveHandler Appointments_Control_Result1.Cancelled, AddressOf Appointments_Control_Result1_Cancelled
            RemoveHandler Appointments_Control_List_NoWorkshop1.Cancel, AddressOf Appointments_Control_List_NoWorkshop1_Cancel
            RemoveHandler Appointments_Control_Result1.Completed, AddressOf Appointments_Control_Result1_Completed
            RemoveHandler Appointments_Control_Result1.Completing, AddressOf Appointments_Control_Result1_Completing
        End Sub

        Private Sub Appointments_Form_Result_Load(ByVal sender As Object, ByVal e As EventArgs)
            UnRegisterHandlers()

            ' Load the previous placement and size for the form
            LoadPlacement("Client.Appointment.Appointments_Form_Result")

            Try
                If clientID >= 0 Then
                    Appointments_Control_List_NoWorkshop1.BringToFront()
                    Appointments_Control_List_NoWorkshop1.Visible = True
                    Appointments_Control_List_NoWorkshop1.ReadForm(bc, clientID)
                End If

            Finally
                RegisterHandlers()
            End Try
        End Sub

        Protected Overridable Sub Appointments_Control_List_NoWorkshop1_EditItem(ByVal sender As Object, ByVal caRecord As client_appointment)

            ' Mark the current record as "kept"
            If caRecord IsNot Nothing Then
                Appointments_Control_List_NoWorkshop1.SendToBack()
                Appointments_Control_List_NoWorkshop1.Visible = False

                Appointments_Control_Result1.BringToFront()
                Appointments_Control_Result1.Visible = True
                Appointments_Control_Result1.ReadForm(bc, caRecord)
            End If
        End Sub

        Private Sub Appointments_Control_Result1_Cancelled(ByVal sender As Object, ByVal e As EventArgs)
            Appointments_Control_Result1.SendToBack()
            Appointments_Control_Result1.Visible = False

            Appointments_Control_List_NoWorkshop1.BringToFront()
            Appointments_Control_List_NoWorkshop1.Visible = True
        End Sub

        Private Sub Appointments_Control_List_NoWorkshop1_Cancel(ByVal sender As Object, ByVal e As EventArgs)
            DialogResult = DialogResult.Cancel
        End Sub

        Private Sub Appointments_Control_Result1_Completed(ByVal Sender As Object, ByVal e As EventArgs)
            DialogResult = DialogResult.OK
        End Sub

        Private Sub Appointments_Control_Result1_Completing(sender As Object, ByVal caRecord As client_appointment)
            caRecord.status = "K"c
            caRecord.date_updated = DebtPlus.LINQ.BusinessContext.getdate()
        End Sub
    End Class
End NameSpace
