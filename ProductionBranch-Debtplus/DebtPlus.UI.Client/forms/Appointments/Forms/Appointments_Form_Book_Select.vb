#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.Svc.Client.Appointments
Imports DevExpress.XtraGrid.Views.Grid
Imports System.IO
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo
Imports DebtPlus.LINQ
Imports System.Linq
Imports System.Collections.Generic

Namespace forms.Appointments

    Public Class Appointments_Form_Book_Select
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Protected bc As BusinessContext = Nothing
        Private Property ClientTimezoneOffset As Integer
        Private Property IsDstInEffect As Boolean
        Private Property DstApplies As Boolean

        Public Class SelectionCriteria

            ' Result of the appointment search is an appointment. This is the ID of the appointment.
            Public Property clientAppointment As client_appointment

            ' The appointment types and other parameters are passed to the search operation. Most are optional.
            Public Property apptTypeID As System.Nullable(Of Int32)
            Public Property datePart As String
            Public Property am_pm As String
            Public Property zipcode As String
            Public Property counselorID As System.Nullable(Of Int32)
            Public Property fromDate As DateTime
            Public Property toDate As DateTime
            Public Property priority As System.Nullable(Of Int32)
            Public Property bankruptcyClassID As System.Nullable(Of Int32)


            Public Sub New()
                apptTypeID = Nothing
                counselorID = Nothing
                bankruptcyClassID = Nothing
                priority = 0
                datePart = Nothing
                am_pm = Nothing
                zipcode = Nothing
                clientAppointment = Nothing

                ' Find the date range based upon today's date
                fromDate = DateTime.Now.Date
                Select Case fromDate.DayOfWeek
                    Case DayOfWeek.Monday
                        fromDate = fromDate.AddDays(-1)
                    Case DayOfWeek.Tuesday
                        fromDate = fromDate.AddDays(-2)
                    Case DayOfWeek.Wednesday
                        fromDate = fromDate.AddDays(-3)
                    Case DayOfWeek.Thursday
                        fromDate = fromDate.AddDays(-4)
                    Case DayOfWeek.Friday
                        fromDate = fromDate.AddDays(-5)
                    Case DayOfWeek.Saturday
                        fromDate = fromDate.AddDays(-6)
                    Case Else
                End Select

                ' The ending date is the following Saturday (midnight Sunday)
                toDate = fromDate.AddDays(6)

            End Sub

            ''' <summary>
            ''' Can the calendar be retarded a week?
            ''' </summary>
            Public Function canPreviousWeek() As Boolean
                If toDate.AddDays(-7) < DateTime.Now.Date Then
                    Return False
                End If

                Return True
            End Function

            ''' <summary>
            ''' Can you advance the calendar forward?
            ''' </summary>
            Public Function canNextWeek() As Boolean
                Return True
            End Function

            ''' <summary>
            ''' Advance to the previous week
            ''' </summary>
            Public Sub PreviousWeek()
                fromDate = fromDate.AddDays(-7)
                toDate = fromDate.AddDays(6)
            End Sub

            ''' <summary>
            ''' Advance to the next week
            ''' </summary>
            Public Sub NextWeek()
                fromDate = fromDate.AddDays(7)
                toDate = fromDate.AddDays(6)
            End Sub

            ''' <summary>
            ''' Convert the class to a suitable display string
            ''' </summary>
            Public Overrides Function ToString() As String
                Return String.Format("Appointments from {0:d} through {1:d}", fromDate, toDate)
            End Function
        End Class

        Private Const Default_Path As String = "Client.Update\Appointments"
        Private Const Default_File As String = "Book_Select.Gridview1.xml"

        ' List of the possible appointments based upon the selection criteria passed to the form
        Private colRecords As System.Collections.Generic.List(Of xpr_appt_selectResult)
        Private selection As SelectionCriteria
        Private clientAppointment As DebtPlus.LINQ.client_appointment

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(bc As BusinessContext, ByVal selection As SelectionCriteria, ByVal clientAppointment As DebtPlus.LINQ.client_appointment)
            MyBase.New()
            InitializeComponent()
            Me.selection = selection
            Me.bc = bc
            Me.clientAppointment = clientAppointment
            RegisterHandlers()

            CheckDstInEffect()

            GetClientTimezone()

        End Sub

        Private Sub RegisterHandlers()
            AddHandler SimpleButton_Next.Click, AddressOf SimpleButton_Next_Click
            AddHandler SimpleButton_Previous.Click, AddressOf SimpleButton_Previous_Click
            AddHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
            AddHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
            AddHandler Load, AddressOf Appointments_Select_Form_Load
            AddHandler GridView1.Layout, AddressOf LayoutChanged
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler SimpleButton_Next.Click, AddressOf SimpleButton_Next_Click
            RemoveHandler SimpleButton_Previous.Click, AddressOf SimpleButton_Previous_Click
            RemoveHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
            RemoveHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
            RemoveHandler Load, AddressOf Appointments_Select_Form_Load
            RemoveHandler GridView1.Layout, AddressOf LayoutChanged
        End Sub

        Private Sub CheckDstInEffect()

            Dim dt As DateTime = DateTime.Now

            IsDstInEffect = TimeZoneInfo.Local.IsDaylightSavingTime(dt)

        End Sub

        Private Sub GetClientTimezone()

            '' get the client timezone offset
            Dim clientTimezoneDetail As xpr_get_client_timezoneResult = Nothing

            clientTimezoneDetail = bc.xpr_get_client_timezone(clientAppointment.client).FirstOrDefault()

            If clientTimezoneDetail Is Nothing Then
                ClientTimezoneOffset = 0
                DstApplies = False
            Else
                If clientTimezoneDetail.gmt_offset.HasValue Then
                    ClientTimezoneOffset = Convert.ToInt32(clientTimezoneDetail.gmt_offset)

                    If (clientTimezoneDetail.DstApplies > 0) Then
                        DstApplies = True
                    Else
                        DstApplies = False
                    End If

                Else
                    ClientTimezoneOffset = 0
                End If
            End If
        End Sub

        Private Function GetAppointmentRecords() As List(Of xpr_appt_selectResult)
            Dim r As xpr_appt_selectResult = Nothing
            Dim records As List(Of xpr_appt_selectResult) = Nothing
            Dim gmtDateTime As DateTime

            records = bc.xpr_appt_select(clientAppointment.client, selection.apptTypeID, selection.datePart, selection.am_pm, selection.zipcode, selection.counselorID, selection.fromDate, selection.toDate)

            For Each r In records

                '' get counselor timezone
                Dim offset As Integer = 0
                If r.timezone_offset.HasValue Then
                    If IsDstInEffect Then
                        offset = Convert.ToInt32(r.timezone_offset) + 1
                    Else
                        offset = Convert.ToInt32(r.timezone_offset)
                    End If
                End If
                '' set gmt date from counselor date
                gmtDateTime = r.start_time.AddHours(offset * -1)

                Dim timeoffset As Integer = ClientTimezoneOffset

                If IsDstInEffect = True AndAlso DstApplies = True Then
                    timeoffset = ClientTimezoneOffset + 1
                End If
                '' set client date from gmt date
                r.client_time = gmtDateTime.AddHours(timeoffset)

            Next

            Return records

        End Function

        Private Sub Appointments_Select_Form_Load(ByVal sender As Object, ByVal e As EventArgs)

            UnRegisterHandlers()

            Try
                ' Restore the form size and location
                LoadPlacement("Appointments.Book.Select")
                RestoreLayout(GridView1, Path.Combine(XMLBasePath(), Default_File))

                colRecords = GetAppointmentRecords()

                ' Select the appointment information from the database
                ''colRecords = bc.xpr_appt_select(clientAppointment.client, selection.apptTypeID, selection.datePart, selection.am_pm, selection.zipcode, selection.counselorID, selection.fromDate, selection.toDate)
                GridControl1.DataSource = colRecords

                ' Load the status and enable the controls
                BarStaticItem_Week.Caption = selection.ToString()
                SimpleButton_Previous.Enabled = selection.canPreviousWeek()
                SimpleButton_Next.Enabled = selection.canNextWeek()
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Sub SimpleButton_Next_Click(ByVal sender As Object, ByVal e As EventArgs)
            UnRegisterHandlers()
            Try
                selection.NextWeek()
                BarStaticItem_Week.Caption = selection.ToString()

                colRecords = GetAppointmentRecords()
                ''colRecords = bc.xpr_appt_select(clientAppointment.client, selection.apptTypeID, selection.datePart, selection.am_pm, selection.zipcode, selection.counselorID, selection.fromDate, selection.toDate)
                GridControl1.DataSource = colRecords
                GridControl1.RefreshDataSource()
                GridView1.RefreshData()

                ' Refresh the grid
                GridControl1.RefreshDataSource()
                GridView1.RefreshData()

                ' The previous button is invalid if we are already beyond today
                SimpleButton_Previous.Enabled = selection.canPreviousWeek()
                SimpleButton_Next.Enabled = selection.canNextWeek()
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Sub SimpleButton_Previous_Click(ByVal sender As Object, ByVal e As EventArgs)
            UnRegisterHandlers()
            Try
                selection.PreviousWeek()
                BarStaticItem_Week.Caption = selection.ToString()

                colRecords = GetAppointmentRecords()
                ''colRecords = bc.xpr_appt_select(clientAppointment.client, selection.apptTypeID, selection.datePart, selection.am_pm, selection.zipcode, selection.counselorID, selection.fromDate, selection.toDate)
                GridControl1.DataSource = colRecords
                GridControl1.RefreshDataSource()
                GridView1.RefreshData()

                ' Refresh the grid
                GridControl1.RefreshDataSource()
                GridView1.RefreshData()

                ' The previous button is invalid if we are already beyond today
                SimpleButton_Previous.Enabled = selection.canPreviousWeek()
                SimpleButton_Next.Enabled = selection.canNextWeek()
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Protected Sub GridView1_DoubleClick(ByVal sender As Object, ByVal e As EventArgs)
            UnRegisterHandlers()
            Try
                ' Find the row targeted as the double-click item
                Dim hi As GridHitInfo = GridView1.CalcHitInfo((GridControl1.PointToClient(MousePosition)))
                Dim ControlRow As Int32 = hi.RowHandle

                ' Find the record from the input tables.
                Dim record As xpr_appt_selectResult = TryCast(GridView1.GetRow(ControlRow), xpr_appt_selectResult)
                If record IsNot Nothing Then
                    selection.clientAppointment = CanBook(record)
                    If selection.clientAppointment IsNot Nothing Then
                        DialogResult = DialogResult.OK
                    End If
                End If
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Sub SimpleButton_OK_Click(ByVal Sender As Object, ByVal e As EventArgs)
            UnRegisterHandlers()
            Try
                Dim record As xpr_appt_selectResult = TryCast(GridView1.GetFocusedRow, xpr_appt_selectResult)

                If record IsNot Nothing Then
                    selection.clientAppointment = CanBook(record)
                    If selection.clientAppointment IsNot Nothing Then
                        DialogResult = DialogResult.OK
                    End If
                End If
            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Attempt to book the appointment
        ''' </summary>
        Private Function CanBook(ByVal record As xpr_appt_selectResult) As client_appointment

            If record.appt_time > 0 Then
                Try
                    Dim Id As Int32 = bc.xpr_appt_book(clientAppointment.client, record.appt_time, selection.priority, selection.apptTypeID, clientAppointment.previous_appointment, selection.counselorID, selection.bankruptcyClassID, clientAppointment.partner)

                    ' If there is an ID then re-read the fields that are set by the booking stored procedure and update the clientAppointment record
                    If Id > 0 Then
                        Dim q As DebtPlus.LINQ.client_appointment = bc.client_appointments.Where(Function(ca) ca.Id = Id).FirstOrDefault()
                        If q IsNot Nothing Then
                            clientAppointment.Id = q.Id
                            clientAppointment.appt_time = q.appt_time
                            clientAppointment.appt_type = q.appt_type
                            clientAppointment.bankruptcy_class = q.bankruptcy_class
                            clientAppointment.counselor = q.counselor
                            clientAppointment.credit = q.credit
                            clientAppointment.date_updated = q.date_updated
                            clientAppointment.end_time = q.end_time
                            clientAppointment.housing = q.housing
                            clientAppointment.office = q.office
                            clientAppointment.partner = q.partner
                            clientAppointment.post_purchase = q.post_purchase
                            clientAppointment.start_time = q.start_time
                            clientAppointment.status = q.status

                            ' Return the appointment as confirmation that it was booked
                            Return clientAppointment
                        End If
                    End If

                Catch ex As System.Data.SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error booking appointment")
                End Try
            End If

            Return Nothing
        End Function

        Protected Shared Sub RestoreLayout(ByRef Control As GridView, ByVal Filename As String)
            Try
                If File.Exists(Filename) Then
                    Control.RestoreLayoutFromXml(Filename)
                End If
            Catch ex As Exception
            End Try
        End Sub

        ''' <summary>
        ''' Return the directory to the saved layout information
        ''' </summary>
        Protected Overridable Function XMLBasePath() As String
            Dim basePath As String = String.Format("{0}{1}DebtPlus", Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), Path.DirectorySeparatorChar)
            Return Path.Combine(basePath, Default_Path)
        End Function

        ''' <summary>
        ''' If the layout is changed then update the file
        ''' </summary>
        Private Sub LayoutChanged(ByVal Sender As Object, ByVal e As EventArgs)
            Dim pathName As String = XMLBasePath()
            If Not Directory.Exists(pathName) Then
                Directory.CreateDirectory(pathName)
            End If

            Dim fileName As String = Path.Combine(pathName, Default_File)
            Try
                GridView1.SaveLayoutToXml(fileName)

            Catch ex As Exception
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)
            End Try
        End Sub


    End Class
End Namespace
