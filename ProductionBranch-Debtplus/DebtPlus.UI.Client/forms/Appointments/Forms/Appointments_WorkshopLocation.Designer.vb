Namespace forms.Appointments
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Appointments_WorkshopLocation
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then
                        components.Dispose()
                    End If
                End If
                wlRecord = Nothing

            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
            Me.LabelControl_Phone = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl_Addr = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl_Name = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem_Right = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem_Left = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem4 = New DevExpress.XtraLayout.EmptySpaceItem()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem_Right, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem_Left, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'DefaultLookAndFeel1
            '
            Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin"
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.SimpleButton1)
            Me.LayoutControl1.Controls.Add(Me.LabelControl_Phone)
            Me.LayoutControl1.Controls.Add(Me.LabelControl_Addr)
            Me.LayoutControl1.Controls.Add(Me.LabelControl_Name)
            Me.LayoutControl1.Controls.Add(Me.LabelControl1)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(319, 193)
            Me.LayoutControl1.TabIndex = 0
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'SimpleButton1
            '
            Me.SimpleButton1.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.SimpleButton1.Location = New System.Drawing.Point(120, 158)
            Me.SimpleButton1.MaximumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton1.MinimumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton1.Name = "SimpleButton1"
            Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton1.StyleController = Me.LayoutControl1
            Me.SimpleButton1.TabIndex = 8
            Me.SimpleButton1.Text = "&OK"
            '
            'LabelControl_Phone
            '
            Me.LabelControl_Phone.Location = New System.Drawing.Point(45, 82)
            Me.LabelControl_Phone.Name = "LabelControl_Phone"
            Me.LabelControl_Phone.Size = New System.Drawing.Size(262, 13)
            Me.LabelControl_Phone.StyleController = Me.LayoutControl1
            Me.LabelControl_Phone.TabIndex = 6
            Me.LabelControl_Phone.UseMnemonic = False
            '
            'LabelControl_Addr
            '
            Me.LabelControl_Addr.Location = New System.Drawing.Point(45, 65)
            Me.LabelControl_Addr.Name = "LabelControl_Addr"
            Me.LabelControl_Addr.Size = New System.Drawing.Size(262, 13)
            Me.LabelControl_Addr.StyleController = Me.LayoutControl1
            Me.LabelControl_Addr.TabIndex = 5
            Me.LabelControl_Addr.UseMnemonic = False
            '
            'LabelControl_Name
            '
            Me.LabelControl_Name.Location = New System.Drawing.Point(45, 48)
            Me.LabelControl_Name.Name = "LabelControl_Name"
            Me.LabelControl_Name.Size = New System.Drawing.Size(262, 13)
            Me.LabelControl_Name.StyleController = Me.LayoutControl1
            Me.LabelControl_Name.TabIndex = 4
            Me.LabelControl_Name.UseMnemonic = False
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(12, 12)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(239, 13)
            Me.LabelControl1.StyleController = Me.LayoutControl1
            Me.LabelControl1.TabIndex = 9
            Me.LabelControl1.Text = "The workshop will be held at the following location"
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup1.GroupBordersVisible = False
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem5, Me.EmptySpaceItem1, Me.EmptySpaceItem_Right, Me.EmptySpaceItem_Left, Me.LayoutControlItem4, Me.EmptySpaceItem4})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(319, 193)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = True
            Me.LayoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LayoutControlItem1.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LayoutControlItem1.Control = Me.LabelControl_Name
            Me.LayoutControlItem1.CustomizationFormText = "LayoutControlItem1"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 36)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(299, 17)
            Me.LayoutControlItem1.Text = "Name"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(30, 13)
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = True
            Me.LayoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LayoutControlItem2.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LayoutControlItem2.Control = Me.LabelControl_Addr
            Me.LayoutControlItem2.CustomizationFormText = "LayoutControlItem2"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 53)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(299, 17)
            Me.LayoutControlItem2.Text = "Addr"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(30, 13)
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = True
            Me.LayoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LayoutControlItem3.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LayoutControlItem3.Control = Me.LabelControl_Phone
            Me.LayoutControlItem3.CustomizationFormText = "LayoutControlItem3"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 70)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(299, 17)
            Me.LayoutControlItem3.Text = "Phone"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(30, 13)
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Control = Me.SimpleButton1
            Me.LayoutControlItem5.CustomizationFormText = "LayoutControlItem5"
            Me.LayoutControlItem5.Location = New System.Drawing.Point(108, 146)
            Me.LayoutControlItem5.Name = "LayoutControlItem5"
            Me.LayoutControlItem5.Size = New System.Drawing.Size(79, 27)
            Me.LayoutControlItem5.Text = "LayoutControlItem5"
            Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem5.TextToControlDistance = 0
            Me.LayoutControlItem5.TextVisible = False
            '
            'EmptySpaceItem1
            '
            Me.EmptySpaceItem1.AllowHotTrack = False
            Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 87)
            Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Size = New System.Drawing.Size(299, 59)
            Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
            Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem_Right
            '
            Me.EmptySpaceItem_Right.AllowHotTrack = False
            Me.EmptySpaceItem_Right.CustomizationFormText = "EmptySpaceItem2"
            Me.EmptySpaceItem_Right.Location = New System.Drawing.Point(187, 146)
            Me.EmptySpaceItem_Right.Name = "EmptySpaceItem_Right"
            Me.EmptySpaceItem_Right.Size = New System.Drawing.Size(112, 27)
            Me.EmptySpaceItem_Right.Text = "EmptySpaceItem_Right"
            Me.EmptySpaceItem_Right.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem_Left
            '
            Me.EmptySpaceItem_Left.AllowHotTrack = False
            Me.EmptySpaceItem_Left.CustomizationFormText = "EmptySpaceItem3"
            Me.EmptySpaceItem_Left.Location = New System.Drawing.Point(0, 146)
            Me.EmptySpaceItem_Left.Name = "EmptySpaceItem_Left"
            Me.EmptySpaceItem_Left.Size = New System.Drawing.Size(108, 27)
            Me.EmptySpaceItem_Left.Text = "EmptySpaceItem_Left"
            Me.EmptySpaceItem_Left.TextSize = New System.Drawing.Size(0, 0)
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.LabelControl1
            Me.LayoutControlItem4.CustomizationFormText = "LayoutControlItem4"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(299, 17)
            Me.LayoutControlItem4.Text = "LayoutControlItem4"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem4.TextToControlDistance = 0
            Me.LayoutControlItem4.TextVisible = False
            '
            'EmptySpaceItem4
            '
            Me.EmptySpaceItem4.AllowHotTrack = False
            Me.EmptySpaceItem4.CustomizationFormText = "EmptySpaceItem4"
            Me.EmptySpaceItem4.Location = New System.Drawing.Point(0, 17)
            Me.EmptySpaceItem4.MaxSize = New System.Drawing.Size(0, 19)
            Me.EmptySpaceItem4.MinSize = New System.Drawing.Size(10, 19)
            Me.EmptySpaceItem4.Name = "EmptySpaceItem4"
            Me.EmptySpaceItem4.Size = New System.Drawing.Size(299, 19)
            Me.EmptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
            Me.EmptySpaceItem4.Text = "EmptySpaceItem4"
            Me.EmptySpaceItem4.TextSize = New System.Drawing.Size(0, 0)
            '
            'Appointments_WorkshopLocation
            '
            Me.AcceptButton = Me.SimpleButton1
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.SimpleButton1
            Me.ClientSize = New System.Drawing.Size(319, 193)
            Me.Controls.Add(Me.LayoutControl1)
            Me.Name = "Appointments_WorkshopLocation"
            Me.Text = "Workshop Location"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem_Right, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem_Left, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Public WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Public WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
        Public WithEvents LabelControl_Phone As DevExpress.XtraEditors.LabelControl
        Public WithEvents LabelControl_Addr As DevExpress.XtraEditors.LabelControl
        Public WithEvents LabelControl_Name As DevExpress.XtraEditors.LabelControl
        Public WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Public WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Public WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Public WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Public WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
        Public WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
        Public WithEvents EmptySpaceItem_Right As DevExpress.XtraLayout.EmptySpaceItem
        Public WithEvents EmptySpaceItem_Left As DevExpress.XtraLayout.EmptySpaceItem
        Public WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Public WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Public WithEvents EmptySpaceItem4 As DevExpress.XtraLayout.EmptySpaceItem
    End Class
End NameSpace