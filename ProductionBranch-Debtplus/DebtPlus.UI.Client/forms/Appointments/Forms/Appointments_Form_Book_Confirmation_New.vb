#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DebtPlus.Letters.Compose
Imports DebtPlus.Events
Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.Svc.Client.Appointments
Imports System.Threading
Imports DevExpress.XtraEditors
Imports DebtPlus.Utils.Format
Imports DebtPlus.LINQ
Imports System.Linq

Namespace forms.Appointments
    Public Class Appointments_Form_Book_Confirmation_New

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Protected record As client_appointment
        Protected OfficeDirections As String = String.Empty
        Protected LetterType As String = Nothing
        Protected bc As BusinessContext = Nothing

        ''' <summary>
        ''' Initialize the form
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(bc As BusinessContext, ByVal record As client_appointment)
            MyClass.New()
            Me.record = record
            Me.bc = bc
            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf Appointments_Book_Confirmation_Form_Load
            AddHandler SimpleButton_Directions.Click, AddressOf SimpleButton_Directions_Click
            AddHandler SimpleButton_No.Click, AddressOf SimpleButton_No_Click
            AddHandler SimpleButton_Yes.Click, AddressOf SimpleButton_Yes_Click
            AddHandler lk_referral.ButtonClick, AddressOf lk_referral_ButtonPressed
            AddHandler lk_referral.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler lk_referral.EditValueChanged, AddressOf lk_referral_EditValueChanged
            AddHandler lk_referral.EditValueChanged, AddressOf FormChanged
            AddHandler TextEdit_partner_code.EditValueChanged, AddressOf FormChanged
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler Load, AddressOf Appointments_Book_Confirmation_Form_Load
            RemoveHandler SimpleButton_Directions.Click, AddressOf SimpleButton_Directions_Click
            RemoveHandler SimpleButton_No.Click, AddressOf SimpleButton_No_Click
            RemoveHandler SimpleButton_Yes.Click, AddressOf SimpleButton_Yes_Click
            RemoveHandler lk_referral.ButtonClick, AddressOf lk_referral_ButtonPressed
            RemoveHandler lk_referral.EditValueChanged, AddressOf lk_referral_EditValueChanged
            RemoveHandler lk_referral.EditValueChanged, AddressOf FormChanged
            RemoveHandler TextEdit_partner_code.EditValueChanged, AddressOf FormChanged
        End Sub

        ''' <summary>
        ''' Control the editing of the partner code value
        ''' </summary>
        Private Sub EnablePartner()

            ' Remove the validation handler for the partner. It will be re-enabled if we have one
            RemoveHandler TextEdit_partner_code.Validating, AddressOf TextEdit_partner_code_Validating

            ' Retrieve the referral source. If missing then there is no referral.
            Dim referral As DebtPlus.LINQ.referred_by = TryCast(lk_referral.GetSelectedDataRow(), DebtPlus.LINQ.referred_by)
            If referral Is Nothing Then
                TextEdit_partner_code.EditValue = Nothing
                TextEdit_partner_code.Enabled = False
                Return
            End If

            ' If the referral does not allow the partner code then remove it
            Dim Validation As String = referral.partnerValidation
            If String.IsNullOrEmpty(Validation) Then
                TextEdit_partner_code.EditValue = Nothing
                TextEdit_partner_code.Enabled = False
                Return
            End If

            ' Enable the partner code entry
            TextEdit_partner_code.Enabled = True

            ' Try to determine if the partner code as entered is valid. If not, clear it.
            Try
                Dim currentValue As String = Convert.ToString(TextEdit_partner_code.EditValue)
                If currentValue Is Nothing Then currentValue = String.Empty

                If Not System.Text.RegularExpressions.Regex.IsMatch(currentValue, "^" + Validation + "$") Then
                    TextEdit_partner_code.EditValue = Nothing
                End If
            Catch
            End Try

            ' Set the referral code value
            Try
                TextEdit_partner_code.Properties.Mask.BeepOnError = True
                TextEdit_partner_code.Properties.Mask.EditMask = Validation
                TextEdit_partner_code.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
                TextEdit_partner_code.Properties.Mask.SaveLiteral = True
                TextEdit_partner_code.Properties.Mask.ShowPlaceHolders = True

                ' Finally, re-enable the validation code validator so that it is checked for the full expression
                AddHandler TextEdit_partner_code.Validating, AddressOf TextEdit_partner_code_Validating
            Catch
            End Try

        End Sub

        ''' <summary>
        ''' Validate the partner code to ensure that it is acceptable for input
        ''' </summary>
        Private Sub TextEdit_partner_code_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs)

            ' No partner code is acceptable for the entry. It will be caught elsewhere
            Dim strCode As String = TryCast(TextEdit_partner_code.EditValue, String)
            If strCode Is Nothing Then
                TextEdit_partner_code.ErrorText = "A value is required for this field"
                Return
            End If

            ' Pass the code through the regular expression routine to see if it is valid. We put the
            ' beginning of string and end of string anchors on the expression to ensure that the entire
            ' string matches the entire expression and not just some sub-string component.
            Try
                If System.Text.RegularExpressions.Regex.IsMatch(strCode, "^" + TextEdit_partner_code.Properties.Mask.EditMask + "$") Then
                    TextEdit_partner_code.ErrorText = String.Empty
                    Return
                End If

                ' The entry is not completely valid. Reject it.
                TextEdit_partner_code.ErrorText = "invalid entry"

            Catch ex As System.Exception
                TextEdit_partner_code.ErrorText = ex.Message
            End Try

            e.Cancel = True
        End Sub

        ''' <summary>
        ''' Process the change in the referral source for the appointment
        ''' </summary>
        Private Sub lk_referral_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            UnRegisterHandlers()
            Try
                EnablePartner()
            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Generate the select statement command for the appointment letter
        ''' </summary>
        Protected Overridable Function RetrieveLetterCode() As String
            If record IsNot Nothing AndAlso record.appt_type.HasValue Then
                Dim q As appt_type = DebtPlus.LINQ.Cache.appt_type.getList().Find(Function(s) s.Id = record.appt_type.Value)
                If q IsNot Nothing Then
                    Return q.create_letter
                End If
            End If
            Return Nothing
        End Function

        ''' <summary>
        ''' Is the form valid for the submission?
        ''' </summary>
        Private Function HasErrors() As Boolean

            ' Ensure that there is no error condition for the partner code
            If Not String.IsNullOrEmpty(TextEdit_partner_code.ErrorText) Then Return True

            Return False
        End Function

        ''' <summary>
        ''' Handle the CLOSE event for the form
        ''' </summary>
        Protected Overridable Sub FormClosingRoutine(ByVal sender As Object, ByVal e As FormClosingEventArgs)

            ' If the form has some errors then we can not accept the input and deny the form closure
            If HasErrors() Then
                e.Cancel = True
                Return
            End If

            UnRegisterHandlers()
            RemoveHandler FormClosing, AddressOf FormClosingRoutine
            Try

                ' There must be an appointment record to continue
                If record Is Nothing Then
                    Return
                End If

                ' Set the callback telephone number
                Dim CallbackNumber As String = DebtPlus.Utils.Format.Strings.DigitsOnly(cbo_callback.Text)
                If CallbackNumber <> String.Empty Then
                    record.callback_ph = CallbackNumber
                End If

                ' Set the referral code
                record.referred_by = DebtPlus.Utils.Nulls.v_Int32(lk_referral.EditValue)
                record.partner = DebtPlus.Utils.Nulls.v_String(TextEdit_partner_code.EditValue)

                ' Complete the processing for the record
                Dim q As client_appointment = bc.client_appointments.Where(Function(ca) record.Id = ca.Id).FirstOrDefault()
                If q IsNot Nothing Then
                    q.previous_appointment = record.previous_appointment
                    q.callback_ph = record.callback_ph
                    q.referred_by = record.referred_by
                    q.partner = record.partner
                    bc.SubmitChanges()
                End If

            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Handle the LOAD event for the form
        ''' </summary>
        Private Sub Appointments_Book_Confirmation_Form_Load(ByVal sender As Object, ByVal e As EventArgs)
            UnRegisterHandlers()

            ' Load the previous placement and size for the form
            LoadPlacement("Client.Appointment.Appointments_Form_Book_Confirmation_New")

            Try
                Dim MyIcon As Icon = Nothing

                If record Is Nothing Then
                    Return
                End If

                LoadCallbacks()

                ' Set the controls to the current record contents
                lk_referral.Properties.DataSource = DebtPlus.LINQ.Cache.referred_by.getList()
                lk_referral.EditValue = record.referred_by
                TextEdit_partner_code.EditValue = record.partner

                ' Find the appointment row to find the office number
                Dim Office As Int32 = record.office.GetValueOrDefault(0)
                If Office > 0 Then
                    Dim q As DebtPlus.LINQ.office = DebtPlus.LINQ.Cache.office.getList().Find(Function(s) s.Id = Office)
                    If q IsNot Nothing Then
                        OfficeDirections = q.directions
                    End If
                End If

                ' Disable the directions button if there are no directions
                If String.IsNullOrEmpty(OfficeDirections) Then
                    SimpleButton_Directions.Enabled = False
                Else
                    SimpleButton_Directions.Enabled = True
                End If

                ' If there is a letter type then ask the user if it should be generated
                LetterType = RetrieveLetterCode()
                If LetterType = String.Empty Then
                    SimpleButton_Yes.Visible = False
                    SimpleButton_No.Text = "&Ok"
                    MyIcon = SystemIcons.Asterisk
                    DoMessageBeep(MessageBeepEnum.Asterisk)
                Else
                    MyIcon = SystemIcons.Question
                    LabelControl1.Text += String.Format("{0}{0}Do you wish to send the client a confirmation letter?",
                                                        Environment.NewLine)
                    DoMessageBeep(MessageBeepEnum.Question)
                End If

                ' Set the bitmap for the item
                PictureBox1.Image = MyIcon.ToBitmap

                ' Enable the partner code update
                EnablePartner()

                ' Enable or disable the buttons based upon the error status
                SimpleButton_No.Enabled = Not HasErrors()
                SimpleButton_Yes.Enabled = Not HasErrors()

                ' Only when we have done everything should we look for the closing event to update the record.
                AddHandler FormClosing, AddressOf FormClosingRoutine
            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Build the list of callback telephone numbers
        ''' </summary>
        Private Sub LoadCallbacks()
            Dim clientID As Int32 = record.client

            ' Load the client's home phone number
            Dim homeTelephone As TelephoneNumber = bc.clients.Join(bc.TelephoneNumbers, Function(c) c.HomeTelephoneID, Function(t) t.Id, Function(c, t) New With {Key .clientID = c.Id, Key .t = t}).Where(Function(x) x.clientID = clientID).Select(Function(x) x.t).FirstOrDefault()
            If homeTelephone IsNot Nothing Then
                AddCallbackNumber(homeTelephone.ToString())
            End If

            ' Load the message telephone number
            Dim msgTelephone As TelephoneNumber = bc.clients.Join(bc.TelephoneNumbers, Function(c) c.MsgTelephoneID, Function(t) t.Id, Function(c, t) New With {Key .clientID = c.Id, Key .t = t}).Where(Function(x) x.clientID = clientID).Select(Function(x) x.t).FirstOrDefault()
            If msgTelephone IsNot Nothing Then
                AddCallbackNumber(msgTelephone.ToString())
            End If

            ' Load the work telephone numbers
            Dim colWorkPhones As System.Collections.Generic.List(Of TelephoneNumber) = bc.peoples.Join(bc.TelephoneNumbers, Function(p) p.WorkTelephoneID, Function(t) t.Id, Function(p, t) New With {Key .clientID = p.Client, Key .t = t}).Where(Function(x) x.clientID = clientID).Select(Function(x) x.t).ToList()
            colWorkPhones.ForEach(Sub(s) AddCallbackNumber(s.ToString()))

            ' Load the cell telephone numbers
            Dim colCellPhones As System.Collections.Generic.List(Of TelephoneNumber) = bc.peoples.Join(bc.TelephoneNumbers, Function(p) p.CellTelephoneID, Function(t) t.Id, Function(p, t) New With {Key .clientID = p.Client, Key .t = t}).Where(Function(x) x.clientID = clientID).Select(Function(x) x.t).ToList()
            colCellPhones.ForEach(Sub(s) AddCallbackNumber(s.ToString()))
        End Sub

        ''' <summary>
        ''' Add an entry to the callback list if it is not duplicated in the list.
        ''' </summary>
        ''' <param name="phoneNumber"></param>
        ''' <remarks></remarks>
        Private Sub AddCallbackNumber(phoneNumber As String)
            If Not cbo_callback.Properties.Items.Contains(phoneNumber) Then
                cbo_callback.Properties.Items.Add(phoneNumber)
            End If
        End Sub

        ''' <summary>
        ''' Show the directions to the office
        ''' </summary>
        Private Sub SimpleButton_Directions_Click(ByVal sender As Object, ByVal e As EventArgs)
            Using frm As New Appointments_Form_OfficeDirections(bc, OfficeDirections)
                frm.ShowDialog()
            End Using
        End Sub

        ''' <summary>
        ''' Process the YES button click event
        ''' </summary>
        Private Sub SimpleButton_Yes_Click(ByVal sender As Object, ByVal e As EventArgs)
            If Not HasErrors() Then
                AppointmentLetters.StartLetter(record, LetterType)
                DialogResult = DialogResult.OK
            End If
        End Sub

        Private Sub SimpleButton_No_Click(ByVal sender As Object, ByVal e As EventArgs)
            If Not HasErrors() Then
                DialogResult = DialogResult.Cancel
            End If
        End Sub

        ''' <summary>
        ''' Handle the clear button on the referral source
        ''' </summary>
        Private Sub lk_referral_ButtonPressed(ByVal sender As Object, ByVal e As ButtonPressedEventArgs)

            UnRegisterHandlers()
            Try
                If e.Button.Kind = ButtonPredefines.Delete Then
                    lk_referral.EditValue = Nothing
                    EnablePartner()
                End If
            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Process a change in the input form
        ''' </summary>
        Private Sub FormChanged(ByVal sender As Object, ByVal e As System.EventArgs)

            ' IF there are errors then do not allow the buttons to be used
            If HasErrors() Then
                SimpleButton_No.Enabled = False
                SimpleButton_Yes.Enabled = False
                Return
            End If

            SimpleButton_No.Enabled = True
            SimpleButton_Yes.Enabled = True
        End Sub
    End Class
End Namespace
