#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DebtPlus.Svc.Client.Appointments

Imports DebtPlus.LINQ
Imports System.Linq

Namespace forms.Appointments
    Public Class Appointments_Form_Book_Confirmation_Reschedule

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        ' Previous appointment for the reschedule event
        Private ReadOnly oldClientAppointment As client_appointment

        ''' <summary>
        ''' Initialize the form
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        ''' <summary>
        ''' Initialize the form with context information and appointments
        ''' </summary>
        Public Sub New(bc As BusinessContext, ByVal record As client_appointment, ByVal oldClientAppointment As client_appointment)
            MyBase.New(bc, record)
            InitializeComponent()
            Me.oldClientAppointment = oldClientAppointment
            AddHandler Load, AddressOf form_load
        End Sub

        Private Sub form_load(sender As Object, e As System.EventArgs)

            ' Load the previous placement and size for the form
            LoadPlacement("Client.Appointment.Appointments_Form_Book_Confirmation_Reschedule")

        End Sub

        ''' <summary>
        ''' Process the CLOSE of the form
        ''' </summary>
        Protected Overrides Sub FormClosingRoutine(ByVal sender As Object, ByVal e As FormClosingEventArgs)
            If record IsNot Nothing AndAlso oldClientAppointment IsNot Nothing Then
                record.previous_appointment = oldClientAppointment.Id
            End If
            MyBase.FormClosingRoutine(sender, e)
        End Sub

        ''' <summary>
        ''' Find the reschedule letter for the appointment
        ''' </summary>
        Protected Overrides Function RetrieveLetterCode() As String
            If record IsNot Nothing AndAlso record.appt_type.HasValue Then
                Dim q As appt_type = DebtPlus.LINQ.Cache.appt_type.getList().Find(Function(s) s.Id = record.appt_type.Value)
                If q IsNot Nothing Then
                    Return q.reschedule_letter
                End If
            End If
            Return Nothing
        End Function
    End Class
End NameSpace
