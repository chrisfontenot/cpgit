Namespace forms.Appointments
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Appointments_Form_Book
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
            Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
            Me.Appointments_Book_Workshop_Control1 = New DebtPlus.UI.Client.forms.Appointments.Appointments_Control_Book_Workshop()
            Me.XtraTabPage_Person1 = New DevExpress.XtraTab.XtraTabPage()
            Me.Appointments_Book_Counseling_Control1 = New DebtPlus.UI.Client.forms.Appointments.Appointments_Control_Book_Counseling()
            Me.Appointments_Control_Workshop_New1 = New DebtPlus.UI.Client.forms.Appointments.Appointments_Control_Workshop_New()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.XtraTabControl1.SuspendLayout()
            Me.XtraTabPage1.SuspendLayout()
            Me.XtraTabPage_Person1.SuspendLayout()
            Me.SuspendLayout()
            '
            'DefaultLookAndFeel1
            '
            Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin"
            '
            'XtraTabControl1
            '
            Me.XtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.XtraTabControl1.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.[True]
            Me.XtraTabControl1.Location = New System.Drawing.Point(0, 0)
            Me.XtraTabControl1.Name = "XtraTabControl1"
            Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage1
            Me.XtraTabControl1.Size = New System.Drawing.Size(576, 318)
            Me.XtraTabControl1.TabIndex = 1
            Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage_Person1})
            '
            'XtraTabPage1
            '
            Me.XtraTabPage1.Controls.Add(Me.Appointments_Book_Workshop_Control1)
            Me.XtraTabPage1.Name = "XtraTabPage1"
            Me.XtraTabPage1.Size = New System.Drawing.Size(568, 288)
            Me.XtraTabPage1.Text = "Workshop"
            '
            'Appointments_Book_Workshop_Control1
            '
            Me.Appointments_Book_Workshop_Control1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.Appointments_Book_Workshop_Control1.Location = New System.Drawing.Point(0, 0)
            Me.Appointments_Book_Workshop_Control1.Name = "Appointments_Book_Workshop_Control1"
            Me.Appointments_Book_Workshop_Control1.Size = New System.Drawing.Size(568, 288)
            Me.Appointments_Book_Workshop_Control1.TabIndex = 0
            '
            'XtraTabPage_Person1
            '
            Me.XtraTabPage_Person1.Controls.Add(Me.Appointments_Book_Counseling_Control1)
            Me.XtraTabPage_Person1.Name = "XtraTabPage_Person1"
            Me.XtraTabPage_Person1.Size = New System.Drawing.Size(568, 288)
            Me.XtraTabPage_Person1.Text = "Counseling"
            '
            'Appointments_Book_Counseling_Control1
            '
            Me.Appointments_Book_Counseling_Control1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.Appointments_Book_Counseling_Control1.Location = New System.Drawing.Point(0, 0)
            Me.Appointments_Book_Counseling_Control1.Name = "Appointments_Book_Counseling_Control1"
            Me.Appointments_Book_Counseling_Control1.Size = New System.Drawing.Size(568, 288)
            Me.Appointments_Book_Counseling_Control1.TabIndex = 1
            '
            'Appointments_Control_Workshop_New1
            '
            Me.Appointments_Control_Workshop_New1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.Appointments_Control_Workshop_New1.Location = New System.Drawing.Point(0, 0)
            Me.Appointments_Control_Workshop_New1.Name = "Appointments_Control_Workshop_New1"
            Me.Appointments_Control_Workshop_New1.Size = New System.Drawing.Size(576, 318)
            Me.Appointments_Control_Workshop_New1.TabIndex = 2
            Me.Appointments_Control_Workshop_New1.Visible = False
            '
            'Appointments_Form_Book
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(576, 318)
            Me.Controls.Add(Me.XtraTabControl1)
            Me.Controls.Add(Me.Appointments_Control_Workshop_New1)
            Me.Name = "Appointments_Form_Book"
            Me.Text = "Appointments"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.XtraTabControl1.ResumeLayout(False)
            Me.XtraTabPage1.ResumeLayout(False)
            Me.XtraTabPage_Person1.ResumeLayout(False)
            Me.ResumeLayout(False)

        End Sub
        Public WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
        Public WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
        Public WithEvents XtraTabPage_Person1 As DevExpress.XtraTab.XtraTabPage
        Public WithEvents Appointments_Book_Counseling_Control1 As Appointments_Control_Book_Counseling
        Public WithEvents Appointments_Book_Workshop_Control1 As Appointments_Control_Book_Workshop
        Public WithEvents Appointments_Control_Workshop_New1 As Appointments_Control_Workshop_New
    End Class
End NameSpace