#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DebtPlus.Letters.Compose
Imports DebtPlus.Svc.Client.Appointments
Imports DebtPlus.Data.Forms
Imports System.Threading

Imports DebtPlus.LINQ
Imports System.Linq

Namespace forms.Appointments
    Public Class Appointments_Form_Cancel
        Private clientID As Int32
        Protected bc As BusinessContext = Nothing

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf Appointments_Cancel_Load
            AddHandler Appointments_List_Control1.Cancel, AddressOf Appointments_List_Control1_Cancel
            AddHandler Appointments_List_Control1.EditItem, AddressOf Appointments_List_Control1_EditItem
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler Load, AddressOf Appointments_Cancel_Load
            RemoveHandler Appointments_List_Control1.Cancel, AddressOf Appointments_List_Control1_Cancel
            RemoveHandler Appointments_List_Control1.EditItem, AddressOf Appointments_List_Control1_EditItem
        End Sub

        Public Sub New(bc As BusinessContext, ByVal clientID As Int32)
            MyClass.New()
            Me.bc = bc
            Me.clientID = clientID
            RegisterHandlers()
        End Sub

        Private Sub Appointments_Cancel_Load(ByVal sender As Object, ByVal e As EventArgs)

            ' Load the previous placement and size for the form
            LoadPlacement("Client.Appointment.Appointments_Form_Cancel")

            Appointments_List_Control1.ReadForm(bc, clientID)
        End Sub

        Private Sub Appointments_List_Control1_Cancel(ByVal sender As Object, ByVal e As EventArgs)
            DialogResult = DialogResult.Cancel
        End Sub

        Private Sub Appointments_List_Control1_EditItem(ByVal sender As Object, ByVal record As client_appointment)

            ' Cancel the appointment
            Dim q As client_appointment = bc.client_appointments.Where(Function(ca) ca.Id = record.Id).FirstOrDefault()
            If q IsNot Nothing Then
                q.workshop_people = 0
                q.status = "C"c
                q.date_updated = DebtPlus.LINQ.BusinessContext.getdate()

                ' Generate a note reflecting the change in the appointment. We do this only for face-to-face appointments, not workshops.
                If q.office.HasValue Then

                    ' Generate the textual names for the fields.
                    Dim officeName As String = If(record.office.HasValue, DebtPlus.LINQ.Cache.office.getList().Where(Function(o) o.Id = record.office.Value).Select(Function(o) o.name).FirstOrDefault(), Nothing)
                    Dim apptTypeName As String = If(record.appt_type.HasValue, DebtPlus.LINQ.Cache.appt_type.getList().Where(Function(apt) apt.Id = record.appt_type.Value).Select(Function(apt) apt.appt_name).FirstOrDefault(), Nothing)

                    Dim counselorNameRecord As DebtPlus.LINQ.Name = If(record.counselor.HasValue, DebtPlus.LINQ.Cache.counselor.getList().Where(Function(c) c.Id = record.counselor.Value).Select(Function(c) c.Name).FirstOrDefault(), Nothing)
                    Dim counselorName As String = If(counselorNameRecord Is Nothing, String.Empty, counselorNameRecord.ToString())

                    ' Ensure that the strings are not null
                    If officeName Is Nothing Then officeName = String.Empty
                    If apptTypeName Is Nothing Then apptTypeName = String.Empty

                    ' Allocate the note structure and write it to the pending tables
                    Dim newNote As DebtPlus.LINQ.client_note = DebtPlus.LINQ.Factory.Manufacture_client_note(record.client, 3)
                    newNote.subject = String.Format("Canceled {0:d} appointment with {1}", record.start_time, counselorName)
                    newNote.note = String.Format("The counselor appointment (#{1:f0}) for {2:MMM d yyyy h:mmtt} was canceled.{0}It is an appointment type of '{3}'{0}The appointment was with {4} at the {5} office.", New Object() {Environment.NewLine, record.Id, record.start_time, apptTypeName, counselorName, officeName})
                    bc.client_notes.InsertOnSubmit(newNote)

                    ' Take the appointment time slot out of the record just in case the user does not really look for "Pending" first.
                    q.appt_time = Nothing
                End If

                ' Submit the changes to mark the appointment as canceled and create the note.
                bc.SubmitChanges()

                ' After the appointment is marked canceled in the databased then we can generate the cancel letter.
                If record.office.HasValue AndAlso record.appt_type.HasValue Then
                    Dim qType As appt_type = DebtPlus.LINQ.Cache.appt_type.getList().Find(Function(s) s.Id = record.appt_type.Value)
                    If qType IsNot Nothing Then

                        ' Retrieve the letter code for the cancellation
                        Dim letterType As String = qType.cancel_letter
                        If Not String.IsNullOrEmpty(letterType) Then
                            If MessageBox.Show(String.Format("The appointment has been canceled.{0}{0}Do you wish to send the client a cancellation letter?", Environment.NewLine), "Operation Completed", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = DialogResult.Yes Then
                                AppointmentLetters.StartLetter(record, letterType)
                            End If
                        End If
                    End If
                End If
            End If

            DialogResult = DialogResult.OK
        End Sub
    End Class
End NameSpace
