#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DebtPlus.Svc.Client.Appointments

Imports DebtPlus.LINQ
Imports System.Linq

Namespace forms.Appointments

    Public Class Appointments_Form_Confirm
        Private clientId As Int32
        Protected bc As BusinessContext = Nothing

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        ''' <summary>
        ''' Initialize the form
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        ''' <summary>
        ''' Initialize the form with context information
        ''' </summary>
        Public Sub New(bc As BusinessContext, ByVal clientID As Int32)
            MyClass.New()
            Me.clientId = clientID
            Me.bc = bc
            RegisterHandlers()
            Appointments_List_Control1.Text = "Select the appointment that you wish to confirm."
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf Appointments_Confirm_Load
            AddHandler Appointments_List_Control1.Cancel, AddressOf Appointments_List_Control1_Cancel
            AddHandler Appointments_List_Control1.EditItem, AddressOf Appointments_List_Control1_EditItem
            AddHandler Appointments_Control_Confirmation1.Cancelled, AddressOf Appointments_Control_Confirmation1_Cancelled
            AddHandler Appointments_Control_Confirmation1.Completed, AddressOf Appointments_Control_Confirmation1_Completed
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler Load, AddressOf Appointments_Confirm_Load
            RemoveHandler Appointments_List_Control1.Cancel, AddressOf Appointments_List_Control1_Cancel
            RemoveHandler Appointments_List_Control1.EditItem, AddressOf Appointments_List_Control1_EditItem
            RemoveHandler Appointments_Control_Confirmation1.Cancelled, AddressOf Appointments_Control_Confirmation1_Cancelled
            RemoveHandler Appointments_Control_Confirmation1.Completed, AddressOf Appointments_Control_Confirmation1_Completed
        End Sub

        ''' <summary>
        ''' Process the FORM LOAD event
        ''' </summary>
        Private Sub Appointments_Confirm_Load(ByVal sender As Object, ByVal e As EventArgs)

            ' Load the previous placement and size for the form
            LoadPlacement("Client.Appointment.Appointments_Form_Confirm")

            Appointments_List_Control1.ReadForm(bc, clientId)
        End Sub

        ''' <summary>
        ''' Process the CANCEL event
        ''' </summary>
        Private Sub Appointments_List_Control1_Cancel(ByVal sender As Object, ByVal e As EventArgs)
            DialogResult = DialogResult.Cancel
        End Sub

        ''' <summary>
        ''' Process the SELECT event
        ''' </summary>
        Private Sub Appointments_List_Control1_EditItem(ByVal sender As Object, ByVal record As client_appointment)

            ' Hide the selection control and enable the new appointment control
            Appointments_List_Control1.Visible = False
            Appointments_List_Control1.SendToBack()

            ' Display the information to retrieve the new appointment for the client.
            Appointments_Control_Confirmation1.Visible = True
            Appointments_Control_Confirmation1.BringToFront()
            Appointments_Control_Confirmation1.ReadForm(bc, record)
        End Sub

        ''' <summary>
        ''' The confirmation dialog was canceled
        ''' </summary>
        Private Sub Appointments_Control_Confirmation1_Cancelled(ByVal sender As Object, ByVal e As EventArgs)

            ' Hide the information to retrieve the new appointment for the client.
            Appointments_Control_Confirmation1.Visible = False
            Appointments_Control_Confirmation1.SendToBack()

            ' Show the selection control and enable the new appointment control
            Appointments_List_Control1.Visible = True
            Appointments_List_Control1.BringToFront()
        End Sub

        ''' <summary>
        ''' The confirmation dialog was OKed
        ''' </summary>
        Private Sub Appointments_Control_Confirmation1_Completed(ByVal sender As Object, ByVal e As EventArgs)
            DialogResult = DialogResult.OK
        End Sub
    End Class
End NameSpace
