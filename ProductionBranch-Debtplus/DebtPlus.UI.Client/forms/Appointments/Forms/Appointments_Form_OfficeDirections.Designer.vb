Namespace forms.Appointments
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Appointments_Form_OfficeDirections
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
            Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'MemoEdit1
            '
            Me.MemoEdit1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                                          Or System.Windows.Forms.AnchorStyles.Left) _
                                         Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.MemoEdit1.Location = New System.Drawing.Point(13, 13)
            Me.MemoEdit1.Name = "MemoEdit1"
            Me.MemoEdit1.Properties.ScrollBars = System.Windows.Forms.ScrollBars.Both
            Me.MemoEdit1.Size = New System.Drawing.Size(339, 106)
            Me.MemoEdit1.TabIndex = 1
            '
            'SimpleButton1
            '
            Me.SimpleButton1.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton1.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.SimpleButton1.Location = New System.Drawing.Point(145, 133)
            Me.SimpleButton1.Name = "SimpleButton1"
            Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton1.TabIndex = 0
            Me.SimpleButton1.Text = "&OK"
            '
            'Appointments_Form_OfficeDirections
            '
            Me.AcceptButton = Me.SimpleButton1
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.SimpleButton1
            Me.ClientSize = New System.Drawing.Size(364, 168)
            Me.Controls.Add(Me.SimpleButton1)
            Me.Controls.Add(Me.MemoEdit1)
            Me.MinimizeBox = False
            Me.Name = "Appointments_Form_OfficeDirections"
            Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show
            Me.Text = "Office Directions"
            Me.TopMost = True
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Public WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
        Public WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    End Class
End NameSpace