#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.Svc.Client.Appointments
Imports DevExpress.XtraTab
Imports DebtPlus.Data.Forms
Imports DebtPlus.LINQ
Imports System.Linq

Namespace forms.Appointments

    Public Class Appointments_Form_Book

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Protected clientID As Int32 = 0
        Protected partnerCode As String = Nothing
        Protected referralSource As Int32? = Nothing
        Protected bc As BusinessContext = Nothing

        Public Sub New(bc As BusinessContext, ByVal clientID As Int32, ByVal partnerCode As String, ByVal referralSource As Int32?)
            MyClass.New()
            Me.clientID = clientID
            Me.partnerCode = partnerCode
            Me.referralSource = referralSource
            Me.bc = bc

            RegisterHandlers()
        End Sub

        Public Sub New(bc As BusinessContext, ByVal clientID As Int32)
            MyClass.New(bc, clientID, Nothing, DebtPlus.LINQ.Cache.referred_by.getDefault())
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf Appointments_Book_Form_Load
            AddHandler Appointments_Control_Workshop_New1.Cancelled, AddressOf Appointments_Control_Workshop_New1_Cancelled
            AddHandler Appointments_Book_Workshop_Control1.EditItem, AddressOf Appointments_Book_Workshop_Control1_EditItem
            AddHandler Appointments_Book_Counseling_Control1.Completed, AddressOf Appointments_Book_Counseling_Control1_Completed
            AddHandler Appointments_Control_Workshop_New1.Completed, AddressOf Appointments_Control_Workshop_New1_Completed
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler Load, AddressOf Appointments_Book_Form_Load
            RemoveHandler Appointments_Control_Workshop_New1.Cancelled, AddressOf Appointments_Control_Workshop_New1_Cancelled
            RemoveHandler Appointments_Book_Workshop_Control1.EditItem, AddressOf Appointments_Book_Workshop_Control1_EditItem
            RemoveHandler Appointments_Book_Counseling_Control1.Completed, AddressOf Appointments_Book_Counseling_Control1_Completed
            RemoveHandler Appointments_Control_Workshop_New1.Completed, AddressOf Appointments_Control_Workshop_New1_Completed
        End Sub

        Private Sub XtraTabControl1_SelectedPageChanged(ByVal sender As Object, ByVal e As TabPageChangedEventArgs)
            SwitchPages(XtraTabControl1.TabPages.IndexOf(e.Page))
        End Sub

        Private Sub SwitchPages(ByVal NewIndex As Int32)
            Select Case NewIndex

                Case 0 ' workshop
                    Appointments_Book_Workshop_Control1.ReadForm(bc)

                Case 1 ' counseling
                    '' OCSNS-44, get referred by value used while creating the client
                    Dim referred_by As Integer? = referralSource

                    Dim client As DebtPlus.LINQ.client = bc.clients.Where(Function(c) c.Id = clientID).FirstOrDefault()
                    If Not client Is Nothing AndAlso client.referred_by.HasValue Then
                        referred_by = client.referred_by
                    End If

                    Dim clientAppointment As DebtPlus.LINQ.client_appointment = DebtPlus.LINQ.Factory.Manufacture_client_appointment(clientID)
                    clientAppointment.referred_by = referred_by
                    clientAppointment.partner = partnerCode
                    Appointments_Book_Counseling_Control1.ReadForm(bc, clientAppointment)
                Case Else
            End Select
        End Sub

        Private Sub Appointments_Book_Form_Load(ByVal sender As Object, ByVal e As EventArgs)

            ' Load the previous placement and size for the form
            LoadPlacement("Client.Appointment.Appointments_Form_Book")

            XtraTabControl1.SelectedTabPageIndex = 1
            SwitchPages(1)
            AddHandler XtraTabControl1.SelectedPageChanged, AddressOf XtraTabControl1_SelectedPageChanged
        End Sub

        Private Sub Appointments_Book_Workshop_Control1_EditItem(ByVal sender As Object, ByVal record As DebtPlus.LINQ.xpr_appt_workshop_listResult)
            XtraTabControl1.SendToBack()
            XtraTabControl1.Visible = False

            Appointments_Control_Workshop_New1.BringToFront()
            Appointments_Control_Workshop_New1.Visible = True

            ' Create a new appointment with some of the items defined that are not passed along directly
            Dim clientAppointment As DebtPlus.LINQ.client_appointment
            clientAppointment = bc.client_appointments.Where(Function(ca) ca.workshop.GetValueOrDefault() = record.workshop.GetValueOrDefault() AndAlso ca.status = "P" AndAlso ca.client = clientID).FirstOrDefault()
            If clientAppointment IsNot Nothing Then
                Appointments_Control_Workshop_New1.ReadForm(bc, clientAppointment, record)
                Return
            End If

            '' OCSNS-44, get referred by value used while creating the client
            Dim referred_by As Integer? = referralSource

            Dim client As DebtPlus.LINQ.client = bc.clients.Where(Function(c) c.Id = clientID).FirstOrDefault()

            If Not client Is Nothing AndAlso client.referred_by.HasValue Then
                referred_by = client.referred_by
            End If

            ' Create a new appointment for the record.
            clientAppointment = DebtPlus.LINQ.Factory.Manufacture_client_appointment(clientID)
            clientAppointment.referred_by = referred_by
            clientAppointment.partner = partnerCode

            ' The appointment routine will supply the other items when it creates the appointment
            Appointments_Control_Workshop_New1.ReadForm(bc, clientAppointment, record)
        End Sub

        Private Sub Appointments_Control_Workshop_New1_Cancelled(ByVal sender As Object, ByVal e As EventArgs)

            Appointments_Control_Workshop_New1.SendToBack()
            Appointments_Control_Workshop_New1.Visible = False

            XtraTabControl1.BringToFront()
            XtraTabControl1.Visible = True
        End Sub

        Protected Overridable Sub Appointments_Book_Counseling_Control1_Completed(ByVal sender As Object, ByVal clientAppointment As client_appointment)

            ' Indicate the appointment is booked and if a referral result is desired
            Using frm As New Appointments_Form_Book_Confirmation_New(bc, clientAppointment)
                frm.ShowDialog()
            End Using

            ' Indicate that we have completed processing.
            DialogResult = DialogResult.OK
        End Sub

        Protected Overridable Sub Appointments_Control_Workshop_New1_Completed(ByVal sender As Object, ByVal clientAppointment As client_appointment)

            ' Display a message that the appointment has been booked
            MessageBox.Show("The workshop appointment has been booked.", "Booking successful", MessageBoxButtons.OK, MessageBoxIcon.Information)
            DialogResult = DialogResult.OK
        End Sub
    End Class
End NameSpace
