#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DebtPlus.Svc.Client.Appointments

Imports DebtPlus.LINQ
Imports System.Linq

Namespace forms.Appointments
    Public Class Appointments_Form_Reschedule
        Private clientID As Int32 = -1
        Private oldAppointment As client_appointment = Nothing
        Protected bc As BusinessContext = Nothing

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        ''' <summary>
        ''' Initialize the form
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(bc As BusinessContext, ByVal clientID As Int32)
            MyClass.New()
            Me.clientID = clientID
            Me.bc = bc
            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf Appointments_Reschedule_Load
            AddHandler Appointments_Control_List_NoWorkshop1.Cancel, AddressOf Appointments_List_Control1_Cancel
            AddHandler Appointments_Control_List_NoWorkshop1.EditItem, AddressOf Appointments_Control_List_NoWorkshop1_EditItem
            AddHandler Appointments_Book_Counseling_Control1.Completed, AddressOf Appointments_Book_Counseling_Control1_Completed
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler Load, AddressOf Appointments_Reschedule_Load
            RemoveHandler Appointments_Control_List_NoWorkshop1.Cancel, AddressOf Appointments_List_Control1_Cancel
            RemoveHandler Appointments_Control_List_NoWorkshop1.EditItem, AddressOf Appointments_Control_List_NoWorkshop1_EditItem
            RemoveHandler Appointments_Book_Counseling_Control1.Completed, AddressOf Appointments_Book_Counseling_Control1_Completed
        End Sub

        ''' <summary>
        ''' Process the FORM LOAD event
        ''' </summary>
        Private Sub Appointments_Reschedule_Load(ByVal sender As Object, ByVal e As EventArgs)

            ' Load the previous placement and size for the form
            LoadPlacement("Client.Appointment.Appointments_Form_Reschedule")

            If clientID >= 0 Then
                Appointments_Control_List_NoWorkshop1.ReadForm(bc, clientID)
            End If
        End Sub

        Private Sub Appointments_List_Control1_Cancel(ByVal sender As Object, ByVal e As EventArgs)
            DialogResult = DialogResult.Cancel
        End Sub

        Private Sub Appointments_Control_List_NoWorkshop1_EditItem(ByVal sender As Object, ByVal e As client_appointment)
            If e IsNot Nothing Then
                oldAppointment = e

                ' Hide the selection control and enable the new appointment control
                Appointments_Control_List_NoWorkshop1.Visible = False
                Appointments_Control_List_NoWorkshop1.SendToBack()

                ' Display the information to retrieve the new appointment for the client.
                Appointments_Book_Counseling_Control1.Visible = True
                Appointments_Book_Counseling_Control1.BringToFront()

                ' Create a new appointment for the booking
                Dim clientAppointment As DebtPlus.LINQ.client_appointment = DebtPlus.LINQ.Factory.Manufacture_client_appointment(clientID)
                clientAppointment.referred_by = DebtPlus.LINQ.Cache.referred_by.getDefault()
                clientAppointment.partner = Nothing

                ' If there is a previous appointment then copy some of the items to the new appointment
                If oldAppointment IsNot Nothing Then
                    clientAppointment.previous_appointment = oldAppointment.Id
                    clientAppointment.partner = oldAppointment.partner
                    clientAppointment.referred_by = oldAppointment.referred_by
                End If

                Appointments_Book_Counseling_Control1.ReadForm(bc, clientAppointment)
            End If
        End Sub

        Private Sub Appointments_Book_Counseling_Control1_Completed(ByVal sender As Object, ByVal clientAppointment As client_appointment)

            ' Show the confirmation information
            Using frm As New Appointments_Form_Book_Confirmation_Reschedule(bc, clientAppointment, oldAppointment)
                frm.ShowDialog()
            End Using

            ' Indicate that the form should terminate at this point
            DialogResult = DialogResult.OK
        End Sub
    End Class
End NameSpace
