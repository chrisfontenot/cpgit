Namespace forms.Appointments
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Appointments_Form_Book_Confirmation_New
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Me.PictureBox1 = New System.Windows.Forms.PictureBox()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
            Me.DxValidationProvider1 = New DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(Me.components)
            Me.SimpleButton_Yes = New DevExpress.XtraEditors.SimpleButton()
            Me.SimpleButton_No = New DevExpress.XtraEditors.SimpleButton()
            Me.SimpleButton_Directions = New DevExpress.XtraEditors.SimpleButton()
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
            Me.cbo_callback = New DevExpress.XtraEditors.ComboBoxEdit()
            Me.lk_referral = New DevExpress.XtraEditors.LookUpEdit()
            Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
            Me.TextEdit_partner_code = New DevExpress.XtraEditors.TextEdit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DxValidationProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.cbo_callback.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lk_referral.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_partner_code.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'PictureBox1
            '
            Me.PictureBox1.Location = New System.Drawing.Point(12, 25)
            Me.PictureBox1.Margin = New System.Windows.Forms.Padding(0)
            Me.PictureBox1.Name = "PictureBox1"
            Me.PictureBox1.Size = New System.Drawing.Size(32, 32)
            Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
            Me.PictureBox1.TabIndex = 0
            Me.PictureBox1.TabStop = False
            '
            'LabelControl1
            '
            Me.LabelControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom
            Me.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl1.Location = New System.Drawing.Point(49, 2)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(304, 55)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "The appointment has been scheduled."
            Me.LabelControl1.UseMnemonic = False
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(12, 101)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(75, 13)
            Me.LabelControl2.TabIndex = 3
            Me.LabelControl2.Text = "Referral Source"
            '
            'SimpleButton_Yes
            '
            Me.SimpleButton_Yes.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_Yes.Location = New System.Drawing.Point(62, 159)
            Me.SimpleButton_Yes.Name = "SimpleButton_Yes"
            Me.SimpleButton_Yes.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Yes.TabIndex = 7
            Me.SimpleButton_Yes.Text = "&Yes"
            '
            'SimpleButton_No
            '
            Me.SimpleButton_No.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_No.Location = New System.Drawing.Point(143, 159)
            Me.SimpleButton_No.Name = "SimpleButton_No"
            Me.SimpleButton_No.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_No.TabIndex = 8
            Me.SimpleButton_No.Text = "&No"
            '
            'SimpleButton_Directions
            '
            Me.SimpleButton_Directions.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_Directions.Location = New System.Drawing.Point(249, 159)
            Me.SimpleButton_Directions.Name = "SimpleButton_Directions"
            Me.SimpleButton_Directions.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Directions.TabIndex = 9
            Me.SimpleButton_Directions.Text = "&Directions..."
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(12, 75)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(103, 13)
            Me.LabelControl3.TabIndex = 1
            Me.LabelControl3.Text = "Callback Telephone #"
            '
            'cbo_callback
            '
            Me.cbo_callback.Location = New System.Drawing.Point(121, 72)
            Me.cbo_callback.Name = "cbo_callback"
            Me.cbo_callback.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.cbo_callback.Size = New System.Drawing.Size(232, 20)
            Me.cbo_callback.TabIndex = 2
            '
            'lk_referral
            '
            Me.lk_referral.Location = New System.Drawing.Point(121, 98)
            Me.lk_referral.Name = "lk_referral"
            Me.lk_referral.Properties.ActionButtonIndex = 1
            Me.lk_referral.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete), New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lk_referral.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.lk_referral.Properties.DisplayMember = "description"
            Me.lk_referral.Properties.NullText = "Please choose one"
            Me.lk_referral.Properties.ShowFooter = False
            Me.lk_referral.Properties.ShowHeader = False
            Me.lk_referral.Properties.SortColumnIndex = 1
            Me.lk_referral.Properties.ValueMember = "Id"
            Me.lk_referral.Size = New System.Drawing.Size(232, 20)
            Me.lk_referral.TabIndex = 4
            '
            'LabelControl4
            '
            Me.LabelControl4.Location = New System.Drawing.Point(13, 129)
            Me.LabelControl4.Name = "LabelControl4"
            Me.LabelControl4.Size = New System.Drawing.Size(75, 13)
            Me.LabelControl4.TabIndex = 5
            Me.LabelControl4.Text = "Partner Code #"
            '
            'TextEdit_partner_code
            '
            Me.TextEdit_partner_code.Location = New System.Drawing.Point(121, 125)
            Me.TextEdit_partner_code.Name = "TextEdit_partner_code"
            Me.TextEdit_partner_code.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.TextEdit_partner_code.Properties.Mask.BeepOnError = True
            Me.TextEdit_partner_code.Properties.Mask.EditMask = "[A-Z0-9]\d\d*"
            Me.TextEdit_partner_code.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.TextEdit_partner_code.Properties.MaxLength = 50
            Me.TextEdit_partner_code.Size = New System.Drawing.Size(232, 20)
            Me.TextEdit_partner_code.TabIndex = 6
            '
            'Appointments_Form_Book_Confirmation_New
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(365, 194)
            Me.Controls.Add(Me.TextEdit_partner_code)
            Me.Controls.Add(Me.LabelControl4)
            Me.Controls.Add(Me.lk_referral)
            Me.Controls.Add(Me.cbo_callback)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.SimpleButton_Directions)
            Me.Controls.Add(Me.SimpleButton_No)
            Me.Controls.Add(Me.SimpleButton_Yes)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.PictureBox1)
            Me.Name = "Appointments_Form_Book_Confirmation_New"
            Me.Text = "Appointment Confirmed"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DxValidationProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.cbo_callback.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lk_referral.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_partner_code.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Protected Friend WithEvents DxValidationProvider1 As DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider
        Protected Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Protected Friend WithEvents cbo_callback As DevExpress.XtraEditors.ComboBoxEdit
        Protected Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Protected Friend WithEvents lk_referral As DevExpress.XtraEditors.LookUpEdit
        Protected Friend WithEvents SimpleButton_Yes As DevExpress.XtraEditors.SimpleButton
        Protected Friend WithEvents SimpleButton_No As DevExpress.XtraEditors.SimpleButton
        Protected Friend WithEvents SimpleButton_Directions As DevExpress.XtraEditors.SimpleButton
        Protected Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Protected Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
        Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents TextEdit_partner_code As DevExpress.XtraEditors.TextEdit
    End Class
End NameSpace