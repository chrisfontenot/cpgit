#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DebtPlus.Svc.Client.Appointments

Imports DebtPlus.LINQ
Imports System.Linq

Namespace forms.Appointments

    Public Class Appointments_Form_Walkin
        Protected bc As BusinessContext = Nothing
        Protected clientID As Int32

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        ''' <summary>
        ''' Initialize the form
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(bc As BusinessContext, ByVal clientID As Int32)
            MyClass.New()
            Me.bc = bc
            Me.clientID = clientID
            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf Appointments_Form_Walkin_Load
            AddHandler Appointments_Control_Walkin1.Cancelled, AddressOf Appointments_Control_Walkin1_Cancelled
            AddHandler Appointments_Control_Walkin1.Completed, AddressOf Appointments_Control_Walkin1_Completed
        End Sub

        Private Sub UnregisterHandlers()
            RemoveHandler Load, AddressOf Appointments_Form_Walkin_Load
            RemoveHandler Appointments_Control_Walkin1.Cancelled, AddressOf Appointments_Control_Walkin1_Cancelled
            RemoveHandler Appointments_Control_Walkin1.Completed, AddressOf Appointments_Control_Walkin1_Completed
        End Sub

        ''' <summary>
        ''' Process the load event
        ''' </summary>
        Private Sub Appointments_Form_Walkin_Load(ByVal sender As Object, ByVal e As EventArgs)
            Appointments_Control_Walkin1.ReadForm(bc, clientID)
        End Sub

        ''' <summary>
        ''' Handle the closing events for the form
        ''' </summary>
        Private Sub Appointments_Control_Walkin1_Cancelled(ByVal sender As Object, ByVal e As EventArgs)
            DialogResult = DialogResult.Cancel
        End Sub

        Private Sub Appointments_Control_Walkin1_Completed(ByVal Sender As Object, ByVal e As EventArgs)
            DialogResult = DialogResult.OK
        End Sub
    End Class
End NameSpace
