﻿#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DebtPlus.Letters.Compose
Imports DebtPlus.Events
Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.Svc.Client.Appointments
Imports System.Threading
Imports DevExpress.XtraEditors
Imports DebtPlus.Utils.Format
Imports DebtPlus.LINQ
Imports System.Linq

Namespace forms.Appointments
    Public Class AppointmentLetters
        Private ReadOnly clientAppointment As client_appointment
        Private ReadOnly letterType As String

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Public Sub New(ByVal clientAppointment As DebtPlus.LINQ.client_appointment, ByVal letterType As String)
            Me.clientAppointment = clientAppointment
            Me.letterType = letterType
        End Sub

        ''' <summary>
        ''' Start generating the letter image
        ''' </summary>
        Public Shared Sub StartLetter(ByVal clientAppointment As client_appointment, ByVal letterType As String)
            Dim clientLetter As AppointmentLetters = New AppointmentLetters(clientAppointment, letterType)
            Dim thrd As New Thread(AddressOf clientLetter.LetterThread)
            thrd.Name = "Letters"
            thrd.SetApartmentState(ApartmentState.STA)
            thrd.Start()
        End Sub

        ''' <summary>
        ''' Process the letter thread routine to generate the letter
        ''' </summary>
        Public Sub LetterThread(ByVal obj As Object)
            Using ltr As New Composition(letterType)
                AddHandler ltr.GetValue, AddressOf LetterThread_GetValue
                ltr.PrintLetter()
                RemoveHandler ltr.GetValue, AddressOf LetterThread_GetValue
            End Using
        End Sub

        ''' <summary>
        ''' Supply values for the parameters on the letter
        ''' </summary>
        Public Sub LetterThread_GetValue(ByVal Sender As Object, ByVal value As DebtPlus.Events.ParameterValueEventArgs)

            ' Return the client ID to the letter thread if needed
            If String.Compare(value.Name, ParameterValueEventArgs.name_client, True) = 0 Then
                value.Value = clientAppointment.client

                ' Return the appointment ID to the letter thread if needed
            ElseIf String.Compare(value.Name, ParameterValueEventArgs.name_appointment, True) = 0 Then
                value.Value = clientAppointment.Id

            End If
        End Sub
    End Class
End Namespace
