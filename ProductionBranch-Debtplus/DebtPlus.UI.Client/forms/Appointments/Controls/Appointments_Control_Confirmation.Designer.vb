Namespace forms.Appointments
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Appointments_Control_Confirmation
        Inherits DevExpress.XtraEditors.XtraUserControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
            Me.SimpleButton_OK = New DevExpress.XtraEditors.SimpleButton()
            Me.SimpleButton_Cancel = New DevExpress.XtraEditors.SimpleButton()
            Me.lkCounselor = New DevExpress.XtraEditors.LookUpEdit()
            Me.lkConfirmation = New DevExpress.XtraEditors.LookUpEdit()
            Me.lkReferredBy = New DevExpress.XtraEditors.LookUpEdit()
            Me.LabelControl_Priority = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl_Status = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl_StartTime = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl_Language = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl_HomePhone = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl_Client = New DevExpress.XtraEditors.LabelControl()
            CType(Me.lkCounselor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lkConfirmation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lkReferredBy.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(4, 4)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(27, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Client"
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(4, 23)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(60, 13)
            Me.LabelControl2.TabIndex = 1
            Me.LabelControl2.Text = "Home Phone"
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(4, 42)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(93, 13)
            Me.LabelControl3.TabIndex = 2
            Me.LabelControl3.Text = "Language Required"
            '
            'LabelControl4
            '
            Me.LabelControl4.Location = New System.Drawing.Point(4, 61)
            Me.LabelControl4.Name = "LabelControl4"
            Me.LabelControl4.Size = New System.Drawing.Size(70, 13)
            Me.LabelControl4.TabIndex = 3
            Me.LabelControl4.Text = "Date And Time"
            '
            'LabelControl5
            '
            Me.LabelControl5.Location = New System.Drawing.Point(4, 80)
            Me.LabelControl5.Name = "LabelControl5"
            Me.LabelControl5.Size = New System.Drawing.Size(31, 13)
            Me.LabelControl5.TabIndex = 4
            Me.LabelControl5.Text = "Status"
            '
            'LabelControl6
            '
            Me.LabelControl6.Location = New System.Drawing.Point(4, 99)
            Me.LabelControl6.Name = "LabelControl6"
            Me.LabelControl6.Size = New System.Drawing.Size(34, 13)
            Me.LabelControl6.TabIndex = 5
            Me.LabelControl6.Text = "Priority"
            '
            'LabelControl7
            '
            Me.LabelControl7.Location = New System.Drawing.Point(4, 127)
            Me.LabelControl7.Name = "LabelControl7"
            Me.LabelControl7.Size = New System.Drawing.Size(48, 13)
            Me.LabelControl7.TabIndex = 6
            Me.LabelControl7.Text = "Counselor"
            '
            'LabelControl8
            '
            Me.LabelControl8.Location = New System.Drawing.Point(4, 153)
            Me.LabelControl8.Name = "LabelControl8"
            Me.LabelControl8.Size = New System.Drawing.Size(61, 13)
            Me.LabelControl8.TabIndex = 7
            Me.LabelControl8.Text = "Confirmation"
            '
            'LabelControl9
            '
            Me.LabelControl9.Location = New System.Drawing.Point(4, 179)
            Me.LabelControl9.Name = "LabelControl9"
            Me.LabelControl9.Size = New System.Drawing.Size(75, 13)
            Me.LabelControl9.TabIndex = 8
            Me.LabelControl9.Text = "Referral Source"
            '
            'SimpleButton_OK
            '
            Me.SimpleButton_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_OK.Location = New System.Drawing.Point(69, 227)
            Me.SimpleButton_OK.Name = "SimpleButton_OK"
            Me.SimpleButton_OK.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.TabIndex = 9
            Me.SimpleButton_OK.Text = "&OK"
            '
            'SimpleButton_Cancel
            '
            Me.SimpleButton_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_Cancel.Location = New System.Drawing.Point(183, 227)
            Me.SimpleButton_Cancel.Name = "SimpleButton_Cancel"
            Me.SimpleButton_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.TabIndex = 10
            Me.SimpleButton_Cancel.Text = "&Cancel"
            '
            'lkCounselor
            '
            Me.lkCounselor.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lkCounselor.Location = New System.Drawing.Point(103, 124)
            Me.lkCounselor.Name = "lkCounselor"
            Me.lkCounselor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lkCounselor.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.lkCounselor.Properties.DisplayMember = "Name"
            Me.lkCounselor.Properties.NullText = ""
            Me.lkCounselor.Properties.ShowFooter = False
            Me.lkCounselor.Properties.ShowHeader = False
            Me.lkCounselor.Properties.ShowLines = False
            Me.lkCounselor.Properties.SortColumnIndex = 1
            Me.lkCounselor.Properties.ValueMember = "Id"
            Me.lkCounselor.Size = New System.Drawing.Size(220, 20)
            Me.lkCounselor.TabIndex = 11
            '
            'lkConfirmation
            '
            Me.lkConfirmation.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lkConfirmation.Location = New System.Drawing.Point(103, 150)
            Me.lkConfirmation.Name = "lkConfirmation"
            Me.lkConfirmation.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lkConfirmation.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.lkConfirmation.Properties.DisplayMember = "description"
            Me.lkConfirmation.Properties.NullText = ""
            Me.lkConfirmation.Properties.ShowFooter = False
            Me.lkConfirmation.Properties.ShowHeader = False
            Me.lkConfirmation.Properties.ShowLines = False
            Me.lkConfirmation.Properties.SortColumnIndex = 1
            Me.lkConfirmation.Properties.ValueMember = "Id"
            Me.lkConfirmation.Size = New System.Drawing.Size(220, 20)
            Me.lkConfirmation.TabIndex = 12
            '
            'lkReferredBy
            '
            Me.lkReferredBy.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lkReferredBy.Location = New System.Drawing.Point(103, 176)
            Me.lkReferredBy.Name = "lkReferredBy"
            Me.lkReferredBy.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lkReferredBy.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.lkReferredBy.Properties.DisplayMember = "description"
            Me.lkReferredBy.Properties.NullText = ""
            Me.lkReferredBy.Properties.ShowFooter = False
            Me.lkReferredBy.Properties.ShowHeader = False
            Me.lkReferredBy.Properties.ShowLines = False
            Me.lkReferredBy.Properties.SortColumnIndex = 1
            Me.lkReferredBy.Properties.ValueMember = "Id"
            Me.lkReferredBy.Size = New System.Drawing.Size(220, 20)
            Me.lkReferredBy.TabIndex = 13
            '
            'LabelControl_Priority
            '
            Me.LabelControl_Priority.Location = New System.Drawing.Point(103, 99)
            Me.LabelControl_Priority.Name = "LabelControl_Priority"
            Me.LabelControl_Priority.Size = New System.Drawing.Size(34, 13)
            Me.LabelControl_Priority.TabIndex = 19
            Me.LabelControl_Priority.Text = "Priority"
            '
            'LabelControl_Status
            '
            Me.LabelControl_Status.Location = New System.Drawing.Point(103, 80)
            Me.LabelControl_Status.Name = "LabelControl_Status"
            Me.LabelControl_Status.Size = New System.Drawing.Size(31, 13)
            Me.LabelControl_Status.TabIndex = 18
            Me.LabelControl_Status.Text = "Status"
            '
            'LabelControl_StartTime
            '
            Me.LabelControl_StartTime.Location = New System.Drawing.Point(103, 61)
            Me.LabelControl_StartTime.Name = "LabelControl_StartTime"
            Me.LabelControl_StartTime.Size = New System.Drawing.Size(70, 13)
            Me.LabelControl_StartTime.TabIndex = 17
            Me.LabelControl_StartTime.Text = "Date And Time"
            '
            'LabelControl_Language
            '
            Me.LabelControl_Language.Location = New System.Drawing.Point(103, 42)
            Me.LabelControl_Language.Name = "LabelControl_Language"
            Me.LabelControl_Language.Size = New System.Drawing.Size(93, 13)
            Me.LabelControl_Language.TabIndex = 16
            Me.LabelControl_Language.Text = "Language Required"
            '
            'LabelControl_HomePhone
            '
            Me.LabelControl_HomePhone.Location = New System.Drawing.Point(103, 23)
            Me.LabelControl_HomePhone.Name = "LabelControl_HomePhone"
            Me.LabelControl_HomePhone.Size = New System.Drawing.Size(60, 13)
            Me.LabelControl_HomePhone.TabIndex = 15
            Me.LabelControl_HomePhone.Text = "Home Phone"
            '
            'LabelControl_Client
            '
            Me.LabelControl_Client.Location = New System.Drawing.Point(103, 4)
            Me.LabelControl_Client.Name = "LabelControl_Client"
            Me.LabelControl_Client.Size = New System.Drawing.Size(27, 13)
            Me.LabelControl_Client.TabIndex = 14
            Me.LabelControl_Client.Text = "Client"
            '
            'Appointments_Control_Confirmation
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.LabelControl_Priority)
            Me.Controls.Add(Me.LabelControl_Status)
            Me.Controls.Add(Me.LabelControl_StartTime)
            Me.Controls.Add(Me.LabelControl_Language)
            Me.Controls.Add(Me.LabelControl_HomePhone)
            Me.Controls.Add(Me.LabelControl_Client)
            Me.Controls.Add(Me.lkReferredBy)
            Me.Controls.Add(Me.lkConfirmation)
            Me.Controls.Add(Me.lkCounselor)
            Me.Controls.Add(Me.SimpleButton_Cancel)
            Me.Controls.Add(Me.SimpleButton_OK)
            Me.Controls.Add(Me.LabelControl9)
            Me.Controls.Add(Me.LabelControl8)
            Me.Controls.Add(Me.LabelControl7)
            Me.Controls.Add(Me.LabelControl6)
            Me.Controls.Add(Me.LabelControl5)
            Me.Controls.Add(Me.LabelControl4)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LabelControl1)
            Me.Name = "Appointments_Control_Confirmation"
            Me.Size = New System.Drawing.Size(326, 270)
            CType(Me.lkCounselor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lkConfirmation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lkReferredBy.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Public WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Public WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Public WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Public WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
        Public WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
        Public WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
        Public WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
        Public WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
        Public WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
        Public WithEvents SimpleButton_OK As DevExpress.XtraEditors.SimpleButton
        Public WithEvents SimpleButton_Cancel As DevExpress.XtraEditors.SimpleButton
        Public WithEvents lkCounselor As DevExpress.XtraEditors.LookUpEdit
        Public WithEvents lkConfirmation As DevExpress.XtraEditors.LookUpEdit
        Public WithEvents lkReferredBy As DevExpress.XtraEditors.LookUpEdit
        Public WithEvents LabelControl_Priority As DevExpress.XtraEditors.LabelControl
        Public WithEvents LabelControl_Status As DevExpress.XtraEditors.LabelControl
        Public WithEvents LabelControl_StartTime As DevExpress.XtraEditors.LabelControl
        Public WithEvents LabelControl_Language As DevExpress.XtraEditors.LabelControl
        Public WithEvents LabelControl_HomePhone As DevExpress.XtraEditors.LabelControl
        Public WithEvents LabelControl_Client As DevExpress.XtraEditors.LabelControl

    End Class
End NameSpace