#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.Svc.Client.Appointments
Imports System.Globalization
Imports DebtPlus.UI.Client.forms.Housing
Imports DebtPlus.Utils.Format
Imports DebtPlus.LINQ
Imports System.Linq

Namespace forms.Appointments

    Public Class Appointments_Control_Result

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Public Event Cancelled As System.EventHandler
        Public Event Completed As System.EventHandler
        Public Event Completing(ByVal sender As Object, record As DebtPlus.LINQ.client_appointment)

        Protected record As DebtPlus.LINQ.client_appointment = Nothing
        Private hudTransaction As DebtPlus.LINQ.hud_transaction = Nothing
        Protected bc As BusinessContext = Nothing

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            Me.spnDuration.Location = New System.Drawing.Point(158, 285)
            RegisterHandlers()
        End Sub

        ''' <summary>
        ''' Register the event handlers
        ''' </summary>
        Private Sub RegisterHandlers()
            AddHandler lkApptType.EditValueChanged, AddressOf lkApptType_EditValueChanged
            AddHandler spnDuration.EditValueChanging, AddressOf SpnDuration_EditValueChanging
            AddHandler spnDuration.EditValueChanged, AddressOf spnDuration_EditValueChanged
            AddHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
            AddHandler SimpleButton_Cancel.Click, AddressOf SimpleButton_Cancel_Click
            AddHandler SimpleButton_HUD.Click, AddressOf SimpleButton_HUD_Click
            AddHandler lkClientStatus.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler lkOffice.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler lkReferredBy.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler lkReferredTo.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler lkApptType.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler lkCounselor.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler lkBankruptcyClass.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler spnDuration.EditValueChanged, AddressOf FormChanged
            AddHandler lkClientStatus.EditValueChanged, AddressOf FormChanged
            AddHandler lkReferredBy.EditValueChanged, AddressOf FormChanged
            AddHandler lkReferredTo.EditValueChanged, AddressOf FormChanged
            AddHandler lkApptType.EditValueChanged, AddressOf FormChanged
            AddHandler lkOffice.EditValueChanged, AddressOf FormChanged
            AddHandler lkCounselor.EditValueChanged, AddressOf FormChanged
            AddHandler lkBankruptcyClass.EditValueChanged, AddressOf FormChanged
            AddHandler lkReferredBy.EditValueChanged, AddressOf lkReferredBy_EditValueChanged
        End Sub

        ''' <summary>
        ''' Remove the event registration
        ''' </summary>
        Private Sub UnRegisterHandlers()
            RemoveHandler lkApptType.EditValueChanged, AddressOf lkApptType_EditValueChanged
            RemoveHandler spnDuration.EditValueChanging, AddressOf SpnDuration_EditValueChanging
            RemoveHandler spnDuration.EditValueChanged, AddressOf spnDuration_EditValueChanged
            RemoveHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
            RemoveHandler SimpleButton_Cancel.Click, AddressOf SimpleButton_Cancel_Click
            RemoveHandler SimpleButton_HUD.Click, AddressOf SimpleButton_HUD_Click
            RemoveHandler lkClientStatus.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler lkOffice.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler lkReferredBy.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler lkReferredTo.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler lkApptType.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler lkCounselor.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler lkBankruptcyClass.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler spnDuration.EditValueChanged, AddressOf FormChanged
            RemoveHandler lkClientStatus.EditValueChanged, AddressOf FormChanged
            RemoveHandler lkReferredBy.EditValueChanged, AddressOf FormChanged
            RemoveHandler lkReferredTo.EditValueChanged, AddressOf FormChanged
            RemoveHandler lkApptType.EditValueChanged, AddressOf FormChanged
            RemoveHandler lkOffice.EditValueChanged, AddressOf FormChanged
            RemoveHandler lkCounselor.EditValueChanged, AddressOf FormChanged
            RemoveHandler lkBankruptcyClass.EditValueChanged, AddressOf FormChanged
            RemoveHandler lkReferredBy.EditValueChanged, AddressOf lkReferredBy_EditValueChanged
        End Sub

        ''' <summary>
        ''' Event processing
        ''' </summary>
        Protected Sub RaiseCancelled(ByVal e As EventArgs)
            RaiseEvent Cancelled(Me, e)
        End Sub

        ''' <summary>
        ''' Event processing
        ''' </summary>
        Protected Overridable Sub OnCancelled(ByVal e As EventArgs)
            RaiseCancelled(e)
        End Sub

        ''' <summary>
        ''' Event processing
        ''' </summary>
        Protected Sub RaiseCompleted(ByVal e As EventArgs)
            RaiseEvent Completed(Me, e)
        End Sub

        ''' <summary>
        ''' Event processing
        ''' </summary>
        Protected Overridable Sub OnCompleted(ByVal e As EventArgs)
            RaiseCompleted(e)
        End Sub

        ''' <summary>
        ''' Event processing
        ''' </summary>
        Protected Sub RaiseCompleting(record As DebtPlus.LINQ.client_appointment)
            RaiseEvent Completing(Me, record)
        End Sub

        ''' <summary>
        ''' Event processing
        ''' </summary>
        Protected Overridable Sub OnCompleting(record As DebtPlus.LINQ.client_appointment)
            RaiseCompleting(record)
        End Sub

        ''' <summary>
        ''' Initialize the controls for the current row
        ''' </summary>
        Protected Friend Sub ReadForm(bc As BusinessContext, ByVal caRecord As client_appointment)
            Me.record = caRecord
            Me.bc = bc
            hudTransaction = Nothing

            UnRegisterHandlers()
            Try

                ' Bind the information to the tables
                lkClientStatus.Properties.DataSource = DebtPlus.LINQ.Cache.ClientStatusType.getList()
                lkReferredBy.Properties.DataSource = DebtPlus.LINQ.Cache.referred_by.getList()
                lkReferredTo.Properties.DataSource = DebtPlus.LINQ.Cache.referred_to.getList()
                lkApptType.Properties.DataSource = DebtPlus.LINQ.Cache.appt_type.getList()
                lkOffice.Properties.DataSource = DebtPlus.LINQ.Cache.office.getList()
                lkCounselor.Properties.DataSource = DebtPlus.LINQ.Cache.counselor.CounselorsList()
                lkBankruptcyClass.Properties.DataSource = DebtPlus.LINQ.Cache.BankruptcyClassType.getList()

                ' Load the edit values
                lkClientStatus.EditValue = caRecord.result
                lkReferredBy.EditValue = caRecord.referred_by
                lkReferredTo.EditValue = caRecord.referred_to
                lkApptType.EditValue = caRecord.appt_type
                lkOffice.EditValue = caRecord.office
                lkCounselor.EditValue = caRecord.counselor
                lkBankruptcyClass.EditValue = caRecord.bankruptcy_class
                chkCredit.Checked = record.credit
                TextEdit_partner_code.EditValue = record.partner

                ' Calculate the number of minutes for the appointment
                If caRecord.end_time.HasValue Then
                    Dim ts As TimeSpan = caRecord.end_time.Value.Subtract(caRecord.start_time)
                    Dim minutes As Int32 = Convert.ToInt32(System.Math.Floor(ts.TotalMinutes))
                    spnDuration.EditValue = minutes
                End If

                ' Enable the buttons
                SimpleButton_OK.Enabled = Not HasErrors() AndAlso (Not AppointmentInFuture())
                SimpleButton_HUD.Enabled = ValidOffice() AndAlso (Not AppointmentInFuture())

                ' Do the various items that would have been normally tripped by events.
                EnablePartner()
                ControlApptType()
                RefreshDurationDisplay()

            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Control the editing of the partner code value
        ''' </summary>
        Private Sub EnablePartner()

            ' Remove the validation handler for the partner. It will be re-enabled if we have one
            RemoveHandler TextEdit_partner_code.Validating, AddressOf TextEdit_partner_code_Validating

            ' Retrieve the referral source. If missing then there is no referral.
            Dim referral As DebtPlus.LINQ.referred_by = TryCast(lkReferredBy.GetSelectedDataRow(), DebtPlus.LINQ.referred_by)
            If referral Is Nothing Then
                TextEdit_partner_code.EditValue = Nothing
                TextEdit_partner_code.Enabled = False
                Return
            End If

            ' If the referral does not allow the partner code then remove it
            Dim Validation As String = referral.partnerValidation
            If String.IsNullOrEmpty(Validation) Then
                TextEdit_partner_code.EditValue = Nothing
                TextEdit_partner_code.Enabled = False
                Return
            End If

            ' Enable the partner code entry
            TextEdit_partner_code.Enabled = True

            ' Try to determine if the partner code as entered is valid. If not, clear it.
            Try
                Dim currentValue As String = Convert.ToString(TextEdit_partner_code.EditValue)
                If currentValue Is Nothing Then currentValue = String.Empty

                If Not System.Text.RegularExpressions.Regex.IsMatch(currentValue, "^" + Validation + "$") Then
                    TextEdit_partner_code.EditValue = Nothing
                End If
            Catch
            End Try

            ' Set the referral code value
            Try
                TextEdit_partner_code.Properties.Mask.BeepOnError = True
                TextEdit_partner_code.Properties.Mask.EditMask = Validation
                TextEdit_partner_code.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
                TextEdit_partner_code.Properties.Mask.SaveLiteral = True
                TextEdit_partner_code.Properties.Mask.ShowPlaceHolders = True

                ' Finally, re-enable the validation code validator so that it is checked for the full expression
                AddHandler TextEdit_partner_code.Validating, AddressOf TextEdit_partner_code_Validating
            Catch
            End Try
        End Sub

        ''' <summary>
        ''' Validate the partner code to ensure that it is acceptable for input
        ''' </summary>
        Private Sub TextEdit_partner_code_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs)

            ' No partner code is acceptable for the entry. It will be caught elsewhere
            Dim strCode As String = TryCast(TextEdit_partner_code.EditValue, String)
            If strCode Is Nothing Then
                TextEdit_partner_code.ErrorText = "A value is required for this field"
                Return
            End If

            ' Pass the code through the regular expression routine to see if it is valid. We put the
            ' beginning of string and end of string anchors on the expression to ensure that the entire
            ' string matches the entire expression and not just some sub-string component.
            Try
                If System.Text.RegularExpressions.Regex.IsMatch(strCode, "^" + TextEdit_partner_code.Properties.Mask.EditMask + "$") Then
                    TextEdit_partner_code.ErrorText = String.Empty
                    Return
                End If

                ' The entry is not completely valid. Reject it.
                TextEdit_partner_code.ErrorText = "invalid entry"

            Catch ex As System.Exception
                TextEdit_partner_code.ErrorText = ex.Message
            End Try

            e.Cancel = True
        End Sub

        ''' <summary>
        ''' Process the change in the referral source for the appointment
        ''' </summary>
        Private Sub lkReferredBy_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            UnRegisterHandlers()
            Try
                EnablePartner()
            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Enable the buttons when the form changes
        ''' </summary>
        Private Sub FormChanged(ByVal Sender As Object, ByVal e As EventArgs)
            SimpleButton_OK.Enabled = Not HasErrors() AndAlso (Not AppointmentInFuture())
            SimpleButton_HUD.Enabled = ValidOffice() AndAlso (Not AppointmentInFuture())
        End Sub

        ''' <summary>
        ''' Is the office entered?
        ''' </summary>
        Protected Function ValidOffice() As Boolean
            Return lkOffice.EditValue IsNot Nothing
        End Function

        ''' <summary>
        ''' Are there pending errors on the form?
        ''' </summary>
        Protected Overridable Function HasErrors() As Boolean

            ' A duration must be specified
            If spnDuration.EditValue Is Nothing Then Return True
            If Convert.ToInt32(spnDuration.EditValue) <= 0 Then Return True

            ' These fields are required for counseling appointments.
            If Not ValidOffice() Then Return True
            If lkApptType.EditValue Is Nothing Then Return True
            If lkCounselor.EditValue Is Nothing Then Return True
            If Not String.IsNullOrEmpty(TextEdit_partner_code.ErrorText) Then Return True
            If String.IsNullOrEmpty(DebtPlus.Utils.Nulls.v_String(lkClientStatus.EditValue)) Then Return True

            ' There are no pending errors
            Return False
        End Function

        ''' <summary>
        ''' Correct the appointment duration as needed
        ''' </summary>
        Private Sub spnDuration_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            RefreshDurationDisplay()
        End Sub

        ''' <summary>
        ''' Update the duration display in minutes when needed
        ''' </summary>
        Private Sub RefreshDurationDisplay()
            Dim minutes As Int32 = Convert.ToInt32(DebtPlus.Utils.Nulls.v_Decimal(spnDuration.EditValue).GetValueOrDefault(0D))

            ' Update the text for the label and force a re-draw
            Dim TimeValue As String = DebtPlus.Utils.Format.Time.HoursAndMinutes(minutes)
            If TimeValue = String.Empty Then TimeValue = "in minutes"
            LabelControl_duration_label.Text = String.Format("({0})", TimeValue)
        End Sub

        ''' <summary>
        ''' Process the ACCEPT button on the form
        ''' </summary>
        Protected Overridable Sub SimpleButton_OK_Click(ByVal sender As Object, ByVal e As EventArgs)

            ' Calculate the ending time based upon the starting time and the duration
            Dim minutes As Int32 = Convert.ToInt32(spnDuration.EditValue)

            ' Update the other fields in the current record from the input controls.
            record.appt_type = DebtPlus.Utils.Nulls.v_Int32(lkApptType.EditValue)
            record.bankruptcy_class = DebtPlus.Utils.Nulls.v_Int32(lkBankruptcyClass.EditValue).GetValueOrDefault(0)
            record.counselor = DebtPlus.Utils.Nulls.v_Int32(lkCounselor.EditValue)
            record.credit = chkCredit.Checked
            record.date_updated = DebtPlus.LINQ.BusinessContext.getdate()
            record.end_time = record.start_time.AddMinutes(minutes)
            record.office = DebtPlus.Utils.Nulls.v_Int32(lkOffice.EditValue)
            record.partner = DebtPlus.Utils.Nulls.v_String(TextEdit_partner_code.EditValue)
            record.referred_by = DebtPlus.Utils.Nulls.v_Int32(lkReferredBy.EditValue)
            record.referred_to = DebtPlus.Utils.Nulls.v_Int32(lkReferredTo.EditValue)
            record.result = DebtPlus.Utils.Nulls.v_String(lkClientStatus.EditValue)

            ' Indicate that the form is being completed
            OnCompleting(record)

            Try

                ' If this is a walk-in appointment, the appointment record has not been created. We need
                ' to create the record before we can update it with xpr_appt_result. Do the create now.
                If record.Id < 1 Then
                    bc.client_appointments.InsertOnSubmit(record)
                    bc.SubmitChanges()
                End If

                ' Update the record in the database tables. The procedure will update the database with the record
                ' values. We don't submit this changes to this record ourselves since the stored procedure does this
                ' for us. We do need to refresh the record after the update however.
                bc.xpr_appt_result(record)
                bc.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, record)

                ' If there is a transaction then link the transaction to the appointment
                If hudTransaction IsNot Nothing Then
                    Dim q As DebtPlus.LINQ.hud_transaction = bc.hud_transactions.Where(Function(tran) tran.Id = hudTransaction.Id).FirstOrDefault()
                    If q IsNot Nothing Then
                        q.client_appointment = record.Id
                        bc.SubmitChanges()
                    End If
                End If

                ' Indicate that the OK button was pressed
                RaiseCompleted(EventArgs.Empty)

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating the client appointment")
            End Try
        End Sub

        ''' <summary>
        ''' Process the CANCEL button on the form
        ''' </summary>
        Private Sub SimpleButton_Cancel_Click(ByVal sender As Object, ByVal e As EventArgs)
            RaiseCancelled(EventArgs.Empty)
        End Sub

        ''' <summary>
        ''' Process a change in the duration. Don't allow negatives.
        ''' </summary>
        Private Sub SpnDuration_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)
            If Convert.ToInt32(DebtPlus.Utils.Nulls.v_Decimal(e.NewValue).GetValueOrDefault(0)) < 0 Then
                e.Cancel = True
            End If
        End Sub

        ''' <summary>
        ''' Handle the click of the HUD button
        ''' </summary>
        Private Sub SimpleButton_HUD_Click(ByVal sender As Object, ByVal e As EventArgs)

            ' Conduct the appointment information for the default duration
            Dim appointmentDuration As Int32 = Convert.ToInt32(DebtPlus.Utils.Nulls.v_Decimal(spnDuration.EditValue).GetValueOrDefault(0))
            Using frm As New HUD_Interview_Form(bc, record.client, appointmentDuration)
                If frm.ShowDialog() = DialogResult.OK Then
                    hudTransaction = frm.HudTransaction
                End If
            End Using

        End Sub

        ''' <summary>
        ''' Correct the duration when the appointment type changes
        ''' </summary>
        Private Sub lkApptType_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            ControlApptType()
        End Sub

        ''' <summary>
        ''' Correct the duration when the appointment type changes
        ''' </summary>
        Private Sub ControlApptType()
            Dim enableHUD As Boolean = False

            ' Find the appointment type information
            Dim newMinutes As Int32 = -1
            If lkApptType.EditValue IsNot Nothing Then
                Dim q As DebtPlus.LINQ.appt_type = CType(lkApptType.Properties.GetDataSourceRowByKeyValue(lkApptType.EditValue), DebtPlus.LINQ.appt_type)
                If q IsNot Nothing Then
                    newMinutes = q.appt_duration
                    enableHUD = q.housing
                End If
            End If

            If newMinutes > 0 Then
                spnDuration.EditValue = newMinutes
            End If

            ' Enable the HUD reminder if needed
            LayoutControlItem_hud.Visibility = If(enableHUD, DevExpress.XtraLayout.Utils.LayoutVisibility.Always, DevExpress.XtraLayout.Utils.LayoutVisibility.OnlyInCustomization)
        End Sub

        Private Function AppointmentInFuture() As Boolean

            ' If the appointment is not pending (it is a walk-in) then accept it.
            If record.status <> "P" AndAlso record.status <> "K" Then
                Return False
            End If

            ' If the appointment time tomorrow or later then reject the item.
            If record.start_time >= System.DateTime.Now.AddDays(1).Date Then
                Return True
            End If

            Return False
        End Function
    End Class
End Namespace
