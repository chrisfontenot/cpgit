#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Views.Base
Imports DebtPlus.Svc.Client.Appointments
Imports DevExpress.Utils
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo
Imports DebtPlus.LINQ
Imports System.Linq

Namespace forms.Appointments
    Public Class Appointments_Control_List

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Public Event EditItem(ByVal sender As Object, ByVal record As client_appointment)
        Public Event Cancel(ByVal sender As Object, ByVal e As EventArgs)

        Private colRecords As System.Collections.Generic.List(Of client_appointment)
        Protected ControlRow As Int32 = -1
        Protected bc As BusinessContext = Nothing

        ''' <summary>
        ''' Text for the control
        ''' </summary>
        <Description("Text for the control"), Category("Appearance"), DefaultValue(GetType(String), ""), Browsable(True)>
        Public Overrides Property Text() As String
            Get
                Return LabelControl_Caption.Text
            End Get

            Set(ByVal value As String)
                LabelControl_Caption.Text = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
            AddHandler GridView1.FocusedRowChanged, AddressOf GridView1_FocusedRowChanged
            AddHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
            AddHandler SimpleButton_Cancel.Click, AddressOf SimpleButton_Cancel_Click
            AddHandler GridView1.CustomColumnDisplayText, AddressOf GridView1_CustomColumnDisplayText
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
            RemoveHandler GridView1.FocusedRowChanged, AddressOf GridView1_FocusedRowChanged
            RemoveHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
            RemoveHandler SimpleButton_Cancel.Click, AddressOf SimpleButton_Cancel_Click
            RemoveHandler GridView1.CustomColumnDisplayText, AddressOf GridView1_CustomColumnDisplayText
        End Sub

        ''' <summary>
        ''' Load the list with the items to be displayed
        ''' </summary>
        Public Overridable Sub ReadForm(bc As BusinessContext, ByVal clientID As Int32)
            Try
                Using cm As New DebtPlus.UI.Common.CursorManager()
                    ReadForm(bc, bc.client_appointments.Where(Function(ca) ca.client = clientID AndAlso ca.status = "P"c).ToList())
                End Using

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading client appointments")
            End Try
        End Sub

        ''' <summary>
        ''' Load the list with the items to be displayed
        ''' </summary>
        Protected Sub ReadForm(bc As BusinessContext, ByVal colRecords As System.Collections.Generic.List(Of client_appointment))
            Me.bc = bc
            Me.colRecords = colRecords

            UnRegisterHandlers()
            Try
                GridControl1.DataSource = colRecords
                GridView1.BestFitColumns()

                ControlRow = -1
                GridView1.ClearSelection()

                ' If there are items, then select the first one in the list.
                If colRecords.Count > 0 Then
                    GridView1.FocusedRowHandle = 0
                    SimpleButton_OK.Enabled = True
                Else
                    GridView1.FocusedRowHandle = -1
                    SimpleButton_OK.Enabled = False
                End If

            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Handle the case where the item is selected for editing
        ''' </summary>
        Protected Overridable Sub OnEditItem(ByVal record As client_appointment)
            RaiseEvent EditItem(Me, record)
        End Sub

        ''' <summary>
        ''' Handle the case where the item is selected for editing
        ''' </summary>
        Protected Overridable Sub OnCancel(ByVal e As EventArgs)
            RaiseEvent Cancel(Me, e)
        End Sub

        ''' <summary>
        ''' Double Click on an item -- edit it
        ''' </summary>
        Protected Sub GridView1_DoubleClick(ByVal sender As Object, ByVal e As EventArgs)

            ' Find the row targeted as the double-click item
            If Not DesignMode Then
                Dim hi As GridHitInfo = GridView1.CalcHitInfo((GridControl1.PointToClient(MousePosition)))
                If hi.InRow Then
                    ControlRow = hi.RowHandle
                    GridView1.FocusedRowHandle = ControlRow
                    SimpleButton_OK.PerformClick()
                End If
            End If
        End Sub

        Private Sub GridView1_FocusedRowChanged(ByVal sender As Object, ByVal e As FocusedRowChangedEventArgs)

            ' Handle the condition where the row is selected. Enable the OK button.
            If Not DesignMode Then
                ControlRow = GridView1.FocusedRowHandle
                SimpleButton_OK.Enabled = (GridView1.GetRow(ControlRow) IsNot Nothing)
            End If
        End Sub

        ''' <summary>
        ''' Handle the OK button to edit the row
        ''' </summary>
        Private Sub SimpleButton_OK_Click(ByVal sender As Object, ByVal e As EventArgs)

            ' If there is a row then process the row. Otherwise, ignore the select button.
            If SimpleButton_OK.Enabled Then
                Dim record As client_appointment = TryCast(GridView1.GetRow(GridView1.FocusedRowHandle), client_appointment)
                If record IsNot Nothing Then
                    OnEditItem(record)
                End If
            End If
        End Sub

        ''' <summary>
        ''' Pass along the CANCEL event
        ''' </summary>
        Private Sub SimpleButton_Cancel_Click(ByVal sender As Object, ByVal e As EventArgs)
            OnCancel(e)
        End Sub

        ''' <summary>
        ''' Do the custom formatting for the columns in the grid control.
        ''' </summary>
        Private Sub GridView1_CustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs)

            ' Find the current client appointment desired
            Dim record As client_appointment = TryCast(GridView1.GetRow(e.RowHandle), client_appointment)
            If record Is Nothing Then
                Return
            End If

            ' Look for the confirmation type
            If System.Object.Equals(e.Column, GridColumn_confirmed) Then
                Dim q As DebtPlus.LINQ.AppointmentConfirmationType = DebtPlus.LINQ.Cache.AppointmentConfirmationType.getList().Find(Function(s) s.Id = record.confirmation_status)
                If q IsNot Nothing Then
                    e.DisplayText = q.description
                    Return
                End If
                e.DisplayText = String.Empty
                Return
            End If

            ' Look for the counselor
            If System.Object.Equals(e.Column, GridColumn_counselor) Then
                If record.counselor.HasValue Then
                    Dim q As counselor = DebtPlus.LINQ.Cache.counselor.getList().Find(Function(s) s.Id = record.counselor.Value)
                    If q IsNot Nothing AndAlso q.Name IsNot Nothing Then
                        e.DisplayText = q.Name.ToString()
                        Return
                    End If
                End If
                e.DisplayText = String.Empty
                Return
            End If

            ' Look for the appointment type
            If System.Object.Equals(e.Column, GridColumn_appt_type) Then
                If record.office.HasValue AndAlso record.appt_type.HasValue Then
                    Dim q As appt_type = DebtPlus.LINQ.Cache.appt_type.getList().Find(Function(s) s.Id = record.appt_type.Value)
                    If q IsNot Nothing Then
                        e.DisplayText = q.appt_name
                        Return
                    End If
                Else

                    ' Workshops have types from the workshop type record, not the appointment type.
                    If record.workshop.HasValue Then
                        Dim w As DebtPlus.LINQ.workshop = bc.workshops.Where(Function(s) s.Id = record.workshop.Value).FirstOrDefault()
                        If w IsNot Nothing Then
                            Dim wt As workshop_type = DebtPlus.LINQ.Cache.workshop_type.getList().Find(Function(s) s.Id = w.workshop_type)
                            If wt IsNot Nothing Then
                                e.DisplayText = wt.description
                                Return
                            End If
                        End If
                    Else
                    End If
                End If
                e.DisplayText = String.Empty
                Return
            End If

            ' Look for the location
            If System.Object.Equals(e.Column, GridColumn_location) Then

                ' The location may be an office
                If record.office.HasValue Then
                    Dim q As office = DebtPlus.LINQ.Cache.office.getList().Find(Function(s) s.Id = record.office.Value)
                    If q IsNot Nothing Then
                        e.DisplayText = q.name
                        Return
                    End If
                End If

                ' Or it may be a workshop
                If record.workshop.HasValue Then
                    Dim w As DebtPlus.LINQ.workshop = bc.workshops.Where(Function(s) s.Id = record.workshop.Value).FirstOrDefault()
                    If w IsNot Nothing AndAlso w.workshop_location > 0 Then
                        Dim wl As DebtPlus.LINQ.workshop_location = bc.workshop_locations.Where(Function(l) l.Id = w.workshop_location).FirstOrDefault()
                        If wl IsNot Nothing Then
                            e.DisplayText = wl.name
                            Return
                        End If
                    End If
                End If

                e.DisplayText = String.Empty
            End If
        End Sub
    End Class
End NameSpace
