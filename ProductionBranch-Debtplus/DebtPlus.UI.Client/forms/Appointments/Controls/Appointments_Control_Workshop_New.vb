#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.LINQ
Imports System.Linq

Namespace forms.Appointments
    Public Class Appointments_Control_Workshop_New

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Private workshopRecord As xpr_appt_workshop_listResult

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler LabelControl_where.Click, AddressOf LabelControl_where_Click
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler LabelControl_where.Click, AddressOf LabelControl_where_Click
        End Sub

        ''' <summary>
        ''' Process the new form control
        ''' </summary>
        Public Shadows Sub ReadForm(bc As BusinessContext, ByVal clientAppointment As DebtPlus.LINQ.client_appointment, ByVal workshopRecord As xpr_appt_workshop_listResult)
            Me.bc = bc
            Me.clientAppointment = clientAppointment
            Me.workshopRecord = workshopRecord

            ' Supply the information about the workshop
            LabelControl_description.Text = If(String.IsNullOrEmpty(workshopRecord.description), String.Empty, workshopRecord.description)
            LabelControl_where.Text = If(String.IsNullOrEmpty(workshopRecord.workshop_location), String.Empty, workshopRecord.workshop_location)
            LabelControl_presenter.Text = If(String.IsNullOrEmpty(workshopRecord.counselor), String.Empty, workshopRecord.counselor)
            LabelControl_available_seats.Text = String.Format("{0:n0}", SeatsAvailableToYou())
            LabelControl_when.Text = If(workshopRecord.start_time.HasValue, String.Format("{0:ddd hh:mm tt}", workshopRecord.start_time.Value), String.Empty)
            LabelControl_duration.Text = DebtPlus.Utils.Format.Time.HoursAndMinutes(workshopRecord.duration.GetValueOrDefault(0))

            ' Find the current clientAppointment record for this workshopRecord and this client
            If clientAppointment.Id > 0 Then
                Dim foundAppointment As client_appointment = bc.client_appointments.Where(Function(ca) ca.client = clientAppointment.client AndAlso ca.workshop.GetValueOrDefault(0) = clientAppointment.workshop.GetValueOrDefault(-1) AndAlso ca.status = "P"c).FirstOrDefault()
                If foundAppointment IsNot Nothing Then
                    clientAppointment.Id = foundAppointment.Id
                    clientAppointment.workshop_people = foundAppointment.workshop_people
                    LabelControl_booked_seats.Text = String.Format("{0:n0}", clientAppointment.workshop_people.GetValueOrDefault(0))
                    MyBase.ReadForm(bc, clientAppointment)
                    Return
                End If
            End If

            ' The appointment can not be found. Mark it as being "new" and create it
            clientAppointment.status = "P"c
            clientAppointment.workshop_people = 0
            clientAppointment.workshop = workshopRecord.workshop
            clientAppointment.start_time = workshopRecord.start_time.Value
            clientAppointment.end_time = clientAppointment.start_time.AddMinutes(Convert.ToDouble(workshopRecord.duration.GetValueOrDefault(0)))

            ' Set the number of seats booked for this client
            LabelControl_booked_seats.Text = String.Format("{0:n0}", clientAppointment.workshop_people.GetValueOrDefault(0))
            MyBase.ReadForm(bc, clientAppointment)
        End Sub

        Private Sub LabelControl_where_Click(sender As Object, e As EventArgs)

            'Dim q As workshop_location = (From w In bc.workshops Join wl In bc.workshop_locations On w.workshop_location Equals wl.Id Where w.Id = workshopRecord.workshop.Value Select wl).FirstOrDefault()
            Dim q As workshop_location = bc.workshops.Join(bc.workshop_locations, Function(w) w.workshop_location, Function(wl) wl.Id, Function(w, wl) New With {.workshopID = w.Id, .wl = wl}).Where(Function(x) x.workshopID = workshopRecord.workshop.Value).Select(Function(x) x.wl).FirstOrDefault()
            If q IsNot Nothing Then
                Using frm As New Appointments_WorkshopLocation(bc, q)
                    frm.ShowDialog()
                End Using
            End If

        End Sub

        Protected Overrides Function HasErrors() As Boolean

            ' The base is checked first
            If MyBase.HasErrors() Then
                Return True
            End If

            ' Ensure that you are not booking too many seats
            Dim SeatCount As Int32 = Convert.ToInt32(SpinEdit_booked_seats.EditValue)
            If SeatCount > SeatsAvailableToYou() Then
                Return True
            End If

            Return False
        End Function

        Private Function SeatsAvailableToYou() As Int32
            Dim SeatsBookedByYou As Int32 = clientAppointment.workshop_people.GetValueOrDefault(0)
            Dim SeatsBooked As Int32 = workshopRecord.seats_available.GetValueOrDefault(0) - workshopRecord.seats_left
            Dim SeatsBookedByOthers As Int32 = SeatsBooked - SeatsBookedByYou
            Return workshopRecord.seats_available.GetValueOrDefault(0) - SeatsBookedByOthers
        End Function
    End Class
End NameSpace
