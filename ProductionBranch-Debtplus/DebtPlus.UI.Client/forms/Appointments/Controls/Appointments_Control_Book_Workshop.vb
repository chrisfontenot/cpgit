#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DevExpress.XtraGrid.Views.Base
Imports DebtPlus.Svc.Client.Appointments
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo

Imports DebtPlus.LINQ
Imports System.Linq

Namespace forms.Appointments

    Public Class Appointments_Control_Book_Workshop

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Protected bc As BusinessContext = Nothing

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
            AddHandler GridView1.FocusedRowChanged, AddressOf GridView1_FocusedRowChanged
        End Sub

        Private colRecords As System.Collections.Generic.List(Of DebtPlus.LINQ.xpr_appt_workshop_listResult) = Nothing
        Private ControlRecord As xpr_appt_workshop_listResult = Nothing

        ' The appointment is complete
        Public Event Completed(ByVal Sender As Object, ByVal CounselingAppointment As Int32)
        Public Event EditItem(ByVal sender As Object, ByVal record As xpr_appt_workshop_listResult)

        ''' <summary>
        ''' Handle the case where the item is selected for editing
        ''' </summary>
        Protected Overridable Sub OnEditItem(ByVal record As xpr_appt_workshop_listResult)
            RaiseEvent EditItem(Me, record)
        End Sub

        ''' <summary>
        ''' Determine if the row is a valid row for processing
        ''' </summary>
        Protected Overridable Function IsValidRow(ByVal row As xpr_appt_workshop_listResult) As Boolean
            Return True
        End Function

        ''' <summary>
        ''' Double Click on an item -- edit it
        ''' </summary>
        Protected Sub GridView1_DoubleClick(ByVal sender As Object, ByVal e As EventArgs)

            ' Find the row targeted as the double-click item
            If Not DesignMode Then
                Dim hi As GridHitInfo = GridView1.CalcHitInfo((GridControl1.PointToClient(MousePosition)))
                Dim RowHandle As Int32 = hi.RowHandle

                Dim record As xpr_appt_workshop_listResult = TryCast(GridView1.GetRow(RowHandle), xpr_appt_workshop_listResult)
                If record IsNot Nothing AndAlso IsValidRow(record) Then
                    OnEditItem(record)
                End If
            End If
        End Sub

        ''' <summary>
        ''' Process a change in the current row for the control
        ''' </summary>
        Private Sub GridView1_FocusedRowChanged(ByVal sender As Object, ByVal e As FocusedRowChangedEventArgs)

            ' Handle the condition where the row is selected. Enable the OK button.
            If Not DesignMode Then
                Dim RowHandle As Int32 = e.FocusedRowHandle
                Dim record As xpr_appt_workshop_listResult = TryCast(GridView1.GetRow(RowHandle), xpr_appt_workshop_listResult)
                If record IsNot Nothing AndAlso IsValidRow(record) Then
                    ControlRecord = record
                End If
            End If
        End Sub

        Public Sub ReadForm(bc As BusinessContext)
            Me.bc = bc
            Try
                colRecords = bc.xpr_appt_workshop_list()
                GridControl1.DataSource = colRecords
                GridControl1.RefreshDataSource()
                GridView1.BestFitColumns()

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading workshop data")
            End Try
        End Sub

    End Class
End NameSpace
