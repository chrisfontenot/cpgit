Namespace forms.Appointments
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Appointments_Control_Walkin

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            CType(Me.lkClientStatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lkReferredBy.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lkReferredTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lkApptType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lkOffice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lkCounselor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lkBankruptcyClass.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.spnDuration.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.chkCredit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.LabelControl1.Location = New System.Drawing.Point(4, 4)
            '
            'lkClientStatus
            '
            '
            'lkReferredBy
            '
            '
            'lkReferredTo
            '
            '
            'lkApptType
            '
            '
            'lkOffice
            '
            '
            'lkCounselor
            '
            '
            'lkBankruptcyClass
            '
            '
            'spnDuration
            '
            Me.spnDuration.Properties.Mask.BeepOnError = True
            Me.spnDuration.Properties.Mask.EditMask = "\d+"
            Me.spnDuration.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            '
            'chkCredit
            '
            Me.chkCredit.Size = New System.Drawing.Size(178, 20)
            '
            'LabelControl_hud_note
            '
            Me.LabelControl_hud_note.Appearance.BackColor = System.Drawing.Color.Red
            Me.LabelControl_hud_note.Appearance.ForeColor = System.Drawing.Color.White
            Me.LabelControl_hud_note.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.LabelControl_hud_note.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            '
            'LabelControl_duration_label
            '
            Me.LabelControl_duration_label.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            '
            'Appointments_Control_Walkin
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Name = "Appointments_Control_Walkin"
            CType(Me.lkClientStatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lkReferredBy.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lkReferredTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lkApptType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lkOffice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lkCounselor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lkBankruptcyClass.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.spnDuration.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.chkCredit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

    End Class
End NameSpace