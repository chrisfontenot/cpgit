#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.Svc.Client.Appointments
Imports System.Text
Imports DevExpress.XtraEditors
Imports DebtPlus.Utils.Format
Imports DebtPlus.LINQ
Imports System.Linq

Namespace forms.Appointments
    Public Class Appointments_Control_Book_Counseling

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Protected bc As BusinessContext = Nothing

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler SimpleButton_ClearAll.Click, AddressOf SimpleButton_ClearAll_Click
            AddHandler SimpleButton_SelectAll.Click, AddressOf SimpleButton_ClearAll_Click
            AddHandler lkCounselor.ButtonClick, AddressOf lkApptType_ButtonClick
            AddHandler chkOffice_Zipcode.CheckedChanged, AddressOf chkOffice_Zipcode_CheckedChanged
            AddHandler SimpleButton_Search.Click, AddressOf SimpleButton_Search_Click
            AddHandler lkApptType.EditValueChanged, AddressOf lkApptType_EditValueChanging
            AddHandler lkCounselor.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler lkApptType.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler lkBankruptcyClass.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler SimpleButton_ClearAll.Click, AddressOf SimpleButton_ClearAll_Click
            RemoveHandler SimpleButton_SelectAll.Click, AddressOf SimpleButton_ClearAll_Click
            RemoveHandler lkCounselor.ButtonClick, AddressOf lkApptType_ButtonClick
            RemoveHandler chkOffice_Zipcode.CheckedChanged, AddressOf chkOffice_Zipcode_CheckedChanged
            RemoveHandler SimpleButton_Search.Click, AddressOf SimpleButton_Search_Click
            RemoveHandler lkApptType.EditValueChanged, AddressOf lkApptType_EditValueChanging
            RemoveHandler lkCounselor.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler lkApptType.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler lkBankruptcyClass.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
        End Sub

        ' Completion event when we have finished
        Public Delegate Sub CompletedEventHandler(ByVal sender As Object, ByVal e As client_appointment)
        Public Event Completed As CompletedEventHandler

        ' Template appointment as passed in by the caller
        Private clientAppointment As DebtPlus.LINQ.client_appointment

        Private Function Counselor() As System.Nullable(Of Int32)
            Return DebtPlus.Utils.Nulls.v_Int32(lkCounselor.EditValue)
        End Function

        Private Function ZipCode() As String
            If chkOffice_All.Checked Then Return Nothing
            If (txtZipcode.EditValue Is Nothing) OrElse txtZipcode.EditValue Is DBNull.Value Then Return Nothing
            Return Convert.ToString(txtZipcode.EditValue).Trim().Replace("-", "")
        End Function

        Private Function ApptType() As System.Nullable(Of Int32)
            Return DebtPlus.Utils.Nulls.v_Int32(lkApptType.EditValue)
        End Function

        Private Function Priority() As Int32
            Return If(chkPriority.Checked, 1, 0)
        End Function

        Private Function BankruptcyClass() As System.Nullable(Of Int32)
            Return DebtPlus.Utils.Nulls.v_Int32(lkBankruptcyClass.EditValue)
        End Function

        Private Function Days() As String
            Dim sb As New StringBuilder

            If chkDays_Sunday.Checked Then sb.Append(",1")
            If chkDays_Monday.Checked Then sb.Append(",2")
            If chkDays_Tuesday.Checked Then sb.Append(",3")
            If chkDays_Wednesday.Checked Then sb.Append(",4")
            If chkDays_Thursday.Checked Then sb.Append(",5")
            If chkDays_Friday.Checked Then sb.Append(",6")
            If chkDays_Saturday.Checked Then sb.Append(",7")

            If sb.Length > 0 Then
                sb.Remove(0, 1)
            End If

            If sb.ToString() = "1,2,3,4,5,6,7" OrElse sb.Length = 0 Then
                Return Nothing
            End If

            Return sb.ToString()
        End Function

        Private Function Time() As String
            Dim answer As String = Nothing
            If Not chkHours_All.Checked Then
                If chkHours_AM.Checked Then answer = "AM"
                If chkHours_PM.Checked Then answer = "PM"
            End If
            Return answer
        End Function

        Public Sub ReadForm(bc As BusinessContext, ByVal clientAppointment As DebtPlus.LINQ.client_appointment)
            Me.clientAppointment = clientAppointment
            Me.bc = bc

            UnRegisterHandlers()
            Try
                lkCounselor.Properties.DataSource = DebtPlus.LINQ.Cache.counselor.CounselorsList()
                lkApptType.Properties.DataSource = DebtPlus.LINQ.Cache.appt_type.getList()
                lkBankruptcyClass.Properties.DataSource = DebtPlus.LINQ.Cache.BankruptcyClassType.getList()

                lkCounselor.EditValue = Nothing
                lkApptType.EditValue = DebtPlus.LINQ.Cache.appt_type.getDefault()
                lkBankruptcyClass.EditValue = DebtPlus.LINQ.Cache.BankruptcyClassType.getDefault()

                ' Insert the client's zipcode into the form
                Dim adr As DebtPlus.LINQ.address = bc.clients.Join(bc.addresses, Function(cl) cl.AddressID, Function(a) a.Id, Function(cl, a) New With {Key .clientID = cl.Id, Key .a = a}).Where(Function(x) x.clientID = clientAppointment.client).Select(Function(x) x.a).FirstOrDefault()

                If adr IsNot Nothing Then
                    txtZipcode.EditValue = DebtPlus.Utils.Format.Strings.DigitsOnly(adr.PostalCode).PadRight(9, "0"c).Substring(0, 5)
                End If

                ' Set the enable condition
                SimpleButton_Search.Enabled = Not HasErrors()
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Sub SimpleButton_ClearAll_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim NewCheckedState As Boolean = (sender Is SimpleButton_SelectAll)
            chkDays_Monday.Checked = NewCheckedState
            chkDays_Tuesday.Checked = NewCheckedState
            chkDays_Wednesday.Checked = NewCheckedState
            chkDays_Thursday.Checked = NewCheckedState
            chkDays_Friday.Checked = NewCheckedState
            chkDays_Saturday.Checked = NewCheckedState
            chkDays_Sunday.Checked = NewCheckedState
        End Sub

        Private Sub lkApptType_ButtonClick(ByVal sender As Object, ByVal e As ButtonPressedEventArgs)
            If e.Button.Kind = ButtonPredefines.Delete Then
                lkApptType.EditValue = Nothing
            End If
        End Sub

        Private Sub chkOffice_Zipcode_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
            txtZipcode.Enabled = chkOffice_Zipcode.Checked
        End Sub

        Private Sub SimpleButton_Search_Click(ByVal sender As Object, ByVal e As EventArgs)

            ' Generate the search operation based upon the input controls
            Dim sel As New Appointments_Form_Book_Select.SelectionCriteria() With _
                {
                    .am_pm = Time(),
                    .apptTypeID = ApptType(),
                    .bankruptcyClassID = BankruptcyClass(),
                    .counselorID = Counselor(),
                    .datePart = Days(),
                    .priority = Priority(),
                    .zipcode = ZipCode()
                }

            Using frm As New Appointments_Form_Book_Select(bc, sel, clientAppointment)
                If frm.ShowDialog() = DialogResult.OK AndAlso sel.clientAppointment IsNot Nothing Then
                    RaiseEvent Completed(Me, sel.clientAppointment)
                End If
            End Using
        End Sub

        Private Function HasErrors() As Boolean
            Dim answer As Boolean = (lkApptType.EditValue Is Nothing)
            Return answer
        End Function

        Private Sub lkApptType_EditValueChanging(ByVal sender As Object, ByVal e As EventArgs)
            SimpleButton_Search.Enabled = Not HasErrors()
        End Sub
    End Class
End Namespace
