#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports System.Linq
Imports DebtPlus.LINQ

Namespace forms.Appointments
    Public Class Appointments_Control_Walkin
        Inherits Appointments_Control_Result

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        ''' <summary>
        ''' Process the ReadForm function reference for the control. This will load the client appointment to be processed.
        ''' </summary>
        Public Overloads Sub ReadForm(bc As BusinessContext, ByVal clientID As Int32)

            ' We need to create a new appointment for a walk-in entry. There is no existing appointment to edit.
            Dim record As client_appointment = DebtPlus.LINQ.Factory.Manufacture_client_appointment(clientID)
            record.status = "W"c
            record.start_time = DebtPlus.LINQ.BusinessContext.getdate()
            record.end_time = record.start_time
            record.date_updated = record.start_time

            ReadForm(bc, record)
        End Sub

    End Class
End Namespace
