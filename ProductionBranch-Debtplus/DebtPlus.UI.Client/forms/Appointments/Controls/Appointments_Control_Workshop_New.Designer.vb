Namespace forms.Appointments
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Appointments_Control_Workshop_New
        Inherits Appointments_Control_Workshop_New_Detail

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LabelControl_description = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl_presenter = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl_duration = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl_when = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl_where = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
            CType(Me.lkReferredBy.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.SpinEdit_booked_seats.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_HousingFeeAmount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'SimpleButton_Cancel
            '
            Me.SimpleButton_Cancel.Location = New System.Drawing.Point(213, 145)
            '
            'SimpleButton_OK
            '
            Me.SimpleButton_OK.Location = New System.Drawing.Point(134, 145)
            '
            'lkReferredBy
            '
            Me.lkReferredBy.Size = New System.Drawing.Size(222, 20)
            '
            'SpinEdit_booked_seats
            '
            Me.SpinEdit_booked_seats.Properties.DisplayFormat.FormatString = "n0"
            Me.SpinEdit_booked_seats.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.SpinEdit_booked_seats.Properties.EditFormat.FormatString = "f0"
            Me.SpinEdit_booked_seats.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.SpinEdit_booked_seats.Properties.Mask.BeepOnError = True
            Me.SpinEdit_booked_seats.Properties.Mask.EditMask = "f0"
            Me.SpinEdit_booked_seats.Properties.Mask.SaveLiteral = False
            Me.SpinEdit_booked_seats.Size = New System.Drawing.Size(65, 20)
            '
            'LabelControl_available_seats
            '
            Me.LabelControl_available_seats.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
            '
            'CalcEdit_HousingFeeAmount
            '
            Me.CalcEdit_HousingFeeAmount.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit_HousingFeeAmount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_HousingFeeAmount.Properties.DisplayFormat.FormatString = "c"
            Me.CalcEdit_HousingFeeAmount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_HousingFeeAmount.Properties.EditFormat.FormatString = "c"
            Me.CalcEdit_HousingFeeAmount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_HousingFeeAmount.Properties.Mask.BeepOnError = True
            Me.CalcEdit_HousingFeeAmount.Properties.Mask.EditMask = "c"
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.None
            Me.LayoutControl1.Location = New System.Drawing.Point(3, 101)
            Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(536, 100, 250, 350)
            Me.LayoutControl1.Size = New System.Drawing.Size(428, 180)
            Me.LayoutControl1.Controls.SetChildIndex(Me.SimpleButton_OK, 0)
            Me.LayoutControl1.Controls.SetChildIndex(Me.lkReferredBy, 0)
            Me.LayoutControl1.Controls.SetChildIndex(Me.CalcEdit_HousingFeeAmount, 0)
            Me.LayoutControl1.Controls.SetChildIndex(Me.SpinEdit_booked_seats, 0)
            Me.LayoutControl1.Controls.SetChildIndex(Me.LabelControl_booked_seats, 0)
            Me.LayoutControl1.Controls.SetChildIndex(Me.LabelControl_available_seats, 0)
            Me.LayoutControl1.Controls.SetChildIndex(Me.SimpleButton_Cancel, 0)
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(428, 180)
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LayoutControlItem1.AppearanceItemCaption.Options.UseFont = True
            Me.LayoutControlItem1.Size = New System.Drawing.Size(408, 24)
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Size = New System.Drawing.Size(408, 17)
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Size = New System.Drawing.Size(251, 24)
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Size = New System.Drawing.Size(408, 24)
            '
            'LayoutControlItem7
            '
            Me.LayoutControlItem7.Location = New System.Drawing.Point(122, 133)
            '
            'LayoutControlItem8
            '
            Me.LayoutControlItem8.Location = New System.Drawing.Point(201, 133)
            '
            'EmptySpaceItem1
            '
            Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 113)
            Me.EmptySpaceItem1.Size = New System.Drawing.Size(408, 10)
            '
            'EmptySpaceItem2
            '
            Me.EmptySpaceItem2.Location = New System.Drawing.Point(280, 133)
            Me.EmptySpaceItem2.Size = New System.Drawing.Size(128, 27)
            '
            'EmptySpaceItem3
            '
            Me.EmptySpaceItem3.Location = New System.Drawing.Point(0, 133)
            Me.EmptySpaceItem3.Size = New System.Drawing.Size(122, 27)
            '
            'EmptySpaceItem4
            '
            Me.EmptySpaceItem4.Location = New System.Drawing.Point(251, 41)
            Me.EmptySpaceItem4.Size = New System.Drawing.Size(157, 24)
            '
            'EmptySpaceItem5
            '
            Me.EmptySpaceItem5.Location = New System.Drawing.Point(0, 123)
            Me.EmptySpaceItem5.Size = New System.Drawing.Size(408, 10)
            '
            'LabelControl_description
            '
            Me.LabelControl_description.Appearance.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
            Me.LabelControl_description.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.LabelControl_description.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
            Me.LabelControl_description.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.LabelControl_description.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_description.Dock = System.Windows.Forms.DockStyle.Top
            Me.LabelControl_description.Location = New System.Drawing.Point(0, 0)
            Me.LabelControl_description.Name = "LabelControl_description"
            Me.LabelControl_description.Size = New System.Drawing.Size(434, 19)
            Me.LabelControl_description.TabIndex = 0
            Me.LabelControl_description.Text = "Workshop Appointment"
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(4, 25)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(36, 13)
            Me.LabelControl1.TabIndex = 1
            Me.LabelControl1.Text = "Where:"
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(4, 44)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(32, 13)
            Me.LabelControl2.TabIndex = 2
            Me.LabelControl2.Text = "When:"
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(4, 63)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(45, 13)
            Me.LabelControl3.TabIndex = 3
            Me.LabelControl3.Text = "Duration:"
            '
            'LabelControl4
            '
            Me.LabelControl4.Location = New System.Drawing.Point(4, 82)
            Me.LabelControl4.Name = "LabelControl4"
            Me.LabelControl4.Size = New System.Drawing.Size(51, 13)
            Me.LabelControl4.TabIndex = 4
            Me.LabelControl4.Text = "Presenter:"
            '
            'LabelControl_presenter
            '
            Me.LabelControl_presenter.Location = New System.Drawing.Point(188, 82)
            Me.LabelControl_presenter.Name = "LabelControl_presenter"
            Me.LabelControl_presenter.Size = New System.Drawing.Size(48, 13)
            Me.LabelControl_presenter.TabIndex = 8
            Me.LabelControl_presenter.Text = "Counselor"
            '
            'LabelControl_duration
            '
            Me.LabelControl_duration.Location = New System.Drawing.Point(188, 63)
            Me.LabelControl_duration.Name = "LabelControl_duration"
            Me.LabelControl_duration.Size = New System.Drawing.Size(41, 13)
            Me.LabelControl_duration.TabIndex = 7
            Me.LabelControl_duration.Text = "Duration"
            '
            'LabelControl_when
            '
            Me.LabelControl_when.Location = New System.Drawing.Point(188, 44)
            Me.LabelControl_when.Name = "LabelControl_when"
            Me.LabelControl_when.Size = New System.Drawing.Size(28, 13)
            Me.LabelControl_when.TabIndex = 6
            Me.LabelControl_when.Text = "When"
            '
            'LabelControl_where
            '
            Me.LabelControl_where.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Underline)
            Me.LabelControl_where.Location = New System.Drawing.Point(188, 25)
            Me.LabelControl_where.Name = "LabelControl_where"
            Me.LabelControl_where.Size = New System.Drawing.Size(32, 13)
            Me.LabelControl_where.TabIndex = 5
            Me.LabelControl_where.Text = "Where"
            '
            'LabelControl15
            '
            Me.LabelControl15.Appearance.ForeColor = System.Drawing.Color.Orchid
            Me.LabelControl15.Dock = System.Windows.Forms.DockStyle.Bottom
            Me.LabelControl15.Location = New System.Drawing.Point(0, 281)
            Me.LabelControl15.Name = "LabelControl15"
            Me.LabelControl15.Padding = New System.Windows.Forms.Padding(5, 0, 5, 3)
            Me.LabelControl15.Size = New System.Drawing.Size(419, 16)
            Me.LabelControl15.TabIndex = 19
            Me.LabelControl15.Text = "Note: to cancel an appointment you may set the number of people attending to zero" & _
        "."
            '
            'Appointments_Control_Workshop_New
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.LabelControl15)
            Me.Controls.Add(Me.LabelControl_presenter)
            Me.Controls.Add(Me.LabelControl_duration)
            Me.Controls.Add(Me.LabelControl_when)
            Me.Controls.Add(Me.LabelControl_where)
            Me.Controls.Add(Me.LabelControl4)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.LabelControl_description)
            Me.Name = "Appointments_Control_Workshop_New"
            Me.Size = New System.Drawing.Size(434, 297)
            Me.Controls.SetChildIndex(Me.LayoutControl1, 0)
            Me.Controls.SetChildIndex(Me.LabelControl_description, 0)
            Me.Controls.SetChildIndex(Me.LabelControl1, 0)
            Me.Controls.SetChildIndex(Me.LabelControl2, 0)
            Me.Controls.SetChildIndex(Me.LabelControl3, 0)
            Me.Controls.SetChildIndex(Me.LabelControl4, 0)
            Me.Controls.SetChildIndex(Me.LabelControl_where, 0)
            Me.Controls.SetChildIndex(Me.LabelControl_when, 0)
            Me.Controls.SetChildIndex(Me.LabelControl_duration, 0)
            Me.Controls.SetChildIndex(Me.LabelControl_presenter, 0)
            Me.Controls.SetChildIndex(Me.LabelControl15, 0)
            CType(Me.lkReferredBy.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.SpinEdit_booked_seats.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_HousingFeeAmount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem5, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Protected Friend WithEvents LabelControl_description As DevExpress.XtraEditors.LabelControl
        Protected Friend WithEvents LabelControl_presenter As DevExpress.XtraEditors.LabelControl
        Protected Friend WithEvents LabelControl_duration As DevExpress.XtraEditors.LabelControl
        Protected Friend WithEvents LabelControl_when As DevExpress.XtraEditors.LabelControl
        Protected Friend WithEvents LabelControl_where As DevExpress.XtraEditors.LabelControl
        Public WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Public WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Public WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Public WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
        Public WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    End Class
End NameSpace