Namespace forms.Appointments
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Appointments_Control_Book_Workshop
        Inherits DevExpress.XtraEditors.XtraUserControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
            Me.GridColumn_seats = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_seats.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_workshop = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_workshop.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_date = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_counselor = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_counselor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_location = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_location.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_name = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridControl1
            '
            Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.GridControl1.EmbeddedNavigator.Name = ""
            Me.GridControl1.Location = New System.Drawing.Point(0, 0)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(469, 205)
            Me.GridControl1.TabIndex = 0
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_seats, Me.GridColumn_date, Me.GridColumn_counselor, Me.GridColumn_location, Me.GridColumn_name, Me.GridColumn_workshop})
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            '
            'GridColumn_seats
            '
            Me.GridColumn_seats.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_seats.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_seats.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_seats.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_seats.Caption = "Seats"
            Me.GridColumn_seats.CustomizationCaption = "Number of available seats"
            Me.GridColumn_seats.DisplayFormat.FormatString = "{0:n0}"
            Me.GridColumn_seats.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_seats.FieldName = "seats_available"
            Me.GridColumn_seats.GroupFormat.FormatString = "{0:n0}"
            Me.GridColumn_seats.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_seats.Name = "GridColumn_seats"
            Me.GridColumn_seats.Visible = True
            Me.GridColumn_seats.VisibleIndex = 0
            '
            'GridColumn_workshop
            '
            Me.GridColumn_workshop.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_workshop.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_workshop.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_workshop.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_workshop.Caption = "ID"
            Me.GridColumn_workshop.CustomizationCaption = "Record Number"
            Me.GridColumn_workshop.DisplayFormat.FormatString = "f0"
            Me.GridColumn_workshop.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_workshop.FieldName = "workshop"
            Me.GridColumn_workshop.GroupFormat.FormatString = "f0"
            Me.GridColumn_workshop.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_workshop.Name = "GridColumn_workshop"
            Me.GridColumn_workshop.Visible = False
            Me.GridColumn_workshop.VisibleIndex = -1
            '
            'GridColumn_date
            '
            Me.GridColumn_date.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_date.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_date.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date.Caption = "Date/Time"
            Me.GridColumn_date.CustomizationCaption = "Date and Time of the workshop"
            Me.GridColumn_date.DisplayFormat.FormatString = "ddd M/d/yy h:mm tt"
            Me.GridColumn_date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date.FieldName = "start_time"
            Me.GridColumn_date.GroupFormat.FormatString = "d"
            Me.GridColumn_date.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date.Name = "GridColumn_date"
            Me.GridColumn_date.Visible = True
            Me.GridColumn_date.VisibleIndex = 1
            '
            'GridColumn_counselor
            '
            Me.GridColumn_counselor.Caption = "Presenter"
            Me.GridColumn_counselor.CustomizationCaption = "Person conducting workshop"
            Me.GridColumn_counselor.FieldName = "counselor"
            Me.GridColumn_counselor.Name = "GridColumn_counselor"
            Me.GridColumn_counselor.Visible = True
            Me.GridColumn_counselor.VisibleIndex = 2
            '
            'GridColumn_location
            '
            Me.GridColumn_location.Caption = "Location"
            Me.GridColumn_location.CustomizationCaption = "Location for workshop"
            Me.GridColumn_location.FieldName = "workshop_location"
            Me.GridColumn_location.Name = "GridColumn_location"
            Me.GridColumn_location.Visible = True
            Me.GridColumn_location.VisibleIndex = 3
            '
            'GridColumn_name
            '
            Me.GridColumn_name.Caption = "Name"
            Me.GridColumn_name.CustomizationCaption = "Name of the workshop"
            Me.GridColumn_name.FieldName = "description"
            Me.GridColumn_name.Name = "GridColumn_name"
            Me.GridColumn_name.Visible = True
            Me.GridColumn_name.VisibleIndex = 4
            '
            'Appointments_Control_Book_Workshop
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.GridControl1)
            Me.Name = "Appointments_Control_Book_Workshop"
            Me.Size = New System.Drawing.Size(469, 205)
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Public WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Public WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Public WithEvents GridColumn_seats As DevExpress.XtraGrid.Columns.GridColumn
        Public WithEvents GridColumn_workshop As DevExpress.XtraGrid.Columns.GridColumn
        Public WithEvents GridColumn_date As DevExpress.XtraGrid.Columns.GridColumn
        Public WithEvents GridColumn_counselor As DevExpress.XtraGrid.Columns.GridColumn
        Public WithEvents GridColumn_location As DevExpress.XtraGrid.Columns.GridColumn
        Public WithEvents GridColumn_name As DevExpress.XtraGrid.Columns.GridColumn

    End Class
End NameSpace