#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DebtPlus.Svc.Client.Appointments
Imports DebtPlus.Utils.Format

Imports DebtPlus.LINQ
Imports System.Linq

Namespace forms.Appointments

    Public Class Appointments_Control_Confirmation

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Public Event Completed(ByVal Sender As Object, ByVal e As EventArgs)
        Public Event Cancelled(ByVal sender As Object, ByVal e As EventArgs)

        Private record As client_appointment = Nothing
        Protected bc As BusinessContext = Nothing

        Private Class clientInfo
            Public Property language As Int32?
            Public Property AddressID As Int32?
            Public Property NameID As Int32?
            Public Property HomeTelephoneID As Int32?
        End Class

        Private Sub RegisterHandlers()
            AddHandler lkConfirmation.EditValueChanged, AddressOf lkConfirmation_EditValueChanged
            AddHandler lkConfirmation.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler lkCounselor.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler lkReferredBy.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
            AddHandler SimpleButton_Cancel.Click, AddressOf SimpleButton_Cancel_Click
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler lkConfirmation.EditValueChanged, AddressOf lkConfirmation_EditValueChanged
            RemoveHandler lkConfirmation.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler lkCounselor.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler lkReferredBy.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
            RemoveHandler SimpleButton_Cancel.Click, AddressOf SimpleButton_Cancel_Click
        End Sub

        ''' <summary>
        ''' Read the appointment information and update the form
        ''' </summary>
        Public Sub ReadForm(bc As BusinessContext, ByVal record As client_appointment)
            Me.record = record
            Me.bc = bc

            UnRegisterHandlers()
            Try
                ' Find the appointment's priority status
                LabelControl_Priority.Text = If(record.priority, "YES", "NO")

                ' Find the status information
                Select Case record.status
                    Case "C"c : LabelControl_Status.Text = "CANCELLED"
                    Case "R"c : LabelControl_Status.Text = "RESCHEDULED"
                    Case "K"c : LabelControl_Status.Text = "COUNSELED"
                    Case "W"c : LabelControl_Status.Text = "WALK-IN"
                    Case Else : LabelControl_Status.Text = "PENDING"
                End Select

                ' Find the appointment date/time
                LabelControl_StartTime.Text = String.Format("{0:ddd MMMM d, yyyy \a\t h:mm tt}", record.start_time)

                ' Look at the client for additional data
                'Dim q As clientInfo = (From c In bc.clients Join p In bc.peoples On c.Id Equals p.Client Where p.Relation = 1 AndAlso c.Id = record.client Select New clientInfo With {.language = c.language, .AddressID = c.AddressID, .HomeTelephoneID = c.HomeTelephoneID, .NameID = p.NameID}).FirstOrDefault()
                Dim q As clientInfo = bc.clients.Join(bc.peoples, Function(cl) cl.Id, Function(p) p.Client, Function(cl, p) New With {Key .person = p.Id, .language = cl.language, .AddressID = cl.AddressID, .HomeTelephoneID = cl.HomeTelephoneID, .NameID = p.NameID, .Relation = p.Relation, .ClientID = cl.Id}).Where(Function(x) x.ClientID = record.client AndAlso x.Relation = 1).Select(Function(x) New clientInfo With {.language = x.language, .AddressID = x.AddressID, .HomeTelephoneID = x.HomeTelephoneID, .NameID = x.NameID}).FirstOrDefault()
                If q IsNot Nothing Then

                    ' Find the client's name
                    Dim nameString As String = String.Empty
                    If q.NameID.HasValue AndAlso q.NameID.Value > 0 Then
                        Dim nameRecord As Name = bc.Names.Where(Function(n) n.Id = q.NameID.Value).FirstOrDefault()
                        If nameRecord IsNot Nothing Then
                            nameString = nameRecord.ToString()
                        End If
                    End If
                    LabelControl_Client.Text = String.Format("{0} {1}", String.Format("{0:0000000}", record.client), nameString)

                    ' Find the client's telephone number
                    If q.HomeTelephoneID.HasValue AndAlso q.HomeTelephoneID.Value > 0 Then
                        Dim telephoneRecord As TelephoneNumber = bc.TelephoneNumbers.Where(Function(t) t.Id = q.HomeTelephoneID.Value).FirstOrDefault()
                        If telephoneRecord IsNot Nothing Then
                            LabelControl_HomePhone.Text = telephoneRecord.ToString()
                        End If
                    End If

                    ' Find the language information
                    LabelControl_Language.Text = "English (default)"
                    If q.language.HasValue AndAlso q.language.Value > 0 Then
                        Dim qLanguage As AttributeType = DebtPlus.LINQ.Cache.AttributeType.getList().Find(Function(s) s.Id = q.language.Value)
                        If qLanguage IsNot Nothing Then
                            LabelControl_Language.Text = qLanguage.Attribute
                        End If
                    End If
                End If

                ' Load the update controls with the proper values
                lkCounselor.Properties.DataSource = DebtPlus.LINQ.Cache.counselor.CounselorsList()
                lkReferredBy.Properties.DataSource = DebtPlus.LINQ.Cache.referred_by.getList()

                ' Bind the confirmation type to the appointment
                lkConfirmation.Properties.DataSource = DebtPlus.LINQ.Cache.AppointmentConfirmationType.getList()
                lkConfirmation.EditValue = record.confirmation_status
                lkCounselor.EditValue = record.counselor
                lkReferredBy.EditValue = record.referred_by

                SimpleButton_OK.Enabled = DebtPlus.Utils.Nulls.v_Int32(lkConfirmation.EditValue).GetValueOrDefault(-1) >= 0

            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Sub lkConfirmation_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            SimpleButton_OK.Enabled = DebtPlus.Utils.Nulls.v_Int32(lkConfirmation.EditValue).GetValueOrDefault(-1) >= 0
        End Sub

        Private Sub SimpleButton_OK_Click(ByVal sender As Object, ByVal e As EventArgs)

            record.counselor = DebtPlus.Utils.Nulls.v_Int32(lkCounselor.EditValue)
            record.referred_by = DebtPlus.Utils.Nulls.v_Int32(lkReferredBy.EditValue)
            record.confirmation_status = DebtPlus.Utils.Nulls.v_Int32(lkConfirmation.EditValue).GetValueOrDefault(0)

            Try
                Dim q As client_appointment = bc.client_appointments.Where(Function(ca) ca.Id = record.Id).FirstOrDefault()
                If q IsNot Nothing Then
                    q.counselor = record.counselor
                    q.referred_by = record.referred_by
                    q.confirmation_status = record.confirmation_status
                    bc.SubmitChanges()
                End If

                ' Tell the user that the appointment was updated correctly
                RaiseEvent Completed(Me, EventArgs.Empty)

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating client appointment table")
            End Try
        End Sub

        Private Sub SimpleButton_Cancel_Click(ByVal sender As Object, ByVal e As EventArgs)
            RaiseEvent Cancelled(Me, EventArgs.Empty)
        End Sub
    End Class
End Namespace
