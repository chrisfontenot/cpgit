#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DebtPlus.Svc.Client.Appointments

Imports DebtPlus.LINQ
Imports System.Linq

Namespace forms.Appointments

    Public Class Appointments_Control_Workshop_New_Detail

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Protected clientAppointment As client_appointment
        Protected bc As BusinessContext = Nothing

        ' Completion event when we have finished
        Public Event Completed(ByVal sender As Object, ByVal e As DebtPlus.LINQ.client_appointment)
        Public Event Cancelled(ByVal sender As Object, ByVal e As EventArgs)

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
            AddHandler SimpleButton_Cancel.Click, AddressOf SimpleButton_Cancel_Click
            AddHandler SpinEdit_booked_seats.EditValueChanged, AddressOf FormUpdated
            AddHandler lkReferredBy.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler lkReferredBy.EditValueChanged, AddressOf lkReferredBy_EditValueChanged
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
            RemoveHandler SimpleButton_Cancel.Click, AddressOf SimpleButton_Cancel_Click
            RemoveHandler SpinEdit_booked_seats.EditValueChanged, AddressOf FormUpdated
            RemoveHandler lkReferredBy.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler lkReferredBy.EditValueChanged, AddressOf lkReferredBy_EditValueChanged
        End Sub

        Protected Sub RaiseCancelled(ByVal e As EventArgs)
            RaiseEvent Cancelled(Me, e)
        End Sub

        Protected Overridable Sub OnCancelled(ByVal e As EventArgs)
            RaiseCancelled(e)
        End Sub

        Protected Sub RaiseCompleted(ByVal e As DebtPlus.LINQ.client_appointment)
            RaiseEvent Completed(Me, e)
        End Sub

        Protected Overridable Sub OnCompleted(ByVal e As DebtPlus.LINQ.client_appointment)
            RaiseCompleted(e)
        End Sub

        Public Overridable Sub ReadForm(bc As BusinessContext, ByVal clientAppointment As client_appointment)
            Me.bc = bc
            UnRegisterHandlers()
            Try
                Me.clientAppointment = clientAppointment
                lkReferredBy.Properties.DataSource = DebtPlus.LINQ.Cache.referred_by.getList()

                ' Supply the information about the current client_appointment record
                lkReferredBy.EditValue = clientAppointment.referred_by
                CalcEdit_HousingFeeAmount.EditValue = clientAppointment.HousingFeeAmount
                SpinEdit_booked_seats.EditValue = clientAppointment.workshop_people
                TextEdit_partner_code.EditValue = clientAppointment.partner

                EnablePartner()
                SimpleButton_OK.Enabled = Not HasErrors()

            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Process the change in the referral source for the appointment
        ''' </summary>
        Private Sub lkReferredBy_EditValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            UnRegisterHandlers()
            Try
                EnablePartner()
                SimpleButton_OK.Enabled = Not HasErrors()
            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Control the editing of the partner code value
        ''' </summary>
        Private Sub EnablePartner()

            ' Remove the validation handler for the partner. It will be re-enabled if we have one
            RemoveHandler TextEdit_partner_code.Validating, AddressOf TextEdit_partner_code_Validating

            ' Retrieve the referral source. If missing then there is no referral.
            Dim referral As DebtPlus.LINQ.referred_by = TryCast(lkReferredBy.GetSelectedDataRow(), DebtPlus.LINQ.referred_by)
            If referral Is Nothing Then
                TextEdit_partner_code.EditValue = Nothing
                TextEdit_partner_code.Enabled = False
                Return
            End If

            ' If the referral does not allow the partner code then remove it
            Dim Validation As String = referral.partnerValidation
            If String.IsNullOrEmpty(Validation) Then
                TextEdit_partner_code.EditValue = Nothing
                TextEdit_partner_code.Enabled = False
                Return
            End If

            ' Enable the partner code entry
            TextEdit_partner_code.Enabled = True

            ' Try to determine if the partner code as entered is valid. If not, clear it.
            Try
                Dim currentValue As String = Convert.ToString(TextEdit_partner_code.EditValue)
                If currentValue Is Nothing Then currentValue = String.Empty

                If Not System.Text.RegularExpressions.Regex.IsMatch(currentValue, "^" + Validation + "$") Then
                    TextEdit_partner_code.EditValue = Nothing
                End If
            Catch
            End Try

            ' Set the referral code value
            Try
                TextEdit_partner_code.Properties.Mask.BeepOnError = True
                TextEdit_partner_code.Properties.Mask.EditMask = Validation
                TextEdit_partner_code.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
                TextEdit_partner_code.Properties.Mask.SaveLiteral = True
                TextEdit_partner_code.Properties.Mask.ShowPlaceHolders = True

                ' Finally, re-enable the validation code validation so that it is checked for the full expression
                AddHandler TextEdit_partner_code.Validating, AddressOf TextEdit_partner_code_Validating
            Catch
            End Try
        End Sub

        ''' <summary>
        ''' Validate the partner code to ensure that it is acceptable for input
        ''' </summary>
        Private Sub TextEdit_partner_code_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs)

            ' No partner code is acceptable for the entry. It will be caught elsewhere
            Dim strCode As String = TryCast(TextEdit_partner_code.EditValue, String)
            If strCode Is Nothing Then
                TextEdit_partner_code.ErrorText = "A value is required for this field"
                Return
            End If

            ' Pass the code through the regular expression routine to see if it is valid. We put the
            ' beginning of string and end of string anchors on the expression to ensure that the entire
            ' string matches the entire expression and not just some sub-string component.
            Try
                If System.Text.RegularExpressions.Regex.IsMatch(strCode, "^" + TextEdit_partner_code.Properties.Mask.EditMask + "$") Then
                    TextEdit_partner_code.ErrorText = String.Empty
                    Return
                End If

                ' The entry is not completely valid. Reject it.
                TextEdit_partner_code.ErrorText = "invalid entry"

            Catch ex As System.Exception
                TextEdit_partner_code.ErrorText = ex.Message
            End Try

            e.Cancel = True
        End Sub

        Private Sub FormUpdated(ByVal Sender As Object, ByVal e As EventArgs)
            SimpleButton_OK.Enabled = Not HasErrors()
        End Sub

        Protected Overridable Function HasErrors() As Boolean
            If Convert.ToInt32(SpinEdit_booked_seats.EditValue) <= 0 Then Return True
            Return False
        End Function

        Private Sub SimpleButton_Cancel_Click(ByVal sender As Object, ByVal e As EventArgs)
            RaiseCancelled(EventArgs.Empty)
        End Sub

        Private Sub SimpleButton_OK_Click(ByVal sender As Object, ByVal e As EventArgs)

            ' Update the record with the control values
            clientAppointment.referred_by = DebtPlus.Utils.Nulls.v_Int32(lkReferredBy.EditValue)
            clientAppointment.HousingFeeAmount = Convert.ToDecimal(CalcEdit_HousingFeeAmount.EditValue)
            clientAppointment.workshop_people = DebtPlus.Utils.Nulls.v_Int32(SpinEdit_booked_seats.EditValue)
            clientAppointment.partner = DebtPlus.Utils.Nulls.v_String(TextEdit_partner_code.EditValue)

            ' Mark the record as canceled if there are no people attending the workshop
            If clientAppointment.workshop_people.GetValueOrDefault(0) <= 0 Then
                clientAppointment.workshop_people = 0
                clientAppointment.status = "C"c
            End If

            Using cn As New System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault.ConnectionString)
                cn.Open()
                Dim txn As System.Data.SqlClient.SqlTransaction = cn.BeginTransaction(IsolationLevel.RepeatableRead)
                Using transaction_bc As New BusinessContext(cn)
                    transaction_bc.Transaction = txn
                    Try

                        ' If there was no appointment and it is now cancelled then ignore the update.
                        If clientAppointment.Id <= 0 AndAlso clientAppointment.status = "C"c Then
                            RaiseCancelled(EventArgs.Empty)
                            Return
                        End If

                        ' Find the current record and update it if it is found
                        Dim q As client_appointment = transaction_bc.client_appointments.Where(Function(ca) ca.Id = clientAppointment.Id).FirstOrDefault()
                        If q IsNot Nothing Then
                            q.partner = clientAppointment.partner
                            q.status = clientAppointment.status
                            q.workshop_people = clientAppointment.workshop_people
                            q.referred_by = clientAppointment.referred_by
                            q.referred_to = clientAppointment.referred_to
                            q.priority = clientAppointment.priority
                            q.HousingFeeAmount = clientAppointment.HousingFeeAmount
                            q.bankruptcy_class = clientAppointment.bankruptcy_class
                            q.callback_ph = clientAppointment.callback_ph
                            q.confirmation_status = clientAppointment.confirmation_status
                            q.counselor = clientAppointment.counselor
                            q.date_confirmed = clientAppointment.date_confirmed
                            q.end_time = clientAppointment.end_time
                            q.start_time = clientAppointment.start_time
                            q.post_purchase = clientAppointment.post_purchase
                            q.housing = clientAppointment.housing
                            q.credit = clientAppointment.credit

                            ' If the clientAppointment is changed then set the date_updated field as well
                            If transaction_bc.GetChangeSet.Updates.Count > 0 Then
                                q.date_updated = DebtPlus.LINQ.BusinessContext.getdate()
                            End If
                        Else

                            ' Add the new record to the system
                            clientAppointment.date_updated = DebtPlus.LINQ.BusinessContext.getdate()
                            transaction_bc.client_appointments.InsertOnSubmit(clientAppointment)
                        End If

                        transaction_bc.SubmitChanges()

                        ' Find the number of people attending the workshop at this point.
                        Dim peopleAttending As Int32 = If(clientAppointment.workshop.HasValue, transaction_bc.client_appointments.Where(Function(ca) ca.status = "P" AndAlso ca.workshop.GetValueOrDefault() = clientAppointment.workshop.Value).Select(Function(ca) ca.workshop_people).Sum().GetValueOrDefault(0), 0)
                        Dim wks As DebtPlus.LINQ.workshop = transaction_bc.workshops.Where(Function(w) w.Id = clientAppointment.workshop.Value).FirstOrDefault()
                        If wks IsNot Nothing AndAlso wks.seats_available < peopleAttending Then
                            DebtPlus.Data.Forms.MessageBox.Show("There are not enough seats for the workshop. Please find a new workshop or reduce the attendance.", "Sorry, but booking is not possible", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Else

                            ' Commit the changes to the database and release the locks
                            txn.Commit()
                            txn.Dispose()
                            txn = Nothing

                            RaiseCompleted(clientAppointment)
                        End If

                    Catch ex As System.Data.SqlClient.SqlException
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error creating appointment")

                    Finally
                        ' Roll the changes out of the database
                        If txn IsNot Nothing Then
                            txn.Rollback()
                            txn.Dispose()
                            txn = Nothing
                        End If
                    End Try
                End Using
            End Using
        End Sub
    End Class
End Namespace
