Namespace forms.Appointments
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Appointments_Control_Result
        Inherits DevExpress.XtraEditors.XtraUserControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Appointments_Control_Result))
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.LabelControl_duration_label = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl_hud_note = New DevExpress.XtraEditors.LabelControl()
            Me.TextEdit_partner_code = New DevExpress.XtraEditors.TextEdit()
            Me.chkCredit = New DevExpress.XtraEditors.CheckEdit()
            Me.SimpleButton_Cancel = New DevExpress.XtraEditors.SimpleButton()
            Me.spnDuration = New DevExpress.XtraEditors.SpinEdit()
            Me.SimpleButton_OK = New DevExpress.XtraEditors.SimpleButton()
            Me.lkBankruptcyClass = New DevExpress.XtraEditors.LookUpEdit()
            Me.SimpleButton_HUD = New DevExpress.XtraEditors.SimpleButton()
            Me.lkCounselor = New DevExpress.XtraEditors.LookUpEdit()
            Me.lkClientStatus = New DevExpress.XtraEditors.LookUpEdit()
            Me.lkOffice = New DevExpress.XtraEditors.LookUpEdit()
            Me.lkReferredBy = New DevExpress.XtraEditors.LookUpEdit()
            Me.lkApptType = New DevExpress.XtraEditors.LookUpEdit()
            Me.lkReferredTo = New DevExpress.XtraEditors.LookUpEdit()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_hud = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.TextEdit_partner_code.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.chkCredit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.spnDuration.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lkBankruptcyClass.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lkCounselor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lkClientStatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lkOffice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lkReferredBy.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lkApptType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lkReferredTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_hud, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl1.Location = New System.Drawing.Point(12, 12)
            Me.LabelControl1.MinimumSize = New System.Drawing.Size(0, 45)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(484, 45)
            Me.LabelControl1.StyleController = Me.LayoutControl1
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = resources.GetString("LabelControl1.Text")
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.LabelControl1)
            Me.LayoutControl1.Controls.Add(Me.LabelControl_duration_label)
            Me.LayoutControl1.Controls.Add(Me.LabelControl_hud_note)
            Me.LayoutControl1.Controls.Add(Me.TextEdit_partner_code)
            Me.LayoutControl1.Controls.Add(Me.chkCredit)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton_Cancel)
            Me.LayoutControl1.Controls.Add(Me.spnDuration)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton_OK)
            Me.LayoutControl1.Controls.Add(Me.lkBankruptcyClass)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton_HUD)
            Me.LayoutControl1.Controls.Add(Me.lkCounselor)
            Me.LayoutControl1.Controls.Add(Me.lkClientStatus)
            Me.LayoutControl1.Controls.Add(Me.lkOffice)
            Me.LayoutControl1.Controls.Add(Me.lkReferredBy)
            Me.LayoutControl1.Controls.Add(Me.lkApptType)
            Me.LayoutControl1.Controls.Add(Me.lkReferredTo)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.OptionsFocus.MoveFocusDirection = DevExpress.XtraLayout.MoveFocusDirection.DownThenAcross
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(508, 310)
            Me.LayoutControl1.TabIndex = 25
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'LabelControl_duration_label
            '
            Me.LabelControl_duration_label.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl_duration_label.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_duration_label.Location = New System.Drawing.Point(256, 253)
            Me.LabelControl_duration_label.Name = "LabelControl_duration_label"
            Me.LabelControl_duration_label.Size = New System.Drawing.Size(240, 13)
            Me.LabelControl_duration_label.StyleController = Me.LayoutControl1
            Me.LabelControl_duration_label.TabIndex = 19
            Me.LabelControl_duration_label.Text = "(in minutes)"
            Me.LabelControl_duration_label.UseMnemonic = False
            '
            'LabelControl_hud_note
            '
            Me.LabelControl_hud_note.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl_hud_note.Appearance.BackColor = System.Drawing.Color.Red
            Me.LabelControl_hud_note.Appearance.ForeColor = System.Drawing.Color.White
            Me.LabelControl_hud_note.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.LabelControl_hud_note.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap
            Me.LabelControl_hud_note.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl_hud_note.Location = New System.Drawing.Point(189, 277)
            Me.LabelControl_hud_note.Name = "LabelControl_hud_note"
            Me.LabelControl_hud_note.Size = New System.Drawing.Size(307, 13)
            Me.LabelControl_hud_note.StyleController = Me.LayoutControl1
            Me.LabelControl_hud_note.TabIndex = 21
            Me.LabelControl_hud_note.Text = "Please do not forget to enter the H.U.D. information"
            Me.LabelControl_hud_note.UseMnemonic = False
            Me.LabelControl_hud_note.Visible = False
            '
            'TextEdit_partner_code
            '
            Me.TextEdit_partner_code.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.TextEdit_partner_code.Location = New System.Drawing.Point(163, 109)
            Me.TextEdit_partner_code.Name = "TextEdit_partner_code"
            Me.TextEdit_partner_code.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.TextEdit_partner_code.Properties.MaxLength = 50
            Me.TextEdit_partner_code.Size = New System.Drawing.Size(254, 20)
            Me.TextEdit_partner_code.StyleController = Me.LayoutControl1
            Me.TextEdit_partner_code.TabIndex = 6
            '
            'chkCredit
            '
            Me.chkCredit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.chkCredit.Location = New System.Drawing.Point(12, 277)
            Me.chkCredit.Name = "chkCredit"
            Me.chkCredit.Properties.Caption = "This is a Cr&edit Review session"
            Me.chkCredit.Size = New System.Drawing.Size(173, 19)
            Me.chkCredit.StyleController = Me.LayoutControl1
            Me.chkCredit.TabIndex = 20
            '
            'SimpleButton_Cancel
            '
            Me.SimpleButton_Cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton_Cancel.Location = New System.Drawing.Point(421, 115)
            Me.SimpleButton_Cancel.MaximumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.MinimumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.Name = "SimpleButton_Cancel"
            Me.SimpleButton_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.StyleController = Me.LayoutControl1
            Me.SimpleButton_Cancel.TabIndex = 24
            Me.SimpleButton_Cancel.Text = "&Cancel"
            '
            'spnDuration
            '
            Me.spnDuration.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
            Me.spnDuration.Location = New System.Drawing.Point(163, 253)
            Me.spnDuration.Name = "spnDuration"
            Me.spnDuration.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.spnDuration.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.[Default]
            Me.spnDuration.Properties.Increment = New Decimal(New Integer() {15, 0, 0, 0})
            Me.spnDuration.Properties.IsFloatValue = False
            Me.spnDuration.Properties.Mask.BeepOnError = True
            Me.spnDuration.Properties.Mask.EditMask = "\d+"
            Me.spnDuration.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.spnDuration.Size = New System.Drawing.Size(89, 20)
            Me.spnDuration.StyleController = Me.LayoutControl1
            Me.spnDuration.TabIndex = 18
            '
            'SimpleButton_OK
            '
            Me.SimpleButton_OK.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton_OK.Location = New System.Drawing.Point(421, 88)
            Me.SimpleButton_OK.MaximumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.MinimumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.Name = "SimpleButton_OK"
            Me.SimpleButton_OK.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.StyleController = Me.LayoutControl1
            Me.SimpleButton_OK.TabIndex = 23
            Me.SimpleButton_OK.Text = "&OK"
            '
            'lkBankruptcyClass
            '
            Me.lkBankruptcyClass.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lkBankruptcyClass.Location = New System.Drawing.Point(163, 229)
            Me.lkBankruptcyClass.Name = "lkBankruptcyClass"
            Me.lkBankruptcyClass.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lkBankruptcyClass.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 25, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 75, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.lkBankruptcyClass.Properties.DisplayMember = "description"
            Me.lkBankruptcyClass.Properties.NullText = "Please Choose a value from the following list"
            Me.lkBankruptcyClass.Properties.ShowFooter = False
            Me.lkBankruptcyClass.Properties.ShowHeader = False
            Me.lkBankruptcyClass.Properties.SortColumnIndex = 1
            Me.lkBankruptcyClass.Properties.ValueMember = "Id"
            Me.lkBankruptcyClass.Size = New System.Drawing.Size(254, 20)
            Me.lkBankruptcyClass.StyleController = Me.LayoutControl1
            Me.lkBankruptcyClass.TabIndex = 16
            '
            'SimpleButton_HUD
            '
            Me.SimpleButton_HUD.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton_HUD.Location = New System.Drawing.Point(421, 61)
            Me.SimpleButton_HUD.MaximumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_HUD.MinimumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_HUD.Name = "SimpleButton_HUD"
            Me.SimpleButton_HUD.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_HUD.StyleController = Me.LayoutControl1
            Me.SimpleButton_HUD.TabIndex = 22
            Me.SimpleButton_HUD.Text = "H.U.D. ..."
            '
            'lkCounselor
            '
            Me.lkCounselor.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lkCounselor.Location = New System.Drawing.Point(163, 205)
            Me.lkCounselor.Name = "lkCounselor"
            Me.lkCounselor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lkCounselor.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 5, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Note", 5, "Note")})
            Me.lkCounselor.Properties.DisplayMember = "Name"
            Me.lkCounselor.Properties.NullText = ""
            Me.lkCounselor.Properties.ShowFooter = False
            Me.lkCounselor.Properties.ShowLines = False
            Me.lkCounselor.Properties.SortColumnIndex = 1
            Me.lkCounselor.Properties.ValueMember = "Id"
            Me.lkCounselor.Size = New System.Drawing.Size(254, 20)
            Me.lkCounselor.StyleController = Me.LayoutControl1
            Me.lkCounselor.TabIndex = 14
            Me.lkCounselor.ToolTip = "The counselor assigned to this appointment."
            '
            'lkClientStatus
            '
            Me.lkClientStatus.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lkClientStatus.Location = New System.Drawing.Point(163, 61)
            Me.lkClientStatus.Name = "lkClientStatus"
            Me.lkClientStatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lkClientStatus.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 25, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 75, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.lkClientStatus.Properties.DisplayMember = "description"
            Me.lkClientStatus.Properties.NullText = "Please Choose a value from the following list"
            Me.lkClientStatus.Properties.ShowFooter = False
            Me.lkClientStatus.Properties.ShowHeader = False
            Me.lkClientStatus.Properties.SortColumnIndex = 1
            Me.lkClientStatus.Properties.ValueMember = "Id"
            Me.lkClientStatus.Size = New System.Drawing.Size(254, 20)
            Me.lkClientStatus.StyleController = Me.LayoutControl1
            Me.lkClientStatus.TabIndex = 2
            '
            'lkOffice
            '
            Me.lkOffice.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lkOffice.Location = New System.Drawing.Point(163, 181)
            Me.lkOffice.Name = "lkOffice"
            Me.lkOffice.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lkOffice.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("name", "Name", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.lkOffice.Properties.DisplayMember = "name"
            Me.lkOffice.Properties.NullText = ""
            Me.lkOffice.Properties.ShowFooter = False
            Me.lkOffice.Properties.ShowHeader = False
            Me.lkOffice.Properties.ShowLines = False
            Me.lkOffice.Properties.SortColumnIndex = 1
            Me.lkOffice.Properties.ValueMember = "Id"
            Me.lkOffice.Size = New System.Drawing.Size(254, 20)
            Me.lkOffice.StyleController = Me.LayoutControl1
            Me.lkOffice.TabIndex = 12
            Me.lkOffice.ToolTip = "In which office is this client serviced?"
            '
            'lkReferredBy
            '
            Me.lkReferredBy.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lkReferredBy.Location = New System.Drawing.Point(163, 85)
            Me.lkReferredBy.Name = "lkReferredBy"
            Me.lkReferredBy.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lkReferredBy.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_key", "ID", 25, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 75, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.lkReferredBy.Properties.DisplayMember = "description"
            Me.lkReferredBy.Properties.NullText = "Please Choose a value from the following list"
            Me.lkReferredBy.Properties.ShowFooter = False
            Me.lkReferredBy.Properties.ShowHeader = False
            Me.lkReferredBy.Properties.SortColumnIndex = 1
            Me.lkReferredBy.Properties.ValueMember = "Id"
            Me.lkReferredBy.Size = New System.Drawing.Size(254, 20)
            Me.lkReferredBy.StyleController = Me.LayoutControl1
            Me.lkReferredBy.TabIndex = 4
            '
            'lkApptType
            '
            Me.lkApptType.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lkApptType.Location = New System.Drawing.Point(163, 157)
            Me.lkApptType.Name = "lkApptType"
            Me.lkApptType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lkApptType.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("appt_type", "ID", 25, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("appt_name", "Description", 75, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.lkApptType.Properties.DisplayMember = "appt_name"
            Me.lkApptType.Properties.NullText = "Please Choose a value from the following list"
            Me.lkApptType.Properties.ShowFooter = False
            Me.lkApptType.Properties.ShowHeader = False
            Me.lkApptType.Properties.SortColumnIndex = 1
            Me.lkApptType.Properties.ValueMember = "Id"
            Me.lkApptType.Size = New System.Drawing.Size(254, 20)
            Me.lkApptType.StyleController = Me.LayoutControl1
            Me.lkApptType.TabIndex = 10
            '
            'lkReferredTo
            '
            Me.lkReferredTo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lkReferredTo.Location = New System.Drawing.Point(163, 133)
            Me.lkReferredTo.Name = "lkReferredTo"
            Me.lkReferredTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lkReferredTo.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_key", "ID", 25, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 75, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.lkReferredTo.Properties.DisplayMember = "description"
            Me.lkReferredTo.Properties.NullText = "Please Choose a value from the following list"
            Me.lkReferredTo.Properties.ShowFooter = False
            Me.lkReferredTo.Properties.ShowHeader = False
            Me.lkReferredTo.Properties.SortColumnIndex = 1
            Me.lkReferredTo.Properties.ValueMember = "Id"
            Me.lkReferredTo.Size = New System.Drawing.Size(254, 20)
            Me.lkReferredTo.StyleController = Me.LayoutControl1
            Me.lkReferredTo.TabIndex = 8
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup1.GroupBordersVisible = False
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem4, Me.LayoutControlItem5, Me.LayoutControlItem6, Me.LayoutControlItem7, Me.LayoutControlItem8, Me.LayoutControlItem9, Me.LayoutControlItem10, Me.LayoutControlItem11, Me.LayoutControlItem16, Me.LayoutControlItem_hud, Me.LayoutControlItem13, Me.LayoutControlItem1, Me.LayoutControlItem12, Me.LayoutControlItem14, Me.LayoutControlItem2, Me.LayoutControlItem3})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(508, 310)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.lkClientStatus
            Me.LayoutControlItem4.CustomizationFormText = "LayoutControlItem4"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 49)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(409, 24)
            Me.LayoutControlItem4.Text = "Appointment &Result"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(148, 13)
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Control = Me.lkReferredBy
            Me.LayoutControlItem5.CustomizationFormText = "LayoutControlItem5"
            Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 73)
            Me.LayoutControlItem5.Name = "LayoutControlItem5"
            Me.LayoutControlItem5.Size = New System.Drawing.Size(409, 24)
            Me.LayoutControlItem5.Text = "Appointment R&eferral Source"
            Me.LayoutControlItem5.TextSize = New System.Drawing.Size(148, 13)
            '
            'LayoutControlItem6
            '
            Me.LayoutControlItem6.Control = Me.TextEdit_partner_code
            Me.LayoutControlItem6.CustomizationFormText = "LayoutControlItem6"
            Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 97)
            Me.LayoutControlItem6.Name = "LayoutControlItem6"
            Me.LayoutControlItem6.Size = New System.Drawing.Size(409, 24)
            Me.LayoutControlItem6.Text = "Partner Code #"
            Me.LayoutControlItem6.TextSize = New System.Drawing.Size(148, 13)
            '
            'LayoutControlItem7
            '
            Me.LayoutControlItem7.Control = Me.lkReferredTo
            Me.LayoutControlItem7.CustomizationFormText = "LayoutControlItem7"
            Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 121)
            Me.LayoutControlItem7.Name = "LayoutControlItem7"
            Me.LayoutControlItem7.Size = New System.Drawing.Size(409, 24)
            Me.LayoutControlItem7.Text = "Where did you &send the client?"
            Me.LayoutControlItem7.TextSize = New System.Drawing.Size(148, 13)
            '
            'LayoutControlItem8
            '
            Me.LayoutControlItem8.Control = Me.lkApptType
            Me.LayoutControlItem8.CustomizationFormText = "LayoutControlItem8"
            Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 145)
            Me.LayoutControlItem8.Name = "LayoutControlItem8"
            Me.LayoutControlItem8.Size = New System.Drawing.Size(409, 24)
            Me.LayoutControlItem8.Text = "Appointment &Type"
            Me.LayoutControlItem8.TextSize = New System.Drawing.Size(148, 13)
            '
            'LayoutControlItem9
            '
            Me.LayoutControlItem9.Control = Me.lkOffice
            Me.LayoutControlItem9.CustomizationFormText = "LayoutControlItem9"
            Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 169)
            Me.LayoutControlItem9.Name = "LayoutControlItem9"
            Me.LayoutControlItem9.Size = New System.Drawing.Size(409, 24)
            Me.LayoutControlItem9.Text = "&Office Location"
            Me.LayoutControlItem9.TextSize = New System.Drawing.Size(148, 13)
            '
            'LayoutControlItem10
            '
            Me.LayoutControlItem10.Control = Me.lkCounselor
            Me.LayoutControlItem10.CustomizationFormText = "LayoutControlItem10"
            Me.LayoutControlItem10.Location = New System.Drawing.Point(0, 193)
            Me.LayoutControlItem10.Name = "LayoutControlItem10"
            Me.LayoutControlItem10.Size = New System.Drawing.Size(409, 24)
            Me.LayoutControlItem10.Text = "C&ounselor"
            Me.LayoutControlItem10.TextSize = New System.Drawing.Size(148, 13)
            '
            'LayoutControlItem11
            '
            Me.LayoutControlItem11.Control = Me.lkBankruptcyClass
            Me.LayoutControlItem11.CustomizationFormText = "LayoutControlItem11"
            Me.LayoutControlItem11.Location = New System.Drawing.Point(0, 217)
            Me.LayoutControlItem11.Name = "LayoutControlItem11"
            Me.LayoutControlItem11.Size = New System.Drawing.Size(409, 24)
            Me.LayoutControlItem11.Text = "Initial &BK Intent"
            Me.LayoutControlItem11.TextSize = New System.Drawing.Size(148, 13)
            '
            'LayoutControlItem16
            '
            Me.LayoutControlItem16.Control = Me.LabelControl1
            Me.LayoutControlItem16.CustomizationFormText = "LayoutControlItem16"
            Me.LayoutControlItem16.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem16.Name = "LayoutControlItem16"
            Me.LayoutControlItem16.Size = New System.Drawing.Size(488, 49)
            Me.LayoutControlItem16.Text = "LayoutControlItem16"
            Me.LayoutControlItem16.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem16.TextToControlDistance = 0
            Me.LayoutControlItem16.TextVisible = False
            '
            'LayoutControlItem_hud
            '
            Me.LayoutControlItem_hud.Control = Me.LabelControl_hud_note
            Me.LayoutControlItem_hud.CustomizationFormText = "LayoutControlItem15"
            Me.LayoutControlItem_hud.Location = New System.Drawing.Point(177, 265)
            Me.LayoutControlItem_hud.Name = "LayoutControlItem_hud"
            Me.LayoutControlItem_hud.Size = New System.Drawing.Size(311, 25)
            Me.LayoutControlItem_hud.Text = "LayoutControlItem_hud"
            Me.LayoutControlItem_hud.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem_hud.TextToControlDistance = 0
            Me.LayoutControlItem_hud.TextVisible = False
            '
            'LayoutControlItem13
            '
            Me.LayoutControlItem13.Control = Me.chkCredit
            Me.LayoutControlItem13.CustomizationFormText = "LayoutControlItem13"
            Me.LayoutControlItem13.Location = New System.Drawing.Point(0, 265)
            Me.LayoutControlItem13.Name = "LayoutControlItem13"
            Me.LayoutControlItem13.Size = New System.Drawing.Size(177, 25)
            Me.LayoutControlItem13.Text = "LayoutControlItem13"
            Me.LayoutControlItem13.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem13.TextToControlDistance = 0
            Me.LayoutControlItem13.TextVisible = False
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.SimpleButton_Cancel
            Me.LayoutControlItem1.CustomizationFormText = "LayoutControlItem1"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(409, 103)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(79, 138)
            Me.LayoutControlItem1.Text = "LayoutControlItem1"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem1.TextToControlDistance = 0
            Me.LayoutControlItem1.TextVisible = False
            '
            'LayoutControlItem12
            '
            Me.LayoutControlItem12.Control = Me.spnDuration
            Me.LayoutControlItem12.CustomizationFormText = "LayoutControlItem12"
            Me.LayoutControlItem12.Location = New System.Drawing.Point(0, 241)
            Me.LayoutControlItem12.Name = "LayoutControlItem12"
            Me.LayoutControlItem12.Size = New System.Drawing.Size(244, 24)
            Me.LayoutControlItem12.Text = "Appointment &Duration"
            Me.LayoutControlItem12.TextSize = New System.Drawing.Size(148, 13)
            '
            'LayoutControlItem14
            '
            Me.LayoutControlItem14.Control = Me.LabelControl_duration_label
            Me.LayoutControlItem14.CustomizationFormText = "LayoutControlItem14"
            Me.LayoutControlItem14.Location = New System.Drawing.Point(244, 241)
            Me.LayoutControlItem14.Name = "LayoutControlItem14"
            Me.LayoutControlItem14.Size = New System.Drawing.Size(244, 24)
            Me.LayoutControlItem14.Text = "LayoutControlItem14"
            Me.LayoutControlItem14.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem14.TextToControlDistance = 0
            Me.LayoutControlItem14.TextVisible = False
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.SimpleButton_OK
            Me.LayoutControlItem2.CustomizationFormText = "LayoutControlItem2"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(409, 76)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(79, 27)
            Me.LayoutControlItem2.Text = "LayoutControlItem2"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem2.TextToControlDistance = 0
            Me.LayoutControlItem2.TextVisible = False
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.SimpleButton_HUD
            Me.LayoutControlItem3.CustomizationFormText = "LayoutControlItem3"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(409, 49)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(79, 27)
            Me.LayoutControlItem3.Text = "LayoutControlItem3"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem3.TextToControlDistance = 0
            Me.LayoutControlItem3.TextVisible = False
            '
            'Appointments_Control_Result
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.LayoutControl1)
            Me.Margin = New System.Windows.Forms.Padding(0)
            Me.Name = "Appointments_Control_Result"
            Me.Size = New System.Drawing.Size(508, 310)
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.TextEdit_partner_code.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.chkCredit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.spnDuration.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lkBankruptcyClass.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lkCounselor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lkClientStatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lkOffice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lkReferredBy.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lkApptType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lkReferredTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_hud, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Protected Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Protected Friend WithEvents SimpleButton_HUD As DevExpress.XtraEditors.SimpleButton
        Protected Friend WithEvents SimpleButton_OK As DevExpress.XtraEditors.SimpleButton
        Protected Friend WithEvents SimpleButton_Cancel As DevExpress.XtraEditors.SimpleButton
        Protected Friend WithEvents lkClientStatus As DevExpress.XtraEditors.LookUpEdit
        Protected Friend WithEvents lkReferredBy As DevExpress.XtraEditors.LookUpEdit
        Protected Friend WithEvents lkReferredTo As DevExpress.XtraEditors.LookUpEdit
        Protected Friend WithEvents lkApptType As DevExpress.XtraEditors.LookUpEdit
        Protected Friend WithEvents lkOffice As DevExpress.XtraEditors.LookUpEdit
        Protected Friend WithEvents lkCounselor As DevExpress.XtraEditors.LookUpEdit
        Protected Friend WithEvents lkBankruptcyClass As DevExpress.XtraEditors.LookUpEdit
        Protected Friend WithEvents spnDuration As DevExpress.XtraEditors.SpinEdit
        Protected Friend WithEvents chkCredit As DevExpress.XtraEditors.CheckEdit
        Protected Friend WithEvents LabelControl_hud_note As DevExpress.XtraEditors.LabelControl
        Protected Friend WithEvents LabelControl_duration_label As DevExpress.XtraEditors.LabelControl
        Friend WithEvents TextEdit_partner_code As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem_hud As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem

    End Class
End NameSpace