Namespace forms.Appointments
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Appointments_Control_Book_Counseling
        Inherits DevExpress.XtraEditors.XtraUserControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
            Me.SimpleButton_SelectAll = New DevExpress.XtraEditors.SimpleButton()
            Me.SimpleButton_ClearAll = New DevExpress.XtraEditors.SimpleButton()
            Me.chkDays_Sunday = New DevExpress.XtraEditors.CheckEdit()
            Me.chkDays_Saturday = New DevExpress.XtraEditors.CheckEdit()
            Me.chkDays_Friday = New DevExpress.XtraEditors.CheckEdit()
            Me.chkDays_Thursday = New DevExpress.XtraEditors.CheckEdit()
            Me.chkDays_Wednesday = New DevExpress.XtraEditors.CheckEdit()
            Me.chkDays_Tuesday = New DevExpress.XtraEditors.CheckEdit()
            Me.chkDays_Monday = New DevExpress.XtraEditors.CheckEdit()
            Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
            Me.chkHours_All = New DevExpress.XtraEditors.CheckEdit()
            Me.chkHours_PM = New DevExpress.XtraEditors.CheckEdit()
            Me.chkHours_AM = New DevExpress.XtraEditors.CheckEdit()
            Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
            Me.txtZipcode = New DevExpress.XtraEditors.TextEdit()
            Me.chkOffice_Zipcode = New DevExpress.XtraEditors.CheckEdit()
            Me.chkOffice_All = New DevExpress.XtraEditors.CheckEdit()
            Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
            Me.lkCounselor = New DevExpress.XtraEditors.LookUpEdit()
            Me.GroupControl5 = New DevExpress.XtraEditors.GroupControl()
            Me.lkBankruptcyClass = New DevExpress.XtraEditors.LookUpEdit()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
            Me.chkPriority = New DevExpress.XtraEditors.CheckEdit()
            Me.lkApptType = New DevExpress.XtraEditors.LookUpEdit()
            Me.SimpleButton_Search = New DevExpress.XtraEditors.SimpleButton()
            CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupControl1.SuspendLayout()
            CType(Me.chkDays_Sunday.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.chkDays_Saturday.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.chkDays_Friday.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.chkDays_Thursday.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.chkDays_Wednesday.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.chkDays_Tuesday.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.chkDays_Monday.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupControl2.SuspendLayout()
            CType(Me.chkHours_All.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.chkHours_PM.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.chkHours_AM.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupControl3.SuspendLayout()
            CType(Me.txtZipcode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.chkOffice_Zipcode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.chkOffice_All.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupControl4.SuspendLayout()
            CType(Me.lkCounselor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupControl5.SuspendLayout()
            CType(Me.lkBankruptcyClass.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.chkPriority.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lkApptType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GroupControl1
            '
            Me.GroupControl1.Controls.Add(Me.SimpleButton_SelectAll)
            Me.GroupControl1.Controls.Add(Me.SimpleButton_ClearAll)
            Me.GroupControl1.Controls.Add(Me.chkDays_Sunday)
            Me.GroupControl1.Controls.Add(Me.chkDays_Saturday)
            Me.GroupControl1.Controls.Add(Me.chkDays_Friday)
            Me.GroupControl1.Controls.Add(Me.chkDays_Thursday)
            Me.GroupControl1.Controls.Add(Me.chkDays_Wednesday)
            Me.GroupControl1.Controls.Add(Me.chkDays_Tuesday)
            Me.GroupControl1.Controls.Add(Me.chkDays_Monday)
            Me.GroupControl1.Location = New System.Drawing.Point(4, 4)
            Me.GroupControl1.Name = "GroupControl1"
            Me.GroupControl1.Size = New System.Drawing.Size(240, 202)
            Me.GroupControl1.TabIndex = 0
            Me.GroupControl1.Text = "Restrict Appointments to these days"
            '
            'SimpleButton_SelectAll
            '
            Me.SimpleButton_SelectAll.Location = New System.Drawing.Point(152, 70)
            Me.SimpleButton_SelectAll.Name = "SimpleButton_SelectAll"
            Me.SimpleButton_SelectAll.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_SelectAll.TabIndex = 8
            Me.SimpleButton_SelectAll.Text = "Select All"
            '
            'SimpleButton_ClearAll
            '
            Me.SimpleButton_ClearAll.Location = New System.Drawing.Point(152, 41)
            Me.SimpleButton_ClearAll.Name = "SimpleButton_ClearAll"
            Me.SimpleButton_ClearAll.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_ClearAll.TabIndex = 7
            Me.SimpleButton_ClearAll.Text = "Clear All"
            '
            'chkDays_Sunday
            '
            Me.chkDays_Sunday.Location = New System.Drawing.Point(6, 174)
            Me.chkDays_Sunday.Name = "chkDays_Sunday"
            Me.chkDays_Sunday.Properties.Appearance.ForeColor = System.Drawing.Color.Green
            Me.chkDays_Sunday.Properties.Appearance.Options.UseForeColor = True
            Me.chkDays_Sunday.Properties.Caption = "Sunday"
            Me.chkDays_Sunday.Size = New System.Drawing.Size(75, 20)
            Me.chkDays_Sunday.TabIndex = 6
            '
            'chkDays_Saturday
            '
            Me.chkDays_Saturday.Location = New System.Drawing.Point(6, 149)
            Me.chkDays_Saturday.Name = "chkDays_Saturday"
            Me.chkDays_Saturday.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
            Me.chkDays_Saturday.Properties.Appearance.Options.UseForeColor = True
            Me.chkDays_Saturday.Properties.Caption = "Saturday"
            Me.chkDays_Saturday.Size = New System.Drawing.Size(75, 20)
            Me.chkDays_Saturday.TabIndex = 5
            '
            'chkDays_Friday
            '
            Me.chkDays_Friday.Location = New System.Drawing.Point(6, 124)
            Me.chkDays_Friday.Name = "chkDays_Friday"
            Me.chkDays_Friday.Properties.Caption = "Friday"
            Me.chkDays_Friday.Size = New System.Drawing.Size(75, 20)
            Me.chkDays_Friday.TabIndex = 4
            '
            'chkDays_Thursday
            '
            Me.chkDays_Thursday.Location = New System.Drawing.Point(6, 99)
            Me.chkDays_Thursday.Name = "chkDays_Thursday"
            Me.chkDays_Thursday.Properties.Caption = "Thursday"
            Me.chkDays_Thursday.Size = New System.Drawing.Size(75, 20)
            Me.chkDays_Thursday.TabIndex = 3
            '
            'chkDays_Wednesday
            '
            Me.chkDays_Wednesday.Location = New System.Drawing.Point(6, 74)
            Me.chkDays_Wednesday.Name = "chkDays_Wednesday"
            Me.chkDays_Wednesday.Properties.Caption = "Wednesday"
            Me.chkDays_Wednesday.Size = New System.Drawing.Size(88, 20)
            Me.chkDays_Wednesday.TabIndex = 2
            '
            'chkDays_Tuesday
            '
            Me.chkDays_Tuesday.Location = New System.Drawing.Point(6, 49)
            Me.chkDays_Tuesday.Name = "chkDays_Tuesday"
            Me.chkDays_Tuesday.Properties.Caption = "Tuesday"
            Me.chkDays_Tuesday.Size = New System.Drawing.Size(75, 20)
            Me.chkDays_Tuesday.TabIndex = 1
            '
            'chkDays_Monday
            '
            Me.chkDays_Monday.Location = New System.Drawing.Point(6, 24)
            Me.chkDays_Monday.Name = "chkDays_Monday"
            Me.chkDays_Monday.Properties.Caption = "Monday"
            Me.chkDays_Monday.Size = New System.Drawing.Size(75, 20)
            Me.chkDays_Monday.TabIndex = 0
            '
            'GroupControl2
            '
            Me.GroupControl2.Controls.Add(Me.chkHours_All)
            Me.GroupControl2.Controls.Add(Me.chkHours_PM)
            Me.GroupControl2.Controls.Add(Me.chkHours_AM)
            Me.GroupControl2.Location = New System.Drawing.Point(253, 4)
            Me.GroupControl2.Name = "GroupControl2"
            Me.GroupControl2.Size = New System.Drawing.Size(307, 78)
            Me.GroupControl2.TabIndex = 1
            Me.GroupControl2.Text = "Appointment Hours"
            '
            'chkHours_All
            '
            Me.chkHours_All.EditValue = True
            Me.chkHours_All.Location = New System.Drawing.Point(149, 23)
            Me.chkHours_All.Name = "chkHours_All"
            Me.chkHours_All.Properties.Caption = "No Preference"
            Me.chkHours_All.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
            Me.chkHours_All.Properties.RadioGroupIndex = 1
            Me.chkHours_All.Size = New System.Drawing.Size(104, 21)
            Me.chkHours_All.TabIndex = 2
            '
            'chkHours_PM
            '
            Me.chkHours_PM.Location = New System.Drawing.Point(5, 49)
            Me.chkHours_PM.Name = "chkHours_PM"
            Me.chkHours_PM.Properties.Caption = "P.M. (After Noon)"
            Me.chkHours_PM.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
            Me.chkHours_PM.Properties.RadioGroupIndex = 1
            Me.chkHours_PM.Size = New System.Drawing.Size(126, 21)
            Me.chkHours_PM.TabIndex = 1
            Me.chkHours_PM.TabStop = False
            '
            'chkHours_AM
            '
            Me.chkHours_AM.Location = New System.Drawing.Point(5, 24)
            Me.chkHours_AM.Name = "chkHours_AM"
            Me.chkHours_AM.Properties.Caption = "A.M. (Before Noon)"
            Me.chkHours_AM.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
            Me.chkHours_AM.Properties.RadioGroupIndex = 1
            Me.chkHours_AM.Size = New System.Drawing.Size(126, 21)
            Me.chkHours_AM.TabIndex = 0
            Me.chkHours_AM.TabStop = False
            '
            'GroupControl3
            '
            Me.GroupControl3.Controls.Add(Me.txtZipcode)
            Me.GroupControl3.Controls.Add(Me.chkOffice_Zipcode)
            Me.GroupControl3.Controls.Add(Me.chkOffice_All)
            Me.GroupControl3.Location = New System.Drawing.Point(250, 88)
            Me.GroupControl3.Name = "GroupControl3"
            Me.GroupControl3.Size = New System.Drawing.Size(310, 78)
            Me.GroupControl3.TabIndex = 2
            Me.GroupControl3.Text = "Restrict Office Location to Nearest Office based upon Zipcode"
            '
            'txtZipcode
            '
            Me.txtZipcode.Enabled = False
            Me.txtZipcode.Location = New System.Drawing.Point(154, 48)
            Me.txtZipcode.Name = "txtZipcode"
            Me.txtZipcode.Properties.Mask.BeepOnError = True
            Me.txtZipcode.Properties.Mask.EditMask = "\d\d\d\d\d(-\d\d\d\d)?"
            Me.txtZipcode.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.txtZipcode.Size = New System.Drawing.Size(151, 20)
            Me.txtZipcode.TabIndex = 2
            '
            'chkOffice_Zipcode
            '
            Me.chkOffice_Zipcode.Location = New System.Drawing.Point(7, 49)
            Me.chkOffice_Zipcode.Name = "chkOffice_Zipcode"
            Me.chkOffice_Zipcode.Properties.Caption = "Limit office near zipcode:"
            Me.chkOffice_Zipcode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
            Me.chkOffice_Zipcode.Properties.RadioGroupIndex = 2
            Me.chkOffice_Zipcode.Size = New System.Drawing.Size(141, 21)
            Me.chkOffice_Zipcode.TabIndex = 1
            Me.chkOffice_Zipcode.TabStop = False
            '
            'chkOffice_All
            '
            Me.chkOffice_All.EditValue = True
            Me.chkOffice_All.Location = New System.Drawing.Point(7, 24)
            Me.chkOffice_All.Name = "chkOffice_All"
            Me.chkOffice_All.Properties.Caption = "Use any location without regard to zipcode"
            Me.chkOffice_All.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
            Me.chkOffice_All.Properties.RadioGroupIndex = 2
            Me.chkOffice_All.Size = New System.Drawing.Size(234, 21)
            Me.chkOffice_All.TabIndex = 0
            '
            'GroupControl4
            '
            Me.GroupControl4.Controls.Add(Me.lkCounselor)
            Me.GroupControl4.Location = New System.Drawing.Point(250, 172)
            Me.GroupControl4.Name = "GroupControl4"
            Me.GroupControl4.Size = New System.Drawing.Size(310, 54)
            Me.GroupControl4.TabIndex = 3
            Me.GroupControl4.Text = "Restrict to counselors"
            '
            'lkCounselor
            '
            Me.lkCounselor.Location = New System.Drawing.Point(9, 24)
            Me.lkCounselor.Name = "lkCounselor"
            Me.lkCounselor.Properties.ActionButtonIndex = 1
            Me.lkCounselor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete), New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lkCounselor.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 25, DevExpress.Utils.FormatType.None, "{0:f0}", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 75, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Note", 5, "Note")})
            Me.lkCounselor.Properties.DisplayMember = "Name"
            Me.lkCounselor.Properties.NullText = ""
            Me.lkCounselor.Properties.ShowFooter = False
            Me.lkCounselor.Properties.SortColumnIndex = 2
            Me.lkCounselor.Properties.ValueMember = "Id"
            Me.lkCounselor.Size = New System.Drawing.Size(296, 20)
            Me.lkCounselor.TabIndex = 0
            '
            'GroupControl5
            '
            Me.GroupControl5.Controls.Add(Me.lkBankruptcyClass)
            Me.GroupControl5.Controls.Add(Me.LabelControl1)
            Me.GroupControl5.Controls.Add(Me.chkPriority)
            Me.GroupControl5.Controls.Add(Me.lkApptType)
            Me.GroupControl5.Location = New System.Drawing.Point(4, 212)
            Me.GroupControl5.Name = "GroupControl5"
            Me.GroupControl5.Size = New System.Drawing.Size(240, 70)
            Me.GroupControl5.TabIndex = 4
            Me.GroupControl5.Text = "Appointment Type"
            '
            'lkBankruptcyClass
            '
            Me.lkBankruptcyClass.Location = New System.Drawing.Point(127, 45)
            Me.lkBankruptcyClass.Name = "lkBankruptcyClass"
            Me.lkBankruptcyClass.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lkBankruptcyClass.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 25, DevExpress.Utils.FormatType.None, "{0:f0}", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 75, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.lkBankruptcyClass.Properties.DisplayMember = "description"
            Me.lkBankruptcyClass.Properties.NullText = ""
            Me.lkBankruptcyClass.Properties.ShowFooter = False
            Me.lkBankruptcyClass.Properties.ShowHeader = False
            Me.lkBankruptcyClass.Properties.SortColumnIndex = 1
            Me.lkBankruptcyClass.Properties.ValueMember = "Id"
            Me.lkBankruptcyClass.Size = New System.Drawing.Size(100, 20)
            Me.lkBankruptcyClass.TabIndex = 3
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(76, 49)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(45, 13)
            Me.LabelControl1.TabIndex = 2
            Me.LabelControl1.Text = "BK Intent"
            '
            'chkPriority
            '
            Me.chkPriority.Location = New System.Drawing.Point(8, 46)
            Me.chkPriority.Name = "chkPriority"
            Me.chkPriority.Properties.Caption = "Priority"
            Me.chkPriority.Size = New System.Drawing.Size(62, 20)
            Me.chkPriority.TabIndex = 1
            '
            'lkApptType
            '
            Me.lkApptType.Location = New System.Drawing.Point(8, 22)
            Me.lkApptType.Name = "lkApptType"
            Me.lkApptType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lkApptType.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 25, DevExpress.Utils.FormatType.None, "{0:f0}", False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("appt_name", "Description", 75, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.lkApptType.Properties.DisplayMember = "appt_name"
            Me.lkApptType.Properties.NullText = ""
            Me.lkApptType.Properties.ShowFooter = False
            Me.lkApptType.Properties.ShowHeader = False
            Me.lkApptType.Properties.SortColumnIndex = 1
            Me.lkApptType.Properties.ValueMember = "Id"
            Me.lkApptType.Size = New System.Drawing.Size(219, 20)
            Me.lkApptType.TabIndex = 0
            '
            'SimpleButton_Search
            '
            Me.SimpleButton_Search.Location = New System.Drawing.Point(250, 251)
            Me.SimpleButton_Search.Name = "SimpleButton_Search"
            Me.SimpleButton_Search.Size = New System.Drawing.Size(310, 23)
            Me.SimpleButton_Search.TabIndex = 5
            Me.SimpleButton_Search.Text = "Search For Appointment"
            '
            'Appointments_Control_Book_Counseling
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.SimpleButton_Search)
            Me.Controls.Add(Me.GroupControl5)
            Me.Controls.Add(Me.GroupControl4)
            Me.Controls.Add(Me.GroupControl3)
            Me.Controls.Add(Me.GroupControl2)
            Me.Controls.Add(Me.GroupControl1)
            Me.Name = "Appointments_Control_Book_Counseling"
            Me.Size = New System.Drawing.Size(563, 285)
            CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupControl1.ResumeLayout(False)
            CType(Me.chkDays_Sunday.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.chkDays_Saturday.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.chkDays_Friday.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.chkDays_Thursday.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.chkDays_Wednesday.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.chkDays_Tuesday.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.chkDays_Monday.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupControl2.ResumeLayout(False)
            CType(Me.chkHours_All.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.chkHours_PM.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.chkHours_AM.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupControl3.ResumeLayout(False)
            CType(Me.txtZipcode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.chkOffice_Zipcode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.chkOffice_All.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupControl4.ResumeLayout(False)
            CType(Me.lkCounselor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupControl5.ResumeLayout(False)
            Me.GroupControl5.PerformLayout()
            CType(Me.lkBankruptcyClass.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.chkPriority.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lkApptType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Public WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
        Public WithEvents SimpleButton_SelectAll As DevExpress.XtraEditors.SimpleButton
        Public WithEvents SimpleButton_ClearAll As DevExpress.XtraEditors.SimpleButton
        Public WithEvents chkDays_Sunday As DevExpress.XtraEditors.CheckEdit
        Public WithEvents chkDays_Saturday As DevExpress.XtraEditors.CheckEdit
        Public WithEvents chkDays_Friday As DevExpress.XtraEditors.CheckEdit
        Public WithEvents chkDays_Thursday As DevExpress.XtraEditors.CheckEdit
        Public WithEvents chkDays_Wednesday As DevExpress.XtraEditors.CheckEdit
        Public WithEvents chkDays_Tuesday As DevExpress.XtraEditors.CheckEdit
        Public WithEvents chkDays_Monday As DevExpress.XtraEditors.CheckEdit
        Public WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
        Public WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
        Public WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
        Public WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
        Public WithEvents chkHours_All As DevExpress.XtraEditors.CheckEdit
        Public WithEvents chkHours_PM As DevExpress.XtraEditors.CheckEdit
        Public WithEvents chkHours_AM As DevExpress.XtraEditors.CheckEdit
        Public WithEvents chkOffice_All As DevExpress.XtraEditors.CheckEdit
        Public WithEvents txtZipcode As DevExpress.XtraEditors.TextEdit
        Public WithEvents chkOffice_Zipcode As DevExpress.XtraEditors.CheckEdit
        Public WithEvents SimpleButton_Search As DevExpress.XtraEditors.SimpleButton
        Public WithEvents lkCounselor As DevExpress.XtraEditors.LookUpEdit
        Public WithEvents lkBankruptcyClass As DevExpress.XtraEditors.LookUpEdit
        Public WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Public WithEvents chkPriority As DevExpress.XtraEditors.CheckEdit
        Public WithEvents lkApptType As DevExpress.XtraEditors.LookUpEdit
    End Class
End NameSpace