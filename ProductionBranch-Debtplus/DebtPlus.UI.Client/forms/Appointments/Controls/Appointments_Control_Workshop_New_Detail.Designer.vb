Namespace forms.Appointments
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Appointments_Control_Workshop_New_Detail
        Inherits DevExpress.XtraEditors.XtraUserControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.SimpleButton_Cancel = New DevExpress.XtraEditors.SimpleButton()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.TextEdit_partner_code = New DevExpress.XtraEditors.TextEdit()
            Me.LabelControl_available_seats = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl_booked_seats = New DevExpress.XtraEditors.LabelControl()
            Me.SpinEdit_booked_seats = New DevExpress.XtraEditors.SpinEdit()
            Me.CalcEdit_HousingFeeAmount = New DevExpress.XtraEditors.CalcEdit()
            Me.lkReferredBy = New DevExpress.XtraEditors.LookUpEdit()
            Me.SimpleButton_OK = New DevExpress.XtraEditors.SimpleButton()
            Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem4 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem5 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.TextEdit_partner_code.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.SpinEdit_booked_seats.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_HousingFeeAmount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lkReferredBy.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'SimpleButton_Cancel
            '
            Me.SimpleButton_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_Cancel.Location = New System.Drawing.Point(210, 223)
            Me.SimpleButton_Cancel.MaximumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.MinimumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.Name = "SimpleButton_Cancel"
            Me.SimpleButton_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.StyleController = Me.LayoutControl1
            Me.SimpleButton_Cancel.TabIndex = 10
            Me.SimpleButton_Cancel.Text = "&Cancel"
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.TextEdit_partner_code)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton_Cancel)
            Me.LayoutControl1.Controls.Add(Me.LabelControl_available_seats)
            Me.LayoutControl1.Controls.Add(Me.LabelControl_booked_seats)
            Me.LayoutControl1.Controls.Add(Me.SpinEdit_booked_seats)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit_HousingFeeAmount)
            Me.LayoutControl1.Controls.Add(Me.lkReferredBy)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton_OK)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.HiddenItems.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem5})
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(536, 100, 250, 350)
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(417, 258)
            Me.LayoutControl1.TabIndex = 11
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'TextEdit_partner_code
            '
            Me.TextEdit_partner_code.Location = New System.Drawing.Point(193, 101)
            Me.TextEdit_partner_code.Name = "TextEdit_partner_code"
            Me.TextEdit_partner_code.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.TextEdit_partner_code.Properties.Mask.BeepOnError = True
            Me.TextEdit_partner_code.Properties.Mask.EditMask = "[A-Z0-9]\d\d*"
            Me.TextEdit_partner_code.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.TextEdit_partner_code.Properties.MaxLength = 50
            Me.TextEdit_partner_code.Size = New System.Drawing.Size(212, 20)
            Me.TextEdit_partner_code.StyleController = Me.LayoutControl1
            Me.TextEdit_partner_code.TabIndex = 11
            '
            'LabelControl_available_seats
            '
            Me.LabelControl_available_seats.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
            Me.LabelControl_available_seats.Location = New System.Drawing.Point(193, 12)
            Me.LabelControl_available_seats.Name = "LabelControl_available_seats"
            Me.LabelControl_available_seats.Size = New System.Drawing.Size(8, 16)
            Me.LabelControl_available_seats.StyleController = Me.LayoutControl1
            Me.LabelControl_available_seats.TabIndex = 1
            Me.LabelControl_available_seats.Text = "0"
            '
            'LabelControl_booked_seats
            '
            Me.LabelControl_booked_seats.Location = New System.Drawing.Point(193, 36)
            Me.LabelControl_booked_seats.Name = "LabelControl_booked_seats"
            Me.LabelControl_booked_seats.Size = New System.Drawing.Size(6, 13)
            Me.LabelControl_booked_seats.StyleController = Me.LayoutControl1
            Me.LabelControl_booked_seats.TabIndex = 3
            Me.LabelControl_booked_seats.Text = "0"
            '
            'SpinEdit_booked_seats
            '
            Me.SpinEdit_booked_seats.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
            Me.SpinEdit_booked_seats.Location = New System.Drawing.Point(193, 53)
            Me.SpinEdit_booked_seats.Name = "SpinEdit_booked_seats"
            Me.SpinEdit_booked_seats.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.SpinEdit_booked_seats.Properties.DisplayFormat.FormatString = "n0"
            Me.SpinEdit_booked_seats.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.SpinEdit_booked_seats.Properties.EditFormat.FormatString = "f0"
            Me.SpinEdit_booked_seats.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.SpinEdit_booked_seats.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.[Default]
            Me.SpinEdit_booked_seats.Properties.IsFloatValue = False
            Me.SpinEdit_booked_seats.Properties.Mask.BeepOnError = True
            Me.SpinEdit_booked_seats.Properties.Mask.EditMask = "f0"
            Me.SpinEdit_booked_seats.Properties.Mask.SaveLiteral = False
            Me.SpinEdit_booked_seats.Size = New System.Drawing.Size(59, 20)
            Me.SpinEdit_booked_seats.StyleController = Me.LayoutControl1
            Me.SpinEdit_booked_seats.TabIndex = 5
            '
            'CalcEdit_HousingFeeAmount
            '
            Me.CalcEdit_HousingFeeAmount.Location = New System.Drawing.Point(194, 101)
            Me.CalcEdit_HousingFeeAmount.Name = "CalcEdit_HousingFeeAmount"
            Me.CalcEdit_HousingFeeAmount.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit_HousingFeeAmount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_HousingFeeAmount.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_HousingFeeAmount.Properties.DisplayFormat.FormatString = "c"
            Me.CalcEdit_HousingFeeAmount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_HousingFeeAmount.Properties.EditFormat.FormatString = "c"
            Me.CalcEdit_HousingFeeAmount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_HousingFeeAmount.Properties.Mask.BeepOnError = True
            Me.CalcEdit_HousingFeeAmount.Properties.Mask.EditMask = "c"
            Me.CalcEdit_HousingFeeAmount.Properties.Precision = 2
            Me.CalcEdit_HousingFeeAmount.Size = New System.Drawing.Size(96, 20)
            Me.CalcEdit_HousingFeeAmount.StyleController = Me.LayoutControl1
            Me.CalcEdit_HousingFeeAmount.TabIndex = 1
            Me.CalcEdit_HousingFeeAmount.ToolTip = "If you charged the client a fee for this workshop, how much did you charge?"
            '
            'lkReferredBy
            '
            Me.lkReferredBy.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lkReferredBy.Location = New System.Drawing.Point(193, 77)
            Me.lkReferredBy.Name = "lkReferredBy"
            Me.lkReferredBy.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lkReferredBy.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.lkReferredBy.Properties.DisplayMember = "description"
            Me.lkReferredBy.Properties.NullText = ""
            Me.lkReferredBy.Properties.ShowFooter = False
            Me.lkReferredBy.Properties.ShowHeader = False
            Me.lkReferredBy.Properties.SortColumnIndex = 1
            Me.lkReferredBy.Properties.ValueMember = "Id"
            Me.lkReferredBy.Size = New System.Drawing.Size(212, 20)
            Me.lkReferredBy.StyleController = Me.LayoutControl1
            Me.lkReferredBy.TabIndex = 7
            '
            'SimpleButton_OK
            '
            Me.SimpleButton_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_OK.Enabled = False
            Me.SimpleButton_OK.Location = New System.Drawing.Point(131, 223)
            Me.SimpleButton_OK.MaximumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.MinimumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.Name = "SimpleButton_OK"
            Me.SimpleButton_OK.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.StyleController = Me.LayoutControl1
            Me.SimpleButton_OK.TabIndex = 9
            Me.SimpleButton_OK.Text = "&OK"
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Control = Me.CalcEdit_HousingFeeAmount
            Me.LayoutControlItem5.CustomizationFormText = "Housing Workshop Fee Charged"
            Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 89)
            Me.LayoutControlItem5.Name = "LayoutControlItem5"
            Me.LayoutControlItem5.Size = New System.Drawing.Size(282, 24)
            Me.LayoutControlItem5.Text = "Housing Workshop Fee Charged:"
            Me.LayoutControlItem5.TextSize = New System.Drawing.Size(178, 13)
            Me.LayoutControlItem5.TextToControlDistance = 5
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup1.GroupBordersVisible = False
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem4, Me.LayoutControlItem7, Me.LayoutControlItem8, Me.EmptySpaceItem1, Me.EmptySpaceItem2, Me.EmptySpaceItem3, Me.LayoutControlItem3, Me.EmptySpaceItem4, Me.EmptySpaceItem5, Me.LayoutControlItem6})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(417, 258)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LayoutControlItem1.AppearanceItemCaption.Options.UseFont = True
            Me.LayoutControlItem1.Control = Me.LabelControl_available_seats
            Me.LayoutControlItem1.CustomizationFormText = "Total Available Seats"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Padding = New DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 6)
            Me.LayoutControlItem1.Size = New System.Drawing.Size(397, 24)
            Me.LayoutControlItem1.Text = "Total Available Seats:"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(178, 13)
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.LabelControl_booked_seats
            Me.LayoutControlItem2.CustomizationFormText = "Previous Count booked by this client"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(397, 17)
            Me.LayoutControlItem2.Text = "Previous Count booked by this client:"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(178, 13)
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.lkReferredBy
            Me.LayoutControlItem4.CustomizationFormText = "Referral Source"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 65)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(397, 24)
            Me.LayoutControlItem4.Text = "Referral Source:"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(178, 13)
            '
            'LayoutControlItem7
            '
            Me.LayoutControlItem7.Control = Me.SimpleButton_OK
            Me.LayoutControlItem7.CustomizationFormText = "LayoutControlItem7"
            Me.LayoutControlItem7.Location = New System.Drawing.Point(119, 211)
            Me.LayoutControlItem7.Name = "LayoutControlItem7"
            Me.LayoutControlItem7.Size = New System.Drawing.Size(79, 27)
            Me.LayoutControlItem7.Text = "LayoutControlItem7"
            Me.LayoutControlItem7.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem7.TextToControlDistance = 0
            Me.LayoutControlItem7.TextVisible = False
            '
            'LayoutControlItem8
            '
            Me.LayoutControlItem8.Control = Me.SimpleButton_Cancel
            Me.LayoutControlItem8.CustomizationFormText = "LayoutControlItem8"
            Me.LayoutControlItem8.Location = New System.Drawing.Point(198, 211)
            Me.LayoutControlItem8.Name = "LayoutControlItem8"
            Me.LayoutControlItem8.Size = New System.Drawing.Size(79, 27)
            Me.LayoutControlItem8.Text = "LayoutControlItem8"
            Me.LayoutControlItem8.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem8.TextToControlDistance = 0
            Me.LayoutControlItem8.TextVisible = False
            '
            'EmptySpaceItem1
            '
            Me.EmptySpaceItem1.AllowHotTrack = False
            Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 132)
            Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Size = New System.Drawing.Size(397, 79)
            Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
            Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem2
            '
            Me.EmptySpaceItem2.AllowHotTrack = False
            Me.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Location = New System.Drawing.Point(277, 211)
            Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Size = New System.Drawing.Size(120, 27)
            Me.EmptySpaceItem2.Text = "EmptySpaceItem2"
            Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem3
            '
            Me.EmptySpaceItem3.AllowHotTrack = False
            Me.EmptySpaceItem3.CustomizationFormText = "EmptySpaceItem3"
            Me.EmptySpaceItem3.Location = New System.Drawing.Point(0, 211)
            Me.EmptySpaceItem3.Name = "EmptySpaceItem3"
            Me.EmptySpaceItem3.Size = New System.Drawing.Size(119, 27)
            Me.EmptySpaceItem3.Text = "EmptySpaceItem3"
            Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.SpinEdit_booked_seats
            Me.LayoutControlItem3.CustomizationFormText = "Number of people attending"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 41)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(244, 24)
            Me.LayoutControlItem3.Text = "Number of people attending:"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(178, 13)
            '
            'EmptySpaceItem4
            '
            Me.EmptySpaceItem4.AllowHotTrack = False
            Me.EmptySpaceItem4.CustomizationFormText = "EmptySpaceItem4"
            Me.EmptySpaceItem4.Location = New System.Drawing.Point(244, 41)
            Me.EmptySpaceItem4.Name = "EmptySpaceItem4"
            Me.EmptySpaceItem4.Size = New System.Drawing.Size(153, 24)
            Me.EmptySpaceItem4.Text = "EmptySpaceItem4"
            Me.EmptySpaceItem4.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem5
            '
            Me.EmptySpaceItem5.AllowHotTrack = False
            Me.EmptySpaceItem5.CustomizationFormText = "EmptySpaceItem5"
            Me.EmptySpaceItem5.Location = New System.Drawing.Point(0, 113)
            Me.EmptySpaceItem5.Name = "EmptySpaceItem5"
            Me.EmptySpaceItem5.Size = New System.Drawing.Size(397, 19)
            Me.EmptySpaceItem5.Text = "EmptySpaceItem5"
            Me.EmptySpaceItem5.TextSize = New System.Drawing.Size(0, 0)
            '
            'LayoutControlItem6
            '
            Me.LayoutControlItem6.Control = Me.TextEdit_partner_code
            Me.LayoutControlItem6.CustomizationFormText = "Partner Code #"
            Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 89)
            Me.LayoutControlItem6.Name = "LayoutControlItem6"
            Me.LayoutControlItem6.Size = New System.Drawing.Size(397, 24)
            Me.LayoutControlItem6.Text = "Partner Code #:"
            Me.LayoutControlItem6.TextSize = New System.Drawing.Size(178, 13)
            '
            'Appointments_Control_Workshop_New_Detail
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.LayoutControl1)
            Me.Name = "Appointments_Control_Workshop_New_Detail"
            Me.Size = New System.Drawing.Size(417, 258)
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.TextEdit_partner_code.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.SpinEdit_booked_seats.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_HousingFeeAmount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lkReferredBy.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

        Protected Friend WithEvents SimpleButton_Cancel As DevExpress.XtraEditors.SimpleButton
        Protected Friend WithEvents SimpleButton_OK As DevExpress.XtraEditors.SimpleButton
        Protected Friend WithEvents lkReferredBy As DevExpress.XtraEditors.LookUpEdit
        Protected Friend WithEvents SpinEdit_booked_seats As DevExpress.XtraEditors.SpinEdit
        Protected Friend WithEvents LabelControl_booked_seats As DevExpress.XtraEditors.LabelControl
        Protected Friend WithEvents LabelControl_available_seats As DevExpress.XtraEditors.LabelControl
        Protected Friend WithEvents CalcEdit_HousingFeeAmount As DevExpress.XtraEditors.CalcEdit
        Protected Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Protected Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Protected Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
        Protected Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
        Protected Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
        Protected Friend WithEvents EmptySpaceItem3 As DevExpress.XtraLayout.EmptySpaceItem
        Protected Friend WithEvents EmptySpaceItem4 As DevExpress.XtraLayout.EmptySpaceItem
        Protected Friend WithEvents EmptySpaceItem5 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents TextEdit_partner_code As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    End Class
End NameSpace