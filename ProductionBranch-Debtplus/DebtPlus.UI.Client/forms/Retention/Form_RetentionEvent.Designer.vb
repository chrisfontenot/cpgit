﻿Namespace forms.Retention
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form_RetentionEvent
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.SimpleButton_ok = New DevExpress.XtraEditors.SimpleButton()
            Me.SimpleButton_Cancel = New DevExpress.XtraEditors.SimpleButton()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
            Me.SpinEdit_priority = New DevExpress.XtraEditors.SpinEdit()
            Me.LookUpEdit_retention_event = New DevExpress.XtraEditors.LookUpEdit()
            Me.CalcEdit_amount = New DevExpress.XtraEditors.CalcEdit()
            Me.MemoEdit_note = New DevExpress.XtraEditors.MemoEdit()
            Me.GroupControl_expires = New DevExpress.XtraEditors.GroupControl()
            Me.DateEdit_expires_date = New DevExpress.XtraEditors.DateEdit()
            Me.CheckEdit_expires_3 = New DevExpress.XtraEditors.CheckEdit()
            Me.CheckEdit_expires_2 = New DevExpress.XtraEditors.CheckEdit()
            Me.CheckEdit_expires_1 = New DevExpress.XtraEditors.CheckEdit()
            Me.RetentionActionsControl1 = New RetentionActionsControl()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.SpinEdit_priority.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_retention_event.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_amount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.MemoEdit_note.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GroupControl_expires, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupControl_expires.SuspendLayout()
            CType(Me.DateEdit_expires_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit_expires_date.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_expires_3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_expires_2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_expires_1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RetentionActionsControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'SimpleButton_ok
            '
            Me.SimpleButton_ok.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton_ok.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.SimpleButton_ok.Location = New System.Drawing.Point(341, 12)
            Me.SimpleButton_ok.Name = "SimpleButton_ok"
            Me.SimpleButton_ok.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_ok.TabIndex = 10
            Me.SimpleButton_ok.Text = "&OK"
            '
            'SimpleButton_Cancel
            '
            Me.SimpleButton_Cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SimpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.SimpleButton_Cancel.Location = New System.Drawing.Point(341, 54)
            Me.SimpleButton_Cancel.Name = "SimpleButton_Cancel"
            Me.SimpleButton_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.TabIndex = 11
            Me.SimpleButton_Cancel.Text = "&Cancel"
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(12, 12)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(28, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "&Event"
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(12, 38)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(37, 13)
            Me.LabelControl2.TabIndex = 2
            Me.LabelControl2.Text = "&Amount"
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(12, 64)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(23, 13)
            Me.LabelControl3.TabIndex = 6
            Me.LabelControl3.Text = "&Note"
            '
            'LabelControl4
            '
            Me.LabelControl4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl4.Location = New System.Drawing.Point(237, 38)
            Me.LabelControl4.Name = "LabelControl4"
            Me.LabelControl4.Size = New System.Drawing.Size(34, 13)
            Me.LabelControl4.TabIndex = 4
            Me.LabelControl4.Text = "&Priority"
            '
            'SpinEdit_priority
            '
            Me.SpinEdit_priority.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SpinEdit_priority.EditValue = New Decimal(New Int32() {0, 0, 0, 0})
            Me.SpinEdit_priority.Location = New System.Drawing.Point(277, 35)
            Me.SpinEdit_priority.Name = "SpinEdit_priority"
            Me.SpinEdit_priority.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.SpinEdit_priority.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.[Default]
            Me.SpinEdit_priority.Properties.IsFloatValue = False
            Me.SpinEdit_priority.Properties.Mask.BeepOnError = True
            Me.SpinEdit_priority.Properties.Mask.EditMask = "0"
            Me.SpinEdit_priority.Properties.MaxLength = 1
            Me.SpinEdit_priority.Properties.MaxValue = New Decimal(New Int32() {9, 0, 0, 0})
            Me.SpinEdit_priority.Size = New System.Drawing.Size(33, 20)
            Me.SpinEdit_priority.TabIndex = 5
            '
            'LookUpEdit_retention_event
            '
            Me.LookUpEdit_retention_event.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                                          Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LookUpEdit_retention_event.Location = New System.Drawing.Point(62, 9)
            Me.LookUpEdit_retention_event.Name = "LookUpEdit_retention_event"
            Me.LookUpEdit_retention_event.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_retention_event.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_retention_event.Properties.DisplayMember = "description"
            Me.LookUpEdit_retention_event.Properties.NullText = ""
            Me.LookUpEdit_retention_event.Properties.ShowFooter = False
            Me.LookUpEdit_retention_event.Properties.ShowHeader = False
            Me.LookUpEdit_retention_event.Properties.ShowLines = False
            Me.LookUpEdit_retention_event.Properties.SortColumnIndex = 1
            Me.LookUpEdit_retention_event.Properties.ValueMember = "Id"
            Me.LookUpEdit_retention_event.Size = New System.Drawing.Size(248, 20)
            Me.LookUpEdit_retention_event.TabIndex = 1
            '
            'CalcEdit_amount
            '
            Me.CalcEdit_amount.Location = New System.Drawing.Point(62, 35)
            Me.CalcEdit_amount.Name = "CalcEdit_amount"
            Me.CalcEdit_amount.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_amount.Properties.DisplayFormat.FormatString = "{0:c2}"
            Me.CalcEdit_amount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_amount.Properties.EditFormat.FormatString = "{0:f2}"
            Me.CalcEdit_amount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_amount.Properties.Precision = 2
            Me.CalcEdit_amount.Size = New System.Drawing.Size(100, 20)
            Me.CalcEdit_amount.TabIndex = 3
            '
            'MemoEdit_note
            '
            Me.MemoEdit_note.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                             Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.MemoEdit_note.Location = New System.Drawing.Point(62, 61)
            Me.MemoEdit_note.Name = "MemoEdit_note"
            Me.MemoEdit_note.Properties.MaxLength = 256
            Me.MemoEdit_note.Size = New System.Drawing.Size(248, 58)
            Me.MemoEdit_note.TabIndex = 7
            '
            'GroupControl_expires
            '
            Me.GroupControl_expires.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GroupControl_expires.Controls.Add(Me.DateEdit_expires_date)
            Me.GroupControl_expires.Controls.Add(Me.CheckEdit_expires_3)
            Me.GroupControl_expires.Controls.Add(Me.CheckEdit_expires_2)
            Me.GroupControl_expires.Controls.Add(Me.CheckEdit_expires_1)
            Me.GroupControl_expires.Location = New System.Drawing.Point(13, 128)
            Me.GroupControl_expires.Name = "GroupControl_expires"
            Me.GroupControl_expires.Size = New System.Drawing.Size(403, 87)
            Me.GroupControl_expires.TabIndex = 8
            Me.GroupControl_expires.Text = "Expires"
            '
            'DateEdit_expires_date
            '
            Me.DateEdit_expires_date.EditValue = Nothing
            Me.DateEdit_expires_date.Location = New System.Drawing.Point(155, 63)
            Me.DateEdit_expires_date.Name = "DateEdit_expires_date"
            Me.DateEdit_expires_date.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.DateEdit_expires_date.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.DateEdit_expires_date.Size = New System.Drawing.Size(100, 20)
            Me.DateEdit_expires_date.TabIndex = 3
            '
            'CheckEdit_expires_3
            '
            Me.CheckEdit_expires_3.Location = New System.Drawing.Point(6, 63)
            Me.CheckEdit_expires_3.Name = "CheckEdit_expires_3"
            Me.CheckEdit_expires_3.Properties.Caption = "Expires automatically on"
            Me.CheckEdit_expires_3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
            Me.CheckEdit_expires_3.Properties.RadioGroupIndex = 0
            Me.CheckEdit_expires_3.Size = New System.Drawing.Size(143, 19)
            Me.CheckEdit_expires_3.TabIndex = 2
            Me.CheckEdit_expires_3.TabStop = False
            Me.CheckEdit_expires_3.Tag = "3"
            '
            'CheckEdit_expires_2
            '
            Me.CheckEdit_expires_2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                                   Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.CheckEdit_expires_2.Location = New System.Drawing.Point(6, 43)
            Me.CheckEdit_expires_2.Name = "CheckEdit_expires_2"
            Me.CheckEdit_expires_2.Properties.Appearance.Options.UseTextOptions = True
            Me.CheckEdit_expires_2.Properties.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter
            Me.CheckEdit_expires_2.Properties.AppearanceDisabled.Options.UseTextOptions = True
            Me.CheckEdit_expires_2.Properties.AppearanceDisabled.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter
            Me.CheckEdit_expires_2.Properties.AppearanceFocused.Options.UseTextOptions = True
            Me.CheckEdit_expires_2.Properties.AppearanceFocused.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter
            Me.CheckEdit_expires_2.Properties.AppearanceReadOnly.Options.UseTextOptions = True
            Me.CheckEdit_expires_2.Properties.AppearanceReadOnly.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter
            Me.CheckEdit_expires_2.Properties.Caption = "Expires on next posted deposit of at least the dollar amount indicated above"
            Me.CheckEdit_expires_2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
            Me.CheckEdit_expires_2.Properties.RadioGroupIndex = 0
            Me.CheckEdit_expires_2.Size = New System.Drawing.Size(390, 19)
            Me.CheckEdit_expires_2.TabIndex = 1
            Me.CheckEdit_expires_2.TabStop = False
            Me.CheckEdit_expires_2.Tag = "2"
            '
            'CheckEdit_expires_1
            '
            Me.CheckEdit_expires_1.EditValue = True
            Me.CheckEdit_expires_1.Location = New System.Drawing.Point(6, 24)
            Me.CheckEdit_expires_1.Name = "CheckEdit_expires_1"
            Me.CheckEdit_expires_1.Properties.Caption = "Never expires. Must be manually cancelled."
            Me.CheckEdit_expires_1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
            Me.CheckEdit_expires_1.Properties.RadioGroupIndex = 0
            Me.CheckEdit_expires_1.Size = New System.Drawing.Size(231, 19)
            Me.CheckEdit_expires_1.TabIndex = 0
            Me.CheckEdit_expires_1.Tag = "1"
            '
            'RetentionActionsControl1
            '
            Me.RetentionActionsControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                                                         Or System.Windows.Forms.AnchorStyles.Left) _
                                                        Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.RetentionActionsControl1.Location = New System.Drawing.Point(1, 221)
            Me.RetentionActionsControl1.Name = "RetentionActionsControl1"
            Me.RetentionActionsControl1.Size = New System.Drawing.Size(427, 95)
            Me.RetentionActionsControl1.TabIndex = 9
            '
            'Form_RetentionEvent
            '
            Me.AcceptButton = Me.SimpleButton_ok
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.SimpleButton_Cancel
            Me.ClientSize = New System.Drawing.Size(428, 315)
            Me.Controls.Add(Me.GroupControl_expires)
            Me.Controls.Add(Me.MemoEdit_note)
            Me.Controls.Add(Me.CalcEdit_amount)
            Me.Controls.Add(Me.LookUpEdit_retention_event)
            Me.Controls.Add(Me.SpinEdit_priority)
            Me.Controls.Add(Me.LabelControl4)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.RetentionActionsControl1)
            Me.Controls.Add(Me.SimpleButton_Cancel)
            Me.Controls.Add(Me.SimpleButton_ok)
            Me.Name = "Form_RetentionEvent"
            Me.Text = "Client Retention Event"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.SpinEdit_priority.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_retention_event.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_amount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.MemoEdit_note.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GroupControl_expires, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupControl_expires.ResumeLayout(False)
            CType(Me.DateEdit_expires_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit_expires_date.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_expires_3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_expires_2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_expires_1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RetentionActionsControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents SimpleButton_ok As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents RetentionActionsControl1 As RetentionActionsControl
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents SpinEdit_priority As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents LookUpEdit_retention_event As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents CalcEdit_amount As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents MemoEdit_note As DevExpress.XtraEditors.MemoEdit
        Friend WithEvents GroupControl_expires As DevExpress.XtraEditors.GroupControl
        Friend WithEvents DateEdit_expires_date As DevExpress.XtraEditors.DateEdit
        Friend WithEvents CheckEdit_expires_3 As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents CheckEdit_expires_2 As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents CheckEdit_expires_1 As DevExpress.XtraEditors.CheckEdit
    End Class
End Namespace