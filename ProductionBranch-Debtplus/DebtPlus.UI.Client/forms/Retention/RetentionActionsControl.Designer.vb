﻿Imports DebtPlus.UI.Client.controls

Namespace forms.Retention

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class RetentionActionsControl
        Inherits MyGridControlClient

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.GridColumn_client_retention_action = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_client_retention_action.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_date_created = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_counselor = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_counselor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_retention_action = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_retention_action.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.SuspendLayout()
            '
            'GridView1
            '
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_date_created, Me.GridColumn_counselor, Me.GridColumn_client_retention_action, Me.GridColumn_retention_action})
            Me.GridView1.PreviewFieldName = "message"
            Me.GridView1.PreviewIndent = 5
            Me.GridView1.OptionsView.ShowPreviewRowLines = DevExpress.Utils.DefaultBoolean.True
            Me.GridView1.OptionsView.RowAutoHeight = True
            Me.GridView1.OptionsView.AutoCalcPreviewLineCount = True
            Me.GridView1.OptionsView.ShowPreview = True
            Me.GridView1.Appearance.Preview.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.GridView1.SortInfo.Add(New DevExpress.XtraGrid.Columns.GridColumnSortInfo(GridColumn_date_created, DevExpress.Data.ColumnSortOrder.Descending))
            '
            'GridColumn_client_retention_action
            '
            Me.GridColumn_client_retention_action.Caption = "ID"
            Me.GridColumn_client_retention_action.CustomizationCaption = "Record ID"
            Me.GridColumn_client_retention_action.DisplayFormat.FormatString = "{0:f0}"
            Me.GridColumn_client_retention_action.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_client_retention_action.FieldName = "client_retention_action"
            Me.GridColumn_client_retention_action.GroupFormat.FormatString = "{0:f0}"
            Me.GridColumn_client_retention_action.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_client_retention_action.Name = "GridColumn_client_retention_action"
            '
            'GridColumn_date_created
            '
            Me.GridColumn_date_created.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_date_created.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date_created.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_date_created.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_date_created.Caption = "Date"
            Me.GridColumn_date_created.CustomizationCaption = "Action Date"
            Me.GridColumn_date_created.DisplayFormat.FormatString = "d"
            Me.GridColumn_date_created.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_created.FieldName = "date_created"
            Me.GridColumn_date_created.GroupFormat.FormatString = "d"
            Me.GridColumn_date_created.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_created.Name = "GridColumn_date_created"
            Me.GridColumn_date_created.Visible = True
            Me.GridColumn_date_created.VisibleIndex = 0
            Me.GridColumn_date_created.Width = 58
            Me.GridColumn_date_created.SortIndex = 0
            Me.GridColumn_date_created.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value
            Me.GridColumn_date_created.SortOrder = DevExpress.Data.ColumnSortOrder.Descending
            '
            'GridColumn_counselor
            '
            Me.GridColumn_counselor.Caption = "Counselor"
            Me.GridColumn_counselor.CustomizationCaption = "Counselor Name"
            Me.GridColumn_counselor.FieldName = "created_by"
            Me.GridColumn_counselor.Name = "GridColumn_counselor"
            Me.GridColumn_counselor.Visible = True
            Me.GridColumn_counselor.VisibleIndex = 1
            Me.GridColumn_counselor.Width = 106
            '
            'GridColumn_retention_action
            '
            Me.GridColumn_retention_action.Caption = "Description"
            Me.GridColumn_retention_action.CustomizationCaption = "Action ID"
            Me.GridColumn_retention_action.FieldName = "retention_action"
            Me.GridColumn_retention_action.Name = "GridColumn_retention_action"
            Me.GridColumn_retention_action.Visible = True
            Me.GridColumn_retention_action.VisibleIndex = 2
            Me.GridColumn_retention_action.Width = 232
            '
            Me.GridColumn_client_retention_action.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_counselor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_retention_action.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            '
            'RetentionActionsControl
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.GridControl1)
            Me.Name = "RetentionActionsControl"
            Me.Size = New System.Drawing.Size(406, 207)
            Me.ResumeLayout(False)
        End Sub
        Friend WithEvents GridColumn_client_retention_action As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_date_created As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_counselor As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_retention_action As DevExpress.XtraGrid.Columns.GridColumn
    End Class
End Namespace