#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.UI.Client.Service

Namespace forms.Retention

    Friend Class Form_RetentionAction
        Implements IContext

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Private ReadOnly drv As DataRowView

        ''' <summary>
        ''' Initialize the form
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()

            AddHandler LookUpEdit_retention_action.EditValueChanged, AddressOf FieldChanged
            AddHandler LookUpEdit_retention_action.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler CalcEdit_amount.Validating, AddressOf DebtPlus.Data.Validation.NonNegative
            AddHandler CalcEdit_amount.EditValueChanged, AddressOf FieldChanged
            AddHandler MemoEdit1.EditValueChanged, AddressOf FieldChanged
            AddHandler Load, AddressOf Form_RetentionAction_Load
        End Sub

        Public Sub New(ByVal Context As ClientUpdateClass)
            MyClass.New()
            privateContext = Context
        End Sub

        Public Sub New(ByVal Context As ClientUpdateClass, ByVal drv As DataRowView)
            MyClass.New(Context)
            Me.drv = drv
        End Sub

        ''' <summary>
        ''' Pointer to the client update information for the current ClientId
        ''' </summary>
        Private ReadOnly privateContext As ClientUpdateClass

        Public ReadOnly Property Context() As ClientUpdateClass Implements IContext.Context
            Get
                Return privateContext
            End Get
        End Property

        Private Sub Form_RetentionAction_Load(ByVal sender As Object, ByVal e As EventArgs)

            ' These are just for "show" and have no real meaning since they are not written to the database, only read.
            If drv.IsNew Then
                drv("created_by") = "Me"
                drv("date_created") = DateTime.Now
            End If

            ' Bind the form to the items
            With LookUpEdit_retention_action
                .Properties.DataSource = DebtPlus.LINQ.Cache.retention_action.getList()
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "retention_action", False, DataSourceUpdateMode.OnPropertyChanged)
            End With

            With CalcEdit_amount
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "amount", False, DataSourceUpdateMode.OnPropertyChanged)
            End With

            With MemoEdit1
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", drv, "message", False, DataSourceUpdateMode.OnPropertyChanged)
            End With

            ' Enable the OK button. The only required field is the action type.
            SimpleButton_OK.Enabled = Not HasErrors()
        End Sub

        Private Sub FieldChanged(ByVal Sender As Object, ByVal e As EventArgs)
            SimpleButton_OK.Enabled = Not HasErrors()
        End Sub

        Private Function HasErrors() As Boolean
            Return _
                LookUpEdit_retention_action.EditValue Is Nothing OrElse
                LookUpEdit_retention_action.EditValue Is DBNull.Value
        End Function
    End Class
End Namespace
