#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DebtPlus.UI.Client.Service
Imports DevExpress.XtraEditors

Namespace forms.Retention

    Friend Class Form_RetentionEvent
        Implements IContext

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Friend RetentionEventDRV As DataRowView
        Private bc As DebtPlus.LINQ.BusinessContext = Nothing

        ''' <summary>
        ''' Initialize the form
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Load, AddressOf Form_RetentionEvent_Load
        End Sub

        Public Sub New(bc As DebtPlus.LINQ.BusinessContext)
            MyClass.New()
            Me.bc = bc
        End Sub

        Public Sub New(ByVal Context As ClientUpdateClass)
            MyClass.New(Context.ClientDs.bc)
            privateContext = Context
        End Sub

        Public Sub New(ByVal Context As ClientUpdateClass, ByVal RetentionEventDRV As DataRowView)
            MyClass.New(Context)
            Me.RetentionEventDRV = RetentionEventDRV
        End Sub

        ''' <summary>
        ''' Pointer to the client update information for the current ClientId
        ''' </summary>
        Private ReadOnly privateContext As ClientUpdateClass

        Public ReadOnly Property Context() As ClientUpdateClass Implements IContext.Context
            Get
                Return privateContext
            End Get
        End Property

        Public Sub SaveForm(bc As DebtPlus.LINQ.BusinessContext, ByVal RetentionEventDRV As DataRowView)
            RetentionActionsControl1.SaveForm(bc, RetentionEventDRV)
        End Sub

        Private Sub Form_RetentionEvent_Load(ByVal sender As Object, ByVal e As EventArgs)

            With LookUpEdit_retention_event
                .Properties.DataSource = DebtPlus.LINQ.Cache.retention_event.getList()
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", RetentionEventDRV, "retention_event")
                AddHandler .EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            End With

            With CalcEdit_amount
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", RetentionEventDRV, "amount")
            End With

            With SpinEdit_priority
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", RetentionEventDRV, "priority")
            End With

            With MemoEdit_note
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", RetentionEventDRV, "message")
            End With

            With DateEdit_expires_date
                .DataBindings.Clear()
                .DataBindings.Add("EditValue", RetentionEventDRV, "expire_date")
            End With

            ' Enable the proper radio button
            Dim ExpireType As Int32 = DebtPlus.Utils.Nulls.DInt(RetentionEventDRV("expire_type"), 1)

            CheckEdit_expires_1.Checked = False
            CheckEdit_expires_2.Checked = False
            CheckEdit_expires_3.Checked = False

            Select Case ExpireType
                Case 3
                    CheckEdit_expires_3.Checked = True
                    DateEdit_expires_date.Enabled = True
                Case 2
                    CheckEdit_expires_2.Checked = True
                    DateEdit_expires_date.Enabled = False
                Case Else
                    CheckEdit_expires_1.Checked = True
                    DateEdit_expires_date.Enabled = False
            End Select

            ' Monitor the changes in the radio buttons
            AddHandler CheckEdit_expires_1.EditValueChanged, AddressOf CheckEdit_EditValueChanged
            AddHandler CheckEdit_expires_2.EditValueChanged, AddressOf CheckEdit_EditValueChanged
            AddHandler CheckEdit_expires_3.EditValueChanged, AddressOf CheckEdit_EditValueChanged

            ' Add the retention events
            RetentionActionsControl1.ReadForm(bc, RetentionEventDRV)
        End Sub

        Private Sub CheckEdit_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            With CType(sender, CheckEdit)
                If RetentionEventDRV IsNot Nothing AndAlso .Checked Then
                    Dim Tag As Int32 = Convert.ToInt32(.Tag)
                    DateEdit_expires_date.Enabled = (Tag = 3)
                    RetentionEventDRV("expire_type") = Tag
                End If
            End With
        End Sub
    End Class
End Namespace
