#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DevExpress.XtraGrid.Views.Base
Imports DebtPlus.UI.Client.controls
Imports DebtPlus.UI.Client.Service
Imports DevExpress.Utils
Imports DebtPlus.LINQ
Imports System.Linq

Namespace forms.Retention

    Friend Class RetentionControl
        Inherits MyGridControlClient

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Private bc As DebtPlus.LINQ.BusinessContext = Nothing

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Load, AddressOf RetentionControl_Load
            EnableMenus = True
        End Sub

        Friend Shadows Sub ReadForm(bc As BusinessContext, ByVal SelectAll As Boolean)
            Me.bc = bc
            UpdateTable = Context.ClientDs.client_retention_events_table

            Try
                GridView1.BeginUpdate()

                With GridControl1
                    .DataSource = UpdateTable.DefaultView
                    .RefreshDataSource()
                End With

                ' Filter the items in the list
                Dim SelectionString As String = String.Format("[effective_date] IS NULL OR [effective_date]<#{0:d}#",
                                                              DateTime.Now.AddDays(1).Date)
                If Not SelectAll Then
                    SelectionString += " AND ([date_expired] Is Null)"
                End If

                GridView1.ActiveFilterString = SelectionString

                ' Do not show the filter conditions on the form.
                GridView1.OptionsView.ShowFilterPanelMode = ShowFilterPanelMode.Never

            Finally
                GridView1.EndUpdate()
            End Try
        End Sub

        Protected Overrides Sub OnEditItem(ByVal obj As Object)
            Dim drv As DataRowView = TryCast(obj, DataRowView)
            If drv IsNot Nothing Then
                drv.BeginEdit()

                Using frm As New Form_RetentionEvent(Context, drv)
                    If frm.ShowDialog() = DialogResult.OK Then
                        drv.EndEdit()
                        frm.SaveForm(bc, drv)
                    Else
                        drv.CancelEdit()
                    End If
                End Using
            End If
        End Sub

        Protected Overrides Sub OnCreateItem()
            Dim vue As DataView = CType(GridControl1.DataSource, System.Data.DataView)
            Dim drv As DataRowView = vue.AddNew
            With drv
                .Item("amount") = Context.ClientDs.colClientDeposits.Where(Function(s) Not s.one_time).Sum(Function(s) s.deposit_amount)
                .Item("expire_date") = DateTime.Now.Date.AddMonths(3)
                .Item("date_created") = DateTime.Now
                .Item("created_by") = "Me"
                .Item("client") = Context.ClientDs.ClientId
            End With
            OnEditItem(drv)
        End Sub

        Protected Overrides Sub OnDeleteItem(ByVal obj As Object)
            Dim drv As DataRowView = TryCast(obj, DataRowView)
            If drv IsNot Nothing Then
                drv.Delete()
            End If
        End Sub

        Public Overrides Sub SaveForm(bc As DebtPlus.LINQ.BusinessContext)
            Context.ClientDs.UpdateClientRetentionEvents()
            Context.ClientDs.UpdateClientRetentionActions()
        End Sub

        Private Sub RetentionControl_Load(sender As Object, e As EventArgs)

            ' Do this on the load event which occurs after the context is defined.
            With View1_retention_event
                With .DisplayFormat
                    .Format = New RetentionEventNameFormatter()
                    .FormatType = FormatType.Custom
                    .FormatString = String.Empty
                End With

                With .GroupFormat
                    .Format = New RetentionEventNameFormatter()
                    .FormatType = FormatType.Custom
                    .FormatString = String.Empty
                End With
                .AppearanceCell.TextOptions.HAlignment = HorzAlignment.Near
                .AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Near
            End With

            With View2_retention_action
                With .DisplayFormat
                    .Format = New RetentionActionNameFormatter()
                    .FormatType = FormatType.Custom
                End With

                With .GroupFormat
                    .Format = New RetentionActionNameFormatter()
                    .FormatType = FormatType.Custom
                End With
                .AppearanceCell.TextOptions.HAlignment = HorzAlignment.Near
                .AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Near
            End With

            ' Establish the linkage between the event and action tables if needed
            Dim EventsTbl As DataTable = Context.ClientDs.client_retention_events_table
            If EventsTbl IsNot Nothing Then
                Dim ds As DataSet = EventsTbl.DataSet
                Dim ActionsTbl As DataTable = Context.ClientDs.client_retention_actions_table
                If Not ds.Relations.Contains("client_retention_actions") Then
                    Dim rel_events_actions As New DataRelation("client_retention_actions", EventsTbl.Columns("client_retention_event"), ActionsTbl.Columns("client_retention_event"), False)
                    ds.Relations.Add(rel_events_actions)
                End If
            End If
        End Sub
    End Class

    Friend Class RetentionEventNameFormatter
        Implements ICustomFormatter, IFormatProvider

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function Format(format1 As String, arg As Object, formatProvider As IFormatProvider) As String Implements ICustomFormatter.Format
            Dim key As System.Nullable(Of System.Int32) = DebtPlus.Utils.Nulls.v_Int32(arg)
            Return Helpers.getRetentionEventText(key)
        End Function

        Public Function GetFormat(formatType As Type) As Object Implements IFormatProvider.GetFormat
            Return Me
        End Function
    End Class

    Friend Class RetentionActionNameFormatter
        Implements ICustomFormatter, IFormatProvider

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function Format(format1 As String, arg As Object, formatProvider As IFormatProvider) As String Implements ICustomFormatter.Format
            Dim key As System.Nullable(Of System.Int32) = DebtPlus.Utils.Nulls.v_Int32(arg)
            Return Helpers.getRetentionActionText(key)
        End Function

        Public Function GetFormat(formatType As Type) As Object Implements IFormatProvider.GetFormat
            Return Me
        End Function
    End Class
End Namespace