﻿Namespace forms.Retention
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form_RetentionAction
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
            Me.SimpleButton_OK = New DevExpress.XtraEditors.SimpleButton()
            Me.SimpleButton_Cancel = New DevExpress.XtraEditors.SimpleButton()
            Me.CalcEdit_amount = New DevExpress.XtraEditors.CalcEdit()
            Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
            Me.LookUpEdit_retention_action = New DevExpress.XtraEditors.LookUpEdit()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_amount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_retention_action.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(4, 9)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(30, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "&Action"
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(4, 35)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(37, 13)
            Me.LabelControl2.TabIndex = 1
            Me.LabelControl2.Text = "A&mount"
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(4, 62)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(23, 13)
            Me.LabelControl3.TabIndex = 2
            Me.LabelControl3.Text = "&Note"
            '
            'SimpleButton_OK
            '
            Me.SimpleButton_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.SimpleButton_OK.Location = New System.Drawing.Point(68, 110)
            Me.SimpleButton_OK.Name = "SimpleButton_OK"
            Me.SimpleButton_OK.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.TabIndex = 3
            Me.SimpleButton_OK.Text = "&OK"
            '
            'SimpleButton_Cancel
            '
            Me.SimpleButton_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.SimpleButton_Cancel.Location = New System.Drawing.Point(149, 110)
            Me.SimpleButton_Cancel.Name = "SimpleButton_Cancel"
            Me.SimpleButton_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.TabIndex = 4
            Me.SimpleButton_Cancel.Text = "&Cancel"
            '
            'CalcEdit_amount
            '
            Me.CalcEdit_amount.Location = New System.Drawing.Point(77, 32)
            Me.CalcEdit_amount.Name = "CalcEdit_amount"
            Me.CalcEdit_amount.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_amount.Properties.DisplayFormat.FormatString = "{0:c2}"
            Me.CalcEdit_amount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_amount.Properties.Mask.EditMask = "c2"
            Me.CalcEdit_amount.Properties.Precision = 2
            Me.CalcEdit_amount.Size = New System.Drawing.Size(100, 20)
            Me.CalcEdit_amount.TabIndex = 6
            '
            'MemoEdit1
            '
            Me.MemoEdit1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                                          Or System.Windows.Forms.AnchorStyles.Left) _
                                         Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.MemoEdit1.Location = New System.Drawing.Point(77, 59)
            Me.MemoEdit1.Name = "MemoEdit1"
            Me.MemoEdit1.Properties.MaxLength = 256
            Me.MemoEdit1.Size = New System.Drawing.Size(203, 45)
            Me.MemoEdit1.TabIndex = 7
            '
            'LookUpEdit_retention_action
            '
            Me.LookUpEdit_retention_action.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                                           Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LookUpEdit_retention_action.Location = New System.Drawing.Point(77, 6)
            Me.LookUpEdit_retention_action.Name = "LookUpEdit_retention_action"
            Me.LookUpEdit_retention_action.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_retention_action.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_retention_action.Properties.DisplayMember = "description"
            Me.LookUpEdit_retention_action.Properties.NullText = ""
            Me.LookUpEdit_retention_action.Properties.ShowFooter = False
            Me.LookUpEdit_retention_action.Properties.ShowHeader = False
            Me.LookUpEdit_retention_action.Properties.ShowLines = False
            Me.LookUpEdit_retention_action.Properties.SortColumnIndex = 1
            Me.LookUpEdit_retention_action.Properties.ValueMember = "Id"
            Me.LookUpEdit_retention_action.Size = New System.Drawing.Size(203, 20)
            Me.LookUpEdit_retention_action.TabIndex = 8
            '
            'Form_RetentionAction
            '
            Me.AcceptButton = Me.SimpleButton_OK
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.SimpleButton_Cancel
            Me.ClientSize = New System.Drawing.Size(292, 145)
            Me.Controls.Add(Me.LookUpEdit_retention_action)
            Me.Controls.Add(Me.MemoEdit1)
            Me.Controls.Add(Me.CalcEdit_amount)
            Me.Controls.Add(Me.SimpleButton_Cancel)
            Me.Controls.Add(Me.SimpleButton_OK)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LabelControl1)
            Me.Name = "Form_RetentionAction"
            Me.Text = "Retention Action"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_amount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_retention_action.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents SimpleButton_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents CalcEdit_amount As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
        Friend WithEvents LookUpEdit_retention_action As DevExpress.XtraEditors.LookUpEdit
    End Class
End Namespace