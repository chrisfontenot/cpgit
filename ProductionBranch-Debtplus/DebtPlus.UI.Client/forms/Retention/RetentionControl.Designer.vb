Imports DebtPlus.UI.Client.controls

Namespace forms.Retention

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class RetentionControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.GridLevelNode1 = New DevExpress.XtraGrid.GridLevelNode()
            Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
            Me.View2_client_retention_action = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.View2_client_retention_event = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.View2_retention_action = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.View2_message = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.View2_amount = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.View2_date_created = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.View2_created_by = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.View1_client_retention_event = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.View1_client = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.View1_retention_event = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.View1_amount = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.View1_message = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.View1_expire_type = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.View1_expire_date = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.View1_priority = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.View1_effective_date = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.View1_date_expired = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.View1_date_created = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.View1_created_by = New DevExpress.XtraGrid.Columns.GridColumn()
            CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridControl1
            '
            Me.GridControl1.Location = New System.Drawing.Point(13, 13)
            Me.GridControl1.Size = New System.Drawing.Size(400, 200)
            Me.GridControl1.TabIndex = 0
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
            '
            'GridView1
            '
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.View1_client_retention_event, Me.View1_client, Me.View1_retention_event, Me.View1_amount, Me.View1_message, Me.View1_expire_type, Me.View1_expire_date, Me.View1_priority, Me.View1_effective_date, Me.View1_date_expired, Me.View1_date_created, Me.View1_created_by})
            Me.GridView1.OptionsView.ShowPreview = True
            Me.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedRow
            Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.View1_date_created, DevExpress.Data.ColumnSortOrder.Descending)})
            '
            Me.GridView1.PreviewFieldName = "message"
            Me.GridView1.PreviewIndent = 5
            Me.GridView1.OptionsView.ShowPreviewRowLines = DevExpress.Utils.DefaultBoolean.True
            Me.GridView1.OptionsView.RowAutoHeight = True
            Me.GridView1.OptionsView.AutoCalcPreviewLineCount = True
            Me.GridView1.OptionsView.ShowPreview = True
            Me.GridView1.Appearance.Preview.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            '
            'GridView2
            '
            Me.GridView2.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView2.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView2.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView2.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView2.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView2.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView2.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView2.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView2.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView2.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView2.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView2.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView2.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView2.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView2.Appearance.Empty.Options.UseBackColor = True
            Me.GridView2.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView2.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView2.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView2.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView2.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView2.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView2.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView2.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView2.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView2.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView2.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView2.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView2.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView2.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView2.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView2.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView2.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView2.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(133, Byte), Int32), CType(CType(195, Byte), Int32))
            Me.GridView2.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView2.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView2.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView2.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView2.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView2.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Int32), CType(CType(109, Byte), Int32), CType(CType(189, Byte), Int32))
            Me.GridView2.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Int32), CType(CType(139, Byte), Int32), CType(CType(206, Byte), Int32))
            Me.GridView2.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView2.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView2.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView2.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView2.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView2.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView2.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView2.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView2.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView2.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView2.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView2.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView2.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView2.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView2.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView2.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView2.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView2.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView2.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView2.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView2.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView2.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView2.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView2.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView2.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView2.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView2.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Int32), CType(CType(216, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView2.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView2.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView2.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView2.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView2.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView2.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Int32), CType(CType(201, Byte), Int32), CType(CType(254, Byte), Int32))
            Me.GridView2.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView2.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView2.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView2.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView2.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Int32), CType(CType(170, Byte), Int32), CType(CType(225, Byte), Int32))
            Me.GridView2.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView2.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView2.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView2.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView2.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView2.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView2.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView2.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView2.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView2.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView2.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView2.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView2.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView2.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView2.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView2.Appearance.Preview.Options.UseFont = True
            Me.GridView2.Appearance.Preview.Options.UseForeColor = True
            Me.GridView2.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Int32), CType(CType(251, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView2.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView2.Appearance.Row.Options.UseBackColor = True
            Me.GridView2.Appearance.Row.Options.UseForeColor = True
            Me.GridView2.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Int32), CType(CType(246, Byte), Int32), CType(CType(255, Byte), Int32))
            Me.GridView2.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView2.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView2.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Int32), CType(CType(155, Byte), Int32), CType(CType(215, Byte), Int32))
            Me.GridView2.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView2.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView2.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView2.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView2.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView2.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Int32), CType(CType(184, Byte), Int32), CType(CType(251, Byte), Int32))
            Me.GridView2.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
            '
            'GridView2
            '
            Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.View2_client_retention_action, Me.View2_client_retention_event, Me.View2_retention_action, Me.View2_message, Me.View2_amount, Me.View2_date_created, Me.View2_created_by})
            Me.GridView2.GridControl = Me.GridControl1
            Me.GridView2.Name = "GridView2"
            Me.GridView2.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridView2.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridView2.OptionsBehavior.AllowIncrementalSearch = True
            Me.GridView2.OptionsBehavior.AutoPopulateColumns = False
            Me.GridView2.OptionsBehavior.Editable = False
            Me.GridView2.OptionsView.ShowGroupPanel = False
            Me.GridView2.OptionsView.ShowIndicator = False
            Me.GridView2.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.View2_date_created, DevExpress.Data.ColumnSortOrder.Descending)})
            Me.GridView2.ViewCaption = "Action List"
            '
            'View2_client_retention_action
            '
            Me.View2_client_retention_action.Caption = "Row ID"
            Me.View2_client_retention_action.CustomizationCaption = "Primary key to the table"
            Me.View2_client_retention_action.FieldName = "client_retention_action"
            Me.View2_client_retention_action.Name = "View2_client_retention_action"
            Me.View2_client_retention_action.OptionsColumn.AllowEdit = False
            Me.View2_client_retention_action.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'View2_client_retention_event
            '
            Me.View2_client_retention_event.Caption = "Event ID"
            Me.View2_client_retention_event.CustomizationCaption = "Pointer to the event"
            Me.View2_client_retention_event.FieldName = "client_retention_event"
            Me.View2_client_retention_event.Name = "View2_client_retention_event"
            Me.View2_client_retention_event.OptionsColumn.AllowEdit = False
            Me.View2_client_retention_event.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'View2_retention_action
            '
            Me.View2_retention_action.Caption = "Action"
            Me.View2_retention_action.CustomizationCaption = "Action ID"
            Me.View2_retention_action.FieldName = "retention_action"
            Me.View2_retention_action.Name = "View2_retention_action"
            Me.View2_retention_action.OptionsColumn.AllowEdit = False
            Me.View2_retention_action.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.View2_retention_action.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText
            Me.View2_retention_action.Visible = True
            Me.View2_retention_action.VisibleIndex = 0
            '
            'View2_amount
            '
            Me.View2_amount.Caption = "Amount"
            Me.View2_amount.CustomizationCaption = "associated amount"
            Me.View2_amount.DisplayFormat.FormatString = "$0.00"
            Me.View2_amount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.View2_amount.FieldName = "amount"
            Me.View2_amount.GroupFormat.FormatString = "$0.00"
            Me.View2_amount.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.View2_amount.Name = "View2_amount"
            Me.View2_amount.OptionsColumn.AllowEdit = False
            Me.View2_amount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.View2_amount.Visible = True
            Me.View2_amount.VisibleIndex = 1
            Me.View2_amount.Width = 78
            '
            'View2_message
            '
            Me.View2_message.Caption = "Message"
            Me.View2_message.CustomizationCaption = "associated message"
            Me.View2_message.FieldName = "message"
            Me.View2_message.Name = "View2_message"
            Me.View2_message.OptionsColumn.AllowEdit = False
            Me.View2_message.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.View2_message.Visible = True
            Me.View2_message.VisibleIndex = 2
            '
            'View2_date_created
            '
            Me.View2_date_created.Caption = "Date"
            Me.View2_date_created.CustomizationCaption = "Date when created"
            Me.View2_date_created.FieldName = "date_created"
            Me.View2_date_created.Name = "View2_date_created"
            Me.View2_date_created.OptionsColumn.AllowEdit = False
            Me.View2_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.View2_date_created.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value
            Me.View2_date_created.Visible = True
            Me.View2_date_created.VisibleIndex = 3
            '
            'View2_created_by
            '
            Me.View2_created_by.Caption = "Created By"
            Me.View2_created_by.CustomizationCaption = "person who created the action"
            Me.View2_created_by.FieldName = "created_by"
            Me.View2_created_by.Name = "View2_created_by"
            Me.View2_created_by.OptionsColumn.AllowEdit = False
            Me.View2_created_by.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.View2_created_by.Visible = True
            Me.View2_created_by.VisibleIndex = 4
            '
            'GridControl1
            '
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
            Me.GridLevelNode1.LevelTemplate = Me.GridView2
            Me.GridLevelNode1.RelationName = "client_retention_actions"
            Me.GridControl1.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode1})
            '
            'View1_client_retention_event
            '
            Me.View1_client_retention_event.Caption = "ID"
            Me.View1_client_retention_event.CustomizationCaption = "Row ID"
            Me.View1_client_retention_event.FieldName = "client_retention_event"
            Me.View1_client_retention_event.Name = "View1_client_retention_event"
            Me.View1_client_retention_event.OptionsColumn.AllowEdit = False
            Me.View1_client_retention_event.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.View1_retention_event.Visible = True
            Me.View1_retention_event.VisibleIndex = 0
            '
            'View1_client
            '
            Me.View1_client.CustomizationCaption = "Client ID"
            Me.View1_client.Caption = "Client"
            Me.View1_client.FieldName = "client"
            Me.View1_client.Name = "View1_client"
            Me.View1_client.OptionsColumn.AllowEdit = False
            Me.View1_client.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'View1_retention_event
            '
            Me.View1_retention_event.CustomizationCaption = "Retention Event"
            Me.View1_retention_event.Caption = "Event Type"
            Me.View1_retention_event.FieldName = "retention_event"
            Me.View1_retention_event.Name = "View1_retention_event"
            Me.View1_retention_event.OptionsColumn.AllowEdit = False
            Me.View1_retention_event.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.View1_retention_event.Visible = True
            Me.View1_retention_event.VisibleIndex = 1
            Me.View1_retention_event.Width = 194
            '
            'View1_amount
            '
            Me.View1_amount.CustomizationCaption = "Dollar amount associated with event"
            Me.View1_amount.Caption = "Amount"
            Me.View1_amount.FieldName = "amount"
            Me.View1_amount.Name = "View1_amount"
            Me.View1_amount.OptionsColumn.AllowEdit = False
            Me.View1_amount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.View1_amount.Visible = False
            Me.View1_amount.Width = 78
            Me.View1_amount.DisplayFormat.FormatString = "$0.00"
            Me.View1_amount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.View1_amount.GroupFormat.FormatString = "$0.00"
            Me.View1_amount.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            '
            'View1_message
            '
            Me.View1_message.CustomizationCaption = "Associated message text"
            Me.View1_message.Caption = "Message"
            Me.View1_message.FieldName = "message"
            Me.View1_message.Name = "View1_message"
            Me.View1_message.OptionsColumn.AllowEdit = False
            Me.View1_message.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.View1_message.Width = 125
            '
            'View1_expire_type
            '
            Me.View1_expire_type.CustomizationCaption = "Expiration type"
            Me.View1_expire_type.Caption = "Expire Type"
            Me.View1_expire_type.FieldName = "expire_type"
            Me.View1_expire_type.Name = "View1_expire_type"
            Me.View1_expire_type.OptionsColumn.AllowEdit = False
            Me.View1_expire_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'View1_expire_date
            '
            Me.View1_expire_date.CustomizationCaption = "Date when event will expire"
            Me.View1_expire_date.Caption = "Expire Date"
            Me.View1_expire_date.FieldName = "expire_date"
            Me.View1_expire_date.Name = "View1_expire_date"
            Me.View1_expire_date.OptionsColumn.AllowEdit = False
            Me.View1_expire_date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.View1_expire_date.DisplayFormat.FormatString = "d"
            Me.View1_expire_date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.View1_expire_date.GroupFormat.FormatString = "d"
            Me.View1_expire_date.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            '
            'View1_priority
            '
            Me.View1_priority.CustomizationCaption = "Associated Priority"
            Me.View1_priority.Caption = "Priority"
            Me.View1_priority.FieldName = "priority"
            Me.View1_priority.Name = "View1_priority"
            Me.View1_priority.OptionsColumn.AllowEdit = False
            Me.View1_priority.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'View1_effective_date
            '
            Me.View1_effective_date.CustomizationCaption = "Date for showing the item"
            Me.View1_effective_date.Caption = "Effective"
            Me.View1_effective_date.FieldName = "effective_date"
            Me.View1_effective_date.Name = "View1_effective_date"
            Me.View1_effective_date.OptionsColumn.AllowEdit = False
            Me.View1_effective_date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.View1_effective_date.DisplayFormat.FormatString = "d"
            Me.View1_effective_date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.View1_effective_date.GroupFormat.FormatString = "d"
            Me.View1_effective_date.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            '
            'View1_date_expired
            '
            Me.View1_date_expired.CustomizationCaption = "Date when item was deleted"
            Me.View1_date_expired.Caption = "Expired"
            Me.View1_date_expired.FieldName = "date_expired"
            Me.View1_date_expired.Name = "View1_date_expired"
            Me.View1_date_expired.OptionsColumn.AllowEdit = False
            Me.View1_date_expired.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.View1_date_expired.DisplayFormat.FormatString = "d"
            Me.View1_date_expired.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.View1_date_expired.GroupFormat.FormatString = "d"
            Me.View1_date_expired.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            '
            'View1_date_created
            '
            Me.View1_date_created.CustomizationCaption = "Date when item was created"
            Me.View1_date_created.Caption = "Created"
            Me.View1_date_created.FieldName = "date_created"
            Me.View1_date_created.Name = "View1_date_created"
            Me.View1_date_created.OptionsColumn.AllowEdit = False
            Me.View1_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.View1_date_created.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value
            Me.View1_date_created.Visible = True
            Me.View1_date_created.VisibleIndex = 2
            Me.View1_date_created.Width = 99
            Me.View1_date_created.DisplayFormat.FormatString = "d"
            Me.View1_date_created.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.View1_date_created.GroupFormat.FormatString = "d"
            Me.View1_date_created.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            '
            'View1_created_by
            '
            Me.View1_created_by.CustomizationCaption = "Person who created event"
            Me.View1_created_by.Caption = "Creator"
            Me.View1_created_by.FieldName = "created_by"
            Me.View1_created_by.Name = "View1_created_by"
            Me.View1_created_by.OptionsColumn.AllowEdit = False
            Me.View1_created_by.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.View1_created_by.Visible = True
            Me.View1_created_by.VisibleIndex = 3
            Me.View1_created_by.Width = 107
            '
            'RetentionControl
            '
            Me.Name = "RetentionControl"
            CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
        End Sub

        Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend GridLevelNode1 As DevExpress.XtraGrid.GridLevelNode
        Friend WithEvents View1_client_retention_event As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents View1_client As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents View1_retention_event As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents View1_amount As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents View1_message As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents View1_expire_type As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents View1_expire_date As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents View1_priority As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents View1_effective_date As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents View1_date_expired As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents View1_date_created As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents View1_created_by As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents View2_client_retention_action As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents View2_client_retention_event As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents View2_retention_action As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents View2_message As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents View2_amount As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents View2_created_by As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents View2_date_created As DevExpress.XtraGrid.Columns.GridColumn
    End Class
End Namespace
