#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DebtPlus.UI.Common
Imports DebtPlus.UI.Client.controls
Imports DebtPlus.UI.Client.Service
Imports System.Threading
Imports DevExpress.Utils
Imports DevExpress.XtraGrid

Namespace forms.Retention

    Friend Class RetentionActionsControl
        Inherits MyGridControlClient

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Private bc As DebtPlus.LINQ.BusinessContext = Nothing

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Load, AddressOf RetentionActionsControl_Load
            EnableMenus = True
        End Sub

        Private ClientRetentionEventDRV As DataRowView

        Public Shadows Sub ReadForm(bc As DebtPlus.LINQ.BusinessContext, ByVal ClientRetentionEventDRV As DataRowView)
            Me.bc = bc
            Me.ClientRetentionEventDRV = ClientRetentionEventDRV

            ' Create the view to select only the desired event items
            UpdateTable = Context.ClientDs.client_retention_actions_table

            ' Ensure that the retention event is properly defaulted to the current item.
            With UpdateTable
                .Columns("client_retention_event").DefaultValue = ClientRetentionEventDRV("client_retention_event")
                .Columns("message").DefaultValue = String.Empty
            End With

            With GridControl1
                .DataSource = New DataView(UpdateTable, String.Format("[client_retention_event]={0:f0}", ClientRetentionEventDRV("client_retention_event")), String.Empty, DataViewRowState.CurrentRows)
                .RefreshDataSource()
            End With

            GridView1.BestFitColumns()
        End Sub

        Public Shadows Sub SaveForm(bc As DebtPlus.LINQ.BusinessContext, ByVal ClientRetentionEventDRV As DataRowView)
            Me.ClientRetentionEventDRV = ClientRetentionEventDRV

            ' Find the items that are added to the list
            Using vue As New DataView(UpdateTable, String.Empty, String.Empty, DataViewRowState.Added)
                For Each drv As DataRowView In vue

                    ' New items need to have the proper values in the linkage
                    drv.BeginEdit()
                    drv("client_retention_event") = ClientRetentionEventDRV("client_retention_event")
                    drv.EndEdit()

                    GeneratePossibleLetter(drv)
                Next
            End Using

            ' Perform the updates
            Context.ClientDs.UpdateClientRetentionActions()
        End Sub

        Protected Overrides Sub OnEditItem(ByVal obj As Object)
            Dim drv As DataRowView = TryCast(obj, DataRowView)
            If drv IsNot Nothing Then
                drv.BeginEdit()

                Using frm As New Form_RetentionAction(Context, drv)
                    If frm.ShowDialog() = DialogResult.OK Then

                        ' If the event is not defined then we need to define it here.
                        Dim client_retention_event As Int32 = DebtPlus.Utils.Nulls.DInt(ClientRetentionEventDRV("client_retention_event"), -1)
                        If client_retention_event <= 0 Then

                            ' Complete the edit of the retention event
                            ClientRetentionEventDRV.EndEdit()

                            Using (New CursorManager())
                                ' Save the retention event to the database
                                Context.ClientDs.UpdateClientRetentionEvents()
                            End Using

                            ' Restart the edit operation again.
                            ClientRetentionEventDRV.BeginEdit()

                            ' The event ID that we want is what the database gave us back.
                            client_retention_event = DebtPlus.Utils.Nulls.DInt(ClientRetentionEventDRV("client_retention_event"), -1)

                            ' Correct the view so that it shows the proper list of items since we have reidentified the item
                            With GridControl1
                                .DataSource = New DataView(UpdateTable,
                                                           String.Format("[client_retention_event]={0:f0}",
                                                                         client_retention_event), String.Empty,
                                                           DataViewRowState.CurrentRows)
                                .RefreshDataSource()
                            End With

                        End If
                        drv("client_retention_event") = client_retention_event

                        drv.EndEdit()
                    Else
                        drv.CancelEdit()
                    End If
                End Using
            End If
        End Sub

        Protected Overrides Sub OnCreateItem()
            Dim vue As DataView = CType(GridControl1.DataSource, System.Data.DataView)
            OnEditItem(vue.AddNew)
        End Sub

        Protected Overrides Sub OnDeleteItem(ByVal obj As Object)
            Dim drv As DataRowView = TryCast(obj, DataRowView)
            If drv IsNot Nothing Then
                drv.Delete()
            End If
        End Sub

        Private Sub GeneratePossibleLetter(ByVal drv As DataRowView)
            Dim RetentionAction As Int32 = DebtPlus.Utils.Nulls.DInt(drv("retention_action"), -1)

            ' Find the retention action from the ADO.NET dataset
            Dim q As DebtPlus.LINQ.retention_action = DebtPlus.LINQ.Cache.retention_action.getList().Find(Function(s) s.Id = RetentionAction)

            ' If there is a letter then generate it here.
            If q IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(q.letter_code) Then
                PrintRetentionLetter(q.letter_code)
            End If
        End Sub

        Private Sub PrintRetentionLetter(ByVal LetterCode As String)
            Dim letter_class As New ClientLetterThread(Context, LetterCode, Nothing)
            Dim thrd As New Thread(AddressOf letter_class.LetterThread)
            thrd.SetApartmentState(ApartmentState.STA)
            thrd.IsBackground = False
            thrd.Name = "Letters"
            thrd.Start()
        End Sub

        Private Sub RetentionActionsControl_Load(sender As Object, e As EventArgs)

            With GridColumn_retention_action
                With .DisplayFormat
                    .Format = New RetentionActionNameFormatter()
                    .FormatType = FormatType.Custom
                End With

                With .GroupFormat
                    .Format = New RetentionActionNameFormatter()
                    .FormatType = FormatType.Custom
                End With

                .AppearanceCell.TextOptions.HAlignment = HorzAlignment.Near
                .AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Near
                .SortMode = ColumnSortMode.DisplayText
            End With
        End Sub
    End Class
End Namespace