#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.UI.Client.Service
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports DevExpress.Utils
Imports DevExpress.XtraEditors
Imports DebtPlus.LINQ
Imports System.Linq

Namespace forms.SecuredProperty

    Friend Class Form_SecuredProperty
        Implements IContext

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Private ReadOnly privateContext As ClientUpdateClass
        Private ReadOnly bc As BusinessContext
        Private Record As secured_property

        ''' <summary>
        ''' Initialize the form
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(ByVal Context As ClientUpdateClass, ByVal bc As BusinessContext, ByVal Record As secured_property)
            MyClass.New()
            Me.privateContext = Context
            Me.bc = bc
            Me.Record = Record

            ' Set the formatting class references for the custom items
            SpinEdit_year_acquired.Properties.DisplayFormat.Format = New FormatNumbers()
            SpinEdit_year_mfg.Properties.DisplayFormat.Format = New FormatNumbers()

            ' Define the data sources when the class is created. It is OK here because it is not the main form and everything should be initialized by this time.
            LookUpEdit_type.Properties.DataSource = DebtPlus.LINQ.Cache.secured_type_group.getList().AsQueryable().Where(Function(stg) String.Compare(stg.KeyType, "HOME", True) <> 0).Join(DebtPlus.LINQ.Cache.secured_type.getList().AsQueryable(), Function(stg) stg.Id, Function(st) st.secured_type_group, Function(stg, st) st).ToList()
            LookUpEdit_general_outcome.Properties.DataSource = DebtPlus.LINQ.Cache.studentLoan_generalOutcome.getList()
            LookUpEdit_workout_type.Properties.DataSource = DebtPlus.LINQ.Cache.studentLoan_workoutType.getList()
            LookUpEdit_borrower.Properties.DataSource = DebtPlus.LINQ.Cache.studentLoan_borrower.getList()

            ' Register the event handlers
            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf Form_Load
            AddHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
            AddHandler SpinEdit_year_acquired.EditValueChanging, AddressOf SpinEdit_EditValueChanging
            AddHandler SpinEdit_year_mfg.EditValueChanging, AddressOf SpinEdit_EditValueChanging
            AddHandler SpinEdit_year_mfg.Spin, AddressOf CalcEdit_current_value_Spin
            AddHandler SpinEdit_year_acquired.Spin, AddressOf CalcEdit_current_value_Spin
            AddHandler LookUpEdit_type.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler LookUpEdit_general_outcome.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler LookUpEdit_workout_type.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler LookUpEdit_borrower.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler LayoutControl1.Resize, AddressOf LayoutControl1_Resize
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler Load, AddressOf Form_Load
            RemoveHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
            RemoveHandler SpinEdit_year_acquired.EditValueChanging, AddressOf SpinEdit_EditValueChanging
            RemoveHandler SpinEdit_year_mfg.EditValueChanging, AddressOf SpinEdit_EditValueChanging
            RemoveHandler SpinEdit_year_mfg.Spin, AddressOf CalcEdit_current_value_Spin
            RemoveHandler SpinEdit_year_acquired.Spin, AddressOf CalcEdit_current_value_Spin
            RemoveHandler LookUpEdit_type.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler LookUpEdit_general_outcome.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler LookUpEdit_workout_type.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler LookUpEdit_borrower.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler LayoutControl1.Resize, AddressOf LayoutControl1_Resize
        End Sub

        ''' <summary>
        ''' Pointer to the client update information for the current client
        ''' </summary>
        Public ReadOnly Property Context() As ClientUpdateClass Implements IContext.Context
            Get
                Return privateContext
            End Get
        End Property

        Private Sub SimpleButton_OK_Click(ByVal sender As Object, ByVal e As EventArgs)

            ' Update the record with the resulting values when the OK button is pressed
            Record.secured_type = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_type.EditValue).GetValueOrDefault()
            Record.general_outcome_id = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_general_outcome.EditValue)
            Record.type_of_workout_id = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_workout_type.EditValue)
            Record.borrower_id = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_borrower.EditValue)
            Record.school_name = DebtPlus.Utils.Nulls.v_String(TextEdit_SchoolName.EditValue)
            Record.description = DebtPlus.Utils.Nulls.v_String(TextEdit_description.EditValue)
            Record.sub_description = DebtPlus.Utils.Nulls.v_String(TextEdit_sub_description.EditValue)
            Record.current_value = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_current_value.EditValue)
            Record.original_price = Convert.ToDecimal(CalcEdit_original_price.EditValue)
            Record.year_mfg = Convert.ToInt32(SpinEdit_year_mfg.EditValue)
            Record.year_acquired = Convert.ToInt32(SpinEdit_year_acquired.EditValue)

        End Sub

        Private Sub Form_Load(ByVal sender As Object, ByVal e As EventArgs)
            UnRegisterHandlers()
            Try
                ' Define the control values
                LookUpEdit_type.EditValue = Record.secured_type
                LookUpEdit_general_outcome.EditValue = Record.general_outcome_id
                LookUpEdit_workout_type.EditValue = Record.type_of_workout_id
                LookUpEdit_borrower.EditValue = Record.borrower_id
                TextEdit_SchoolName.EditValue = Record.school_name
                TextEdit_description.EditValue = Record.description
                TextEdit_sub_description.EditValue = Record.sub_description
                CalcEdit_current_value.EditValue = Record.current_value
                CalcEdit_original_price.EditValue = Record.original_price
                SpinEdit_year_acquired.EditValue = Record.year_acquired
                SpinEdit_year_mfg.EditValue = Record.year_mfg

                ' Load the loan information if this is not a new record.
                SecuredLoansControl1.ReadForm(bc, Record)

            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Sub SpinEdit_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)

            ' Do not allow the value to go negative
            Dim newValue As Int32 = DebtPlus.Utils.Nulls.DInt(e.NewValue)
            If newValue < 0 Then
                e.Cancel = True
            End If
        End Sub

        Private Sub CalcEdit_current_value_Spin(ByVal sender As Object, ByVal e As SpinEventArgs)
            Dim ctl As DevExpress.XtraEditors.SpinEdit = CType(sender, SpinEdit)

            ' Make it jump from 0 to 1980 in one fell swoop. The user can take it from there.
            If ctl.EditValue IsNot Nothing AndAlso ctl.EditValue IsNot DBNull.Value AndAlso Convert.ToInt32(ctl.EditValue) = 0 Then
                ctl.EditValue = 1980
                e.Handled = True
            End If
        End Sub

        Private Class FormatNumbers
            Implements IFormatProvider, ICustomFormatter

            Public Function Format(ByVal format1 As String, ByVal arg As Object, ByVal formatProvider As IFormatProvider) _
                As String Implements ICustomFormatter.Format
                Dim answer As String = String.Empty

                If arg IsNot Nothing AndAlso arg IsNot DBNull.Value Then
                    Dim value As Int32 = Convert.ToInt32(arg)
                    If value <> 0 Then
                        answer = value.ToString()
                    End If
                End If

                Return answer
            End Function

            Public Function GetFormat(ByVal formatType As Type) As Object Implements IFormatProvider.GetFormat
                Return Me
            End Function
        End Class

        Private Sub LayoutControl1_Resize(sender As Object, e As System.EventArgs)
            EmptySpaceItem_Left.Width = (EmptySpaceItem_Left.Width + EmptySpaceItem_Right.Width) \ 2
        End Sub
    End Class
End Namespace
