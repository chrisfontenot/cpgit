﻿Namespace forms.SecuredProperty
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form_SecuredProperty
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.SimpleButton_OK = New DevExpress.XtraEditors.SimpleButton()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.LookUpEdit_borrower = New DevExpress.XtraEditors.LookUpEdit()
            Me.LookUpEdit_type = New DevExpress.XtraEditors.LookUpEdit()
            Me.TextEdit_description = New DevExpress.XtraEditors.TextEdit()
            Me.TextEdit_sub_description = New DevExpress.XtraEditors.TextEdit()
            Me.SecuredLoansControl1 = New DebtPlus.UI.Client.forms.SecuredProperty.SecuredLoansControl()
            Me.LookUpEdit_general_outcome = New DevExpress.XtraEditors.LookUpEdit()
            Me.LookUpEdit_workout_type = New DevExpress.XtraEditors.LookUpEdit()
            Me.TextEdit_SchoolName = New DevExpress.XtraEditors.TextEdit()
            Me.SpinEdit_year_acquired = New DevExpress.XtraEditors.SpinEdit()
            Me.CalcEdit_original_price = New DevExpress.XtraEditors.CalcEdit()
            Me.SpinEdit_year_mfg = New DevExpress.XtraEditors.SpinEdit()
            Me.SimpleButton_Cancel = New DevExpress.XtraEditors.SimpleButton()
            Me.CalcEdit_current_value = New DevExpress.XtraEditors.CalcEdit()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem_Left = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem_Right = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.LookUpEdit_borrower.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_type.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_description.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_sub_description.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.SecuredLoansControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_general_outcome.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_workout_type.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_SchoolName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.SpinEdit_year_acquired.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_original_price.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.SpinEdit_year_mfg.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_current_value.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem_Left, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem_Right, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'DefaultLookAndFeel1
            '
            Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin"
            '
            'SimpleButton_OK
            '
            Me.SimpleButton_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_OK.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.SimpleButton_OK.Location = New System.Drawing.Point(282, 256)
            Me.SimpleButton_OK.MaximumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.MinimumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.Name = "SimpleButton_OK"
            Me.SimpleButton_OK.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_OK.StyleController = Me.LayoutControl1
            Me.SimpleButton_OK.TabIndex = 12
            Me.SimpleButton_OK.Text = "&OK"
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_borrower)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_type)
            Me.LayoutControl1.Controls.Add(Me.TextEdit_description)
            Me.LayoutControl1.Controls.Add(Me.TextEdit_sub_description)
            Me.LayoutControl1.Controls.Add(Me.SecuredLoansControl1)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_general_outcome)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_workout_type)
            Me.LayoutControl1.Controls.Add(Me.TextEdit_SchoolName)
            Me.LayoutControl1.Controls.Add(Me.SpinEdit_year_acquired)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit_original_price)
            Me.LayoutControl1.Controls.Add(Me.SpinEdit_year_mfg)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton_Cancel)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit_current_value)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton_OK)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.OptionsFocus.EnableAutoTabOrder = False
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(643, 291)
            Me.LayoutControl1.TabIndex = 24
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'LookUpEdit_borrower
            '
            Me.LookUpEdit_borrower.Location = New System.Drawing.Point(84, 204)
            Me.LookUpEdit_borrower.Name = "LookUpEdit_borrower"
            Me.LookUpEdit_borrower.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_borrower.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("borrower_id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "n0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_borrower.Properties.DisplayMember = "description"
            Me.LookUpEdit_borrower.Properties.NullText = ""
            Me.LookUpEdit_borrower.Properties.ShowFooter = False
            Me.LookUpEdit_borrower.Properties.ShowHeader = False
            Me.LookUpEdit_borrower.Properties.ShowLines = False
            Me.LookUpEdit_borrower.Properties.SortColumnIndex = 1
            Me.LookUpEdit_borrower.Properties.ValueMember = "Id"
            Me.LookUpEdit_borrower.Size = New System.Drawing.Size(232, 20)
            Me.LookUpEdit_borrower.StyleController = Me.LayoutControl1
            Me.LookUpEdit_borrower.TabIndex = 10
            Me.LookUpEdit_borrower.ToolTip = "Is the borrower the Applicant or Co-applicant"
            '
            'LookUpEdit_type
            '
            Me.LookUpEdit_type.Location = New System.Drawing.Point(84, 12)
            Me.LookUpEdit_type.Name = "LookUpEdit_type"
            Me.LookUpEdit_type.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_type.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("secured_type", "ID", 20, DevExpress.Utils.FormatType.Numeric, "n0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_type.Properties.DisplayMember = "description"
            Me.LookUpEdit_type.Properties.NullText = ""
            Me.LookUpEdit_type.Properties.ShowFooter = False
            Me.LookUpEdit_type.Properties.ShowHeader = False
            Me.LookUpEdit_type.Properties.ShowLines = False
            Me.LookUpEdit_type.Properties.SortColumnIndex = 1
            Me.LookUpEdit_type.Properties.ValueMember = "Id"
            Me.LookUpEdit_type.Size = New System.Drawing.Size(232, 20)
            Me.LookUpEdit_type.StyleController = Me.LayoutControl1
            Me.LookUpEdit_type.TabIndex = 0
            Me.LookUpEdit_type.ToolTip = "Please choose the appropriate type of property from this list."
            '
            'TextEdit_description
            '
            Me.TextEdit_description.Location = New System.Drawing.Point(84, 36)
            Me.TextEdit_description.Name = "TextEdit_description"
            Me.TextEdit_description.Properties.MaxLength = 50
            Me.TextEdit_description.Size = New System.Drawing.Size(232, 20)
            Me.TextEdit_description.StyleController = Me.LayoutControl1
            Me.TextEdit_description.TabIndex = 1
            Me.TextEdit_description.ToolTip = "This is the description of the item. If this is a car, put the maker, i.e. ""Ford""" & _
        " here. Put the model, i.e. ""Mustang"" in the ""model"" field below."
            '
            'TextEdit_sub_description
            '
            Me.TextEdit_sub_description.Location = New System.Drawing.Point(84, 60)
            Me.TextEdit_sub_description.Name = "TextEdit_sub_description"
            Me.TextEdit_sub_description.Properties.MaxLength = 50
            Me.TextEdit_sub_description.Size = New System.Drawing.Size(232, 20)
            Me.TextEdit_sub_description.StyleController = Me.LayoutControl1
            Me.TextEdit_sub_description.TabIndex = 2
            Me.TextEdit_sub_description.ToolTip = "If this is a car, what model is it? The maker, i.e. ""Ford"" is in the description " & _
        "field."
            '
            'SecuredLoansControl1
            '
            Me.SecuredLoansControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SecuredLoansControl1.Location = New System.Drawing.Point(320, 12)
            Me.SecuredLoansControl1.MinimumSize = New System.Drawing.Size(311, 0)
            Me.SecuredLoansControl1.Name = "SecuredLoansControl1"
            Me.SecuredLoansControl1.Size = New System.Drawing.Size(311, 223)
            Me.SecuredLoansControl1.TabIndex = 11
            '
            'LookUpEdit_general_outcome
            '
            Me.LookUpEdit_general_outcome.Location = New System.Drawing.Point(84, 156)
            Me.LookUpEdit_general_outcome.Name = "LookUpEdit_general_outcome"
            Me.LookUpEdit_general_outcome.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_general_outcome.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("general_outcome_id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "n0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_general_outcome.Properties.DisplayMember = "description"
            Me.LookUpEdit_general_outcome.Properties.NullText = ""
            Me.LookUpEdit_general_outcome.Properties.ShowFooter = False
            Me.LookUpEdit_general_outcome.Properties.ShowHeader = False
            Me.LookUpEdit_general_outcome.Properties.ShowLines = False
            Me.LookUpEdit_general_outcome.Properties.SortColumnIndex = 1
            Me.LookUpEdit_general_outcome.Properties.ValueMember = "Id"
            Me.LookUpEdit_general_outcome.Size = New System.Drawing.Size(232, 20)
            Me.LookUpEdit_general_outcome.StyleController = Me.LayoutControl1
            Me.LookUpEdit_general_outcome.TabIndex = 8
            Me.LookUpEdit_general_outcome.ToolTip = "Please choose the general outcome of the student Loan session"
            '
            'LookUpEdit_workout_type
            '
            Me.LookUpEdit_workout_type.Location = New System.Drawing.Point(84, 180)
            Me.LookUpEdit_workout_type.Name = "LookUpEdit_workout_type"
            Me.LookUpEdit_workout_type.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_workout_type.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("type_of_workout_id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "n0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_workout_type.Properties.DisplayMember = "description"
            Me.LookUpEdit_workout_type.Properties.NullText = ""
            Me.LookUpEdit_workout_type.Properties.ShowFooter = False
            Me.LookUpEdit_workout_type.Properties.ShowHeader = False
            Me.LookUpEdit_workout_type.Properties.ShowLines = False
            Me.LookUpEdit_workout_type.Properties.SortColumnIndex = 1
            Me.LookUpEdit_workout_type.Properties.ValueMember = "Id"
            Me.LookUpEdit_workout_type.Size = New System.Drawing.Size(232, 20)
            Me.LookUpEdit_workout_type.StyleController = Me.LayoutControl1
            Me.LookUpEdit_workout_type.TabIndex = 9
            Me.LookUpEdit_workout_type.ToolTip = "Please choose the workout type client applied for"
            '
            'TextEdit_SchoolName
            '
            Me.TextEdit_SchoolName.Location = New System.Drawing.Point(84, 132)
            Me.TextEdit_SchoolName.Name = "TextEdit_SchoolName"
            Me.TextEdit_SchoolName.Properties.MaxLength = 50
            Me.TextEdit_SchoolName.Size = New System.Drawing.Size(232, 20)
            Me.TextEdit_SchoolName.StyleController = Me.LayoutControl1
            Me.TextEdit_SchoolName.TabIndex = 7
            Me.TextEdit_SchoolName.ToolTip = "This is the description of the item. If this is a car, put the maker, i.e. ""Ford""" & _
        " here. Put the model, i.e. ""Mustang"" in the ""model"" field below."
            '
            'SpinEdit_year_acquired
            '
            Me.SpinEdit_year_acquired.Location = New System.Drawing.Point(263, 84)
            Me.SpinEdit_year_acquired.Name = "SpinEdit_year_acquired"
            Me.SpinEdit_year_acquired.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.SpinEdit_year_acquired.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.SpinEdit_year_acquired.Properties.DisplayFormat.FormatString = "{0:f0}"
            Me.SpinEdit_year_acquired.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.SpinEdit_year_acquired.Properties.EditFormat.FormatString = "{0:f0}"
            Me.SpinEdit_year_acquired.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.SpinEdit_year_acquired.Properties.Increment = New Decimal(New Integer() {10, 0, 0, 0})
            Me.SpinEdit_year_acquired.Properties.IsFloatValue = False
            Me.SpinEdit_year_acquired.Properties.Mask.EditMask = "\d+"
            Me.SpinEdit_year_acquired.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.SpinEdit_year_acquired.Size = New System.Drawing.Size(53, 20)
            Me.SpinEdit_year_acquired.StyleController = Me.LayoutControl1
            Me.SpinEdit_year_acquired.TabIndex = 4
            Me.SpinEdit_year_acquired.ToolTip = "When did you purchase the item?"
            '
            'CalcEdit_original_price
            '
            Me.CalcEdit_original_price.Location = New System.Drawing.Point(84, 108)
            Me.CalcEdit_original_price.Name = "CalcEdit_original_price"
            Me.CalcEdit_original_price.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.CalcEdit_original_price.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit_original_price.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_original_price.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_original_price.Properties.DisplayFormat.FormatString = "{0:c}"
            Me.CalcEdit_original_price.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_original_price.Properties.EditFormat.FormatString = "{0:f2}"
            Me.CalcEdit_original_price.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_original_price.Properties.Mask.EditMask = "f2"
            Me.CalcEdit_original_price.Properties.Precision = 2
            Me.CalcEdit_original_price.Size = New System.Drawing.Size(103, 20)
            Me.CalcEdit_original_price.StyleController = Me.LayoutControl1
            Me.CalcEdit_original_price.TabIndex = 5
            Me.CalcEdit_original_price.ToolTip = "This is the original purchase price for the item."
            '
            'SpinEdit_year_mfg
            '
            Me.SpinEdit_year_mfg.Location = New System.Drawing.Point(263, 108)
            Me.SpinEdit_year_mfg.Name = "SpinEdit_year_mfg"
            Me.SpinEdit_year_mfg.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.SpinEdit_year_mfg.Properties.DisplayFormat.FormatString = "{0:f0}"
            Me.SpinEdit_year_mfg.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.SpinEdit_year_mfg.Properties.EditFormat.FormatString = "{0:f0}"
            Me.SpinEdit_year_mfg.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.SpinEdit_year_mfg.Properties.Increment = New Decimal(New Integer() {10, 0, 0, 0})
            Me.SpinEdit_year_mfg.Properties.IsFloatValue = False
            Me.SpinEdit_year_mfg.Properties.Mask.EditMask = "\d+"
            Me.SpinEdit_year_mfg.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.SpinEdit_year_mfg.Size = New System.Drawing.Size(53, 20)
            Me.SpinEdit_year_mfg.StyleController = Me.LayoutControl1
            Me.SpinEdit_year_mfg.TabIndex = 6
            Me.SpinEdit_year_mfg.ToolTip = "If this is a car or home, when was it built?"
            '
            'SimpleButton_Cancel
            '
            Me.SimpleButton_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.SimpleButton_Cancel.Location = New System.Drawing.Point(361, 256)
            Me.SimpleButton_Cancel.MaximumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.MinimumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.Name = "SimpleButton_Cancel"
            Me.SimpleButton_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_Cancel.StyleController = Me.LayoutControl1
            Me.SimpleButton_Cancel.TabIndex = 13
            Me.SimpleButton_Cancel.Text = "&Cancel"
            '
            'CalcEdit_current_value
            '
            Me.CalcEdit_current_value.Location = New System.Drawing.Point(84, 84)
            Me.CalcEdit_current_value.Name = "CalcEdit_current_value"
            Me.CalcEdit_current_value.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.CalcEdit_current_value.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit_current_value.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_current_value.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_current_value.Properties.DisplayFormat.FormatString = "{0:c}"
            Me.CalcEdit_current_value.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_current_value.Properties.EditFormat.FormatString = "{0:f2}"
            Me.CalcEdit_current_value.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_current_value.Properties.Mask.BeepOnError = True
            Me.CalcEdit_current_value.Properties.Mask.EditMask = "f2"
            Me.CalcEdit_current_value.Properties.Precision = 2
            Me.CalcEdit_current_value.Size = New System.Drawing.Size(103, 20)
            Me.CalcEdit_current_value.StyleController = Me.LayoutControl1
            Me.CalcEdit_current_value.TabIndex = 3
            Me.CalcEdit_current_value.ToolTip = "Current value for the item. This is the ""best guess"" for its worth."
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup1.GroupBordersVisible = False
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.LayoutControlItem8, Me.LayoutControlItem9, Me.LayoutControlItem10, Me.LayoutControlItem11, Me.LayoutControlItem12, Me.LayoutControlItem6, Me.LayoutControlItem13, Me.LayoutControlItem14, Me.EmptySpaceItem_Left, Me.EmptySpaceItem_Right, Me.EmptySpaceItem2, Me.EmptySpaceItem1, Me.LayoutControlItem7})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(643, 291)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.SecuredLoansControl1
            Me.LayoutControlItem1.CustomizationFormText = "LayoutControlItem1"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(308, 0)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(315, 227)
            Me.LayoutControlItem1.Text = "LayoutControlItem1"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem1.TextToControlDistance = 0
            Me.LayoutControlItem1.TextVisible = False
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.LookUpEdit_type
            Me.LayoutControlItem2.CustomizationFormText = "&Type"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(308, 24)
            Me.LayoutControlItem2.Text = "&Type"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(68, 13)
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.TextEdit_description
            Me.LayoutControlItem3.CustomizationFormText = "&Description"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(308, 24)
            Me.LayoutControlItem3.Text = "&Description"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(68, 13)
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.TextEdit_sub_description
            Me.LayoutControlItem4.CustomizationFormText = "&Model"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 48)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(308, 24)
            Me.LayoutControlItem4.Text = "&Model"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(68, 13)
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Control = Me.CalcEdit_current_value
            Me.LayoutControlItem5.CustomizationFormText = "&Value"
            Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 72)
            Me.LayoutControlItem5.Name = "LayoutControlItem5"
            Me.LayoutControlItem5.Size = New System.Drawing.Size(179, 24)
            Me.LayoutControlItem5.Text = "&Value"
            Me.LayoutControlItem5.TextSize = New System.Drawing.Size(68, 13)
            '
            'LayoutControlItem8
            '
            Me.LayoutControlItem8.Control = Me.SpinEdit_year_mfg
            Me.LayoutControlItem8.CustomizationFormText = "Model &Year"
            Me.LayoutControlItem8.Location = New System.Drawing.Point(179, 96)
            Me.LayoutControlItem8.Name = "LayoutControlItem8"
            Me.LayoutControlItem8.Size = New System.Drawing.Size(129, 24)
            Me.LayoutControlItem8.Text = "Model &Year"
            Me.LayoutControlItem8.TextSize = New System.Drawing.Size(68, 13)
            '
            'LayoutControlItem9
            '
            Me.LayoutControlItem9.Control = Me.TextEdit_SchoolName
            Me.LayoutControlItem9.CustomizationFormText = "&School Name"
            Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 120)
            Me.LayoutControlItem9.Name = "LayoutControlItem9"
            Me.LayoutControlItem9.Size = New System.Drawing.Size(308, 24)
            Me.LayoutControlItem9.Text = "&School Name"
            Me.LayoutControlItem9.TextSize = New System.Drawing.Size(68, 13)
            '
            'LayoutControlItem10
            '
            Me.LayoutControlItem10.Control = Me.LookUpEdit_general_outcome
            Me.LayoutControlItem10.CustomizationFormText = "&Outcome"
            Me.LayoutControlItem10.Location = New System.Drawing.Point(0, 144)
            Me.LayoutControlItem10.Name = "LayoutControlItem10"
            Me.LayoutControlItem10.Size = New System.Drawing.Size(308, 24)
            Me.LayoutControlItem10.Text = "&Outcome"
            Me.LayoutControlItem10.TextSize = New System.Drawing.Size(68, 13)
            '
            'LayoutControlItem11
            '
            Me.LayoutControlItem11.Control = Me.LookUpEdit_workout_type
            Me.LayoutControlItem11.CustomizationFormText = "&Workout Type"
            Me.LayoutControlItem11.Location = New System.Drawing.Point(0, 168)
            Me.LayoutControlItem11.Name = "LayoutControlItem11"
            Me.LayoutControlItem11.Size = New System.Drawing.Size(308, 24)
            Me.LayoutControlItem11.Text = "&Workout Type"
            Me.LayoutControlItem11.TextSize = New System.Drawing.Size(68, 13)
            '
            'LayoutControlItem12
            '
            Me.LayoutControlItem12.Control = Me.LookUpEdit_borrower
            Me.LayoutControlItem12.CustomizationFormText = "&Borrower"
            Me.LayoutControlItem12.Location = New System.Drawing.Point(0, 192)
            Me.LayoutControlItem12.Name = "LayoutControlItem12"
            Me.LayoutControlItem12.Size = New System.Drawing.Size(308, 24)
            Me.LayoutControlItem12.Text = "&Borrower"
            Me.LayoutControlItem12.TextSize = New System.Drawing.Size(68, 13)
            '
            'LayoutControlItem6
            '
            Me.LayoutControlItem6.Control = Me.SpinEdit_year_acquired
            Me.LayoutControlItem6.CustomizationFormText = "Year &Acquired"
            Me.LayoutControlItem6.Location = New System.Drawing.Point(179, 72)
            Me.LayoutControlItem6.Name = "LayoutControlItem6"
            Me.LayoutControlItem6.Size = New System.Drawing.Size(129, 24)
            Me.LayoutControlItem6.Text = "Year &Acquired"
            Me.LayoutControlItem6.TextSize = New System.Drawing.Size(68, 13)
            '
            'LayoutControlItem13
            '
            Me.LayoutControlItem13.Control = Me.SimpleButton_OK
            Me.LayoutControlItem13.CustomizationFormText = "LayoutControlItem13"
            Me.LayoutControlItem13.Location = New System.Drawing.Point(270, 244)
            Me.LayoutControlItem13.Name = "LayoutControlItem13"
            Me.LayoutControlItem13.Size = New System.Drawing.Size(79, 27)
            Me.LayoutControlItem13.Text = "LayoutControlItem13"
            Me.LayoutControlItem13.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem13.TextToControlDistance = 0
            Me.LayoutControlItem13.TextVisible = False
            '
            'LayoutControlItem14
            '
            Me.LayoutControlItem14.Control = Me.SimpleButton_Cancel
            Me.LayoutControlItem14.CustomizationFormText = "LayoutControlItem14"
            Me.LayoutControlItem14.Location = New System.Drawing.Point(349, 244)
            Me.LayoutControlItem14.Name = "LayoutControlItem14"
            Me.LayoutControlItem14.Size = New System.Drawing.Size(79, 27)
            Me.LayoutControlItem14.Text = "LayoutControlItem14"
            Me.LayoutControlItem14.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem14.TextToControlDistance = 0
            Me.LayoutControlItem14.TextVisible = False
            '
            'EmptySpaceItem_Left
            '
            Me.EmptySpaceItem_Left.AllowHotTrack = False
            Me.EmptySpaceItem_Left.CustomizationFormText = "EmptySpaceItem_Left"
            Me.EmptySpaceItem_Left.Location = New System.Drawing.Point(0, 244)
            Me.EmptySpaceItem_Left.Name = "EmptySpaceItem_Left"
            Me.EmptySpaceItem_Left.Size = New System.Drawing.Size(270, 27)
            Me.EmptySpaceItem_Left.Text = "EmptySpaceItem_Left"
            Me.EmptySpaceItem_Left.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem_Right
            '
            Me.EmptySpaceItem_Right.AllowHotTrack = False
            Me.EmptySpaceItem_Right.CustomizationFormText = "EmptySpaceItem_Right"
            Me.EmptySpaceItem_Right.Location = New System.Drawing.Point(428, 244)
            Me.EmptySpaceItem_Right.Name = "EmptySpaceItem_Right"
            Me.EmptySpaceItem_Right.Size = New System.Drawing.Size(195, 27)
            Me.EmptySpaceItem_Right.Text = "EmptySpaceItem_Right"
            Me.EmptySpaceItem_Right.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem2
            '
            Me.EmptySpaceItem2.AllowHotTrack = False
            Me.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Location = New System.Drawing.Point(0, 227)
            Me.EmptySpaceItem2.MaxSize = New System.Drawing.Size(0, 17)
            Me.EmptySpaceItem2.MinSize = New System.Drawing.Size(10, 17)
            Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Size = New System.Drawing.Size(623, 17)
            Me.EmptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
            Me.EmptySpaceItem2.Text = "EmptySpaceItem2"
            Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem1
            '
            Me.EmptySpaceItem1.AllowHotTrack = False
            Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 216)
            Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Size = New System.Drawing.Size(308, 11)
            Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
            Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
            '
            'LayoutControlItem7
            '
            Me.LayoutControlItem7.Control = Me.CalcEdit_original_price
            Me.LayoutControlItem7.CustomizationFormText = "&Price"
            Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 96)
            Me.LayoutControlItem7.Name = "LayoutControlItem7"
            Me.LayoutControlItem7.Size = New System.Drawing.Size(179, 24)
            Me.LayoutControlItem7.Text = "&Price"
            Me.LayoutControlItem7.TextSize = New System.Drawing.Size(68, 13)
            '
            Me.SpinEdit_year_acquired.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.SpinEdit_year_mfg.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            '
            'Form_SecuredProperty
            '
            Me.AcceptButton = Me.SimpleButton_OK
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.SimpleButton_Cancel
            Me.ClientSize = New System.Drawing.Size(643, 291)
            Me.Controls.Add(Me.LayoutControl1)
            Me.Name = "Form_SecuredProperty"
            Me.Text = "Secured Property Information"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.LookUpEdit_borrower.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_type.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_description.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_sub_description.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.SecuredLoansControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_general_outcome.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_workout_type.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_SchoolName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.SpinEdit_year_acquired.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_original_price.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.SpinEdit_year_mfg.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_current_value.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem_Left, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem_Right, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents SimpleButton_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents LookUpEdit_type As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LookUpEdit_workout_type As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents TextEdit_description As DevExpress.XtraEditors.TextEdit
        Friend WithEvents TextEdit_sub_description As DevExpress.XtraEditors.TextEdit
        Friend WithEvents CalcEdit_current_value As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents CalcEdit_original_price As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents SpinEdit_year_acquired As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents SpinEdit_year_mfg As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents SecuredLoansControl1 As SecuredLoansControl
        Friend WithEvents LookUpEdit_general_outcome As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents TextEdit_SchoolName As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LookUpEdit_borrower As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem_Left As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem_Right As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    End Class
End Namespace