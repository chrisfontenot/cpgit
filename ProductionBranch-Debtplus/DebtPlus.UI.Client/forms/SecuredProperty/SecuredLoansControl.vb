#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports System.ComponentModel
Imports DebtPlus.UI.Client.controls
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports DebtPlus.LINQ
Imports System.Linq

Namespace forms.SecuredProperty
    Friend Class SecuredLoansControl
        Inherits MyGridControlClient

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Private propertyRecord As secured_property
        Private bc As BusinessContext

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            MyBase.EnableMenus = True
        End Sub

        Public Shadows Sub ReadForm(bc As BusinessContext, ByVal propertyRecord As secured_property)
            MyBase.ReadForm(bc)
            Me.bc = bc
            Me.propertyRecord = propertyRecord

            ' Generate a separate collection for the grid since it does not like LINQ items
            GridControl1.DataSource = propertyRecord.secured_loans.GetNewBindingList()
        End Sub

        Protected Overrides Sub OnCreateItem()
            Dim loanRecord As secured_loan = DebtPlus.LINQ.Factory.Manufacture_secured_loan()
            Using frm As New Form_SecuredLoan(loanRecord)
                If frm.ShowDialog() <> DialogResult.OK Then
                    Return
                End If
            End Using

            propertyRecord.secured_loans.Add(loanRecord)
            GridControl1.DataSource = propertyRecord.secured_loans.GetNewBindingList()
            GridView1.RefreshData()

        End Sub

        Protected Overrides Sub OnEditItem(ByVal obj As Object)
            Dim loanRecord As secured_loan = TryCast(obj, secured_loan)
            If loanRecord Is Nothing Then
                Return
            End If

            Using frm As New Form_SecuredLoan(loanRecord)
                If frm.ShowDialog() <> DialogResult.OK Then
                    Return
                End If
            End Using

            ' Regenerate the collection of loans and update the grid
            GridControl1.DataSource = propertyRecord.secured_loans.GetNewBindingList()
            GridView1.RefreshData()
        End Sub

        Protected Overrides Sub OnDeleteItem(ByVal obj As Object)

            Dim loanRecord As secured_loan = TryCast(obj, secured_loan)
            If loanRecord Is Nothing Then
                Return
            End If

            If DebtPlus.Data.Prompts.RequestConfirmation_Delete <> DialogResult.Yes Then
                Return
            End If

            ' Delete the record from the database
            loanRecord.secured_property1 = Nothing

            ' Remove the item from the list
            propertyRecord.secured_loans.Remove(loanRecord)

            ' Regenerate the list of loans
            GridControl1.DataSource = propertyRecord.secured_loans.GetNewBindingList()
            GridView1.RefreshData()
        End Sub
    End Class
End Namespace
