#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.UI.Client.Service
Imports System.Linq
Imports DebtPlus.LINQ

Namespace forms.SecuredProperty
    Friend Class Form_SecuredLoan

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Private loanRecord As secured_loan

        ''' <summary>
        ''' Initialize the form
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(ByVal loanRecord As secured_loan)
            MyClass.New()
            Me.loanRecord = loanRecord

            AddHandler Load, AddressOf Form_Load
            AddHandler SpinEdit_periods.Validating, AddressOf DebtPlus.Data.Validation.NonNegative
            AddHandler SimpleButton_ok.Click, AddressOf SimpleButton_ok_Click
        End Sub

        ''' <summary>
        ''' FORM LOAD event processing
        ''' </summary>
        Private Sub Form_Load(ByVal sender As Object, ByVal e As EventArgs)

            RemoveHandler Load, AddressOf Form_Load
            RemoveHandler SpinEdit_periods.Validating, AddressOf DebtPlus.Data.Validation.NonNegative
            RemoveHandler SimpleButton_ok.Click, AddressOf SimpleButton_ok_Click
            Try
                CalcEdit_balance.EditValue = loanRecord.balance
                CalcEdit_original_amount.EditValue = loanRecord.original_amount
                CalcEdit_past_due_amount.EditValue = loanRecord.past_due_amount
                SpinEdit_periods.EditValue = loanRecord.periods
                CalcEdit_Payment.EditValue = loanRecord.payment
                PercentEdit_interest_rate.EditValue = loanRecord.interest_rate
                SpinEdit_past_due_periods.EditValue = loanRecord.past_due_periods
                TextEdit_account_number.EditValue = loanRecord.account_number
                TextEdit_lender.EditValue = loanRecord.lender

            Finally
                AddHandler Load, AddressOf Form_Load
                AddHandler SpinEdit_periods.Validating, AddressOf DebtPlus.Data.Validation.NonNegative
                AddHandler SimpleButton_ok.Click, AddressOf SimpleButton_ok_Click
            End Try
        End Sub

        Private Sub SimpleButton_ok_Click(sender As Object, e As System.EventArgs)

            ' Update the record when the OK button is pressed
            loanRecord.balance = Convert.ToDecimal(CalcEdit_balance.EditValue)
            loanRecord.original_amount = Convert.ToDecimal(CalcEdit_original_amount.EditValue)
            loanRecord.past_due_amount = Convert.ToDecimal(CalcEdit_past_due_amount.EditValue)
            loanRecord.periods = Convert.ToInt32(SpinEdit_periods.EditValue)
            loanRecord.payment = Convert.ToDecimal(CalcEdit_Payment.EditValue)
            loanRecord.interest_rate = Convert.ToDouble(PercentEdit_interest_rate.EditValue)
            loanRecord.past_due_periods = Convert.ToInt32(SpinEdit_past_due_periods.EditValue)
            loanRecord.account_number = TextEdit_account_number.Text.Trim()
            loanRecord.lender = TextEdit_lender.Text.Trim()

        End Sub
    End Class
End Namespace
