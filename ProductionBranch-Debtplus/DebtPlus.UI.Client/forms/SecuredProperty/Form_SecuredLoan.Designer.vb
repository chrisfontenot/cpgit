﻿Namespace forms.SecuredProperty
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Form_SecuredLoan
        Inherits DebtPlus.Data.Forms.DebtPlusForm

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.PercentEdit_interest_rate = New DebtPlus.Data.Controls.PercentEdit()
            Me.CalcEdit_Payment = New DevExpress.XtraEditors.CalcEdit()
            Me.TextEdit_account_number = New DevExpress.XtraEditors.TextEdit()
            Me.TextEdit_lender = New DevExpress.XtraEditors.TextEdit()
            Me.SimpleButton_cancel = New DevExpress.XtraEditors.SimpleButton()
            Me.SpinEdit_past_due_periods = New DevExpress.XtraEditors.SpinEdit()
            Me.SimpleButton_ok = New DevExpress.XtraEditors.SimpleButton()
            Me.CalcEdit_past_due_amount = New DevExpress.XtraEditors.CalcEdit()
            Me.SpinEdit_periods = New DevExpress.XtraEditors.SpinEdit()
            Me.CalcEdit_balance = New DevExpress.XtraEditors.CalcEdit()
            Me.CalcEdit_original_amount = New DevExpress.XtraEditors.CalcEdit()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlGroup3 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem6 = New DevExpress.XtraLayout.EmptySpaceItem()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.PercentEdit_interest_rate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_Payment.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_account_number.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_lender.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.SpinEdit_past_due_periods.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_past_due_amount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.SpinEdit_periods.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_balance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_original_amount.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'DefaultLookAndFeel1
            '
            Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin"
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.PercentEdit_interest_rate)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit_Payment)
            Me.LayoutControl1.Controls.Add(Me.TextEdit_account_number)
            Me.LayoutControl1.Controls.Add(Me.TextEdit_lender)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton_cancel)
            Me.LayoutControl1.Controls.Add(Me.SpinEdit_past_due_periods)
            Me.LayoutControl1.Controls.Add(Me.SimpleButton_ok)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit_past_due_amount)
            Me.LayoutControl1.Controls.Add(Me.SpinEdit_periods)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit_balance)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit_original_amount)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(596, 127, 250, 350)
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(428, 302)
            Me.LayoutControl1.TabIndex = 6
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'PercentEdit_interest_rate
            '
            Me.PercentEdit_interest_rate.Location = New System.Drawing.Point(111, 92)
            Me.PercentEdit_interest_rate.Name = "PercentEdit_interest_rate"
            Me.PercentEdit_interest_rate.Properties.Allow100Percent = False
            Me.PercentEdit_interest_rate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.PercentEdit_interest_rate.Properties.DisplayFormat.FormatString = "{0:p3}"
            Me.PercentEdit_interest_rate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.PercentEdit_interest_rate.Properties.EditFormat.FormatString = "{0:f3}"
            Me.PercentEdit_interest_rate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.PercentEdit_interest_rate.Properties.Precision = 3
            Me.PercentEdit_interest_rate.Size = New System.Drawing.Size(108, 20)
            Me.PercentEdit_interest_rate.StyleController = Me.LayoutControl1
            Me.PercentEdit_interest_rate.TabIndex = 16
            '
            'CalcEdit_Payment
            '
            Me.CalcEdit_Payment.Location = New System.Drawing.Point(310, 44)
            Me.CalcEdit_Payment.Name = "CalcEdit_Payment"
            Me.CalcEdit_Payment.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_Payment.Properties.DisplayFormat.FormatString = "c"
            Me.CalcEdit_Payment.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_Payment.Properties.EditFormat.FormatString = "f2"
            Me.CalcEdit_Payment.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_Payment.Properties.Mask.EditMask = "f2"
            Me.CalcEdit_Payment.Properties.Precision = 2
            Me.CalcEdit_Payment.Size = New System.Drawing.Size(94, 20)
            Me.CalcEdit_Payment.StyleController = Me.LayoutControl1
            Me.CalcEdit_Payment.TabIndex = 15
            '
            'TextEdit_account_number
            '
            Me.TextEdit_account_number.Location = New System.Drawing.Point(111, 219)
            Me.TextEdit_account_number.Name = "TextEdit_account_number"
            Me.TextEdit_account_number.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.TextEdit_account_number.Properties.Mask.EditMask = "[^ ].*"
            Me.TextEdit_account_number.Properties.Mask.IgnoreMaskBlank = False
            Me.TextEdit_account_number.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.TextEdit_account_number.Properties.Mask.ShowPlaceHolders = False
            Me.TextEdit_account_number.Properties.MaxLength = 50
            Me.TextEdit_account_number.Size = New System.Drawing.Size(293, 20)
            Me.TextEdit_account_number.StyleController = Me.LayoutControl1
            Me.TextEdit_account_number.TabIndex = 13
            '
            'TextEdit_lender
            '
            Me.TextEdit_lender.Location = New System.Drawing.Point(111, 195)
            Me.TextEdit_lender.Name = "TextEdit_lender"
            Me.TextEdit_lender.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.TextEdit_lender.Properties.Mask.BeepOnError = True
            Me.TextEdit_lender.Properties.Mask.EditMask = "[^ ].*"
            Me.TextEdit_lender.Properties.Mask.IgnoreMaskBlank = False
            Me.TextEdit_lender.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.TextEdit_lender.Properties.Mask.ShowPlaceHolders = False
            Me.TextEdit_lender.Properties.MaxLength = 50
            Me.TextEdit_lender.Size = New System.Drawing.Size(293, 20)
            Me.TextEdit_lender.StyleController = Me.LayoutControl1
            Me.TextEdit_lender.TabIndex = 12
            '
            'SimpleButton_cancel
            '
            Me.SimpleButton_cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.SimpleButton_cancel.Location = New System.Drawing.Point(213, 267)
            Me.SimpleButton_cancel.MaximumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_cancel.MinimumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_cancel.Name = "SimpleButton_cancel"
            Me.SimpleButton_cancel.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_cancel.StyleController = Me.LayoutControl1
            Me.SimpleButton_cancel.TabIndex = 5
            Me.SimpleButton_cancel.Text = "&Cancel"
            '
            'SpinEdit_past_due_periods
            '
            Me.SpinEdit_past_due_periods.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SpinEdit_past_due_periods.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
            Me.SpinEdit_past_due_periods.Location = New System.Drawing.Point(310, 92)
            Me.SpinEdit_past_due_periods.Name = "SpinEdit_past_due_periods"
            Me.SpinEdit_past_due_periods.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.SpinEdit_past_due_periods.Properties.DisplayFormat.FormatString = "n0"
            Me.SpinEdit_past_due_periods.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.SpinEdit_past_due_periods.Properties.EditFormat.FormatString = "f0"
            Me.SpinEdit_past_due_periods.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.SpinEdit_past_due_periods.Properties.IsFloatValue = False
            Me.SpinEdit_past_due_periods.Properties.Mask.EditMask = "N00"
            Me.SpinEdit_past_due_periods.Size = New System.Drawing.Size(94, 20)
            Me.SpinEdit_past_due_periods.StyleController = Me.LayoutControl1
            Me.SpinEdit_past_due_periods.TabIndex = 11
            '
            'SimpleButton_ok
            '
            Me.SimpleButton_ok.Anchor = System.Windows.Forms.AnchorStyles.Bottom
            Me.SimpleButton_ok.DialogResult = System.Windows.Forms.DialogResult.OK
            Me.SimpleButton_ok.Location = New System.Drawing.Point(134, 267)
            Me.SimpleButton_ok.MaximumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_ok.MinimumSize = New System.Drawing.Size(75, 23)
            Me.SimpleButton_ok.Name = "SimpleButton_ok"
            Me.SimpleButton_ok.Size = New System.Drawing.Size(75, 23)
            Me.SimpleButton_ok.StyleController = Me.LayoutControl1
            Me.SimpleButton_ok.TabIndex = 4
            Me.SimpleButton_ok.Text = "&OK"
            '
            'CalcEdit_past_due_amount
            '
            Me.CalcEdit_past_due_amount.Location = New System.Drawing.Point(310, 116)
            Me.CalcEdit_past_due_amount.Name = "CalcEdit_past_due_amount"
            Me.CalcEdit_past_due_amount.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_past_due_amount.Properties.DisplayFormat.FormatString = "{0:c2}"
            Me.CalcEdit_past_due_amount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_past_due_amount.Properties.EditFormat.FormatString = "{0:f2}"
            Me.CalcEdit_past_due_amount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_past_due_amount.Properties.Precision = 2
            Me.CalcEdit_past_due_amount.Size = New System.Drawing.Size(94, 20)
            Me.CalcEdit_past_due_amount.StyleController = Me.LayoutControl1
            Me.CalcEdit_past_due_amount.TabIndex = 7
            '
            'SpinEdit_periods
            '
            Me.SpinEdit_periods.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.SpinEdit_periods.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
            Me.SpinEdit_periods.Location = New System.Drawing.Point(310, 68)
            Me.SpinEdit_periods.Name = "SpinEdit_periods"
            Me.SpinEdit_periods.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.SpinEdit_periods.Properties.DisplayFormat.FormatString = "n0"
            Me.SpinEdit_periods.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.SpinEdit_periods.Properties.EditFormat.FormatString = "f0"
            Me.SpinEdit_periods.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.SpinEdit_periods.Properties.IsFloatValue = False
            Me.SpinEdit_periods.Properties.Mask.EditMask = "N00"
            Me.SpinEdit_periods.Size = New System.Drawing.Size(94, 20)
            Me.SpinEdit_periods.StyleController = Me.LayoutControl1
            Me.SpinEdit_periods.TabIndex = 9
            '
            'CalcEdit_balance
            '
            Me.CalcEdit_balance.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.CalcEdit_balance.Location = New System.Drawing.Point(111, 44)
            Me.CalcEdit_balance.Name = "CalcEdit_balance"
            Me.CalcEdit_balance.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_balance.Properties.DisplayFormat.FormatString = "{0:c2}"
            Me.CalcEdit_balance.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_balance.Properties.EditFormat.FormatString = "{0:f2}"
            Me.CalcEdit_balance.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_balance.Properties.Precision = 2
            Me.CalcEdit_balance.Size = New System.Drawing.Size(108, 20)
            Me.CalcEdit_balance.StyleController = Me.LayoutControl1
            Me.CalcEdit_balance.TabIndex = 5
            '
            'CalcEdit_original_amount
            '
            Me.CalcEdit_original_amount.Location = New System.Drawing.Point(111, 68)
            Me.CalcEdit_original_amount.Name = "CalcEdit_original_amount"
            Me.CalcEdit_original_amount.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_original_amount.Properties.DisplayFormat.FormatString = "{0:c2}"
            Me.CalcEdit_original_amount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_original_amount.Properties.EditFormat.FormatString = "{0:f2}"
            Me.CalcEdit_original_amount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_original_amount.Properties.Precision = 2
            Me.CalcEdit_original_amount.Size = New System.Drawing.Size(108, 20)
            Me.CalcEdit_original_amount.StyleController = Me.LayoutControl1
            Me.CalcEdit_original_amount.TabIndex = 3
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup1.GroupBordersVisible = False
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem8, Me.LayoutControlItem9, Me.EmptySpaceItem1, Me.EmptySpaceItem2, Me.EmptySpaceItem3, Me.LayoutControlGroup2, Me.LayoutControlGroup3, Me.EmptySpaceItem6})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(428, 302)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem8
            '
            Me.LayoutControlItem8.Control = Me.SimpleButton_ok
            Me.LayoutControlItem8.CustomizationFormText = "LayoutControlItem8"
            Me.LayoutControlItem8.Location = New System.Drawing.Point(122, 255)
            Me.LayoutControlItem8.Name = "LayoutControlItem8"
            Me.LayoutControlItem8.Size = New System.Drawing.Size(79, 27)
            Me.LayoutControlItem8.Text = "LayoutControlItem8"
            Me.LayoutControlItem8.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem8.TextToControlDistance = 0
            Me.LayoutControlItem8.TextVisible = False
            '
            'LayoutControlItem9
            '
            Me.LayoutControlItem9.Control = Me.SimpleButton_cancel
            Me.LayoutControlItem9.CustomizationFormText = "LayoutControlItem9"
            Me.LayoutControlItem9.Location = New System.Drawing.Point(201, 255)
            Me.LayoutControlItem9.Name = "LayoutControlItem9"
            Me.LayoutControlItem9.Size = New System.Drawing.Size(79, 27)
            Me.LayoutControlItem9.Text = "LayoutControlItem9"
            Me.LayoutControlItem9.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem9.TextToControlDistance = 0
            Me.LayoutControlItem9.TextVisible = False
            '
            'EmptySpaceItem1
            '
            Me.EmptySpaceItem1.AllowHotTrack = False
            Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 243)
            Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Size = New System.Drawing.Size(408, 12)
            Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
            Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem2
            '
            Me.EmptySpaceItem2.AllowHotTrack = False
            Me.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Location = New System.Drawing.Point(0, 255)
            Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Size = New System.Drawing.Size(122, 27)
            Me.EmptySpaceItem2.Text = "EmptySpaceItem2"
            Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem3
            '
            Me.EmptySpaceItem3.AllowHotTrack = False
            Me.EmptySpaceItem3.CustomizationFormText = "EmptySpaceItem3"
            Me.EmptySpaceItem3.Location = New System.Drawing.Point(280, 255)
            Me.EmptySpaceItem3.Name = "EmptySpaceItem3"
            Me.EmptySpaceItem3.Size = New System.Drawing.Size(128, 27)
            Me.EmptySpaceItem3.Text = "EmptySpaceItem3"
            Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
            '
            'LayoutControlGroup2
            '
            Me.LayoutControlGroup2.CustomizationFormText = "Balance Information"
            Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem4, Me.LayoutControlItem13, Me.LayoutControlItem3, Me.LayoutControlItem6, Me.LayoutControlItem14, Me.LayoutControlItem7, Me.LayoutControlItem5})
            Me.LayoutControlGroup2.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
            Me.LayoutControlGroup2.Size = New System.Drawing.Size(408, 140)
            Me.LayoutControlGroup2.Text = "Balance Information"
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.CalcEdit_balance
            Me.LayoutControlItem4.CustomizationFormText = "Balance Owing"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(199, 24)
            Me.LayoutControlItem4.Text = "Balance Owing"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(83, 13)
            '
            'LayoutControlItem13
            '
            Me.LayoutControlItem13.Control = Me.CalcEdit_Payment
            Me.LayoutControlItem13.CustomizationFormText = "Payment"
            Me.LayoutControlItem13.Location = New System.Drawing.Point(199, 0)
            Me.LayoutControlItem13.Name = "LayoutControlItem13"
            Me.LayoutControlItem13.Size = New System.Drawing.Size(185, 24)
            Me.LayoutControlItem13.Text = "Payment"
            Me.LayoutControlItem13.TextSize = New System.Drawing.Size(83, 13)
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.CalcEdit_original_amount
            Me.LayoutControlItem3.CustomizationFormText = "Original Amount"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(199, 24)
            Me.LayoutControlItem3.Text = "Original Amount"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(83, 13)
            '
            'LayoutControlItem6
            '
            Me.LayoutControlItem6.Control = Me.SpinEdit_periods
            Me.LayoutControlItem6.CustomizationFormText = "Loan Term"
            Me.LayoutControlItem6.Location = New System.Drawing.Point(199, 24)
            Me.LayoutControlItem6.Name = "LayoutControlItem6"
            Me.LayoutControlItem6.Size = New System.Drawing.Size(185, 24)
            Me.LayoutControlItem6.Text = "Loan Term"
            Me.LayoutControlItem6.TextSize = New System.Drawing.Size(83, 13)
            '
            'LayoutControlItem14
            '
            Me.LayoutControlItem14.Control = Me.PercentEdit_interest_rate
            Me.LayoutControlItem14.CustomizationFormText = "Interest Rate"
            Me.LayoutControlItem14.Location = New System.Drawing.Point(0, 48)
            Me.LayoutControlItem14.Name = "LayoutControlItem14"
            Me.LayoutControlItem14.Size = New System.Drawing.Size(199, 48)
            Me.LayoutControlItem14.Text = "Interest Rate"
            Me.LayoutControlItem14.TextSize = New System.Drawing.Size(83, 13)
            '
            'LayoutControlItem7
            '
            Me.LayoutControlItem7.Control = Me.SpinEdit_past_due_periods
            Me.LayoutControlItem7.CustomizationFormText = "Past Due Months"
            Me.LayoutControlItem7.Location = New System.Drawing.Point(199, 48)
            Me.LayoutControlItem7.Name = "LayoutControlItem7"
            Me.LayoutControlItem7.Size = New System.Drawing.Size(185, 24)
            Me.LayoutControlItem7.Text = "Past Due Months"
            Me.LayoutControlItem7.TextSize = New System.Drawing.Size(83, 13)
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Control = Me.CalcEdit_past_due_amount
            Me.LayoutControlItem5.CustomizationFormText = "Past Due Amount"
            Me.LayoutControlItem5.Location = New System.Drawing.Point(199, 72)
            Me.LayoutControlItem5.Name = "LayoutControlItem5"
            Me.LayoutControlItem5.Size = New System.Drawing.Size(185, 24)
            Me.LayoutControlItem5.Text = "Past Due Amount"
            Me.LayoutControlItem5.TextSize = New System.Drawing.Size(83, 13)
            '
            'LayoutControlGroup3
            '
            Me.LayoutControlGroup3.CustomizationFormText = "Lender Information"
            Me.LayoutControlGroup3.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem11, Me.LayoutControlItem10})
            Me.LayoutControlGroup3.Location = New System.Drawing.Point(0, 151)
            Me.LayoutControlGroup3.Name = "LayoutControlGroup3"
            Me.LayoutControlGroup3.Size = New System.Drawing.Size(408, 92)
            Me.LayoutControlGroup3.Text = "Lender Information"
            '
            'LayoutControlItem11
            '
            Me.LayoutControlItem11.Control = Me.TextEdit_account_number
            Me.LayoutControlItem11.CustomizationFormText = "Account Number"
            Me.LayoutControlItem11.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem11.Name = "LayoutControlItem11"
            Me.LayoutControlItem11.Size = New System.Drawing.Size(384, 24)
            Me.LayoutControlItem11.Text = "Account Number"
            Me.LayoutControlItem11.TextSize = New System.Drawing.Size(83, 13)
            '
            'LayoutControlItem10
            '
            Me.LayoutControlItem10.Control = Me.TextEdit_lender
            Me.LayoutControlItem10.CustomizationFormText = "Current Lender"
            Me.LayoutControlItem10.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem10.Name = "LayoutControlItem10"
            Me.LayoutControlItem10.Size = New System.Drawing.Size(384, 24)
            Me.LayoutControlItem10.Text = "Current Lender"
            Me.LayoutControlItem10.TextSize = New System.Drawing.Size(83, 13)
            '
            'EmptySpaceItem6
            '
            Me.EmptySpaceItem6.AllowHotTrack = False
            Me.EmptySpaceItem6.CustomizationFormText = "EmptySpaceItem6"
            Me.EmptySpaceItem6.Location = New System.Drawing.Point(0, 140)
            Me.EmptySpaceItem6.Name = "EmptySpaceItem6"
            Me.EmptySpaceItem6.Size = New System.Drawing.Size(408, 11)
            Me.EmptySpaceItem6.Text = "EmptySpaceItem6"
            Me.EmptySpaceItem6.TextSize = New System.Drawing.Size(0, 0)
            '
            'Form_SecuredLoan
            '
            Me.AcceptButton = Me.SimpleButton_ok
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.CancelButton = Me.SimpleButton_cancel
            Me.ClientSize = New System.Drawing.Size(428, 302)
            Me.Controls.Add(Me.LayoutControl1)
            Me.Name = "Form_SecuredLoan"
            Me.Text = "Loans Against Secured Property"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.PercentEdit_interest_rate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_Payment.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_account_number.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_lender.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.SpinEdit_past_due_periods.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_past_due_amount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.SpinEdit_periods.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_balance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_original_amount.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem6, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents CalcEdit_past_due_amount As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents CalcEdit_balance As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents CalcEdit_original_amount As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents SpinEdit_past_due_periods As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents SpinEdit_periods As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents SimpleButton_ok As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents SimpleButton_cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem3 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents CalcEdit_Payment As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents TextEdit_account_number As DevExpress.XtraEditors.TextEdit
        Friend WithEvents TextEdit_lender As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlGroup3 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem6 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents PercentEdit_interest_rate As DebtPlus.Data.Controls.PercentEdit
        Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
    End Class
End Namespace