
Namespace forms.SecuredProperty

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class SecuredLoansControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then
                        components.Dispose()
                    End If
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.GridColumn_lender = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_lender.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_account_number = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_account_number.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_interest_rate = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_interest_rate.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_balance = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_balance.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_payment = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_payment.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridControl1
            '
            Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.GridControl1.EmbeddedNavigator.Name = ""
            Me.GridControl1.Location = New System.Drawing.Point(0, 0)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(463, 317)
            Me.GridControl1.TabIndex = 0
            '
            'GridView1
            '
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_lender, Me.GridColumn_account_number, Me.GridColumn_interest_rate, Me.GridColumn_balance, Me.GridColumn_payment})
            Me.GridView1.OptionsView.ShowFooter = True
            '
            'GridColumn_lender
            '
            Me.GridColumn_lender.Caption = "Lender"
            Me.GridColumn_lender.CustomizationCaption = "Lender Name"
            Me.GridColumn_lender.FieldName = "lender"
            Me.GridColumn_lender.Name = "GridColumn_lender"
            Me.GridColumn_lender.Visible = True
            Me.GridColumn_lender.VisibleIndex = 0
            '
            'GridColumn_account_number
            '
            Me.GridColumn_account_number.Caption = "Account"
            Me.GridColumn_account_number.CustomizationCaption = "Account Number"
            Me.GridColumn_account_number.FieldName = "account_number"
            Me.GridColumn_account_number.Name = "GridColumn_account_number"
            Me.GridColumn_account_number.Visible = False
            Me.GridColumn_account_number.VisibleIndex = -1
            '
            'GridColumn_interest_rate
            '
            Me.GridColumn_interest_rate.Caption = "Rate"
            Me.GridColumn_interest_rate.CustomizationCaption = "Interest Rate"
            Me.GridColumn_interest_rate.DisplayFormat.FormatString = "{0:p}"
            Me.GridColumn_interest_rate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_interest_rate.FieldName = "interest_rate"
            Me.GridColumn_interest_rate.GroupFormat.FormatString = "{0:p}"
            Me.GridColumn_interest_rate.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_interest_rate.Name = "GridColumn_interest_rate"
            Me.GridColumn_interest_rate.Visible = False
            Me.GridColumn_interest_rate.VisibleIndex = -1
            '
            'GridColumn_balance
            '
            Me.GridColumn_balance.Caption = "Balance"
            Me.GridColumn_balance.CustomizationCaption = "Current Balance"
            Me.GridColumn_balance.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_balance.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_balance.FieldName = "balance"
            Me.GridColumn_balance.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_balance.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_balance.Name = "GridColumn_balance"
            Me.GridColumn_balance.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_balance.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_balance.Visible = True
            Me.GridColumn_balance.VisibleIndex = 1
            '
            'GridColumn_payment
            '
            Me.GridColumn_payment.Caption = "Payment"
            Me.GridColumn_payment.CustomizationCaption = "Monthly Payment"
            Me.GridColumn_payment.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_payment.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_payment.FieldName = "payment"
            Me.GridColumn_payment.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_payment.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_payment.Name = "GridColumn_payment"
            Me.GridColumn_payment.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_payment.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_payment.Visible = True
            Me.GridColumn_payment.VisibleIndex = 2
            '
            Me.GridColumn_lender.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_account_number.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_interest_rate.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_balance.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_payment.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            '
            'SecuredLoansControl
            '
            Me.Name = "SecuredLoansControl"
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents GridColumn_lender As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_account_number As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_interest_rate As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_balance As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_payment As DevExpress.XtraGrid.Columns.GridColumn

    End Class
End Namespace
