#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.LINQ
Imports DebtPlus.Utils
Imports DebtPlus.UI.Client.Service
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraEditors
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo
Imports System.ComponentModel
Imports System.Data.SqlClient
Imports System.Linq

Namespace forms.Employer
    Friend Class EmployerList
        Inherits DebtPlus.Data.Controls.UserControl

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Public Event EmployerChanged As EventHandler

        ' Current list of employers displayed
        Private colRecords As System.Collections.Generic.List(Of DebtPlus.LINQ.employer) = Nothing
        Private bc As BusinessContext = Nothing
        Private colRecordsKey As Char = "*"c

        ''' <summary>
        ''' The employer changed. Trip the event.
        ''' </summary>
        Protected Overridable Sub OnEmployerChanged(ByVal e As EventArgs)
            RaiseEvent EmployerChanged(Me, e)
        End Sub

        ''' <summary>
        ''' Protected function to trip the employer change event.
        ''' </summary>
        Protected Sub PerformEmployerChanged(ByVal e As EventArgs)
            OnEmployerChanged(e)
        End Sub

        Public Event ItemSelected As EventHandler
        Public Event Cancelled As EventHandler
        Private ControlRow As Int32 = -1

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

#Region " Windows Form Designer generated code "

        'UserControl overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then components.Dispose()
                    If bc IsNot Nothing Then bc.Dispose()
                    If colRecords IsNot Nothing Then colRecords.Clear()
                End If
                components = Nothing
                bc = Nothing
                colRecords = Nothing
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer = Nothing

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.
        'Do not modify it using the code editor.
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents GridColumn_Employer As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Name As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Address1 As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Address2 As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Address3 As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_City As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_State As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_PostalCode As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Address As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents Button_Select As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents TextEditFilter As DevExpress.XtraEditors.TextEdit
        Friend WithEvents Button_New As DevExpress.XtraEditors.SimpleButton

        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
            Me.GridColumn_Employer = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_Name = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_Address1 = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_Address2 = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_Address3 = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_City = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_State = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_PostalCode = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_Address = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
            Me.Button_Select = New DevExpress.XtraEditors.SimpleButton()
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton()
            Me.Button_New = New DevExpress.XtraEditors.SimpleButton()
            Me.TextEditFilter = New DevExpress.XtraEditors.TextEdit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEditFilter.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridControl1
            '
            Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GridControl1.Location = New System.Drawing.Point(0, 0)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(424, 224)
            Me.GridControl1.TabIndex = 0
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_Employer, Me.GridColumn_Name, Me.GridColumn_Address1, Me.GridColumn_Address2, Me.GridColumn_Address3, Me.GridColumn_City, Me.GridColumn_State, Me.GridColumn_PostalCode, Me.GridColumn_Address})
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsView.AutoCalcPreviewLineCount = True
            Me.GridView1.OptionsView.EnableAppearanceEvenRow = True
            Me.GridView1.OptionsView.EnableAppearanceOddRow = True
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            Me.GridView1.OptionsView.ShowPreview = True
            Me.GridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
            Me.GridView1.PreviewFieldName = "address"
            Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn_Name, DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.GridView1.OptionsSelection.EnableAppearanceFocusedRow = False
            Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = True
            '
            'GridColumn_Employer
            '
            Me.GridColumn_Employer.Caption = "ID"
            Me.GridColumn_Employer.CustomizationCaption = "Employer ID"
            Me.GridColumn_Employer.Name = "GridColumn_Employer"
            Me.GridColumn_Employer.FieldName = "Id"
            Me.GridColumn_Employer.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_Name
            '
            Me.GridColumn_Name.Caption = "Name"
            Me.GridColumn_Name.CustomizationCaption = "Employer Name"
            Me.GridColumn_Name.FieldName = "name"
            Me.GridColumn_Name.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Alphabetical
            Me.GridColumn_Name.Name = "GridColumn_Name"
            Me.GridColumn_Name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_Name.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value
            Me.GridColumn_Name.Visible = True
            Me.GridColumn_Name.VisibleIndex = 0
            '
            'GridColumn_Address1
            '
            Me.GridColumn_Address1.Caption = "Address Line 1"
            Me.GridColumn_Address1.CustomizationCaption = "Address: Line 1"
            Me.GridColumn_Address1.FieldName = "address.AddressLine1"
            Me.GridColumn_Address1.Name = "GridColumn_Address1"
            Me.GridColumn_Address1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_Address2
            '
            Me.GridColumn_Address2.Caption = "Address Line 2"
            Me.GridColumn_Address2.CustomizationCaption = "Address: Line 2"
            Me.GridColumn_Address2.FieldName = "address.address_line_2"
            Me.GridColumn_Address2.Name = "GridColumn_Address2"
            Me.GridColumn_Address2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_Address3
            '
            Me.GridColumn_Address3.Caption = "Address Line 3"
            Me.GridColumn_Address3.CustomizationCaption = "Address: Line 3"
            Me.GridColumn_Address3.FieldName = "address.address_line_3"
            Me.GridColumn_Address3.Name = "GridColumn_Address3"
            Me.GridColumn_Address3.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_City
            '
            Me.GridColumn_City.Caption = "City"
            Me.GridColumn_City.CustomizationCaption = "Address: City"
            Me.GridColumn_City.FieldName = "address.city"
            Me.GridColumn_City.Name = "GridColumn_City"
            Me.GridColumn_City.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_State
            '
            Me.GridColumn_State.Caption = "State"
            Me.GridColumn_State.CustomizationCaption = "Address: State"
            Me.GridColumn_State.FieldName = "address.StateProvince"
            Me.GridColumn_State.Name = "GridColumn_State"
            Me.GridColumn_State.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_PostalCode
            '
            Me.GridColumn_PostalCode.Caption = "Postal Code"
            Me.GridColumn_PostalCode.CustomizationCaption = "Address: Postal Code"
            Me.GridColumn_PostalCode.FieldName = "address.PostalCode"
            Me.GridColumn_PostalCode.Name = "GridColumn_PostalCode"
            Me.GridColumn_PostalCode.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_Address
            '
            Me.GridColumn_Address.Caption = "Address"
            Me.GridColumn_Address.CustomizationCaption = "All Address Lines"
            Me.GridColumn_Address.FieldName = "address"
            Me.GridColumn_Address.Name = "GridColumn_Address"
            Me.GridColumn_Address.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'LabelControl1
            '
            Me.LabelControl1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl1.Location = New System.Drawing.Point(16, 244)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(28, 13)
            Me.LabelControl1.TabIndex = 2
            Me.LabelControl1.Text = "&Filter:"
            '
            'Button_Select
            '
            Me.Button_Select.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_Select.Location = New System.Drawing.Point(144, 240)
            Me.Button_Select.Name = "Button_Select"
            Me.Button_Select.Size = New System.Drawing.Size(75, 23)
            Me.Button_Select.TabIndex = 3
            Me.Button_Select.Text = "&Select"
            '
            'Button_Cancel
            '
            Me.Button_Cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_Cancel.Location = New System.Drawing.Point(232, 240)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.TabIndex = 4
            Me.Button_Cancel.Text = "&Cancel"
            '
            'Button_New
            '
            Me.Button_New.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.Button_New.Location = New System.Drawing.Point(320, 240)
            Me.Button_New.Name = "Button_New"
            Me.Button_New.Size = New System.Drawing.Size(75, 23)
            Me.Button_New.TabIndex = 5
            Me.Button_New.Text = "&New..."
            '
            'TextEditFilter
            '
            Me.TextEditFilter.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.TextEditFilter.Location = New System.Drawing.Point(50, 240)
            Me.TextEditFilter.Name = "TextEditFilter"
            Me.TextEditFilter.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.TextEditFilter.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.TextEditFilter.Properties.Appearance.Options.UseFont = True
            Me.TextEditFilter.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.TextEditFilter.Size = New System.Drawing.Size(78, 20)
            Me.TextEditFilter.TabIndex = 1
            '
            'EmployerList
            '
            Me.Controls.Add(Me.Button_New)
            Me.Controls.Add(Me.Button_Cancel)
            Me.Controls.Add(Me.Button_Select)
            Me.Controls.Add(Me.LabelControl1)
            Me.Controls.Add(Me.GridControl1)
            Me.Controls.Add(Me.TextEditFilter)
            Me.Name = "EmployerList"
            Me.Size = New System.Drawing.Size(424, 280)
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEditFilter.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

#End Region

        Private Sub RegisterEventHandlers()
            AddHandler Button_Cancel.Click, AddressOf Button_Cancel_Click
            AddHandler Button_New.Click, AddressOf Button_New_Click
            AddHandler Button_Select.Click, AddressOf Button_Select_Click
            AddHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
            AddHandler GridView1.FocusedRowChanged, AddressOf GridView1_FocusedRowChanged
            AddHandler GridView1.MouseDown, AddressOf GridView1_MouseDown
            AddHandler TextEditFilter.EditValueChanging, AddressOf TextEditFilter_EditValueChanging
        End Sub

        Private Sub UnRegisterEventHandlers()
            RemoveHandler Button_Cancel.Click, AddressOf Button_Cancel_Click
            RemoveHandler Button_New.Click, AddressOf Button_New_Click
            RemoveHandler Button_Select.Click, AddressOf Button_Select_Click
            RemoveHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
            RemoveHandler GridView1.FocusedRowChanged, AddressOf GridView1_FocusedRowChanged
            RemoveHandler GridView1.MouseDown, AddressOf GridView1_MouseDown
            RemoveHandler TextEditFilter.EditValueChanging, AddressOf TextEditFilter_EditValueChanging
        End Sub

        ''' <summary>
        ''' Process the load event on the control
        ''' </summary>
        Public Sub LoadForm(ByVal EmployerID As System.Nullable(Of Int32))
            Me.EmployerID = EmployerID

            ' Create a connection context to pre-load the address references
            If bc Is Nothing Then
                bc = New BusinessContext()
                Dim dl As New System.Data.Linq.DataLoadOptions()
                dl.LoadWith(Of DebtPlus.LINQ.employer)(Function(s) s.address)
                bc.LoadOptions = dl
            End If

            UnRegisterEventHandlers()
            Try
                Button_Select.Enabled = EmployerID.HasValue
                If Not EmployerID.HasValue Then
                    Return
                End If

                ' Mark the current employer in the list
                Dim StyleFormatCondition1 As StyleFormatCondition = New StyleFormatCondition() With {.ApplyToRow = True, .Column = GridColumn_Employer, .Expression = String.Format("[Id]={0:f0}", EmployerID.Value), .Condition = FormatConditionEnum.Expression}

                ' Set the appearance criteria for the row.
                StyleFormatCondition1.Appearance.BackColor = Color.Navy
                StyleFormatCondition1.Appearance.Font = New Font("Tahoma", 8.25!, FontStyle.Bold)
                StyleFormatCondition1.Appearance.ForeColor = Color.White
                StyleFormatCondition1.Appearance.Options.UseBackColor = True
                StyleFormatCondition1.Appearance.Options.UseFont = True
                StyleFormatCondition1.Appearance.Options.UseForeColor = True

                ' Bind this to the grid control
                GridView1.FormatConditions.Add(StyleFormatCondition1)

                ' Find the current employer from the database. We don't want all just now.
                Dim employerRecord As DebtPlus.LINQ.employer = bc.employers.Where(Function(s) s.Id = EmployerID.Value).FirstOrDefault()
                If employerRecord Is Nothing OrElse String.IsNullOrEmpty(employerRecord.name) Then
                    Return
                End If

                ' Set the filtering text buffer from the name and load the grid with similar names
                TextEditFilter.Text = employerRecord.name(0)
                ReadGridView1(TextEditFilter.Text)

                ' Find the employer in the list once it has been read. Try to scroll the grid to that location.
                Dim rowhandle As Int32 = GridView1.LocateByValue("Id", employerRecord.Id)
                If rowhandle >= 0 Then
                    GridView1.MakeRowVisible(rowhandle, True)
                    GridView1.FocusedRowHandle = rowhandle
                    GridView1.MakeRowVisible(rowhandle)
                End If

            Finally
                RegisterEventHandlers()
            End Try
        End Sub

        Private _Employer As System.Nullable(Of Int32) = Nothing

        ''' <summary>
        ''' The current employer ID
        ''' </summary>
        <System.ComponentModel.DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), System.ComponentModel.Browsable(False)> _
        Public Property EmployerID() As System.Nullable(Of Int32)
            Get
                Return _Employer
            End Get
            Set(ByVal Value As System.Nullable(Of Int32))
                If Not _Employer.Equals(Value) Then
                    _Employer = Value
                    PerformEmployerChanged(EventArgs.Empty)
                End If
            End Set
        End Property

        ''' <summary>
        ''' Process the SELECT button click
        ''' </summary>
        Private Sub Button_Select_Click(ByVal sender As Object, ByVal e As EventArgs)
            RaiseEvent ItemSelected(Me, EventArgs.Empty)
        End Sub

        ''' <summary>
        ''' Process the CANCEL button click
        ''' </summary>
        Private Sub Button_Cancel_Click(ByVal sender As Object, ByVal e As EventArgs)
            RaiseEvent Cancelled(Me, EventArgs.Empty)
        End Sub

        ''' <summary>
        ''' Process the NEW button click
        ''' </summary>
        Private Sub Button_New_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim record As DebtPlus.LINQ.employer = DebtPlus.LINQ.Factory.Manufacture_employer()
            Using frm As New NewForm(record)
                If frm.ShowDialog() <> DialogResult.OK Then
                    Return
                End If
            End Using

            bc.employers.InsertOnSubmit(record)
            bc.SubmitChanges()
            EmployerID = record.Id
            RaiseEvent ItemSelected(Me, EventArgs.Empty)
        End Sub

        ''' <summary>
        ''' Update the employer when the grid control moves the focus
        ''' </summary>
        Private Sub GridView1_FocusedRowChanged(ByVal sender As Object, ByVal e As FocusedRowChangedEventArgs)
            ControlRow = e.FocusedRowHandle

            ' Find the record from the selection
            If ControlRow >= 0 Then
                Dim record As DebtPlus.LINQ.employer = CType(GridView1.GetRow(ControlRow), DebtPlus.LINQ.employer)
                If record IsNot Nothing Then
                    EmployerID = record.Id
                End If
            End If

            ' Ask the user to do something with the selected row
            Button_Select.Enabled = EmployerID.HasValue
        End Sub

        ''' <summary>
        ''' Double Click on an item -- select it
        ''' </summary>
        Private Sub GridView1_DoubleClick(ByVal sender As Object, ByVal e As EventArgs)

            ' Make sure that the hit is in the valid region
            Dim hi As GridHitInfo = GridView1.CalcHitInfo((GridControl1.PointToClient(MousePosition)))
            If Not hi.IsValid OrElse Not hi.InRow Then
                Return
            End If

            ControlRow = hi.RowHandle
            If ControlRow < 0 Then
                Return
            End If

            Dim record As DebtPlus.LINQ.employer = TryCast(GridView1.GetRow(ControlRow), DebtPlus.LINQ.employer)
            If record Is Nothing Then
                Return
            End If

            EmployerID = record.Id
            If EmployerID.HasValue Then
                RaiseEvent ItemSelected(Me, EventArgs.Empty)
            End If
        End Sub

        ''' <summary>
        ''' Process a click event on the row in the grid
        ''' </summary>
        Private Sub GridView1_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs)
            Dim hi As GridHitInfo = GridView1.CalcHitInfo(New Point(e.X, e.Y))
            If Not hi.IsValid OrElse Not hi.InRow Then
                Return
            End If

            ControlRow = hi.RowHandle
            If ControlRow < 0 Then
                Return
            End If

            Dim record As DebtPlus.LINQ.employer = TryCast(GridView1.GetRow(ControlRow), DebtPlus.LINQ.employer)
            If record Is Nothing Then
                Return
            End If

            EmployerID = record.Id
            Button_Select.Enabled = EmployerID.HasValue
        End Sub

        Private Sub TextEditFilter_EditValueChanging(sender As Object, e As ChangingEventArgs)

            ' Leading spaces are not allowed
            If (If(DebtPlus.Utils.Nulls.v_String(e.NewValue), " ") = " ") AndAlso e.OldValue Is Nothing Then
                e.Cancel = True
                Return
            End If

            ' Process the new selection
            ReadGridView1(DebtPlus.Utils.Nulls.v_String(e.NewValue))
        End Sub

        ''' <summary>
        ''' Update the grid to show the items that reflect the name field.
        ''' </summary>
        ''' <param name="NewValue">Employer name to be displayed</param>
        ''' <remarks></remarks>
        Private Sub ReadGridView1(ByVal NewValue As String)

            ' If the key is empty then empty the list
            If String.IsNullOrWhiteSpace(NewValue) OrElse NewValue.Length = 0 Then
                GridControl1.DataSource = Nothing
                GridControl1.RefreshDataSource()
                Return
            End If

            ' Find the key code for the database search.
            Dim newKey As Char = NewValue(0)
            If Not System.Char.IsLetter(newKey) Then
                newKey = "?"c
            End If

            ' Generate the proper filter for just the entered name
            GridView1.BeginUpdate()
            Try
                ' Read the collection of items from the database if they are different
                If newKey <> colRecordsKey Then
                    colRecordsKey = newKey
                    colRecords = bc.employers.Where(Function(s) s.SelectionKey.Value = newKey).ToList()
                    GridControl1.DataSource = colRecords
                    GridControl1.RefreshDataSource()
                    GridView1.RefreshData()
                End If

                ' We need to escape special characters in the name so that the search will be proper
                Dim scrub As New System.Text.StringBuilder(NewValue)
                scrub.Replace("[", "[[]") ' This must be done first!!
                scrub.Replace("_", "[_]")
                scrub.Replace("%", "[%]")
                scrub.Append("%") ' This must be done last!!

                GridView1.ActiveFilter.Clear()
                GridView1.ActiveFilterCriteria = New DevExpress.Data.Filtering.BinaryOperator("name", scrub.ToString(), DevExpress.Data.Filtering.BinaryOperatorType.Like)

            Catch ex As DevExpress.Data.Filtering.Exceptions.InvalidPropertyPathException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)

            Finally
                GridView1.EndUpdate()
            End Try
        End Sub
    End Class
End Namespace