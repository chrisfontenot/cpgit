#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.Data.Controls
Imports DebtPlus.Data.Forms
Imports DebtPlus.UI.Client.Service
Imports DevExpress.XtraLayout
Imports System.Text
Imports DebtPlus.LINQ
Imports System.Linq

Namespace forms.Employer
    Public Class NewForm
        Inherits DebtPlusForm

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Private record As DebtPlus.LINQ.employer = Nothing

        Friend WithEvents LayoutControl1 As LayoutControl
        Friend WithEvents TelephoneNumberRecordControl_FAXID As DebtPlus.Data.Controls.TelephoneNumberRecordControl
        Friend WithEvents TelephoneNumberRecordControl_TelephoneID As DebtPlus.Data.Controls.TelephoneNumberRecordControl
        Friend WithEvents AddressRecordControl_AddressID As AddressRecordControl
        Friend WithEvents LayoutControlGroup1 As LayoutControlGroup
        Friend WithEvents LayoutControlItem2 As LayoutControlItem
        Friend WithEvents LayoutControlItem1 As LayoutControlItem
        Friend WithEvents LayoutControlItem3 As LayoutControlItem
        Friend WithEvents LayoutControlItem4 As LayoutControlItem
        Friend WithEvents LayoutControlItem5 As LayoutControlItem
        Friend WithEvents LayoutControlItem6 As LayoutControlItem
        Friend WithEvents LayoutControlItem7 As LayoutControlItem
        Friend WithEvents EmptySpaceItem1 As EmptySpaceItem
        Friend WithEvents EmptySpaceItem3 As EmptySpaceItem
        Friend WithEvents EmptySpaceItem4 As EmptySpaceItem

        ''' <summary>
        ''' Initialize the form
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub New(ByVal record As DebtPlus.LINQ.employer)
            MyClass.New()
            Me.record = record

            ' Load the lookup controls with the proper datasets.
            industry.Properties.DataSource = DebtPlus.LINQ.Cache.industry.getList()

            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Load, AddressOf NewEmployerForm_Load
            AddHandler industry.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler employer_name.EditValueChanging, AddressOf employer_name_EditValueChanging
            AddHandler Button_OK.Click, AddressOf Button_OK_Click
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler Load, AddressOf NewEmployerForm_Load
            RemoveHandler industry.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler employer_name.EditValueChanging, AddressOf employer_name_EditValueChanging
            RemoveHandler Button_OK.Click, AddressOf Button_OK_Click
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.
        'Do not modify it using the code editor.
        Friend WithEvents employer_name As DevExpress.XtraEditors.TextEdit
        Friend WithEvents industry As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents Button_OK As DevExpress.XtraEditors.SimpleButton
        Friend WithEvents Button_Cancel As DevExpress.XtraEditors.SimpleButton

        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim SuperToolTip1 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
            Dim ToolTipTitleItem1 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
            Dim ToolTipItem1 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
            Me.employer_name = New DevExpress.XtraEditors.TextEdit()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.TelephoneNumberRecordControl_FAXID = New DebtPlus.Data.Controls.TelephoneNumberRecordControl()
            Me.TelephoneNumberRecordControl_TelephoneID = New DebtPlus.Data.Controls.TelephoneNumberRecordControl()
            Me.AddressRecordControl_AddressID = New DebtPlus.Data.Controls.AddressRecordControl()
            Me.industry = New DevExpress.XtraEditors.LookUpEdit()
            Me.Button_OK = New DevExpress.XtraEditors.SimpleButton()
            Me.Button_Cancel = New DevExpress.XtraEditors.SimpleButton()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem4 = New DevExpress.XtraLayout.EmptySpaceItem()
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.employer_name.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.TelephoneNumberRecordControl_FAXID, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TelephoneNumberRecordControl_TelephoneID, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.AddressRecordControl_AddressID, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.industry.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'employer_name
            '
            Me.employer_name.Location = New System.Drawing.Point(57, 12)
            Me.employer_name.Name = "employer_name"
            Me.employer_name.Properties.MaxLength = 50
            Me.employer_name.Size = New System.Drawing.Size(411, 20)
            Me.employer_name.StyleController = Me.LayoutControl1
            ToolTipTitleItem1.Text = "Required Field"
            ToolTipItem1.LeftIndent = 6
            ToolTipItem1.Text = "The name of the employer"
            SuperToolTip1.Items.Add(ToolTipTitleItem1)
            SuperToolTip1.Items.Add(ToolTipItem1)
            Me.employer_name.SuperTip = SuperToolTip1
            Me.employer_name.TabIndex = 1
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.TelephoneNumberRecordControl_FAXID)
            Me.LayoutControl1.Controls.Add(Me.TelephoneNumberRecordControl_TelephoneID)
            Me.LayoutControl1.Controls.Add(Me.AddressRecordControl_AddressID)
            Me.LayoutControl1.Controls.Add(Me.employer_name)
            Me.LayoutControl1.Controls.Add(Me.industry)
            Me.LayoutControl1.Controls.Add(Me.Button_OK)
            Me.LayoutControl1.Controls.Add(Me.Button_Cancel)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(480, 219)
            Me.LayoutControl1.TabIndex = 12
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'TelephoneNumberRecordControl_FAXID
            '
            Me.TelephoneNumberRecordControl_FAXID.Location = New System.Drawing.Point(261, 112)
            Me.TelephoneNumberRecordControl_FAXID.Margin = New System.Windows.Forms.Padding(0)
            Me.TelephoneNumberRecordControl_FAXID.Name = "TelephoneNumberRecordControl_FAXID"
            Me.TelephoneNumberRecordControl_FAXID.Size = New System.Drawing.Size(207, 20)
            Me.TelephoneNumberRecordControl_FAXID.TabIndex = 6
            Me.ToolTipController1.SetToolTip(Me.TelephoneNumberRecordControl_FAXID,
                                             "If you have a FAX number, enter it here")
            '
            'TelephoneNumberRecordControl_TelephoneID
            '
            Me.TelephoneNumberRecordControl_TelephoneID.Location = New System.Drawing.Point(57, 112)
            Me.TelephoneNumberRecordControl_TelephoneID.Margin = New System.Windows.Forms.Padding(0)
            Me.TelephoneNumberRecordControl_TelephoneID.Name = "TelephoneNumberRecordControl_TelephoneID"
            Me.TelephoneNumberRecordControl_TelephoneID.Size = New System.Drawing.Size(155, 20)
            Me.TelephoneNumberRecordControl_TelephoneID.TabIndex = 5
            Me.ToolTipController1.SetToolTip(Me.TelephoneNumberRecordControl_TelephoneID,
                                             "If you have a telephone number, enter it here")
            '
            'AddressRecordControl_AddressID
            '
            Me.AddressRecordControl_AddressID.Location = New System.Drawing.Point(57, 36)
            Me.AddressRecordControl_AddressID.Name = "AddressRecordControl_AddressID"
            Me.AddressRecordControl_AddressID.Size = New System.Drawing.Size(411, 72)
            Me.AddressRecordControl_AddressID.TabIndex = 4
            '
            'industry
            '
            Me.industry.Location = New System.Drawing.Point(57, 136)
            Me.industry.Name = "industry"
            Me.industry.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.industry.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Industry", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.industry.Properties.DisplayMember = "description"
            Me.industry.Properties.NullText = "...Please Choose One ..."
            Me.industry.Properties.ShowFooter = False
            Me.industry.Properties.ShowHeader = False
            Me.industry.Properties.ShowLines = False
            Me.industry.Properties.ValueMember = "Id"
            Me.industry.Size = New System.Drawing.Size(411, 20)
            Me.industry.StyleController = Me.LayoutControl1
            Me.industry.TabIndex = 9
            Me.industry.ToolTip = "It is helpful if you would please choose an apporiate industry for this employer." &
                                  ""
            Me.industry.Properties.SortColumnIndex = 1
            '
            'Button_OK
            '
            Me.Button_OK.Enabled = False
            Me.Button_OK.Location = New System.Drawing.Point(157, 184)
            Me.Button_OK.MaximumSize = New System.Drawing.Size(75, 23)
            Me.Button_OK.MinimumSize = New System.Drawing.Size(75, 23)
            Me.Button_OK.Name = "Button_OK"
            Me.Button_OK.Size = New System.Drawing.Size(75, 23)
            Me.Button_OK.StyleController = Me.LayoutControl1
            Me.Button_OK.TabIndex = 10
            Me.Button_OK.Text = "&OK"
            '
            'Button_Cancel
            '
            Me.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
            Me.Button_Cancel.Location = New System.Drawing.Point(236, 184)
            Me.Button_Cancel.MaximumSize = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.MinimumSize = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.Name = "Button_Cancel"
            Me.Button_Cancel.Size = New System.Drawing.Size(75, 23)
            Me.Button_Cancel.StyleController = Me.LayoutControl1
            Me.Button_Cancel.TabIndex = 11
            Me.Button_Cancel.Text = "&Cancel"
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup1.GroupBordersVisible = False
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() _
                                                     {Me.LayoutControlItem2, Me.LayoutControlItem1,
                                                      Me.LayoutControlItem3, Me.LayoutControlItem4,
                                                      Me.LayoutControlItem6, Me.LayoutControlItem7,
                                                      Me.LayoutControlItem5, Me.EmptySpaceItem1, Me.EmptySpaceItem3,
                                                      Me.EmptySpaceItem4})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(480, 219)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = True
            Me.LayoutControlItem2.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LayoutControlItem2.Control = Me.AddressRecordControl_AddressID
            Me.LayoutControlItem2.CustomizationFormText = "Address"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(460, 76)
            Me.LayoutControlItem2.Text = "Address"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(41, 13)
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.employer_name
            Me.LayoutControlItem1.CustomizationFormText = "Name"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(460, 24)
            Me.LayoutControlItem1.Text = "Name"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(41, 13)
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.TelephoneNumberRecordControl_TelephoneID
            Me.LayoutControlItem3.CustomizationFormText = "Phone"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 100)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(204, 24)
            Me.LayoutControlItem3.Text = "Phone"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(41, 13)
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.TelephoneNumberRecordControl_FAXID
            Me.LayoutControlItem4.CustomizationFormText = "FAX"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(204, 100)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(256, 24)
            Me.LayoutControlItem4.Text = "FAX"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(41, 13)
            '
            'LayoutControlItem6
            '
            Me.LayoutControlItem6.Control = Me.Button_OK
            Me.LayoutControlItem6.CustomizationFormText = "LayoutControlItem6"
            Me.LayoutControlItem6.Location = New System.Drawing.Point(145, 172)
            Me.LayoutControlItem6.Name = "LayoutControlItem6"
            Me.LayoutControlItem6.Size = New System.Drawing.Size(79, 27)
            Me.LayoutControlItem6.Text = "LayoutControlItem6"
            Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem6.TextToControlDistance = 0
            Me.LayoutControlItem6.TextVisible = False
            '
            'LayoutControlItem7
            '
            Me.LayoutControlItem7.Control = Me.Button_Cancel
            Me.LayoutControlItem7.CustomizationFormText = "LayoutControlItem7"
            Me.LayoutControlItem7.Location = New System.Drawing.Point(224, 172)
            Me.LayoutControlItem7.Name = "LayoutControlItem7"
            Me.LayoutControlItem7.Size = New System.Drawing.Size(79, 27)
            Me.LayoutControlItem7.Text = "LayoutControlItem7"
            Me.LayoutControlItem7.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem7.TextToControlDistance = 0
            Me.LayoutControlItem7.TextVisible = False
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Control = Me.industry
            Me.LayoutControlItem5.CustomizationFormText = "Industry"
            Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 124)
            Me.LayoutControlItem5.Name = "LayoutControlItem5"
            Me.LayoutControlItem5.Size = New System.Drawing.Size(460, 24)
            Me.LayoutControlItem5.Text = "Industry"
            Me.LayoutControlItem5.TextSize = New System.Drawing.Size(41, 13)
            '
            'EmptySpaceItem1
            '
            Me.EmptySpaceItem1.AllowHotTrack = False
            Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 148)
            Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Size = New System.Drawing.Size(460, 24)
            Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
            Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem3
            '
            Me.EmptySpaceItem3.AllowHotTrack = False
            Me.EmptySpaceItem3.CustomizationFormText = "EmptySpaceItem3"
            Me.EmptySpaceItem3.Location = New System.Drawing.Point(0, 172)
            Me.EmptySpaceItem3.Name = "EmptySpaceItem3"
            Me.EmptySpaceItem3.Size = New System.Drawing.Size(145, 27)
            Me.EmptySpaceItem3.Text = "EmptySpaceItem3"
            Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem4
            '
            Me.EmptySpaceItem4.AllowHotTrack = False
            Me.EmptySpaceItem4.CustomizationFormText = "EmptySpaceItem4"
            Me.EmptySpaceItem4.Location = New System.Drawing.Point(303, 172)
            Me.EmptySpaceItem4.Name = "EmptySpaceItem4"
            Me.EmptySpaceItem4.Size = New System.Drawing.Size(157, 27)
            Me.EmptySpaceItem4.Text = "EmptySpaceItem4"
            Me.EmptySpaceItem4.TextSize = New System.Drawing.Size(0, 0)
            '
            'NewForm
            '
            Me.AcceptButton = Me.Button_OK
            Me.CancelButton = Me.Button_Cancel
            Me.ClientSize = New System.Drawing.Size(480, 219)
            Me.Controls.Add(Me.LayoutControl1)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
            Me.MaximizeBox = False
            Me.Name = "NewForm"
            Me.Text = "New Employer Information"
            CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.employer_name.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.TelephoneNumberRecordControl_FAXID, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TelephoneNumberRecordControl_TelephoneID, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.AddressRecordControl_AddressID, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.industry.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
        End Sub

#End Region

        Private Sub NewEmployerForm_Load(ByVal sender As Object, ByVal e As EventArgs)

            UnRegisterHandlers()
            Try
                industry.EditValue = record.industry
                employer_name.EditValue = record.name
                TelephoneNumberRecordControl_TelephoneID.EditValue = record.TelephoneID
                TelephoneNumberRecordControl_FAXID.EditValue = record.FAXID
                AddressRecordControl_AddressID.EditValue = record.AddressID
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Sub Button_OK_Click(ByVal sender As Object, ByVal e As EventArgs)

            record.name = If(DebtPlus.Utils.Nulls.v_String(employer_name.EditValue), String.Empty).Trim()
            record.industry = DebtPlus.Utils.Nulls.v_Int32(industry.EditValue)
            record.TelephoneID = TelephoneNumberRecordControl_TelephoneID.EditValue
            record.FAXID = TelephoneNumberRecordControl_FAXID.EditValue
            record.AddressID = AddressRecordControl_AddressID.EditValue

            DialogResult = DialogResult.OK
        End Sub

        Private Sub employer_name_EditValueChanging(sender As Object, e As ChangingEventArgs)
            Button_OK.Enabled = DebtPlus.Utils.Nulls.DStr(e.NewValue).Trim() <> String.Empty
        End Sub
    End Class
End Namespace