#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.UI.Client.Service
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Text

Namespace forms

    Friend Class Form_CreditorNote
        Implements IContext

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Private ReadOnly CurrentDebtRecord As Object

        ''' <summary>
        ''' Initialize the form
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            AddHandler Load, AddressOf Form_CreditorNote_Load
            AddHandler SimpleButton_OK.Click, AddressOf SimpleButton_OK_Click
        End Sub

        Public Sub New(ByVal Context As ClientUpdateClass)
            MyClass.New()
            privateContext = Context
        End Sub

        Public Sub New(ByVal Context As ClientUpdateClass, ByVal CurrentDebtRecord As Object)
            MyClass.New(Context)
            Me.CurrentDebtRecord = CurrentDebtRecord
        End Sub

        ''' <summary>
        ''' Pointer to the client update information for the current client
        ''' </summary>
        Private ReadOnly privateContext As ClientUpdateClass

        Public ReadOnly Property Context() As ClientUpdateClass Implements IContext.Context
            Get
                Return privateContext
            End Get
        End Property

        ''' <summary>
        ''' Process the FORM LOAD event
        ''' </summary>
        Private Sub Form_CreditorNote_Load(ByVal sender As Object, ByVal e As EventArgs)

            With LookUpEdit_MessageList
                .Properties.DataSource = DebtPlus.LINQ.Cache.disbursement_creditor_note_type.getList()
                AddHandler .EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
                AddHandler .EditValueChanging, AddressOf MessageList_EditValueChanging
            End With

            ' The item is blank for the message
            With MemoEdit_Message
                .EditValue = String.Empty
                AddHandler .EditValueChanging, AddressOf EditValueChanging
            End With

            ' Set the date and amount fields
            With DateEdit_note_date
                .Properties.MinValue = Now.Date
                .EditValue = Now.Date
                AddHandler .EditValueChanging, AddressOf EditValueChanging
            End With

            With CalcEdit_note_amount
                .EditValue = 0D
                AddHandler .EditValueChanging, AddressOf EditValueChanging
            End With

            SimpleButton_OK.Enabled = False
            CheckEdit1.Checked = True
        End Sub

        ''' <summary>
        ''' Handle a change in the list of standard messages
        ''' </summary>
        Private Sub MessageList_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)
            If e.NewValue IsNot Nothing AndAlso e.NewValue IsNot DBNull.Value Then
                MemoEdit_Message.Text = Convert.ToString(e.NewValue)
            End If
        End Sub

        ''' <summary>
        ''' Handle a change in the form contents
        ''' </summary>
        Private Sub EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)

            ' If this is the amount, do not allow it to go negative.
            If sender Is CalcEdit_note_amount Then
                Dim val As Decimal = 0D
                If e.NewValue IsNot Nothing AndAlso e.NewValue IsNot DBNull.Value Then val = Convert.ToDecimal(e.NewValue)
                e.Cancel = (val < 0D)
            End If

            ' Enable the note button if there is a message text to be used
            Dim NoteText As String
            If sender Is MemoEdit_Message Then
                NoteText = If(e.NewValue IsNot Nothing AndAlso e.NewValue IsNot DBNull.Value, Convert.ToString(e.NewValue), String.Empty)
            Else
                NoteText = Convert.ToString(MemoEdit_Message.EditValue)
            End If
            SimpleButton_OK.Enabled = (NoteText <> String.Empty)
        End Sub

        ''' <summary>
        ''' Handle the OK button being clicked
        ''' </summary>
        Private Sub SimpleButton_OK_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
            Dim current_cursor As Cursor = Cursor.Current

            Dim MessageText As Object = DBNull.Value
            If MemoEdit_Message.EditValue IsNot Nothing AndAlso MemoEdit_Message IsNot DBNull.Value Then
                Dim sb As New StringBuilder(Convert.ToString(MemoEdit_Message.EditValue))
                sb.Replace(ControlChars.Cr, String.Empty)
                sb.Replace(ControlChars.Lf, " ")
                sb.Replace(ControlChars.Tab, " ")
                sb.Replace(ControlChars.Back, " ")
                MessageText = sb.ToString().Trim()
            End If

            Try
                Cursor.Current = Cursors.WaitCursor
                cn.Open()
                Using cmd As SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn

                        If CurrentDebtRecord Is Nothing OrElse CheckEdit1.Checked Then
                            .CommandText = "INSERT INTO debt_notes (creditor, type, client, client_creditor, note_text, note_amount, note_date) SELECT cc.creditor, 'CN', cc.client, cc.client_creditor, @note_text, @note_amount, @note_date FROM client_creditor cc INNER JOIN client_creditor_balances b ON cc.client_creditor = cc.client_creditor AND b.client_creditor_balance = cc.client_creditor_balance WHERE cc.reassigned_debt = 0 AND cc.client = @client"
                            .Parameters.Add("@client", SqlDbType.Int).Value = Context.ClientId
                        Else
                            .CommandText = "INSERT INTO debt_notes (creditor, type, client, client_creditor, note_text, note_amount, note_date) SELECT cc.creditor, 'CN', cc.client, cc.client_creditor, @note_text, @note_amount, @note_date FROM client_creditor cc WHERE cc.client_creditor = @client_creditor"
                            .Parameters.Add("@client_creditor", SqlDbType.Int).Value = CurrentDebtRecord
                        End If

                        .CommandType = CommandType.Text
                        .Parameters.Add("@note_text", SqlDbType.Text).Value = MessageText
                        .Parameters.Add("@note_date", SqlDbType.DateTime).Value = DateEdit_note_date.EditValue
                        .Parameters.Add("@note_amount", SqlDbType.Decimal).Value = CalcEdit_note_amount.EditValue

                        Dim RecordsInserted As Int32 = .ExecuteNonQuery()
                    End With
                End Using

            Catch ex As SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error inserting into debt_notes table")

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try
        End Sub
    End Class
End Namespace
