﻿Namespace pages
    Partial Class Page_Products

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer = Nothing

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then components.Dispose()
                    If bc IsNot Nothing Then bc.Dispose()
                End If
                bc = Nothing
                components = Nothing
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim GridLevelNode1 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
            Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
            Me.GridColumn_detail_vendor = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_detail_disputed_status = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_detail_disputed_date = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_detail_disputed_note = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_detail_resolution_status = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_detail_resolution_date = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_detail_resolution_note = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_detail_resolved_by = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_Id = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_Vendor = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_VendorName = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_Reference = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_Type = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_Escrow = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_OrigBalance = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_Balance = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.BarButtonItem_Reassign = New DevExpress.XtraBars.BarButtonItem()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PopupMenu_Items, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridControl1
            '
            GridLevelNode1.LevelTemplate = Me.GridView2
            GridLevelNode1.RelationName = "client_products_histories"
            Me.GridControl1.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode1})
            Me.GridControl1.Size = New System.Drawing.Size(506, 296)
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(195, Byte), Integer))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(189, Byte), Integer))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(206, Byte), Integer))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(225, Byte), Integer))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(215, Byte), Integer))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(215, Byte), Integer))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(215, Byte), Integer))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_Id, Me.GridColumn_Vendor, Me.GridColumn_VendorName, Me.GridColumn_Reference, Me.GridColumn_Type, Me.GridColumn_Escrow, Me.GridColumn_OrigBalance, Me.GridColumn_Balance})
            Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridView1.OptionsBehavior.AutoPopulateColumns = False
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsDetail.AllowZoomDetail = False
            Me.GridView1.OptionsDetail.EnableMasterViewMode = False
            Me.GridView1.OptionsDetail.ShowDetailTabs = False
            Me.GridView1.OptionsDetail.SmartDetailExpand = False
            Me.GridView1.OptionsLayout.Columns.StoreAllOptions = True
            Me.GridView1.OptionsLayout.LayoutVersion = "1"
            Me.GridView1.OptionsLayout.StoreAllOptions = True
            Me.GridView1.OptionsLayout.StoreDataSettings = False
            Me.GridView1.OptionsMenu.EnableFooterMenu = False
            Me.GridView1.OptionsMenu.ShowAutoFilterRowItem = False
            Me.GridView1.OptionsView.EnableAppearanceEvenRow = True
            Me.GridView1.OptionsView.EnableAppearanceOddRow = True
            Me.GridView1.OptionsView.ShowDetailButtons = False
            Me.GridView1.OptionsView.ShowFooter = True
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridView1.OptionsView.ShowIndicator = False
            '
            'BarManager1
            '
            Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarButtonItem_Reassign})
            Me.BarManager1.MaxItemId = 12
            '
            'barDockControlTop
            '
            Me.barDockControlTop.Size = New System.Drawing.Size(506, 0)
            '
            'barDockControlBottom
            '
            Me.barDockControlBottom.Size = New System.Drawing.Size(506, 0)
            '
            'barDockControlRight
            '
            Me.barDockControlRight.Location = New System.Drawing.Point(506, 0)
            '
            'PopupMenu_Items
            '
            Me.PopupMenu_Items.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Reassign, True)})
            '
            'BarButtonItem_Change
            '
            Me.BarButtonItem_Change.Enabled = False
            '
            'BarButtonItem_Delete
            '
            Me.BarButtonItem_Delete.Enabled = False
            '
            'GridView2
            '
            Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_detail_vendor, Me.GridColumn_detail_disputed_status, Me.GridColumn_detail_disputed_date, Me.GridColumn_detail_disputed_note, Me.GridColumn_detail_resolution_status, Me.GridColumn_detail_resolution_date, Me.GridColumn_detail_resolution_note, Me.GridColumn_detail_resolved_by})
            Me.GridView2.GridControl = Me.GridControl1
            Me.GridView2.Name = "GridView2"
            Me.GridView2.OptionsBehavior.Editable = False
            Me.GridView2.OptionsDetail.EnableMasterViewMode = False
            Me.GridView2.OptionsDetail.ShowDetailTabs = False
            Me.GridView2.OptionsDetail.SmartDetailExpand = False
            Me.GridView2.OptionsView.EnableAppearanceEvenRow = True
            Me.GridView2.OptionsView.EnableAppearanceOddRow = True
            Me.GridView2.OptionsView.ShowDetailButtons = False
            Me.GridView2.OptionsView.ShowGroupPanel = False
            Me.GridView2.OptionsView.ShowIndicator = False
            '
            'GridColumn_detail_vendor
            '
            Me.GridColumn_detail_vendor.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_detail_vendor.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_detail_vendor.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_detail_vendor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_detail_vendor.Caption = "Vendor"
            Me.GridColumn_detail_vendor.FieldName = "vendor_label"
            Me.GridColumn_detail_vendor.Name = "GridColumn_detail_vendor"
            Me.GridColumn_detail_vendor.Visible = True
            Me.GridColumn_detail_vendor.VisibleIndex = 0
            '
            'GridColumn_detail_disputed_status
            '
            Me.GridColumn_detail_disputed_status.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_detail_disputed_status.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_detail_disputed_status.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_detail_disputed_status.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_detail_disputed_status.Caption = "Disputed"
            Me.GridColumn_detail_disputed_status.FieldName = "disputed_status"
            Me.GridColumn_detail_disputed_status.Name = "GridColumn_detail_disputed_status"
            Me.GridColumn_detail_disputed_status.Visible = True
            Me.GridColumn_detail_disputed_status.VisibleIndex = 1
            '
            'GridColumn_detail_disputed_date
            '
            Me.GridColumn_detail_disputed_date.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_detail_disputed_date.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_detail_disputed_date.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_detail_disputed_date.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_detail_disputed_date.Caption = "Disp Date"
            Me.GridColumn_detail_disputed_date.DisplayFormat.FormatString = "d"
            Me.GridColumn_detail_disputed_date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_detail_disputed_date.FieldName = "disputed_date"
            Me.GridColumn_detail_disputed_date.GroupFormat.FormatString = "d"
            Me.GridColumn_detail_disputed_date.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_detail_disputed_date.Name = "GridColumn_detail_disputed_date"
            Me.GridColumn_detail_disputed_date.Visible = True
            Me.GridColumn_detail_disputed_date.VisibleIndex = 2
            '
            'GridColumn_detail_disputed_note
            '
            Me.GridColumn_detail_disputed_note.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_detail_disputed_note.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_detail_disputed_note.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_detail_disputed_note.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_detail_disputed_note.Caption = "Note"
            Me.GridColumn_detail_disputed_note.FieldName = "disputed_note"
            Me.GridColumn_detail_disputed_note.Name = "GridColumn_detail_disputed_note"
            Me.GridColumn_detail_disputed_note.Visible = True
            Me.GridColumn_detail_disputed_note.VisibleIndex = 3
            '
            'GridColumn_detail_resolution_status
            '
            Me.GridColumn_detail_resolution_status.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_detail_resolution_status.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_detail_resolution_status.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_detail_resolution_status.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_detail_resolution_status.Caption = "Resolution"
            Me.GridColumn_detail_resolution_status.FieldName = "resolution_status"
            Me.GridColumn_detail_resolution_status.Name = "GridColumn_detail_resolution_status"
            Me.GridColumn_detail_resolution_status.Visible = True
            Me.GridColumn_detail_resolution_status.VisibleIndex = 4
            '
            'GridColumn_detail_resolution_date
            '
            Me.GridColumn_detail_resolution_date.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_detail_resolution_date.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_detail_resolution_date.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_detail_resolution_date.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_detail_resolution_date.Caption = "Res Date"
            Me.GridColumn_detail_resolution_date.DisplayFormat.FormatString = "d"
            Me.GridColumn_detail_resolution_date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_detail_resolution_date.FieldName = "resolution_date"
            Me.GridColumn_detail_resolution_date.GroupFormat.FormatString = "d"
            Me.GridColumn_detail_resolution_date.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_detail_resolution_date.Name = "GridColumn_detail_resolution_date"
            Me.GridColumn_detail_resolution_date.Visible = True
            Me.GridColumn_detail_resolution_date.VisibleIndex = 5
            '
            'GridColumn_detail_resolution_note
            '
            Me.GridColumn_detail_resolution_note.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_detail_resolution_note.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_detail_resolution_note.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_detail_resolution_note.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_detail_resolution_note.Caption = "Res Note"
            Me.GridColumn_detail_resolution_note.FieldName = "resolution_note"
            Me.GridColumn_detail_resolution_note.Name = "GridColumn_detail_resolution_note"
            Me.GridColumn_detail_resolution_note.Visible = True
            Me.GridColumn_detail_resolution_note.VisibleIndex = 6
            '
            'GridColumn_detail_resolved_by
            '
            Me.GridColumn_detail_resolved_by.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_detail_resolved_by.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_detail_resolved_by.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_detail_resolved_by.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_detail_resolved_by.Caption = "Resolved By"
            Me.GridColumn_detail_resolved_by.FieldName = "created_by"
            Me.GridColumn_detail_resolved_by.Name = "GridColumn_detail_resolved_by"
            Me.GridColumn_detail_resolved_by.Visible = True
            Me.GridColumn_detail_resolved_by.VisibleIndex = 7
            '
            'GridColumn_Id
            '
            Me.GridColumn_Id.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Id.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Id.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Id.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Id.Caption = "ID"
            Me.GridColumn_Id.FieldName = "Id"
            Me.GridColumn_Id.Name = "GridColumn_Id"
            Me.GridColumn_Id.Width = 47
            '
            'GridColumn_Vendor
            '
            Me.GridColumn_Vendor.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Vendor.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_Vendor.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Vendor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_Vendor.Caption = "Vendor"
            Me.GridColumn_Vendor.FieldName = "vendor1.Label"
            Me.GridColumn_Vendor.Name = "GridColumn_Vendor"
            Me.GridColumn_Vendor.Visible = True
            Me.GridColumn_Vendor.VisibleIndex = 0
            Me.GridColumn_Vendor.Width = 66
            '
            'GridColumn_VendorName
            '
            Me.GridColumn_VendorName.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_VendorName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_VendorName.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_VendorName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_VendorName.Caption = "Name"
            Me.GridColumn_VendorName.FieldName = "vendor1.Name"
            Me.GridColumn_VendorName.Name = "GridColumn_VendorName"
            Me.GridColumn_VendorName.Visible = True
            Me.GridColumn_VendorName.VisibleIndex = 1
            Me.GridColumn_VendorName.Width = 197
            '
            'GridColumn_Reference
            '
            Me.GridColumn_Reference.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Reference.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_Reference.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Reference.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_Reference.Caption = "Reference #"
            Me.GridColumn_Reference.FieldName = "reference"
            Me.GridColumn_Reference.Name = "GridColumn_Reference"
            Me.GridColumn_Reference.Visible = True
            Me.GridColumn_Reference.VisibleIndex = 2
            Me.GridColumn_Reference.Width = 104
            '
            'GridColumn_Type
            '
            Me.GridColumn_Type.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Type.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_Type.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Type.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_Type.Caption = "Type"
            Me.GridColumn_Type.FieldName = "type"
            Me.GridColumn_Type.Name = "GridColumn_Type"
            Me.GridColumn_Type.Visible = True
            Me.GridColumn_Type.VisibleIndex = 3
            Me.GridColumn_Type.Width = 59
            '
            'GridColumn_Escrow
            '
            Me.GridColumn_Escrow.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Escrow.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_Escrow.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Escrow.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_Escrow.Caption = "Escrow"
            Me.GridColumn_Escrow.FieldName = "escrow"
            Me.GridColumn_Escrow.Name = "GridColumn_Escrow"
            Me.GridColumn_Escrow.Visible = True
            Me.GridColumn_Escrow.VisibleIndex = 4
            Me.GridColumn_Escrow.Width = 59
            '
            'GridColumn_OrigBalance
            '
            Me.GridColumn_OrigBalance.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_OrigBalance.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_OrigBalance.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_OrigBalance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_OrigBalance.Caption = "Orig Bal"
            Me.GridColumn_OrigBalance.DisplayFormat.FormatString = "c2"
            Me.GridColumn_OrigBalance.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_OrigBalance.FieldName = "cost"
            Me.GridColumn_OrigBalance.GroupFormat.FormatString = "c2"
            Me.GridColumn_OrigBalance.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_OrigBalance.Name = "GridColumn_OrigBalance"
            Me.GridColumn_OrigBalance.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "cost", "{0:c2}")})
            Me.GridColumn_OrigBalance.Visible = True
            Me.GridColumn_OrigBalance.VisibleIndex = 5
            Me.GridColumn_OrigBalance.Width = 71
            '
            'GridColumn_Balance
            '
            Me.GridColumn_Balance.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Balance.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Balance.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Balance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_Balance.Caption = "Balance"
            Me.GridColumn_Balance.DisplayFormat.FormatString = "c2"
            Me.GridColumn_Balance.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Balance.FieldName = "balance"
            Me.GridColumn_Balance.GroupFormat.FormatString = "c2"
            Me.GridColumn_Balance.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Balance.Name = "GridColumn_Balance"
            Me.GridColumn_Balance.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "balance", "{0:c2}")})
            Me.GridColumn_Balance.Visible = True
            Me.GridColumn_Balance.VisibleIndex = 6
            Me.GridColumn_Balance.Width = 61
            '
            'BarButtonItem_Reassign
            '
            Me.BarButtonItem_Reassign.Caption = "&Reassign..."
            Me.BarButtonItem_Reassign.Id = 11
            Me.BarButtonItem_Reassign.Name = "BarButtonItem_Reassign"
            '
            'Page_Products
            '
            Me.Name = "Page_Products"
            Me.Size = New System.Drawing.Size(506, 296)
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PopupMenu_Items, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents GridColumn_Id As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Vendor As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_VendorName As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Reference As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Type As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Escrow As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_OrigBalance As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Balance As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents BarButtonItem_Reassign As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents GridColumn_detail_vendor As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_detail_disputed_status As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_detail_disputed_date As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_detail_resolution_status As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_detail_resolution_date As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_detail_resolution_note As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_detail_disputed_note As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_detail_resolved_by As DevExpress.XtraGrid.Columns.GridColumn
    End Class
End Namespace
