#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.UI.Client.controls
Imports DevExpress.Utils
Imports DevExpress.XtraGrid
Imports System.Threading
Imports DebtPlus.UI.Client.forms.SecuredProperty
Imports System.Linq
Imports DebtPlus.LINQ

Namespace pages
    Friend Class Page_SecuredProperty
        Inherits MyGridControlClient

        ' Collection of secured_properties for this client
        Dim colProperties As System.Collections.Generic.List(Of DebtPlus.LINQ.secured_property) = Nothing
        Dim bc As BusinessContext

        ''' <summary>
        ''' Create a new instance of our class
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            EnableMenus = True

            ' Bind the formatting logic to the new class
            GridColumn_Type.DisplayFormat.Format = New SecuredTypeFormatter()
            GridColumn_Type.GroupFormat.Format = New SecuredTypeFormatter()
        End Sub

        ''' <summary>
        ''' Process the read operation for the page
        ''' </summary>
        Public Overrides Sub ReadForm(bc As DebtPlus.LINQ.BusinessContext)
            Me.bc = bc

            ' Read the list of secured properties for the client. Each client has a distinct form and each
            ' form has a distinct copy of this page so it is local for the client.
            If colProperties Is Nothing Then
                colProperties = bc.secured_properties.Where(Function(s) s.client = Context.ClientDs.ClientId).ToList()
                GridControl1.DataSource = colProperties
            End If
        End Sub

        ''' <summary>
        ''' Refresh the grid control when the list is updated
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub RefreshGrid()
            If GridControl1.InvokeRequired Then
                GridControl1.EndInvoke(GridControl1.BeginInvoke(New MethodInvoker(AddressOf RefreshGrid)))
                Return
            End If

            Try
                bc.SubmitChanges()
                GridView1.RefreshData()
            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
            End Try
        End Sub

        ''' <summary>
        ''' Create the row
        ''' </summary>
        Protected Overrides Sub OnCreateItem()
            Dim thrd As New System.Threading.Thread(New System.Threading.ParameterizedThreadStart(AddressOf OnCreateItemThread))
            thrd.IsBackground = True
            thrd.SetApartmentState(ApartmentState.STA)
            thrd.Start(Nothing)
        End Sub

        ''' <summary>
        ''' Handle the thread to create the secured property
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub OnCreateItemThread(ByVal obj As Object)

            Dim record As DebtPlus.LINQ.secured_property = DebtPlus.LINQ.Factory.Manufacture_secured_property()
            record.client = Context.ClientDs.ClientId

            Using frm As New Form_SecuredProperty(Context, bc, record)
                If frm.ShowDialog() <> DialogResult.OK Then
                    Return
                End If
            End Using

            bc.secured_properties.InsertOnSubmit(record)
            colProperties.Add(record)

            ' Refresh the grid
            RefreshGrid()
        End Sub

        ''' <summary>
        ''' Edit the current record
        ''' </summary>
        Protected Overrides Sub OnEditItem(ByVal obj As Object)
            Dim record As secured_property = TryCast(obj, secured_property)
            If record IsNot Nothing Then
                Dim thrd As New System.Threading.Thread(New System.Threading.ParameterizedThreadStart(AddressOf OnEditItemThread))
                thrd.IsBackground = True
                thrd.SetApartmentState(ApartmentState.STA)
                thrd.Start(record)
            End If
        End Sub

        ''' <summary>
        ''' Handle the thread to create the secured property
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub OnEditItemThread(ByVal obj As Object)
            Dim record As secured_property = TryCast(obj, secured_property)
            If record Is Nothing Then
                Return
            End If

            Using frm As New Form_SecuredProperty(Context, bc, record)
                If frm.ShowDialog() <> DialogResult.OK Then
                    Return
                End If
            End Using

            RefreshGrid()
        End Sub

        Protected Overrides Sub OnDeleteItem(ByVal obj As Object)
            Dim record As secured_property = TryCast(obj, secured_property)
            If record Is Nothing Then
                Return
            End If

            If DebtPlus.Data.Prompts.RequestConfirmation_Delete() <> DialogResult.Yes Then
                Return
            End If

            ' Delete the secured property from the list
            bc.secured_properties.DeleteOnSubmit(record)
            colProperties.Remove(record)
            RefreshGrid()
        End Sub

        ''' <summary>
        ''' Format the type string
        ''' </summary>
        Private Class SecuredTypeFormatter
            Implements ICustomFormatter, IFormatProvider

            ''' <summary>
            ''' Format the type string
            ''' </summary>
            Public Function Format(ByVal format1 As String, ByVal arg As Object, ByVal formatProvider As IFormatProvider) As String Implements ICustomFormatter.Format
                Dim key As System.Nullable(Of System.Int32) = DebtPlus.Utils.Nulls.v_Int32(arg)
                Return Helpers.GetSecuredTypeText(key)
            End Function

            ''' <summary>
            ''' Retrieve the format provider
            ''' </summary>
            Public Function GetFormat(ByVal formatType As Type) As Object Implements IFormatProvider.GetFormat
                Return Me
            End Function
        End Class
    End Class
End Namespace
