#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.Reports.Template.Forms
Imports ComboboxItem = DebtPlus.Data.Controls.ComboboxItem
Imports System.Linq
Imports DebtPlus.LINQ

Namespace pages

    Friend Class Page_Payments
        Private FromDate As DateTime
        Private ToDate As DateTime

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            FromDate = System.DateTime.Now.Date
            ToDate = System.DateTime.Now.Date

            ComboBoxEdit1.Properties.Items.Clear()
            For Each dt As DebtPlus.LINQ.InMemory.dateRangeType In DebtPlus.LINQ.InMemory.dateRangeTypes.getList()
                ComboBoxEdit1.Properties.Items.Add(New DebtPlus.Data.Controls.ComboboxItem(dt.description, dt, dt.ActiveFlag))
            Next

            ' The default item is the last 3 months.
            Dim defaultItem As Int32 = DebtPlus.LINQ.InMemory.dateRangeTypes.getDefault().GetValueOrDefault(System.Convert.ToInt32(DebtPlus.LINQ.InMemory.dateRangeType.DateItems.DateItem_Last3Months))
            Dim desiredItem As DebtPlus.Data.Controls.ComboboxItem = ComboBoxEdit1.Properties.Items.OfType(Of DebtPlus.Data.Controls.ComboboxItem).Where(Function(s) DirectCast(s.value, DebtPlus.LINQ.InMemory.dateRangeType).Id = defaultItem).FirstOrDefault()
            If desiredItem IsNot Nothing Then
                ComboBoxEdit1.SelectedItem = desiredItem
                SetDateRange(desiredItem)
            End If

            ' Hook into changes for the range settings
            AddHandler ComboBoxEdit1.EditValueChanging, AddressOf ComboBoxEdit1_EditValueChanging
        End Sub

        Private Sub ComboBoxEdit1_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)

            If SetDateRange(TryCast(e.NewValue, DebtPlus.Data.Controls.ComboboxItem)) Then
                PaymentListControl1.ReadForm(FromDate, ToDate)
            Else
                e.Cancel = True
            End If

        End Sub

        Private Function SetDateRange(ByVal DateField As DebtPlus.Data.Controls.ComboboxItem) As Boolean
            If DateField Is Nothing Then
                Return False
            End If

            Return SetDateRange(DirectCast(DateField.value, DebtPlus.LINQ.InMemory.dateRangeType))
        End Function

        Private Function SetDateRange(ByVal DateField As DebtPlus.LINQ.InMemory.dateRangeType) As Boolean
            If DateField Is Nothing Then
                Return False
            End If

            ' If the date is variable then ask for the date
            If DateField.isVariable Then
                Using frm As New DateReportParametersForm(DebtPlus.Utils.DateRange.Specific_Date_Range)
                    Dim FormAnswer As DialogResult = frm.ShowDialog()
                    FromDate = frm.Parameter_FromDate.Date
                    ToDate = frm.Parameter_ToDate.Date
                    If FormAnswer <> DialogResult.OK Then
                        Return False
                    End If
                End Using
                Return True
            End If

            ' Use the dates from the control to define the proper ranges
            FromDate = DateField.startingDate.GetValueOrDefault(New System.DateTime(1800, 1, 1))
            ToDate = DateField.endingDate.GetValueOrDefault(New System.DateTime(2099, 12, 31))
            Return True
        End Function

        Public Overrides Sub ReadForm(bc As DebtPlus.LINQ.BusinessContext)
            MyBase.ReadForm(bc)
            PaymentListControl1.ReadForm(FromDate, ToDate)
        End Sub
    End Class
End Namespace