#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.Collections.Generic
Imports System.Data.Linq
Imports System.Data.SqlClient
Imports DebtPlus.UI.Client.controls
Imports System.Threading
Imports System.Linq
Imports DebtPlus.Data
Imports DebtPlus.LINQ
Imports DebtPlus.UI.Client.forms
Imports DebtPlus.UI.Common
Imports DevExpress.XtraGrid.Views.Base

Namespace pages

    Friend Class Page_Products
        Inherits MyGridControlClient

        ''' <summary>
        ''' Initialize a new class instance
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            EnableMenus = True

            ' Enable the detail items that were turned off for every other instance of the grid control.
            GridView1.OptionsDetail.EnableMasterViewMode = True
            GridView1.OptionsDetail.ShowDetailTabs = True
            GridView1.OptionsDetail.AllowZoomDetail = True
            GridView1.OptionsDetail.AllowOnlyOneMasterRowExpanded = True
            GridView1.OptionsDetail.AllowExpandEmptyDetails = False

            GridView1.OptionsView.ShowDetailButtons = True
        End Sub

        Private bc As BusinessContext = Nothing
        Private colRecords As List(Of client_product) = Nothing

        ' Delegate to the thread completion routines
        Private Delegate Sub RefreshGridDelegate(record As client_product)

        ''' <summary>
        ''' Register the event handlers
        ''' </summary>
        Private Sub RegisterHandlers()
            AddHandler GridView1.CustomColumnDisplayText, AddressOf GridView1_CustomColumnDisplayText
            AddHandler GridView1.MasterRowGetRelationDisplayCaption, AddressOf GridView1_MasterRowGetRelationDisplayCaption
            AddHandler GridView2.CustomColumnDisplayText, AddressOf GridView2_CustomColumnDisplayText
            AddHandler BarButtonItem_Reassign.ItemClick, AddressOf BarButtonItem_Reassign_ItemClick
        End Sub

        ''' <summary>
        ''' Remove the event handler registrations
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub UnRegisterHandlers()
            RemoveHandler GridView1.CustomColumnDisplayText, AddressOf GridView1_CustomColumnDisplayText
            RemoveHandler GridView1.MasterRowGetRelationDisplayCaption, AddressOf GridView1_MasterRowGetRelationDisplayCaption
            RemoveHandler GridView2.CustomColumnDisplayText, AddressOf GridView2_CustomColumnDisplayText
            RemoveHandler BarButtonItem_Reassign.ItemClick, AddressOf BarButtonItem_Reassign_ItemClick
        End Sub

        Public Overrides Sub LoadForm(notUsedBc As BusinessContext)
            MyBase.LoadForm(NotUsedBC)

            ' Set the business context for the page. We need to process the new context to load the associated tables
            bc = New BusinessContext()
            Dim dl As DataLoadOptions = New DataLoadOptions()
            dl.LoadWith(Of client_product)(Function(s) s.client_products_histories)
            bc.LoadOptions = dl
        End Sub

        ''' <summary>
        ''' Process the initial form load sequence
        ''' </summary>
        Public Overrides Sub ReadForm(notUsedBc As BusinessContext)

            ' Load the history information along with the product references
            GridControl1.BeginUpdate()
            UnRegisterHandlers()
            Try

                ' Load the list of products for this client reference
                If colRecords Is Nothing Then
                    colRecords = bc.client_products.Where(Function(s) s.client = Context.ClientDs.clientRecord.Id).ToList()
                    GridControl1.DataSource = colRecords
                End If

            Finally
                MyBase.ReadForm(bc)
                RegisterHandlers()
                GridControl1.EndUpdate()
            End Try
        End Sub

        ''' <summary>
        ''' Handle the CREATE operation
        ''' </summary>
        Protected Overrides Sub OnCreateItem()
            Dim thrd As New Thread(New ThreadStart(AddressOf CreateThread))
            thrd.SetApartmentState(ApartmentState.STA)
            thrd.Name = "ProductsCreate"
            thrd.IsBackground = True
            thrd.Start()
        End Sub

        ''' <summary>
        ''' Handle the EDIT (UPDATE) operation
        ''' </summary>
        Protected Overrides Sub OnEditItem(ByVal obj As Object)
            Dim thrd As New Thread(New ParameterizedThreadStart(AddressOf EditThread))
            thrd.SetApartmentState(ApartmentState.STA)
            thrd.Name = "ProductsEdit"
            thrd.IsBackground = True
            thrd.Start(obj)
        End Sub

        ''' <summary>
        ''' Handle the DELETE operation
        ''' </summary>
        Protected Overrides Sub OnDeleteItem(ByVal obj As Object)
            Dim record As client_product = TryCast(obj, client_product)
            If record Is Nothing Then
                Return
            End If

            ' Ask if the delete is intentional
            If Prompts.RequestConfirmation_Delete() <> DialogResult.Yes Then
                Return
            End If

            Try
                ' Delete the transactions before we can delete the product.
                ' There is a foreign key relationship between the two tables and we need to clear the transaction logs first.
                Dim colDeletes As List(Of product_transaction) = bc.product_transactions.Where(Function(s) s.client_product.Value = record.Id).ToList()
                If colDeletes.Count > 0 Then
                    bc.product_transactions.DeleteAllOnSubmit(colDeletes)
                    bc.SubmitChanges()
                End If

                ' Do the delete operation on the product reference
                bc.client_products.DeleteOnSubmit(record)
                bc.SubmitChanges()

                ' Update the grid to show the item has been removed
                colRecords.Remove(record)
                GridView1.RefreshData()

            Catch ex As SqlException
                ErrorHandling.HandleErrors(ex, "Error deleting client_product reference")
            End Try

        End Sub

        ''' <summary>
        ''' Process the create logic for a new record
        ''' </summary>
        Private Sub CreateThread()

            ' Create the new record
            Dim clientProductRecord As client_product = Factory.Manufacture_client_product()
            clientProductRecord.client = Context.ClientDs.ClientId

            Using frm As New Form_NewProduct(bc, Context.ClientDs.clientRecord, clientProductRecord)
                If frm.ShowDialog() <> DialogResult.OK Then
                    Return
                End If
            End Using

            ' Refresh the grid with the record update
            RefreshGrid(clientProductRecord)

            ' Add a reference to the transaction log showing that we created the new debt
            Dim xactRecord As product_transaction = Factory.Manufacture_product_transaction()
            xactRecord.transaction_type = "BB"
            xactRecord.client_product = clientProductRecord.Id
            xactRecord.product = clientProductRecord.product
            xactRecord.vendor = clientProductRecord.vendor
            xactRecord.cost = clientProductRecord.cost

            bc.product_transactions.InsertOnSubmit(xactRecord)
            bc.SubmitChanges()

            ' Edit the new product reference
            Using frmEdit As New Form_Product(bc, clientProductRecord)
                frmEdit.SimpleButton_Cancel.Enabled = False
                frmEdit.ShowDialog()
                RefreshGrid(Nothing)
            End Using
        End Sub

        ''' <summary>
        ''' Edit the current record
        ''' </summary>
        Private Sub EditThread(obj As Object)

            ' Find the record to be edited
            Dim record As client_product = TryCast(obj, client_product)
            If record Is Nothing Then
                Return
            End If

            ' Edit the product record
            Using frmEdit As New Form_Product(bc, record)
                frmEdit.SimpleButton_Cancel.Enabled = True
                If frmEdit.ShowDialog() <> DialogResult.OK Then
                    Return
                End If
                RefreshGrid(Nothing)
            End Using
        End Sub

        ''' <summary>
        ''' Update the database and refresh the grid when needed
        ''' </summary>
        Private Sub RefreshGrid(record As client_product)

            ' If we need to switch threads then do so now.
            If GridControl1.InvokeRequired() Then
                Dim ia As IAsyncResult = GridControl1.BeginInvoke(New RefreshGridDelegate(AddressOf RefreshGrid), New Object() {record})
                GridControl1.EndInvoke(ia)
                Return
            End If

            ' Update the collection list if we have a new record
            If record IsNot Nothing Then
                bc.client_products.InsertOnSubmit(record)
                colRecords.Add(record)
            End If

            Try
                ' Do the database update and refresh the grid view
                bc.SubmitChanges()
                GridView1.RefreshData()

            Catch ex As SqlException
                ErrorHandling.HandleErrors(ex, "Error updating the client_prodcuts table")
            End Try
        End Sub

        Dim currentRecord As client_product = Nothing
        Private Sub frm_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)

            ' Find the current vendor
            Dim newVendorId As Int32? = DirectCast(sender, DebtPlus.UI.Vendor.Widgets.Search.VendorSearchClass).Vendor
            If Not newVendorId.HasValue Then
                Return
            End If

            ' Do not allow the same Vendor ID to be used for the new vendor item
            If newVendorId.Value = currentRecord.vendor Then
                e.Cancel = True
            End If
        End Sub

        ''' <summary>
        ''' Process a REASSIGN menu item
        ''' </summary>
        Protected Sub BarButtonItem_Reassign_ItemClick(ByVal sender As Object, ByVal e As EventArgs)

            If ControlRow < 0 Then
                Return
            End If

            currentRecord = TryCast(GridView1.GetRow(ControlRow), client_product)
            If currentRecord Is Nothing Then
                Return
            End If

            Using frm As New DebtPlus.UI.Vendor.Widgets.Search.VendorSearchClass()
                AddHandler frm.Validating, AddressOf frm_Validating
                Dim answer As System.Windows.Forms.DialogResult = frm.ShowDialog()
                RemoveHandler frm.Validating, AddressOf frm_Validating
                If answer <> DialogResult.OK OrElse Not frm.Vendor.HasValue Then
                    Return
                End If

                ' Generate a new copy of the record to do the reassign
                Dim newVendor As LINQ.vendor = bc.vendors.FirstOrDefault(Function(s) s.Id = frm.Vendor.Value)
                If newVendor Is Nothing Then
                    Return
                End If

                ' Do not allow the reassign to the same vendor
                If newVendor.Id = currentRecord.vendor Then
                    DebtPlus.Data.Forms.MessageBox.Show("You can not reassign the vendor to the same value.", "Sorry, but that does not make sense.")
                    Return
                End If

                ' Create the history record with the current information
                Dim historyRecord As client_products_history = Factory.Manufacture_client_products_history()
                historyRecord.cost = currentRecord.cost
                historyRecord.discount = currentRecord.discount
                historyRecord.tendered = currentRecord.tendered
                historyRecord.disputed_amount = currentRecord.disputed_amount

                historyRecord.disputed_date = currentRecord.disputed_date
                historyRecord.disputed_note = currentRecord.disputed_note
                historyRecord.disputed_status = currentRecord.disputed_status
                historyRecord.resolution_date = currentRecord.resolution_date
                historyRecord.resolution_note = currentRecord.resolution_note
                historyRecord.resolution_status = currentRecord.resolution_status

                historyRecord.vendor = currentRecord.vendor1.Id
                historyRecord.vendor_label = currentRecord.vendor1.Label
                historyRecord.invoice_date = currentRecord.invoice_date
                historyRecord.invoice_status = currentRecord.invoice_status

                ' Add the record to the history for this product
                currentRecord.client_products_histories.Add(historyRecord)

                ' Reassign the vendor
                currentRecord.vendor1 = newVendor

                ' Correct the invoice status to reflect that the invoice needs to be generated again
                If currentRecord.invoice_status <> client_product.invoice_status_none Then
                    currentRecord.invoice_status = client_product.invoice_status_pending
                    currentRecord.invoice_date = Nothing
                End If

                ' Clear the resolution information
                currentRecord.resolution_date = Nothing
                currentRecord.resolution_note = Nothing
                currentRecord.resolution_status = Nothing

                ' Clear the disputed information
                currentRecord.disputed_amount = 0D
                currentRecord.disputed_date = Nothing
                currentRecord.disputed_note = Nothing
                currentRecord.disputed_status = Nothing

                ' Update the database with the change in vendor
                bc.SubmitChanges()
                GridView1.RefreshData()
            End Using
        End Sub

        Private Sub GridView2_CustomColumnDisplayText(sender As Object, e As CustomColumnDisplayTextEventArgs)

            ' Look for the disputed status value
            If e.Column.FieldName = "disputed_status" Then
                Dim status As Int32? = DirectCast(e.Value, Int32?)
                If status.HasValue Then
                    Dim q As DebtPlus.LINQ.product_dispute_type = DebtPlus.LINQ.Cache.product_dispute_type.getList().Find(Function(s) s.Id = status.Value)
                    If q IsNot Nothing Then
                        e.DisplayText = q.Description
                        Return
                    End If
                End If

                e.DisplayText = String.Empty
                Return
            End If

            ' Look for the resolution status value
            If e.Column.FieldName = "resolution_status" Then
                Dim status As Int32? = DirectCast(e.Value, Int32?)
                If status.HasValue Then
                    Dim q As DebtPlus.LINQ.product_resolution_type = DebtPlus.LINQ.Cache.product_resolution_type.getList().Find(Function(s) s.Id = status.Value)
                    If q IsNot Nothing Then
                        e.DisplayText = q.Description
                        Return
                    End If
                End If

                e.DisplayText = String.Empty
                Return
            End If

        End Sub

        Private Sub GridView1_CustomColumnDisplayText(sender As Object, e As CustomColumnDisplayTextEventArgs)

            ' Find the source record for the field
            Dim record As client_product = TryCast(GridView1.GetRow(e.RowHandle), client_product)
            If record Is Nothing Then
                Return
            End If

            ' Map the product type field
            If e.Column.Equals(GridColumn_Type) Then
                Dim type As product = record.product1
                If type IsNot Nothing Then
                    e.DisplayText = type.description
                    Return
                End If

                e.DisplayText = String.Empty
                Return
            End If

            ' Look for the escrow product reference
            If e.Column.Equals(GridColumn_Escrow) Then
                Dim q As vendor_product = record.vendor1.vendor_products.FirstOrDefault(Function(s) s.product = record.product)
                If q IsNot Nothing Then
                    e.DisplayText = GetEscrowProBonoText(q.escrowProBono)
                    Return
                End If

                e.DisplayText = String.Empty
                Return
            End If

            If e.Column.Equals(GridColumn_Balance) Then
                e.DisplayText = (record.cost - record.discount - record.tendered).ToString("c2")
                Return
            End If

            ' The grid column is not special.
        End Sub

        Private Sub GridView1_MasterRowGetRelationDisplayCaption(sender As Object, e As DevExpress.XtraGrid.Views.Grid.MasterRowGetRelationNameEventArgs)
            e.RelationName = "History"
        End Sub
    End Class
End Namespace
