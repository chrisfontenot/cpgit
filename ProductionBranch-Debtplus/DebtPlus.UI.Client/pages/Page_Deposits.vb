#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.Interfaces.Debt
Imports DebtPlus.Svc.Common
Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.UI.Client.controls
Imports DebtPlus.Events
Imports DevExpress.Utils
Imports DebtPlus.UI.Client.Service
Imports System.Linq

Namespace pages

    Friend Class Page_Deposits
        Inherits ControlBaseClient
        Implements System.ComponentModel.INotifyPropertyChanged

        ''' <summary>
        ''' Implement the property changed to reflect that the ACH data record was modified
        ''' </summary>
        Public Event PropertyChanged As System.ComponentModel.PropertyChangedEventHandler Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged
        Protected Sub RaisePropertyChanged(e As System.ComponentModel.PropertyChangedEventArgs)
            RaiseEvent PropertyChanged(Me, e)
        End Sub

        Protected Overridable Sub OnPropertyChanged(e As System.ComponentModel.PropertyChangedEventArgs)
            RaisePropertyChanged(e)
        End Sub

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Sub RegisterHandlers()
            AddHandler lk_config_fee.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler AchControl1.PropertyChanged, AddressOf AchControl1_PropertyChanged
        End Sub

        Public Sub UnRegisterHandlers()
            RemoveHandler lk_config_fee.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler AchControl1.PropertyChanged, AddressOf AchControl1_PropertyChanged
        End Sub

        ''' <summary>
        ''' Do the one-time load sequence for the new page
        ''' </summary>
        Public Overrides Sub LoadForm(bc As LINQ.BusinessContext)
            MyBase.LoadForm(bc)

            ClientDepositsControl1.LoadForm(bc)
            AchControl1.LoadForm(bc)
            AchOneTimeListControl1.LoadForm(bc)
        End Sub

        ''' <summary>
        ''' Forward the property changed event to the interested party
        ''' </summary>
        Private Sub AchControl1_PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            OnPropertyChanged(e)
        End Sub

        ''' <summary>
        ''' Load the page with the information from the clients table
        ''' </summary>
        Public Overrides Sub ReadForm(bc As DebtPlus.LINQ.BusinessContext)

            UnRegisterHandlers()
            Try

                ' Process the sub-controls on this page
                ClientDepositsControl1.ReadForm(bc)
                AchControl1.ReadForm(bc)
                AchOneTimeListControl1.ReadForm(bc)

                ' load the values.
                chk_personal_checks.Checked = Context.ClientDs.clientRecord.personal_checks

                ' The config fee is a bit more complicated. We want only active items but will also take the current value active or not.
                lk_config_fee.Properties.DataSource = DebtPlus.LINQ.Cache.config_fee.getList().Where(Function(f) f.ActiveFlag OrElse f.Id = Context.ClientDs.clientRecord.config_fee).ToList()
                lk_config_fee.EditValue = Context.ClientDs.clientRecord.config_fee

                ' These are display only.
                lbl_last_deposit_date.Text = If(Context.ClientDs.clientRecord.last_deposit_date.HasValue, Context.ClientDs.clientRecord.last_deposit_date.Value.ToShortDateString(), "NONE")
                lbl_last_deposit_amount.Text = String.Format("{0:c}", Context.ClientDs.clientRecord.last_deposit_amount.GetValueOrDefault(0D))

            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Write any changes to the database for this page
        ''' </summary>
        Public Overrides Sub SaveForm(bc As DebtPlus.LINQ.BusinessContext)

            ' Do the sub-controls first
            ClientDepositsControl1.SaveForm(bc)
            AchControl1.SaveForm(bc)
            AchOneTimeListControl1.SaveForm(bc)

            ' Update the row with the values from the controls
            Context.ClientDs.clientRecord.personal_checks = chk_personal_checks.Checked

            ' If the fee is changed then we need to recompute the fee amount on the X0001 creditor
            Dim newConfigFee As Int32 = DebtPlus.Utils.Nulls.v_Int32(lk_config_fee.EditValue).GetValueOrDefault(0)
            If newConfigFee <> Context.ClientDs.clientRecord.config_fee Then
                Context.ClientDs.clientRecord.config_fee = newConfigFee
                RecomputeFeeAmount()
            End If

        End Sub

        ''' <summary>
        ''' Recalculate the disbursement factor if the calculation logic changes
        ''' </summary>
        Private Sub RecomputeFeeAmount()
            Dim EditValue As System.Nullable(Of Int32) = DebtPlus.Utils.Nulls.v_Int32(lk_config_fee.EditValue)
            If Not EditValue.HasValue OrElse EditValue.Value <= 0 Then
                Return
            End If

            Using fee As New MonthlyFeeCalculation()
                Try
                    Dim DebtRecord As ClientUpdateDebtRecord = TryCast(Context.DebtRecords.MonthlyFeeDebt, ClientUpdateDebtRecord)
                    If DebtRecord Is Nothing Then
                        Return
                    End If

                    ' Adjust the disbursement factor
                    AddHandler fee.QueryValue, AddressOf QueryValue
                    DebtRecord.disbursement_factor = fee.FeeAmount()

                    ' Adjust the scheduled payment information
                    Dim SchedPayment As Decimal = DebtPlus.Utils.Nulls.DDec(DebtRecord.disbursement_factor) - DebtPlus.Utils.Nulls.DDec(DebtRecord.payments_month_0)
                    If SchedPayment < 0D Then SchedPayment = 0D
                    DebtRecord.sched_payment = SchedPayment

                Catch ex As System.Data.SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error calculating fee information")

                Finally
                    RemoveHandler fee.QueryValue, AddressOf QueryValue
                End Try
            End Using
        End Sub

        ''' <summary>
        ''' Ask the debt list for the fee calculation information
        ''' </summary>
        Private Sub QueryValue(ByVal Sender As Object, ByVal e As DebtPlus.Events.ParameterValueEventArgs)

            ' Use the disbursement factor for the amount to be paid
            If e.Name = DebtPlus.Events.ParameterValueEventArgs.name_SchedPayment Then
                e.Value = CType(Context.DebtRecords, IFeeable).QueryFeeValue("disbursement_factor")
                Return
            End If

            ' Look at the total of the disbursement factors rather than the debit_amt field
            If e.Name = DebtPlus.Events.ParameterValueEventArgs.name_DollarsDisbursed Then
                e.Value = Context.DebtRecords.Cast(Of ClientUpdateDebtRecord).Where(Function(s) s.IsProratable AndAlso Not (s.IsHeld OrElse s.IsFee)).Sum(Function(s) s.disbursement_factor)
                Return
            End If

            ' Replace the config fee with the control's new value
            If e.Name = DebtPlus.Events.ParameterValueEventArgs.name_ConfigFee Then
                e.Value = lk_config_fee.EditValue
                Return
            End If

            ' Since we are using the disbursement factor, limit the amount to the monthly max.
            If e.Name = DebtPlus.Events.ParameterValueEventArgs.name_LimitMonthlyMax Then
                e.Value = True
                Return
            End If

            ' All others are simply passed to the debt list
            e.Value = CType(Context.DebtRecords, IFeeable).QueryFeeValue(e.Name)
        End Sub
    End Class
End Namespace
