Imports DebtPlus.UI.Client.controls

Namespace pages

    Partial Class Page_Assets

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView
            Me.GridColumn_asset = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_asset.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_asset_id = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_asset_id.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_type_description = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_type_description.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_amount = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_amount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_frequency = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_frequency.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_asset_amount = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_asset_amount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.RepositoryItemCalcEdit_amount = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
            Me.RepositoryItemLookUpEdit_frequency = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RepositoryItemCalcEdit_amount, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RepositoryItemLookUpEdit_frequency, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                                             Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl1.Appearance.Options.UseTextOptions = True
            Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl1.Location = New System.Drawing.Point(16, 8)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(544, 34)
            Me.LabelControl1.TabIndex = 20
            Me.LabelControl1.Text = "This panel lists additional income sources. Items which are not directly related " & _
                                    "to employment income are listed here. Employment income is listed with the appli" & _
                                    "cant and co-applicant tabs."
            Me.LabelControl1.UseMnemonic = False
            '
            'GridColumn_asset
            '
            Me.GridColumn_asset.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_asset.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_asset.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_asset.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_asset.Caption = "ID"
            Me.GridColumn_asset.CustomizationCaption = "Record ID"
            Me.GridColumn_asset.DisplayFormat.FormatString = "{0:f0}"
            Me.GridColumn_asset.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_asset.FieldName = "assetRecord.Id"
            Me.GridColumn_asset.GroupFormat.FormatString = "{0:f0}"
            Me.GridColumn_asset.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_asset.Name = "GridColumn_asset"
            Me.GridColumn_asset.OptionsColumn.AllowEdit = False
            '
            'GridColumn_asset_id
            '
            Me.GridColumn_asset_id.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_asset_id.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_asset_id.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_asset_id.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_asset_id.Caption = "Type"
            Me.GridColumn_asset_id.CustomizationCaption = "Record Type"
            Me.GridColumn_asset_id.DisplayFormat.FormatString = "{0:f0}"
            Me.GridColumn_asset_id.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_asset_id.FieldName = "assetIdRecord.Id"
            Me.GridColumn_asset_id.GroupFormat.FormatString = "{0:f0}"
            Me.GridColumn_asset_id.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_asset_id.Name = "GridColumn_asset_id"
            Me.GridColumn_asset_id.OptionsColumn.AllowEdit = False
            '
            'GridColumn_type_description
            '
            Me.GridColumn_type_description.Caption = "Description"
            Me.GridColumn_type_description.CustomizationCaption = "Type Description"
            Me.GridColumn_type_description.FieldName = "assetIdRecord.description"
            Me.GridColumn_type_description.Name = "GridColumn_type_description"
            Me.GridColumn_type_description.OptionsColumn.AllowEdit = False
            Me.GridColumn_type_description.Visible = True
            Me.GridColumn_type_description.VisibleIndex = 0
            Me.GridColumn_type_description.Width = 263
            '
            'GridColumn_amount
            '
            Me.GridColumn_amount.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_amount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_amount.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_amount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_amount.Caption = "Amount"
            Me.GridColumn_amount.ColumnEdit = Me.RepositoryItemCalcEdit_amount
            Me.GridColumn_amount.CustomizationCaption = "Amount value"
            Me.GridColumn_amount.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_amount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_amount.FieldName = "assetRecord.amount"
            Me.GridColumn_amount.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_amount.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_amount.Name = "GridColumn_amount"
            Me.GridColumn_amount.Visible = True
            Me.GridColumn_amount.VisibleIndex = 1
            Me.GridColumn_amount.Width = 131
            '
            'RepositoryItemCalcEdit_amount
            '
            Me.RepositoryItemCalcEdit_amount.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.RepositoryItemCalcEdit_amount.Appearance.Options.UseTextOptions = True
            Me.RepositoryItemCalcEdit_amount.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.RepositoryItemCalcEdit_amount.AutoHeight = False
            Me.RepositoryItemCalcEdit_amount.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.RepositoryItemCalcEdit_amount.DisplayFormat.FormatString = "{0:c}"
            Me.RepositoryItemCalcEdit_amount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.RepositoryItemCalcEdit_amount.EditFormat.FormatString = "{0:c}"
            Me.RepositoryItemCalcEdit_amount.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.RepositoryItemCalcEdit_amount.Mask.BeepOnError = True
            Me.RepositoryItemCalcEdit_amount.Mask.EditMask = "c"
            Me.RepositoryItemCalcEdit_amount.Name = "RepositoryItemCalcEdit_amount"
            Me.RepositoryItemCalcEdit_amount.Precision = 2
            '
            'GridColumn_frequency
            '
            Me.GridColumn_frequency.Caption = "Frequency"
            Me.GridColumn_frequency.ColumnEdit = Me.RepositoryItemLookUpEdit_frequency
            Me.GridColumn_frequency.CustomizationCaption = "Payment Frequency"
            Me.GridColumn_frequency.FieldName = "assetRecord.frequency"
            Me.GridColumn_frequency.Name = "GridColumn_frequency"
            Me.GridColumn_frequency.Visible = True
            Me.GridColumn_frequency.VisibleIndex = 2
            Me.GridColumn_frequency.Width = 113
            '
            'RepositoryItemLookUpEdit_frequency
            '
            Me.RepositoryItemLookUpEdit_frequency.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.RepositoryItemLookUpEdit_frequency.AutoHeight = False
            Me.RepositoryItemLookUpEdit_frequency.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.RepositoryItemLookUpEdit_frequency.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "{0:f0}", False, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("PeriodsPerYear", "Periods", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Far)})
            Me.RepositoryItemLookUpEdit_frequency.DisplayMember = "description"
            Me.RepositoryItemLookUpEdit_frequency.Name = "RepositoryItemLookUpEdit_frequency"
            Me.RepositoryItemLookUpEdit_frequency.NullText = ""
            Me.RepositoryItemLookUpEdit_frequency.ShowFooter = False
            Me.RepositoryItemLookUpEdit_frequency.ValueMember = "Id"
            '
            'GridColumn_asset_amount
            '
            Me.GridColumn_asset_amount.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_asset_amount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_asset_amount.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_asset_amount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_asset_amount.Caption = "Monthly"
            Me.GridColumn_asset_amount.CustomizationCaption = "Monthly dollar amount"
            Me.GridColumn_asset_amount.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_asset_amount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_asset_amount.FieldName = "assetRecord.asset_amount"
            Me.GridColumn_asset_amount.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_asset_amount.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_asset_amount.Name = "GridColumn_asset_amount"
            Me.GridColumn_asset_amount.OptionsColumn.AllowEdit = False
            Me.GridColumn_asset_amount.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_asset_amount.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_asset_amount.Visible = True
            Me.GridColumn_asset_amount.VisibleIndex = 3
            Me.GridColumn_asset_amount.Width = 96
            '
            Me.GridColumn_asset.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_asset_id.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_type_description.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_amount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_frequency.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            Me.GridColumn_asset_amount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            '
            'GridControl1
            '
            Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                                             Or System.Windows.Forms.AnchorStyles.Left) _
                                            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GridControl1.Location = New System.Drawing.Point(0, 48)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemLookUpEdit_frequency, Me.RepositoryItemCalcEdit_amount})
            Me.GridControl1.Size = New System.Drawing.Size(576, 244)
            Me.GridControl1.TabIndex = 21
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_asset, Me.GridColumn_asset_id, Me.GridColumn_type_description, Me.GridColumn_amount, Me.GridColumn_frequency, Me.GridColumn_asset_amount})
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowFooter = True
            '
            'Page_Assets
            '
            Me.Controls.Add(Me.GridControl1)
            Me.Controls.Add(Me.LabelControl1)
            Me.Name = "Page_Assets"
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RepositoryItemCalcEdit_amount, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RepositoryItemLookUpEdit_frequency, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
        End Sub

        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents GridColumn_asset As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_asset_id As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_type_description As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_amount As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_frequency As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents RepositoryItemLookUpEdit_frequency As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
        Friend WithEvents RepositoryItemCalcEdit_amount As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
        Friend WithEvents GridColumn_asset_amount As DevExpress.XtraGrid.Columns.GridColumn
    End Class
End Namespace
