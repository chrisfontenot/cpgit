#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.UI.Client.controls
Imports DebtPlus.LINQ
Imports System.Linq

Namespace pages

    Friend Class Page_Housing
        Inherits ControlBaseClient

        ' Current housing record being edited in this page
        Private clientHousingRecord As DebtPlus.LINQ.client_housing = Nothing

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            RegisterHandlers()

            Try
                ' Always select the left most tab page (but don't die if there is no tab group implemented.)
                TabbedControlGroup1.SelectedTabPageIndex = 0
            Catch ex As Exception
            End Try
        End Sub

        Private Sub RegisterHandlers()
            AddHandler CalcEdit_BackEnd_DTI.CustomDisplayText, AddressOf CalcEdit_NFCMP_DTI_CustomDisplayText
            AddHandler CalcEdit_FrontEnd_DTI.CustomDisplayText, AddressOf CalcEdit_NFCMP_DTI_CustomDisplayText
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler CalcEdit_BackEnd_DTI.CustomDisplayText, AddressOf CalcEdit_NFCMP_DTI_CustomDisplayText
            RemoveHandler CalcEdit_FrontEnd_DTI.CustomDisplayText, AddressOf CalcEdit_NFCMP_DTI_CustomDisplayText
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer = Nothing

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents CheckEdit_predatory_lending As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents CheckEdit_migrant_farm_worker As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents LookUpEdit_housing_status As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ComboBoxEdit_nfmcp_level As DevExpress.XtraEditors.ComboBoxEdit
        Friend WithEvents CheckEdit_nfmcp_decline_authorization As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents CheckEdit_nfmcp_privacy_policy As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents DateEdit_HECM_Certificate_Expires As DevExpress.XtraEditors.DateEdit
        Friend WithEvents DateEdit_HECM_Certificate_Date As DevExpress.XtraEditors.DateEdit
        Friend WithEvents TextEdit_HECM_Certificate_ID As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LayoutControlItem_housing_type As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LookUpEdit_HUD_Assistance As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents CalcEdit_OtherHUDFunding As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents CheckEdit_HUD_DiscriminationVictim As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents CheckEdit_HUD_FirstTimeHomeBuyer As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents TabbedControlGroup1 As DevExpress.XtraLayout.TabbedControlGroup
        Friend WithEvents HUD_Information As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents NFMCP_Information As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem17 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents CalcEdit_BackEnd_DTI As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents CheckEdit_LoanScam As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents LayoutControlItem21 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem22 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents CalcEdit_FrontEnd_DTI As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents LayoutControlGroup3 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem4 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents PropertyListControl1 As DebtPlus.UI.Client.controls.PropertyListControl
        Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents CheckEdit_colonias As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlGroup_HECM_Default As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents HECMDefault1 As DebtPlus.UI.Client.controls.HECMDefault
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents fair_housing As DevExpress.XtraEditors.CheckEdit

        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.HECMDefault1 = New DebtPlus.UI.Client.controls.HECMDefault()
            Me.CheckEdit_colonias = New DevExpress.XtraEditors.CheckEdit()
            Me.CalcEdit_FrontEnd_DTI = New DevExpress.XtraEditors.CalcEdit()
            Me.CalcEdit_BackEnd_DTI = New DevExpress.XtraEditors.CalcEdit()
            Me.CheckEdit_LoanScam = New DevExpress.XtraEditors.CheckEdit()
            Me.CheckEdit_HUD_FirstTimeHomeBuyer = New DevExpress.XtraEditors.CheckEdit()
            Me.CheckEdit_HUD_DiscriminationVictim = New DevExpress.XtraEditors.CheckEdit()
            Me.CalcEdit_OtherHUDFunding = New DevExpress.XtraEditors.CalcEdit()
            Me.LookUpEdit_HUD_Assistance = New DevExpress.XtraEditors.LookUpEdit()
            Me.TextEdit_HECM_Certificate_ID = New DevExpress.XtraEditors.TextEdit()
            Me.DateEdit_HECM_Certificate_Expires = New DevExpress.XtraEditors.DateEdit()
            Me.DateEdit_HECM_Certificate_Date = New DevExpress.XtraEditors.DateEdit()
            Me.ComboBoxEdit_nfmcp_level = New DevExpress.XtraEditors.ComboBoxEdit()
            Me.CheckEdit_nfmcp_decline_authorization = New DevExpress.XtraEditors.CheckEdit()
            Me.CheckEdit_nfmcp_privacy_policy = New DevExpress.XtraEditors.CheckEdit()
            Me.LookUpEdit_housing_status = New DevExpress.XtraEditors.LookUpEdit()
            Me.PropertyListControl1 = New DebtPlus.UI.Client.controls.PropertyListControl()
            Me.CheckEdit_predatory_lending = New DevExpress.XtraEditors.CheckEdit()
            Me.CheckEdit_migrant_farm_worker = New DevExpress.XtraEditors.CheckEdit()
            Me.fair_housing = New DevExpress.XtraEditors.CheckEdit()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.TabbedControlGroup1 = New DevExpress.XtraLayout.TabbedControlGroup()
            Me.LayoutControlGroup3 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem22 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem4 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.HUD_Information = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem17 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem21 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.NFMCP_Information = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlGroup_HECM_Default = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_housing_type = New DevExpress.XtraLayout.LayoutControlItem()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.HECMDefault1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_colonias.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_FrontEnd_DTI.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_BackEnd_DTI.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_LoanScam.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_HUD_FirstTimeHomeBuyer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_HUD_DiscriminationVictim.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CalcEdit_OtherHUDFunding.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_HUD_Assistance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextEdit_HECM_Certificate_ID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit_HECM_Certificate_Expires.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit_HECM_Certificate_Expires.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit_HECM_Certificate_Date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DateEdit_HECM_Certificate_Date.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ComboBoxEdit_nfmcp_level.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_nfmcp_decline_authorization.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_nfmcp_privacy_policy.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_housing_status.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PropertyListControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_predatory_lending.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_migrant_farm_worker.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.fair_housing.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TabbedControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.HUD_Information, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.NFMCP_Information, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup_HECM_Default, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_housing_type, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.HECMDefault1)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_colonias)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit_FrontEnd_DTI)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit_BackEnd_DTI)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_LoanScam)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_HUD_FirstTimeHomeBuyer)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_HUD_DiscriminationVictim)
            Me.LayoutControl1.Controls.Add(Me.CalcEdit_OtherHUDFunding)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_HUD_Assistance)
            Me.LayoutControl1.Controls.Add(Me.TextEdit_HECM_Certificate_ID)
            Me.LayoutControl1.Controls.Add(Me.DateEdit_HECM_Certificate_Expires)
            Me.LayoutControl1.Controls.Add(Me.DateEdit_HECM_Certificate_Date)
            Me.LayoutControl1.Controls.Add(Me.ComboBoxEdit_nfmcp_level)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_nfmcp_decline_authorization)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_nfmcp_privacy_policy)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_housing_status)
            Me.LayoutControl1.Controls.Add(Me.PropertyListControl1)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_predatory_lending)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_migrant_farm_worker)
            Me.LayoutControl1.Controls.Add(Me.fair_housing)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(691, 295, 250, 350)
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(612, 366)
            Me.LayoutControl1.TabIndex = 8
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'HECMDefault1
            '
            Me.HECMDefault1.Location = New System.Drawing.Point(4, 226)
            Me.HECMDefault1.Name = "HECMDefault1"
            Me.HECMDefault1.Size = New System.Drawing.Size(600, 132)
            Me.HECMDefault1.TabIndex = 34
            '
            'CheckEdit_colonias
            '
            Me.CheckEdit_colonias.Location = New System.Drawing.Point(4, 319)
            Me.CheckEdit_colonias.Name = "CheckEdit_colonias"
            Me.CheckEdit_colonias.Properties.Caption = "Colonias Resident"
            Me.CheckEdit_colonias.Size = New System.Drawing.Size(167, 19)
            Me.CheckEdit_colonias.StyleController = Me.LayoutControl1
            Me.CheckEdit_colonias.TabIndex = 33
            Me.CheckEdit_colonias.ToolTip = "Is the client a colonias resident (along the Mexican border)?"
            '
            'CalcEdit_FrontEnd_DTI
            '
            Me.CalcEdit_FrontEnd_DTI.Location = New System.Drawing.Point(489, 226)
            Me.CalcEdit_FrontEnd_DTI.Name = "CalcEdit_FrontEnd_DTI"
            Me.CalcEdit_FrontEnd_DTI.TabIndex = 32
            Me.CalcEdit_FrontEnd_DTI.ToolTipTitle = "Front-End Debt to Income ratio."
            Me.CalcEdit_FrontEnd_DTI.Size = New System.Drawing.Size(115, 20)
            Me.CalcEdit_FrontEnd_DTI.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_FrontEnd_DTI.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.CalcEdit_FrontEnd_DTI.Properties.EditFormat.FormatString = "f3"
            Me.CalcEdit_FrontEnd_DTI.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_FrontEnd_DTI.Properties.Mask.BeepOnError = True
            Me.CalcEdit_FrontEnd_DTI.Properties.Mask.EditMask = "f3"
            Me.CalcEdit_FrontEnd_DTI.StyleController = Me.LayoutControl1
            '
            'CalcEdit_BackEnd_DTI
            '
            Me.CalcEdit_BackEnd_DTI.Location = New System.Drawing.Point(489, 250)
            Me.CalcEdit_BackEnd_DTI.Name = "CalcEdit_BackEnd_DTI"
            Me.CalcEdit_BackEnd_DTI.TabIndex = 31
            Me.CalcEdit_BackEnd_DTI.ToolTipTitle = "Back-End Debt to Income ratio."
            Me.CalcEdit_BackEnd_DTI.Size = New System.Drawing.Size(115, 20)
            Me.CalcEdit_BackEnd_DTI.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_BackEnd_DTI.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.CalcEdit_BackEnd_DTI.Properties.EditFormat.FormatString = "f3"
            Me.CalcEdit_BackEnd_DTI.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_BackEnd_DTI.Properties.Mask.BeepOnError = True
            Me.CalcEdit_BackEnd_DTI.Properties.Mask.EditMask = "f3"
            Me.CalcEdit_BackEnd_DTI.StyleController = Me.LayoutControl1
            '
            'CheckEdit_LoanScam
            '
            Me.CheckEdit_LoanScam.Location = New System.Drawing.Point(4, 273)
            Me.CheckEdit_LoanScam.Name = "CheckEdit_LoanScam"
            Me.CheckEdit_LoanScam.Properties.Caption = "Loan Scam Victim"
            Me.CheckEdit_LoanScam.Size = New System.Drawing.Size(167, 19)
            Me.CheckEdit_LoanScam.StyleController = Me.LayoutControl1
            Me.CheckEdit_LoanScam.TabIndex = 30
            '
            'CheckEdit_HUD_FirstTimeHomeBuyer
            '
            Me.CheckEdit_HUD_FirstTimeHomeBuyer.Location = New System.Drawing.Point(175, 296)
            Me.CheckEdit_HUD_FirstTimeHomeBuyer.Name = "CheckEdit_HUD_FirstTimeHomeBuyer"
            Me.CheckEdit_HUD_FirstTimeHomeBuyer.Properties.Caption = "First Time Home Buyer"
            Me.CheckEdit_HUD_FirstTimeHomeBuyer.Size = New System.Drawing.Size(429, 19)
            Me.CheckEdit_HUD_FirstTimeHomeBuyer.StyleController = Me.LayoutControl1
            Me.CheckEdit_HUD_FirstTimeHomeBuyer.TabIndex = 29
            Me.CheckEdit_HUD_FirstTimeHomeBuyer.ToolTip = "Is the client a first time home buyer?"
            '
            'CheckEdit_HUD_DiscriminationVictim
            '
            Me.CheckEdit_HUD_DiscriminationVictim.Location = New System.Drawing.Point(4, 296)
            Me.CheckEdit_HUD_DiscriminationVictim.Name = "CheckEdit_HUD_DiscriminationVictim"
            Me.CheckEdit_HUD_DiscriminationVictim.Properties.Caption = "Discrimination Victim"
            Me.CheckEdit_HUD_DiscriminationVictim.Size = New System.Drawing.Size(167, 19)
            Me.CheckEdit_HUD_DiscriminationVictim.StyleController = Me.LayoutControl1
            Me.CheckEdit_HUD_DiscriminationVictim.TabIndex = 28
            Me.CheckEdit_HUD_DiscriminationVictim.ToolTip = "Client is a victim of housing discrimination"
            '
            'CalcEdit_OtherHUDFunding
            '
            Me.CalcEdit_OtherHUDFunding.Location = New System.Drawing.Point(393, 226)
            Me.CalcEdit_OtherHUDFunding.Name = "CalcEdit_OtherHUDFunding"
            Me.CalcEdit_OtherHUDFunding.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.CalcEdit_OtherHUDFunding.Properties.Appearance.Options.UseTextOptions = True
            Me.CalcEdit_OtherHUDFunding.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.CalcEdit_OtherHUDFunding.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.CalcEdit_OtherHUDFunding.Properties.DisplayFormat.FormatString = "{0:c}"
            Me.CalcEdit_OtherHUDFunding.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_OtherHUDFunding.Properties.EditFormat.FormatString = "{0:c}"
            Me.CalcEdit_OtherHUDFunding.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.CalcEdit_OtherHUDFunding.Properties.Mask.BeepOnError = True
            Me.CalcEdit_OtherHUDFunding.Properties.Mask.EditMask = "c"
            Me.CalcEdit_OtherHUDFunding.Properties.Precision = 2
            Me.CalcEdit_OtherHUDFunding.Size = New System.Drawing.Size(211, 20)
            Me.CalcEdit_OtherHUDFunding.StyleController = Me.LayoutControl1
            Me.CalcEdit_OtherHUDFunding.TabIndex = 27
            Me.CalcEdit_OtherHUDFunding.ToolTip = "Amount of other funding given for this client"
            '
            'LookUpEdit_HUD_Assistance
            '
            Me.LookUpEdit_HUD_Assistance.Location = New System.Drawing.Point(90, 226)
            Me.LookUpEdit_HUD_Assistance.Name = "LookUpEdit_HUD_Assistance"
            Me.LookUpEdit_HUD_Assistance.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.LookUpEdit_HUD_Assistance.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_HUD_Assistance.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_HUD_Assistance.Properties.DisplayMember = "description"
            Me.LookUpEdit_HUD_Assistance.Properties.NullText = ""
            Me.LookUpEdit_HUD_Assistance.Properties.ShowFooter = False
            Me.LookUpEdit_HUD_Assistance.Properties.ShowHeader = False
            Me.LookUpEdit_HUD_Assistance.Properties.SortColumnIndex = 1
            Me.LookUpEdit_HUD_Assistance.Properties.ValueMember = "Id"
            Me.LookUpEdit_HUD_Assistance.Size = New System.Drawing.Size(213, 20)
            Me.LookUpEdit_HUD_Assistance.StyleController = Me.LayoutControl1
            Me.LookUpEdit_HUD_Assistance.TabIndex = 26
            Me.LookUpEdit_HUD_Assistance.ToolTip = "Type of additional HUD assistance for the client"
            '
            'TextEdit_HECM_Certificate_ID
            '
            Me.TextEdit_HECM_Certificate_ID.Location = New System.Drawing.Point(90, 226)
            Me.TextEdit_HECM_Certificate_ID.Name = "TextEdit_HECM_Certificate_ID"
            Me.TextEdit_HECM_Certificate_ID.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.TextEdit_HECM_Certificate_ID.Properties.Mask.BeepOnError = True
            Me.TextEdit_HECM_Certificate_ID.Properties.Mask.EditMask = "[1-9]\d*"
            Me.TextEdit_HECM_Certificate_ID.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
            Me.TextEdit_HECM_Certificate_ID.Properties.MaxLength = 10
            Me.TextEdit_HECM_Certificate_ID.Size = New System.Drawing.Size(178, 20)
            Me.TextEdit_HECM_Certificate_ID.StyleController = Me.LayoutControl1
            Me.TextEdit_HECM_Certificate_ID.TabIndex = 24
            Me.TextEdit_HECM_Certificate_ID.ToolTip = "The ID number of the certificate that was issued"
            '
            'DateEdit_HECM_Certificate_Expires
            '
            Me.DateEdit_HECM_Certificate_Expires.EditValue = Nothing
            Me.DateEdit_HECM_Certificate_Expires.Location = New System.Drawing.Point(90, 274)
            Me.DateEdit_HECM_Certificate_Expires.Name = "DateEdit_HECM_Certificate_Expires"
            Me.DateEdit_HECM_Certificate_Expires.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.DateEdit_HECM_Certificate_Expires.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.DateEdit_HECM_Certificate_Expires.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.DateEdit_HECM_Certificate_Expires.Size = New System.Drawing.Size(178, 20)
            Me.DateEdit_HECM_Certificate_Expires.StyleController = Me.LayoutControl1
            Me.DateEdit_HECM_Certificate_Expires.TabIndex = 23
            Me.DateEdit_HECM_Certificate_Expires.ToolTip = "Date when the HECM Certificate expires"
            '
            'DateEdit_HECM_Certificate_Date
            '
            Me.DateEdit_HECM_Certificate_Date.EditValue = Nothing
            Me.DateEdit_HECM_Certificate_Date.Location = New System.Drawing.Point(90, 250)
            Me.DateEdit_HECM_Certificate_Date.Name = "DateEdit_HECM_Certificate_Date"
            Me.DateEdit_HECM_Certificate_Date.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.DateEdit_HECM_Certificate_Date.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.DateEdit_HECM_Certificate_Date.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.DateEdit_HECM_Certificate_Date.Size = New System.Drawing.Size(178, 20)
            Me.DateEdit_HECM_Certificate_Date.StyleController = Me.LayoutControl1
            Me.DateEdit_HECM_Certificate_Date.TabIndex = 22
            Me.DateEdit_HECM_Certificate_Date.ToolTip = "Date when the certificate was issued"
            '
            'ComboBoxEdit_nfmcp_level
            '
            Me.ComboBoxEdit_nfmcp_level.EditValue = "None"
            Me.ComboBoxEdit_nfmcp_level.Location = New System.Drawing.Point(90, 226)
            Me.ComboBoxEdit_nfmcp_level.Name = "ComboBoxEdit_nfmcp_level"
            Me.ComboBoxEdit_nfmcp_level.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit_nfmcp_level.Properties.Items.AddRange(New Object() {"None", "Level 1", "Level 2", "Level 3", "Level 4a", "Level 4b", "Level 5 (for future use)", "Level 5a (for future use)", "Level 5b (for future use)"})
            Me.ComboBoxEdit_nfmcp_level.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
            Me.ComboBoxEdit_nfmcp_level.Size = New System.Drawing.Size(514, 20)
            Me.ComboBoxEdit_nfmcp_level.StyleController = Me.LayoutControl1
            Me.ComboBoxEdit_nfmcp_level.TabIndex = 17
            '
            'CheckEdit_nfmcp_decline_authorization
            '
            Me.CheckEdit_nfmcp_decline_authorization.Location = New System.Drawing.Point(4, 273)
            Me.CheckEdit_nfmcp_decline_authorization.Name = "CheckEdit_nfmcp_decline_authorization"
            Me.CheckEdit_nfmcp_decline_authorization.Properties.Caption = "Declined Authorization"
            Me.CheckEdit_nfmcp_decline_authorization.Size = New System.Drawing.Size(600, 19)
            Me.CheckEdit_nfmcp_decline_authorization.StyleController = Me.LayoutControl1
            Me.CheckEdit_nfmcp_decline_authorization.TabIndex = 15
            '
            'CheckEdit_nfmcp_privacy_policy
            '
            Me.CheckEdit_nfmcp_privacy_policy.Location = New System.Drawing.Point(4, 250)
            Me.CheckEdit_nfmcp_privacy_policy.Name = "CheckEdit_nfmcp_privacy_policy"
            Me.CheckEdit_nfmcp_privacy_policy.Properties.Caption = "Privacy Policy Signed"
            Me.CheckEdit_nfmcp_privacy_policy.Size = New System.Drawing.Size(600, 19)
            Me.CheckEdit_nfmcp_privacy_policy.StyleController = Me.LayoutControl1
            Me.CheckEdit_nfmcp_privacy_policy.TabIndex = 14
            '
            'LookUpEdit_housing_status
            '
            Me.LookUpEdit_housing_status.Location = New System.Drawing.Point(90, 226)
            Me.LookUpEdit_housing_status.Name = "LookUpEdit_housing_status"
            Me.LookUpEdit_housing_status.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_housing_status.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_housing_status.Properties.DisplayMember = "description"
            Me.LookUpEdit_housing_status.Properties.NullText = ""
            Me.LookUpEdit_housing_status.Properties.ShowFooter = False
            Me.LookUpEdit_housing_status.Properties.ShowHeader = False
            Me.LookUpEdit_housing_status.Properties.SortColumnIndex = 1
            Me.LookUpEdit_housing_status.Properties.ValueMember = "Id"
            Me.LookUpEdit_housing_status.Size = New System.Drawing.Size(309, 20)
            Me.LookUpEdit_housing_status.StyleController = Me.LayoutControl1
            Me.LookUpEdit_housing_status.TabIndex = 8
            '
            'PropertyListControl1
            '
            Me.PropertyListControl1.Location = New System.Drawing.Point(3, 3)
            Me.PropertyListControl1.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
            Me.PropertyListControl1.Name = "PropertyListControl1"
            Me.PropertyListControl1.Size = New System.Drawing.Size(606, 196)
            Me.PropertyListControl1.TabIndex = 13
            '
            'CheckEdit_predatory_lending
            '
            Me.CheckEdit_predatory_lending.Location = New System.Drawing.Point(4, 250)
            Me.CheckEdit_predatory_lending.Name = "CheckEdit_predatory_lending"
            Me.CheckEdit_predatory_lending.Properties.Caption = "Predatory Lending Victim"
            Me.CheckEdit_predatory_lending.Size = New System.Drawing.Size(167, 19)
            Me.CheckEdit_predatory_lending.StyleController = Me.LayoutControl1
            Me.CheckEdit_predatory_lending.TabIndex = 11
            Me.CheckEdit_predatory_lending.ToolTip = "Is the client a victim of predatory lending?"
            '
            'CheckEdit_migrant_farm_worker
            '
            Me.CheckEdit_migrant_farm_worker.Location = New System.Drawing.Point(175, 273)
            Me.CheckEdit_migrant_farm_worker.Name = "CheckEdit_migrant_farm_worker"
            Me.CheckEdit_migrant_farm_worker.Properties.Caption = "Migrant Farm Worker"
            Me.CheckEdit_migrant_farm_worker.Size = New System.Drawing.Size(429, 19)
            Me.CheckEdit_migrant_farm_worker.StyleController = Me.LayoutControl1
            Me.CheckEdit_migrant_farm_worker.TabIndex = 10
            Me.CheckEdit_migrant_farm_worker.ToolTip = "Is the client a migrant farm worker?"
            '
            'fair_housing
            '
            Me.fair_housing.Location = New System.Drawing.Point(175, 250)
            Me.fair_housing.Name = "fair_housing"
            Me.fair_housing.Properties.Caption = "Fair Housing Issue"
            Me.fair_housing.Size = New System.Drawing.Size(429, 19)
            Me.fair_housing.StyleController = Me.LayoutControl1
            Me.fair_housing.TabIndex = 7
            Me.fair_housing.ToolTip = "Is this an issue relating to fair housing?"
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.TabbedControlGroup1, Me.LayoutControlItem8})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "Root"
            Me.LayoutControlGroup1.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(612, 366)
            Me.LayoutControlGroup1.Text = "Root"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'TabbedControlGroup1
            '
            Me.TabbedControlGroup1.AllowCustomizeChildren = False
            Me.TabbedControlGroup1.AllowHide = False
            Me.TabbedControlGroup1.CustomizationFormText = "Grant Information"
            Me.TabbedControlGroup1.Location = New System.Drawing.Point(0, 200)
            Me.TabbedControlGroup1.MultiLine = DevExpress.Utils.DefaultBoolean.[False]
            Me.TabbedControlGroup1.Name = "TabbedControlGroup1"
            Me.TabbedControlGroup1.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
            Me.TabbedControlGroup1.SelectedTabPage = Me.LayoutControlGroup3
            Me.TabbedControlGroup1.SelectedTabPageIndex = 0
            Me.TabbedControlGroup1.Size = New System.Drawing.Size(610, 164)
            Me.TabbedControlGroup1.Spacing = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
            Me.TabbedControlGroup1.TabPages.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup3, Me.HUD_Information, Me.LayoutControlGroup2, Me.NFMCP_Information, Me.LayoutControlGroup_HECM_Default})
            Me.TabbedControlGroup1.Text = "Grant Information"
            Me.TabbedControlGroup1.TextLocation = DevExpress.Utils.Locations.[Default]
            '
            'LayoutControlGroup3
            '
            Me.LayoutControlGroup3.CustomizationFormText = "General"
            Me.LayoutControlGroup3.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem22, Me.LayoutControlItem3, Me.LayoutControlItem1, Me.EmptySpaceItem4})
            Me.LayoutControlGroup3.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup3.Name = "LayoutControlGroup3"
            Me.LayoutControlGroup3.Size = New System.Drawing.Size(604, 136)
            Me.LayoutControlGroup3.Text = "General"
            '
            'LayoutControlItem22
            '
            Me.LayoutControlItem22.Control = Me.CalcEdit_BackEnd_DTI
            Me.LayoutControlItem22.CustomizationFormText = "Back End DTI"
            Me.LayoutControlItem22.Location = New System.Drawing.Point(399, 24)
            Me.LayoutControlItem22.Name = "LayoutControlItem22"
            Me.LayoutControlItem22.Size = New System.Drawing.Size(205, 112)
            Me.LayoutControlItem22.Text = "Back End DTI"
            Me.LayoutControlItem22.TextSize = New System.Drawing.Size(83, 13)
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.LookUpEdit_housing_status
            Me.LayoutControlItem3.CustomizationFormText = "Housing Situation"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(399, 24)
            Me.LayoutControlItem3.Text = "Housing Situation"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(83, 13)
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.CalcEdit_FrontEnd_DTI
            Me.LayoutControlItem1.CustomizationFormText = "Front End DTI"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(399, 0)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(205, 24)
            Me.LayoutControlItem1.Text = "Front End DTI"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(83, 13)
            '
            'EmptySpaceItem4
            '
            Me.EmptySpaceItem4.AllowHotTrack = False
            Me.EmptySpaceItem4.CustomizationFormText = "EmptySpaceItem4"
            Me.EmptySpaceItem4.Location = New System.Drawing.Point(0, 24)
            Me.EmptySpaceItem4.Name = "EmptySpaceItem4"
            Me.EmptySpaceItem4.Size = New System.Drawing.Size(399, 112)
            Me.EmptySpaceItem4.Text = "EmptySpaceItem4"
            Me.EmptySpaceItem4.TextSize = New System.Drawing.Size(0, 0)
            '
            'HUD_Information
            '
            Me.HUD_Information.CustomizationFormText = "Items for H.U.D. Reports"
            Me.HUD_Information.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem6, Me.LayoutControlItem17, Me.LayoutControlItem19, Me.LayoutControlItem20, Me.LayoutControlItem21, Me.LayoutControlItem5, Me.LayoutControlItem2, Me.LayoutControlItem18, Me.LayoutControlItem14})
            Me.HUD_Information.Location = New System.Drawing.Point(0, 0)
            Me.HUD_Information.Name = "HUD_Information"
            Me.HUD_Information.Size = New System.Drawing.Size(604, 136)
            Me.HUD_Information.Text = "H.U.D."
            '
            'LayoutControlItem6
            '
            Me.LayoutControlItem6.Control = Me.CheckEdit_predatory_lending
            Me.LayoutControlItem6.CustomizationFormText = "Predatory Lending Victim"
            Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem6.Name = "LayoutControlItem6"
            Me.LayoutControlItem6.Size = New System.Drawing.Size(171, 23)
            Me.LayoutControlItem6.Text = "Predatory Lending Victim"
            Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem6.TextToControlDistance = 0
            Me.LayoutControlItem6.TextVisible = False
            '
            'LayoutControlItem17
            '
            Me.LayoutControlItem17.Control = Me.LookUpEdit_HUD_Assistance
            Me.LayoutControlItem17.CustomizationFormText = "HUD Assistance"
            Me.LayoutControlItem17.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem17.Name = "LayoutControlItem17"
            Me.LayoutControlItem17.Size = New System.Drawing.Size(303, 24)
            Me.LayoutControlItem17.Text = "HUD Assistance"
            Me.LayoutControlItem17.TextSize = New System.Drawing.Size(83, 13)
            '
            'LayoutControlItem19
            '
            Me.LayoutControlItem19.Control = Me.CheckEdit_HUD_DiscriminationVictim
            Me.LayoutControlItem19.CustomizationFormText = "Discrimination Victim"
            Me.LayoutControlItem19.Location = New System.Drawing.Point(0, 70)
            Me.LayoutControlItem19.Name = "LayoutControlItem19"
            Me.LayoutControlItem19.Size = New System.Drawing.Size(171, 23)
            Me.LayoutControlItem19.Text = "Discrimination Victim"
            Me.LayoutControlItem19.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem19.TextToControlDistance = 0
            Me.LayoutControlItem19.TextVisible = False
            '
            'LayoutControlItem20
            '
            Me.LayoutControlItem20.Control = Me.CheckEdit_HUD_FirstTimeHomeBuyer
            Me.LayoutControlItem20.CustomizationFormText = "LayoutControlItem20"
            Me.LayoutControlItem20.Location = New System.Drawing.Point(171, 70)
            Me.LayoutControlItem20.Name = "LayoutControlItem20"
            Me.LayoutControlItem20.Size = New System.Drawing.Size(433, 66)
            Me.LayoutControlItem20.Text = "LayoutControlItem20"
            Me.LayoutControlItem20.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem20.TextToControlDistance = 0
            Me.LayoutControlItem20.TextVisible = False
            '
            'LayoutControlItem21
            '
            Me.LayoutControlItem21.Control = Me.CheckEdit_LoanScam
            Me.LayoutControlItem21.CustomizationFormText = "LayoutControlItem21"
            Me.LayoutControlItem21.Location = New System.Drawing.Point(0, 47)
            Me.LayoutControlItem21.Name = "LayoutControlItem21"
            Me.LayoutControlItem21.Size = New System.Drawing.Size(171, 23)
            Me.LayoutControlItem21.Text = "LayoutControlItem21"
            Me.LayoutControlItem21.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem21.TextToControlDistance = 0
            Me.LayoutControlItem21.TextVisible = False
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Control = Me.CheckEdit_migrant_farm_worker
            Me.LayoutControlItem5.CustomizationFormText = "Migrant Farm Worker"
            Me.LayoutControlItem5.Location = New System.Drawing.Point(171, 47)
            Me.LayoutControlItem5.Name = "LayoutControlItem5"
            Me.LayoutControlItem5.Size = New System.Drawing.Size(433, 23)
            Me.LayoutControlItem5.Text = "Migrant Farm Worker"
            Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem5.TextToControlDistance = 0
            Me.LayoutControlItem5.TextVisible = False
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.fair_housing
            Me.LayoutControlItem2.CustomizationFormText = "Fair Housing Issue"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(171, 24)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(433, 23)
            Me.LayoutControlItem2.Text = "Fair Housing Issue"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem2.TextToControlDistance = 0
            Me.LayoutControlItem2.TextVisible = False
            '
            'LayoutControlItem18
            '
            Me.LayoutControlItem18.Control = Me.CalcEdit_OtherHUDFunding
            Me.LayoutControlItem18.CustomizationFormText = "Other Funding"
            Me.LayoutControlItem18.Location = New System.Drawing.Point(303, 0)
            Me.LayoutControlItem18.Name = "LayoutControlItem18"
            Me.LayoutControlItem18.Size = New System.Drawing.Size(301, 24)
            Me.LayoutControlItem18.Text = "Other Funding"
            Me.LayoutControlItem18.TextSize = New System.Drawing.Size(83, 13)
            '
            'LayoutControlItem14
            '
            Me.LayoutControlItem14.Control = Me.CheckEdit_colonias
            Me.LayoutControlItem14.CustomizationFormText = "Colonias Resident"
            Me.LayoutControlItem14.Location = New System.Drawing.Point(0, 93)
            Me.LayoutControlItem14.Name = "LayoutControlItem14"
            Me.LayoutControlItem14.Size = New System.Drawing.Size(171, 43)
            Me.LayoutControlItem14.Text = "Colonias Resident"
            Me.LayoutControlItem14.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem14.TextToControlDistance = 0
            Me.LayoutControlItem14.TextVisible = False
            '
            'LayoutControlGroup2
            '
            Me.LayoutControlGroup2.CustomizationFormText = "H.E.C.M. Information"
            Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem13, Me.LayoutControlItem7, Me.LayoutControlItem11, Me.EmptySpaceItem1})
            Me.LayoutControlGroup2.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
            Me.LayoutControlGroup2.Size = New System.Drawing.Size(604, 136)
            Me.LayoutControlGroup2.Text = "H.E.C.M."
            '
            'LayoutControlItem13
            '
            Me.LayoutControlItem13.Control = Me.TextEdit_HECM_Certificate_ID
            Me.LayoutControlItem13.CustomizationFormText = "HECM Certificate ID"
            Me.LayoutControlItem13.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem13.Name = "LayoutControlItem13"
            Me.LayoutControlItem13.Size = New System.Drawing.Size(268, 24)
            Me.LayoutControlItem13.Text = "Certificate ID"
            Me.LayoutControlItem13.TextSize = New System.Drawing.Size(83, 13)
            '
            'LayoutControlItem7
            '
            Me.LayoutControlItem7.Control = Me.DateEdit_HECM_Certificate_Date
            Me.LayoutControlItem7.CustomizationFormText = "HECM Issue Date"
            Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem7.Name = "LayoutControlItem7"
            Me.LayoutControlItem7.Size = New System.Drawing.Size(268, 24)
            Me.LayoutControlItem7.Text = "Issue Date"
            Me.LayoutControlItem7.TextSize = New System.Drawing.Size(83, 13)
            '
            'LayoutControlItem11
            '
            Me.LayoutControlItem11.Control = Me.DateEdit_HECM_Certificate_Expires
            Me.LayoutControlItem11.CustomizationFormText = "HECM Expiration Date"
            Me.LayoutControlItem11.Location = New System.Drawing.Point(0, 48)
            Me.LayoutControlItem11.Name = "LayoutControlItem11"
            Me.LayoutControlItem11.Size = New System.Drawing.Size(268, 88)
            Me.LayoutControlItem11.Text = "Expiration Date"
            Me.LayoutControlItem11.TextSize = New System.Drawing.Size(83, 13)
            '
            'EmptySpaceItem1
            '
            Me.EmptySpaceItem1.AllowHotTrack = False
            Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Location = New System.Drawing.Point(268, 0)
            Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Size = New System.Drawing.Size(336, 136)
            Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
            Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
            '
            'NFMCP_Information
            '
            Me.NFMCP_Information.CustomizationFormText = "Items for NFCMP Reports"
            Me.NFMCP_Information.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem9, Me.LayoutControlItem10, Me.LayoutControlItem12})
            Me.NFMCP_Information.Location = New System.Drawing.Point(0, 0)
            Me.NFMCP_Information.Name = "NFMCP_Information"
            Me.NFMCP_Information.Size = New System.Drawing.Size(604, 136)
            Me.NFMCP_Information.Text = "N.F.M.C.P."
            '
            'LayoutControlItem9
            '
            Me.LayoutControlItem9.Control = Me.CheckEdit_nfmcp_privacy_policy
            Me.LayoutControlItem9.CustomizationFormText = "NFMCP - privacy policy"
            Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem9.Name = "LayoutControlItem9"
            Me.LayoutControlItem9.Size = New System.Drawing.Size(604, 23)
            Me.LayoutControlItem9.Text = "NFMCP - privacy policy"
            Me.LayoutControlItem9.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem9.TextToControlDistance = 0
            Me.LayoutControlItem9.TextVisible = False
            '
            'LayoutControlItem10
            '
            Me.LayoutControlItem10.Control = Me.CheckEdit_nfmcp_decline_authorization
            Me.LayoutControlItem10.CustomizationFormText = "NFMCP - decline authorization"
            Me.LayoutControlItem10.Location = New System.Drawing.Point(0, 47)
            Me.LayoutControlItem10.Name = "LayoutControlItem10"
            Me.LayoutControlItem10.Size = New System.Drawing.Size(604, 89)
            Me.LayoutControlItem10.Text = "NFMCP - decline authorization"
            Me.LayoutControlItem10.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem10.TextToControlDistance = 0
            Me.LayoutControlItem10.TextVisible = False
            '
            'LayoutControlItem12
            '
            Me.LayoutControlItem12.Control = Me.ComboBoxEdit_nfmcp_level
            Me.LayoutControlItem12.CustomizationFormText = "Level"
            Me.LayoutControlItem12.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem12.Name = "LayoutControlItem12"
            Me.LayoutControlItem12.Size = New System.Drawing.Size(604, 24)
            Me.LayoutControlItem12.Text = "Level"
            Me.LayoutControlItem12.TextSize = New System.Drawing.Size(83, 13)
            '
            'LayoutControlGroup_HECM_Default
            '
            Me.LayoutControlGroup_HECM_Default.CustomizationFormText = "HECM Default"
            Me.LayoutControlGroup_HECM_Default.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem4})
            Me.LayoutControlGroup_HECM_Default.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup_HECM_Default.Name = "LayoutControlGroup_HECM_Default"
            Me.LayoutControlGroup_HECM_Default.Size = New System.Drawing.Size(604, 136)
            Me.LayoutControlGroup_HECM_Default.Text = "HECM Default"
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.HECMDefault1
            Me.LayoutControlItem4.CustomizationFormText = "LayoutControlItem4"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(604, 136)
            Me.LayoutControlItem4.Text = "LayoutControlItem4"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem4.TextToControlDistance = 0
            Me.LayoutControlItem4.TextVisible = False
            '
            'LayoutControlItem8
            '
            Me.LayoutControlItem8.Control = Me.PropertyListControl1
            Me.LayoutControlItem8.CustomizationFormText = "Property List"
            Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem8.MaxSize = New System.Drawing.Size(0, 200)
            Me.LayoutControlItem8.MinSize = New System.Drawing.Size(1, 1)
            Me.LayoutControlItem8.Name = "LayoutControlItem8"
            Me.LayoutControlItem8.Size = New System.Drawing.Size(610, 200)
            Me.LayoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
            Me.LayoutControlItem8.Text = "Property List"
            Me.LayoutControlItem8.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem8.TextToControlDistance = 0
            Me.LayoutControlItem8.TextVisible = False
            '
            'LayoutControlItem_housing_type
            '
            Me.LayoutControlItem_housing_type.CustomizationFormText = "Housing Type"
            Me.LayoutControlItem_housing_type.Location = New System.Drawing.Point(262, 0)
            Me.LayoutControlItem_housing_type.Name = "LayoutControlItem_housing_type"
            Me.LayoutControlItem_housing_type.Size = New System.Drawing.Size(262, 24)
            Me.LayoutControlItem_housing_type.Text = "Housing Type"
            Me.LayoutControlItem_housing_type.TextSize = New System.Drawing.Size(83, 13)
            Me.LayoutControlItem_housing_type.TextToControlDistance = 5
            '
            'Page_Housing
            '
            Me.Controls.Add(Me.LayoutControl1)
            Me.Name = "Page_Housing"
            Me.Size = New System.Drawing.Size(612, 366)
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.HECMDefault1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_colonias.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_FrontEnd_DTI.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_BackEnd_DTI.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_LoanScam.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_HUD_FirstTimeHomeBuyer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_HUD_DiscriminationVictim.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CalcEdit_OtherHUDFunding.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_HUD_Assistance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextEdit_HECM_Certificate_ID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit_HECM_Certificate_Expires.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit_HECM_Certificate_Expires.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit_HECM_Certificate_Date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DateEdit_HECM_Certificate_Date.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ComboBoxEdit_nfmcp_level.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_nfmcp_decline_authorization.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_nfmcp_privacy_policy.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_housing_status.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PropertyListControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_predatory_lending.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_migrant_farm_worker.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.fair_housing.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TabbedControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.HUD_Information, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.NFMCP_Information, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup_HECM_Default, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_housing_type, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
#End Region

        ''' <summary>
        ''' Process the load of the information on the page selection
        ''' </summary>
        Public Overrides Sub ReadForm(bc As DebtPlus.LINQ.BusinessContext)
            UnRegisterHandlers()
            Try
                ' If we haven't read the housing record then now is the time to do it.
                If clientHousingRecord Is Nothing Then

                    ' Retrieve the housing record from the database. It should exist since it is created at the same time
                    ' as the client is created. However, punt if the record could not be read and attempt to create a new
                    ' empty record.
                    clientHousingRecord = bc.client_housings.Where(Function(s) s.Id = Context.ClientDs.ClientId).FirstOrDefault()
                    If clientHousingRecord Is Nothing Then
                        clientHousingRecord = DebtPlus.LINQ.Factory.Manufacture_client_housing(Context.ClientDs.ClientId)
                        bc.client_housings.InsertOnSubmit(clientHousingRecord)
                        bc.SubmitChanges()
                    End If
                End If

                ' Load the lookup controls with the proper datasets
                LookUpEdit_housing_status.Properties.DataSource = DebtPlus.LINQ.Cache.Housing_StatusType.getList()
                LookUpEdit_HUD_Assistance.Properties.DataSource = DebtPlus.LINQ.Cache.Housing_HUDAssistanceType.getList()

                ' Define the edit values accordingly
                LookUpEdit_housing_status.EditValue = clientHousingRecord.housing_status
                LookUpEdit_HUD_Assistance.EditValue = clientHousingRecord.HUD_Assistance
                TextEdit_HECM_Certificate_ID.EditValue = clientHousingRecord.HECM_Certificate_ID
                DateEdit_HECM_Certificate_Date.EditValue = clientHousingRecord.HECM_Certificate_Date
                DateEdit_HECM_Certificate_Expires.EditValue = clientHousingRecord.HECM_Certificate_Expires
                CheckEdit_colonias.EditValue = clientHousingRecord.HUD_colonias
                CheckEdit_LoanScam.EditValue = clientHousingRecord.HUD_LoanScam
                fair_housing.EditValue = clientHousingRecord.HUD_fair_housing
                CheckEdit_migrant_farm_worker.EditValue = clientHousingRecord.HUD_migrant_farm_worker
                CheckEdit_predatory_lending.EditValue = clientHousingRecord.HUD_predatory_lending
                CheckEdit_HUD_DiscriminationVictim.EditValue = clientHousingRecord.HUD_DiscriminationVictim
                CheckEdit_HUD_FirstTimeHomeBuyer.EditValue = clientHousingRecord.HUD_FirstTimeHomeBuyer
                CalcEdit_OtherHUDFunding.EditValue = clientHousingRecord.OtherHUDFunding
                CheckEdit_nfmcp_decline_authorization.EditValue = clientHousingRecord.NFMCP_decline_authorization
                CheckEdit_nfmcp_privacy_policy.EditValue = clientHousingRecord.NFMCP_privacy_policy
                CalcEdit_BackEnd_DTI.EditValue = clientHousingRecord.BackEnd_DTI
                CalcEdit_FrontEnd_DTI.EditValue = clientHousingRecord.FrontEnd_DTI

                Select Case clientHousingRecord.NFMCP_level
                    Case "1"
                        ComboBoxEdit_nfmcp_level.SelectedIndex = 1
                    Case "2"
                        ComboBoxEdit_nfmcp_level.SelectedIndex = 2
                    Case "3"
                        ComboBoxEdit_nfmcp_level.SelectedIndex = 3
                    Case "4a"
                        ComboBoxEdit_nfmcp_level.SelectedIndex = 4
                    Case "4b"
                        ComboBoxEdit_nfmcp_level.SelectedIndex = 5
                    Case "5"
                        ComboBoxEdit_nfmcp_level.SelectedIndex = 6
                    Case "5a"
                        ComboBoxEdit_nfmcp_level.SelectedIndex = 7
                    Case "5b"
                        ComboBoxEdit_nfmcp_level.SelectedIndex = 8
                    Case Else
                        ComboBoxEdit_nfmcp_level.SelectedIndex = 0
                End Select

                ' Display the properties
                PropertyListControl1.ReadForm(clientHousingRecord)
                HECMDefault1.ReadForm(clientHousingRecord)

                ' Force a refresh of the display values
                CalcEdit_FrontEnd_DTI.Text = FormatPercent(CalcEdit_FrontEnd_DTI.EditValue)
                CalcEdit_BackEnd_DTI.Text = FormatPercent(CalcEdit_BackEnd_DTI.EditValue)

            Catch ex As Exception
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading client_housing data")
            Finally
                RegisterHandlers()
            End Try
        End Sub

        Private Function GetDouble(value As Object) As System.Nullable(Of Double)

            ' If the value is missing then the answer is missing
            If value Is Nothing Then
                Return Nothing
            End If

            ' If the value is simply a double then return it
            If TypeOf value Is Double Then
                Return New System.Nullable(Of Double)(DirectCast(value, Double))
            End If

            ' Decimal numbers are also passed
            If TypeOf value Is Decimal Then
                Return New System.Nullable(Of Double)(Convert.ToDouble(DirectCast(value, Decimal)))
            End If

            ' If the value is a string then remove the non-decimal characters
            If TypeOf value Is String Then
                Dim strItem As String = Convert.ToString(value).Trim().Replace("%", "")
                Dim dbl As Double
                If Double.TryParse(strItem, dbl) Then
                    Return New System.Nullable(Of Double)(dbl)
                End If
            End If

            Return Nothing
        End Function

        ''' <summary>
        ''' Save the form data
        ''' </summary>
        Public Overrides Sub SaveForm(bc As DebtPlus.LINQ.BusinessContext)

            ' Save the property information
            PropertyListControl1.SaveForm(bc)
            HECMDefault1.SaveForm(bc)

            ' Convert the editing controls back to the record setting from the form.
            Select Case ComboBoxEdit_nfmcp_level.SelectedIndex
                Case 1
                    clientHousingRecord.NFMCP_level = "1"
                Case 2
                    clientHousingRecord.NFMCP_level = "2"
                Case 3
                    clientHousingRecord.NFMCP_level = "3"
                Case 4
                    clientHousingRecord.NFMCP_level = "4a"
                Case 5
                    clientHousingRecord.NFMCP_level = "4b"
                Case 6
                    clientHousingRecord.NFMCP_level = "5"
                Case 7
                    clientHousingRecord.NFMCP_level = "5a"
                Case 8
                    clientHousingRecord.NFMCP_level = "5b"
                Case Else
                    clientHousingRecord.NFMCP_level = String.Empty
            End Select

            clientHousingRecord.housing_status = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_housing_status.EditValue).GetValueOrDefault()
            clientHousingRecord.HUD_Assistance = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_HUD_Assistance.EditValue)
            clientHousingRecord.HECM_Certificate_ID = DebtPlus.Utils.Nulls.v_String(TextEdit_HECM_Certificate_ID.EditValue)
            clientHousingRecord.HECM_Certificate_Date = DebtPlus.Utils.Nulls.v_DateTime(DateEdit_HECM_Certificate_Date.EditValue)
            clientHousingRecord.HECM_Certificate_Expires = DebtPlus.Utils.Nulls.v_DateTime(DateEdit_HECM_Certificate_Expires.EditValue)
            clientHousingRecord.HUD_colonias = CheckEdit_colonias.Checked
            clientHousingRecord.HUD_LoanScam = CheckEdit_LoanScam.Checked
            clientHousingRecord.HUD_fair_housing = fair_housing.Checked
            clientHousingRecord.HUD_migrant_farm_worker = CheckEdit_migrant_farm_worker.Checked
            clientHousingRecord.HUD_predatory_lending = CheckEdit_predatory_lending.Checked
            clientHousingRecord.HUD_DiscriminationVictim = CheckEdit_HUD_DiscriminationVictim.Checked
            clientHousingRecord.HUD_FirstTimeHomeBuyer = CheckEdit_HUD_FirstTimeHomeBuyer.Checked
            clientHousingRecord.OtherHUDFunding = DebtPlus.Utils.Nulls.v_Decimal(CalcEdit_OtherHUDFunding.EditValue).GetValueOrDefault()
            clientHousingRecord.NFMCP_decline_authorization = CheckEdit_nfmcp_decline_authorization.Checked
            clientHousingRecord.NFMCP_privacy_policy = CheckEdit_nfmcp_privacy_policy.Checked
            clientHousingRecord.BackEnd_DTI = GetDouble(CalcEdit_BackEnd_DTI.EditValue).GetValueOrDefault(0L)
            clientHousingRecord.FrontEnd_DTI = GetDouble(CalcEdit_FrontEnd_DTI.EditValue).GetValueOrDefault(0L)

            Try
                ' Update the housing record appropriately
                Dim q As client_housing = bc.client_housings.Where(Function(s) s.Id = Context.ClientDs.ClientId).FirstOrDefault()
                If q IsNot Nothing Then
                    q.BackEnd_DTI = clientHousingRecord.BackEnd_DTI
                    q.DSA_CaseID = clientHousingRecord.DSA_CaseID
                    q.DSA_NoDsaReason = clientHousingRecord.DSA_NoDsaReason
                    q.DSA_Program = clientHousingRecord.DSA_Program
                    q.DSA_ProgramBenefit = clientHousingRecord.DSA_ProgramBenefit
                    q.DSA_Specialist = clientHousingRecord.DSA_Specialist
                    q.FrontEnd_DTI = clientHousingRecord.FrontEnd_DTI
                    q.HECM_Certificate_Date = clientHousingRecord.HECM_Certificate_Date
                    q.HECM_Certificate_Date = clientHousingRecord.HECM_Certificate_Date
                    q.HECM_Certificate_Expires = clientHousingRecord.HECM_Certificate_Expires
                    q.HECM_Certificate_Expires = clientHousingRecord.HECM_Certificate_Expires
                    q.HECM_Certificate_ID = clientHousingRecord.HECM_Certificate_ID
                    q.housing_status = clientHousingRecord.housing_status
                    q.HUD_Assistance = clientHousingRecord.HUD_Assistance
                    q.HUD_colonias = clientHousingRecord.HUD_colonias
                    q.HUD_DiscriminationVictim = clientHousingRecord.HUD_DiscriminationVictim
                    q.HUD_fair_housing = clientHousingRecord.HUD_fair_housing
                    q.HUD_FirstTimeHomeBuyer = clientHousingRecord.HUD_FirstTimeHomeBuyer
                    q.HUD_FirstTimeHomeBuyer = clientHousingRecord.HUD_FirstTimeHomeBuyer
                    q.HUD_grant = clientHousingRecord.HUD_grant
                    q.HUD_home_ownership_voucher = clientHousingRecord.HUD_home_ownership_voucher
                    q.HUD_housing_voucher = clientHousingRecord.HUD_housing_voucher
                    q.HUD_LoanScam = clientHousingRecord.HUD_LoanScam
                    q.HUD_migrant_farm_worker = clientHousingRecord.HUD_migrant_farm_worker
                    q.HUD_predatory_lending = clientHousingRecord.HUD_predatory_lending
                    q.NFMCP_decline_authorization = clientHousingRecord.NFMCP_decline_authorization
                    q.NFMCP_level = clientHousingRecord.NFMCP_level
                    q.NFMCP_privacy_policy = clientHousingRecord.NFMCP_privacy_policy
                    q.OtherHUDFunding = clientHousingRecord.OtherHUDFunding

                    bc.SubmitChanges()
                End If

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating client_housing table")
            End Try
        End Sub

        Private Function FormatPercent(value As Object) As String
            Dim dbl As System.Nullable(Of Double) = GetDouble(value)
            If dbl.HasValue Then
                Return String.Format("{0:f3}", dbl.Value) + "%"
            End If
            Return String.Empty
        End Function

        Private Sub CalcEdit_NFCMP_DTI_CustomDisplayText(sender As Object, e As CustomDisplayTextEventArgs)
            e.DisplayText = FormatPercent(e.Value)
        End Sub
    End Class
End Namespace
