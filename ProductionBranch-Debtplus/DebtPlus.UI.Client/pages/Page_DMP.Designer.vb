﻿Imports DebtPlus.UI.Client.controls

Namespace pages

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Page_DMP
        Inherits ControlBaseClient

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.lbl_program_months = New DevExpress.XtraEditors.LabelControl()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.lbl_drop_date = New DevExpress.XtraEditors.LabelControl()
            Me.spn_disbursement_date = New DevExpress.XtraEditors.SpinEdit()
            Me.lbl_Drop_Reason = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_active_status_date = New DevExpress.XtraEditors.LabelControl()
            Me.lk_bankruptcy_class = New DevExpress.XtraEditors.LookUpEdit()
            Me.chk_hold_disbursements = New DevExpress.XtraEditors.CheckEdit()
            Me.chk_mortgage_problems = New DevExpress.XtraEditors.CheckEdit()
            Me.dt_start_date = New DevExpress.XtraEditors.DateEdit()
            Me.chk_stack_proration = New DevExpress.XtraEditors.CheckEdit()
            Me.lbl_deposit_in_trust = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_reserved_in_trust = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_held_in_trust = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_total_deposits = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_total_disb_factors = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_total_payments = New DevExpress.XtraEditors.LabelControl()
            Me.lk_active_status = New DevExpress.XtraEditors.LookUpEdit()
            Me.lk_client_status = New DevExpress.XtraEditors.LookUpEdit()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.LayoutControlGroup3 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem17 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.item0 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.item2 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.item3 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.item1 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.LayoutControlItem_drop_date = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_drop_reason = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.spn_disbursement_date.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lk_bankruptcy_class.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.chk_hold_disbursements.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.chk_mortgage_problems.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_start_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dt_start_date.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.chk_stack_proration.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lk_active_status.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lk_client_status.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.item0, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.item2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.item3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.item1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_drop_date, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_drop_reason, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'lbl_program_months
            '
            Me.lbl_program_months.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_program_months.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_program_months.Location = New System.Drawing.Point(482, 12)
            Me.lbl_program_months.Name = "lbl_program_months"
            Me.lbl_program_months.Size = New System.Drawing.Size(83, 13)
            Me.lbl_program_months.StyleController = Me.LayoutControl1
            Me.lbl_program_months.TabIndex = 20
            Me.lbl_program_months.Text = "0"
            Me.lbl_program_months.UseMnemonic = False
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.lbl_drop_date)
            Me.LayoutControl1.Controls.Add(Me.spn_disbursement_date)
            Me.LayoutControl1.Controls.Add(Me.lbl_Drop_Reason)
            Me.LayoutControl1.Controls.Add(Me.lbl_active_status_date)
            Me.LayoutControl1.Controls.Add(Me.lk_bankruptcy_class)
            Me.LayoutControl1.Controls.Add(Me.chk_hold_disbursements)
            Me.LayoutControl1.Controls.Add(Me.chk_mortgage_problems)
            Me.LayoutControl1.Controls.Add(Me.dt_start_date)
            Me.LayoutControl1.Controls.Add(Me.chk_stack_proration)
            Me.LayoutControl1.Controls.Add(Me.lbl_deposit_in_trust)
            Me.LayoutControl1.Controls.Add(Me.lbl_reserved_in_trust)
            Me.LayoutControl1.Controls.Add(Me.lbl_held_in_trust)
            Me.LayoutControl1.Controls.Add(Me.lbl_total_deposits)
            Me.LayoutControl1.Controls.Add(Me.lbl_total_disb_factors)
            Me.LayoutControl1.Controls.Add(Me.lbl_total_payments)
            Me.LayoutControl1.Controls.Add(Me.lk_active_status)
            Me.LayoutControl1.Controls.Add(Me.lbl_program_months)
            Me.LayoutControl1.Controls.Add(Me.lk_client_status)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(447, 180, 250, 350)
            Me.LayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = True
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(577, 336)
            Me.LayoutControl1.TabIndex = 33
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'lbl_drop_date
            '
            Me.lbl_drop_date.Location = New System.Drawing.Point(126, 149)
            Me.lbl_drop_date.Name = "lbl_drop_date"
            Me.lbl_drop_date.Size = New System.Drawing.Size(74, 13)
            Me.lbl_drop_date.StyleController = Me.LayoutControl1
            Me.lbl_drop_date.TabIndex = 14
            Me.lbl_drop_date.Text = "NONE Specified"
            Me.lbl_drop_date.UseMnemonic = False
            Me.lbl_drop_date.Visible = False
            '
            'spn_disbursement_date
            '
            Me.spn_disbursement_date.EditValue = New Decimal(New Integer() {1, 0, 0, 0})
            Me.spn_disbursement_date.Location = New System.Drawing.Point(126, 60)
            Me.spn_disbursement_date.Name = "spn_disbursement_date"
            Me.spn_disbursement_date.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.spn_disbursement_date.Properties.IsFloatValue = False
            Me.spn_disbursement_date.Properties.Mask.EditMask = "N00"
            Me.spn_disbursement_date.Properties.MaxValue = New Decimal(New Integer() {31, 0, 0, 0})
            Me.spn_disbursement_date.Properties.MinValue = New Decimal(New Integer() {1, 0, 0, 0})
            Me.spn_disbursement_date.Size = New System.Drawing.Size(142, 20)
            Me.spn_disbursement_date.StyleController = Me.LayoutControl1
            Me.spn_disbursement_date.TabIndex = 6
            '
            'lbl_Drop_Reason
            '
            Me.lbl_Drop_Reason.Location = New System.Drawing.Point(126, 132)
            Me.lbl_Drop_Reason.Name = "lbl_Drop_Reason"
            Me.lbl_Drop_Reason.Size = New System.Drawing.Size(74, 13)
            Me.lbl_Drop_Reason.StyleController = Me.LayoutControl1
            Me.lbl_Drop_Reason.TabIndex = 12
            Me.lbl_Drop_Reason.Text = "NONE Specified"
            Me.lbl_Drop_Reason.UseMnemonic = False
            Me.lbl_Drop_Reason.Visible = False
            '
            'lbl_active_status_date
            '
            Me.lbl_active_status_date.Location = New System.Drawing.Point(273, 36)
            Me.lbl_active_status_date.Name = "lbl_active_status_date"
            Me.lbl_active_status_date.Size = New System.Drawing.Size(91, 13)
            Me.lbl_active_status_date.StyleController = Me.LayoutControl1
            Me.lbl_active_status_date.TabIndex = 4
            Me.lbl_active_status_date.UseMnemonic = False
            Me.lbl_active_status_date.Visible = False
            '
            'lk_bankruptcy_class
            '
            Me.lk_bankruptcy_class.Location = New System.Drawing.Point(126, 108)
            Me.lk_bankruptcy_class.Name = "lk_bankruptcy_class"
            Me.lk_bankruptcy_class.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.lk_bankruptcy_class.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lk_bankruptcy_class.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 25, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 75, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.lk_bankruptcy_class.Properties.DisplayMember = "description"
            Me.lk_bankruptcy_class.Properties.NullText = ""
            Me.lk_bankruptcy_class.Properties.ShowFooter = False
            Me.lk_bankruptcy_class.Properties.ShowHeader = False
            Me.lk_bankruptcy_class.Properties.SortColumnIndex = 1
            Me.lk_bankruptcy_class.Properties.ValueMember = "Id"
            Me.lk_bankruptcy_class.Size = New System.Drawing.Size(142, 20)
            Me.lk_bankruptcy_class.StyleController = Me.LayoutControl1
            Me.lk_bankruptcy_class.TabIndex = 10
            '
            'chk_hold_disbursements
            '
            Me.chk_hold_disbursements.Location = New System.Drawing.Point(12, 207)
            Me.chk_hold_disbursements.Name = "chk_hold_disbursements"
            Me.chk_hold_disbursements.Properties.Caption = "Hold all Disbursements"
            Me.chk_hold_disbursements.Size = New System.Drawing.Size(193, 20)
            Me.chk_hold_disbursements.StyleController = Me.LayoutControl1
            Me.chk_hold_disbursements.TabIndex = 16
            '
            'chk_mortgage_problems
            '
            Me.chk_mortgage_problems.Location = New System.Drawing.Point(12, 231)
            Me.chk_mortgage_problems.Name = "chk_mortgage_problems"
            Me.chk_mortgage_problems.Properties.Caption = "Started with Housing Problems"
            Me.chk_mortgage_problems.Size = New System.Drawing.Size(193, 20)
            Me.chk_mortgage_problems.StyleController = Me.LayoutControl1
            Me.chk_mortgage_problems.TabIndex = 17
            '
            'dt_start_date
            '
            Me.dt_start_date.EditValue = Nothing
            Me.dt_start_date.Location = New System.Drawing.Point(126, 84)
            Me.dt_start_date.Name = "dt_start_date"
            Me.dt_start_date.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.dt_start_date.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.dt_start_date.Size = New System.Drawing.Size(142, 20)
            Me.dt_start_date.StyleController = Me.LayoutControl1
            Me.dt_start_date.TabIndex = 8
            '
            'chk_stack_proration
            '
            Me.chk_stack_proration.Location = New System.Drawing.Point(12, 183)
            Me.chk_stack_proration.Name = "chk_stack_proration"
            Me.chk_stack_proration.Properties.Caption = "Stack creditors for prorate function"
            Me.chk_stack_proration.Size = New System.Drawing.Size(193, 20)
            Me.chk_stack_proration.StyleController = Me.LayoutControl1
            Me.chk_stack_proration.TabIndex = 15
            '
            'lbl_deposit_in_trust
            '
            Me.lbl_deposit_in_trust.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_deposit_in_trust.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_deposit_in_trust.Location = New System.Drawing.Point(482, 114)
            Me.lbl_deposit_in_trust.Name = "lbl_deposit_in_trust"
            Me.lbl_deposit_in_trust.Size = New System.Drawing.Size(83, 13)
            Me.lbl_deposit_in_trust.StyleController = Me.LayoutControl1
            Me.lbl_deposit_in_trust.TabIndex = 32
            Me.lbl_deposit_in_trust.Text = "$0.00"
            Me.lbl_deposit_in_trust.UseMnemonic = False
            '
            'lbl_reserved_in_trust
            '
            Me.lbl_reserved_in_trust.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_reserved_in_trust.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_reserved_in_trust.Location = New System.Drawing.Point(482, 97)
            Me.lbl_reserved_in_trust.Name = "lbl_reserved_in_trust"
            Me.lbl_reserved_in_trust.Size = New System.Drawing.Size(83, 13)
            Me.lbl_reserved_in_trust.StyleController = Me.LayoutControl1
            Me.lbl_reserved_in_trust.TabIndex = 30
            Me.lbl_reserved_in_trust.Text = "$0.00"
            Me.lbl_reserved_in_trust.UseMnemonic = False
            '
            'lbl_held_in_trust
            '
            Me.lbl_held_in_trust.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_held_in_trust.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_held_in_trust.Location = New System.Drawing.Point(482, 80)
            Me.lbl_held_in_trust.Name = "lbl_held_in_trust"
            Me.lbl_held_in_trust.Size = New System.Drawing.Size(83, 13)
            Me.lbl_held_in_trust.StyleController = Me.LayoutControl1
            Me.lbl_held_in_trust.TabIndex = 28
            Me.lbl_held_in_trust.Text = "$0.00"
            Me.lbl_held_in_trust.UseMnemonic = False
            '
            'lbl_total_deposits
            '
            Me.lbl_total_deposits.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_total_deposits.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_total_deposits.Location = New System.Drawing.Point(482, 63)
            Me.lbl_total_deposits.Name = "lbl_total_deposits"
            Me.lbl_total_deposits.Size = New System.Drawing.Size(83, 13)
            Me.lbl_total_deposits.StyleController = Me.LayoutControl1
            Me.lbl_total_deposits.TabIndex = 26
            Me.lbl_total_deposits.Text = "$0.00"
            Me.lbl_total_deposits.UseMnemonic = False
            '
            'lbl_total_disb_factors
            '
            Me.lbl_total_disb_factors.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_total_disb_factors.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_total_disb_factors.Location = New System.Drawing.Point(482, 46)
            Me.lbl_total_disb_factors.Name = "lbl_total_disb_factors"
            Me.lbl_total_disb_factors.Size = New System.Drawing.Size(83, 13)
            Me.lbl_total_disb_factors.StyleController = Me.LayoutControl1
            Me.lbl_total_disb_factors.TabIndex = 24
            Me.lbl_total_disb_factors.Text = "$0.00"
            Me.lbl_total_disb_factors.UseMnemonic = False
            '
            'lbl_total_payments
            '
            Me.lbl_total_payments.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_total_payments.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_total_payments.Location = New System.Drawing.Point(482, 29)
            Me.lbl_total_payments.Name = "lbl_total_payments"
            Me.lbl_total_payments.Size = New System.Drawing.Size(83, 13)
            Me.lbl_total_payments.StyleController = Me.LayoutControl1
            Me.lbl_total_payments.TabIndex = 22
            Me.lbl_total_payments.Text = "$0.00"
            Me.lbl_total_payments.UseMnemonic = False
            '
            'lk_active_status
            '
            Me.lk_active_status.Location = New System.Drawing.Point(126, 36)
            Me.lk_active_status.Name = "lk_active_status"
            Me.lk_active_status.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.lk_active_status.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lk_active_status.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 25, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 75, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.lk_active_status.Properties.DisplayMember = "description"
            Me.lk_active_status.Properties.NullText = ""
            Me.lk_active_status.Properties.ShowFooter = False
            Me.lk_active_status.Properties.ShowHeader = False
            Me.lk_active_status.Properties.SortColumnIndex = 1
            Me.lk_active_status.Properties.ValueMember = "Id"
            Me.lk_active_status.Size = New System.Drawing.Size(143, 20)
            Me.lk_active_status.StyleController = Me.LayoutControl1
            Me.lk_active_status.TabIndex = 3
            '
            'lk_client_status
            '
            Me.lk_client_status.Location = New System.Drawing.Point(126, 12)
            Me.lk_client_status.Name = "lk_client_status"
            Me.lk_client_status.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.lk_client_status.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lk_client_status.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 25, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 75, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.lk_client_status.Properties.DisplayMember = "description"
            Me.lk_client_status.Properties.NullText = ""
            Me.lk_client_status.Properties.ShowFooter = False
            Me.lk_client_status.Properties.ShowHeader = False
            Me.lk_client_status.Properties.SortColumnIndex = 1
            Me.lk_client_status.Properties.ValueMember = "Id"
            Me.lk_client_status.Size = New System.Drawing.Size(143, 20)
            Me.lk_client_status.StyleController = Me.LayoutControl1
            Me.lk_client_status.TabIndex = 1
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup1.GroupBordersVisible = False
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem6, Me.LayoutControlItem7, Me.LayoutControlItem5, Me.EmptySpaceItem1, Me.LayoutControlGroup3, Me.item1, Me.LayoutControlItem_drop_date, Me.LayoutControlItem_drop_reason, Me.EmptySpaceItem2})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(577, 336)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem6
            '
            Me.LayoutControlItem6.Control = Me.chk_hold_disbursements
            Me.LayoutControlItem6.CustomizationFormText = "LayoutControlItem6"
            Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 195)
            Me.LayoutControlItem6.Name = "LayoutControlItem6"
            Me.LayoutControlItem6.Size = New System.Drawing.Size(197, 24)
            Me.LayoutControlItem6.Text = "LayoutControlItem6"
            Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem6.TextToControlDistance = 0
            Me.LayoutControlItem6.TextVisible = False
            '
            'LayoutControlItem7
            '
            Me.LayoutControlItem7.Control = Me.chk_stack_proration
            Me.LayoutControlItem7.CustomizationFormText = "LayoutControlItem7"
            Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 171)
            Me.LayoutControlItem7.Name = "LayoutControlItem7"
            Me.LayoutControlItem7.Size = New System.Drawing.Size(197, 24)
            Me.LayoutControlItem7.Text = "LayoutControlItem7"
            Me.LayoutControlItem7.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem7.TextToControlDistance = 0
            Me.LayoutControlItem7.TextVisible = False
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Control = Me.chk_mortgage_problems
            Me.LayoutControlItem5.CustomizationFormText = "LayoutControlItem5"
            Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 219)
            Me.LayoutControlItem5.Name = "LayoutControlItem5"
            Me.LayoutControlItem5.Size = New System.Drawing.Size(197, 24)
            Me.LayoutControlItem5.Text = "LayoutControlItem5"
            Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem5.TextToControlDistance = 0
            Me.LayoutControlItem5.TextVisible = False
            '
            'EmptySpaceItem1
            '
            Me.EmptySpaceItem1.AllowHotTrack = False
            Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 154)
            Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Size = New System.Drawing.Size(197, 17)
            Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
            Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
            '
            'LayoutControlGroup3
            '
            Me.LayoutControlGroup3.CustomizationFormText = "LayoutControlGroup3"
            Me.LayoutControlGroup3.GroupBordersVisible = False
            Me.LayoutControlGroup3.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem8, Me.LayoutControlItem9, Me.LayoutControlItem10, Me.LayoutControlItem11, Me.LayoutControlItem12, Me.LayoutControlItem13, Me.LayoutControlItem14, Me.LayoutControlItem15, Me.LayoutControlItem16, Me.LayoutControlItem18, Me.LayoutControlItem19, Me.LayoutControlItem20, Me.LayoutControlItem17, Me.item0, Me.item2, Me.item3, Me.EmptySpaceItem3})
            Me.LayoutControlGroup3.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup3.Name = "LayoutControlGroup3"
            Me.LayoutControlGroup3.Size = New System.Drawing.Size(557, 120)
            Me.LayoutControlGroup3.Text = "LayoutControlGroup3"
            '
            'LayoutControlItem8
            '
            Me.LayoutControlItem8.Control = Me.lbl_deposit_in_trust
            Me.LayoutControlItem8.CustomizationFormText = "Unposted Deposits:"
            Me.LayoutControlItem8.Location = New System.Drawing.Point(356, 102)
            Me.LayoutControlItem8.Name = "LayoutControlItem8"
            Me.LayoutControlItem8.Size = New System.Drawing.Size(201, 18)
            Me.LayoutControlItem8.Text = "Unposted Deposits:"
            Me.LayoutControlItem8.TextSize = New System.Drawing.Size(111, 13)
            '
            'LayoutControlItem9
            '
            Me.LayoutControlItem9.Control = Me.lbl_reserved_in_trust
            Me.LayoutControlItem9.CustomizationFormText = "Trust Funds on Hold"
            Me.LayoutControlItem9.Location = New System.Drawing.Point(356, 85)
            Me.LayoutControlItem9.Name = "LayoutControlItem9"
            Me.LayoutControlItem9.Size = New System.Drawing.Size(201, 17)
            Me.LayoutControlItem9.Text = "Trust Funds on Hold"
            Me.LayoutControlItem9.TextSize = New System.Drawing.Size(111, 13)
            '
            'LayoutControlItem10
            '
            Me.LayoutControlItem10.Control = Me.lbl_held_in_trust
            Me.LayoutControlItem10.CustomizationFormText = "Trust Funds"
            Me.LayoutControlItem10.Location = New System.Drawing.Point(356, 68)
            Me.LayoutControlItem10.Name = "LayoutControlItem10"
            Me.LayoutControlItem10.Size = New System.Drawing.Size(201, 17)
            Me.LayoutControlItem10.Text = "Trust Funds"
            Me.LayoutControlItem10.TextSize = New System.Drawing.Size(111, 13)
            '
            'LayoutControlItem11
            '
            Me.LayoutControlItem11.Control = Me.lbl_total_deposits
            Me.LayoutControlItem11.CustomizationFormText = "Expected Deposit:"
            Me.LayoutControlItem11.Location = New System.Drawing.Point(356, 51)
            Me.LayoutControlItem11.Name = "LayoutControlItem11"
            Me.LayoutControlItem11.Size = New System.Drawing.Size(201, 17)
            Me.LayoutControlItem11.Text = "Expected Deposit:"
            Me.LayoutControlItem11.TextSize = New System.Drawing.Size(111, 13)
            '
            'LayoutControlItem12
            '
            Me.LayoutControlItem12.Control = Me.lbl_total_disb_factors
            Me.LayoutControlItem12.CustomizationFormText = "Total Disb Factors:"
            Me.LayoutControlItem12.Location = New System.Drawing.Point(356, 34)
            Me.LayoutControlItem12.Name = "LayoutControlItem12"
            Me.LayoutControlItem12.Size = New System.Drawing.Size(201, 17)
            Me.LayoutControlItem12.Text = "Total Disb Factors:"
            Me.LayoutControlItem12.TextSize = New System.Drawing.Size(111, 13)
            '
            'LayoutControlItem13
            '
            Me.LayoutControlItem13.Control = Me.lbl_total_payments
            Me.LayoutControlItem13.CustomizationFormText = "Total Paid to Creditors:"
            Me.LayoutControlItem13.Location = New System.Drawing.Point(356, 17)
            Me.LayoutControlItem13.Name = "LayoutControlItem13"
            Me.LayoutControlItem13.Size = New System.Drawing.Size(201, 17)
            Me.LayoutControlItem13.Text = "Total Paid to Creditors:"
            Me.LayoutControlItem13.TextSize = New System.Drawing.Size(111, 13)
            '
            'LayoutControlItem14
            '
            Me.LayoutControlItem14.Control = Me.lbl_program_months
            Me.LayoutControlItem14.CustomizationFormText = "Months on Program:"
            Me.LayoutControlItem14.Location = New System.Drawing.Point(356, 0)
            Me.LayoutControlItem14.Name = "LayoutControlItem14"
            Me.LayoutControlItem14.Size = New System.Drawing.Size(201, 17)
            Me.LayoutControlItem14.Text = "Months on Program:"
            Me.LayoutControlItem14.TextSize = New System.Drawing.Size(111, 13)
            '
            'LayoutControlItem15
            '
            Me.LayoutControlItem15.Control = Me.lk_client_status
            Me.LayoutControlItem15.CustomizationFormText = "Client Status"
            Me.LayoutControlItem15.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem15.Name = "LayoutControlItem15"
            Me.LayoutControlItem15.Size = New System.Drawing.Size(261, 24)
            Me.LayoutControlItem15.Text = "Client Status"
            Me.LayoutControlItem15.TextSize = New System.Drawing.Size(111, 13)
            '
            'LayoutControlItem16
            '
            Me.LayoutControlItem16.Control = Me.lk_active_status
            Me.LayoutControlItem16.CustomizationFormText = "Active Status"
            Me.LayoutControlItem16.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem16.Name = "LayoutControlItem16"
            Me.LayoutControlItem16.Size = New System.Drawing.Size(261, 24)
            Me.LayoutControlItem16.Text = "Active Status"
            Me.LayoutControlItem16.TextSize = New System.Drawing.Size(111, 13)
            '
            'LayoutControlItem18
            '
            Me.LayoutControlItem18.Control = Me.spn_disbursement_date
            Me.LayoutControlItem18.CustomizationFormText = "Disbursement Cycle:"
            Me.LayoutControlItem18.Location = New System.Drawing.Point(0, 48)
            Me.LayoutControlItem18.Name = "LayoutControlItem18"
            Me.LayoutControlItem18.Size = New System.Drawing.Size(260, 24)
            Me.LayoutControlItem18.Text = "Disbursement Cycle:"
            Me.LayoutControlItem18.TextSize = New System.Drawing.Size(111, 13)
            '
            'LayoutControlItem19
            '
            Me.LayoutControlItem19.Control = Me.dt_start_date
            Me.LayoutControlItem19.CustomizationFormText = "Start Date:"
            Me.LayoutControlItem19.Location = New System.Drawing.Point(0, 72)
            Me.LayoutControlItem19.Name = "LayoutControlItem19"
            Me.LayoutControlItem19.Size = New System.Drawing.Size(260, 24)
            Me.LayoutControlItem19.Text = "Start Date:"
            Me.LayoutControlItem19.TextSize = New System.Drawing.Size(111, 13)
            '
            'LayoutControlItem20
            '
            Me.LayoutControlItem20.Control = Me.lk_bankruptcy_class
            Me.LayoutControlItem20.CustomizationFormText = "Initial BK Intent:"
            Me.LayoutControlItem20.Location = New System.Drawing.Point(0, 96)
            Me.LayoutControlItem20.Name = "LayoutControlItem20"
            Me.LayoutControlItem20.Size = New System.Drawing.Size(260, 24)
            Me.LayoutControlItem20.Text = "Initial BK Intent:"
            Me.LayoutControlItem20.TextSize = New System.Drawing.Size(111, 13)
            '
            'LayoutControlItem17
            '
            Me.LayoutControlItem17.Control = Me.lbl_active_status_date
            Me.LayoutControlItem17.CustomizationFormText = "LayoutControlItem17"
            Me.LayoutControlItem17.Location = New System.Drawing.Point(261, 24)
            Me.LayoutControlItem17.Name = "LayoutControlItem17"
            Me.LayoutControlItem17.Size = New System.Drawing.Size(95, 24)
            Me.LayoutControlItem17.Text = "LayoutControlItem17"
            Me.LayoutControlItem17.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem17.TextToControlDistance = 0
            Me.LayoutControlItem17.TextVisible = False
            '
            'item0
            '
            Me.item0.AllowHotTrack = False
            Me.item0.CustomizationFormText = "item0"
            Me.item0.Location = New System.Drawing.Point(260, 48)
            Me.item0.Name = "item0"
            Me.item0.Size = New System.Drawing.Size(96, 24)
            Me.item0.Text = "item0"
            Me.item0.TextSize = New System.Drawing.Size(0, 0)
            '
            'item2
            '
            Me.item2.AllowHotTrack = False
            Me.item2.CustomizationFormText = "item2"
            Me.item2.Location = New System.Drawing.Point(260, 72)
            Me.item2.Name = "item2"
            Me.item2.Size = New System.Drawing.Size(96, 24)
            Me.item2.Text = "item2"
            Me.item2.TextSize = New System.Drawing.Size(0, 0)
            '
            'item3
            '
            Me.item3.AllowHotTrack = False
            Me.item3.CustomizationFormText = "item3"
            Me.item3.Location = New System.Drawing.Point(260, 96)
            Me.item3.Name = "item3"
            Me.item3.Size = New System.Drawing.Size(96, 24)
            Me.item3.Text = "item3"
            Me.item3.TextSize = New System.Drawing.Size(0, 0)
            '
            'item1
            '
            Me.item1.AllowHotTrack = False
            Me.item1.CustomizationFormText = "item1"
            Me.item1.Location = New System.Drawing.Point(197, 120)
            Me.item1.Name = "item1"
            Me.item1.Size = New System.Drawing.Size(360, 123)
            Me.item1.Text = "item1"
            Me.item1.TextSize = New System.Drawing.Size(0, 0)
            '
            'LayoutControlItem_drop_date
            '
            Me.LayoutControlItem_drop_date.Control = Me.lbl_Drop_Reason
            Me.LayoutControlItem_drop_date.CustomizationFormText = "Drop Reason:"
            Me.LayoutControlItem_drop_date.Location = New System.Drawing.Point(0, 120)
            Me.LayoutControlItem_drop_date.Name = "LayoutControlItem_drop_date"
            Me.LayoutControlItem_drop_date.Size = New System.Drawing.Size(197, 17)
            Me.LayoutControlItem_drop_date.Text = "Drop Reason:"
            Me.LayoutControlItem_drop_date.TextSize = New System.Drawing.Size(111, 13)
            '
            'LayoutControlItem_drop_reason
            '
            Me.LayoutControlItem_drop_reason.Control = Me.lbl_drop_date
            Me.LayoutControlItem_drop_reason.CustomizationFormText = "Drop Date:"
            Me.LayoutControlItem_drop_reason.Location = New System.Drawing.Point(0, 137)
            Me.LayoutControlItem_drop_reason.Name = "LayoutControlItem_drop_reason"
            Me.LayoutControlItem_drop_reason.Size = New System.Drawing.Size(197, 17)
            Me.LayoutControlItem_drop_reason.Text = "Drop Date:"
            Me.LayoutControlItem_drop_reason.TextSize = New System.Drawing.Size(111, 13)
            '
            'EmptySpaceItem2
            '
            Me.EmptySpaceItem2.AllowHotTrack = False
            Me.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Location = New System.Drawing.Point(0, 243)
            Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Size = New System.Drawing.Size(557, 73)
            Me.EmptySpaceItem2.Text = "EmptySpaceItem2"
            Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem3
            '
            Me.EmptySpaceItem3.AllowHotTrack = False
            Me.EmptySpaceItem3.CustomizationFormText = "EmptySpaceItem3"
            Me.EmptySpaceItem3.Location = New System.Drawing.Point(261, 0)
            Me.EmptySpaceItem3.Name = "EmptySpaceItem3"
            Me.EmptySpaceItem3.Size = New System.Drawing.Size(95, 24)
            Me.EmptySpaceItem3.Text = "EmptySpaceItem3"
            Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
            '
            'Page_DMP
            '
            Me.Controls.Add(Me.LayoutControl1)
            Me.Name = "Page_DMP"
            Me.Size = New System.Drawing.Size(577, 336)
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.spn_disbursement_date.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lk_bankruptcy_class.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.chk_hold_disbursements.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.chk_mortgage_problems.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_start_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dt_start_date.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.chk_stack_proration.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lk_active_status.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lk_client_status.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.item0, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.item2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.item3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.item1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_drop_date, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_drop_reason, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents lbl_program_months As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_total_payments As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_total_disb_factors As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_total_deposits As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_held_in_trust As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_reserved_in_trust As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_deposit_in_trust As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lk_client_status As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents lk_active_status As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents dt_start_date As DevExpress.XtraEditors.DateEdit
        Friend WithEvents lk_bankruptcy_class As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents lbl_Drop_Reason As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_drop_date As DevExpress.XtraEditors.LabelControl
        Friend WithEvents chk_stack_proration As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents chk_hold_disbursements As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents chk_mortgage_problems As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents lbl_active_status_date As DevExpress.XtraEditors.LabelControl
        Friend WithEvents spn_disbursement_date As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents LayoutControlGroup3 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem17 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents item0 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents item2 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents item3 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents item1 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents LayoutControlItem_drop_date As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem_drop_reason As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem3 As DevExpress.XtraLayout.EmptySpaceItem

    End Class
End Namespace