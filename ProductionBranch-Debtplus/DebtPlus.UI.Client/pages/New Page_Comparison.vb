#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DebtPlus.UI.Client.forms.Comparison
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Linq
Imports DebtPlus.LINQ

Namespace pages

    Friend Class Page_Comparison
        Dim colRecords As System.Collections.Generic.List(Of sales_file) = Nothing
        Dim bc As DebtPlus.LINQ.BusinessContext = Nothing

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            EnableMenus = True
        End Sub

        Public Overrides Sub ReadForm()
            bc = New DebtPlus.LINQ.BusinessContext()

            Try
                ' Retrieve the sales file data
                colRecords = bc.sales_files.Where(Function(s) s.client = Context.ClientDs.ClientId).ToList()
                GridControl1.DataSource = colRecords

            Catch ex As Exception
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error loading sales file information")
            End Try
        End Sub

        Protected Overrides Sub OnCreateItem()
            Using fac As New DebtPlus.LINQ.Factory()
                Dim salesFileRecord As sales_file = fac.Manufacture_sales_file(Context.ClientDs.ClientId)

                ' Edit the record
                Dim thrd As New Thread(New ParameterizedThreadStart(AddressOf EditThread))
                Context.ThreadList.Add(thrd)
                thrd.SetApartmentState(ApartmentState.STA)
                thrd.Name = "SalesFile"
                thrd.IsBackground = True
                thrd.Start(salesFileRecord)
            End Using
        End Sub

        Protected Overrides Sub OnDeleteItem(obj As Object)
            Dim salesFileRecord As sales_file = TryCast(obj, sales_file)
            If salesFileRecord Is Nothing Then
                Return
            End If

            If DebtPlus.Data.Prompts.RequestConfirmation_Delete() <> DialogResult.Yes Then
                Return
            End If

            bc.sales_files.DeleteOnSubmit(salesFileRecord)
            bc.SubmitChanges()

            colRecords.Remove(salesFileRecord)
            GridControl1.RefreshDataSource()
            GridView1.RefreshData()
        End Sub

        Protected Overrides Sub OnEditItem(ByVal obj As Object)

            Dim record As sales_file = TryCast(obj, sales_file)
            If record Is Nothing Then
                Return
            End If

            ' Edit the record
            Dim thrd As New Thread(New ParameterizedThreadStart(AddressOf EditThread))
            Context.ThreadList.Add(thrd)
            thrd.SetApartmentState(ApartmentState.STA)
            thrd.Name = "SalesFile"
            thrd.IsBackground = True
            thrd.Start(record)
        End Sub

        Public Overrides Sub SaveForm()
            Try
                bc.SubmitChanges()

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error saving sales file information")
            End Try
        End Sub

        Private Delegate Sub CompleteEditDelegate(obj As Object, exportRequired As Boolean)
        Private Sub CompleteEdit(obj As Object, exportRequired As Boolean)

            ' Ensure that we are in the proper thread at this point.
            If InvokeRequired Then
                EndInvoke(BeginInvoke(New CompleteEditDelegate(AddressOf CompleteEdit), New Object() {obj, exportRequired}))
                Return
            End If

            ' If the record is new then insert it
            Dim salesFileRecord As sales_file = TryCast(obj, sales_file)
            If salesFileRecord Is Nothing Then
                Return
            End If

            ' If the record is still "new" then insert it on the update operation
            If salesFileRecord.Id < 1 Then
                Debug.Assert(salesFileRecord.client = Context.ClientDs.ClientId)
                bc.sales_files.InsertOnSubmit(salesFileRecord)
                colRecords.Add(salesFileRecord)
            End If

            ' Do the database update to make it consistent
            bc.SubmitChanges()

            ' Look at the result. If the result is "YES" then we need to export the records
            If exportRequired Then
                bc.xpr_sales_debt_export(salesFileRecord)

                ' Update the exported date from the database so that the record may not be exported again.
                bc.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, salesFileRecord)
            End If

            ' Finally, ensure that the grid control is properly refreshed.
            GridControl1.RefreshDataSource()
            GridView1.RefreshData()
        End Sub

        Private Sub EditThread(ByVal obj As Object)
            Dim salesFileRecord As sales_file = TryCast(obj, sales_file)
            If salesFileRecord Is Nothing Then
                Return
            End If

            ' Do the edit operation. Look at the result to determine if we need to export the debts
            Using frm As New Form_Comparison_List(bc, salesFileRecord)
                Dim answer As System.Windows.Forms.DialogResult = frm.ShowDialog()

                ' If the response was "exit" then do not do a change.
                If answer = DialogResult.No Then
                    Return
                End If

                ' If the response was "export" then do the export operation here
                If answer = DialogResult.Yes Then
                    CompleteEdit(salesFileRecord, True)
                    Return
                End If

                ' Otherwise, just save the data and get on with life
                CompleteEdit(salesFileRecord, False)
            End Using
        End Sub

        ''' <summary>
        ''' Determine if the record may be deleted
        ''' </summary>
        Protected Overrides Function OkToDelete(obj As Object) As Boolean
            Return (MyBase.OkToDelete(obj)) AndAlso (Not isReadOnly(obj))
        End Function

        Private Function isReadOnly(obj As Object) As Boolean
            Dim salesFileRecord As sales_file = TryCast(obj, sales_file)
            Return (salesFileRecord Is Nothing) OrElse (salesFileRecord.date_exported IsNot Nothing)
        End Function
    End Class
End Namespace
