﻿Imports DebtPlus.UI.Client.controls

Namespace pages

    Partial Class Page_General
        Inherits ControlBaseClient

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Dim SuperToolTip2 As DevExpress.Utils.SuperToolTip = New DevExpress.Utils.SuperToolTip()
            Dim ToolTipTitleItem2 As DevExpress.Utils.ToolTipTitleItem = New DevExpress.Utils.ToolTipTitleItem()
            Dim ToolTipItem2 As DevExpress.Utils.ToolTipItem = New DevExpress.Utils.ToolTipItem()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Page_General))
            Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
            Me.marital_status = New DevExpress.XtraEditors.LookUpEdit()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.LookUpEdit_ElectronicStatements = New DevExpress.XtraEditors.LookUpEdit()
            Me.CheckEdit_ElectronicCorrespondence = New DevExpress.XtraEditors.CheckEdit()
            Me.household = New DevExpress.XtraEditors.LookUpEdit()
            Me.TelephoneNumberRecordControl_MsgTelephoneID = New DebtPlus.Data.Controls.TelephoneNumberRecordControl()
            Me.TelephoneNumberRecordControl_HomeTelephoneID = New DebtPlus.Data.Controls.TelephoneNumberRecordControl()
            Me.dependents = New DevExpress.XtraEditors.SpinEdit()
            Me.AddressRecordControl_AddressID = New DebtPlus.Data.Controls.AddressRecordControl()
            Me.counselor = New DevExpress.XtraEditors.LookUpEdit()
            Me.salutation = New DevExpress.XtraEditors.TextEdit()
            Me.mail_error_date = New DevExpress.XtraEditors.DateEdit()
            Me.csr = New DevExpress.XtraEditors.LookUpEdit()
            Me.Referred_By = New DevExpress.XtraEditors.LookUpEdit()
            Me.office = New DevExpress.XtraEditors.LookUpEdit()
            Me.language = New DevExpress.XtraEditors.LookUpEdit()
            Me.preferred_contact = New DevExpress.XtraEditors.LookUpEdit()
            Me.client_Region = New DevExpress.XtraEditors.LookUpEdit()
            Me.county = New DevExpress.XtraEditors.LookUpEdit()
            Me.method_first_contact = New DevExpress.XtraEditors.LookUpEdit()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem17 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem()
            CType(Me.marital_status.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.LookUpEdit_ElectronicStatements.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_ElectronicCorrespondence.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.household.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TelephoneNumberRecordControl_MsgTelephoneID, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TelephoneNumberRecordControl_HomeTelephoneID, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dependents.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.AddressRecordControl_AddressID, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.counselor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.salutation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.mail_error_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.mail_error_date.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.csr.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.Referred_By.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.office.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.language.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.preferred_contact.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.client_Region.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.county.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.method_first_contact.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl16
            '
            Me.LabelControl16.Location = New System.Drawing.Point(8, 203)
            Me.LabelControl16.Name = "LabelControl16"
            Me.LabelControl16.Size = New System.Drawing.Size(39, 13)
            Me.LabelControl16.TabIndex = 32
            Me.LabelControl16.Text = "Address"
            '
            'marital_status
            '
            Me.marital_status.Location = New System.Drawing.Point(92, 12)
            Me.marital_status.Name = "marital_status"
            Me.marital_status.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.marital_status.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.marital_status.Properties.DisplayMember = "description"
            Me.marital_status.Properties.NullText = ""
            Me.marital_status.Properties.ShowFooter = False
            Me.marital_status.Properties.ShowHeader = False
            Me.marital_status.Properties.ShowLines = False
            Me.marital_status.Properties.SortColumnIndex = 1
            Me.marital_status.Properties.ValueMember = "Id"
            Me.marital_status.Size = New System.Drawing.Size(175, 20)
            Me.marital_status.StyleController = Me.LayoutControl1
            Me.marital_status.TabIndex = 1
            Me.marital_status.ToolTip = "Choose the appropriate marital status (married / single / divorced, etc.)"
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_ElectronicStatements)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_ElectronicCorrespondence)
            Me.LayoutControl1.Controls.Add(Me.household)
            Me.LayoutControl1.Controls.Add(Me.TelephoneNumberRecordControl_MsgTelephoneID)
            Me.LayoutControl1.Controls.Add(Me.TelephoneNumberRecordControl_HomeTelephoneID)
            Me.LayoutControl1.Controls.Add(Me.marital_status)
            Me.LayoutControl1.Controls.Add(Me.dependents)
            Me.LayoutControl1.Controls.Add(Me.AddressRecordControl_AddressID)
            Me.LayoutControl1.Controls.Add(Me.counselor)
            Me.LayoutControl1.Controls.Add(Me.salutation)
            Me.LayoutControl1.Controls.Add(Me.mail_error_date)
            Me.LayoutControl1.Controls.Add(Me.csr)
            Me.LayoutControl1.Controls.Add(Me.Referred_By)
            Me.LayoutControl1.Controls.Add(Me.office)
            Me.LayoutControl1.Controls.Add(Me.language)
            Me.LayoutControl1.Controls.Add(Me.preferred_contact)
            Me.LayoutControl1.Controls.Add(Me.client_Region)
            Me.LayoutControl1.Controls.Add(Me.county)
            Me.LayoutControl1.Controls.Add(Me.method_first_contact)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(496, 66, 250, 350)
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(528, 313)
            Me.LayoutControl1.TabIndex = 34
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'LookUpEdit_ElectronicStatements
            '
            Me.LookUpEdit_ElectronicStatements.Location = New System.Drawing.Point(230, 180)
            Me.LookUpEdit_ElectronicStatements.Name = "LookUpEdit_ElectronicStatements"
            Me.LookUpEdit_ElectronicStatements.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.LookUpEdit_ElectronicStatements.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_ElectronicStatements.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True])})
            Me.LookUpEdit_ElectronicStatements.Properties.DisplayMember = "description"
            Me.LookUpEdit_ElectronicStatements.Properties.NullText = ""
            Me.LookUpEdit_ElectronicStatements.Properties.ShowFooter = False
            Me.LookUpEdit_ElectronicStatements.Properties.ShowHeader = False
            Me.LookUpEdit_ElectronicStatements.Properties.ValueMember = "Id"
            Me.LookUpEdit_ElectronicStatements.Size = New System.Drawing.Size(50, 20)
            Me.LookUpEdit_ElectronicStatements.StyleController = Me.LayoutControl1
            Me.LookUpEdit_ElectronicStatements.TabIndex = 40
            Me.LookUpEdit_ElectronicStatements.ToolTip = "Suppress paper statements (Yes/No, ASK is unspecified, ask the client)"
            '
            'CheckEdit_ElectronicCorrespondence
            '
            Me.CheckEdit_ElectronicCorrespondence.EditValue = 0
            Me.CheckEdit_ElectronicCorrespondence.Location = New System.Drawing.Point(284, 180)
            Me.CheckEdit_ElectronicCorrespondence.Name = "CheckEdit_ElectronicCorrespondence"
            Me.CheckEdit_ElectronicCorrespondence.Properties.AllowGrayed = True
            Me.CheckEdit_ElectronicCorrespondence.Properties.Caption = "E-Corr"
            Me.CheckEdit_ElectronicCorrespondence.Properties.ValueChecked = 1
            Me.CheckEdit_ElectronicCorrespondence.Properties.ValueGrayed = 2
            Me.CheckEdit_ElectronicCorrespondence.Properties.ValueUnchecked = 0
            Me.CheckEdit_ElectronicCorrespondence.Size = New System.Drawing.Size(130, 19)
            Me.CheckEdit_ElectronicCorrespondence.StyleController = Me.LayoutControl1
            Me.CheckEdit_ElectronicCorrespondence.TabIndex = 39
            Me.CheckEdit_ElectronicCorrespondence.ToolTip = "Check this to send all letters via E-Mail to the client"
            '
            'household
            '
            Me.household.Location = New System.Drawing.Point(92, 36)
            Me.household.Name = "household"
            Me.household.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.household.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.household.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.household.Properties.DisplayMember = "description"
            Me.household.Properties.NullText = ""
            Me.household.Properties.ShowFooter = False
            Me.household.Properties.ShowHeader = False
            Me.household.Properties.SortColumnIndex = 1
            Me.household.Properties.ValueMember = "Id"
            Me.household.Size = New System.Drawing.Size(175, 20)
            Me.household.StyleController = Me.LayoutControl1
            Me.household.TabIndex = 36
            Me.household.ToolTip = "How is the household best described?"
            '
            'TelephoneNumberRecordControl_MsgTelephoneID
            '
            Me.TelephoneNumberRecordControl_MsgTelephoneID.ErrorIcon = Nothing
            Me.TelephoneNumberRecordControl_MsgTelephoneID.ErrorText = ""
            Me.TelephoneNumberRecordControl_MsgTelephoneID.Location = New System.Drawing.Point(351, 156)
            Me.TelephoneNumberRecordControl_MsgTelephoneID.Margin = New System.Windows.Forms.Padding(0)
            Me.TelephoneNumberRecordControl_MsgTelephoneID.Name = "TelephoneNumberRecordControl_MsgTelephoneID"
            Me.TelephoneNumberRecordControl_MsgTelephoneID.Size = New System.Drawing.Size(165, 20)
            Me.TelephoneNumberRecordControl_MsgTelephoneID.TabIndex = 35
            '
            'TelephoneNumberRecordControl_HomeTelephoneID
            '
            Me.TelephoneNumberRecordControl_HomeTelephoneID.ErrorIcon = Nothing
            Me.TelephoneNumberRecordControl_HomeTelephoneID.ErrorText = ""
            Me.TelephoneNumberRecordControl_HomeTelephoneID.Location = New System.Drawing.Point(351, 132)
            Me.TelephoneNumberRecordControl_HomeTelephoneID.Margin = New System.Windows.Forms.Padding(0)
            Me.TelephoneNumberRecordControl_HomeTelephoneID.Name = "TelephoneNumberRecordControl_HomeTelephoneID"
            Me.TelephoneNumberRecordControl_HomeTelephoneID.Size = New System.Drawing.Size(165, 20)
            Me.TelephoneNumberRecordControl_HomeTelephoneID.TabIndex = 34
            '
            'dependents
            '
            Me.dependents.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
            Me.dependents.Location = New System.Drawing.Point(92, 156)
            Me.dependents.Name = "dependents"
            Me.dependents.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.dependents.Properties.IsFloatValue = False
            Me.dependents.Properties.Mask.EditMask = "f0"
            Me.dependents.Size = New System.Drawing.Size(92, 20)
            Me.dependents.StyleController = Me.LayoutControl1
            Me.dependents.TabIndex = 37
            Me.dependents.ToolTip = "The number of dependents for this client. It is normally the number of people, le" &
        "ss the applicant and co-applicant."
            '
            'AddressRecordControl_AddressID
            '
            Me.AddressRecordControl_AddressID.Location = New System.Drawing.Point(92, 228)
            Me.AddressRecordControl_AddressID.Name = "AddressRecordControl_AddressID"
            Me.AddressRecordControl_AddressID.Size = New System.Drawing.Size(424, 72)
            Me.AddressRecordControl_AddressID.TabIndex = 33
            '
            'counselor
            '
            Me.counselor.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.counselor.Location = New System.Drawing.Point(351, 12)
            Me.counselor.Name = "counselor"
            Me.counselor.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.counselor.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 5, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Note", 5, "Note")})
            Me.counselor.Properties.DisplayMember = "Name"
            Me.counselor.Properties.NullText = ""
            Me.counselor.Properties.ShowFooter = False
            Me.counselor.Properties.ShowLines = False
            Me.counselor.Properties.SortColumnIndex = 1
            Me.counselor.Properties.ValueMember = "Id"
            Me.counselor.Size = New System.Drawing.Size(165, 20)
            Me.counselor.StyleController = Me.LayoutControl1
            Me.counselor.TabIndex = 3
            Me.counselor.ToolTip = "The counselor assigned to this client."
            '
            'salutation
            '
            Me.salutation.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.salutation.EditValue = "Salutation, i.e. ""John and Mary"" for letters to be sent to ""Dear John and Mary""."
            Me.salutation.Location = New System.Drawing.Point(92, 204)
            Me.salutation.Name = "salutation"
            Me.salutation.Properties.MaxLength = 50
            Me.salutation.Size = New System.Drawing.Size(424, 20)
            Me.salutation.StyleController = Me.LayoutControl1
            Me.salutation.TabIndex = 31
            Me.salutation.ToolTip = "The salutation is used in letters as in ""Dear ...."""
            '
            'mail_error_date
            '
            Me.mail_error_date.EditValue = New Date(2007, 3, 23, 0, 0, 0, 0)
            Me.mail_error_date.Location = New System.Drawing.Point(92, 180)
            Me.mail_error_date.Name = "mail_error_date"
            Me.mail_error_date.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.mail_error_date.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.mail_error_date.Size = New System.Drawing.Size(92, 20)
            Me.mail_error_date.StyleController = Me.LayoutControl1
            Me.mail_error_date.TabIndex = 25
            Me.mail_error_date.ToolTip = "This is the date that postal mail was returned due to a bad address. If set, no n" &
        "ew statements are sent to the client."
            '
            'csr
            '
            Me.csr.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.csr.Location = New System.Drawing.Point(351, 36)
            Me.csr.Name = "csr"
            Me.csr.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.csr.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Counselor", "ID", 5, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Note", 5, "Note")})
            Me.csr.Properties.DisplayMember = "Name"
            Me.csr.Properties.NullText = ""
            Me.csr.Properties.ShowFooter = False
            Me.csr.Properties.ShowLines = False
            Me.csr.Properties.SortColumnIndex = 1
            Me.csr.Properties.ValueMember = "Id"
            Me.csr.Size = New System.Drawing.Size(165, 20)
            Me.csr.StyleController = Me.LayoutControl1
            Me.csr.TabIndex = 7
            Me.csr.ToolTip = "The Customer Service Representative that will help in issues for this client."
            '
            'Referred_By
            '
            Me.Referred_By.Location = New System.Drawing.Point(92, 60)
            Me.Referred_By.Name = "Referred_By"
            Me.Referred_By.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.Referred_By.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.Referred_By.Properties.DisplayMember = "description"
            Me.Referred_By.Properties.NullText = ""
            Me.Referred_By.Properties.ShowFooter = False
            Me.Referred_By.Properties.ShowHeader = False
            Me.Referred_By.Properties.ShowLines = False
            Me.Referred_By.Properties.SortColumnIndex = 1
            Me.Referred_By.Properties.ValueMember = "Id"
            Me.Referred_By.Size = New System.Drawing.Size(175, 20)
            Me.Referred_By.StyleController = Me.LayoutControl1
            SuperToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.[False]
            ToolTipTitleItem2.Text = "How did the client find the agency?"
            ToolTipItem2.LeftIndent = 6
            ToolTipItem2.Text = resources.GetString("ToolTipItem2.Text")
            SuperToolTip2.Items.Add(ToolTipTitleItem2)
            SuperToolTip2.Items.Add(ToolTipItem2)
            Me.Referred_By.SuperTip = SuperToolTip2
            Me.Referred_By.TabIndex = 9
            '
            'office
            '
            Me.office.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.office.Location = New System.Drawing.Point(351, 60)
            Me.office.Name = "office"
            Me.office.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.office.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("name", "Name", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.office.Properties.DisplayMember = "name"
            Me.office.Properties.NullText = ""
            Me.office.Properties.ShowFooter = False
            Me.office.Properties.ShowHeader = False
            Me.office.Properties.ShowLines = False
            Me.office.Properties.SortColumnIndex = 1
            Me.office.Properties.ValueMember = "Id"
            Me.office.Size = New System.Drawing.Size(165, 20)
            Me.office.StyleController = Me.LayoutControl1
            Me.office.TabIndex = 11
            Me.office.ToolTip = "In which office is this client serviced?"
            '
            'language
            '
            Me.language.Location = New System.Drawing.Point(92, 84)
            Me.language.Name = "language"
            Me.language.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.language.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Attribute", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.language.Properties.DisplayMember = "Attribute"
            Me.language.Properties.NullText = ""
            Me.language.Properties.ShowFooter = False
            Me.language.Properties.ShowHeader = False
            Me.language.Properties.ShowLines = False
            Me.language.Properties.SortColumnIndex = 1
            Me.language.Properties.ValueMember = "Id"
            Me.language.Size = New System.Drawing.Size(175, 20)
            Me.language.StyleController = Me.LayoutControl1
            Me.language.TabIndex = 13
            Me.language.ToolTip = "This is the spoken language for the client. Counseling and communications should " &
        "be done in this language."
            '
            'preferred_contact
            '
            Me.preferred_contact.Location = New System.Drawing.Point(92, 132)
            Me.preferred_contact.Name = "preferred_contact"
            Me.preferred_contact.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.preferred_contact.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_value", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.preferred_contact.Properties.DisplayMember = "description"
            Me.preferred_contact.Properties.NullText = ""
            Me.preferred_contact.Properties.ShowFooter = False
            Me.preferred_contact.Properties.ShowHeader = False
            Me.preferred_contact.Properties.ShowLines = False
            Me.preferred_contact.Properties.SortColumnIndex = 1
            Me.preferred_contact.Properties.ValueMember = "Id"
            Me.preferred_contact.Size = New System.Drawing.Size(175, 20)
            Me.preferred_contact.StyleController = Me.LayoutControl1
            Me.preferred_contact.TabIndex = 21
            Me.preferred_contact.ToolTip = "If you are going to contact the client, what is the best method to do this?"
            '
            'client_Region
            '
            Me.client_Region.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.client_Region.Location = New System.Drawing.Point(351, 84)
            Me.client_Region.Name = "client_Region"
            Me.client_Region.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.client_Region.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.client_Region.Properties.DisplayMember = "description"
            Me.client_Region.Properties.NullText = ""
            Me.client_Region.Properties.ShowFooter = False
            Me.client_Region.Properties.ShowHeader = False
            Me.client_Region.Properties.ShowLines = False
            Me.client_Region.Properties.SortColumnIndex = 1
            Me.client_Region.Properties.ValueMember = "Id"
            Me.client_Region.Size = New System.Drawing.Size(165, 20)
            Me.client_Region.StyleController = Me.LayoutControl1
            Me.client_Region.TabIndex = 15
            Me.client_Region.Tag = "Region"
            Me.client_Region.ToolTip = "This is the Region for the client. It is similar to the locality, but is on a lar" &
        "ger basis."
            '
            'county
            '
            Me.county.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.county.Location = New System.Drawing.Point(351, 108)
            Me.county.Name = "county"
            Me.county.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.county.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("name", "Name", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.county.Properties.DisplayMember = "name"
            Me.county.Properties.NullText = ""
            Me.county.Properties.ShowFooter = False
            Me.county.Properties.ShowHeader = False
            Me.county.Properties.ShowLines = False
            Me.county.Properties.SortColumnIndex = 1
            Me.county.Properties.ValueMember = "Id"
            Me.county.Size = New System.Drawing.Size(165, 20)
            Me.county.StyleController = Me.LayoutControl1
            Me.county.TabIndex = 19
            Me.county.ToolTip = "This is the locality used for HUD reporting. It is typically the county/parish."
            '
            'method_first_contact
            '
            Me.method_first_contact.Location = New System.Drawing.Point(92, 108)
            Me.method_first_contact.Name = "method_first_contact"
            Me.method_first_contact.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.method_first_contact.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.method_first_contact.Properties.DisplayMember = "description"
            Me.method_first_contact.Properties.NullText = ""
            Me.method_first_contact.Properties.ShowFooter = False
            Me.method_first_contact.Properties.ShowHeader = False
            Me.method_first_contact.Properties.ShowLines = False
            Me.method_first_contact.Properties.SortColumnIndex = 1
            Me.method_first_contact.Properties.ValueMember = "Id"
            Me.method_first_contact.Size = New System.Drawing.Size(175, 20)
            Me.method_first_contact.StyleController = Me.LayoutControl1
            Me.method_first_contact.TabIndex = 17
            Me.method_first_contact.ToolTip = "How did the client first contact this agency?"
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup1.GroupBordersVisible = False
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem4, Me.LayoutControlItem6, Me.LayoutControlItem8, Me.LayoutControlItem10, Me.LayoutControlItem17, Me.LayoutControlItem12, Me.LayoutControlItem15, Me.LayoutControlItem11, Me.LayoutControlItem9, Me.LayoutControlItem7, Me.LayoutControlItem5, Me.LayoutControlItem3, Me.LayoutControlItem18, Me.LayoutControlItem13, Me.EmptySpaceItem1, Me.EmptySpaceItem2, Me.LayoutControlItem16, Me.LayoutControlItem20, Me.LayoutControlItem19})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(528, 313)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.marital_status
            Me.LayoutControlItem1.CustomizationFormText = "&Marital Status"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(259, 24)
            Me.LayoutControlItem1.Text = "&Marital Status"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(77, 13)
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.counselor
            Me.LayoutControlItem2.CustomizationFormText = "Counselor"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(259, 0)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(249, 24)
            Me.LayoutControlItem2.Text = "Counselor"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(77, 13)
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.csr
            Me.LayoutControlItem4.CustomizationFormText = "CSR"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(259, 24)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(249, 24)
            Me.LayoutControlItem4.Text = "CSR"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(77, 13)
            '
            'LayoutControlItem6
            '
            Me.LayoutControlItem6.Control = Me.office
            Me.LayoutControlItem6.CustomizationFormText = "Office"
            Me.LayoutControlItem6.Location = New System.Drawing.Point(259, 48)
            Me.LayoutControlItem6.Name = "LayoutControlItem6"
            Me.LayoutControlItem6.Size = New System.Drawing.Size(249, 24)
            Me.LayoutControlItem6.Text = "Office"
            Me.LayoutControlItem6.TextSize = New System.Drawing.Size(77, 13)
            '
            'LayoutControlItem8
            '
            Me.LayoutControlItem8.Control = Me.client_Region
            Me.LayoutControlItem8.CustomizationFormText = "Region"
            Me.LayoutControlItem8.Location = New System.Drawing.Point(259, 72)
            Me.LayoutControlItem8.Name = "LayoutControlItem8"
            Me.LayoutControlItem8.Size = New System.Drawing.Size(249, 24)
            Me.LayoutControlItem8.Text = "Region"
            Me.LayoutControlItem8.TextSize = New System.Drawing.Size(77, 13)
            '
            'LayoutControlItem10
            '
            Me.LayoutControlItem10.Control = Me.county
            Me.LayoutControlItem10.CustomizationFormText = "Locality"
            Me.LayoutControlItem10.Location = New System.Drawing.Point(259, 96)
            Me.LayoutControlItem10.Name = "LayoutControlItem10"
            Me.LayoutControlItem10.Size = New System.Drawing.Size(249, 24)
            Me.LayoutControlItem10.Text = "Locality"
            Me.LayoutControlItem10.TextSize = New System.Drawing.Size(77, 13)
            '
            'LayoutControlItem17
            '
            Me.LayoutControlItem17.AppearanceItemCaption.Options.UseTextOptions = True
            Me.LayoutControlItem17.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LayoutControlItem17.Control = Me.AddressRecordControl_AddressID
            Me.LayoutControlItem17.CustomizationFormText = "Address"
            Me.LayoutControlItem17.Location = New System.Drawing.Point(0, 216)
            Me.LayoutControlItem17.Name = "LayoutControlItem17"
            Me.LayoutControlItem17.Size = New System.Drawing.Size(508, 77)
            Me.LayoutControlItem17.Text = "Address"
            Me.LayoutControlItem17.TextSize = New System.Drawing.Size(77, 13)
            '
            'LayoutControlItem12
            '
            Me.LayoutControlItem12.Control = Me.TelephoneNumberRecordControl_HomeTelephoneID
            Me.LayoutControlItem12.CustomizationFormText = "Home Phone"
            Me.LayoutControlItem12.Location = New System.Drawing.Point(259, 120)
            Me.LayoutControlItem12.Name = "LayoutControlItem12"
            Me.LayoutControlItem12.Size = New System.Drawing.Size(249, 24)
            Me.LayoutControlItem12.Text = "Home Phone"
            Me.LayoutControlItem12.TextSize = New System.Drawing.Size(77, 13)
            '
            'LayoutControlItem15
            '
            Me.LayoutControlItem15.Control = Me.TelephoneNumberRecordControl_MsgTelephoneID
            Me.LayoutControlItem15.CustomizationFormText = "Msg Phone"
            Me.LayoutControlItem15.Location = New System.Drawing.Point(259, 144)
            Me.LayoutControlItem15.Name = "LayoutControlItem15"
            Me.LayoutControlItem15.Size = New System.Drawing.Size(249, 24)
            Me.LayoutControlItem15.Text = "Msg Phone"
            Me.LayoutControlItem15.TextSize = New System.Drawing.Size(77, 13)
            '
            'LayoutControlItem11
            '
            Me.LayoutControlItem11.Control = Me.preferred_contact
            Me.LayoutControlItem11.CustomizationFormText = "Contact Method"
            Me.LayoutControlItem11.Location = New System.Drawing.Point(0, 120)
            Me.LayoutControlItem11.Name = "LayoutControlItem11"
            Me.LayoutControlItem11.Size = New System.Drawing.Size(259, 24)
            Me.LayoutControlItem11.Text = "Contact Method"
            Me.LayoutControlItem11.TextSize = New System.Drawing.Size(77, 13)
            '
            'LayoutControlItem9
            '
            Me.LayoutControlItem9.Control = Me.method_first_contact
            Me.LayoutControlItem9.CustomizationFormText = "First Contact"
            Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 96)
            Me.LayoutControlItem9.Name = "LayoutControlItem9"
            Me.LayoutControlItem9.Size = New System.Drawing.Size(259, 24)
            Me.LayoutControlItem9.Text = "First Contact"
            Me.LayoutControlItem9.TextSize = New System.Drawing.Size(77, 13)
            '
            'LayoutControlItem7
            '
            Me.LayoutControlItem7.Control = Me.language
            Me.LayoutControlItem7.CustomizationFormText = "Language Reqd"
            Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 72)
            Me.LayoutControlItem7.Name = "LayoutControlItem7"
            Me.LayoutControlItem7.Size = New System.Drawing.Size(259, 24)
            Me.LayoutControlItem7.Text = "Language Reqd"
            Me.LayoutControlItem7.TextSize = New System.Drawing.Size(77, 13)
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Control = Me.Referred_By
            Me.LayoutControlItem5.CustomizationFormText = "Referral Code"
            Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 48)
            Me.LayoutControlItem5.Name = "LayoutControlItem5"
            Me.LayoutControlItem5.Size = New System.Drawing.Size(259, 24)
            Me.LayoutControlItem5.Text = "Initial Referral"
            Me.LayoutControlItem5.TextSize = New System.Drawing.Size(77, 13)
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.household
            Me.LayoutControlItem3.CustomizationFormText = "Household Head"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(259, 24)
            Me.LayoutControlItem3.Text = "Household"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(77, 13)
            '
            'LayoutControlItem18
            '
            Me.LayoutControlItem18.Control = Me.dependents
            Me.LayoutControlItem18.CustomizationFormText = "Dependents"
            Me.LayoutControlItem18.Location = New System.Drawing.Point(0, 144)
            Me.LayoutControlItem18.Name = "LayoutControlItem18"
            Me.LayoutControlItem18.Size = New System.Drawing.Size(176, 24)
            Me.LayoutControlItem18.Text = "Dependents"
            Me.LayoutControlItem18.TextSize = New System.Drawing.Size(77, 13)
            '
            'LayoutControlItem13
            '
            Me.LayoutControlItem13.Control = Me.mail_error_date
            Me.LayoutControlItem13.CustomizationFormText = "Error Date"
            Me.LayoutControlItem13.Location = New System.Drawing.Point(0, 168)
            Me.LayoutControlItem13.Name = "LayoutControlItem13"
            Me.LayoutControlItem13.Size = New System.Drawing.Size(176, 24)
            Me.LayoutControlItem13.Text = "Error Date"
            Me.LayoutControlItem13.TextSize = New System.Drawing.Size(77, 13)
            '
            'EmptySpaceItem1
            '
            Me.EmptySpaceItem1.AllowHotTrack = False
            Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Location = New System.Drawing.Point(406, 168)
            Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Size = New System.Drawing.Size(102, 24)
            Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
            Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem2
            '
            Me.EmptySpaceItem2.AllowHotTrack = False
            Me.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Location = New System.Drawing.Point(176, 144)
            Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Size = New System.Drawing.Size(83, 24)
            Me.EmptySpaceItem2.Text = "EmptySpaceItem2"
            Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
            '
            'LayoutControlItem16
            '
            Me.LayoutControlItem16.Control = Me.salutation
            Me.LayoutControlItem16.CustomizationFormText = "Salutation"
            Me.LayoutControlItem16.Location = New System.Drawing.Point(0, 192)
            Me.LayoutControlItem16.Name = "LayoutControlItem16"
            Me.LayoutControlItem16.Size = New System.Drawing.Size(508, 24)
            Me.LayoutControlItem16.Text = "Salutation"
            Me.LayoutControlItem16.TextSize = New System.Drawing.Size(77, 13)
            '
            'LayoutControlItem20
            '
            Me.LayoutControlItem20.Control = Me.CheckEdit_ElectronicCorrespondence
            Me.LayoutControlItem20.CustomizationFormText = "Electronic Correspondence"
            Me.LayoutControlItem20.Location = New System.Drawing.Point(272, 168)
            Me.LayoutControlItem20.Name = "LayoutControlItem20"
            Me.LayoutControlItem20.Size = New System.Drawing.Size(134, 24)
            Me.LayoutControlItem20.Text = "Electronic Correspondence"
            Me.LayoutControlItem20.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem20.TextToControlDistance = 0
            Me.LayoutControlItem20.TextVisible = False
            Me.LayoutControlItem20.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
            '
            'LayoutControlItem19
            '
            Me.LayoutControlItem19.Control = Me.LookUpEdit_ElectronicStatements
            Me.LayoutControlItem19.CustomizationFormText = "Electronic Statements"
            Me.LayoutControlItem19.Location = New System.Drawing.Point(176, 168)
            Me.LayoutControlItem19.Name = "LayoutControlItem19"
            Me.LayoutControlItem19.Size = New System.Drawing.Size(96, 24)
            Me.LayoutControlItem19.Text = "E-Stmts"
            Me.LayoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
            Me.LayoutControlItem19.TextSize = New System.Drawing.Size(37, 13)
            Me.LayoutControlItem19.TextToControlDistance = 5
            '
            'Page_General
            '
            Me.Controls.Add(Me.LayoutControl1)
            Me.Controls.Add(Me.LabelControl16)
            Me.Name = "Page_General"
            Me.Size = New System.Drawing.Size(528, 313)
            CType(Me.marital_status.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.LookUpEdit_ElectronicStatements.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_ElectronicCorrespondence.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.household.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TelephoneNumberRecordControl_MsgTelephoneID, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TelephoneNumberRecordControl_HomeTelephoneID, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dependents.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.AddressRecordControl_AddressID, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.counselor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.salutation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.mail_error_date.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.mail_error_date.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.csr.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.Referred_By.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.office.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.language.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.preferred_contact.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.client_Region.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.county.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.method_first_contact.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()
        End Sub

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents marital_status As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents language As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents method_first_contact As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents county As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents Referred_By As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents preferred_contact As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents counselor As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents csr As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents office As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents client_Region As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents salutation As DevExpress.XtraEditors.TextEdit
        Friend WithEvents AddressRecordControl_AddressID As DebtPlus.Data.Controls.AddressRecordControl
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents TelephoneNumberRecordControl_MsgTelephoneID As DebtPlus.Data.Controls.TelephoneNumberRecordControl
        Friend WithEvents TelephoneNumberRecordControl_HomeTelephoneID As DebtPlus.Data.Controls.TelephoneNumberRecordControl
        Friend WithEvents dependents As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents household As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem17 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents CheckEdit_ElectronicCorrespondence As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LookUpEdit_ElectronicStatements As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents mail_error_date As DevExpress.XtraEditors.DateEdit
    End Class
End Namespace