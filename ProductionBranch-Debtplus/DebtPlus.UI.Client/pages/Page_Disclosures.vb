#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.UI.Client.forms.Comparison
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Threading
Imports System.Linq
Imports DebtPlus.LINQ

Namespace pages

    Friend Class Page_Disclosure
        Implements ICustomFormatter, IFormatProvider

        ' Use for logging purposes if needed
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        ' Local storage
        Private bc As DebtPlus.LINQ.BusinessContext = Nothing
        Private colRecords As System.Collections.Generic.List(Of DebtPlus.LINQ.client_disclosure)

        ' Delegate to the create thread completion function
        Private Delegate Sub CompletionThreadDelegate(ByVal obj As Object)

        ''' <summary>
        ''' Initialize the new instance of the class
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            EnableMenus = True

            ' Set the formatting routines for the columns
            GridColumn_property.DisplayFormat.Format = Me
            GridColumn_property.GroupFormat.Format = Me
            GridColumn_applicant.DisplayFormat.Format = Me
            GridColumn_applicant.GroupFormat.Format = Me
            GridColumn_coapplicant.DisplayFormat.Format = Me
            GridColumn_coapplicant.GroupFormat.Format = Me
        End Sub

        ''' <summary>
        ''' Read the data from the database for this page.
        ''' </summary>
        ''' <remarks></remarks>
        Public Overrides Sub ReadForm(parentBc As DebtPlus.LINQ.BusinessContext)
            log.Debug("ENTER ReadForm")
            MyBase.ReadForm(parentBc)

            If bc Is Nothing Then
                bc = New DebtPlus.LINQ.BusinessContext()
            End If

            If colRecords Is Nothing Then
                log.DebugFormat("Reading client_disclosure records for client {0:f0}", Context.ClientDs.ClientId)
                colRecords = bc.client_disclosures.Where(Function(cd) cd.client = Context.ClientDs.ClientId).ToList()
                log.DebugFormat("Read complete. Count = {0:n0}", colRecords.Count)
            End If

            GridControl1.DataSource = colRecords
            log.Debug("EXIT ReadForm")
        End Sub

        ''' <summary>
        ''' Handle the event to create a new item for this class
        ''' </summary>
        ''' <remarks></remarks>
        Protected Overrides Sub OnCreateItem()
            log.Debug("ENTER OnCreateItem")
            Dim thrd As New System.Threading.Thread(New System.Threading.ParameterizedThreadStart(AddressOf CreateThread))
            thrd.SetApartmentState(ApartmentState.STA)
            thrd.IsBackground = True
            thrd.Start(Nothing)
            log.Debug("EXIT ReadForm")
        End Sub

        ''' <summary>
        ''' Handle the event to edit the current item for this class
        ''' </summary>
        ''' <param name="obj"></param>
        ''' <remarks></remarks>
        Protected Overrides Sub OnEditItem(ByVal obj As Object)
            log.Debug("ENTER OnEditItem")
            Dim thrd As New System.Threading.Thread(New System.Threading.ParameterizedThreadStart(AddressOf EditThread))
            thrd.SetApartmentState(ApartmentState.STA)
            thrd.IsBackground = True
            thrd.Start(obj)
            log.Debug("EXIT OnEditItem")
        End Sub

        ''' <summary>
        ''' Handle the event to delete the item for this class
        ''' </summary>
        ''' <param name="obj"></param>
        ''' <remarks></remarks>
        Protected Overrides Sub OnDeleteItem(ByVal obj As Object)
            log.Debug("ENTER OnDeleteItem")

            ' Find the record to be deleted
            Dim record As client_disclosure = TryCast(obj, client_disclosure)
            If record Is Nothing Then
                log.Info("record is not specified")
                Return
            End If

            ' Give the user the chance to bail out
            If DebtPlus.Data.Prompts.RequestConfirmation_Delete() <> DialogResult.Yes Then
                log.Info("Delete operation was cancelled by the user")
                Return
            End If

            Try
                log.InfoFormat("Deleting disclosure {0:f0}", record.Id)
                ' Do the delete operation
                bc.client_disclosures.DeleteOnSubmit(record)
                bc.SubmitChanges()
                log.Info("Delete operation successful")

                ' Remove the item from the list
                colRecords.Remove(record)
                GridControl1.RefreshDataSource()
                GridView1.RefreshData()
                log.Debug("Grid refreshed after delete operation")

            Catch ex As SqlClient.SqlException
                log.Fatal("Delete operation failed", ex)
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error deleting the disclosure item")
            End Try
        End Sub

        ''' <summary>
        ''' Thread to process the Create logic.
        ''' </summary>
        ''' <param name="obj"></param>
        ''' <remarks></remarks>
        Private Sub CreateThread(ByVal obj As Object)
            log.DebugFormat("Starting Create Thread operation. Id = {0:f0}", Thread.CurrentThread.ManagedThreadId)

            Dim record As client_disclosure = DebtPlus.LINQ.Factory.Manufacture_client_disclosure(Context.ClientDs.ClientId)
            log.Debug("New record created -- Starting edit operation")

            Using frm As New forms.Form_ClientDisclosure(bc, Context, record)
                Dim answer As System.Windows.Forms.DialogResult = frm.ShowDialog()
                If answer <> DialogResult.OK Then
                    log.DebugFormat("Create operation terminated with result code {0}", answer.ToString())
                    Return
                End If
            End Using

            Try
                ' Add the new record to the list and update the database with the information
                log.Debug("Adding new disclosure to system")
                bc.client_disclosures.InsertOnSubmit(record)
                bc.SubmitChanges()
                log.DebugFormat("Disclosure created. ID = {0:f0}", record.Id)
                CreateThreadCompletion(record)
                log.Debug("EXIT CreateThreadCompletion")

            Catch ex As Exception
                log.Fatal("Create operation generated exception", ex)
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error creating client disclosure")
            End Try
        End Sub

        ''' <summary>
        ''' This routine is called when the create function has completed. The new record is
        ''' inserted into the collection list and the list is updated in the grid control.
        ''' </summary>
        ''' <param name="obj"></param>
        ''' <remarks></remarks>
        Private Sub CreateThreadCompletion(ByVal obj As Object)

            If GridControl1.InvokeRequired Then
                Dim ia As System.IAsyncResult = GridControl1.BeginInvoke(New CompletionThreadDelegate(AddressOf CreateThreadCompletion), New Object() {obj})
                GridControl1.EndInvoke(ia)
                Return
            End If

            colRecords.Add(DirectCast(obj, client_disclosure))
            GridControl1.RefreshDataSource()
            GridView1.RefreshData()
            log.Debug("CreateThreadCompletion refreshed grid")
        End Sub

        ''' <summary>
        ''' Thread to process the EDIT function.
        ''' </summary>
        ''' <param name="obj"></param>
        ''' <remarks></remarks>
        Private Sub EditThread(ByVal obj As Object)
            log.DebugFormat("Starting Edit Thread operation. Id = {0:f0}", Thread.CurrentThread.ManagedThreadId)

            ' Find the record to be deleted
            Dim record As client_disclosure = TryCast(obj, client_disclosure)
            If record Is Nothing Then
                log.Info("record is not specified")
                Return
            End If

            ' Do the edit operation on the record. Since edits are always "read-only" there is no
            ' need to have a completion event routine as the data is never updated.
            log.InfoFormat("Editing disclosure {0:f0}", record.Id)
            Using frm As New forms.Form_ClientDisclosure(bc, Context, record)
                Dim answer As System.Windows.Forms.DialogResult = frm.ShowDialog()
                log.DebugFormat("Edit operation terminated with result code {0}", answer.ToString())
                Return
            End Using
        End Sub

        ''' <summary>
        ''' Format the items for the grid control. The specific formatting is controlled by the "format" parameter.
        ''' </summary>
        ''' <param name="format1"></param>
        ''' <param name="arg"></param>
        ''' <param name="formatProvider"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Format(format1 As String, arg As Object, formatProvider As System.IFormatProvider) As String Implements System.ICustomFormatter.Format
            If arg Is Nothing Then Return String.Empty
            Dim intArg As Int32 = Convert.ToInt32(arg)
            If intArg < 1 Then Return String.Empty

            ' Format the "applies to applicant" if the format string is "a"
            If format1 = "a" Then
                Return format_applicant(intArg)
            End If

            ' Format the "applies to co-applicant" if the format string is "c"
            If format1 = "c" Then
                Return format_applicant(intArg)
            End If

            Dim clientDisclosureRecord As DebtPlus.LINQ.client_disclosure = colRecords.Find(Function(s) s.Id = intArg)
            If clientDisclosureRecord Is Nothing Then Return String.Empty

            ' Format the property address if the format string is "p"
            If format1 = "p" Then
                If clientDisclosureRecord.propertyID Is Nothing Then Return String.Empty
                Dim housingPropertyRecord As DebtPlus.LINQ.Housing_property = bc.Housing_properties.Where(Function(s) s.Id = clientDisclosureRecord.propertyID.Value).FirstOrDefault()

                ' If there is an alternate property address then use it
                If housingPropertyRecord IsNot Nothing Then
                    If housingPropertyRecord.PropertyAddress IsNot Nothing AndAlso housingPropertyRecord.UseHomeAddress = False Then
                        Dim altAddress As DebtPlus.LINQ.address = bc.addresses.Where(Function(s) s.Id = housingPropertyRecord.PropertyAddress.Value).FirstOrDefault()
                        If altAddress IsNot Nothing Then
                            Return altAddress.AddressLine1
                        End If
                        Return String.Empty
                    End If

                    ' Use the address from the client's home address
                    Dim clientHousingAddressID As System.Nullable(Of Int32) = bc.clients.Where(Function(c) c.Id = Context.ClientDs.ClientId).Select(Function(c) c.AddressID).FirstOrDefault()
                    If clientHousingAddressID IsNot Nothing Then
                        Dim clientAddress As DebtPlus.LINQ.address = bc.addresses.Where(Function(s) s.Id = clientHousingAddressID.Value).FirstOrDefault()
                        If clientAddress IsNot Nothing Then
                            Return clientAddress.AddressLine1
                        End If
                    End If
                End If

                ' When all fails, return the empty string.
                Return String.Empty
            End If

            ' All other format strings are invalid
            Return format1
        End Function

        ''' <summary>
        ''' Translate the disclosure attributes to the corresponding text format.
        ''' </summary>
        ''' <param name="value"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Shared Function format_applicant(ByVal value As Int32) As String
            Dim q As DebtPlus.LINQ.InMemory.disclosure_applicantType = DebtPlus.LINQ.InMemory.disclosure_applicantTypes.getList().Find(Function(s) s.Id = value)
            If q IsNot Nothing Then
                Return q.description
            End If
            Return String.Empty
        End Function

        ''' <summary>
        ''' Return the format processing class for the indicated type of object. Since we only reference
        ''' this class for our own objects, the answer is always "us".
        ''' </summary>
        ''' <param name="formatType"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function GetFormat(formatType As System.Type) As Object Implements System.IFormatProvider.GetFormat
            Return Me
        End Function
    End Class
End Namespace
