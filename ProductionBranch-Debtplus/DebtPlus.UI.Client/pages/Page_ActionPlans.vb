#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.LINQ
Imports System.Linq
Imports DebtPlus.UI.Client.controls
Imports System.Threading
Imports DebtPlus.UI.Client.forms

Namespace pages

    Friend Class Page_ActionPlan
        Inherits MyGridControlClient

        ' Delegate for the completion routines.
        Private Delegate Sub CompleteCreateDelegate(ByVal record As action_plan, ByVal completionResult As System.Windows.Forms.DialogResult)

        ' List of the action plans for this client
        Private colPlans As System.Collections.Generic.List(Of DebtPlus.LINQ.action_plan)
        Private bc As DebtPlus.LINQ.BusinessContext = Nothing

        ''' <summary>
        ''' Create an instance of our page class
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            MyBase.EnableMenus = True
            RegisterEventHandlers()
        End Sub

        Private Sub RegisterEventHandlers()
        End Sub

        Private Sub UnRegisterEventHandlers()
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then components.Dispose()
                End If

                ' Dispose of the other references to structures.
                bc = Nothing
                colPlans = Nothing
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents GridColumn_ActionPlan As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_date_created As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_created_by As DevExpress.XtraGrid.Columns.GridColumn

        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.GridColumn_ActionPlan = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_date_created = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_created_by = New DevExpress.XtraGrid.Columns.GridColumn()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PopupMenu_Items, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(195, Byte), Integer))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(189, Byte), Integer))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(206, Byte), Integer))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(225, Byte), Integer))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(215, Byte), Integer))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(215, Byte), Integer))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(215, Byte), Integer))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_ActionPlan, Me.GridColumn_date_created, Me.GridColumn_created_by})
            Me.GridView1.OptionsBehavior.AutoPopulateColumns = False
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsDetail.AllowZoomDetail = False
            Me.GridView1.OptionsDetail.EnableMasterViewMode = False
            Me.GridView1.OptionsDetail.ShowDetailTabs = False
            Me.GridView1.OptionsDetail.SmartDetailExpand = False
            Me.GridView1.OptionsLayout.Columns.StoreAllOptions = True
            Me.GridView1.OptionsLayout.LayoutVersion = "1"
            Me.GridView1.OptionsLayout.StoreAllOptions = True
            Me.GridView1.OptionsLayout.StoreDataSettings = False
            Me.GridView1.OptionsView.EnableAppearanceEvenRow = True
            Me.GridView1.OptionsView.EnableAppearanceOddRow = True
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridView1.OptionsView.ShowIndicator = False
            Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn_date_created, DevExpress.Data.ColumnSortOrder.Descending)})
            '
            'GridColumn_ActionPlan
            '
            Me.GridColumn_ActionPlan.Caption = "ID"
            Me.GridColumn_ActionPlan.CustomizationCaption = "ActionPlan ID"
            Me.GridColumn_ActionPlan.FieldName = "Id"
            Me.GridColumn_ActionPlan.Name = "GridColumn_ActionPlan"
            Me.GridColumn_ActionPlan.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_ActionPlan.Visible = True
            Me.GridColumn_ActionPlan.VisibleIndex = 0
            '
            'GridColumn_date_created
            '
            Me.GridColumn_date_created.Caption = "Date Created"
            Me.GridColumn_date_created.CustomizationCaption = "Date Created"
            Me.GridColumn_date_created.FieldName = "date_created"
            Me.GridColumn_date_created.Name = "GridColumn_date_created"
            Me.GridColumn_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_date_created.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value
            Me.GridColumn_date_created.Visible = True
            Me.GridColumn_date_created.VisibleIndex = 1
            '
            'GridColumn_created_by
            '
            Me.GridColumn_created_by.Caption = "Created By"
            Me.GridColumn_created_by.CustomizationCaption = "Created By"
            Me.GridColumn_created_by.FieldName = "created_by"
            Me.GridColumn_created_by.Name = "GridColumn_created_by"
            Me.GridColumn_created_by.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_created_by.Visible = True
            Me.GridColumn_created_by.VisibleIndex = 2
            '
            'Page_ActionPlan
            '
            Me.Name = "Page_ActionPlan"
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PopupMenu_Items, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        ''' <summary>
        ''' Process the new tab being the action plan list. Load the gird.
        ''' </summary>
        Public Overrides Sub ReadForm(bc As DebtPlus.LINQ.BusinessContext)

            UnRegisterEventHandlers()

            Me.bc = bc
            GridView1.BeginUpdate()
            Try
                If colPlans Is Nothing Then
                    colPlans = bc.action_plans.Where(Function(s) s.client = Context.ClientDs.ClientId).ToList()
                End If

                ' Bind the collection to the grid control
                GridControl1.DataSource = colPlans
                GridControl1.RefreshDataSource()
                GridView1.BestFitColumns()
            Finally
                GridView1.EndUpdate()
                RegisterEventHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Delete the indicated action plan.
        ''' </summary>
        ''' <param name="obj">Pointer to the record to be deleted</param>
        ''' <remarks></remarks>
        Protected Overrides Sub OnDeleteItem(ByVal obj As Object)

            Dim record As action_plan = TryCast(obj, action_plan)
            If record Is Nothing Then
                Return
            End If

            ' Ask for permission to delete the plan
            If DebtPlus.Data.Prompts.RequestConfirmation_Delete() <> DialogResult.Yes Then
                Return
            End If

            ' Delete the action plan
            Try
                bc.action_plans.DeleteOnSubmit(record)
                bc.SubmitChanges()

                colPlans.Remove(record)
                GridView1.RefreshData()

            Catch ex As DebtPlus.LINQ.DataValidationException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating action_plan table")

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating action_plan table")
            End Try
        End Sub

        ''' <summary>
        ''' Create the item in the current view
        ''' </summary>
        ''' <remarks></remarks>
        Protected Overrides Sub OnCreateItem()

            ' Invoke the edit function on the plan
            Dim editThread As New System.Threading.Thread(New System.Threading.ParameterizedThreadStart(AddressOf CreateRecordThread)) With {.IsBackground = False, .Name = "ActionPlan"}
            editThread.SetApartmentState(ApartmentState.STA)
            editThread.Start(Nothing)
        End Sub

        ''' <summary>
        ''' Edit the current item.
        ''' </summary>
        ''' <param name="obj">Pointer to the record to be updated</param>
        ''' <remarks></remarks>
        Protected Overrides Sub OnEditItem(ByVal obj As Object)
            Dim record As action_plan = TryCast(obj, action_plan)
            If record Is Nothing Then
                Return
            End If

            ' Invoke the edit function on the plan
            Dim editThread As New System.Threading.Thread(New System.Threading.ParameterizedThreadStart(AddressOf EditRecordThread)) With {.IsBackground = False, .Name = "ActionPlan"}
            editThread.SetApartmentState(ApartmentState.STA)
            editThread.Start(record)
        End Sub

        ''' <summary>
        ''' Handle the create function in a different thread so that the original form is available.
        ''' </summary>
        ''' <param name="obj">Nothing</param>
        ''' <remarks></remarks>
        Private Sub CreateRecordThread(ByVal obj As Object)

            Dim record As DebtPlus.LINQ.action_plan = DebtPlus.LINQ.Factory.Manufacture_action_plan()
            record.client = Context.ClientDs.ClientId

            Using frm As New Form_ActionPlan(bc, record)
                Dim result As System.Windows.Forms.DialogResult = frm.ShowDialog()
                CompleteCreate(record, result)
            End Using
        End Sub

        ''' <summary>
        ''' Marshal the thread back to the gridcontrol and update the database and grid with the new record.
        ''' </summary>
        ''' <param name="record">Pointer to the new record</param>
        ''' <remarks></remarks>
        Private Sub CompleteCreate(ByVal record As action_plan, ByVal completionResult As System.Windows.Forms.DialogResult)
            If GridControl1.InvokeRequired Then
                Dim ia As System.IAsyncResult = GridControl1.BeginInvoke(New CompleteCreateDelegate(AddressOf CompleteCreate), New Object() {record, completionResult})
                GridControl1.EndInvoke(ia)
                Return
            End If

            ' Update the list and the grid to show the new plan
            Try
                If completionResult = DialogResult.OK Then
                    bc.action_plans.InsertOnSubmit(record)
                    bc.SubmitChanges()

                    colPlans.Add(record)
                    GridView1.RefreshData()
                End If

            Catch ex As DebtPlus.LINQ.DataValidationException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating action_plan table")

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating action_plan table")
            End Try
        End Sub

        ''' <summary>
        ''' Perform the edit operation on the action plan
        ''' </summary>
        ''' <param name="obj">Pointer to the record to be edited</param>
        ''' <remarks></remarks>
        Private Sub EditRecordThread(ByVal obj As Object)
            Dim record As action_plan = TryCast(obj, action_plan)
            If record Is Nothing Then
                Return
            End If

            Using frm As New Form_ActionPlan(bc, record)
                Dim result As System.Windows.Forms.DialogResult = frm.ShowDialog()
                CompleteEdit(record, result)
                Return
            End Using
        End Sub

        ''' <summary>
        ''' Marshal the thread back to the gridcontrol and update the database and grid with the record
        ''' </summary>
        ''' <param name="record">Pointer to the record that was updated</param>
        ''' <remarks></remarks>
        Private Sub CompleteEdit(ByVal record As action_plan, ByVal completionResult As System.Windows.Forms.DialogResult)

            If GridControl1.InvokeRequired Then
                Dim ia As System.IAsyncResult = GridControl1.BeginInvoke(New CompleteCreateDelegate(AddressOf CompleteEdit), New Object() {record, completionResult})
                GridControl1.EndInvoke(ia)
                Return
            End If

            ' Update the record in the database
            Try
                If (completionResult = System.Windows.Forms.DialogResult.OK) Then
                    bc.SubmitChanges()
                Else
                    bc.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, record)
                End If
                GridView1.RefreshData()

            Catch ex As DebtPlus.LINQ.DataValidationException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating action_plan table")

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating action_plan table")
            End Try
        End Sub
    End Class
End Namespace