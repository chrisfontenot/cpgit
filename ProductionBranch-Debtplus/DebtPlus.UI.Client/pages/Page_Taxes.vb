#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports System.ComponentModel
Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.UI.Client.controls
Imports DevExpress.XtraEditors

Namespace pages

    Friend Class Page_Taxes
        Inherits ControlBaseClient

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler fed_tax_months.Spin, AddressOf months_Spin
            AddHandler fed_tax_months.Validated, AddressOf FedTaxes_EditValueChanging
            AddHandler fed_tax_months.Validating, AddressOf DebtPlus.Data.Validation.NonNegative
            AddHandler fed_tax_owed.Validated, AddressOf FedTaxes_EditValueChanging
            AddHandler fed_tax_owed.EditValueChanging, AddressOf FedTaxes_EditValueChanging
            AddHandler local_tax_months.Spin, AddressOf months_Spin
            AddHandler local_tax_months.Validating, AddressOf DebtPlus.Data.Validation.NonNegative
            AddHandler state_tax_months.Spin, AddressOf months_Spin
            AddHandler state_tax_months.Validating, AddressOf DebtPlus.Data.Validation.NonNegative
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler fed_tax_months.Spin, AddressOf months_Spin
            RemoveHandler fed_tax_months.Validated, AddressOf FedTaxes_EditValueChanging
            RemoveHandler fed_tax_months.Validating, AddressOf DebtPlus.Data.Validation.NonNegative

            RemoveHandler fed_tax_owed.Validated, AddressOf FedTaxes_EditValueChanging
            RemoveHandler fed_tax_owed.EditValueChanging, AddressOf FedTaxes_EditValueChanging

            RemoveHandler local_tax_months.Spin, AddressOf months_Spin
            RemoveHandler local_tax_months.Validating, AddressOf DebtPlus.Data.Validation.NonNegative

            RemoveHandler state_tax_months.Spin, AddressOf months_Spin
            RemoveHandler state_tax_months.Validating, AddressOf DebtPlus.Data.Validation.NonNegative
        End Sub

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents fed_tax_banner As DevExpress.XtraEditors.GroupControl
        Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents fed_tax_owed As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents fed_tax_months As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents state_tax_months As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents state_tax_owed As DevExpress.XtraEditors.CalcEdit
        Friend WithEvents local_tax_months As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents local_tax_owed As DevExpress.XtraEditors.CalcEdit

        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager =
                    New System.ComponentModel.ComponentResourceManager(GetType(Page_Taxes))
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl
            Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl
            Me.fed_tax_banner = New DevExpress.XtraEditors.GroupControl
            Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl
            Me.fed_tax_owed = New DevExpress.XtraEditors.CalcEdit
            Me.fed_tax_months = New DevExpress.XtraEditors.SpinEdit
            Me.state_tax_months = New DevExpress.XtraEditors.SpinEdit
            Me.state_tax_owed = New DevExpress.XtraEditors.CalcEdit
            Me.local_tax_months = New DevExpress.XtraEditors.SpinEdit
            Me.local_tax_owed = New DevExpress.XtraEditors.CalcEdit
            CType(Me.fed_tax_banner, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.fed_tax_banner.SuspendLayout()
            CType(Me.fed_tax_owed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.fed_tax_months.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.state_tax_months.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.state_tax_owed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.local_tax_months.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.local_tax_owed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular,
                                                                       System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.LabelControl1.Appearance.Options.UseFont = True
            Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal
            Me.LabelControl1.Location = New System.Drawing.Point(216, 16)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(145, 23)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Delinquent Taxes"
            Me.LabelControl1.UseMnemonic = False
            '
            'LabelControl2
            '
            Me.LabelControl2.Appearance.Options.UseTextOptions = True
            Me.LabelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl2.Location = New System.Drawing.Point(169, 80)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(37, 13)
            Me.LabelControl2.TabIndex = 1
            Me.LabelControl2.Text = "Amount"
            '
            'LabelControl3
            '
            Me.LabelControl3.Appearance.Options.UseTextOptions = True
            Me.LabelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.LabelControl3.Location = New System.Drawing.Point(269, 80)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(35, 13)
            Me.LabelControl3.TabIndex = 2
            Me.LabelControl3.Text = "Months"
            '
            'LabelControl4
            '
            Me.LabelControl4.Location = New System.Drawing.Point(56, 104)
            Me.LabelControl4.Name = "LabelControl4"
            Me.LabelControl4.Size = New System.Drawing.Size(36, 13)
            Me.LabelControl4.TabIndex = 3
            Me.LabelControl4.Text = "Federal"
            '
            'LabelControl5
            '
            Me.LabelControl5.Location = New System.Drawing.Point(56, 128)
            Me.LabelControl5.Name = "LabelControl5"
            Me.LabelControl5.Size = New System.Drawing.Size(26, 13)
            Me.LabelControl5.TabIndex = 4
            Me.LabelControl5.Text = "State"
            '
            'LabelControl6
            '
            Me.LabelControl6.Location = New System.Drawing.Point(56, 152)
            Me.LabelControl6.Name = "LabelControl6"
            Me.LabelControl6.Size = New System.Drawing.Size(24, 13)
            Me.LabelControl6.TabIndex = 5
            Me.LabelControl6.Text = "Local"
            '
            'fed_tax_banner
            '
            Me.fed_tax_banner.Appearance.BackColor = System.Drawing.Color.Red
            Me.fed_tax_banner.Appearance.BackColor2 = System.Drawing.Color.Red
            Me.fed_tax_banner.Appearance.BorderColor = System.Drawing.Color.Red
            Me.fed_tax_banner.Appearance.ForeColor = System.Drawing.Color.White
            Me.fed_tax_banner.Appearance.Options.UseBackColor = True
            Me.fed_tax_banner.Appearance.Options.UseBorderColor = True
            Me.fed_tax_banner.Appearance.Options.UseForeColor = True
            Me.fed_tax_banner.AppearanceCaption.BackColor = System.Drawing.Color.Red
            Me.fed_tax_banner.AppearanceCaption.BackColor2 = System.Drawing.Color.Red
            Me.fed_tax_banner.AppearanceCaption.BorderColor = System.Drawing.Color.Red
            Me.fed_tax_banner.AppearanceCaption.ForeColor = System.Drawing.Color.White
            Me.fed_tax_banner.AppearanceCaption.Options.UseBackColor = True
            Me.fed_tax_banner.AppearanceCaption.Options.UseBorderColor = True
            Me.fed_tax_banner.AppearanceCaption.Options.UseForeColor = True
            Me.fed_tax_banner.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly
            Me.fed_tax_banner.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
            Me.fed_tax_banner.Controls.Add(Me.LabelControl7)
            Me.fed_tax_banner.Location = New System.Drawing.Point(56, 184)
            Me.fed_tax_banner.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat
            Me.fed_tax_banner.LookAndFeel.UseDefaultLookAndFeel = False
            Me.fed_tax_banner.Name = "fed_tax_banner"
            Me.fed_tax_banner.ShowCaption = False
            Me.fed_tax_banner.Size = New System.Drawing.Size(440, 72)
            Me.fed_tax_banner.TabIndex = 8
            Me.fed_tax_banner.Text = "GroupControl1"
            Me.fed_tax_banner.Visible = False
            '
            'LabelControl7
            '
            Me.LabelControl7.Anchor =
                CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                       Or System.Windows.Forms.AnchorStyles.Right), 
                      System.Windows.Forms.AnchorStyles)
            Me.LabelControl7.Appearance.BackColor = System.Drawing.Color.Red
            Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl7.Appearance.ForeColor = System.Drawing.Color.White
            Me.LabelControl7.Appearance.Options.UseBackColor = True
            Me.LabelControl7.Appearance.Options.UseFont = True
            Me.LabelControl7.Appearance.Options.UseForeColor = True
            Me.LabelControl7.Appearance.Options.UseTextOptions = True
            Me.LabelControl7.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.LabelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl7.Location = New System.Drawing.Point(3, 3)
            Me.LabelControl7.Name = "LabelControl7"
            Me.LabelControl7.Size = New System.Drawing.Size(434, 66)
            Me.LabelControl7.TabIndex = 8
            Me.LabelControl7.Text = resources.GetString("LabelControl7.Text")
            Me.LabelControl7.UseMnemonic = False
            '
            'fed_tax_owed
            '
            Me.fed_tax_owed.Location = New System.Drawing.Point(144, 104)
            Me.fed_tax_owed.Name = "fed_tax_owed"
            Me.fed_tax_owed.Properties.Appearance.Options.UseTextOptions = True
            Me.fed_tax_owed.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.fed_tax_owed.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() _
                                                           {New DevExpress.XtraEditors.Controls.EditorButton(
                                                               DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.fed_tax_owed.Properties.DisplayFormat.FormatString = "c"
            Me.fed_tax_owed.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.fed_tax_owed.Properties.Mask.BeepOnError = True
            Me.fed_tax_owed.Properties.Mask.EditMask = "c"
            Me.fed_tax_owed.Properties.Mask.IgnoreMaskBlank = False
            Me.fed_tax_owed.Properties.Mask.SaveLiteral = False
            Me.fed_tax_owed.Properties.Mask.ShowPlaceHolders = False
            Me.fed_tax_owed.Properties.Mask.UseMaskAsDisplayFormat = True
            Me.fed_tax_owed.Properties.Precision = 2
            Me.fed_tax_owed.Size = New System.Drawing.Size(100, 20)
            Me.fed_tax_owed.TabIndex = 9
            Me.fed_tax_owed.ToolTip = "Dollar amount of back taxes owed to the US Tresurary"
            '
            'fed_tax_months
            '
            Me.fed_tax_months.EditValue = New Decimal(New Int32() {0, 0, 0, 0})
            Me.fed_tax_months.Location = New System.Drawing.Point(272, 104)
            Me.fed_tax_months.Name = "fed_tax_months"
            Me.fed_tax_months.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() _
                                                             {New DevExpress.XtraEditors.Controls.EditorButton})
            Me.fed_tax_months.Properties.IsFloatValue = False
            Me.fed_tax_months.Properties.Mask.EditMask = "N00"
            Me.fed_tax_months.Size = New System.Drawing.Size(72, 20)
            Me.fed_tax_months.TabIndex = 10
            Me.fed_tax_months.ToolTip = "How many months has the client owed this amount?"
            '
            'state_tax_months
            '
            Me.state_tax_months.EditValue = New Decimal(New Int32() {0, 0, 0, 0})
            Me.state_tax_months.Location = New System.Drawing.Point(272, 128)
            Me.state_tax_months.Name = "state_tax_months"
            Me.state_tax_months.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() _
                                                               {New DevExpress.XtraEditors.Controls.EditorButton})
            Me.state_tax_months.Properties.IsFloatValue = False
            Me.state_tax_months.Properties.Mask.EditMask = "N00"
            Me.state_tax_months.Size = New System.Drawing.Size(72, 20)
            Me.state_tax_months.TabIndex = 12
            Me.state_tax_months.ToolTip = "How many months has the client owed this amount to the state?"
            '
            'state_tax_owed
            '
            Me.state_tax_owed.Location = New System.Drawing.Point(144, 128)
            Me.state_tax_owed.Name = "state_tax_owed"
            Me.state_tax_owed.Properties.Appearance.Options.UseTextOptions = True
            Me.state_tax_owed.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.state_tax_owed.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() _
                                                             {New DevExpress.XtraEditors.Controls.EditorButton(
                                                                 DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.state_tax_owed.Properties.DisplayFormat.FormatString = "c"
            Me.state_tax_owed.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.state_tax_owed.Properties.Mask.BeepOnError = True
            Me.state_tax_owed.Properties.Mask.EditMask = "c"
            Me.state_tax_owed.Properties.Mask.IgnoreMaskBlank = False
            Me.state_tax_owed.Properties.Mask.SaveLiteral = False
            Me.state_tax_owed.Properties.Mask.ShowPlaceHolders = False
            Me.state_tax_owed.Properties.Mask.UseMaskAsDisplayFormat = True
            Me.state_tax_owed.Properties.Precision = 2
            Me.state_tax_owed.Size = New System.Drawing.Size(100, 20)
            Me.state_tax_owed.TabIndex = 11
            Me.state_tax_owed.ToolTip = "Amount of money owed to the state for back taxes"
            '
            'local_tax_months
            '
            Me.local_tax_months.EditValue = New Decimal(New Int32() {0, 0, 0, 0})
            Me.local_tax_months.Location = New System.Drawing.Point(272, 152)
            Me.local_tax_months.Name = "local_tax_months"
            Me.local_tax_months.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() _
                                                               {New DevExpress.XtraEditors.Controls.EditorButton})
            Me.local_tax_months.Properties.IsFloatValue = False
            Me.local_tax_months.Properties.Mask.EditMask = "N00"
            Me.local_tax_months.Size = New System.Drawing.Size(72, 20)
            Me.local_tax_months.TabIndex = 14
            Me.local_tax_months.ToolTip = "How many months has the client owed this amount to the local governments?"
            '
            'local_tax_owed
            '
            Me.local_tax_owed.Location = New System.Drawing.Point(144, 152)
            Me.local_tax_owed.Name = "local_tax_owed"
            Me.local_tax_owed.Properties.Appearance.Options.UseTextOptions = True
            Me.local_tax_owed.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.local_tax_owed.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() _
                                                             {New DevExpress.XtraEditors.Controls.EditorButton(
                                                                 DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.local_tax_owed.Properties.DisplayFormat.FormatString = "c"
            Me.local_tax_owed.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.local_tax_owed.Properties.Mask.BeepOnError = True
            Me.local_tax_owed.Properties.Mask.EditMask = "c"
            Me.local_tax_owed.Properties.Mask.IgnoreMaskBlank = False
            Me.local_tax_owed.Properties.Mask.SaveLiteral = False
            Me.local_tax_owed.Properties.Mask.ShowPlaceHolders = False
            Me.local_tax_owed.Properties.Mask.UseMaskAsDisplayFormat = True
            Me.local_tax_owed.Properties.Precision = 2
            Me.local_tax_owed.Size = New System.Drawing.Size(100, 20)
            Me.local_tax_owed.TabIndex = 13
            Me.local_tax_owed.ToolTip = "Amount of money owed to the local/city/county governments for back taxes?"
            '
            'Page_Taxes
            '
            Me.Controls.Add(Me.local_tax_months)
            Me.Controls.Add(Me.local_tax_owed)
            Me.Controls.Add(Me.state_tax_months)
            Me.Controls.Add(Me.state_tax_owed)
            Me.Controls.Add(Me.fed_tax_months)
            Me.Controls.Add(Me.fed_tax_owed)
            Me.Controls.Add(Me.fed_tax_banner)
            Me.Controls.Add(Me.LabelControl6)
            Me.Controls.Add(Me.LabelControl5)
            Me.Controls.Add(Me.LabelControl4)
            Me.Controls.Add(Me.LabelControl3)
            Me.Controls.Add(Me.LabelControl2)
            Me.Controls.Add(Me.LabelControl1)
            Me.Name = "Page_Taxes"
            CType(Me.fed_tax_banner, System.ComponentModel.ISupportInitialize).EndInit()
            Me.fed_tax_banner.ResumeLayout(False)
            CType(Me.fed_tax_owed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.fed_tax_months.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.state_tax_months.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.state_tax_owed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.local_tax_months.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.local_tax_owed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()
        End Sub

#End Region

        ''' <summary>
        ''' Load the page with the information from the clients table
        ''' </summary>
        Public Overrides Sub ReadForm(bc As DebtPlus.LINQ.BusinessContext)
            UnRegisterHandlers()
            Try
                fed_tax_owed.EditValue = Context.ClientDs.clientRecord.fed_tax_owed
                fed_tax_months.EditValue = Context.ClientDs.clientRecord.fed_tax_months
                state_tax_owed.EditValue = Context.ClientDs.clientRecord.state_tax_owed
                state_tax_months.EditValue = Context.ClientDs.clientRecord.state_tax_months
                local_tax_owed.EditValue = Context.ClientDs.clientRecord.local_tax_owed
                local_tax_months.EditValue = Context.ClientDs.clientRecord.local_tax_months

                EnableDisplayBanner(Context.ClientDs.clientRecord.fed_tax_owed)
            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Write any changes to the database for this page
        ''' </summary>
        Public Overrides Sub SaveForm(bc As DebtPlus.LINQ.BusinessContext)

            Context.ClientDs.clientRecord.fed_tax_owed = DebtPlus.Utils.Nulls.v_Decimal(fed_tax_owed.EditValue).GetValueOrDefault()
            Context.ClientDs.clientRecord.fed_tax_months = DebtPlus.Utils.Nulls.v_Int32(fed_tax_months.EditValue).GetValueOrDefault()
            Context.ClientDs.clientRecord.state_tax_owed = DebtPlus.Utils.Nulls.v_Decimal(state_tax_owed.EditValue).GetValueOrDefault()
            Context.ClientDs.clientRecord.state_tax_months = DebtPlus.Utils.Nulls.v_Int32(state_tax_months.EditValue).GetValueOrDefault()
            Context.ClientDs.clientRecord.local_tax_owed = DebtPlus.Utils.Nulls.v_Decimal(local_tax_owed.EditValue).GetValueOrDefault()
            Context.ClientDs.clientRecord.local_tax_months = DebtPlus.Utils.Nulls.v_Int32(local_tax_months.EditValue).GetValueOrDefault()

        End Sub

        ''' <summary>
        ''' Handle a change in the display of the federal tax banner
        ''' </summary>
        Private Sub FedTaxes_EditValueChanging(ByVal sender As Object, ByVal e As EventArgs)
            EnableDisplayBanner(Convert.ToDecimal(fed_tax_owed.EditValue))
        End Sub

        ''' <summary>
        ''' Show or hide the federal tax banner as the numbers change
        ''' </summary>
        Private Sub EnableDisplayBanner(ByVal Amount As Decimal)
            fed_tax_banner.Visible = Amount > 0
        End Sub

        ''' <summary>
        ''' Do not allow the user to spin the months negative
        ''' </summary>
        Private Sub months_Spin(ByVal sender As Object, ByVal e As SpinEventArgs)

            ' Do not allow the spin to go negative
            With CType(sender, SpinEdit)
                If Not e.IsSpinUp Then
                    Dim Value As Int32 = 0
                    Dim obj As Object = .EditValue
                    If obj IsNot Nothing AndAlso obj IsNot DBNull.Value Then Value = Convert.ToInt32(obj)
                    If Value = 0 Then e.Handled = True
                End If
            End With
        End Sub
    End Class
End Namespace
