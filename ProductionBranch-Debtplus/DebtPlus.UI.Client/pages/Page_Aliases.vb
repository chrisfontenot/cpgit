#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.UI.Client.controls
Imports DevExpress.XtraLayout.Utils
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo
Imports System.Collections.Generic
Imports DebtPlus.LINQ
Imports System.Linq
Imports DevExpress.XtraGrid.Views.Base
Imports DebtPlus.UI.Client.forms.Goal
Imports DevExpress.XtraGrid.Views.Grid

Namespace pages

    Friend Class Page_Aliases
        Inherits ControlBaseClient

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents agreement_for_services As DevExpress.XtraEditors.LabelControl
        Friend WithEvents AliasesControl1 As AliasesControl
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LookUpEdit_satisfaction_score As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents lk_Cause_Fin_Problem3 As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents lk_Cause_Fin_Problem2 As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents lk_Cause_Fin_Problem1 As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LayoutControlGroup3 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents lk_Cause_Fin_Problem4 As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents IndicatorsControl1 As DebtPlus.UI.Client.controls.IndicatorsControl
        Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlGroup4 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents gridControl1 As DevExpress.XtraGrid.GridControl
        Friend WithEvents gridView1 As DevExpress.XtraGrid.Views.Grid.GridView
        Friend WithEvents gridcolumn_GoalID As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents gridcolumn_CreatedBy As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents gridcolumn_Created As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents gridcolumn_LOS As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents gridcolumn_Applicant As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents gridcolumn_Coapplicant As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents gridcolumn_ModifiedBy As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents gridcolumn_ModifiedDate As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents PopupMenu_Items As DevExpress.XtraBars.PopupMenu
        Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
        Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
        Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
        Friend WithEvents BarButtonItem_Add As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Change As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents BarButtonItem_Delete As DevExpress.XtraBars.BarButtonItem
        Friend WithEvents LayoutControlItem_ShowAgreement As DevExpress.XtraLayout.LayoutControlItem

        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim GridLevelNode1 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.gridControl1 = New DevExpress.XtraGrid.GridControl()
            Me.gridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
            Me.gridcolumn_GoalID = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.gridcolumn_CreatedBy = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.gridcolumn_Created = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.gridcolumn_LOS = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.gridcolumn_Applicant = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.gridcolumn_Coapplicant = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.gridcolumn_ModifiedBy = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.gridcolumn_ModifiedDate = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.IndicatorsControl1 = New DebtPlus.UI.Client.controls.IndicatorsControl()
            Me.lk_Cause_Fin_Problem4 = New DevExpress.XtraEditors.LookUpEdit()
            Me.lk_Cause_Fin_Problem3 = New DevExpress.XtraEditors.LookUpEdit()
            Me.lk_Cause_Fin_Problem2 = New DevExpress.XtraEditors.LookUpEdit()
            Me.lk_Cause_Fin_Problem1 = New DevExpress.XtraEditors.LookUpEdit()
            Me.LookUpEdit_satisfaction_score = New DevExpress.XtraEditors.LookUpEdit()
            Me.AliasesControl1 = New DebtPlus.UI.Client.controls.AliasesControl()
            Me.agreement_for_services = New DevExpress.XtraEditors.LabelControl()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_ShowAgreement = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlGroup3 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlGroup4 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.PopupMenu_Items = New DevExpress.XtraBars.PopupMenu()
            Me.BarButtonItem_Add = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Change = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Delete = New DevExpress.XtraBars.BarButtonItem()
            Me.BarManager1 = New DevExpress.XtraBars.BarManager()
            Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.gridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.gridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.IndicatorsControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lk_Cause_Fin_Problem4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lk_Cause_Fin_Problem3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lk_Cause_Fin_Problem2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.lk_Cause_Fin_Problem1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_satisfaction_score.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.AliasesControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_ShowAgreement, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PopupMenu_Items, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Appearance.ImageAlign = System.Drawing.ContentAlignment.TopLeft
            Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top
            Me.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
            Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl1.Location = New System.Drawing.Point(12, 12)
            Me.LabelControl1.MinimumSize = New System.Drawing.Size(0, 50)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(260, 50)
            Me.LabelControl1.StyleController = Me.LayoutControl1
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "This form lists the alias names by which the client may have been known.  You are" & _
        " free to use any additional names or sequences by which this client may be found" & _
        "."
            Me.LabelControl1.UseMnemonic = False
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.gridControl1)
            Me.LayoutControl1.Controls.Add(Me.IndicatorsControl1)
            Me.LayoutControl1.Controls.Add(Me.lk_Cause_Fin_Problem4)
            Me.LayoutControl1.Controls.Add(Me.lk_Cause_Fin_Problem3)
            Me.LayoutControl1.Controls.Add(Me.lk_Cause_Fin_Problem2)
            Me.LayoutControl1.Controls.Add(Me.lk_Cause_Fin_Problem1)
            Me.LayoutControl1.Controls.Add(Me.LookUpEdit_satisfaction_score)
            Me.LayoutControl1.Controls.Add(Me.AliasesControl1)
            Me.LayoutControl1.Controls.Add(Me.agreement_for_services)
            Me.LayoutControl1.Controls.Add(Me.LabelControl1)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(448, 104, 250, 350)
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(576, 344)
            Me.LayoutControl1.TabIndex = 4
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'gridControl1
            '
            Me.gridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            GridLevelNode1.RelationName = "Level1"
            Me.gridControl1.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode1})
            Me.gridControl1.Location = New System.Drawing.Point(298, 183)
            Me.gridControl1.MainView = Me.gridView1
            Me.gridControl1.Name = "gridControl1"
            Me.gridControl1.Size = New System.Drawing.Size(254, 113)
            Me.gridControl1.TabIndex = 34
            Me.gridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gridView1})
            '
            'gridView1
            '
            Me.gridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.gridcolumn_GoalID, Me.gridcolumn_CreatedBy, Me.gridcolumn_Created, Me.gridcolumn_LOS, Me.gridcolumn_Applicant, Me.gridcolumn_Coapplicant, Me.gridcolumn_ModifiedBy, Me.gridcolumn_ModifiedDate})
            Me.gridView1.GridControl = Me.gridControl1
            Me.gridView1.Name = "gridView1"
            Me.gridView1.OptionsBehavior.Editable = False
            Me.gridView1.OptionsNavigation.AutoFocusNewRow = True
            Me.gridView1.OptionsNavigation.EnterMoveNextColumn = True
            Me.gridView1.OptionsPrint.EnableAppearanceEvenRow = True
            Me.gridView1.OptionsPrint.EnableAppearanceOddRow = True
            Me.gridView1.OptionsView.EnableAppearanceEvenRow = True
            Me.gridView1.OptionsView.EnableAppearanceOddRow = True
            Me.gridView1.OptionsView.RowAutoHeight = True
            Me.gridView1.OptionsView.ShowGroupPanel = False
            Me.gridView1.OptionsView.ShowIndicator = False
            '
            'gridcolumn_GoalID
            '
            Me.gridcolumn_GoalID.Caption = "Goal ID"
            Me.gridcolumn_GoalID.FieldName = "GoalID"
            Me.gridcolumn_GoalID.Name = "gridcolumn_GoalID"
            Me.gridcolumn_GoalID.Visible = True
            Me.gridcolumn_GoalID.VisibleIndex = 0
            '
            'gridcolumn_CreatedBy
            '
            Me.gridcolumn_CreatedBy.Caption = "Created By"
            Me.gridcolumn_CreatedBy.FieldName = "CreatedBy"
            Me.gridcolumn_CreatedBy.Name = "gridcolumn_CreatedBy"
            Me.gridcolumn_CreatedBy.Visible = True
            Me.gridcolumn_CreatedBy.VisibleIndex = 1
            '
            'gridcolumn_Created
            '
            Me.gridcolumn_Created.Caption = "Created Date"
            Me.gridcolumn_Created.DisplayFormat.FormatString = "mm/dd/yyyy"
            Me.gridcolumn_Created.FieldName = "CreatedTimestamp"
            Me.gridcolumn_Created.Name = "gridcolumn_Created"
            Me.gridcolumn_Created.Visible = True
            Me.gridcolumn_Created.VisibleIndex = 2
            '
            'gridcolumn_LOS
            '
            Me.gridcolumn_LOS.Caption = "LOS"
            Me.gridcolumn_LOS.FieldName = "LineOfService"
            Me.gridcolumn_LOS.Name = "gridcolumn_LOS"
            Me.gridcolumn_LOS.Visible = True
            Me.gridcolumn_LOS.VisibleIndex = 3
            '
            'gridcolumn_Applicant
            '
            Me.gridcolumn_Applicant.Caption = "Applicant"
            Me.gridcolumn_Applicant.FieldName = "Applicant"
            Me.gridcolumn_Applicant.Name = "gridcolumn_Applicant"
            Me.gridcolumn_Applicant.Visible = True
            Me.gridcolumn_Applicant.VisibleIndex = 4
            '
            'gridcolumn_Coapplicant
            '
            Me.gridcolumn_Coapplicant.Caption = "CoApplicant"
            Me.gridcolumn_Coapplicant.FieldName = "Coapplicant"
            Me.gridcolumn_Coapplicant.Name = "gridcolumn_Coapplicant"
            Me.gridcolumn_Coapplicant.Visible = True
            Me.gridcolumn_Coapplicant.VisibleIndex = 5
            '
            'gridcolumn_ModifiedBy
            '
            Me.gridcolumn_ModifiedBy.Caption = "Modified By"
            Me.gridcolumn_ModifiedBy.FieldName = "ModifiedBy"
            Me.gridcolumn_ModifiedBy.Name = "gridcolumn_ModifiedBy"
            Me.gridcolumn_ModifiedBy.Visible = True
            Me.gridcolumn_ModifiedBy.VisibleIndex = 6
            '
            'gridcolumn_ModifiedDate
            '
            Me.gridcolumn_ModifiedDate.Caption = "Modified Date"
            Me.gridcolumn_ModifiedDate.DisplayFormat.FormatString = "mm/dd/yyyy"
            Me.gridcolumn_ModifiedDate.FieldName = "ModifiedTimestamp"
            Me.gridcolumn_ModifiedDate.Name = "gridcolumn_ModifiedDate"
            Me.gridcolumn_ModifiedDate.Visible = True
            Me.gridcolumn_ModifiedDate.VisibleIndex = 7
            '
            'IndicatorsControl1
            '
            Me.IndicatorsControl1.Location = New System.Drawing.Point(24, 194)
            Me.IndicatorsControl1.Name = "IndicatorsControl1"
            Me.IndicatorsControl1.Size = New System.Drawing.Size(236, 126)
            Me.IndicatorsControl1.TabIndex = 10
            '
            'lk_Cause_Fin_Problem4
            '
            Me.lk_Cause_Fin_Problem4.Location = New System.Drawing.Point(385, 116)
            Me.lk_Cause_Fin_Problem4.Name = "lk_Cause_Fin_Problem4"
            Me.lk_Cause_Fin_Problem4.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.lk_Cause_Fin_Problem4.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lk_Cause_Fin_Problem4.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.lk_Cause_Fin_Problem4.Properties.DisplayMember = "description"
            Me.lk_Cause_Fin_Problem4.Properties.NullText = ""
            Me.lk_Cause_Fin_Problem4.Properties.ShowFooter = False
            Me.lk_Cause_Fin_Problem4.Properties.ShowHeader = False
            Me.lk_Cause_Fin_Problem4.Properties.SortColumnIndex = 1
            Me.lk_Cause_Fin_Problem4.Properties.ValueMember = "Id"
            Me.lk_Cause_Fin_Problem4.Size = New System.Drawing.Size(167, 20)
            Me.lk_Cause_Fin_Problem4.StyleController = Me.LayoutControl1
            Me.lk_Cause_Fin_Problem4.TabIndex = 9
            '
            'lk_Cause_Fin_Problem3
            '
            Me.lk_Cause_Fin_Problem3.Location = New System.Drawing.Point(385, 92)
            Me.lk_Cause_Fin_Problem3.Name = "lk_Cause_Fin_Problem3"
            Me.lk_Cause_Fin_Problem3.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.lk_Cause_Fin_Problem3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lk_Cause_Fin_Problem3.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.lk_Cause_Fin_Problem3.Properties.DisplayMember = "description"
            Me.lk_Cause_Fin_Problem3.Properties.NullText = ""
            Me.lk_Cause_Fin_Problem3.Properties.ShowFooter = False
            Me.lk_Cause_Fin_Problem3.Properties.ShowHeader = False
            Me.lk_Cause_Fin_Problem3.Properties.SortColumnIndex = 1
            Me.lk_Cause_Fin_Problem3.Properties.ValueMember = "Id"
            Me.lk_Cause_Fin_Problem3.Size = New System.Drawing.Size(167, 20)
            Me.lk_Cause_Fin_Problem3.StyleController = Me.LayoutControl1
            Me.lk_Cause_Fin_Problem3.TabIndex = 8
            '
            'lk_Cause_Fin_Problem2
            '
            Me.lk_Cause_Fin_Problem2.Location = New System.Drawing.Point(385, 68)
            Me.lk_Cause_Fin_Problem2.Name = "lk_Cause_Fin_Problem2"
            Me.lk_Cause_Fin_Problem2.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.lk_Cause_Fin_Problem2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lk_Cause_Fin_Problem2.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.lk_Cause_Fin_Problem2.Properties.DisplayMember = "description"
            Me.lk_Cause_Fin_Problem2.Properties.NullText = ""
            Me.lk_Cause_Fin_Problem2.Properties.ShowFooter = False
            Me.lk_Cause_Fin_Problem2.Properties.ShowHeader = False
            Me.lk_Cause_Fin_Problem2.Properties.SortColumnIndex = 1
            Me.lk_Cause_Fin_Problem2.Properties.ValueMember = "Id"
            Me.lk_Cause_Fin_Problem2.Size = New System.Drawing.Size(167, 20)
            Me.lk_Cause_Fin_Problem2.StyleController = Me.LayoutControl1
            Me.lk_Cause_Fin_Problem2.TabIndex = 7
            '
            'lk_Cause_Fin_Problem1
            '
            Me.lk_Cause_Fin_Problem1.Location = New System.Drawing.Point(385, 44)
            Me.lk_Cause_Fin_Problem1.Name = "lk_Cause_Fin_Problem1"
            Me.lk_Cause_Fin_Problem1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.lk_Cause_Fin_Problem1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lk_Cause_Fin_Problem1.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.lk_Cause_Fin_Problem1.Properties.DisplayMember = "description"
            Me.lk_Cause_Fin_Problem1.Properties.NullText = ""
            Me.lk_Cause_Fin_Problem1.Properties.ShowFooter = False
            Me.lk_Cause_Fin_Problem1.Properties.ShowHeader = False
            Me.lk_Cause_Fin_Problem1.Properties.SortColumnIndex = 1
            Me.lk_Cause_Fin_Problem1.Properties.ValueMember = "Id"
            Me.lk_Cause_Fin_Problem1.Size = New System.Drawing.Size(167, 20)
            Me.lk_Cause_Fin_Problem1.StyleController = Me.LayoutControl1
            Me.lk_Cause_Fin_Problem1.TabIndex = 6
            '
            'LookUpEdit_satisfaction_score
            '
            Me.LookUpEdit_satisfaction_score.Location = New System.Drawing.Point(373, 312)
            Me.LookUpEdit_satisfaction_score.Name = "LookUpEdit_satisfaction_score"
            Me.LookUpEdit_satisfaction_score.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.LookUpEdit_satisfaction_score.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_satisfaction_score.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True])})
            Me.LookUpEdit_satisfaction_score.Properties.DisplayMember = "description"
            Me.LookUpEdit_satisfaction_score.Properties.NullText = ""
            Me.LookUpEdit_satisfaction_score.Properties.ShowFooter = False
            Me.LookUpEdit_satisfaction_score.Properties.ShowHeader = False
            Me.LookUpEdit_satisfaction_score.Properties.SortColumnIndex = 1
            Me.LookUpEdit_satisfaction_score.Properties.ValueMember = "Id"
            Me.LookUpEdit_satisfaction_score.Size = New System.Drawing.Size(191, 20)
            Me.LookUpEdit_satisfaction_score.StyleController = Me.LayoutControl1
            Me.LookUpEdit_satisfaction_score.TabIndex = 5
            '
            'AliasesControl1
            '
            Me.AliasesControl1.Location = New System.Drawing.Point(12, 66)
            Me.AliasesControl1.Name = "AliasesControl1"
            Me.AliasesControl1.Size = New System.Drawing.Size(260, 75)
            Me.AliasesControl1.TabIndex = 1
            '
            'agreement_for_services
            '
            Me.agreement_for_services.Appearance.ForeColor = System.Drawing.Color.Red
            Me.agreement_for_services.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.agreement_for_services.Location = New System.Drawing.Point(12, 145)
            Me.agreement_for_services.Name = "agreement_for_services"
            Me.agreement_for_services.Size = New System.Drawing.Size(260, 13)
            Me.agreement_for_services.StyleController = Me.LayoutControl1
            Me.agreement_for_services.TabIndex = 3
            Me.agreement_for_services.Text = "Agreement for Services was signed electronically and is on file."
            Me.agreement_for_services.UseMnemonic = False
            Me.agreement_for_services.Visible = False
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.LayoutControlGroup1.GroupBordersVisible = False
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem_ShowAgreement, Me.LayoutControlGroup2, Me.LayoutControlGroup3, Me.LayoutControlGroup4, Me.LayoutControlItem4, Me.EmptySpaceItem1})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(576, 344)
            Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.AliasesControl1
            Me.LayoutControlItem1.CustomizationFormText = "List of aliases"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 54)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(264, 79)
            Me.LayoutControlItem1.Text = "List of aliases"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem1.TextToControlDistance = 0
            Me.LayoutControlItem1.TextVisible = False
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.LabelControl1
            Me.LayoutControlItem2.CustomizationFormText = "Instructions"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(264, 54)
            Me.LayoutControlItem2.Text = "Instructions"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem2.TextToControlDistance = 0
            Me.LayoutControlItem2.TextVisible = False
            '
            'LayoutControlItem_ShowAgreement
            '
            Me.LayoutControlItem_ShowAgreement.Control = Me.agreement_for_services
            Me.LayoutControlItem_ShowAgreement.CustomizationFormText = "Agreement For Services"
            Me.LayoutControlItem_ShowAgreement.Location = New System.Drawing.Point(0, 133)
            Me.LayoutControlItem_ShowAgreement.Name = "LayoutControlItem_ShowAgreement"
            Me.LayoutControlItem_ShowAgreement.Size = New System.Drawing.Size(264, 17)
            Me.LayoutControlItem_ShowAgreement.Text = "Agreement For Services"
            Me.LayoutControlItem_ShowAgreement.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem_ShowAgreement.TextToControlDistance = 0
            Me.LayoutControlItem_ShowAgreement.TextVisible = False
            '
            'LayoutControlGroup2
            '
            Me.LayoutControlGroup2.CustomizationFormText = "Miscellaneous Indicators"
            Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem9})
            Me.LayoutControlGroup2.Location = New System.Drawing.Point(0, 150)
            Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
            Me.LayoutControlGroup2.Size = New System.Drawing.Size(264, 174)
            Me.LayoutControlGroup2.Text = "Miscellaneous Indicators"
            '
            'LayoutControlItem9
            '
            Me.LayoutControlItem9.Control = Me.IndicatorsControl1
            Me.LayoutControlItem9.CustomizationFormText = "LayoutControlItem9"
            Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem9.Name = "LayoutControlItem9"
            Me.LayoutControlItem9.Size = New System.Drawing.Size(240, 130)
            Me.LayoutControlItem9.Text = "LayoutControlItem9"
            Me.LayoutControlItem9.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem9.TextToControlDistance = 0
            Me.LayoutControlItem9.TextVisible = False
            '
            'LayoutControlGroup3
            '
            Me.LayoutControlGroup3.CustomizationFormText = "Cause of Financial Problem"
            Me.LayoutControlGroup3.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem5, Me.LayoutControlItem6, Me.LayoutControlItem7, Me.LayoutControlItem8})
            Me.LayoutControlGroup3.Location = New System.Drawing.Point(274, 0)
            Me.LayoutControlGroup3.Name = "LayoutControlGroup3"
            Me.LayoutControlGroup3.Size = New System.Drawing.Size(282, 139)
            Me.LayoutControlGroup3.Text = "Cause of Financial Problems"
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Control = Me.lk_Cause_Fin_Problem1
            Me.LayoutControlItem5.CustomizationFormText = "LayoutControlItem5"
            Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem5.Name = "LayoutControlItem5"
            Me.LayoutControlItem5.Size = New System.Drawing.Size(258, 24)
            Me.LayoutControlItem5.Text = "1st"
            Me.LayoutControlItem5.TextSize = New System.Drawing.Size(84, 13)
            '
            'LayoutControlItem6
            '
            Me.LayoutControlItem6.Control = Me.lk_Cause_Fin_Problem2
            Me.LayoutControlItem6.CustomizationFormText = "LayoutControlItem6"
            Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem6.Name = "LayoutControlItem6"
            Me.LayoutControlItem6.Size = New System.Drawing.Size(258, 24)
            Me.LayoutControlItem6.Text = "2nd"
            Me.LayoutControlItem6.TextSize = New System.Drawing.Size(84, 13)
            '
            'LayoutControlItem7
            '
            Me.LayoutControlItem7.Control = Me.lk_Cause_Fin_Problem3
            Me.LayoutControlItem7.CustomizationFormText = "LayoutControlItem7"
            Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 48)
            Me.LayoutControlItem7.Name = "LayoutControlItem7"
            Me.LayoutControlItem7.Size = New System.Drawing.Size(258, 24)
            Me.LayoutControlItem7.Text = "3rd"
            Me.LayoutControlItem7.TextSize = New System.Drawing.Size(84, 13)
            '
            'LayoutControlItem8
            '
            Me.LayoutControlItem8.Control = Me.lk_Cause_Fin_Problem4
            Me.LayoutControlItem8.CustomizationFormText = "4th"
            Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 72)
            Me.LayoutControlItem8.Name = "LayoutControlItem8"
            Me.LayoutControlItem8.Size = New System.Drawing.Size(258, 24)
            Me.LayoutControlItem8.Text = "4th"
            Me.LayoutControlItem8.TextSize = New System.Drawing.Size(84, 13)
            '
            'LayoutControlGroup4
            '
            Me.LayoutControlGroup4.CustomizationFormText = "Goal Tracking"
            Me.LayoutControlGroup4.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem3})
            Me.LayoutControlGroup4.Location = New System.Drawing.Point(274, 139)
            Me.LayoutControlGroup4.Name = "LayoutControlGroup4"
            Me.LayoutControlGroup4.Size = New System.Drawing.Size(282, 161)
            Me.LayoutControlGroup4.Text = "Goal Tracking"
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.gridControl1
            Me.LayoutControlItem3.CustomizationFormText = "LayoutControlItem3"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(258, 117)
            Me.LayoutControlItem3.Text = "LayoutControlItem3"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem3.TextToControlDistance = 0
            Me.LayoutControlItem3.TextVisible = False
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.LookUpEdit_satisfaction_score
            Me.LayoutControlItem4.CustomizationFormText = "Satisfaction Level"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(274, 300)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(282, 24)
            Me.LayoutControlItem4.Text = "Satisfaction Level"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(84, 13)
            '
            'EmptySpaceItem1
            '
            Me.EmptySpaceItem1.AllowHotTrack = False
            Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Location = New System.Drawing.Point(264, 0)
            Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Size = New System.Drawing.Size(10, 324)
            Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
            Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
            '
            'PopupMenu_Items
            '
            Me.PopupMenu_Items.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Add), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Change), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Delete)})
            Me.PopupMenu_Items.Manager = Me.BarManager1
            Me.PopupMenu_Items.MenuCaption = "Right Click Menus"
            Me.PopupMenu_Items.Name = "PopupMenu_Items"
            '
            'BarButtonItem_Add
            '
            Me.BarButtonItem_Add.Caption = "&Add"
            Me.BarButtonItem_Add.Description = "Add an item to the list"
            Me.BarButtonItem_Add.Id = 0
            Me.BarButtonItem_Add.Name = "BarButtonItem_Add"
            '
            'BarButtonItem_Change
            '
            Me.BarButtonItem_Change.Caption = "&Change"
            Me.BarButtonItem_Change.Description = "Change the list item"
            Me.BarButtonItem_Change.Id = 1
            Me.BarButtonItem_Change.Name = "BarButtonItem_Change"
            '
            'BarButtonItem_Delete
            '
            Me.BarButtonItem_Delete.Caption = "&Delete"
            Me.BarButtonItem_Delete.Description = "Delete the list item"
            Me.BarButtonItem_Delete.Id = 2
            Me.BarButtonItem_Delete.Name = "BarButtonItem_Delete"
            '
            'BarManager1
            '
            Me.BarManager1.DockControls.Add(Me.barDockControlTop)
            Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
            Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
            Me.BarManager1.DockControls.Add(Me.barDockControlRight)
            Me.BarManager1.Form = Me
            Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarButtonItem_Add, Me.BarButtonItem_Change, Me.BarButtonItem_Delete})
            Me.BarManager1.MaxItemId = 6
            '
            'barDockControlTop
            '
            Me.barDockControlTop.CausesValidation = False
            Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
            Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
            Me.barDockControlTop.Size = New System.Drawing.Size(576, 0)
            '
            'barDockControlBottom
            '
            Me.barDockControlBottom.CausesValidation = False
            Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
            Me.barDockControlBottom.Location = New System.Drawing.Point(0, 344)
            Me.barDockControlBottom.Size = New System.Drawing.Size(576, 0)
            '
            'barDockControlLeft
            '
            Me.barDockControlLeft.CausesValidation = False
            Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
            Me.barDockControlLeft.Location = New System.Drawing.Point(0, 0)
            Me.barDockControlLeft.Size = New System.Drawing.Size(0, 344)
            '
            'barDockControlRight
            '
            Me.barDockControlRight.CausesValidation = False
            Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
            Me.barDockControlRight.Location = New System.Drawing.Point(576, 0)
            Me.barDockControlRight.Size = New System.Drawing.Size(0, 344)
            '
            'Page_Aliases
            '
            Me.Controls.Add(Me.LayoutControl1)
            Me.Controls.Add(Me.barDockControlLeft)
            Me.Controls.Add(Me.barDockControlRight)
            Me.Controls.Add(Me.barDockControlBottom)
            Me.Controls.Add(Me.barDockControlTop)
            Me.Name = "Page_Aliases"
            Me.Size = New System.Drawing.Size(576, 344)
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.gridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.gridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.IndicatorsControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lk_Cause_Fin_Problem4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lk_Cause_Fin_Problem3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lk_Cause_Fin_Problem2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.lk_Cause_Fin_Problem1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_satisfaction_score.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.AliasesControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_ShowAgreement, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PopupMenu_Items, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Protected ControlRow As Int32
        Private bc As DebtPlus.LINQ.BusinessContext = Nothing

        ''' <summary>
        ''' Register the event handlers
        ''' </summary>
        Private Sub RegisterHandlers()

            ' Goals Grid related events
            AddHandler gridView1.MouseUp, AddressOf GridView1_MouseUp
            AddHandler PopupMenu_Items.Popup, AddressOf PopupMenu_Items_Popup
            AddHandler BarButtonItem_Add.ItemClick, AddressOf MenuItemCreate
            AddHandler BarButtonItem_Change.ItemClick, AddressOf MenuItemEdit
            AddHandler BarButtonItem_Delete.ItemClick, AddressOf MenuItemDelete
            AddHandler gridView1.MouseDown, AddressOf GridView1_MouseDown
            AddHandler gridView1.FocusedRowChanged, AddressOf GridView1_FocusedRowChanged
            AddHandler gridView1.DoubleClick, AddressOf GridView1_DoubleClick
        End Sub

        ''' <summary>
        ''' Deregister the event handlers
        ''' </summary>
        Private Sub UnRegisterHandlers()

            ' Goals Grid related events
            RemoveHandler gridView1.MouseUp, AddressOf GridView1_MouseUp
            RemoveHandler PopupMenu_Items.Popup, AddressOf PopupMenu_Items_Popup
            RemoveHandler BarButtonItem_Add.ItemClick, AddressOf MenuItemCreate
            RemoveHandler BarButtonItem_Change.ItemClick, AddressOf MenuItemEdit
            RemoveHandler BarButtonItem_Delete.ItemClick, AddressOf MenuItemDelete
            RemoveHandler gridView1.MouseDown, AddressOf GridView1_MouseDown
            RemoveHandler gridView1.FocusedRowChanged, AddressOf GridView1_FocusedRowChanged
            RemoveHandler gridView1.DoubleClick, AddressOf GridView1_DoubleClick
        End Sub

        ''' <summary>
        ''' Read the alias table
        ''' </summary>
        Public Overrides Sub ReadForm(bc As DebtPlus.LINQ.BusinessContext)
            Me.bc = bc
            MyBase.ReadForm(bc)
            UnRegisterHandlers()
            Try

                ' Load the satisfaction level indicators
                LookUpEdit_satisfaction_score.Properties.DataSource = DebtPlus.LINQ.InMemory.SatisfactionScoreTypes.getList()
                lk_Cause_Fin_Problem1.Properties.DataSource = DebtPlus.LINQ.Cache.financial_problemType.getList()
                lk_Cause_Fin_Problem2.Properties.DataSource = DebtPlus.LINQ.Cache.financial_problemType.getList()
                lk_Cause_Fin_Problem3.Properties.DataSource = DebtPlus.LINQ.Cache.financial_problemType.getList()
                lk_Cause_Fin_Problem4.Properties.DataSource = DebtPlus.LINQ.Cache.financial_problemType.getList()

                ' Set the agreement status
                LayoutControlItem_ShowAgreement.Visibility = If(Context.ClientDs.clientRecord.intake_agreement, LayoutVisibility.Always, LayoutVisibility.OnlyInCustomization)

                ' Load the controls for this page
                LookUpEdit_satisfaction_score.EditValue = Context.ClientDs.clientRecord.satisfaction_score
                lk_Cause_Fin_Problem1.EditValue = Context.ClientDs.clientRecord.cause_fin_problem1
                lk_Cause_Fin_Problem2.EditValue = Context.ClientDs.clientRecord.cause_fin_problem2
                lk_Cause_Fin_Problem3.EditValue = Context.ClientDs.clientRecord.cause_fin_problem3
                lk_Cause_Fin_Problem4.EditValue = Context.ClientDs.clientRecord.cause_fin_problem4

                AliasesControl1.ReadForm(bc)
                IndicatorsControl1.ReadForm(bc)
                RefreshGoals()

            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Apply the changes to the alias table update(s)
        ''' </summary>
        Public Overrides Sub SaveForm(bc As DebtPlus.LINQ.BusinessContext)

            AliasesControl1.SaveForm(bc)
            IndicatorsControl1.SaveForm(bc)

            ' Correct the values in the client table
            Context.ClientDs.clientRecord.satisfaction_score = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_satisfaction_score.EditValue).GetValueOrDefault()
            Context.ClientDs.clientRecord.cause_fin_problem1 = DebtPlus.Utils.Nulls.v_Int32(lk_Cause_Fin_Problem1.EditValue)
            Context.ClientDs.clientRecord.cause_fin_problem2 = DebtPlus.Utils.Nulls.v_Int32(lk_Cause_Fin_Problem2.EditValue)
            Context.ClientDs.clientRecord.cause_fin_problem3 = DebtPlus.Utils.Nulls.v_Int32(lk_Cause_Fin_Problem3.EditValue)
            Context.ClientDs.clientRecord.cause_fin_problem4 = DebtPlus.Utils.Nulls.v_Int32(lk_Cause_Fin_Problem4.EditValue)

            ' Save the base form now
            MyBase.SaveForm(bc)
        End Sub

        ''' <summary>
        ''' Handle the condition where the popup menu was activated
        ''' </summary>
        Private Sub GridView1_MouseUp(ByVal sender As Object, ByVal e As MouseEventArgs)
            If e.Button = MouseButtons.Right Then
                PopupMenu_Items.ShowPopup(MousePosition)
            End If
        End Sub

        ''' <summary>
        ''' Handle the condition where the popup menu was activated
        ''' </summary>
        Protected Overridable Sub PopupMenu_Items_Popup(ByVal sender As Object, ByVal e As EventArgs)
            BarButtonItem_Add.Enabled = OkToCreate()
            BarButtonItem_Delete.Enabled = (ControlRow >= 0) AndAlso OkToDelete(gridView1.GetRow(ControlRow))
            BarButtonItem_Change.Enabled = (ControlRow >= 0) AndAlso OkToEdit(gridView1.GetRow(ControlRow))
        End Sub

        ''' <summary>
        ''' Process a CREATE menu item choice
        ''' </summary>
        Protected Sub MenuItemCreate(ByVal sender As Object, ByVal e As EventArgs)
            OnCreateItem()
        End Sub

        ''' <summary>
        ''' Process an EDIT menu item choice
        ''' </summary>
        Protected Sub MenuItemEdit(ByVal sender As Object, ByVal e As EventArgs)
            If ControlRow >= 0 Then
                Dim obj As Object = gridView1.GetRow(ControlRow)
                If obj IsNot Nothing Then
                    OnEditItem(obj)
                End If
            End If
        End Sub

        ''' <summary>
        ''' Process a DELETE menu item
        ''' </summary>
        Protected Sub MenuItemDelete(ByVal sender As Object, ByVal e As EventArgs)
            If ControlRow >= 0 Then
                Dim obj As Object = gridView1.GetRow(ControlRow)
                If obj IsNot Nothing Then
                    OnDeleteItem(obj)
                End If
            End If
        End Sub

        ''' <summary>
        ''' Look for the mouse down on the grid control
        ''' </summary>
        Private Sub GridView1_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs)
            Dim gv As GridView = gridView1
            Dim hi As GridHitInfo = gv.CalcHitInfo(New Point(e.X, e.Y))

            ' Remember the position for the popup menu handler.
            If hi.InRow Then
                ControlRow = hi.RowHandle
            Else
                ControlRow = -1
            End If
        End Sub

        ''' <summary>
        ''' Is the item valid to be deleted?
        ''' </summary>
        Protected Overridable Function OkToDelete(ByVal obj As Object) As Boolean
            Return obj IsNot Nothing
        End Function

        ''' <summary>
        ''' Is the item valid to be edited?
        ''' </summary>
        Protected Overridable Function OkToEdit(ByVal obj As Object) As Boolean
            Return obj IsNot Nothing
        End Function

        ''' <summary>
        ''' Is the item valid to be created?
        ''' </summary>
        Protected Overridable Function OkToCreate() As Boolean
            Return True
        End Function

        ''' <summary>
        ''' The create operation was desired
        ''' </summary>
        ''' <remarks></remarks>
        Protected Overridable Sub OnCreateItem()

            Using frm As New LineOfServiceForm(Context.ClientDs.ClientId)
                If frm.ShowDialog() = DialogResult.OK Then
                    RefreshGoals()
                End If
            End Using
        End Sub

        ''' <summary>
        ''' Overridable function to delete the item in the list
        ''' </summary>
        Protected Overridable Sub OnDeleteItem(ByVal obj As Object)

            ' Find the grid row to delete
            Dim g As GoalSummary = TryCast(obj, GoalSummary)
            If g Is Nothing Then
                Return
            End If

            ' Ask if the delete is valid.
            If DebtPlus.Data.Forms.MessageBox.Show(String.Format("Are you sure you want to delete Goal {0}?", g.GoalID), "Delete Goal?", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) = System.Windows.Forms.DialogResult.OK Then
                Try
                    ' set the goal and goal items to inactive
                    Dim currentGoal As DebtPlus.LINQ.ClientGoal = bc.ClientGoals.Where(Function(cg) cg.Id = g.GoalID AndAlso cg.ActiveFlag = True).FirstOrDefault()
                    If currentGoal IsNot Nothing Then
                        currentGoal.ModifiedBy = DebtPlus.LINQ.BusinessContext.suser_sname()
                        currentGoal.ModifiedTimestamp = DebtPlus.LINQ.BusinessContext.getdate()
                        currentGoal.ActiveFlag = False
                    End If

                    Dim oldGoalItems As List(Of ClientGoalItem) = bc.ClientGoalItems.Where(Function(i) i.ClientGoalID = g.GoalID AndAlso i.ActiveFlag = True).ToList()
                    If oldGoalItems IsNot Nothing Then
                        oldGoalItems.ForEach(Sub(s) s.ActiveFlag = False)
                    End If

                    bc.SubmitChanges()
                    RefreshGoals()
                    Return

                Catch ex As System.Data.SqlClient.SqlException
                End Try
            End If

        End Sub

        ''' <summary>
        ''' Edit the indicated object in the list
        ''' </summary>
        ''' <param name="obj"></param>
        ''' <remarks></remarks>
        Protected Overridable Sub OnEditItem(ByVal obj As Object)
            Dim r As GoalSummary = TryCast(obj, GoalSummary)
            If r IsNot Nothing Then
                If Not EditItem(r) Then
                    Return
                End If
                Return
            End If

        End Sub

        ''' <summary>
        ''' Overridable function to edit the item in the list
        ''' </summary>
        Protected Overridable Function EditItem(ByVal r As GoalSummary) As Boolean

            Dim EditGoal As Integer = 2
            Dim answer As System.Windows.Forms.DialogResult = DialogResult.OK

            Select Case r.LineOfServiceType
                Case 1
                    Using frm As New HCFEGoalTrackerForm(r.GoalID, EditGoal)
                        answer = frm.ShowDialog()
                    End Using

                Case 2
                    Using frm As New SmartScholarsGoalTrackerForm(r.GoalID, EditGoal)
                        answer = frm.ShowDialog()
                    End Using

                Case 3
                    Using frm As New PostModPilotGoalTrackerForm(r.GoalID, EditGoal)
                        answer = frm.ShowDialog()
                    End Using
                Case Else

            End Select

            If answer = DialogResult.OK Then
                RefreshGoals()
            End If

        End Function

        Private Sub RefreshGoals()

            Dim goals As List(Of GoalSummary) = (From g In bc.ClientGoals _
                         Join los In bc.LineofServiceTypes On g.LineOfServiceID Equals los.Id
                         Where g.ClientID = Context.ClientDs.ClientId AndAlso g.ActiveFlag = True AndAlso los.ActiveFlag = True
                         Select New GoalSummary With {
                                .GoalID = g.Id,
                                .LineOfServiceType = g.LineOfServiceID,
                                .LineOfService = los.Description,
                                .Applicant = If(g.ClientGoalItems.Any(Function(ci) ci.UserType = 1 AndAlso ci.ActiveFlag = True), "Yes", "No"),
                                .Coapplicant = If(g.ClientGoalItems.Any(Function(ci) ci.UserType = 2 AndAlso ci.ActiveFlag = True) Or (g.CopyApplicantSetting.HasValue() AndAlso g.CopyApplicantSetting = True), "Yes", "No"),
                                .CreatedBy = GetUserName(g.CreatedBy),
                                .CreatedTimestamp = g.CreatedTimestamp,
                                .ModifiedBy = GetUserName(g.ModifiedBy),
                                .ModifiedTimestamp = g.ModifiedTimestamp}).ToList()

            gridControl1.DataSource = goals

            gridView1.RefreshData()
        End Sub

        Private Function GetUserName(user As String) As String
            Dim userName As String = String.Empty
            If Not String.IsNullOrWhiteSpace(user) Then
                Dim domainIndex As Integer = user.IndexOf("\")
                userName = If((domainIndex > -1), user.Substring(domainIndex + 1, user.Length - domainIndex - 1), String.Empty)
            End If
            Return userName
        End Function

        ''' <summary>
        ''' Process a change in the current row for the control
        ''' </summary>
        Private Sub GridView1_FocusedRowChanged(ByVal sender As Object, ByVal e As FocusedRowChangedEventArgs)
            OnSelectItem(gridView1.GetRow(e.FocusedRowHandle))
        End Sub

        ''' <summary>
        ''' Edit the indicated object in the list
        ''' </summary>
        Protected Overridable Sub OnSelectItem(ByVal obj As Object)
        End Sub

        ''' <summary>
        ''' Double Click on an item -- edit it
        ''' </summary>
        Private Sub GridView1_DoubleClick(ByVal sender As Object, ByVal e As EventArgs)

            ' Find the row targeted as the double-click item
            Dim hi As GridHitInfo = gridView1.CalcHitInfo((gridControl1.PointToClient(MousePosition)))
            Dim ControlRow As Int32 = If(hi.IsValid AndAlso hi.InRow, hi.RowHandle, -1)

            ' Find the item from the input tables.
            If ControlRow >= 0 Then
                Dim obj As Object = gridView1.GetRow(ControlRow)
                OnSelectItem(obj)
                OnEditItem(obj)
            End If
        End Sub

#If False Then
        ''' <summary>
        ''' Overridable function to create an item in the list
        ''' </summary>
        Protected Overridable Function CreateItem(ByVal drv As DataRowView) As Boolean
            Return False
        End Function

        Protected Overridable Function CreateItem(ByVal row As DataRow) As Boolean
            Return False
        End Function

        ''' <summary>
        ''' Over-ridable function to indicate that the row was selected
        ''' </summary>
        Protected Overridable Sub SelectItem(ByVal dvr As DataRowView)
        End Sub

        Protected Overridable Sub SelectItem(ByVal row As DataRow)
        End Sub
#End If
    End Class
End Namespace
