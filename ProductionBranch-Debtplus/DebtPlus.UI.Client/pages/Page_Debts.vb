#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Interfaces.Client
Imports DebtPlus.Interfaces.Debt
Imports DebtPlus.Svc.Debt
Imports DebtPlus.UI.Client.forms.Reassign
Imports DebtPlus.UI.Client.Service
Imports DebtPlus.Utils
Imports DevExpress.Data
Imports DevExpress.Data.Filtering.Exceptions
Imports DevExpress.Utils
Imports DevExpress.XtraBars
Imports DevExpress.XtraGrid
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports System.Threading
Imports System.Xml.Serialization

Namespace pages
    Friend Class Page_Debts


        Dim m_filters As RowFilterList

        ''' <summary>
        ''' Specific filters for the debt list
        ''' </summary>
        Protected Friend Property CurrentFilter() As String
            Get
                Return GridView1.ActiveFilterString
            End Get

            Set(ByVal value As String)
                If GridView1.GridControl.DataSource IsNot Nothing AndAlso CType(GridView1.GridControl.DataSource, DebtRecordList).Count > 0 Then
                    GridView1.BeginUpdate()
                    Try
                        GridView1.ActiveFilter.Clear()
                        GridView1.ActiveFilterString = value

                    Catch ex As InvalidPropertyPathException
                        DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

                    Finally
                        GridView1.EndUpdate()
                    End Try
                End If
            End Set
        End Property

        ''' <summary>
        ''' Create an instance of the class
        ''' </summary>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
            ConfigureGridView1()
            RegisterHandlers()
        End Sub

        Dim HandlersRegisterd As Boolean = False
        Private Sub RegisterHandlers()
            Debug.Assert(Not HandlersRegisterd)
            HandlersRegisterd = True
            AddHandler GridView1.Layout, AddressOf LayoutChanged
            AddHandler GridView1.MouseDown, AddressOf GridView1_MouseDown
            AddHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
            AddHandler BarButtonItem_Add.ItemClick, AddressOf BarButtonItem_Add_ItemClick
            AddHandler BarButtonItem_Change.ItemClick, AddressOf BarButtonItem_Change_ItemClick
            AddHandler BarButtonItem_Delete.ItemClick, AddressOf BarButtonItem_Delete_ItemClick
            AddHandler BarButtonItem_Reassign.ItemClick, AddressOf BarButtonItem_Reassign_ItemClick

            If Context IsNot Nothing AndAlso Context.DebtRecords IsNot Nothing Then
                AddHandler Context.DebtRecords.DataChanged, AddressOf DataChangedEventHandler
                AddHandler Context.DebtRecords.CollectionChanged, AddressOf RefreshGrid
            End If
        End Sub

        Private Sub UnRegisterHandlers()
            HandlersRegisterd = False
            RemoveHandler GridView1.Layout, AddressOf LayoutChanged
            RemoveHandler GridView1.MouseDown, AddressOf GridView1_MouseDown
            RemoveHandler GridView1.DoubleClick, AddressOf GridView1_DoubleClick
            RemoveHandler BarButtonItem_Add.ItemClick, AddressOf BarButtonItem_Add_ItemClick
            RemoveHandler BarButtonItem_Change.ItemClick, AddressOf BarButtonItem_Change_ItemClick
            RemoveHandler BarButtonItem_Delete.ItemClick, AddressOf BarButtonItem_Delete_ItemClick
            RemoveHandler BarButtonItem_Reassign.ItemClick, AddressOf BarButtonItem_Reassign_ItemClick

            If Context IsNot Nothing AndAlso Context.DebtRecords IsNot Nothing Then
                RemoveHandler Context.DebtRecords.DataChanged, AddressOf DataChangedEventHandler
                RemoveHandler Context.DebtRecords.CollectionChanged, AddressOf RefreshGrid
            End If
        End Sub

        ''' <summary>
        ''' Read or load the default filters
        ''' </summary>
        Public Sub LoadDefaultFilters()
            m_filters = ReadDebtFilters()
            If m_filters.Count = 0 Then

                m_filters.Add(New RowFilter("All Debts", "", False))
                m_filters.Add(New RowFilter("Debts with a balance", "([AlwaysShowInDebtList] <> False) OR ((([current_balance] > 0.0) Or ([ccl_zero_balance] <> False)) And ([IsActive] <> False))", True))
                m_filters.Add(New RowFilter("Non-Reassigned debts", "([AlwaysShowInDebtList] <> False) OR ([IsActive] <> False)", False))

                ' Save the filter list to the disk so that it is ready for the next run
                SaveDebtFilters(m_filters)
            End If
        End Sub

        ''' <summary>
        ''' Read the filter list from the disk
        ''' </summary>
        Public Shared Function ReadDebtFilters() As RowFilterList
            Dim answer As RowFilterList = Nothing
            Dim fn As String = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "DebtPlus\Client.Update\Form_ClientUpdate\DebtFilters.xml")

            If File.Exists(fn) Then
                Try
                    Using fs As New StreamReader(fn)
                        Dim ser As New XmlSerializer(GetType(RowFilterList), New XmlRootAttribute("RowFilters"))
                        answer = CType(ser.Deserialize(fs), RowFilterList)
                        If answer IsNot Nothing AndAlso Not answer.IsValidVersion Then
                            answer = Nothing
                        End If
                    End Using

                Catch ex As Exception
                    answer = Nothing
                End Try
            End If

            If answer Is Nothing Then
                answer = New RowFilterList()
            End If

            answer.HookHandlers()
            Return answer
        End Function

        ''' <summary>
        ''' Save the filter list to the disk
        ''' </summary>
        Private Shared Sub SaveDebtFiltersNow(ByVal obj As Object)
            Dim list As RowFilterList = CType(obj, RowFilterList)
            Dim fs As StreamWriter = Nothing

            Try
                Dim dn As String = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "DebtPlus\Client.Update\Form_ClientUpdate")
                If Not Directory.Exists(dn) Then Directory.CreateDirectory(dn)

                Dim fn As String = Path.Combine(dn, "DebtFilters.xml")
                fs = New StreamWriter(fn, False, Encoding.ASCII)
                Dim ser As New XmlSerializer(GetType(RowFilterList), New XmlRootAttribute("RowFilters"))
                ser.Serialize(fs, list)

            Catch ex As Exception
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex)

            Finally
                If fs IsNot Nothing Then
                    fs.Close()
                    fs.Dispose()
                End If
            End Try
        End Sub

        ''' <summary>
        ''' Save the filter list to the disk
        ''' </summary>
        Public Sub SaveDebtFilters(ByVal List As RowFilterList)
            Dim th As New Thread(New ParameterizedThreadStart(AddressOf SaveDebtFiltersNow))
            th.Name = "SaveFilters"
            th.IsBackground = True
            th.SetApartmentState(ApartmentState.STA)
            th.Start(List)
        End Sub

        ''' <summary>
        ''' Load the list of filter items into the view menu
        ''' </summary>
        Public Sub SetDebtMenuFilters(ByRef MenuSubItem As BarSubItem, ByRef Manager As BarManager)

            ' Populate the dropdown menu with the list of filters
            MenuSubItem.LinksPersistInfo.Clear()
            For Each rowItem As RowFilter In m_filters

                Dim menuItem As New BarCheckItem(Manager)
                AddHandler menuItem.ItemClick, AddressOf FilterMenuPick
                menuItem.Checked = rowItem.Default
                menuItem.Caption = rowItem.Name.Replace("&", "&&")
                menuItem.Id = Manager.Items.Count + 1
                menuItem.Name = String.Format("{0}_{1}", MenuSubItem.Name, Guid.NewGuid().ToString())
                menuItem.Tag = rowItem

                ' Add the item to the menus
                MenuSubItem.AddItem(menuItem)
            Next
        End Sub

        ''' <summary>
        ''' Set the checkbox status for the view menu items
        ''' </summary>
        Public Sub CheckViewSelections(ByVal MenuList As BarSubItem)

            ' Check the appropriate filter mode
            For Each menuLink As LinkPersistInfo In MenuList.LinksPersistInfo
                Dim menuItem As BarCheckItem = TryCast(menuLink.Item, BarCheckItem)
                If menuItem IsNot Nothing Then
                    Dim filterItem As RowFilter = CType(menuItem.Tag, RowFilter)
                    menuItem.Checked = filterItem.Default
                End If
            Next
        End Sub

        ''' <summary>
        ''' Handle the click event on a filter menu item
        ''' </summary>
        Private Sub FilterMenuPick(ByVal Sender As Object, ByVal e As ItemClickEventArgs)

            UnRegisterHandlers()
            Try
                ' Turn off all default values for each row filter
                Dim filterItem As RowFilter
                For Each filterItem In m_filters
                    filterItem.Default = False
                Next

                ' Find the item to be selected. Mark it as default and save the new list
                filterItem = CType(e.Item.Tag, RowFilter)
                filterItem.Default = True

                ' Go thorough the debt list and mark all items as "no longer new". This will cause the
                ' debts to be hidden according to the new filter rules and not be shown just because they
                ' were just created.
                For Each debt As ClientUpdateDebtRecord In Context.DebtRecords
                    debt.AlwaysShowInDebtList = False
                Next

                ' Set the filter and rewrite the filter list marking the default item
                CurrentFilter = filterItem.Filter
                SaveDebtFilters(m_filters)

            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Redraw the row when the data is changed
        ''' </summary>
        Private Delegate Sub RefreshGridDelegate(ByVal Sender As Object, ByVal e As EventArgs)

        Private Sub RefreshGrid(ByVal Sender As Object, ByVal e As EventArgs)

            If GridControl1.InvokeRequired Then
                Dim iA As IAsyncResult = GridControl1.BeginInvoke(New RefreshGridDelegate(AddressOf RefreshGrid), New Object() {Sender, e})
                GridControl1.EndInvoke(iA)
                ' Wait for the thread to complete so that we don't burn up all items in the thread-pool.

            Else

                ' Generate the update event for the entire grid
                GridControl1.RefreshDataSource()
                GridView1.RefreshData()
                GridView1.UpdateTotalSummary()

            End If
        End Sub

        ''' <summary>
        ''' Handle the condition where the popup menu was activated
        ''' </summary>
        Private Sub GridView1_MouseDown(ByVal sender As Object, ByVal e As MouseEventArgs)

            ' Handle the right mouse button being released
            If Not InDesignMode AndAlso e.Button = MouseButtons.Right Then

                ' Locate the current row for the context menu
                Dim gv As GridView = GridView1
                Dim hi As GridHitInfo = gv.CalcHitInfo(New Point(e.X, e.Y))

                ' Remember the position for the popup menu handler.
                If hi.IsValid AndAlso hi.InRow Then
                    Dim Record As ClientUpdateDebtRecord = CType(GridView1.GetRow(hi.RowHandle), ClientUpdateDebtRecord)

                    ' Add the items to the menus
                    With BarButtonItem_Delete
                        BarButtonItem_Delete.Tag = Record
                        BarButtonItem_Delete.Enabled = OkToDelete(Record)
                    End With

                    With BarButtonItem_Change
                        BarButtonItem_Change.Tag = Record
                        BarButtonItem_Change.Enabled = OkToEdit(Record)
                    End With

                    With BarButtonItem_Reassign
                        BarButtonItem_Reassign.Tag = Record
                        BarButtonItem_Reassign.Enabled = OkToReassign(Record)
                    End With

                Else ' The mouse is not a valid location. Disable the menu items.

                    BarButtonItem_Delete.Enabled = False
                    BarButtonItem_Change.Enabled = False
                    BarButtonItem_Reassign.Enabled = False
                End If

                ' The add event is normally always enabled.
                With BarButtonItem_Add
                    BarButtonItem_Add.Enabled = OkToCreate(CType(GridControl1.DataSource, ClientUpdateDebtRecordList))
                End With

                ' Check the view menu
                CheckViewSelections(BarSubItem_View)

                ' Do the pop-up menu processing with the form
                PopupMenu_Items.ShowPopup(MousePosition)
            End If
        End Sub

        ''' <summary>
        ''' Read the changes to the form
        ''' </summary>
        Public Overrides Sub ReadForm(bc As DebtPlus.LINQ.BusinessContext)
            Static LastSequence As Int64 = -1

            If Not InDesignMode AndAlso Context.DebtRecords IsNot Nothing Then
                If LastSequence <> Context.DebtRecords.DebtListCurrentStorage.PrivateID Then
                    RefreshDebtsList()
                    LastSequence = Context.DebtRecords.DebtListCurrentStorage.PrivateID
                End If
            End If

        End Sub

        ''' <summary>
        ''' Handle the DataChanged events from the list.
        ''' </summary>
        Private Sub DataChangedEventHandler(ByVal Sender As Object, ByVal e As DebtPlus.Events.DataChangedEventArgs)
            RaiseDebtDataChanged(e.Record, e.PropertyName)
        End Sub

        Private Delegate Sub RaiseDebtDataChangedDelegate(ByVal Sender As Object, ByVal PropertyName As String)

        ''' <summary>
        ''' Handle the individual data changed events. We need to raise the DataChanged to generate the update for the grid.
        ''' </summary>
        Private Sub RaiseDebtDataChanged(ByVal Sender As Object, ByVal PropertyName As String)
            If GridControl1.InvokeRequired Then
                Dim iA As IAsyncResult = GridControl1.BeginInvoke(New RaiseDebtDataChangedDelegate(AddressOf RaiseDebtDataChanged), New Object() {Sender, PropertyName})
                GridControl1.EndInvoke(iA)
            Else
                ' This will trap if we are not in the same thread as the gridcontrol!!
                CType(Sender, DebtPlus.Interfaces.Debt.INotifyDataChanged).RaisePropertyChanged(PropertyName)
                GridView1.UpdateSummary()
            End If
        End Sub

        ''' <summary>
        ''' Do the update of the debt list for the display
        ''' </summary>
        Private Delegate Sub RefreshDebtsListDelegate()

        Private Sub RefreshDebtsList()

            If GridControl1.InvokeRequired Then
                Dim ia As IAsyncResult = GridControl1.BeginInvoke(New RefreshDebtsListDelegate(AddressOf RefreshDebtsList))
                GridControl1.EndInvoke(ia)
            Else

                UnRegisterHandlers()
                Try
                    GridControl1.DataSource = Nothing

                    ' Re-add the items so that the current information is properly set
                    If Context.DebtRecords.Count > 0 Then
                        GridControl1.DataSource = Context.DebtRecords
                        Dim item As RowFilter = m_filters.DefaultFilter
                        If item IsNot Nothing Then CurrentFilter = item.Filter
                    End If

                Finally
                    RegisterHandlers()
                End Try
            End If
        End Sub

        ''' <summary>
        ''' Process a CREATE menu item choice
        ''' </summary>
        Protected Sub BarButtonItem_Add_ItemClick(ByVal sender As Object, ByVal e As EventArgs)

            If Not InDesignMode Then
                CreateItem()
            End If
        End Sub

        ''' <summary>
        ''' Process an EDIT menu item choice
        ''' </summary>
        Protected Sub BarButtonItem_Change_ItemClick(ByVal sender As Object, ByVal e As EventArgs)

            If Not InDesignMode Then
                Dim dataRow As ClientUpdateDebtRecord = CType(BarButtonItem_Change.Tag, ClientUpdateDebtRecord)
                If dataRow IsNot Nothing Then EditItem(dataRow)
            End If
        End Sub

        ''' <summary>
        ''' Process a DELETE menu item
        ''' </summary>
        Protected Sub BarButtonItem_Delete_ItemClick(ByVal sender As Object, ByVal e As EventArgs)

            If Not InDesignMode Then
                Dim dataRow As ClientUpdateDebtRecord = CType(BarButtonItem_Delete.Tag, ClientUpdateDebtRecord)
                If dataRow IsNot Nothing Then DeleteItem(dataRow)
            End If
        End Sub

        ''' <summary>
        ''' Process a REASSIGN menu item
        ''' </summary>
        Protected Sub BarButtonItem_Reassign_ItemClick(ByVal sender As Object, ByVal e As EventArgs)
            Dim dataRow As ClientUpdateDebtRecord = CType(BarButtonItem_Reassign.Tag, ClientUpdateDebtRecord)
                If dataRow IsNot Nothing Then
                    ReassignItem(dataRow)
                End If
        End Sub

        ''' <summary>
        ''' Double Click on an item -- edit it
        ''' </summary>
        Private Sub GridView1_DoubleClick(ByVal sender As Object, ByVal e As EventArgs)

            If Not InDesignMode Then
                Dim pt As Point = GridControl1.PointToClient(MousePosition)
                Dim hi As GridHitInfo = GridView1.CalcHitInfo(pt)
                If hi.IsValid AndAlso hi.InRow Then
                    Dim record As ClientUpdateDebtRecord = CType(GridView1.GetRow(hi.RowHandle), ClientUpdateDebtRecord)
                    If record IsNot Nothing Then
                        If OkToEdit(record) Then EditItem(record)
                    End If
                End If
            End If
        End Sub

        ''' <summary>
        ''' Is the item valid to be edited?
        ''' </summary>
        Protected Function OkToEdit(ByVal drv As ClientUpdateDebtRecord) As Boolean
            Return True
        End Function

        ''' <summary>
        ''' Is the item valid to be reassigned?
        ''' </summary>
        Protected Shared Function OkToReassign(ByVal record As ClientUpdateDebtRecord) As Boolean
            Dim answer As Boolean = False
            With record
                Do
                    ' Do not reassign if the debt is reassigned
                    If Not record.IsActive Then Exit Do

                    ' The debt must be coded with a creditor to be reassigned
                    If record.Creditor = String.Empty Then Exit Do

                    ' If the debt has been paid off then do not reassign the debt
                    If Not record.ccl_zero_balance AndAlso record.current_balance <= 0D Then Exit Do

                    ' Otherwise, it is valid to do the reassignment
                    answer = True
                    Exit Do
                Loop
            End With

            Return answer
        End Function

        ''' <summary>
        ''' Is the item valid to be deleted?
        ''' </summary>
        Protected Function OkToDelete(ByVal Record As ClientUpdateDebtRecord) As Boolean
            Return String.IsNullOrEmpty(Record.Creditor) OrElse (Record.first_payment <= 0)
        End Function

        ''' <summary>
        ''' Delete the item from the list
        ''' </summary>
        Protected Sub DeleteItem(ByVal record As ClientUpdateDebtRecord)
            If record IsNot Nothing Then
                Context.DebtRecords.RemoveDebt(record)
            End If
        End Sub

        ''' <summary>
        ''' Is the item valid to be created?
        ''' </summary>
        Protected Function OkToCreate(ByVal itemList As ClientUpdateDebtRecordList) As Boolean
            Return True
        End Function

        ''' <summary>
        ''' Create a new debt in the list
        ''' </summary>
        Protected Sub CreateItem()
            CreateProcedure()
        End Sub

        ''' <summary>
        ''' Edit the item in the list
        ''' </summary>
        Protected Sub EditItem(ByVal record As ClientUpdateDebtRecord)
            EditProcedure(record)
        End Sub

        ''' <summary>
        ''' Over-ridable function to reassign an item in the list
        ''' </summary>
        Protected Sub ReassignItem(ByVal record As ClientUpdateDebtRecord)

            ' Ask the user to give us a new creditor
            Dim answer As DialogResult
            Dim newCreditor As String
            Dim newAccountNumber As String
            Dim newProposal As Boolean

            Using frm As New Form_ReassignDebt(Context)
                frm.TextEdit_new_account_number.EditValue = record.account_number
                answer = frm.ShowDialog()
                If frm.DialogResult <> DialogResult.OK Then
                    Return
                End If

                newCreditor = frm.CreditorID_New.EditValue
                newAccountNumber = frm.TextEdit_new_account_number.Text.Trim()
                newProposal = frm.CheckEdit_NewProposals.Checked
            End Using

            If Not answer = DialogResult.OK Then
                Return
            End If

            ' Do the debt reassignment
            Try
                Using cn As New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    cn.Open()
                    Using txn As SqlTransaction = cn.BeginTransaction(IsolationLevel.RepeatableRead)

                        ' Save the current debt record information to the database so that the reassigned debts are updated
                        Context.DebtRecords.SaveData(txn)

                        ' Find the new debt record and mark the current one as reassigned.
                        Dim newDebtID As Int32 = record.ReassignDebt(newCreditor, If(newProposal, 1, 2), cn)
                        If newDebtID <= 0 Then
                            Try
                                txn.Rollback()
                            Catch
                            End Try

                            RefreshDebtsList()
                            Return
                        End If

                        ' Add a new record to hold the new debt
                        Dim newDebtRecord As New ClientUpdateDebtRecord()
                        Context.DebtRecords.Add(newDebtRecord)
                        newDebtRecord.client_creditor = newDebtID

                        ' Read the debt record from the database
                        newDebtRecord.ReloadFromDataStore(cn, txn)

                        ' If there is a new account number then update it
                        If newAccountNumber <> String.Empty Then
                            newDebtRecord.account_number = newAccountNumber
                        End If

                        ' Reload the current record from the database
                        record.ReloadFromDataStore(cn, txn)

                        ' Commit the changes to the database and destroy the object so that it does not rollback
                        txn.Commit()
                        RefreshDebtsList()
                        Return
                    End Using
                End Using

            Catch ex As SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error deleting budgets")
            End Try

            ' Refresh the debt list with the new values.
            RefreshDebtsList()
        End Sub

        ''' <summary>
        ''' Return the directory to the saved layout information
        ''' </summary>
        Protected Overridable Function XMLBasePath() As String
            If InDesignMode Then Return String.Empty
            Return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "DebtPlus", "client.Update", "Form_ClientUpdate")
        End Function

        ''' <summary>
        ''' If the layout is changed then update the file
        ''' </summary>
        Private Sub LayoutChanged(ByVal Sender As Object, ByVal e As EventArgs)
            Dim pathName As String = XMLBasePath()

            If pathName <> String.Empty Then
                If GridView1.Columns.Count > 0 Then
                    If Not Directory.Exists(pathName) Then
                        Directory.CreateDirectory(pathName)
                    End If

                    Dim fileName As String = Path.Combine(pathName, "Page_Debts1.Grid.xml")
                    GridView1.SaveLayoutToXml(fileName)
                End If
            End If
        End Sub

        ''' <summary>
        ''' Handle the creation of a debt
        ''' </summary>
        Public Sub CreateProcedure()
            Dim recordNumber As Int32 = -1

            ' Create the debt record for the user
            Using cls As IDebtCreate = CType(New DebtPlus.UI.Debt.Create.Form_CreateDebt(Context.ClientId), IDebtCreate)
                If cls.ShowDialog(Context.ClientId) = DialogResult.OK Then
                    recordNumber = cls.DebtId
                End If
            End Using

            ' If there is a record then process the record at this time
            If recordNumber > 0 Then

                ' Create the new record
                Dim record As ClientUpdateDebtRecord = New ClientUpdateDebtRecord(True)
                record.client_creditor = recordNumber

                ' Add it to the collection
                Context.DebtRecords.Add(record)

                ' Open a database connection to the storage
                Using cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                    cn.Open()
                    record.ReloadFromDataStore(cn)
                End Using

                ' Force the update of the tables when a new debt is added
                RefreshDebtsList()

                ' Fall through to update the record once it is created
                EditProcedure(record)
            End If
        End Sub

        ''' <summary>
        ''' Procedure to edit the debt record
        ''' </summary>
        Public Sub EditProcedure(ByVal record As ClientUpdateDebtRecord)
            Dim thrd As New System.Threading.Thread(New System.Threading.ParameterizedThreadStart(AddressOf EditThread))
            thrd.SetApartmentState(ApartmentState.STA)
            thrd.Name = "DebtUpdate"
            thrd.Start(record)
        End Sub

        ''' <summary>
        ''' Thread Procedure to edit the debt record
        ''' </summary>
        Public Sub EditThread(ByVal obj As Object)
            Dim record As ClientUpdateDebtRecord = TryCast(obj, ClientUpdateDebtRecord)
            If record Is Nothing Then
                Throw New ArgumentNullException("EditThread record is null")
            Else

                ' Display the debt alert notes
                If record.client_creditor > 0 Then
                    ShowAlertNotes(record)
                End If

                ' Edit the debt record
                Using frm As New DebtPlus.UI.Debt.Update.forms.Form_DebtUpdate(record)
                    frm.ShowDialog()
                End Using
            End If
        End Sub

        ''' <summary>
        ''' Display the debt alert notes
        ''' </summary>
        Private Sub ShowAlertNotes(ByVal record As ClientUpdateDebtRecord)
            Using alertNoteClass As IAlertNotes = New DebtPlus.Notes.AlertNotes.Client()
                alertNoteClass.ShowAlerts(Context.ClientId, record.client_creditor)
            End Using
        End Sub

#Region "Form Grid Definition"

        Dim [col_cr_creditor_name] As New GridColumn()
        Dim [col_creditor] As New GridColumn()
        Dim col_contact_name As New GridColumn()
        Dim [col_account_number] As New GridColumn()
        Dim col_created_by As New GridColumn()
        Dim [col_creditor_name] As New GridColumn()
        Dim col_last_communication As New GridColumn()
        Dim col_client_name As New GridColumn()
        Dim col_display_verified_status As New GridColumn()
        Dim col_last_payment_type As New GridColumn()
        Dim col_display_debt_id As New GridColumn()
        Dim col_first_payment_type As New GridColumn()
        Dim col_balance_verify_by As New GridColumn()
        Dim col_message As New GridColumn()
        Dim col_display_account_number As New GridColumn()
        Dim col_display_creditor_name As New GridColumn()
        Dim [col_adjusted_orig_balance] As New GridColumn()
        Dim [col_current_balance] As New GridColumn()
        Dim col_display_current_balance As New GridColumn()
        Dim col_last_stmt_balance As New GridColumn()
        Dim col_non_dmp_payment As New GridColumn()
        Dim col_orig_dmp_payment As New GridColumn()
        Dim [col_disbursement_factor] As New GridColumn()
        Dim col_first_payment_amt As New GridColumn()
        Dim col_last_payment_amt As New GridColumn()
        Dim col_payments_month_1 As New GridColumn()
        Dim col_payments_month_0 As New GridColumn()
        Dim col_display_total_interest As New GridColumn()
        Dim [col_total_interest] As New GridColumn()
        Dim col_orig_balance_adjustment As New GridColumn()
        Dim col_display_payments_this_creditor As New GridColumn()
        Dim col_payments_this_creditor As New GridColumn()
        Dim col_display_total_payments As New GridColumn()
        Dim [col_total_payments] As New GridColumn()
        Dim col_interest_this_creditor As New GridColumn()
        Dim col_sched_payment As New GridColumn()
        Dim col_current_sched_payment As New GridColumn()
        Dim col_total_sched_payment As New GridColumn()
        Dim col_display_orig_balance As New GridColumn()
        Dim [col_orig_balance] As New GridColumn()
        Dim col_display_disbursement_factor As New GridColumn()
        Dim col_returns_this_creditor As New GridColumn()
        Dim col_check_payments As New GridColumn()
        Dim col_client_creditor As New GridColumn()
        Dim col_client_creditor_balance As New GridColumn()
        Dim col_client_creditor_proposal As New GridColumn()
        Dim col_first_payment As New GridColumn()
        Dim col_last_payment As New GridColumn()
        Dim col_person As New GridColumn()
        Dim col_balance_client_creditor As New GridColumn()
        Dim col_payment_rpps_mask As New GridColumn()
        Dim col_Drop_Reason As New GridColumn()
        Dim col_percent_balance As New GridColumn()
        Dim col_priority As New GridColumn()
        Dim col_rpps_mask As New GridColumn()
        Dim col_terms As New GridColumn()
        Dim col_months_delinquent As New GridColumn()
        Dim col_line_number As New GridColumn()
        Dim col_display_dmp_interest As New GridColumn()
        Dim [col_dmp_interest] As New GridColumn()
        Dim col_dmp_payout_interest As New GridColumn()
        Dim col_fairshare_pct_check As New GridColumn()
        Dim col_non_dmp_interest As New GridColumn()
        Dim col_fairshare_pct_eft As New GridColumn()
        Dim col_prenote_date As New GridColumn()
        Dim col_drop_date As New GridColumn()
        Dim col_Drop_Reason_sent As New GridColumn()
        Dim col_date_created As New GridColumn()
        Dim col_Drop_Reason_date As New GridColumn()
        Dim col_date_disp_changed As New GridColumn()
        Dim col_last_payment_date_b4_dmp As New GridColumn()
        Dim col_expected_payout_date As New GridColumn()
        Dim col_start_date As New GridColumn()
        Dim col_last_stmt_date As New GridColumn()
        Dim col_verify_request_date As New GridColumn()
        Dim col_balance_verify_date As New GridColumn()
        Dim col_first_payment_date As New GridColumn()
        Dim col_last_payment_date As New GridColumn()
        Dim col_hold_disbursements As New GridColumn()
        Dim col_irs_form_on_file As New GridColumn()
        Dim col_send_bal_verify As New GridColumn()
        Dim col_send_drop_notice As New GridColumn()
        Dim col_student_loan_release As New GridColumn()
        Dim col_ccl_zero_balance As New GridColumn()
        Dim col_ccl_prorate As New GridColumn()
        Dim col_ccl_always_disburse As New GridColumn()
        Dim col_ccl_agency_account As New GridColumn()
        Dim col_proposal_balance As New GridColumn()
        Dim col_ActiveFlag As New GridColumn()

        Private Sub ConfigureGridView1()
            '
            'col_ActiveFlag
            '
            col_ActiveFlag.AppearanceCell.Options.UseTextOptions = True
            col_ActiveFlag.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center
            col_ActiveFlag.AppearanceHeader.Options.UseTextOptions = True
            col_ActiveFlag.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
            col_ActiveFlag.Caption = "Active?"
            col_ActiveFlag.CustomizationCaption = "Active Account?"
            col_ActiveFlag.DisplayFormat.FormatString = "{0:f0}"
            col_ActiveFlag.DisplayFormat.FormatType = FormatType.Numeric
            col_ActiveFlag.FieldName = "IsActive"
            col_ActiveFlag.GroupFormat.FormatString = "{0:f0}"
            col_ActiveFlag.GroupFormat.FormatType = FormatType.Numeric
            col_ActiveFlag.Name = "col_ActiveFlag"
            col_ActiveFlag.OptionsColumn.AllowEdit = False
            col_ActiveFlag.ToolTip = "Yes if this is closed or reassigned debt"
            col_ActiveFlag.OptionsColumn.AllowGroup = DefaultBoolean.True
            col_ActiveFlag.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_contact_name
            '
            col_contact_name.Caption = "Contact Name"
            col_contact_name.CustomizationCaption = "Contact Name"
            col_contact_name.FieldName = "contact_name"
            col_contact_name.Name = "col_contact_name"
            col_contact_name.ToolTip = "contact name for debt at creditor"
            col_contact_name.OptionsColumn.AllowGroup = DefaultBoolean.True
            col_contact_name.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_created_by
            '
            col_contact_name.Caption = "Creator"
            col_contact_name.CustomizationCaption = "Person who created the debt"
            col_contact_name.FieldName = "created_by"
            col_contact_name.GroupInterval = ColumnGroupInterval.[Date]
            col_contact_name.Name = "col_created_by"
            col_contact_name.SortMode = ColumnSortMode.Value
            col_contact_name.ToolTip = "Person who created the debt"
            col_contact_name.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_last_communication
            '
            col_contact_name.Caption = "Last Comm"
            col_contact_name.CustomizationCaption = "ID of Last Communication"
            col_contact_name.FieldName = "last_communication"
            col_contact_name.Name = "col_last_communication"
            col_contact_name.ToolTip = "ID of the last RPPS transaction for this debt."
            col_contact_name.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_client_name
            '
            col_client_name.Caption = "Client Name"
            col_client_name.CustomizationCaption = "Client Name"
            col_client_name.FieldName = "client_name"
            col_client_name.Name = "col_client_name"
            col_client_name.ToolTip = "Name of the signer (owner) for this debt."
            col_client_name.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_display_verified_status
            '
            col_display_verified_status.AppearanceCell.Options.UseTextOptions = True
            col_display_verified_status.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center
            col_display_verified_status.AppearanceHeader.Options.UseTextOptions = True
            col_display_verified_status.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
            col_display_verified_status.Caption = "V"
            col_display_verified_status.CustomizationCaption = "Verified Debt Status"
            col_display_verified_status.FieldName = "display_verified_status"
            col_display_verified_status.GroupInterval = ColumnGroupInterval.DisplayText
            col_display_verified_status.MinWidth = 10
            col_display_verified_status.Name = "col_display_verified_status"
            col_display_verified_status.SortMode = ColumnSortMode.DisplayText
            col_display_verified_status.ToolTip = """V"" for verified balances. Blank otherwise."
            col_display_verified_status.Visible = True
            col_display_verified_status.VisibleIndex = 10
            col_display_verified_status.Width = 27
            col_display_verified_status.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_last_payment_type
            '
            col_last_payment_type.Caption = "Last Pmt Type"
            col_last_payment_type.CustomizationCaption = "Last Payment Type"
            col_last_payment_type.FieldName = "last_payment_type"
            col_last_payment_type.Name = "col_last_payment_type"
            col_last_payment_type.ToolTip = "Last payment type. BW = EFT, all others are paper check."
            col_last_payment_type.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_display_debt_id
            '
            col_display_debt_id.Caption = "Creditor"
            col_display_debt_id.CustomizationCaption = "Creditor Debt ID"
            col_display_debt_id.FieldName = "display_debt_id"
            col_display_debt_id.Name = "col_display_debt_id"
            col_display_debt_id.ToolTip = "Creditor, debt number, etc. for the debt."
            col_display_debt_id.Visible = True
            col_display_debt_id.VisibleIndex = 1
            col_display_debt_id.Width = 55
            col_display_debt_id.OptionsColumn.AllowGroup = DefaultBoolean.True
            col_display_debt_id.OptionsColumn.AllowSort = DefaultBoolean.True
            col_display_debt_id.SummaryItem.DisplayFormat = "{0:n0} item(s)"
            col_display_debt_id.SummaryItem.SummaryType = SummaryItemType.Count
            '
            'col_first_payment_type
            '
            col_first_payment_type.Caption = "First Pmt Type"
            col_first_payment_type.CustomizationCaption = "First Payment Type"
            col_first_payment_type.FieldName = "first_payment_type"
            col_first_payment_type.Name = "col_first_payment_type"
            col_first_payment_type.ToolTip = "Type of the first payment. BW = EFT. All others are check."
            col_first_payment_type.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_balance_verify_by
            '
            col_balance_verify_by.Caption = "Verified By"
            col_balance_verify_by.CustomizationCaption = "Verified By"
            col_balance_verify_by.FieldName = "balance_verify_by"
            col_balance_verify_by.Name = "col_balance_verify_by"
            col_balance_verify_by.ToolTip = "Name of the contact at the creditor who verified the balance."
            col_balance_verify_by.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_message
            '
            col_message.Caption = "Message"
            col_message.CustomizationCaption = "Message"
            col_message.FieldName = "message"
            col_message.Name = "col_message"
            col_message.ToolTip = "General message field for the debt. Maybe shown as account number."
            col_message.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_display_account_number
            '
            col_display_account_number.Caption = "Acct#"
            col_display_account_number.CustomizationCaption = "Account Number"
            col_display_account_number.FieldName = "display_account_number"
            col_display_account_number.Name = "col_display_account_number"
            col_display_account_number.ToolTip = "Either the message or the account number."
            col_display_account_number.Visible = True
            col_display_account_number.VisibleIndex = 3
            col_display_account_number.Width = 102
            'col_display_account_number.SortIndex = 2
            'col_display_account_number.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
            col_display_account_number.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_display_creditor_name
            '
            col_display_creditor_name.Caption = "Name"
            col_display_creditor_name.CustomizationCaption = "Creditor Name"
            col_display_creditor_name.FieldName = "display_creditor_and_division"
            col_display_creditor_name.Name = "col_display_creditor_name"
            col_display_creditor_name.ToolTip = "Either the coded or uncoded creditor name."
            col_display_creditor_name.Visible = True
            col_display_creditor_name.VisibleIndex = 2
            col_display_creditor_name.Width = 91
            'col_display_creditor_name.SortIndex = 1
            'col_display_creditor_name.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
            col_display_creditor_name.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_display_current_balance
            '
            col_display_current_balance.AppearanceCell.Options.UseTextOptions = True
            col_display_current_balance.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_display_current_balance.AppearanceHeader.Options.UseTextOptions = True
            col_display_current_balance.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_display_current_balance.Caption = "Balance"
            col_display_current_balance.CustomizationCaption = "Current Balance"
            col_display_current_balance.DisplayFormat.FormatType = FormatType.Numeric
            col_display_current_balance.FieldName = "display_current_balance"
            col_display_current_balance.GroupFormat.FormatType = FormatType.Numeric
            col_display_current_balance.GroupInterval = ColumnGroupInterval.Value
            col_display_current_balance.Name = "col_display_current_balance"
            col_display_current_balance.SummaryItem.SummaryType = SummaryItemType.Sum
            col_display_current_balance.ToolTip = "IF reassigned debt, then zero. Otherwise the balance."
            col_display_current_balance.Visible = True
            col_display_current_balance.VisibleIndex = 9
            col_display_current_balance.Width = 80
            col_display_current_balance.DisplayFormat.FormatString = "{0:c}"
            col_display_current_balance.GroupFormat.FormatString = "{0:c}"
            col_display_current_balance.SummaryItem.DisplayFormat = "{0:c}"
            col_display_current_balance.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_last_stmt_balance
            '
            col_last_stmt_balance.AppearanceCell.Options.UseTextOptions = True
            col_last_stmt_balance.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_last_stmt_balance.AppearanceHeader.Options.UseTextOptions = True
            col_last_stmt_balance.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_last_stmt_balance.Caption = "Stmt Bal"
            col_last_stmt_balance.CustomizationCaption = "Balance when last statement run"
            col_last_stmt_balance.DisplayFormat.FormatType = FormatType.Numeric
            col_last_stmt_balance.FieldName = "last_stmt_balance"
            col_last_stmt_balance.GroupFormat.FormatType = FormatType.Numeric
            col_last_stmt_balance.GroupInterval = ColumnGroupInterval.Value
            col_last_stmt_balance.Name = "col_last_stmt_balance"
            col_last_stmt_balance.SummaryItem.SummaryType = SummaryItemType.Sum
            col_last_stmt_balance.ToolTip = "Debt balance at the last statement generation."
            col_last_stmt_balance.DisplayFormat.FormatString = "{0:c}"
            col_last_stmt_balance.GroupFormat.FormatString = "{0:c}"
            col_last_stmt_balance.SummaryItem.DisplayFormat = "{0:c}"
            col_last_stmt_balance.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_non_dmp_payment
            '
            col_non_dmp_payment.AppearanceCell.Options.UseTextOptions = True
            col_non_dmp_payment.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_non_dmp_payment.AppearanceHeader.Options.UseTextOptions = True
            col_non_dmp_payment.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_non_dmp_payment.Caption = "NonDMP $"
            col_non_dmp_payment.CustomizationCaption = "Non-DMP Payment Amount"
            col_non_dmp_payment.DisplayFormat.FormatType = FormatType.Numeric
            col_non_dmp_payment.FieldName = "non_dmp_payment"
            col_non_dmp_payment.GroupFormat.FormatType = FormatType.Numeric
            col_non_dmp_payment.GroupInterval = ColumnGroupInterval.Value
            col_non_dmp_payment.Name = "col_non_dmp_payment"
            col_non_dmp_payment.SummaryItem.SummaryType = SummaryItemType.Sum
            col_non_dmp_payment.ToolTip = "Non-DMP Payment amount."
            col_non_dmp_payment.DisplayFormat.FormatString = "{0:c}"
            col_non_dmp_payment.GroupFormat.FormatString = "{0:c}"
            col_non_dmp_payment.SummaryItem.DisplayFormat = "{0:c}"
            col_non_dmp_payment.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_orig_dmp_payment
            '
            col_orig_dmp_payment.AppearanceCell.Options.UseTextOptions = True
            col_orig_dmp_payment.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_orig_dmp_payment.AppearanceHeader.Options.UseTextOptions = True
            col_orig_dmp_payment.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_orig_dmp_payment.Caption = "Orig Pmt"
            col_orig_dmp_payment.CustomizationCaption = "Original Proposed Payment Amount"
            col_orig_dmp_payment.DisplayFormat.FormatType = FormatType.Numeric
            col_orig_dmp_payment.FieldName = "orig_dmp_payment"
            col_orig_dmp_payment.GroupFormat.FormatType = FormatType.Numeric
            col_orig_dmp_payment.GroupInterval = ColumnGroupInterval.Value
            col_orig_dmp_payment.Name = "col_orig_dmp_payment"
            col_orig_dmp_payment.SummaryItem.SummaryType = SummaryItemType.Sum
            col_orig_dmp_payment.ToolTip = "Original DMP amount accepted by the proposal."
            col_orig_dmp_payment.Visible = True
            col_orig_dmp_payment.VisibleIndex = 5
            col_orig_dmp_payment.Width = 71
            col_orig_dmp_payment.DisplayFormat.FormatString = "{0:c}"
            col_orig_dmp_payment.GroupFormat.FormatString = "{0:c}"
            col_orig_dmp_payment.SummaryItem.DisplayFormat = "{0:c}"
            col_orig_dmp_payment.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_first_payment_amt
            '
            col_first_payment_amt.AppearanceCell.Options.UseTextOptions = True
            col_first_payment_amt.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_first_payment_amt.AppearanceHeader.Options.UseTextOptions = True
            col_first_payment_amt.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_first_payment_amt.Caption = "First Pmt Amt"
            col_first_payment_amt.CustomizationCaption = "First Payment Amount"
            col_first_payment_amt.DisplayFormat.FormatType = FormatType.Numeric
            col_first_payment_amt.FieldName = "first_payment_amt"
            col_first_payment_amt.GroupFormat.FormatType = FormatType.Numeric
            col_first_payment_amt.GroupInterval = ColumnGroupInterval.Value
            col_first_payment_amt.Name = "col_first_payment_amt"
            col_first_payment_amt.SummaryItem.SummaryType = SummaryItemType.Sum
            col_first_payment_amt.ToolTip = "First payment amount on this debt"
            col_first_payment_amt.DisplayFormat.FormatString = "{0:c}"
            col_first_payment_amt.GroupFormat.FormatString = "{0:c}"
            col_first_payment_amt.SummaryItem.DisplayFormat = "{0:c}"
            col_first_payment_amt.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_last_payment_amt
            '
            col_last_payment_amt.AppearanceCell.Options.UseTextOptions = True
            col_last_payment_amt.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_last_payment_amt.AppearanceHeader.Options.UseTextOptions = True
            col_last_payment_amt.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_last_payment_amt.Caption = "Last Pmt Amt"
            col_last_payment_amt.CustomizationCaption = "Last Payment Amount"
            col_last_payment_amt.DisplayFormat.FormatType = FormatType.Numeric
            col_last_payment_amt.FieldName = "last_payment_amt"
            col_last_payment_amt.GroupFormat.FormatType = FormatType.Numeric
            col_last_payment_amt.GroupInterval = ColumnGroupInterval.Value
            col_last_payment_amt.Name = "col_last_payment_amt"
            col_last_payment_amt.SummaryItem.SummaryType = SummaryItemType.Sum
            col_last_payment_amt.ToolTip = "Last payment amount on this debt"
            col_last_payment_amt.Visible = True
            col_last_payment_amt.VisibleIndex = 14
            col_last_payment_amt.Width = 188
            col_last_payment_amt.DisplayFormat.FormatString = "{0:c}"
            col_last_payment_amt.GroupFormat.FormatString = "{0:c}"
            col_last_payment_amt.SummaryItem.DisplayFormat = "{0:c}"
            col_last_payment_amt.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_payments_month_1
            '
            col_payments_month_1.AppearanceCell.Options.UseTextOptions = True
            col_payments_month_1.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_payments_month_1.AppearanceHeader.Options.UseTextOptions = True
            col_payments_month_1.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_payments_month_1.Caption = "Last Mo Disb"
            col_payments_month_1.CustomizationCaption = "Last Month's disbursement amount"
            col_payments_month_1.DisplayFormat.FormatType = FormatType.Numeric
            col_payments_month_1.FieldName = "payments_month_1"
            col_payments_month_1.GroupFormat.FormatType = FormatType.Numeric
            col_payments_month_1.GroupInterval = ColumnGroupInterval.Value
            col_payments_month_1.Name = "col_payments_month_1"
            col_payments_month_1.SummaryItem.SummaryType = SummaryItemType.Sum
            col_payments_month_1.ToolTip = "Net payment last month."
            col_payments_month_1.Visible = True
            col_payments_month_1.VisibleIndex = 8
            col_payments_month_1.Width = 74
            col_payments_month_1.DisplayFormat.FormatString = "{0:c}"
            col_payments_month_1.GroupFormat.FormatString = "{0:c}"
            col_payments_month_1.SummaryItem.DisplayFormat = "{0:c}"
            col_payments_month_1.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_payments_month_0
            '
            col_payments_month_0.AppearanceCell.Options.UseTextOptions = True
            col_payments_month_0.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_payments_month_0.AppearanceHeader.Options.UseTextOptions = True
            col_payments_month_0.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_payments_month_0.Caption = "Curr Mo Disb"
            col_payments_month_0.CustomizationCaption = "Current Month's disbursement amount"
            col_payments_month_0.DisplayFormat.FormatType = FormatType.Numeric
            col_payments_month_0.FieldName = "payments_month_0"
            col_payments_month_0.GroupFormat.FormatType = FormatType.Numeric
            col_payments_month_0.GroupInterval = ColumnGroupInterval.Value
            col_payments_month_0.Name = "col_payments_month_0"
            col_payments_month_0.SummaryItem.SummaryType = SummaryItemType.Sum
            col_payments_month_0.ToolTip = "Net payment this month (so far)."
            col_payments_month_0.Visible = True
            col_payments_month_0.VisibleIndex = 7
            col_payments_month_0.Width = 74
            col_payments_month_0.DisplayFormat.FormatString = "{0:c}"
            col_payments_month_0.GroupFormat.FormatString = "{0:c}"
            col_payments_month_0.SummaryItem.DisplayFormat = "{0:c}"
            col_payments_month_0.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_display_total_interest
            '
            col_display_total_interest.AppearanceCell.Options.UseTextOptions = True
            col_display_total_interest.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_display_total_interest.AppearanceHeader.Options.UseTextOptions = True
            col_display_total_interest.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_display_total_interest.Caption = "Total Interest"
            col_display_total_interest.CustomizationCaption = "Total Interest Amt"
            col_display_total_interest.DisplayFormat.FormatType = FormatType.Numeric
            col_display_total_interest.FieldName = "display_total_interest"
            col_display_total_interest.GroupFormat.FormatType = FormatType.Numeric
            col_display_total_interest.GroupInterval = ColumnGroupInterval.Value
            col_display_total_interest.Name = "col_display_total_interest"
            col_display_total_interest.SummaryItem.SummaryType = SummaryItemType.Sum
            col_display_total_interest.ToolTip = "If reassigned debt then $0.00. Otherwise, the total interest charged on this debt."
            col_display_total_interest.DisplayFormat.FormatString = "{0:c}"
            col_display_total_interest.GroupFormat.FormatString = "{0:c}"
            col_display_total_interest.SummaryItem.DisplayFormat = "{0:c}"
            col_display_total_interest.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_orig_balance_adjustment
            '
            col_orig_balance_adjustment.AppearanceCell.Options.UseTextOptions = True
            col_orig_balance_adjustment.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_orig_balance_adjustment.AppearanceHeader.Options.UseTextOptions = True
            col_orig_balance_adjustment.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_orig_balance_adjustment.Caption = "Orig Bal Adj"
            col_orig_balance_adjustment.CustomizationCaption = "Original Balance Adjustment"
            col_orig_balance_adjustment.DisplayFormat.FormatType = FormatType.Numeric
            col_orig_balance_adjustment.FieldName = "orig_balance_adjustment"
            col_orig_balance_adjustment.GroupFormat.FormatType = FormatType.Numeric
            col_orig_balance_adjustment.GroupInterval = ColumnGroupInterval.Value
            col_orig_balance_adjustment.Name = "col_orig_balance_adjustment"
            col_orig_balance_adjustment.SummaryItem.SummaryType = SummaryItemType.Sum
            col_orig_balance_adjustment.ToolTip = "Adjustment to the original balance from the creditor."
            col_orig_balance_adjustment.DisplayFormat.FormatString = "{0:c}"
            col_orig_balance_adjustment.GroupFormat.FormatString = "{0:c}"
            col_orig_balance_adjustment.SummaryItem.DisplayFormat = "{0:c}"
            col_orig_balance_adjustment.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_display_payments_this_creditor
            '
            col_display_payments_this_creditor.AppearanceCell.Options.UseTextOptions = True
            col_display_payments_this_creditor.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_display_payments_this_creditor.AppearanceHeader.Options.UseTextOptions = True
            col_display_payments_this_creditor.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_display_payments_this_creditor.Caption = "Paid To Date"
            col_display_payments_this_creditor.CustomizationCaption = "Total amount Paid To Date for this creditor"
            col_display_payments_this_creditor.DisplayFormat.FormatType = FormatType.Numeric
            col_display_payments_this_creditor.FieldName = "display_payments_this_creditor"
            col_display_payments_this_creditor.GroupFormat.FormatType = FormatType.Numeric
            col_display_payments_this_creditor.GroupInterval = ColumnGroupInterval.Value
            col_display_payments_this_creditor.Name = "col_display_payments_this_creditor"
            col_display_payments_this_creditor.SummaryItem.SummaryType = SummaryItemType.Sum
            col_display_payments_this_creditor.ToolTip = "Net payments to this creditor. Equals payments - returns."
            col_display_payments_this_creditor.Visible = True
            col_display_payments_this_creditor.VisibleIndex = 11
            col_display_payments_this_creditor.Width = 85
            col_display_payments_this_creditor.DisplayFormat.FormatString = "{0:c}"
            col_display_payments_this_creditor.GroupFormat.FormatString = "{0:c}"
            col_display_payments_this_creditor.SummaryItem.DisplayFormat = "{0:c}"
            col_display_payments_this_creditor.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_payments_this_creditor
            '
            col_payments_this_creditor.AppearanceCell.Options.UseTextOptions = True
            col_payments_this_creditor.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_payments_this_creditor.AppearanceHeader.Options.UseTextOptions = True
            col_payments_this_creditor.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_payments_this_creditor.Caption = "Payments"
            col_payments_this_creditor.CustomizationCaption = "Payments made through this payee"
            col_payments_this_creditor.DisplayFormat.FormatType = FormatType.Numeric
            col_payments_this_creditor.FieldName = "payments_this_creditor"
            col_payments_this_creditor.GroupFormat.FormatType = FormatType.Numeric
            col_payments_this_creditor.GroupInterval = ColumnGroupInterval.Value
            col_payments_this_creditor.Name = "col_payments_this_creditor"
            col_payments_this_creditor.SummaryItem.SummaryType = SummaryItemType.Sum
            col_payments_this_creditor.ToolTip = "Total ""gross"" payments to this creditor. Does not count refunds/voids."
            col_payments_this_creditor.DisplayFormat.FormatString = "{0:c}"
            col_payments_this_creditor.GroupFormat.FormatString = "{0:c}"
            col_payments_this_creditor.SummaryItem.DisplayFormat = "{0:c}"
            col_payments_this_creditor.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_display_total_payments
            '
            col_display_total_payments.AppearanceCell.Options.UseTextOptions = True
            col_display_total_payments.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_display_total_payments.AppearanceHeader.Options.UseTextOptions = True
            col_display_total_payments.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_display_total_payments.Caption = "Paid To Date"
            col_display_total_payments.CustomizationCaption = "Total Paid To Date Amount"
            col_display_total_payments.DisplayFormat.FormatType = FormatType.Numeric
            col_display_total_payments.FieldName = "display_total_payments"
            col_display_total_payments.GroupFormat.FormatType = FormatType.Numeric
            col_display_total_payments.GroupInterval = ColumnGroupInterval.Value
            col_display_total_payments.Name = "col_display_total_payments"
            col_display_total_payments.SummaryItem.SummaryType = SummaryItemType.Sum
            col_display_total_payments.ToolTip = "If reassigned debt, value is $0.00. Otherwise, total net payments."
            col_display_total_payments.Width = 85
            col_display_total_payments.DisplayFormat.FormatString = "{0:c}"
            col_display_total_payments.GroupFormat.FormatString = "{0:c}"
            col_display_total_payments.SummaryItem.DisplayFormat = "{0:c}"
            col_display_total_payments.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_interest_this_creditor
            '
            col_interest_this_creditor.AppearanceCell.Options.UseTextOptions = True
            col_interest_this_creditor.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_interest_this_creditor.AppearanceHeader.Options.UseTextOptions = True
            col_interest_this_creditor.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_interest_this_creditor.Caption = "Interest Amt for this Payee"
            col_interest_this_creditor.CustomizationCaption = "Interest Amount for this Payee"
            col_interest_this_creditor.DisplayFormat.FormatType = FormatType.Numeric
            col_interest_this_creditor.FieldName = "interest_this_creditor"
            col_interest_this_creditor.GroupFormat.FormatType = FormatType.Numeric
            col_interest_this_creditor.GroupInterval = ColumnGroupInterval.Value
            col_interest_this_creditor.Name = "col_interest_this_creditor"
            col_interest_this_creditor.SummaryItem.SummaryType = SummaryItemType.Sum
            col_interest_this_creditor.ToolTip = "Total interest charged by this creditor."
            col_interest_this_creditor.DisplayFormat.FormatString = "{0:c}"
            col_interest_this_creditor.GroupFormat.FormatString = "{0:c}"
            col_interest_this_creditor.SummaryItem.DisplayFormat = "{0:c}"
            col_interest_this_creditor.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_sched_payment
            '
            col_sched_payment.AppearanceCell.Options.UseTextOptions = True
            col_sched_payment.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_sched_payment.AppearanceHeader.Options.UseTextOptions = True
            col_sched_payment.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_sched_payment.Caption = "Sched Pay"
            col_sched_payment.CustomizationCaption = "Scheduled payment amount"
            col_sched_payment.DisplayFormat.FormatType = FormatType.Numeric
            col_sched_payment.FieldName = "sched_payment"
            col_sched_payment.GroupFormat.FormatType = FormatType.Numeric
            col_sched_payment.GroupInterval = ColumnGroupInterval.Value
            col_sched_payment.Name = "col_sched_payment"
            col_sched_payment.SummaryItem.SummaryType = SummaryItemType.Sum
            col_sched_payment.ToolTip = "Current scheduled payment."
            col_sched_payment.DisplayFormat.FormatString = "{0:c}"
            col_sched_payment.GroupFormat.FormatString = "{0:c}"
            col_sched_payment.SummaryItem.DisplayFormat = "{0:c}"
            col_sched_payment.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_current_sched_payment
            '
            col_current_sched_payment.AppearanceCell.Options.UseTextOptions = True
            col_current_sched_payment.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_current_sched_payment.AppearanceHeader.Options.UseTextOptions = True
            col_current_sched_payment.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_current_sched_payment.Caption = "Sched"
            col_current_sched_payment.CustomizationCaption = "Current Scheduled Payment"
            col_current_sched_payment.DisplayFormat.FormatType = FormatType.Numeric
            col_current_sched_payment.FieldName = "current_sched_payment"
            col_current_sched_payment.GroupFormat.FormatType = FormatType.Numeric
            col_current_sched_payment.GroupInterval = ColumnGroupInterval.Value
            col_current_sched_payment.Name = "col_current_sched_payment"
            col_current_sched_payment.SummaryItem.SummaryType = SummaryItemType.Sum
            col_current_sched_payment.ToolTip = "Original scheduled payment at the start of the month."
            col_current_sched_payment.DisplayFormat.FormatString = "{0:c}"
            col_current_sched_payment.GroupFormat.FormatString = "{0:c}"
            col_current_sched_payment.SummaryItem.DisplayFormat = "{0:c}"
            col_current_sched_payment.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_total_sched_payment
            '
            col_total_sched_payment.AppearanceCell.Options.UseTextOptions = True
            col_total_sched_payment.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_total_sched_payment.AppearanceHeader.Options.UseTextOptions = True
            col_total_sched_payment.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_total_sched_payment.Caption = "Scheduled"
            col_total_sched_payment.CustomizationCaption = "Total Scheduled Payments Amount"
            col_total_sched_payment.DisplayFormat.FormatType = FormatType.Numeric
            col_total_sched_payment.FieldName = "total_sched_payment"
            col_total_sched_payment.GroupFormat.FormatType = FormatType.Numeric
            col_total_sched_payment.GroupInterval = ColumnGroupInterval.Value
            col_total_sched_payment.Name = "col_total_sched_payment"
            col_total_sched_payment.SummaryItem.SummaryType = SummaryItemType.Sum
            col_total_sched_payment.ToolTip = "Total amount of all scheduled payments on this debt."
            col_total_sched_payment.DisplayFormat.FormatString = "{0:c}"
            col_total_sched_payment.GroupFormat.FormatString = "{0:c}"
            col_total_sched_payment.SummaryItem.DisplayFormat = "{0:c}"
            col_total_sched_payment.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_display_orig_balance
            '
            col_display_orig_balance.AppearanceCell.Options.UseTextOptions = True
            col_display_orig_balance.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_display_orig_balance.AppearanceHeader.Options.UseTextOptions = True
            col_display_orig_balance.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_display_orig_balance.Caption = "Orig Bal"
            col_display_orig_balance.CustomizationCaption = "Original Balance"
            col_display_orig_balance.DisplayFormat.FormatType = FormatType.Numeric
            col_display_orig_balance.FieldName = "display_orig_balance"
            col_display_orig_balance.GroupFormat.FormatType = FormatType.Numeric
            col_display_orig_balance.GroupInterval = ColumnGroupInterval.Value
            col_display_orig_balance.Name = "col_display_orig_balance"
            col_display_orig_balance.SummaryItem.SummaryType = SummaryItemType.Sum
            col_display_orig_balance.ToolTip = "If reassigned value is $0.00. Otherwise it is the original balance."
            col_display_orig_balance.Visible = True
            col_display_orig_balance.VisibleIndex = 4
            col_display_orig_balance.Width = 69
            col_display_orig_balance.DisplayFormat.FormatString = "{0:c}"
            col_display_orig_balance.GroupFormat.FormatString = "{0:c}"
            col_display_orig_balance.SummaryItem.DisplayFormat = "{0:c}"
            col_display_orig_balance.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_display_disbursement_factor
            '
            col_display_disbursement_factor.AppearanceCell.Options.UseTextOptions = True
            col_display_disbursement_factor.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_display_disbursement_factor.AppearanceHeader.Options.UseTextOptions = True
            col_display_disbursement_factor.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_display_disbursement_factor.Caption = "Disb Factor"
            col_display_disbursement_factor.CustomizationCaption = "Disbursement Factor"
            col_display_disbursement_factor.DisplayFormat.FormatType = FormatType.Numeric
            col_display_disbursement_factor.FieldName = "display_disbursement_factor"
            col_display_disbursement_factor.GroupFormat.FormatType = FormatType.Numeric
            col_display_disbursement_factor.GroupInterval = ColumnGroupInterval.Value
            col_display_disbursement_factor.Name = "col_display_disbursement_factor"
            col_display_disbursement_factor.SummaryItem.SummaryType = SummaryItemType.Sum
            col_display_disbursement_factor.ToolTip = "Adjusted disbursement factor. Limited to debt balance."
            col_display_disbursement_factor.Visible = True
            col_display_disbursement_factor.VisibleIndex = 6
            col_display_disbursement_factor.Width = 79
            col_display_disbursement_factor.DisplayFormat.FormatString = "{0:c}"
            col_display_disbursement_factor.GroupFormat.FormatString = "{0:c}"
            col_display_disbursement_factor.SummaryItem.DisplayFormat = "{0:c}"
            col_display_disbursement_factor.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_returns_this_creditor
            '
            col_returns_this_creditor.AppearanceCell.Options.UseTextOptions = True
            col_returns_this_creditor.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_returns_this_creditor.AppearanceHeader.Options.UseTextOptions = True
            col_returns_this_creditor.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_returns_this_creditor.Caption = "Returns"
            col_returns_this_creditor.CustomizationCaption = "Dollar amount refunded for this payee"
            col_returns_this_creditor.DisplayFormat.FormatType = FormatType.Numeric
            col_returns_this_creditor.FieldName = "returns_this_creditor"
            col_returns_this_creditor.GroupFormat.FormatType = FormatType.Numeric
            col_returns_this_creditor.GroupInterval = ColumnGroupInterval.Value
            col_returns_this_creditor.Name = "col_returns_this_creditor"
            col_returns_this_creditor.SummaryItem.SummaryType = SummaryItemType.Sum
            col_returns_this_creditor.ToolTip = "Refunds/Voids from this creditor."
            col_returns_this_creditor.DisplayFormat.FormatString = "{0:c}"
            col_returns_this_creditor.GroupFormat.FormatString = "{0:c}"
            col_returns_this_creditor.SummaryItem.DisplayFormat = "{0:c}"
            col_returns_this_creditor.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_check_payments
            '
            col_check_payments.AppearanceCell.Options.UseTextOptions = True
            col_check_payments.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_check_payments.AppearanceHeader.Options.UseTextOptions = True
            col_check_payments.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_check_payments.Caption = "#CHK Pmts"
            col_check_payments.CustomizationCaption = "Number of Check Payments"
            col_check_payments.DisplayFormat.FormatString = "f0"
            col_check_payments.DisplayFormat.FormatType = FormatType.Numeric
            col_check_payments.FieldName = "check_payments"
            col_check_payments.GroupFormat.FormatString = "f0"
            col_check_payments.GroupFormat.FormatType = FormatType.Numeric
            col_check_payments.Name = "col_check_payments"
            col_check_payments.ToolTip = "Number of payments made by check."
            col_check_payments.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_client_creditor
            '
            col_client_creditor.AppearanceCell.Options.UseTextOptions = True
            col_client_creditor.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_client_creditor.AppearanceHeader.Options.UseTextOptions = True
            col_client_creditor.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_client_creditor.Caption = "Payment ID"
            col_client_creditor.CustomizationCaption = "ID For payment record"
            col_client_creditor.DisplayFormat.FormatString = "f0"
            col_client_creditor.DisplayFormat.FormatType = FormatType.Numeric
            col_client_creditor.FieldName = "client_creditor"
            col_client_creditor.GroupFormat.FormatString = "f0"
            col_client_creditor.GroupFormat.FormatType = FormatType.Numeric
            col_client_creditor.Name = "col_client_creditor"
            col_client_creditor.ToolTip = "Pointer to the payee record in the client_creditor table."
            col_client_creditor.OptionsColumn.AllowGroup = DefaultBoolean.True
            col_client_creditor.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_client_creditor_balance
            '
            col_client_creditor_balance.AppearanceCell.Options.UseTextOptions = True
            col_client_creditor_balance.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_client_creditor_balance.AppearanceHeader.Options.UseTextOptions = True
            col_client_creditor_balance.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_client_creditor_balance.Caption = "Debt ID"
            col_client_creditor_balance.CustomizationCaption = "ID for debt record"
            col_client_creditor_balance.DisplayFormat.FormatString = "f0"
            col_client_creditor_balance.DisplayFormat.FormatType = FormatType.Numeric
            col_client_creditor_balance.FieldName = "client_creditor_balance"
            col_client_creditor_balance.GroupFormat.FormatString = "f0"
            col_client_creditor_balance.GroupFormat.FormatType = FormatType.Numeric
            col_client_creditor_balance.Name = "col_client_creditor_balance"
            col_client_creditor_balance.ToolTip = "Pointer to the debt record in the client_creditor_balances table."
            col_client_creditor_balance.OptionsColumn.AllowGroup = DefaultBoolean.True
            col_client_creditor_balance.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_client_creditor_proposal
            '
            col_client_creditor_proposal.AppearanceCell.Options.UseTextOptions = True
            col_client_creditor_proposal.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_client_creditor_proposal.AppearanceHeader.Options.UseTextOptions = True
            col_client_creditor_proposal.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_client_creditor_proposal.Caption = "Proposal ID"
            col_client_creditor_proposal.CustomizationCaption = "ID for proposal record"
            col_client_creditor_proposal.DisplayFormat.FormatString = "f0"
            col_client_creditor_proposal.DisplayFormat.FormatType = FormatType.Numeric
            col_client_creditor_proposal.FieldName = "client_creditor_proposal"
            col_client_creditor_proposal.GroupFormat.FormatString = "f0"
            col_client_creditor_proposal.GroupFormat.FormatType = FormatType.Numeric
            col_client_creditor_proposal.Name = "col_client_creditor_proposal"
            col_client_creditor_proposal.ToolTip = "Pointer to the proposal in client_creditor_proposals table."
            col_client_creditor_proposal.OptionsColumn.AllowGroup = DefaultBoolean.True
            col_client_creditor_proposal.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_first_payment
            '
            col_first_payment.AppearanceCell.Options.UseTextOptions = True
            col_first_payment.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_first_payment.AppearanceHeader.Options.UseTextOptions = True
            col_first_payment.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_first_payment.Caption = "First Payment ID"
            col_first_payment.CustomizationCaption = "ID of the first payment record"
            col_first_payment.DisplayFormat.FormatString = "f0"
            col_first_payment.DisplayFormat.FormatType = FormatType.Numeric
            col_first_payment.FieldName = "first_payment"
            col_first_payment.GroupFormat.FormatString = "f0"
            col_first_payment.GroupFormat.FormatType = FormatType.Numeric
            col_first_payment.Name = "col_first_payment"
            col_first_payment.ToolTip = "Pointer to the first payment in registers_client_creditor table"
            col_first_payment.OptionsColumn.AllowGroup = DefaultBoolean.True
            col_first_payment.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_last_payment
            '
            col_last_payment.AppearanceCell.Options.UseTextOptions = True
            col_last_payment.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_last_payment.AppearanceHeader.Options.UseTextOptions = True
            col_last_payment.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_last_payment.Caption = "Last Payment ID"
            col_last_payment.CustomizationCaption = "ID of the last payment record"
            col_last_payment.DisplayFormat.FormatString = "f0"
            col_last_payment.DisplayFormat.FormatType = FormatType.Numeric
            col_last_payment.FieldName = "last_payment"
            col_last_payment.GroupFormat.FormatString = "f0"
            col_last_payment.GroupFormat.FormatType = FormatType.Numeric
            col_last_payment.Name = "col_last_payment"
            col_last_payment.ToolTip = "Pointer to the last payment in registers_client_creditor table"
            col_last_payment.OptionsColumn.AllowGroup = DefaultBoolean.True
            col_last_payment.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_person
            '
            col_person.AppearanceCell.Options.UseTextOptions = True
            col_person.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_person.AppearanceHeader.Options.UseTextOptions = True
            col_person.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_person.Caption = "Signer ID"
            col_person.CustomizationCaption = "ID of the signer (account owner)"
            col_person.DisplayFormat.FormatString = "f0"
            col_person.DisplayFormat.FormatType = FormatType.Numeric
            col_person.FieldName = "person"
            col_person.GroupFormat.FormatString = "f0"
            col_person.GroupFormat.FormatType = FormatType.Numeric
            col_person.Name = "col_person"
            col_person.ToolTip = "Pointer to the people table for the signer of this debt"
            col_person.OptionsColumn.AllowGroup = DefaultBoolean.True
            col_person.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_balance_client_creditor
            '
            col_balance_client_creditor.AppearanceCell.Options.UseTextOptions = True
            col_balance_client_creditor.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_balance_client_creditor.AppearanceHeader.Options.UseTextOptions = True
            col_balance_client_creditor.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_balance_client_creditor.Caption = "Payee ID"
            col_balance_client_creditor.CustomizationCaption = "Pointer to payee record"
            col_balance_client_creditor.DisplayFormat.FormatString = "f0"
            col_balance_client_creditor.DisplayFormat.FormatType = FormatType.Numeric
            col_balance_client_creditor.FieldName = "balance_client_creditor"
            col_balance_client_creditor.GroupFormat.FormatString = "f0"
            col_balance_client_creditor.GroupFormat.FormatType = FormatType.Numeric
            col_balance_client_creditor.Name = "col_balance_client_creditor"
            col_balance_client_creditor.ToolTip = "Pointer from the debt record to the payee record. reassigned debts have a different value from client_creditor"
            col_balance_client_creditor.OptionsColumn.AllowGroup = DefaultBoolean.True
            col_balance_client_creditor.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_payment_rpps_mask
            '
            col_payment_rpps_mask.AppearanceCell.Options.UseTextOptions = True
            col_payment_rpps_mask.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_payment_rpps_mask.AppearanceHeader.Options.UseTextOptions = True
            col_payment_rpps_mask.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_payment_rpps_mask.Caption = "Payment Mask ID"
            col_payment_rpps_mask.CustomizationCaption = "ID of payment mask record"
            col_payment_rpps_mask.DisplayFormat.FormatString = "f0"
            col_payment_rpps_mask.DisplayFormat.FormatType = FormatType.Numeric
            col_payment_rpps_mask.FieldName = "payment_rpps_mask"
            col_payment_rpps_mask.GroupFormat.FormatString = "f0"
            col_payment_rpps_mask.GroupFormat.FormatType = FormatType.Numeric
            col_payment_rpps_mask.Name = "col_payment_rpps_mask"
            col_payment_rpps_mask.ToolTip = "Pointer to the RPPS mask record for payments."
            col_payment_rpps_mask.OptionsColumn.AllowGroup = DefaultBoolean.True
            col_payment_rpps_mask.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_Drop_Reason
            '
            col_Drop_Reason.AppearanceCell.Options.UseTextOptions = True
            col_Drop_Reason.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_Drop_Reason.AppearanceHeader.Options.UseTextOptions = True
            col_Drop_Reason.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_Drop_Reason.Caption = "Drop Reason"
            col_Drop_Reason.CustomizationCaption = "ID For Drop Reason"
            col_Drop_Reason.DisplayFormat.FormatString = "f0"
            col_Drop_Reason.DisplayFormat.FormatType = FormatType.Numeric
            col_Drop_Reason.FieldName = "Drop_Reason"
            col_Drop_Reason.GroupFormat.FormatString = "f0"
            col_Drop_Reason.GroupFormat.FormatType = FormatType.Numeric
            col_Drop_Reason.Name = "col_Drop_Reason"
            col_Drop_Reason.ToolTip = "Reason why the debt was closed"
            col_Drop_Reason.OptionsColumn.AllowGroup = DefaultBoolean.True
            col_Drop_Reason.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_percent_balance
            '
            col_percent_balance.AppearanceCell.Options.UseTextOptions = True
            col_percent_balance.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_percent_balance.AppearanceHeader.Options.UseTextOptions = True
            col_percent_balance.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_percent_balance.Caption = "Pct Bal"
            col_percent_balance.CustomizationCaption = "Percentage of the debt balance"
            col_percent_balance.DisplayFormat.FormatString = "p3"
            col_percent_balance.DisplayFormat.FormatType = FormatType.Numeric
            col_percent_balance.FieldName = "percent_balance"
            col_percent_balance.GroupFormat.FormatString = "p3"
            col_percent_balance.GroupFormat.FormatType = FormatType.Numeric
            col_percent_balance.Name = "col_percent_balance"
            col_percent_balance.ToolTip = "Percent of the balance for proposals of this debt."
            col_percent_balance.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_priority
            '
            col_priority.AppearanceCell.Options.UseTextOptions = True
            col_priority.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_priority.AppearanceHeader.Options.UseTextOptions = True
            col_priority.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_priority.Caption = "Priority"
            col_priority.CustomizationCaption = "Payment Priority"
            col_priority.DisplayFormat.FormatString = "f0"
            col_priority.DisplayFormat.FormatType = FormatType.Numeric
            col_priority.FieldName = "priority"
            col_priority.GroupFormat.FormatString = "f0"
            col_priority.GroupFormat.FormatType = FormatType.Numeric
            col_priority.Name = "col_priority"
            col_priority.ToolTip = "Payment priority (0 = highest, 9 = lowest)"
            col_priority.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_rpps_mask
            '
            col_rpps_mask.AppearanceCell.Options.UseTextOptions = True
            col_rpps_mask.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_rpps_mask.AppearanceHeader.Options.UseTextOptions = True
            col_rpps_mask.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_rpps_mask.Caption = "RPPS Mask ID"
            col_rpps_mask.CustomizationCaption = "ID for the RPPS mask record"
            col_rpps_mask.DisplayFormat.FormatString = "f0"
            col_rpps_mask.DisplayFormat.FormatType = FormatType.Numeric
            col_rpps_mask.FieldName = "rpps_mask"
            col_rpps_mask.GroupFormat.FormatString = "f0"
            col_rpps_mask.GroupFormat.FormatType = FormatType.Numeric
            col_rpps_mask.Name = "col_rpps_mask"
            col_rpps_mask.ToolTip = "Pointer to the record for the RPPS mask table"
            col_rpps_mask.OptionsColumn.AllowGroup = DefaultBoolean.True
            col_rpps_mask.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_terms
            '
            col_terms.AppearanceCell.Options.UseTextOptions = True
            col_terms.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_terms.AppearanceHeader.Options.UseTextOptions = True
            col_terms.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_terms.Caption = "Terms"
            col_terms.CustomizationCaption = "Proposed payment term in months"
            col_terms.DisplayFormat.FormatString = "f0"
            col_terms.DisplayFormat.FormatType = FormatType.Numeric
            col_terms.FieldName = "terms"
            col_terms.GroupFormat.FormatString = "f0"
            col_terms.GroupFormat.FormatType = FormatType.Numeric
            col_terms.Name = "col_terms"
            col_terms.ToolTip = "Payment terms (in months) for proposals"
            col_terms.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_months_delinquent
            '
            col_months_delinquent.AppearanceCell.Options.UseTextOptions = True
            col_months_delinquent.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_months_delinquent.AppearanceHeader.Options.UseTextOptions = True
            col_months_delinquent.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_months_delinquent.Caption = "Months Delinquent"
            col_months_delinquent.CustomizationCaption = "Number of Months Delinquent"
            col_months_delinquent.DisplayFormat.FormatString = "f0"
            col_months_delinquent.DisplayFormat.FormatType = FormatType.Numeric
            col_months_delinquent.FieldName = "months_delinquent"
            col_months_delinquent.GroupFormat.FormatString = "f0"
            col_months_delinquent.GroupFormat.FormatType = FormatType.Numeric
            col_months_delinquent.Name = "col_months_delinquent"
            col_months_delinquent.ToolTip = "Number of months payments have been in default."
            col_months_delinquent.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_line_number
            '
            col_line_number.AppearanceCell.Options.UseTextOptions = True
            col_line_number.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_line_number.AppearanceHeader.Options.UseTextOptions = True
            col_line_number.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_line_number.Caption = "Ln"
            col_line_number.CustomizationCaption = "Stacking Sequence Number"
            col_line_number.DisplayFormat.FormatString = "f0"
            col_line_number.DisplayFormat.FormatType = FormatType.Numeric
            col_line_number.FieldName = "line_number"
            col_line_number.GroupFormat.FormatString = "f0"
            col_line_number.GroupFormat.FormatType = FormatType.Numeric
            col_line_number.Name = "col_line_number"
            col_line_number.ToolTip = "Relative stacking order for debts."
            col_line_number.Visible = True
            col_line_number.VisibleIndex = 0
            col_line_number.Width = 31
            col_line_number.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_display_dmp_interest
            '
            col_display_dmp_interest.AppearanceCell.Options.UseTextOptions = True
            col_display_dmp_interest.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_display_dmp_interest.AppearanceHeader.Options.UseTextOptions = True
            col_display_dmp_interest.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_display_dmp_interest.Caption = "Interest"
            col_display_dmp_interest.CustomizationCaption = "Interest rate used for plan"
            col_display_dmp_interest.DisplayFormat.FormatString = "p3"
            col_display_dmp_interest.DisplayFormat.FormatType = FormatType.Numeric
            col_display_dmp_interest.FieldName = "display_dmp_interest"
            col_display_dmp_interest.GroupFormat.FormatString = "p3"
            col_display_dmp_interest.GroupFormat.FormatType = FormatType.Numeric
            col_display_dmp_interest.Name = "col_display_dmp_interest"
            col_display_dmp_interest.ToolTip = "DMP interest rate override."
            col_display_dmp_interest.Visible = True
            col_display_dmp_interest.VisibleIndex = 12
            col_display_dmp_interest.Width = 37
            col_display_dmp_interest.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_dmp_payout_interest
            '
            col_dmp_payout_interest.AppearanceCell.Options.UseTextOptions = True
            col_dmp_payout_interest.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_dmp_payout_interest.AppearanceHeader.Options.UseTextOptions = True
            col_dmp_payout_interest.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_dmp_payout_interest.Caption = "Payout %"
            col_dmp_payout_interest.CustomizationCaption = "Interest rate used for payout"
            col_dmp_payout_interest.DisplayFormat.FormatString = "p3"
            col_dmp_payout_interest.DisplayFormat.FormatType = FormatType.Numeric
            col_dmp_payout_interest.FieldName = "dmp_payout_interest"
            col_dmp_payout_interest.GroupFormat.FormatString = "p3"
            col_dmp_payout_interest.GroupFormat.FormatType = FormatType.Numeric
            col_dmp_payout_interest.Name = "col_dmp_payout_interest"
            col_dmp_payout_interest.ToolTip = "Interest rate for payout schedules."
            col_dmp_payout_interest.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_fairshare_pct_check
            '
            col_fairshare_pct_check.AppearanceCell.Options.UseTextOptions = True
            col_fairshare_pct_check.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_fairshare_pct_check.AppearanceHeader.Options.UseTextOptions = True
            col_fairshare_pct_check.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_fairshare_pct_check.Caption = "Fairshare % CHK"
            col_fairshare_pct_check.CustomizationCaption = "Fair-share Percentage for Checks"
            col_fairshare_pct_check.DisplayFormat.FormatString = "p3"
            col_fairshare_pct_check.DisplayFormat.FormatType = FormatType.Numeric
            col_fairshare_pct_check.FieldName = "fairshare_pct_check"
            col_fairshare_pct_check.GroupFormat.FormatString = "p3"
            col_fairshare_pct_check.GroupFormat.FormatType = FormatType.Numeric
            col_fairshare_pct_check.Name = "col_fairshare_pct_check"
            col_fairshare_pct_check.ToolTip = "Override to the fair-share percentage on checks."
            col_fairshare_pct_check.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_non_dmp_interest
            '
            col_non_dmp_interest.AppearanceCell.Options.UseTextOptions = True
            col_non_dmp_interest.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_non_dmp_interest.AppearanceHeader.Options.UseTextOptions = True
            col_non_dmp_interest.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_non_dmp_interest.Caption = "NonDMP %"
            col_non_dmp_interest.CustomizationCaption = "Non-DMP Interest Rate"
            col_non_dmp_interest.DisplayFormat.FormatString = "p3"
            col_non_dmp_interest.DisplayFormat.FormatType = FormatType.Numeric
            col_non_dmp_interest.FieldName = "non_dmp_interest"
            col_non_dmp_interest.GroupFormat.FormatString = "p3"
            col_non_dmp_interest.GroupFormat.FormatType = FormatType.Numeric
            col_non_dmp_interest.Name = "col_non_dmp_interest"
            col_non_dmp_interest.ToolTip = "Interest rate client was paying before starting DMP"
            col_non_dmp_interest.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_fairshare_pct_eft
            '
            col_fairshare_pct_eft.AppearanceCell.Options.UseTextOptions = True
            col_fairshare_pct_eft.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_fairshare_pct_eft.AppearanceHeader.Options.UseTextOptions = True
            col_fairshare_pct_eft.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_fairshare_pct_eft.Caption = "Fairshare % EFT"
            col_fairshare_pct_eft.CustomizationCaption = "Fair-share Percentage for EFT"
            col_fairshare_pct_eft.DisplayFormat.FormatString = "p3"
            col_fairshare_pct_eft.DisplayFormat.FormatType = FormatType.Numeric
            col_fairshare_pct_eft.FieldName = "fairshare_pct_eft"
            col_fairshare_pct_eft.GroupFormat.FormatString = "p3"
            col_fairshare_pct_eft.GroupFormat.FormatType = FormatType.Numeric
            col_fairshare_pct_eft.Name = "col_fairshare_pct_eft"
            col_fairshare_pct_eft.ToolTip = "Override to the fair-share percentage on EFT."
            col_fairshare_pct_eft.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_prenote_date
            '
            col_prenote_date.AppearanceCell.Options.UseTextOptions = True
            col_prenote_date.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_prenote_date.AppearanceHeader.Options.UseTextOptions = True
            col_prenote_date.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_prenote_date.Caption = "Prenoted"
            col_prenote_date.CustomizationCaption = "Date account prenoted"
            col_prenote_date.DisplayFormat.FormatString = "d"
            col_prenote_date.DisplayFormat.FormatType = FormatType.DateTime
            col_prenote_date.FieldName = "prenote_date"
            col_prenote_date.GroupFormat.FormatString = "d"
            col_prenote_date.GroupFormat.FormatType = FormatType.DateTime
            col_prenote_date.GroupInterval = ColumnGroupInterval.[Date]
            col_prenote_date.Name = "col_prenote_date"
            col_prenote_date.ToolTip = "Date the account was prenoted."
            col_prenote_date.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_drop_date
            '
            col_drop_date.AppearanceCell.Options.UseTextOptions = True
            col_drop_date.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_drop_date.AppearanceHeader.Options.UseTextOptions = True
            col_drop_date.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_drop_date.Caption = "Dropped"
            col_drop_date.CustomizationCaption = "Date debt was dropped"
            col_drop_date.DisplayFormat.FormatString = "d"
            col_drop_date.DisplayFormat.FormatType = FormatType.DateTime
            col_drop_date.FieldName = "drop_date"
            col_drop_date.GroupFormat.FormatString = "d"
            col_drop_date.GroupFormat.FormatType = FormatType.DateTime
            col_drop_date.GroupInterval = ColumnGroupInterval.[Date]
            col_drop_date.Name = "col_drop_date"
            col_drop_date.ToolTip = "Date the account was closed"
            col_drop_date.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_Drop_Reason_sent
            '
            col_Drop_Reason_sent.AppearanceCell.Options.UseTextOptions = True
            col_Drop_Reason_sent.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_Drop_Reason_sent.AppearanceHeader.Options.UseTextOptions = True
            col_Drop_Reason_sent.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_Drop_Reason_sent.Caption = "Drop Sent"
            col_Drop_Reason_sent.CustomizationCaption = "Date Drop Reason Sent"
            col_Drop_Reason_sent.DisplayFormat.FormatString = "d"
            col_Drop_Reason_sent.DisplayFormat.FormatType = FormatType.DateTime
            col_Drop_Reason_sent.FieldName = "Drop_Reason_sent"
            col_Drop_Reason_sent.GroupFormat.FormatString = "d"
            col_Drop_Reason_sent.GroupFormat.FormatType = FormatType.DateTime
            col_Drop_Reason_sent.GroupInterval = ColumnGroupInterval.[Date]
            col_Drop_Reason_sent.Name = "col_Drop_Reason_sent"
            col_Drop_Reason_sent.ToolTip = "Date when the creditor was notified that the account was closed"
            col_Drop_Reason_sent.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_date_created
            '
            col_date_created.AppearanceCell.Options.UseTextOptions = True
            col_date_created.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_date_created.AppearanceHeader.Options.UseTextOptions = True
            col_date_created.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_date_created.Caption = "Date Created"
            col_date_created.CustomizationCaption = "Date Debt Created"
            col_date_created.DisplayFormat.FormatString = "d"
            col_date_created.DisplayFormat.FormatType = FormatType.DateTime
            col_date_created.FieldName = "date_created"
            col_date_created.GroupFormat.FormatString = "d"
            col_date_created.GroupFormat.FormatType = FormatType.DateTime
            col_date_created.GroupInterval = ColumnGroupInterval.[Date]
            col_date_created.Name = "col_date_created"
            col_date_created.ToolTip = "Date when the debt was created"
            col_date_created.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_Drop_Reason_date
            '
            col_Drop_Reason_date.AppearanceCell.Options.UseTextOptions = True
            col_Drop_Reason_date.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_Drop_Reason_date.AppearanceHeader.Options.UseTextOptions = True
            col_Drop_Reason_date.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_Drop_Reason_date.Caption = "Drop Reason Date"
            col_Drop_Reason_date.CustomizationCaption = "Drop Reason Date"
            col_Drop_Reason_date.DisplayFormat.FormatString = "d"
            col_Drop_Reason_date.DisplayFormat.FormatType = FormatType.DateTime
            col_Drop_Reason_date.FieldName = "Drop_Reason_date"
            col_Drop_Reason_date.GroupFormat.FormatString = "d"
            col_Drop_Reason_date.GroupFormat.FormatType = FormatType.DateTime
            col_Drop_Reason_date.GroupInterval = ColumnGroupInterval.[Date]
            col_Drop_Reason_date.Name = "col_Drop_Reason_date"
            col_Drop_Reason_date.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_date_disp_changed
            '
            col_date_disp_changed.AppearanceCell.Options.UseTextOptions = True
            col_date_disp_changed.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_date_disp_changed.AppearanceHeader.Options.UseTextOptions = True
            col_date_disp_changed.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_date_disp_changed.Caption = "Date Disb Changed"
            col_date_disp_changed.CustomizationCaption = "Date Disbursement Factor Changed"
            col_date_disp_changed.DisplayFormat.FormatString = "d"
            col_date_disp_changed.DisplayFormat.FormatType = FormatType.DateTime
            col_date_disp_changed.FieldName = "date_disp_changed"
            col_date_disp_changed.GroupFormat.FormatString = "d"
            col_date_disp_changed.GroupFormat.FormatType = FormatType.DateTime
            col_date_disp_changed.GroupInterval = ColumnGroupInterval.[Date]
            col_date_disp_changed.Name = "col_date_disp_changed"
            col_date_disp_changed.ToolTip = "Date when the disbursement factor was last changed."
            col_date_disp_changed.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_last_payment_date_b4_dmp
            '
            col_last_payment_date_b4_dmp.AppearanceCell.Options.UseTextOptions = True
            col_last_payment_date_b4_dmp.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_last_payment_date_b4_dmp.AppearanceHeader.Options.UseTextOptions = True
            col_last_payment_date_b4_dmp.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_last_payment_date_b4_dmp.Caption = "Pmt Before DMP"
            col_last_payment_date_b4_dmp.CustomizationCaption = "Last Payment Date before DMP program"
            col_last_payment_date_b4_dmp.DisplayFormat.FormatString = "d"
            col_last_payment_date_b4_dmp.DisplayFormat.FormatType = FormatType.DateTime
            col_last_payment_date_b4_dmp.FieldName = "last_payment_date_b4_dmp"
            col_last_payment_date_b4_dmp.GroupFormat.FormatString = "d"
            col_last_payment_date_b4_dmp.GroupFormat.FormatType = FormatType.DateTime
            col_last_payment_date_b4_dmp.GroupInterval = ColumnGroupInterval.[Date]
            col_last_payment_date_b4_dmp.Name = "col_last_payment_date_b4_dmp"
            col_last_payment_date_b4_dmp.ToolTip = "Date the client last made a payment before starting the DMP program"
            col_last_payment_date_b4_dmp.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_expected_payout_date
            '
            col_expected_payout_date.AppearanceCell.Options.UseTextOptions = True
            col_expected_payout_date.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_expected_payout_date.AppearanceHeader.Options.UseTextOptions = True
            col_expected_payout_date.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_expected_payout_date.Caption = "Payout Date"
            col_expected_payout_date.CustomizationCaption = "Expected Payout Date"
            col_expected_payout_date.DisplayFormat.FormatString = "d"
            col_expected_payout_date.DisplayFormat.FormatType = FormatType.DateTime
            col_expected_payout_date.FieldName = "expected_payout_date"
            col_expected_payout_date.GroupFormat.FormatString = "d"
            col_expected_payout_date.GroupFormat.FormatType = FormatType.DateTime
            col_expected_payout_date.GroupInterval = ColumnGroupInterval.[Date]
            col_expected_payout_date.Name = "col_expected_payout_date"
            col_expected_payout_date.ToolTip = "Expected payout date for this debt"
            col_expected_payout_date.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_start_date
            '
            col_start_date.AppearanceCell.Options.UseTextOptions = True
            col_start_date.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_start_date.AppearanceHeader.Options.UseTextOptions = True
            col_start_date.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_start_date.Caption = "Start Date"
            col_start_date.CustomizationCaption = "Start Date"
            col_start_date.DisplayFormat.FormatString = "d"
            col_start_date.DisplayFormat.FormatType = FormatType.DateTime
            col_start_date.FieldName = "start_date"
            col_start_date.GroupFormat.FormatString = "d"
            col_start_date.GroupFormat.FormatType = FormatType.DateTime
            col_start_date.GroupInterval = ColumnGroupInterval.[Date]
            col_start_date.Name = "col_start_date"
            col_start_date.ToolTip = "Start date for this debt."
            col_start_date.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_last_stmt_date
            '
            col_last_stmt_date.AppearanceCell.Options.UseTextOptions = True
            col_last_stmt_date.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_last_stmt_date.AppearanceHeader.Options.UseTextOptions = True
            col_last_stmt_date.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_last_stmt_date.Caption = "STMT Date"
            col_last_stmt_date.CustomizationCaption = "Date of last client statemenet received"
            col_last_stmt_date.DisplayFormat.FormatString = "d"
            col_last_stmt_date.DisplayFormat.FormatType = FormatType.DateTime
            col_last_stmt_date.FieldName = "last_stmt_date"
            col_last_stmt_date.GroupFormat.FormatString = "d"
            col_last_stmt_date.GroupFormat.FormatType = FormatType.DateTime
            col_last_stmt_date.GroupInterval = ColumnGroupInterval.[Date]
            col_last_stmt_date.Name = "col_last_stmt_date"
            col_last_stmt_date.ToolTip = "Date when a statement was last received from the client"
            col_last_stmt_date.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_verify_request_date
            '
            col_verify_request_date.AppearanceCell.Options.UseTextOptions = True
            col_verify_request_date.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_verify_request_date.AppearanceHeader.Options.UseTextOptions = True
            col_verify_request_date.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_verify_request_date.Caption = "Bal Verified Date"
            col_verify_request_date.CustomizationCaption = "Date Balance Verification last Sent"
            col_verify_request_date.DisplayFormat.FormatString = "d"
            col_verify_request_date.DisplayFormat.FormatType = FormatType.DateTime
            col_verify_request_date.FieldName = "verify_request_date"
            col_verify_request_date.GroupFormat.FormatString = "d"
            col_verify_request_date.GroupFormat.FormatType = FormatType.DateTime
            col_verify_request_date.GroupInterval = ColumnGroupInterval.[Date]
            col_verify_request_date.Name = "col_verify_request_date"
            col_verify_request_date.ToolTip = "Date when the balance verification was generated"
            col_verify_request_date.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_balance_verify_date
            '
            col_balance_verify_date.AppearanceCell.Options.UseTextOptions = True
            col_balance_verify_date.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_balance_verify_date.AppearanceHeader.Options.UseTextOptions = True
            col_balance_verify_date.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_balance_verify_date.Caption = "Verified Date"
            col_balance_verify_date.CustomizationCaption = "Date debt was Verified"
            col_balance_verify_date.DisplayFormat.FormatString = "d"
            col_balance_verify_date.DisplayFormat.FormatType = FormatType.DateTime
            col_balance_verify_date.FieldName = "balance_verify_date"
            col_balance_verify_date.GroupFormat.FormatString = "d"
            col_balance_verify_date.GroupFormat.FormatType = FormatType.DateTime
            col_balance_verify_date.GroupInterval = ColumnGroupInterval.[Date]
            col_balance_verify_date.Name = "col_balance_verify_date"
            col_balance_verify_date.ToolTip = "Date when a balance verification was last performed."
            col_balance_verify_date.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_first_payment_date
            '
            col_first_payment_date.AppearanceCell.Options.UseTextOptions = True
            col_first_payment_date.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_first_payment_date.AppearanceHeader.Options.UseTextOptions = True
            col_first_payment_date.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_first_payment_date.Caption = "First Pmt"
            col_first_payment_date.CustomizationCaption = "First Payment Date"
            col_first_payment_date.DisplayFormat.FormatString = "d"
            col_first_payment_date.DisplayFormat.FormatType = FormatType.DateTime
            col_first_payment_date.FieldName = "first_payment_date"
            col_first_payment_date.GroupFormat.FormatString = "d"
            col_first_payment_date.GroupFormat.FormatType = FormatType.DateTime
            col_first_payment_date.GroupInterval = ColumnGroupInterval.[Date]
            col_first_payment_date.Name = "col_first_payment_date"
            col_first_payment_date.ToolTip = "Date of the first payment on this debt"
            col_first_payment_date.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_last_payment_date
            '
            col_last_payment_date.AppearanceCell.Options.UseTextOptions = True
            col_last_payment_date.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_last_payment_date.AppearanceHeader.Options.UseTextOptions = True
            col_last_payment_date.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_last_payment_date.Caption = "Last Pmt Date"
            col_last_payment_date.CustomizationCaption = "Last Payment Date"
            col_last_payment_date.DisplayFormat.FormatString = "d"
            col_last_payment_date.DisplayFormat.FormatType = FormatType.DateTime
            col_last_payment_date.FieldName = "last_payment_date"
            col_last_payment_date.GroupFormat.FormatString = "d"
            col_last_payment_date.GroupFormat.FormatType = FormatType.DateTime
            col_last_payment_date.GroupInterval = ColumnGroupInterval.[Date]
            col_last_payment_date.Name = "col_last_payment_date"
            col_last_payment_date.ToolTip = "Date of the last payment on this debt"
            col_last_payment_date.Visible = True
            col_last_payment_date.VisibleIndex = 13
            col_last_payment_date.Width = 96
            col_last_payment_date.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_hold_disbursements
            '
            col_hold_disbursements.AppearanceCell.Options.UseTextOptions = True
            col_hold_disbursements.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center
            col_hold_disbursements.AppearanceHeader.Options.UseTextOptions = True
            col_hold_disbursements.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
            col_hold_disbursements.Caption = "Hold"
            col_hold_disbursements.CustomizationCaption = "Hold Disbursements"
            col_hold_disbursements.DisplayFormat.FormatString = "Y;N;N"
            col_hold_disbursements.DisplayFormat.FormatType = FormatType.Numeric
            col_hold_disbursements.FieldName = "hold_disbursements"
            col_hold_disbursements.GroupFormat.FormatString = "Y;N;N"
            col_hold_disbursements.GroupFormat.FormatType = FormatType.Numeric
            col_hold_disbursements.Name = "col_hold_disbursements"
            col_hold_disbursements.ToolTip = "Should disbursements be held?"
            col_hold_disbursements.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_irs_form_on_file
            '
            col_irs_form_on_file.AppearanceCell.Options.UseTextOptions = True
            col_irs_form_on_file.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center
            col_irs_form_on_file.AppearanceHeader.Options.UseTextOptions = True
            col_irs_form_on_file.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
            col_irs_form_on_file.Caption = "IRS Form"
            col_irs_form_on_file.CustomizationCaption = "IRS Form On File?"
            col_irs_form_on_file.DisplayFormat.FormatString = "Y;N;N"
            col_irs_form_on_file.DisplayFormat.FormatType = FormatType.Numeric
            col_irs_form_on_file.FieldName = "irs_form_on_file"
            col_irs_form_on_file.GroupFormat.FormatString = "Y;N;N"
            col_irs_form_on_file.GroupFormat.FormatType = FormatType.Numeric
            col_irs_form_on_file.Name = "col_irs_form_on_file"
            col_irs_form_on_file.ToolTip = "Do you have the IRS release form on file?"
            col_irs_form_on_file.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_send_bal_verify
            '
            col_send_bal_verify.AppearanceCell.Options.UseTextOptions = True
            col_send_bal_verify.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center
            col_send_bal_verify.AppearanceHeader.Options.UseTextOptions = True
            col_send_bal_verify.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
            col_send_bal_verify.Caption = "Bal Ver Reqd"
            col_send_bal_verify.CustomizationCaption = "Balance verification required"
            col_send_bal_verify.DisplayFormat.FormatString = "Y;N;N"
            col_send_bal_verify.DisplayFormat.FormatType = FormatType.Numeric
            col_send_bal_verify.FieldName = "send_bal_verify"
            col_send_bal_verify.GroupFormat.FormatString = "Y;N;N"
            col_send_bal_verify.GroupFormat.FormatType = FormatType.Numeric
            col_send_bal_verify.Name = "col_send_bal_verify"
            col_send_bal_verify.ToolTip = "Yes if a balance verification should be sent on this debt"
            col_send_bal_verify.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_send_drop_notice
            '
            col_send_drop_notice.AppearanceCell.Options.UseTextOptions = True
            col_send_drop_notice.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center
            col_send_drop_notice.AppearanceHeader.Options.UseTextOptions = True
            col_send_drop_notice.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
            col_send_drop_notice.Caption = "Drop Reqd"
            col_send_drop_notice.CustomizationCaption = "Drop notice is required"
            col_send_drop_notice.DisplayFormat.FormatString = "Y;N;N"
            col_send_drop_notice.DisplayFormat.FormatType = FormatType.Numeric
            col_send_drop_notice.FieldName = "send_drop_notice"
            col_send_drop_notice.GroupFormat.FormatString = "Y;N;N"
            col_send_drop_notice.GroupFormat.FormatType = FormatType.Numeric
            col_send_drop_notice.Name = "col_send_drop_notice"
            col_send_drop_notice.ToolTip = "Yes if the close event should be sent on this debt"
            col_send_drop_notice.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_student_loan_release
            '
            col_student_loan_release.AppearanceCell.Options.UseTextOptions = True
            col_student_loan_release.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center
            col_student_loan_release.AppearanceHeader.Options.UseTextOptions = True
            col_student_loan_release.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
            col_student_loan_release.Caption = "Student"
            col_student_loan_release.CustomizationCaption = "Student Loan Release on file?"
            col_student_loan_release.DisplayFormat.FormatString = "Y;N;N"
            col_student_loan_release.DisplayFormat.FormatType = FormatType.Numeric
            col_student_loan_release.FieldName = "student_loan_release"
            col_student_loan_release.GroupFormat.FormatString = "Y;N;N"
            col_student_loan_release.GroupFormat.FormatType = FormatType.Numeric
            col_student_loan_release.Name = "col_student_loan_release"
            col_student_loan_release.ToolTip = "Do you have the student loan release on file?"
            col_student_loan_release.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_ccl_zero_balance
            '
            col_ccl_zero_balance.AppearanceCell.Options.UseTextOptions = True
            col_ccl_zero_balance.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center
            col_ccl_zero_balance.AppearanceHeader.Options.UseTextOptions = True
            col_ccl_zero_balance.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
            col_ccl_zero_balance.Caption = "ZeroBal"
            col_ccl_zero_balance.CustomizationCaption = "Keep account at $0.00?"
            col_ccl_zero_balance.DisplayFormat.FormatString = "Y;N;N"
            col_ccl_zero_balance.DisplayFormat.FormatType = FormatType.Numeric
            col_ccl_zero_balance.FieldName = "ccl_zero_balance"
            col_ccl_zero_balance.GroupFormat.FormatString = "Y;N;N"
            col_ccl_zero_balance.GroupFormat.FormatType = FormatType.Numeric
            col_ccl_zero_balance.Name = "col_ccl_zero_balance"
            col_ccl_zero_balance.ToolTip = "Should the balance be kept at $0.00 on this debt?"
            col_ccl_zero_balance.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_ccl_prorate
            '
            col_ccl_prorate.AppearanceCell.Options.UseTextOptions = True
            col_ccl_prorate.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center
            col_ccl_prorate.AppearanceHeader.Options.UseTextOptions = True
            col_ccl_prorate.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
            col_ccl_prorate.Caption = "Prorate"
            col_ccl_prorate.CustomizationCaption = "Participate in Prorate function?"
            col_ccl_prorate.DisplayFormat.FormatString = "Y;N;N"
            col_ccl_prorate.DisplayFormat.FormatType = FormatType.Numeric
            col_ccl_prorate.FieldName = "ccl_prorate"
            col_ccl_prorate.GroupFormat.FormatString = "Y;N;N"
            col_ccl_prorate.GroupFormat.FormatType = FormatType.Numeric
            col_ccl_prorate.Name = "col_ccl_prorate"
            col_ccl_prorate.ToolTip = "Does the debt participate in the prorate function?"
            col_ccl_prorate.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_ccl_always_disburse
            '
            col_ccl_always_disburse.AppearanceCell.Options.UseTextOptions = True
            col_ccl_always_disburse.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center
            col_ccl_always_disburse.AppearanceHeader.Options.UseTextOptions = True
            col_ccl_always_disburse.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
            col_ccl_always_disburse.Caption = "Always Disburse"
            col_ccl_always_disburse.CustomizationCaption = "Always disburse even if paid off"
            col_ccl_always_disburse.DisplayFormat.FormatString = "Y;N;N"
            col_ccl_always_disburse.DisplayFormat.FormatType = FormatType.Numeric
            col_ccl_always_disburse.FieldName = "ccl_always_disburse"
            col_ccl_always_disburse.GroupFormat.FormatString = "Y;N;N"
            col_ccl_always_disburse.GroupFormat.FormatType = FormatType.Numeric
            col_ccl_always_disburse.Name = "col_ccl_always_disburse"
            col_ccl_always_disburse.ToolTip = "Should payments go out even if the balance is zero?"
            col_ccl_always_disburse.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_ccl_agency_account
            '
            col_ccl_agency_account.AppearanceCell.Options.UseTextOptions = True
            col_ccl_agency_account.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center
            col_ccl_agency_account.AppearanceHeader.Options.UseTextOptions = True
            col_ccl_agency_account.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
            col_ccl_agency_account.Caption = "Agency"
            col_ccl_agency_account.CustomizationCaption = "Agency Account?"
            col_ccl_agency_account.DisplayFormat.FormatString = "Y;N;N"
            col_ccl_agency_account.DisplayFormat.FormatType = FormatType.Numeric
            col_ccl_agency_account.FieldName = "ccl_agency_account"
            col_ccl_agency_account.GroupFormat.FormatString = "Y;N;N"
            col_ccl_agency_account.GroupFormat.FormatType = FormatType.Numeric
            col_ccl_agency_account.Name = "col_ccl_agency_account"
            col_ccl_agency_account.ToolTip = "Is this an account for your agency? Account numbers are the client ID."
            col_ccl_agency_account.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            'col_proposal_balance
            '
            col_proposal_balance.AppearanceCell.Options.UseTextOptions = True
            col_proposal_balance.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center
            col_proposal_balance.AppearanceHeader.Options.UseTextOptions = True
            col_proposal_balance.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
            col_proposal_balance.Caption = "Proposal Bal"
            col_proposal_balance.CustomizationCaption = "Include in proposals to others?"
            col_proposal_balance.DisplayFormat.FormatString = "Y;N;N"
            col_proposal_balance.DisplayFormat.FormatType = FormatType.Numeric
            col_proposal_balance.FieldName = "proposal_balance"
            col_proposal_balance.GroupFormat.FormatString = "Y;N;N"
            col_proposal_balance.GroupFormat.FormatType = FormatType.Numeric
            col_proposal_balance.Name = "col_proposal_balance"
            col_proposal_balance.ToolTip = "Should this debt be included in proposals to other creditors?"
            col_proposal_balance.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            '[col_cr_creditor_name]
            '
            col_cr_creditor_name.Caption = "Creditor Name"
            col_cr_creditor_name.CustomizationCaption = "[DO NOT USE]Creditor Name"
            col_cr_creditor_name.FieldName = "cr_creditor_name"
            col_cr_creditor_name.Name = "col_cr_creditor_name"
            col_cr_creditor_name.ToolTip = "Name from coded creditor ID. Blank otherwise."
            col_cr_creditor_name.OptionsColumn.AllowGroup = DefaultBoolean.True
            col_cr_creditor_name.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            '[col_creditor]
            '
            col_creditor.Caption = "Creditor"
            col_creditor.CustomizationCaption = "[DO NOT USE]Creditor"
            col_creditor.FieldName = "creditor"
            col_creditor.Name = "col_creditor"
            col_creditor.ToolTip = "Coded creditor ID"
            col_creditor.OptionsColumn.AllowGroup = DefaultBoolean.True
            col_creditor.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            '[col_account_number]
            '
            col_account_number.Caption = "Acct#"
            col_account_number.CustomizationCaption = "[DO NOT USE]Account Number"
            col_account_number.FieldName = "account_number"
            col_account_number.Name = "col_account_number"
            col_account_number.ToolTip = "un-modified account number"
            col_account_number.OptionsColumn.AllowGroup = DefaultBoolean.True
            col_account_number.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            '[col_creditor_name]
            '
            col_creditor_name.Caption = "Creditor Name"
            col_creditor_name.CustomizationCaption = "[DO NOT USE]Uncoded Creditor Name"
            col_creditor_name.FieldName = "creditor_name"
            col_creditor_name.Name = "col_creditor_name"
            col_creditor_name.ToolTip = "un-coded creditor name. Blank for coded items."
            col_creditor_name.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            '[col_adjusted_orig_balance]
            '
            col_adjusted_orig_balance.AppearanceCell.Options.UseTextOptions = True
            col_adjusted_orig_balance.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_adjusted_orig_balance.AppearanceHeader.Options.UseTextOptions = True
            col_adjusted_orig_balance.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_adjusted_orig_balance.Caption = "Orig Bal"
            col_adjusted_orig_balance.CustomizationCaption = "[DO NOT USE]Adjusted Orig Balance"
            col_adjusted_orig_balance.DisplayFormat.FormatType = FormatType.Numeric
            col_adjusted_orig_balance.FieldName = "adjusted_orig_balance"
            col_adjusted_orig_balance.GroupFormat.FormatType = FormatType.Numeric
            col_adjusted_orig_balance.GroupInterval = ColumnGroupInterval.Value
            col_adjusted_orig_balance.Name = "col_adjusted_orig_balance"
            col_adjusted_orig_balance.SortMode = ColumnSortMode.Value
            col_adjusted_orig_balance.SummaryItem.SummaryType = SummaryItemType.Sum
            col_adjusted_orig_balance.ToolTip = "Original balance plus adjustment amount."
            col_adjusted_orig_balance.DisplayFormat.FormatString = "{0:c}"
            col_adjusted_orig_balance.GroupFormat.FormatString = "{0:c}"
            col_adjusted_orig_balance.SummaryItem.DisplayFormat = "{0:c}"
            col_adjusted_orig_balance.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            '[col_current_balance]
            '
            col_current_balance.AppearanceCell.Options.UseTextOptions = True
            col_current_balance.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_current_balance.AppearanceHeader.Options.UseTextOptions = True
            col_current_balance.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_current_balance.Caption = "Balance"
            col_current_balance.CustomizationCaption = "[DO NOT USE]Current Balance"
            col_current_balance.DisplayFormat.FormatType = FormatType.Numeric
            col_current_balance.FieldName = "current_balance"
            col_current_balance.GroupFormat.FormatType = FormatType.Numeric
            col_current_balance.GroupInterval = ColumnGroupInterval.Value
            col_current_balance.Name = "col_current_balance"
            col_current_balance.SummaryItem.SummaryType = SummaryItemType.Sum
            col_current_balance.ToolTip = """real"" current balance. Maybe inflated for reassigned debts."
            col_current_balance.DisplayFormat.FormatString = "{0:c}"
            col_current_balance.GroupFormat.FormatString = "{0:c}"
            col_current_balance.SummaryItem.DisplayFormat = "{0:c}"
            col_current_balance.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            '[col_disbursement_factor]
            '
            col_disbursement_factor.AppearanceCell.Options.UseTextOptions = True
            col_disbursement_factor.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_disbursement_factor.AppearanceHeader.Options.UseTextOptions = True
            col_disbursement_factor.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_disbursement_factor.Caption = "Disb Factor"
            col_disbursement_factor.CustomizationCaption = "[DO NOT USE]Disbursement Factor"
            col_disbursement_factor.DisplayFormat.FormatType = FormatType.Numeric
            col_disbursement_factor.FieldName = "disbursement_factor"
            col_disbursement_factor.GroupFormat.FormatType = FormatType.Numeric
            col_disbursement_factor.GroupInterval = ColumnGroupInterval.Value
            col_disbursement_factor.Name = "col_disbursement_factor"
            col_disbursement_factor.SummaryItem.SummaryType = SummaryItemType.Sum
            col_disbursement_factor.ToolTip = "Current disbursement factor."
            col_disbursement_factor.DisplayFormat.FormatString = "{0:c}"
            col_disbursement_factor.GroupFormat.FormatString = "{0:c}"
            col_disbursement_factor.SummaryItem.DisplayFormat = "{0:c}"
            col_disbursement_factor.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            '[col_total_interest]
            '
            col_total_interest.AppearanceCell.Options.UseTextOptions = True
            col_total_interest.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_total_interest.AppearanceHeader.Options.UseTextOptions = True
            col_total_interest.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_total_interest.Caption = "Total Interest"
            col_total_interest.CustomizationCaption = "[DO NOT USE]Total Interest Amt"
            col_total_interest.DisplayFormat.FormatType = FormatType.Numeric
            col_total_interest.FieldName = "total_interest"
            col_total_interest.GroupFormat.FormatType = FormatType.Numeric
            col_total_interest.GroupInterval = ColumnGroupInterval.Value
            col_total_interest.Name = "col_total_interest"
            col_total_interest.SummaryItem.SummaryType = SummaryItemType.Sum
            col_total_interest.ToolTip = "Total interest charged on this debt. Maybe inflated for reassigned debts."
            col_total_interest.DisplayFormat.FormatString = "{0:c}"
            col_total_interest.GroupFormat.FormatString = "{0:c}"
            col_total_interest.SummaryItem.DisplayFormat = "{0:c}"
            col_total_interest.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            '[col_total_payments]
            '
            col_total_payments.AppearanceCell.Options.UseTextOptions = True
            col_total_payments.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_total_payments.AppearanceHeader.Options.UseTextOptions = True
            col_total_payments.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_total_payments.Caption = "Payments"
            col_total_payments.CustomizationCaption = "[DO NOT USE]Total Payments Amount"
            col_total_payments.DisplayFormat.FormatType = FormatType.Numeric
            col_total_payments.FieldName = "total_payments"
            col_total_payments.GroupFormat.FormatType = FormatType.Numeric
            col_total_payments.GroupInterval = ColumnGroupInterval.Value
            col_total_payments.Name = "col_total_payments"
            col_total_payments.SummaryItem.SummaryType = SummaryItemType.Sum
            col_total_payments.ToolTip = "Total net payments. Maybe inflated for reassigned debts."
            col_total_payments.Width = 39
            col_total_payments.DisplayFormat.FormatString = "{0:c}"
            col_total_payments.GroupFormat.FormatString = "{0:c}"
            col_total_payments.SummaryItem.DisplayFormat = "{0:c}"
            col_total_payments.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            '[col_orig_balance]
            '
            col_orig_balance.AppearanceCell.Options.UseTextOptions = True
            col_orig_balance.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_orig_balance.AppearanceHeader.Options.UseTextOptions = True
            col_orig_balance.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_orig_balance.Caption = "Orig Bal"
            col_orig_balance.CustomizationCaption = "[DO NOT USE]Original Balance"
            col_orig_balance.DisplayFormat.FormatType = FormatType.Numeric
            col_orig_balance.FieldName = "orig_balance"
            col_orig_balance.GroupFormat.FormatType = FormatType.Numeric
            col_orig_balance.GroupInterval = ColumnGroupInterval.Value
            col_orig_balance.Name = "col_orig_balance"
            col_orig_balance.SummaryItem.SummaryType = SummaryItemType.Sum
            col_orig_balance.ToolTip = "Original balance from the client."
            col_orig_balance.Width = 52
            col_orig_balance.DisplayFormat.FormatString = "{0:c}"
            col_orig_balance.GroupFormat.FormatString = "{0:c}"
            col_orig_balance.SummaryItem.DisplayFormat = "{0:c}"
            col_orig_balance.OptionsColumn.AllowSort = DefaultBoolean.True
            '
            '[col_dmp_interest]
            '
            col_dmp_interest.AppearanceCell.Options.UseTextOptions = True
            col_dmp_interest.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Far
            col_dmp_interest.AppearanceHeader.Options.UseTextOptions = True
            col_dmp_interest.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Far
            col_dmp_interest.Caption = "Interest"
            col_dmp_interest.CustomizationCaption = "[DO NOT USE]Interest rate used for plan"
            col_dmp_interest.DisplayFormat.FormatString = "p3"
            col_dmp_interest.DisplayFormat.FormatType = FormatType.Numeric
            col_dmp_interest.FieldName = "dmp_interest"
            col_dmp_interest.GroupFormat.FormatString = "p3"
            col_dmp_interest.GroupFormat.FormatType = FormatType.Numeric
            col_dmp_interest.Name = "col_dmp_interest"
            col_dmp_interest.ToolTip = "DMP interest rate override."
            col_dmp_interest.Visible = False
            col_dmp_interest.VisibleIndex = -1
            col_dmp_interest.Width = 37
            col_dmp_interest.OptionsColumn.AllowSort = DefaultBoolean.True

            Dim StyleFormatCondition2 As New StyleFormatCondition
            StyleFormatCondition2.Appearance.BackColor = Color.FromArgb(CType(CType(128, Byte), Int32), CType(CType(255, Byte), Int32), CType(CType(255, Byte), Int32))
            StyleFormatCondition2.Appearance.BackColor2 = Color.FromArgb(CType(CType(128, Byte), Int32), CType(CType(255, Byte), Int32), CType(CType(255, Byte), Int32))
            StyleFormatCondition2.Appearance.BorderColor = Color.FromArgb(CType(CType(128, Byte), Int32), CType(CType(255, Byte), Int32), CType(CType(255, Byte), Int32))
            StyleFormatCondition2.Appearance.Options.UseBackColor = True
            StyleFormatCondition2.Appearance.Options.UseBorderColor = True
            StyleFormatCondition2.ApplyToRow = True
            StyleFormatCondition2.Column = col_ActiveFlag
            StyleFormatCondition2.Condition = FormatConditionEnum.Equal
            StyleFormatCondition2.Value1 = False

            Dim StyleFormatCondition1 As New StyleFormatCondition
            StyleFormatCondition1.Appearance.BackColor = Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(192, Byte), Int32), CType(CType(192, Byte), Int32))
            StyleFormatCondition1.Appearance.BackColor2 = Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(192, Byte), Int32), CType(CType(192, Byte), Int32))
            StyleFormatCondition1.Appearance.BorderColor = Color.FromArgb(CType(CType(255, Byte), Int32), CType(CType(192, Byte), Int32), CType(CType(192, Byte), Int32))
            StyleFormatCondition1.Appearance.Options.UseBackColor = True
            StyleFormatCondition1.Appearance.Options.UseBorderColor = True
            StyleFormatCondition1.ApplyToRow = True
            StyleFormatCondition1.Column = col_hold_disbursements
            StyleFormatCondition1.Condition = FormatConditionEnum.NotEqual
            StyleFormatCondition1.Value1 = False

            ' Define the grid view
            GridView1.BeginInit()
            BeginInit()
            Try
                GridView1.FormatConditions.Clear()
                GridView1.Columns.Clear()
                GridView1.SortInfo.Clear()

                ' Set the formatting conditions
                GridView1.FormatConditions.Add(StyleFormatCondition1)
                GridView1.FormatConditions.Add(StyleFormatCondition2)

                ' Add the columns to the grid
                GridView1.Columns.AddRange(New GridColumn() { _
                            [col_dmp_interest],
                            [col_creditor],
                            [col_account_number],
                            [col_cr_creditor_name],
                            [col_creditor_name],
                            [col_adjusted_orig_balance],
                            [col_current_balance],
                            [col_disbursement_factor],
                            [col_total_interest],
                            [col_total_payments],
                            [col_orig_balance],
                            col_line_number,
                            col_display_debt_id,
                            col_display_creditor_name,
                            col_display_account_number,
                            col_display_orig_balance,
                            col_orig_dmp_payment,
                            col_display_disbursement_factor,
                            col_payments_month_1,
                            col_payments_month_0,
                            col_display_current_balance,
                            col_display_dmp_interest,
                            col_display_total_payments,
                            col_display_verified_status,
                            col_last_payment_date,
                            col_last_payment_amt,
                            col_display_payments_this_creditor,
                            col_contact_name,
                            col_created_by,
                            col_last_communication,
                            col_client_name,
                            col_last_payment_type,
                            col_first_payment_type,
                            col_balance_verify_by,
                            col_message,
                            col_last_stmt_balance,
                            col_non_dmp_payment,
                            col_first_payment_amt,
                            col_display_total_interest,
                            col_priority,
                            col_orig_balance_adjustment,
                            col_payments_this_creditor,
                            col_interest_this_creditor,
                            col_sched_payment,
                            col_current_sched_payment,
                            col_total_sched_payment,
                            col_returns_this_creditor,
                            col_check_payments,
                            col_client_creditor,
                            col_client_creditor_balance,
                            col_client_creditor_proposal,
                            col_first_payment,
                            col_last_payment,
                            col_person,
                            col_balance_client_creditor,
                            col_payment_rpps_mask,
                            col_Drop_Reason,
                            col_percent_balance,
                            col_rpps_mask,
                            col_terms,
                            col_months_delinquent,
                            col_dmp_payout_interest,
                            col_fairshare_pct_check,
                            col_non_dmp_interest,
                            col_fairshare_pct_eft,
                            col_prenote_date,
                            col_drop_date,
                            col_Drop_Reason_sent,
                            col_date_created,
                            col_Drop_Reason_date,
                            col_date_disp_changed,
                            col_last_payment_date_b4_dmp,
                            col_expected_payout_date,
                            col_start_date,
                            col_last_stmt_date,
                            col_verify_request_date,
                            col_balance_verify_date,
                            col_first_payment_date,
                            col_hold_disbursements,
                            col_irs_form_on_file,
                            col_ActiveFlag,
                            col_send_bal_verify,
                            col_send_drop_notice,
                            col_student_loan_release,
                            col_ccl_zero_balance,
                            col_ccl_prorate,
                            col_ccl_always_disburse,
                            col_ccl_agency_account,
                            col_proposal_balance
                            })

                GridView1.SortInfo.Add(col_display_debt_id, ColumnSortOrder.Ascending)
                GridView1.SortInfo.Add(col_display_creditor_name, ColumnSortOrder.Ascending)
                GridView1.SortInfo.Add(col_display_account_number, ColumnSortOrder.Ascending)

                Dim pathName As String = XMLBasePath()
                If pathName <> String.Empty Then
                    Dim fileName As String = Path.Combine(pathName, "Page_Debts1.Grid.xml")

                    Try
                        If File.Exists(fileName) Then
                            GridView1.RestoreLayoutFromXml(fileName)
                        End If
                    Catch ex As Exception   ' Don't die because of an XML error in the restore
                    End Try                 ' or because there was a disk error in the file system. Just ignore them.

                    ' Ensure that the flags are properly set even if they were saved differently.
                    GridView1.OptionsBehavior.Editable = False
                    GridView1.OptionsBehavior.AutoUpdateTotalSummary = True
                    GridView1.OptionsBehavior.AutoPopulateColumns = False
                    GridView1.OptionsBehavior.AllowAddRows = DefaultBoolean.False
                    GridView1.OptionsBehavior.AllowDeleteRows = DefaultBoolean.False

                    ' Ensure that some of the view states are properly set as well.
                    GridView1.OptionsView.ShowColumnHeaders = True
                    GridView1.OptionsView.ShowFooter = True
                    GridView1.OptionsView.ShowGroupPanel = False
                    GridView1.OptionsView.ShowIndicator = False
                    GridView1.OptionsView.ShowPreview = False
                    GridView1.OptionsView.ShowGroupExpandCollapseButtons = False
                End If

            Finally
                GridView1.EndInit()
                EndInit()
            End Try
        End Sub

#End Region
    End Class
End Namespace