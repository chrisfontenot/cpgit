Imports DebtPlus.UI.Client.controls

Namespace pages

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Page_SecuredProperty

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing Then
                    If components IsNot Nothing Then components.Dispose()
                    ' If bc IsNot Nothing Then bc.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.GridColumn_Type = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_description = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_value = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_balance = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_secured_property = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_year_acquired = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_year_mfg = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_original_price = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_secured_type_grouping = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_primary_residence = New DevExpress.XtraGrid.Columns.GridColumn()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PopupMenu_Items, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridControl1
            '
            Me.GridControl1.Size = New System.Drawing.Size(470, 294)
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(195, Byte), Integer))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(189, Byte), Integer))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(206, Byte), Integer))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(225, Byte), Integer))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(215, Byte), Integer))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(215, Byte), Integer))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(215, Byte), Integer))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_Type, Me.GridColumn_description, Me.GridColumn_value, Me.GridColumn_balance, Me.GridColumn_secured_property, Me.GridColumn_year_acquired, Me.GridColumn_year_mfg, Me.GridColumn_original_price, Me.GridColumn_secured_type_grouping, Me.GridColumn_primary_residence})
            Me.GridView1.OptionsBehavior.AutoPopulateColumns = False
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsDetail.EnableMasterViewMode = False
            Me.GridView1.OptionsLayout.Columns.StoreAllOptions = True
            Me.GridView1.OptionsLayout.LayoutVersion = "1"
            Me.GridView1.OptionsLayout.StoreAllOptions = True
            Me.GridView1.OptionsLayout.StoreDataSettings = False
            Me.GridView1.OptionsView.EnableAppearanceEvenRow = True
            Me.GridView1.OptionsView.EnableAppearanceOddRow = True
            Me.GridView1.OptionsView.ShowFooter = True
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridView1.OptionsView.ShowIndicator = False
            '
            'barDockControlTop
            '
            Me.barDockControlTop.Size = New System.Drawing.Size(470, 0)
            '
            'barDockControlBottom
            '
            Me.barDockControlBottom.Location = New System.Drawing.Point(0, 294)
            Me.barDockControlBottom.Size = New System.Drawing.Size(470, 0)
            '
            'barDockControlLeft
            '
            Me.barDockControlLeft.Size = New System.Drawing.Size(0, 294)
            '
            'barDockControlRight
            '
            Me.barDockControlRight.Location = New System.Drawing.Point(470, 0)
            Me.barDockControlRight.Size = New System.Drawing.Size(0, 294)
            '
            'GridColumn_Type
            '
            Me.GridColumn_Type.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_Type.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_Type.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_Type.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_Type.Caption = "Type"
            Me.GridColumn_Type.CustomizationCaption = "Type Description"
            Me.GridColumn_Type.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_Type.FieldName = "secured_type"
            Me.GridColumn_Type.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_Type.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DisplayText
            Me.GridColumn_Type.Name = "GridColumn_Type"
            Me.GridColumn_Type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_Type.Visible = True
            Me.GridColumn_Type.VisibleIndex = 0
            Me.GridColumn_Type.Width = 99
            '
            'GridColumn_description
            '
            Me.GridColumn_description.Caption = "Description"
            Me.GridColumn_description.CustomizationCaption = "Description"
            Me.GridColumn_description.FieldName = "description"
            Me.GridColumn_description.Name = "GridColumn_description"
            Me.GridColumn_description.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_description.Visible = True
            Me.GridColumn_description.VisibleIndex = 1
            Me.GridColumn_description.Width = 157
            '
            'GridColumn_value
            '
            Me.GridColumn_value.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_value.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_value.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_value.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_value.Caption = "Value"
            Me.GridColumn_value.CustomizationCaption = "Value"
            Me.GridColumn_value.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_value.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_value.FieldName = "current_value"
            Me.GridColumn_value.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_value.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_value.Name = "GridColumn_value"
            Me.GridColumn_value.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_value.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "current_value", "{0:c}")})
            Me.GridColumn_value.Visible = True
            Me.GridColumn_value.VisibleIndex = 2
            Me.GridColumn_value.Width = 69
            '
            'GridColumn_balance
            '
            Me.GridColumn_balance.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_balance.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_balance.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_balance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_balance.Caption = "Balance"
            Me.GridColumn_balance.CustomizationCaption = "Loan Balance"
            Me.GridColumn_balance.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_balance.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_balance.FieldName = "balance"
            Me.GridColumn_balance.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_balance.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_balance.Name = "GridColumn_balance"
            Me.GridColumn_balance.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_balance.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "balance", "{0:c}")})
            Me.GridColumn_balance.Visible = True
            Me.GridColumn_balance.VisibleIndex = 3
            Me.GridColumn_balance.Width = 71
            '
            'GridColumn_secured_property
            '
            Me.GridColumn_secured_property.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_secured_property.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_secured_property.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_secured_property.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_secured_property.Caption = "ID"
            Me.GridColumn_secured_property.CustomizationCaption = "Property ID"
            Me.GridColumn_secured_property.DisplayFormat.FormatString = "{0:f0}"
            Me.GridColumn_secured_property.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_secured_property.FieldName = "secured_property"
            Me.GridColumn_secured_property.GroupFormat.FormatString = "{0:f0}"
            Me.GridColumn_secured_property.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_secured_property.Name = "GridColumn_secured_property"
            Me.GridColumn_secured_property.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_secured_property.Width = 71
            '
            'GridColumn_year_acquired
            '
            Me.GridColumn_year_acquired.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_year_acquired.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_year_acquired.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_year_acquired.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_year_acquired.Caption = "Acquired"
            Me.GridColumn_year_acquired.CustomizationCaption = "Year Acquired"
            Me.GridColumn_year_acquired.DisplayFormat.FormatString = "{0:f0}"
            Me.GridColumn_year_acquired.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_year_acquired.FieldName = "year_acquired"
            Me.GridColumn_year_acquired.GroupFormat.FormatString = "{0:f0}"
            Me.GridColumn_year_acquired.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_year_acquired.Name = "GridColumn_year_acquired"
            Me.GridColumn_year_acquired.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_year_acquired.Width = 71
            '
            'GridColumn_year_mfg
            '
            Me.GridColumn_year_mfg.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_year_mfg.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_year_mfg.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_year_mfg.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_year_mfg.Caption = "Built"
            Me.GridColumn_year_mfg.CustomizationCaption = "Year manufactured"
            Me.GridColumn_year_mfg.DisplayFormat.FormatString = "{0:f0}"
            Me.GridColumn_year_mfg.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_year_mfg.FieldName = "year_mfg"
            Me.GridColumn_year_mfg.GroupFormat.FormatString = "{0:f0}"
            Me.GridColumn_year_mfg.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_year_mfg.Name = "GridColumn_year_mfg"
            Me.GridColumn_year_mfg.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_year_mfg.Width = 71
            '
            'GridColumn_original_price
            '
            Me.GridColumn_original_price.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_original_price.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_original_price.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_original_price.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_original_price.Caption = "Price"
            Me.GridColumn_original_price.CustomizationCaption = "Original Purchase Price"
            Me.GridColumn_original_price.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_original_price.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_original_price.FieldName = "original_price"
            Me.GridColumn_original_price.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_original_price.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_original_price.Name = "GridColumn_original_price"
            Me.GridColumn_original_price.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_original_price.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "original_price", "{0:c}")})
            Me.GridColumn_original_price.Width = 71
            '
            'GridColumn_secured_type_grouping
            '
            Me.GridColumn_secured_type_grouping.Caption = "Group"
            Me.GridColumn_secured_type_grouping.CustomizationCaption = "Group Name"
            Me.GridColumn_secured_type_grouping.FieldName = "secured_type_grouping"
            Me.GridColumn_secured_type_grouping.Name = "GridColumn_secured_type_grouping"
            Me.GridColumn_secured_type_grouping.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_primary_residence
            '
            Me.GridColumn_primary_residence.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_primary_residence.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_primary_residence.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_primary_residence.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.GridColumn_primary_residence.Caption = "Residence"
            Me.GridColumn_primary_residence.CustomizationCaption = "Primary Residence (Y/N)"
            Me.GridColumn_primary_residence.FieldName = "primary_residence"
            Me.GridColumn_primary_residence.Name = "GridColumn_primary_residence"
            Me.GridColumn_primary_residence.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_primary_residence.Width = 25
            '
            'Page_SecuredProperty
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Name = "Page_SecuredProperty"
            Me.Size = New System.Drawing.Size(470, 294)
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PopupMenu_Items, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents GridColumn_Type As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_description As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_value As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_balance As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_secured_property As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_year_acquired As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_year_mfg As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_original_price As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_secured_type_grouping As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_primary_residence As DevExpress.XtraGrid.Columns.GridColumn
    End Class
End Namespace
