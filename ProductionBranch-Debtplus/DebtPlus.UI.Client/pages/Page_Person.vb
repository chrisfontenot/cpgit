#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict Off

Imports System.ComponentModel
Imports DebtPlus.Svc.DataSets
Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.Data.Controls
Imports DebtPlus.UI.Client.controls
Imports System.Data.SqlClient
Imports DebtPlus.UI.Client.Service
Imports DevExpress.XtraEditors
Imports DebtPlus.Utils.Format
Imports DebtPlus.LINQ
Imports System.Linq

Namespace pages

    Friend Class Page_Person
        Inherits ControlBaseClient

        Private Class FICOReasons
            Public Sub New()
            End Sub

            Public Sub New(ByVal ctl As DevExpress.XtraEditors.CheckedListBoxControl)
                Me.New()
                Me.ctl = ctl
            End Sub

            Private peopleRecord As DebtPlus.LINQ.people = Nothing
            Private ctl As DevExpress.XtraEditors.CheckedListBoxControl = Nothing

            ''' <summary>
            ''' Read the list of checked items for the FICO score reasons
            ''' </summary>
            ''' <param name="peopleRecord"></param>
            ''' <remarks></remarks>
            Public Sub ReadForm(ByVal peopleRecord As DebtPlus.LINQ.people)
                Me.peopleRecord = peopleRecord

                ' Suppress the sorting for a moment while it is being loaded.
                ctl.SortOrder = Windows.Forms.SortOrder.None

                ' Read the list of valid items from the cache
                Dim colItems As System.Collections.Generic.List(Of DebtPlus.LINQ.FICOScoreReasonCode) = DebtPlus.LINQ.Cache.FICOScoreReasonCode.getList().FindAll(Function(s) s.ActiveFlag)
                For Each item As DebtPlus.LINQ.FICOScoreReasonCode In colItems
                    Dim checkedItem As New DebtPlus.Data.Controls.SortedCheckedListboxControlItem(item, item.description, CheckState.Unchecked)
                    ctl.Items.Add(checkedItem)
                Next

                ' Sort the list
                ctl.SortOrder = Windows.Forms.SortOrder.Ascending
                ctl.SelectedIndex = 0

                ' Check the items in the people record for this collection
                For Each item As DebtPlus.LINQ.PeopleFICOScoreReason In peopleRecord.PeopleFICOScoreReasons
                    Dim listItem As DebtPlus.Data.Controls.SortedCheckedListboxControlItem = FindListBoxItem(item.ScoreReasonID)
                    If listItem IsNot Nothing Then
                        listItem.CheckState = CheckState.Checked
                    End If
                Next
            End Sub

            ''' <summary>
            ''' Save the form information to reflect the changes to the FICO reason list
            ''' </summary>
            ''' <remarks></remarks>
            Public Sub SaveForm()
                For Each checkItem As DebtPlus.Data.Controls.SortedCheckedListboxControlItem In ctl.Items

                    ' Find the master record in the list
                    Dim masterRecord As DebtPlus.LINQ.FICOScoreReasonCode = TryCast(checkItem.Value, DebtPlus.LINQ.FICOScoreReasonCode)
                    If masterRecord Is Nothing Then
                        Continue For
                    End If

                    ' Determine if the record exists in our person list.
                    Dim qPeopleReason As DebtPlus.LINQ.PeopleFICOScoreReason = peopleRecord.PeopleFICOScoreReasons.Where(Function(s) s.ScoreReasonID = masterRecord.Id).FirstOrDefault()

                    ' If the record is not checked then remove a found item
                    If checkItem.CheckState <> CheckState.Checked Then
                        If qPeopleReason IsNot Nothing Then
                            peopleRecord.PeopleFICOScoreReasons.Remove(qPeopleReason)
                        End If
                        Continue For
                    End If

                    ' If the record is missing then add it
                    If qPeopleReason Is Nothing Then
                        qPeopleReason = DebtPlus.LINQ.Factory.Manufacture_PeopleFICOScoreReason()
                        qPeopleReason.ScoreReasonID = masterRecord.Id
                        peopleRecord.PeopleFICOScoreReasons.Add(qPeopleReason)
                    End If
                Next
            End Sub

            Private Function FindListBoxItem(ScoreReasonID As Int32) As DebtPlus.Data.Controls.SortedCheckedListboxControlItem
                For Each searchItem As DebtPlus.Data.Controls.SortedCheckedListboxControlItem In ctl.Items
                    Dim record As DebtPlus.LINQ.FICOScoreReasonCode = TryCast(searchItem.Value, DebtPlus.LINQ.FICOScoreReasonCode)
                    If record.Id = ScoreReasonID Then
                        Return searchItem
                    End If
                Next
                Return Nothing
            End Function
        End Class

        Private bc As BusinessContext = Nothing
        Private FicoReasonClass As FICOReasons = Nothing

        ''' <summary>
        ''' Test the SSN field to determine if there is a value in it.
        ''' </summary>
        Private ReadOnly Property EmptySSN() As Boolean
            Get
                If personRecord IsNot Nothing Then
                    If String.IsNullOrWhiteSpace(personRecord.SSN) OrElse personRecord.SSN.Length < 4 OrElse personRecord.SSN = New String("0"c, personRecord.SSN.Length) Then
                        Return True
                    End If
                End If

                Return False
            End Get
        End Property

        ''' <summary>
        ''' Cached copy of the AllowSSN value
        ''' </summary>
        Private Shared privateAllowSSN As System.Nullable(Of Boolean) = Nothing

        ''' <summary>
        ''' Should the counselor be allowed to view the SSN field or not?
        ''' </summary>
        Private ReadOnly Property AllowSSN As Boolean
            Get
                If Not privateAllowSSN.HasValue Then
                    privateAllowSSN = IsAdmin("Client SSN")
                End If

                Return privateAllowSSN.Value
            End Get
        End Property

        ''' <summary>
        ''' Determine if the current counselor has the proper attribute to update the SSN field.
        ''' </summary>
        Private Function IsAdmin(ByVal KeyName As String) As Boolean

            ' Find the ID of the desired attribute code. We know the name, not the ID.
            Dim adminType As AttributeType = DebtPlus.LINQ.Cache.AttributeType.getList().Find(Function(s) s.Grouping Is Nothing AndAlso String.Compare(s.Attribute, KeyName, True) = 0)
            If adminType Is Nothing Then
                Return False
            End If

            ' Find the counselor id associated with the current person.
            Dim co As DebtPlus.LINQ.counselor = DebtPlus.LINQ.Cache.counselor.getList().Find(Function(s) String.Compare(s.Person, DebtPlus.LINQ.BusinessContext.suser_sname(), True) = 0)
            If co Is Nothing Then
                Return False
            End If

            ' Look for the corresponding counselor attribute in the attributes list for the counselor
            Dim id As counselor_attribute = bc.counselor_attributes.Where(Function(s) s.Attribute = adminType.Id AndAlso s.Counselor = co.Id).FirstOrDefault()
            If id IsNot Nothing Then
                Return True
            End If

            Return False
        End Function

        ''' <summary>
        ''' Event tripped when the bottom line form information should be changed.
        ''' </summary>
        ''' <remarks></remarks>
        Public Event Changed As System.EventHandler(Of System.EventArgs)
        Protected Sub RaiseChanged(e As System.EventArgs)
            RaiseEvent Changed(Me, e)
        End Sub
        Protected Overridable Sub OnChanged(e As System.EventArgs)
            RaiseChanged(e)
        End Sub

        ' Current record being edited.
        Private personRecord As DebtPlus.LINQ.people = Nothing
        Private _IsApplicant As Boolean = True

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        ''' <summary>
        ''' Initialize the page the first time that it is called.
        ''' </summary>
        Public Overrides Sub LoadForm(bc As LINQ.BusinessContext)
            MyBase.LoadForm(bc)
            Me.bc = bc

            UnRegisterHandlers()
            Try
                gender.Properties.DataSource = DebtPlus.LINQ.Cache.GenderType.getList()
                race.Properties.DataSource = DebtPlus.LINQ.Cache.RaceType.getList()
                education.Properties.DataSource = DebtPlus.LINQ.Cache.EducationType.getList()
                LookupEdit_Ethnicity.Properties.DataSource = DebtPlus.LINQ.Cache.EthnicityType.getList()
                milDependent.Properties.DataSource = DebtPlus.LINQ.Cache.MilitaryDependentType.getList()
                milGrade.Properties.DataSource = DebtPlus.LINQ.Cache.MilitaryGradeType.getList()
                milService.Properties.DataSource = DebtPlus.LINQ.Cache.MilitaryServiceType.getList()
                milStatus.Properties.DataSource = DebtPlus.LINQ.Cache.MilitaryStatusType.getList()
                LookUpEdit_CreditAgency.Properties.DataSource = DebtPlus.LINQ.InMemory.CreditAgencyTypes.getList()
                no_fico_score_reason.Properties.DataSource = DebtPlus.LINQ.Cache.Housing_FICONotIncludedReason.getList()
                luEverFiledBKChapter.Properties.DataSource = DebtPlus.LINQ.Cache.BankruptcyClassType.getList()
                luPmtCurrent.Properties.DataSource = DebtPlus.LINQ.InMemory.YesNos.getList()

            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Record a system note when the name is changed
        ''' </summary>
        Private Sub NameRecordControl1_Changed(ByVal Sender As Object, ByVal e As DebtPlus.Events.NameChangeEventArgs)

            ' Update the cache entry with the new name values.
            Dim nameRecord As DebtPlus.LINQ.Name = Context.ClientDs.GetNameByID(e.NewName.Id)
            If nameRecord IsNot Nothing Then
                nameRecord.Prefix = e.NewName.Prefix
                nameRecord.First = e.NewName.First
                nameRecord.Middle = e.NewName.Middle
                nameRecord.Last = e.NewName.Last
                nameRecord.Suffix = e.NewName.Suffix
            End If

            Dim NoteText As String = String.Format("Name Changed from ""{0}"" to ""{1}""", e.OldName.ToString(), e.NewName.ToString())
            RecordPersonSystemNote("Changed {0} Name", NoteText)
        End Sub

        ''' <summary>
        ''' Handle the dynamic change in the text for the client. Pass it along to the status bar routine.
        ''' </summary>
        Private Sub NameRecordControl1_TextChanged(sender As Object, e As System.EventArgs)

            ' Pass along the update event to the status bar when the name is changed on the applicant tab
            If IsApplicant Then
                Dim q As DebtPlus.LINQ.Name = NameRecordControl1.GetCurrentValues()
                Context.ClientDs.EnvokeHandlers(Me, New DebtPlus.Events.FieldChangedEventArgs("form.Name", q.ToString()))
            End If

        End Sub

        ''' <summary>
        ''' Record a system note when the email address is changed
        ''' </summary>
        Private Sub EmailRecordControl1_Changed(ByVal Sender As Object, ByVal e As DebtPlus.Events.EmailChangedEventArgs)
            RecordPersonSystemNote("Changed {0} Email", String.Format("Changed email address from ""{0}"" to ""{1}""", e.OldEmail.ToString(), e.NewEmail.ToString()))
        End Sub

        ''' <summary>
        ''' Record a system note when the job information is changed
        ''' </summary>
        Private Sub Job_Name_RecordSystemNote(ByVal Sender As Object, ByVal e As JobControl.RecordSystemNoteArgs)
            RecordPersonSystemNote(e.Subject, e.Note)
        End Sub

        ''' <summary>
        ''' Write applicant notes as needed
        ''' </summary>
        Public Sub RecordPersonSystemNote(ByVal Subject As String, ByVal Note As String)
            Dim ApplicantString As String = If(IsApplicant, "Applicant", "Co-Applicant")
            RecordClientSystemNote(bc, String.Format(Subject, ApplicantString), Note)
        End Sub

        ''' <summary>
        ''' Write client notes as needed
        ''' </summary>
        Public Sub RecordClientSystemNote(bc As BusinessContext, ByVal Subject As String, ByVal Note As String)
            Try
                Dim n As DebtPlus.LINQ.client_note = DebtPlus.LINQ.Factory.Manufacture_client_note(Context.ClientDs.ClientId, 3)
                n.note = Note

                ' Make sure that the subject is not too long for the note.
                If Subject.Length > 80 Then
                    n.subject = Subject.Substring(0, 77) + "..."
                Else
                    n.subject = Subject
                End If

                bc.client_notes.InsertOnSubmit(n)
                bc.SubmitChanges()

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error writing system note")
            End Try
        End Sub

        ''' <summary>
        ''' Register the event handlers
        ''' </summary>
        Private Sub RegisterHandlers()
            AddHandler birthdate.Validating, AddressOf birthdate_Validating
            AddHandler CheckEdit_ViewSSN.CheckStateChanged, AddressOf CheckEdit_ViewSSN_CheckStateChanged
            AddHandler dtEverFiledBKFiledDate.EditValueChanged, AddressOf dtEverFiledBKFiledDate_EditValueChanged
            AddHandler education.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler EmailRecordControl1.EmailChanged, AddressOf EmailRecordControl1_Changed
            AddHandler EmailRecordControl1.SendEmailMessage, AddressOf email_SendEmailMessage
            AddHandler fico_score.Spin, AddressOf fico_score_Spin
            AddHandler fico_score.Validating, AddressOf fico_score_Validating
            AddHandler gender.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler job_name.RecordSystemNote, AddressOf Job_Name_RecordSystemNote
            AddHandler LookUpEdit_BankruptcyDistrict.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler LookUpEdit_CreditAgency.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler LookupEdit_Ethnicity.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler milDependent.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler milGrade.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler milService.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler milStatus.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler NameRecordControl1.NameChanged, AddressOf NameRecordControl1_Changed
            AddHandler NameRecordControl1.TextChanged, AddressOf NameRecordControl1_TextChanged
            AddHandler no_fico_score_reason.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler PoaControl1.RecordNote, AddressOf RecordPersonSystemNote
            AddHandler PopupContainerEdit_EverFiledBK.QueryDisplayText, AddressOf PopupContainerEdit_EverFiledBK_QueryDisplayText
            AddHandler PopupContainerEdit_FICO.QueryDisplayText, AddressOf PopupContainerEdit_FICO_QueryDisplayText
            AddHandler PopupContainerEdit_Military.QueryDisplayText, AddressOf PopupContainerEdit_Military_QueryDisplayText
            AddHandler race.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler relation.EditValueChanged, AddressOf relation_EditValueChanged
            AddHandler relation.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler txCertificateNo.Validating, AddressOf txCertificateNo_Validating
        End Sub

        ''' <summary>
        ''' Remove the event handler registrations
        ''' </summary>
        Private Sub UnRegisterHandlers()
            RemoveHandler birthdate.Validating, AddressOf birthdate_Validating
            RemoveHandler CheckEdit_ViewSSN.CheckStateChanged, AddressOf CheckEdit_ViewSSN_CheckStateChanged
            RemoveHandler dtEverFiledBKFiledDate.EditValueChanged, AddressOf dtEverFiledBKFiledDate_EditValueChanged
            RemoveHandler education.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler EmailRecordControl1.EmailChanged, AddressOf EmailRecordControl1_Changed
            RemoveHandler EmailRecordControl1.SendEmailMessage, AddressOf email_SendEmailMessage
            RemoveHandler fico_score.Spin, AddressOf fico_score_Spin
            RemoveHandler fico_score.Validating, AddressOf fico_score_Validating
            RemoveHandler gender.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler job_name.RecordSystemNote, AddressOf Job_Name_RecordSystemNote
            RemoveHandler LookUpEdit_BankruptcyDistrict.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler LookUpEdit_CreditAgency.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler LookupEdit_Ethnicity.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler milDependent.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler milGrade.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler milService.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler milStatus.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler NameRecordControl1.NameChanged, AddressOf NameRecordControl1_Changed
            RemoveHandler NameRecordControl1.TextChanged, AddressOf NameRecordControl1_TextChanged
            RemoveHandler no_fico_score_reason.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler PoaControl1.RecordNote, AddressOf RecordPersonSystemNote
            RemoveHandler PopupContainerEdit_EverFiledBK.QueryDisplayText, AddressOf PopupContainerEdit_EverFiledBK_QueryDisplayText
            RemoveHandler PopupContainerEdit_FICO.QueryDisplayText, AddressOf PopupContainerEdit_FICO_QueryDisplayText
            RemoveHandler PopupContainerEdit_Military.QueryDisplayText, AddressOf PopupContainerEdit_Military_QueryDisplayText
            RemoveHandler race.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler relation.EditValueChanged, AddressOf relation_EditValueChanged
            RemoveHandler relation.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler txCertificateNo.Validating, AddressOf txCertificateNo_Validating
        End Sub

        ''' <summary>
        ''' Process the loading of the record
        ''' </summary>
        Public Overrides Sub ReadForm(bc As DebtPlus.LINQ.BusinessContext)
            Dim createdRecord As Boolean = False

            Me.bc = bc
            UnRegisterHandlers()

            Try
                If IsApplicant Then
                    personRecord = Context.ClientDs.applicantRecord
                    Debug.Assert(personRecord IsNot Nothing)
                Else

                    ' We need to do this only on the first call. All other times, the controls will have the current values.
                    If personRecord Is Nothing Then
                        personRecord = Context.ClientDs.colPeople.Find(Function(s) s.Relation <> 1)

                        ' Attempt to locate the applicant record in the database if we don't have it in our cache.
                        If personRecord Is Nothing Then
                            personRecord = bc.peoples.Where(Function(s) s.Client = Context.ClientDs.ClientId AndAlso If(IsApplicant, s.Relation = 1, s.Relation <> 1)).FirstOrDefault()
                            If personRecord IsNot Nothing Then
                                Context.ClientDs.colPeople.Add(personRecord)
                            End If
                        End If

                        ' If the record is not located then manufacture a new item.
                        If personRecord Is Nothing Then
                            personRecord = DebtPlus.LINQ.Factory.Manufacture_people(Context.ClientDs.ClientId)

                            ' The gender is flipped from the applicant to the co-applicant
                            Dim newGender As Int32 = 3 - Context.ClientDs.applicantRecord.Gender
                            If newGender <> 2 Then
                                newGender = 1
                            End If
                            personRecord.Gender = newGender

                            bc.peoples.InsertOnSubmit(personRecord)
                            bc.SubmitChanges()
                            Context.ClientDs.colPeople.Add(personRecord)
                            createdRecord = True
                        End If
                    End If
                End If

                ' Load the relationship table
                If IsApplicant Then
                    relation.Properties.DataSource = DebtPlus.LINQ.Cache.RelationType.getList().FindAll(Function(s) s.Id = DebtPlus.LINQ.Cache.RelationType.Self)
                Else
                    If AllowRelationshipNONE() Then
                        relation.Properties.DataSource = DebtPlus.LINQ.Cache.RelationType.getList().FindAll(Function(s) s.Id <> DebtPlus.LINQ.Cache.RelationType.Self)
                        If createdRecord Then
                            personRecord.Relation = DebtPlus.LINQ.Cache.RelationType.None
                        End If
                    Else
                        relation.Properties.DataSource = DebtPlus.LINQ.Cache.RelationType.getList().FindAll(Function(s) Not (New Int32() {DebtPlus.LINQ.Cache.RelationType.Self, DebtPlus.LINQ.Cache.RelationType.None}.Contains(s.Id)))
                    End If
                End If

                ' After everything is loaded, define the popups with the proper text values. THese are normally
                ' tripped on the events, but since we have disabled events during the load, we need to do it here.
                PopupContainerEdit_EverFiledBK.Text = getEverFiledBKText(personRecord.bkfileddate)
                PopupContainerEdit_Military.Text = GetMilitaryStatusText(personRecord.MilitaryStatusID)
                PopupContainerEdit_FICO.Text = GetFICOText(personRecord.FICO_Score, personRecord.no_fico_score_reason)

                ' Before we can do the shadow copy of the name, we need to load the control with the proper
                ' value. It is possibly replaced with a shadow copy of the applicant, but that must happen
                ' after we have set it because we don't want to over-write the shadow copy.
                NameRecordControl1.EditValue = personRecord.NameID

                ' Correct the fields in the controls for the co-applicant if needed
                If createdRecord Then

                    ' The last name field is loaded from the applicant information
                    Dim applicantName As DebtPlus.LINQ.Name = Context.ClientDs.GetNameByID(Context.ClientDs.applicantRecord.NameID)
                    If applicantName IsNot Nothing Then
                        Dim n As New DebtPlus.LINQ.Name() With {.Last = applicantName.Last}
                        NameRecordControl1.SetValues(n)
                    End If
                End If

                ' Define the bankruptcy district. It varies depending upon the client's state
                If Context.ClientDs.clientAddress IsNot Nothing Then
                    Dim col As System.Collections.Generic.List(Of DebtPlus.LINQ.bankruptcy_district) = (From v In DebtPlus.LINQ.Cache.bankruptcy_district.getList() Where ((personRecord.bkDistrict.HasValue AndAlso v.Id = personRecord.bkDistrict.Value) OrElse (v.ActiveFlag AndAlso v.state = Context.ClientDs.clientAddress.state)) Select v).ToList()
                    LookUpEdit_BankruptcyDistrict.Properties.DataSource = col
                End If
                LookUpEdit_BankruptcyDistrict.EditValue = personRecord.bkDistrict

                ' Ok. The record is complete. Now, load the other controls. (name has been loaded already.)
                birthdate.EditValue = personRecord.Birthdate
                CheckEdit_Disabled.Checked = personRecord.Disabled
                dtAuthLetter.EditValue = personRecord.bkAuthLetterDate
                dtBkDischargedDate.EditValue = personRecord.bkdischarge
                dtEverFiledBKFiledDate.EditValue = personRecord.bkfileddate
                education.EditValue = personRecord.Education
                EmailRecordControl1.EditValue = personRecord.EmailID
                fico_score.EditValue = personRecord.FICO_Score
                former.EditValue = personRecord.Former
                gender.EditValue = personRecord.Gender
                LookUpEdit_CreditAgency.EditValue = personRecord.CreditAgency
                LookupEdit_Ethnicity.EditValue = personRecord.Ethnicity
                luEverFiledBKChapter.EditValue = personRecord.bkchapter
                luPmtCurrent.EditValue = personRecord.bkPaymentCurrent
                milDependent.EditValue = personRecord.MilitaryDependentID
                milGrade.EditValue = personRecord.MilitaryGradeID
                milService.EditValue = personRecord.MilitaryServiceID
                milStatus.EditValue = personRecord.MilitaryStatusID
                no_fico_score_reason.EditValue = personRecord.no_fico_score_reason
                race.EditValue = personRecord.Race
                relation.EditValue = personRecord.Relation
                dtCertificateIssued.EditValue = personRecord.bkCertificateIssued
                dtCertificateExpires.EditValue = personRecord.bkCertificateExpires
                txCertificateNo.EditValue = personRecord.bkCertificateNo
                txCaseNumber.EditValue = personRecord.bkCaseNumber

                ' Load the SSN field with the proper display text
                CheckEdit_ViewSSN.Enabled = EmptySSN OrElse AllowSSN
                CheckEdit_ViewSSN.Properties.ReadOnly = False
                CheckEdit_ViewSSN.Checked = EmptySSN

                ' Show or hide the SSN field
                If CheckEdit_ViewSSN.Checked Then
                    ShowSSN()
                Else
                    HideSSN()
                End If

                ' Once the person record is fully defined then we can pass it along to the
                ' sub-form controls.
                PoaControl1.ReadForm(bc, personRecord)
                job_name.ReadForm(bc, personRecord)
                EnableControls()

                ' Reset the list of changed fields for the system note
                personRecord.ClearChangedFieldItems()

                ' Load the list of reasons why a fico score is low
                If FicoReasonClass Is Nothing Then
                    FicoReasonClass = New FICOReasons(CheckedListBoxControl_ScoreReasons)
                    FicoReasonClass.ReadForm(personRecord)
                End If

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading people information")
            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Display the SSN field when the user checks the show SSN control
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub ShowSSN()
            ssn.EditValue = personRecord.SSN
            ssn.Properties.ReadOnly = False
        End Sub

        ''' <summary>
        ''' Hide the SSN field when the user un-checks the display control.
        ''' </summary>
        Private Sub HideSSN()
            ssn.Properties.ReadOnly = True

            ' If there is no SSN field (or all zeros) then use an empty field.
            If EmptySSN Then
                ssn.EditValue = Nothing
                Return
            End If

            ' Otherwise, use the right most 4 digits for the account number
            ssn.EditValue = "00000" + personRecord.SSN.PadLeft(4, "0"c).Right(4)
        End Sub

        ''' <summary>
        ''' Do not allow the co-applicant to be deleted if there are references
        ''' </summary>
        Private Function AllowRelationshipNONE() As Boolean
            Dim answer As Boolean = True

            ' The applicant can never have "none". In fact, it can only have "self".
            If IsApplicant Then
                Return False
            End If

            ' No debts = ok to allow "none".
            If Context.DebtRecords Is Nothing Then
                Return True
            End If

            ' Do this only if the item is not new. New items can be canceled
            ' since they did not exist earlier.
            If personRecord IsNot Nothing AndAlso personRecord.Id > 0 Then
                For Each Debt As ClientUpdateDebtRecord In Context.DebtRecords

                    ' If the debt is owned by the co-applicant then reject the delete operation
                    Dim Signor As Int32 = DebtPlus.Utils.Nulls.DInt(Debt.person, -1)
                    If Signor = personRecord.Id Then
                        Return False
                    End If
                Next
            End If

            ' If we made it this far then we can allow "none" to occur.
            Return True
        End Function

        ''' <summary>
        ''' Enable the controls if a relation is specified.
        ''' </summary>
        Private Sub EnableControls()
            Dim Enabled As Boolean = (Convert.ToInt32(relation.EditValue) <> 0)

            ' Disable or enable the controls based upon the fact that a relationship is specified
            job_name.Enabled = Enabled
            NameRecordControl1.Enabled = Enabled
            former.Enabled = Enabled
            EmailRecordControl1.Enabled = Enabled
            race.Enabled = Enabled
            LookupEdit_Ethnicity.Enabled = Enabled
            gender.Enabled = Enabled
            birthdate.Enabled = Enabled
            education.Enabled = Enabled
            LookUpEdit_CreditAgency.Enabled = Enabled
            PopupContainerEdit_Military.Enabled = Enabled
            PopupContainerEdit_FICO.Enabled = Enabled
            CheckEdit_Disabled.Enabled = Enabled
            PopupContainerEdit_EverFiledBK.Enabled = Enabled
            PoaControl1.Enabled = Enabled
            ssn.Enabled = Enabled

            ' Do not enable the checkbox unless the user is allowed to check the item.
            CheckEdit_ViewSSN.Enabled = Enabled AndAlso (EmptySSN OrElse AllowSSN)
        End Sub

        ''' <summary>
        ''' Process the saving of the record
        ''' </summary>
        Public Overrides Sub SaveForm(bc As DebtPlus.LINQ.BusinessContext)

            UnRegisterHandlers() ' Suspend the validating events from happening now.
            Try
                MyBase.SaveForm(bc)

                ' If there is a certificate number but no issue date then use today
                If txCertificateNo.Text <> String.Empty AndAlso dtCertificateIssued.EditValue Is Nothing Then
                    dtCertificateIssued.EditValue = System.DateTime.Now.Date
                End If

                ' If there is an issue date but no expiration date then make it expire 180 days after issue
                If dtCertificateIssued.EditValue IsNot Nothing AndAlso dtCertificateExpires.EditValue Is Nothing Then
                    dtCertificateExpires.EditValue = DebtPlus.Utils.Nulls.v_DateTime(dtCertificateIssued.EditValue).Value.AddDays(180)
                End If

                ' Ensure that there is a person record to be saved
                '
                ' This is caused by doing something that cases the save event to be called twice, for example, looking at
                ' appointments or printing a report with the relationship being "none". Then, you close the client while you
                ' are still on the co-applicant tab. We can ignore the second save without problems.
                If personRecord Is Nothing OrElse personRecord.Id <= 0 Then
                    Return
                End If

                ' Make double-sure that the applicant is valid. It can not be changed. It can not be deleted.
                If IsApplicant Then
                    personRecord.Relation = 1

                    ' Now, if we are going to delete the record then we need to do it now before we gather the telephone, email, etc. fields.
                ElseIf DebtPlus.Utils.Nulls.v_Int32(relation.EditValue).GetValueOrDefault() = DebtPlus.LINQ.Cache.RelationType.None Then

                    ' Delete the POA information from the database
                    PoaControl1.DeleteRecord(personRecord)

                    ' If the record is found in the database then delete it from the database
                    bc.peoples.DeleteOnSubmit(personRecord)
                    bc.SubmitChanges()
                    Context.ClientDs.colPeople.Remove(personRecord)

                    ' Tell the bottom line form that the information was changed
                    OnChanged(EventArgs.Empty)

                    ' Clear the record pointer so that we will re-evaluate the item on the next call.
                    personRecord = Nothing
                    Return
                End If

                ' If the FICO score changed then we need to set the date that the FICO score was pulled
                ' There should be no other reason to change the FICO score other than a date from the credit report.
                Dim new_FICO_Score As System.Nullable(Of Int32) = DebtPlus.Utils.Nulls.v_Int32(fico_score.EditValue)
                If Not System.Nullable.Equals(Of Int32)(personRecord.FICO_Score, new_FICO_Score) Then
                    personRecord.FICO_Score = new_FICO_Score
                    If new_FICO_Score.GetValueOrDefault() > 0 Then
                        personRecord.FICO_pull_date = DebtPlus.LINQ.BusinessContext.getdate()
                    End If
                End If

                ' Retrieve the job related fields as needed
                job_name.SaveForm(bc, personRecord)

                ' Update the people record with the changed FICO reason list.
                FicoReasonClass.SaveForm()

                ' Retrieve the current fields from the editing controls into the record
                personRecord.Birthdate = DebtPlus.Utils.Nulls.v_DateTime(birthdate.EditValue)
                personRecord.bkAuthLetterDate = DebtPlus.Utils.Nulls.v_DateTime(dtAuthLetter.EditValue)
                personRecord.bkCertificateExpires = DebtPlus.Utils.Nulls.v_DateTime(dtCertificateExpires.EditValue)
                personRecord.bkCertificateIssued = DebtPlus.Utils.Nulls.v_DateTime(dtCertificateIssued.EditValue)
                personRecord.bkCertificateNo = DebtPlus.Utils.Nulls.v_String(txCertificateNo.EditValue)
                personRecord.bkCaseNumber = DebtPlus.Utils.Nulls.v_String(txCaseNumber.EditValue)
                personRecord.bkchapter = DebtPlus.Utils.Nulls.v_Int32(luEverFiledBKChapter.EditValue)
                personRecord.bkdischarge = DebtPlus.Utils.Nulls.v_DateTime(dtBkDischargedDate.EditValue)
                personRecord.bkDistrict = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_BankruptcyDistrict.EditValue)
                personRecord.bkfileddate = DebtPlus.Utils.Nulls.v_DateTime(dtEverFiledBKFiledDate.EditValue)
                personRecord.bkPaymentCurrent = DebtPlus.Utils.Nulls.v_Int32(luPmtCurrent.EditValue)
                personRecord.CreditAgency = DebtPlus.Utils.Nulls.v_String(LookUpEdit_CreditAgency.EditValue)
                personRecord.Disabled = CheckEdit_Disabled.Checked
                personRecord.Education = DebtPlus.Utils.Nulls.v_Int32(education.EditValue)
                personRecord.EmailID = EmailRecordControl1.EditValue
                personRecord.Ethnicity = DebtPlus.Utils.Nulls.v_Int32(LookupEdit_Ethnicity.EditValue)
                personRecord.Former = DebtPlus.Utils.Nulls.v_String(former.EditValue)
                personRecord.Gender = DebtPlus.Utils.Nulls.v_Int32(gender.EditValue)
                personRecord.MilitaryDependentID = DebtPlus.Utils.Nulls.v_Int32(milDependent.EditValue)
                personRecord.MilitaryGradeID = DebtPlus.Utils.Nulls.v_Int32(milGrade.EditValue)
                personRecord.MilitaryServiceID = DebtPlus.Utils.Nulls.v_Int32(milService.EditValue)
                personRecord.MilitaryStatusID = DebtPlus.Utils.Nulls.v_Int32(milStatus.EditValue)
                personRecord.NameID = NameRecordControl1.EditValue
                personRecord.no_fico_score_reason = DebtPlus.Utils.Nulls.v_Int32(no_fico_score_reason.EditValue)
                personRecord.Race = DebtPlus.Utils.Nulls.v_Int32(race.EditValue)
                personRecord.Relation = DebtPlus.Utils.Nulls.v_Int32(relation.EditValue)

                ' the SSN field is normally updated when the checkbox is un-checked.
                If CheckEdit_ViewSSN.Checked AndAlso (AllowSSN OrElse EmptySSN) AndAlso (Not ssn.Properties.ReadOnly) Then
                    personRecord.SSN = DebtPlus.Utils.Nulls.v_String(ssn.EditValue)
                End If

                bc.SubmitChanges()
                GeneratePersonSystemNote(bc, personRecord.getChangedFieldItems())

                ' After the person record is fully formed (the Id field set) then we can pass it along
                ' to the POA control so that it can use it as the key field to its table.
                PoaControl1.SaveForm(bc, personRecord)

                ' Tell the bottom line form that the information was changed
                OnChanged(EventArgs.Empty)

            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Generate the system note with the changed field
        ''' </summary>
        ''' <param name="colChanges">Collection of changed fields in the people table</param>
        ''' <remarks></remarks>
        Private Sub GeneratePersonSystemNote(bc As BusinessContext, colChanges As System.Collections.Generic.List(Of DebtPlus.LINQ.ChangedFieldItem))

            ' No changed fields are easy to handle here.
            If colChanges.Count < 1 Then
                Return
            End If

            ' Get a list of the fields that we are changing
            Dim sbNote As New System.Text.StringBuilder()
            colChanges.ForEach(Sub(s) sbNote.Append(FormatPeopleNoteField(s)))

            Dim subject As String = "Changed {0} " + String.Join(",", colChanges.Select(Function(s) s.FieldName))
            RecordPersonSystemNote(subject, sbNote.ToString())
        End Sub

        ''' <summary>
        ''' Routine to translate the code values in the people record to suitable text strings.
        ''' This is conversion routine to convert the changes in the person record to a system note.
        ''' </summary>
        ''' <param name="item">Pointer to the field changed event arguments</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function FormatPeopleNoteField(item As DebtPlus.LINQ.ChangedFieldItem) As String

            ' Translate the fields which have code values to the suitable string. All others use the standard format routine.
            Select Case item.FieldName
                Case "job"
                    Return Helpers.FormatString(item.FieldName, Helpers.GetJobText(item.OldValue), Helpers.GetJobText(item.NewValue))
                Case "Education"
                    Return Helpers.FormatString(item.FieldName, Helpers.GetEducationText(item.OldValue), Helpers.GetEducationText(item.NewValue))
                Case "Ethnicity"
                    Return Helpers.FormatString(item.FieldName, Helpers.GetEthnicityText(item.OldValue), Helpers.GetEthnicityText(item.NewValue))
                Case "Gender"
                    Return Helpers.FormatString(item.FieldName, Helpers.GetGenderText(item.OldValue), Helpers.GetGenderText(item.NewValue))
                Case "MilitaryDependentID"
                    Return Helpers.FormatString(item.FieldName, Helpers.GetMilitaryDependentText(item.OldValue), Helpers.GetMilitaryDependentText(item.NewValue))
                Case "MilitaryGradeID"
                    Return Helpers.FormatString(item.FieldName, Helpers.GetMilitaryGradeText(item.OldValue), Helpers.GetMilitaryGradeText(item.NewValue))
                Case "MilitaryServiceID"
                    Return Helpers.FormatString(item.FieldName, Helpers.GetMilitaryServiceText(item.OldValue), Helpers.GetMilitaryServiceText(item.NewValue))
                Case "MilitaryStatusID"
                    Return Helpers.FormatString(item.FieldName, Helpers.GetMilitaryStatusText(item.OldValue), Helpers.GetMilitaryStatusText(item.NewValue))
                Case "no_fico_score_reason"
                    Return Helpers.FormatString(item.FieldName, Helpers.GetNoFicoScoreReasonText(item.OldValue), Helpers.GetNoFicoScoreReasonText(item.NewValue))
                Case "Race"
                    Return Helpers.FormatString(item.FieldName, Helpers.GetRaceText(item.OldValue), Helpers.GetRaceText(item.NewValue))
                Case "Relation"
                    Return Helpers.FormatString(item.FieldName, Helpers.GetRelationText(item.OldValue), Helpers.GetRelationText(item.NewValue))
                Case "bkchapter"
                    Return Helpers.FormatString(item.FieldName, Helpers.GetBankruptcyClassTypeText(item.OldValue), Helpers.GetBankruptcyClassTypeText(item.NewValue))
                Case "bkDistrict"
                    Return Helpers.FormatString(item.FieldName, Helpers.GetBankruptcyDistrictText(item.OldValue), Helpers.GetBankruptcyDistrictText(item.NewValue))
                Case "Frequency"
                    Return Helpers.FormatString(item.FieldName, Helpers.GetFrequencyText(item.OldValue), Helpers.GetFrequencyText(item.NewValue))
                Case Else
                    ' Fall out of the select statement to return the standard formatting routine.
            End Select

            ' Simply punt and record the item as a string. This works for strings, boolean and simple numbers
            Return Helpers.FormatString(item.FieldName, item.OldValue, item.NewValue)
        End Function

        ''' <summary>
        ''' Validate a change in the FICO score
        ''' </summary>
        Private Sub fico_score_Validating(ByVal sender As Object, ByVal e As CancelEventArgs)

            UnRegisterHandlers()
            Try
                Const STR_RangeError As String = "Valid entries range from 250 to 900"
                Dim ErrorMessage As String = String.Empty

                ' Find the old and new values for the fields
                Dim NewValue As Int32
                If fico_score.EditValue IsNot Nothing AndAlso fico_score.EditValue IsNot DBNull.Value Then
                    NewValue = Convert.ToInt32(fico_score.EditValue)
                Else
                    NewValue = 0
                End If

                ' Validate the range for the entry
                If NewValue < 0 Then
                    ErrorMessage = STR_RangeError
                    e.Cancel = True

                Else

                    If NewValue > 0 Then
                        If NewValue < 250 OrElse NewValue > 900 Then
                            ErrorMessage = STR_RangeError
                            e.Cancel = True
                        End If
                    End If
                End If

                fico_score.ErrorText = ErrorMessage

            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Process the spin button for the fico score to skip the invalids
        ''' </summary>
        Private Sub fico_score_Spin(ByVal sender As Object, ByVal e As SpinEventArgs)

            UnRegisterHandlers()
            Try
                ' Find the current (old) value so that it may be properly changed.
                Dim old_score As System.Nullable(Of System.Int32) = DebtPlus.Utils.Nulls.v_Int32(fico_score.EditValue)

                ' Up or down from nothing is now zero
                If Not old_score.HasValue Then
                    fico_score.EditValue = 0
                    e.Handled = True
                    Return
                End If

                ' Spin down from 0 is nothing
                If Not e.IsSpinUp AndAlso old_score.Value = 0 Then
                    fico_score.EditValue = Nothing
                    e.Handled = True
                    Return
                End If

                ' Spin up from 0 is now 250
                If e.IsSpinUp AndAlso old_score.Value = 0 Then
                    fico_score.EditValue = 250
                    e.Handled = True
                    Return
                End If

                ' Spin down from 250 is now 0
                If Not e.IsSpinUp AndAlso old_score.Value <= 250 Then
                    fico_score.EditValue = 0
                    e.Handled = True
                    Return
                End If

                ' Spin up from 900 is still 900
                If e.IsSpinUp AndAlso old_score.Value >= 900 Then
                    fico_score.EditValue = 900
                    e.Handled = True
                    Return
                End If

                ' Otherwise, we allow the system to do the spin operation. It will do it properly.
                e.Handled = False
            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' If double-clicking on email then start the email to the address
        ''' </summary>
        Private Sub email_SendEmailMessage(ByVal sender As Object, ByVal e As OpenLinkEventArgs)
            Dim ctl As TextEdit = CType(sender, TextEdit)

            Dim emailAddress As String = DebtPlus.Utils.Nulls.DStr(e.EditValue).Trim()

            ' If there is an email address then generate the email message program request
            If emailAddress <> String.Empty Then

                ' Find the header information in the registry
                Dim emailHeader As String = DebtPlus.Configuration.Config.EmailHeader

                ' Form the request with the proper syntax
                emailHeader = emailHeader.Replace("?client?", String.Format("{0:0000000}", Context.ClientId))
                If emailHeader <> String.Empty Then emailHeader = "?" + emailHeader

                ' Request the email message be generated at this point.
                Process.Start(String.Format("mailto:{0}{1}", emailAddress, emailHeader))

                ' Suppress the system from doing the same operation
                e.Handled = True
            End If
        End Sub

        ''' <summary>
        ''' Handle a change in the relation record
        ''' </summary>
        Private Sub relation_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            UnRegisterHandlers()
            Try
                EnableControls()
            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Validate the date fields
        ''' </summary>
        Private Sub birthdate_Validating(ByVal sender As Object, ByVal e As CancelEventArgs)
            Const STR_MustBe1800 As String = "Date must be greater than 1800"

            Dim ctl As DateEdit = CType(sender, DateEdit)
            Dim ErrorMessage As String = String.Empty

            ' Generate an error if the person is too young. The birthdate may be missing. That's ok. But if it is given, it must be valid.
            If (ctl.EditValue IsNot Nothing) AndAlso (ctl.EditValue IsNot DBNull.Value) Then

                ' Validate the date to a minimum level
                Dim NewDate As DateTime = Convert.ToDateTime(ctl.EditValue)
                If NewDate.Year < 1800 Then
                    ErrorMessage = STR_MustBe1800
                ElseIf NewDate > Now.Date Then
                    ErrorMessage = "Date can not be in the future"

                Else

                    ' If this is a birthdate then check the age to ensure that the person is at least 16 years old.
                    If sender Is birthdate Then
                        Const INT_MinimumAge As Int32 = 16
                        Dim CompareDate As DateTime = NewDate.AddYears(INT_MinimumAge).Date
                        If CompareDate.CompareTo(Now.Date) > 0 Then
                            ErrorMessage = String.Format("Must be at least {0:f0} years old", INT_MinimumAge)
                        End If
                    End If
                End If
            End If

            ' Set the error condition for the control
            ctl.ErrorText = ErrorMessage
            e.Cancel = (ErrorMessage <> String.Empty)
        End Sub

        ''' <summary>
        ''' Relative person in the people table for this record
        ''' </summary>
        <Description("True for applicant, false for co-applicant"), Category("DebtPlus"), Browsable(True), DefaultValue(GetType(Boolean), "True")>
        Public Property IsApplicant() As Boolean
            Get
                Return _IsApplicant
            End Get
            Set(ByVal Value As Boolean)
                _IsApplicant = Value
            End Set
        End Property

        Private Sub dtEverFiledBKFiledDate_EditValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

            If dtEverFiledBKFiledDate.EditValue Is Nothing Then
                luEverFiledBKChapter.Enabled = False
                dtBkDischargedDate.Enabled = False
                txCertificateNo.Enabled = False
                txCaseNumber.Enabled = False
                dtCertificateExpires.Enabled = False
                dtCertificateIssued.Enabled = False
                luPmtCurrent.Enabled = False
                dtAuthLetter.Enabled = False

                luEverFiledBKChapter.EditValue = Nothing
                dtBkDischargedDate.EditValue = Nothing
                luPmtCurrent.EditValue = Nothing
                dtAuthLetter.EditValue = Nothing
            Else
                luEverFiledBKChapter.Enabled = True
                dtBkDischargedDate.Enabled = True
                luPmtCurrent.Enabled = True
                dtAuthLetter.Enabled = True
                txCertificateNo.Enabled = True
                txCaseNumber.Enabled = True
                dtCertificateExpires.Enabled = True
                dtCertificateIssued.Enabled = True
            End If
        End Sub

        ''' <summary>
        ''' Request from the editing control to get the display text for the Bankruptcy control
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub PopupContainerEdit_EverFiledBK_QueryDisplayText(ByVal sender As System.Object, ByVal e As DevExpress.XtraEditors.Controls.QueryDisplayTextEventArgs)
            e.DisplayText = getEverFiledBKText(DebtPlus.Utils.Nulls.v_DateTime(dtEverFiledBKFiledDate.EditValue))
        End Sub

        ''' <summary>
        ''' Get the appropriate display text for the bankruptcy indication
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function getEverFiledBKText(itemDate As System.Nullable(Of System.DateTime)) As String
            If itemDate.HasValue Then
                Return itemDate.Value.ToShortDateString()
            End If
            Return String.Empty
        End Function

        ''' <summary>
        ''' Request from the editing control to get the display text value for military status
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub PopupContainerEdit_Military_QueryDisplayText(sender As Object, e As DevExpress.XtraEditors.Controls.QueryDisplayTextEventArgs)
            e.DisplayText = GetMilitaryStatusText(DebtPlus.Utils.Nulls.v_Int32(milStatus.EditValue))
        End Sub

        ''' <summary>
        ''' Request from the editing control to get the display text value for the FICO score
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub PopupContainerEdit_FICO_QueryDisplayText(sender As Object, e As DevExpress.XtraEditors.Controls.QueryDisplayTextEventArgs)
            e.DisplayText = GetFICOText(DebtPlus.Utils.Nulls.v_Int32(fico_score.EditValue), DebtPlus.Utils.Nulls.v_Int32(no_fico_score_reason.EditValue))
        End Sub

        ''' <summary>
        ''' Retrieve the text string for the FICO score control.
        ''' </summary>
        ''' <param name="scoreID">The current FICO score</param>
        ''' <param name="keyId">The key to the reason why there is no score</param>
        Private Function GetFICOText(ByVal scoreID As System.Nullable(Of Int32), ByVal keyId As System.Nullable(Of Int32)) As String

            ' If there is a FICO score then use the FICO score itself.
            If scoreID.GetValueOrDefault() > 0 Then
                Return scoreID.Value.ToString()
            End If

            ' Find the reason why there is no score
            If Not keyId.HasValue Then
                Return String.Empty
            End If

            ' Use the reason why there is no score for the fico score entry.
            Dim q As Housing_FICONotIncludedReason = TryCast(no_fico_score_reason.GetSelectedDataRow(), Housing_FICONotIncludedReason)
            If q Is Nothing Then
                Return String.Empty
            End If

            ' Use the description for the reason why there is no score as the value.
            Return q.description
        End Function

        ''' <summary>
        ''' Event handler to display the last 4 digits of the SSN field or the entire number
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        Private Sub CheckEdit_ViewSSN_CheckStateChanged(sender As Object, e As EventArgs)

            ' Going from un-checked to checked. We know that we are allowed to do this from earlier tests.
            If CheckEdit_ViewSSN.Checked Then
                ShowSSN()
                Return
            End If

            ' Save the SSN field if we are going to un-checked status
            If EmptySSN OrElse AllowSSN Then
                personRecord.SSN = DebtPlus.Utils.Nulls.v_String(ssn.EditValue)
            End If

            ' Hide the SSN field. This will disable the input fields as well.
            HideSSN()

            ' If there is an SSN and not allowed then disable the checkbox
            If Not EmptySSN AndAlso Not AllowSSN Then
                CheckEdit_ViewSSN.Enabled = False
            End If
        End Sub

        ''' <summary>
        ''' Do not allow leading or trailing spaces in the certificate text buffer.
        ''' </summary>
        Private Sub txCertificateNo_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs)
            Dim str As String = txCertificateNo.Text.Trim()
            If str <> txCertificateNo.Text Then
                txCertificateNo.Text = str
            End If
        End Sub
    End Class
End Namespace
