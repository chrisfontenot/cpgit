#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.UI.Client.forms.Comparison
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Threading

Namespace pages

    Friend Class Page_Comparison

        '' temporary hack
        Public Property SqlInfo As DebtPlus.LINQ.SQLInfoClass = DebtPlus.LINQ.SQLInfoClass.getDefault()
        Private bc As DebtPlus.LINQ.BusinessContext = Nothing

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            EnableMenus = True
        End Sub

        Public Overrides Sub ReadForm(bc As DebtPlus.LINQ.BusinessContext)
            Me.bc = bc
            UpdateTable = Context.ClientDs.SalesFileTable()
            GridControl1.DataSource = UpdateTable.DefaultView
            GridView1.OptionsSelection.EnableAppearanceFocusedRow = False
        End Sub

        Protected Overrides Sub OnCreateItem()
            Dim vue As DataView = CType(GridControl1.DataSource, System.Data.DataView)

            ' Create a new row in the table
            Dim drv As DataRowView = vue.AddNew
            drv.BeginEdit()
            drv("date_created") = Now
            drv("created_by") = "Me"
            drv("client") = Context.ClientId

            ' Update the database with the new row
            Dim cn As SqlConnection = New SqlConnection(SqlInfo.ConnectionString)
            Try
                cn.Open()
                Using cmd As SqlCommand = New SqlCommand
                    With cmd
                        .Connection = cn
                        .CommandText = "xpr_insert_sales_file"
                        .CommandType = CommandType.StoredProcedure
                        .Parameters.Add("@RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue
                        .Parameters.Add("@client", SqlDbType.Int).Value = Context.ClientId
                        .ExecuteNonQuery()
                        drv("sales_file") = .Parameters(0).Value
                    End With
                End Using

            Catch ex As SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error creating sales file")

            Finally
                If cn IsNot Nothing Then cn.Dispose()
            End Try

            ' Accept the update of the row so that we don't see changes later
            drv.EndEdit()
            drv.Row.AcceptChanges()

            ' Fall through to edit the new sales file
            OnEditItem(drv)
        End Sub

        Protected Overrides Sub OnDeleteItem(obj As Object)
            Dim drv As System.Data.DataRowView = TryCast(obj, System.Data.DataRowView)
            If drv Is Nothing Then
                Return
            End If

            If DebtPlus.Data.Prompts.RequestConfirmation_Delete() <> DialogResult.Yes Then
                Return
            End If

            drv.Delete()
        End Sub

        Protected Overrides Sub OnEditItem(ByVal obj As Object)

            Dim drv As DataRowView = TryCast(obj, DataRowView)
            If drv IsNot Nothing Then
                Dim thrd As New Thread(New ParameterizedThreadStart(AddressOf EditThread))
                thrd.SetApartmentState(ApartmentState.STA)
                thrd.Name = "SalesFile"
                thrd.IsBackground = True
                thrd.Start(drv)
            End If
        End Sub

        Public Overrides Sub SaveForm(bc As DebtPlus.LINQ.BusinessContext)
            Context.ClientDs.UpdateSalesFiles()
        End Sub

        Private Sub ReloadDebtList()

            ' Ensure that we are in the proper thread context when we do this operation.
            If InvokeRequired Then
                EndInvoke(BeginInvoke(New MethodInvoker(AddressOf ReloadDebtList)))
                Return
            End If

            Try
                ' Reload the debt list
                Context.DebtRecords.RefreshData()

                ' Re-read the client information, forgetting all of the changes to the tables.
                bc.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, Context.ClientDs.clientRecord)

                ' If the debts page is being shown then reload it now
                If TypeOf CType(ParentForm, forms.Form_ClientUpdate).FindCurrentPage Is Page_Debts Then
                    CType(ParentForm, forms.Form_ClientUpdate).FindCurrentPage.ReadForm(bc)
                End If

            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
            End Try
        End Sub

        Private Sub EditThread(ByVal obj As Object)
            Dim drv As DataRowView = CType(obj, DataRowView)
            Dim answer As DialogResult

            ' Do the editing operation on the form
            Using frm As New Form_Comparison_List(Context, drv)
                drv.BeginEdit()
                answer = frm.ShowDialog()
                drv.EndEdit()
            End Using

            ' If the form ended with "export" then reload the debt list
            If answer = DialogResult.Yes Then
                ReloadDebtList()
            End If
        End Sub
    End Class
End Namespace
