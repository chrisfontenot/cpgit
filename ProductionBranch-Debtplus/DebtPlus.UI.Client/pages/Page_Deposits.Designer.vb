﻿Imports DebtPlus.UI.Client.controls

Namespace pages

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Page_Deposits
        Inherits ControlBaseClient

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.ClientDepositsControl1 = New DebtPlus.UI.Client.controls.ClientDepositsControl()
            Me.chk_personal_checks = New DevExpress.XtraEditors.CheckEdit()
            Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
            Me.lbl_last_deposit_amount = New DevExpress.XtraEditors.LabelControl()
            Me.lbl_last_deposit_date = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
            Me.lk_config_fee = New DevExpress.XtraEditors.LookUpEdit()
            Me.AchControl1 = New DebtPlus.UI.Client.controls.ACHControl()
            Me.AchOneTimeListControl1 = New DebtPlus.UI.Client.controls.ACHOneTimeListControl()
            CType(Me.ClientDepositsControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.chk_personal_checks.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupControl1.SuspendLayout()
            CType(Me.lk_config_fee.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.AchControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.AchOneTimeListControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'ClientDepositsControl1
            '
            Me.ClientDepositsControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ClientDepositsControl1.Location = New System.Drawing.Point(390, 3)
            Me.ClientDepositsControl1.Name = "ClientDepositsControl1"
            Me.ClientDepositsControl1.Size = New System.Drawing.Size(183, 126)
            Me.ClientDepositsControl1.TabIndex = 4
            '
            'chk_personal_checks
            '
            Me.chk_personal_checks.Location = New System.Drawing.Point(4, 4)
            Me.chk_personal_checks.Name = "chk_personal_checks"
            Me.chk_personal_checks.Properties.Caption = "Accept Personal Checks for Deposits"
            Me.chk_personal_checks.Size = New System.Drawing.Size(213, 20)
            Me.chk_personal_checks.TabIndex = 0
            Me.chk_personal_checks.ToolTip = "Does the agency accept personal checks by this client for deposit?"
            '
            'GroupControl1
            '
            Me.GroupControl1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GroupControl1.Controls.Add(Me.lbl_last_deposit_amount)
            Me.GroupControl1.Controls.Add(Me.lbl_last_deposit_date)
            Me.GroupControl1.Controls.Add(Me.LabelControl2)
            Me.GroupControl1.Controls.Add(Me.LabelControl1)
            Me.GroupControl1.Location = New System.Drawing.Point(245, 6)
            Me.GroupControl1.Name = "GroupControl1"
            Me.GroupControl1.Size = New System.Drawing.Size(139, 71)
            Me.GroupControl1.TabIndex = 3
            Me.GroupControl1.Text = "Last Deposit"
            '
            'lbl_last_deposit_amount
            '
            Me.lbl_last_deposit_amount.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_last_deposit_amount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_last_deposit_amount.Location = New System.Drawing.Point(54, 25)
            Me.lbl_last_deposit_amount.Name = "lbl_last_deposit_amount"
            Me.lbl_last_deposit_amount.Size = New System.Drawing.Size(80, 13)
            Me.lbl_last_deposit_amount.TabIndex = 1
            Me.lbl_last_deposit_amount.Text = "$0.00"
            Me.lbl_last_deposit_amount.ToolTip = "This is the amount of the last deposit by this client."
            '
            'lbl_last_deposit_date
            '
            Me.lbl_last_deposit_date.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.lbl_last_deposit_date.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.lbl_last_deposit_date.Location = New System.Drawing.Point(54, 44)
            Me.lbl_last_deposit_date.Name = "lbl_last_deposit_date"
            Me.lbl_last_deposit_date.Size = New System.Drawing.Size(80, 13)
            Me.lbl_last_deposit_date.TabIndex = 3
            Me.lbl_last_deposit_date.Text = "None"
            Me.lbl_last_deposit_date.ToolTip = "This is the date that the last depost was entered."
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(6, 44)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(23, 13)
            Me.LabelControl2.TabIndex = 2
            Me.LabelControl2.Text = "Date"
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(6, 25)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(37, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Amount"
            '
            'LabelControl5
            '
            Me.LabelControl5.Location = New System.Drawing.Point(5, 32)
            Me.LabelControl5.Name = "LabelControl5"
            Me.LabelControl5.Size = New System.Drawing.Size(61, 13)
            Me.LabelControl5.TabIndex = 1
            Me.LabelControl5.Text = "Program Fee"
            '
            'lk_config_fee
            '
            Me.lk_config_fee.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.lk_config_fee.Location = New System.Drawing.Point(73, 29)
            Me.lk_config_fee.Name = "lk_config_fee"
            Me.lk_config_fee.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.lk_config_fee.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.lk_config_fee.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 25, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 75, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.lk_config_fee.Properties.DisplayMember = "description"
            Me.lk_config_fee.Properties.NullText = "A VALUE IS REQUIRED HERE. PLEASE CHOOSE ONE."
            Me.lk_config_fee.Properties.ShowFooter = False
            Me.lk_config_fee.Properties.ShowHeader = False
            Me.lk_config_fee.Properties.SortColumnIndex = 1
            Me.lk_config_fee.Properties.ValueMember = "Id"
            Me.lk_config_fee.Size = New System.Drawing.Size(166, 20)
            Me.lk_config_fee.TabIndex = 2
            Me.lk_config_fee.ToolTip = "Each client's monthly fee may be calculated differently. Please choose the method" & _
        " of calculating the fee amount."
            '
            'AchControl1
            '
            Me.AchControl1.Dock = System.Windows.Forms.DockStyle.Bottom
            Me.AchControl1.Location = New System.Drawing.Point(0, 132)
            Me.AchControl1.Margin = New System.Windows.Forms.Padding(0)
            Me.AchControl1.Name = "AchControl1"
            Me.AchControl1.Size = New System.Drawing.Size(576, 184)
            Me.AchControl1.TabIndex = 5
            '
            'AchOneTimeListControl1
            '
            Me.AchOneTimeListControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.AchOneTimeListControl1.Location = New System.Drawing.Point(6, 55)
            Me.AchOneTimeListControl1.Name = "AchOneTimeListControl1"
            Me.AchOneTimeListControl1.Size = New System.Drawing.Size(233, 74)
            Me.AchOneTimeListControl1.TabIndex = 6
            '
            'Page_Deposits
            '
            Me.Controls.Add(Me.AchOneTimeListControl1)
            Me.Controls.Add(Me.AchControl1)
            Me.Controls.Add(Me.lk_config_fee)
            Me.Controls.Add(Me.LabelControl5)
            Me.Controls.Add(Me.GroupControl1)
            Me.Controls.Add(Me.chk_personal_checks)
            Me.Controls.Add(Me.ClientDepositsControl1)
            Me.Name = "Page_Deposits"
            Me.Size = New System.Drawing.Size(576, 316)
            CType(Me.ClientDepositsControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.chk_personal_checks.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupControl1.ResumeLayout(False)
            Me.GroupControl1.PerformLayout()
            CType(Me.lk_config_fee.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.AchControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.AchOneTimeListControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents ClientDepositsControl1 As ClientDepositsControl
        Friend WithEvents chk_personal_checks As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
        Friend WithEvents lbl_last_deposit_amount As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lbl_last_deposit_date As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents lk_config_fee As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents AchControl1 As ACHControl
        Friend WithEvents AchOneTimeListControl1 As DebtPlus.UI.Client.controls.ACHOneTimeListControl

    End Class
End Namespace