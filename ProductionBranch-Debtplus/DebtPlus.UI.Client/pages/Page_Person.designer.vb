
Imports DebtPlus.UI.Client.controls

Namespace pages

    Partial Class Page_Person

        'UserControl overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
            Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.PopupContainerEdit_FICO = New DevExpress.XtraEditors.PopupContainerEdit()
            Me.PopupContainerControl_FICO = New DevExpress.XtraEditors.PopupContainerControl()
            Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
            Me.CheckedListBoxControl_ScoreReasons = New DevExpress.XtraEditors.CheckedListBoxControl()
            Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
            Me.fico_score = New DevExpress.XtraEditors.SpinEdit()
            Me.no_fico_score_reason = New DevExpress.XtraEditors.LookUpEdit()
            Me.LookUpEdit_CreditAgency = New DevExpress.XtraEditors.LookUpEdit()
            Me.CheckEdit_ViewSSN = New DevExpress.XtraEditors.CheckEdit()
            Me.PoaControl1 = New DebtPlus.UI.Client.controls.POAControl()
            Me.PopupContainerControl_EverFiledBK = New DevExpress.XtraEditors.PopupContainerControl()
            Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
            Me.LookUpEdit_BankruptcyDistrict = New DevExpress.XtraEditors.LookUpEdit()
            Me.txCertificateNo = New DevExpress.XtraEditors.TextEdit()
            Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
            Me.dtCertificateExpires = New DevExpress.XtraEditors.DateEdit()
            Me.dtCertificateIssued = New DevExpress.XtraEditors.DateEdit()
            Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
            Me.dtAuthLetter = New DevExpress.XtraEditors.DateEdit()
            Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
            Me.luPmtCurrent = New DevExpress.XtraEditors.LookUpEdit()
            Me.dtBkDischargedDate = New DevExpress.XtraEditors.DateEdit()
            Me.luEverFiledBKChapter = New DevExpress.XtraEditors.LookUpEdit()
            Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
            Me.dtEverFiledBKFiledDate = New DevExpress.XtraEditors.DateEdit()
            Me.PopupContainerEdit_EverFiledBK = New DevExpress.XtraEditors.PopupContainerEdit()
            Me.PopupContainerControl_Military = New DevExpress.XtraEditors.PopupContainerControl()
            Me.milDependent = New DevExpress.XtraEditors.LookUpEdit()
            Me.milGrade = New DevExpress.XtraEditors.LookUpEdit()
            Me.milService = New DevExpress.XtraEditors.LookUpEdit()
            Me.milStatus = New DevExpress.XtraEditors.LookUpEdit()
            Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
            Me.NameRecordControl1 = New DebtPlus.Data.Controls.NameRecordControl()
            Me.relation = New DevExpress.XtraEditors.LookUpEdit()
            Me.birthdate = New DevExpress.XtraEditors.DateEdit()
            Me.education = New DevExpress.XtraEditors.LookUpEdit()
            Me.CheckEdit_Disabled = New DevExpress.XtraEditors.CheckEdit()
            Me.former = New DevExpress.XtraEditors.TextEdit()
            Me.race = New DevExpress.XtraEditors.LookUpEdit()
            Me.job_name = New DebtPlus.UI.Client.controls.JobControl()
            Me.ssn = New DebtPlus.Data.Controls.ssn()
            Me.gender = New DevExpress.XtraEditors.LookUpEdit()
            Me.EmailRecordControl1 = New DebtPlus.Data.Controls.EmailRecordControl()
            Me.LookupEdit_Ethnicity = New DevExpress.XtraEditors.LookUpEdit()
            Me.PopupContainerEdit_Military = New DevExpress.XtraEditors.PopupContainerEdit()
            Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.LayoutControlItem_Relationship = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_FormerName = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_Race = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_Gender = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_Job = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_MilitaryService = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_SSN = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_Birthdate = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_Education = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.LayoutControlItem_Ethnic = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem_Disabled = New DevExpress.XtraLayout.LayoutControlItem()
            Me.P = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem()
            Me.txCaseNumber = New DevExpress.XtraEditors.TextEdit()
            Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.LayoutControl1.SuspendLayout()
            CType(Me.PopupContainerEdit_FICO.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PopupContainerControl_FICO, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.PopupContainerControl_FICO.SuspendLayout()
            CType(Me.CheckedListBoxControl_ScoreReasons, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.fico_score.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.no_fico_score_reason.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookUpEdit_CreditAgency.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_ViewSSN.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PoaControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PopupContainerControl_EverFiledBK, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.PopupContainerControl_EverFiledBK.SuspendLayout()
            CType(Me.LookUpEdit_BankruptcyDistrict.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txCertificateNo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dtCertificateExpires.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dtCertificateExpires.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dtCertificateIssued.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dtCertificateIssued.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dtAuthLetter.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dtAuthLetter.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.luPmtCurrent.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dtBkDischargedDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dtBkDischargedDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.luEverFiledBKChapter.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dtEverFiledBKFiledDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dtEverFiledBKFiledDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PopupContainerEdit_EverFiledBK.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PopupContainerControl_Military, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.PopupContainerControl_Military.SuspendLayout()
            CType(Me.milDependent.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.milGrade.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.milService.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.milStatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.relation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.birthdate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.birthdate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.education.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_Disabled.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.former.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.race.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.job_name, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ssn.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.gender.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmailRecordControl1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LookupEdit_Ethnicity.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PopupContainerEdit_Military.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_Relationship, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_FormerName, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_Race, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_Gender, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_Job, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_MilitaryService, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_SSN, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_Birthdate, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_Education, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_Ethnic, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem_Disabled, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.P, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.txCaseNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl12
            '
            Me.LabelControl12.Location = New System.Drawing.Point(4, 18)
            Me.LabelControl12.Name = "LabelControl12"
            Me.LabelControl12.Size = New System.Drawing.Size(58, 13)
            Me.LabelControl12.TabIndex = 1
            Me.LabelControl12.Text = "Tel. Number"
            '
            'LayoutControl1
            '
            Me.LayoutControl1.Controls.Add(Me.PopupContainerEdit_FICO)
            Me.LayoutControl1.Controls.Add(Me.PopupContainerControl_FICO)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_ViewSSN)
            Me.LayoutControl1.Controls.Add(Me.PoaControl1)
            Me.LayoutControl1.Controls.Add(Me.PopupContainerControl_EverFiledBK)
            Me.LayoutControl1.Controls.Add(Me.PopupContainerEdit_EverFiledBK)
            Me.LayoutControl1.Controls.Add(Me.PopupContainerControl_Military)
            Me.LayoutControl1.Controls.Add(Me.NameRecordControl1)
            Me.LayoutControl1.Controls.Add(Me.relation)
            Me.LayoutControl1.Controls.Add(Me.birthdate)
            Me.LayoutControl1.Controls.Add(Me.education)
            Me.LayoutControl1.Controls.Add(Me.CheckEdit_Disabled)
            Me.LayoutControl1.Controls.Add(Me.former)
            Me.LayoutControl1.Controls.Add(Me.race)
            Me.LayoutControl1.Controls.Add(Me.job_name)
            Me.LayoutControl1.Controls.Add(Me.ssn)
            Me.LayoutControl1.Controls.Add(Me.gender)
            Me.LayoutControl1.Controls.Add(Me.EmailRecordControl1)
            Me.LayoutControl1.Controls.Add(Me.LookupEdit_Ethnicity)
            Me.LayoutControl1.Controls.Add(Me.PopupContainerEdit_Military)
            Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControl1.Name = "LayoutControl1"
            Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(352, 40, 450, 350)
            Me.LayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = True
            Me.LayoutControl1.OptionsView.UseDefaultDragAndDropRendering = False
            Me.LayoutControl1.Root = Me.LayoutControlGroup1
            Me.LayoutControl1.Size = New System.Drawing.Size(544, 681)
            Me.LayoutControl1.TabIndex = 0
            Me.LayoutControl1.Text = "LayoutControl1"
            '
            'PopupContainerEdit_FICO
            '
            Me.PopupContainerEdit_FICO.Location = New System.Drawing.Point(79, 234)
            Me.PopupContainerEdit_FICO.Name = "PopupContainerEdit_FICO"
            Me.PopupContainerEdit_FICO.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.PopupContainerEdit_FICO.Properties.PopupControl = Me.PopupContainerControl_FICO
            Me.PopupContainerEdit_FICO.Properties.ShowPopupCloseButton = False
            Me.PopupContainerEdit_FICO.Size = New System.Drawing.Size(189, 20)
            Me.PopupContainerEdit_FICO.StyleController = Me.LayoutControl1
            Me.PopupContainerEdit_FICO.TabIndex = 35
            '
            'PopupContainerControl_FICO
            '
            Me.PopupContainerControl_FICO.Controls.Add(Me.LabelControl11)
            Me.PopupContainerControl_FICO.Controls.Add(Me.CheckedListBoxControl_ScoreReasons)
            Me.PopupContainerControl_FICO.Controls.Add(Me.LabelControl10)
            Me.PopupContainerControl_FICO.Controls.Add(Me.LabelControl9)
            Me.PopupContainerControl_FICO.Controls.Add(Me.LabelControl8)
            Me.PopupContainerControl_FICO.Controls.Add(Me.fico_score)
            Me.PopupContainerControl_FICO.Controls.Add(Me.no_fico_score_reason)
            Me.PopupContainerControl_FICO.Controls.Add(Me.LookUpEdit_CreditAgency)
            Me.PopupContainerControl_FICO.Location = New System.Drawing.Point(233, 483)
            Me.PopupContainerControl_FICO.Name = "PopupContainerControl_FICO"
            Me.PopupContainerControl_FICO.Size = New System.Drawing.Size(295, 176)
            Me.PopupContainerControl_FICO.TabIndex = 34
            '
            'LabelControl11
            '
            Me.LabelControl11.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl11.Appearance.BackColor = System.Drawing.Color.DarkBlue
            Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
            Me.LabelControl11.Appearance.ForeColor = System.Drawing.Color.White
            Me.LabelControl11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
            Me.LabelControl11.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
            Me.LabelControl11.Location = New System.Drawing.Point(5, 55)
            Me.LabelControl11.Name = "LabelControl11"
            Me.LabelControl11.Size = New System.Drawing.Size(286, 13)
            Me.LabelControl11.TabIndex = 6
            Me.LabelControl11.Text = "FACTORS"
            Me.LabelControl11.UseMnemonic = False
            '
            'CheckedListBoxControl_ScoreReasons
            '
            Me.CheckedListBoxControl_ScoreReasons.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.CheckedListBoxControl_ScoreReasons.CheckOnClick = True
            Me.CheckedListBoxControl_ScoreReasons.Location = New System.Drawing.Point(5, 70)
            Me.CheckedListBoxControl_ScoreReasons.Name = "CheckedListBoxControl_ScoreReasons"
            Me.CheckedListBoxControl_ScoreReasons.Size = New System.Drawing.Size(287, 101)
            Me.CheckedListBoxControl_ScoreReasons.TabIndex = 7
            '
            'LabelControl10
            '
            Me.LabelControl10.Location = New System.Drawing.Point(138, 6)
            Me.LabelControl10.Name = "LabelControl10"
            Me.LabelControl10.Size = New System.Drawing.Size(36, 13)
            Me.LabelControl10.TabIndex = 2
            Me.LabelControl10.Text = "Agency"
            '
            'LabelControl9
            '
            Me.LabelControl9.Location = New System.Drawing.Point(5, 32)
            Me.LabelControl9.Name = "LabelControl9"
            Me.LabelControl9.Size = New System.Drawing.Size(70, 13)
            Me.LabelControl9.TabIndex = 4
            Me.LabelControl9.Text = "Reject Reason"
            '
            'LabelControl8
            '
            Me.LabelControl8.Location = New System.Drawing.Point(5, 6)
            Me.LabelControl8.Name = "LabelControl8"
            Me.LabelControl8.Size = New System.Drawing.Size(71, 13)
            Me.LabelControl8.TabIndex = 0
            Me.LabelControl8.Text = "F.I.C.O. Score"
            '
            'fico_score
            '
            Me.fico_score.EditValue = New Decimal(New Integer() {0, 0, 0, 0})
            Me.fico_score.Location = New System.Drawing.Point(80, 3)
            Me.fico_score.Name = "fico_score"
            Me.fico_score.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.fico_score.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.fico_score.Properties.IsFloatValue = False
            Me.fico_score.Properties.Mask.EditMask = "N00"
            Me.fico_score.Properties.MaxValue = New Decimal(New Integer() {880, 0, 0, 0})
            Me.fico_score.Size = New System.Drawing.Size(50, 20)
            Me.fico_score.TabIndex = 1
            '
            'no_fico_score_reason
            '
            Me.no_fico_score_reason.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.no_fico_score_reason.Location = New System.Drawing.Point(80, 29)
            Me.no_fico_score_reason.Name = "no_fico_score_reason"
            Me.no_fico_score_reason.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.no_fico_score_reason.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.no_fico_score_reason.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.no_fico_score_reason.Properties.DisplayMember = "description"
            Me.no_fico_score_reason.Properties.NullText = ""
            Me.no_fico_score_reason.Properties.ShowFooter = False
            Me.no_fico_score_reason.Properties.ShowHeader = False
            Me.no_fico_score_reason.Properties.SortColumnIndex = 1
            Me.no_fico_score_reason.Properties.ValueMember = "Id"
            Me.no_fico_score_reason.Size = New System.Drawing.Size(212, 20)
            Me.no_fico_score_reason.TabIndex = 5
            Me.no_fico_score_reason.ToolTip = "Reason why you have no credit score"
            '
            'LookUpEdit_CreditAgency
            '
            Me.LookUpEdit_CreditAgency.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LookUpEdit_CreditAgency.Location = New System.Drawing.Point(180, 3)
            Me.LookUpEdit_CreditAgency.Name = "LookUpEdit_CreditAgency"
            Me.LookUpEdit_CreditAgency.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_CreditAgency.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.LookUpEdit_CreditAgency.Properties.DisplayMember = "description"
            Me.LookUpEdit_CreditAgency.Properties.NullText = ""
            Me.LookUpEdit_CreditAgency.Properties.ShowFooter = False
            Me.LookUpEdit_CreditAgency.Properties.ShowHeader = False
            Me.LookUpEdit_CreditAgency.Properties.SortColumnIndex = 1
            Me.LookUpEdit_CreditAgency.Properties.ValueMember = "Id"
            Me.LookUpEdit_CreditAgency.Size = New System.Drawing.Size(112, 20)
            Me.LookUpEdit_CreditAgency.TabIndex = 3
            Me.LookUpEdit_CreditAgency.ToolTip = "Agency where you obtained the credit score"
            '
            'CheckEdit_ViewSSN
            '
            Me.CheckEdit_ViewSSN.Location = New System.Drawing.Point(195, 210)
            Me.CheckEdit_ViewSSN.Name = "CheckEdit_ViewSSN"
            Me.CheckEdit_ViewSSN.Properties.Caption = "View SSN"
            Me.CheckEdit_ViewSSN.Size = New System.Drawing.Size(76, 19)
            Me.CheckEdit_ViewSSN.StyleController = Me.LayoutControl1
            Me.CheckEdit_ViewSSN.TabIndex = 33
            '
            'PoaControl1
            '
            Me.PoaControl1.Location = New System.Drawing.Point(12, 132)
            Me.PoaControl1.Name = "PoaControl1"
            Me.PoaControl1.Size = New System.Drawing.Size(520, 74)
            Me.PoaControl1.TabIndex = 32
            '
            'PopupContainerControl_EverFiledBK
            '
            Me.PopupContainerControl_EverFiledBK.Controls.Add(Me.txCaseNumber)
            Me.PopupContainerControl_EverFiledBK.Controls.Add(Me.LabelControl17)
            Me.PopupContainerControl_EverFiledBK.Controls.Add(Me.LabelControl16)
            Me.PopupContainerControl_EverFiledBK.Controls.Add(Me.LookUpEdit_BankruptcyDistrict)
            Me.PopupContainerControl_EverFiledBK.Controls.Add(Me.txCertificateNo)
            Me.PopupContainerControl_EverFiledBK.Controls.Add(Me.LabelControl15)
            Me.PopupContainerControl_EverFiledBK.Controls.Add(Me.LabelControl13)
            Me.PopupContainerControl_EverFiledBK.Controls.Add(Me.dtCertificateExpires)
            Me.PopupContainerControl_EverFiledBK.Controls.Add(Me.dtCertificateIssued)
            Me.PopupContainerControl_EverFiledBK.Controls.Add(Me.LabelControl14)
            Me.PopupContainerControl_EverFiledBK.Controls.Add(Me.LabelControl20)
            Me.PopupContainerControl_EverFiledBK.Controls.Add(Me.dtAuthLetter)
            Me.PopupContainerControl_EverFiledBK.Controls.Add(Me.LabelControl19)
            Me.PopupContainerControl_EverFiledBK.Controls.Add(Me.luPmtCurrent)
            Me.PopupContainerControl_EverFiledBK.Controls.Add(Me.dtBkDischargedDate)
            Me.PopupContainerControl_EverFiledBK.Controls.Add(Me.luEverFiledBKChapter)
            Me.PopupContainerControl_EverFiledBK.Controls.Add(Me.LabelControl7)
            Me.PopupContainerControl_EverFiledBK.Controls.Add(Me.LabelControl6)
            Me.PopupContainerControl_EverFiledBK.Controls.Add(Me.LabelControl5)
            Me.PopupContainerControl_EverFiledBK.Controls.Add(Me.dtEverFiledBKFiledDate)
            Me.PopupContainerControl_EverFiledBK.Location = New System.Drawing.Point(18, 370)
            Me.PopupContainerControl_EverFiledBK.Name = "PopupContainerControl_EverFiledBK"
            Me.PopupContainerControl_EverFiledBK.ScrollBarSmallChange = 3
            Me.PopupContainerControl_EverFiledBK.Size = New System.Drawing.Size(200, 265)
            Me.PopupContainerControl_EverFiledBK.TabIndex = 0
            '
            'LabelControl16
            '
            Me.LabelControl16.Location = New System.Drawing.Point(6, 216)
            Me.LabelControl16.Name = "LabelControl16"
            Me.LabelControl16.Size = New System.Drawing.Size(33, 13)
            Me.LabelControl16.TabIndex = 16
            Me.LabelControl16.Text = "District"
            '
            'LookUpEdit_BankruptcyDistrict
            '
            Me.LookUpEdit_BankruptcyDistrict.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LookUpEdit_BankruptcyDistrict.Location = New System.Drawing.Point(94, 213)
            Me.LookUpEdit_BankruptcyDistrict.Name = "LookUpEdit_BankruptcyDistrict"
            Me.LookUpEdit_BankruptcyDistrict.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.LookUpEdit_BankruptcyDistrict.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookUpEdit_BankruptcyDistrict.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.LookUpEdit_BankruptcyDistrict.Properties.DisplayMember = "Description"
            Me.LookUpEdit_BankruptcyDistrict.Properties.NullText = ""
            Me.LookUpEdit_BankruptcyDistrict.Properties.ShowFooter = False
            Me.LookUpEdit_BankruptcyDistrict.Properties.ShowHeader = False
            Me.LookUpEdit_BankruptcyDistrict.Properties.ValueMember = "Id"
            Me.LookUpEdit_BankruptcyDistrict.Size = New System.Drawing.Size(100, 20)
            Me.LookUpEdit_BankruptcyDistrict.TabIndex = 17
            '
            'txCertificateNo
            '
            Me.txCertificateNo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.txCertificateNo.Location = New System.Drawing.Point(94, 134)
            Me.txCertificateNo.Name = "txCertificateNo"
            Me.txCertificateNo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.txCertificateNo.Properties.MaxLength = 50
            Me.txCertificateNo.Size = New System.Drawing.Size(102, 20)
            Me.txCertificateNo.TabIndex = 11
            '
            'LabelControl15
            '
            Me.LabelControl15.Location = New System.Drawing.Point(6, 137)
            Me.LabelControl15.Name = "LabelControl15"
            Me.LabelControl15.Size = New System.Drawing.Size(61, 13)
            Me.LabelControl15.TabIndex = 10
            Me.LabelControl15.Text = "Certificate #"
            '
            'LabelControl13
            '
            Me.LabelControl13.Location = New System.Drawing.Point(6, 189)
            Me.LabelControl13.Name = "LabelControl13"
            Me.LabelControl13.Size = New System.Drawing.Size(61, 13)
            Me.LabelControl13.TabIndex = 14
            Me.LabelControl13.Text = "Date Expires"
            '
            'dtCertificateExpires
            '
            Me.dtCertificateExpires.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.dtCertificateExpires.EditValue = Nothing
            Me.dtCertificateExpires.Location = New System.Drawing.Point(94, 186)
            Me.dtCertificateExpires.Name = "dtCertificateExpires"
            Me.dtCertificateExpires.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.dtCertificateExpires.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.dtCertificateExpires.Size = New System.Drawing.Size(102, 20)
            Me.dtCertificateExpires.TabIndex = 15
            '
            'dtCertificateIssued
            '
            Me.dtCertificateIssued.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.dtCertificateIssued.EditValue = Nothing
            Me.dtCertificateIssued.Location = New System.Drawing.Point(94, 160)
            Me.dtCertificateIssued.Name = "dtCertificateIssued"
            Me.dtCertificateIssued.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.dtCertificateIssued.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.dtCertificateIssued.Properties.VistaTimeProperties.EditFormat.FormatString = "d"
            Me.dtCertificateIssued.Properties.VistaTimeProperties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.dtCertificateIssued.Properties.VistaTimeProperties.Mask.EditMask = "d"
            Me.dtCertificateIssued.Size = New System.Drawing.Size(102, 20)
            Me.dtCertificateIssued.TabIndex = 13
            '
            'LabelControl14
            '
            Me.LabelControl14.Location = New System.Drawing.Point(6, 163)
            Me.LabelControl14.Name = "LabelControl14"
            Me.LabelControl14.Size = New System.Drawing.Size(58, 13)
            Me.LabelControl14.TabIndex = 12
            Me.LabelControl14.Text = "Date Issued"
            '
            'LabelControl20
            '
            Me.LabelControl20.Location = New System.Drawing.Point(6, 112)
            Me.LabelControl20.Name = "LabelControl20"
            Me.LabelControl20.Size = New System.Drawing.Size(82, 13)
            Me.LabelControl20.TabIndex = 8
            Me.LabelControl20.Text = "Auth Letter Rcvd"
            '
            'dtAuthLetter
            '
            Me.dtAuthLetter.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.dtAuthLetter.EditValue = Nothing
            Me.dtAuthLetter.Location = New System.Drawing.Point(94, 108)
            Me.dtAuthLetter.Name = "dtAuthLetter"
            Me.dtAuthLetter.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.dtAuthLetter.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.dtAuthLetter.Size = New System.Drawing.Size(102, 20)
            Me.dtAuthLetter.TabIndex = 9
            '
            'LabelControl19
            '
            Me.LabelControl19.Location = New System.Drawing.Point(6, 60)
            Me.LabelControl19.Name = "LabelControl19"
            Me.LabelControl19.Size = New System.Drawing.Size(60, 13)
            Me.LabelControl19.TabIndex = 4
            Me.LabelControl19.Text = "PMT Current"
            '
            'luPmtCurrent
            '
            Me.luPmtCurrent.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.luPmtCurrent.Location = New System.Drawing.Point(94, 56)
            Me.luPmtCurrent.Name = "luPmtCurrent"
            Me.luPmtCurrent.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
            Me.luPmtCurrent.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.luPmtCurrent.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.luPmtCurrent.Properties.DisplayMember = "description"
            Me.luPmtCurrent.Properties.NullText = ""
            Me.luPmtCurrent.Properties.ShowFooter = False
            Me.luPmtCurrent.Properties.ShowHeader = False
            Me.luPmtCurrent.Properties.SortColumnIndex = 1
            Me.luPmtCurrent.Properties.ValueMember = "Id"
            Me.luPmtCurrent.Size = New System.Drawing.Size(102, 20)
            Me.luPmtCurrent.TabIndex = 5
            '
            'dtBkDischargedDate
            '
            Me.dtBkDischargedDate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.dtBkDischargedDate.EditValue = Nothing
            Me.dtBkDischargedDate.Location = New System.Drawing.Point(94, 82)
            Me.dtBkDischargedDate.Name = "dtBkDischargedDate"
            Me.dtBkDischargedDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.dtBkDischargedDate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.dtBkDischargedDate.Properties.VistaTimeProperties.EditFormat.FormatString = "d"
            Me.dtBkDischargedDate.Properties.VistaTimeProperties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.dtBkDischargedDate.Properties.VistaTimeProperties.Mask.EditMask = "d"
            Me.dtBkDischargedDate.Size = New System.Drawing.Size(102, 20)
            Me.dtBkDischargedDate.TabIndex = 7
            '
            'luEverFiledBKChapter
            '
            Me.luEverFiledBKChapter.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.luEverFiledBKChapter.EditValue = ""
            Me.luEverFiledBKChapter.Location = New System.Drawing.Point(94, 30)
            Me.luEverFiledBKChapter.Name = "luEverFiledBKChapter"
            Me.luEverFiledBKChapter.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.luEverFiledBKChapter.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "description"), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.None, "", False, DevExpress.Utils.HorzAlignment.[Default])})
            Me.luEverFiledBKChapter.Properties.DisplayMember = "description"
            Me.luEverFiledBKChapter.Properties.DropDownRows = 5
            Me.luEverFiledBKChapter.Properties.NullText = ""
            Me.luEverFiledBKChapter.Properties.ShowFooter = False
            Me.luEverFiledBKChapter.Properties.ShowHeader = False
            Me.luEverFiledBKChapter.Properties.ValueMember = "Id"
            Me.luEverFiledBKChapter.Size = New System.Drawing.Size(102, 20)
            Me.luEverFiledBKChapter.StyleController = Me.LayoutControl1
            Me.luEverFiledBKChapter.TabIndex = 3
            '
            'LabelControl7
            '
            Me.LabelControl7.Location = New System.Drawing.Point(6, 34)
            Me.LabelControl7.Name = "LabelControl7"
            Me.LabelControl7.Size = New System.Drawing.Size(39, 13)
            Me.LabelControl7.TabIndex = 2
            Me.LabelControl7.Text = "Chapter"
            '
            'LabelControl6
            '
            Me.LabelControl6.Location = New System.Drawing.Point(6, 86)
            Me.LabelControl6.Name = "LabelControl6"
            Me.LabelControl6.Size = New System.Drawing.Size(53, 13)
            Me.LabelControl6.TabIndex = 6
            Me.LabelControl6.Text = "Discharged"
            '
            'LabelControl5
            '
            Me.LabelControl5.Location = New System.Drawing.Point(6, 8)
            Me.LabelControl5.Name = "LabelControl5"
            Me.LabelControl5.Size = New System.Drawing.Size(22, 13)
            Me.LabelControl5.TabIndex = 0
            Me.LabelControl5.Text = "Filed"
            '
            'dtEverFiledBKFiledDate
            '
            Me.dtEverFiledBKFiledDate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.dtEverFiledBKFiledDate.EditValue = Nothing
            Me.dtEverFiledBKFiledDate.Location = New System.Drawing.Point(94, 4)
            Me.dtEverFiledBKFiledDate.Name = "dtEverFiledBKFiledDate"
            Me.dtEverFiledBKFiledDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.dtEverFiledBKFiledDate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.dtEverFiledBKFiledDate.Properties.VistaTimeProperties.Mask.EditMask = "d"
            Me.dtEverFiledBKFiledDate.Size = New System.Drawing.Size(102, 20)
            Me.dtEverFiledBKFiledDate.TabIndex = 1
            '
            'PopupContainerEdit_EverFiledBK
            '
            Me.PopupContainerEdit_EverFiledBK.Location = New System.Drawing.Point(419, 108)
            Me.PopupContainerEdit_EverFiledBK.Name = "PopupContainerEdit_EverFiledBK"
            Me.PopupContainerEdit_EverFiledBK.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.PopupContainerEdit_EverFiledBK.Properties.PopupControl = Me.PopupContainerControl_EverFiledBK
            Me.PopupContainerEdit_EverFiledBK.Properties.ShowPopupCloseButton = False
            Me.PopupContainerEdit_EverFiledBK.Size = New System.Drawing.Size(113, 20)
            Me.PopupContainerEdit_EverFiledBK.StyleController = Me.LayoutControl1
            Me.PopupContainerEdit_EverFiledBK.TabIndex = 30
            '
            'PopupContainerControl_Military
            '
            Me.PopupContainerControl_Military.Controls.Add(Me.milDependent)
            Me.PopupContainerControl_Military.Controls.Add(Me.milGrade)
            Me.PopupContainerControl_Military.Controls.Add(Me.milService)
            Me.PopupContainerControl_Military.Controls.Add(Me.milStatus)
            Me.PopupContainerControl_Military.Controls.Add(Me.LabelControl4)
            Me.PopupContainerControl_Military.Controls.Add(Me.LabelControl3)
            Me.PopupContainerControl_Military.Controls.Add(Me.LabelControl2)
            Me.PopupContainerControl_Military.Controls.Add(Me.LabelControl1)
            Me.PopupContainerControl_Military.Location = New System.Drawing.Point(303, 373)
            Me.PopupContainerControl_Military.Margin = New System.Windows.Forms.Padding(0)
            Me.PopupContainerControl_Military.MinimumSize = New System.Drawing.Size(0, 104)
            Me.PopupContainerControl_Military.Name = "PopupContainerControl_Military"
            Me.PopupContainerControl_Military.Size = New System.Drawing.Size(224, 104)
            Me.PopupContainerControl_Military.TabIndex = 23
            '
            'milDependent
            '
            Me.milDependent.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.milDependent.Location = New System.Drawing.Point(62, 81)
            Me.milDependent.Name = "milDependent"
            Me.milDependent.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.milDependent.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.milDependent.Properties.DisplayMember = "description"
            Me.milDependent.Properties.NullText = ""
            Me.milDependent.Properties.ShowFooter = False
            Me.milDependent.Properties.ShowHeader = False
            Me.milDependent.Properties.SortColumnIndex = 1
            Me.milDependent.Properties.ValueMember = "Id"
            Me.milDependent.Size = New System.Drawing.Size(159, 20)
            Me.milDependent.TabIndex = 7
            '
            'milGrade
            '
            Me.milGrade.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.milGrade.Location = New System.Drawing.Point(62, 55)
            Me.milGrade.Name = "milGrade"
            Me.milGrade.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.milGrade.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.milGrade.Properties.DisplayMember = "description"
            Me.milGrade.Properties.NullText = ""
            Me.milGrade.Properties.ShowFooter = False
            Me.milGrade.Properties.ShowHeader = False
            Me.milGrade.Properties.SortColumnIndex = 1
            Me.milGrade.Properties.ValueMember = "Id"
            Me.milGrade.Size = New System.Drawing.Size(159, 20)
            Me.milGrade.TabIndex = 5
            '
            'milService
            '
            Me.milService.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.milService.Location = New System.Drawing.Point(62, 29)
            Me.milService.Name = "milService"
            Me.milService.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.milService.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.milService.Properties.DisplayMember = "description"
            Me.milService.Properties.NullText = ""
            Me.milService.Properties.ShowFooter = False
            Me.milService.Properties.ShowHeader = False
            Me.milService.Properties.SortColumnIndex = 1
            Me.milService.Properties.ValueMember = "Id"
            Me.milService.Size = New System.Drawing.Size(159, 20)
            Me.milService.TabIndex = 3
            '
            'milStatus
            '
            Me.milStatus.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.milStatus.Location = New System.Drawing.Point(62, 3)
            Me.milStatus.Name = "milStatus"
            Me.milStatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.milStatus.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.milStatus.Properties.DisplayMember = "description"
            Me.milStatus.Properties.NullText = ""
            Me.milStatus.Properties.ShowFooter = False
            Me.milStatus.Properties.ShowHeader = False
            Me.milStatus.Properties.SortColumnIndex = 1
            Me.milStatus.Properties.ValueMember = "Id"
            Me.milStatus.Size = New System.Drawing.Size(159, 20)
            Me.milStatus.TabIndex = 1
            '
            'LabelControl4
            '
            Me.LabelControl4.Location = New System.Drawing.Point(4, 84)
            Me.LabelControl4.Name = "LabelControl4"
            Me.LabelControl4.Size = New System.Drawing.Size(53, 13)
            Me.LabelControl4.TabIndex = 6
            Me.LabelControl4.Text = "Dependent"
            '
            'LabelControl3
            '
            Me.LabelControl3.Location = New System.Drawing.Point(4, 58)
            Me.LabelControl3.Name = "LabelControl3"
            Me.LabelControl3.Size = New System.Drawing.Size(24, 13)
            Me.LabelControl3.TabIndex = 4
            Me.LabelControl3.Text = "Rank"
            '
            'LabelControl2
            '
            Me.LabelControl2.Location = New System.Drawing.Point(4, 32)
            Me.LabelControl2.Name = "LabelControl2"
            Me.LabelControl2.Size = New System.Drawing.Size(33, 13)
            Me.LabelControl2.TabIndex = 2
            Me.LabelControl2.Text = "Branch"
            '
            'LabelControl1
            '
            Me.LabelControl1.Location = New System.Drawing.Point(4, 6)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(31, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Status"
            '
            'NameRecordControl1
            '
            Me.NameRecordControl1.Location = New System.Drawing.Point(79, 36)
            Me.NameRecordControl1.Name = "NameRecordControl1"
            Me.NameRecordControl1.Size = New System.Drawing.Size(453, 20)
            Me.NameRecordControl1.TabIndex = 25
            '
            'relation
            '
            Me.relation.Location = New System.Drawing.Point(79, 12)
            Me.relation.Name = "relation"
            Me.relation.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.relation.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("relationship", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.[Default], DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.relation.Properties.DisplayMember = "description"
            Me.relation.Properties.NullText = ""
            Me.relation.Properties.ShowFooter = False
            Me.relation.Properties.ShowHeader = False
            Me.relation.Properties.ShowLines = False
            Me.relation.Properties.ShowPopupShadow = False
            Me.relation.Properties.SortColumnIndex = 1
            Me.relation.Properties.ValueMember = "Id"
            Me.relation.Size = New System.Drawing.Size(174, 20)
            Me.relation.StyleController = Me.LayoutControl1
            Me.relation.TabIndex = 1
            Me.relation.ToolTip = "Choose the relationship to the main applicant. The main applicant is always calle" & _
        "d ""SELF""."
            '
            'birthdate
            '
            Me.birthdate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.birthdate.EditValue = Nothing
            Me.birthdate.Location = New System.Drawing.Point(455, 84)
            Me.birthdate.Name = "birthdate"
            Me.birthdate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.birthdate.Properties.MinValue = New Date(1800, 1, 1, 0, 0, 0, 0)
            Me.birthdate.Properties.ShowToday = False
            Me.birthdate.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
            Me.birthdate.Size = New System.Drawing.Size(77, 20)
            Me.birthdate.StyleController = Me.LayoutControl1
            Me.birthdate.TabIndex = 12
            Me.birthdate.ToolTip = "Enter the person's birthdate. We need to find the person's age, so if you don't k" & _
        "now the month or day, use 1 for the values."
            '
            'education
            '
            Me.education.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.education.Location = New System.Drawing.Point(337, 210)
            Me.education.Name = "education"
            Me.education.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.education.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_value", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.education.Properties.DisplayMember = "description"
            Me.education.Properties.NullText = ""
            Me.education.Properties.ShowFooter = False
            Me.education.Properties.ShowHeader = False
            Me.education.Properties.SortColumnIndex = 1
            Me.education.Properties.ValueMember = "Id"
            Me.education.Size = New System.Drawing.Size(195, 20)
            Me.education.StyleController = Me.LayoutControl1
            Me.education.TabIndex = 20
            Me.education.ToolTip = "Highest level of formal education for the person."
            '
            'CheckEdit_Disabled
            '
            Me.CheckEdit_Disabled.EditValue = 0
            Me.CheckEdit_Disabled.Location = New System.Drawing.Point(417, 60)
            Me.CheckEdit_Disabled.Name = "CheckEdit_Disabled"
            Me.CheckEdit_Disabled.Properties.Caption = "Disabled"
            Me.CheckEdit_Disabled.Properties.ValueChecked = 1
            Me.CheckEdit_Disabled.Properties.ValueUnchecked = 0
            Me.CheckEdit_Disabled.Size = New System.Drawing.Size(115, 19)
            Me.CheckEdit_Disabled.StyleController = Me.LayoutControl1
            Me.CheckEdit_Disabled.TabIndex = 22
            Me.CheckEdit_Disabled.ToolTip = "Is the person disabled?"
            '
            'former
            '
            Me.former.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.former.Location = New System.Drawing.Point(79, 60)
            Me.former.Name = "former"
            Me.former.Properties.MaxLength = 50
            Me.former.Size = New System.Drawing.Size(112, 20)
            Me.former.StyleController = Me.LayoutControl1
            Me.former.TabIndex = 5
            Me.former.ToolTip = "If there is a previous name for the person, enter it here. A ""maiden"" name would " & _
        "be suitable here."
            '
            'race
            '
            Me.race.Location = New System.Drawing.Point(79, 84)
            Me.race.Name = "race"
            Me.race.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.race.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_value", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.race.Properties.DisplayMember = "description"
            Me.race.Properties.NullText = ""
            Me.race.Properties.ShowFooter = False
            Me.race.Properties.ShowHeader = False
            Me.race.Properties.SortColumnIndex = 1
            Me.race.Properties.ValueMember = "Id"
            Me.race.Size = New System.Drawing.Size(112, 20)
            Me.race.StyleController = Me.LayoutControl1
            Me.race.TabIndex = 7
            Me.race.ToolTip = "Select the person's race"
            '
            'job_name
            '
            Me.job_name.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.job_name.Appearance.ForeColor = System.Drawing.Color.Black
            Me.job_name.Appearance.Options.UseForeColor = True
            Me.job_name.Location = New System.Drawing.Point(12, 258)
            Me.job_name.Margin = New System.Windows.Forms.Padding(0)
            Me.job_name.Name = "job_name"
            Me.job_name.Size = New System.Drawing.Size(520, 411)
            Me.job_name.TabIndex = 21
            '
            'ssn
            '
            Me.ssn.Location = New System.Drawing.Point(79, 210)
            Me.ssn.Name = "ssn"
            Me.ssn.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.ssn.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.ssn.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong
            Me.ssn.Properties.Mask.BeepOnError = True
            Me.ssn.Properties.Mask.EditMask = "000-00-0000"
            Me.ssn.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple
            Me.ssn.Properties.Mask.SaveLiteral = False
            Me.ssn.Properties.Mask.UseMaskAsDisplayFormat = True
            Me.ssn.Size = New System.Drawing.Size(112, 20)
            Me.ssn.StyleController = Me.LayoutControl1
            Me.ssn.TabIndex = 16
            Me.ssn.ToolTip = "This is the person's social security number."
            '
            'gender
            '
            Me.gender.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.gender.Location = New System.Drawing.Point(350, 84)
            Me.gender.Name = "gender"
            Me.gender.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.gender.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.gender.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_value", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 0, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.gender.Properties.DisplayMember = "description"
            Me.gender.Properties.NullText = ""
            Me.gender.Properties.ShowFooter = False
            Me.gender.Properties.ShowHeader = False
            Me.gender.Properties.SortColumnIndex = 1
            Me.gender.Properties.ValueMember = "Id"
            Me.gender.Size = New System.Drawing.Size(63, 20)
            Me.gender.StyleController = Me.LayoutControl1
            Me.gender.TabIndex = 10
            Me.gender.ToolTip = "Select the person's gender"
            '
            'EmailRecordControl1
            '
            Me.EmailRecordControl1.ClickedColor = System.Drawing.Color.Maroon
            Me.EmailRecordControl1.Location = New System.Drawing.Point(79, 108)
            Me.EmailRecordControl1.Name = "EmailRecordControl1"
            Me.EmailRecordControl1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            Me.EmailRecordControl1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Underline)
            Me.EmailRecordControl1.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
            Me.EmailRecordControl1.Properties.Appearance.Options.UseFont = True
            Me.EmailRecordControl1.Properties.Appearance.Options.UseForeColor = True
            Me.EmailRecordControl1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.EmailRecordControl1.Properties.MaxLength = 998
            Me.EmailRecordControl1.Size = New System.Drawing.Size(269, 20)
            Me.EmailRecordControl1.StyleController = Me.LayoutControl1
            Me.EmailRecordControl1.TabIndex = 26
            Me.EmailRecordControl1.ToolTip = "Person's E-Mail address"
            '
            'LookupEdit_Ethnicity
            '
            Me.LookupEdit_Ethnicity.EditValue = 0
            Me.LookupEdit_Ethnicity.Location = New System.Drawing.Point(241, 84)
            Me.LookupEdit_Ethnicity.Name = "LookupEdit_Ethnicity"
            Me.LookupEdit_Ethnicity.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
            Me.LookupEdit_Ethnicity.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.LookupEdit_Ethnicity.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("oID", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", False, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True]), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.[True])})
            Me.LookupEdit_Ethnicity.Properties.DisplayMember = "description"
            Me.LookupEdit_Ethnicity.Properties.NullText = ""
            Me.LookupEdit_Ethnicity.Properties.ShowFooter = False
            Me.LookupEdit_Ethnicity.Properties.ShowHeader = False
            Me.LookupEdit_Ethnicity.Properties.SortColumnIndex = 1
            Me.LookupEdit_Ethnicity.Properties.ValueMember = "Id"
            Me.LookupEdit_Ethnicity.Size = New System.Drawing.Size(65, 18)
            Me.LookupEdit_Ethnicity.StyleController = Me.LayoutControl1
            Me.LookupEdit_Ethnicity.TabIndex = 8
            Me.LookupEdit_Ethnicity.ToolTip = "Is the person Hispanic?"
            '
            'PopupContainerEdit_Military
            '
            Me.PopupContainerEdit_Military.Location = New System.Drawing.Point(262, 60)
            Me.PopupContainerEdit_Military.Name = "PopupContainerEdit_Military"
            Me.PopupContainerEdit_Military.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.PopupContainerEdit_Military.Properties.PopupControl = Me.PopupContainerControl_Military
            Me.PopupContainerEdit_Military.Properties.ShowPopupCloseButton = False
            Me.PopupContainerEdit_Military.Size = New System.Drawing.Size(151, 20)
            Me.PopupContainerEdit_Military.StyleController = Me.LayoutControl1
            Me.PopupContainerEdit_Military.TabIndex = 23
            Me.PopupContainerEdit_Military.ToolTip = "Type of military service"
            '
            'LayoutControlGroup1
            '
            Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
            Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem_Relationship, Me.LayoutControlItem_FormerName, Me.LayoutControlItem_Race, Me.LayoutControlItem_Gender, Me.LayoutControlItem_Job, Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem_MilitaryService, Me.LayoutControlItem_SSN, Me.LayoutControlItem_Birthdate, Me.LayoutControlItem_Education, Me.EmptySpaceItem1, Me.EmptySpaceItem2, Me.LayoutControlItem_Ethnic, Me.LayoutControlItem_Disabled, Me.P, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.EmptySpaceItem3})
            Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlGroup1.Name = "Root"
            Me.LayoutControlGroup1.Size = New System.Drawing.Size(544, 681)
            Me.LayoutControlGroup1.Text = "Root"
            Me.LayoutControlGroup1.TextVisible = False
            '
            'LayoutControlItem_Relationship
            '
            Me.LayoutControlItem_Relationship.Control = Me.relation
            Me.LayoutControlItem_Relationship.CustomizationFormText = "Relationship"
            Me.LayoutControlItem_Relationship.Location = New System.Drawing.Point(0, 0)
            Me.LayoutControlItem_Relationship.Name = "LayoutControlItem_Relationship"
            Me.LayoutControlItem_Relationship.Size = New System.Drawing.Size(245, 24)
            Me.LayoutControlItem_Relationship.Text = "Relationship"
            Me.LayoutControlItem_Relationship.TextSize = New System.Drawing.Size(64, 13)
            '
            'LayoutControlItem_FormerName
            '
            Me.LayoutControlItem_FormerName.Control = Me.former
            Me.LayoutControlItem_FormerName.CustomizationFormText = "Former Name"
            Me.LayoutControlItem_FormerName.Location = New System.Drawing.Point(0, 48)
            Me.LayoutControlItem_FormerName.Name = "LayoutControlItem_FormerName"
            Me.LayoutControlItem_FormerName.Size = New System.Drawing.Size(183, 24)
            Me.LayoutControlItem_FormerName.Text = "Former Name"
            Me.LayoutControlItem_FormerName.TextSize = New System.Drawing.Size(64, 13)
            '
            'LayoutControlItem_Race
            '
            Me.LayoutControlItem_Race.Control = Me.race
            Me.LayoutControlItem_Race.CustomizationFormText = "Race"
            Me.LayoutControlItem_Race.Location = New System.Drawing.Point(0, 72)
            Me.LayoutControlItem_Race.Name = "LayoutControlItem_Race"
            Me.LayoutControlItem_Race.Size = New System.Drawing.Size(183, 24)
            Me.LayoutControlItem_Race.Text = "Race"
            Me.LayoutControlItem_Race.TextSize = New System.Drawing.Size(64, 13)
            '
            'LayoutControlItem_Gender
            '
            Me.LayoutControlItem_Gender.Control = Me.gender
            Me.LayoutControlItem_Gender.CustomizationFormText = "Gender"
            Me.LayoutControlItem_Gender.Location = New System.Drawing.Point(298, 72)
            Me.LayoutControlItem_Gender.Name = "LayoutControlItem_Gender"
            Me.LayoutControlItem_Gender.Size = New System.Drawing.Size(107, 24)
            Me.LayoutControlItem_Gender.Text = "Gender"
            Me.LayoutControlItem_Gender.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
            Me.LayoutControlItem_Gender.TextSize = New System.Drawing.Size(35, 13)
            Me.LayoutControlItem_Gender.TextToControlDistance = 5
            '
            'LayoutControlItem_Job
            '
            Me.LayoutControlItem_Job.Control = Me.job_name
            Me.LayoutControlItem_Job.CustomizationFormText = "Job"
            Me.LayoutControlItem_Job.Location = New System.Drawing.Point(0, 246)
            Me.LayoutControlItem_Job.Name = "LayoutControlItem_Job"
            Me.LayoutControlItem_Job.Size = New System.Drawing.Size(524, 415)
            Me.LayoutControlItem_Job.Text = "Job"
            Me.LayoutControlItem_Job.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem_Job.TextToControlDistance = 0
            Me.LayoutControlItem_Job.TextVisible = False
            '
            'LayoutControlItem1
            '
            Me.LayoutControlItem1.Control = Me.NameRecordControl1
            Me.LayoutControlItem1.CustomizationFormText = "Name"
            Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 24)
            Me.LayoutControlItem1.Name = "LayoutControlItem1"
            Me.LayoutControlItem1.Size = New System.Drawing.Size(524, 24)
            Me.LayoutControlItem1.Text = "Name"
            Me.LayoutControlItem1.TextSize = New System.Drawing.Size(64, 13)
            '
            'LayoutControlItem2
            '
            Me.LayoutControlItem2.Control = Me.EmailRecordControl1
            Me.LayoutControlItem2.CustomizationFormText = "LayoutControlItem2"
            Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 96)
            Me.LayoutControlItem2.Name = "LayoutControlItem2"
            Me.LayoutControlItem2.Size = New System.Drawing.Size(340, 24)
            Me.LayoutControlItem2.Text = "E-Mail"
            Me.LayoutControlItem2.TextSize = New System.Drawing.Size(64, 13)
            '
            'LayoutControlItem_MilitaryService
            '
            Me.LayoutControlItem_MilitaryService.Control = Me.PopupContainerEdit_Military
            Me.LayoutControlItem_MilitaryService.CustomizationFormText = "Military"
            Me.LayoutControlItem_MilitaryService.Location = New System.Drawing.Point(183, 48)
            Me.LayoutControlItem_MilitaryService.Name = "LayoutControlItem_MilitaryService"
            Me.LayoutControlItem_MilitaryService.Size = New System.Drawing.Size(222, 24)
            Me.LayoutControlItem_MilitaryService.Text = "Military"
            Me.LayoutControlItem_MilitaryService.TextSize = New System.Drawing.Size(64, 13)
            '
            'LayoutControlItem_SSN
            '
            Me.LayoutControlItem_SSN.Control = Me.ssn
            Me.LayoutControlItem_SSN.CustomizationFormText = "Social Security Number"
            Me.LayoutControlItem_SSN.Location = New System.Drawing.Point(0, 198)
            Me.LayoutControlItem_SSN.Name = "LayoutControlItem_SSN"
            Me.LayoutControlItem_SSN.Size = New System.Drawing.Size(183, 24)
            Me.LayoutControlItem_SSN.Text = "SSN"
            Me.LayoutControlItem_SSN.TextSize = New System.Drawing.Size(64, 13)
            '
            'LayoutControlItem_Birthdate
            '
            Me.LayoutControlItem_Birthdate.Control = Me.birthdate
            Me.LayoutControlItem_Birthdate.CustomizationFormText = "D.O.B."
            Me.LayoutControlItem_Birthdate.Location = New System.Drawing.Point(405, 72)
            Me.LayoutControlItem_Birthdate.Name = "LayoutControlItem_Birthdate"
            Me.LayoutControlItem_Birthdate.Size = New System.Drawing.Size(119, 24)
            Me.LayoutControlItem_Birthdate.Text = "D.O.B."
            Me.LayoutControlItem_Birthdate.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
            Me.LayoutControlItem_Birthdate.TextSize = New System.Drawing.Size(33, 13)
            Me.LayoutControlItem_Birthdate.TextToControlDistance = 5
            '
            'LayoutControlItem_Education
            '
            Me.LayoutControlItem_Education.Control = Me.education
            Me.LayoutControlItem_Education.CustomizationFormText = "Education"
            Me.LayoutControlItem_Education.Location = New System.Drawing.Point(273, 198)
            Me.LayoutControlItem_Education.Name = "LayoutControlItem_Education"
            Me.LayoutControlItem_Education.Size = New System.Drawing.Size(251, 24)
            Me.LayoutControlItem_Education.Text = "Education"
            Me.LayoutControlItem_Education.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
            Me.LayoutControlItem_Education.TextSize = New System.Drawing.Size(47, 13)
            Me.LayoutControlItem_Education.TextToControlDistance = 5
            '
            'EmptySpaceItem1
            '
            Me.EmptySpaceItem1.AllowHotTrack = False
            Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Location = New System.Drawing.Point(245, 0)
            Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
            Me.EmptySpaceItem1.Size = New System.Drawing.Size(279, 24)
            Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
            Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
            '
            'EmptySpaceItem2
            '
            Me.EmptySpaceItem2.AllowHotTrack = False
            Me.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Location = New System.Drawing.Point(263, 198)
            Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
            Me.EmptySpaceItem2.Size = New System.Drawing.Size(10, 24)
            Me.EmptySpaceItem2.Text = "EmptySpaceItem2"
            Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
            '
            'LayoutControlItem_Ethnic
            '
            Me.LayoutControlItem_Ethnic.Control = Me.LookupEdit_Ethnicity
            Me.LayoutControlItem_Ethnic.CustomizationFormText = "Ethnicity"
            Me.LayoutControlItem_Ethnic.Location = New System.Drawing.Point(183, 72)
            Me.LayoutControlItem_Ethnic.Name = "LayoutControlItem_Ethnic"
            Me.LayoutControlItem_Ethnic.Size = New System.Drawing.Size(115, 24)
            Me.LayoutControlItem_Ethnic.Text = "Ethnicity"
            Me.LayoutControlItem_Ethnic.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
            Me.LayoutControlItem_Ethnic.TextSize = New System.Drawing.Size(41, 13)
            Me.LayoutControlItem_Ethnic.TextToControlDistance = 5
            '
            'LayoutControlItem_Disabled
            '
            Me.LayoutControlItem_Disabled.Control = Me.CheckEdit_Disabled
            Me.LayoutControlItem_Disabled.CustomizationFormText = "Disabled"
            Me.LayoutControlItem_Disabled.Location = New System.Drawing.Point(405, 48)
            Me.LayoutControlItem_Disabled.Name = "LayoutControlItem_Disabled"
            Me.LayoutControlItem_Disabled.Size = New System.Drawing.Size(119, 24)
            Me.LayoutControlItem_Disabled.Text = "Disabled"
            Me.LayoutControlItem_Disabled.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem_Disabled.TextToControlDistance = 0
            Me.LayoutControlItem_Disabled.TextVisible = False
            '
            'P
            '
            Me.P.Control = Me.PopupContainerEdit_EverFiledBK
            Me.P.CustomizationFormText = "Ever Filed BK"
            Me.P.Location = New System.Drawing.Point(340, 96)
            Me.P.Name = "P"
            Me.P.Size = New System.Drawing.Size(184, 24)
            Me.P.Text = "Ever Filed BK"
            Me.P.TextSize = New System.Drawing.Size(64, 13)
            '
            'LayoutControlItem3
            '
            Me.LayoutControlItem3.Control = Me.PoaControl1
            Me.LayoutControlItem3.CustomizationFormText = "POA Information"
            Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 120)
            Me.LayoutControlItem3.Name = "LayoutControlItem3"
            Me.LayoutControlItem3.Size = New System.Drawing.Size(524, 78)
            Me.LayoutControlItem3.Text = "POA Information"
            Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem3.TextToControlDistance = 0
            Me.LayoutControlItem3.TextVisible = False
            '
            'LayoutControlItem4
            '
            Me.LayoutControlItem4.Control = Me.CheckEdit_ViewSSN
            Me.LayoutControlItem4.CustomizationFormText = "LayoutControlItem4"
            Me.LayoutControlItem4.Location = New System.Drawing.Point(183, 198)
            Me.LayoutControlItem4.Name = "LayoutControlItem4"
            Me.LayoutControlItem4.Size = New System.Drawing.Size(80, 24)
            Me.LayoutControlItem4.Text = "LayoutControlItem4"
            Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
            Me.LayoutControlItem4.TextToControlDistance = 0
            Me.LayoutControlItem4.TextVisible = False
            '
            'LayoutControlItem5
            '
            Me.LayoutControlItem5.Control = Me.PopupContainerEdit_FICO
            Me.LayoutControlItem5.CustomizationFormText = "FICO Score"
            Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 222)
            Me.LayoutControlItem5.Name = "LayoutControlItem5"
            Me.LayoutControlItem5.Size = New System.Drawing.Size(260, 24)
            Me.LayoutControlItem5.Text = "FICO Score"
            Me.LayoutControlItem5.TextSize = New System.Drawing.Size(64, 13)
            '
            'EmptySpaceItem3
            '
            Me.EmptySpaceItem3.AllowHotTrack = False
            Me.EmptySpaceItem3.CustomizationFormText = "EmptySpaceItem3"
            Me.EmptySpaceItem3.Location = New System.Drawing.Point(260, 222)
            Me.EmptySpaceItem3.Name = "EmptySpaceItem3"
            Me.EmptySpaceItem3.Size = New System.Drawing.Size(264, 24)
            Me.EmptySpaceItem3.Text = "EmptySpaceItem3"
            Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
            '
            'txCaseNumber
            '
            Me.txCaseNumber.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.txCaseNumber.Location = New System.Drawing.Point(94, 239)
            Me.txCaseNumber.Name = "txCaseNumber"
            Me.txCaseNumber.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
            Me.txCaseNumber.Properties.MaxLength = 50
            Me.txCaseNumber.Size = New System.Drawing.Size(102, 20)
            Me.txCaseNumber.TabIndex = 19
            '
            'LabelControl17
            '
            Me.LabelControl17.Location = New System.Drawing.Point(6, 242)
            Me.LabelControl17.Name = "LabelControl17"
            Me.LabelControl17.Size = New System.Drawing.Size(64, 13)
            Me.LabelControl17.TabIndex = 18
            Me.LabelControl17.Text = "Case Number"
            '
            'Page_Person
            '
            Me.Controls.Add(Me.LayoutControl1)
            Me.Name = "Page_Person"
            Me.Size = New System.Drawing.Size(544, 681)
            CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.LayoutControl1.ResumeLayout(False)
            CType(Me.PopupContainerEdit_FICO.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PopupContainerControl_FICO, System.ComponentModel.ISupportInitialize).EndInit()
            Me.PopupContainerControl_FICO.ResumeLayout(False)
            Me.PopupContainerControl_FICO.PerformLayout()
            CType(Me.CheckedListBoxControl_ScoreReasons, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.fico_score.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.no_fico_score_reason.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookUpEdit_CreditAgency.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_ViewSSN.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PoaControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PopupContainerControl_EverFiledBK, System.ComponentModel.ISupportInitialize).EndInit()
            Me.PopupContainerControl_EverFiledBK.ResumeLayout(False)
            Me.PopupContainerControl_EverFiledBK.PerformLayout()
            CType(Me.LookUpEdit_BankruptcyDistrict.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txCertificateNo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dtCertificateExpires.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dtCertificateExpires.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dtCertificateIssued.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dtCertificateIssued.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dtAuthLetter.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dtAuthLetter.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.luPmtCurrent.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dtBkDischargedDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dtBkDischargedDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.luEverFiledBKChapter.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dtEverFiledBKFiledDate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dtEverFiledBKFiledDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PopupContainerEdit_EverFiledBK.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PopupContainerControl_Military, System.ComponentModel.ISupportInitialize).EndInit()
            Me.PopupContainerControl_Military.ResumeLayout(False)
            Me.PopupContainerControl_Military.PerformLayout()
            CType(Me.milDependent.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.milGrade.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.milService.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.milStatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.relation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.birthdate.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.birthdate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.education.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_Disabled.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.former.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.race.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.job_name, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ssn.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.gender.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmailRecordControl1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LookupEdit_Ethnicity.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PopupContainerEdit_Military.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_Relationship, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_FormerName, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_Race, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_Gender, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_Job, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_MilitaryService, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_SSN, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_Birthdate, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_Education, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_Ethnic, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem_Disabled, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.P, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.txCaseNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

        Friend WithEvents fico_score As DevExpress.XtraEditors.SpinEdit
        Friend WithEvents job_name As JobControl
        Friend WithEvents relation As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents former As DevExpress.XtraEditors.TextEdit
        Friend WithEvents race As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents ssn As DebtPlus.Data.Controls.ssn
        Friend WithEvents education As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents gender As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
        Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Friend WithEvents CheckEdit_Disabled As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents LookUpEdit_CreditAgency As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents NameRecordControl1 As DebtPlus.Data.Controls.NameRecordControl
        Friend WithEvents birthdate As DevExpress.XtraEditors.DateEdit
        Friend WithEvents EmailRecordControl1 As DebtPlus.Data.Controls.EmailRecordControl
        Friend WithEvents no_fico_score_reason As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LookupEdit_Ethnicity As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LayoutControlItem_Relationship As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem_FormerName As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem_Race As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem_Gender As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem_MilitaryService As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem_SSN As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem_Birthdate As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem_Education As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents LayoutControlItem_Ethnic As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LayoutControlItem_Disabled As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents PopupContainerEdit_Military As DevExpress.XtraEditors.PopupContainerEdit
        Friend WithEvents PopupContainerControl_Military As DevExpress.XtraEditors.PopupContainerControl
        Friend WithEvents milDependent As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents milGrade As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents milService As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents milStatus As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents PopupContainerControl_EverFiledBK As DevExpress.XtraEditors.PopupContainerControl
        Friend WithEvents luEverFiledBKChapter As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents dtEverFiledBKFiledDate As DevExpress.XtraEditors.DateEdit
        Friend WithEvents PopupContainerEdit_EverFiledBK As DevExpress.XtraEditors.PopupContainerEdit
        Friend WithEvents P As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents dtBkDischargedDate As DevExpress.XtraEditors.DateEdit
        Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LayoutControlItem_Job As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents dtAuthLetter As DevExpress.XtraEditors.DateEdit
        Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents luPmtCurrent As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents PoaControl1 As DebtPlus.UI.Client.controls.POAControl
        Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents CheckEdit_ViewSSN As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents PopupContainerEdit_FICO As DevExpress.XtraEditors.PopupContainerEdit
        Friend WithEvents PopupContainerControl_FICO As DevExpress.XtraEditors.PopupContainerControl
        Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents CheckedListBoxControl_ScoreReasons As DevExpress.XtraEditors.CheckedListBoxControl
        Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
        Friend WithEvents EmptySpaceItem3 As DevExpress.XtraLayout.EmptySpaceItem
        Friend WithEvents txCertificateNo As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents dtCertificateExpires As DevExpress.XtraEditors.DateEdit
        Friend WithEvents dtCertificateIssued As DevExpress.XtraEditors.DateEdit
        Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents LookUpEdit_BankruptcyDistrict As DevExpress.XtraEditors.LookUpEdit
        Friend WithEvents txCaseNumber As DevExpress.XtraEditors.TextEdit
        Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    End Class
End Namespace
