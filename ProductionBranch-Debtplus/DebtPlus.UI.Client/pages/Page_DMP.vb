#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region
Imports DebtPlus.Svc.Debt
Imports DebtPlus.Svc.DataSets
Imports DevExpress.XtraEditors.Controls
Imports DebtPlus.UI.Client.controls
Imports DebtPlus.Events
Imports DebtPlus.UI.Client.forms
Imports DevExpress.XtraLayout.Utils
Imports DebtPlus.LINQ
Imports System.Linq

Namespace pages

    Friend Class Page_DMP
        Inherits ControlBaseClient
        Implements System.ComponentModel.INotifyPropertyChanged

        ''' <summary>
        ''' Implement the property changed to reflect that the ACH data record was modified
        ''' </summary>
        Public Event PropertyChanged As System.ComponentModel.PropertyChangedEventHandler Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged
        Protected Sub RaisePropertyChanged(e As System.ComponentModel.PropertyChangedEventArgs)
            RaiseEvent PropertyChanged(Me, e)
        End Sub

        Protected Overridable Sub OnPropertyChanged(e As System.ComponentModel.PropertyChangedEventArgs)
            RaisePropertyChanged(e)
        End Sub

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler Context.ClientDs.clientRecord.PropertyChanged, AddressOf clientRecord_PropertyChanged
            AddHandler lk_bankruptcy_class.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler lk_active_status.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            AddHandler lk_active_status.EditValueChanging, AddressOf lk_active_status_EditValueChanging
            AddHandler lk_active_status.EditValueChanged, AddressOf lk_active_status_EditValueChanged
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler Context.ClientDs.clientRecord.PropertyChanged, AddressOf clientRecord_PropertyChanged
            RemoveHandler lk_bankruptcy_class.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler lk_active_status.EditValueChanging, AddressOf DebtPlus.Data.Validation.LookUpEdit_ActiveTest
            RemoveHandler lk_active_status.EditValueChanging, AddressOf lk_active_status_EditValueChanging
            RemoveHandler lk_active_status.EditValueChanged, AddressOf lk_active_status_EditValueChanged
        End Sub

        ''' <summary>
        ''' Update the display if the reserved amount is changed
        ''' </summary>
        Private Sub clientRecord_PropertyChanged(ByVal Sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)

            If (New String() {"reserved_in_trust", "reserved_in_trust_cutoff"}).Contains(e.PropertyName, StringComparer.InvariantCultureIgnoreCase) Then
                ShowReservedInTrust()
            End If

        End Sub

        ''' <summary>
        ''' Save the table changes when needed
        ''' </summary>
        Public Overrides Sub SaveForm(bc As DebtPlus.LINQ.BusinessContext)

            ' Update the DMP status date
            Dim status As String = DebtPlus.Utils.Nulls.v_String(lk_client_status.EditValue)
            If Context.ClientDs.clientRecord.client_status <> status Then
                Context.ClientDs.clientRecord.client_status = status
                Context.ClientDs.clientRecord.client_status_date = BusinessContext.getdate

                If status = "DMP" Then
                    Context.ClientDs.clientRecord.dmp_status_date = Context.ClientDs.clientRecord.client_status_date
                End If

                OnPropertyChanged(New System.ComponentModel.PropertyChangedEventArgs("client_status"))
            End If

            ' Update the active status information
            status = DebtPlus.Utils.Nulls.v_String(lk_active_status.EditValue)
            If Context.ClientDs.clientRecord.active_status <> status Then

                ' If the new status is inactive and we have a drop reason then drop the debts for this client.
                If status = "I" AndAlso Context.ClientDs.clientRecord.drop_reason.HasValue Then

                    ' Drop the debts for this client in the database.
                    bc.xpr_drop_client(Context.ClientDs.clientRecord.Id, Context.ClientDs.clientRecord.drop_reason, Context.ClientDs.clientRecord.drop_reason_other)

                    ' Mark the debts as having been dropped
                    For Each Debt As DebtRecord In Context.DebtRecords
                        If Debt.IsActive Then
                            Dim dropReason As System.Nullable(Of Int32) = DebtPlus.Utils.Nulls.v_Int32(Debt.drop_reason)
                            Dim dropReasonDate As System.Nullable(Of DateTime) = DebtPlus.Utils.Nulls.v_DateTime(Debt.drop_reason_date)
                            If Not dropReason.HasValue AndAlso Not dropReasonDate.HasValue Then
                                Debt.drop_reason = Context.ClientDs.clientRecord.drop_reason
                                Debt.drop_date = Context.ClientDs.clientRecord.drop_date
                            End If
                        End If
                    Next
                End If

                ' Update the active status and date then tell those interested that we changed the active status
                Context.ClientDs.clientRecord.active_status = status
                Context.ClientDs.clientRecord.active_status_date = BusinessContext.getdate
                OnPropertyChanged(New System.ComponentModel.PropertyChangedEventArgs("active_status"))
            End If

            ' Update the record with the control contents
            Context.ClientDs.clientRecord.disbursement_date = DebtPlus.Utils.Nulls.v_Int32(spn_disbursement_date.EditValue).GetValueOrDefault()
            Context.ClientDs.clientRecord.start_date = DebtPlus.Utils.Nulls.v_DateTime(dt_start_date.EditValue)
            Context.ClientDs.clientRecord.bankruptcy_class = DebtPlus.Utils.Nulls.v_Int32(lk_bankruptcy_class.EditValue).GetValueOrDefault()
            Context.ClientDs.clientRecord.stack_proration = chk_stack_proration.Checked
            Context.ClientDs.clientRecord.hold_disbursements = chk_hold_disbursements.Checked
            Context.ClientDs.clientRecord.mortgage_problems = chk_mortgage_problems.Checked
        End Sub

        ''' <summary>
        ''' Load the page with the information from the clients table
        ''' </summary>
        Public Overrides Sub ReadForm(bc As DebtPlus.LINQ.BusinessContext)

            UnRegisterHandlers()
            Try
                lk_bankruptcy_class.Properties.DataSource = DebtPlus.LINQ.Cache.BankruptcyClassType.getList()
                lk_client_status.Properties.DataSource = DebtPlus.LINQ.Cache.ClientStatusType.getList()
                lk_active_status.Properties.DataSource = DebtPlus.LINQ.Cache.ActiveStatusType.getList()

                ' Set the controls to the field values
                lk_client_status.EditValue = Context.ClientDs.clientRecord.client_status
                spn_disbursement_date.EditValue = Context.ClientDs.clientRecord.disbursement_date
                dt_start_date.EditValue = Context.ClientDs.clientRecord.start_date
                lk_bankruptcy_class.EditValue = Context.ClientDs.clientRecord.bankruptcy_class
                lk_active_status.EditValue = Context.ClientDs.clientRecord.active_status
                chk_stack_proration.Checked = Context.ClientDs.clientRecord.stack_proration
                chk_hold_disbursements.Checked = Context.ClientDs.clientRecord.hold_disbursements
                chk_mortgage_problems.Checked = Context.ClientDs.clientRecord.mortgage_problems

                ' Show the drop reason and drop reason date
                ShowDropReason()
                ShowReservedInTrust()

                ' Program months
                lbl_program_months.Text = String.Format("{0:n0}", Context.ClientDs.clientRecord.program_months)

                ' Held in trust
                lbl_held_in_trust.Text = String.Format("{0:c}", Context.ClientDs.clientRecord.held_in_trust)

                ' Total disbursement factors
                Dim TotalPayments As Decimal = 0D
                Dim TotalDisbursementFactor As Decimal = 0D
                For Each Debt As DebtRecord In Context.DebtRecords
                    If Debt.proratable_IsActive Then
                        TotalDisbursementFactor += DebtPlus.Utils.Nulls.DDec(Debt.display_disbursement_factor)
                        TotalPayments += DebtPlus.Utils.Nulls.DDec(Debt.total_payments)
                    End If
                Next

                lbl_total_disb_factors.Text = String.Format("{0:c}", TotalDisbursementFactor)
                lbl_total_payments.Text = String.Format("{0:c}", TotalPayments)

                ' Total deposits
                Dim TotalDeposits As Decimal = Context.ClientDs.colClientDeposits.Where(Function(s) Not s.one_time).Sum(Function(s) s.deposit_amount)
                lbl_total_deposits.Text = String.Format("{0:c}", TotalDeposits)

                Try
                    ' Find the amount that is pending to be deposited into the trust. These items are created but not posted
                    ' because they are in batches that are presently being worked.
                    Dim colUnPostedDeposits As System.Collections.Generic.List(Of DebtPlus.LINQ.deposit_batch_detail) = _
                        (
                            From cd In bc.deposit_batch_details
                            Join b In bc.deposit_batch_ids On b.Id Equals cd.deposit_batch_id
                            Where cd.client = Context.ClientDs.ClientId AndAlso b.batch_type = "CL" AndAlso cd.ok_to_post AndAlso b.date_posted Is Nothing
                            Select cd
                        ).ToList()

                    Dim depositAmount As Decimal = colUnPostedDeposits.Sum(Function(s) s.amount)
                    lbl_deposit_in_trust.Text = String.Format("{0:c}", depositAmount)

                Catch ex As SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading deposit in trust")
                End Try

                ' Display the change date for the active status
                Dim active_status_date As System.Nullable(Of DateTime) = Context.ClientDs.clientRecord.active_status_date
                If Context.ClientDs.clientRecord.active_status_date.HasValue Then
                    lbl_active_status_date.Text = String.Format("as of {0:d}", Context.ClientDs.clientRecord.active_status_date.Value)
                    lbl_active_status_date.Visible = True
                Else
                    lbl_active_status_date.Visible = False
                End If

            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Process a change in the active status for the client
        ''' </summary>
        Private Sub lk_active_status_EditValueChanged(ByVal sender As Object, ByVal e As EventArgs)
            lbl_active_status_date.Text = String.Format("as of {0:d}", System.DateTime.Now.Date)
            lbl_active_status_date.Visible = True
        End Sub

        ''' <summary>
        ''' Process a change in the active status
        ''' </summary>
        Private Sub lk_active_status_EditValueChanging(ByVal sender As Object, ByVal e As ChangingEventArgs)

            ' If the new status is not inactive then clear the drop reasons.
            If e.NewValue <> "I" Then
                Context.ClientDs.clientRecord.drop_reason = Nothing
                Context.ClientDs.clientRecord.drop_date = Nothing
                ShowDropReason()
                Return
            End If

            ' If changing from active to inactive then ask for a drop reason.
            If e.OldValue <> "I" Then
                Using frm As New Form_DropReason(Context)
                    If frm.ShowDialog() <> DialogResult.OK Then
                        e.Cancel = True
                        Return
                    End If

                    Dim DropReason As System.Nullable(Of Int32) = DebtPlus.Utils.Nulls.v_Int32(frm.lk_Drop_Reason.EditValue)
                    If Not DropReason.HasValue Then
                        e.Cancel = True
                        Return
                    End If

                    ' Save the drop reason. This will set the drop date when we update the database.
                    Context.ClientDs.clientRecord.drop_reason = DropReason.Value
                    Context.ClientDs.clientRecord.drop_date = DateTime.Now.Date
                End Using
            End If

            ' Show the new drop reason and date
            ShowDropReason()
        End Sub

        ''' <summary>
        ''' Display the amount reserved in the trust
        ''' </summary>
        Private Sub ShowReservedInTrust()

            ' Avoid cross-thread issues in our call
            If InvokeRequired Then
                BeginInvoke(New MethodInvoker(AddressOf ShowReservedInTrust))
                Return
            End If

            ' Reserved in trust
            Dim reserved_in_trust As Decimal = Context.ClientDs.clientRecord.reserved_in_trust
            Dim reserved_in_trust_cutoff As System.Nullable(Of DateTime) = Context.ClientDs.clientRecord.reserved_in_trust_cutoff

            If Not reserved_in_trust_cutoff.HasValue OrElse reserved_in_trust_cutoff.Value < System.DateTime.Now.Date.AddDays(1) OrElse reserved_in_trust <= 0D Then
                reserved_in_trust = 0D
            End If

            lbl_reserved_in_trust.Text = String.Format("{0:c}", reserved_in_trust)
        End Sub

        ''' <summary>
        ''' Display the drop reason
        ''' </summary>
        Private Sub ShowDropReason()

            Dim drop_date As System.Nullable(Of DateTime) = Context.ClientDs.clientRecord.drop_date
            If drop_date.HasValue Then
                LayoutControlItem_drop_reason.Visibility = LayoutVisibility.Always
                lbl_drop_date.Text = String.Format("{0:d}", drop_date)
            Else
                LayoutControlItem_drop_reason.Visibility = LayoutVisibility.OnlyInCustomization
            End If

            ' Drop reason
            Dim drop_reason_id As System.Nullable(Of Int32) = Context.ClientDs.clientRecord.drop_reason
            If drop_reason_id.HasValue Then
                Dim txt As String = Drop_Reason_text(drop_reason_id.Value)
                If Not String.IsNullOrEmpty(txt) Then
                    lbl_Drop_Reason.Text = txt
                    LayoutControlItem_drop_reason.Visibility = LayoutVisibility.Always
                    Return
                End If
            End If

            LayoutControlItem_drop_reason.Visibility = LayoutVisibility.OnlyInCustomization
        End Sub

        ''' <summary>
        ''' Display the textual drop reason
        ''' </summary>
        Private Function Drop_Reason_text(ByVal id As Int32) As String
            Dim reason As DebtPlus.LINQ.drop_reason = DebtPlus.LINQ.Cache.drop_reason.getList().Find(Function(d) d.Id = id)
            If reason IsNot Nothing Then
                Return reason.description
            End If
            Return String.Empty
        End Function
    End Class
End Namespace
