#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.UI.Client.controls
Imports System.Linq
Imports DebtPlus.LINQ

Namespace pages
    Friend Class Page_Assets
        Inherits ControlBaseClient

        Private Class RecordLayout
            Public Sub New()
            End Sub
            Public Property assetRecord As DebtPlus.LINQ.asset = Nothing
            Public Property assetIdRecord As DebtPlus.LINQ.asset_id = Nothing
        End Class

        Private colRecords As System.Collections.Generic.List(Of RecordLayout) = Nothing

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Public Overrides Sub ReadForm(bc As DebtPlus.LINQ.BusinessContext)

            ' If there is no collection of records for the grid then we need to load it.
            If colRecords Is Nothing Then
                Try
                    RepositoryItemLookUpEdit_frequency.DataSource = DebtPlus.LINQ.Cache.PayFrequencyType.getList()
                    colRecords = DebtPlus.LINQ.Cache.asset_id.getList().Select(Function(s) New RecordLayout() With {.assetIdRecord = s}).ToList()

                    ' Ensure that we have a collection of the client assets now.
                    If Context.ClientDs.colAssets Is Nothing Then
                        Context.ClientDs.colAssets = bc.assets.Where(Function(s) s.client = Context.ClientDs.ClientId).ToList()
                    End If

                    ' Merge the assets into the descriptions
                    Dim ieRecords As System.Collections.IEnumerator = colRecords.GetEnumerator()
                    Do While ieRecords.MoveNext()
                        Dim layoutRecord As RecordLayout = DirectCast(ieRecords.Current, RecordLayout)

                        ' Find the asset record in the collection. If there is not one then create a new item but don't
                        ' update the database to insert it. Wait for the save operation to do that.
                        Dim qAsset As DebtPlus.LINQ.asset = Context.ClientDs.colAssets.Find(Function(s) s.asset_id = layoutRecord.assetIdRecord.Id)
                        If qAsset Is Nothing Then
                            qAsset = DebtPlus.LINQ.Factory.Manufacture_asset()
                            qAsset.client = Context.ClientDs.ClientId
                            qAsset.asset_id = layoutRecord.assetIdRecord.Id
                            Context.ClientDs.colAssets.Add(qAsset)
                        End If

                        layoutRecord.assetRecord = qAsset
                    Loop

                Catch ex As System.Data.SqlClient.SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
                End Try

                ' Bind the grid to the display list
                GridControl1.DataSource = colRecords
            End If
        End Sub

        Public Overrides Sub SaveForm(bc As DebtPlus.LINQ.BusinessContext)

            ' Add the items that are new to the list where possible
            For Each newRecord As DebtPlus.LINQ.asset In Context.ClientDs.colAssets.Where(Function(s) s.Id < 1 AndAlso s.amount > 0D)
                bc.assets.InsertOnSubmit(newRecord)
            Next

            ' Submit the changes that were made to the database
            Try
                bc.SubmitChanges()
            Catch ex As System.Data.SqlClient.SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
            End Try

        End Sub
    End Class
End Namespace