﻿Imports DebtPlus.UI.Client.controls

Namespace pages

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Page_Notices
        Inherits ControlBaseClient

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.RichEditControl1 = New DevExpress.XtraRichEdit.RichEditControl()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'RichEditControl1
            '
            Me.RichEditControl1.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple
            Me.RichEditControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.RichEditControl1.Location = New System.Drawing.Point(0, 0)
            Me.RichEditControl1.Name = "RichEditControl1"
            Me.RichEditControl1.ReadOnly = True
            Me.RichEditControl1.ShowCaretInReadOnly = False
            Me.RichEditControl1.Size = New System.Drawing.Size(576, 296)
            Me.RichEditControl1.TabIndex = 0
            Me.RichEditControl1.Text = "Reading notice information from database. Please wait."
            '
            'Page_Notices
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.RichEditControl1)
            Me.Name = "Page_Notices"
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents RichEditControl1 As DevExpress.XtraRichEdit.RichEditControl
    End Class
End Namespace