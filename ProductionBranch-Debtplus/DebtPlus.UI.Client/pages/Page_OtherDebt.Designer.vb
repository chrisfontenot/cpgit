﻿Imports DebtPlus.UI.Client.controls

Namespace pages

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Page_OtherDebt
        Inherits MyGridControlClient

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.GridColumn_ID = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_creditor_name = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_account_number = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_balance = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_payment = New DevExpress.XtraGrid.Columns.GridColumn
            Me.GridColumn_interest_rate = New DevExpress.XtraGrid.Columns.GridColumn
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridControl1
            '
            Me.GridControl1.EmbeddedNavigator.Name = ""
            Me.GridControl1.Location = New System.Drawing.Point(3, 3)
            Me.GridControl1.MainView = Me.GridView1
            ''Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(400, 200)
            Me.GridControl1.TabIndex = 0
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridColumn_ID
            '
            Me.GridColumn_ID.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_ID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_ID.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_ID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_ID.Caption = "ID"
            Me.GridColumn_ID.CustomizationCaption = "Record ID"
            Me.GridColumn_ID.DisplayFormat.FormatString = "f0"
            Me.GridColumn_ID.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_ID.FieldName = "other_debt"
            Me.GridColumn_ID.GroupFormat.FormatString = "f0"
            Me.GridColumn_ID.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_ID.Name = "GridColumn_ID"
            Me.GridColumn_ID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            '
            'GridColumn_creditor_name
            '
            Me.GridColumn_creditor_name.Caption = "Creditor"
            Me.GridColumn_creditor_name.CustomizationCaption = "Creditor Name"
            Me.GridColumn_creditor_name.FieldName = "creditor_name"
            Me.GridColumn_creditor_name.Name = "GridColumn_creditor_name"
            Me.GridColumn_creditor_name.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText
            Me.GridColumn_creditor_name.Visible = True
            Me.GridColumn_creditor_name.VisibleIndex = 0
            Me.GridColumn_creditor_name.Width = 256
            Me.GridColumn_creditor_name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            '
            'GridColumn_account_number
            '
            Me.GridColumn_account_number.Caption = "Account"
            Me.GridColumn_account_number.CustomizationCaption = "Account Number"
            Me.GridColumn_account_number.FieldName = "account_number"
            Me.GridColumn_account_number.Name = "GridColumn_account_number"
            Me.GridColumn_account_number.Visible = True
            Me.GridColumn_account_number.VisibleIndex = 1
            Me.GridColumn_account_number.Width = 104
            Me.GridColumn_account_number.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            '
            'GridColumn_balance
            '
            Me.GridColumn_balance.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_balance.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_balance.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_balance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_balance.Caption = "Balance"
            Me.GridColumn_balance.CustomizationCaption = "Account Balance"
            Me.GridColumn_balance.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_balance.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_balance.FieldName = "balance"
            Me.GridColumn_balance.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_balance.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_balance.Name = "GridColumn_balance"
            Me.GridColumn_balance.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value
            Me.GridColumn_balance.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_balance.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_balance.Visible = True
            Me.GridColumn_balance.VisibleIndex = 2
            Me.GridColumn_balance.Width = 77
            Me.GridColumn_balance.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            '
            'GridColumn_payment
            '
            Me.GridColumn_payment.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_payment.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_payment.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_payment.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_payment.Caption = "Payment"
            Me.GridColumn_payment.CustomizationCaption = "Monthly Payment"
            Me.GridColumn_payment.DisplayFormat.FormatString = "{0:c}"
            Me.GridColumn_payment.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_payment.FieldName = "payment"
            Me.GridColumn_payment.GroupFormat.FormatString = "{0:c}"
            Me.GridColumn_payment.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_payment.Name = "GridColumn_payment"
            Me.GridColumn_payment.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value
            Me.GridColumn_payment.SummaryItem.DisplayFormat = "{0:c}"
            Me.GridColumn_payment.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            Me.GridColumn_payment.Visible = True
            Me.GridColumn_payment.VisibleIndex = 3
            Me.GridColumn_payment.Width = 72
            Me.GridColumn_payment.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            '
            'GridColumn_interest_rate
            '
            Me.GridColumn_interest_rate.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_interest_rate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_interest_rate.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_interest_rate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            Me.GridColumn_interest_rate.Caption = "A.P.R."
            Me.GridColumn_interest_rate.CustomizationCaption = "Interest Rate"
            Me.GridColumn_interest_rate.DisplayFormat.FormatString = "{0:p}"
            Me.GridColumn_interest_rate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_interest_rate.FieldName = "interest_rate"
            Me.GridColumn_interest_rate.GroupFormat.FormatString = "{0:p}"
            Me.GridColumn_interest_rate.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_interest_rate.Name = "GridColumn_interest_rate"
            Me.GridColumn_interest_rate.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value
            Me.GridColumn_interest_rate.Visible = True
            Me.GridColumn_interest_rate.VisibleIndex = 4
            Me.GridColumn_interest_rate.Width = 63
            Me.GridColumn_interest_rate.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True
            '
            'GridControl1
            '
            Me.GridControl1.Anchor = System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left
            Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.GridControl1.Location = New System.Drawing.Point(0, 0)
            Me.GridControl1.Size = New System.Drawing.Size(576, 248)
            '
            'GridView1
            '
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_account_number, Me.GridColumn_balance, Me.GridColumn_creditor_name, Me.GridColumn_ID, Me.GridColumn_interest_rate, Me.GridColumn_payment})
            Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn_creditor_name, DevExpress.Data.ColumnSortOrder.Ascending)})
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsView.ShowFooter = True
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowIndicator = False
            '
            'Page_OtherDebt
            '
            Me.Controls.Add(Me.GridControl1)
            Me.Name = "Page_OtherDebt"
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
        End Sub
        Protected Friend WithEvents GridColumn_ID As DevExpress.XtraGrid.Columns.GridColumn
        Protected Friend WithEvents GridColumn_creditor_name As DevExpress.XtraGrid.Columns.GridColumn
        Protected Friend WithEvents GridColumn_account_number As DevExpress.XtraGrid.Columns.GridColumn
        Protected Friend WithEvents GridColumn_balance As DevExpress.XtraGrid.Columns.GridColumn
        Protected Friend WithEvents GridColumn_payment As DevExpress.XtraGrid.Columns.GridColumn
        Protected Friend WithEvents GridColumn_interest_rate As DevExpress.XtraGrid.Columns.GridColumn
    End Class
End Namespace