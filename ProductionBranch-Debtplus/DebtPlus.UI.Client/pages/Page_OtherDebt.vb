#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.Svc.DataSets
Imports DebtPlus.UI.Client.forms
Imports DebtPlus.Events
Imports DebtPlus.LINQ
Imports System.Linq

Namespace pages

    Friend Class Page_OtherDebt
        Private bc As BusinessContext = Nothing

        ''' <summary>
        ''' Event raised when the collection of debts is changed. The bottom-line form
        ''' would need to be updated accordingly.
        ''' </summary>
        ''' <remarks></remarks>
        Public Event Changed As System.EventHandler(Of System.EventArgs)

        Protected Sub RaiseChanged(e As System.EventArgs)
            RaiseEvent Changed(Me, e)
        End Sub

        Protected Overridable Sub OnChanged(e As System.EventArgs)
            RaiseChanged(e)
        End Sub

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            EnableMenus = True
        End Sub

        Public Overrides Sub ReadForm(bc As DebtPlus.LINQ.BusinessContext)
            Me.bc = bc
            GridControl1.DataSource = Context.ClientDs.colOtherDebts
            GridControl1.RefreshDataSource()
            GridView1.BestFitColumns()
        End Sub

        Protected Overrides Sub onEditItem(ByVal obj As Object)

            ' Find the record
            Dim record As client_other_debt = TryCast(obj, client_other_debt)
            If record Is Nothing Then
                Return
            End If

            Using frm As New Form_OtherDebt(record)
                If frm.ShowDialog() <> DialogResult.OK Then
                    Return
                End If
            End Using

            ' Change the record as needed when the OK button is pressed
            bc.SubmitChanges()
            OnChanged(System.EventArgs.Empty)

            GridControl1.RefreshDataSource()
            GridView1.RefreshData()
        End Sub

        Protected Overrides Sub OnDeleteItem(ByVal obj As Object)

            ' Find the record
            Dim record As client_other_debt = TryCast(obj, client_other_debt)
            If record Is Nothing Then
                Return
            End If

            ' Delete the row from the database
            bc.client_other_debts.DeleteOnSubmit(record)
            bc.SubmitChanges()
            Context.ClientDs.colOtherDebts.Remove(record)

            OnChanged(System.EventArgs.Empty)
            GridControl1.RefreshDataSource()
            GridView1.RefreshData()
        End Sub

        Protected Overrides Sub OnCreateItem()

            ' Find the record
            Dim record As client_other_debt = DebtPlus.LINQ.Factory.Manufacture_client_other_debt(Context.ClientDs.ClientId)

            Using frm As New Form_OtherDebt(record)
                If frm.ShowDialog() <> DialogResult.OK Then
                    Return
                End If
            End Using

            ' Update the table with the changes.
            bc.client_other_debts.InsertOnSubmit(record)
            Context.ClientDs.colOtherDebts.Add(record)

            bc.SubmitChanges()
            GridControl1.RefreshDataSource()
            GridView1.RefreshData()
            OnChanged(System.EventArgs.Empty)
        End Sub
    End Class
End Namespace