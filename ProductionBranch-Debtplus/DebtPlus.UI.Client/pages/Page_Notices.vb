﻿#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Imports DebtPlus.UI.Client.controls
Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Text

Namespace pages

    Friend Class Page_Notices
        Inherits ControlBaseClient

        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        Dim privateDisplayedState As Int32 = Int32.MinValue

        Public Overrides Sub ReadForm(bc As DebtPlus.LINQ.BusinessContext)
            MyBase.ReadForm(bc)

            ' If the state differs from what is shown then update the display
            Dim clientState As Int32 = Context.ClientDs.clientAddress.state
            If clientState <> privateDisplayedState Then
                privateDisplayedState = clientState

                ' Obtain the name of the client's state
                Dim DisplayedState As String = String.Empty
                Dim item As DebtPlus.LINQ.state = DebtPlus.LINQ.Cache.state.getList().Find(Function(s) s.Id = privateDisplayedState)
                If item IsNot Nothing Then
                    DisplayedState = item.Name
                End If

                Dim sb As New StringBuilder
                Try
                    Using cn As New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        cn.Open()

                        Using cmd As SqlCommand = New SqlCommand
                            cmd.Connection = cn
                            cmd.CommandText = "xpr_StateNotices_Select"
                            cmd.CommandType = CommandType.StoredProcedure
                            cmd.Parameters.Add("@state", SqlDbType.Int).Value = privateDisplayedState

                            Using rd As SqlDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection Or CommandBehavior.SingleResult)
                                If rd IsNot Nothing Then
                                    Do While rd.Read()
                                        Dim Ordinal As Int32

                                        ' Start with the message type
                                        sb.Append("<tr>")

                                        ' Append the section name as a bold field across both columns so that it is out-dented
                                        sb.Append("<td colspan=""2"" align=""left"" valign=""top"">")
                                        Ordinal = rd.GetOrdinal("StateMessageType")

                                        Using trans As New DebtPlus.Svc.DataSets.ValueDescripitions()
                                            Dim MessageType As Int32 = Convert.ToInt32(rd.GetValue(Ordinal))
                                            Dim MessageTypeString As String = trans.GetStateMessageTypeDescription(MessageType)

                                            ' This needs to be in bold
                                            sb.Append("<b>")
                                            sb.Append(ToHTML(MessageTypeString))
                                            Ordinal = rd.GetOrdinal("effective")
                                            If Ordinal >= 0 AndAlso Not rd.IsDBNull(Ordinal) Then
                                                Dim DateUpdated As DateTime = Convert.ToDateTime(rd.GetValue(Ordinal))
                                                sb.AppendFormat(" (effective {0:d})", DateUpdated)
                                            End If
                                            sb.Append("</b>")
                                        End Using

                                        sb.Append("</td>")
                                        sb.Append("</tr>")

                                        ' Append the message text in a section that will wrap while it is indented
                                        sb.Append("<tr>")
                                        sb.Append("<td>&nbsp;</td>")

                                        sb.Append("<td align=""left"" valign=""top"">")
                                        Dim MessageString As String = GetNoteText(rd).Trim()
                                        If MessageString = String.Empty Then
                                            MessageString = "Nothing was mentioned"
                                        End If
                                        sb.Append(ToHTML(MessageString))

                                        sb.Append("</td>")
                                        sb.Append("</tr>")

                                        ' Add a blank row between sections
                                        sb.Append("<tr colspan=""2""><td><font size=""1"">&nbsp;</font></td></tr>")
                                    Loop

                                    ' If there is text then wrap it with the proper table definiton.
                                    If sb.Length > 0 Then
                                        sb.Insert(0, "<table width=""100%"">")
                                        sb.Append("</table>")
                                    End If
                                End If
                            End Using
                        End Using
                    End Using

                Catch ex As SqlException
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading note text")
                End Try

                ' If there are no notices then say so. Just don't leave the area blank.
                If sb.Length = 0 Then
                    sb.Append("There are no notices for the client's state at this time.")
                End If

                ' Include the state name as the first item shown.
                If DisplayedState <> String.Empty Then
                    sb.Insert(0, String.Format("<font size=""5""><b>{0}</b></font><br/>", sbHTML(DisplayedState).ToString()))
                End If

                ' Wrap the text string with a valid HTML document header and body.
                sb.Insert(0, "<html><head><title></title></head><body>")
                sb.Append("</body></html>")

                RichEditControl1.HtmlText = sb.ToString()
            End If
        End Sub

        Private Function GetNoteText(ByRef rd As SqlDataReader) As String
            Dim Ordinal As Int32 = rd.GetOrdinal("Details")
            Dim sb As New StringBuilder

            Const BufferSize As Long = 4096
            Dim InputBlock(BufferSize - 1) As Char
            Dim OffsetValue As Long = 0

            Dim BytesRead As Long = rd.GetChars(Ordinal, OffsetValue, InputBlock, 0, BufferSize)
            Do While BytesRead = BufferSize
                sb.Append(InputBlock)
                OffsetValue += BytesRead
                BytesRead = rd.GetChars(Ordinal, OffsetValue, InputBlock, 0, BufferSize)
            Loop

            If BytesRead > 0 Then
                sb.Append(InputBlock, 0, Convert.ToInt32(BytesRead))
            End If

            Return sb.ToString()
        End Function

        Private Function sbHTML(ByVal InputString As String) As StringBuilder
            Dim sb As New StringBuilder(InputString)

            sb.Replace("&", "&amp;")
            sb.Replace("<", "&lt;")
            sb.Replace(">", "&gt;")
            sb.Replace("""", "&quot;")

            sb.Replace(ControlChars.CrLf, "<br/>")

            Return sb
        End Function

        Private Function ToHTML(ByVal InputString As String) As String

            Dim sb As StringBuilder = sbHTML(InputString)

            ' Change the font to size 3
            sb.Insert(0, "<font size=""3"">")
            sb.Append("</font>")

            Return sb.ToString()
        End Function
    End Class
End Namespace