#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict Off

Imports System.Data.SqlClient
Imports System.Linq
Imports System.Text
Imports DebtPlus.Data
Imports DebtPlus.Events
Imports DebtPlus.LINQ
Imports DebtPlus.LINQ.InMemory
Imports DebtPlus.UI.Common
Imports DebtPlus.Utils
Imports DevExpress.XtraEditors.Controls
Imports HouseholdHeadType = DebtPlus.LINQ.Cache.HouseholdHeadType
Imports MaritalType = DebtPlus.LINQ.Cache.MaritalType
Imports AttributeType = DebtPlus.LINQ.Cache.AttributeType
Imports FirstContactType = DebtPlus.LINQ.Cache.FirstContactType
Imports ContactMethodType = DebtPlus.LINQ.Cache.ContactMethodType

Namespace pages

    Friend Class Page_General

        ''' <summary>
        ''' Create a new instance of our class
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New()
            MyBase.New()
            InitializeComponent()
        End Sub

        ''' <summary>
        ''' Register the event handlers
        ''' </summary>
        Private Sub RegisterHandlers()
            AddHandler dependents.EditValueChanging, AddressOf dependents_EditValueChanging
            AddHandler TelephoneNumberRecordControl_HomeTelephoneID.TelephoneNumberChanged, AddressOf TelephoneNumberRecordControl_Changed
            AddHandler TelephoneNumberRecordControl_MsgTelephoneID.TelephoneNumberChanged, AddressOf TelephoneNumberRecordControl_Changed
            AddHandler counselor.CustomDisplayText, AddressOf counselor_CustomDisplayText
            AddHandler csr.CustomDisplayText, AddressOf counselor_CustomDisplayText
            AddHandler AddressRecordControl_AddressID.AddressChanged, AddressOf AddressRecordControl_AddressID_Changed
            AddHandler AddressRecordControl_AddressID.StateChanged, AddressOf AddressRecordControl_AddressID_StateChanged
            AddHandler client_Region.EditValueChanging, AddressOf Validation.LookUpEdit_ActiveTest
            AddHandler preferred_contact.EditValueChanging, AddressOf Validation.LookUpEdit_ActiveTest
            AddHandler counselor.EditValueChanging, AddressOf Validation.LookUpEdit_ActiveTest
            AddHandler csr.EditValueChanging, AddressOf Validation.LookUpEdit_ActiveTest
            AddHandler method_first_contact.EditValueChanging, AddressOf Validation.LookUpEdit_ActiveTest
            AddHandler county.EditValueChanging, AddressOf Validation.LookUpEdit_ActiveTest
            AddHandler language.EditValueChanging, AddressOf Validation.LookUpEdit_ActiveTest
            AddHandler marital_status.EditValueChanging, AddressOf Validation.LookUpEdit_ActiveTest
            AddHandler office.EditValueChanging, AddressOf Validation.LookUpEdit_ActiveTest
            AddHandler Referred_By.EditValueChanging, AddressOf Validation.LookUpEdit_ActiveTest
            AddHandler household.EditValueChanging, AddressOf Validation.LookUpEdit_ActiveTest
        End Sub

        ''' <summary>
        ''' Remove the event handler registrations
        ''' </summary>
        Private Sub UnRegisterHandlers()
            RemoveHandler dependents.EditValueChanging, AddressOf dependents_EditValueChanging
            RemoveHandler TelephoneNumberRecordControl_HomeTelephoneID.TelephoneNumberChanged, AddressOf TelephoneNumberRecordControl_Changed
            RemoveHandler TelephoneNumberRecordControl_MsgTelephoneID.TelephoneNumberChanged, AddressOf TelephoneNumberRecordControl_Changed
            RemoveHandler counselor.CustomDisplayText, AddressOf counselor_CustomDisplayText
            RemoveHandler csr.CustomDisplayText, AddressOf counselor_CustomDisplayText
            RemoveHandler AddressRecordControl_AddressID.AddressChanged, AddressOf AddressRecordControl_AddressID_Changed
            RemoveHandler AddressRecordControl_AddressID.StateChanged, AddressOf AddressRecordControl_AddressID_StateChanged
            RemoveHandler client_Region.EditValueChanging, AddressOf Validation.LookUpEdit_ActiveTest
            RemoveHandler preferred_contact.EditValueChanging, AddressOf Validation.LookUpEdit_ActiveTest
            RemoveHandler counselor.EditValueChanging, AddressOf Validation.LookUpEdit_ActiveTest
            RemoveHandler csr.EditValueChanging, AddressOf Validation.LookUpEdit_ActiveTest
            RemoveHandler method_first_contact.EditValueChanging, AddressOf Validation.LookUpEdit_ActiveTest
            RemoveHandler county.EditValueChanging, AddressOf Validation.LookUpEdit_ActiveTest
            RemoveHandler language.EditValueChanging, AddressOf Validation.LookUpEdit_ActiveTest
            RemoveHandler marital_status.EditValueChanging, AddressOf Validation.LookUpEdit_ActiveTest
            RemoveHandler office.EditValueChanging, AddressOf Validation.LookUpEdit_ActiveTest
            RemoveHandler Referred_By.EditValueChanging, AddressOf Validation.LookUpEdit_ActiveTest
            RemoveHandler household.EditValueChanging, AddressOf Validation.LookUpEdit_ActiveTest
        End Sub

        ''' <summary>
        ''' Process a change in the Address control
        ''' </summary>
        Private Sub AddressRecordControl_AddressID_Changed(ByVal Sender As Object, ByVal e As AddressChangedEventArgs)

            ' Update the client address in the cache
            Context.ClientDs.clientAddress = e.NewAddress

            ' Build the system note
            Dim sb As New StringBuilder()

            sb.Append("Changed Home Address from:")
            sb.Append(Environment.NewLine)
            sb.Append(e.OldAddress.ToString())

            sb.Append(Environment.NewLine)
            sb.Append(Environment.NewLine)

            sb.Append("to:")
            sb.Append(Environment.NewLine)
            sb.Append(e.NewAddress.ToString())

            Try
                Using bc As New BusinessContext()
                    Dim n As client_note = Factory.Manufacture_client_note(Context.ClientDs.ClientId, 3)
                    n.note = sb.ToString()
                    n.subject = "Changed home address"
                    bc.client_notes.InsertOnSubmit(n)
                    bc.SubmitChanges()
                End Using

            Catch ex As SqlException
                ErrorHandling.HandleErrors(ex)
            End Try
        End Sub

        ''' <summary>
        ''' Process a change in the telephone number
        ''' </summary>
        Private Sub TelephoneNumberRecordControl_Changed(ByVal Sender As Object, ByVal e As TelephoneNumberChangedEventArgs)
            Dim myName As String = If(Sender Is TelephoneNumberRecordControl_HomeTelephoneID, "Home", "Msg")
            Dim subject As String = String.Format("Changed {0} Telephone Number", myName)
            Dim note As String = String.Format("Changed {0} telephone number from ""{1}"" to ""{2}""", myName, e.OldTelephoneNumber.ToString(), e.NewTelephoneNumber.ToString())

            Try
                Using bc As New BusinessContext()
                    Dim n As client_note = Factory.Manufacture_client_note(Context.ClientDs.ClientId, 3)
                    n.note = note
                    n.subject = subject
                    bc.client_notes.InsertOnSubmit(n)
                    bc.SubmitChanges()
                End Using

            Catch ex As SqlException
                ErrorHandling.HandleErrors(ex)
            End Try
        End Sub

        ''' <summary>
        ''' Process the display of the new form information
        ''' </summary>
        Public Overrides Sub ReadForm(bc As BusinessContext)

            UnRegisterHandlers()
            Try

                ' Find the list of offices
                Dim qOffice As Predicate(Of office) = Function(o) o.ActiveFlag
                If Context.ClientDs.clientRecord.office.HasValue Then
                    qOffice = Function(o) o.ActiveFlag OrElse o.Id = Context.ClientDs.clientRecord.office.Value
                End If
                office.Properties.DataSource = Cache.office.getList().FindAll(qOffice).ToList()

                ' Load the other list items with the appropriate table references
                Referred_By.Properties.DataSource = Cache.referred_by.getList()
                household.Properties.DataSource = HouseholdHeadType.getList()
                marital_status.Properties.DataSource = MaritalType.getList()
                language.Properties.DataSource = AttributeType.getLanguageList()
                method_first_contact.Properties.DataSource = FirstContactType.getList()
                csr.Properties.DataSource = Cache.counselor.CSRsList(Context.ClientDs.clientRecord.csr)
                counselor.Properties.DataSource = Cache.counselor.CounselorsList(Context.ClientDs.clientRecord.counselor)
                preferred_contact.Properties.DataSource = ContactMethodType.getList()
                client_Region.Properties.DataSource = Cache.region.getList()
                LookUpEdit_ElectronicStatements.Properties.DataSource = TristateTypes.getList()

                ' Load the address and then use the state to find the list of counties.
                AddressRecordControl_AddressID.EditValue = Context.ClientDs.clientRecord.AddressID
                SetStateID(AddressRecordControl_AddressID.StateID)

                ' After setting the county list, load the remainder of the record into the editing controls.
                client_Region.EditValue = Context.ClientDs.clientRecord.region
                preferred_contact.EditValue = Context.ClientDs.clientRecord.preferred_contact
                counselor.EditValue = Context.ClientDs.clientRecord.counselor
                csr.EditValue = Context.ClientDs.clientRecord.csr
                method_first_contact.EditValue = Context.ClientDs.clientRecord.method_first_contact
                county.EditValue = Context.ClientDs.clientRecord.county
                language.EditValue = Context.ClientDs.clientRecord.language
                marital_status.EditValue = Context.ClientDs.clientRecord.marital_status
                office.EditValue = Context.ClientDs.clientRecord.office
                Referred_By.EditValue = Context.ClientDs.clientRecord.referred_by
                household.EditValue = Context.ClientDs.clientRecord.household
                mail_error_date.EditValue = Context.ClientDs.clientRecord.mail_error_date
                dependents.EditValue = Context.ClientDs.clientRecord.dependents
                salutation.EditValue = Context.ClientDs.clientRecord.salutation
                CheckEdit_ElectronicCorrespondence.EditValue = Context.ClientDs.clientRecord.ElectronicCorrespondence
                LookUpEdit_ElectronicStatements.EditValue = Context.ClientDs.clientRecord.ElectronicStatements

                ' Bind the telephone numbers to the editing controls
                TelephoneNumberRecordControl_HomeTelephoneID.EditValue = Context.ClientDs.clientRecord.HomeTelephoneID
                TelephoneNumberRecordControl_MsgTelephoneID.EditValue = Context.ClientDs.clientRecord.MsgTelephoneID

                ' Mask the telephone numbers if appropriate
                If Context.ClientDs.clientRecord.doNotDisturb Then
                    TelephoneNumberRecordControl_HomeTelephoneID.MaskPhone()
                    TelephoneNumberRecordControl_MsgTelephoneID.MaskPhone()
                End If

            Finally
                RegisterHandlers()
            End Try
        End Sub

        ''' <summary>
        ''' Save the form when needed. Record all of the changed items when the tab is changed.
        ''' </summary>
        Public Overrides Sub SaveForm(bc As BusinessContext)

            ' Save the control values into the editing record.
            Context.ClientDs.clientRecord.HomeTelephoneID = TelephoneNumberRecordControl_HomeTelephoneID.EditValue
            Context.ClientDs.clientRecord.MsgTelephoneID = TelephoneNumberRecordControl_MsgTelephoneID.EditValue
            Context.ClientDs.clientRecord.AddressID = AddressRecordControl_AddressID.EditValue

            ' Set the other field values
            Context.ClientDs.clientRecord.region = Nulls.v_Int32(client_Region.EditValue)
            Context.ClientDs.clientRecord.preferred_contact = Nulls.v_Int32(preferred_contact.EditValue)
            Context.ClientDs.clientRecord.counselor = Nulls.v_Int32(counselor.EditValue)
            Context.ClientDs.clientRecord.csr = Nulls.v_Int32(csr.EditValue)
            Context.ClientDs.clientRecord.method_first_contact = Nulls.v_Int32(method_first_contact.EditValue)
            Context.ClientDs.clientRecord.county = Nulls.v_Int32(county.EditValue)
            Context.ClientDs.clientRecord.language = Nulls.v_Int32(language.EditValue)
            Context.ClientDs.clientRecord.marital_status = Nulls.v_Int32(marital_status.EditValue)
            Context.ClientDs.clientRecord.office = Nulls.v_Int32(office.EditValue)
            Context.ClientDs.clientRecord.referred_by = Nulls.v_Int32(Referred_By.EditValue)
            Context.ClientDs.clientRecord.household = Nulls.v_Int32(household.EditValue)
            Context.ClientDs.clientRecord.dependents = Nulls.v_Int32(dependents.EditValue)
            Context.ClientDs.clientRecord.ElectronicCorrespondence = Nulls.v_Int32(CheckEdit_ElectronicCorrespondence.EditValue)
            Context.ClientDs.clientRecord.ElectronicStatements = Nulls.v_Int32(LookUpEdit_ElectronicStatements.EditValue)
            Context.ClientDs.clientRecord.salutation = Nulls.v_String(salutation.EditValue)
            Context.ClientDs.clientRecord.mail_error_date = Nulls.v_DateTime(mail_error_date.EditValue)

            ' Do the standard save logic at this point.
            MyBase.SaveForm(bc)
        End Sub

        ''' <summary>
        ''' When the state ID is changed, change the list of suitable counties
        ''' </summary>
        Private Sub AddressRecordControl_AddressID_StateChanged(ByVal sender As Object, ByVal e As EventArgs)
            SetStateID(AddressRecordControl_AddressID.StateID)
        End Sub

        ''' <summary>
        ''' Local routine to update the county list based upon the state ID. Only those counties in that state are shown.
        ''' </summary>
        ''' <param name="stateId"></param>
        Private Sub SetStateId(ByVal stateId As Nullable(Of Int32))

            If stateID.HasValue AndAlso stateID.Value >= 0 Then
                county.Properties.DataSource = Cache.county.getList().FindAll(Function(s) s.state = stateID.Value)
                Return
            End If

            county.Properties.DataSource = Nothing
        End Sub

        ''' <summary>
        ''' Process a change in the number of dependents. Do not allow it to go negative.
        ''' </summary>
        Private Sub dependents_EditValueChanging(sender As Object, e As ChangingEventArgs)
            If Nulls.v_Int32(e.NewValue).GetValueOrDefault(-1) < 0 Then
                e.Cancel = True
            End If
        End Sub

        ''' <summary>
        ''' Translate the counselor or CSR from the lookup control to a suitable text string. We want the
        ''' name followed by the telephone number. Both of which are separate fields and we need to combine
        ''' them, ergo this override to the display routine.
        ''' </summary>
        Private Sub counselor_CustomDisplayText(ByVal sender As Object, ByVal e As CustomDisplayTextEventArgs)

            ' If there is a value then convert the item to a string.
            If e.Value IsNot Nothing AndAlso e.Value IsNot DBNull.Value Then
                Dim counselorId As Int32 = Convert.ToInt32(e.Value)

                ' Find the counselor in the list of counselors from the list.
                Dim q As counselor = Cache.counselor.getList().Find(Function(s) s.Id = counselorId)
                If q IsNot Nothing Then
                    Dim nameString As String = If(q.Name Is Nothing, String.Empty, q.Name.ToString().Trim())
                    Dim phoneString As String = If(q.TelephoneNumber Is Nothing, String.Empty, q.TelephoneNumber.ToString().Trim())
                    If nameString <> String.Empty AndAlso phoneString <> String.Empty Then
                        e.DisplayText = String.Format("{0} ({1})", nameString, phoneString)
                        Return
                    End If

                    e.DisplayText = nameString + phoneString
                    Return
                End If
            End If

            e.DisplayText = String.Empty
        End Sub
    End Class
End Namespace
