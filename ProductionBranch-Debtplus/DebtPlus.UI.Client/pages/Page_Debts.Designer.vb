﻿Imports DebtPlus.UI.Client.controls
Imports DebtPlus.UI.Client

Namespace pages

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Page_Debts
        Inherits ControlBaseClient

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Me.barManager1 = New DevExpress.XtraBars.BarManager(Me.components)
            Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
            Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
            Me.BarButtonItem_Add = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Change = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Delete = New DevExpress.XtraBars.BarButtonItem()
            Me.BarButtonItem_Reassign = New DevExpress.XtraBars.BarButtonItem()
            Me.BarSubItem_View = New DevExpress.XtraBars.BarSubItem()
            Me.BarCheckItem_View_All = New DevExpress.XtraBars.BarCheckItem()
            Me.BarCheckItem_View_Active = New DevExpress.XtraBars.BarCheckItem()
            Me.BarCheckItem_View_Balance = New DevExpress.XtraBars.BarCheckItem()
            Me.PopupMenu_Items = New DevExpress.XtraBars.PopupMenu(Me.components)
            Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
            Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PopupMenu_Items, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'barManager1
            '
            Me.barManager1.DockControls.Add(Me.barDockControlTop)
            Me.barManager1.DockControls.Add(Me.barDockControlBottom)
            Me.barManager1.DockControls.Add(Me.barDockControlLeft)
            Me.barManager1.DockControls.Add(Me.barDockControlRight)
            Me.barManager1.Form = Me
            Me.barManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarButtonItem_Add, Me.BarButtonItem_Change, Me.BarButtonItem_Delete, Me.BarButtonItem_Reassign, Me.BarSubItem_View, Me.BarCheckItem_View_All, Me.BarCheckItem_View_Active, Me.BarCheckItem_View_Balance})
            Me.barManager1.MaxItemId = 11
            '
            'barDockControlTop
            '
            Me.barDockControlTop.CausesValidation = False
            Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
            Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
            Me.barDockControlTop.Size = New System.Drawing.Size(632, 0)
            '
            'barDockControlBottom
            '
            Me.barDockControlBottom.CausesValidation = False
            Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
            Me.barDockControlBottom.Location = New System.Drawing.Point(0, 286)
            Me.barDockControlBottom.Size = New System.Drawing.Size(632, 0)
            '
            'barDockControlLeft
            '
            Me.barDockControlLeft.CausesValidation = False
            Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
            Me.barDockControlLeft.Location = New System.Drawing.Point(0, 0)
            Me.barDockControlLeft.Size = New System.Drawing.Size(0, 286)
            '
            'barDockControlRight
            '
            Me.barDockControlRight.CausesValidation = False
            Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
            Me.barDockControlRight.Location = New System.Drawing.Point(632, 0)
            Me.barDockControlRight.Size = New System.Drawing.Size(0, 286)
            '
            'BarButtonItem_Add
            '
            Me.BarButtonItem_Add.Caption = "&Add"
            Me.BarButtonItem_Add.Id = 0
            Me.BarButtonItem_Add.Name = "BarButtonItem_Add"
            '
            'BarButtonItem_Change
            '
            Me.BarButtonItem_Change.Caption = "&Change"
            Me.BarButtonItem_Change.Id = 1
            Me.BarButtonItem_Change.Name = "BarButtonItem_Change"
            '
            'BarButtonItem_Delete
            '
            Me.BarButtonItem_Delete.Caption = "&Delete"
            Me.BarButtonItem_Delete.Id = 2
            Me.BarButtonItem_Delete.Name = "BarButtonItem_Delete"
            '
            'BarButtonItem_Reassign
            '
            Me.BarButtonItem_Reassign.Caption = "&Reassign"
            Me.BarButtonItem_Reassign.Id = 3
            Me.BarButtonItem_Reassign.Name = "BarButtonItem_Reassign"
            '
            'BarSubItem_View
            '
            Me.BarSubItem_View.Caption = "&View"
            Me.BarSubItem_View.Id = 4
            Me.BarSubItem_View.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarCheckItem_View_All), New DevExpress.XtraBars.LinkPersistInfo(Me.BarCheckItem_View_Active), New DevExpress.XtraBars.LinkPersistInfo(Me.BarCheckItem_View_Balance)})
            Me.BarSubItem_View.Name = "BarSubItem_View"
            '
            'BarCheckItem_View_All
            '
            Me.BarCheckItem_View_All.Caption = "&All"
            Me.BarCheckItem_View_All.Id = 8
            Me.BarCheckItem_View_All.Name = "BarCheckItem_View_All"
            '
            'BarCheckItem_View_Active
            '
            Me.BarCheckItem_View_Active.Caption = "&Non-Reassigned"
            Me.BarCheckItem_View_Active.Id = 9
            Me.BarCheckItem_View_Active.Name = "BarCheckItem_View_Active"
            '
            'BarCheckItem_View_Balance
            '
            Me.BarCheckItem_View_Balance.Caption = "&Only ones with a balance"
            Me.BarCheckItem_View_Balance.Id = 10
            Me.BarCheckItem_View_Balance.Name = "BarCheckItem_View_Balance"
            '
            'PopupMenu_Items
            '
            Me.PopupMenu_Items.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Add), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Change), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Delete), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem_Reassign, True), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem_View, True)})
            Me.PopupMenu_Items.Manager = Me.barManager1
            Me.PopupMenu_Items.Name = "PopupMenu_Items"
            '
            'GridControl1
            '
            Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.GridControl1.Location = New System.Drawing.Point(0, 0)
            Me.GridControl1.MainView = Me.GridView1
            Me.GridControl1.MenuManager = Me.barManager1
            Me.GridControl1.Name = "GridControl1"
            Me.GridControl1.Size = New System.Drawing.Size(632, 286)
            Me.GridControl1.TabIndex = 4
            Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
            '
            'GridView1
            '
            Me.GridView1.GridControl = Me.GridControl1
            Me.GridView1.Name = "GridView1"
            Me.GridView1.OptionsView.ColumnAutoWidth = False
            Me.GridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
            Me.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedRow
            '
            'Page_Debts
            '
            Me.Controls.Add(Me.GridControl1)
            Me.Controls.Add(Me.barDockControlLeft)
            Me.Controls.Add(Me.barDockControlRight)
            Me.Controls.Add(Me.barDockControlBottom)
            Me.Controls.Add(Me.barDockControlTop)
            Me.Name = "Page_Debts"
            Me.Size = New System.Drawing.Size(632, 286)
            CType(Me.barManager1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PopupMenu_Items, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

        Protected Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
        Protected Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
        Protected Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
        Protected Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
        Protected Friend WithEvents barManager1 As DevExpress.XtraBars.BarManager
        Protected Friend WithEvents PopupMenu_Items As DevExpress.XtraBars.PopupMenu
        Protected Friend WithEvents BarButtonItem_Add As DevExpress.XtraBars.BarButtonItem
        Protected Friend WithEvents BarButtonItem_Change As DevExpress.XtraBars.BarButtonItem
        Protected Friend WithEvents BarButtonItem_Delete As DevExpress.XtraBars.BarButtonItem
        Protected Friend WithEvents BarButtonItem_Reassign As DevExpress.XtraBars.BarButtonItem
        Protected Friend WithEvents BarSubItem_View As DevExpress.XtraBars.BarSubItem
        Protected Friend WithEvents BarCheckItem_View_All As DevExpress.XtraBars.BarCheckItem
        Protected Friend WithEvents BarCheckItem_View_Active As DevExpress.XtraBars.BarCheckItem
        Protected Friend WithEvents BarCheckItem_View_Balance As DevExpress.XtraBars.BarCheckItem
        Protected Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
        Protected Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    End Class
End Namespace