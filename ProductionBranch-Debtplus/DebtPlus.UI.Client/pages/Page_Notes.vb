#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports System.ComponentModel
Imports DebtPlus.Svc.DataSets
Imports DebtPlus.UI.Client.controls
Imports DebtPlus.Utils
Imports DebtPlus.UI.Client.Service
Imports System.Data.SqlClient
Imports DevExpress.XtraEditors
Imports System.Threading
Imports DebtPlus.Events
Imports DebtPlus.Notes
Imports System.Collections.Generic
Imports System.Linq
Imports DebtPlus.LINQ

Namespace pages

    Friend Class Page_Notes
        Inherits MyGridControlClient

        ' Thread to process the reading of the notes from the database.
        ' This may take a while so do it in the background.
        Private WithEvents bt As BackgroundWorker

        ' We display the grid from this list
        Private colNotes As System.Collections.Generic.List(Of Note)

        Private isAdmin As Boolean
        Friend WithEvents CheckEdit_Archived As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents GridColumn_Archived As DevExpress.XtraGrid.Columns.GridColumn
        Private currentUser As String

        Public Sub New()
            MyBase.New()
            InitializeComponent()

            EnableMenus = True
            isAdmin = False
            currentUser = Nothing
            colNotes = Nothing
            bt = New BackgroundWorker()

            RegisterHandlers()
        End Sub

        Private Sub RegisterHandlers()
            AddHandler bt.DoWork, AddressOf bt_DoWork
            AddHandler bt.RunWorkerCompleted, AddressOf bt_RunWorkerCompleted
            AddHandler CheckEdit_Alerts.CheckStateChanged, AddressOf CheckEdit_CheckStateChanged
            AddHandler CheckEdit_Perm.CheckStateChanged, AddressOf CheckEdit_CheckStateChanged
            AddHandler CheckEdit_CurrentDisbursement.CheckStateChanged, AddressOf CheckEdit_CheckStateChanged
            AddHandler CheckEdit_PriorDisbursement.CheckStateChanged, AddressOf CheckEdit_CheckStateChanged
            AddHandler CheckEdit_System.CheckStateChanged, AddressOf CheckEdit_CheckStateChanged
            AddHandler CheckEdit_Temporary.CheckStateChanged, AddressOf CheckEdit_CheckStateChanged
            AddHandler CheckEdit_Archived.CheckStateChanged, AddressOf CheckEdit_CheckStateChanged
            AddHandler GridView1.CustomColumnDisplayText, AddressOf GridView1_CustomColumnDisplayText
        End Sub

        Private Sub UnRegisterHandlers()
            RemoveHandler bt.DoWork, AddressOf bt_DoWork
            RemoveHandler bt.RunWorkerCompleted, AddressOf bt_RunWorkerCompleted
            RemoveHandler CheckEdit_Alerts.CheckStateChanged, AddressOf CheckEdit_CheckStateChanged
            RemoveHandler CheckEdit_Perm.CheckStateChanged, AddressOf CheckEdit_CheckStateChanged
            RemoveHandler CheckEdit_CurrentDisbursement.CheckStateChanged, AddressOf CheckEdit_CheckStateChanged
            RemoveHandler CheckEdit_PriorDisbursement.CheckStateChanged, AddressOf CheckEdit_CheckStateChanged
            RemoveHandler CheckEdit_System.CheckStateChanged, AddressOf CheckEdit_CheckStateChanged
            RemoveHandler CheckEdit_Temporary.CheckStateChanged, AddressOf CheckEdit_CheckStateChanged
            RemoveHandler CheckEdit_Archived.CheckStateChanged, AddressOf CheckEdit_CheckStateChanged
            RemoveHandler GridView1.CustomColumnDisplayText, AddressOf GridView1_CustomColumnDisplayText
        End Sub

        Private Class Note
            Public Sub New()
            End Sub

            Public Property source As Int32
            Public Property Id As Int32
            Public Property type As Int32
            Public Property subject As String
            Public Property date_created As DateTime
            Public Property dont_edit As Boolean
            Public Property dont_delete As Boolean
            Public Property isArchived As String

            Private m_created_by As String
            Public Property created_by As String
                Get
                    Return m_created_by
                End Get
                Set(value As String)
                    If String.IsNullOrEmpty(value) Then
                        m_created_by = String.Empty
                        Return
                    End If

                    Dim iPos As Int32 = value.LastIndexOfAny(New Char() {"/"c, "\"c})
                    If iPos > 0 Then
                        m_created_by = value.Substring(iPos + 1).Trim()
                        Return
                    End If
                    m_created_by = value.Trim()
                End Set
            End Property

            Public Sub New(ByVal source As Int32, ByVal Id As Int32, ByVal type As Int32, ByVal subject As String, ByVal date_created As DateTime, ByVal created_by As String, ByVal dont_edit As Boolean, ByVal dont_delete As Boolean, ByVal isArchived As String)
                MyClass.New()
                Me.source = source
                Me.Id = Id
                Me.type = type
                Me.subject = subject
                Me.date_created = date_created
                Me.created_by = created_by
                Me.dont_edit = dont_edit
                Me.dont_delete = dont_delete
                Me.isArchived = isArchived
            End Sub
        End Class

        Private Class OptionsClass
            Implements IDisposable

            Public Sub Dispose() Implements IDisposable.Dispose
                Dispose(True)
                GC.SuppressFinalize(Me)
            End Sub

            Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            End Sub

            Public IncludePerm As Boolean = True
            Public IncludeTemp As Boolean = True
            Public IncludeSystem As Boolean = True
            Public IncludeAlert As Boolean = True
            Public IncludeDisbCurrent As Boolean = True
            Public IncludeDisbPrior As Boolean = True
            Public IncludeArchivedNotes As Boolean = False

            Public ReadOnly Property IsEmpty() As Boolean
                Get
                    Return Not (IncludeAlert OrElse IncludeDisbCurrent OrElse IncludeDisbPrior OrElse IncludePerm OrElse IncludeSystem OrElse IncludeTemp OrElse IncludeArchivedNotes)
                End Get
            End Property

            Public ReadOnly Property WantClientNotes() As Boolean
                Get
                    Return (IncludeAlert OrElse IncludePerm OrElse IncludeSystem OrElse IncludeTemp)
                End Get
            End Property

            Public ReadOnly Property WantDisbNotes() As Boolean
                Get
                    Return (IncludeDisbCurrent OrElse IncludeDisbPrior)
                End Get
            End Property
        End Class

#Region " Windows Form Designer generated code "

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer
        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
        Friend WithEvents CheckEdit_Perm As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents CheckEdit_System As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents CheckEdit_CurrentDisbursement As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents CheckEdit_PriorDisbursement As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents CheckEdit_Temporary As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents CheckEdit_Alerts As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents GridColumn_date_created As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_type As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Counselor As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Subject As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_ID As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_dont_edit As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_dont_delete As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_dont_print As DevExpress.XtraGrid.Columns.GridColumn
        Friend WithEvents GridColumn_Source As DevExpress.XtraGrid.Columns.GridColumn

        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
            Me.CheckEdit_Archived = New DevExpress.XtraEditors.CheckEdit()
            Me.CheckEdit_PriorDisbursement = New DevExpress.XtraEditors.CheckEdit()
            Me.CheckEdit_CurrentDisbursement = New DevExpress.XtraEditors.CheckEdit()
            Me.CheckEdit_Alerts = New DevExpress.XtraEditors.CheckEdit()
            Me.CheckEdit_System = New DevExpress.XtraEditors.CheckEdit()
            Me.CheckEdit_Temporary = New DevExpress.XtraEditors.CheckEdit()
            Me.CheckEdit_Perm = New DevExpress.XtraEditors.CheckEdit()
            Me.GridColumn_date_created = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_type = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_Counselor = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_Subject = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_ID = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_dont_edit = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_dont_delete = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_dont_print = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_Source = New DevExpress.XtraGrid.Columns.GridColumn()
            Me.GridColumn_Archived = New DevExpress.XtraGrid.Columns.GridColumn()
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PopupMenu_Items, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.GroupControl1.SuspendLayout()
            CType(Me.CheckEdit_Archived.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_PriorDisbursement.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_CurrentDisbursement.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_Alerts.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_System.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_Temporary.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.CheckEdit_Perm.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'GridControl1
            '
            Me.GridControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GridControl1.Dock = System.Windows.Forms.DockStyle.None
            Me.GridControl1.Location = New System.Drawing.Point(0, 48)
            Me.GridControl1.Size = New System.Drawing.Size(781, 248)
            '
            'GridView1
            '
            Me.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = True
            Me.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = True
            Me.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.Empty.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.EvenRow.Options.UseBackColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.EvenRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = True
            Me.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FilterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FilterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(133, Byte), Integer), CType(CType(195, Byte), Integer))
            Me.GridView1.Appearance.FixedLine.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FocusedCell.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedCell.Options.UseForeColor = True
            Me.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(189, Byte), Integer))
            Me.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(59, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(206, Byte), Integer))
            Me.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.FocusedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.FocusedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.FooterPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.FooterPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.GroupButton.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupButton.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupFooter.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupFooter.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.GroupRow.Options.UseBackColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.GroupRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(254, Byte), Integer))
            Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
            Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
            Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(225, Byte), Integer))
            Me.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(215, Byte), Integer))
            Me.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = True
            Me.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.HorzLine.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.OddRow.Options.UseBackColor = True
            Me.GridView1.Appearance.OddRow.Options.UseBorderColor = True
            Me.GridView1.Appearance.OddRow.Options.UseForeColor = True
            Me.GridView1.Appearance.Preview.Font = New System.Drawing.Font("Verdana", 7.5!)
            Me.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(215, Byte), Integer))
            Me.GridView1.Appearance.Preview.Options.UseFont = True
            Me.GridView1.Appearance.Preview.Options.UseForeColor = True
            Me.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(CType(CType(247, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black
            Me.GridView1.Appearance.Row.Options.UseBackColor = True
            Me.GridView1.Appearance.Row.Options.UseForeColor = True
            Me.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(255, Byte), Integer))
            Me.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White
            Me.GridView1.Appearance.RowSeparator.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Integer), CType(CType(155, Byte), Integer), CType(CType(215, Byte), Integer))
            Me.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White
            Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
            Me.GridView1.Appearance.SelectedRow.Options.UseForeColor = True
            Me.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White
            Me.GridView1.Appearance.TopNewRow.Options.UseBackColor = True
            Me.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(CType(CType(104, Byte), Integer), CType(CType(184, Byte), Integer), CType(CType(251, Byte), Integer))
            Me.GridView1.Appearance.VertLine.Options.UseBackColor = True
            Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn_date_created, Me.GridColumn_type, Me.GridColumn_Counselor, Me.GridColumn_Subject, Me.GridColumn_ID, Me.GridColumn_dont_edit, Me.GridColumn_dont_delete, Me.GridColumn_dont_print, Me.GridColumn_Source, Me.GridColumn_Archived})
            Me.GridView1.OptionsBehavior.AutoPopulateColumns = False
            Me.GridView1.OptionsBehavior.Editable = False
            Me.GridView1.OptionsLayout.Columns.StoreAllOptions = True
            Me.GridView1.OptionsLayout.LayoutVersion = "1"
            Me.GridView1.OptionsLayout.StoreAllOptions = True
            Me.GridView1.OptionsLayout.StoreDataSettings = False
            Me.GridView1.OptionsView.EnableAppearanceEvenRow = True
            Me.GridView1.OptionsView.EnableAppearanceOddRow = True
            Me.GridView1.OptionsView.ShowGroupPanel = False
            Me.GridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.[False]
            Me.GridView1.OptionsView.ShowIndicator = False
            Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn_date_created, DevExpress.Data.ColumnSortOrder.Descending)})
            '
            'barDockControlTop
            '
            Me.barDockControlTop.Size = New System.Drawing.Size(781, 0)
            '
            'barDockControlBottom
            '
            Me.barDockControlBottom.Size = New System.Drawing.Size(781, 0)
            '
            'barDockControlRight
            '
            Me.barDockControlRight.Location = New System.Drawing.Point(781, 0)
            '
            'GroupControl1
            '
            Me.GroupControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.GroupControl1.Controls.Add(Me.CheckEdit_Archived)
            Me.GroupControl1.Controls.Add(Me.CheckEdit_PriorDisbursement)
            Me.GroupControl1.Controls.Add(Me.CheckEdit_CurrentDisbursement)
            Me.GroupControl1.Controls.Add(Me.CheckEdit_Alerts)
            Me.GroupControl1.Controls.Add(Me.CheckEdit_System)
            Me.GroupControl1.Controls.Add(Me.CheckEdit_Temporary)
            Me.GroupControl1.Controls.Add(Me.CheckEdit_Perm)
            Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
            Me.GroupControl1.Name = "GroupControl1"
            Me.GroupControl1.Size = New System.Drawing.Size(781, 48)
            Me.GroupControl1.TabIndex = 1
            Me.GroupControl1.Text = "Show Notes of the following type(s)"
            '
            'CheckEdit_Archived
            '
            Me.CheckEdit_Archived.Location = New System.Drawing.Point(573, 24)
            Me.CheckEdit_Archived.Name = "CheckEdit_Archived"
            Me.CheckEdit_Archived.Properties.Caption = "Archived Notes"
            Me.CheckEdit_Archived.Size = New System.Drawing.Size(130, 20)
            Me.CheckEdit_Archived.TabIndex = 5
            '
            'CheckEdit_PriorDisbursement
            '
            Me.CheckEdit_PriorDisbursement.Location = New System.Drawing.Point(454, 24)
            Me.CheckEdit_PriorDisbursement.Name = "CheckEdit_PriorDisbursement"
            Me.CheckEdit_PriorDisbursement.Properties.Caption = "Prior Disbursement"
            Me.CheckEdit_PriorDisbursement.Size = New System.Drawing.Size(117, 20)
            Me.CheckEdit_PriorDisbursement.TabIndex = 2
            '
            'CheckEdit_CurrentDisbursement
            '
            Me.CheckEdit_CurrentDisbursement.EditValue = True
            Me.CheckEdit_CurrentDisbursement.Location = New System.Drawing.Point(324, 24)
            Me.CheckEdit_CurrentDisbursement.Name = "CheckEdit_CurrentDisbursement"
            Me.CheckEdit_CurrentDisbursement.Properties.Caption = "Current Disbursement"
            Me.CheckEdit_CurrentDisbursement.Size = New System.Drawing.Size(130, 20)
            Me.CheckEdit_CurrentDisbursement.TabIndex = 2
            '
            'CheckEdit_Alerts
            '
            Me.CheckEdit_Alerts.EditValue = True
            Me.CheckEdit_Alerts.Location = New System.Drawing.Point(162, 24)
            Me.CheckEdit_Alerts.Name = "CheckEdit_Alerts"
            Me.CheckEdit_Alerts.Properties.Caption = "Alerts"
            Me.CheckEdit_Alerts.Size = New System.Drawing.Size(80, 20)
            Me.CheckEdit_Alerts.TabIndex = 4
            '
            'CheckEdit_System
            '
            Me.CheckEdit_System.EditValue = True
            Me.CheckEdit_System.Location = New System.Drawing.Point(81, 24)
            Me.CheckEdit_System.Name = "CheckEdit_System"
            Me.CheckEdit_System.Properties.Caption = "System"
            Me.CheckEdit_System.Size = New System.Drawing.Size(80, 20)
            Me.CheckEdit_System.TabIndex = 1
            '
            'CheckEdit_Temporary
            '
            Me.CheckEdit_Temporary.EditValue = True
            Me.CheckEdit_Temporary.Location = New System.Drawing.Point(243, 24)
            Me.CheckEdit_Temporary.Name = "CheckEdit_Temporary"
            Me.CheckEdit_Temporary.Properties.Caption = "Temporary"
            Me.CheckEdit_Temporary.Size = New System.Drawing.Size(80, 20)
            Me.CheckEdit_Temporary.TabIndex = 3
            '
            'CheckEdit_Perm
            '
            Me.CheckEdit_Perm.EditValue = True
            Me.CheckEdit_Perm.Location = New System.Drawing.Point(0, 24)
            Me.CheckEdit_Perm.Name = "CheckEdit_Perm"
            Me.CheckEdit_Perm.Properties.Caption = "Permanent"
            Me.CheckEdit_Perm.Size = New System.Drawing.Size(80, 20)
            Me.CheckEdit_Perm.TabIndex = 0
            '
            'GridColumn_date_created
            '
            Me.GridColumn_date_created.Caption = "Date"
            Me.GridColumn_date_created.CustomizationCaption = "Date and Time"
            Me.GridColumn_date_created.DisplayFormat.FormatString = "M/d/yyyy h:mm tt"
            Me.GridColumn_date_created.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
            Me.GridColumn_date_created.FieldName = "date_created"
            Me.GridColumn_date_created.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.[Date]
            Me.GridColumn_date_created.Name = "GridColumn_date_created"
            Me.GridColumn_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_date_created.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value
            Me.GridColumn_date_created.Visible = True
            Me.GridColumn_date_created.VisibleIndex = 0
            Me.GridColumn_date_created.Width = 135
            '
            'GridColumn_type
            '
            Me.GridColumn_type.AppearanceCell.Options.UseTextOptions = True
            Me.GridColumn_type.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_type.AppearanceHeader.Options.UseTextOptions = True
            Me.GridColumn_type.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
            Me.GridColumn_type.Caption = "Type"
            Me.GridColumn_type.CustomizationCaption = "Note Type"
            Me.GridColumn_type.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_type.FieldName = "type"
            Me.GridColumn_type.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom
            Me.GridColumn_type.Name = "GridColumn_type"
            Me.GridColumn_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_type.Visible = True
            Me.GridColumn_type.VisibleIndex = 1
            Me.GridColumn_type.Width = 93
            '
            'GridColumn_Counselor
            '
            Me.GridColumn_Counselor.Caption = "Counselor"
            Me.GridColumn_Counselor.CustomizationCaption = "Counselor"
            Me.GridColumn_Counselor.FieldName = "created_by"
            Me.GridColumn_Counselor.Name = "GridColumn_Counselor"
            Me.GridColumn_Counselor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_Counselor.Visible = True
            Me.GridColumn_Counselor.VisibleIndex = 2
            Me.GridColumn_Counselor.Width = 108
            '
            'GridColumn_Subject
            '
            Me.GridColumn_Subject.Caption = "Subject"
            Me.GridColumn_Subject.CustomizationCaption = "Subject"
            Me.GridColumn_Subject.FieldName = "subject"
            Me.GridColumn_Subject.Name = "GridColumn_Subject"
            Me.GridColumn_Subject.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_Subject.Visible = True
            Me.GridColumn_Subject.VisibleIndex = 3
            Me.GridColumn_Subject.Width = 263
            '
            'GridColumn_ID
            '
            Me.GridColumn_ID.Caption = "ID"
            Me.GridColumn_ID.CustomizationCaption = "Record ID"
            Me.GridColumn_ID.DisplayFormat.FormatString = "f0"
            Me.GridColumn_ID.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_ID.FieldName = "Id"
            Me.GridColumn_ID.Name = "GridColumn_ID"
            Me.GridColumn_ID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_dont_edit
            '
            Me.GridColumn_dont_edit.Caption = "No Edit?"
            Me.GridColumn_dont_edit.CustomizationCaption = "Disallow Edit?"
            Me.GridColumn_dont_edit.FieldName = "dont_edit"
            Me.GridColumn_dont_edit.Name = "GridColumn_dont_edit"
            Me.GridColumn_dont_edit.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_dont_delete
            '
            Me.GridColumn_dont_delete.Caption = "No Delete?"
            Me.GridColumn_dont_delete.CustomizationCaption = "Disallow Delete?"
            Me.GridColumn_dont_delete.FieldName = "dont_delete"
            Me.GridColumn_dont_delete.Name = "GridColumn_dont_delete"
            Me.GridColumn_dont_delete.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_dont_print
            '
            Me.GridColumn_dont_print.Caption = "No Print?"
            Me.GridColumn_dont_print.CustomizationCaption = "Disallow Print?"
            Me.GridColumn_dont_print.FieldName = "dont_print"
            Me.GridColumn_dont_print.Name = "GridColumn_dont_print"
            Me.GridColumn_dont_print.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_Source
            '
            Me.GridColumn_Source.Caption = "Source"
            Me.GridColumn_Source.CustomizationCaption = "Record Source"
            Me.GridColumn_Source.DisplayFormat.FormatString = "f0"
            Me.GridColumn_Source.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            Me.GridColumn_Source.FieldName = "source"
            Me.GridColumn_Source.Name = "GridColumn_Source"
            Me.GridColumn_Source.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            '
            'GridColumn_Archived
            '
            Me.GridColumn_Archived.Caption = "Is Archived"
            Me.GridColumn_Archived.CustomizationCaption = "Is Archived"
            Me.GridColumn_Archived.FieldName = "isArchived"
            Me.GridColumn_Archived.Name = "GridColumn_Archived"
            Me.GridColumn_Archived.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
            Me.GridColumn_Archived.Visible = True
            Me.GridColumn_Archived.VisibleIndex = 4
            '
            'Page_Notes
            '
            Me.Controls.Add(Me.GroupControl1)
            Me.Name = "Page_Notes"
            Me.Size = New System.Drawing.Size(781, 296)
            Me.Controls.SetChildIndex(Me.barDockControlTop, 0)
            Me.Controls.SetChildIndex(Me.barDockControlBottom, 0)
            Me.Controls.SetChildIndex(Me.barDockControlRight, 0)
            Me.Controls.SetChildIndex(Me.barDockControlLeft, 0)
            Me.Controls.SetChildIndex(Me.GroupControl1, 0)
            Me.Controls.SetChildIndex(Me.GridControl1, 0)
            CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PopupMenu_Items, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.GroupControl1.ResumeLayout(False)
            CType(Me.CheckEdit_Archived.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_PriorDisbursement.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_CurrentDisbursement.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_Alerts.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_System.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_Temporary.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.CheckEdit_Perm.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

#End Region

        Public Overrides Sub ReadForm(bc As DebtPlus.LINQ.BusinessContext)
            currentUser = BusinessContext.suser_sname()
            isAdmin = String.Compare(currentUser, "sa", False) = 0

            ' Remove the system identifier from the user name
            Dim iPos As Int32 = currentUser.LastIndexOfAny(New Char() {"/"c, "\"c})
            If iPos > 0 Then
                currentUser = currentUser.Substring(iPos + 1).Trim()
            End If

            RefreshList()
        End Sub

        ''' <summary>
        ''' Refresh the note list when appropriate
        ''' </summary>
        Private Sub RefreshList()
            If Not bt.IsBusy Then

                ' Create the class to pass to the worker thread
                Dim Options As New OptionsClass() With {.IncludePerm = CheckEdit_Perm.Checked, .IncludeTemp = CheckEdit_Temporary.Checked, .IncludeSystem = CheckEdit_System.Checked, .IncludeAlert = CheckEdit_Alerts.Checked, .IncludeDisbCurrent = CheckEdit_CurrentDisbursement.Checked, .IncludeDisbPrior = CheckEdit_PriorDisbursement.Checked, .IncludeArchivedNotes = CheckEdit_Archived.Checked}

                ' Set the wait cursor and clear the current list
                GridControl1.UseWaitCursor = True
                GridControl1.DataSource = Nothing
                GridControl1.RefreshDataSource()

                ' Run the thread to read the notes list
                bt.RunWorkerAsync(Options)
            End If
        End Sub

        ''' <summary>
        ''' Read the list of client notes in the background thread.
        ''' </summary>
        Private Sub bt_DoWork(ByVal sender As Object, ByVal e As DoWorkEventArgs)

            Dim options As OptionsClass = CType(e.Argument, OptionsClass)

            ' Build the request to retrieve the list of notes
            Try
                Using dc As New BusinessContext()
                    Dim colClientNotes As System.Collections.Generic.List(Of Note) = Nothing
                    Dim colDisbursementNotes As System.Collections.Generic.List(Of Note) = Nothing


                    ' If nothing is checked then include the client notes and nothing else
                    If options.IsEmpty Then
                        colClientNotes = dc.client_notes.Where(Function(n) n.client_creditor Is Nothing AndAlso n.client = Context.ClientDs.ClientId).Select(Function(n) New Note(1, n.Id, n.type, n.subject, n.date_created, n.created_by, n.dont_edit, n.dont_delete, "No")).ToList()

                    ElseIf options.WantClientNotes Then
                        Dim types As New System.Collections.Generic.List(Of Int32)
                        If options.IncludePerm Then types.Add(1)
                        If options.IncludeTemp Then types.Add(2)
                        If options.IncludeSystem Then types.Add(3)
                        If options.IncludeAlert Then types.Add(4)

                        colClientNotes = dc.client_notes.Where(Function(n) n.client_creditor Is Nothing AndAlso n.client = Context.ClientDs.ClientId AndAlso types.Contains(n.type)).Select(Function(n) New Note(1, n.Id, n.type, n.subject, n.date_created, n.created_by, n.dont_edit, n.dont_delete, "No")).ToList()
                    End If

                    ' Build the list of disbursement notes
                    If options.IncludeDisbCurrent AndAlso options.IncludeDisbPrior Then
                        colDisbursementNotes = dc.disbursement_notes.Where(Function(n) n.client = Context.ClientDs.ClientId).Select(Function(n) New Note(2, n.Id, n.type, n.subject, n.date_created, n.created_by, n.dont_edit, n.dont_delete, "No")).ToList()

                    ElseIf options.IncludeDisbPrior Then
                        colDisbursementNotes = dc.disbursement_notes.Where(Function(n) n.client = Context.ClientDs.ClientId AndAlso n.disbursement_register IsNot Nothing).Select(Function(n) New Note(2, n.Id, n.type, n.subject, n.date_created, n.created_by, n.dont_edit, n.dont_delete, "No")).ToList()

                    ElseIf options.IncludeDisbCurrent Then
                        colDisbursementNotes = dc.disbursement_notes.Where(Function(n) n.client = Context.ClientDs.ClientId AndAlso n.disbursement_register Is Nothing).Select(Function(n) New Note(2, n.Id, n.type, n.subject, n.date_created, n.created_by, n.dont_edit, n.dont_delete, "No")).ToList()
                    End If

                    ' Merge the two lists together to form the display list
                    colNotes = New System.Collections.Generic.List(Of Note)
                    If colClientNotes IsNot Nothing Then
                        colNotes = colNotes.Concat(colClientNotes).ToList()
                    End If

                    If colDisbursementNotes IsNot Nothing Then
                        colNotes = colNotes.Concat(colDisbursementNotes).ToList()
                    End If
                End Using

                If options.IncludeArchivedNotes = True Then
                    Dim colArchivedNotes As System.Collections.Generic.List(Of Note) = Nothing

                    Dim archivedNoteTypes As New System.Collections.Generic.List(Of Int32)
                    If options.IncludePerm Then archivedNoteTypes.Add(1)
                    If options.IncludeTemp Then archivedNoteTypes.Add(2)
                    If options.IncludeSystem Then archivedNoteTypes.Add(3)

                    Using ac As New ArchiveContext()
                        colArchivedNotes = ac.client_notes.Where(Function(n) n.client_creditor Is Nothing AndAlso n.client = Context.ClientDs.ClientId AndAlso archivedNoteTypes.Contains(n.type)).Select(Function(n) New Note(1, n.Id, n.type, n.subject, n.date_created, n.created_by, n.dont_edit, n.dont_delete, "Yes")).ToList()
                    End Using

                    If colArchivedNotes IsNot Nothing Then
                        colNotes = colNotes.Concat(colArchivedNotes).ToList()
                    End If

                End If

            Catch ex As SqlException
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex)
            End Try
        End Sub

        ''' <summary>
        ''' The note read thread has completed. Update the note display
        ''' </summary>
        Private Sub bt_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs)
            GridControl1.UseWaitCursor = False
            GridControl1.DataSource = colNotes
            GridControl1.RefreshDataSource()
        End Sub

        Private Sub CheckEdit_CheckStateChanged(ByVal sender As Object, ByVal e As EventArgs)
            With CType(sender, CheckEdit)

                ' Look for any checked status among any of the checkboxes
                Do
                    If CheckEdit_CurrentDisbursement.Checked Then Exit Do
                    If CheckEdit_PriorDisbursement.Checked Then Exit Do
                    If CheckEdit_Perm.Checked Then Exit Do
                    If CheckEdit_Temporary.Checked Then Exit Do
                    If CheckEdit_System.Checked Then Exit Do
                    If CheckEdit_Alerts.Checked Then Exit Do
                    If CheckEdit_Archived.Checked Then Exit Do

                    ' We do not allow all of the check boxes to be un-checked.
                    .Checked = True
                    Return
                Loop

                ' Rebuild the note list from the check status
                RefreshList()
            End With
        End Sub

        ''' <summary>
        ''' Determine if the current user is allowed to edit the current note.
        ''' </summary>
        ''' <param name="obj">Pointer to the Note structure for the current note</param>
        Protected Function OkToEditNote(obj As Object) As Boolean

            ' THe user must be allowed to edit the note.
            If Not MyBase.OkToEdit(obj) Then Return False

            ' Find the current note. If none then don't allow the edit operation
            Dim currentNote As Note = TryCast(obj, Note)
            If currentNote Is Nothing Then Return False

            ' System notes are never allowed to be edited, even by admin users
            If currentNote.type = 3 Then Return False

            ' Admin users are always allowed to edit the note
            If isAdmin Then Return True

            ' If the user is the same then the edit operation is allowed if the registry says so
            If String.Compare(currentNote.created_by, currentUser, True) = 0 Then
                Return DebtPlus.Configuration.Config.EditExistingNoteSameUser
            End If

            ' If the user is not the same then we look at the indicator flags
            Return DebtPlus.Configuration.Config.EditExistingNoteOtherUser AndAlso Not currentNote.dont_edit
        End Function

        ''' <summary>
        ''' Determine if the current user is allowed to delete the current note.
        ''' </summary>
        Protected Overrides Function OkToDelete(obj As Object) As Boolean
            If Not MyBase.OkToDelete(obj) Then Return False

            ' Find the current note. If none then don't allow the edit operation
            Dim currentNote As Note = TryCast(obj, Note)
            If currentNote Is Nothing Then Return False

            ' Admin users are always allowed to delete the note
            If isAdmin Then Return True

            ' If the user is not allowed to delete a note then the user is not allowed
            If Not DebtPlus.Configuration.Config.DeleteNotes Then Return False

            ' Look at the status if the user is different
            If String.Compare(currentNote.created_by, currentUser, True) <> 0 Then
                Return Not currentNote.dont_delete
            End If

            ' Finally, the user is the same. Allow the delete operation.
            Return True
        End Function

        Protected Overrides Sub PopupMenu_Items_Popup(ByVal sender As Object, ByVal e As EventArgs)

            ' Change the text to show the action allowed for the edit operation.
            If ControlRow >= 0 Then
                Dim obj As Object = GridView1.GetRow(ControlRow)
                If obj IsNot Nothing Then
                    BarButtonItem_Change.Caption = If(OkToEditNote(obj), "&Edit...", "&Show...")
                End If
            End If

            ' Finally, do the standard logic for the event.
            MyBase.PopupMenu_Items_Popup(sender, e)
        End Sub

        Protected Overrides Sub OnEditItem(ByVal obj As Object)
            Dim EditClass As New EditThreadClass(Context, DirectCast(obj, Note))
            AddHandler EditClass.Completed, AddressOf EditCompletion

            Dim ithrd As Thread = Nothing
            If Not OkToEditNote(obj) Then
                ithrd = New Thread(AddressOf EditClass.Show)
            Else
                ithrd = New Thread(AddressOf EditClass.Edit)
            End If

            ithrd.SetApartmentState(ApartmentState.STA)
            ithrd.Name = "EditNote"
            ithrd.IsBackground = True
            ithrd.Start()
        End Sub

        Protected Overrides Sub OnCreateItem()
            Dim EditClass As New EditThreadClass(Context, Nothing)
            AddHandler EditClass.Completed, AddressOf EditCompletion
            Dim ithrd As New Thread(AddressOf EditClass.Create)
            ithrd.SetApartmentState(ApartmentState.STA)
            ithrd.Name = "CreateNote"
            ithrd.IsBackground = True
            ithrd.Start()
        End Sub

        Protected Overrides Sub OnDeleteItem(ByVal obj As Object)
            Dim EditClass As New EditThreadClass(Context, DirectCast(obj, Note))
            AddHandler EditClass.Completed, AddressOf EditCompletion
            Dim ithrd As New Thread(AddressOf EditClass.Delete)
            ithrd.SetApartmentState(ApartmentState.STA)
            ithrd.Name = "DeleteNote"
            ithrd.Start()
        End Sub

        Private Sub EditCompletion(ByVal Sender As Object, ByVal e As EventArgs)

            ' Ensure that we are in the proper thread space.
            If InvokeRequired Then
                Dim ia As IAsyncResult = BeginInvoke(New EventHandler(AddressOf EditCompletion), New Object() {Sender, e})
                Dim Result As Object = EndInvoke(ia)
            Else
                ' Tell the world that the note list has been changed
                Context.ClientDs.EnvokeHandlers(Me, New FieldChangedEventArgs("client_notes"))

                ' Refresh the list of notes on the display if needed
                If TypeOf CType(FindForm(), forms.Form_ClientUpdate).FindCurrentPage Is Page_Notes Then
                    RefreshList()
                End If
            End If
        End Sub

        ''' <summary>
        ''' Do any custom formatting of fields for the current grid. We format the type to a string.
        ''' </summary>
        Private Sub GridView1_CustomColumnDisplayText(sender As Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs)

            ' Find the record being displayed
            Dim record As Note = TryCast(GridView1.GetRow(e.RowHandle), Note)
            If record Is Nothing Then
                Return
            End If

            ' If the column is the note type then format it properly
            If e.Column Is GridColumn_type Then
                Try
                    Dim noteType As DebtPlus.LINQ.InMemory.Notes.NoteTypes = CType(record.type, DebtPlus.LINQ.InMemory.Notes.NoteTypes)
                    e.DisplayText = noteType.ToString()
                Catch
                    e.DisplayText = "Unknown"
                End Try
            End If
        End Sub

        Private Class EditThreadClass
            Inherits Object
            Public Event Completed(ByVal Sender As Object, ByVal e As EventArgs)

            Private ReadOnly Context As ClientUpdateClass
            Private currentNote As Note

            Public Sub New(ByVal context As ClientUpdateClass, ByVal currentNote As Note)
                MyBase.New()
                Me.Context = context
                Me.currentNote = currentNote
            End Sub

            Public Sub Show()
                Dim Edited As Boolean = False

                ' From the source and ID, display the note.
                If currentNote.source = 1 Then ' these are client_notes
                    Using n As New DebtPlus.Notes.NoteClass.ClientNote()
                        If (String.Compare(currentNote.isArchived, "yes", True) = 0) Then
                            n.IsArchivedNote = True
                        Else
                            n.IsArchivedNote = False
                        End If
                        n.Show(currentNote.Id)
                    End Using
                Else ' these are disbursement_notes
                    Using n As New DebtPlus.Notes.NoteClass.DisbursementNote()
                        n.Show(currentNote.Id)
                    End Using
                End If
            End Sub

            Public Sub Edit()
                Dim Edited As Boolean = False

                If currentNote.source = 1 Then
                    Using n As New DebtPlus.Notes.NoteClass.ClientNote()
                        Edited = n.Edit(currentNote.Id)
                    End Using
                Else
                    Using n As New DebtPlus.Notes.NoteClass.DisbursementNote()
                        Edited = n.Edit(currentNote.Id)
                    End Using
                End If

                ' If the note was changed then refresh the list
                If Edited Then
                    RaiseEvent Completed(Me, EventArgs.Empty)
                End If
            End Sub

            Public Sub Create()

                ' Create the note. This may be a disbursement note or not but the notes procedure will handle the table differences then.
                Using NoteClass As New NoteClass.ClientNote()
                    If NoteClass.Create(Context.ClientDs.ClientId) Then
                        RaiseEvent Completed(Me, EventArgs.Empty)
                    End If
                End Using
            End Sub

            Public Sub Delete()
                Dim Edited As Boolean = False

                If currentNote.source = 1 Then
                    Using n As New NoteClass.ClientNote()
                        Edited = n.Delete(currentNote.Id)
                    End Using
                Else
                    Using n As New NoteClass.DisbursementNote()
                        Edited = n.Delete(currentNote.Id)
                    End Using
                End If

                ' If the record was changed then update the information in the display list
                If Edited Then
                    RaiseEvent Completed(Me, EventArgs.Empty)
                End If
            End Sub
        End Class
    End Class
End Namespace
