﻿Imports DebtPlus.UI.Client.controls
Imports DebtPlus.UI.Client.forms.Retention

Namespace pages

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Page_Retention
        Inherits ControlBaseClient

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit
            Me.RetentionControl1 = New RetentionControl
            CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'CheckEdit1
            '
            Me.CheckEdit1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.CheckEdit1.Location = New System.Drawing.Point(4, 269)
            Me.CheckEdit1.Name = "CheckEdit1"
            Me.CheckEdit1.Properties.Caption = "Show All Retention Events"
            Me.CheckEdit1.Size = New System.Drawing.Size(153, 19)
            Me.CheckEdit1.TabIndex = 0
            '
            'RetentionControl1
            '
            Me.RetentionControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                                                  Or System.Windows.Forms.AnchorStyles.Left) _
                                                 Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.RetentionControl1.Location = New System.Drawing.Point(6, 4)
            Me.RetentionControl1.Name = "RetentionControl1"
            Me.RetentionControl1.Size = New System.Drawing.Size(541, 259)
            Me.RetentionControl1.TabIndex = 1
            '
            'Page_Retention
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.RetentionControl1)
            Me.Controls.Add(Me.CheckEdit1)
            Me.Name = "Page_Retention"
            Me.Size = New System.Drawing.Size(550, 291)
            CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
        Friend WithEvents RetentionControl1 As RetentionControl
    End Class
End Namespace