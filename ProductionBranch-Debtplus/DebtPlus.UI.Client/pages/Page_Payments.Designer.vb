﻿Imports DebtPlus.UI.Client.controls

Namespace pages

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Page_Payments
        Inherits ControlBaseClient

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
            Me.ComboBoxEdit1 = New DevExpress.XtraEditors.ComboBoxEdit()
            Me.PaymentListControl1 = New PaymentListControl()
            CType(Me.ComboBoxEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PaymentListControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'LabelControl1
            '
            Me.LabelControl1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.LabelControl1.Location = New System.Drawing.Point(212, 212)
            Me.LabelControl1.Name = "LabelControl1"
            Me.LabelControl1.Size = New System.Drawing.Size(57, 13)
            Me.LabelControl1.TabIndex = 0
            Me.LabelControl1.Text = "Date Range"
            '
            'ComboBoxEdit1
            '
            Me.ComboBoxEdit1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.ComboBoxEdit1.Location = New System.Drawing.Point(275, 209)
            Me.ComboBoxEdit1.Name = "ComboBoxEdit1"
            Me.ComboBoxEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
            Me.ComboBoxEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
            Me.ComboBoxEdit1.Size = New System.Drawing.Size(298, 20)
            Me.ComboBoxEdit1.TabIndex = 1
            '
            'PaymentListControl1
            '
            Me.PaymentListControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                                                    Or System.Windows.Forms.AnchorStyles.Left) _
                                                   Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.PaymentListControl1.Location = New System.Drawing.Point(4, 4)
            Me.PaymentListControl1.Name = "PaymentListControl1"
            Me.PaymentListControl1.Size = New System.Drawing.Size(569, 199)
            Me.PaymentListControl1.TabIndex = 2
            '
            'Page_Payments
            '
            Me.Controls.Add(Me.PaymentListControl1)
            Me.Controls.Add(Me.ComboBoxEdit1)
            Me.Controls.Add(Me.LabelControl1)
            Me.Name = "Page_Payments"
            Me.Size = New System.Drawing.Size(576, 237)
            CType(Me.ComboBoxEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PaymentListControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
        Friend WithEvents ComboBoxEdit1 As DevExpress.XtraEditors.ComboBoxEdit
        Friend WithEvents PaymentListControl1 As PaymentListControl
    End Class
End Namespace