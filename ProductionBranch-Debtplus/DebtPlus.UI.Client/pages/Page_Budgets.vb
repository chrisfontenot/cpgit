#Region "Copyright 2000-2012 DebtPlus, L.L.C."

'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}

#End Region

Option Compare Binary
Option Explicit On
Option Strict On

Imports DebtPlus.Svc.DataSets
Imports DebtPlus.UI.Client.forms.Budgets
Imports DebtPlus.UI.Client.controls
Imports DebtPlus.Utils
Imports System.Threading
Imports DebtPlus.Data.Forms
Imports DebtPlus.Events
Imports System.Data.SqlClient

Namespace pages

    Friend Class Page_Budgets
        Inherits MyGridControlClient
        Private bc As DebtPlus.LINQ.BusinessContext = Nothing

        Public Sub New()
            MyBase.New()
            InitializeComponent()
            EnableMenus = True
        End Sub

        Public Overrides Sub ReadForm(bc As DebtPlus.LINQ.BusinessContext)
            Me.bc = bc
            RefreshBudgetList(False)
            UpdateTable = Context.ClientDs.BudgetsTable()
            UpdateTable.AcceptChanges()
        End Sub

        Protected Overrides Sub OnCreateItem()
            Dim vue As DataView = CType(GridControl1.DataSource, System.Data.DataView)
            OnEditItem(vue.AddNew)
        End Sub

        Protected Overrides Sub OnDeleteItem(obj As Object)
            Dim drv As System.Data.DataRowView = TryCast(obj, System.Data.DataRowView)
            If drv Is Nothing Then
                Return
            End If

            If DebtPlus.Data.Prompts.RequestConfirmation_Delete() <> DialogResult.Yes Then
                Return
            End If

            drv.Delete()
        End Sub

        Protected Overrides Sub OnEditItem(ByVal obj As Object)
            Dim drv As DataRowView = TryCast(obj, DataRowView)
            If drv IsNot Nothing Then
                Dim thrd As New Thread(New ParameterizedThreadStart(AddressOf EditThread))
                thrd.Name = "Budget"
                thrd.SetApartmentState(ApartmentState.STA)
                thrd.IsBackground = True
                thrd.Start(drv)
            End If
        End Sub

        Private Sub EditThread(ByVal obj As Object)
            Dim drv As DataRowView = CType(obj, DataRowView)

            If Not DesignMode Then
                drv.BeginEdit()

                Try
                    Using frm As New Form_Budget(Context, drv)
                        If frm.ShowDialog() = DialogResult.OK Then
                            ReloadBudgetList(drv)
                        Else
                            CancelBudgetList(drv)
                        End If
                    End Using

                Catch ex As System.Data.SqlClient.SqlException
                    MessageBox.Show(ex.ToString(), "Error processing the budget update", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    CancelBudgetList(drv)
                End Try
            End If
        End Sub

        Private Delegate Sub CancelBudgetListDelegate(ByVal drv As DataRowView)

        Private Sub CancelBudgetList(ByVal drv As DataRowView)
            If Not DesignMode Then
                If GridControl1.InvokeRequired Then
                    GridControl1.EndInvoke(GridControl1.BeginInvoke(New CancelBudgetListDelegate(AddressOf CancelBudgetList), New Object() {drv}))
                Else
                    drv.CancelEdit()
                    Context.ClientDs.EnvokeHandlers(drv.Row.Table, New FieldChangedEventArgs("reload_budgets"))
                End If
            End If
        End Sub

        Private Delegate Sub ReloadBudgetListDelegate(ByVal drv As DataRowView)

        Private Sub ReloadBudgetList(ByVal drv As DataRowView)
            If Not DesignMode Then

                ' If called from another thread then pass the buck to someone else to handle it.
                If GridControl1.InvokeRequired Then
                    GridControl1.EndInvoke(GridControl1.BeginInvoke(New ReloadBudgetListDelegate(AddressOf ReloadBudgetList), New Object() {drv}))
                Else
                    drv.EndEdit()

                    ' If the budget page is being shown then reload the form list.
                    ' If not, then don't bother as it will be reloaded when we come back.
                    If TypeOf CType(FindForm(), forms.Form_ClientUpdate).FindCurrentPage Is Page_Budgets Then
                        SaveForm(bc)
                        RefreshBudgetList(True)
                        UpdateTable = Context.ClientDs.BudgetsTable()
                        UpdateTable.AcceptChanges()
                    End If
                End If
            End If
        End Sub

        Private Sub RefreshBudgetList(ByVal Reload As Boolean)
            If Not DesignMode Then
                If Reload Then Context.ClientDs.RemoveBudgetsTable()
                GridControl1.DataSource = Context.ClientDs.BudgetsTable().DefaultView
                GridControl1.RefreshDataSource()
                GridView1.BestFitColumns()
            End If
        End Sub

        Public Overrides Sub SaveForm(bc As DebtPlus.LINQ.BusinessContext)
            If Not DesignMode Then

                ' Find the list of deleted rows
                Using vue As New DataView(UpdateTable, String.Empty, String.Empty, DataViewRowState.Deleted)
                    If vue.Count > 0 Then

                        ' Create a connection to the database
                        Dim cn As SqlConnection = New SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)
                        Dim current_cursor As Cursor = Cursor.Current
                        Dim txn As SqlTransaction = Nothing
                        Try
                            Cursor.Current = Cursors.WaitCursor
                            cn.Open()
                            txn = cn.BeginTransaction

                            ' Delete all of the items in the deleted view
                            For Each drv As DataRowView In vue
                                Dim Budget As Int32 = -1
                                If drv("budget") IsNot Nothing AndAlso drv("budget") IsNot DBNull.Value Then Budget = Convert.ToInt32(drv("budget"))
                                If Budget > 0 Then

                                    ' Delete the budget detail
                                    Using cmd As SqlCommand = New SqlCommand
                                        cmd.Connection = cn
                                        cmd.Transaction = txn
                                        cmd.CommandText = "DELETE FROM budget_detail WHERE budget=@budget"
                                        cmd.Parameters.Add("@budget", SqlDbType.Int).Value = Budget
                                        cmd.ExecuteNonQuery()
                                    End Using

                                    ' Delete the budget
                                    Using cmd As SqlCommand = New SqlCommand
                                        cmd.Connection = cn
                                        cmd.Transaction = txn
                                        cmd.CommandText = "DELETE FROM budgets WHERE budget=@budget"
                                        cmd.Parameters.Add("@budget", SqlDbType.Int).Value = Budget
                                        cmd.ExecuteNonQuery()
                                    End Using
                                End If
                            Next

                            UpdateTable.AcceptChanges()

                            ' Commit the changes to the disk
                            txn.Commit()
                            txn.Dispose()
                            txn = Nothing

                        Catch ex As SqlException
                            DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error deleting budgets")

                        Finally
                            If txn IsNot Nothing Then
                                Try
                                    txn.Rollback()
                                Catch exRollback As System.Data.SqlClient.SqlException
                                End Try
                                txn.Dispose()
                                txn = Nothing
                            End If

                            If cn IsNot Nothing Then cn.Dispose()
                            Cursor.Current = current_cursor
                        End Try
                    End If
                End Using
            End If
        End Sub
    End Class
End Namespace
